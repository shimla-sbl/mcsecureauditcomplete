﻿using SBL.DomainModel.Models.AuditTrail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Helpers;

namespace SBL.eLegistrator.HouseController.Filters
{
    public class AuditAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ////Stores the Request in an Accessible object
            var request = filterContext.HttpContext.Request;


            //Generate an audit
            Audit audit = new Audit()
            {
                UserName = (request.IsAuthenticated) ? filterContext.HttpContext.User.Identity.Name : "Anonymous",
                IPAddress = request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.ServerVariables["REMOTE_ADDR"],
                LoginDateTime = CurrentSession.LoginTime,
                LoginStatus = CurrentSession.LoginStatus,
                LogoutDateTime = Convert.ToDateTime("1753-01-01 00:00:00"),
                ModuleName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                ActionName = filterContext.ActionDescriptor.ActionName,                
                ActionType = filterContext.HttpContext.Request.HttpMethod,
                URL = request.RawUrl,
                ActionDate = Convert.ToDateTime(DateTime.Now)
            };

            //Stores the Audit in the Database
            if (CurrentSession.LoginStatus != "" && request.IsAjaxRequest()==false)
            Helper.ExecuteService("AuditTrail", "AddAudit", audit);

            //Finishes executing the Action as normal 
            base.OnActionExecuting(filterContext);
        }
    }
}