﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SBL.eLegistrator.HouseController.Web.Filters.Security
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public string UsersConfigKey { get; set; }

        public string RolesConfigKey { get; set; }

        protected virtual CustomPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                var authorizedUsers = ConfigurationManager.AppSettings[UsersConfigKey];
                var authorizedRoles = ConfigurationManager.AppSettings[RolesConfigKey];

                Users = String.IsNullOrEmpty(Users) ? authorizedUsers : Users;
                Roles = String.IsNullOrEmpty(Roles) ? authorizedRoles : Roles;

                if (!String.IsNullOrEmpty(Users))
                {
                    if (CurrentUser != null)
                    {
                        if (!CurrentUser.IsInRole(Users))
                        {
                            filterContext.Result = new RedirectToRouteResult(new
                         RouteValueDictionary(new { controller = "Error", action = "AccessDenied", area = "" }));

                            //base.OnAuthorization(filterContext); //returns to login url
                        }
                    }
                    else
                    {
                        base.OnAuthorization(filterContext); //returns to login url
                    }
                }

                if (!String.IsNullOrEmpty(Roles))
                {
                    if (CurrentUser != null)
                    {
                        if (!CurrentUser.IsInModule(Roles))
                        {
                            filterContext.Result = new RedirectToRouteResult(new
                         RouteValueDictionary(new { controller = "Error", action = "AccessDenied", area = "" }));

                            //base.OnAuthorization(filterContext); //returns to login url
                        }
                    }
                    else
                    {
                        base.OnAuthorization(filterContext); //returns to login url
                    }
                }
            }
        }
    }
}