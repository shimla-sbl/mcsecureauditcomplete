﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;

namespace SBL.eLegistrator.HouseController.Web.Filters.Security
{
    public class CustomPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role)
        {
            string[] _role = role.Split(',');
            if (roles.Any(r => _role.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsInModule(string module)
        {
            string[] _module = module.Split(',');
            if (modules.Any(r => _module.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public CustomPrincipal(string UserName)
        {
            this.Identity = new GenericIdentity(UserName);
        }

        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string[] roles { get; set; }//Its Define as array in case of one user  have multiple roles.
        public string[] modules { get; set; }
        public string UserType { get; set; }
    }
    public class CustomPrincipalSerializeModel
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string[] roles { get; set; }//Its Define as array in case of one user  have multiple roles.
        public string[] modules { get; set; }
        public string UserType { get; set; }
    }
}