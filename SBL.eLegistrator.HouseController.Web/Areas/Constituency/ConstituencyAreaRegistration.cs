﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Constituency
{
    public class ConstituencyAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Constituency";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Constituency_default",
                "Constituency/{controller}/{action}/{id}",
                new { action = "Index", Controller = "Mapping", id = UrlParameter.Optional }
            );
        }
    }
}
