﻿
using System.Collections.Generic;
namespace SBL.eLegistrator.HouseController.Web.Areas.Constituency.Models
{
    public class DepartmentWiseReport
    {
        public string DeptId { get; set; }
        public string DeptName { get; set; }
        public string TotalWorks { get; set; }
        public string SanctionedAmount { get; set; }
    }
    public class ConstituencyWiseWorkList
    {
        public string ConstituencyName { get; set; }
        public string TotalDiary { get; set; }
        public string TotalWorks { get; set; }
        public string TotalSanctionedPost { get; set; }
        public string TotalFilledPost { get; set; }
        public string OfficeName { get; set; }
        public string oTotalDiary { get; set; }
        public string oTotalWorks { get; set; }
        public string oTotalSanctionedPost { get; set; }
        public string oTotalFilledPost { get; set; }
        public int AssemblyId { get; set; }
        public int ConstituencyCode { get; set; }
        public int mCount { get; set; }
        public virtual ICollection<ConstituencyWiseWorkList> tConstituencyWiseWorkList { get; set; }
    }
}