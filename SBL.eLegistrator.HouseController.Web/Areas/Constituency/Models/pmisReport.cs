﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SBL.eLegistrator.HouseController.Web.Areas.Constituency.Models
{
    public class pmisListReport
    {
        [JsonProperty("DeptId")]
        public string DeptID { get; set; }

        [JsonProperty("DeptName")]
        public string DeptName { get; set; }

        public int? memberID { get; set; }

        public string memberName { get; set; }

        public string officeID { get; set; }

        public string officeName { get; set; }

        public int DesignationId { get; set; }

        public string DesignationName { get; set; }

        public string EmpCode { get; set; }

        public string Name { get; set; }

        public string Designation { get; set; }

        public string DateOfJoining { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        [JsonProperty("SanctionedPost")]
        public int SanctionedPost { get; set; }

        [JsonProperty("FilledPost")]
        public int? FilledPost { get; set; }

        [JsonProperty("VacantPost")]
        public int? VacantPost { get; set; }

        public string AadhaarId { get; set; }
    }

    public class PMISReport
    {
        public PMISReport()
        {
            List1 = new List<pmisListReport>();
            List2 = new List<pmisListReport>();
            List3 = new List<pmisListReport>();
            List4 = new List<pmisListReport>();
        }

        public List<pmisListReport> List1 { get; set; }

        public List<pmisListReport> List2 { get; set; }

        public List<pmisListReport> List3 { get; set; }

        public List<pmisListReport> List4 { get; set; }
    }



}