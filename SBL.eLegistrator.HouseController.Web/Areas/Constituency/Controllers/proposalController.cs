﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Constituency.Controllers
{
    public class proposalController : Controller
    {
        //
        // GET: /Constituency/proposal/

        public ActionResult Index()
        {
            return View();
        }
        #region Manage Proposal
        public ActionResult ManageProposals()
        {
            return PartialView("_ManageProposals");
        }
        #endregion

        #region Sanctioned Proposal
        public ActionResult SanctionedProposals()
        {
            return PartialView("_SanctionedProposals");
        }
        #endregion

    }
}
