﻿using Newtonsoft.Json;
using SBL.DomainModel.Models.PISModules;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Areas.Constituency.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Constituency.Controllers
{
    public class PMISController : Controller
    {
        //
        // GET: /Constituency/PMIS/

        string memberCode = CurrentSession.MemberCode;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PMISReport()
        {
            return PartialView("_PMISReport");
        }

        //public ActionResult GetPMISReport(string departmentID, string officeID)
        //{
        //    PMISReport report = new Models.PMISReport();
        //    report.List1 = (List<pmisListReport>)GetAllDepartmentPost("48");
        //    return PartialView("_PMISSearchResult", report);
        //}

        public ActionResult GetDepartmentPost()
        {
            PMISReport report = new Models.PMISReport();
            report.List1 = (List<pmisListReport>)GetAllDepartmentPost(memberCode);
            ViewBag.Header = "Department List";
            return PartialView("_List1", report.List1);

        }
        public ActionResult GetDepartmentPostForCM()
        {
            PMISReport report = new Models.PMISReport();
            report.List1 = (List<pmisListReport>)GetAllDepartmentPost(memberCode);
            ViewBag.Header = "Department List";
           // return PartialView(("/Areas/PaperLaidMinister/Views/PaperLaidMinister/_List1ForCM.cshtml", report.List1);
            return PartialView("/Areas/PaperLaidMinister/Views/PaperLaidMinister/_List1ForCM.cshtml",report.List1);
            

        }
        public ActionResult GetOfficePost(string departmentID)
        {
            PMISReport report = new Models.PMISReport();
            //   report.List1 = (List<pmisListReport>)GetAllDepartmentPost(memberCode.ToString());
            report.List2 = (List<pmisListReport>)GetAllOfficePost(departmentID, memberCode.ToString());
            ViewBag.DeptID = departmentID;
            ViewBag.Header = departmentID + " >> Offlice List";
            return PartialView("_list2", report.List2);
        }
        public ActionResult GetOfficePostForCM(string departmentID)
        {
            PMISReport report = new Models.PMISReport();
            //   report.List1 = (List<pmisListReport>)GetAllDepartmentPost(memberCode.ToString());
            report.List2 = (List<pmisListReport>)GetAllOfficePost(departmentID, memberCode.ToString());
            ViewBag.DeptID = departmentID;
            ViewBag.Header = departmentID + " >> Offlice List";
      
            return PartialView("/Areas/PaperLaidMinister/Views/PaperLaidMinister/_list2ForCM.cshtml", report.List2);
            
        }
        public ActionResult GetDesignationPost(string departmentID, string officeID)
        {
            PMISReport report = new Models.PMISReport();
            report.List3 = (List<pmisListReport>)GetAllDesignationPost(departmentID, officeID);
            ViewBag.DeptID = departmentID;
            ViewBag.officeID = officeID;
            ViewBag.Header = departmentID + " >> " + officeID + " >> Designation List";
            return PartialView("_list3", report.List3);
        }
        public ActionResult GetDesignationPostForCM(string departmentID, string officeID)
        {
            PMISReport report = new Models.PMISReport();
            report.List3 = (List<pmisListReport>)GetAllDesignationPost(departmentID, officeID);
            ViewBag.DeptID = departmentID;
            ViewBag.officeID = officeID;
            ViewBag.Header = departmentID + " >> " + officeID + " >> Designation List";
            return PartialView("/Areas/PaperLaidMinister/Views/PaperLaidMinister/_list3ForCM.cshtml", report.List3);
           // return PartialView("/PaperLaidMinister/PaperLaidMinister/_list3ForCM", report.List3);
        }
        public ActionResult GetOfficeEmployeePost(string departmentID, string officeID, string designation)
        {
            PMISReport report = new Models.PMISReport();
            report.List4 = (List<pmisListReport>)GetAllOfficeEmployeePost(departmentID, officeID, designation);
            ViewBag.Header = departmentID + " >> " + officeID + " >> " + designation + " >>  Employee List";
            return PartialView("_list4", report.List4);
        }
        public ActionResult GetOfficeEmployeePostForCM(string departmentID, string officeID, string designation)
        {
            PMISReport report = new Models.PMISReport();
            report.List4 = (List<pmisListReport>)GetAllOfficeEmployeePost(departmentID, officeID, designation);
            ViewBag.Header = departmentID + " >> " + officeID + " >> " + designation + " >>  Employee List";
            return PartialView("/Areas/PaperLaidMinister/Views/PaperLaidMinister/_list4ForCM.cshtml", report.List4);
            //return PartialView("_list4ForCM", report.List4);
        }



        #region Generic Function

        public List<pmisListReport> GetAllDepartmentPost(string memberCode)
        {
            if (memberCode == "")
            {
                if (CurrentSession.MemberCode != "")
                {
                    memberCode = CurrentSession.MemberCode;
                }
            }
            if (CurrentSession.SubDivisionId != "")
            {
                string requestUrl = "http://10.25.128.163/PMISWebServiceN/PMISWCFService.svc/GetAllDepartmentPost_ForSDM/" + CurrentSession.SubDivisionId + "";
               // string requestUrl = "http://localhost:51444/PMISWCFService.svc/GetAllDepartmentPost_ForSDM/" + CurrentSession.SubDivisionId + "";
          

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
                try
                {
                    WebResponse response = request.GetResponse();
                    List<pmisListReport> List1 = new List<pmisListReport>();
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                        string str = reader.ReadToEnd();

                        if (!str.Trim().Equals("\"No Data Found\""))
                        {
                            List1 = JsonConvert.DeserializeObject<List<pmisListReport>>(str);
                        }
                        return List1;
                    }
                }
                catch (WebException ex)
                {
                    WebResponse errorResponse = ex.Response;
                    using (Stream responseStream = errorResponse.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                        String errorText = reader.ReadToEnd();
                        return null;
                    }
                }
            }
            else
            {
               // string requestUrl = "http://localhost:51444/PMISWCFService.svc/GetAllDepartmentPost/" + memberCode + "";
               string requestUrl = "http://10.25.128.163/PMISWebServiceN/PMISWCFService.svc/GetAllDepartmentPost/" + memberCode + "";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
                try
                {
                    WebResponse response = request.GetResponse();
                    List<pmisListReport> List1 = new List<pmisListReport>();
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                        string str = reader.ReadToEnd();

                        if (!str.Trim().Equals("\"No Data Found\""))
                        {
                            List1 = JsonConvert.DeserializeObject<List<pmisListReport>>(str);
                        }
                        return List1;
                    }
                }
                catch (WebException ex)
                {
                    WebResponse errorResponse = ex.Response;
                    using (Stream responseStream = errorResponse.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                        String errorText = reader.ReadToEnd();
                        return null;
                    }
                }
            }
        }

        public List<pmisListReport> GetAllOfficePost(string departmentID, string memberCode)
        {
            if (CurrentSession.SubDivisionId != "")
            {
               string requestUrl = "http://10.25.128.163/PMISWebServiceN/PMISWCFService.svc/GetAllOfficePost_ForSDM/" + CurrentSession.SubDivisionId + "/" + departmentID;
                //string requestUrl = "http://localhost:51444/PMISWCFService.svc/GetAllOfficePost_ForSDM/" + CurrentSession.SubDivisionId + "/" + departmentID;
          
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
                try
                {

                    List<pmisListReport> List1 = new List<pmisListReport>();
                    WebResponse response = request.GetResponse();
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                        string str = reader.ReadToEnd();

                        if (!str.Trim().Equals("\"No Data Found\""))
                        {
                            List1 = JsonConvert.DeserializeObject<List<pmisListReport>>(str);
                        }

                        return List1;
                    }
                }
                catch (WebException ex)
                {
                    WebResponse errorResponse = ex.Response;
                    List<pmisListReport> List1 = new List<pmisListReport>();
                    WebResponse response = request.GetResponse();
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                        string str = reader.ReadToEnd();

                        if (!str.Trim().Equals("\"No Data Found\""))
                        {
                            List1 = JsonConvert.DeserializeObject<List<pmisListReport>>(str);
                        }

                        return List1;
                    }
                }
            }
            else
            {
              //  string requestUrl = "http://localhost:51444/PMISWCFService.svc/GetAllOfficePost/" + memberCode + "/" + departmentID;
              string requestUrl = "http://10.25.128.163/PMISWebServiceN/PMISWCFService.svc/GetAllOfficePost/" + memberCode + "/" + departmentID;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
                try
                {

                    List<pmisListReport> List1 = new List<pmisListReport>();
                    WebResponse response = request.GetResponse();
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                        string str = reader.ReadToEnd();

                        if (!str.Trim().Equals("\"No Data Found\""))
                        {
                            List1 = JsonConvert.DeserializeObject<List<pmisListReport>>(str);
                        }

                        return List1;
                    }
                }
                catch (WebException ex)
                {
                    WebResponse errorResponse = ex.Response;
                    List<pmisListReport> List1 = new List<pmisListReport>();
                    WebResponse response = request.GetResponse();
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                        string str = reader.ReadToEnd();

                        if (!str.Trim().Equals("\"No Data Found\""))
                        {
                            List1 = JsonConvert.DeserializeObject<List<pmisListReport>>(str);
                        }

                        return List1;
                    }
                }
            }
        }

        public List<pmisListReport> GetAllDesignationPost(string departmentID, string officeID)
        {
            //string requestUrl = "http://localhost:51444/PMISWCFService.svc/GetAllDesignationPost/" + departmentID + "/" + officeID;
            string requestUrl = "http://10.25.128.163/PMISWebServiceN/PMISWCFService.svc/GetAllDesignationPost/" + departmentID + "/" + officeID;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
            try
            {
                WebResponse response = request.GetResponse();
                List<pmisListReport> List1 = new List<pmisListReport>();

                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    string str = reader.ReadToEnd();

                    if (!str.Trim().Equals("\"No Data Found\""))
                    {
                        List1 = JsonConvert.DeserializeObject<List<pmisListReport>>(str);
                    }

                    return List1;
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    return null;
                }
            }
        }

        public List<pmisListReport> GetAllOfficeEmployeePost(string departmentID, string officeID, string DesignationID)
        {
           // string requestUrl = "http://localhost:51444/PMISWCFService.svc/GetAllOfficeEmployeePost/" + departmentID + "/" + officeID + "/" + DesignationID;
            string requestUrl = "http://10.25.128.163/PMISWebServiceN/PMISWCFService.svc/GetAllOfficeEmployeePost/" + departmentID + "/" + officeID + "/" + DesignationID;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
            try
            {
                List<pmisListReport> List1 = new List<pmisListReport>();
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    string str = reader.ReadToEnd();

                    if (!str.Trim().Equals("\"No Data Found\""))
                    {
                        List1 = JsonConvert.DeserializeObject<List<pmisListReport>>(str);
                    }

                    return List1;
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    return null;
                }
            }
        }

        #endregion Generic Function

        public ActionResult getEmpDetails(string AdhaarID)
        {
            tPISEmployeePersonal PisEmpdetails = new tPISEmployeePersonal();

            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@adhadr_id", AdhaarID));
            DataSet dataSetUser = new DataSet();
            dataSetUser = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetEmpDetails_Sel", methodParameter);
            if (dataSetUser != null && dataSetUser.Tables.Count > 0 && dataSetUser.Tables[0].Rows.Count > 0)
            {
                PisEmpdetails.AadharID = dataSetUser.Tables[0].Rows[0]["AadharID"].ToString();
                PisEmpdetails.empcd = dataSetUser.Tables[0].Rows[0]["empcd"].ToString();
                PisEmpdetails.empfname = dataSetUser.Tables[0].Rows[0]["empfname"].ToString();
                PisEmpdetails.empfmh = dataSetUser.Tables[0].Rows[0]["empfmh"].ToString();
                PisEmpdetails.age = Convert.ToInt32(dataSetUser.Tables[0].Rows[0]["age"].ToString());
                PisEmpdetails.DateOFBirth = dataSetUser.Tables[0].Rows[0]["empdob"].ToString();
                PisEmpdetails.empgender = dataSetUser.Tables[0].Rows[0]["empgender"].ToString();
                PisEmpdetails.Mobile = Convert.ToInt64(dataSetUser.Tables[0].Rows[0]["Mobile"].ToString());
                PisEmpdetails.Email = dataSetUser.Tables[0].Rows[0]["Email"].ToString();
                PisEmpdetails.Address = dataSetUser.Tables[0].Rows[0]["Address"].ToString();
                PisEmpdetails.PinCode = Convert.ToInt64(dataSetUser.Tables[0].Rows[0]["PinCode"].ToString());
                PisEmpdetails.emphmdist = dataSetUser.Tables[0].Rows[0]["emphmdist"].ToString();
                PisEmpdetails.empphoto = dataSetUser.Tables[0].Rows[0]["empphoto"].ToString();
                PisEmpdetails.OfficeName = dataSetUser.Tables[0].Rows[0]["officename"].ToString();
                PisEmpdetails.DepartmentName = dataSetUser.Tables[0].Rows[0]["deptname"].ToString();
                PisEmpdetails.DateOFJoining = dataSetUser.Tables[0].Rows[0]["dofjoin"].ToString();
                PisEmpdetails.DesignationName = dataSetUser.Tables[0].Rows[0]["PDesignationId"].ToString();
                PisEmpdetails.empblood = dataSetUser.Tables[0].Rows[0]["empblood"].ToString();
            }
            return PartialView("_empDetails", PisEmpdetails);
        }

    }
}