﻿using Newtonsoft.Json;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.ComplexModel.Constituency;
using SBL.DomainModel.Models.ConstituencyVS;
using SBL.DomainModel.Models.Grievance;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Constituency.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.eFile;
using EvoPdf;
using iTextSharp.text;
using System.Linq;
using SBL.DomainModel.Models.Assembly;
using System.Collections.Generic;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.ComplexModel.Conzstituency;

namespace SBL.eLegistrator.HouseController.Web.Areas.Constituency.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class ConstituencyController : Controller
    {
        //
        // GET: /Constituency/Constituency/
        public string myConsituency = CurrentSession.ConstituencyID;

        public string MemberCode = CurrentSession.MemberCode;
        public string currentUser = CurrentSession.UserID;
        public string aadharID = CurrentSession.AadharId;
        public string currentAssemby = CurrentSession.AssemblyId;

        public ActionResult Index()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            return RedirectToAction("index", "Mapping");
        }

        [HttpGet]

        #region generic function

        public ActionResult getFinancialYearList()
        {

            string CurrentAssembly = Convert.ToString(CurrentSession.AssemblyId);
            string strAssemblyPeriod = "";
            int StartYear = 0;
            int EndYear = 0;
            if (CurrentAssembly != "")
            {
                strAssemblyPeriod = (String)Helper.ExecuteService("Assembly", "GetAssemblyPeriodByAssemblyCode", CurrentAssembly);
                if (strAssemblyPeriod != "")
                {
                    StartYear = Convert.ToInt16(Convert.ToString(strAssemblyPeriod.Substring(1, 4)));
                    if (strAssemblyPeriod.Substring(6) == ")")
                    {
                        EndYear = 0;
                    }
                    else
                    {
                        EndYear = Convert.ToInt16(Convert.ToString(strAssemblyPeriod.Substring(6, 4)));
                    }
                }
            }

            if (EndYear == 0)
            {
                EndYear = DateTime.Today.Year;
            }
            List<financialYearList> list1 = new List<financialYearList>();
            for (int i = StartYear; i < EndYear; i++)  //StartYear -1
            {
                financialYearList _list1 = new financialYearList();
                int NexYear = i + 1;
                string FinYearText = null;
                string FinYearValue = null;
                FinYearText = string.Format(" {0} - {1}", i, NexYear);
                FinYearValue = string.Format("{0}-{1}", i, NexYear);
                _list1.yearText = FinYearText;
                _list1.yearValue = FinYearValue;
                list1.Add(_list1);
            }



            //List<financialYearList> list = new List<financialYearList>();
            //for (int i = 0; i >= -5; i--)
            //{
            //    financialYearList _list = new financialYearList();
            //    int CurrentYear = DateTime.Today.AddYears(i).Year;
            //    int PreviousYear = DateTime.Today.AddYears(i).Year - 1;
            //    int NextYear = DateTime.Today.AddYears(i).Year + 1;
            //    string PreYear = PreviousYear.ToString();
            //    string NexYear = NextYear.ToString();
            //    string CurYear = CurrentYear.ToString();
            //    string FinYearText = null;
            //    string FinYearValue = null;
            //    if (DateTime.Today.Month > 3)
            //    {
            //        FinYearText = string.Format(" {0} - {1}", CurYear, NexYear);
            //        FinYearValue = string.Format("{0}-{1}", CurYear, NexYear);
            //    }
            //    else
            //    {
            //        FinYearText = string.Format(" {0} - {1}", PreYear, CurYear);
            //        FinYearValue = string.Format("{0}-{1}", PreYear, CurYear);
            //    }
            //    _list.yearText = FinYearText;
            //    _list.yearValue = FinYearValue;
            //    list.Add(_list);
            // }

            return Json(list1, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getFinancialYearListAdvance()
        {
            List<financialYearList> list = new List<financialYearList>();
            for (int i = 2; i >= -5; i--)
            {
                financialYearList _list = new financialYearList();
                int CurrentYear = DateTime.Today.AddYears(i).Year;
                int PreviousYear = DateTime.Today.AddYears(i).Year - 1;
                int NextYear = DateTime.Today.AddYears(i).Year + 1;
                string PreYear = PreviousYear.ToString();
                string NexYear = NextYear.ToString();
                string CurYear = CurrentYear.ToString();
                string FinYearText = null;
                string FinYearValue = null;
                if (DateTime.Today.Month > 3)
                {
                    FinYearText = string.Format(" {0} - {1}", CurYear, NexYear);
                    FinYearValue = string.Format("{0}-{1}", CurYear, NexYear);
                }
                else
                {
                    FinYearText = string.Format(" {0} - {1}", PreYear, CurYear);
                    FinYearValue = string.Format("{0}-{1}", PreYear, CurYear);
                }
                _list.yearText = FinYearText;
                _list.yearValue = FinYearValue;
                list.Add(_list);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllProgramme()
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getAllProgramme", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAllWorkStatus()
        {
            List<fillListGenricLong> _list = (List<fillListGenricLong>)Helper.ExecuteService("ConstituencyVS", "getAllWorkStatus", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAllWorkNature()
        {
            List<fillListGenricLong> _list = (List<fillListGenricLong>)Helper.ExecuteService("ConstituencyVS", "getAllWorkNature", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAllWorkType()
        {
            List<fillListGenricLong> _list = (List<fillListGenricLong>)Helper.ExecuteService("ConstituencyVS", "getAllWorkType", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult getAllControllingAuthority(string DistrictID)
        {
            List<fillListGenric> _list = (List<fillListGenric>)Helper.ExecuteService("ConstituencyVS", "getAllControllingAuthority", DistrictID);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetExecutiveOffice(string ExecutiveDepartment, string memberCode)
        {
            //MemberCode = "0";
            if (string.IsNullOrEmpty(MemberCode))
            {
                MemberCode = memberCode;
            }
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetExecutiveOffice", new string[] { ExecutiveDepartment, MemberCode });
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getConstituencyDistrict(string constituencyID)
        {
            if (constituencyID == "" || constituencyID == null || constituencyID == "0")
            {
                constituencyID = CurrentSession.ConstituencyID;
                string[] str = new string[2];
                str[0] = CurrentSession.AssemblyId;
                str[1] = constituencyID;
                string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
                if (strOutput != null)
                {
                    constituencyID = Convert.ToString(strOutput[0]);
                    CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                }
            }

            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyDistrict", constituencyID);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getDistrictConstituency(string districtID)
        {
            if (districtID == "" || districtID == "0" || districtID == null)
            {
                districtID = CurrentSession.DistrictCode;
                if (districtID == "")
                {
                    districtID = "0";
                }

            }
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getDistrictConstituency", districtID);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getConstituencyForMLA_SDM(string constituencyID)
        {
            //if (constituencyID == "" || constituencyID == null || constituencyID == "0")
            //{
            //    constituencyID = CurrentSession.ConstituencyID;
            //}

            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyForMLA_SDM", constituencyID);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSubdivisionForSDM(string Subdivision)
        {
            if (Subdivision == "" || Subdivision == null || Subdivision == "0")
            {
                Subdivision = CurrentSession.SubDivisionId;
            }

            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getSubdivisionForSDM", Subdivision);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getSubdivisionForUser(string DisttId)
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getSubdivisionForUser", DisttId);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }


        public ActionResult getConstituencyPanchayat(string constituencyID)
        {
            if (constituencyID == "" || constituencyID == null || constituencyID == "0")
            {
                if (CurrentSession.ConstituencyID != "" && CurrentSession.ConstituencyID != null)
                {
                    constituencyID = CurrentSession.ConstituencyID;
                }
            }
            //if (constituencyID == "" || constituencyID == null || constituencyID == "0")
            //{
            //    string[] str = new string[2];
            //    str[0] = CurrentSession.AssemblyId;
            //    str[1] = CurrentSession.ConstituencyID;
            //    string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
            //    if (strOutput != null)
            //    {
            //        constituencyID = Convert.ToString(strOutput[0]);
            //        CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
            //    }

            //}
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyPanchayat", constituencyID);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getConstituencyByDistrictId(string DistrictID)
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyByDistrictID", DistrictID);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllDemand()
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetAllDemand", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllDistrict()
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getAllDistrict", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDistrictForDC(string DisttCode)
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetDistrictForDC", DisttCode);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllMlaListforWorks()   // by madhur
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetAllMlaListforWorks", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllSubOfficeList()  // by madhur
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetAllSubOfficeList", CurrentSession.OfficeId);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAllPanchayat()
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getAllPanchayat", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAllDepartmentBase(string DepartMent)
        {

            List<fillListGenric> _list = (List<fillListGenric>)Helper.ExecuteService("ConstituencyVS", "getAllDepartmentBase", DepartMent);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getAllDepartment()
        {

            List<fillListGenric> _list = (List<fillListGenric>)Helper.ExecuteService("ConstituencyVS", "getAllDepartment", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getWorkDepartment()
        {

            List<fillListGenric> _list = (List<fillListGenric>)Helper.ExecuteService("ConstituencyVS", "getWorkDepartment", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getAllmemberDepartment(string memberCode)
        {
            if (string.IsNullOrEmpty(MemberCode))
            {
                MemberCode = memberCode;
            }
            if(MemberCode =="3")
            {

            }
            List<fillListGenric> _list = (List<fillListGenric>)Helper.ExecuteService("ConstituencyVS", "memberDepartment", new string[] { MemberCode, currentAssemby });
            if(_list.Count ==0)
            {
                _list = (List<fillListGenric>)Helper.ExecuteService("ConstituencyVS", "memberDepartment", new string[] { "0", currentAssemby });
            }
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAllOfficerDepartment(string officeID)
        {
            List<fillListGenric> _list = (List<fillListGenric>)Helper.ExecuteService("ConstituencyVS", "getAllOfficerDepartment", officeID);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDistrictForOffice(int OfficeId) // Madhur
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("Diary", "GetDistrictForOffice", OfficeId);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllOfficemappedByMemberCode(int MemberCode) // Madhur
        {
            List<SBL.DomainModel.Models.Office.mOffice> _list = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ConstituencyVS", "GetAllOfficemappedByMemberCode", MemberCode);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult get_subdevision(string constituencyID)
        {
            string SubId = "0";
            if (constituencyID == "")
            {
                constituencyID = "0";
            }
            //  int Test = Convert.ToInt32(constituencyID);
            string Test = constituencyID;
            string subid = CurrentSession.SubDivisionId;
            if (subid == null)
            {
                subid = "";
            }
            if (CurrentSession.SubDivisionId != "" && CurrentSession.SubDivisionId != null)
            {
                SubId = CurrentSession.SubDivisionId.ToString();
                List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "get_subdevision_SDM", SubId);
                return Json(_list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "get_subdevision", Test);
                return Json(_list, JsonRequestBehavior.AllowGet);
            }
            //   List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "get_subdevision", new string[] { Test, SubId });

        }
        public ActionResult fillSchemeType()
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "get_SchemeType", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getConstituencyNameByID()
        {
            WorkDetails work = new WorkDetails();
            string asCode = CurrentSession.AssemblyId;
            // if (constituencyID != "" || constituencyID != "0" || constituencyID != null)
            // {
            string constituencyID = "0";
            // CurrentSession.ConstituencyID;
            // }
            if (asCode == "" || asCode == null)
            {
                asCode = "0";
            }
            work.AssemblyId = Convert.ToInt16(asCode);
            work.ConId_ForSDM_ = constituencyID;
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetConstituencyNameByID", work);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getworkDetailsByUpdateID(string updateID)
        {
            // ErrorLog.WriteToLog(null, "updateID : " + updateID);
            WorkDetails _scheme = (WorkDetails)Helper.ExecuteService("ConstituencyVS", "getworkDetailsByUpdateID", updateID);
            // ErrorLog.WriteToLog(null, "SchemeID : " + _scheme.mySchemeID);
            workBasicView details = (workBasicView)Helper.ExecuteService("ConstituencyVS", "getBasicDetailsOFWorks", _scheme.mySchemeID);
            if (details != null)
            {
                ViewBag.wordCode = details;

                ViewBag.financialYear = details.financialYear;
                ViewBag.workName = details.workName;
                ViewBag.workStatus = details.workStatus;
            }
            List<ConstituencyWorkProgressImages> list = (List<ConstituencyWorkProgressImages>)Helper.ExecuteService("ConstituencyVS", "getAllProgressImages", _scheme.mySchemeID);
            _scheme.Imagelist = list;
            return PartialView("_workDetails", _scheme);
        }


        //_pWorksAllUpdate

        #endregion generic function

        #region constituency Mapping

        public ActionResult constituecyMapping()
        {
            return PartialView("_constituecyMapping");
        }

        public ActionResult GetFilterSchemeList(string District, string Demand, string ControllingAuthority, string FinancialYear)
        {
            string[] str = new string[2];
            str[0] = CurrentSession.AssemblyId;
            str[1] = myConsituency;
            string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
            if (strOutput != null)
            {
                myConsituency = Convert.ToString(strOutput[0]);
                CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
            }

            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyDistrict", myConsituency);
            schemeMapping _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredVSSchemeList", new string[] { District, Demand, ControllingAuthority, FinancialYear, myConsituency, _list[0].value.ToString() });
            return PartialView("_constituecyMapping", _scheme);
        }

        public ActionResult saveConstituencytoMyList(string checkedList)
        {
            Helper.ExecuteService("ConstituencyVS", "saveConstituencytoMyList", new string[] { checkedList, myConsituency, currentUser, MemberCode });
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult deleteConstituencyfromMyList(string checkedList)
        {
            Helper.ExecuteService("ConstituencyVS", "deleteConstituencyfromMyList", checkedList);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        #endregion constituency Mapping

        #region my constituency
        const int recordsPerPage = 8;
        //public ActionResult MyConstituencyWorks()//
        //{
        //    schemeMapping _scheme = new schemeMapping();
        //    _scheme.eConstituencyID = Convert.ToInt64(myConsituency);
        //    List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyDistrict", myConsituency);
        //    _scheme.districtID = Convert.ToInt64(_list[0].value.ToString());
        //    _scheme.memberCode = string.IsNullOrEmpty(MemberCode) ? 0 : Convert.ToInt32(MemberCode);
        //    return PartialView("_MyConstituencyWorks", _scheme);
        //}
        public ActionResult MyConstituencyWorks()
        {
            schemeMapping _scheme = new schemeMapping();
            _scheme.eConstituencyID = Convert.ToInt64(myConsituency);
            string[] str = new string[2];
            str[0] = CurrentSession.AssemblyId;
            str[1] = myConsituency;
            string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
            if (strOutput != null)
            {
                myConsituency = Convert.ToString(strOutput[0]);
                CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
            }

            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyDistrict", myConsituency);
            if (_list != null && _list.Count > 0)
            { _scheme.districtID = Convert.ToInt64(_list[0].value.ToString()); }
            else { _scheme.districtID = Convert.ToInt64(0); }
            _scheme.memberCode = string.IsNullOrEmpty(MemberCode) ? 0 : Convert.ToInt32(MemberCode);
            _scheme.eConstituencyID = Convert.ToInt32( CurrentSession.UserTypeConstituencyId);
            return PartialView("_MyConstituencyWorks", _scheme);
        }
        //getLatestUpdate
        public ActionResult getLatestUpdate(string pages)
        {

            List<workTrans> wList = (List<workTrans>)Helper.ExecuteService("ConstituencyVS", "getLatestUpdate", new string[] { MemberCode, pages });
            return PartialView("_pWorkUpdates", wList);
        }

        public ActionResult getAllUpdate()
        {
            List<workTrans> wList = (List<workTrans>)Helper.ExecuteService("ConstituencyVS", "getAllLatestUpdate", MemberCode);
            return PartialView("_pWorksAllUpdate", wList);
        }
        //updateLatestUpdate
        public ActionResult updateLatestUpdate(string autoid)
        {
            Helper.ExecuteService("ConstituencyVS", "updateWorkTrans", autoid);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult updateexcutingagency(string schemeID, string mode)
        {
            if (mode.ToLower() != "all")
            {
                workBasicView details = (workBasicView)Helper.ExecuteService("ConstituencyVS", "getBasicDetailsOFWorks", schemeID);
                if (details != null)
                {
                    ViewBag.workCode = details.workCode;
                    ViewBag.financialYear = details.financialYear;
                    ViewBag.workName = details.workName;
                    ViewBag.workStatus = details.workStatus;
                }
            }
            string[] arr = Helper.ExecuteService("ConstituencyVS", "getExecutingDepartmentandOffice", mode.ToLower() == "all" ? "0" : schemeID) as string[];
            return PartialView("_updateexcutingagency", new string[] { schemeID, MemberCode, string.IsNullOrEmpty(arr[0]) ? "0" : arr[0], string.IsNullOrEmpty(arr[1]) ? "0" : arr[1], mode });
        }

        public ActionResult constituencyReport()
        {
            string PassesDeptmement = ""; string PassedOffice = ""; string DistrictCode = "";

            var methodParameter = new List<KeyValuePair<string, string>>();
            schemeMapping _scheme = new schemeMapping();
            var santioned1 = _scheme.mySchemeList;
            if(CurrentSession.DeptID =="")
            {
                CurrentSession.DeptID = "0";

            }
            //if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "7")
            //{
            //    if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
            //    {
            //        ViewBag.DeptID = CurrentSession.DeptID;
            //        if (string.IsNullOrEmpty(CurrentSession.UserID))
            //        {
            //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
            //        }
            //        MlaDiary model1 = new MlaDiary();
            //        model1.UserofcId = CurrentSession.OfficeId;
            //        var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);

            //        var Agency = CurrentSession.DeptID;
            //        _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
            //        santioned1 = _scheme.mySchemeList.Where(x => Officelist.Any(y => y.OfficeId == Convert.ToInt16(x.ExecutiveOfficeId))).ToList();
            //        methodParameter.Add(new KeyValuePair<string, string>("@Department", Agency));
            //        methodParameter.Add(new KeyValuePair<string, string>("@OfficeId", model1.UserofcId));
            //        PassesDeptmement = CurrentSession.DeptID;
            //        PassedOffice = CurrentSession.OfficeId;
            //        DistrictCode = CurrentSession.DistrictCode;
            //    }
            //}


            //if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "2")
            //{
            //    if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
            //    {
            //        ViewBag.DeptID = CurrentSession.DeptID;
            //        if (string.IsNullOrEmpty(CurrentSession.UserID))
            //        {
            //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
            //        }
            //        MlaDiary model1 = new MlaDiary();
            //        var Agency = CurrentSession.DeptID;
            //        _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
            //        santioned1 = _scheme.mySchemeList.ToList();
            //        methodParameter.Add(new KeyValuePair<string, string>("@Department", Agency));
            //        PassesDeptmement = CurrentSession.DeptID;
            //        PassedOffice = CurrentSession.OfficeId;
            //        DistrictCode = CurrentSession.DistrictCode;
            //    }
            //}

            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "0" || string.IsNullOrEmpty(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId))
            {
                if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
                {
                    ViewBag.DeptID = CurrentSession.DeptID;
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    MlaDiary model1 = new MlaDiary();
                    //CurrentSession.DeptID = "0";
                    //CurrentSession.OfficeId = "0";
                    //CurrentSession.DistrictCode = "0";
                    var Agency = CurrentSession.DeptID;
                    if(Agency.Contains (","))
                    {

                        Agency = "0";

                    }
                    PassedOffice = CurrentSession.OfficeId;
                    if (!string.IsNullOrEmpty(PassedOffice) && PassedOffice != "0")
                    {
                        _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency, PassedOffice });
                    }
                    else
                    {
                        _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
                    }

                    methodParameter.Add(new KeyValuePair<string, string>("@Department", Agency));
                    santioned1 = _scheme.mySchemeList.ToList();
                    PassesDeptmement = Agency;

                    DistrictCode = "0";
                }
            }
            //if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "-1")
            //{
            //    if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
            //    {
            //        ViewBag.DeptID = CurrentSession.DeptID;
            //        if (string.IsNullOrEmpty(CurrentSession.UserID))
            //        {
            //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
            //        }
            //        MlaDiary model1 = new MlaDiary();
            //        //CurrentSession.DeptID = "0";
            //        //CurrentSession.OfficeId = "0";
            //        //CurrentSession.DistrictCode = "0";
            //        var Agency = "0";
            //        _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
            //        methodParameter.Add(new KeyValuePair<string, string>("@Department", null));
            //        santioned1 = _scheme.mySchemeList.ToList();
            //        PassesDeptmement = "0";
            //        PassedOffice = "0";
            //        DistrictCode = "0";
            //    }
            //}


           else if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId!="0")
            {
                if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
                {
                    //ViewBag.DeptID = CurrentSession.DeptID;
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    //MlaDiary model1 = new MlaDiary();
                    //model1.UserofcId = CurrentSession.OfficeId;
                    //var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);

                    //var distt = CurrentSession.DistrictCode;

                    //DiaryModel dobj = new DiaryModel();
                    //dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                    //dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                    //dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
                    var Consttituency = CurrentSession.UserTypeConstituencyId;
                
                    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType2Count", new string[] { Consttituency });
                    santioned1 = _scheme.mySchemeList.ToList();
                    methodParameter.Add(new KeyValuePair<string, string>("@Consttituency", Consttituency));
                    PassesDeptmement = CurrentSession.DeptID;
                    PassedOffice = CurrentSession.OfficeId;
                    DistrictCode = CurrentSession.DistrictCode;

                }
            }



            //if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "8")
            //{
            //    if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
            //    {
            //        ViewBag.DeptID = CurrentSession.DeptID;
            //        if (string.IsNullOrEmpty(CurrentSession.UserID))
            //        {
            //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
            //        }
            //        var distt = CurrentSession.DistrictCode;
            //        MlaDiary model1 = new MlaDiary();
            //        PassesDeptmement = "0";
            //        PassedOffice = "0";
            //        _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType1Count", new string[] { distt });
            //        methodParameter.Add(new KeyValuePair<string, string>("@distt", distt));
            //        santioned1 = _scheme.mySchemeList.ToList();
            //        DistrictCode = CurrentSession.DistrictCode;
            //    }
            //}


            //if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "9")
            //{
            //    if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
            //    {
            //        ViewBag.DeptID = CurrentSession.DeptID;
            //        if (string.IsNullOrEmpty(CurrentSession.UserID))
            //        {
            //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
            //        }
            //        var SubDivisionId = CurrentSession.SubDivisionId;
            //        MlaDiary model1 = new MlaDiary();
            //        _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeSDMCount", new string[] { SubDivisionId });
            //        methodParameter.Add(new KeyValuePair<string, string>("@SubDivisionId", SubDivisionId));
            //        santioned1 = _scheme.mySchemeList.ToList();
            //        PassesDeptmement = CurrentSession.DeptID;
            //        PassedOffice = CurrentSession.OfficeId;
            //        DistrictCode = CurrentSession.DistrictCode;
            //    }
            //}


            DataSet dataSetUser = new DataSet();
            dataSetUser = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "getAllLatestProgressImages", methodParameter);
            DataSet dataSetUser1 = new DataSet();
            dataSetUser1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "getAllLatestwork", methodParameter);

            var empList = dataSetUser.Tables[0].AsEnumerable()
             .Select(dataRow => new ConstituencyWorkProgressImages
             {
                 filePath = dataRow.Field<string>("filePath"),
                 fileName = dataRow.Field<string>("fileName"),
                 tFileCaption = dataRow.Field<string>("workName"),
                 UploadedDate = dataRow.Field<DateTime>("UploadedDate"),
                 constituencyCode = dataRow.Field<Int32>("Number"),
                 schemeCode = dataRow.Field<long>("schemeCode"),
                 ConstituencyName = dataRow.Field<string>("ConstituencyName"),
                 ControllingAuthoritydept = dataRow.Field<string>("ControllingAuthoritydept"),
                 DepartmentIdOwner = dataRow.Field<string>("DepartmentIdOwner"),
                 workStatus = dataRow.Field<string>("workStatus"),
             }).ToList();



            var getAllLatestworkList = dataSetUser1.Tables[0].AsEnumerable()
           .Select(dataRow => new myShemeList()
           {
               workName = dataRow.Field<string>("workName"),
               ExecutiveDepartment = dataRow.Field<string>("DepartmentName"),
               ExecutiveOffice = dataRow.Field<string>("ExecutingAgency"),
               workStatus = dataRow.IsNull("WorkStatus") ? "" : dataRow.Field<string>("WorkStatus"),
               schemeTypeId = dataRow.IsNull("Physical Progress till Date") ? 0 : dataRow.Field<int>("Physical Progress till Date"),
               physicalProgress = dataRow.IsNull("financial Progress") ? 0 : dataRow.Field<double>("financial Progress"),
               sanctionedDate = dataRow.IsNull("LastUpdatedDate") ? "" : Convert.ToDateTime(dataRow.Field<DateTime>("LastUpdatedDate")).ToString("dd/MM/yyyy"),
           }).ToList();
            ViewBag.LatestUpdate = getAllLatestworkList;
            ViewBag.Images = empList;



            var santioned = santioned1.Where(x => x.workStatusCode == 1).Count();
            var Inprogress = santioned1.Where(x => x.workStatusCode == 2).Count();
            var Completed = santioned1.Where(x => x.workStatusCode == 3).Count();
            var PendingCourt = santioned1.Where(x => x.workStatusCode == 5).Count();
            var Canceled = santioned1.Where(x => x.workStatusCode == 4).Count();
            var PendingClear = santioned1.Where(x => x.workStatusCode == 6).Count();
            var PendingClearDPR = santioned1.Where(x => x.workStatusCode == 7).Count();

            return PartialView("workReport", new string[] { PassedOffice, PassesDeptmement, CurrentSession.UserTypeConstituencyId, DistrictCode, Convert.ToString(santioned), Convert.ToString(Inprogress), Convert.ToString(Completed), Convert.ToString(PendingCourt), Convert.ToString(Canceled), Convert.ToString(PendingClear), Convert.ToString(PendingClearDPR) });
        }

        public ActionResult SchemaReport()
        {
            var methodParameter = new List<KeyValuePair<string, string>>();
            DataSet dataSetUser = new DataSet();
            dataSetUser = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Table1", methodParameter);
            DataSet dataSetUser1 = new DataSet();
            dataSetUser1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Table2", methodParameter);
            var empList = dataSetUser.Tables[0].AsEnumerable()
             .Select(dataRow => new ConstituencyWorkProgressImages
             {
                 filePath = dataRow.Field<string>("TableName"),
                 fileName = dataRow.Field<string>("ColumnName"),
                 tFileCaption = dataRow.Field<string>("DataType"),
                 workCode = dataRow.Field<string>("Mandatory/Optional"),
                 thumbFilePath = dataRow.Field<string>("Constraint"),
             }).ToList();

            var getAllLatestworkList = dataSetUser1.Tables[0].AsEnumerable()
           .Select(dataRow => new myShemeList()
           {
               workName = dataRow.Field<string>("Name"),
           }).ToList();

            ViewBag.Images = empList;
            ViewBag.LatestUpdate = getAllLatestworkList;
            return View();
        }

        //public ActionResult constituencyReport()
        //{

        //    schemeMapping _scheme = new schemeMapping();
        //    var santioned1 = _scheme.mySchemeList;
        //    if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "7")
        //    {
        //        if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
        //        {
        //            ViewBag.DeptID = CurrentSession.DeptID;
        //            if (string.IsNullOrEmpty(CurrentSession.UserID))
        //            {
        //                return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //            }
        //            MlaDiary model1 = new MlaDiary();
        //            model1.UserofcId = CurrentSession.OfficeId;
        //            var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);

        //            var Agency = CurrentSession.DeptID;
        //            _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
        //             santioned1 = _scheme.mySchemeList.Where(x => Officelist.Any(y => y.OfficeId == Convert.ToInt16(x.ExecutiveOfficeId))).ToList();
        //        }
        //    }


        //    if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "2")
        //    {
        //        if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
        //        {
        //            ViewBag.DeptID = CurrentSession.DeptID;
        //            if (string.IsNullOrEmpty(CurrentSession.UserID))
        //            {
        //                return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //            }
        //            MlaDiary model1 = new MlaDiary();
        //            var Agency = CurrentSession.DeptID;
        //           _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
        //            santioned1 = _scheme.mySchemeList.ToList();
        //        }
        //    }

        //    if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "0")
        //    {
        //        if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
        //        {
        //            ViewBag.DeptID = CurrentSession.DeptID;
        //            if (string.IsNullOrEmpty(CurrentSession.UserID))
        //            {
        //                return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //            }
        //            MlaDiary model1 = new MlaDiary();
        //            CurrentSession.DeptID = "0";
        //            CurrentSession.OfficeId = "0";
        //            CurrentSession.DistrictCode = "0";
        //            var Agency = "0";
        //            _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
        //            santioned1 = _scheme.mySchemeList.ToList();
        //        }
        //    }


        //    if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "1")
        //    {
        //        if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
        //        {
        //            //ViewBag.DeptID = CurrentSession.DeptID;
        //            if (string.IsNullOrEmpty(CurrentSession.UserID))
        //            {
        //                return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //            }
        //            MlaDiary model1 = new MlaDiary();
        //            model1.UserofcId = CurrentSession.OfficeId;
        //            var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);

        //            var distt = CurrentSession.DistrictCode;

        //            DiaryModel dobj = new DiaryModel();
        //            dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
        //            dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
        //            dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
        //            var Consttituency = Convert.ToString(dobj.ConstituencyCode);
        //            CurrentSession.ConstituencyID = Consttituency;
        //            _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType2Count", new string[] { Consttituency });
        //            santioned1 = _scheme.mySchemeList.ToList();
        //        }
        //    }



        //    if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "8")
        //    {
        //        if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
        //        {
        //            ViewBag.DeptID = CurrentSession.DeptID;
        //            if (string.IsNullOrEmpty(CurrentSession.UserID))
        //            {
        //                return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //            }
        //            var distt = CurrentSession.DistrictCode;
        //            MlaDiary model1 = new MlaDiary();
        //            CurrentSession.DeptID = "0";
        //            CurrentSession.OfficeId = "0";
        //            _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType1Count", new string[] { distt });

        //            santioned1 = _scheme.mySchemeList.ToList();
        //        }
        //    }


        //    if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "9")
        //    {
        //        if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
        //        {
        //            ViewBag.DeptID = CurrentSession.DeptID;
        //            if (string.IsNullOrEmpty(CurrentSession.UserID))
        //            {
        //                return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //            }
        //            var SubDivisionId = CurrentSession.SubDivisionId;
        //            MlaDiary model1 = new MlaDiary();
        //            _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeSDMCount", new string[] { SubDivisionId });
        //            santioned1 = _scheme.mySchemeList.ToList();
        //        }
        //    }




        //    var santioned = santioned1.Where(x => x.workStatusCode == 1).Count();
        //    var Inprogress = santioned1.Where(x => x.workStatusCode == 2).Count();
        //    var Completed = santioned1.Where(x => x.workStatusCode == 3).Count();
        //    var PendingCourt = santioned1.Where(x => x.workStatusCode == 5).Count();
        //    var Canceled = santioned1.Where(x => x.workStatusCode == 4).Count();
        //    var PendingClear = santioned1.Where(x => x.workStatusCode == 6).Count();
        //    var PendingClearDPR = santioned1.Where(x => x.workStatusCode == 7).Count();

        //    return PartialView("workReport", new string[] { CurrentSession.OfficeId, CurrentSession.DeptID, CurrentSession.UserTypeConstituencyId,CurrentSession.DistrictCode,Convert.ToString(santioned), Convert.ToString(Inprogress), Convert.ToString(Completed), Convert.ToString(PendingCourt), Convert.ToString(Canceled), Convert.ToString(PendingClear), Convert.ToString(PendingClearDPR) });
        //}

        //      public ActionResult FilteredconstituencyListForAgency(string Agency, string Office, string Status,string UserType)
        //      {
        //          if (string.IsNullOrEmpty(CurrentSession.UserID))
        //          {
        //              return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //          }
        //          schemeMapping _scheme = new schemeMapping();

        //          string SubDivisionCode = "0";
        //          if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
        //          {
        //              if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
        //              {
        //                  SubDivisionCode = CurrentSession.SubDivisionId;
        //                  // SubdivisionIds = SubDivisionCode.Split(',').ToList();
        //              }
        //          }
        //     if (UserType == "7"){

        //          MlaDiary model1 = new MlaDiary();
        //          model1.UserofcId = Office;
        //          var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);
        //          _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType", new string[] { Agency, Status });
        //          _scheme.mySchemeList = _scheme.mySchemeList.Where(x => Officelist.Any(y => y.OfficeId == Convert.ToInt16(x.ExecutiveOfficeId))).ToList();

        //     }
        //if (UserType == "2")
        //     {
        //         _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType", new string[] { Agency, Status });
        //         _scheme.mySchemeList = _scheme.mySchemeList.ToList();

        //     }
        //     if (UserType == "0")
        //     {
        //         _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType", new string[] { "0", Status });
        //         _scheme.mySchemeList = _scheme.mySchemeList.ToList();

        //     }

        //       if (UserType == "8")
        //       {
        //         var distt = CurrentSession.DistrictCode;
        //         MlaDiary model1 = new MlaDiary();
        //         model1.UserofcId = Office;
        //        _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType1", new string[] { Status,distt });
        //       }

        //       if (UserType == "9")
        //       {
        //           var Subdivision = CurrentSession.SubDivisionId;
        //           MlaDiary model1 = new MlaDiary();
        //           model1.UserofcId = Office;
        //           _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListBySubDivision", new string[] { Status, Subdivision });
        //       }


        //       if (UserType == "1")
        //       {
        //           var distt = CurrentSession.DistrictCode;
        //           DiaryModel dobj = new DiaryModel();
        //           dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
        //           dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
        //           dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
        //           var Consttituency = Convert.ToString(dobj.ConstituencyCode);
        //           _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType2", new string[] { Status, Consttituency });
        //       }


        //      return PartialView("_constituencyListForOfficer", _scheme);


        //      }

        public ActionResult FilteredconstituencyListForAgency(string Agency, string Office, string Status, string UserType, string MLAfeb)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            schemeMapping _scheme = new schemeMapping();

#pragma warning disable CS0219 // The variable 'SubDivisionCode' is assigned but its value is never used
            string SubDivisionCode = "0";
#pragma warning restore CS0219 // The variable 'SubDivisionCode' is assigned but its value is never used
            if (CurrentSession.UserTypeConstituencyId == "" || CurrentSession.UserTypeConstituencyId == "0")
            {
                _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType", new string[] { Agency, Status });
            }
            else
            {
                _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByConstId", new string[] { CurrentSession.UserTypeConstituencyId , Status });
            }
            _scheme.mySchemeList = _scheme.mySchemeList.ToList();
            //if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            //{
            //    if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
            //    {
            //        SubDivisionCode = CurrentSession.SubDivisionId;
            //        // SubdivisionIds = SubDivisionCode.Split(',').ToList();
            //    }
            //}
            //if (UserType == "7")
            //{

            //    MlaDiary model1 = new MlaDiary();
            //    model1.UserofcId = Office;
            //    var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);
            //    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType", new string[] { Agency, Status });
            //    _scheme.mySchemeList = _scheme.mySchemeList.Where(x => Officelist.Any(y => y.OfficeId == Convert.ToInt16(x.ExecutiveOfficeId))).ToList();

            //}
            //if (UserType == "2")
            //{
            //    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType", new string[] { Agency, Status });
            //    _scheme.mySchemeList = _scheme.mySchemeList.ToList();

            //}
            //if (UserType == "0" || UserType == "")
            //{
            //    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType", new string[] { Agency, Status });
            //    _scheme.mySchemeList = _scheme.mySchemeList.ToList();

            //}

            //if (UserType == "8")
            //{
            //    var distt = CurrentSession.DistrictCode;
            //    MlaDiary model1 = new MlaDiary();
            //    model1.UserofcId = Office;
            //    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType1", new string[] { Status, distt });
            //}

            //if (UserType == "9")
            //{
            //    var Subdivision = CurrentSession.SubDivisionId;
            //    MlaDiary model1 = new MlaDiary();
            //    model1.UserofcId = Office;
            //    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListBySubDivision", new string[] { Status, Subdivision });
            //}


            //if (UserType == "1")
            //{
            //    var distt = CurrentSession.DistrictCode;
            //    DiaryModel dobj = new DiaryModel();
            //    dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            //    dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            //    dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
            //    var Consttituency = Convert.ToString(dobj.ConstituencyCode);
            //    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType2", new string[] { Status, Consttituency });

            //}
            //if (MLAfeb != "0")
            //{
            //    _scheme.mySchemeList = _scheme.mySchemeList.Where(x => x.programName == Convert.ToString(MLAfeb)).ToList();
            //}

            return PartialView("_constituencyListForOfficer", _scheme);


        }

        //public ActionResult FilteredconstituencyListForAll(string Program, string Demand, string ControllingAuthority, string FinancialYear, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string DistrictID, string SubDivisionID, string Constituency, string cFinancialYear, string sanctionedDate, string SectionAmount, string Estimate, string Images, string MLAfeb, string Office)
        //{
        //    if (string.IsNullOrEmpty(CurrentSession.UserID))
        //    {
        //        return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //    }

        //    schemeMapping _scheme = new schemeMapping();

        //    string SubDivisionCode = "0";
        //    if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
        //    {
        //        if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
        //        {
        //            SubDivisionCode = CurrentSession.SubDivisionId;
        //            // SubdivisionIds = SubDivisionCode.Split(',').ToList();
        //        }
        //    }

        //    //_scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListAll", new string[] { Constituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate, SubDivisionCode.ToString(), SectionAmount, Estimate, Images, MLAfeb, Office });

        //    MlaDiary model1 = new MlaDiary();
        //    model1.UserofcId = Office;
        //    var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);
        //    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListAll", new string[] { Constituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate, SubDivisionCode.ToString(), SectionAmount, Estimate, Images, MLAfeb, Office, SubDivisionID });
        //   if (model1.UserofcId != "0")
        //   {
        //      _scheme.mySchemeList = _scheme.mySchemeList.Where(x => Officelist.Any(y => y.OfficeId == Convert.ToInt16(x.ExecutiveOfficeId))).ToList();
        //   }
        //    return PartialView("_constituencyListForOfficer", _scheme);


        //}

        public ActionResult FilteredconstituencyListForAll(string Program, string Demand, string ControllingAuthority, string FinancialYear, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string DistrictID, string SubDivisionID, string Constituency, string cFinancialYear, string sanctionedDate, string SectionAmount, string Estimate, string Images, string MLAfeb, string Office)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            schemeMapping _scheme = new schemeMapping();

            string SubDivisionCode = "0";
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            {
                if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
                {
                    SubDivisionCode = CurrentSession.SubDivisionId;
                    // SubdivisionIds = SubDivisionCode.Split(',').ToList();
                }
            }

            //_scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListAll", new string[] { Constituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate, SubDivisionCode.ToString(), SectionAmount, Estimate, Images, MLAfeb, Office });

            MlaDiary model1 = new MlaDiary();
            model1.UserofcId = Office;
            var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);
            _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListAll", new string[] { Constituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate, SubDivisionCode.ToString(), SectionAmount, Estimate, Images, MLAfeb, Office, SubDivisionID });
            if (model1.UserofcId != "0")
            {
                _scheme.mySchemeList = _scheme.mySchemeList.Where(x => Officelist.Any(y => y.OfficeId == Convert.ToInt16(x.ExecutiveOfficeId))).ToList();
            }


            var Total = _scheme.mySchemeList;
            var santioned = Total.Where(x => x.workStatusCode == 1).Count();
            var Inprogress = Total.Where(x => x.workStatusCode == 2).Count();
            var Completed = Total.Where(x => x.workStatusCode == 3).Count();
            var PendingCourt = Total.Where(x => x.workStatusCode == 5).Count();
            var Canceled = Total.Where(x => x.workStatusCode == 4).Count();
            var PendingClear = Total.Where(x => x.workStatusCode == 6).Count();
            var PendingClearDPR = Total.Where(x => x.workStatusCode == 7).Count();

            ViewBag.santioned = santioned;
            ViewBag.Inprogress = Inprogress;
            ViewBag.Completed = Completed;
            ViewBag.PendingCourt = PendingCourt;
            ViewBag.Canceled = Canceled;
            ViewBag.PendingClear = PendingClear;
            ViewBag.PendingClearDPR = PendingClearDPR;




            return PartialView("_constituencyListForOfficer", _scheme);


        }

        public ActionResult DepartmentAllOffices(string ExecutiveDepartment)
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetExecutiveOffice", new string[] { ExecutiveDepartment, "0" });
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DepartmentAllSubOffices(string ExecutiveDepartment)
        {
            // List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetExecutiveOffice", new string[] { ExecutiveDepartment, "0" });
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetAllSubOfficeList", ExecutiveDepartment);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }



        public ActionResult FilteredMyConstituencyWorks(string Program, string Demand, string ControllingAuthority, string FinancialYear, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string cFinancialYear, string sanctionedDate, string Constituency)
        {
            string SubDivisionCode = "0";
            //if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            //{
            //    if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
            //    {
            //        SubDivisionCode = CurrentSession.SubDivisionId;

            //    }
            //}

            if (Constituency == "" && Constituency != "")
            {
                string[] str = new string[2];
                str[0] = CurrentSession.AssemblyId;
                str[1] = MemberCode;
                string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCodebyMemberCodeAndAssemblyCode", str);
                if (strOutput != null)
                {
                    Constituency = Convert.ToString(strOutput[0]);
                    CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                }
            }
            else
            {


                if (Constituency == "1028")
                {
                    Constituency = "0";
                }
                else
                {
                    string[] str = new string[2];
                    str[0] = CurrentSession.AssemblyId;
                    str[1] = Constituency;
                    string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
                    if (strOutput != null)
                    {
                        Constituency = Convert.ToString(strOutput[0]);
                        CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                    }

                }

            }

            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyDistrict", Constituency);




            schemeMapping _scheme =(schemeMapping)gelFilteredMySchemeList(new string[]{ Constituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, _list[0].value.ToString(), aadharID, "true", cFinancialYear, sanctionedDate, SubDivisionCode, null} );
            return PartialView("_myConstituencyWorkList", _scheme);
        }

        public static object gelFilteredMySchemeList(object param)
        {
            string[] arr = param as string[];
            string program = arr[1], ControllingAuthority = arr[3], FinancialYear = arr[4], agency = arr[5], mOwnerDepartment = arr[6],
                Panchayat = arr[7], WorkType = arr[8]; // OfficeId = arr[16];

            long constituencyID = Convert.ToInt32(arr[0]);
            long districtCode = Convert.ToInt32(arr[10]);
            //  long demandCode = Convert.ToInt64(arr[2]);
            long workstatus = Convert.ToInt64(arr[9]);
            string aadharID = arr[11];
            bool member = Convert.ToBoolean(arr[12]);
            string cFinancialYear = arr[13];
            string sanctionedDate = arr[14];
            //Int32 SubdivisionCode = Convert.ToInt32(arr[15]);
            string SubdivisionCode = arr[15];
            DateTime? dtDate = null;
            if (sanctionedDate != "0" && sanctionedDate != "" && sanctionedDate != null)
            {
                //DateTime ScDate = Convert.ToDateTime(sanctionedDate);
                string Date = sanctionedDate;
                Int16 dd = Convert.ToInt16(sanctionedDate.Substring(0, 2));
                Int16 mm = Convert.ToInt16(sanctionedDate.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(sanctionedDate.Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                dtDate = new DateTime(yyyy, mm, dd);
                //  model.EnclosureDate = dtDate;
                //  myresult = myresult.Where(a => a.SanctionedDate.Value.ToString("dd-MM-yyyy") == sanctionedDate).ToList();
                // myresult = myresult.Where(a => a.SanctionedDate.Value.ToString("dd-MM-yyyy") == dtDate.ToString("dd-MM-yyyy")).ToList();
            }

            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@workstatus", Convert.ToString(workstatus)));
            methodParameter.Add(new KeyValuePair<string, string>("@ControllingAuthority", Convert.ToString(ControllingAuthority)));
            methodParameter.Add(new KeyValuePair<string, string>("@WorkType", Convert.ToString(WorkType)));
            methodParameter.Add(new KeyValuePair<string, string>("@program", Convert.ToString(program)));
            methodParameter.Add(new KeyValuePair<string, string>("@sanctionedDate", Convert.ToString(dtDate)));
            methodParameter.Add(new KeyValuePair<string, string>("@agency", Convert.ToString(agency)));
            methodParameter.Add(new KeyValuePair<string, string>("@mOwnerDepartment", Convert.ToString(mOwnerDepartment)));
            methodParameter.Add(new KeyValuePair<string, string>("@FinancialYear", Convert.ToString(FinancialYear)));
            methodParameter.Add(new KeyValuePair<string, string>("@constituencyID", Convert.ToString(constituencyID)));
            DataSet dataSetUser = new DataSet();
            dataSetUser = SBL.eLegislator.HPMS.ServiceAdaptor.ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetFilterWork", methodParameter);
            List<vsSchemeList> list = new List<vsSchemeList>();
            List<myShemeList> mylist = new List<myShemeList>();
            schemeMapping scheme = new schemeMapping();

            scheme.vsSchemeList = list;
            scheme.mySchemeList = mylist;

            foreach (DataRow dr in dataSetUser.Tables[0].Rows)
            {

                mylist.Add(new myShemeList()
                {
                    workCode = Convert.ToString(dr["WorkCode"]),
                    workName = Convert.ToString(dr["workName"]),
                    mySchemeID = Convert.ToInt64(dr["mySchemeID"]),

                    sanctionedAmount = Convert.ToDouble(dr["sanctionedAmount"]),

                    EstimatedAmount = Convert.ToDouble(dr["EstimatedAmount"]),
                    Panchayat = Convert.ToInt64(dr["Panchayat"]),
                    PanchayatName = Convert.ToString(dr["PanchayatName"]),
                    programName = Convert.ToString(dr["programName"]),
                    ProgrammeName = Convert.ToString(dr["ProgrammeName"]),
                    sanctionedDate = Convert.ToString(dr["sanctionedDate"]),

                    districtID = Convert.ToInt64(dr["districtID"]),
                    WardName = Convert.ToString(dr["WardName"]),
                    ExecutiveDepartmentName = Convert.ToString(dr["ExecutiveDepartmentName"]),

                    ExecutiveDepartment = Convert.ToString(dr["ExecutiveDepartment"]),
                    // workDetails.ExecutingDepartmentId != null ? workDetails.ExecutingDepartmentId : "",

                    // ExecutiveOffice = Convert.ToString(workDetails.ExecutingOfficeID),
                    ExecutiveOffice = Convert.ToString(dr["ExecutiveOffice"]),
                    oWnerDepartment = Convert.ToString(dr["oWnerDepartment"]),
                    ExecutiveOfficeName = Convert.ToString(dr["ExecutiveOfficeName"]),
                    physicalProgress = Convert.ToDouble(dr["physicalProgress"]),
                    financialProgress = Convert.ToDouble(dr["financialProgress"]),
                    financialYear = Convert.ToString(dr["financialYear"]),

                    CompletionDate = Convert.ToString(dr["CompletionDate"]),

                    imageCount = Convert.ToInt32(dr["imageCount"]),
                    CompletionFinancialYear = Convert.ToString(dr["CompletionFinancialYear"]),
                    workStatus = Convert.ToString(dr["workStatus"]),
                    workStatusCode = Convert.ToInt64(dr["workStatusCode"]),
                    subdevisionId = Convert.ToInt32(dr["subdevisionId"]),
                    // ProgrammeName = item.ProgrammeName != null ? item.ProgrammeName:"0",
                    WorkType = Convert.ToString(dr["WorkType"]),
                    // WorkType = item.work
                });

            }
            return scheme;
        }

        public ActionResult findMyConstituencyWorkbyCode(string schemeID)
        {
            //WorkDetails _scheme = (WorkDetails)Helper.ExecuteService("ConstituencyVS", "getWorkDetailsByIDForMember", schemeID);
            WorkDetails _scheme = new WorkDetails();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@ai_scheme_id", schemeID.ToString()));
            DataSet dataSetUser = new DataSet();
            dataSetUser = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetWorkDetailsForWeb_Sel", methodParameter);
            if (dataSetUser != null && dataSetUser.Tables.Count > 0 && dataSetUser.Tables[0].Rows.Count > 0)
            {

                _scheme.ControllingAuthorityName = dataSetUser.Tables[0].Rows[0]["ControllingAuthorityName"].ToString();
                _scheme.FinancialYear = dataSetUser.Tables[0].Rows[0]["FinancialYear"].ToString();
                _scheme.demandName = dataSetUser.Tables[0].Rows[0]["Demand"].ToString();
                _scheme.workCode = dataSetUser.Tables[0].Rows[0]["WorkCode"].ToString();
                _scheme.workName = dataSetUser.Tables[0].Rows[0]["WorkName"].ToString();
                _scheme.programName = dataSetUser.Tables[0].Rows[0]["ProgrammeName"].ToString();
                _scheme.WorkNatureID = dataSetUser.Tables[0].Rows[0]["WorkNatureName"].ToString();
                _scheme.Owner_deptName = dataSetUser.Tables[0].Rows[0]["OwnerDeptName"].ToString();
                _scheme.ExecutingDepartmentName = dataSetUser.Tables[0].Rows[0]["ExecutingDeptName"].ToString();
                _scheme.ExecutingOfficeName = dataSetUser.Tables[0].Rows[0]["OfficeName"].ToString();
                _scheme.districtName = dataSetUser.Tables[0].Rows[0]["DistrictName"].ToString();
                _scheme.constituenctName = dataSetUser.Tables[0].Rows[0]["ConstituencyName"].ToString();
                _scheme.PanchayatName = dataSetUser.Tables[0].Rows[0]["PanchayatName"].ToString();
                _scheme.EstimatedAmount = string.IsNullOrEmpty(dataSetUser.Tables[0].Rows[0]["EstimatedAmount"].ToString()) ? 0 : Convert.ToDouble(dataSetUser.Tables[0].Rows[0]["EstimatedAmount"]);
                _scheme.EstimatedCompletionDate = dataSetUser.Tables[0].Rows[0]["EstimatedCompletionDate"].ToString();
                _scheme.sanctionedAmount = string.IsNullOrEmpty(dataSetUser.Tables[0].Rows[0]["SanctionedAmount"].ToString()) ? 0 : Convert.ToDouble(dataSetUser.Tables[0].Rows[0]["SanctionedAmount"]);
                _scheme.sanctionedDate = dataSetUser.Tables[0].Rows[0]["SanctionedDate"].ToString();
                _scheme.TotalPhysicalProgress = string.IsNullOrEmpty(dataSetUser.Tables[0].Rows[0]["toalPhysicalProgress"].ToString()) ? 0 : Convert.ToDouble(dataSetUser.Tables[0].Rows[0]["toalPhysicalProgress"]);
                _scheme.workStatusText = dataSetUser.Tables[0].Rows[0]["workstatus"].ToString();
                _scheme.TotalExpenditureAmount = string.IsNullOrEmpty(dataSetUser.Tables[0].Rows[0]["toalExpenditure"].ToString()) ? 0 : Convert.ToDouble(dataSetUser.Tables[0].Rows[0]["toalExpenditure"]);
                _scheme.TotalFundReleased = string.IsNullOrEmpty(dataSetUser.Tables[0].Rows[0]["totalfundreleased"].ToString()) ? 0 : Convert.ToDouble(dataSetUser.Tables[0].Rows[0]["totalfundreleased"]);
                _scheme.ImagePath = dataSetUser.Tables[0].Rows[0]["ImageName"].ToString();
                _scheme.dprFile = Convert.ToString(dataSetUser.Tables[0].Rows[0]["dprFile"]);
                _scheme.featuresfile = Convert.ToString(dataSetUser.Tables[0].Rows[0]["featuresfile"]);
                _scheme.ContractorAddress = dataSetUser.Tables[0].Rows[0]["ContractorAddress"].ToString();
                _scheme.ContractorName = dataSetUser.Tables[0].Rows[0]["ContractorName"].ToString();
                _scheme.SubdivisionName = dataSetUser.Tables[0].Rows[0]["Subdivision"].ToString();
                _scheme.completionDate = dataSetUser.Tables[0].Rows[0]["completionDate"].ToString();
                _scheme.WorkType = dataSetUser.Tables[0].Rows[0]["workType"].ToString();
                ViewBag.workName = _scheme.workName;
                ViewBag.workCode = _scheme.workCode;
            }
            List<ConstituencyWorkProgressImages> list = (List<ConstituencyWorkProgressImages>)Helper.ExecuteService("ConstituencyVS", "getAllProgressImages", schemeID);
            _scheme.Imagelist = list;
            workBasicView details = (workBasicView)Helper.ExecuteService("ConstituencyVS", "getBasicDetailsOFWorks", schemeID);
            if (details != null)
            {
                ViewBag.workCode = details.workCode;
                ViewBag.financialYear = details.financialYear;
                ViewBag.workName = details.workName;
                ViewBag.workStatus = details.workStatus;
            }
            List<ConstituencyWorkProgress> list1 = (List<ConstituencyWorkProgress>)Helper.ExecuteService("ConstituencyVS", "viewAllProgressByMember", schemeID);
            _scheme.WorkProgresslist = list1;
            return PartialView("_workDetails", _scheme);
        }


        public ActionResult updateWorksbyMembers(WorkDetails work, HttpPostedFileBase photo, HttpPostedFileBase photo1)
        {
            string checkedData = "";
            string Statecode = Helper.ExecuteService("SiteSetting", "Get_StateCode", null) as string;
            work.StateCode = Convert.ToInt32(Statecode);
            work.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
            }


            //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            //string url = "/constituencyDPR";
            //string directory = FileSettings.SettingValue + url;

            //if (!System.IO.Directory.Exists(directory))
            //{
            //    System.IO.Directory.CreateDirectory(directory);
            //}
            //int fileID = GetFileRandomNo();

            //string url1 = "~/constituencyDPR";
            //string directory1 = Server.MapPath(url1);

            //if (Directory.Exists(directory))
            //{
            //    string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
            //    if (savedFileName.Length > 0)
            //    {
            //        string SourceFile = savedFileName[0];
            //        foreach (string page in savedFileName)
            //        {
            //            Guid FileName = Guid.NewGuid();

            //            string name = Path.GetFileName(page);

            //            string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


            //            if (!string.IsNullOrEmpty(name))
            //            {
            //                if (!string.IsNullOrEmpty(work.dprFile))
            //                    System.IO.File.Delete(System.IO.Path.Combine(directory, work.dprFile));
            //                System.IO.File.Copy(SourceFile, path, true);
            //                work.dprFile = "/constituencyDPR/" + FileName + "_" + name.Replace(" ", "");
            //            }

            //        }

            //    }
            //    else
            //    {
            //        return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
            //    }

            //}
            //foreach (var item in photo)
            //{
            //    var s = item.FileName;
            //}



#pragma warning disable CS0219 // The variable 'isSavedSuccessfully' is assigned but its value is never used
            bool isSavedSuccessfully = true;
#pragma warning restore CS0219 // The variable 'isSavedSuccessfully' is assigned but its value is never used
            string fName = "";
            string pathString = "";
            string NFileName = "";

            string fName1 = "";
            string pathString1 = "";
            string NFileName1 = "";

            if (photo != null)
            {
                //Save file content goes here
                fName = photo.FileName;
                if (photo != null && photo.ContentLength > 0)
                {


                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    // string directory = FileSettings.SettingValue + SavedPdfPath;
                    string url = "constituencyDPR\\" + currentAssemby + "\\" + work.workCode + "\\";
                    pathString = FileSettings.SettingValue + url;

                    var fileName1 = Path.GetFileName(photo.FileName);
                    string ext = Path.GetExtension(photo.FileName);
                    NFileName = Guid.NewGuid().ToString() + ext;

                    bool isExists = System.IO.Directory.Exists(pathString);

                    if (!isExists)
                        System.IO.Directory.CreateDirectory(pathString);

                    var path = string.Format("{0}\\{1}", pathString, NFileName);
                    photo.SaveAs(path);
                    work.dprFile = url + NFileName;
                }
            }
            if (photo1 != null)
            {
                //Save file content goes here
                fName1 = photo1.FileName;
                if (photo1 != null && photo1.ContentLength > 0)
                {


                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    // string directory = FileSettings.SettingValue + SavedPdfPath;
                    string url = "salientfeatures\\" + currentAssemby + "\\" + work.workCode + "\\";
                    pathString1 = FileSettings.SettingValue + url;

                    var fileName1 = Path.GetFileName(photo1.FileName);
                    string ext = Path.GetExtension(photo1.FileName);
                    NFileName1 = Guid.NewGuid().ToString() + ext;

                    bool isExists = System.IO.Directory.Exists(pathString1);

                    if (!isExists)
                        System.IO.Directory.CreateDirectory(pathString1);

                    var path = string.Format("{0}\\{1}", pathString1, NFileName1);
                    photo1.SaveAs(path);
                    work.featuresfile = url + NFileName1;
                }
            }
            work.userid = currentUser;
            //if (work.eConstituencyID == null)
            //{
            //    work.eConstituencyID = Convert.ToInt64(work.ConId_ForSDM_);
            //}
            //  work.eConstituencyID = Convert.ToInt64(work.ConId_ForSDM_);
            work.memberCode = string.IsNullOrEmpty(MemberCode) ? work.memberCode : Convert.ToInt32(MemberCode);
            string membercode = "";
            if (work.memberCode == null)
            {
                work.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
                membercode = Helper.ExecuteService("ConstituencyVS", "getMemberCodeByConstituency", work) as string;
                work.memberCode = Convert.ToInt32(membercode);
            }

            work.isMember = string.IsNullOrEmpty(CurrentSession.IsMember) ? false : Convert.ToBoolean(CurrentSession.IsMember);
            //  work.WorkTypeID = "work";
            if (work.WorkTypeID.Contains("work") || (work.programName.Contains("program")))
            {
                return Json("Please Wait To Fill All Input field", JsonRequestBehavior.AllowGet);
            };
            if (work.Mode == "New")
            {
                checkedData = Helper.ExecuteService("ConstituencyVS", "CheckWorkCode_ByCID", work) as string;
                if (checkedData != "N")
                {
                    checkedData = "Y";
                }
                else
                {
                    Helper.ExecuteService("ConstituencyVS", "CreateNewWorkbyMembers", work);


                }
                // Helper.ExecuteService("ConstituencyVS", "CreateNewWorkbyMembers", work);
            }
            else
            {
                Helper.ExecuteService("ConstituencyVS", "updateWorksbyMembers", work);
            }

            return Json(checkedData, JsonRequestBehavior.AllowGet); ;
        }

        static int GetFileRandomNo()
        {
            Random ran = new Random();
            int rno = ran.Next(1, 99999);
            return rno;
        }
        //public ActionResult updateWorksbyMembers(WorkDetails work, HttpPostedFileBase photo)
        //{

        //    //HttpPostedFileBase photo = Request.Files["DPR"];

        //    //if (photo != null)
        //    //{
        //    //    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
        //    //    // string directory = FileSettings.SettingValue + SavedPdfPath;
        //    //    string url = "constituencyDPR\\" + currentAssemby + "\\" + work.mySchemeID + "\\";
        //    //    string path = FileSettings.SettingValue + url;
        //    //    bool isExists = System.IO.Directory.Exists(path);

        //    //    if (!isExists)
        //    //        System.IO.Directory.CreateDirectory(path);
        //    //    photo.SaveAs(path + photo.FileName);
        //    //    work.dprFile = url + photo.FileName;
        //    //}
        //    bool isSavedSuccessfully = true;
        //    string fName = "";
        //    string pathString = "";
        //    string NFileName = "";

        //    if (photo != null)
        //    {
        //        //Save file content goes here
        //        fName = photo.FileName;
        //        if (photo != null && photo.ContentLength > 0)
        //        {
        //            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
        //            // string directory = FileSettings.SettingValue + SavedPdfPath;
        //            string url = "constituencyDPR\\" + currentAssemby + "\\" + work.mySchemeID + "\\";
        //            pathString = FileSettings.SettingValue + url;

        //            var fileName1 = Path.GetFileName(photo.FileName);
        //            string ext = Path.GetExtension(photo.FileName);
        //            NFileName = Guid.NewGuid().ToString() + ext;

        //            bool isExists = System.IO.Directory.Exists(pathString);

        //            if (!isExists)
        //                System.IO.Directory.CreateDirectory(pathString);

        //            var path = string.Format("{0}\\{1}", pathString, NFileName);
        //            photo.SaveAs(path);
        //            work.dprFile = url + NFileName;
        //        }
        //    }
        //    work.userid = currentUser;
        //    work.memberCode = string.IsNullOrEmpty(MemberCode) ? work.memberCode : Convert.ToInt32(MemberCode);
        //    work.isMember = string.IsNullOrEmpty(CurrentSession.IsMember) ? false : Convert.ToBoolean(CurrentSession.IsMember);
        //    if (work.Mode == "New")
        //    {


        //        Helper.ExecuteService("ConstituencyVS", "CreateNewWorkbyMembers", work);
        //    }
        //    else
        //    {
        //        Helper.ExecuteService("ConstituencyVS", "updateWorksbyMembers", work);
        //    }

        //    return Json("", JsonRequestBehavior.AllowGet); ;
        //}

        public ActionResult updateWorksbyMembers1(WorkDetails work, HttpPostedFileBase photo)
        {
            //HttpPostedFileBase photo = Request.Files["DPR"];

            //if (photo != null)
            //{
            //    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            //    // string directory = FileSettings.SettingValue + SavedPdfPath;
            //    string url = "constituencyDPR\\" + currentAssemby + "\\" + work.mySchemeID + "\\";
            //    string path = FileSettings.SettingValue + url;
            //    bool isExists = System.IO.Directory.Exists(path);

            //    if (!isExists)
            //        System.IO.Directory.CreateDirectory(path);
            //    photo.SaveAs(path + photo.FileName);
            //    work.dprFile = url + photo.FileName;
            //}
#pragma warning disable CS0219 // The variable 'isSavedSuccessfully' is assigned but its value is never used
            bool isSavedSuccessfully = true;
#pragma warning restore CS0219 // The variable 'isSavedSuccessfully' is assigned but its value is never used
            string fName = "";
            string pathString = "";
            string NFileName = "";

            if (photo != null)
            {
                //Save file content goes here
                fName = photo.FileName;
                if (photo != null && photo.ContentLength > 0)
                {
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    // string directory = FileSettings.SettingValue + SavedPdfPath;
                    string url = "constituencyDPR\\" + currentAssemby + "\\" + work.mySchemeID + "\\";
                    pathString = FileSettings.SettingValue + url;

                    var fileName1 = Path.GetFileName(photo.FileName);
                    string ext = Path.GetExtension(photo.FileName);
                    NFileName = Guid.NewGuid().ToString() + ext;

                    bool isExists = System.IO.Directory.Exists(pathString);

                    if (!isExists)
                        System.IO.Directory.CreateDirectory(pathString);

                    var path = string.Format("{0}\\{1}", pathString, NFileName);
                    photo.SaveAs(path);
                    work.dprFile = url + NFileName;
                }
            }
            work.userid = currentUser;
            work.memberCode = string.IsNullOrEmpty(MemberCode) ? work.memberCode : Convert.ToInt32(MemberCode);
            work.isMember = string.IsNullOrEmpty(CurrentSession.IsMember) ? false : Convert.ToBoolean(CurrentSession.IsMember);
            if (work.Mode == "New")
            {
                //int recordexist = (int)Helper.ExecuteService("ConstituencyVS", "CheckWorkCode_ByCID", work);
                //if (recordexist == 0)
                //{
                //   Helper.ExecuteService("ConstituencyVS", "CreateNewWorkbyMembers", work);
                //}
                //else
                //{

                //}
                string checkedData = Helper.ExecuteService("ConstituencyVS", "CheckWorkCode_ByCID", work) as string;
                if (checkedData != "N")
                //if (checkedData != string.Empty || checkedData != null)
                {
                    // checkedData = "Y";
                }
                else
                {
                    Helper.ExecuteService("ConstituencyVS", "CreateNewWorkbyMembers", work);
                }

                // model.RecordNumber = Convert.ToInt32(GetRecordNumber_mlaDiary(model.DocmentType));

            }
            else
            {
                Helper.ExecuteService("ConstituencyVS", "updateWorksbyMembers", work);
            }

            return Json("", JsonRequestBehavior.AllowGet); ;
        }

        public ActionResult CreateNewWorkbyMembers(WorkDetails work)
        {
            return Json("", JsonRequestBehavior.AllowGet); ;
        }

        public ActionResult NewConstituencyWork()
        {
            WorkDetails work = new WorkDetails();
            work.Mode = "New";
            CurrentSession.Name = work.Mode;
            string asCode = CurrentSession.AssemblyId;
            if (asCode == "" || asCode == null)
            {
                asCode = "0";
            }
            if (myConsituency == "" || myConsituency == null)
            {
                myConsituency = "0";
            }

            work.AssemblyId = Convert.ToInt16(asCode);
            work.ConId_ForSDM_ = myConsituency;
            //work.eConstituencyID  = 
            if (myConsituency.Contains(','))
            {
                //  List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetConstituencyNameByID", myConsituency);
                return PartialView("_NewConstituencyWork", work);
            }
            else
            {
                if (myConsituency == "" && MemberCode != "")
                {
                    string[] str = new string[2];
                    str[0] = CurrentSession.AssemblyId;
                    str[1] = MemberCode;
                    string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCodebyMemberCodeAndAssemblyCode", str);
                    if (strOutput != null)
                    {
                        myConsituency = Convert.ToString(strOutput[0]);
                        CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                    }

                }
                else
                {
                    string[] str = new string[2];
                    str[0] = CurrentSession.AssemblyId;
                    str[1] = myConsituency;
                    string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
                    if (strOutput != null)
                    {
                        myConsituency = Convert.ToString(strOutput[0]);
                        CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                    }
                }
                List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyDistrict", myConsituency);  //ByConCode
                work.districtID = Convert.ToInt64(_list[0].value.ToString());
                work.eConstituencyID = Convert.ToInt64(myConsituency);
                work.constituenctName = CurrentSession.ConstituencyName;
                return PartialView("_NewConstituencyWork", work);
            }
        }

        public ActionResult viewProgressImages(string schemeID)
        {
            workBasicView details = (workBasicView)Helper.ExecuteService("ConstituencyVS", "getBasicDetailsOFWorks", schemeID);
            if (details != null)
            {
                ViewBag.workCode = details.workCode;
                ViewBag.financialYear = details.financialYear;
                ViewBag.workName = details.workName;
                ViewBag.workStatus = details.workStatus;
            }
            List<ConstituencyWorkProgressImages> list = (List<ConstituencyWorkProgressImages>)Helper.ExecuteService("ConstituencyVS", "getAllProgressImages", schemeID);
            return PartialView("_viewImageProgress", list);
        }

        public ActionResult viewProgressbyMember(string schemeID)
        {
            workBasicView details = (workBasicView)Helper.ExecuteService("ConstituencyVS", "getBasicDetailsOFWorks", schemeID);
            if (details != null)
            {
                ViewBag.workCode = details.workCode;
                ViewBag.financialYear = details.financialYear;
                ViewBag.workName = details.workName;
                ViewBag.workStatus = details.workStatus;
            }
            List<ConstituencyWorkProgress> list = (List<ConstituencyWorkProgress>)Helper.ExecuteService("ConstituencyVS", "viewAllProgressByMember", schemeID);
            return PartialView("_viewWorkProgress", list);
        }

        public ActionResult FinancialviewProgressbyMember(string schemeID)
        {
            workBasicView details = (workBasicView)Helper.ExecuteService("ConstituencyVS", "getBasicDetailsOFWorks", schemeID);
            if (details != null)
            {
                ViewBag.workCode = details.workCode;
                ViewBag.financialYear = details.financialYear;
                ViewBag.workName = details.workName;
                ViewBag.workStatus = details.workStatus;
            }
            List<ConstituencyWorkProgress> list = (List<ConstituencyWorkProgress>)Helper.ExecuteService("ConstituencyVS", "viewAllProgressByMember", schemeID);
            return PartialView("_viewFinancialWorkProgress", list);
        }

        public ActionResult UpdateExecutingDetails(FormCollection collection)
        {
            string ExecutingDepartmentForUpdate = collection["ExecutingDepartmentForUpdate"];
            string ExecutingOfficeIDForUpdate = collection["ExecutingOfficeIDForUpdate"];
            string schemeCodes = collection["schemeCodes"];
            string updateMode = collection["updateMode"];
            ConstituencySchemesBasicDetails details = new ConstituencySchemesBasicDetails();
            details.Mode = updateMode;
            if (updateMode.ToLower() != "all")
            {
                details.mySchemeID = long.Parse(schemeCodes);
            }
            else
            {
                details.schemeList = schemeCodes;
            }
            details.ExecutingAgencyID = ExecutingDepartmentForUpdate;
            details.ExecutingDepartmentId = ExecutingDepartmentForUpdate;
            details.ExecutingOfficeID = Convert.ToInt64(ExecutingOfficeIDForUpdate);
            Helper.ExecuteService("ConstituencyVS", "UpdateExecutingDetails", details);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult removeExecutingDetails(String SchemeID)
        {

            ConstituencySchemesBasicDetails details = new ConstituencySchemesBasicDetails();
            details.Mode = "Single";

            details.mySchemeID = long.Parse(SchemeID);
            details.schemeList = string.Empty;

            details.ExecutingAgencyID = null;
            details.ExecutingDepartmentId = null;
            details.ExecutingOfficeID = null;
            Helper.ExecuteService("ConstituencyVS", "UpdateExecutingDetails", details);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        #endregion my constituency

        #region Assign Officer

        public ActionResult AssigntoOfficer(string schemeCode)
        {
            int iMemberCode = Convert.ToInt32(MemberCode);
            ViewBag.schemeCode = schemeCode;
            var OfficerInfoList = (List<ForwardGrievance>)Helper.ExecuteService("Grievance", "GetAllFieldOfficer", new ForwardGrievance { MemberCode = iMemberCode, GrvCode = schemeCode });
            return PartialView("_officerList", OfficerInfoList);
        }

        public ActionResult SaveOfficer(FormCollection collection, int pageId = 1, int pageSize = 10)
        {
            // string MemberCode = CurrentSession.MemberCode;
            string GrvCode = collection["hdnGrvCode"];
            string[] RowCount = collection["OfficerRowCount"].Split(',');
            //tMemberGrievances MemGrievance = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = GrvCode });
            try
            {
                DataSet dataSet = new DataSet();
                var ForwardGrievanceParam = new List<KeyValuePair<string, string>>();
                ForwardGrievanceParam.Add(new KeyValuePair<string, string>("@MemberCode", MemberCode));
                ForwardGrievanceParam.Add(new KeyValuePair<string, string>("@GrvCode", GrvCode));
                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteForwardGrievance", ForwardGrievanceParam);

                foreach (var item in RowCount)
                {
                    string OfficerCode = collection["OfficerCode-" + item];
                    string Checked = collection["ChbAssignBox-" + item];

                    if (Checked == "on")
                    {
                        ForwardGrievance obj = new ForwardGrievance();
                        obj.OfficerCode = OfficerCode;
                        obj.MemberCode = int.Parse(MemberCode);
                        obj.GrvCode = GrvCode;
                        obj.ForwardDate = DateTime.Now;
                        obj.Type = "Work";
                        Helper.ExecuteService("Grievance", "CreateForwardGrievance", obj);

                        ViewBag.Msg = "Sucessfully assign officer";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";
                    }
                }
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! Something went wrong.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        #endregion Assign Officer

        #region Constituency List for officer

        public ActionResult constituencyListForOfficer()
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }
            return PartialView("_constituencyOfficer", null);
        }

        public ActionResult FilteredconstituencyListForOfficer(string Program, string Demand, string ControllingAuthority, string FinancialYear, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string DistrictID, string Constituency, string cFinancialYear, string sanctionedDate, string ExecutingOffice)
        {

            string SubDivisionCode = "0";
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            {
                if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
                {
                    SubDivisionCode = CurrentSession.SubDivisionId;
                    // SubdivisionIds = SubDivisionCode.Split(',').ToList();
                }
            }

            // List<SBL.DomainModel.Models.SubDivision.mSubDivision> sublist = new List<SBL.DomainModel.Models.SubDivision.mSubDivision>();
            // sublist = (List<SBL.DomainModel.Models.SubDivision.mSubDivision>)Helper.ExecuteService("ConstituencyVS", "getSubdivision_ByConstituency", Constituency);
            schemeMapping _scheme = (schemeMapping)gelFilteredMySchemeList(new string[] { Constituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate, SubDivisionCode.ToString(), ExecutingOffice });



            return PartialView("_constituencyListForOfficer", _scheme);


        }


        public ActionResult FilteredconstituencyListForCommisioner(string Program, string Demand, string ControllingAuthority, string FinancialYear, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string DistrictID, string Constituency, string cFinancialYear, string sanctionedDate, string ExecutingOffice)
        {

            string SubDivisionCode = "0";
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            {
                if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
                {
                    SubDivisionCode = CurrentSession.SubDivisionId;
                    // SubdivisionIds = SubDivisionCode.Split(',').ToList();
                }
            }

            // List<SBL.DomainModel.Models.SubDivision.mSubDivision> sublist = new List<SBL.DomainModel.Models.SubDivision.mSubDivision>();
            // sublist = (List<SBL.DomainModel.Models.SubDivision.mSubDivision>)Helper.ExecuteService("ConstituencyVS", "getSubdivision_ByConstituency", Constituency);
            schemeMapping _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredCommisionerSchemeList", new string[] { Constituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate, SubDivisionCode.ToString(), ExecutingOffice });
            return PartialView("_constituencyListForOfficer", _scheme);


        }


        public ActionResult findConstituencyWorkbyCode(string schemeID)
        {
            CurrentSession.Name = null;
            WorkDetails _scheme = (WorkDetails)Helper.ExecuteService("ConstituencyVS", "getWorkDetailsByID", schemeID);
            return PartialView("_updateWorkforOfficer", _scheme);
        }

        public ActionResult updateProgress(string schemeID)
        {
            ConstituencyWorkProgress work = new ConstituencyWorkProgress();
            work.schemeCode = Convert.ToInt64(schemeID);
            return PartialView("_updateProgress", work);
        }

        public ActionResult UpdateProgressbyOfficer(ConstituencyWorkProgress progress)
        {
            WorkDetails _scheme = (WorkDetails)Helper.ExecuteService("ConstituencyVS", "getWorkDetailsByID", progress.schemeCode);
            progress.workCode = _scheme.workCode;
            progress.constituencyCode = _scheme.eConstituencyID.Value;
            progress.submitDate = DateTime.Now;
            progress.createdBY = currentUser;
            progress.createdDate = DateTime.Now;
            progress.isActive = true;
            progress.isDeleted = false;

            Helper.ExecuteService("ConstituencyVS", "UpdateProgressbyOfficer", progress);
            return Json(progress.schemeCode, JsonRequestBehavior.AllowGet);
        }

        public ActionResult updateProgressImages(string schemeID)
        {
            // WorkDetails _scheme = (WorkDetails)Helper.ExecuteService("ConstituencyVS", "getWorkDetailsByID", schemeID);
            ConstituencyWorkProgressImages images = new ConstituencyWorkProgressImages();
            images.schemeCode = Convert.ToInt64(schemeID);

            return PartialView("_updateProgressImages", images);
        }

        public ActionResult SaveUploadedFile()
        {
            bool isSavedSuccessfully = true;
            string fName = "";
            string pathString = "";
            string NFileName = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    fName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {
                        var originalDirectory = new DirectoryInfo(string.Format("{0}tempimg\\", Server.MapPath(@"\")));

                        pathString = System.IO.Path.Combine(originalDirectory.ToString(), "conimg");

                        var fileName1 = Path.GetFileName(file.FileName);
                        string ext = Path.GetExtension(file.FileName);
                        NFileName = Guid.NewGuid().ToString() + ext;

                        bool isExists = System.IO.Directory.Exists(pathString);

                        if (!isExists)
                            System.IO.Directory.CreateDirectory(pathString);

                        var path = string.Format("{0}\\{1}", pathString, NFileName);
                        file.SaveAs(path);
                    }
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                isSavedSuccessfully = false;
            }

            if (isSavedSuccessfully)
            {
                return Json(new { Message = NFileName }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }

        public ActionResult UpdateImagesbyOfficer(ConstituencyWorkProgressImages images, string FileCaption)
        {
            string pathString = "";
            string TemppathString = "";

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            // string directory = FileSettings.SettingValue + SavedPdfPath;
            string url = "constituencyImage\\" + currentAssemby + "\\" + images.schemeCode + "\\";

            string path = FileSettings.SettingValue + url;
            var originalDirectory = new DirectoryInfo(string.Format("{0}", path));
            pathString = System.IO.Path.Combine(originalDirectory.ToString(), "Full");
            bool isExists = System.IO.Directory.Exists(pathString);

            if (!isExists)
                System.IO.Directory.CreateDirectory(pathString);

            var tempDir = new DirectoryInfo(string.Format("{0}tempimg\\", Server.MapPath(@"\")));
            TemppathString = System.IO.Path.Combine(tempDir.ToString(), "conimg");
            ResizeImage(600, TemppathString + "/" + images.tempPath, pathString + "\\" + images.tempPath);

            pathString = System.IO.Path.Combine(originalDirectory.ToString(), "thumb");
            isExists = System.IO.Directory.Exists(pathString);
            if (!isExists)
                System.IO.Directory.CreateDirectory(pathString);
            ResizeImage(150, TemppathString + "/" + images.tempPath, pathString + "\\" + images.tempPath);
            isExists = System.IO.File.Exists(TemppathString + "/" + images.tempPath);
            if (!isExists)
                System.IO.File.Delete(pathString);
            images.fileName = images.tempPath;
            images.thumbFilePath = url + "thumb";
            images.filePath = url + "Full"; ;
            images.createdBY = currentUser;
            images.createdDate = DateTime.Now;
            images.isActive = true;
            images.isDeleted = false;
            // images.
            WorkDetails _scheme = (WorkDetails)Helper.ExecuteService("ConstituencyVS", "getWorkDetailsByID", images.schemeCode);
            images.workCode = _scheme.workCode;
            images.constituencyCode = _scheme.eConstituencyID.Value;
            images.UploadedDate = DateTime.Now;
            Helper.ExecuteService("ConstituencyVS", "UpdateImagesbyOfficer", images);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public static void ResizeImage(int size, string filePath, string saveFilePath)
        {
            //variables for image dimension/scale
            double newHeight = 0;
            double newWidth = 0;
            double scale = 0;

            //create new image object
            Bitmap curImage = new Bitmap(filePath);

            //Determine image scaling
            if (curImage.Height > curImage.Width)
            {
                scale = Convert.ToSingle(size) / curImage.Height;
            }
            else
            {
                scale = Convert.ToSingle(size) / curImage.Width;
            }
            if (scale < 0 || scale > 1) { scale = 1; }

            //New image dimension
            newHeight = Math.Floor(Convert.ToSingle(curImage.Height) * scale);
            newWidth = Math.Floor(Convert.ToSingle(curImage.Width) * scale);

            //Create new object image
            Bitmap newImage = new Bitmap(curImage, Convert.ToInt32(newWidth), Convert.ToInt32(newHeight));
            Graphics imgDest = Graphics.FromImage(newImage);
            imgDest.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            imgDest.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            imgDest.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            imgDest.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            ImageCodecInfo[] info = ImageCodecInfo.GetImageEncoders();
            EncoderParameters param = new EncoderParameters(1);
            param.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);

            //Draw the object image
            imgDest.DrawImage(curImage, 0, 0, newImage.Width, newImage.Height);

            //Save image file
            newImage.Save(saveFilePath, info[1], param);

            //Dispose the image objects
            curImage.Dispose();
            newImage.Dispose();
            imgDest.Dispose();
        }
        public JsonResult Check_WorkCode(string CID, string workcode)
        {
            WorkDetails work = new WorkDetails();
            work.ControllingAuthorityID = CID;
            work.workCode = workcode;

            string checkedData = Helper.ExecuteService("ConstituencyVS", "CheckWorkCode_ByCID", work) as string;
            if (checkedData != "N")
            //if (checkedData != string.Empty || checkedData != null)
            {
                checkedData = "Y";
            }
            else
            {
                checkedData = "N";
            }
            return Json(checkedData, JsonRequestBehavior.AllowGet);

        }

        //schemeMapping _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeList", new string[] { Constituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate });
        [HttpGet]
        public ActionResult PdfconstituencyListForAcceptMLA(string Program, string Demand, string ControllingAuthority, string FinancialYear, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string DistrictID, string Constituency, string cFinancialYear, string sanctionedDate)
        {
            string SubDivisionCode = "0";
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            {
                if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
                {
                    SubDivisionCode = CurrentSession.SubDivisionId;
                    // SubdivisionIds = SubDivisionCode.Split(',').ToList();
                }
            }

            schemeMapping _scheme = (schemeMapping)gelFilteredMySchemeList(new string[] { Constituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate, SubDivisionCode });


           // (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeList", new string[] { Constituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate, SubDivisionCode });
            var Location = GeneratePdf(_scheme);
            return Json(Location, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Pdfconstituency(schemeMapping _scheme)
        {
            //string SubDivisionCode = "0";
            // schemeMapping _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeList", new string[] { Constituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate, SubDivisionCode });
            var Location = GeneratePdf(_scheme);
            return Json(Location, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult PdfconstituencyListForOfficer(string Program, string Demand, string ControllingAuthority, string FinancialYear, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string cFinancialYear, string sanctionedDate, string persontype,string ConId)
        {
            myConsituency = ConId;
            string SubDivisionCode = "0";
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            {
                if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
                {
                    SubDivisionCode = CurrentSession.SubDivisionId;

                }
            }


            if (myConsituency == "" && MemberCode != "")
            {
                string[] str = new string[2];
                str[0] = CurrentSession.AssemblyId;
                str[1] = MemberCode;
                string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCodebyMemberCodeAndAssemblyCode", str);
                if (strOutput != null)
                {
                    myConsituency = Convert.ToString(strOutput[0]);
                    CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                }
            }
            else
            {

                if (myConsituency == "1028")
                {
                    myConsituency = "0";
                }
                else
                {
                    string[] str = new string[2];
                    str[0] = CurrentSession.AssemblyId;
                    str[1] = ConId;
                    string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode", str);
                    if (strOutput != null)
                    {
                        myConsituency = Convert.ToString(strOutput[0]);
                        CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                    }

                    //string[] str = new string[2];
                    //str[0] = CurrentSession.AssemblyId;
                    //str[1] = myConsituency;
                    //string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
                    //if (strOutput != null)
                    //{
                    //    myConsituency = Convert.ToString(strOutput[0]);
                    //    CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                    //}

                }




            }


            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyDistrict", myConsituency);
            schemeMapping _scheme = (schemeMapping)gelFilteredMySchemeList(new string[] { myConsituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, _list[0].value.ToString(), aadharID, "true", cFinancialYear, sanctionedDate, SubDivisionCode });


           // (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeList", new string[] { myConsituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, _list[0].value.ToString(), aadharID, "true", cFinancialYear, sanctionedDate, SubDivisionCode });
            var Location = GeneratePdf(_scheme);
            return Json(Location, JsonRequestBehavior.AllowGet);
        }
        public Object GeneratePdf(schemeMapping _scheme)
        {
            try
            {
                int count = 0;
                string outXml = "";

                string fileName = "";
                string savedPDF = string.Empty;

                if (_scheme != null)
                {

                    outXml = @"<html>                           
                                <body style='font-family:DVOT-Yogesh;color:#003366;'><div style='width: 100%;'>";
                    outXml += @"</div>
<table style='width:100%'>
 
      <tr>
          <td style='text-align:center;font-size:28px;' >
             <b>" + CurrentSession.ConstituencyName + " Ward Works" + @"</b>
              </td>
              </tr>
      </table>

<table style='width: 100%; border-collapse: collapse' border: solid 1px black'>
               <thead>


  <tr>
       <td style='text-align: center; font-size: large;border:1px  solid black; '>Sr.No.</td>
       <td style='text-align: center; font-size: large;border:1px  solid black; '>Work Code</td>
       <td style='text-align: center; font-size: large;border:1px  solid black; '>Work Name</td>
       <td style='text-align: center; font-size: large; border:1px  solid black; '>Estimated Amount (Rs)/ Expected Completion Date</td>
       <td style='text-align: center; font-size: large; border:1px  solid black; '>Sanctioned Amount (Rs)/Date</td>
       <td style='text-align: center; font-size: large; border:1px  solid black; '>Ward Name</td>
       <td style='text-align: center; font-size: large;border:1px  solid black; '>Expenditure Till Date (Rs)</td>
       <td style='text-align: center; font-size: large;border:1px  solid black; '>Physical Progress Till Date (%)</td>
       <td style='text-align: center; font-size: large;border:1px  solid black; '>Executing Agency/Executing Office</td>
       <td style='text-align: center; font-size: large;border:1px  solid black; '>Current Status</td>
                  </tr>  

               </thead>

     

           ";



                    foreach (var item in _scheme.mySchemeList)
                    {
                        count = count + 1;
                        string style = (count % 2 == 0) ? "odd" : "even";
                        outXml += @"

  <tr>
            <td style='text-align: center;border:1px  solid black; '>" + count + @" </td>
            <td style='text-align: center;border:1px  solid black; '>" + item.workCode + @"</td>
            <td style='text-align: center;border:1px  solid black; '>" + item.workName + @"</td>
            <td style='text-align: center;border:1px  solid black; '>" + item.EstimatedAmount + "<br>" + item.CompletionDate + @"</td>
              <td style='text-align: center;border:1px  solid black; '>" + item.sanctionedAmount + "<br>" + item.sanctionedDate + @"</td>
            <td style='text-align: center;border:1px  solid black; '>" + item.WardName + @"</td>
<td style='text-align: center;border:1px  solid black; '>" + item.financialProgress + @"</td>
<td style='text-align: center;border:1px  solid black; '>" + item.physicalProgress + @"</td>
<td style='text-align: center;border:1px  solid black; '>" + item.ExecutiveDepartmentName + "<br>" + item.ExecutiveOfficeName + @"  </td>
<td style='text-align: center;border:1px  solid black; '>" + item.workStatus + @"</td>
        </tr>
                       
            

     
";
                    }
                }
                outXml += @"</table></body>
</html>";


                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ /ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/ReportPdfFile/";
                // fileName = "QuestionList" + "DateFrom" + "_" + Convert.ToDateTime(model.DateFrom).ToString("dd/MM/yyyy") + "_" + "DateTo" + "_" + Convert.ToDateTime(model.DateTo).ToString("dd/MM/yyyy") + "_" + "S" + ".pdf";
                // fileName = fileName.Replace("/", "-");
                //  HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);
                fileName = "ConstituencyWorksRpt.pdf";
                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                //DownloadDateWiseQuestion();
                return url + fileName;


            }

            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }

        #endregion Constituency List for officer


        #region Department Wise Report
        public ActionResult DepartmentWiseReport()
        {

            string[] str = new string[2];
            str[0] = CurrentSession.AssemblyId;
            str[1] = myConsituency;
            string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
            if (strOutput != null)
            {
                myConsituency = Convert.ToString(strOutput[0]);
                CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
            }

            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyDistrict", myConsituency);
            return PartialView("_DepartmentWiseReport", _list[0].value.ToString());
        }

        public ActionResult DepartmentWiseReportList(string financiaYear, string deptCode)
        {
            string requestUrl = "http://164.100.88.163/AndroidServiceSPBuild/AndroidService.svc/GetDepartmentWiseCummalativeWorksProgressDetails/" + MemberCode + "/" + financiaYear + "/" + deptCode + "/null/null/null/null";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
            try
            {
                WebResponse response = request.GetResponse();
                List<DepartmentWiseReport> List1 = new List<DepartmentWiseReport>();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    string str = reader.ReadToEnd();

                    if (!str.Trim().Equals("\"No Data Found\""))
                    {
                        List1 = JsonConvert.DeserializeObject<List<DepartmentWiseReport>>(str);
                    }
                    return PartialView("_DepartmentWiseReportList", List1);
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    return null;
                }
            }
            //return PartialView("_DepartmentWiseReportList",list1);
        }


        [HttpPost]
        public ContentResult UploadFiles()
        {
            string url = "~/constituencyDPR";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/constituencyDPR"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }

        [HttpPost]
        public ContentResult UploadFileSalientfeatures()
        {
            string url = "~/salientfeatures";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/salientfeatures"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }

        public JsonResult RemovePDFFiles()
        {
            string url = "~/constituencyDPR";
            //  string url = "~/ePaper/TempFile";
            string directory = Server.MapPath(url);

            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region for pdf and wordfile by robin
        public ActionResult ConstituencyWorksExcelAcceptMLA(string Program, string Demand, string ControllingAuthority, string FinancialYear, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string DistrictID, string Constituency, string cFinancialYear, string sanctionedDate,string ConId)
        {
            string SubDivisionCode = "0";
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            {
                if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
                {
                    SubDivisionCode = CurrentSession.SubDivisionId;

                }
            }
            schemeMapping _scheme = (schemeMapping)gelFilteredMySchemeList(new string[] { ConId, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate, SubDivisionCode });




            //(schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeList", new string[] { ConId, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, DistrictID, aadharID, "false", cFinancialYear, sanctionedDate, SubDivisionCode });
            string Result = this.GetSchemeHTML(_scheme.mySchemeList);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=ExcelList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();

            //return PartialView("_myConstituencyWorkList", _scheme);
        }

        public ActionResult FilteredMyConstituencyWorksExcel(string Program, string Demand, string ControllingAuthority, string FinancialYear, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string cFinancialYear, string sanctionedDate,string ConId)
        {
            string SubDivisionCode = "0";
            myConsituency = ConId;
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            {
                if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
                {
                    SubDivisionCode = CurrentSession.SubDivisionId;

                }
            }

            //if (persontype == "Speaker")
            //  {
            if (myConsituency == "" && MemberCode != "")
            {
                string[] str = new string[2];
                str[0] = CurrentSession.AssemblyId;
                str[1] = MemberCode;
                string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCodebyMemberCodeAndAssemblyCode", str);
                if (strOutput != null)
                {
                    myConsituency = Convert.ToString(strOutput[0]);
                    CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                }
            }
            else
            {
                if (myConsituency == "1028")
                {
                    myConsituency = "0";
                }
                else
                {
                    string[] str = new string[2];
                    str[0] = CurrentSession.AssemblyId;
                    str[1] = ConId;
                    string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode", str);
                    if (strOutput != null)
                    {
                        myConsituency = Convert.ToString(strOutput[0]);
                        CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                    }

                }




                
            }

            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyDistrict", myConsituency);
            schemeMapping _scheme = (schemeMapping)gelFilteredMySchemeList(new string[] { myConsituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, _list[0].value.ToString(), aadharID, "true", cFinancialYear, sanctionedDate, SubDivisionCode });


           // (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeList", new string[] { myConsituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, _list[0].value.ToString(), aadharID, "true", cFinancialYear, sanctionedDate, SubDivisionCode });

            string Result = this.GetSchemeHTML(_scheme.mySchemeList);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=ExcelList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();

            //return PartialView("_myConstituencyWorkList", _scheme);
        }

        //public ActionResult FilteredMyConstituencyWorksPDF(string Program, string Demand, string ControllingAuthority, string FinancialYear, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string cFinancialYear)
        //{
        //    if (myConsituency == "" && MemberCode != "")
        //    {
        //        string[] str = new string[2];
        //        str[0] = CurrentSession.AssemblyId;
        //        str[1] = MemberCode;
        //        string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCodebyMemberCodeAndAssemblyCode", str);
        //        if (strOutput != null)
        //        {
        //            myConsituency = Convert.ToString(strOutput[0]);
        //            CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
        //        }
        //    }
        //    List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyDistrict", myConsituency);
        //    schemeMapping _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeList", new string[] { myConsituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, _list[0].value.ToString(), aadharID, "true", cFinancialYear });

        //    string Result = this.GetSchemeHTML(_scheme.mySchemeList);

        //    Result = HttpUtility.UrlDecode(Result);
        //    Response.Clear();
        //    Response.AddHeader("content-disposition", "attachment;filename=PDFList.pdf");
        //    Response.Charset = "";
        //    Response.ContentType = "application/pdf";
        //    Response.Write(Result);
        //    Response.Flush();
        //    Response.End();
        //    return new EmptyResult();

        //    //return PartialView("_myConstituencyWorkList", _scheme);
        //}

        public ActionResult FilteredMyConstituencyWorksPDF(string Program, string Demand, string ControllingAuthority, string FinancialYear, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string cFinancialYear)
        {
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);
            string path = "";
            try
            {
                if (myConsituency == "" && MemberCode != "")
                {
                    string[] str = new string[2];
                    str[0] = CurrentSession.AssemblyId;
                    str[1] = MemberCode;
                    string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCodebyMemberCodeAndAssemblyCode", str);
                    if (strOutput != null)
                    {
                        myConsituency = Convert.ToString(strOutput[0]);
                        CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                    }
                }
                else
                {
                    string[] str = new string[2];
                    str[0] = CurrentSession.AssemblyId;
                    str[1] = myConsituency;
                    string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
                    if (strOutput != null)
                    {
                        myConsituency = Convert.ToString(strOutput[0]);
                        CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                    }
                }
                List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getConstituencyDistrict", myConsituency);
                schemeMapping _scheme = (schemeMapping)gelFilteredMySchemeList(new string[] { myConsituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, _list[0].value.ToString(), aadharID, "true", cFinancialYear });


               // (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeList", new string[] { myConsituency, Program, Demand, ControllingAuthority, FinancialYear, Agency, OwnerDepartment, Panchayat, WorkType, Status, _list[0].value.ToString(), aadharID, "true", cFinancialYear });

                string Result = GetSchemeHTML(_scheme.mySchemeList);

                Guid FId = Guid.NewGuid();
                string fileName = FId + "FilteredMyConstituencyWorksPDF.pdf";

                MemoryStream output = new MemoryStream();

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                           PdfPageOrientation.Portrait);

                string htmlStringToConvert = Result;

                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                AddElementResult addResult = page.AddElement(htmlToPdfElement);


                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;

                string url = "/PublicWorksPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "PublicWorks.pdf";

                }

                return pathRes;
                //return new EmptyResult();

            }
            catch (Exception)
            {

                throw;
            }
        }


        public string GetSchemeHTML(IEnumerable<myShemeList> model)
        {

            StringBuilder ReplyList = new StringBuilder();
            string CurrentConname = CurrentSession.ConstituencyName;
            if (model != null && model.Count() > 0)
            {
                ReplyList.Append(string.Format("<div style='text-align:center;'><h2>" + CurrentConname + " Ward Works</h2></div>"));
                ReplyList.Append("<div style='width:1000px;font-size:22px;'><table id='ResultTable'>");
                ReplyList.Append("<thead class='header' ><tr style='background-color: #428bca ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:30px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px;'>Sr No.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Work Code</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Work Name</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Estimated Amount (Rs)/ Expected Completion Date</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Sanctioned Amount (Rs)/Date</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Ward Name</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Expenditure Till Date (Rs)</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Physical Progress Till Date (%)</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Executing Agency/Executing Office</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Current Status</th>");
                ReplyList.Append("</tr></thead>");
                ReplyList.Append("<tbody>");
                int count = 0;
                foreach (var obj in model)
                {
                    count = count + 1;
                    ReplyList.Append("<tr>");
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", count));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.workCode));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.workName));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}/{1}</td>", obj.EstimatedAmount, obj.CompletionDate));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}/{1}</td>", obj.sanctionedAmount, obj.sanctionedDate));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.WardName ));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.financialProgress));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.physicalProgress));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}{1}</td>", obj.ExecutiveDepartmentName, obj.ExecutiveOfficeName));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.workStatus));
                    ReplyList.Append("</tr>");
                }
                ReplyList.Append("</tbody></table></div> ");
            }
            else
            {
                ReplyList.Append("No Record Found");
            }
            return ReplyList.ToString();
        }

        #endregion






        #region work details from services

        public ActionResult SearchWorkList(string memberCode, string financialYear,
       string controllingAuthorityId, string executingAgencyCode, string programmeName, string panchayatId, string workStatusId, string sanctionedDate, string cfinancialYear)
        {
            memberCode = CurrentSession.MemberCode;
            ViewBag.Header = "Get Works Details";

            return PartialView("_SummaryReportView", new string[] { memberCode, financialYear, controllingAuthorityId, executingAgencyCode, programmeName, panchayatId, workStatusId, sanctionedDate, cfinancialYear });
        }

        public ActionResult GetWorkPostList(string financialYear, string controllingAuthorityId, string programmeName, string executingAgencyCode, string panchayatId, string workStatusId, string summaryType, string cFinancialYear, string modifiedDate, string OwnerDepartment, string WorkType)
        {
            //WorkDetails report = new WorkDetails();
            string memberCode = CurrentSession.MemberCode;
            //string modifiedDate="null";
            var reportList = (List<DepartmentWiseWorksCummalativeProgressD>)GetAllWorkPostList(memberCode, financialYear,
         controllingAuthorityId, executingAgencyCode, programmeName, panchayatId, workStatusId, modifiedDate, summaryType, WorkType, cFinancialYear, OwnerDepartment);
            if (summaryType == "Executing Department")
            {
                ViewBag.Header = "Summary Type by Executing Department";
                ViewBag.ColName = "Executing Department";
            }
            if (summaryType == "Panchayat")
            {
                ViewBag.Header = "Summary Type by Panchayat";
                ViewBag.ColName = "Panchayat";
            }
            if (summaryType == "Program Name")
            {
                ViewBag.Header = "Summary Type by Program Name";
                ViewBag.ColName = "Program Name";
            }
            if (summaryType == "Work Type")
            {
                ViewBag.Header = "Summary Type by Work Type";
                ViewBag.ColName = "Work Type";
            }

            if (summaryType == "Controling Authority")
            {
                ViewBag.Header = "Summary Type by Controling Authority";
                ViewBag.ColName = "Controling Authority";
            }

            return PartialView("_WorkList1", reportList);
        }

        public ActionResult GetWorkPostListProgress(string financialYear, string controllingAuthorityId, string programmeName, string executingAgencyCode, string panchayatId, string workStatusId)
        {
            //WorkDetails report = new WorkDetails();
            string memberCode = CurrentSession.MemberCode;
            var reportList = (List<DepartmentWiseWorksProgressD>)GetAllWorkPostListProgress(memberCode, financialYear,
         controllingAuthorityId, executingAgencyCode, programmeName, panchayatId, workStatusId);
            ViewBag.Header = "Get Works Details by scheme";
            return PartialView("_WorkList2", reportList);
        }

        public ActionResult GetDetailsBySchemeId(string schemeID)
        {
            //var reportDetailsById = (WorkDetailsD)GetWorkDetailsBySId(schemeID);
            //WorkDetails thisModel = new WorkDetails();

            ////thisModel.districtName = reportDetailsById.dis

            //ViewBag.Header = "Detail by Work Scheme";
            //return PartialView("_WorkList3", reportDetailsById);
            // ErrorLog.WriteToLog(null, "updateID : " + updateID);


            WorkDetails _scheme = (WorkDetails)Helper.ExecuteService("ConstituencyVS", "getworkDetailsByUpdateID", schemeID);

            workBasicView details = (workBasicView)Helper.ExecuteService("ConstituencyVS", "getBasicDetailsOFWorks", _scheme.mySchemeID);
            if (details != null)
            {
                ViewBag.wordCode = details;

                ViewBag.financialYear = details.financialYear;
                ViewBag.workName = details.workName;
                ViewBag.workStatus = details.workStatus;
            }
            List<ConstituencyWorkProgressImages> list = (List<ConstituencyWorkProgressImages>)Helper.ExecuteService("ConstituencyVS", "getAllProgressImages", _scheme.mySchemeID);
            _scheme.Imagelist = list;
            return PartialView("_WorkList3", _scheme);
        }
        #endregion

        #region genric function for getting from services
        //public List<DepartmentWiseWorksCummalativeProgressD> GetAllWorkPostList(string memberCode, string financialYear,
        //string controllingAuthorityId, string executingAgencyCode, string programmeName, string panchayatId, string workStatusId, string modifiedDate, string SummaryType)
        //{
        //    string requestUrl = "http://164.100.88.163/AndroidServiceSPBuild/AndroidService.svc/GetWorkListWithSummaryType/" + memberCode + "/" + financialYear + "/" + controllingAuthorityId + "/" + executingAgencyCode + "/" + programmeName + "/" + panchayatId + "/" + workStatusId + "/" + modifiedDate + "/" + SummaryType;
        //    //GetDepartmentWiseCummalativeWorksProgressDetails
        //    //string requestUrl = "http://164.100.88.163/AndroidServiceSPBuild/AndroidService.svc/GetDepartmentWiseCummalativeWorksProgressDetails/" + memberCode + "/" + financialYear + "/" + controllingAuthorityId + "/" + executingAgencyCode + "/" + programmeName + "/" + panchayatId + "/" + workStatusId + "/" + modifiedDate;
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
        //    try
        //    {
        //        WebResponse response = request.GetResponse();
        //        List<DepartmentWiseWorksCummalativeProgressD> List1 = new List<DepartmentWiseWorksCummalativeProgressD>();
        //        using (Stream responseStream = response.GetResponseStream())
        //        {
        //            StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
        //            string str = reader.ReadToEnd();

        //            if (!str.Trim().Equals("\"No Data Found\""))
        //            {
        //                List1 = JsonConvert.DeserializeObject<List<DepartmentWiseWorksCummalativeProgressD>>(str);
        //            }
        //            return List1;
        //        }
        //    }
        //    catch (WebException ex)
        //    {
        //        WebResponse errorResponse = ex.Response;
        //        using (Stream responseStream = errorResponse.GetResponseStream())
        //        {
        //            StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
        //            String errorText = reader.ReadToEnd();
        //            return null;
        //        }
        //    }
        //}

        public List<DepartmentWiseWorksCummalativeProgressD> GetAllWorkPostList(string memberCode, string financialYear,//
       string controllingAuthorityId, string executingAgencyCode, string programmeName, string panchayatId, string workStatusId, string modifiedDate, string SummaryType, string WorkType, string cFinancialYear, string OwnerDepartment)
        {//cFinancialYear
            if (workStatusId != "3")
            {
                cFinancialYear = null;
            }
            if (financialYear == "0")
            {
                financialYear = null;
            }
            if (controllingAuthorityId == "null")
            {
                controllingAuthorityId = null;
            }
            if (executingAgencyCode == "null")
            {
                executingAgencyCode = null;
            }
            if (programmeName == "null")
            {
                programmeName = null;
            }
            if (panchayatId == "null")
            {
                panchayatId = null;
            }
            if (workStatusId == "null")
            {
                workStatusId = null;
            }
            if (modifiedDate == "null")
            {
                modifiedDate = null;
            }
            if (WorkType == "0")
            {
                WorkType = null;
            }
            if (OwnerDepartment == "0")
            {
                OwnerDepartment = null;
            }
            List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
            mParameter.Add(new KeyValuePair<string, string>("@ai_member_id", Convert.ToString(memberCode)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_financialYear", Convert.ToString(financialYear)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_ControllingAuthority_id", Convert.ToString(controllingAuthorityId)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_executing_agency_code", Convert.ToString(executingAgencyCode)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_programme_name", Convert.ToString(programmeName)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_panchayat_id", Convert.ToString(panchayatId)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_workStatus_id", Convert.ToString(workStatusId)));
            mParameter.Add(new KeyValuePair<string, string>("@ad_modified_date", Convert.ToString(modifiedDate)));//sanctioned date is modified date
            mParameter.Add(new KeyValuePair<string, string>("@summaryType", Convert.ToString(SummaryType)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_CfinancialYear", Convert.ToString(cFinancialYear)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_WorkType", Convert.ToString(WorkType)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_OwnerDepartment", Convert.ToString(OwnerDepartment)));


            DataSet dataDate = null;
            dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "USP_GetsummaryTypeWorkDetails", mParameter);


            List<DepartmentWiseWorksCummalativeProgressD> List1 = new List<DepartmentWiseWorksCummalativeProgressD>();
            if (dataDate != null)
            {
                DepartmentWiseWorksCummalativeProgressD PublicPas = new DepartmentWiseWorksCummalativeProgressD();
                foreach (DataRow dr in dataDate.Tables[0].Rows)
                {
                    //var empList = dataDate.Tables[0].AsEnumerable().Select(dataRow => new DepartmentWiseWorksCummalativeProgressD
                    //{
                    //    DeptId = dataRow.Field<string>("DeptID"),
                    //    DeptName = dataRow.Field<string>("DeptName"),
                    //    SanctionedAmount = dataRow.Field<string>("TotalWorks"),
                    //    TotalWorks = dataRow.Field<string>("SanctionedAmount")
                    //});
                    DepartmentWiseWorksCummalativeProgressD obj = new DepartmentWiseWorksCummalativeProgressD();
                    obj.DeptId = Convert.ToString(dr["DeptID"]).Trim();
                    obj.DeptName = Convert.ToString(dr["DeptName"]).Trim();
                    obj.TotalWorks = Convert.ToString(dr["TotalWorks"]).Trim();
                    obj.SanctionedAmount = Convert.ToString(dr["SanctionedAmount"]).Trim();
                    List1.Add(obj);
                }

            }
            return List1;
        }

        public List<DepartmentWiseWorksProgressD> GetAllWorkPostListProgress(string memberCode, string financialYear,
        string controllingAuthorityId, string executingAgencyCode, string programmeName, string panchayatId, string workStatusId)
        {
            string requestUrl = "http://164.100.88.163/AndroidServiceSPBuild/AndroidService.svc/GetDepartmentWiseWorksProgressDetails/" + memberCode + "/" + financialYear + "/" + controllingAuthorityId + "/" + executingAgencyCode + "/" + programmeName + "/" + panchayatId + "/" + workStatusId;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
            try
            {
                WebResponse response = request.GetResponse();
                List<DepartmentWiseWorksProgressD> List1 = new List<DepartmentWiseWorksProgressD>();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    string str = reader.ReadToEnd();

                    if (!str.Trim().Equals("\"No Data Found\""))
                    {
                        List1 = JsonConvert.DeserializeObject<List<DepartmentWiseWorksProgressD>>(str);
                    }
                    return List1;
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    return null;
                }
            }
        }

        public WorkDetailsD GetWorkDetailsBySId(string schemeID)
        {
            string requestUrl = "http://164.100.88.163/AndroidServiceSPBuild/AndroidService.svc/GetWorksDetails/" + schemeID;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
            try
            {
                WebResponse response = request.GetResponse();
                WorkDetailsD ReportDetails = new WorkDetailsD();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    string str = reader.ReadToEnd();

                    if (!str.Trim().Equals("\"No Data Found\""))
                    {
                        ReportDetails = JsonConvert.DeserializeObject<WorkDetailsD>(str);
                    }
                    return ReportDetails;
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    return null;
                }
            }
        }
        #endregion

        public ActionResult SummaryListPDF(string financialYear, string controllingAuthorityId, string programmeName, string executingAgencyCode, string panchayatId, string workStatusId, string summaryType, string cFinancialYear, string modifiedDate, string OwnerDepartment, string WorkType)
        {
            //WorkDetails report = new WorkDetails();
            string memberCode = CurrentSession.MemberCode;
            //string modifiedDate = "null";//
            var reportList = (List<DepartmentWiseWorksCummalativeProgressD>)GetAllWorkPostList(memberCode, financialYear,
         controllingAuthorityId, executingAgencyCode, programmeName, panchayatId, workStatusId, modifiedDate, summaryType, WorkType, cFinancialYear, OwnerDepartment);

            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);
            string path = "";
            try
            {

                string Result = GetWorksReport(reportList, summaryType);

                Guid FId = Guid.NewGuid();
                string fileName = FId + "SummaryReportofWorks.pdf";

                MemoryStream output = new MemoryStream();
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                           PdfPageOrientation.Portrait);
                string htmlStringToConvert = Result;

                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                AddElementResult addResult = page.AddElement(htmlToPdfElement);


                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;

                string url = "/SummaryWorksPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "PublicWorks.pdf";

                }

                return pathRes;
                //return new EmptyResult();

            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult SummaryListExcel(string financialYear, string controllingAuthorityId, string Demand, string programmeName, string executingAgencyCode, string panchayatId, string workStatusId, string summaryType, string cFinancialYear, string modifiedDate, string WorkType, string OwnerDepartment)
        {
            string memberCode = CurrentSession.MemberCode;
            //string modifiedDate = "null";
            var reportList = (List<DepartmentWiseWorksCummalativeProgressD>)GetAllWorkPostList(memberCode, financialYear,
          controllingAuthorityId, executingAgencyCode, programmeName, panchayatId, workStatusId, modifiedDate, summaryType, WorkType, cFinancialYear, OwnerDepartment);

            string Result = this.GetWorksReport(reportList, summaryType);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=ExcelList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();

            //return PartialView("_myConstituencyWorkList", _scheme);
        }

        public string GetWorksReport(List<DepartmentWiseWorksCummalativeProgressD> model, string summaryType)
        {
            StringBuilder ReplyList = new StringBuilder();
            //string ReplyList;
            if (model != null && model.Count() > 0)
            {
                ReplyList.Append(string.Format("<div style='text-align:center;'><h2>Summary Details</h2></div>"));
                ReplyList.Append("<div style='width:1000px;font-size:22px;'><table id='ResultTable'>");
                ReplyList.Append("<thead class='header' ><tr style='background-color: #428bca ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:30px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px;'>Sr.No</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >" + summaryType + "</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Total Works</th>");
                ReplyList.Append("<th style='width:150px;text-align:right;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Sanctioned Amount (Rs)</th>");
                ReplyList.Append("</tr></thead>");
                ReplyList.Append("<tbody>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    ReplyList.Append("<tr>");
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", count));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.DeptName));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.TotalWorks));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;text-align:right'>{0}</td>", obj.SanctionedAmount));
                    ReplyList.Append("</tr>");
                }
                ReplyList.Append("</tbody></table></div> ");
            }
            else
            {
                ReplyList.Append("No Record Found");
            }
            return ReplyList.ToString();
        }



        public ActionResult SearchWorkListForNodal(string FinancialYear, string ControllingAuthority, string Demand, string Program, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string DistrictID, string Constituency, string cFinancialYear, string sanctionedDate, string summaryTypeID)
        {
            string SubDivisionCode = "0";
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            {
                if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
                {
                    SubDivisionCode = CurrentSession.SubDivisionId;

                }
            }

            string ExecutingOfficeCode = CurrentSession.OfficeId;
            var reportList = (List<DepartmentWiseWorksCummalativeProgressD>)GetAllWorkPostListNodal(ExecutingOfficeCode, FinancialYear,
         ControllingAuthority, Agency, Program, Panchayat, Status, sanctionedDate, summaryTypeID, WorkType, cFinancialYear, OwnerDepartment, DistrictID, Constituency, SubDivisionCode);
            if (summaryTypeID == "Executing Department")
            {
                ViewBag.Header = "Summary Type by Executing Department";
                ViewBag.ColName = "Executing Department";
            }
            if (summaryTypeID == "Panchayat")
            {
                ViewBag.Header = "Summary Type by Panchayat";
                ViewBag.ColName = "Panchayat";
            }
            if (summaryTypeID == "Program Name")
            {
                ViewBag.Header = "Summary Type by Program Name";
                ViewBag.ColName = "Program Name";
            }
            if (summaryTypeID == "Work Type")
            {
                ViewBag.Header = "Summary Type by Work Type";
                ViewBag.ColName = "Work Type";
            }

            if (summaryTypeID == "Controling Authority")
            {
                ViewBag.Header = "Summary Type by Controling Authority";
                ViewBag.ColName = "Controling Authority";
            }

            return PartialView("_WorkListForNodal", reportList);
        }

        public List<DepartmentWiseWorksCummalativeProgressD> GetAllWorkPostListNodal(string ExecutingOfficeCode, string financialYear,//
      string controllingAuthorityId, string executingAgencyCode, string programmeName, string panchayatId, string workStatusId, string modifiedDate, string SummaryType, string WorkType, string cFinancialYear, string OwnerDepartment, string DistrictID, string Constituency, string SubDivisionCode)
        {//cFinancialYear
            if (workStatusId != "3")
            {
                cFinancialYear = null;
            }
            if (financialYear == "0")
            {
                financialYear = null;
            }
            if (controllingAuthorityId == "0")
            {
                controllingAuthorityId = null;
            }
            if (executingAgencyCode == "0")
            {
                executingAgencyCode = null;
            }
            if (programmeName == "0")
            {
                programmeName = null;
            }
            if (panchayatId == "0")
            {
                panchayatId = null;
            }
            if (workStatusId == "0")
            {
                workStatusId = null;
            }
            if (modifiedDate == "")
            {
                modifiedDate = null;
            }
            if (WorkType == "0")
            {
                WorkType = null;
            }
            if (OwnerDepartment == "0")
            {
                OwnerDepartment = null;
            }
            if (DistrictID == "0")
            {
                DistrictID = null;
            }
            if (Constituency == "0")
            {
                Constituency = null;
            }
            if (SubDivisionCode == "0")
            {
                SubDivisionCode = null;
            }
            if (ExecutingOfficeCode == "0")
            {
                ExecutingOfficeCode = null;
            }
            List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
            //mParameter.Add(new KeyValuePair<string, string>("@ai_member_id", Convert.ToString(memberCode)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_financialYear", Convert.ToString(financialYear)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_ControllingAuthority_id", Convert.ToString(controllingAuthorityId)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_executing_agency_code", Convert.ToString(executingAgencyCode)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_executing_agency_oofice_code", Convert.ToString(ExecutingOfficeCode)));//executing office code
            mParameter.Add(new KeyValuePair<string, string>("@ai_programme_name", Convert.ToString(programmeName)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_panchayat_id", Convert.ToString(panchayatId)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_workStatus_id", Convert.ToString(workStatusId)));
            mParameter.Add(new KeyValuePair<string, string>("@ad_modified_date", Convert.ToString(modifiedDate)));//sanctioned date is modified date
            mParameter.Add(new KeyValuePair<string, string>("@summaryType", Convert.ToString(SummaryType)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_CfinancialYear", Convert.ToString(cFinancialYear)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_WorkType", Convert.ToString(WorkType)));
            mParameter.Add(new KeyValuePair<string, string>("@ai_OwnerDepartment", Convert.ToString(OwnerDepartment)));//@ai_DistrictId
            //mParameter.Add(new KeyValuePair<string, string>("@ai_DistrictId", Convert.ToString(DistrictID)));
            mParameter.Add(new KeyValuePair<string, string>("@SubDivisionCode", SubDivisionCode));//@ai_DistrictId
            DataSet dataDate = null;
            dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "USP_GetWorksDetails_ForNodal", mParameter);


            List<DepartmentWiseWorksCummalativeProgressD> List1 = new List<DepartmentWiseWorksCummalativeProgressD>();
            if (dataDate != null)
            {
                DepartmentWiseWorksCummalativeProgressD PublicPas = new DepartmentWiseWorksCummalativeProgressD();
                foreach (DataRow dr in dataDate.Tables[0].Rows)
                {
                    //var empList = dataDate.Tables[0].AsEnumerable().Select(dataRow => new DepartmentWiseWorksCummalativeProgressD
                    //{
                    //    DeptId = dataRow.Field<string>("DeptID"),
                    //    DeptName = dataRow.Field<string>("DeptName"),
                    //    SanctionedAmount = dataRow.Field<string>("TotalWorks"),
                    //    TotalWorks = dataRow.Field<string>("SanctionedAmount")
                    //});
                    DepartmentWiseWorksCummalativeProgressD obj = new DepartmentWiseWorksCummalativeProgressD();
                    obj.DeptId = Convert.ToString(dr["DeptID"]).Trim();
                    obj.DeptName = Convert.ToString(dr["DeptName"]).Trim();
                    obj.TotalWorks = Convert.ToString(dr["TotalWorks"]).Trim();
                    obj.SanctionedAmount = Convert.ToString(dr["SanctionedAmount"]).Trim();
                    List1.Add(obj);
                }

            }
            return List1;
        }

        public ActionResult SummaryListExcelNodal(string FinancialYear, string ControllingAuthority, string Demand, string Program, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string DistrictID, string Constituency, string cFinancialYear, string sanctionedDate, string summaryTypeID)
        {
            string SubDivisionCode = "0";
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            {
                if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
                {
                    SubDivisionCode = CurrentSession.SubDivisionId;

                }
            }
            // string memberCode = CurrentSession.MemberCode;
            string ExecutingOfficeCode = CurrentSession.OfficeId;
            //string modifiedDate = "null";
            var reportList = (List<DepartmentWiseWorksCummalativeProgressD>)GetAllWorkPostListNodal(ExecutingOfficeCode, FinancialYear,
         ControllingAuthority, Agency, Program, Panchayat, Status, sanctionedDate, summaryTypeID, WorkType, cFinancialYear, OwnerDepartment, DistrictID, Constituency, SubDivisionCode);

            string Result = this.GetWorksReport(reportList, summaryTypeID);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=ExcelList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();

            //return PartialView("_myConstituencyWorkList", _scheme);
        }

        public ActionResult SummaryListPDFNodal(string FinancialYear, string ControllingAuthority, string Demand, string Program, string Agency, string OwnerDepartment, string Panchayat, string WorkType, string Status, string DistrictID, string Constituency, string cFinancialYear, string sanctionedDate, string summaryTypeID)
        {
            string SubDivisionCode = "0";
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SubUserTypeID == "40")
            {
                if (CurrentSession.SubDivisionId != "" || CurrentSession.SubDivisionId != null)
                {
                    SubDivisionCode = CurrentSession.SubDivisionId;

                }
            }
            //WorkDetails report = new WorkDetails();
            string memberCode = CurrentSession.MemberCode;
            string ExecutingOfficeCode = CurrentSession.OfficeId;
            //string modifiedDate = "null";//
            var reportList = (List<DepartmentWiseWorksCummalativeProgressD>)GetAllWorkPostListNodal(ExecutingOfficeCode, FinancialYear,
         ControllingAuthority, Agency, Program, Panchayat, Status, sanctionedDate, summaryTypeID, WorkType, cFinancialYear, OwnerDepartment, DistrictID, Constituency, SubDivisionCode);

            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);
            string path = "";
            try
            {

                string Result = GetWorksReport(reportList, summaryTypeID);

                Guid FId = Guid.NewGuid();
                string fileName = FId + "SummaryReportofWorks.pdf";

                MemoryStream output = new MemoryStream();
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                           PdfPageOrientation.Portrait);
                string htmlStringToConvert = Result;

                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                AddElementResult addResult = page.AddElement(htmlToPdfElement);


                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;

                string url = "/SummaryWorksPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "PublicWorks.pdf";

                }

                return pathRes;
                //return new EmptyResult();

            }
            catch (Exception)
            {

                throw;
            }
        }



        public ActionResult StepsforUpdatingWorks()
        {
            return PartialView("_StepsforUpdatingWorks");

        }
    }
}