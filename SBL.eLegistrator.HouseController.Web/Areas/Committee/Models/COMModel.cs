﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SBL.DomainModel.Models.LOB;
#pragma warning disable CS0105 // The using directive for 'System' appeared previously in this namespace
using System;
#pragma warning restore CS0105 // The using directive for 'System' appeared previously in this namespace
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace SBL.eLegistrator.HouseController.Web.Areas.Committee.Models
{
    public class COMModel
    {
        public string COMId { get; set; }
        public string AssemblyId { get; set; }
        public string AssemblyName { get; set; }
        public string AssemblyNameLocal { get; set; }
        public string SessionId { get; set; }
        public string SessionName { get; set; }
        public string SessionNameLocal { get; set; }
        public string MeetingDate { get; set; }
        // public string SessionDateLocal { get; set; }
        public string MeetingTime { get; set; }
        // public string SessionTimeLocal { get; set; }
        public string SrNo1 { get; set; }
        public string SrNo2 { get; set; }
        public string SrNo3 { get; set; }

        [Column(TypeName = "ntext")]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string TextCOM { get; set; }

        public string TextMinister { get; set; }
        public string TextSpeaker { get; set; }
        public string TextBrief { get; set; }
        public string TextCurrent { get; set; }

        public string PPTLocation { get; set; }
        public string PDFLocation { get; set; }
        public string VideoLocation { get; set; }
        public string ActionDocumentType { get; set; }
        public bool IsEVoting { get; set; }
        public string ApproverdBy { get; set; }
        public string ApproverdDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreateDate { get; set; }
        public string PublishedBy { get; set; }
        public string PublishedDate { get; set; }
        // public string IsSubmitted { get; set; }
        // public string SubmittedDate { get; set; }

        //  public bool IsCorrisandumPending { get; set; }


        public bool? IsDeleted { get; set; }

        public bool? IsCorrigendum { get; set; }

        //  public bool? IsApproved { get; set; }

        public string SubmittedTime { get; set; }

        /*All List Declaration*/
        public List<COMModel> ListCOMBYId { get; set; }
        public string CommitteeId { get; set; }
        public string CommitteeName { get; set; }
        //Paging Variables
        public int ResultCount { get; set; }
        public int RowsPerPage { get; set; }
        public int selectedPage { get; set; }
        public int loopStart { get; set; }
        public int loopEnd { get; set; }
        public int PageNumber { get; set; }

        public virtual ICollection<mSession> mSessionList { get; set; }
        public virtual ICollection<mAssembly> mAssemblyList { get; set; }
        public virtual ICollection<mDepartment> mDepartmentList { get; set; }
        public virtual ICollection<mMinister> mMinisterList { get; set; }
        public virtual ICollection<mCommittee> mCommitteeList { get; set; }
        public virtual ICollection<mCommitteeRepType> mCommitteeRepTypeList { get; set; }
        //  public virtual ICollection<mSession> mSessionDateList { get; set; }
    }
    public class AddCOMModel
    {
        public string RecordId { get; set; }
        public string COMId { get; set; }
        public string CommitteeId { get; set; }
        public string AssemblyId { get; set; }
        public string AssemblyName { get; set; }
        public string SessionId { get; set; }
        public string SessionName { get; set; }

        [Required(ErrorMessage = "Please select Session Date")]
        public string MeetingDate { get; set; }
        public string MeetingTime { get; set; }

        public string SrNo1 { get; set; }
        public string SrNo2 { get; set; }
        public string SrNo3 { get; set; }
        public string SrNo11 { get; set; }
        public string SrNo22 { get; set; }
        public string SrNo33 { get; set; }

        [Required(ErrorMessage = "Please enter Business")]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string TextCOM { get; set; }

        public bool PageBreak { get; set; }

        public string TextMinister { get; set; }

        [Required(ErrorMessage = "Please enter Business")]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string TextSpeaker { get; set; }
        public bool IsSpeakerPageBreak { get; set; }


        public string TextBrief { get; set; }
        //    public string ConcernedMemberId { get; set; }
        //    public string ConcernedMemberName { get; set; }
        //    public string ConcernedMemberDesignationId { get; set; }
        //    public string ConcernedMemberDesignationName { get; set; }
        //    public string ConcernedDeptId { get; set; }
        ////    public string ConcernedDeptName { get; set; }
        //     public string ConcernedCommitteeId { get; set; }
        //   public string ConcernedCommitteeName { get; set; }
        //   public string ConcernedRuleId { get; set; }
        //     public string ConcernedRuleName { get; set; }
        //    public string ConcernedRuleNameLocal { get; set; }


        public string ConcernedCommitteeRepTypId { get; set; }
        public string ConcernedCommitteeRepTypName { get; set; }

        public string ConcernedDeptId { get; set; }
        public string ConcernedDeptName { get; set; }

        public string ConcernedCommitteeId { get; set; }
        [Required(ErrorMessage = "Please select Committee Number ")]
        public string ConcernedCommitteeNumberId { get; set; }
        
        public string ConcernedCommitteeName { get; set; }

        public string CommitteeReportTitle { get; set; }

        public string ConcernedEventId { get; set; }
        public string ConcernedEventName { get; set; }
        public string ConcernedEventNameLocal { get; set; }

        public string PPTLocation { get; set; }
        public string PDFLocation { get; set; }
        public string VideoLocation { get; set; }
        public string ActionDocumentType { get; set; }
        public string NatureOfDocument { get; set; }

        public bool IsEVoting { get; set; }
        public string PaperLaidIdTemp { get; set; }
        public string PaperLaidId { get; set; }
        public string NewPdfPath { get; set; }
        public string ApproverdBy { get; set; }
        public string ApproverdDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreateDate { get; set; }
        public string PublishedBy { get; set; }
        public string PublishedDate { get; set; }

        public int? CorrigendumId { get; set; }
        /*Paging Properties Start*/
        public int ResultCount { get; set; }
        public int RowsPerPage { get; set; }
        public int selectedPage { get; set; }
        public int loopStart { get; set; }
        public int loopEnd { get; set; }
        public int PageNumber { get; set; }

        /*Paging Properties End*/

        public bool? IsDeleted { get; set; }

        public bool? IsCorrigendum { get; set; }

        public HttpPostedFileBase Document { get; set; }

        public virtual ICollection<mMinister> mMinisterList { get; set; }
        public virtual ICollection<mDepartment> mDepartmentList { get; set; }
        public virtual ICollection<mEvent> mEventList { get; set; }
        public virtual ICollection<mCommittee> mCommitteeList { get; set; }
        public virtual ICollection<mCommitteeNumber> mCommitteeNumberList { get; set; }
        public virtual ICollection<mRule> mRuleList { get; set; }

        public virtual ICollection<mCommitteeRepType> mCommitteeRepTypeList { get; set; }

        public virtual ICollection<mResolution> mResolution { get; set; }

        public virtual ICollection<mSession> mSessionList { get; set; }

        public int ResolutionId { get; set; }
        public List<AddCOMModel> COMList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<SelectListItem> yearList { get; set; }
        //[NotMapped]
        //public string BillTextNumber { get; set; }
        //[NotMapped]
        //public int BillNumberYear { get; set; }
        //[NotMapped]
        //public string BillTextTitle { get; set; }

    }

    public class AddLines
    {
        public string SrNo1 { get; set; }
        public string SrNo2 { get; set; }
        public string SrNo3 { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string TextCOM { get; set; }

        public string TextMinister { get; set; }
        public string TextSpeaker { get; set; }
        public string TextBrief { get; set; }
        //public string ConcernedMemberId { get; set; }
        //public string ConcernedMemberName { get; set; }
        //public string ConcernedMemberDesignationId { get; set; }
        //public string ConcernedMemberDesignationName { get; set; }
        //public string ConcernedDeptId { get; set; }
        //public string ConcernedDeptName { get; set; }
        //public string ConcernedCommitteeId { get; set; }
        //public string ConcernedCommitteeName { get; set; }
        public string PPTLocation { get; set; }
        public string PDFLocation { get; set; }
        public string VideoLocation { get; set; }
        public string ActionDocumentType { get; set; }
        public string NatureOfDocument { get; set; }

        public bool IsEVoting { get; set; }
        public string ApproverdBy { get; set; }
        public string ApproverdDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreateDate { get; set; }
        public string PublishedBy { get; set; }
        public string PublishedDate { get; set; }

        public virtual ICollection<mMinister> mMinisterList { get; set; }
        public virtual ICollection<mDepartment> mDepartmentList { get; set; }
        public virtual ICollection<mCommittee> mCommitteeList { get; set; }

        public virtual ICollection<mCommitteeRepType> mCommitteeRepTypeList { get; set; }
    }

    public class mSession
    {
        public string AssemblyId { get; set; }
        public string SessionId { get; set; }
        public string SessionDate { get; set; }
        public string Name { get; set; }

    }

    public class mMinister
    {
        public string MinisterName { get; set; }
        public string MinisterId { get; set; }
        public string MinisterDesignation { get; set; }
    }


    public class mDepartment
    {
        public string DeptName { get; set; }
        public string DeptId { get; set; }
    }
    public class mEvent
    {
        public string EventName { get; set; }
        public string EventId { get; set; }
    }

    public class mCommittee
    {
        public string CommitteeName { get; set; }
        public string CommitteeId { get; set; }
    }
    public class mCommitteeNumber
    {
        public string CommitteeNumberName { get; set; }
        public string CommitteeNumberId { get; set; }
    }
    public class mCommitteeRepType
    {
        public string ReportTypeId { get; set; }
        public string ReportTypeName { get; set; }
    }
    public class mRule
    {
        public string RuleId { get; set; }
        public string RuleName { get; set; }
    }

    public class mAssembly
    {
        public string AssemblyID { get; set; }
        public string AssemblyName { get; set; }
        public string AssemblyName_Local { get; set; }
    }

}