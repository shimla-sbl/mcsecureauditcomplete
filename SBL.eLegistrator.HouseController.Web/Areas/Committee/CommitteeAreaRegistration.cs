﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Committee
{
    public class CommitteeAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Committee";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Committee_default",
                "Committee/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
