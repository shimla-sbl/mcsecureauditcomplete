﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.eFile;

namespace SBL.eLegistrator.HouseController.Web.Areas.VidhanSabhaDepartment.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    //[CustomAuthorize(Users = "4,5")]//==>UserType
    //[CustomAuthorize(Roles = "53")]//==>ModuleIDs
    public class VidhanSabhaDeptController : Controller
    {
        //
        // GET: /VidhanSabhaDepartment/VidhanSabhaDept/

        public ActionResult Index()
        {


            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();

            mSession mSessionModel = new mSession();
            mAssembly mAssemblymodel = new mAssembly();
            mBranches modelBranch = new mBranches();
            modelBranch.UserId =Guid.Parse(CurrentSession.UserID);
            model = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", model);
            model.mSession = (ICollection<mSession>)Helper.ExecuteService("Session", "GetAllSessionData", mSessionModel);
            model.mAssembly = (ICollection<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssembly", mAssemblymodel);

            model.BranchesList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "GBranchList", modelBranch);
            var firstOrDefault = model.BranchesList.FirstOrDefault();
           // CurrentSession.BranchId = model.BranchesList.First();

            if (firstOrDefault != null)
            {
                if (string.IsNullOrEmpty(CurrentSession.BranchId))
                {
                    CurrentSession.BranchId = firstOrDefault.BranchId.ToString();
                    CurrentSession.BranchName = firstOrDefault.BranchName.ToString();
                }
                model.BranchId = Convert.ToInt32(CurrentSession.BranchId);
            }
            //foreach (var item in model.BranchesList)
            //        {
            //                CurrentSession.BranchId = item.BranchId.ToString();
            //        }
            //model.CurtSessBranchId=
           // CurrentSession.DeptID = DistDept;
            //Added by Ashwani 
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            if (siteSettingMod != null)
            {
                if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    CurrentSession.AssemblyId = siteSettingMod.AssemblyCode.ToString();
                }

                if (string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    CurrentSession.SessionId = siteSettingMod.SessionCode.ToString();
                }
            }

            //return View(paperLaidModel);
            //tMemberNotice model = new tMemberNotice();

            //if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
            //{
            //    model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            //}

            //model.UserID = new Guid(CurrentSession.UserID);

            //if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            //{
            //    model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            //}

            //if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            //{
            //    model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            //}

            //model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDynamicMainMenuByUserId", model);

            //if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            //{
            //    CurrentSession.AssemblyId = model.AssemblyID.ToString();
            //}

            //if (string.IsNullOrEmpty(CurrentSession.SessionId))
            //{
            //    CurrentSession.SessionId = model.SessionID.ToString();
            //}


            //ViewBag.MenuId = CurrentSession.MenuId;

            //Get the Total count of All Type of question.
            //added code venkat for dynamic
            model.UserID = new Guid(CurrentSession.UserID);
            //end--------------------------------------------------
            if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
            {
                model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            }
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }


            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model);

            if (!string.IsNullOrEmpty(CurrentSession.DeptID))
            {
                tPaperLaidV model1 = new tPaperLaidV();
                List<mDepartment> deptInfo = new List<mDepartment>();
                model1.DepartmentId = CurrentSession.DeptID;
                deptInfo = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartmentByIDs", model1);
                foreach (var item in deptInfo)
                {
                    if (item != null)
                    {
                        model.DeparmentNameByids += item.deptname + ", ";
                    }
                }
                if (model.DeparmentNameByids != null && model.DeparmentNameByids.LastIndexOf(",") > 0)
                {
                    model.DeparmentNameByids = model.DeparmentNameByids.Substring(0, model.DeparmentNameByids.Length - 2);
                }
            }

            return View(model);
        }


        public ActionResult GetSubMenu(int ModuleId, string ActIds, string Assigned_SubAccess)
        {
            mUserSubModules SM = new mUserSubModules();
            List<mUserSubModules> lstmUserSubModules = new List<mUserSubModules>();
            SM.ModuleId = ModuleId;
            SM = (mUserSubModules)Helper.ExecuteService("Notice", "GetSubMenuByMenuId", SM);
            //SM.MemberId = MemberId;
            SM.ActionIds = ActIds;
            //////////////////////////////////////This code For Manage SubMenu By User//////////////////////////////////////
            if (SM != null && SM.AccessList.Count > 0)
            {
                if (!string.IsNullOrEmpty(Assigned_SubAccess))
                {
                    string[] SubAccessArry = Assigned_SubAccess.Split(',');
                    foreach (var item in SM.AccessList)
                    {
                        if (item != null)
                        {
                            if (SubAccessArry.Contains(item.SubModuleId.ToString()))
                            {
                                lstmUserSubModules.Add(item);
                            }
                        }
                    }
                    if (lstmUserSubModules.Count > 0)
                    {
                        SM.AccessList = lstmUserSubModules;
                    }
                }
            }
            //////////////////////////////////////End This code For Manage SubMenu By User////////////////////////////////////

            //add code for house committee count by robin - 23-june2017
            #region For received list count
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string Year = "5";
            string papers = "1";
            string[] strngBIDN = new string[2];
            strngBIDN[0] = CurrentSession.BranchId;
            var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = value123[0];
            str[4] = Year;
            str[5] = papers;
            var ReceivedList = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
            ViewBag.ReceivedListCount = ReceivedList.Count;
            #endregion

            #region For Draft List Count
            string[] strD = new string[6];
            strD[0] = DepartmentId;
            strD[1] = Officecode;
            strD[2] = AadharId;
            strD[3] = currtBId;
            strD[4] = CurrentSession.Designation;
            strD[5] = "1";
            var DraftListCount = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
            ViewBag.DraftListCount = DraftListCount.Count;
            #endregion

            #region For Send List Count
            string[] strS = new string[6];
            strS[0] = DepartmentId;
            strS[1] = Officecode;
            strS[2] = AadharId;
            strS[3] = currtBId;
            strS[4] = "5";
            strS[5] = "1";
            List<eFileAttachment> ListModelForSend = new List<eFileAttachment>();
            ListModelForSend = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendList", strS);
            ViewBag.SentPaperListCount = ListModelForSend.Count;
            #endregion

            return PartialView("_SubMenu", SM);
        }


        public PartialViewResult GetActionView(int SubMenuID, int AssemId, int SessId, int MainMenuID, int Count)
        {

            if (MainMenuID == 24)
            {

                if (SubMenuID == 89)
                {
                    return PartialView("_BudgetHead");
                }
                else if (SubMenuID == 90)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_AllocateFund.cshtml");
                }


            }
            else if (MainMenuID == 84)
            {

                if (SubMenuID == 1307)
                {
                    return PartialView("_ListOfBusiness");
                }
                else if (SubMenuID == 90)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_AllocateFund.cshtml");
                }


            }
            else if (MainMenuID == 63)
            {

                if (SubMenuID == 241)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_RoadPermit.cshtml");
                }
                else if (SubMenuID == 242)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_RoadPermitReport.cshtml");
                }


            }
            else if (MainMenuID == 66)
            {

                if (SubMenuID == 249)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_HPTR2FormIndex.cshtml");
                }
                if (SubMenuID == 250)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_TR2salary.cshtml");
                }
                else if (SubMenuID == 251)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_SaveHPTR2Form.cshtml");
                }
                else if (SubMenuID == 260)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_TR2ArrearReport.cshtml");
                }                
                else if (SubMenuID == 259)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_Form16Index.cshtml");
                }
                else if (SubMenuID == 258)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_Form16.cshtml");
                }

				else if (SubMenuID == 1286)
				{
					return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_TR2SummaryReport.cshtml");
				}

                else if (SubMenuID == 281)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_Q1Report.cshtml");
                }

                else if (SubMenuID == 275)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_SalaryVoucherNumbersIndex.cshtml");
                }
                //if (SubMenuID == 270)
                //{
                //    return PartialView("_SanctionBill", SubMenuID);
                //}

            }
            else if (MainMenuID == 57)
            {

                if (SubMenuID == 229)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_ReimbursementBillReport.cshtml");
                }
                else if (SubMenuID == 230)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_EstablishBillReport.cshtml");
                }



            }
            if (MainMenuID == 24)
            {

                if (SubMenuID == 89)
                {
                    return PartialView("_BudgetHead");
                }
                else if (SubMenuID == 90)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_AllocateFund.cshtml");
                }


            }
            else if (MainMenuID == 27)
            {
                if (SubMenuID == 1301)
                {

                    return PartialView("/Areas/Accounts/Views/salary/_YearlySalaryStatement.cshtml");

                }
                return PartialView("MemberAccounts/_salarySection", SubMenuID);

            }
            else if (MainMenuID == 25)
            {

                if (SubMenuID == 91)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_ReimbursementBillIndex.cshtml");

                }
                else if (SubMenuID == 92)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_SanctionedBillIndex.cshtml");
                }
                else if (SubMenuID == 93)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_AttachPDFIndex.cshtml");
                }
                else if (SubMenuID == 94)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_ApproveSactionBill.cshtml");
                }
                else if (SubMenuID == 95)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_SetEncashmentDateList.cshtml");
                }
                else if (SubMenuID == 96)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_ApproveEncashmentBill.cshtml");
                }
                else if (SubMenuID == 97)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_GenerateBillReport.cshtml");
                }
                else if (SubMenuID == 122)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_ExpenseReport.cshtml");
                }
                else if (SubMenuID == 125)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_EstablishBill.cshtml");
                }
                else if (SubMenuID == 126)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_SOApproveBillIndex.cshtml");
                }
                else if (SubMenuID == 127)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_AuthorityLetter.cshtml");
                }
                else if (SubMenuID == 128)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_OtherFirmIndex.cshtml");
                }
                else if (SubMenuID == 129)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_RtgsSoftIndex.cshtml");
                }
                else if (SubMenuID == 222)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_CancelEstablishIndex.cshtml");
                }
                else if (SubMenuID == 280)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_DebitReceiptExp.cshtml");
                }
                else if (SubMenuID == 282)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_BudgerMapWithDesig.cshtml");
                }
            }
            else if (MainMenuID == 32)
            {
                if (SubMenuID == 130)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_MedicineIndex.cshtml");
                }
                else if (SubMenuID == 131)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_SearchMedicine.cshtml");
                }
                else if (SubMenuID == 132)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_UpdateMedicine.cshtml");
                }



                if (SubMenuID == 133)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_HospitalIndex.cshtml");
                }
                else if (SubMenuID == 134)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_SearchHospital.cshtml");
                }
                else if (SubMenuID == 135)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_UpdateHospital.cshtml");
                }

                if (SubMenuID == 136)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_MedicalTestIndex.cshtml");
                }
                else if (SubMenuID == 137)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_SearchMedicalTest.cshtml");
                }
                else if (SubMenuID == 138)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_UpdateMedicalTest.cshtml");
                }
            }
            else if (MainMenuID == 55)
            {

                if (SubMenuID == 223)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/MemberAccounts/_UpdateMemberAccounts.cshtml");

                }


            }
            else if (MainMenuID == 62)
            {

                if (SubMenuID == 240)
                {
                    return PartialView("MemberAccounts/_SalaryStatusForSO", SubMenuID);

                }
            }
            else if (MainMenuID == 65)
            {

                if (SubMenuID == 246)
                {
                    return PartialView("_amenities", SubMenuID);
                }
                //if (SubMenuID == 270)
                //{
                //    return PartialView("_SanctionBill", SubMenuID);
                //}
            }
            else if (MainMenuID == 59)
            {

                if (SubMenuID == 2339)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/MemberAccounts/_UpdateMemberAccounts.cshtml");

                }
                if (SubMenuID == 233)
                {

                    return PartialView("MemberAccounts/_SalaryStatus", SubMenuID);

                }
            }

            else if (MainMenuID == 27)
            {

                return PartialView("MemberAccounts/_salarySection", SubMenuID);

            }
            else if (MainMenuID == 28)
            {

                return PartialView("MemberAccounts/_loanSection", SubMenuID);

            }
            else if (MainMenuID == 33)
            {
                return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/Staff/_Staff.cshtml", SubMenuID);

            }
            else if (MainMenuID == 64)
            {

                if (SubMenuID == 243)  //efile
                {
                    return PartialView("/Areas/eFile/Views/eFile/_MethodeFile.cshtml");

                }
                if (SubMenuID == 244) //receive
                {

                    return PartialView("/Areas/eFile/Views/eFile/_MethodReceive.cshtml");

                }
                if (SubMenuID == 245) //send
                {

                    return PartialView("/Areas/eFile/Views/eFile/_MethodSend.cshtml");

                }
				if (SubMenuID == 277) //Pendency Report
				{

					return PartialView("/Areas/eFile/Views/eFile/_MethodPendencyReport.cshtml");

				}


            }



			else if (MainMenuID == 72) //Committee (eFile)
			{

				if (SubMenuID == 267)  //Committee Files
				{
					return PartialView("/Areas/eFile/Views/eFile/_MethodeFileCommitteeAttach.cshtml");

				}
				if (SubMenuID == 269)  //For Lay Paper 
				{
					return PartialView("/Areas/eFile/Views/eFile/_MethodsCommitteePaperLaid.cshtml");

				}
				if (SubMenuID == 266)  //For Committee Members
				{
					return PartialView("/Areas/eFile/Views/eFile/_MethodCommitteeMembers.cshtml");

				}
				if (SubMenuID == 268)  //For Committee Proceeding
				{
					return PartialView("/Areas/eFile/Views/eFile/_MethodCommitteeProceeding.cshtml");

				}
				if (SubMenuID == 276)  //For Replace Lay Paper 
				{
					return PartialView("/Areas/eFile/Views/eFile/_MethodCommitteePaperLaidReplace.cshtml");

				}
			}
		 
			else if (MainMenuID == 22) 
            {
				if (SubMenuID == 81)   //For Bulk SMS
                {
                    return PartialView("/Areas/eFile/Views/eFile/_MethodBulkSMS.cshtml");

                }

                if (SubMenuID == 257)  
                {
                    return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_SMSRpt.cshtml");

                }
                if (SubMenuID == 263)   
                {
                    SBL.DomainModel.ComplexModel.OnlineMemberQmodel model = new SBL.DomainModel.ComplexModel.OnlineMemberQmodel();
                    ViewBag.GenericOnly = "GENRICONLY";
                    return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_CustomizeSMSEMAIL.cshtml", model);

                }
            }
            else if (MainMenuID == 74)
            {

                if (SubMenuID == 272)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_MemberIncomeTaxList.cshtml");
                }
                else if (SubMenuID == 273)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_CalculateIncomeTax.cshtml");
                }
                else if (SubMenuID == 274)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_MemberIncomTaxIndex.cshtml");
                }

            }
            else if (MainMenuID == 75)
            {

                if (SubMenuID == 278)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_BudgetType.cshtml");
                }
                else if (SubMenuID == 279)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/_BudgetBillType.cshtml");
                }


            }




            #region notices for vidhan sabha
            //else if (MainMenuID == 67)
            //{

            //    if (SubMenuID == 254)
            //    {
            //        return PartialView("/Areas/Admin/Views/Notice/_NoticeIndexPage.cshtml");

            //    }
            //    if (SubMenuID == 255)
            //    {
            //        return PartialView("/Areas/Admin/Views/Notice/_NoticeIndexCatPage.cshtml");

            //    }
            //}
            else if (MainMenuID == 71)
            {

                if (SubMenuID == 261)
                {
                    return PartialView("/Areas/Admin/Views/Notice/_NoticeIndexPage.cshtml");

                }
                if (SubMenuID == 262)
                {
                    return PartialView("/Areas/Admin/Views/Notice/_NoticeIndexCatPage.cshtml");

                }
            }
            else if (MainMenuID == 77)
            {
                if (SubMenuID == 1283)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/Job/_Job.cshtml");
                }
                if (SubMenuID == 1284)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/Job/_Post.cshtml");
                }
                if (SubMenuID == 1285)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/Job/_ApporveCandidates.cshtml");
                }
                if (SubMenuID == 1299)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/Job/_AdmitCard.cshtml");
                }
            }
            #endregion


            #region Offices/Institutes
            else if (MainMenuID == 60)
            {

                if (SubMenuID == 237)
                {
                    return PartialView("/Areas/PublishDocument/Views/PublishDocument/_DocumentList.cshtml");

                }
                if (SubMenuID == 235)
                {
                    return PartialView("/Areas/PISModule/Views/SanctionedPost/_TransferMetodPage.cshtml");

                }
                if (SubMenuID == 234)
                {
                    return PartialView("/Areas/PISModule/Views/PISDesignation/_DesignationMethodPage.cshtml");

                }
                if (SubMenuID == 236)
                {
                    return PartialView("/Areas/PISModule/Views/PISEmployee/_EmpMethodPage.cshtml");

                }
            }



            #endregion


            #region DynamicMenu For Notices Area
            #region MainMenu/SunMenu
            //LegislationFixation
            else if (MainMenuID == 39 || MainMenuID == 40 || MainMenuID == 41 || MainMenuID == 42 || MainMenuID == 43 || MainMenuID == 44 || MainMenuID == 45 || MainMenuID == 46 || MainMenuID == 47 || MainMenuID == 48 || MainMenuID == 49)
            {
                if (SubMenuID > 0)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/Notices/LegislationFixation/_MethodsPage.cshtml", SubMenuID);
                }
            }
            //Diaries
            else if (MainMenuID == 50)
            {
                if (SubMenuID > 0)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/Notices/Diaries/_MethodsPage.cshtml", SubMenuID);
                }
            }
            //Typist
            else if (MainMenuID == 53)
            {
                if (SubMenuID > 0)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/Notices/Typist/_MethodsPage.cshtml", SubMenuID);
                }
            }
            //Proof-Reader
            else if (MainMenuID == 54)
            {
                if (SubMenuID > 0)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/Notices/ProofReader/_MethodsPage.cshtml", SubMenuID);
                }
            }

            //Translator
            else if (MainMenuID == 56)
            {
                if (SubMenuID > 0)
                {
                    return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/Notices/Translator/_MethodsPage.cshtml", SubMenuID);
                }
            }
            #endregion
            #region MainMenu/SunMenuFor(Access Control)
            else if (MainMenuID == 12)
            {
                if (SubMenuID == 62)
                {
                    return PartialView("/Areas/PaperLaidDepartment/Views/PaperLaidDepartment/_GetPendingListUserAccess.cshtml");
                }
                if (SubMenuID == 63)
                {
                    return PartialView("/Areas/PaperLaidDepartment/Views/PaperLaidDepartment/_GetAcceptedListUserAccess.cshtml");
                }
                if (SubMenuID == 64)
                {
                    return PartialView("/Areas/PaperLaidDepartment/Views/PaperLaidDepartment/_GetRejectedListUserAccess.cshtml");
                }
            }
            #endregion
            #endregion
            else if (MainMenuID == 78)
            {
             
                if (SubMenuID == 1288) //Received Paper
                {

                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodReceive.cshtml");

                }
                if (SubMenuID == 1289) //Send Paper
                {

                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodSend.cshtml");
                   

                }

                // By Lakshay
                if (SubMenuID == 1291)  //File
                {
                    //return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodeFile.cshtml");
                    //New eFile View
                    return PartialView("/Areas/eFile/Views/eFile/_efileIndex.cshtml");
                }
                if (SubMenuID == 1309)  // Letters/Correspondence
                {
                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_LettersView.cshtml");
                }
                if (SubMenuID == 1312)  // Letters/New Item pendency
                {
                    return PartialView("/Areas/HouseCommittee/Views/CommitteReplyPendency/nItemPendencyIndexView.cshtml");
                    //return PartialView("/Areas/HouseCommittee/Views/CommitteeReplyPendency/nItemPendencyIndexView.cshtml");
                }

               if (SubMenuID == 1292)  //Papers Mettings
                {
                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodPapersMettings.cshtml");

                }
               if (SubMenuID == 1293)  //List of Proceeding in HC
               {
                   return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodProceddings.cshtml");

               }
                if (SubMenuID == 1295)  //Committe Reply Pendency
                {
                    return PartialView("/Areas/HouseCommittee/Views/CommitteReplyPendency/Index.cshtml");
                }
               
                if (SubMenuID == 1294)  //Committe Report to be Laid
                {
                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodsCommitteePaperLaid.cshtml");
                }
                if (SubMenuID == 1290)  //Draft Paper
                {
                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodDraft.cshtml");

                }
                if (SubMenuID == 1296)  //For Committee Members
                {
                    return PartialView("/Areas/eFile/Views/eFile/_MethodCommitteeMembers.cshtml");

                }
                if (SubMenuID == 1303)  // Meeting Agenda
                {
                    return PartialView("/Areas/eFile/Views/eFile/MeetingView.cshtml");
                }

                if (SubMenuID == 1302)  // Meeting Agenda
                {
                    return PartialView("/Areas/eFile/Views/eFile/_eCommitteeProceeding.cshtml");
                }

            }
            else if (MainMenuID == 81)
            {
                if (SubMenuID == 1298)  //Committe Reply Pendency Verify Papers
                {
                    return PartialView("/Areas/HouseCommittee/Views/CommitteReplyPendency/_VerifyPapersIndex.cshtml");
                }
            }

            else if (MainMenuID == 82)
            {
                if (SubMenuID == 1302)  // Upload Procedding
                {
                    return PartialView("/Areas/eFile/Views/eFile/_eCommitteeProceeding.cshtml");
                }
                //if (SubMenuID == 1303)  // Meeting Agenda
                //{
                //    return PartialView("/Areas/eFile/Views/eFile/MeetingView.cshtml");
                //}
            }
           

            return null;
        }





        public ActionResult GetDivData()
        {
            return PartialView("_DivData");
        }
    }
}

