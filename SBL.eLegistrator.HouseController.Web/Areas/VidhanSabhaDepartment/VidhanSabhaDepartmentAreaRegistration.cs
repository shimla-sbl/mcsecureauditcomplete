﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.VidhanSabhaDepartment
{
    public class VidhanSabhaDepartmentAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "VidhanSabhaDepartment";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "VidhanSabhaDepartment_default",
                "VidhanSabhaDepartment/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
