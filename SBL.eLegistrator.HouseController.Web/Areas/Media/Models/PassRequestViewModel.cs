﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Media.Models
{
    public class PassRequestViewModel
    {

        public int passrequestid { get; set; }

        [Required(ErrorMessage = "DepartmentName is Required")]
        public string DepartmentName { get; set; }
        [Required(ErrorMessage = "Empcode is Required")]
        [StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string EmpCode { get; set; }
        [Required(ErrorMessage = "PassType is Required")]
        public string PassType { get; set; }

        public List<SelectListItem> DeptList { get; set; }

        public SelectList PasstypeList { get; set; }
        public IndexModel Indexmodel { get; set; }
        public bool IsActive { get; set; }
    }
}