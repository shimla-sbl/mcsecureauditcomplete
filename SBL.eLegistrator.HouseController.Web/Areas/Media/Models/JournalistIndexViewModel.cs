﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Media.Models
{
    public class JournalistIndexViewModel
    {
        public IEnumerable<JournalistViewModel> JournalistList { get; set; }
     
        public bool IsAcredited { get; set; }
        public IndexModel Indexmodel { get; set; }
    }
}