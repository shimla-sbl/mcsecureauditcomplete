﻿using SBL.DomainModel.Models.Media;
using SBL.DomainModel.Models.Passes;
using SBL.eLegistrator.HouseController.Web.Areas.Passes.Models;
using System;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.ComplexModel.MediaComplexModel;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Media.Models
{
    public static class ModelMapping
    {
        #region Organisation

        public static IndexModel GetYears()
        {
            IndexModel model = new IndexModel();
            MediaModel Mpass = new MediaModel();
            List<int> yearList = new List<int>();
            int currentYear = DateTime.Now.Year;
            for (int i = currentYear - 4; i <= currentYear; i++)
            {
                yearList.Add(i);
            }
            model.Years = yearList.OrderByDescending(m => m).ToList();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            var model1 = (MediaModel)Helper.ExecuteService("Media", "GetDashBoardValue", Mpass);

            model.AssemblyId = model1.AssemblyId;
            model.SessionId = model1.SessionId;
            model.AssemblyName = model1.AssemblyName;
            model.SessionName = model1.SessionName;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;

            return model;
        }

        /// <summary>
        /// Method to Convert Organization Domain Model to View Model along with Operation Mode("Add","Edit")
        /// </summary>
        /// <param name="organization"></param>
        /// <param name="Mode"></param>
        /// <returns></returns>
        public static OrganizationViewModel ToViewModel(this Organization organization, string Mode)
        {
            var ViewModel = new OrganizationViewModel()
            {
                OrganizationID = organization.OrganizationID,
                Name = organization.Name,
                Address = organization.Address,
                IsActive = organization.IsActive,
                Type = organization.Type,
                Mode = Mode,
                TypeList = GetOrganizationTypes()

            };
            ViewModel.Indexmodel = GetYears();
            ViewModel.AssemblyId = ViewModel.Indexmodel.AssemblyId;
            ViewModel.SessionId = ViewModel.Indexmodel.SessionId;
            ViewModel.AssemblyName = ViewModel.Indexmodel.AssemblyName;
            ViewModel.SessionName = ViewModel.Indexmodel.SessionName;
            ViewModel.mAssemblyList = ViewModel.Indexmodel.mAssemblyList;
            ViewModel.sessionList = ViewModel.Indexmodel.sessionList;
            return ViewModel;
        }
        public static IEnumerable<OrganizationViewModel> ToViewModel(this IEnumerable<Organization> organizationList)
        {
            var ViewModel = organizationList.Select(a => new OrganizationViewModel()
            {
                OrganizationID = a.OrganizationID,
                Name = a.Name,
                Address = a.Address,
                IsActive = a.IsActive,
                Type = a.Type,


            });
            return ViewModel;
        }



        /// <summary>
        /// Method to Convert Organisation ViewModel to DomainModel
        /// </summary>
        /// <param name="Organization"></param>
        /// <returns></returns>
        public static Organization ToDomainModel(this OrganizationViewModel Organization)
        {
            var DomainModel = new Organization()
            {
                OrganizationID = Organization.OrganizationID,
                Name = Organization.Name,
                Address = Organization.Address,
                IsActive = Organization.IsActive,
                Type = Organization.Type,
            };
            return DomainModel;
        }
        public static IEnumerable<Organization> ToDomainModel(this IEnumerable<OrganizationViewModel> OrganizationList)
        {

            var DomainModel = OrganizationList.Select(a => new Organization()
          {
              OrganizationID = a.OrganizationID,
              Name = a.Name,
              Address = a.Address,
              IsActive = a.IsActive,
              Type = a.Type,
          });
            return DomainModel;
        }

        #endregion


        #region Journalist


        public static SelectList GetGender()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Male", Value = "Male" });
            result.Add(new SelectListItem() { Text = "Female", Value = "Female" });
            result.Add(new SelectListItem() { Text = "Third Gender", Value = "Third Gender" });


            return new SelectList(result, "Value", "Text");
        }

        /// <summary>
        /// Method to Convert Organization Domain Model to View Model along with Operation Mode("Add","Edit")
        /// </summary>
        /// <param name="organization"></param>
        /// <param name="Mode"></param>
        /// <returns></returns>
        public static JournalistViewModel ToViewModel(this Journalist Journalist, string Mode)
        {
            var ViewModel = new JournalistViewModel()
            {
                JournalistID = Journalist.JournalistID,
                Name = Journalist.Name,
                Email = Journalist.Email,

                FullImage = Journalist.ThumbName,
                ThumbImage = Journalist.ThumbName,
                FileLocation = Journalist.Photo,

                IsActive = Journalist.IsActive,
                Phone = Journalist.Phone,
                UID = Journalist.UID,
                AcreditationNumber = Journalist.AcreditationNumber,
                OrganizationID = Journalist.OrganizationID,
                Mode = Mode,
                // Photo = Journalist.Photo + Journalist.ThumbName,
                OrganisationName = Journalist.Organization.Name,
                Gender = Journalist.Gender,
                Age = Journalist.Age,
                IsDeleted = Journalist.IsDeleted,
                Designation = Journalist.Designation,
                FathersName = Journalist.FatherName,
                Address = Journalist.Address,
                IsAcredited = Journalist.IsAcredited
            };
            return ViewModel;
        }
        public static IEnumerable<JournalistViewModel> ToViewModel(this IEnumerable<Journalist> JournlistList)
        {
            //var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            var ViewModel = JournlistList.Select(a => new JournalistViewModel()
            {
                JournalistID = a.JournalistID,
                Name = a.Name,
                Email = a.Email,
                IsActive = a.IsActive,
                Phone = a.Phone,

                FullImage = a.FileName,
                ThumbImage = a.FileName,
                FileLocation = a.ImageAcessurl,
                ImageLocation = fileAcessingSettings.SettingValue + a.Photo + a.ThumbName,

                UID = a.UID,
                AcreditationNumber = a.AcreditationNumber,
                OrganizationID = a.OrganizationID,
                // Photo = a.Photo,
                OrganisationName = a.Organization.Name,
                Gender = a.Gender,
                Age = a.Age,
                IsDeleted = a.IsDeleted,
                Designation = a.Designation,
                Address = a.Address,
                FathersName = a.FatherName,
                IsAcredited = a.IsAcredited,
                IsInCurrentYear = a.IsInCurrentYear,
                IsRequested = a.IsRequested,
                RequestId = a.RequestId,
                IsInCurrentSession = a.IsInCurrentSession
            });
            return ViewModel;
        }

        /// <summary>
        /// Method to Convert Organisation ViewModel to DomainModel
        /// </summary>
        /// <param name="Organization"></param>
        /// <returns></returns>
        public static Journalist ToDomainModel(this JournalistViewModel Journalist)
        {
            var DomainModel = new Journalist()
            {
                JournalistID = Journalist.JournalistID,
                Name = Journalist.Name,
                Email = Journalist.Email,
                IsActive = Journalist.IsActive,
                Phone = Journalist.Phone,

                FileName = Journalist.ThumbName,
                Photo = Journalist.FileLocation,
                ThumbName = Journalist.ThumbName,

                UID = Journalist.UID,
                AcreditationNumber = Journalist.AcreditationNumber,
                OrganizationID = Journalist.OrganizationID,
                // Photo = Journalist.Photo,
                Gender = Journalist.Gender,
                Age = Journalist.Age,
                IsDeleted = Journalist.IsDeleted,
                Address = Journalist.Address,
                Designation = Journalist.Designation,
                FatherName = Journalist.FathersName,
                IsAcredited = Journalist.IsAcredited
            };
            return DomainModel;
        }
        public static IEnumerable<Journalist> ToDomainModel(this IEnumerable<JournalistViewModel> JournalistList)
        {

            var DomainModel = JournalistList.Select(a => new Journalist()
            {
                JournalistID = a.JournalistID,
                Name = a.Name,
                Email = a.Email,
                IsActive = a.IsActive,
                Phone = a.Phone,
                UID = a.UID,
                Gender = a.Gender,
                IsDeleted = a.IsDeleted,
                Age = a.Age,
                AcreditationNumber = a.AcreditationNumber,
                OrganizationID = a.OrganizationID,
                Photo = a.Photo,
                IsAcredited = a.IsAcredited
            });
            return DomainModel;
        }




        #endregion


        #region "MediaYear"

        public static List<MediaYearViewModel> ToViewModel(this IEnumerable<tMediaYears> MediaYear)
        {
            var ViewModel = MediaYear.Select(a => new MediaYearViewModel()
            {
                JournalistID = a.JournalistID,
                ID = a.ID,
                IsActive = a.IsActive,
                Year = a.Year
            }).ToList();
            return ViewModel;
        }

        #endregion


        #region Private

        public static SelectList GetOrganizationTypes()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Print Media", Value = "Print Media" });
            result.Add(new SelectListItem() { Text = "Broadcast Media", Value = "Broadcast Media" });
            result.Add(new SelectListItem() { Text = "Web Media", Value = "Web Media" });


            return new SelectList(result, "Value", "Text");
        }
        public static SelectList GetNameTitles()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Shri.", Value = "Shri." });
            result.Add(new SelectListItem() { Text = "Smt.", Value = "Smt." });
            result.Add(new SelectListItem() { Text = "Kumari.", Value = "Kumari." });

            return new SelectList(result, "Value", "Text");
        }
        #endregion
        //#region PassRequest
        //public static PassRequestViewModel ToViewModel(this PassRequest passRequest)
        //{
        //    var ViewModel = new PassRequestViewModel()
        //    {
        //        passrequestid = passRequest.passrequestid,
        //        DepartmentName = passRequest.DepartmentName,
        //        EmpCode = passRequest.EmpCode,
        //        IsActive = passRequest.IsActive,
        //        PassType = passRequest.PassType              
        //    };
        //    return ViewModel;
        //}
        //public static PassRequest ToDomain(this PassRequestViewModel passRequestview)
        //{
        //    var domain = new PassRequest()
        //    {
        //        passrequestid = passRequestview.passrequestid,
        //        DepartmentName = passRequestview.DepartmentName,
        //        EmpCode = passRequestview.EmpCode,
        //        IsActive = passRequestview.IsActive,
        //        PassType = passRequestview.PassType
        //    };
        //    return domain;
        //}

        //public static IEnumerable<PassRequestViewModel> ToViewModel(this IEnumerable<PassRequest> PassRequestList)
        //{
        //    var ViewModel = PassRequestList.Select(a => new PassRequestViewModel()
        //    {
        //        passrequestid = a.passrequestid,
        //        DepartmentName = a.DepartmentName,
        //        EmpCode = a.EmpCode,
        //        IsActive = a.IsActive,
        //        PassType = a.PassType


        //    });
        //    return ViewModel;
        //}

        //#endregion

    }
}