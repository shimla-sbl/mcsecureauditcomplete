﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Media;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Media.Models
{
    public class OrganizationViewModel
    {

        public int OrganizationID { get; set; }
        [Required(ErrorMessage = "Name is Required")]
        [StringLength(30, ErrorMessage = "Maximum 30 Characters Allowed")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Address is Required")]
        [StringLength(150, ErrorMessage = "Maximum 150 characters Allowed")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Type is Required")]
        public string Type { get; set; }

        public SelectList TypeList { get; set; }

        public string Mode { get; set; }

        public bool IsActive { get; set; }
        public string Heading { get; set; }
        public IndexModel Indexmodel { get; set; }

        //For List Of Assembly and Session-- sanjay
        [NotMapped]
        public string AssemblyName { get; set; }
        [NotMapped]
        public string SessionName { get; set; }
        [NotMapped]
        public int AssemblyId { get; set; }
        [NotMapped]
        public int SessionId { get; set; }
        [NotMapped]
        public IList<mAssembly> mAssemblyList { get; set; }
        [NotMapped]
        public IList<mSession> sessionList { get; set; }
        [NotMapped]
        public IList<Organization> OrganizationList { get; set; }
    }
}