﻿using SBL.DomainModel.Models.Passes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Media;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Media.Models
{
    public class JournalistViewModel : PhotoCaptureGenericModel
    {

        public int JournalistID { get; set; }
        [Required(ErrorMessage = "Name is Required")]
        [StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        [StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string FathersName { get; set; }

        //public string Photo { get; set; }


        public string UID { get; set; }

        [Required(ErrorMessage = "Phone Number is Required")]
        [RegularExpression(@"^[0-9\.\+\-\/]+$", ErrorMessage = "Invalid phone number")]
        [StringLength(10, ErrorMessage = "Maximum 10 characters allowed")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Email Id is Required")]
        [EmailAddress(ErrorMessage = "Invalid Email")]
      //  [StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Designation is Required")]
        [StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string Designation { get; set; }

        public string Gender { get; set; }

        [Required(ErrorMessage = "Age is Required")]
        public int? Age { get; set; }

        [Required(ErrorMessage = "Address is Required")]
        //[StringLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string Address { get; set; }

        public bool IsActive { get; set; }

        [StringLength(20, ErrorMessage = "Maximum 20 characters allowed")]
        public string AcreditationNumber { get; set; }

        public bool IsRequested { get; set; }

        public string Mode { get; set; }

        public SelectList Organisation { get; set; }
        public int OrganizationID { get; set; }
        public bool? IsDeleted { get; set; }
        [Required(ErrorMessage = "OrganisationName is Required")]
        public string OrganisationName { get; set; }
        public SelectList GenderList { get; set; }
        public bool IsCurrentYear { get; set; }
        public int RequestId { get; set; }
        public IndexModel Indexmodel { get; set; }

        public bool IsAcredited { get; set; }

        //Added By Ram
        public int? AssemblyCode { get; set; }
        public int? SessionCode { get; set; }

        public bool IsInCurrentYear { get; set; }

        public bool? IsInCurrentSession { get; set; }


        //Added by Umang

        //public string FullImage { get; set; }
        //public string ThumbImage { get; set; }
        //public string FileLocation { get; set; }
        //public string ImageLocation { get; set; }
        //public string ThumbName { get; set; }

        //Added by sanjay
        [NotMapped]
        public IList<Journalist> JournalistList { get; set; }
        public IEnumerable<JournalistViewModel> JournalistList1 { get; set; }
        [NotMapped]
        public string AssemblyName { get; set; }
        [NotMapped]
        public string SessionName { get; set; }
        [NotMapped]
        public IList<mAssembly> mAssemblyList { get; set; }
        [NotMapped]
        public IList<mSession> sessionList { get; set; }
        [NotMapped]
        public bool IsJournalistIdForApproval { get; set; }
        [NotMapped]
        public bool IsCurrentSession { get; set; }
    }

    public class IndexModel
    {
        public List<int> Years { get; set; }


        //For List Of Assembly and Session-- sanjay
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AssemblyName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int AssemblyId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SessionId { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mAssembly> mAssemblyList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mSession> sessionList { get; set; }
    }
}