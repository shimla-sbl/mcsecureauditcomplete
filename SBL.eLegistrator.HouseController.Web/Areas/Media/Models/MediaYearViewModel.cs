﻿using System;
using System.Collections.Generic;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Assembly;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Media.Models
{
    public class MediaYearViewModel
    {
        public int ID { get; set; }

        public int JournalistID { get; set; }

        public int Year { get; set; }

        public bool IsActive { get; set; }

        public IndexModel Years { get; set; }

        public IEnumerable<JournalistViewModel> ListJounalist { get; set; }

        public List<MediaYearViewModel> ListMediaYearList { get; set; }

        //For List Of Assembly and Session-- sanjay

        public int IsCurrentSessionCode { get; set; }

        public string AssemblyName { get; set; }

        public string SessionName { get; set; }


        public int AssemblyId { get; set; }


        public int SessionId { get; set; }


        public IList<mAssembly> mAssemblyList { get; set; }


        public IList<mSession> sessionList { get; set; }

        public IndexModel Indexmodel { get; set; }

    }
}