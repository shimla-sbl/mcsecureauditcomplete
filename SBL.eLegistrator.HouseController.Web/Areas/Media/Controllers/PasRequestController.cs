﻿using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Media.Controllers
{
    [Audit]
    public class PasRequestController : Controller
    {
        //
        // GET: /Media/PasRequest/
        [NoCache]
        public ActionResult Index()
        {
            return View();
        }

    }
}
