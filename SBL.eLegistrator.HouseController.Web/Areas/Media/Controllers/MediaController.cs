﻿using Microsoft.Security.Application;
using SBL.DomainModel.Models.Media;
using SBL.DomainModel.Models.Passes;
using SBL.DomainModel.Models.Role;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Media.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.ComplexModel.MediaComplexModel;
using SMSServices;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Data;
using SBL.DomainModel.Models.Adhaar;
#pragma warning disable CS0105 // The using directive for 'SMSServices' appeared previously in this namespace
using SMSServices;
#pragma warning restore CS0105 // The using directive for 'SMSServices' appeared previously in this namespace
using SMS.API;
namespace SBL.eLegistrator.HouseController.Web.Areas.Media.Controllers
{
    [SiteSettingsFilter]
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class MediaController : Controller
    {

        public ActionResult Index()
        {

            //   paperLaidModel.yearList = yearList;
            return View("IPRDashboard", GetYears());
        }

        public IndexModel GetYears()
        {
            IndexModel model = new IndexModel();
            MediaModel Mpass = new MediaModel();
            // List<int> yearList = new List<int>();


            //int currentYear = DateTime.Now.Year;
            //for (int i = currentYear - 4; i <= currentYear; i++)
            //{
            //    yearList.Add(i);
            //}
            //for (int i = currentYear; i < currentYear - 5; i++)
            //{
            //    yearList.Add(i);
            //}


            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                Mpass.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Mpass.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            var model1 = (MediaModel)Helper.ExecuteService("Media", "GetDashBoardValue", Mpass);

            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model1.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model1.SessionId.ToString();
            }
            model.AssemblyId = model1.AssemblyId;
            model.SessionId = model1.SessionId;
            model.AssemblyName = model1.AssemblyName;
            model.SessionName = model1.SessionName;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;

            //  model.Years = yearList.OrderByDescending(m => m).ToList();
            return model;
        }

        //
        // GET: /Media/Media/
        #region Organization

        /// <summary>
        /// Show the Organization Grid
        /// </summary>
        /// <returns></returns>
        public ActionResult OrganizationList()
        {
            OrganizationViewModel ViewModel = new OrganizationViewModel();
            ViewModel.Indexmodel = GetYears();

            ViewModel.AssemblyId = ViewModel.Indexmodel.AssemblyId;
            ViewModel.SessionId = ViewModel.Indexmodel.SessionId;
            ViewModel.OrganizationList = (List<Organization>)Helper.ExecuteService("Media", "GetAllOrganizations", null);
            ViewModel.AssemblyName = ViewModel.Indexmodel.AssemblyName;
            ViewModel.SessionName = ViewModel.Indexmodel.SessionName;

            ViewModel.sessionList = ViewModel.Indexmodel.sessionList;
            ViewModel.mAssemblyList = ViewModel.Indexmodel.mAssemblyList;
            //var Organization = (List<Organization>)Helper.ExecuteService("Media", "GetAllOrganizations", null);
            //List<OrganizationViewModel> ViewModel = new List<OrganizationViewModel>();
            //if (Organization.Count != 0)
            //{
            //    ViewModel = Organization.Select(a => new OrganizationViewModel()
            //    {
            //        OrganizationID = a.OrganizationID,
            //        Name = a.Name,
            //        Address = a.Address,
            //        IsActive = a.IsActive,
            //        Type = a.Type,
            //        Indexmodel = GetYears(),


            //    }).ToList();
            //}
            //else
            //{
            //    OrganizationViewModel mod = new OrganizationViewModel()
            //    {
            //        OrganizationID = 0,
            //        Indexmodel = GetYears()
            //    };
            //    ViewModel.Add(mod);
            //}
            return View("Index", ViewModel);
        }

        /// <summary>
        /// Render view for creation of Organization
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var model = new OrganizationViewModel()
            {
                Mode = "Add",
                TypeList = ModelMapping.GetOrganizationTypes()
                //Indexmodel = GetYears()
            };
            model.Indexmodel = GetYears();
            model.AssemblyId = model.Indexmodel.AssemblyId;
            model.SessionId = model.Indexmodel.SessionId;
            model.AssemblyName = model.Indexmodel.AssemblyName;
            model.SessionName = model.Indexmodel.SessionName;
            model.sessionList = model.Indexmodel.sessionList;
            model.mAssemblyList = model.Indexmodel.mAssemblyList;
            return View(model);
        }

        /// <summary>
        /// render Edit Organization View
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// 
        ///[HttpPost]
        public ActionResult Edit(int Id)
        {
            OrganizationViewModel model = new OrganizationViewModel();
            var Organization = (Organization)Helper.ExecuteService("Media", "GetOrganizationById", new Organization { OrganizationID = Id });
            model = Organization.ToViewModel("Edit");
            return View("Create", model);
        }

        /// <summary>
        /// Post method for saving the organization
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveOrganization(OrganizationViewModel model)
        {
            if (model.Mode == "Add")
            {
                model.IsActive = true;
                Helper.ExecuteService("Media", "AddOrganization", model.ToDomainModel());
            }
            else
                Helper.ExecuteService("Media", "UpdateOrganization", model.ToDomainModel());
            return RedirectToAction("OrganizationList");
        }

        /// <summary>
        /// Delete Organization Method
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult DeleteOrganization(int Id)
        {
            var Organization = Helper.ExecuteService("Media", "DeleteOrganization", new Organization { OrganizationID = Id });

            return RedirectToAction("OrganizationList");
        }
        public ActionResult ChangeOrganization(int Id)
        {
            var Organization = Helper.ExecuteService("Media", "ChangeOrganizationStatus", new Organization { OrganizationID = Id });

            return RedirectToAction("OrganizationList");
        }

        #endregion

        #region Journalist

        /// <summary>
        /// Not in use
        /// </summary>
        /// <returns></returns>
        public ActionResult JournalistIndex()
        {
            var Journalist = (List<Journalist>)Helper.ExecuteService("Media", "GetAllJournalists", null);

            return View(Journalist.ToViewModel());
        }

        public object CheckAadhar(string AadharId)
        {
            AadharId = Sanitizer.GetSafeHtmlFragment(AadharId);
            var Journalist = (Journalist)Helper.ExecuteService("Media", "GetJournalistsByAadharId", new Journalist { UID = AadharId });
            if (Journalist == null)
            {
                return false;
            }
            else
            {
                return true;
            }
#pragma warning disable CS0162 // Unreachable code detected
            return false;
#pragma warning restore CS0162 // Unreachable code detected
        }
        public JsonResult GetAdhaarDetails(string AdhaarID, string fileLoaction)
        {
            AdhaarDetails details = new AdhaarDetails();
            AdhaarServices.ServiceSoapClient obj = new AdhaarServices.ServiceSoapClient();
            EncryptionDecryption.EncryptionDecryption objencr = new EncryptionDecryption.EncryptionDecryption();
            string inputValue = objencr.Encryption(AdhaarID.Trim());
            try
            {
                // var GetAdharIdExist = (Int32)Helper.ExecuteService("AdministrationBranch", "GetAdharIdExist", new mPasses { AssemblyCode = 12, SessionCode = 8 });

                DataTable dt = obj.getHPKYCInDataTable(inputValue);
                if (dt != null && dt.Rows.Count > 0)
                {
                    details.AdhaarID = AdhaarID;
                    details.Name = objencr.Decryption(Convert.ToString(dt.Rows[0]["Name"]));
                    details.FatherName = objencr.Decryption(Convert.ToString(dt.Rows[0]["FatherName"]));
                    details.Gender = objencr.Decryption(Convert.ToString(dt.Rows[0]["Gender"]));
                    details.Address = objencr.Decryption(Convert.ToString(dt.Rows[0]["Address"]).ToString());
                    details.DOB = objencr.Decryption(Convert.ToString(dt.Rows[0]["DOB"]));
                    details.MobileNo = objencr.Decryption(Convert.ToString(dt.Rows[0]["MobileNumber"]));
                    details.Email = objencr.Decryption(Convert.ToString(dt.Rows[0]["EmailID"]));
                    details.District = objencr.Decryption(Convert.ToString(dt.Rows[0]["DistrictName"]).ToString());
                    details.PinCode = objencr.Decryption(Convert.ToString(dt.Rows[0]["PinCOde"]).ToString());


                    //Calculate the age.
                    DateTime dateOfBirth;

                    if (!string.IsNullOrEmpty(details.DOB) && DateTime.TryParse(details.DOB, out dateOfBirth))
                    {
                        dateOfBirth = Convert.ToDateTime(details.DOB);
                        DateTime today = DateTime.Today;
                        int age = today.Year - dateOfBirth.Year;
                        if (dateOfBirth > today.AddYears(-age))
                            age--;

                        details.DOB = Convert.ToString(age);
                    }
                    else
                    {
                        details.DOB = "";
                    }
                    Byte[] bytes = (Byte[])Convert.FromBase64String(objencr.Decryption(dt.Rows[0]["photo"].ToString()));
                    var mediaSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMediaFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSettingLocation", null);
                    var ServerLocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting1", null);
                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    string Url = ServerLocation.SettingValue + mediaSettings.SettingValue;
                    DirectoryInfo Dir = new DirectoryInfo(Url);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    if (bytes.Length != 0)
                    {
                        if (fileLoaction != null && fileLoaction != "" && (fileLoaction.IndexOf("/") != -1))
                        {
                            int indexof = fileLoaction.LastIndexOf("/");
                            string url = fileLoaction.Substring(0, indexof + 1);
                            string FileName = fileLoaction.Substring(indexof + 1);

                            var path = Url + FileName;
                            System.IO.File.WriteAllBytes(path, bytes);

                            details.Photo = fileLoaction;
                        }
                        else
                        {
                            Guid PicName;
                            PicName = Guid.NewGuid();
                            SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();
                            session.AssemblyID = Convert.ToInt16(siteSettingMod.AssemblyCode);
                            session.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);

                            var path = Url + PicName + ".jpg";
                            System.IO.File.WriteAllBytes(path, bytes);
                            string path1 = System.IO.Path.Combine(FileSettings.SettingValue + mediaSettings.SettingValue + PicName + ".jpg");
                            details.Photo = path1 + "," + PicName + ".jpg"; //by robin
                           // details.Photo = path1 ;
                        }
                        return Json(details, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        details.Photo = null;
                        return Json(details, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }


            }
            catch
            {
                return Json(details, JsonRequestBehavior.AllowGet);

            }
        }

        /// <summary>
        /// It will render the Create Journalist View
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateJournlist(bool IsAcredited = true)
        {
            int year = DateTime.Now.Year;
            JournalistViewModel model1 = new JournalistViewModel();
            model1.Indexmodel = GetYears();
            var organization = (List<Organization>)Helper.ExecuteService("Media", "GetAllOrganizations", null);
            var model = new JournalistViewModel()
            {
                Mode = "Add",
                Organisation = new SelectList(organization, "OrganizationID", "Name"),
                Indexmodel = GetYears(),
                GenderList = ModelMapping.GetGender(),
                IsAcredited = IsAcredited
            };
            model.AssemblyCode = model1.Indexmodel.AssemblyId;
            model.SessionCode = model1.Indexmodel.SessionId;
            model.AssemblyName = model1.Indexmodel.AssemblyName;
            model.SessionName = model1.Indexmodel.SessionName;
            model.mAssemblyList = model1.Indexmodel.mAssemblyList;
            model.sessionList = model1.Indexmodel.sessionList;
            return View(model);
        }

        /// <summary>
        /// Renderv view of rthe edition of journalist
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult EditJournlist(int Id)
        {

            var organization = (List<Organization>)Helper.ExecuteService("Media", "GetAllOrganizations", null);


            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);


            var Journalist = (Journalist)Helper.ExecuteService("Media", "GetJournalistsById", new Journalist { JournalistID = Id });
            var model = Journalist.ToViewModel("Edit");


            // model.ImageLocation = fileAcessingSettings.SettingValue + model.FileLocation + "/" + model.FullImage;

            model.Photo = fileAcessingSettings.SettingValue + model.FileLocation + model.ThumbImage;
            model.Organisation = new SelectList(organization, "OrganizationID", "Name", model.OrganizationID);
            model.Indexmodel = GetYears();
            model.GenderList = ModelMapping.GetGender();

            model.AssemblyCode = model.Indexmodel.AssemblyId;
            model.SessionCode = model.Indexmodel.SessionId;
            model.AssemblyName = model.Indexmodel.AssemblyName;
            model.SessionName = model.Indexmodel.SessionName;
            model.mAssemblyList = model.Indexmodel.mAssemblyList;
            model.sessionList = model.Indexmodel.sessionList;
            model.ImagePath = model.ImageLocation;
            return View("CreateJournlist", model);
        }

        /// <summary>
        /// Save and update the journalist
        /// </summary>
        /// <param name="model"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveJournlist(JournalistViewModel model, HttpPostedFileBase file)
        {
            var tempImage = model.FullImage;
            var Journlist = model.ToDomainModel();
            var mediaSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMediaFileSetting", null);
            string path3 = System.IO.Path.Combine(mediaSettings.SettingValue);
            Journlist.Photo = path3;
            if (ModelState.IsValid)
            {

            }

            if (model.Mode == "Add")
            {


                // umang created
                //UploadPhoto(file, file.FileName);
                Journlist.FileName = "";//file.FileName;
                //Journlist.CreatedBy = Guid.Parse(CurrentSession.UserID);
                //Journlist.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                //
                // var pho = Journlist.Photo;
                // string[] words = pho.Split('/');

                Journlist.IsActive = false;


                //Helper.ExecuteService("Media", "AddJournalists", Journlist);

                //umang created
                var journalistId = (int)Helper.ExecuteService("Media", "AddJournalists", Journlist);
                //



                Journlist.JournalistID = journalistId;


                if (file != null)
                {
                    var filename = journalistId + "_" + file.FileName;
                    Journlist.Photo = UploadPhoto1(file, filename);
                    Journlist.FileName = filename;
                    Journlist.ThumbName = filename;
                    Helper.ExecuteService("Media", "UpdateJournalists", Journlist);

                }



            }
            else
            {


                if (file != null)
                {
                    if (Journlist.FileName != null)
                    {
                        RemoveExistingPhoto1(Journlist.FileName);
                    }
                    var upfileName = Journlist.JournalistID + "_" + file.FileName;

                    Journlist.Photo = UploadPhoto1(file, upfileName);

                    Journlist.FileName = upfileName;
                    Journlist.ThumbName = upfileName;

                    Helper.ExecuteService("Media", "UpdateJournalists", Journlist);
                }



                else
                {
                    Journlist.FileName = tempImage;
                    Journlist.ThumbName = model.ThumbName;
                    Helper.ExecuteService("Media", "UpdateJournalists", Journlist);
                }


            }


            if (!string.IsNullOrEmpty(model.AcreditationNumber) && model.IsAcredited == true)
            {
                return RedirectToAction("AcreditedJournalist");
            }
            else
            {
                return RedirectToAction("NonAcreditedJournalist");
            }
        }

        /// <summary>
        /// Delete the Journatist
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="AcreditationNumber"></param>
        /// <returns></returns>
        public ActionResult DeleteJournlist(int Id, string AcreditationNumber)
        {
            AcreditationNumber = Sanitizer.GetSafeHtmlFragment(AcreditationNumber);
            var Journalist = Helper.ExecuteService("Media", "DeleteJournalists", new Journalist { JournalistID = Id });

            if (AcreditationNumber != "" && AcreditationNumber != null)
            {
                return RedirectToAction("AcreditedJournalist");
            }
            else
            {
                return RedirectToAction("NonAcreditedJournalist");
            }
        }

        /// <summary>
        /// Chages the Status of te Journalist
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public object ChangeJournalistStatus(int Id)
        {
            Helper.ExecuteService("Media", "ChangeJournlistStatus", new Journalist { JournalistID = Id });

            var journlist = Helper.ExecuteService("Media", "GetJournalistsById", new Journalist { JournalistID = Id }) as Journalist;

            //Note : Don't create User Account and dont send the message to the journalist if not Accrecated.
            if (journlist != null && journlist.IsAcredited != false)
            {
                if (journlist.IsActive == true && string.IsNullOrEmpty(journlist.UserID.ToString()))
                {
                    //Add the data to the mUser table.
                    var userData = SubmitUserDetails(journlist);

                    //Update the Journalist with User ID which can be used to remove the user in Deactivated state.
                    journlist.UserID = userData.UserId;
                    Helper.ExecuteService("Media", "UpdateJournalists", journlist);
                }
                else if (journlist.IsActive == false && !string.IsNullOrEmpty(journlist.UserID.ToString()))
                {
                    mUsers model = new mUsers();
                    model.UserId = (Guid)journlist.UserID;
                    //Create problem to delete in mUser//var val = (mUsers)Helper.ExecuteService("User", "DeleteUserByUserID", model);
                    journlist.UserID = null;
                    Helper.ExecuteService("Media", "UpdateJournalists", journlist);
                }
            }
            return null;
        }

        public mUsers SubmitUserDetails(Journalist model1)
        {
            mUsers model = new mUsers();
            try
            {
                model.UserId = Guid.NewGuid();
                model.AdhaarID = model1.UID;
                model.AadarId = model1.UID;
                model.UserName = model1.UID;
                model.MailID = model1.Email;
                model.EmailId = model1.Email;
                model.SecretoryId = null;
                model.Address = model1.Address;
                model.Mobile = model1.Phone;
                model.MobileNo = model1.Phone;
                model.Photo = model1.Photo;
                model.DSCHash = string.Empty;
                model.DSCName = string.Empty;
                model.DSCType = string.Empty;
                model.EmpOrMemberCode = string.Empty;
                model.OTPValue = string.Empty;
                model.OfficeLevel = string.Empty;
                model.OfficeId = 0;
                model.DeptId = string.Empty;
                model.DOB = Convert.ToString(model1.Age);
                model.Name = model1.Name;

                //string password = GeneratePassword();
                string password = Membership.GeneratePassword(8, 1);
                #region Encrypt Password

                string ePassword = ComputeHash(password, "SHA1", null);

                #endregion

                #region Add user detailes
                model.Password = ePassword;
                model.IsDSCAuthorized = false;
                model.IsActive = true;
                model.IsNotification = true;
                model.OfficeLevel = "";
                model.IsMedia = "true";
                model.IsMember = "False";
                model.Permission = false;

                model.EmpOrMemberCode = "J";
                model.OTPValue = "OTP";

                try
                {
                    SMSService Services = new SMSService();
                    SMSMessageList smsMessage = new SMSMessageList();
                    smsMessage.SMSText = "Thanks for registration on eVidhan.\n\n Your eVidhan Id is " + model.AadarId + " and password is " + password;
                    smsMessage.ModuleActionID = 1;
                    smsMessage.UniqueIdentificationID = 1;
                    smsMessage.MobileNo.Add(model.MobileNo);
                    Notification.Send(true, false, smsMessage, null);

                    //SMSService Services = new SMSService();
                    // Services.sendSingleSMS("hpgovt", "hpdit@1234", "hpgovt", model.MobileNo, "Thanks for registration on eVidhan.\n\n Your eVidhan Id is " + model.AadarId + " and password is " + password);
                }
                catch
                {

                }

                model = (mUsers)Helper.ExecuteService("User", "AddUserDetails", model);

                //Assign the User with Media role.
                string roleID = ConfigurationManager.AppSettings["mediaRoleDescription"];
                tUserRoles userRoles = new tUserRoles();
                userRoles.RoleDetailsID = Guid.NewGuid();
                userRoles.Roleid = new Guid(roleID);
                userRoles.UserID = new Guid(model.UserId.ToString());
                userRoles.IsActive = true;
                Helpers.Helper.ExecuteService("User", "AssignRoles", userRoles);

                #endregion

                #region Add Temp USer

                TempUser tempUser = new TempUser();
                tempUser.userGuid = model.UserId;
                tempUser.UserId = model.UserName;
                tempUser.password = password;
                tempUser = (TempUser)Helper.ExecuteService("User", "AddTempUSerData", tempUser);

                #endregion

                #region mail sending
                // bool test = MailSend(model.EmailId, model.EmpId, model.AadarId, password);
                //if (test)
                // {
                //TempData["submitted"] = "Submitted";
                //TempData["password"] = tempUser.password;
                //  }
                #endregion
            }
            catch (Exception)
            {
                return model;
                throw;
            }

            return model;
        }

        public static string GeneratePassword()
        {
            int maxSize = 8; // whatever length you want            
            string a;
            a = "1qaz2wsx3edc4rfv5tgb6yhn7ujm8ik9ol";
            char[] chars = new char[a.Length];
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }

        public static string ComputeHash(string plainText, string hashAlgorithm, byte[] saltBytes)
        {
            // If salt is not specified, generate it on the fly.
            if (saltBytes == null)
            {
                // Define min and max salt sizes.
                int minSaltSize = 4;
                int maxSaltSize = 8;

                // Generate a random number for the size of the salt.
                Random random = new Random();
                int saltSize = random.Next(minSaltSize, maxSaltSize);

                // Allocate a byte array, which will hold the salt.
                saltBytes = new byte[saltSize];

                // Initialize a random number generator.
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

                // Fill the salt with cryptographically strong byte values.
                rng.GetNonZeroBytes(saltBytes);
            }

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes =
                    new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];


            HashAlgorithm hash;
            // Make sure hashing algorithm name is specified.
            if (hashAlgorithm == null)
                hashAlgorithm = "";

            // Initialize appropriate hashing algorithm class.
            switch (hashAlgorithm.ToUpper())
            {
                case "SHA1":
                    hash = new SHA1Managed();
                    break;

                case "SHA256":
                    hash = new SHA256Managed();
                    break;

                case "SHA384":
                    hash = new SHA384Managed();
                    break;

                case "SHA512":
                    hash = new SHA512Managed();
                    break;

                default:
                    hash = new MD5CryptoServiceProvider();
                    break;
            }

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(hashWithSaltBytes);

            // Return the result.
            return hashValue;
        }

        /// <summary>
        /// Not presently using
        /// </summary>
        /// <param name="JournalistIds"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AddJournalistToYearList(int[] JournalistIds, string Year)
        {
            Year = Sanitizer.GetSafeHtmlFragment(Year);

            Year = (Year == "") ? DateTime.Now.Year.ToString() : Year;
            var JounalistsToProcess = new List<JournalistsForPass>();
            foreach (var Id in JournalistIds)
            {
                JounalistsToProcess.Add(new JournalistsForPass { JournalistId = Id, Year = Year, IsCommitteMember = false });
            }

            Helper.ExecuteService("Media", "AddToJournalistForYear", JounalistsToProcess);
            return PartialView("_JurnalistCommitte");
        }

        /// <summary>
        /// Not in user presently
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult RemoveJournalistFromCommitteeMember(int Id)
        {
            Helper.ExecuteService("Media", "RemoveFromJournalistCommitteeList", new Journalist { JournalistID = Id });
            return RedirectToAction("JournalistIndex");
        }

        /// <summary>
        /// It will show the List of Acredited Journatist
        /// </summary>
        /// <returns></returns>
        public ActionResult AcreditedJournalist()
        {

            JournalistViewModel ViewModel = new JournalistViewModel();
            SiteSettings siteSettingMod = new SiteSettings();
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            JournalistsForPass model = new JournalistsForPass();
            model.SessionCode = siteSettingMod.SessionCode;
            model.AssemblyCode = siteSettingMod.AssemblyCode;
            ViewModel.JournalistList = (List<Journalist>)Helper.ExecuteService("Media", "GetAllAcreditedJournalists1", model);
            IEnumerable<JournalistViewModel> ListJounalist = ViewModel.JournalistList.ToViewModel();
            ViewModel.Indexmodel = GetYears();
            ViewModel.AssemblyCode = ViewModel.Indexmodel.AssemblyId;
            ViewModel.SessionCode = ViewModel.Indexmodel.SessionId;
            ViewModel.SessionName = ViewModel.Indexmodel.SessionName;
            ViewModel.AssemblyName = ViewModel.Indexmodel.AssemblyName;
            ViewModel.sessionList = ViewModel.Indexmodel.sessionList;
            ViewModel.mAssemblyList = ViewModel.Indexmodel.mAssemblyList;
            foreach (var list in ListJounalist)
            {

                ViewModel.UID = list.UID;
                // ViewModel.ImageLocation = fileAcessingSettings.SettingValue+list.Photo+list.ThumbImage;
            }
            ViewModel.JournalistList1 = ListJounalist;
            //Change for model pass on --sanjay
            //var Journalist = (List<Journalist>)Helper.ExecuteService("Media", "GetAllAcreditedJournalists", null);
            //IEnumerable<JournalistViewModel> ListJounalist = Journalist.ToViewModel();

            //List<JournalistViewModel> ViewModel = new List<JournalistViewModel>();

            //if (Journalist.Count > 0)
            //{
            //    ViewModel = ListJounalist.Select(a => new JournalistViewModel()
            //    {
            //        JournalistID = a.JournalistID,
            //        Name = a.Name,
            //        Email = a.Email,
            //        IsActive = a.IsActive,
            //        Phone = a.Phone,
            //        UID = a.UID,
            //        AcreditationNumber = a.AcreditationNumber,
            //        OrganizationID = a.OrganizationID,
            //        Photo = a.Photo,
            //        OrganisationName = a.OrganisationName,
            //        Designation = a.Designation,
            //        Address = a.Address,
            //        FathersName = a.FathersName,
            //        Indexmodel = GetYears(),
            //        IsRequested = a.IsInCurrentYear
            //    }).ToList();
            //}
            //else
            //{
            //    JournalistViewModel mod = new JournalistViewModel()
            //    {
            //        JournalistID = 0,
            //        Indexmodel = GetYears()
            //    };
            //    ViewModel.Add(mod);
            //}

            return View(ViewModel);
        }

        /// <summary>
        /// It will check wather the journatist exist in current year of vs
        /// </summary>
        /// <param name="JournalistID"></param>
        /// <returns></returns>
        public bool CheckCurrentYear(int JournalistID)
        {
            JournalistsForPass model = new JournalistsForPass();
            model.JournalistId = JournalistID;
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionCode = siteSettingMod.SessionCode;
            model.AssemblyCode = siteSettingMod.AssemblyCode;
            var Journalist = (List<JournalistsForPass>)Helper.ExecuteService("Media", "GetJournalistsForPassByIdCurrentSession", model);
            if (Journalist != null)
            {
                if (Journalist.Count > 0)
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        /// <summary>
        /// include the selected journalist to the current Year
        /// </summary>
        /// <param name="JIDs"></param>
        /// <returns></returns>
        public object SubmitSelectedToCurrentYear(string JIDs)
        {
            string[] JornalistIDs = JIDs.Split(',');
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            foreach (var JID in JornalistIDs)
            {
                JournalistsForPass model = new JournalistsForPass();
                model.JournalistId = Convert.ToInt16(JID);
                model.Year = Convert.ToString(DateTime.Now.Year);
                model.AssemblyCode = siteSettingMod.AssemblyCode;
                model.SessionCode = siteSettingMod.SessionCode;
                Helper.ExecuteService("Media", "SubmitJournalistToCurrentYear", model);
            }
            return true;
        }

        /// <summary>
        /// will include the one journatist to current year 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public object SubmitToCurrentYear(string ID)
        {
            JournalistsForPass model = new JournalistsForPass();
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.JournalistId = Convert.ToInt16(ID);
            model.Year = Convert.ToString(DateTime.Now.Year);
            model.AssemblyCode = siteSettingMod.AssemblyCode;
            model.SessionCode = siteSettingMod.SessionCode;
            Helper.ExecuteService("Media", "SubmitJournalistToCurrentYear", model);

            return true;
        }

        /// <summary>
        ///will render the NonAcredited Journalist 
        /// </summary>
        /// <returns></returns>
        public ActionResult NonAcreditedJournalist()
        {

            JournalistViewModel ViewModel = new JournalistViewModel();
            SiteSettings siteSettingMod = new SiteSettings();
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            JournalistsForPass md = new JournalistsForPass();
            md.SessionCode = siteSettingMod.SessionCode;
            md.AssemblyCode = siteSettingMod.AssemblyCode;
            ViewModel.JournalistList = (List<Journalist>)Helper.ExecuteService("Media", "GetAllNonAcreditedJournalists1", md);
            IEnumerable<JournalistViewModel> ListJounalist = ViewModel.JournalistList.ToViewModel();

            ViewModel.Indexmodel = GetYears();
            ViewModel.AssemblyCode = ViewModel.Indexmodel.AssemblyId;
            ViewModel.SessionCode = ViewModel.Indexmodel.SessionId;
            ViewModel.SessionName = ViewModel.Indexmodel.SessionName;
            ViewModel.AssemblyName = ViewModel.Indexmodel.AssemblyName;
            ViewModel.sessionList = ViewModel.Indexmodel.sessionList;
            ViewModel.mAssemblyList = ViewModel.Indexmodel.mAssemblyList;
            ViewModel.JournalistList1 = ListJounalist;


            //var Journalist = (List<Journalist>)Helper.ExecuteService("Media", "GetAllNonAcreditedJournalists", null);
            //IEnumerable<JournalistViewModel> ListJounalist = Journalist.ToViewModel();

            //List<JournalistViewModel> ViewModel = new List<JournalistViewModel>();
            //if (Journalist.Count > 0)
            //{
            //    ViewModel = ListJounalist.Select(a => new JournalistViewModel()
            //    {
            //        JournalistID = a.JournalistID,
            //        Name = a.Name,
            //        Email = a.Email,
            //        IsActive = a.IsActive,
            //        Phone = a.Phone,
            //        UID = a.UID,
            //        AcreditationNumber = a.AcreditationNumber,
            //        OrganizationID = a.OrganizationID,
            //        Photo = a.Photo,
            //        OrganisationName = a.OrganisationName,
            //        Designation = a.Designation,
            //        Address = a.Address,
            //        FathersName = a.FathersName,
            //        Indexmodel = GetYears(),
            //        IsRequested = CheckCurrentYear(a.JournalistID)
            //    }).ToList();
            //}
            //else
            //{
            //    JournalistViewModel mod = new JournalistViewModel()
            //    {
            //        JournalistID = 0,
            //        Indexmodel = GetYears()
            //    };
            //    ViewModel.Add(mod);
            //}
            return View(ViewModel);
        }

        /// <summary>
        /// Not in use 
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateNewAcreditedList()
        {
            return View("CreateNewAcreditedList");
        }

        /// <summary>
        /// Not in use 
        /// </summary>
        /// <returns></returns>
        public ActionResult CopyPriorAcreditedList()
        {
            return RedirectToAction("AcreditedJournalist");
        }

        /// <summary>
        /// Not in use 
        /// </summary>
        /// <param name="JournalistIds"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public JsonResult AddToAcreditedList(int[] JournalistIds, string Year)
        {
            return Json(true);
        }

        /// <summary>
        /// Not in use 
        /// </summary>
        /// <returns></returns>
        public ActionResult CommitteMemberList()
        {
            return View("CommitteeMemberList");
        }

        /// <summary>
        /// Not in use 
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateCommitteeMembers()
        {
            return View("CreateCommitteeMembers");
        }

        /// <summary>
        /// Not in use 
        /// </summary>
        /// <param name="JournalistIds"></param>
        /// <returns></returns>
        public ActionResult AddAsCommitteMember(int[] JournalistIds)
        {
            return Json(true);
        }

        #endregion

        /// <summary>
        /// It will send the selected journalist of the current Year to VS for approval
        /// </summary>
        /// <param name="JIDs"></param>
        /// <returns></returns>
        public object SubmitSelectedPassRequest(string JIDs)
        {
            string[] JornalistIDs = JIDs.Split(',');

            foreach (var JID in JornalistIDs)
            {
                JournalistsForPass model = new JournalistsForPass();
                model.JournalistsForPassId = Convert.ToInt16(JID);
                Helper.ExecuteService("Media", "SubmitPassRequest", model);
            }
            return true;
        }

        /// <summary>
        /// It will send the one journalist of the current Year to VS for approval
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public object SubmitPassRequest(string ID)
        {
            JournalistsForPass model = new JournalistsForPass();
            model.JournalistsForPassId = Convert.ToInt16(ID);
            Helper.ExecuteService("Media", "SubmitPassRequest", model);

            return true;
        }

        /// <summary>
        /// It will remave journalist from current year
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public object RemoveJournalistFormCurrentYear(string ID)
        {
            JournalistsForPass model = new JournalistsForPass();
            model.JournalistsForPassId = Convert.ToInt16(ID);
            model.Year = Convert.ToString(DateTime.Now.Year);
            Helper.ExecuteService("Media", "RemoveJournalistFormCurrentYear", model);

            return true;
        }

        /// <summary>
        /// Will show the list of journalist for the VS
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult ListOfJournalistPass(int Id)
        {
            try
            {
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                //Id = Sanitizer.GetSafeHtmlFragment(Id);
                MediaYearViewModel model = new MediaYearViewModel();
                JournalistsForPass model1 = new JournalistsForPass();
                model1.SessionCode = Id;
                model1.AssemblyCode = siteSettingMod.AssemblyCode;
                model.Year = Id;

                var journalist = (List<Journalist>)Helper.ExecuteService("Media", "GetMediaByYear", model1);
                IEnumerable<JournalistViewModel> ListJounalist = journalist.ToViewModel();
                bool IsCurrentYear = DateTime.Now.Year == Convert.ToInt16(Id);
                var ViewModel = new List<JournalistViewModel>();


                bool IsCurrentSession = Convert.ToBoolean(siteSettingMod.SessionCode == Id);
                model.Indexmodel = GetYears();
                if (IsCurrentSession)
                {
                    ViewModel = ListJounalist.Select(a => new JournalistViewModel()
                    {
                        JournalistID = a.JournalistID,
                        Name = a.Name,
                        Email = a.Email,
                        IsActive = a.IsActive,
                        Phone = a.Phone,
                        UID = a.UID,
                        AcreditationNumber = a.AcreditationNumber,
                        OrganizationID = a.OrganizationID,
                        // Photo = a.Photo,
                        Photo = a.ImageLocation,
                        OrganisationName = a.OrganisationName,
                        Designation = a.Designation,
                        Address = a.Address,
                        FathersName = a.FathersName,
                        Indexmodel = GetYears(),
                        //IsRequested = Convert.ToBoolean(CheckIsrequestedByYear(a.JournalistID, Id.ToString(), false)),
                        //RequestId = Convert.ToInt16(CheckIsrequestedByYear(a.JournalistID, Id.ToString(), true)),
                        IsRequested = a.IsRequested,
                        RequestId = a.RequestId,
                        IsCurrentSession = siteSettingMod.SessionCode == Id
                        // IsCurrentYear = DateTime.Now.Year == Convert.ToInt16(Id)
                    }).ToList();
                }
                else
                {
                    ViewModel = ListJounalist.Select(a => new JournalistViewModel()
                    {
                        JournalistID = a.JournalistID,
                        Name = a.Name,
                        Email = a.Email,
                        IsActive = a.IsActive,
                        Phone = a.Phone,
                        UID = a.UID,
                        AcreditationNumber = a.AcreditationNumber,
                        OrganizationID = a.OrganizationID,
                        // Photo = a.Photo,
                        Photo = a.ImageLocation,
                        OrganisationName = a.OrganisationName,
                        Designation = a.Designation,
                        Address = a.Address,
                        FathersName = a.FathersName,
                        Indexmodel = GetYears(),
                        IsRequested = CheckCurrentYear(a.JournalistID),
                        //RequestId = Convert.ToInt16(CheckIsrequestedByYear(a.JournalistID, Id.ToString(), true)),
                        RequestId = a.RequestId,
                        IsCurrentSession = siteSettingMod.SessionCode == Id
                        // IsCurrentYear = DateTime.Now.Year == Convert.ToInt16(Id)
                    }).ToList();
                }

                model.AssemblyId = model.Indexmodel.AssemblyId;
                model.SessionId = model.Indexmodel.SessionId;
                model.SessionName = model.Indexmodel.SessionName;
                model.AssemblyName = model.Indexmodel.AssemblyName;
                model.sessionList = model.Indexmodel.sessionList;
                model.mAssemblyList = model.Indexmodel.mAssemblyList;
                model.IsCurrentSessionCode = siteSettingMod.SessionCode;

                model.ListJounalist = ViewModel;
                model.Years = GetYears();
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return null;
            }
        }

        /// <summary>
        /// It will check weater a journalist is Requested to VS Or not
        /// </summary>
        /// <param name="JournalistID"></param>
        /// <param name="Id"></param>
        /// <param name="IsId"></param>
        /// <returns></returns>
        public object CheckIsrequestedByYear(int JournalistID, string Id, bool IsId)
        {
            if (IsId)
            {
                JournalistsForPass model = new JournalistsForPass();
                model.JournalistId = JournalistID;
                model.Year = Id;
                var Journalist = (JournalistsForPass)Helper.ExecuteService("Media", "GetAllJournalistsForPassByIdYear", model);
                if (Journalist != null)
                {
                    //if (Journalist.Count > 0)
                    //{
                    //return Journalist[0].JournalistsForPassId;
                    return Journalist.JournalistsForPassId;
                    //}
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                JournalistsForPass model = new JournalistsForPass();
                model.JournalistId = JournalistID;
                model.Year = Id;
                var Journalist = (List<JournalistsForPass>)Helper.ExecuteService("Media", "GetJournalistsForPassByIdandYear", model);
                if (Journalist != null)
                {
                    if (Journalist.Count > 0)
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public ActionResult FullJournalistDetails(string Id)
        {
            Id = Sanitizer.GetSafeHtmlFragment(Id);
            var Journalist = (Journalist)Helper.ExecuteService("Media", "GetJournalistsById", new Journalist { JournalistID = Convert.ToInt16(Id) });
            var model = Journalist.ToViewModel("Edit");
            return PartialView("FullJournalistDetails", model);
        }

        /// <summary>
        /// Not in Use
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult PartialYearList(string Id)
        {
            Id = Sanitizer.GetSafeHtmlFragment(Id);
            tMediaYears mode = new tMediaYears();
            var MediaYearList = (List<tMediaYears>)Helper.ExecuteService("Media", "GetMediaByYear", mode);
            List<MediaYearViewModel> ListMediaYears = MediaYearList.ToViewModel();
            MediaYearViewModel model1 = new MediaYearViewModel();
            model1.Year = Convert.ToInt16(Id);
            return PartialView("PartialYearList", model1);
        }

        /// <summary>
        ///  Not in Use
        /// </summary>
        /// <param name="Year"></param>
        /// <returns></returns>
        public ActionResult AddMediaToYear(string Year)
        {
            Year = Sanitizer.GetSafeHtmlFragment(Year);
            tMediaYears mode = new tMediaYears();
            var MediaYearList = (List<tMediaYears>)Helper.ExecuteService("Media", "GetMediaByYear", mode);
            List<MediaYearViewModel> ListMediaYears = MediaYearList.ToViewModel();
            MediaYearViewModel model1 = new MediaYearViewModel();
            model1.Year = Convert.ToInt16(Year);
            return PartialView(model1);
        }

        #region Private Methods

        /// <summary>
        /// It will upload photo
        /// </summary>
        /// <param name="File"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        private string UploadPhoto(HttpPostedFileBase File, Guid FileName)
        {
            if (File != null)
            {
                string extension = System.IO.Path.GetExtension(File.FileName);
                SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                session.AssemblyID = Convert.ToInt16(siteSettingMod.AssemblyCode);
                session.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);
                string Url = "/Images/Pass/Photo/" + session.AssemblyID + "/" + session.SessionCode + "/";
                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                if (!Dir.Exists)
                {
                    Dir.Create();
                }

                string path = System.IO.Path.Combine(Server.MapPath(Url), FileName + extension);
                File.SaveAs(path);
                return (Url + FileName + extension);
            }
            return null;
        }

        private void RemoveExistingPhoto(string OldPhoto)
        {
            string path = System.IO.Path.Combine(Server.MapPath("~/Images/Pass/Photo"), OldPhoto);

            if (System.IO.File.Exists(OldPhoto))
            {
                System.IO.File.Delete(OldPhoto);
            }


        }


        //Added by Umang

        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string UploadPhoto1(HttpPostedFileBase File, string FileName)
        {
            try
            {


                if (File != null)
                {
                    var mediaSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMediaFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting1", null);

                    var mediaThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMediaThumbFileSetting", null);
                    string extension = System.IO.Path.GetExtension(File.FileName);
                    string Dirpath = System.IO.Path.Combine(FileSettings.SettingValue + mediaSettings.SettingValue);//Server.MapPath("~/Images/News/"), FileName);
                    string Dirpath1 = System.IO.Path.Combine(FileSettings.SettingValue + mediaThumbSettings.SettingValue);//Server.MapPath("~/Images/News/"), FileName);
                    string DirbasePath = System.IO.Path.Combine(FileSettings.SettingValue + mediaSettings.SettingValue + "\\Temp");//Server.MapPath("~/Images/News/"), FileName);

                    DirectoryInfo Dir = new DirectoryInfo(Dirpath);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    DirectoryInfo Dir1 = new DirectoryInfo(DirbasePath);
                    if (!Dir1.Exists)
                    {
                        Dir1.Create();
                    }
                    DirectoryInfo Dir2 = new DirectoryInfo(Dirpath1);
                    if (!Dir2.Exists)
                    {
                        Dir2.Create();
                    }
                    string path = System.IO.Path.Combine(Dirpath, FileName);
                    string path1 = System.IO.Path.Combine(Dirpath1, FileName);
                    string basePath = System.IO.Path.Combine(DirbasePath, FileName);
                    Extensions.ImageResizerExtensions imgex = new Extensions.ImageResizerExtensions(600);
                    File.SaveAs(basePath);
                    imgex.Resize(basePath, path);
                    System.IO.File.Delete(basePath);
                    Extensions.ImageResizerExtensions imgext = new Extensions.ImageResizerExtensions(85);
                    File.SaveAs(basePath);
                    imgext.Resize(basePath, path1);

                    System.IO.File.Delete(basePath);
                    return ("/Media/");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            if (File != null)
            {
                string extension = System.IO.Path.GetExtension(File.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Images/Media/"), FileName);
                File.SaveAs(path);
                return ("/Images/Media/");
            }
            return null;
        }

        private void RemoveExistingPhoto1(string OldPhoto)
        {

            try
            {

                var mediaSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMediaFileSetting", null);
                var mediaThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMediaThumbFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting1", null);
                string path1 = System.IO.Path.Combine(FileSettings.SettingValue + mediaThumbSettings.SettingValue, OldPhoto);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + mediaSettings.SettingValue + "\\", OldPhoto);

                //string path = System.IO.Path.Combine(Server.MapPath("~/Images/Gallery/"), OldPhoto);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    System.IO.File.Delete(path1);
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        #endregion

        #region Passrequest

        /// <summary>
        ///  Not in Use
        /// </summary>
        /// <returns></returns>
        public ActionResult PassRequest()
        {
            return View();
        }

        /// <summary>
        ///  Not in Use
        /// </summary>
        /// <returns></returns>
        public ActionResult CreatePassRequest()
        {

            var departments = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Assembly", "GetAllDepartments", null);
            var Passtype = (List<PassCategory>)Helper.ExecuteService("Pass", "GetAllPassCategories", null);
            SelectListItem item = new SelectListItem() { Selected = true, Text = "--Select Department--", Value = "" };
            List<SelectListItem> deotlst = new List<SelectListItem>();
            deotlst.Add(item);

            foreach (var itemdata in departments)
            {
                SelectListItem newitem = new SelectListItem() { Selected = true, Text = itemdata.deptname, Value = itemdata.deptId.ToString() };

                deotlst.Add(newitem);

            }
            var model = new PassRequestViewModel()
            {

                DeptList = deotlst,
                PasstypeList = new SelectList(Passtype, "Name", "Name", null),

            };


            return View(model);
        }

        /// <summary>
        ///  Not in Use
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SavePassRequest(PassRequestViewModel model)
        {

            model.IsActive = true;
            Helper.ExecuteService("Media", "AddPassrequest", model);

            return RedirectToAction("PassRequest");
        }
        #endregion

    }
}
