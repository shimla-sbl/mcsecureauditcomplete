﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.OneTimeUserRegistration
{
    public class OneTimeUserRegistrationAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "OneTimeUserRegistration";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "OneTimeUserRegistration_default",
                "OneTimeUserRegistration/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
