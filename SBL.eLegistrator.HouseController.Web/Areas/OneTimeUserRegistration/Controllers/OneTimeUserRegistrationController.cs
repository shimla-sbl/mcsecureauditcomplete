﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.OneTimeUserRegistration.Controllers
{
    public class OneTimeUserRegistrationController : Controller
    {
        //
        // GET: /OneTimeUserRegistration/OneTimeUserRegistration/

        public ActionResult Index()
        {

            OneTimeUserRegistrationModel model = new OneTimeUserRegistrationModel();

            if (TempData["Id"] != null)
            {
                string TempId = TempData["Id"].ToString();
                string TempA = TempData["AssemblyId"].ToString();
                string TempS = TempData["SessionId"].ToString();
                string TempMNo = TempData["MobileNo"].ToString();
                model.DocumentTypeId = Convert.ToInt32(TempId);
                model.AssemblyID = Convert.ToInt32(TempA);
                model.SessionID = Convert.ToInt32(TempS);
                model.MobileNumber = TempMNo;
                model.ModuleId = Convert.ToInt32(TempId);
            }
            //model.UId = CurrentSession.UserID;
           // model.RoleName = CurrentSession.RoleName;
            
            model.MobileNumber = CurrentSession.MbNo;
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            model = (OneTimeUserRegistrationModel)Helper.ExecuteService("OneTimeUserRegistration", "GetAssemblySessionList", model);
            model = (OneTimeUserRegistrationModel)Helper.ExecuteService("OneTimeUserRegistration", "GetCountForQuestionTypes", model);
            
            if (string.IsNullOrEmpty(CurrentSession.AssemblyId) || string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.AssemblyId = model.AssemblyID.ToString();
                CurrentSession.SessionId = model.SessionID.ToString();
            }
            return View(model);
           
        }
        public ActionResult DraftStaredQuestion(string Id, string AssemblyId,string SessionId, string MobileNo)
        {
           
            tUserRegistrationDetails model = new tUserRegistrationDetails();
            if (Id== null )
            {
             string TempId = TempData["Id"].ToString();
             string TempA = TempData["AssemblyId"].ToString();
             string TempS = TempData["SessionId"].ToString();
             string TempMNo = TempData["MobileNo"].ToString();
             model.DocumentTypeId = Convert.ToInt32(TempId);
             model.AssemblyId = Convert.ToInt32(TempA);
             model.SessionId = Convert.ToInt32(TempS);
             model.MobileNumber = TempMNo;
          
            }
            else
            {
            model.DocumentTypeId = Convert.ToInt32(Id);
            model.AssemblyId = Convert.ToInt32(AssemblyId);
            model.SessionId = Convert.ToInt32(SessionId);
            model.MobileNumber = MobileNo;
            }
            model = (tUserRegistrationDetails)Helper.ExecuteService("OneTimeUserRegistration", "GetSQDetails", model);
            return PartialView("_StarredQuestion", model);
        }

        public ActionResult UpdateProfile(string MobileNo)
        {

            tUserRegistrationDetails model = new tUserRegistrationDetails();
           
             model.MobileNumber = MobileNo;

             model = (tUserRegistrationDetails)Helper.ExecuteService("OneTimeUserRegistration", "GetUserDetails", model);
             foreach (var item in model.OneTimeUserRegistrationModel)
             {
                 model.UserName=item.UserName;
                 model.AddressLocations = item.Addresslocation;
             }
            return PartialView("UpdateProfile", model);
        }
        public JsonResult UpdateProfileUser(string UserName,string Addressloc ,string MobileNo)
        {

            tUserRegistrationDetails model = new tUserRegistrationDetails();
            model.MobileNumber = MobileNo;
            model.UserName = UserName;
            model.AddressLocations = Addressloc;

            string st = Helper.ExecuteService("OneTimeUserRegistration", "UpdateProfileUser", model) as string;
       
            return Json("obj", JsonRequestBehavior.AllowGet);
        }
    }
}
