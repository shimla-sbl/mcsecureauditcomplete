﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.DeputyOrJoint
{
    public class DeputyOrJointAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DeputyOrJoint";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DeputyOrJoint_default",
                "DeputyOrJoint/{controller}/{action}/{id}",
                new { action = "Index", Controller = "DashBoardDeputyOrJoint", id = UrlParameter.Optional }
            );
        }
    }
}
