﻿using SBL.DomainModel.Models.salaryhead;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.DeputyOrJoint.Controllers
{
    public class SalaryBillsController : Controller
    {
        //
        // GET: /DeputyOrJoint/SalaryBills/

        public ActionResult Index()
        {
            return View();
        }

        #region Requested Bills

        public ActionResult RequestedBills()
        {
            return View();
        }
        public ActionResult _PartialRequestedBills()
        {
            return PartialView("_PartialRequestedBills");
        }
        public ActionResult getRequestedBillList(string monthID, String YearID, string status, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] arr = { int.Parse(monthID), int.Parse(YearID), int.Parse(status), assemblyid, ispart };
            List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("DeputyOrJoint", "getSalaryBillList", arr);
            return PartialView("_requestedBillsList", _slist);
        }

        public ActionResult changeBillStatus(string status, string AutoId, string user)
        {
            string[] arr = new string[] { status, AutoId, user };
            Helper.ExecuteService("DeputyOrJoint", "changeBillStatus", arr);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult saveBillRemark(string remark, string AutoID)
        {

            string msg = "UserName**" + remark + "**" + DateTime.Now.ToShortDateString() + " , " + DateTime.Now.ToShortTimeString() + "@@";
            string[] arr = new string[] { msg, AutoID };
            string RemarkList = (string)Helper.ExecuteService("salaryheads", "saveBillRemark", arr);
            return Json(RemarkList, JsonRequestBehavior.AllowGet);
        }
        public Object selectedBills(List<salaryBillInfo> _listsbi, string CStatus)
        {
            string bills = string.Empty;
            List<string> _list = new List<string>();
            foreach (var item in _listsbi)
            {
                if (item.checkStatus.HasValue)
                {
                    string[] arr = new string[] { CStatus, item.AutoID.ToString(), "jdc" };
                    bills = bills + (Int32)Helper.ExecuteService("DeputyOrJoint", "changeBillStatus", arr) + ",";
                }
            }
            //getBillInfo
            string[] ids = bills.Split(',');
            if (ids.Length > 0)
            {
                foreach (var i in ids)
                {
                    int id = 0;
                    int.TryParse(i, out id);
                    if (id != 0)
                    {
                        _list.Add((string)Helper.ExecuteService("DeputyOrJoint", "getBillInfo", id));
                    }
                }
            }
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        #endregion Requested Bills
    }
}