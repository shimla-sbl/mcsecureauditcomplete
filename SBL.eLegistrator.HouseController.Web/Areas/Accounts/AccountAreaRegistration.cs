﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts
{
    public class AccountsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Accounts";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Account_default",
                "Accounts/{controller}/{action}/{id}",
                new { action = "index", Controller="Accountdashboard", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "print_salary",
                "Accounts/{controller}/{action}/{memberid}/{month}/{year}",
                new { action = "printsalary", id = UrlParameter.Optional }
            );
        }
    }
}
