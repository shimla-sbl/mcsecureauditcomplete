﻿using SBL.DomainModel.Models.Loan;
using SBL.DomainModel.Models.Member;
using System.Collections.Generic;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models
{
    public class showMemberDetails
    {
        public mMemberAccountsDetails _mMemberAccountDetails { get; set; }

        public IEnumerable<loanDetails> _loanList { get; set; }

        public string _designation { get; set; }
    }
}