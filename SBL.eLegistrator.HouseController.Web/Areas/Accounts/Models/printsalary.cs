﻿using SBL.DomainModel.Models.salaryhead;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models
{
    public class printsalary
    {
        public membersMonthlySalaryDetails membersMonthlySalaryDetails { get; set; }
        public IEnumerable<membersSalary> membersSalary { get; set; }
        public string MemberName { get; set; }
        public string Designation { get; set; }
        
    }
}