﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models
{
    public class SalaryNotification
    {
        public List<string> emailList { get; set; }
        public List<string> mobileList { get; set; }
        public bool emailStatus { get; set; }
        public bool mobileStatus { get; set; }
        public string mailBody { get; set; }
        public string mailSubject { get; set; }
        public string msgBody { get; set; }
        public string attachmentUrl { get; set; }

    }
}