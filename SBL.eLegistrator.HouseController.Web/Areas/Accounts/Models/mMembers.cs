﻿using SBL.DomainModel.Models.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models
{
    [Serializable]
    public class mMembers
    {
        public int MemberId { get; set; }
        public string Prefix { get; set; }
        public string Name { get; set; }

        public string District { get; set; }
        public string Mobile { get; set; }
        public int catId { get; set; }

    }


    public static class ModelMapping
    {

        public static SelectList GetFinancialYear(string SelectedValue)
        {

            var result = new List<SelectListItem>();
            for (int i = 5; i >= -5; i--)
            {
                int CurrentYear = DateTime.Today.AddYears(i).Year;
                int PreviousYear = DateTime.Today.AddYears(i).Year - 1;
                int NextYear = DateTime.Today.AddYears(i).Year + 1;
                string PreYear = PreviousYear.ToString();
                string NexYear = NextYear.ToString();
                string CurYear = CurrentYear.ToString();
                string FinYearText = null;
                string FinYearValue = null;
                if (DateTime.Today.Month > 3)
                {
                    FinYearText = string.Format("April {0} - March {1}", CurYear, NexYear);
                    FinYearValue = string.Format("{0}-{1}", CurYear, NexYear);
                }
                else
                {
                    FinYearText = string.Format("April {0} - March {1}", PreYear, CurYear);
                    FinYearValue = string.Format("{0}-{1}", PreYear, CurYear);
                }

                result.Add(new SelectListItem() { Text = FinYearText, Value = FinYearValue });

            }


            return new SelectList(result, "Value", "Text", SelectedValue);
        }


        public static SelectList ToViewModelsalarymembers1(this IEnumerable<mMember> list)
        {
            var result1 = new List<SelectListItem>();
            string Name = null;
            string MemberId = null;
            var result = list.Select(paper => new mMembers()
            {
                MemberId = paper.MemberCode,
                Name = paper.Name,

            });
            foreach (var item in result)
            {
                Name = item.Name;
                MemberId = Convert.ToString(item.MemberId);
                result1.Add(new SelectListItem() { Text = Name, Value = MemberId });
            }


            return new SelectList(result1, "Value", "Text");
        }

        public static List<mMembers> ToViewModelsalarymembers(this IEnumerable<mMember> list)
        {
            var result = list.Select(paper => new mMembers()
            {
                MemberId = paper.MemberCode,
                Name = paper.Name,
                //  NameLocal = paper.NameLocal,
                Mobile = paper.Mobile,
                Prefix = paper.Prefix,
                District = paper.District,
                catId = 0
            });
            return result.ToList();
        }

        public static string NumbersToWords(this int inputNumber)
        {
            int inputNo = inputNumber;

            if (inputNo == 0)
                return "Zero";

            int[] numbers = new int[4];
            int first = 0;
            int u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (inputNo < 0)
            {
                sb.Append("Minus ");
                inputNo = -inputNo;
            }

            string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ",
            "Five " ,"Six ", "Seven ", "Eight ", "Nine "};
            string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
            "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};
            string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
            "Seventy ","Eighty ", "Ninety "};
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };

            numbers[0] = inputNo % 1000; // units
            numbers[1] = inputNo / 1000;
            numbers[2] = inputNo / 100000;
            numbers[1] = numbers[1] - 100 * numbers[2]; // thousands
            numbers[3] = inputNo / 10000000; // crores
            numbers[2] = numbers[2] - 100 * numbers[3]; // lakhs

            for (int i = 3; i > 0; i--)
            {
                if (numbers[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (numbers[i] == 0) continue;
                u = numbers[i] % 10; // ones
                t = numbers[i] / 10;
                h = numbers[i] / 100; // hundreds
                t = t - 10 * h; // tens
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i == 0) sb.Append("and ");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            return sb.ToString().TrimEnd();
        }

        public static string NumbersToWords(this long inputNumber)
        {
            long inputNo = inputNumber;

            if (inputNo == 0)
                return "Zero";

            long[] numbers = new long[4];
            int first = 0;
            long u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (inputNo < 0)
            {
                sb.Append("Minus ");
                inputNo = -inputNo;
            }

            string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ",
            "Five " ,"Six ", "Seven ", "Eight ", "Nine "};
            string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
            "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};
            string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
            "Seventy ","Eighty ", "Ninety "};
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };

            numbers[0] = inputNo % 1000; // units
            numbers[1] = inputNo / 1000;
            numbers[2] = inputNo / 100000;
            numbers[1] = numbers[1] - 100 * numbers[2]; // thousands
            numbers[3] = inputNo / 10000000; // crores
            numbers[2] = numbers[2] - 100 * numbers[3]; // lakhs

            for (int i = 3; i > 0; i--)
            {
                if (numbers[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (numbers[i] == 0) continue;
                u = numbers[i] % 10; // ones
                t = numbers[i] / 10;
                h = numbers[i] / 100; // hundreds
                t = t - 10 * h; // tens
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i == 0) sb.Append("and ");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            return sb.ToString().TrimEnd();
        }



    }
}