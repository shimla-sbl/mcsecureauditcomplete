﻿using SBL.DomainModel.Models.Loan;
using System.Collections.Generic;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models
{
    public class loanDetailsViewModal
    {
        public showMemberDetails _showMemberDetails { get; set; }

        public loanDetails _loanDetails { get; set; }

        public IEnumerable<disbursementDetails> _disbursmentslist { get; set; }

        public loaneeDetails _loandeeetails { get; set; }


    }
}