﻿using SBL.DomainModel.Models.JobsPosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models
{
    public class CandidateModel
    {
        public Candidates candidate { get; set; }
        public List<ExamDetails> ExamDetailsList { get; set; }
        public List<ExperiencesDetails> ExperiencesDetails { get; set; }
    }
}