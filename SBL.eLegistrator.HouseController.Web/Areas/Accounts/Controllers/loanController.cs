﻿using EvoPdf;
using Ionic.Zip;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Loan;
using SBL.DomainModel.Models.Member;
using SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Controllers
{
    public class loanController : Controller
    {
        //
        // GET: /Accounts/loan/

        public ActionResult Index()
        {
            return View();
        }

        #region Loan Type

        #region get Loan Details

        public ActionResult loanType()
        {
            return View();
        }
        public ActionResult loanTypePartial()
        {
            return PartialView("_loanType");
        }
        public ActionResult getLoanTypeList()
        {
            List<LoanType> loneTypeList = (List<LoanType>)Helper.ExecuteService("loan", "getLoneTypeList", null);

            return PartialView("_loanTypeList", loneTypeList);
        }

        public ActionResult fillLoanTypeList()
        {
            List<fillLoanType> list = (List<fillLoanType>)Helper.ExecuteService("loan", "fillLoanTypeList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult getLoanType()
        {
            LoanType list = new LoanType();
            list.Mode = "save";
            list.loanName = "";
            return PartialView("_createLoanType", list);
        }

        public ActionResult getLoanTypeByID(int loanID)
        {
            LoanType list = (LoanType)Helper.ExecuteService("loan", "getLoanTypeByID", loanID);
            list.Mode = "update";
            return PartialView("_updateTypeList", list);
        }

        public JsonResult checkLoanTypeByName(string loanName)
        {
            int i = (int)Helper.ExecuteService("loan", "checkLoanTypeByName", loanName);
            return Json(i == 0, JsonRequestBehavior.AllowGet);
        }

        #endregion get Loan Details

        #region save and update loan Type

        public JsonResult saveUpadteLoanType(LoanType list)
        {
            if (ModelState.IsValid)
            {
                if (list.Mode == "save")
                {
                    try
                    {
                        string accountHeads = list.majorCode + list.subMajorCode + list.minorCode + list.subCode;
                        list.accountHeadsName = accountHeads;
                        Helper.ExecuteService("loan", "saveLoneType", list);

                        return Json("Loan Type Saved", JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    try
                    {
                        string accountHeads = list.majorCode + list.subMajorCode + list.minorCode + list.subCode;
                        list.accountHeadsName = accountHeads;
                        Helper.ExecuteService("loan", "updateLoanType", list);
                        return Json("Loan Type Update", JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                ModelState.AddModelError("sdasdasdas", "");
            } return null;
        }

        #endregion save and update loan Type

        #endregion Loan Type

        #region save loanee

        public ActionResult saveLoanee(showMemberDetails _showMemberDetails)
        {
            loaneeDetails _lDetails = new loaneeDetails();
            _lDetails.address = _showMemberDetails._mMemberAccountDetails.MemberAddress;
            _lDetails.bankAccountNumber = _showMemberDetails._mMemberAccountDetails.AccountNo.ToString();
            _lDetails.bankName = _showMemberDetails._mMemberAccountDetails.BankName;
            _lDetails.constituencyCode = _showMemberDetails._mMemberAccountDetails.constituencyCode;
            _lDetails.constituencyName = _showMemberDetails._mMemberAccountDetails.ConstituencyName;
            _lDetails.designationID = _showMemberDetails._designation;
            _lDetails.ifscCode = _showMemberDetails._mMemberAccountDetails.IFSCCode;
            _lDetails.memberCode = _showMemberDetails._mMemberAccountDetails.MemberCode;
            _lDetails.memberName = _showMemberDetails._mMemberAccountDetails.MemberName;
            _lDetails.mobile = _showMemberDetails._mMemberAccountDetails.MobileNumber;
            _lDetails.loaneeID = 1;
            long _lastLoaneeID = (long)Helper.ExecuteService("loan", "getLastLoaneeNo", null);
            if (_lastLoaneeID > 0)
            {
                _lDetails.loaneeID = ++_lastLoaneeID;
            }

            try
            {
                Helper.ExecuteService("loan", "saveLoanee", _lDetails);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                // return Json("
                // <script>Loanee ID is " + ex.Message + "</script>
                // ", JsonRequestBehavior.AllowGet);
            }

            // return Json("
            // <script>Loanee ID is " + _lastLoaneeID++ + "</script>
            // ", JsonRequestBehavior.AllowGet);
            return PartialView("_messageModal", "Loanee ID is " + _lastLoaneeID);

#pragma warning disable CS0162 // Unreachable code detected
            return null;
#pragma warning restore CS0162 // Unreachable code detected
        }

        #endregion save loanee

        #region Loan Master

        #region save and update loan

        [HttpPost]
        public ActionResult saveLoan(loanDetailsViewModal _loanDetailsViewModal, FormCollection _frmCollection)
        {
            long loanNumber = 0;
            try
            {
                loanDetails _lDetails = new loanDetails();
                _lDetails.loanAccountNumber = _loanDetailsViewModal._loanDetails.loanAccountNumber;
                _lDetails.loanAmount = _loanDetailsViewModal._loanDetails.loanAmount;
                _lDetails.loanInterestRate = _loanDetailsViewModal._loanDetails.loanInterestRate;
                _lDetails.loanSanctionDate = _loanDetailsViewModal._loanDetails.loanSanctionDate;
                _lDetails.loanTypeID = _loanDetailsViewModal._loanDetails.loanTypeID;
                _lDetails.totalNoofEMI = _loanDetailsViewModal._loanDetails.totalNoofEMI;
                _lDetails.loaneeID = _loanDetailsViewModal._loandeeetails.loaneeID;
                _lDetails.loanNumber = 1;
                _lDetails.loanStatus = true;
                _lDetails.submitBy = 1;
                _lDetails.submitDate = DateTime.Now;
                _lDetails.releaseDate = DateTime.Now.AddMonths(_loanDetailsViewModal._loanDetails.totalNoofEMI);
                _lDetails.lastEditDate = DateTime.Now;
                int count = int.Parse(_frmCollection["tcount"]);
                List<disbursementDetails> _lst = new List<disbursementDetails>();
                for (int i = 0; i < count; i++)
                {
                    disbursementDetails _pts = new disbursementDetails();
                    _pts.disbursmentID = 0;
                    string str = _frmCollection["span_1_" + i];
                    _pts.disburmentDate = DateTime.Parse(_frmCollection["span_1_" + i], CultureInfo.GetCultureInfo("en-IN"));

                    _pts.disburmentAmount = double.Parse(_frmCollection["span_2_" + i]);
                    _lst.Add(_pts);
                }
                _lDetails.disbursementDetails = _lst;
                loanNumber = (long)Helper.ExecuteService("loan", "saveLoan", _lDetails);
            }
            catch
            {
            }
            return Json(loanNumber, JsonRequestBehavior.AllowGet);
        }
        public ActionResult updateLoan(loanDetailsViewModal _loanDetailsViewModal, FormCollection _frmCollection)
        {

            Helper.ExecuteService("loan", "updateLoanDetails", _loanDetailsViewModal._loanDetails);

            return Json("True", JsonRequestBehavior.AllowGet);
        }
        public ActionResult updateDisbursement(disbursmentList list)
        {
            Helper.ExecuteService("loan", "updateDisbursement", list);
            return View();
        }
        #endregion save and update loan

        #region Fill Search Criteria

        public JsonResult fillDesignation()
        {
            List<FillDesignation> list = (List<FillDesignation>)Helper.ExecuteService("loan", "getDesignationList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult fillCurrentMember()
        {
            List<fillCurrentMamber> list = (List<fillCurrentMamber>)Helper.ExecuteService("loan", "getCurrentMemberList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult fillXMember()
        {
            List<fillCurrentMamber> list = (List<fillCurrentMamber>)Helper.ExecuteService("loan", "getXMemberList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult fillLoaneeList()
        {
            List<fillLoanee> list = (List<fillLoanee>)Helper.ExecuteService("loan", "getLoaneeCodeList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult fillLoanNumberList()
        {
            List<fillLoanee> list = (List<fillLoanee>)Helper.ExecuteService("loan", "getLoanNumberList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult fillLoanAccountList()
        {
            List<fillLoanAccountList> list = (List<fillLoanAccountList>)Helper.ExecuteService("loan", "getLoanAccountList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult searchResult(string searchcriteria, String searchcriteriavalue)
        {
            if (int.Parse(searchcriteria) == 0)
            {
                return RedirectToAction("getDefaultresult");
            }
            else
            {
                if (int.Parse(searchcriteria) == 1)
                {
                    List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getMemberListByDesignation", searchcriteriavalue);
                    return PartialView("_searchResult", list);
                }
                if (int.Parse(searchcriteria) == 2)
                {
                    if (int.Parse(searchcriteriavalue) == 0)
                    {
                        List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getCurrentMemberLoanList", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                    else
                    {
                        List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getMemberListByID", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                }
                if (int.Parse(searchcriteria) == 3)
                {
                    if (int.Parse(searchcriteriavalue) == 0)
                    {
                        List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getXMemberLoanList", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                    else
                    {
                        List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getMemberListByID", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                }
                if (int.Parse(searchcriteria) == 4)
                {
                    if (int.Parse(searchcriteriavalue) == 0)
                    {
                        List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getMemberListByLoaneeID", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                    else
                    {
                        List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getMemberByLoaneeID", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                }
                if (int.Parse(searchcriteria) == 5)
                {
                    if (int.Parse(searchcriteriavalue) == 0)
                    {
                        List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getMemberListByLoanNo", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                    else
                    {
                        List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getMemberByLoanNo", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                }
                if (int.Parse(searchcriteria) == 6)
                {
                    int c = 0;
                    if (int.TryParse(searchcriteriavalue, out c))
                    {
                        List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getMemberListByLoanAccountNo", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                    else
                    {
                        List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getMemberByLoanAccountNo", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                }
                List<loanSearchResult> list1 = new List<loanSearchResult>();
                loanSearchResult src = new loanSearchResult();
                src.loaneeCode = 1;
                src.memberName = "i m  search result";
                list1.Add(src);
                return PartialView("_searchResult", list1);
            }
        }

        #endregion Fill Search Criteria

        public ActionResult loanMaster()
        {
            List<loanSearchResult> list = new List<loanSearchResult>();
            return View(list);
        }
        public ActionResult loanMasterPartial()
        {
            List<loanSearchResult> list = new List<loanSearchResult>();
            return PartialView("_loanMaster", list);
        }

        public ActionResult getDefaultresult()
        {
            List<loanSearchResult> list = (List<loanSearchResult>)Helper.ExecuteService("loan", "getMemberListByLoanNo", null);
            return PartialView("_searchResult", list);
        }

        public ActionResult getLoanDetailsByID(string memberID)
        {
            loanDetailsViewModal _list = new loanDetailsViewModal();

            showMemberDetails _showMemberDetails = new showMemberDetails();
            mMemberAccountsDetails _mMemberAccountDetails = (mMemberAccountsDetails)Helper.ExecuteService("loan", "GetMemberAccountDetailsById", memberID);
            _showMemberDetails._mMemberAccountDetails = _mMemberAccountDetails;
            _showMemberDetails._loanList = null;
            _showMemberDetails._designation = Helper.ExecuteService("salaryheads", "getDesignation", memberID) as string;
            _list._showMemberDetails = _showMemberDetails;

            loaneeDetails _loaneeDetails = (loaneeDetails)Helper.ExecuteService("loan", "getLoaneeDetailsByID", memberID);
            _list._loandeeetails = _loaneeDetails;

            List<loanDetails> _loanList = (List<loanDetails>)Helper.ExecuteService("loan", "activeLoanList", memberID);
            _list._showMemberDetails._loanList = _loanList;
            return PartialView("_continueToLoan", _list);
        }

        public ActionResult getMemberDetails(string memberID)
        {
            showMemberDetails list = new showMemberDetails();
            mMemberAccountsDetails _mMemberAccountDetails = (mMemberAccountsDetails)Helper.ExecuteService("loan", "GetMemberAccountDetailsById", memberID);
            list._mMemberAccountDetails = _mMemberAccountDetails;
            list._loanList = null;
            list._designation = Helper.ExecuteService("salaryheads", "getDesignation", memberID) as string;
            List<loanDetails> _loanList = (List<loanDetails>)Helper.ExecuteService("loan", "activeLoanList", memberID);
            list._loanList = _loanList;
            return PartialView("_loaneeSave", list);
        }

        public ActionResult getMemberDetailsByID(string memberID)
        {
            mMemberAccountsDetails _mMemberAccountDetails = (mMemberAccountsDetails)Helper.ExecuteService("loan", "GetMemberAccountDetailsById", memberID);
            showMemberDetails list = new showMemberDetails();
            list._mMemberAccountDetails = _mMemberAccountDetails;
            list._loanList = null;
            list._designation = Helper.ExecuteService("salaryheads", "getDesignation", memberID) as string;
            List<loanDetails> _loanList = (List<loanDetails>)Helper.ExecuteService("loan", "activeLoanList", memberID);
            list._loanList = _loanList;
            return PartialView("_memberDetails", list);
        }
        public ActionResult manageLoan()
        {
            return View();
        }
        public ActionResult manageLoanPartial()
        {
            return PartialView("_manageLoan");
        }
        public ActionResult getActiveLoanListForManage()
        {
            List<loanSearchResult> _loanSearchResult = (List<loanSearchResult>)Helper.ExecuteService("loan", "getActiveLoanList", null);
            return PartialView("_activeLoanListForManage", _loanSearchResult);
        }
        public ActionResult getLoanDetailsByIDforManage(string LoanNumber)
        {
            installmentDetails inst = (installmentDetails)Helper.ExecuteService("loan", "getLoanDetailsByLoanID", LoanNumber);

            loanDetailsViewModal _list = new loanDetailsViewModal();

            showMemberDetails _showMemberDetails = new showMemberDetails();
            mMemberAccountsDetails _mMemberAccountDetails = (mMemberAccountsDetails)Helper.ExecuteService("loan", "GetMemberAccountDetailsById", inst.memberCode);
            _showMemberDetails._mMemberAccountDetails = _mMemberAccountDetails;
            _showMemberDetails._loanList = null;
            _showMemberDetails._designation = Helper.ExecuteService("salaryheads", "getDesignation", inst.memberCode) as string;
            _list._showMemberDetails = _showMemberDetails;

            loaneeDetails _loaneeDetails = (loaneeDetails)Helper.ExecuteService("loan", "getLoaneeDetailsByID", inst.memberCode);
            _list._loandeeetails = _loaneeDetails;

            List<loanDetails> _loanList = (List<loanDetails>)Helper.ExecuteService("loan", "activeLoanList", inst.memberCode);
            _list._showMemberDetails._loanList = _loanList;
            loanDetails ldtls = new loanDetails();
            ldtls.disbursementDetails = (List<disbursementDetails>)Helper.ExecuteService("loan", "getDisburmentListByID", LoanNumber);
            ldtls.loanAccountNumber = inst.loanAccountNumber;
            ldtls.loanAmount = inst.loanAmount;
            ldtls.loanInterestRate = inst.intRate;
            ldtls.loanNumber = inst.loanNumber;
            ldtls.loanSanctionDate = inst.sancationDate;
            ldtls.loanTypeID = inst.loantypeID;
            ldtls.totalNoofEMI = inst.noOFEMI;
            _list._loanDetails = ldtls;

            return PartialView("_updateLoanDetails", _list);
        }
        public ActionResult getLoanDetailsByIDforManageDis(string LoanNumber)
        {
            List<disbursementDetails> list = (List<disbursementDetails>)Helper.ExecuteService("loan", "getDisburmentListByID", LoanNumber);
            disbursmentList dlist = new disbursmentList();
            dlist.disbursmentListofMember = list;
            installmentDetails ilist = (installmentDetails)Helper.ExecuteService("loan", "getLoanDetailsByLoanID", LoanNumber);
            dlist.loanNumber = ilist.loanNumber;
            dlist.loanTypeID = ilist.loantypeID;
            dlist.loanTypeName = ilist.loanType;
            dlist.Name = ilist.memberName;
            dlist.totalLoanAmount = ilist.loanAmount;

            return PartialView("_updateDisbursementList", dlist);
        }
        #endregion Loan Master

        #region loan recovery
        public ActionResult getLoanDetailsByLoanID(string loanID)
        {
            installmentDetails list = (installmentDetails)Helper.ExecuteService("loan", "getLoanDetailsByLoanID", loanID);
            return PartialView("_updateLoanInstallment", list);
        }
        public ActionResult updateLoanInstallment(installmentDetails ins)
        {
            Helper.ExecuteService("loan", "updateLoanInstallment", ins);
            return Json(ins.loanNumber, JsonRequestBehavior.AllowGet);
        }
        public ActionResult loanRecovery()
        {
            return View();
        }
        public ActionResult loanRecoveryPartial()
        {
            return PartialView("_loanRecovery");
        }
        public ActionResult getActiveLoanList()
        {
            List<loanSearchResult> _loanSearchResult = (List<loanSearchResult>)Helper.ExecuteService("loan", "getActiveLoanList", null);
            return PartialView("_currentActiveLoanList", _loanSearchResult);
        }
        #endregion loan recovery

        #region disbursement Details

        public ActionResult Disbursement()
        {
            return View();
        }
        public ActionResult DisbursementPartial()
        {
            return PartialView("_Disbursement");
        }
        public ActionResult getDisbursementList(string from, string to)
        {
            List<disbursmentList> list = (List<disbursmentList>)Helper.ExecuteService("loan", "getDisburmentList", new string[] { from, to });
            return PartialView("_comingDisbursementList", list);
        }

        public ActionResult changeStatusofDisbursement(string status, string id)
        {
            Helper.ExecuteService("loan", "changeStatusofDisbursement", new string[] { status, id });
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion disbursement Details

        #region stateMent

        public ActionResult loanStatement()
        {
            return View();
        }
        public ActionResult loanStatementPartial()
        {
            return PartialView("_loanStatement");
        }
        public ActionResult getActiveLoanListforStatement(string id)
        {
            List<loanSearchResult> _loanSearchResult = (List<loanSearchResult>)Helper.ExecuteService("loan", "getActiveLoanList", null);
            return PartialView("_currentActiveLoanListforStatement", _loanSearchResult);
        }
        public ActionResult statementforLoan(string loanID)
        {
            LoanStatement list = (LoanStatement)Helper.ExecuteService("loan", "statementforLoan", loanID);

            return PartialView("_statement", list);
        }
        public ActionResult getLoanInstallmentDetails(string autoid)
        {
            installmentDetails list = (installmentDetails)Helper.ExecuteService("loan", "getLoanDetailsByInstAutoID", autoid);
            list.AutoID = Convert.ToInt64(autoid);
            return PartialView("_updateLoanInstallment", list);
        }
        public ActionResult deleteLoanInstallment(string autoid)
        {
            long loannumber = (long)Helper.ExecuteService("loan", "deleteLoanInst", autoid);
            LoanStatement list = (LoanStatement)Helper.ExecuteService("loan", "statementforLoan", loannumber);
            return PartialView("_statement", list);

        }
        public ActionResult updateLoanInst(installmentDetails inst)
        {
            try
            {
                Helper.ExecuteService("loan", "updateLoanInst", inst);

            }
            catch (Exception ex)
            {
                return Content(inst.AutoID + ' ' + ex.InnerException.Message);
            }
            //return View();
            LoanStatement list = (LoanStatement)Helper.ExecuteService("loan", "statementforLoan", inst.loanNumber);
            return PartialView("_statement", list);
        }

        public ActionResult generateLonaPdf(string LoaNNumber, string download)
        {

            LoanStatement list = (LoanStatement)Helper.ExecuteService("loan", "statementforLoan", LoaNNumber);
            loanDetailsViewModal view = new loanDetailsViewModal();
            installmentDetails ilist = (installmentDetails)Helper.ExecuteService("loan", "getLoanDetailsByLoanID", LoaNNumber);
            List<disbursementDetails> dslit = (List<disbursementDetails>)Helper.ExecuteService("loan", "getDisburmentListByID", LoaNNumber);
            view._disbursmentslist = dslit;
            string msg = string.Empty;
            int count = 0, counta = 0;
            double prec = 0, irec = 0, ipaid = 0, outi = 0;
            MemoryStream output = new MemoryStream();

            string outXml = "<html><body  style=\"font-family:DVOT-Yogesh; font-size: 16px;margin-right:15px;margin-left: 15px;\"><div><div  style=\"width: 100%;\">";
            outXml += "<table  style=\"width: 100%;font-size: 16px;\">";
            outXml += "<tr><td  style=\"text-align: center;\" colspan=\"8\"><b>Establishment of HIMACHAL PRADESH VIDHAN SABHA SHIMLA-171004.</b></td>";
            outXml += "</tr>";
            outXml += "<tr>";
            outXml += "<td colspan=\"8\"  style=\"text-align: center;\"><b>" + ilist.loanType + " Loan Recovery Statement</b></td>";
            outXml += "</tr>";
            outXml += "<tr>";
            outXml += "<td colspan=\"2\"></td>";
            outXml += "<td><b>Name : " + ilist.memberName + " </b></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "</tr>";
            outXml += "<tr>";
            outXml += "<td colspan=\"2\"></td>";
            outXml += "<td><b>Address : " + ilist.Address + "</b></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "</tr>";
            outXml += "<tr><td colspan=\"3\"><b>Total Loan Amount - " + ilist.loanAmount + ".00</b></td><td colspan=\"2\"><b>Installment Fixed - " + ilist.noOFEMI + "</b></td><td colspan=\"1\"><b>Loan Account Number - " + ilist.loanAccountNumber + "</b></td><td></td></tr>";

            outXml += "<tr><td colspan=\"8\"><hr /></td></tr>";
            outXml += "<tr ><td colspan=\"8\" align=\"center\"><table  style=\" width:60%;font-size: 16px;\">";

            outXml += "<tr><td><b>Installment Number </b></td><td><b> Installment Amount</b></td><td><b>Disbursment Date</b></td><td><b>Sanctioning Date</b></td></b></tr>";

            foreach (var critem in view._disbursmentslist)
            {
                DateTime dt = Convert.ToDateTime(critem.disburmentDate);
                string[] strDateTime = dt.ToString("dd/MM/yyyy").Split('/');
                string DisDate = strDateTime[0] + "/" + strDateTime[1] + "/" + strDateTime[2];

                //DateTime dt1 = list.senctionDate);
                // string[] strDateTime1 = dt1.ToString("dd/MM/yyyy").Split('/');
                string SecDate = list.senctionDate;



                count++;
                if (critem.disburmentRecDate == null)
                {
                    outXml += "<tr><td><b>" + count + "</b></td><td><b>" + critem.disburmentAmount + ".00 </b></td><td><b>" + DisDate + "</b></td><td><b>" + SecDate + "</b></td></tr>";

                }
                else
                {
                    string disbursementdaterec = Convert.ToString(critem.disburmentRecDate);
                    if (disbursementdaterec != null && disbursementdaterec != "")
                    {
                        disbursementdaterec = Convert.ToDateTime(disbursementdaterec).ToString("dd/MM/yyyy");
                    }

                    outXml += "<tr><td><b>" + count + "</b></td><td><b>" + critem.disburmentAmount + ".00 </b></td><td><b>" + disbursementdaterec + "</b></td><td><b>" + list.senctionDate + "</b></td></tr>";

                }
            }

            outXml += "</table></td></tr><tr><td colspan=\"8\"><hr /></td></tr><tr><td colspan=\"8\"  style=\"vertical-align: top;\"><table border=\"1\" cellpadding=\"2\"  style=\"border-collapse:collapse; width: 100%;font-size: 16px;\">";
            //  outXml += "<tr><td>SNo</td><td>Op. Bal.(?)</td><td>Payment Date</td><td>Pri. Rec.(?) </td><td>Int. Paid(?)</td><td>Int. Rate(%)</td><td>Int. for Month(?)</td><td>Out. Int.(?)</td><td>Cl Bal.(?)</td><td>Payment Details</td> </tr>";
            //outXml += "<tr><td>SNo</td><td>Payment Date(?)</td><td>Op. Bal.</td><td>Pri. Rec.(?) </td><td>Total Pri. Rec.(?)</td><td>Int. Paid(?)</td><td>Int. Rate(%)</td><td>Int. for Month(?)</td><td>Out. Int.(?)</td><td>Cl Bal.(?)</td><td>Payment Details</td> <td>Try Name</td> <td>DDO</td> </tr>";
            outXml += "<tr><td>SNo</td><td>Payment Date</td><td>Op. Bal.(₹)</td><td>Pri. Rec.(₹) </td><td>Total Pri. Rec.(₹)</td><td>Int. Paid(₹)</td><td>Total Int. Paid(₹)</td><td>Int. Rate (%)</td><td>Int. for Month(₹)</td><td>Out. Int.(₹)</td><td>Cl Bal.(₹)</td><td style=\"word-wrap:break-word;\">Pay. Details</td> <td>Try Name</td> <td>DDO</td> </tr>";

            // <td> Voucher No-Date</td> <td>Try Name</td> <td>DDO</td>
            foreach (var dritem in list.statement)
            {

                counta++;
                double Currprecrec;
                Currprecrec = dritem.pAmount + prec;
                double Currintrec;
                Currintrec = dritem.iAmount + irec;
                string date = dritem.Date;
                if (date.Contains("-"))
                {
                    date = date.Replace(" ", "");
                }

                string paymode = dritem.PaymentMode;
                if (paymode.Contains("-"))
                {
                    paymode = paymode.Replace("-", " ");
                }

                outXml += "<tr><td>" + counta + "</td><td>" + date + "</td><td>" + dritem.openingBalance + "</td><td>" + dritem.pAmount + " </td><td>" + Currprecrec + " </td><td>" + dritem.iAmount + "</td><td>" + Currintrec + "</td><td>" + dritem.intsRate + "</td><td>" + dritem.interest + "</td><td>" + dritem.outstandingint + "</td><td>" + dritem.closingBalance + " </td><td style=\"word-wrap:break-word;\">" + paymode + "</td> <td>" + dritem.TreasuryName + "</td> <td>" + dritem.DDOCode + "</td></tr>";

                // outXml += "<tr><td>" + counta + "</td><td>" + dritem.Date + "</td><td>" + dritem.openingBalance + "</td><td>" + dritem.pAmount + " </td><td>" + Currprecrec + " </td><td>" + dritem.iAmount + "</td><td>" + Currintrec + "</td><td>" + dritem.intsRate + "</td><td>" + dritem.interest + "</td><td>" + dritem.outstandingint + "</td><td>" + dritem.closingBalance + " </td><td style=\"word-wrap:break-word;\">" + dritem.PaymentMode + "</td> <td>" + dritem.TreasuryName + "</td> <td>" + dritem.DDOCode + "</td></tr>";

                // outXml += "<tr><td>" + counta + "</td><td>" + dritem.Date + "</td><td>" + dritem.openingBalance + "</td><td>" + dritem.pAmount + " </td><td>" + Currprecrec + " </td><td>" + dritem.iAmount + "</td><td>" + dritem.intsRate + "</td><td>" + dritem.interest + "</td><td>" + dritem.outstandingint + "</td><td>" + dritem.closingBalance + " </td><td>" + dritem.PaymentMode + "</td> <td>" + dritem.TreasuryName + "</td> <td>" + dritem.DDOCode + "</td></tr>";
                // <td>" + dritem.Voucher + "</td> <td>" + dritem.TreasuryName + "</td> <td>" + dritem.DDOCode + "</td>
                prec += dritem.pAmount;
                irec += dritem.iAmount;
                ipaid += dritem.interest;
                outi = dritem.outstandingint;
            }
            outXml += "<tr><td colspan=\"3\"><b> Grand Total</b></td><td><b>" + prec + "</b></td><td></td><td><b>" + irec + " </b></td><td><td></td></td><td><b>" + ipaid + "</b></td><td><b>" + outi + "</b></td><td colspan=\"5\"></td></b></tr>";

            // outXml += "<tr><td colspan=\"3\">Grand Total</td><td>" + prec + "</td><td></td><td>" + irec + " </td><td></td><td>" + ipaid + "</td><td>" + outi + "</td><td colspan=\"5\"></td></tr>";
            outXml += "</table></td></tr></table>";
            outXml += @"</div></div></body></html>";
            string htmlStringToConvert = outXml;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;

            pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;

            TextElement footerTextElement = new TextElement(0, 30, "Page: &p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;


            TextElement footerTextElement3 = new TextElement(-20, 30, "Disbursement Officer",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement3.TextAlign = HorizontalTextAlign.Right;


            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            //  pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            //   stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            string url = "/LoanStatement/12/" + list.memberName.Trim() + "/" + list.LoaNNumber + "/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), list.LoaNNumber + "loanStatement.pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();
            if (download.Equals("EXCEL"))
            {
                outXml = HttpUtility.UrlDecode(outXml);
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment;filename=loanStatement.xls");
                Response.Charset = "";
                Response.ContentType = "application/excel";
                Response.Write(outXml);
                Response.Flush();
            }
            else
            {
                DownloadPDF(url);
            }
            // Response.End();



            return new EmptyResult();

        }



        public ActionResult generateLonaPdfForAG(string LoaNNumber, string download)
        {

            LoanStatement list = (LoanStatement)Helper.ExecuteService("loan", "statementforLoan", LoaNNumber);
            loanDetailsViewModal view = new loanDetailsViewModal();
            installmentDetails ilist = (installmentDetails)Helper.ExecuteService("loan", "getLoanDetailsByLoanID", LoaNNumber);
            List<disbursementDetails> dslit = (List<disbursementDetails>)Helper.ExecuteService("loan", "getDisburmentListByID", LoaNNumber);
            view._disbursmentslist = dslit;
            string msg = string.Empty;
            int count = 0, counta = 0;
            double prec = 0, irec = 0, ipaid = 0, outi = 0;
            MemoryStream output = new MemoryStream();

            string outXml = "<html><body  style=\"font-family:DVOT-Yogesh; font-size: 12px;margin-right:25px;margin-left: 25px;\"><div><div  style=\"width: 100%;\">";
            outXml += "<table  style=\"width: 100%;font-size: 12px;\">";
            outXml += "<tr><td  style=\"text-align: center;\" colspan=\"8\"><b>Establishment of HIMACHAL PRADESH VIDHAN SABHA SHIMLA-171004.</b></td>";
            outXml += "</tr>";
            outXml += "<tr>";
            outXml += "<td colspan=\"8\"  style=\"text-align: center;\">" + ilist.loanType + " Loan Recovery Statement</td>";
            outXml += "</tr>";
            outXml += "<tr>";
            outXml += "<td colspan=\"2\"></td>";
            outXml += "<td>Name : " + ilist.memberName + " </td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "</tr>";
            outXml += "<tr>";
            outXml += "<td colspan=\"2\"></td>";
            outXml += "<td>Address</td>";
            outXml += "<td>" + ilist.Address + " </td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "</tr>";
            outXml += "<tr><td colspan=\"3\">Total Loan Amount - " + ilist.loanAmount + ".00</td><td>Installment Fixed - " + ilist.noOFEMI + "</td><td>Loan Account Number - " + ilist.loanAccountNumber + "</td><td></td></tr>";

            outXml += "<tr><td colspan=\"8\"><hr /></td></tr>";
            outXml += "<tr ><td colspan=\"8\" align=\"center\"><table  style=\" width:60%;font-size: 12px;\">";

            outXml += "<tr><td>Installment Number </td><td> Installment Amount</td><td>Disbursment Date</td><td>Sanctioning Date</td></tr>";

            foreach (var critem in view._disbursmentslist)
            {

                count++;
                if (critem.disburmentRecDate == null)
                {
                    outXml += "<tr><td>" + count + "</td><td>" + critem.disburmentAmount + ".00</td><td>" + critem.disburmentDate.ToString("dd/MM/yyyy") + "</td><td>" + Convert.ToDateTime(list.senctionDate).ToString("dd/MM/yyyy") + "</td></tr>";

                }
                else
                {
                    string disbursementdaterec = Convert.ToString(critem.disburmentRecDate);
                    if (disbursementdaterec != null && disbursementdaterec != "")
                    {
                        disbursementdaterec = Convert.ToDateTime(disbursementdaterec).ToString("dd/MM/yyyy");
                    }

                    outXml += "<tr><td>" + count + "</td><td>" + critem.disburmentAmount + ".00</td><td>" + disbursementdaterec + "</td><td>" + list.senctionDate + "</td></tr>";

                }
            }

            outXml += "</table></td></tr><tr><td colspan=\"8\"><hr /></td></tr><tr><td colspan=\"8\"  style=\"vertical-align: top;\"><table border=\"1\" cellpadding=\"5\"  style=\"border-collapse:collapse; width: 100%;font-size: 12px;\">";
            //  outXml += "<tr><td>SNo</td><td>Op. Bal.(₹)</td><td>Payment Date</td><td>Pri. Rec.(₹) </td><td>Int. Paid(₹)</td><td>Int. Rate(%)</td><td>Int. for Month(₹)</td><td>Out. Int.(₹)</td><td>Cl Bal.(₹)</td><td>Payment Details</td> </tr>";
            outXml += "<tr><td>SNo</td><td>Payment Date(₹)</td><td>Op. Bal.</td><td>Pri. Rec.(₹) </td><td>Total Pri. Rec.(₹)</td><td>Cl Bal.(₹)</td><td>Payment Details</td> <td>Try Name</td> <td>DDO</td> </tr>";
            // <td> Voucher No-Date</td> <td>Try Name</td> <td>DDO</td>
            foreach (var dritem in list.statement)
            {

                counta++;
                double Currprecrec;
                Currprecrec = dritem.pAmount + prec;

                string date = dritem.Date;
                if (date.Contains("-"))
                {
                    date = date.Replace(" ", "");
                }

                string paymode = dritem.PaymentMode;
                if (paymode.Contains("-"))
                {
                    paymode = paymode.Replace("-", " ");
                }

                //  outXml += "<tr><td>" + counta + "</td><td>" + date + "</td><td>" + dritem.openingBalance + "</td><td>" + dritem.pAmount + " </td><td>" + Currprecrec + " </td><td>" + dritem.iAmount + "</td><td>" + Currintrec + "</td><td>" + dritem.intsRate + "</td><td>" + dritem.interest + "</td><td>" + dritem.outstandingint + "</td><td>" + dritem.closingBalance + " </td><td style=\"word-wrap:break-word;\">" + paymode + "</td> <td>" + dritem.TreasuryName + "</td> <td>" + dritem.DDOCode + "</td></tr>";

                outXml += "<tr><td>" + counta + "</td><td>" + date + "</td><td>" + dritem.openingBalance + "</td><td>" + dritem.pAmount + " </td><td>" + Currprecrec + " </td><td>" + dritem.closingBalance + " </td><td>" + paymode + "</td> <td>" + dritem.TreasuryName + "</td> <td>" + dritem.DDOCode + "</td></tr>";
                // <td>" + dritem.Voucher + "</td> <td>" + dritem.TreasuryName + "</td> <td>" + dritem.DDOCode + "</td>
                prec += dritem.pAmount;
                irec += dritem.iAmount;
                ipaid += dritem.interest;
                outi = dritem.outstandingint;
            }
            outXml += "<tr><td colspan=\"3\">Grand Total</td><td>" + prec + "</td><td colspan=\"5\"></td></tr>";
            outXml += "</table></td></tr></table>";
            outXml += @"</div></div></body></html>";
            string htmlStringToConvert = outXml;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;

            pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;

            TextElement footerTextElement = new TextElement(0, 30, "Page: &p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;


            TextElement footerTextElement3 = new TextElement(-20, 30, "Disbursement Officer",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement3.TextAlign = HorizontalTextAlign.Right;


            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            //  pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            //   stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            string url = "/LoanStatement/12/" + list.memberName.Trim() + "/" + list.LoaNNumber + "/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), list.LoaNNumber + "loanStatement.pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();
            if (download.Equals("EXCEL"))
            {
                outXml = HttpUtility.UrlDecode(outXml);
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment;filename=loanStatement.xls");
                Response.Charset = "";
                Response.ContentType = "application/excel";
                Response.Write(outXml);
                Response.Flush();
            }
            else
            {
                DownloadPDF(url);
            }
            // Response.End();



            return new EmptyResult();

        }
        //public ActionResult generateLonaPdf(string LoaNNumber, string download)
        //{

        //    LoanStatement list = (LoanStatement)Helper.ExecuteService("loan", "statementforLoan", LoaNNumber);
        //    loanDetailsViewModal view = new loanDetailsViewModal();
        //    installmentDetails ilist = (installmentDetails)Helper.ExecuteService("loan", "getLoanDetailsByLoanID", LoaNNumber);
        //    List<disbursementDetails> dslit = (List<disbursementDetails>)Helper.ExecuteService("loan", "getDisburmentListByID", LoaNNumber);
        //    view._disbursmentslist = dslit;
        //    string msg = string.Empty;
        //    int count = 0, counta = 0;
        //    double prec = 0, irec = 0, ipaid = 0, outi = 0;
        //    MemoryStream output = new MemoryStream();

        //    string outXml = "<html><body  style=\"font-family:DVOT-Yogesh; font-size: 12px;margin-right:25px;margin-left: 25px;\"><div><div  style=\"width: 100%;\">";
        //    outXml += "<table  style=\"width: 100%;font-size: 12px;\">";
        //    outXml += "<tr><td  style=\"text-align: center;\" colspan=\"8\"><b>Establishment of HIMACHAL PRADESH VIDHAN SABHA SHIMLA-171004.</b></td>";
        //    outXml += "</tr>";
        //    outXml += "<tr>";
        //    outXml += "<td colspan=\"8\"  style=\"text-align: center;\">" + ilist.loanType + " Loan Recovery Statement</td>";
        //    outXml += "</tr>";
        //    outXml += "<tr>";
        //    outXml += "<td colspan=\"2\"></td>";
        //    outXml += "<td>Name : " + ilist.memberName + " </td>";
        //    outXml += "<td></td>";
        //    outXml += "<td></td>";
        //    outXml += "<td></td>";
        //    outXml += "<td></td>";
        //    outXml += "<td></td>";
        //    outXml += "</tr>";
        //    outXml += "<tr>";
        //    outXml += "<td colspan=\"2\"></td>";
        //    outXml += "<td>Address</td>";
        //    outXml += "<td>" + ilist.Address + " </td>";
        //    outXml += "<td></td>";
        //    outXml += "<td></td>";
        //    outXml += "<td></td>";
        //    outXml += "<td></td>";
        //    outXml += "</tr>";
        //    outXml += "<tr><td colspan=\"3\">Total Loan Amount - " + ilist.loanAmount + ".00</td><td>Installment Fixed - " + ilist.noOFEMI + "</td><td>Loan Account Number - " + ilist.loanAccountNumber + "</td><td></td></tr>";

        //    outXml += "<tr><td colspan=\"8\"><hr /></td></tr>";
        //    outXml += "<tr ><td colspan=\"8\" align=\"center\"><table  style=\" width:60%;font-size: 12px;\">";

        //    outXml += "<tr><td>Installment Number </td><td> Installment Amount</td><td>Sanctioning Date</td></tr>";

        //    foreach (var critem in view._disbursmentslist)
        //    {
        //        count++;

        //        outXml += "<tr><td>" + count + "</td><td>" + critem.disburmentAmount + ".00</td><td>" + critem.disburmentRecDate.ToString().Split(' ').GetValue(0).ToString() + "</td></tr>";
        //    }

        //    outXml += "</table></td></tr><tr><td colspan=\"8\"><hr /></td></tr><tr><td colspan=\"8\"  style=\"vertical-align: top;\"><table border=\"1\" cellpadding=\"5\"  style=\"border-collapse:collapse; width: 100%;font-size: 12px;\">";
        //    outXml += "<tr><td>SNo</td><td>Op. Bal.(₹)</td><td>Payment Date</td><td>Pri. Rec.(₹) </td><td>Int. Paid(₹)</td><td>Int. Rate(%)</td><td>Int. for Month(₹)</td><td>Out. Int.(₹)</td><td>Cl Bal.(₹)</td> </tr>";
        //    // <td> Voucher No-Date</td> <td>Try Name</td> <td>DDO</td>
        //    foreach (var dritem in list.statement)
        //    {
        //        counta++;
        //        outXml += "<tr><td>" + counta + "</td><td>" + dritem.openingBalance + "</td><td>" + dritem.Date + "</td><td>" + dritem.pAmount + " </td><td>" + dritem.iAmount + " </td><td>" + dritem.intsRate + "</td><td>" + dritem.interest + "</td><td>" + dritem.outstandingint + "</td><td>" + dritem.closingBalance + "</td></tr>";
        //        //<td>" + dritem.Voucher + "</td> <td>" + dritem.TreasuryName + "</td> <td>" + dritem.DDOCode + "</td>
        //        prec += dritem.pAmount;
        //        irec += dritem.iAmount;
        //        ipaid += dritem.interest;
        //        outi = dritem.outstandingint;
        //    }
        //    outXml += "<tr><td colspan=\"3\">Grand Total</td><td>" + prec + "</td><td>" + irec + " </td><td></td><td>" + ipaid + "</td><td>" + outi + "</td><td colspan=\"5\"></td></tr>";
        //    outXml += "</table></td></tr></table>";
        //    outXml += @"</div></div></body></html>";
        //    string htmlStringToConvert = outXml;
        //    PdfConverter pdfConverter = new PdfConverter();
        //    pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

        //    pdfConverter.PdfDocumentOptions.ShowFooter = true;
        //    pdfConverter.PdfDocumentOptions.ShowHeader = true;
        //    pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;

        //    pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
        //    pdfConverter.PdfFooterOptions.FooterHeight = 60;

        //    TextElement footerTextElement = new TextElement(0, 30, "Page: &p; of &P;",
        //            new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //    footerTextElement.TextAlign = HorizontalTextAlign.Center;


        //    TextElement footerTextElement3 = new TextElement(-20, 30, "Disbursement Officer",
        //           new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //    footerTextElement3.TextAlign = HorizontalTextAlign.Right;


        //    pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

        //    pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

        //    string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

        //    ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
        //    // imageElement1.KeepAspectRatio = true;
        //    imageElement1.Opacity = 100;
        //    imageElement1.DestHeight = 97f;
        //    imageElement1.DestWidth = 71f;
        //    //  pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
        //    ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
        //    imageElement2.KeepAspectRatio = false;
        //    imageElement2.Opacity = 5;
        //    imageElement2.DestHeight = 284f;
        //    imageElement2.DestWidth = 388F;

        //    EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
        //    float stampWidth = float.Parse("400");
        //    float stampHeight = float.Parse("400");

        //    // Center the stamp at the top of PDF page
        //    float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
        //    float stampYLocation = 150;

        //    RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

        //    Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
        //    //   stampTemplate.AddElement(imageElement2);
        //    byte[] pdfBytes = null;

        //    try
        //    {
        //        pdfBytes = pdfDocument.Save();
        //    }
        //    finally
        //    {
        //        // close the Document to realease all the resources
        //        pdfDocument.Close();
        //    }

        //    string url = "/LoanStatement/12/" + list.memberName + "/" + list.LoaNNumber + "/";

        //    string directory = Server.MapPath(url);

        //    if (!Directory.Exists(directory))
        //    {
        //        Directory.CreateDirectory(directory);
        //    }

        //    string path = Path.Combine(Server.MapPath("~" + url), list.LoaNNumber + "loanStatement.pdf");

        //    FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
        //    System.IO.FileAccess.Write);

        //    _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

        //    // close file stream
        //    _FileStream.Close();
        //    if (download.Equals("EXCEL"))
        //    {
        //        outXml = HttpUtility.UrlDecode(outXml);
        //        Response.Clear();
        //        Response.AddHeader("content-disposition", "attachment;filename=loanStatement.xls");
        //        Response.Charset = "";
        //        Response.ContentType = "application/excel";
        //        Response.Write(outXml);
        //        Response.Flush();
        //    }
        //    else
        //    {
        //        DownloadPDF(url);
        //    }
        //    // Response.End();



        //    return new EmptyResult();

        //}
        public object DownloadPDF(string url)
        {
            try
            {
                //string url = "/QuestionList" + "/DiaryWiseStarred/" + "12/" + "7";

                string directory = Server.MapPath(url);

                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                    //zip.AddDirectoryByName("Files");

                    if (Directory.Exists(directory))
                    {
                        zip.AddDirectory(directory, "Loan Statement");
                    }
                    else
                    {
                    }

                    Response.Clear();
                    Response.BufferOutput = false;
                    string zipName = String.Format("{0}.zip", "Loan Statement");
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);

                    zip.Save(Response.OutputStream);
                    Response.End();

                    return Response;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return "No File Available!";
            }

#pragma warning disable CS0162 // Unreachable code detected
            return Response;
#pragma warning restore CS0162 // Unreachable code detected
        }

        #endregion stateMent

        #region loan challan
        public ActionResult loanChallan()
        {
            return View();
        }
        public ActionResult loanChallanPartial()
        {
            return PartialView("_loanChallan");
        }
        public ActionResult GetActiveLoanListForChallan(string from, string to)
        {
            List<loanSearchResult> _list = (List<loanSearchResult>)Helper.ExecuteService("loan", "GetActiveLoanListForChallan", null);
            return PartialView("currentActiveLoanListForChallan", _list);
        }
        public ActionResult getchallanDetailsByLoanID(String installmentID)
        {
            loanChallanDetails list = (loanChallanDetails)Helper.ExecuteService("loan", "getchallanDetailsByLoanID", installmentID);
            list.IAmountInWords = Convert.ToInt32(list.iAmount.Split('.')[0]).NumbersToWords();
            list.PAmountInWords = Convert.ToInt32(list.pAmount.Split('.')[0]).NumbersToWords();
            return PartialView("_challanDetails", list);
        }
        public ActionResult generatechallanPdf(string installmentID)
        {
            loanChallanDetails list = (loanChallanDetails)Helper.ExecuteService("loan", "getchallanDetailsByLoanID", installmentID);
            list.IAmountInWords = Convert.ToInt32(list.iAmount.Split('.')[0]).NumbersToWords();
            list.PAmountInWords = Convert.ToInt32(list.pAmount.Split('.')[0]).NumbersToWords();
            MemoryStream output = new MemoryStream();

            string outXml = "<html><body  style=\"font-family:DVOT-Yogesh; font-size: 12px;margin-right:25px;margin-left: 25px;\"><div><div  style=\"width: 100%;\">";
            outXml += "<section  style=\"width:700px;margin:auto;\"><div id=\"f1\"><h5>" + list.loanName + "A/CNo.18057</h5><p   style=\"text-align:center;\">H.P.T.R-I</p><p   style=\"text-align:center;\">CHALAN</p><div style=\"border-bottom:1px dotted #808080\"></div><br><br><div><span>Challan No</span>.................................................................<span>Challan Date</span>..............................................................</div><br/><br/><table><tr><td style=\"width:25%;text-align:right;vertical-align:top;padding-right:40px;\">Tendered</td>";
            outXml += "<td>" + list.Tenderedby + "</td></tr><tr><td style=\"width:25%;text-align:right;vertical-align:top;padding-right:40px;\">Particulars:</td><td>Repayment of " + list.loanName + " vide " + list.paymentMode + "</td></tr><tr><td style=\"width:25%;text-align:right;vertical-align:top;padding-right:40px;\">Amount:</td><td>Rs." + list.pAmount + "/-(" + list.PAmountInWords + " Only)</td></tr></table><br><br><p  style=\"text-align:right;\">(Signature of Tenderer)</p></div><div style=\"border-bottom:1px dotted #808080v id\"></div><div id=\"f2\">";
            outXml += "<p  style=\"text-align:center;\">(TO BE FILLED IN BY THE DEPARMENTAL OFFICER OF TREASURY)</p><table><tr><td style=\"width:25%;text-align:right;vertical-align:top;padding-right:40px;\">Treasury Code:</td><td>" + list.TreasuryCode + "</td></tr><tr><td style=\"width:25%;text-align:right;vertical-align:top;padding-right:40px;\">D.D.O. CODE:</td><td>" + list.DDOCODE + "<span>(On whose behalf the money.</span></td>";
            outXml += "</tr></table><br/><table style=\"width:100%;\"><tr><td style=\"\">Major Code</td><td>Sub Major</td><td style=\"\">Minor Code</td><td>Sub Code</td><td>Amount</td></tr><tr><td>" + list.CodesList[0] + "</td><td>" + list.CodesList[1] + "</td><td>" + list.CodesList[2] + "</td><td>" + list.CodesList[3] + "</td><td>" + list.pAmount + "</td></tr></table><br/><table style=\"width:50%;\"><tr><td></td></tr><tr><td style=\"\">" + list.CodesList[0] + "-</td><td>" + list.CodetextList[0] + "</td></tr>";
            outXml += "<tr><td style=\"\">" + list.CodesList[1] + "-</td><td>" + list.CodetextList[1] + "</td></tr><tr><td style=\"\">" + list.CodesList[2] + "-</td><td>" + list.CodetextList[2] + "</td></tr><tr><td style=\"\">" + list.CodesList[3] + "-</td><td>" + list.CodetextList[3] + "</td></tr></table><p  style=\"text-align:right;\"><span>(Signatureoftheofficer)</span><br/><span>(orderingthemoneytobepaidin)</span></p><div style=\"border-bottom:1px dotted #808080v id\"></div>";
            outXml += "</div><div id=\"f3\"><p  style=\"text-align:center;\">(FOR BANKING TREASURY ONLY)</p><p >ORDER TO THE BANK:<span style=\"padding-left:100px;\">“Correct Receive and Grant Receipt”</span></p><p ><span style=\"float:right;\">Date:</span><span style=\"float:right;\">(Treasury Officer)</span></p><div style=\"border-bottom:1px dotted #808080v id\"></div></div><div id=\"f4\"><p  style=\"text-align:center;\">";
            outXml += "(FOR NON BANKING TREASURIES ONLY)</p><p >Received Rs._________________<span>(in words)</span>__________________________________<br><br><br><span>Treasure rAccountant Date:</span><span style=\"float:right;\">Treasury Officer Agent</span></p></div>";

            outXml += "<div style=\"page-break-after: always; width: 100%; height: 50px;\"></div>";

            outXml += "<div id=\"f5\"><h5>" + list.loanName + " A/C No.18057</h5><p style=\"text-align: center;\">H.P.T.R-I</p><p style=\"text-align: center;\">C H A L A N </p><div style=\"border-bottom: 1px dotted #808080\"></div><br><br>";
            outXml += "<div><span>Challan No</span>................................................<span>Challan Date</span>........................................ </div><br /><br />";
            outXml += "<table><tr><td style=\"width: 25%; text-align: right; vertical-align: top; padding-right: 40px;\">Tendered</td><td>" + list.Tenderedby + " </td></tr>";
            outXml += "<tr><td style=\"width: 25%; text-align: right; vertical-align: top; padding-right: 40px;\">Particulars:</td><td>Repayment of " + list.loanName + " vide " + list.paymentMode + "</td></tr>";
            outXml += "<tr><td style=\"width: 25%; text-align: right; vertical-align: top; padding-right: 40px;\">Amount:</td><td>Rs. " + list.iAmount + "/- (" + list.IAmountInWords + " Only)</td></tr></table><br><br>";
            outXml += "<p style=\"text-align: right;\">(Signature of Tenderer) </p></div><div style=\"border-bottom: 1px dotted #808080\"></div><div id=\"f6\"><p style=\"text-align: center;\">(TO BE FILLED IN BY THE DEPARMENTAL OFFICER OF TREASURY)</p>";
            outXml += "<table><tr><td style=\"width: 25%; text-align: right; vertical-align: top; padding-right: 40px;\">Treasury Code:</td><td>" + list.TreasuryCode + "</td></tr><tr><td style=\"width: 25%; text-align: right; vertical-align: top; padding-right: 40px;\">D.D.O. CODE:</td>";
            outXml += "<td>" + list.DDOCODE + "<span>(On whose behalf the money.</span></td></tr></table><br /><table style=\"width: 100%;\"><tr><td style=\"\">Major Code </td><td>Sub Major </td><td style=\"\">Minor Code </td><td>Sub Code</td><td>Amount</td></tr><tr><td>0049 </td>";
            outXml += "<td>00</td><td>800</td><td>16</td><td>" + list.iAmount + "</td></tr></table><br /><table style=\"width: 50%;\"><tr><td></td></tr><tr><td style=\"\">0049- </td><td>Interests receipts.</td></tr><tr><td style=\"\">00- </td><td</td></tr><tr><td style=\"\">800- </td>";
            outXml += "<td>800- Other receipts </td></tr><tr><td style=\"\">16- </td><td>Interests on loans to Govt. Servant</td></tr></table><p style=\"text-align: right;\"><span>(Signature of the officer)</span><br /><span>(ordering the money to be paid in)</span> </p>";
            outXml += "<div style=\"border-bottom: 1px dotted #808080\"></div></div><div id=\"f7\"><p style=\"text-align: center;\">(FOR BANKING TREASURY ONLY)</p><p>ORDER TO THE BANK:   <span style=\"padding-left: 100px;\">“Correct Receive and Grant Receipt”</span></p>";
            outXml += "<p><span style=\"float: right;\">Date:</span> <span style=\"float: right;\">(Treasury Officer)</span></p><div style=\"border-bottom: 1px dotted #808080\"></div></div><div id=\"f8\"><p style=\"text-align: center;\">(FOR NON BANKING TREASURIES ONLY)</p>";
            outXml += "<p>Received Rs. _________________ <span>(in words) </span>__________________________________<br><br><br><span>Treasurer Accountant Date:</span> 			<span style=\"float: right;\">Treasury Officer Agent</span></p></div>";

            outXml += @" </section></div></div></body></html>";
            string htmlStringToConvert = outXml;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;

            pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;

            TextElement footerTextElement = new TextElement(0, 30, "&p;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement1.TextAlign = HorizontalTextAlign.Left;
            TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement3.TextAlign = HorizontalTextAlign.Right;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            // stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            string url = "/loanchallan/12/" + list.loanNumber + "/" + list.installmentID + "/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), list.loanNumber + list.loanName + "chalan.pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();

            DownloadPDF(url);
            return View();
        }
        #endregion loan challan

        #region financial Year Report

        public ActionResult financialYearReport()
        {
            return View();
        }
        public ActionResult financialYearReportPartial()
        {
            return PartialView("_financialYearReport");
        }
        public ActionResult getFinancialYearList()
        {
            List<financialYearList> list = new List<financialYearList>();
            for (int i = 5; i >= -5; i--)
            {
                financialYearList _list = new financialYearList();
                int CurrentYear = DateTime.Today.AddYears(i).Year;
                int PreviousYear = DateTime.Today.AddYears(i).Year - 1;
                int NextYear = DateTime.Today.AddYears(i).Year + 1;
                string PreYear = PreviousYear.ToString();
                string NexYear = NextYear.ToString();
                string CurYear = CurrentYear.ToString();
                string FinYearText = null;
                string FinYearValue = null;
                if (DateTime.Today.Month > 3)
                {
                    FinYearText = string.Format("April {0} - March {1}", CurYear, NexYear);
                    FinYearValue = string.Format("{0}-{1}", CurYear, NexYear);
                }
                else
                {
                    FinYearText = string.Format("April {0} - March {1}", PreYear, CurYear);
                    FinYearValue = string.Format("{0}-{1}", PreYear, CurYear);
                }
                _list.yearText = FinYearText;
                _list.yearValue = FinYearValue;
                list.Add(_list);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult resultForFinancialYear(string year)
        {
            string[] selectYear = year.Split('-');
            List<YearlyReport> list = (List<YearlyReport>)Helper.ExecuteService("loan", "financialYearReport", new DateTime[] { new DateTime(Convert.ToInt32(selectYear[0]), 4, 1), new DateTime(Convert.ToInt32(selectYear[1]), 3, 31) });
            return PartialView("_financialYearList", list);
        }
        public ActionResult resultForCustomFinancialYear(string year)
        {
            string[] selectYear = year.Split('-');
            List<YearlyReport> list = (List<YearlyReport>)Helper.ExecuteService("loan", "financialYearReport", new DateTime[] { new DateTime(Convert.ToInt32(selectYear[0]), 4, 1), new DateTime(Convert.ToInt32(selectYear[1]), 7, 31) });
            return PartialView("_financialYearList", list);
        }
        #endregion financial Year Report

        #region other report
        public ActionResult individualReportDateWise()
        {
            return View();
        }
        public ActionResult individualReportDateWisePartial()
        {
            return PartialView("individualReportDateWisePartial");
        }
        public ActionResult getIndividualReportDateWise(string loanNumber, string from, string to)
        {

            otherLoanReports list = (otherLoanReports)Helper.ExecuteService("loan", "individualReportDateWise", new string[] { loanNumber, from, to });
            return PartialView("_individualReportDateWise", list);
        }
        public ActionResult getAllReportDateWise(string from, string to)
        {
            DateTime date1 = new DateTime(Convert.ToInt32(from.Split('/')[2]), Convert.ToInt32(from.Split('/')[0]), Convert.ToInt32(from.Split('/')[1]));
            DateTime date2 = new DateTime(Convert.ToInt32(to.Split('/')[2]), Convert.ToInt32(to.Split('/')[0]), Convert.ToInt32(to.Split('/')[1]));

            List<YearlyReport> list = (List<YearlyReport>)Helper.ExecuteService("loan", "allReportDateWise", new string[] { from, to });
            ViewBag.FromDate = date1.ToString("dd/MM/yyyy");
            ViewBag.toDate = date2.ToString("dd/MM/yyyy");
            return PartialView("_financialYearList", list);
        }
        [ValidateInput(false)]
        public ActionResult generatePDFOFLoanReport(string PDFData)
        {
            string outXml = "<html><body style=\"font-family:DVOT-Yogesh; font-size: 12px;margin-right:25px;margin-left: 25px;\"><div><div style=\"width: 100%;\">";
            outXml += PDFData;
            outXml += @"</div></div></body></html>";
            MemoryStream output = new MemoryStream();
            string htmlStringToConvert = outXml;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;

            pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;


            TextElement footerTextElement = new TextElement(0, 30, "Page: &p; of &P;",
             new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);



            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            //   stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            string url = "/ScheduleReport/12/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), DateTime.Now.Year + "_" + DateTime.Now.Month + "loanReport.pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();

            DownloadPDF(url);
            return View();
        }
        public ActionResult CustomFinancialYearReport()
        {
            return View();
        }
        public ActionResult CustomFinancialYearReportPartial()
        {
            return PartialView("_CustomFinancialYearReport");
        }
        public ActionResult getCustomFinancialYearList()
        {
            List<financialYearList> list = new List<financialYearList>();
            for (int i = 5; i >= -5; i--)
            {
                financialYearList _list = new financialYearList();
                int CurrentYear = DateTime.Today.AddYears(i).Year;
                int PreviousYear = DateTime.Today.AddYears(i).Year - 1;
                int NextYear = DateTime.Today.AddYears(i).Year + 1;
                string PreYear = PreviousYear.ToString();
                string NexYear = NextYear.ToString();
                string CurYear = CurrentYear.ToString();
                string FinYearText = null;
                string FinYearValue = null;
                if (DateTime.Today.Month > 3)
                {
                    FinYearText = string.Format("April {0} - July {1}", CurYear, CurYear);
                    FinYearValue = string.Format("{0}-{1}", CurYear, CurYear);
                }
                else
                {
                    FinYearText = string.Format("April {0} - July {1}", PreYear, PreYear);
                    FinYearValue = string.Format("{0}-{1}", PreYear, PreYear);
                }
                _list.yearText = FinYearText;
                _list.yearValue = FinYearValue;
                list.Add(_list);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}