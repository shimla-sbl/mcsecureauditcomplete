﻿using Email.API;
using EvoPdf;
using Ionic.Zip;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.OldSalary;
using SBL.DomainModel.Models.salaryhead;
using SBL.DomainModel.Models.User;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SMS.API;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Controllers
{
    public class OldSalaryController : Controller
    {
        //
        // GET: /Accounts/OldSalary/

        public ActionResult Index()
        {
            return PartialView("_Index");
        }

        public ActionResult getBillList(int monthID, int yearID)
        {
            List<oldSalaryViewModel> _list = (List<oldSalaryViewModel>)Helper.ExecuteService("OldSalaryContext", "getSalaryDetails", string.Concat(string.Format("{0:00}", monthID), yearID));
            foreach (var item in _list)
            {
                switch (item.MlaAllowances_old.DesignationFlag)
                {
                    case "M": item.MlaAllowances_old.DesignationFlag = "MLA"; break;
                    case "S": item.MlaAllowances_old.DesignationFlag = "SPEAKER"; break;
                    case "D": item.MlaAllowances_old.DesignationFlag = "DEPUTY SPEAKER"; break;
                }
                item.totalAllowances = (Convert.ToInt32(item.MlaAllowances_old.BasicPay ?? "0") + Convert.ToInt32(item.MlaAllowances_old.ConstituencyAllowance ?? "0") + Convert.ToInt32(item.MlaAllowances_old.HaltingAllowance ?? "0") + Convert.ToInt32(item.MlaAllowances_old.OfficeAllowance ?? "0") + Convert.ToInt32(item.MlaAllowances_old.OtherAllowance ?? "0") + Convert.ToInt32(item.MlaAllowances_old.SumptuaryAllowance ?? "0") + Convert.ToInt32(item.MlaAllowances_old.TelephoneAllowance ?? "0") + Convert.ToInt32(item.MlaAllowances_old.CompensatoryAllowance ?? "0"));
                item.totalDeduction = (Convert.ToInt32(item.MlaAllowances_old.HraDeduction ?? "0") + Convert.ToInt32(item.MlaAllowances_old.MiscDeduction ?? "0") + Convert.ToInt32(item.MlaAllowances_old.OtherDeduction ?? "0") + Convert.ToInt32(item.MlaAllowances_old.ProfTax ?? "0"));
                foreach (var ded in item.MlaAdvances_old)
                {
                    item.totalDeduction += Convert.ToInt32(ded.InstallmentAmount) + Convert.ToInt32(ded.InterestAmount);
                }
                item.total = Convert.ToInt32(item.totalAllowances) - Convert.ToInt32(item.totalDeduction);

            }
            return PartialView("_SalaryList", _list);
        }

        public ActionResult generatePdfByAutoID(string ID)
        {
            ZipFile zip = new ZipFile();

            zip.AlternateEncodingUsage = ZipOption.AsNecessary;
            string[] returnResult = null;
            string filepath = string.Empty;
            string SavedPdfPath = string.Empty;

            oldSalaryViewModel s = (oldSalaryViewModel)Helper.ExecuteService("OldSalaryContext", "getsalaryBillByID", ID);
            try
            {
                if (s.MlaAllowances_old.DesignationFlag == "S" || s.MlaAllowances_old.DesignationFlag == "D")
                {
                    returnResult = GeneratePDFforSpeaker(s);
                    if (returnResult != null)
                    {
                        SavedPdfPath = returnResult[0] + s.MlaAllowances_old.MonthYear.Substring(0, 2) + "_" + s.MlaAllowances_old.MonthYear.Substring(2, 4) + "_" + s.MlaAllowances_old.MlaCode + ".pdf";
                    }                   
                }
                else
                {
                    returnResult = GeneratePDFforMembers(s);
                    if (returnResult != null)
                    {
                        SavedPdfPath = returnResult[0] + s.MlaAllowances_old.MonthYear.Substring(0, 2) + "_" + s.MlaAllowances_old.MonthYear.Substring(2, 4) + "_" + s.MlaAllowances_old.MlaCode + ".pdf";
                    }  
                }
            }
               
            catch
            { }

            //Local Server
            //return Json("http://localhost/FileStructure/" + SavedPdfPath, JsonRequestBehavior.AllowGet);
            //Remote Server
            return Json("http://secure.shimlamc.org/secureFileStructure/" + SavedPdfPath, JsonRequestBehavior.AllowGet);
        }

        public string[] GeneratePDFforSpeaker(oldSalaryViewModel qModel)
        {
            string msg = string.Empty;
            string url = string.Empty;
            decimal totalBillsAmount = 0;
            switch (qModel.MlaAllowances_old.DesignationFlag)
            {
                case "M": qModel.MlaAllowances_old.DesignationFlag = "MLA"; break;
                case "S": qModel.MlaAllowances_old.DesignationFlag = "SPEAKER"; break;
                case "D": qModel.MlaAllowances_old.DesignationFlag = "DEPUTY SPEAKER"; break;
            }
            qModel.totalAllowances = (Convert.ToInt32(qModel.MlaAllowances_old.BasicPay ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.ConstituencyAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.HaltingAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.OfficeAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.OtherAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.SumptuaryAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.TelephoneAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.CompensatoryAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.CompensatoryAllowance ?? "0"));
            qModel.totalDeduction = (Convert.ToInt32(qModel.MlaAllowances_old.HraDeduction ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.MiscDeduction ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.OtherDeduction ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.ProfTax ?? "0"));
            foreach (var ded in qModel.MlaAdvances_old)
            {
                qModel.totalDeduction += Convert.ToInt32(ded.InstallmentAmount) + Convert.ToInt32(ded.InterestAmount);
            }
            qModel.total = Convert.ToInt32(qModel.totalAllowances) - Convert.ToInt32(qModel.totalDeduction);
            MemoryStream output = new MemoryStream();
            string AllowancesText = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            string Deductions = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";

            string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

            outXml += "<table class=\"tg\"  style=\"width: 100%;\">";
            outXml += "<tr><th class=\"tg-031e\" colspan=\"8\">A.G.H.P SHIMLA PAY SLIP No : Deposit/MLA/1047-48, Dated " + new DateTime(1900, Convert.ToInt32(qModel.MlaAllowances_old.MonthYear.Substring(0, 2)), 1).ToString("MMMM").ToUpper() + " , " + qModel.MlaAllowances_old.MonthYear.Substring(2,4) + "</th></tr>";
            // outXml += "<tr><td class=\"tg-031e\" colspan=\"8\">Note: Government accepts no responsibility or any fraud or misappropriation in respect of money, cheques or drafts made over to a messenger.<br/><br/></td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"8\"><br /><br /></td></tr> ";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">NAME :   <b>" + qModel.MlaDir_old.Name.ToUpper() + "</b></td><td class=\"tg-031e\" colspan=\"4\"><b><span id=\"Designation\">" + qModel.MlaAllowances_old.DesignationFlag + "</span></b></td> </tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += "<tr><td colspan=\"3\">1. &nbsp &nbsp  Treasury Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> SML00</td><td colspan=\"3\">2. &nbsp &nbsp  Gazetted/Non Gazetted</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>G</td></tr>";
            outXml += "<tr><td colspan=\"3\">3. &nbsp &nbsp  Demand Number</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td><td colspan=\"3\">4. &nbsp &nbsp  Scheme Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>-</td></tr>";
            outXml += "<tr><td colspan=\"3\">5. &nbsp &nbsp  DDO Code</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>092</td><td colspan=\"3\">6. &nbsp &nbsp  Object Code</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td></tr>";
            outXml += "<tr><td colspan=\"3\">7. &nbsp &nbsp  Major Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>2011</td><td colspan=\"3\">8. &nbsp &nbsp  Voted Charged</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>C</td></tr>";

            outXml += "<tr><td colspan=\"3\">9. &nbsp &nbsp  Sub Major Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>02</td><td colspan=\"3\">10. &nbsp &nbsp  Plan/Non Plan</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>N</td></tr>";
            outXml += "<tr><td colspan=\"3\">11. &nbsp &nbsp  Minor Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>101</td><td colspan=\"3\"></td><td style=\"text-align: right; padding-right:60px;\"></td></tr>";
            outXml += "<tr><td colspan=\"3\">12. &nbsp &nbsp  Sub Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td><td colspan=\"3\"></td><td style=\"text-align: right; padding-right:60px;\"></td></tr>";

            outXml += "<tr><td colspan=\"8\"><hr /></td></tr>";
            outXml += "<tr><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\"> ";

            outXml += "<tr> <td style=\"width: 60%\"> Basic Pay  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.BasicPay + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Comp. Allow.  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.CompensatoryAllowance + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Const. Allow. Pay  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.ConstituencyAllowance + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Tel. Allow. Pay  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.TelephoneAllowance + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Office Allow. Pay  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.OfficeAllowance + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Other Allow.</td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.OtherAllowance + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Sumpt Allow.  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.SumptuaryAllowance + "</td></tr>";
            

            outXml += "</table></td><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\">";
            outXml += "<tr> <td style=\"width: 60%\"> Hra Deduction  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.HraDeduction + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Misc Deduction  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.MiscDeduction + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Other Deduction  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.OtherDeduction + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Prof. Tax  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.ProfTax + "</td></tr>";
            foreach (var item in qModel.MlaAdvances_old)
            {
                outXml += "<tr> <td style=\"width: 60%\"> " + item.LoanType + "(P) </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.InstallmentAmount + "</td></tr>";
                outXml += "<tr> <td style=\"width: 60%\"> " + item.LoanType + "(I) </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.InterestAmount + "</td></tr>";
            }
            outXml += " </table> </td> </tr>";
            Deductions += "</table>";

            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += " <tr><td class=\"tg-031e\"  colspan=\"2\"><b>Gross Amount</b></td><td colspan=\"2\" style=\"text-align: right;  padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalAllowances + "</b></td><td class=\"tg-031e\"colspan=\"2\"><b>Total Deductions</b></td>";

            outXml += "<td colspan=\"2\" style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalDeduction + "</b></td></tr><tr><th align=\"text-align:left;\" colspan=\"8\"><b>Net Claim Rs. " + qModel.total + "</b></th></tr><tr><th align=\"text-align:left;\"colspan=\"8\"><b>Net Claim (Words)- " + Convert.ToInt32(qModel.total).NumbersToWords() + " Only</b></th></tr>";

            //  outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Signature<br/><br></td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
            // outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            //  outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">For the Use of A.G. Officer</td><td class=\"tg-031e\" colspan=\"4\">For the Use in Treaury</td> </tr>";
            // outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Admitted Rs:</td><td class=\"tg-031e\" colspan=\"4\">Signature<br/><br></td> </tr>";
            //  outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Object Rs:</td><td class=\"tg-031e\" colspan=\"4\"></td> </tr>";
            // outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Auditor    supdt.</td><td class=\"tg-031e\" colspan=\"4\">Tresury Officer</td> </tr>";
            // outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Gazetted Officer </td><td class=\"tg-031e\" colspan=\"4\">Incorporated in Treasury Accountant</td> </tr>";
            //
            outXml += "</table>";
            // StringBuilder str = new StringBuilder();
            //str.Append(qModel.memberCode);
            //str.Append(",");
            //str.Append(qModel.monthID);
            //str.Append(",");
            //str.Append(qModel.yearId);
            //outXml += "<br/><br/><hr/>TA/DA + MR Bills<hr/>";
            //outXml += "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            //outXml += "<tr><th style=\"text-align:left;\">From Date</th><th style=\"text-align:left;\">To Date</th><th style=\"text-align:left;\">Amount</th><th style=\"text-align:left;\">Remarks</th></tr>";
            //List<mReimbursementBill> _memberRBill = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForSalary", str.ToString());
            //if (_memberRBill.Count > 0)
            //{
            //    foreach (var item in _memberRBill)
            //    {
            //        totalBillsAmount += item.GrossAmount;
            //        outXml += "<tr><td style=\"text-align:left;\">" + item.BillFromDate.Value.ToString("dd/MM/yyyy") + "</td><td style=\"text-align:left;\">" + item.BillToDate.Value.ToString("dd/MM/yyyy") + "</td><td style=\"text-align: right;width: 100px;padding-right: 100px; \"><span style=\"float:left;\">Rs. </span>" + item.GrossAmount + "</td><td>" + item.Remarks + "</td></tr>";
            //    }
            //}
            //else
            //{
            //    ;
            //    outXml += "<tr><td style=\"text-align:left;\">--</td><td style=\"text-align:left;\">--</td><td style=\"text-align: right;width: 100px;padding-right: 100px; \"><span style=\"float:left;\">Rs. </span>0</td><td>--</td></tr>";
            //}
            //outXml += "</table>";
            outXml += "<hr/><table style=\"width: 100%; vertical-align: top;\" class=\"itg\"><th style=\"text-align:left;\">Total Amount</th><th style=\"text-align: right;width: 100px;padding-right: 270px; \"><span style=\"float:left;\">Rs. </span>" + totalBillsAmount + "</th></table><hr/>";
            outXml += @"</div></div></body></html>";

            string htmlStringToConvert = outXml;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;

            // set the header height in points
            pdfConverter.PdfHeaderOptions.HeaderHeight = 20;
            pdfConverter.PdfFooterOptions.FooterHeight = 40;

            TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan v1.0.0",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement1.TextAlign = HorizontalTextAlign.Left;
            TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement3.TextAlign = HorizontalTextAlign.Right;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
            //   pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
            // pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            // stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            url = "/salaryBills/12/" + qModel.MlaAllowances_old.MonthYear.Substring(2, 4) + "/" + qModel.MlaAllowances_old.MonthYear.Substring(0, 2) + "/" + qModel.MlaAllowances_old.MlaCode + "/";
            string directory = FileSettings.SettingValue + url; ;

            //   string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(directory, qModel.MlaAllowances_old.MonthYear.Substring(0, 2) + "_" + qModel.MlaAllowances_old.MonthYear.Substring(2, 4) + "_" + qModel.MlaAllowances_old.MlaCode + ".pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();

            return new string[] { url, AllowancesText, Deductions };

        }

        public string[] GeneratePDFforMembers(oldSalaryViewModel qModel)
        {
           
            string msg = string.Empty;
            string url = string.Empty;
            MemoryStream output = new MemoryStream();
#pragma warning disable CS0219 // The variable 'totalBillsAmount' is assigned but its value is never used
            decimal totalBillsAmount = 0;
#pragma warning restore CS0219 // The variable 'totalBillsAmount' is assigned but its value is never used
            switch (qModel.MlaAllowances_old.DesignationFlag)
            {
                case "M": qModel.MlaAllowances_old.DesignationFlag = "MLA"; break;
                case "S": qModel.MlaAllowances_old.DesignationFlag = "SPEAKER"; break;
                case "D": qModel.MlaAllowances_old.DesignationFlag = "DEPUTY SPEAKER"; break;
            }
            qModel.totalAllowances = (Convert.ToInt32(qModel.MlaAllowances_old.BasicPay ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.ConstituencyAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.HaltingAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.OfficeAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.OtherAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.SumptuaryAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.TelephoneAllowance ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.CompensatoryAllowance ?? "0"));
            qModel.totalDeduction = (Convert.ToInt32(qModel.MlaAllowances_old.HraDeduction ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.MiscDeduction ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.OtherDeduction ?? "0") + Convert.ToInt32(qModel.MlaAllowances_old.ProfTax ?? "0"));
            foreach (var ded in qModel.MlaAdvances_old)
            {
                qModel.totalDeduction += Convert.ToInt32(ded.InstallmentAmount) + Convert.ToInt32(ded.InterestAmount);
            }
            qModel.total = Convert.ToInt32(qModel.totalAllowances) - Convert.ToInt32(qModel.totalDeduction);
            string AllowancesText = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            string Deductions = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

            outXml += "<table class=\"tg\"  style=\"width: 100%;\">";
            outXml += "<tr><th class=\"tg-031e\" colspan=\"8\">A.G.H.P SHIMLA PAY SLIP No : Deposit/MLA/1047-48, Dated " + new DateTime(1900, Convert.ToInt32(qModel.MlaAllowances_old.MonthYear.Substring(0, 2)), 1).ToString("MMMM").ToUpper() + " , " + qModel.MlaAllowances_old.MonthYear.Substring(2, 4) + "</th></tr>";
            // outXml += "<tr><td class=\"tg-031e\" colspan=\"8\">Note: Government accepts no responsibility or any fraud or misappropriation in respect of money, cheques or drafts made over to a messenger.<br/><br/></td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"8\"><br /><br /></td></tr> ";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">NAME:   <b>" + qModel.MlaDir_old.Name.ToUpper() + "</b></td><td class=\"tg-031e\" colspan=\"4\"><b><span id=\"Designation\">" + qModel.MlaAllowances_old.DesignationFlag.ToUpper() + "</span></b></td> </tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";

            outXml += "<tr><td colspan=\"4\"><b>Allowances</b></td><td colspan=\"4\"><b>Deductions</b></td></tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += "<tr><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\"> ";

            outXml += "<tr> <td style=\"width: 60%\"> Basic Pay  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.BasicPay + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Comp. Allow.  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.CompensatoryAllowance + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Const. Allow. Pay  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.ConstituencyAllowance + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Tel. Allow. Pay  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.TelephoneAllowance + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Office Allow. Pay  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.OfficeAllowance + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Other Allow.</td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.OtherAllowance + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Sumpt Allow.  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.SumptuaryAllowance + "</td></tr>";
            

            outXml += "</table></td><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\">";
            outXml += "<tr> <td style=\"width: 60%\"> Hra Deduction  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.HraDeduction + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Misc Deduction  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.MiscDeduction + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Other Deduction  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.OtherDeduction + "</td></tr>";
            outXml += "<tr> <td style=\"width: 60%\"> Prof. Tax  </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + qModel.MlaAllowances_old.ProfTax + "</td></tr>";
            foreach (var item in qModel.MlaAdvances_old)
            
            {
                outXml += "<tr> <td style=\"width: 60%\"> " + item.LoanType + "(P) </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.InstallmentAmount + "</td></tr>";
                outXml += "<tr> <td style=\"width: 60%\"> " + item.LoanType + "(I) </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.InterestAmount + "</td></tr>";
            }
            outXml += " </table> </td> </tr>";
            Deductions += "</table>";

            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += " <tr><td class=\"tg-031e\"  colspan=\"2\"><b>Gross Amount</b></td><td colspan=\"2\" style=\"text-align: right;  padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalAllowances + "</b></td><td class=\"tg-031e\"colspan=\"2\"><b>Total  Deductions</b></td>";

            outXml += "<td colspan=\"2\" style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalDeduction + "</b></td></tr><tr><th align=\"text-align:left;\" colspan=\"8\"><b>Net  Claim Rs. " + (qModel.total) + "</b></th></tr><tr><th align=\"text-align:left;\"colspan=\"8\"><b>Net  Claim (Words)- " + Convert.ToInt32((qModel.total)).NumbersToWords() + " Only</b></th></tr>";

            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Signature<br/><br></td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
            //outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">For the Use of A.G. Officer</td><td class=\"tg-031e\" colspan=\"4\">For the Use in Treaury</td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Admitted Rs:</td><td class=\"tg-031e\" colspan=\"4\">Signature<br/><br></td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Object Rs:</td><td class=\"tg-031e\" colspan=\"4\"></td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Auditor    supdt.</td><td class=\"tg-031e\" colspan=\"4\">Tresury Officer</td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Gazetted Officer </td><td class=\"tg-031e\" colspan=\"4\">Incorporated in Treasury Accountant</td> </tr>";

            outXml += "</table>";

            //StringBuilder str = new StringBuilder();
            //str.Append(qModel.memberCode);
            //str.Append(",");
            //str.Append(qModel.monthID);
            //str.Append(",");
            //str.Append(qModel.yearId);
            //outXml += "<br/><br/><hr/>TA/DA + MR Bills<hr/>";
            //outXml += "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            //outXml += "<tr><th style=\"text-align:left;\">From Date</th><th style=\"text-align:left;\">To Date</th><th style=\"text-align:left;\">Amount</th><th style=\"text-align:left;\">Remarks</th></tr>";
            //List<mReimbursementBill> _memberRBill = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForSalary", str.ToString());
            //if (_memberRBill.Count > 0)
            //{
            //    foreach (var item in _memberRBill)
            //    {
            //        totalBillsAmount += item.GrossAmount;
            //        outXml += "<tr><td style=\"text-align:left;\">" + item.BillFromDate.Value.ToString("dd/MM/yyyy") + "</td><td style=\"text-align:left;\">" + item.BillToDate.Value.ToString("dd/MM/yyyy") + "</td><td style=\"text-align: right;width: 100px;padding-right: 100px; \"><span style=\"float:left;\">Rs. </span>" + item.GrossAmount + "</td><td>" + item.Remarks + "</td></tr>";
            //    }
            //}
            //else
            //{
            //    outXml += "<tr><td style=\"text-align:left;\">--</td><td style=\"text-align:left;\">--</td><td style=\"text-align: right;width: 100px;padding-right: 100px; \"><span style=\"float:left;\">Rs. </span>0</td><td>--</td></tr>";
            //}
            //outXml += "</table>";

            //This line will added 
            //outXml += "<hr/><table style=\"width: 100%; vertical-align: top;\" class=\"itg\"><th style=\"text-align:left;\">Total Amount</th><th style=\"text-align: right;width: 100px;padding-right: 270px; \"><span style=\"float:left;\">Rs. </span>" + totalBillsAmount + "</th></table><hr/>";
            outXml += @"</div></div></body></html>";
           
                string htmlStringToConvert = outXml;
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 20;
                pdfConverter.PdfFooterOptions.FooterHeight = 40;

                TextElement footerTextElement = new TextElement(0, 30, "Page &p; of &P;",
                        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

                string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

                ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
                // imageElement1.KeepAspectRatio = true;
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
                ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 5;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388F;

                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
                float stampWidth = float.Parse("400");
                float stampHeight = float.Parse("400");

                // Center the stamp at the top of PDF page
                float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
                float stampYLocation = 150;

                RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

                Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
                //
                // stampTemplate.AddElement(imageElement2);
                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                url = "/salaryBills/12/" + qModel.MlaAllowances_old.MonthYear.Substring(2, 4) + "/" + qModel.MlaAllowances_old.MonthYear.Substring(0, 2) + "/" + qModel.MlaAllowances_old.MlaCode + "/";
                string directory = FileSettings.SettingValue + url; ;

                //  string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(directory, qModel.MlaAllowances_old.MonthYear.Substring(0, 2) + "_" + qModel.MlaAllowances_old.MonthYear.Substring(2, 4) + "_" + qModel.MlaAllowances_old.MlaCode + ".pdf");


                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                return new string[] { url, AllowancesText, Deductions };
            
        }
    }
}