﻿using SBL.DomainModel.Models.JobsPosting;
using SBL.DomainModel.Models.User;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Controllers
{
    public class JobPortalController : Controller
    {
        //
        // GET: /Accounts/JobPortal/

        public ActionResult Index()
        {
           
            var model = (List<Job>)Helper.ExecuteService("Job", "GetAllJob", null);
            return PartialView("/Areas/Accounts/Views/JobPortal/_Index.cshtml", model);
        }

        public ActionResult PopulateJobForm(int Id, string Action)
        {
            try
            {
                Job model = new Job();
                var State = ((List<EvidhanPost>)Helper.ExecuteService("Job", "GetAllActivePost", null)).OrderBy(m => m.PostId).ToList();
                ViewBag.PostList = new SelectList(State, "PostId", "PostName");
                List<string> PostIds = new List<string>();
            
                if (Id > 0)
                {
                    model = (Job)Helper.ExecuteService("Job", "GetJobById", new Job { JOBID = Id });
                    string PostId = model.PostIds;
                    if (PostId != null && PostId != "")
                    {
                        string[] values = PostId.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            PostIds.Add(values[i]);
                        }

                    }
                }
                model.PostIdList = PostIds;

                model.Action = Action;
                if (Action == "Add")
                    model.IsActive = true;
                return PartialView("/Areas/Accounts/Views/JobPortal/_PopulateJob.cshtml", model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }

        }

        [ValidateAntiForgeryToken]
        public PartialViewResult SaveJobs(Job model, int pageId = 1, int pageSize = 10)
        {
            try
            {
                DateTime LastDate = new DateTime(Convert.ToInt32(model.LastDateS.Split('/')[2]), Convert.ToInt32(model.LastDateS.Split('/')[1]), Convert.ToInt32(model.LastDateS.Split('/')[0]));
                model.LastDate = LastDate;

                DateTime PostingDate = new DateTime(Convert.ToInt32(model.PostingDateS.Split('/')[2]), Convert.ToInt32(model.PostingDateS.Split('/')[1]), Convert.ToInt32(model.PostingDateS.Split('/')[0]));
                model.PublishedOn = PostingDate;

                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "FileLocation" });
                string FileLocation = SiteSettingsSessn.SettingValue;
                Job Job = new Job();
                if (model.JOBID > 0)
                    Job = (Job)Helper.ExecuteService("Job", "GetJobById", new Job { JOBID = model.JOBID });
                Job = model;

                string ext = string.Empty;
                string url = FileLocation + "JobDetails/";
                string ResultUrl = FileLocation + "JobResult/";
                string SupptUrl = FileLocation + "JobSupplementary/";

                if (!System.IO.Directory.Exists(url))
                {
                    System.IO.Directory.CreateDirectory(url);
                }

                if (!System.IO.Directory.Exists(ResultUrl))
                {
                    System.IO.Directory.CreateDirectory(ResultUrl);
                }
                if (!System.IO.Directory.Exists(SupptUrl))
                {
                    System.IO.Directory.CreateDirectory(SupptUrl);
                }
                string url1 = "~/Jobs/DetailPdf";
                string directory1 = Server.MapPath(url1);

                string Suppurl1 = "~/Jobs/SupplemetaryPdf";
                string Suppdirectory1 = Server.MapPath(Suppurl1);

                if (Directory.Exists(url))
                {
                    // this is for Job Description file
                    string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                    if (savedFileName.Length > 0)
                    {
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            Guid FileName = Guid.NewGuid();

                            string name = Path.GetFileName(page);

                            string path = System.IO.Path.Combine(url, FileName + "_" + name);


                            if (!string.IsNullOrEmpty(name))
                            {
                                if (!string.IsNullOrEmpty(Job.JobDetailPdfUrl))
                                    System.IO.File.Delete(System.IO.Path.Combine(url, Job.JobDetailPdfUrl));
                                System.IO.File.Copy(SourceFile, path, true);
                                Job.JobDetailPdfUrl = FileName + "_" + name;

                            }
                            if (Directory.Exists(directory1))
                            {
                                string[] filePaths = Directory.GetFiles(directory1);
                                foreach (string filePath in filePaths)
                                {
                                    System.IO.File.Delete(filePath);
                                }
                            }

                        }
                    }


                    // this is for Job Supplemetary file
                    string[] savedSuppFileName = Directory.GetFiles(Server.MapPath(Suppurl1));
                    if (savedSuppFileName.Length > 0 && !string.IsNullOrEmpty(savedSuppFileName[0]))
                    {
                        string SuppSourceFile = savedSuppFileName[0];
                        foreach (string page in savedSuppFileName)
                        {
                            Guid FileName = Guid.NewGuid();

                            string name = Path.GetFileName(page);

                            string path = System.IO.Path.Combine(SupptUrl, FileName + "_" + name);


                            if (!string.IsNullOrEmpty(name))
                            {
                                if (!string.IsNullOrEmpty(Job.SuppInfoPdfUrl))
                                    System.IO.File.Delete(System.IO.Path.Combine(SupptUrl, Job.SuppInfoPdfUrl));
                                System.IO.File.Copy(SuppSourceFile, path, true);
                                Job.SuppInfoPdfUrl = FileName + "_" + name;

                            }
                            if (Directory.Exists(Suppdirectory1))
                            {
                                string[] filePaths = Directory.GetFiles(Suppdirectory1);
                                foreach (string filePath in filePaths)
                                {
                                    System.IO.File.Delete(filePath);
                                }
                            }

                        }
                    }
                    if (model.PostIdList != null && model.PostIdList.Count() > 0)
                    {

                        string ResultString = string.Join(",", model.PostIdList.ToArray());
                        model.PostIds = ResultString;
                    }

                    if (model.Action == "Add")
                    {
                        if (savedFileName.Length > 0)
                        {
                            ext = Path.GetExtension(Job.JobDetailPdfUrl);
                            if (ext == ".pdf")
                            {
                             
                                int JobId = (int)Helper.ExecuteService("Job", "AddJob", Job);

                                foreach (var item in Job.PostIdList)
                                {
                                    JobWithPostDetail obj = new JobWithPostDetail();
                                    obj.JobId = JobId;
                                    obj.PostId = int.Parse(item);
                                    Helper.ExecuteService("Job", "AddJobWithPost", obj);
                                }

                                ViewBag.Msg = "Sucessfully added ";
                                ViewBag.Class = "alert alert-info";
                                ViewBag.Notification = "Success";
                            }
                            else
                            {
                                ViewBag.Msg = "Only pdf file can attach ";
                                ViewBag.Class = "alert alert-danger";
                                ViewBag.Notification = "Error";
                            }
                        }
                        else
                        {
                            ViewBag.Msg = "Please select pdf file";
                            ViewBag.Class = "alert alert-danger";
                            ViewBag.Notification = "Error";
                        }

                    }
                    else
                    {
                        if (Job.JOBID > 0)
                        {
                            string url2 = "~/Jobs/ResultPdf";
                            string directory2 = Server.MapPath(url2);

                            // this is for Result
                            string[] ResultFileName = Directory.GetFiles(Server.MapPath(url2));
                            if (ResultFileName.Length > 0)
                            {
                                string ResultSourceFile = ResultFileName[0];
                                foreach (string page in ResultFileName)
                                {
                                    Guid FileName = Guid.NewGuid();

                                    string name = Path.GetFileName(page);

                                    string path = System.IO.Path.Combine(ResultUrl, FileName + "_" + name);


                                    if (!string.IsNullOrEmpty(name))
                                    {
                                        if (!string.IsNullOrEmpty(Job.ResultPdfUrl))
                                            System.IO.File.Delete(System.IO.Path.Combine(ResultUrl, Job.ResultPdfUrl));
                                        System.IO.File.Copy(ResultSourceFile, path, true);
                                        Job.ResultPdfUrl = FileName + "_" + name;

                                    }

                                }
                            }
                            if (Directory.Exists(directory2))
                            {
                                string[] filePaths = Directory.GetFiles(directory2);
                                foreach (string filePath in filePaths)
                                {
                                    System.IO.File.Delete(filePath);
                                }
                            }
                            Helper.ExecuteService("Job", "UpdateJob", Job);

                            DataSet dataSet = new DataSet();
                            var Parameter1 = new List<KeyValuePair<string, string>>();
                            Parameter1.Add(new KeyValuePair<string, string>("@JobId", Job.JOBID.ToString()));
                            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteJobWithPostDetail", Parameter1);
                            foreach (var item in Job.PostIdList)
                            {
                                JobWithPostDetail obj = new JobWithPostDetail();
                                obj.JobId = Job.JOBID;
                                obj.PostId = int.Parse(item);
                                Helper.ExecuteService("Job", "AddJobWithPost", obj);
                            }

                            ViewBag.Msg = "Sucessfully updated ";
                            ViewBag.Class = "alert alert-info";
                            ViewBag.Notification = "Success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                ViewBag.Msg = ex.InnerException;
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }
            var modelList = (List<Job>)Helper.ExecuteService("Job", "GetAllJob", null);
            return PartialView("/Areas/Accounts/Views/JobPortal/_JobList.cshtml", modelList);
        }

        #region Attach Pdf
        [HttpPost]
        public ContentResult UploadDetailsFiles()
        {
            string url = "~/Jobs/DetailPdf";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<Job>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/Jobs/DetailPdf"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new Job()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} Kb", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult RemoveDetailsPDFFiles()
        {
            string url = "~/Jobs/DetailPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ContentResult UploadJobResultFiles()
        {
            string url = "~/Jobs/ResultPdf";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<Job>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/Jobs/ResultPdf"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new Job()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} Kb", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult RemoveResultPDFFiles()
        {
            string url = "~/Jobs/ResultPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ContentResult UploadSuppFiles()
        {
            string url = "~/Jobs/SupplemetaryPdf";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<Job>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/Jobs/SupplemetaryPdf"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new Job()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} Kb", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult RemoveSuppPDFFiles()
        {
            string url = "~/Jobs/SupplemetaryPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
