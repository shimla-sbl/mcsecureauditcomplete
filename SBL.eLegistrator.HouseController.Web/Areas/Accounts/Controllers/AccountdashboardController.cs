﻿using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models.SystemModule;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Controllers
{
    public class AccountdashboardController : Controller
    {
        public ActionResult GetSystemFunctList()
        {
            //SystemModuleModel model = new SystemModuleModel();

            //model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetModuleActionList", model);

            return PartialView("testleft", null);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Test()
        {
            return View();
        }
        [HttpPost]
        public ActionResult mainaccountadmindashboard()
        {
            tPaperLaidV model = new tPaperLaidV();
            //Get the Total count of All Type of question.
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            model.SessionCode = model.SessionId = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)  
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDepartmentDashboardItemsCounter", model);

            //mUsers user = new mUsers();
            //user.UserId = new Guid(CurrentSession.UserID);
            //user.IsMember = CurrentSession.IsMember;

            //user = (mUsers)Helper.ExecuteService("User", "GetIsMemberDetails", user);
            //if (user != null)
            //{

            model.CurrentUserName = "Accounts-Admin";

            //}
            return PartialView("_maindashboard", model);
        }
    }
}
