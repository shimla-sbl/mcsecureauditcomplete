﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Pension;
using SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Controllers
{
    public class pensionController : Controller
    {
        //
        // GET: /Accounts/pension/

        public ActionResult Index()
        {
            return View();
        }

        #region pension Master

        public ActionResult pensionDepartment()
        {
            return View();
        }

        public ActionResult getXMemberList()
        {
            var memberList = (List<mMember>)Helper.ExecuteService("pension", "getXMemberList", null);
            var mmlist = memberList.ToViewModelsalarymembers();
            return PartialView("_xMemberList", mmlist);
        }

        public ActionResult getMemberPensionDetails(string memberID)
        {
            pensionerDetailsviewModel _mMemberAccountDetails = (pensionerDetailsviewModel)Helper.ExecuteService("pension", "getMemberDetailsByID", memberID);
            if (_mMemberAccountDetails.IFSCCode == null)
            {
                _mMemberAccountDetails.IFSCCode = "";
            }
            _mMemberAccountDetails.Mode = "save";
            int count = (int)Helper.ExecuteService("pension", "checkMemberList", memberID);
            if (count > 0)
            {
                _mMemberAccountDetails.Mode = "update";
            }
            return PartialView("_memberPensionDetails", _mMemberAccountDetails);
        }

        public ActionResult updatePensionerDetails(FormCollection _frmCollection, pensionerDetailsviewModel _pensionerDetailsViewModal)
        {
            if (ModelState.IsValid)
            {
                if (_pensionerDetailsViewModal.Mode == "save")
                {
                    PensionerDetails _PensionerDetails = new PensionerDetails();
                    _PensionerDetails.bankName = _pensionerDetailsViewModal.BankName;
                    DateTime DateTime1 = DateTime.Parse(_pensionerDetailsViewModal.firstTermStart.ToString());
                    DateTime DateTime2 = DateTime.Parse(_pensionerDetailsViewModal.firstTermEnd.ToString());
                    TimeSpan ts = DateTime2 - DateTime1;
                    _PensionerDetails.firstTermStrat = DateTime1;
                    _PensionerDetails.firstTermEnd = DateTime2;
                    double x = ts.TotalDays / 365.25;
                    _PensionerDetails.extrayearfT = 0;
                    int years = Convert.ToInt32(x);
                    if (years == 0)
                    {
                        years = 1;
                    }
                    if (years > 5)
                    {
                        _PensionerDetails.extrayearfT = years - 5;
                        _PensionerDetails.noOfYears = 5;
                    }

                    _PensionerDetails.isActiveMember = false;
                    _PensionerDetails.isNomneeActivate = 0;
                    _PensionerDetails.isPensioner = 1;
                    _PensionerDetails.isPensionerAlive = 1;
                    _PensionerDetails.lastAssemblyID = 0;
                    _PensionerDetails.memberCode = _pensionerDetailsViewModal.memberCode;
                    _PensionerDetails.nameOfTreasury = "";
                    _PensionerDetails.pensionerID = "P" + _pensionerDetailsViewModal.memberCode;
                    _PensionerDetails.pensionerName = _pensionerDetailsViewModal.PensionerName;
                    _PensionerDetails.pPONO = _pensionerDetailsViewModal.PPOCode;
                    Helper.ExecuteService("pension", "savePensioner", _PensionerDetails);
                    int count = int.Parse(_frmCollection["tcount"]);
                    for (int i = 0; i < count; i++)
                    {
                        pensionerTenuresDetails _pts = new pensionerTenuresDetails();
                        _pts.pensionerID = _PensionerDetails.memberCode;
                        _pts.startDate = DateTime.Parse(_frmCollection["span_1_" + i]);
                        _pts.endDate = DateTime.Parse(_frmCollection["span_2_" + i]);
                        _pts.years = int.Parse(_frmCollection["span_3_" + i]);
                        Helper.ExecuteService("pension", "savepensionerTenuresDetails", _pts);
                    }
                }
                else
                {
                    Helper.ExecuteService("pension", "deleteTenureDetails", _pensionerDetailsViewModal.memberCode);
                    int count = int.Parse(_frmCollection["tcount"]);
                    for (int i = 0; i < count; i++)
                    {
                        pensionerTenuresDetails _pts = new pensionerTenuresDetails();
                        _pts.pensionerID = _pensionerDetailsViewModal.memberCode;
                        _pts.startDate = DateTime.Parse(_frmCollection["span_1_" + i]);
                        _pts.endDate = DateTime.Parse(_frmCollection["span_2_" + i]);
                        _pts.years = int.Parse(_frmCollection["span_3_" + i]);
                        Helper.ExecuteService("pension", "savepensionerTenuresDetails", _pts);
                    }
                }
            }
            else
            {
                ModelState.AddModelError("there is some error.", "");
            }
            return View();
        }

        #endregion pension Master

        #region pension report

        public static void GetDifference(DateTime date1, DateTime date2, out int Years, out int Months, out int Weeks, out int Days)
        {
            //assumes date2 is the bigger date for simplicity

            //years
            TimeSpan diff = date2 - date1;
            Years = diff.Days / 365;
            DateTime workingDate = date1.AddYears(Years);

            while (workingDate.AddYears(1) <= date2)
            {
                workingDate = workingDate.AddYears(1);
                Years++;
            }

            //months
            diff = date2 - workingDate;
            Months = diff.Days / 31;
            workingDate = workingDate.AddMonths(Months);

            while (workingDate.AddMonths(1) <= date2)
            {
                workingDate = workingDate.AddMonths(1);
                Months++;
            }

            //weeks and days
            diff = date2 - workingDate;
            Weeks = diff.Days / 7; //weeks always have 7 days
            Days = diff.Days % 31;
            Days += 1;
        }

        public ActionResult pensionReport()
        {
            IEnumerable<PensionReport> _pr = (IEnumerable<PensionReport>)Helper.ExecuteService("pension", "pensionReport", null);
            foreach (var item in _pr)
            {
                List<string> _list = new List<string>();
                List<string> _tpList = new List<string>();
                string exty = "";
                item.extraYear = 0;
                TimeSpan ts = item.lastDate - item.firstDate;

                double x = ts.TotalDays / 365.25;

                int years = Convert.ToInt32(x);
                if (years == 0)
                {
                    years = 1;
                }
                if (years > 5)
                {
                    exty = (years - 5).ToString();

                }
                _tpList.Add("1st Term");
                _list.Add("<i class=\"fa fa-inr\"></i> " + item.pensionAmount + ".00");

                if (exty != "")
                {
                    if (Convert.ToInt32(exty) != 0)
                    {
                        item.extraYear = Convert.ToInt32(exty);
                        _tpList.Add("+" + exty + " year(s)");
                        _list.Add("<i class=\"fa fa-inr\"></i> " + Convert.ToInt32(exty) * (500) + ".00");
                        _list.Add("--");
                        _tpList.Add("Y - M - D");
                        item.totalPension += Convert.ToInt32(exty) * (500);
                    }
                    else
                    {
                        _list.Add("--");
                        _tpList.Add("Y - M - D");
                    }

                }


                item.totalPension += int.Parse(item.pensionAmount.ToString());
                foreach (var tItem in item._pensionerTenuresDetails)
                {
                    int year = 0;
                    int month = 0;
                    int days = 0;
                    int weeks = 0;
                    TimeSpan tts = tItem.endDate - tItem.startDate;

                    double tx = tts.TotalDays / 365.25;
                    int tyears = Convert.ToInt32(tx) + 1;
                    if (tyears == 0)
                    {
                        tyears = 1;
                    }
                    GetDifference(tItem.startDate, tItem.endDate, out year, out month, out weeks, out days);
                    _tpList.Add(year + " - " + month + " - " + days + "");
                    _list.Add("<i class=\"fa fa-inr\"></i> " + ((tyears) * 500).ToString() + ".00");
                    item.totalPension += double.Parse(((tyears) * 500).ToString());
                    item.lastDate = tItem.endDate;
                }
                item.tPeriodList = _tpList;
                item.AmountList = _list;
            }
            return View(_pr);
        }

        public ActionResult FamilyPensionReport()
        {
            IEnumerable<PensionReport> _pr = (IEnumerable<PensionReport>)Helper.ExecuteService("pension", "FamilypensionReport", null);
            foreach (var item in _pr)
            {
                List<string> _list = new List<string>();
                List<string> _tpList = new List<string>();
                string exty = "";
                item.extraYear = 0;
                TimeSpan ts = item.lastDate - item.firstDate;

                double x = ts.TotalDays / 365.25;

                int years = Convert.ToInt32(x);
                if (years == 0)
                {
                    years = 1;
                }
                if (years > 5)
                {
                    exty = (years - 5).ToString();

                }
                _tpList.Add("1st Term");
                _list.Add("<i class=\"fa fa-inr\"></i> " + item.pensionAmount + ".00");

                if (exty != "")
                {
                    if (Convert.ToInt32(exty) != 0)
                    {
                        item.extraYear = Convert.ToInt32(exty);
                        _tpList.Add("+" + exty + " year(s)");
                        _list.Add("<i class=\"fa fa-inr\"></i> " + Convert.ToInt32(exty) * (250) + ".00");
                        _list.Add("--");
                        _tpList.Add("Y - M - D");
                        item.totalPension += Convert.ToInt32(exty) * (250);
                    }
                    else
                    {
                        _list.Add("--");
                        _tpList.Add("Y - M - D");
                    }

                }


                item.totalPension += int.Parse(item.pensionAmount.ToString());
                foreach (var tItem in item._pensionerTenuresDetails)
                {
                    int year = 0;
                    int month = 0;
                    int days = 0;
                    int weeks = 0;
                    TimeSpan tts = tItem.endDate - tItem.startDate;

                    double tx = tts.TotalDays / 365.25;
                    int tyears = Convert.ToInt32(tx) + 1;
                    if (tyears == 0)
                    {
                        tyears = 1;
                    }
                    GetDifference(tItem.startDate, tItem.endDate, out year, out month, out weeks, out days);
                    _tpList.Add(year + " - " + month + " - " + days + "");
                    _list.Add("<i class=\"fa fa-inr\"></i> " + ((tyears) * 250).ToString() + ".00");
                    item.totalPension += double.Parse(((tyears) * 250).ToString());
                    item.lastDate = tItem.endDate;
                }
                item.tPeriodList = _tpList;
                item.AmountList = _list;
            }
            return View(_pr);
        }
        #endregion pension report
    }
}