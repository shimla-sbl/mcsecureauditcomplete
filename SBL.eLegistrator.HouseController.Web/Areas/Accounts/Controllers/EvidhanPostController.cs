﻿using SBL.DomainModel.Models.JobsPosting;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Controllers
{
    public class EvidhanPostController : Controller
    {
        //
        // GET: /Accounts/EvidhanPost/

        public ActionResult Index()
        {
            //EvidhanPost model = new EvidhanPost();
            var ActiveList = (List<EvidhanPost>)Helper.ExecuteService("Job", "GetAllPost", null);
            return View(ActiveList);
        }

        public ActionResult SavePost(string PostName, bool PostStatus, string Mode, int PostId)
        {
            try
            {
                EvidhanPost model = new EvidhanPost();
                if (PostId > 0)
                    model = (EvidhanPost)Helper.ExecuteService("Job", "GetPostById", PostId);
                model.PostName = PostName;
                model.PostStatus = PostStatus;
                if (Mode == "Edit")
                {
                    Helper.ExecuteService("Job", "UpdatePost", model);
                    ViewBag.Msg = "Sucessfully Edited Post Imformation";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
                else
                {
                    Helper.ExecuteService("Job", "AddPost", model);
                    ViewBag.Msg = "Successfully Added Post information";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
                var ActiveList = (List<EvidhanPost>)Helper.ExecuteService("Job", "GetAllPost", null);
                return PartialView("/Areas/Accounts/Views/EvidhanPost/_PostBillList.cshtml", ActiveList);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public ActionResult PopulateNewPost(int Id, string Mode)
        {
            EvidhanPost model = new EvidhanPost();
            model.Mode = Mode;
            if (Id > 0)
                model = (EvidhanPost)Helper.ExecuteService("Job", "GetPostById", Id);
            return PartialView("_Newpost", model);
        }

         
    }
}
