﻿//using Microsoft.Office.Interop.Excel;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Promotion;
using SBL.DomainModel.Models.StaffManagement;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Controllers
{
    public class StaffManagementController : Controller
    {
        //
        // GET: /Accounts/StaffManagement/

        public ActionResult Index()
        {

            return PartialView("_StaffIndex");
        }


        #region staff

        public ActionResult SatffList()
        {
            List<mStaff> list = (List<mStaff>)Helper.ExecuteService("staff", "StaffList", null);

            return PartialView("_staffList", list);
        }
        public ActionResult NewStaff()
        {
            ViewBag.StaffClass = new SelectList(Enum.GetValues(typeof(StaffClass)).Cast<StaffClass>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");

            mStaff staff = new mStaff();
            staff.Mode = "Save";
            return PartialView("_newStaff", staff);

        }
        public ActionResult saveandupdateStaff(mStaff staff)
        {
            if (staff.Mode == "Save")
            {
                String SbranchId = staff.BranchIDs;
                string[] arr = SbranchId.Split(',');
                string BIds = "";
                foreach (string word in arr)
                {
                    List<FillBranch> BranchList = (List<FillBranch>)Helper.ExecuteService("staff", "getBranchListbyID", word);
                    BIds = BIds + Convert.ToString(BranchList[0].BranchName) + ",";
                }
                string GetBranch = BIds.Remove(BIds.Length - 1);
                staff.Branch = GetBranch;

                List<mStaff> list = (List<mStaff>)Helper.ExecuteService("staff", "newStaff", staff);
                list = list.Where(m => m.IsReject == false).ToList();
                return PartialView("_staffList", list);
            }
            else
            {
                if (staff.BranchIDs != null)
                {
                    String SbranchId = staff.BranchIDs;

                    string[] arr = SbranchId.Split(',');


                    string BIds = "";
                    foreach (string word in arr)
                    {
                        List<FillBranch> BranchList = (List<FillBranch>)Helper.ExecuteService("staff", "getBranchListbyID", word);
                        BIds = BIds + Convert.ToString(BranchList[0].BranchName) + ",";
                    }
                    string GetBranch = BIds.Remove(BIds.Length - 1);
                    staff.Branch = GetBranch;
                }
                List<mStaff> list = (List<mStaff>)Helper.ExecuteService("staff", "updateStaff", staff);
                list = list.Where(m => m.IsReject == false).ToList();
                return PartialView("_staffList", list);
            }
        }

        public ActionResult fillBranch()
        {
            List<FillBranch> list = (List<FillBranch>)Helper.ExecuteService("staff", "getBranchList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateStaff(int staffID)
        {

            mStaff list = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", staffID);
            ViewBag.StaffClass = new SelectList(Enum.GetValues(typeof(StaffClass)).Cast<StaffClass>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text", list.Classes);

            list.Mode = "Update";
            return PartialView("_newStaff", list);
        }

        public ActionResult RejectStaff(int staffID)
        {
            mStaff obj = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", staffID);
            if (obj.IsReject)
                obj.IsReject = false;
            else
                obj.IsReject = true;
            obj.RejectedBy = CurrentSession.UserName;

            List<mStaff> list = (List<mStaff>)Helper.ExecuteService("staff", "updateStaff", obj);
            list = list.Where(m => m.IsReject == false).ToList();
            return PartialView("_staffList", list);
        }
        public ActionResult deActiveStaff(int staffID)
        {
            mStaff obj = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", staffID);
            if (obj.isActive)
                obj.isActive = false;
            else
                obj.isActive = true;
            obj.DeactivatedBy = CurrentSession.UserName;
            List<mStaff> list = (List<mStaff>)Helper.ExecuteService("staff", "updateStaff", obj);
            list = list.Where(m => m.IsReject == false).ToList();
            return PartialView("_staffList", list);
        }
        public ActionResult NominalSatff(int staffID)
        {
            mStaff obj = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", staffID);
            if (obj.isNominal)
                obj.isNominal = false;
            else
                obj.isNominal = true;
            List<mStaff> list = (List<mStaff>)Helper.ExecuteService("staff", "updateStaff", obj);
            list = list.Where(m => m.IsReject == false).ToList();
            return PartialView("_staffList", list);
        }
        public ActionResult deleteStaff(int staffID)
        {
            return PartialView("_staffList", null);
        }

        public ActionResult fillDesignation()
        {
            List<FillDesignation> list = (List<FillDesignation>)Helper.ExecuteService("staff", "getDesignationList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region staff nominee
        public ActionResult newNominee(string staffID)
        {
            mStaffNominee list = new mStaffNominee();
            list.StaffID = Convert.ToInt32(staffID);
            list.Mode = "Save";
            return PartialView("_MemberNewNominee", list);
        }
        public ActionResult NomineeList(string staffID)
        {
            List<mStaffNominee> list = (List<mStaffNominee>)Helper.ExecuteService("staff", "StaffNomineeList", staffID);
            mStaff staff = new mStaff();
            staff.StaffID = Convert.ToInt32(staffID);
            staff.mStaffNominee = list;
            return PartialView("_MembersNomineeList", staff);
        }
        public ActionResult saveNominee(mStaffNominee staffNominee)
        {
            if (staffNominee.Mode == "Save")
            {
                Helper.ExecuteService("staff", "saveNominee", staffNominee);
            }
            else
            {
                Helper.ExecuteService("staff", "updateNominee", staffNominee);
            }
            List<mStaffNominee> list = (List<mStaffNominee>)Helper.ExecuteService("staff", "StaffNomineeList", staffNominee.StaffID);
            mStaff staff = new mStaff();
            staff.mStaffNominee = list;
            return PartialView("_MembersNomineeList", staff);
        }

        public ActionResult updateNominee(string nomineeID)
        {
            mStaffNominee list = (mStaffNominee)Helper.ExecuteService("staff", "getNomineeDetails", nomineeID);
            list.Mode = "Update";
            return PartialView("_MemberNewNominee", list);
        }

        public ActionResult deleteNominee(string NomineeID)
        {
            int staffID = (int)Helper.ExecuteService("staff", "deleteNominee", NomineeID);
            List<mStaffNominee> list = (List<mStaffNominee>)Helper.ExecuteService("staff", "StaffNomineeList", staffID);
            mStaff staff = new mStaff();
            staff.mStaffNominee = list;
            return PartialView("_MembersNomineeList", staff);
        }
        #endregion

        [HttpPost]
        public ActionResult DownloadExcel()
        {
            var OtherFrimList = ((List<mStaff>)Helper.ExecuteService("staff", "StaffList", null)).Where(m => m.isActive == true).ToList();
            string Result = this.GetStaffListData(OtherFrimList);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=SatffList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }


        public string GetStaffListData(List<mStaff> model)
        {

            StringBuilder ReplyList = new StringBuilder();

            if (model != null && model.Count() > 0)
            {
                ReplyList.Append(string.Format("<div style='text-align:center;'><h2>Satff List</h2></div>"));

                ReplyList.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                ReplyList.Append("<thead class='header' ><tr style='background-color: #428bca ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:30px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px;'>SR. NO.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Name</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Designation</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >AadharId</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Mobile Number</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Account Number</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >IFSC Code</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Bank Name</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Branch</th>");
                ReplyList.Append("</tr></thead>");


                ReplyList.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    ReplyList.Append("<tr>");
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", count));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.StaffName));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.Designation));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.AadharID));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.MobileNo));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>ACC- {0}</td>", obj.BankAccountNo));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.IFSC));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.BankName));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.Branch));
                    ReplyList.Append("</tr>");
                }
                ReplyList.Append("</tbody></table></div> ");
            }
            else
            {
                ReplyList.Append("No Staff Found");
            }
            return ReplyList.ToString();
        }


        public ActionResult StaffDesignationIndex()
        {
            return PartialView("_StaffDesignationIndex");
        }


        public ActionResult NewstaffDesignation()
        {
            mStaffDesignation staffDesignation = new mStaffDesignation();
            staffDesignation.Mode = "Save";
            return PartialView("_newstaffDesignation", staffDesignation);
        }
        public ActionResult saveandupdateStaffDesignation(mStaffDesignation staff)
        {
            if (staff.Mode == "Save")
            {
                List<mStaffDesignation> list = (List<mStaffDesignation>)Helper.ExecuteService("staff", "NewstaffDesignation", staff);


                var test = (List<mStaffDesignation>)Helper.ExecuteService("staff", "getStaffDesignationDetailsByID", null);

                var lastItem = test.LastOrDefault();
                int deid = lastItem.DesignationID;

                var StaffUpdate = (List<mStaffDesignation>)Helper.ExecuteService("staff", "getUpdateStaffDesignationDetailsByID", deid);

                return PartialView("_StaffDesignationList", list);
            }
            else
            {
                List<mStaffDesignation> list = (List<mStaffDesignation>)Helper.ExecuteService("staff", "updateStaffDesignation", staff);
                return PartialView("_StaffDesignationList", list);
            }
        }


        public ActionResult SatffListDesignation()
        {
            List<mStaffDesignation> list = (List<mStaffDesignation>)Helper.ExecuteService("staff", "StaffDesignationList", null);

            return PartialView("_StaffDesignationList", list);
        }


        public ActionResult deleteStaffDesignation(int DesignationID)
        {


            var StaffUpdate = (List<mStaffDesignation>)Helper.ExecuteService("staff", "getDeleteStaffDesignationDetailsByID", DesignationID);

            return RedirectToAction("StaffDesignationIndex");

        }


        public ActionResult UpdateStaffDesignation(int DesignationID)
        {
            mStaffDesignation list = (mStaffDesignation)Helper.ExecuteService("staff", "getStaffDesignationDetailsByIDForUpdate", DesignationID);
            list.Mode = "Update";
            return PartialView("_newstaffDesignation", list);
        }
        #region Establishment Branch
        public ActionResult EatablishmentIndex()
        {
            //For Official Details
            ViewBag.StaffClass = new SelectList(Enum.GetValues(typeof(StaffClass)).Cast<StaffClass>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            //end*000000000000000000*

            ePartialViewModel model = new ePartialViewModel();
            //Personal and Official List
            List<mStaff> list = (List<mStaff>)Helper.ExecuteService("staff", "GetAllStaffNameList", null);
            model.mstaffNameList = list;
            //End List
            List<tPromotions> listPro = (List<tPromotions>)Helper.ExecuteService("staff", "promotionList", null);
            model.tPromotionsList = listPro;
            List<tTrainingDetails> Traininglist = (List<tTrainingDetails>)Helper.ExecuteService("staff", "TrainingList", null);
            model.tTrainingDetailsList = Traininglist;
            //Leave List
            List<tLeaveDetail> Leavelist = (List<tLeaveDetail>)Helper.ExecuteService("staff", "LeaveList", null);
            model.tLeaveDetailList = Leavelist;
            model.GenderList = GetGenderList(string.Empty);
            // model.PensionMode = "Save";
            return PartialView("_EBDetails", model);
        }

        public ActionResult GetPersonalDetail(int StaffID, string StaffName)
        {
            ePartialViewModel model = new ePartialViewModel();
            //* Personal and official
            mStaff list = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", StaffID);
            if (string.IsNullOrEmpty(list.Nationality))
                list.Nationality = "Indian";
            if (list != null)
            {
                model.mStaff = list;
                model.GenderList = GetGenderList(list.Gender);
                model.ImageShow = ImageLocation(list.Photo);
                model.GPFCPF = list.GPFCPF;
            }

            ViewBag.StaffClass = new SelectList(Enum.GetValues(typeof(StaffClass)).Cast<StaffClass>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            //*end----------------------
            //* Pension List -----------------------
            tPensiondetail pList = new tPensiondetail();
            pList.StaffID = StaffID;
            pList.StaffName = StaffName;
            tPensiondetail Pensionlist = (tPensiondetail)Helper.ExecuteService("staff", "getStaffPensionDetailsByID", pList);
            if (Pensionlist != null)
            {
                model.WheatherContribution = Pensionlist.WheatherContribution;
                model.GrossService = Pensionlist.GrossService;
                model.NonService = Pensionlist.NonQualifyingservice;
                model.NetService = Pensionlist.NetQualifyingservice;
                model.PensionMode = "Update";
                model.tPensiondetail = Pensionlist;
            }

            //end*----------------------
            //Promoson List
            List<tPromotions> listPro = (List<tPromotions>)Helper.ExecuteService("staff", "promotionList", StaffID);
            model.tPromotionsList = listPro; // listPro.Where(a => a.StaffID == StaffID).ToList();
            //Training List
            List<tTrainingDetails> Traininglist = (List<tTrainingDetails>)Helper.ExecuteService("staff", "TrainingList", StaffID);
            model.tTrainingDetailsList = Traininglist;// Traininglist.Where(a => a.StaffID == StaffID).ToList();
            //Leave List
            List<tLeaveDetail> Leavelist = (List<tLeaveDetail>)Helper.ExecuteService("staff", "LeaveList", StaffID);
            model.tLeaveDetailList = Leavelist; //Leavelist.Where(a => a.StaffID == StaffID).ToList();
            model.Mode = "Update";




            return PartialView("_tabMenuPartial", model);
        }

        public JsonResult CheckAadharNumberExist(int StaffID, string AadharNumber)
        {
            string Messgae = string.Empty;
            mStaff staff = new mStaff();
            staff.StaffID = StaffID;
            staff.AadharID = AadharNumber;
            var StaffExist = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByAadharNum", staff);
            if (StaffExist != null)
                Messgae = "Aadhar number already registered . Please fill new aadhar number";
            return Json(Messgae, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PersonalAndupdateNewStaff(ePartialViewModel models)
        {

            string Date = Request.Form["DOB0"];

            mStaff staff = models.mStaff;


            staff.Category = Request.Form["Category"];
            if (Date.Length < 11 && Date != null)
            {
                DateTime DateofSubmi = new DateTime();
                if (!string.IsNullOrEmpty(Date))
                    DateofSubmi = new DateTime(Convert.ToInt32(Date.Split('/')[2]), Convert.ToInt32(Date.Split('/')[1]), Convert.ToInt32(Date.Split('/')[0]));
                staff.DOB = DateofSubmi;
            }

            //Add personal detail
            mStaff Stafflist = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", staff.StaffID);
            Stafflist.StaffName = staff.StaffName;
            Stafflist.FatherName = staff.FatherName;
            Stafflist.SpouseName = staff.SpouseName;
            Stafflist.DOB = staff.DOB;
            Stafflist.Gender = models.Gender;
            Stafflist.Category = staff.Category;
            Stafflist.BloodGroup = staff.BloodGroup;
            Stafflist.EducationalQualification = staff.EducationalQualification;
            Stafflist.Nationality = staff.Nationality;
            Stafflist.Address = staff.Address;
            Stafflist.PinCode = staff.PinCode;
            Stafflist.District = staff.District;
            Stafflist.State = staff.State;
            Stafflist.AadharID = staff.AadharID;
            Stafflist.PANNo = staff.PANNo;
            Stafflist.IdentificationMark = staff.IdentificationMark;
            Stafflist.Height = staff.Height;
            Stafflist.MobileNo = staff.MobileNo;
            Stafflist.LandlineNo = staff.LandlineNo;
            Stafflist.EmailId = staff.EmailId;
            Stafflist.Photo = staff.Photo;
            Stafflist.DateofSuperannuation = staff.DateofSuperannuation;
            List<mStaff> list = (List<mStaff>)Helper.ExecuteService("staff", "updateStaff", Stafflist);
            list = list.Where(m => m.IsReject == false).ToList();
            //Check Pension detail
            tPensiondetail pstaff = new tPensiondetail();
            pstaff.StaffID = staff.StaffID;
            pstaff.StaffName = staff.StaffName;
            pstaff.Address = staff.Address;
            pstaff.District = staff.District;
            pstaff.State = staff.State;
            pstaff.PinCode = staff.PinCode;
            pstaff.SpouseName = staff.SpouseName;

            tPensiondetail Pensionlist = (tPensiondetail)Helper.ExecuteService("staff", "getStaffPensionDetailsByID", pstaff);
            if (Pensionlist == null)
            {
                string DateofRetirement = Request.Form["hdnDateofre"];
                if (DateofRetirement == "")
                {
                    DateofRetirement = GetRetirement(Convert.ToString(Stafflist.DOB), Convert.ToString(Stafflist.DateofSuperannuation));
                }
                DateTime? DOR = new DateTime();
                if (!string.IsNullOrEmpty(DateofRetirement))
                    DOR = new DateTime(Convert.ToInt32(DateofRetirement.Split('/')[2]), Convert.ToInt32(DateofRetirement.Split('/')[1]), Convert.ToInt32(DateofRetirement.Split('/')[0]));
                else
                    DOR = null;
                pstaff.DateofRetirement = DOR;
                List<tPensiondetail> plist = (List<tPensiondetail>)Helper.ExecuteService("staff", "newStaffPension", pstaff);
                plist = plist.ToList();
            }
            else
            {
                Pensionlist.StaffName = pstaff.StaffName;
                Pensionlist.Address = pstaff.Address;
                Pensionlist.District = pstaff.District;
                Pensionlist.State = pstaff.State;
                Pensionlist.PinCode = pstaff.PinCode;
                Pensionlist.SpouseName = pstaff.SpouseName;
                List<tPensiondetail> plist = (List<tPensiondetail>)Helper.ExecuteService("staff", "updateStaffPension", Pensionlist);
                plist = plist.ToList();
            }

            //TempData["Msg"] = "Sucessfully updated personal details";
            //TempData["Class"] = "alert alert-info";
            //TempData["Notification"] = "Success";

            // models.ImageShow = ImageLocation(staff.Photo);
            models.GenderList = GetGenderList(string.Empty);
            return RedirectToAction("GetPersonalDetail", new { staff.StaffID, staff.StaffName });
            // return PartialView("_PersonalDetailFrom", models);
        }
        public ActionResult OfficialAndupdateNewStaff(ePartialViewModel models)
        {
            string Classes = Request.Form["Classes"];
            string Group = Request.Form["Group"];
            string Designation = Request.Form["Designation1"];
            string Doj = Request.Form["DOJ"];
            //*-------------------*//
            mStaff staff = models.mStaff;
            if (Classes != null)
                staff.Classes = Convert.ToInt32(Classes);
            if (Group != null)
                staff.Group = Group;
            if (Designation != null)
                staff.Designation = Designation;
            staff.GPFCPF = models.GPFCPF;
            if (Doj.Length < 11 && Doj != null)
            {
                DateTime DateofSubmi = new DateTime();
                if (!string.IsNullOrEmpty(Doj))
                    DateofSubmi = new DateTime(Convert.ToInt32(Doj.Split('/')[2]), Convert.ToInt32(Doj.Split('/')[1]), Convert.ToInt32(Doj.Split('/')[0]));
                staff.DOJ = DateofSubmi;
            }
            //Add Official Detail 
            mStaff Stafflist = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", staff.StaffID);
            Stafflist.Designation = staff.Designation;
            Stafflist.Classes = staff.Classes;
            Stafflist.Group = staff.Group;
            Stafflist.EmployeeCode = staff.EmployeeCode;
            Stafflist.DDOCode = staff.DDOCode;
            Stafflist.IDCardNo = staff.IDCardNo;
            Stafflist.eSalaryId = staff.eSalaryId;
            Stafflist.DOJ = staff.DOJ;
            Stafflist.Basic = staff.Basic;
            Stafflist.gradePay = staff.gradePay;
            Stafflist.SecttPay = staff.SecttPay;
            Stafflist.SplPay = staff.SplPay;
            Stafflist.RateofIncrement = staff.RateofIncrement;
            Stafflist.monthID = staff.monthID;
            Stafflist.yearID = staff.yearID;
            Stafflist.Branch = staff.Branch;
            Stafflist.GPFCPF = staff.GPFCPF;
            Stafflist.BankName = staff.BankName;
            Stafflist.IFSC = staff.IFSC;
            Stafflist.BankAccountNo = staff.BankAccountNo;

            List<mStaff> list = (List<mStaff>)Helper.ExecuteService("staff", "updateStaff", Stafflist);
            list = list.Where(m => m.IsReject == false).ToList();
            ViewBag.StaffClass = new SelectList(Enum.GetValues(typeof(StaffClass)).Cast<StaffClass>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");

            models.GenderList = GetGenderList(string.Empty);
            //Check Pension detail
            tPensiondetail pstaff = new tPensiondetail();
            pstaff.StaffID = staff.StaffID;
            pstaff.StaffName = staff.StaffName;
            tPensiondetail Pensionlist = (tPensiondetail)Helper.ExecuteService("staff", "getStaffPensionDetailsByID", pstaff);
            if (Pensionlist != null)
            {
                Pensionlist.GrossService = Request.Form["hdnGrossServise1"];
                Pensionlist.NonQualifyingservice = Request.Form["hdnNonServise1"];
                Pensionlist.NetQualifyingservice = Request.Form["hdnNetServise1"];

                List<tPensiondetail> plist = (List<tPensiondetail>)Helper.ExecuteService("staff", "updateStaffPension", Pensionlist);
                plist = plist.ToList();
            }
            return PartialView("_CurrentOfficialDetail", models);
        }

        public ActionResult SaveNewStaffPension(ePartialViewModel models)
        {
            tPensiondetail staff = models.tPensiondetail;
            staff.StaffID = Convert.ToInt32(Request.Form["hdnstaffId"]);
            staff.StaffName = Request.Form["hdnstaffName"];
            var hdnPensionMode = Request.Form["hdnPensionMode"];
            staff.GrossService = Request.Form["hdnGrossServise"];
            staff.NonQualifyingservice = Request.Form["hdnNonServise"];
            staff.NetQualifyingservice = Request.Form["hdnNetServise"];
            string hdnRetirement = Request.Form["hdnRetirement"];
            string DateofSubmission = Request.Form["DateofSubmission"];
            string DateofBirth = Request.Form["DateofDeath"];
            string DateofRetirement = Request.Form["DateofRetirement"];
            string ForcefullyRetirement = Request.Form["ForcefullyRetirement"];
            string DateofCommencement = Request.Form["DateofCommencement"];
            string DateOfLodgingFir = Request.Form["DateOfLodgingFir"];
            string DateOfMedicalCerti = Request.Form["DateOfMedicalCerti"];
            string DobFamilyPensioner = Request.Form["DobFamilyPensioner"];
            string Foreignservices = Request.Form["Foreignservices"];
            if (DateofSubmission.Length > 11)
            {
                DateTime Submission = Convert.ToDateTime(DateofSubmission);
                DateofSubmission = Submission.ToString("dd/MM/yyyy");
            }
            DateTime DateofSubmi = new DateTime();
            if (!string.IsNullOrEmpty(DateofSubmission))
                DateofSubmi = new DateTime(Convert.ToInt32(DateofSubmission.Split('/')[2]), Convert.ToInt32(DateofSubmission.Split('/')[1]), Convert.ToInt32(DateofSubmission.Split('/')[0]));

            if (DateofBirth != null || DateofBirth != "")
            {
                if (DateofBirth.Length > 11)
                {
                    DateTime DofBirth = Convert.ToDateTime(DateofBirth);
                    DateofBirth = DofBirth.ToString("dd/MM/yyyy");
                }
            }
            DateTime? DOB = new DateTime();
            if (!string.IsNullOrEmpty(DateofBirth))
                DOB = new DateTime(Convert.ToInt32(DateofBirth.Split('/')[2]), Convert.ToInt32(DateofBirth.Split('/')[1]), Convert.ToInt32(DateofBirth.Split('/')[0]));
            else
                DOB = null;
            if (DateofRetirement != null || DateofRetirement != "")
            {
                if (DateofRetirement.Length > 11)
                {
                    DateTime DofRi = Convert.ToDateTime(DateofRetirement);
                    DateofRetirement = DofRi.ToString("dd/MM/yyyy");
                }
            }
            DateTime? DOR = new DateTime();
            if (!string.IsNullOrEmpty(DateofRetirement))
                DOR = new DateTime(Convert.ToInt32(DateofRetirement.Split('/')[2]), Convert.ToInt32(DateofRetirement.Split('/')[1]), Convert.ToInt32(DateofRetirement.Split('/')[0]));
            else
                DOR = null;


            if (ForcefullyRetirement != null || ForcefullyRetirement != "")
            {
                if (ForcefullyRetirement.Length > 11)
                {
                    DateTime Dofforcr = Convert.ToDateTime(ForcefullyRetirement);
                    ForcefullyRetirement = Dofforcr.ToString("dd/MM/yyyy");
                }
            }
            DateTime? DOFR = new DateTime();
            if (!string.IsNullOrEmpty(ForcefullyRetirement))
                DOFR = new DateTime(Convert.ToInt32(ForcefullyRetirement.Split('/')[2]), Convert.ToInt32(ForcefullyRetirement.Split('/')[1]), Convert.ToInt32(ForcefullyRetirement.Split('/')[0]));
            else
                DOFR = null;

            if (DateofCommencement.Length > 11)
            {
                DateTime DateOfCo = Convert.ToDateTime(DateofCommencement);
                DateofCommencement = DateOfCo.ToString("dd/MM/yyyy");
            }
            DateTime DateofCommen = new DateTime();
            if (!string.IsNullOrEmpty(DateofCommencement))
                DateofCommen = new DateTime(Convert.ToInt32(DateofCommencement.Split('/')[2]), Convert.ToInt32(DateofCommencement.Split('/')[1]), Convert.ToInt32(DateofCommencement.Split('/')[0]));

            if (DobFamilyPensioner.Length > 11)
            {
                DateTime DobFamily = Convert.ToDateTime(DobFamilyPensioner);
                DobFamilyPensioner = DobFamily.ToString("dd/MM/yyyy");
            }
            DateTime DobFamilyP = new DateTime();
            if (!string.IsNullOrEmpty(DobFamilyPensioner))
                DobFamilyP = new DateTime(Convert.ToInt32(DobFamilyPensioner.Split('/')[2]), Convert.ToInt32(DobFamilyPensioner.Split('/')[1]), Convert.ToInt32(DobFamilyPensioner.Split('/')[0]));
            if (DateOfMedicalCerti.Length > 11)
            {
                DateTime DateOfM = Convert.ToDateTime(DateOfMedicalCerti);
                DateOfMedicalCerti = DateOfM.ToString("dd/MM/yyyy");
            }
            if (DateOfLodgingFir.Length > 11)
            {
                DateTime DateOfL = Convert.ToDateTime(DateOfLodgingFir);
                DateOfLodgingFir = DateOfL.ToString("dd/MM/yyyy");
            }
            DateTime? DateOfLodging = new DateTime();
            if (!string.IsNullOrEmpty(DateOfLodgingFir))
                DateOfLodging = new DateTime(Convert.ToInt32(DateOfLodgingFir.Split('/')[2]), Convert.ToInt32(DateOfLodgingFir.Split('/')[1]), Convert.ToInt32(DateOfLodgingFir.Split('/')[0]));
            else
                DateOfLodging = null;

            DateTime? DateOfMedical = new DateTime();
            if (!string.IsNullOrEmpty(DateOfMedicalCerti))
                DateOfMedical = new DateTime(Convert.ToInt32(DateOfMedicalCerti.Split('/')[2]), Convert.ToInt32(DateOfMedicalCerti.Split('/')[1]), Convert.ToInt32(DateOfMedicalCerti.Split('/')[0]));
            else
                DateOfMedical = null;
            if (Foreignservices.Length > 11)
            {
                DateTime Foreigns = Convert.ToDateTime(Foreignservices);
                Foreignservices = Foreigns.ToString("dd/MM/yyyy");
            }
            DateTime? Foreign = new DateTime();
            if (!string.IsNullOrEmpty(Foreignservices))
                Foreign = new DateTime(Convert.ToInt32(Foreignservices.Split('/')[2]), Convert.ToInt32(Foreignservices.Split('/')[1]), Convert.ToInt32(Foreignservices.Split('/')[0]));
            else
                Foreign = null;

            staff.DateofSubmission = DateofSubmi;
            staff.DateofCommencement = DateofCommen;
            staff.DateOfLodgingFir = DateOfLodging;
            staff.DateOfMedicalCerti = DateOfMedical;
            staff.DobFamilyPensioner = DobFamilyP;
            staff.Foreignservices = Foreign;
            staff.DateofDeath = DOB;
            staff.DateofRetirement = DOR;
            staff.ForcefullyRetirement = DOFR;
            staff.WheatherContribution = models.WheatherContribution;
            tPensiondetail Pensionlist = (tPensiondetail)Helper.ExecuteService("staff", "getStaffPensionDetailsByID", staff);
            if (Pensionlist != null)
            {
                staff.PensionId = Pensionlist.PensionId;
            }
            List<tPensiondetail> list = (List<tPensiondetail>)Helper.ExecuteService("staff", "updateStaffPension", staff);
            list = list.ToList();

            return PartialView("_pensionStaffDetail", models);


        }

        public object CheckAadhar(string AadharId)
        {

            string inputValue = AadharId.Trim();
            int AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
            int SessionCode = Convert.ToInt32(CurrentSession.SessionId);
            var GetAdharIdExist = (Int32)Helper.ExecuteService("staff", "GetAdharIdExist", new mStaff { AadharID = inputValue });
            if (GetAdharIdExist == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
#pragma warning disable CS0162 // Unreachable code detected
            return false;
#pragma warning restore CS0162 // Unreachable code detected
        }
        public JsonResult GetAdhaarDetails(string AdhaarID, string fileLoaction)
        {
            AdhaarDetails details = new AdhaarDetails();
            AdhaarServices.ServiceSoapClient obj = new AdhaarServices.ServiceSoapClient();
            EncryptionDecryption.EncryptionDecryption objencr = new EncryptionDecryption.EncryptionDecryption();
            string inputValue = objencr.Encryption(AdhaarID.Trim());
            try
            {
                DataTable dt = obj.getHPKYCInDataTable(inputValue);
                if (dt != null && dt.Rows.Count > 0)
                {
                    details.AdhaarID = AdhaarID;
                    details.Name = objencr.Decryption(Convert.ToString(dt.Rows[0]["Name"]));
                    details.FatherName = objencr.Decryption(Convert.ToString(dt.Rows[0]["FatherName"]));
                    details.Gender = objencr.Decryption(Convert.ToString(dt.Rows[0]["Gender"]));
                    details.Address = objencr.Decryption(Convert.ToString(dt.Rows[0]["Address"]).ToString());
                    details.DOB = objencr.Decryption(Convert.ToString(dt.Rows[0]["DOB"]));
                    details.MobileNo = objencr.Decryption(Convert.ToString(dt.Rows[0]["MobileNumber"]));
                    details.Email = objencr.Decryption(Convert.ToString(dt.Rows[0]["EmailID"]));
                    details.District = objencr.Decryption(Convert.ToString(dt.Rows[0]["DistrictName"]).ToString());
                    details.PinCode = objencr.Decryption(Convert.ToString(dt.Rows[0]["PinCOde"]).ToString());
                    //Calculate the age.
                    DateTime dateOfBirth;

                    if (!string.IsNullOrEmpty(details.DOB) && DateTime.TryParse(details.DOB, out dateOfBirth))
                    {
                        dateOfBirth = Convert.ToDateTime(details.DOB);
                        //DateTime today = DateTime.Today;
                        //int age = today.Year - dateOfBirth.Year;
                        //if (dateOfBirth > today.AddYears(-age))
                        //    age--;

                        details.DOB = Convert.ToString(dateOfBirth);
                    }
                    else
                    {
                        details.DOB = "";
                    }
                    Byte[] bytes = (Byte[])Convert.FromBase64String(objencr.Decryption(dt.Rows[0]["photo"].ToString()));
                    if (bytes.Length != 0)
                    {
                        if (fileLoaction != null && fileLoaction != "" && (fileLoaction.IndexOf("/") != -1))
                        {
                            int indexof = fileLoaction.LastIndexOf("/");
                            string url = fileLoaction.Substring(0, indexof + 1);
                            string FileName = fileLoaction.Substring(indexof + 1);
                            var path = Server.MapPath(url) + FileName;
                            System.IO.File.WriteAllBytes(path, bytes);

                            details.Photo = fileLoaction;
                        }
                        else
                        {
                            Guid PicName;
                            PicName = Guid.NewGuid();
                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                            string urlPath = FileSettings.SettingValue + "Staff/";
                            string Url = urlPath;

                            // string Url = "/Images/Pass/Photo/" + session.AssemblyID + "/" + session.SessionCode + "/";
                            DirectoryInfo Dir = new DirectoryInfo(Url);
                            if (!Dir.Exists)
                            {
                                Dir.Create();
                            }
                            var path = Url + PicName + ".jpg";
                            System.IO.File.WriteAllBytes(path, bytes);
                            // var PassFileLocalServerPath = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "LocalFilePath", null);
                            var PassFileLocalServerPath = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);

                            string showUrl = PassFileLocalServerPath.SettingValue + "Staff/" + PicName + ".jpg";
                            details.Photo = showUrl + "," + PicName + ".jpg";
                        }
                        return Json(details, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        details.Photo = null;
                        return Json(details, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }


            }
            catch
            {
                return Json(details, JsonRequestBehavior.AllowGet);

            }
        }
        public static SelectList GetGenderList(string SlectedValue)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Male", Value = "Male" });
            result.Add(new SelectListItem() { Text = "Female", Value = "Female" });

            return new SelectList(result, "Value", "Text", SlectedValue);
        }
        public string GetPicNameFromPath(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                string result = string.Empty;
                result = Path.GetFileNameWithoutExtension(Server.MapPath(filePath));
                return result;
            }
            else
            {
                return "";
            }
        }
        [HttpPost]
        public string UploadPhoto(HttpPostedFileBase file, string filelocation)
        {
            if (file != null)
            {
                var stream = file.InputStream;
                string picName = GetPicNameFromPath(filelocation);

                //var picLocation = SavePicToDirectory(stream, picName);


                string extension = System.IO.Path.GetExtension(file.FileName);

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string urlPath = FileSettings.SettingValue + "Staff/";
                string Url = urlPath;
                DirectoryInfo Dir = new DirectoryInfo(Url);
                if (!Dir.Exists)
                {
                    Dir.Create();
                }
                Guid PicName;
                PicName = Guid.NewGuid();
                string path = System.IO.Path.Combine(Url, PicName + ".jpg");
                file.SaveAs(path);
                //var PassFileLocalServerPath = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "LocalFilePath", null);
                var PassFileLocalServerPath = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);

                string showUrl = PassFileLocalServerPath.SettingValue + "Staff/" + PicName + ".jpg";
                var picLocation = showUrl + "," + PicName + ".jpg";
                return picLocation;
            }
            return null;
        }
        public string ImageLocation(string Photo)
        {
            try
            {
                //var PassFileLocalServerPath = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "LocalFilePath", null);
                var PassFileLocalServerPath = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);

                string showUrl = PassFileLocalServerPath.SettingValue + "Staff/" + Photo;
                var picLocation = showUrl;
                return picLocation;
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }
        public JsonResult CheckExistStaffId(int StaffID, string StaffName)
        {
            tPensiondetail model = new tPensiondetail();
            model.StaffID = StaffID;
            model.StaffName = StaffName;
            tPensiondetail Pensionlist = (tPensiondetail)Helper.ExecuteService("staff", "getStaffPensionDetailsByID", model);
            if (Pensionlist != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EstNewStaff()
        {
            ViewBag.StaffClass = new SelectList(Enum.GetValues(typeof(StaffClass)).Cast<StaffClass>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");

            mStaff staff = new mStaff();
            staff.Mode = "Save";
            return PartialView("_CreateNewStaff", staff);

        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SavePopulateData(mStaff staff)
        {
            try
            {
                staff.DOJ = null;
                string dateofbirth = Request.Form["DOB"];
                DateTime dt = DateTime.ParseExact(dateofbirth, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                staff.DOB = dt;
                List<mStaff> list = (List<mStaff>)Helper.ExecuteService("staff", "newStaff", staff);
                list = list.Where(m => m.IsReject == false).ToList();
                return RedirectToAction("EatablishmentIndex");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return RedirectToAction("EstNewStaff");
            }

        }
        public string GetGrossServiceDetail(string Doj, string DateOfR)
        {
            string date = "";
            if (Doj != null && DateOfR != null)
            {
                if (Doj.Length > 11)
                {
                    DateTime Dateofj = Convert.ToDateTime(Doj);
                    Doj = Dateofj.ToString("dd/MM/yyyy");

                }
                if (DateOfR.Length > 11)
                {
                    DateTime DateofR = Convert.ToDateTime(DateOfR);
                    DateOfR = DateofR.ToString("dd/MM/yyyy");
                }
                DateTime? DateofJoining = new DateTime();
                if (!string.IsNullOrEmpty(Doj))
                    DateofJoining = new DateTime(Convert.ToInt32(Doj.Split('/')[2]), Convert.ToInt32(Doj.Split('/')[1]), Convert.ToInt32(Doj.Split('/')[0]));
                DateTime? DateofRetire = new DateTime();
                if (!string.IsNullOrEmpty(Doj))
                    DateofRetire = new DateTime(Convert.ToInt32(DateOfR.Split('/')[2]), Convert.ToInt32(DateOfR.Split('/')[1]), Convert.ToInt32(DateOfR.Split('/')[0]));

                TimeSpan? dat = DateofRetire - DateofJoining;
                DateTime v = new DateTime(1, 1, 1);
                DateTime vv = v.AddDays(dat.Value.TotalDays);
                int days = vv.Day - 1;
                int months = vv.Month - 1;
                int years = vv.Year - 1;
                date = days + "/" + months + "/" + years;
            }
            return date;
        }
        public string GetRetirement(string DoB, string dateofsuper)
        {
            string date = "";

            if (DoB != null && dateofsuper != null)
            {
                if (DoB.Length > 11)
                {

                    DateTime Submission = Convert.ToDateTime(DoB);
                    DoB = Submission.ToString("dd/MM/yyyy");
                }
                int DateofSuperAnotation = Convert.ToInt32(dateofsuper);
                DateTime? DateofSubmi = new DateTime();
                if (!string.IsNullOrEmpty(DoB))
                    DateofSubmi = new DateTime(Convert.ToInt32(DoB.Split('/')[2]), Convert.ToInt32(DoB.Split('/')[1]), Convert.ToInt32(DoB.Split('/')[0]));
                DateTime? dtnow = DateofSubmi.Value.AddYears(DateofSuperAnotation);
                int days = dtnow.Value.Day;
                int Month = dtnow.Value.Month;
                int year = dtnow.Value.Year;
                if (days == 1)
                {
                    if (Month == 1)
                    {
                        date = "31" + "/" + "12" + "/" + Convert.ToString(year - 1);
                    }
                    else
                    {
                        int month = Month - 1;
                        if (Month == 2)
                        {
                            date = "31" + "/" + month + "/" + year;
                        }
                        if (Month == 4 || Month == 6 || Month == 9 || Month == 11)
                        {
                            date = "30" + "/" + month + "/" + year;
                        }
                        if (Month == 1 || Month == 3 || Month == 5 || Month == 7 || Month == 8 || Month == 10 || Month == 12)
                        {
                            date = "31" + "/" + month + "/" + year;
                        }

                    }

                }
                else
                {
                    if (Month == 2)
                    {
                        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
                        {
                            date = "29" + "/" + Month + "/" + year;
                        }
                        else
                        {
                            date = "28" + "/" + Month + "/" + year;
                        }
                    }
                    if (Month == 4 || Month == 6 || Month == 9 || Month == 11)
                    {
                        date = "30" + "/" + Month + "/" + year;
                    }
                    if (Month == 1 || Month == 3 || Month == 5 || Month == 7 || Month == 8 || Month == 10 || Month == 12)
                    {
                        date = "31" + "/" + Month + "/" + year;
                    }
                }
            }
            return date;
        }
        #endregion






        public ActionResult NewPromotion(int StaffID)
        {
            mStaff obj = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", StaffID);
            tPromotions promotions = new tPromotions();
            promotions.Mode = "Save";
            promotions.StaffID = StaffID;
            promotions.Designation = obj.Designation;
            return PartialView("_NewPromotion", promotions);
        }


        public ActionResult promotionList(int StaffId)
        {
            ePartialViewModel model = new ePartialViewModel();
            List<tPromotions> list = (List<tPromotions>)Helper.ExecuteService("staff", "promotionList", StaffId);
            model.tPromotionsList = list;
            return PartialView("_PromotionDetailsList", model);
        }

        public JsonResult saveandupdatePromotion(tPromotions promotion, string OrderNumber, string Designation, string From, string To, string DOJ, string Branch, string PayScale, string mode)
        {

            promotion.OrderNumber = OrderNumber;
            promotion.Designation = Designation;
            promotion.From = From;
            promotion.To = To;

            promotion.DOJ = DOJ;
            promotion.Branch = Branch;
            promotion.PayScale = PayScale;


            List<tPromotions> list = (List<tPromotions>)Helper.ExecuteService("staff", "NewPromotion", promotion);
            return Json(list, JsonRequestBehavior.AllowGet);

        }


        public JsonResult PromotionDetailsUpdate(tPromotions promotion, string OrderNumber, string Designation, string From, string To, string DOJ, string Branch, string PayScale, string mode)
        {

            promotion.OrderNumber = OrderNumber;
            promotion.Designation = Designation;
            promotion.From = From;
            promotion.To = To;

            promotion.DOJ = DOJ;
            promotion.Branch = Branch;
            promotion.PayScale = PayScale;
            promotion = (tPromotions)Helper.ExecuteService("staff", "updatPromotionDetails", promotion);

            return Json(promotion, JsonRequestBehavior.AllowGet);

        }


        public ActionResult updatePromotion(int PromotionID)
        {
            tPromotions list = (tPromotions)Helper.ExecuteService("staff", "getPromotionDetailsByIDForUpdate", PromotionID);
            list.Mode = "Update";
            return PartialView("_NewPromotion", list);


        }

        public ActionResult deletePromotion(int PromotionID, int StaffID)
        {


            var PromotionUpdate = (List<tPromotions>)Helper.ExecuteService("staff", "getDeletePromotionDetailsByID", PromotionID);

            return RedirectToAction("promotionList", new { StaffId = StaffID });

        }


        #region Establishment Branch
        public ActionResult LeaveList(int StaffId)
        {
            ePartialViewModel model = new ePartialViewModel();
            List<tLeaveDetail> list = (List<tLeaveDetail>)Helper.ExecuteService("staff", "LeaveList", StaffId);
            model.tLeaveDetailList = list;
            return PartialView("_LeaveDetailList", model);
        }


        public ActionResult NewLeave()
        {
            tLeaveDetail Leave = new tLeaveDetail();
            // Leave.StaffID = StaffID;
            Leave.Mode = "Save";
            return PartialView("_NewLeaveDetails", Leave);
        }



        public JsonResult LeaveDetailsSave(tLeaveDetail obj, string LeaveType, int NumofDays, string From, string To, string mode, int StaffID)
        {

            if (mode == "Save")
            {
                // string LeaveType1 = LeaveType.Split('1')[1];
                // string LeaveType2 = LeaveType.Split('0')[1];

                obj.LeaveType = LeaveType;
                obj.StaffID = StaffID;
                List<tLeaveDetail> list = (List<tLeaveDetail>)Helper.ExecuteService("staff", "NewLeave", obj);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //string LeaveType1 = LeaveType.Split('1')[1];
                //string LeaveType2 = LeaveType.Split('0')[1];


                obj.NumofDays = NumofDays;
                obj.From = From;
                obj.To = To;
                //obj.StaffID = StaffID;
                obj = (tLeaveDetail)Helper.ExecuteService("staff", "updateLeave", obj);
                return Json(obj, JsonRequestBehavior.AllowGet);

            }

        }


        public JsonResult LeaveDetailsUpdate(tLeaveDetail obj, string LeaveType, int NumofDays, string From, string To, string mode, int LeaveID)
        {


            // string LeaveType1 = LeaveType.Split('1')[1];
            // string LeaveType2 = LeaveType.Split('0')[1];

            obj.LeaveType = LeaveType;
            obj.LeaveID = LeaveID;
            obj.NumofDays = NumofDays;
            obj.From = From;
            obj.To = To;
            //obj.StaffID = StaffID;
            obj = (tLeaveDetail)Helper.ExecuteService("staff", "updateLeave", obj);

            return Json(obj, JsonRequestBehavior.AllowGet);

        }




        public ActionResult updateLeaveDetails(int LeaveID)
        {
            tLeaveDetail list = (tLeaveDetail)Helper.ExecuteService("staff", "getLeaveDetailsByIDForUpdate", LeaveID);
            list.Mode = "Update";
            return PartialView("_NewLeaveDetails", list);


        }


        public ActionResult deleteLeave(int LeaveID)
        {


            var LeaveDelate = (List<tLeaveDetail>)Helper.ExecuteService("staff", "getDeleteLeaveDetailsByID", LeaveID);

            return RedirectToAction("LeaveList");

        }

        public ActionResult TrainingList(int StaffId)
        {
            ePartialViewModel model = new ePartialViewModel();

            List<tTrainingDetails> list = (List<tTrainingDetails>)Helper.ExecuteService("staff", "TrainingList", StaffId);
            model.tTrainingDetailsList = list;
            return PartialView("_TrainingDetailList", model);
        }


        public ActionResult NewTraining()
        {
            tTrainingDetails Training = new tTrainingDetails();

            Training.Mode = "Save";
            return PartialView("_NewTrainingDetails", Training);
        }



        public JsonResult TrainingDetailsSave(tTrainingDetails obj)
        {

            List<tTrainingDetails> list = (List<tTrainingDetails>)Helper.ExecuteService("staff", "NewTraining", obj);
            return Json(list, JsonRequestBehavior.AllowGet);

        }

        public ActionResult updateTrainingDetails(int TrainingID)
        {
            tTrainingDetails list = (tTrainingDetails)Helper.ExecuteService("staff", "getTrainingDetailsByIDForUpdate", TrainingID);
            list.Mode = "Update";
            return PartialView("_NewTrainingDetails", list);

        }



        public JsonResult TrainingDetailsUpdate(tTrainingDetails obj)
        {

            var TrainingType = obj.TrainingType;
            //string TrainingType1 = TrainingType.Split('1')[1];
            // string TrainingType2 = TrainingType1.Split('0')[1];

            obj.TrainingType = TrainingType;

            obj = (tTrainingDetails)Helper.ExecuteService("staff", "updateTraining", obj);

            return Json(obj, JsonRequestBehavior.AllowGet);

        }


        public ActionResult deleteTraining(int TrainingID, int StaffID)
        {


            var TrainingDelate = (List<tTrainingDetails>)Helper.ExecuteService("staff", "getDeleteTrainingDetailsByID", TrainingID);

            return RedirectToAction("TrainingList", new { StaffId = StaffID });

        }


        #endregion


    }
}
