﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Member;
using SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Controllers
{
    public class MemberAccountDetailsController : Controller
    {
        //
        // GET: /Accounts/MemberAccountDetails/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexPartial()
        {
            return PartialView("_index");
        }
        public JsonResult fillDesignation()
        {
            List<FillDesignation> list = (List<FillDesignation>)Helper.ExecuteService("loan", "getDesignationList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult fillCurrentMember()
        {
            List<fillCurrentMamber> list = (List<fillCurrentMamber>)Helper.ExecuteService("loan", "getCurrentMemberList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult fillXMember()
        {
            List<fillCurrentMamber> list = (List<fillCurrentMamber>)Helper.ExecuteService("loan", "getXMemberList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getDefaultresult()
        {
            List<memberAccountSearchResult> list = (List<memberAccountSearchResult>)Helper.ExecuteService("MemberAccountDetails", "getMemberList", null);
            return PartialView("_searchResult", list);
        }
        public ActionResult searchResult(string searchcriteria, String searchcriteriavalue)
        {
            if (int.Parse(searchcriteria) == 0)
            {
                return RedirectToAction("getDefaultresult");
            }
            else
            {
                if (int.Parse(searchcriteria) == 1)
                {
                    List<memberAccountSearchResult> list = (List<memberAccountSearchResult>)Helper.ExecuteService("MemberAccountDetails", "getMemberListByDesignation", searchcriteriavalue);
                    return PartialView("_searchResult", list);
                }
                if (int.Parse(searchcriteria) == 2)
                {
                    if (int.Parse(searchcriteriavalue) == 0)
                    {
                        List<memberAccountSearchResult> list = (List<memberAccountSearchResult>)Helper.ExecuteService("MemberAccountDetails", "getCurrentMemberList", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                    else
                    {
                        List<memberAccountSearchResult> list = (List<memberAccountSearchResult>)Helper.ExecuteService("MemberAccountDetails", "getMemberListByID", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                }
                if (int.Parse(searchcriteria) == 3)
                {
                    if (int.Parse(searchcriteriavalue) == 0)
                    {
                        List<memberAccountSearchResult> list = (List<memberAccountSearchResult>)Helper.ExecuteService("MemberAccountDetails", "getXMemberList", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                    else
                    {
                        List<memberAccountSearchResult> list = (List<memberAccountSearchResult>)Helper.ExecuteService("MemberAccountDetails", "getMemberListByID", searchcriteriavalue);
                        return PartialView("_searchResult", list);
                    }
                }
                return null;

            }
        }
        public ActionResult getMemberDetailsByID(string memberID)
        {
            mMemberAccountsDetails _mMemberAccountDetails = (mMemberAccountsDetails)Helper.ExecuteService("MemberAccountDetails", "GetMemberAccountDetailsById", memberID);

            return PartialView("_memberDetails", _mMemberAccountDetails);
        }
        public ActionResult SaveContent(mMemberAccountsDetails member)
        {
            string Message = string.Empty;
            try
            {
                member.AccountNo =long.Parse(member.AccountNoS);
                Helper.ExecuteService("MemberAccountDetails", "SaveMembersAccountDetails", member);
                Message = "Sucessfully Saved";
            }
            catch (Exception ex)
            {

                Message = ex.InnerException.ToString();
            }


            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        #region Save Nominee
        public ActionResult newNominee(string memberCode)
        {
            mMemberNominee list = new mMemberNominee();
            list.MemberCode = Convert.ToInt32(memberCode);
            list.Mode = "Save";
            return PartialView("_MemberNewNominee", list);
        }
        public ActionResult saveNominee(mMemberNominee nominee)
        {
            nominee.NomineeAccountNo = long.Parse(nominee.NomineeAccountNoS);
            if (nominee.Mode == "Save")
            {
                Helper.ExecuteService("MemberAccountDetails", "saveNominee", nominee);

            }
            else
            {
                Helper.ExecuteService("MemberAccountDetails", "updateNominee", nominee);
            }
            mMemberAccountsDetails _mMemberAccountDetails = (mMemberAccountsDetails)Helper.ExecuteService("MemberAccountDetails", "GetMemberAccountDetailsById", nominee.MemberCode);
            return PartialView("_MembersNomineeList", _mMemberAccountDetails);

        }
        public ActionResult updateNominee(string nomineeID)
        {
            mMemberNominee list = (mMemberNominee)Helper.ExecuteService("MemberAccountDetails", "getNomineeDetails", nomineeID);
            list.Mode = "Update";
            return PartialView("_MemberNewNominee", list);
        }
        public ActionResult deleteNominee(string nomineeID)
        {
            int memberCode = (int)Helper.ExecuteService("MemberAccountDetails", "deleteNominee", nomineeID);
            mMemberAccountsDetails _mMemberAccountDetails = (mMemberAccountsDetails)Helper.ExecuteService("MemberAccountDetails", "GetMemberAccountDetailsById", memberCode);
            return PartialView("_MembersNomineeList", _mMemberAccountDetails);
        }
        #endregion

        #region Download Excel
          [HttpPost]
        public ActionResult DownloadExcel()
        {
            List<memberAccountSearchResult> list = (List<memberAccountSearchResult>)Helper.ExecuteService("MemberAccountDetails", "getCurrentMemberList", null);

            string Result = this.GetMemberListData(list);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=MemberList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }

          [HttpPost]
          public ActionResult DownloadExcelAll()
          {
              List<memberAccountSearchResult> list = (List<memberAccountSearchResult>)Helper.ExecuteService("MemberAccountDetails", "getCurrentMemberListAll", null);

              string Result = this.GetMemberListData(list);

              Result = HttpUtility.UrlDecode(Result);
              Response.Clear();
              Response.AddHeader("content-disposition", "attachment;filename=MemberList.xls");
              Response.Charset = "";
              Response.ContentType = "application/excel";
              Response.Write(Result);
              Response.Flush();
              Response.End();
              return new EmptyResult();
          }

          public string GetMemberListData(List<memberAccountSearchResult> model)
        {

            StringBuilder ReplyList = new StringBuilder();

            if (model != null && model.Count() > 0)
            {
                ReplyList.Append(string.Format("<div style='text-align:center;'><h2>List of Members</h2></div>"));

                ReplyList.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                ReplyList.Append("<thead class='header' ><tr style='background-color: #428bca ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:30px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px;'>SR. NO.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Name</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Designation</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Address</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Mobile Number</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Account Number</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >IFSC Code</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Bank Name</th>");
                ReplyList.Append("</tr></thead>");


                ReplyList.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    ReplyList.Append("<tr>");
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", count));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.memberName));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.designation));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.Address));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.contactNumber));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.AccountNumber));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.IFSCCode));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.BankName));
                    ReplyList.Append("</tr>");
                }
                ReplyList.Append("</tbody></table></div> ");
            }
            else
            {
                ReplyList.Append("No Member Found");
            }
            return ReplyList.ToString();
        }
        #endregion
    }
}
