﻿using SBL.DomainModel.Models.JobsPosting;
using SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Controllers
{
    public class CandidatesController : Controller
    {
        //
        // GET: /Accounts/Candidates/

        public ActionResult Index()
        {
            var GetAllActivePost = (List<Candidates>)Helper.ExecuteService("Job", "GetAllActivePostForCandidate", null);
            ViewBag.ActivePost = new SelectList(GetAllActivePost, "PostId", "PostName"); 
            var model = (List<Candidates>)Helper.ExecuteService("Job", "GetAllCandidates", null);
            return PartialView("/Areas/Accounts/Views/Candidates/_Index.cshtml", model);
        }

        public ActionResult AdmitCard()
        {
            var model = (List<Candidates>)Helper.ExecuteService("Job", "GetAllApprovedCandidates", null);
            return PartialView("/Areas/Accounts/Views/Candidates/_AdmitEntryList.cshtml", model);
        }

        public ActionResult GetPostCandidates(int Id)
        {
            //int PostId = Convert.ToInt16(Id);
            var model = (List<Candidates>)Helper.ExecuteService("Job", "GetAllCandidatesForPost", new Candidates { postId = Id });
            return PartialView("/Areas/Accounts/Views/Candidates/_CandidatesList.cshtml", model);
        }
        public ActionResult GetAllPostCandidates()
        {
            var model = (List<Candidates>)Helper.ExecuteService("Job", "GetAllCandidates", null);
            return PartialView("/Areas/Accounts/Views/Candidates/_CandidatesList.cshtml", model);
        }

        public ActionResult SaveCandidate(int CandidateId, bool IsApprove, string Reason)
        {
            try
            {
                Candidates model = (Candidates)Helper.ExecuteService("Job", "GetCandidatesById", new Candidates { CandidatesID = CandidateId });
                model.IsApprove = IsApprove;
                model.Reason = Reason;
                model.ApporvedOn = DateTime.Now;
                Helper.ExecuteService("Job", "UpdateCandidates", model);
                ViewBag.Msg = "Sucessfully updated";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";

                var ActiveList = (List<Candidates>)Helper.ExecuteService("Job", "GetAllCandidates", null);
                return PartialView("/Areas/Accounts/Views/Candidates/_CandidatesList.cshtml", ActiveList);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public ActionResult LockUnlockCandidateDetails(int CandidateId, bool islock)
        {
            try
            {
                Candidates model = new Candidates();
                model.CandidatesID = CandidateId;
                model.IsAdmitCardDetailsLocked = islock;
                Helper.ExecuteService("Job", "LockUnlockCandidateDetails", model);
                var ActiveList = (List<Candidates>)Helper.ExecuteService("Job", "GetAllApprovedCandidates", null);
                return PartialView("/Areas/Accounts/Views/Candidates/_ApprovedCandidatesList.cshtml", ActiveList);
                //return Content("saved");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult SaveAdmitCardDetails(tCandidateAdmitDetails model)
        {
            try
            {
                //tCandidateAdmitDetail model = new tCandidateAdmitDetail();
                //model.ID = PID;
                //model.CandidateID = CandidateID;int CandidateID, string RollNo, string ExamVenue, string ExamDate, string ExamTime, int PID
                //model.RollNo = RollNo;
                //model.ExamVenue = ExamVenue;
                //model.ExamDate = ExamDate;
                //model.ExamTime = ExamTime;
                Helper.ExecuteService("Job", "AddAdmitCardDetails", model);
                var ActiveList = (List<Candidates>)Helper.ExecuteService("Job", "GetAllApprovedCandidates", null);
                return PartialView("/Areas/Accounts/Views/Candidates/_ApprovedCandidatesList.cshtml", ActiveList);               
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public ActionResult PopulateCandidates(int Id)
        {
            Candidates model = (Candidates)Helper.ExecuteService("Job", "GetCandidatesById", new Candidates { CandidatesID = Id });
            return PartialView("/Areas/Accounts/Views/Candidates/_PopulateCandidates.cshtml", model);
        }



        public ActionResult PopulateAdmitCard(int Id, string view)
        {
            // Candidates model = (Candidates)Helper.ExecuteService("Job", "GetCandidatesById", new Candidates { CandidatesID = Id });
            ViewBag.IsOnlyView = view;
            tCandidateAdmitDetails model = new tCandidateAdmitDetails();
            model.CandidateID = Id;

            Candidates model1 = (Candidates)Helper.ExecuteService("Job", "GetCandidatesById", new Candidates { CandidatesID = Id });
            if (model1.IsAdmitCardDetailsEntered == false)
            {
                return PartialView("/Areas/Accounts/Views/Candidates/_PopulateAdmitCard.cshtml", model);
            }
            else
            {
                tCandidateAdmitDetails model2 = (tCandidateAdmitDetails)Helper.ExecuteService("Job", "GetAdmitCardDetailsById", new tCandidateAdmitDetails { CandidateID = Id });
                //model.PID = model2.ID;
                //model.CandidateID = model2.CandidateID;
                //model.ExamDate = model2.ExamDate;
                //model.ExamTime = model2.ExamTime;
                //model.ExamVenue = model2.ExamVenue;
                //model.IsAdmit = model2.IsAdmit;
                //model.RollNo = model2.RollNo;
                return PartialView("/Areas/Accounts/Views/Candidates/_PopulateAdmitCard.cshtml", model2);
            }
           // return null;
        }

        public ActionResult CandidateDetail(int Id)
        {
            CandidateModel model = new CandidateModel();
            model.candidate = (Candidates)Helper.ExecuteService("Job", "GetCandidatesById", new Candidates { CandidatesID = Id });
            model.ExperiencesDetails = (List<ExperiencesDetails>)Helper.ExecuteService("Job", "GetAllExperiencesDetails", new ExperiencesDetails { PostId = Id });
            model.ExamDetailsList = (List<ExamDetails>)Helper.ExecuteService("Job", "GetAllExamDetails", new ExamDetails { PostId = Id });
            ViewBag.PostName = (string)Helper.ExecuteService("Job", "GetCandidatesPostById", new Candidates { postId = model.candidate.postId });
            return PartialView("/Areas/Accounts/Views/Candidates/_CandidateDetail.cshtml", model);
        }

    }
}
