﻿using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.salaryhead;
using SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Controllers
{
    public class printSalaryController : Controller
    {
        //
        // GET: /Accounts/printSalary/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult printMembersSalary()
        {
            return View();
        }

        public ActionResult PrintSpeakerSalary()
        {
            return View();
        }

        public ActionResult GetMemberList()
        {
            var memberList = (List<mMember>)Helper.ExecuteService("salaryheads", "getallmembers", null);
            var mmlist = memberList.ToViewModelsalarymembers();
            foreach (var item in mmlist)
            {
                item.catId = getCategoryID(item.MemberId); ;
            }
            return PartialView("memberslist", mmlist);
        }

        public ActionResult GetspeakerList()
        {
            var memberList = (List<mMember>)Helper.ExecuteService("salaryheads", "getspeakerList", null);
            var mmlist = memberList.ToViewModelsalarymembers();
            foreach (var item in mmlist)
            {
                item.catId = getCategoryID(item.MemberId); ;
            }
            return PartialView("memberslist", mmlist);
        }

        public int getCategoryID(int mMemberId)
        {
            var obj = Helper.ExecuteService("salaryheads", "getCatID", mMemberId);
            return Convert.ToInt32(obj);
        }

        public ActionResult getSalaryHeadsList(string memberid, string month, string year)
        {
            string[] arr = { memberid, month, year };
            string MemberName = "";
            List<membersSalary> mHeadList = (List<membersSalary>)Helper.ExecuteService("salaryheads", "getMonthlySalaryHeads", arr);
            membersMonthlySalaryDetails mmss = (membersMonthlySalaryDetails)Helper.ExecuteService("salaryheads", "getMonthlySalarydetails", arr);
            if (mHeadList != null && mmss != null)
            {
                printsalary ps = new printsalary();
                ps.membersSalary = mHeadList;
                ps.membersMonthlySalaryDetails = mmss;

                ps.Designation = Helper.ExecuteService("salaryheads", "getDesignation", arr[0]) as string;
                ps.MemberName = Helper.ExecuteService("salaryheads", "getMemberName", arr[0]) as string;
                return PartialView("getmemberssalaryheads", ps);
            }
            else
            {
                MemberName = Helper.ExecuteService("salaryheads", "getMemberName", arr[0]) as string;
                return Json(" sorry no data found for <b>" + MemberName + "</b> Month of <b>" + string.Format("{0:MMMM}", Convert.ToInt32(month)) + " " + year + "</b>", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult printsalary(string memberid, string month, string year)
        {
            // var a = ViewContext.RouteData.Values["action"];

            string[] arr = { memberid, month, year };
            List<membersSalary> mHeadList = (List<membersSalary>)Helper.ExecuteService("salaryheads", "getMonthlySalaryHeads", arr);
            membersMonthlySalaryDetails mmss = (membersMonthlySalaryDetails)Helper.ExecuteService("salaryheads", "getMonthlySalarydetails", arr);

            printsalary ps = new printsalary();
            ps.membersSalary = mHeadList;
            ps.membersMonthlySalaryDetails = mmss;
            ps.MemberName = Helper.ExecuteService("salaryheads", "getMemberName", arr[0]) as string;
            ps.Designation = Helper.ExecuteService("salaryheads", "getDesignation", arr[0]) as string;
            return View(ps);
        }
    }
}