﻿using Email.API;
using EvoPdf;
using Ionic.Zip;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.salaryhead;
using SBL.DomainModel.Models.User;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SMS.API;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Accounts.Controllers
{
    public class salaryController : Controller
    {
        //
        // GET: /Accounts/salary/

        #region salary Heads

        public JsonResult fillOrderNo(string hType, string Mode)
        {
            List<salaryHeadList> _list = (List<salaryHeadList>)Helper.ExecuteService("salaryheads", "getOrderNo", hType);

            salaryHeadList _addItem = new salaryHeadList();

            if (_list.Count > 0)
            {
                if (Mode == "save")
                {
                    _addItem.sHeadID = _list[0].sHeadID + 1;
                    _list.Insert(0, _addItem);
                }
            }
            else
            {
                _addItem.sHeadID = 1;
                _list.Insert(0, _addItem);
            }

            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ManageHeads()
        {
            return View();
        }

        public ActionResult SalaryHeads()
        {
            // List<salaryheads> headList = (List<salaryheads>)Helper.ExecuteService("salaryheads",
            // "getSalaryHeadList", null);
            return View();
        }

        public ActionResult SalaryHeadsPartial()
        {
            // List<salaryheads> headList = (List<salaryheads>)Helper.ExecuteService("salaryheads",
            // "getSalaryHeadList", null);
            return PartialView("_SalaryHeads");
        }

        public ActionResult getSHeadList()
        {
            List<salaryheads> headList = (List<salaryheads>)Helper.ExecuteService("salaryheads", "getSalaryHeadList", null);
            return PartialView("_sHeadList", headList);
        }

        public ActionResult createNewSHeads()
        {
            salaryheads _slist = new salaryheads();
            return PartialView("_createNewSHeads", _slist);
        }

        public ActionResult getSHeadsByID(string sHeadID)
        {
            salaryheads _sHeads = (salaryheads)Helper.ExecuteService("salaryheads", "getSHeadByID", sHeadID);
            // return Json(_sHeads, JsonRequestBehavior.AllowGet);
            return PartialView("_updateSalaryHeads", _sHeads);
        }

        [HttpPost]
        public ActionResult updateSalaryHeads(salaryheads _salaryHeads)
        {
            salaryheads _salaryHead = new salaryheads();
            _salaryHead.sHeadName = _salaryHeads.sHeadName;
            _salaryHead.amountType = _salaryHeads.amountType;
            _salaryHead.hType = _salaryHeads.hType;
            _salaryHead.perc = _salaryHeads.perc;
            _salaryHead.salaryComponentId = _salaryHeads.salaryComponentId;
            _salaryHead.orderNo = _salaryHeads.orderNo;
            _salaryHead.Status = _salaryHeads.Status;
            Helper.ExecuteService("salaryheads", "updateSalaryHeads", _salaryHeads);
            List<salaryheads> headList = (List<salaryheads>)Helper.ExecuteService("salaryheads", "getSalaryHeadList", null);
            return View("SalaryHeads", headList);
        }

        public JsonResult updateHead(int catID, int sHeadID, int amount, bool status)
        {
            try
            {
                string[] arr = { catID.ToString(), sHeadID.ToString() };
                membersHead mhead = (membersHead)Helper.ExecuteService("salaryheads", "getSalaryHeadByID", arr);
                if (mhead != null)
                {
                    mhead.amount = amount;
                    mhead.status = status;
                    Helper.ExecuteService("salaryheads", "updateMembersHeads", mhead);
                }
            }
            catch
            {
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult deleteSHeadsByID(string sHeadID)
        {
            bool check = (bool)Helper.ExecuteService("salaryheads", "deleteSHeadsByID", sHeadID);
            return Json(check, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult saveSalaryHeads(salaryheads _salaryHeads)
        {
            salaryheads _salaryHead = new salaryheads();
            _salaryHead.sHeadName = _salaryHeads.sHeadName;
            _salaryHead.amountType = _salaryHeads.amountType;
            _salaryHead.hType = _salaryHeads.hType;
            _salaryHead.perc = _salaryHeads.perc;
            _salaryHead.salaryComponentId = _salaryHeads.salaryComponentId;
            _salaryHead.orderNo = _salaryHeads.orderNo;
            _salaryHead.Status = _salaryHeads.Status;
            Helper.ExecuteService("salaryheads", "saveSalaryHeads", _salaryHeads);
            List<salaryheads> headList = (List<salaryheads>)Helper.ExecuteService("salaryheads", "getSalaryHeadList", null);
            return View("SalaryHeads", headList);
        }

        public JsonResult fillSalaryComponent()
        {
            List<salaryHeadList> _list = (List<salaryHeadList>)Helper.ExecuteService("salaryheads", "getActiveSalaryHeadList", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        #endregion salary Heads

        public ActionResult Test(int i = 835)
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SalaryMaster()
        {
            return View();
        }

        public ActionResult SalaryMasterPartial()
        {
            return PartialView("_SalaryMaster");
        }

        public ActionResult generateSpeakerSalary()
        {
            return View();
        }

        public ActionResult GetList()
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            var memberList = (List<mMember>)Helper.ExecuteService("salaryheads", "getallmembers", assemblyid);
            var mmlist = memberList.ToViewModelsalarymembers();
            foreach (var item in mmlist)
            {
                item.catId = getCategoryID(item.MemberId); ;
            }
            return PartialView("memberslist", mmlist);
        }

        public ActionResult GetspeakerList()
        {
            var memberList = (List<mMember>)Helper.ExecuteService("salaryheads", "getspeakerList", null);
            var mmlist = memberList.ToViewModelsalarymembers();
            foreach (var item in mmlist)
            {
                item.catId = getCategoryID(item.MemberId); ;
            }
            return PartialView("memberslist", mmlist);
        }

        public int getCategoryID(int mMemberId)
        {
            var obj = Helper.ExecuteService("salaryheads", "getCatID", mMemberId);
            return Convert.ToInt32(obj);
        }

        public JsonResult getCategoryName(int catID)
        {
            var obj = Helper.ExecuteService("salaryheads", "getCatName", catID);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        private bool checksalary(int MemberId, int[] monthyear, int assemblyid, int ispart, int catid)
        {
            int[] arr = new int[] { MemberId, monthyear[0], monthyear[1], assemblyid, ispart, catid };

            int i = (int)Helper.ExecuteService("salaryheads", "checksalary", arr);
            if (i > 0)
            {
                return false;
            }
            return true;
        }

        public JsonResult getWords(int number)
        {
            string str = number.NumbersToWords();
            return Json(str, JsonRequestBehavior.AllowGet);
        }

        public JsonResult fillCurrentMember()
        {
            List<fillCurrentMamber> list = (List<fillCurrentMamber>)Helper.ExecuteService("salaryheads", "getCurrentMemberList", CurrentSession.AssemblyId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public int[] getMonthYear(int memberID)
        {
            // get last month n year
            int month = 0; int year = 0;
            membersMonthlySalaryDetails mdetails = (membersMonthlySalaryDetails)Helper.ExecuteService("salaryheads", "checkMonthYear", memberID);
            if (mdetails != null)
            {
                month = mdetails.month;
                year = mdetails.year;
                if (!((month == DateTime.Now.Month) && (year == DateTime.Now.Year)))
                {
                    if (month >= 12)
                    {
                        month = 1;
                        year++;
                    }
                    else
                    {
                        month++;
                    }
                }
            }
            else
            {
                month = DateTime.Now.Month;
                year = DateTime.Now.Year;
            }
            return new int[] { month, year };
        }

        public ActionResult getSalaryHeadsList(string memberid, string catId, string month, string year, int ispart)
        {
            salarystructure _salarystructure = null;
            // int[] monthyear = getMonthYear(int.Parse(memberid));
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] monthyear = { int.Parse(month), int.Parse(year) };
            int catid = Convert.ToInt16(catId);
            if (checksalary(int.Parse(memberid), monthyear, assemblyid, ispart, catid))
            {
                int[] arr = new int[] { int.Parse(memberid), int.Parse(catId), monthyear[0], monthyear[1], assemblyid, ispart };
                _salarystructure = (salarystructure)Helper.ExecuteService("salaryheads", "getMembersHeads", arr);
                _salarystructure.monthID = monthyear[0];
                _salarystructure.yearId = monthyear[1];
                _salarystructure.netSalaryInWords = Convert.ToInt32(_salarystructure.netSalary).NumbersToWords();
            }
            else
            {
                int[] arr = new int[] { int.Parse(memberid), monthyear[0], monthyear[1], assemblyid, ispart, catid };
                _salarystructure = (salarystructure)Helper.ExecuteService("salaryheads", "getMonthlySalaryHeads", arr);
                _salarystructure.netSalaryInWords = Convert.ToInt32(_salarystructure.netSalary).NumbersToWords();
            }
            _salarystructure.ispart = ispart;
            return PartialView("_salaryHeadsForMember", _salarystructure);
        }

        public ActionResult SalaryHeadsList(string memberid, string catId, string month, string year, int ispart)
        {
            salarystructure _salarystructure = null;
            // int[] monthyear = getMonthYear(int.Parse(memberid));
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int catid = Convert.ToInt16(catId);
            int[] monthyear = { int.Parse(month), int.Parse(year) };


            int[] arr = new int[] { int.Parse(memberid), monthyear[0], monthyear[1], assemblyid, ispart, catid };
            _salarystructure = (salarystructure)Helper.ExecuteService("salaryheads", "getMonthlySalaryHeads", arr);
            _salarystructure.netSalaryInWords = Convert.ToInt32(_salarystructure.netSalary).NumbersToWords();

            return PartialView("_salaryHeadsForMember", _salarystructure);
        }
        public JsonResult getMemberName(int memberId)
        {
            string obj = Helper.ExecuteService("salaryheads", "getMemberName", memberId) as string;
            // string [] str=(string[])Helper.ExecuteService("salaryheads", "getMemberName", memberId);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getDesignation(int memberId)
        {
            string obj = Helper.ExecuteService("salaryheads", "getDesignation", memberId) as string;
            // string [] str=(string[])Helper.ExecuteService("salaryheads", "getMemberName", memberId);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ManageCategory()
        {
            return View();
        }

        public ActionResult getCategoryList()
        {
            var categoryList = (List<memberCategory>)Helper.ExecuteService("salaryheads", "getCategoryList", null);
            return PartialView("getCategoryList", categoryList);
        }

        private bool checkList(int catID)
        {
            int i = (int)Helper.ExecuteService("salaryheads", "checkHeadList", catID);
            if (i > 0)
            {
                return false;
            }
            return true;
        }

        public ActionResult getHeadList(int designationID)
        {
            List<salaryheads> headList = null;
            if (checkList(designationID))
            {
                headList = (List<salaryheads>)Helper.ExecuteService("salaryheads", "getSalaryHeadList", designationID);
                foreach (var i in headList)
                {
                    if (i.amountType == "Fixed")
                    {
                        i.Fixed = true;
                    }
                    else
                    {
                        i.Fixed = false;
                    }
                    i.Status = true;
                    i.membersCatID = designationID;
                    i.Mode = "save";
                }
            }
            else
            {
                headList = (List<salaryheads>)Helper.ExecuteService("salaryheads", "getSalaryHeadListfrommembersalary", designationID);
                foreach (var i in headList)
                {
                    i.Mode = "update";
                }
            }

            return PartialView("_salaryHeadList", headList);
        }

        public ActionResult getAssociateComponents(string sHeadID)
        {
            List<salaryComponentList> list = (List<salaryComponentList>)Helper.ExecuteService("salaryheads", "getAssociateComponents", sHeadID);

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult printsalary(string memberid, string month, string year)
        {
            // var a = ViewContext.RouteData.Values["action"];
            string[] arr = { memberid, month, year };
            List<membersSalary> mHeadList = (List<membersSalary>)Helper.ExecuteService("salaryheads", "getMonthlySalaryHeads", arr);
            membersMonthlySalaryDetails mmss = (membersMonthlySalaryDetails)Helper.ExecuteService("salaryheads", "getMonthlySalarydetails", arr);

            printsalary ps = new printsalary();
            ps.membersSalary = mHeadList;
            ps.membersMonthlySalaryDetails = mmss;
            ps.MemberName = Helper.ExecuteService("salaryheads", "getMemberName", arr[0]) as string;
            ps.Designation = Helper.ExecuteService("salaryheads", "getDesignation", arr[0]) as string;
            return View(ps);
        }

        [HttpPost]
        public ActionResult saveCatHeads(List<salaryheads> catHeads)
        {
            if (ModelState.IsValid)
            {
                foreach (salaryheads sheads in catHeads)
                {
                    membersHead mheads = new membersHead();
                    mheads.amount = sheads.amount.HasValue ? sheads.amount : 0;
                    mheads.membersCatId = sheads.membersCatID;
                    mheads.sHeadId = sheads.sHeadId;
                    mheads.status = sheads.Status;
                    mheads.htype = sheads.hType;
                    mheads.Fixed = sheads.Fixed;
                    mheads.amountType = sheads.amountType;
                    if (sheads.amountType == "Fixed")
                    {
                        mheads.Fixed = true;
                    }
                    if (sheads.Mode == "save")
                    {
                        Helper.ExecuteService("salaryheads", "saveMembersHeads", mheads);
                    }
                    if (sheads.Mode == "update")
                    {
                        string[] arr = { sheads.membersCatID.ToString(), sheads.sHeadId.ToString() };
                        membersHead mhead = (membersHead)Helper.ExecuteService("salaryheads", "getSalaryHeadByID", arr);
                        if (mhead != null)
                        {
                            mhead.amount = sheads.amount;
                            mhead.status = sheads.Status;
                            mheads.Fixed = sheads.Fixed;
                            mheads.amountType = sheads.amountType;
                            Helper.ExecuteService("salaryheads", "updateMembersHeads", mhead);
                        }
                        else
                        {
                            mheads.amount = sheads.amount.HasValue ? sheads.amount : 0;
                            mheads.membersCatId = sheads.membersCatID;
                            mheads.sHeadId = sheads.sHeadId;
                            mheads.status = sheads.Status;
                            mheads.htype = sheads.hType;
                            mheads.Fixed = sheads.Fixed;
                            mheads.amountType = sheads.amountType;
                            Helper.ExecuteService("salaryheads", "saveMembersHeads", mheads);
                        }
                    }
                }
            }
            else
            {
                ModelState.AddModelError("", "error in code");
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // saving members salary
        [HttpPost]
        public JsonResult savesalary(List<membersHead> membershaed, FormCollection frm)
        {
            int memberid = 0;
            if (ModelState.IsValid)
            {
                if (TempData["exist"].ToString() == "0")
                {
                    var total_allowances = frm["total_allowances"];
                    var total_deductions = frm["total_deductions"];
                    var total = frm["total"];
                    foreach (membersHead sheads in membershaed)
                    {
                        membersSalary ms = new membersSalary();
                        ms.amount = Convert.ToDouble(sheads.amount);
                        ms.headId = sheads.sHeadId;
                        ms.headtype = sheads.htype;
                        ms.membersId = sheads.memberId;
                        memberid = sheads.memberId;
                        ms.monthID = DateTime.Now.Month;
                        ms.Year = DateTime.Now.Year;
                        ms.createdDate = DateTime.Now.Date;
                        ms.CreatedBy = 1;
                        Helper.ExecuteService("salaryheads", "savesalary", ms);
                    }

                    membersMonthlySalaryDetails msdetails = new membersMonthlySalaryDetails();
                    msdetails.createdBy = 1;
                    msdetails.createdDate = DateTime.Now.Date;
                    msdetails.grandtotal = double.Parse(total);
                    msdetails.membersid = memberid;
                    msdetails.month = DateTime.Now.Month;
                    msdetails.year = DateTime.Now.Year;
                    msdetails.totalAllowances = double.Parse(total_allowances);
                    msdetails.totalAGDeductionsA = double.Parse(total_deductions);
                    msdetails.totalTODeducationsB = 0;

                    Helper.ExecuteService("salaryheads", "savesalarydetails", msdetails);
                }
            }
            else
            {
                ModelState.AddModelError("", "error in code");
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        #region YearllyStatement
        public ActionResult YearlySalaryStatement()
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            var MemberList = (List<mMember>)Helper.ExecuteService("salaryheads", "GetAllVSMembers", assemblyid);
            var mmlist = MemberList.ToViewModelsalarymembers1();
            ViewBag.memberList = mmlist;

            return PartialView("YearlySalaryStatement");
        }
        public ActionResult YearlySalaryStatementPartial()
        {
            return PartialView("YearlySalaryStatementPartial");
        }


        public ActionResult _MemberSalaryYearlyStatement(string MemberId, string FinancialYear, String CategoryId)
        {
            string[] FYear = FinancialYear.Split('-');
            string FYearStart = FYear[0];
            string FYearEnd = FYear[1];
            int Categoryid = Convert.ToInt32(CategoryId);
            ViewBag.MemId = MemberId;            
            
            // List<membersOuterReport> _membersOuterReport1 = new List<membersOuterReport>();
            int count = 1;
            int count2 = 11;
            List<membersOuterReport> _membersOuterReport = new List<membersOuterReport>();
            for (var i = 3; i <= 12; i++)
            {
                int monthid = i;
               
                int[] arr = new int[] { Convert.ToInt32(MemberId), Convert.ToInt32(monthid), Convert.ToInt32(FYearStart), count, Categoryid };
                var C = (List<membersOuterReport>)Helper.ExecuteService("salaryheads", "generateFinancialYearSalaryForMembers", arr);
                count++;
                if (monthid == 3)
                {
                    _membersOuterReport.Add(C[0]);
                    _membersOuterReport.Add(C[1]);
                }
                else if (C[0].monthID == null)
                {
                    // _membersOuterReport.Add(C[1]);
                }
                else
                {
                    _membersOuterReport.Add(C[1]);
                }
            }
            for (var i = 1; i < 3; i++)
            {

                int monthid = i;
                int[] arr = new int[] { Convert.ToInt32(MemberId), Convert.ToInt32(monthid), Convert.ToInt32(FYearEnd), count2, Categoryid };
                var C = (List<membersOuterReport>)Helper.ExecuteService("salaryheads", "generateFinancialYearSalaryForMembers", arr);
                count2++;
                if (C[0].monthID == null)
                {
                    // _membersOuterReport.Add(C[1]);
                }
                else if (C[1].columnName.Contains("Total"))
                {
                    // _membersOuterReport.Add(C[1]);
                }
                else
                {
                    _membersOuterReport.Add(C[1]);
                }
            }
            return PartialView("_MemberSalaryYearlyStatement", _membersOuterReport);
        }
        #endregion

        #region salary Bills

        //public ActionResult getSpeakerCurrentList(string designationID,string month,string year)
        //{
        //    List<mMember> _list = (List<mMember>)Helper.ExecuteService("salaryheads", "getDetailsByDesignation", designationID);

        //    List<SpeakerList> _sList = new List<SpeakerList>();
        //    foreach (var item in _list)
        //    {
        //        SpeakerList _speaker = new SpeakerList();
        //        _speaker.speakerID = item.MemberCode;
        //        _speaker.speakerName = item.Name;
        //        _sList.Add(_speaker);
        //    }
        //    return Json(_sList, JsonRequestBehavior.AllowGet);
        //}


        public ActionResult getSpeakerCurrentList(string designationID, string month, string year)
        {
            int AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            string[] arr = { designationID, Convert.ToString(AssemblyId) };
            List<mMember> _list = (List<mMember>)Helper.ExecuteService("salaryheads", "getDetailsByDesignation", arr);

            List<SpeakerList> _sList = new List<SpeakerList>();
            foreach (var item in _list)
            {
                SpeakerList _speaker = new SpeakerList();
                _speaker.speakerID = item.MemberCode;
                _speaker.speakerName = item.Name;
                _sList.Add(_speaker);
            }
            return Json(_sList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SalaryBills()
        {
            return View();
        }

        public ActionResult SalaryBillsPartial()
        {
            return PartialView("_SalaryBills");
        }

        public ActionResult generateBills(salarystructure _salarystructure, List<salarystructure> _salarystructureList, string Command)
        {
            int AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            if (ModelState.IsValid)
            {
                if (Command == "members")
                {
                    
                    if (_salarystructure.Mode == "save")
                    {
                        //Check If Salary Has been already saved
                        membersSalary mems = new membersSalary();
                        mems.membersId = _salarystructure.memberCode;
                        mems.monthID = _salarystructure.monthID;
                        mems.Year = _salarystructure.yearId;
                        mems.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                        mems.ispart = _salarystructure.ispart;
                        mems.catID = _salarystructure.catID;
                        // Helper.ExecuteService("salaryheads", "savesalary", mems);
                        bool CheckSalary = (bool)Helper.ExecuteService("salaryheads", "CheckSalary", mems);
                        ///
                        if (CheckSalary == true)
                        {
                            //salary exist

                        }
                        else
                        {
                            foreach (HeadList sheads in _salarystructure.headList)
                            {
                                membersSalary ms = new membersSalary();
                                ms.amount = Convert.ToDouble(sheads.amount);
                                ms.headId = sheads.headID;
                                ms.headtype = sheads.htype;
                                ms.membersId = _salarystructure.memberCode;
                                ms.monthID = _salarystructure.monthID;
                                ms.Year = _salarystructure.yearId;
                                ms.createdDate = DateTime.Now.Date;
                                ms.CreatedBy = 1;
                                ms.loanNumber = sheads.loanNumber;
                                ms.catID = _salarystructure.catID;
                                ms.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                                ms.ispart = _salarystructure.ispart;
                                Helper.ExecuteService("salaryheads", "savesalary", ms);

                            }



                            membersMonthlySalaryDetails msdetails = new membersMonthlySalaryDetails();
                            msdetails.createdBy = 1;
                            msdetails.createdDate = DateTime.Now.Date;
                            msdetails.grandtotal = _salarystructure.netSalary;
                            msdetails.membersid = _salarystructure.memberCode;
                            msdetails.month = _salarystructure.monthID;
                            msdetails.year = _salarystructure.yearId;
                            msdetails.totalAllowances = _salarystructure.totalAllowances;
                            msdetails.totalAGDeductionsA = _salarystructure.totalDeduction;
                            msdetails.GenerateStatus = 0;
                            msdetails.SOStatus = 0;
                            msdetails.totalTODeducationsB = 0;
                            msdetails.catID = _salarystructure.catID;
                            msdetails.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                            msdetails.Ispart = _salarystructure.ispart;
                            Helper.ExecuteService("salaryheads", "UpdateLoan", new int[] { _salarystructure.monthID, _salarystructure.yearId, _salarystructure.memberCode, AssemblyId });
                            Helper.ExecuteService("salaryheads", "savesalarydetails", msdetails);

                        }
                    }
                    else
                    {
                        if (_salarystructure.Mode == "update")
                        {
                            _salarystructure.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                            Helper.ExecuteService("salaryheads", "updateSalaryBills", _salarystructure);
                            Helper.ExecuteService("salaryheads", "UpdateLoan", new int[] { _salarystructure.monthID, _salarystructure.yearId, _salarystructure.memberCode, AssemblyId });
                            //Helper.ExecuteService("salaryheads", "UpdateLoan", new int[] { _salarystructure.monthID, _salarystructure.yearId });
                        }
                    }
                }
                else
                {
                    if (Command == "AllMembers")
                    {
                        generateBillsforAllMembers(_salarystructureList);
                    }
                }
            }
            else
            {
                ModelState.AddModelError("", "error in code");
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult generateBillsforAllMembers(List<salarystructure> _salarystructureList)
        {
            if (ModelState.IsValid)
            {
                foreach (var _salarystructure in _salarystructureList)
                {
                    if (_salarystructure.Mode == "save")
                    {
                        foreach (HeadList sheads in _salarystructure.headList)
                        {
                            membersSalary ms = new membersSalary();
                            ms.amount = Convert.ToDouble(sheads.amount);
                            ms.headId = sheads.headID;
                            ms.headtype = sheads.htype;
                            ms.membersId = _salarystructure.memberCode;
                            ms.monthID = _salarystructure.monthID;
                            ms.Year = _salarystructure.yearId;
                            ms.createdDate = DateTime.Now.Date;
                            ms.catID = _salarystructure.catID;
                            ms.CreatedBy = 1;
                            ms.loanNumber = sheads.loanNumber;
                            Helper.ExecuteService("salaryheads", "savesalary", ms);
                        }

                        membersMonthlySalaryDetails msdetails = new membersMonthlySalaryDetails();
                        msdetails.catID = _salarystructure.catID;
                        msdetails.createdBy = 1;
                        msdetails.createdDate = DateTime.Now.Date;
                        msdetails.grandtotal = _salarystructure.netSalary;
                        msdetails.membersid = _salarystructure.memberCode;
                        msdetails.month = _salarystructure.monthID;
                        msdetails.year = _salarystructure.yearId;
                        msdetails.totalAllowances = _salarystructure.totalAllowances;
                        msdetails.totalAGDeductionsA = _salarystructure.totalDeduction;
                        msdetails.totalTODeducationsB = 0;
                        msdetails.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                        Helper.ExecuteService("salaryheads", "UpdateLoan", new int[] { _salarystructure.monthID, _salarystructure.yearId });
                        Helper.ExecuteService("salaryheads", "savesalarydetails", msdetails);
                    }
                    else
                    {
                        if (_salarystructure.Mode == "update")
                        {
                            Helper.ExecuteService("salaryheads", "UpdateLoan", new int[] { _salarystructure.monthID, _salarystructure.yearId });
                            Helper.ExecuteService("salaryheads", "updateSalaryBills", _salarystructure);
                        }
                    }
                }
            }
            else
            {
                ModelState.AddModelError("", "error in code");
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult getNumberToWords(string number)
        {
            int num = int.Parse(number);
            if (Request.IsAjaxRequest())
            {
                return Json(num.NumbersToWords(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Content(num.NumbersToWords());
            }
        }

        // _requestedBillsList
        public ActionResult RequestedBills()
        {
            return View();
        }

        public ActionResult RequestedBillsPartial()
        {
            return PartialView("_RequestedBills");
        }

        public ActionResult PrintSalaryBills()
        {
            return View();
        }

        public ActionResult PrintSalaryBillsPartial()
        {
            return PartialView("_PrintSalaryBills");
        }

        public ActionResult PrintSalaryBillsPartialForMember()
        {
            return PartialView("PrintSalaryBillsPartialForMember");
        }

        public ActionResult getBillListformember(string monthID, String YearID)
        {
            int[] arr = { int.Parse(monthID), int.Parse(YearID), int.Parse(CurrentSession.MemberCode) };
            List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "getSalaryBillListForMember", arr);
            return PartialView("_salaryBillListForMember", _slist);
        }

        //public ActionResult getBillList(string monthID, String YearID, string status)
        //{
        //    int[] arr = { int.Parse(monthID), int.Parse(YearID), int.Parse(status) };
        //    List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "getSalaryBillList", arr);
        //    return PartialView("_salaryBillList", _slist);
        //}
        public ActionResult getBillList(string monthID, String YearID, string status, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] arr = { int.Parse(monthID), int.Parse(YearID), int.Parse(status), assemblyid, ispart };
            List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "getSalaryBillList", arr);
            return PartialView("_salaryBillList", _slist);
        }

        public ActionResult getRequestedBillList(string monthID, String YearID, string status, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] arr = { int.Parse(monthID), int.Parse(YearID), int.Parse(status), assemblyid, ispart };
            List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "getSalaryBillList", arr);
            return PartialView("_requestedBillsList", _slist);
        }

        public ActionResult RequestedBill()
        {
            return View();
        }

        public ActionResult RequestedBillPartial()
        {
            return PartialView("_RequestedBill");
        }

        public Object generatePdf(List<salaryBillInfo> _listsbi, string mailStatus, string smsStatus, string downloadStatus)
        {
            ZipFile zip = new ZipFile();

            zip.AlternateEncodingUsage = ZipOption.AsNecessary;

            string[] returnResult = null;
            foreach (var item in _listsbi)
            {
                if (item.checkStatus != null && item.status == 1)
                {
                    salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", item.AutoID);
                    try
                    {
                        string filepath = string.Empty;
                        bool pdf = true;
                        string SavedPdfPath = string.Empty;
                        if (item.generateStatus == 1)
                        {
                            pdf = false;
                            filepath = Helper.ExecuteService("salaryheads", "getBillFilePath", item.AutoID) as string;
                        }
                        if (s.designationName == "Speaker" || s.designationName == "Deputy Speaker")
                        {
                            returnResult = GeneratePDFforSpeaker(s, pdf, filepath);
                        }
                        else
                        {
                            returnResult = GeneratePDFforMembers(s, pdf, filepath);
                        }
                        if (pdf)
                        {
                            Helper.ExecuteService("salaryheads", "updateBillFilePath", new string[] { item.AutoID.ToString(), returnResult[0] + s.monthID + "_" + s.yearId + "_" + s.memberCode + ".pdf" });
                            SavedPdfPath = returnResult[0] + s.monthID + "_" + s.yearId + "_" + s.memberCode + ".pdf";
                        }
                        else
                        {
                            SavedPdfPath = filepath;
                        }
                        SalaryNotification sn = new SalaryNotification();
                        List<string> emailAddresses = new List<string>();
                        List<string> phoneNumbers = new List<string>();
                        sn.mailSubject = "Salary statement of " + s.memberName + "(" + s.designationName + ")" + "for the month of " + new DateTime(2000, s.monthID, 1).ToString("MMMM") + " , " + s.yearId;

                        phoneNumbers.Add(s.mobileNO);
                        // phoneNumbers.Add("8350941776");
                        sn.mobileStatus = true;
                        sn.emailStatus = true;
                        if (string.IsNullOrEmpty(smsStatus))
                        {
                            sn.mobileStatus = false;
                        }
                        if (string.IsNullOrEmpty(mailStatus))
                        {
                            sn.emailStatus = false;
                        }
                        sn.msgBody = "Salary statement of " + s.memberName + "(" + s.designationName + ")" + "for the month of " + new DateTime(2000, s.monthID, 1).ToString("MMMM") + " , " + s.yearId + " . Gross Salary: Rs. " + s.totalAllowances + ", Total Deduction: Rs. " + s.totalDeduction + ", Net Pay: Rs. " + s.netSalary + ". For detailed salary statement kindly login either at http://evidhan.nic.in, your official e-mail http://mail.nic.in or Mobile App." + Environment.NewLine + "...Secretary HP Vidhan Sabha";
                        sn.attachmentUrl = "";
                        emailAddresses.Add(s.email);
                        // emailAddresses.Add("durgesh@sblsoftware.com");

                        sn.mobileList = phoneNumbers;
                        sn.emailList = emailAddresses;
                        string mailText = System.IO.File.ReadAllText(Server.MapPath("/SalaryMailer/index.html"));
                        mailText = mailText.Replace("__MemberName__", s.memberName);
                        mailText = mailText.Replace("__Designation__", s.designationName);
                        mailText = mailText.Replace("__MonthName__ ", new DateTime(1900, Convert.ToInt32(s.monthID), 1).ToString("MMMM"));
                        mailText = mailText.Replace("__year__", s.yearId.ToString());
                        mailText = mailText.Replace("__Allowances__", returnResult[1]);
                        mailText = mailText.Replace("__Deductions__", returnResult[2]);
                        mailText = mailText.Replace("__totalAllowances__", s.totalAllowances + "");
                        mailText = mailText.Replace("__totalDeductions__", s.totalDeduction + "");
                        mailText = mailText.Replace("__grossAmount__", s.netSalary + "");
                        mailText = mailText.Replace("__amountinwords__", Convert.ToInt32(s.netSalary).NumbersToWords() + " Only");
                        sn.mailBody = mailText;
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = FileSettings.SettingValue + SavedPdfPath;
                        EAttachment ea = new EAttachment
                        {
                            FileName = new
                                FileInfo(directory).Name,
                            FileContent =
                                System.IO.File.ReadAllBytes(directory)
                        };

                        SendEmailDetails(sn, directory);

                        zip.AddFile(directory, "SalaryBills");
                    }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                    catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
                    {
                    }
                }
            }

            if (!string.IsNullOrEmpty(downloadStatus))
            {
                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("{0}.zip", "salaryBill");
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);
                Response.End();

                return Response;
            }
            else
            {
                return Json("record has been updated and sms has been sent", JsonRequestBehavior.AllowGet);
            }
        }

        public Object generatePdfByAutoID(string ID, string pdf)
        {
            ZipFile zip = new ZipFile();

            zip.AlternateEncodingUsage = ZipOption.AsNecessary;

            string[] returnResult = null;
            string filepath = string.Empty;
            string SavedPdfPath = string.Empty;
            bool pdfs = Convert.ToBoolean(pdf);
            if (pdfs)
            {
                pdfs = false;
                filepath = Helper.ExecuteService("salaryheads", "getBillFilePath", ID) as string;
            }
            else
            {
                pdfs = true;
            }
            salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", ID);
            try
            {
                if (s.designationName == "Speaker" || s.designationName == "Deputy Speaker")
                {
                    returnResult = GeneratePDFforSpeaker(s, pdfs, filepath);
                }
                else
                {
                    returnResult = GeneratePDFforMembers(s, pdfs, filepath);
                }
                if (pdfs)
                {
                    Helper.ExecuteService("salaryheads", "updateBillFilePath", new string[] { ID, returnResult[0] + s.monthID + "_" + s.yearId + "_" + s.memberCode + ".pdf" });

                    SavedPdfPath = returnResult[0] + s.monthID + "_" + s.yearId + "_" + s.memberCode + ".pdf";
                }
                else
                {
                    SavedPdfPath = filepath;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
            }
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            return Json("http://secure.shimlamc.org/secureFileStructure/" + SavedPdfPath, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSalaryHeadsListforAllMembers(string month, string Year)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            List<mMember> _mList = (List<mMember>)Helper.ExecuteService("salaryheads", "getallmembers", assemblyid);
            List<salarystructure> _sList = new List<salarystructure>();
            foreach (var member in _mList)
            {
                int memberid = member.MemberCode;
                salarystructure _salarystructure = null;
                int[] monthyear = getMonthYear(memberid);
                if (checksalary(memberid, monthyear, assemblyid, 1, 3))
                {
                    int[] arr = new int[] { memberid, 3, monthyear[0], monthyear[1] };
                    _salarystructure = (salarystructure)Helper.ExecuteService("salaryheads", "getMembersHeads", arr);
                    _salarystructure.monthID = monthyear[0];
                    _salarystructure.yearId = monthyear[1];
                    _salarystructure.memberName = member.Name;
                    _salarystructure.memberCode = member.MemberCode;

                    _salarystructure.netSalaryInWords = Convert.ToInt32(_salarystructure.netSalary).NumbersToWords();
                }
                else
                {
                    int[] arr = new int[] { memberid, monthyear[0], monthyear[1] };
                    _salarystructure = (salarystructure)Helper.ExecuteService("salaryheads", "getMonthlySalaryHeads", arr);
                    _salarystructure.netSalaryInWords = Convert.ToInt32(_salarystructure.netSalary).NumbersToWords();
                }
                _sList.Add(_salarystructure);
            }

            return PartialView("_salaryHeadsForAllMembers", _sList);
        }

        public ActionResult changeBillStatus(string status, string AutoId, string user)
        {
            string[] arr = new string[] { status, AutoId, user };
            Helper.ExecuteService("salaryheads", "changeBillStatus", arr);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult saveBillRemark(string remark, string AutoID)
        {
            string msg = "UserName**" + remark + "**" + DateTime.Now.ToShortDateString() + " , " + DateTime.Now.ToShortTimeString() + "@@";
            string[] arr = new string[] { msg, AutoID };
            string RemarkList = (string)Helper.ExecuteService("salaryheads", "saveBillRemark", arr);
            return Json(RemarkList, JsonRequestBehavior.AllowGet);
        }

        public string[] GeneratePDFforSpeaker(salarystructure qModel, bool pdf, string filePath)
        {
            string msg = string.Empty;
            string url = string.Empty;
            decimal totalBillsAmount = 0;
            MemoryStream output = new MemoryStream();
            string AllowancesText = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            string Deductions = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";

            string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

            outXml += "<table class=\"tg\"  style=\"width: 100%;\">";
            outXml += "<tr><th class=\"tg-031e\" colspan=\"8\">A.G.H.P SHIMLA PAY SLIP No : Deposit/MLA/1047-48, Dated " + new DateTime(1900, Convert.ToInt32(qModel.monthID), 1).ToString("MMMM").ToUpper() + " , " + qModel.yearId + "</th></tr>";
            // outXml += "<tr><td class=\"tg-031e\" colspan=\"8\">Note: Government accepts no responsibility or any fraud or misappropriation in respect of money, cheques or drafts made over to a messenger.<br/><br/></td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"8\"><br /><br /></td></tr> ";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">NAME :   <b>" + qModel.memberName.ToUpper() + "</b></td><td class=\"tg-031e\" colspan=\"4\"><b><span id=\"Designation\">" + qModel.designationName.ToUpper() + "</span></b></td> </tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += "<tr><td colspan=\"3\">1. &nbsp &nbsp  Treasury Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> SML00</td><td colspan=\"3\">2. &nbsp &nbsp  Gazetted/Non Gazetted</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>G</td></tr>";
            outXml += "<tr><td colspan=\"3\">3. &nbsp &nbsp  Demand Number</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td><td colspan=\"3\">4. &nbsp &nbsp  Scheme Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>-</td></tr>";
            outXml += "<tr><td colspan=\"3\">5. &nbsp &nbsp  DDO Code</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>092</td><td colspan=\"3\">6. &nbsp &nbsp  Object Code</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td></tr>";
            outXml += "<tr><td colspan=\"3\">7. &nbsp &nbsp  Major Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>2011</td><td colspan=\"3\">8. &nbsp &nbsp  Voted Charged</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>C</td></tr>";

            outXml += "<tr><td colspan=\"3\">9. &nbsp &nbsp  Sub Major Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>02</td><td colspan=\"3\">10. &nbsp &nbsp  Plan/Non Plan</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>N</td></tr>";
            outXml += "<tr><td colspan=\"3\">11. &nbsp &nbsp  Minor Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>101</td><td colspan=\"3\"></td><td style=\"text-align: right; padding-right:60px;\"></td></tr>";
            outXml += "<tr><td colspan=\"3\">12. &nbsp &nbsp  Sub Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td><td colspan=\"3\"></td><td style=\"text-align: right; padding-right:60px;\"></td></tr>";

            outXml += "<tr><td colspan=\"8\"><hr /></td></tr>";
            outXml += "<tr><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\"> ";

            foreach (var item in qModel.headList)
            {
                if (item.htype == "cr")
                {
                    string ctext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + "</td></tr>";
                    outXml += ctext;
                    AllowancesText += ctext;
                }
            }
            AllowancesText += "</table>";
            outXml += "</table></td><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\">";
            foreach (var item in qModel.headList)
            {
                if (item.htype == "dr")
                {
                    string dtext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + "</td></tr>";
                    outXml += dtext;
                    Deductions += dtext;
                }
            }
            outXml += " </table> </td> </tr>";
            Deductions += "</table>";

            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += " <tr><td class=\"tg-031e\"  colspan=\"2\"><b>Gross Amount</b></td><td colspan=\"2\" style=\"text-align: right;  padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalAllowances + "</b></td><td class=\"tg-031e\"colspan=\"2\"><b>Total Deductions</b></td>";

            outXml += "<td colspan=\"2\" style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalDeduction + "</b></td></tr><tr><th align=\"text-align:left;\" colspan=\"8\"><b>Net Claim Rs. " + qModel.netSalary + "</b></th></tr><tr><th align=\"text-align:left;\"colspan=\"8\"><b>Net Claim (Words)- " + Convert.ToInt32(qModel.netSalary).NumbersToWords() + " Only</b></th></tr>";

            //  outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Signature<br/><br></td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
            // outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            //  outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">For the Use of A.G. Officer</td><td class=\"tg-031e\" colspan=\"4\">For the Use in Treaury</td> </tr>";
            // outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Admitted Rs:</td><td class=\"tg-031e\" colspan=\"4\">Signature<br/><br></td> </tr>";
            //  outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Object Rs:</td><td class=\"tg-031e\" colspan=\"4\"></td> </tr>";
            // outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Auditor    supdt.</td><td class=\"tg-031e\" colspan=\"4\">Tresury Officer</td> </tr>";
            // outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Gazetted Officer </td><td class=\"tg-031e\" colspan=\"4\">Incorporated in Treasury Accountant</td> </tr>";
            //
            outXml += "</table>";
            StringBuilder str = new StringBuilder();
            str.Append(qModel.memberCode);
            str.Append(",");
            str.Append(qModel.monthID);
            str.Append(",");
            str.Append(qModel.yearId);
            outXml += "<br/><br/><hr/>TA/DA + MR Bills<hr/>";
            outXml += "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            outXml += "<tr><th style=\"text-align:left;\">From Date</th><th style=\"text-align:left;\">To Date</th><th style=\"text-align:left;\">Amount</th><th style=\"text-align:left;\">Remarks</th></tr>";
            List<mReimbursementBill> _memberRBill = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForSalary", str.ToString());
            if (_memberRBill.Count > 0)
            {
                foreach (var item in _memberRBill)
                {
                    totalBillsAmount += item.GrossAmount;
                    outXml += "<tr><td style=\"text-align:left;\">" + item.BillFromDate.Value.ToString("dd/MM/yyyy") + "</td><td style=\"text-align:left;\">" + item.BillToDate.Value.ToString("dd/MM/yyyy") + "</td><td style=\"text-align: right;width: 100px;padding-right: 100px; \"><span style=\"float:left;\">Rs. </span>" + item.GrossAmount + "</td><td>" + item.Remarks + "</td></tr>";
                }
            }
            else
            {
                ;
                outXml += "<tr><td style=\"text-align:left;\">--</td><td style=\"text-align:left;\">--</td><td style=\"text-align: right;width: 100px;padding-right: 100px; \"><span style=\"float:left;\">Rs. </span>0</td><td>--</td></tr>";
            }
            outXml += "</table>";
            outXml += "<hr/><table style=\"width: 100%; vertical-align: top;\" class=\"itg\"><th style=\"text-align:left;\">Total Amount</th><th style=\"text-align: right;width: 100px;padding-right: 270px; \"><span style=\"float:left;\">Rs. </span>" + totalBillsAmount + "</th></table><hr/>";
            outXml += @"</div></div></body></html>";
            if (pdf)
            {
                string htmlStringToConvert = outXml;
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 20;
                pdfConverter.PdfFooterOptions.FooterHeight = 40;

                TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
                        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan v1.0.0",
                       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement1.TextAlign = HorizontalTextAlign.Left;
                TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement3.TextAlign = HorizontalTextAlign.Right;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
                //   pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
                // pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

                string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

                ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
                // imageElement1.KeepAspectRatio = true;
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
                ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 5;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388F;

                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
                float stampWidth = float.Parse("400");
                float stampHeight = float.Parse("400");

                // Center the stamp at the top of PDF page
                float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
                float stampYLocation = 150;

                RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

                Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
                // stampTemplate.AddElement(imageElement2);
                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                url = "/salaryBills/12/" + qModel.yearId + "/" + qModel.monthID + "/" + qModel.memberCode + "/";
                string directory = FileSettings.SettingValue + url; ;

                //   string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(directory, qModel.monthID + "_" + qModel.yearId + "_" + qModel.memberCode + ".pdf");

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();
            }
            else
            {
                url = filePath;
            }
            return new string[] { url, AllowancesText, Deductions };
        }

        public string[] GeneratePDFforMembers(salarystructure qModel, bool pdf, string filePath)
        {
            string msg = string.Empty;
            string url = string.Empty;
            MemoryStream output = new MemoryStream();
            decimal totalBillsAmount = 0;
            string AllowancesText = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            string Deductions = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

            outXml += "<table class=\"tg\"  style=\"width: 100%;\">";
            outXml += "<tr><th class=\"tg-031e\" colspan=\"8\">A.G.H.P SHIMLA PAY SLIP No : Deposit/MLA/1047-48, Dated " + new DateTime(1900, Convert.ToInt32(qModel.monthID), 1).ToString("MMMM").ToUpper() + " , " + qModel.yearId + "</th></tr>";
            // outXml += "<tr><td class=\"tg-031e\" colspan=\"8\">Note: Government accepts no responsibility or any fraud or misappropriation in respect of money, cheques or drafts made over to a messenger.<br/><br/></td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"8\"><br /><br /></td></tr> ";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">NAME:   <b>" + qModel.memberName.ToUpper() + "</b></td><td class=\"tg-031e\" colspan=\"4\"><b><span id=\"Designation\">" + qModel.designationName.ToUpper() + "</span></b></td> </tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";

            outXml += "<tr><td colspan=\"4\"><b>Allowances</b></td><td colspan=\"4\"><b>Deductions</b></td></tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += "<tr><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\"> ";

            foreach (var item in qModel.headList)
            {
                if (item.htype == "cr")
                {
                    string crtext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + "</td></tr>";
                    outXml += crtext;
                    AllowancesText += crtext;
                }
            }
            AllowancesText += "</table>";
            outXml += "</table></td><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\">";
            foreach (var item in qModel.headList)
            {
                if (item.htype == "dr")
                {
                    string drtext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; \"><span style=\"float:left;\">Rs. </span>" + item.amount + "</td></tr>";
                    outXml += drtext;
                    Deductions += drtext;
                }
            }
            outXml += " </table> </td> </tr>";
            Deductions += "</table>";

            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += " <tr><td class=\"tg-031e\"  colspan=\"2\"><b>Gross Amount</b></td><td colspan=\"2\" style=\"text-align: right;  padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalAllowances + "</b></td><td class=\"tg-031e\"colspan=\"2\"><b>Total  Deductions</b></td>";

            outXml += "<td colspan=\"2\" style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalDeduction + "</b></td></tr><tr><th align=\"text-align:left;\" colspan=\"8\"><b>Net  Claim Rs. " + (qModel.netSalary) + "</b></th></tr><tr><th align=\"text-align:left;\"colspan=\"8\"><b>Net  Claim (Words)- " + Convert.ToInt32((qModel.netSalary)).NumbersToWords() + " Only</b></th></tr>";

            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Signature<br/><br></td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
            //outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">For the Use of A.G. Officer</td><td class=\"tg-031e\" colspan=\"4\">For the Use in Treaury</td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Admitted Rs:</td><td class=\"tg-031e\" colspan=\"4\">Signature<br/><br></td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Object Rs:</td><td class=\"tg-031e\" colspan=\"4\"></td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Auditor    supdt.</td><td class=\"tg-031e\" colspan=\"4\">Tresury Officer</td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Gazetted Officer </td><td class=\"tg-031e\" colspan=\"4\">Incorporated in Treasury Accountant</td> </tr>";

            outXml += "</table>";

            StringBuilder str = new StringBuilder();
            str.Append(qModel.memberCode);
            str.Append(",");
            str.Append(qModel.monthID);
            str.Append(",");
            str.Append(qModel.yearId);
            outXml += "<br/><br/><hr/>TA/DA + MR Bills<hr/>";
            outXml += "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            outXml += "<tr><th style=\"text-align:left;\">From Date</th><th style=\"text-align:left;\">To Date</th><th style=\"text-align:left;\">Amount</th><th style=\"text-align:left;\">Remarks</th></tr>";
            List<mReimbursementBill> _memberRBill = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForSalary", str.ToString());
            if (_memberRBill.Count > 0)
            {
                foreach (var item in _memberRBill)
                {
                    totalBillsAmount += item.GrossAmount;
                    outXml += "<tr><td style=\"text-align:left;\">" + item.BillFromDate.Value.ToString("dd/MM/yyyy") + "</td><td style=\"text-align:left;\">" + item.BillToDate.Value.ToString("dd/MM/yyyy") + "</td><td style=\"text-align: right;width: 100px;padding-right: 100px; \"><span style=\"float:left;\">Rs. </span>" + item.GrossAmount + "</td><td>" + item.Remarks + "</td></tr>";
                }
            }
            else
            {
                outXml += "<tr><td style=\"text-align:left;\">--</td><td style=\"text-align:left;\">--</td><td style=\"text-align: right;width: 100px;padding-right: 100px; \"><span style=\"float:left;\">Rs. </span>0</td><td>--</td></tr>";
            }
            outXml += "</table>";
            outXml += "<hr/><table style=\"width: 100%; vertical-align: top;\" class=\"itg\"><th style=\"text-align:left;\">Total Amount</th><th style=\"text-align: right;width: 100px;padding-right: 270px; \"><span style=\"float:left;\">Rs. </span>" + totalBillsAmount + "</th></table><hr/>";
            outXml += @"</div></div></body></html>";
            if (pdf)
            {
                string htmlStringToConvert = outXml;
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 20;
                pdfConverter.PdfFooterOptions.FooterHeight = 40;

                TextElement footerTextElement = new TextElement(0, 30, "Page &p; of &P;",
                        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

                string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

                ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
                // imageElement1.KeepAspectRatio = true;
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
                ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 5;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388F;

                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
                float stampWidth = float.Parse("400");
                float stampHeight = float.Parse("400");

                // Center the stamp at the top of PDF page
                float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
                float stampYLocation = 150;

                RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

                Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
                //
                // stampTemplate.AddElement(imageElement2);
                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                url = "/salaryBills/12/" + qModel.yearId + "/" + qModel.monthID + "/" + qModel.memberCode + "/";
                string directory = FileSettings.SettingValue + url; ;

                //  string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(directory, qModel.monthID + "_" + qModel.yearId + "_" + qModel.memberCode + ".pdf");

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();
            }
            else
            {
                url = filePath;
            }
            return new string[] { url, AllowancesText, Deductions };
        }

        #endregion salary Bills

        #region Generate Outer Bill

        public ActionResult GenreateOuterBill()
        {
            return View();
        }

        public ActionResult GenreateOuterBillPartial()
        {
            return PartialView("_GenreateOuterBill");
        }

        public ActionResult getOuterBill(string monthID, string yearID, string designationID, string Mode, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] arr = new int[] { int.Parse(designationID), int.Parse(monthID), int.Parse(yearID), assemblyid, ispart };
            MemberOuterBill _memberOuterBill = (MemberOuterBill)Helper.ExecuteService("salaryheads", "generateOuterBill", arr);
            _memberOuterBill.billNO = billNO(int.Parse(designationID) - 1, _memberOuterBill.BudgetId, 7, int.Parse(monthID), int.Parse(yearID), _memberOuterBill.grossTotal, (_memberOuterBill.total_a + _memberOuterBill.total_b), _memberOuterBill.memberID, Mode, ispart);
            _memberOuterBill.ispart = ispart;
            return PartialView("_outerBill", _memberOuterBill);
        }

        //public ActionResult getOuterBillforspeaker(string monthID, string yearID, string designationID, string Mode, int ispart)
        //{
        //    int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);

        //    int[] arr = new int[] { int.Parse(designationID), int.Parse(monthID), int.Parse(yearID), assemblyid, ispart };
        //    MemberOuterBill _memberOuterBill = (MemberOuterBill)Helper.ExecuteService("salaryheads", "generateOuterBill", arr);
        //    _memberOuterBill.billNO = billNO(int.Parse(designationID) - 1, _memberOuterBill.BudgetId, 7, int.Parse(monthID), int.Parse(yearID), _memberOuterBill.grossTotal, _memberOuterBill.total_a, _memberOuterBill.memberID, Mode, 1);
        //    _memberOuterBill.ispart = ispart;
        //    return PartialView("_outerBillforspeaker", _memberOuterBill);
        //}
        public ActionResult getOuterBillforspeaker(string monthID, string yearID, string designationID, string Mode, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);

            int[] arr = new int[] { int.Parse(designationID), int.Parse(monthID), int.Parse(yearID), assemblyid, ispart };
            MemberOuterBill _memberOuterBill = (MemberOuterBill)Helper.ExecuteService("salaryheads", "generateOuterBill", arr);
            _memberOuterBill.billNO = billNO(int.Parse(designationID) - 1, _memberOuterBill.BudgetId, 7, int.Parse(monthID), int.Parse(yearID), _memberOuterBill.grossTotal, _memberOuterBill.total_a, _memberOuterBill.memberID, Mode, ispart);
            _memberOuterBill.ispart = ispart;
            return PartialView("_outerBillforspeaker", _memberOuterBill);
        }
        public ActionResult generatePDFforOuterBill(string designationID, string monthId, string yearID, string Mode, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] arr = new int[] { int.Parse(designationID), int.Parse(monthId), int.Parse(yearID), assemblyid, ispart };
            MemberOuterBill _memberOuterBill = (MemberOuterBill)Helper.ExecuteService("salaryheads", "generateOuterBill", arr);
            _memberOuterBill.billNO = billNO(int.Parse(designationID) - 1, _memberOuterBill.BudgetId, 7, int.Parse(monthId), int.Parse(yearID), _memberOuterBill.grossTotal, (_memberOuterBill.total_a + _memberOuterBill.total_b), _memberOuterBill.memberID, Mode, ispart);
            string msg = string.Empty;

            MemoryStream output = new MemoryStream();

            string outXml = "<html><body style=\"font-family:DVOT-Yogesh; font-size: 12px;margin-right:25px;margin-left: 25px;\"><div><div style=\"width: 100%;\">";

            outXml += "<table style=\"width: 100%;font-size: 12px;\">";
            outXml += "<tr><td style=\"text-align: center;\" colspan=\"8\"><b>HIMACHAL PRADESH VIDHAN SABHA SHIMLA-171004.</b></td>";
            outXml += "</tr>";
            outXml += "<tr>";
            outXml += "<td colspan=\"8\">DETAILED PAYBILL OF ESTABLISHMENT OF MLA'S FOR THE MONTH OF : " + _memberOuterBill.monthName.ToUpper() + " " + _memberOuterBill.yearID + ".</td>";
            outXml += "</tr>";
            outXml += "<tr>";
            outXml += "<td colspan=\"2\"></td>";
            outXml += "<td>Bill NO. </td>";
            outXml += "<td> : " + _memberOuterBill.billNO + " </td>";
            outXml += "<td>Token No. </td>";
            outXml += "<td>: " + _memberOuterBill.tokenNO + " </td>";
            outXml += "<td>Voucher No. </td>";
            outXml += "<td> : " + _memberOuterBill.voucherNo + " </td>";
            outXml += "</tr>";
            outXml += "<tr><td colspan=\"2\">ACCOUNTHEAD : 2011-02-101-03-N-V[Vidhan Sabha Members]</td><td>Bill Date </td><td> : " + _memberOuterBill.billDate.Value.ToString("dd/MM/yyyy") + " </td><td>Token Date  </td><td>" + _memberOuterBill.tokenDate + " </td><td>Voucher Date.</td><td> : " + _memberOuterBill.voucherDate + "</td></tr>";
            outXml += "<tr><td colspan=\"8\"><hr /></td></tr>";
            outXml += "<tr><td colspan=\"8\"><table style=\"width: 100%;font-size: 12px;\">";

            outXml += "<tr><td>1. &nbsp &nbsp  Treasury Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> SML00</td><td>2. &nbsp &nbsp Demand No. </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> 01</td><td>1.&nbsp &nbsp  Gross Total</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> " + _memberOuterBill.grossTotal + "</td><td colspan=\"2\"></td></tr>";
            outXml += "<tr><td>3. &nbsp &nbsp D.D.O </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> 039</td><td>4. &nbsp &nbsp  Gaztd/Non-Gaztd. </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> G</td><td>2.&nbsp &nbsp  Short Drawal</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> 0 </td><td colspan=\"2\"></td></tr>";
            outXml += "<tr><td>5. &nbsp &nbsp Major Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> 2011</td><td>  &nbsp&nbsp &nbsp&nbsp &nbsp 2011 &nbsp &nbsp Parliament/State/UT Legislature</td><td></td><td>3. &nbsp &nbspTotal Amount(1-2)</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> " + _memberOuterBill.grossTotal + "</td><td colspan=\"2\"></td></tr>";
            outXml += "<tr><td>6. &nbsp &nbsp Sub Major Head </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> 02</td><td > &nbsp&nbsp &nbsp&nbsp &nbsp &nbsp &nbsp 02 &nbsp &nbspState/UT Legislatures</td><td></td><td>4.&nbsp &nbsp  A.G. Deductions(A)</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> " + _memberOuterBill.total_a + "</td><td colspan=\"2\"></td></tr>";
            outXml += "<tr><td>7. &nbsp &nbsp Minor Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> 101</td><td> &nbsp &nbsp &nbsp&nbsp &nbsp &nbsp 101 &nbsp &nbsp Legislative Assembly   </td><td></td><td>5. &nbsp &nbsp Balance Amount(3-4)</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> " + (_memberOuterBill.grossTotal - _memberOuterBill.total_a) + "</td><td colspan=\"2\"></td></tr>";
            outXml += "<tr><td>8. &nbsp &nbsp Sub Head </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> 03</td><td> &nbsp &nbsp &nbsp&nbsp &nbsp &nbsp &nbsp 03 &nbsp &nbspMembers of HP Vidhan Sabha </td><td></td><td>6.&nbsp &nbsp  B.T. Deduction(B)</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> " + _memberOuterBill.total_b + "</td><td colspan=\"2\"></td></tr>";
            outXml += "<tr><td>9. &nbsp &nbsp Budget Code </td><td> </td><td>10.&nbsp &nbsp  Object Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> 01-Salaries</td><td>7.&nbsp &nbsp  Net Amount(5-6)</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> " + (_memberOuterBill.grossTotal - (_memberOuterBill.total_a + _memberOuterBill.total_b)) + "</td><td colspan=\"2\"></td></tr>";
            outXml += "<tr><td>11. &nbsp &nbsp Plan/Non plan </td><td></td><td>12.&nbsp &nbsp  Voted/Charged </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> V</td><td></td><td></td><td colspan=\"2\"></td></tr>";

            outXml += "<tr><td colspan=\"8\"><hr /></td></tr>";
            outXml += "<tr><td colspan=\"8\"><table style=\"width: 100%;font-size: 12px;\">";
            outXml += "<tr><td colspan=\"2\"><b>SUB OBJECT(DETAILED) HEAD</b></td><td colspan=\"2\"><b>(A) DEDUCTIONS CLASSIFIED BY AG </b></td><td colspan=\"2\"><b>(B) DEDUCTIONS CLASSIFIED BY(T.O.)</b> </td><td colspan=\"2\"><b>CORRESPONDING RECEIPT CODE</b> </td></tr>";
            outXml += "<tr><td colspan=\"8\"><hr /></td></tr>";
            outXml += "<tr><td colspan=\"2\" style=\"vertical-align: top;\"><table style=\"width: 100%;font-size: 12px;\">";
            foreach (var critem in _memberOuterBill.headList)
            {
                if (critem.htype == "cr")
                {
                    outXml += "<tr><td  cellpadding=\"4\">" + critem.headName + "</td><td style=\"text-align: right; padding-right:30px;\"><span style=\"float:left;\">Rs. </span>" + critem.amount + "  </td></tr>";
                }
            }
            outXml += "</table></td><td colspan=\"2\" style=\"vertical-align: top;\"><table style=\"width: 100%;font-size: 12px;\">";
            foreach (var dritem in _memberOuterBill.headList)
            {
                if (dritem.htype == "dr" && dritem.subType == "a")
                {
                    outXml += "<tr><td  cellpadding=\"4\">" + dritem.headName + "</td><td style=\"text-align: right; padding-right:30px;\"><span style=\"float:left;\">Rs. </span> " + dritem.amount + "  </td></tr>";
                }
            }
            outXml += "</table></td><td colspan=\"2\" style=\"vertical-align: top;\"><table style=\"width: 100%;font-size: 12px;\">";
            foreach (var dritem in _memberOuterBill.headList)
            {
                if (dritem.htype == "dr" && dritem.subType == "b")
                {
                    outXml += "<tr><td  cellpadding=\"4\">" + dritem.headName + "</td><td style=\"text-align: right; padding-right:30px;\"><span style=\"float:left;\">Rs. </span> " + dritem.amount + "  </td></tr>";
                }
            }
            outXml += "</table></td><td colspan=\"2\" style=\"vertical-align: top;\"><table style=\"width: 100%;font-size: 12px;\"><tr><td>Major</td><td>S. Major</td><td>Minor</td><td>S. Head</td><td>DDO Code</td></tr>";
            outXml += "<tr><td>0216</td><td>01</td><td>106</td><td>03</td></tr></table></td></tr><tr><td colspan=\"8\"><hr /></td></tr>";
            outXml += "<tr><td><b>Gross Total</b>  </td></td><td style=\"text-align: right; padding-right:30px;\"><span style=\"float:left;\">Rs. </span> " + _memberOuterBill.grossTotal + "  </td><td><b>Total (A)  </b></td></td><td style=\"text-align: right; padding-right:30px;\"><span style=\"float:left;\">Rs. </span> " + _memberOuterBill.total_a + "  </td><td><b>Total (B) </b></td></td><td style=\"text-align: right; padding-right:30px;\"><span style=\"float:left;\">Rs. </span> " + _memberOuterBill.total_b + "  </td></tr></table></td></tr>";
            outXml += "<tr><td colspan=\"8\"><hr /><br /><br /></td></tr>";
            outXml += "<tr style=\"height: 30px;\"><td colspan=\"2\">Station :Shimla<br /></td><td colspan=\"6\"> <br /><br />Signature and Designation of Drawing Officer<br /></td></tr>";
            outXml += "<tr><td>Date : </td><td>(Treasury Clerk)</td><td colspan=\"6\">Code</td></tr>";
            outXml += "</table>";
            outXml += @"</div></div>";
            outXml += "<div style=\"page-break-after: always; width: 100%; height: 50px;\"></div>";
            outXml += "<div><div style=\"width: 100%;\">";
            outXml += "<section><header><h3 style=\"text-align:center;\">CERTIFICATE</h3><hr><P>1. The Drawal is being made on account of <br> (Copy Enclosed)</P><p>2. Arreas were less drawan vide T/V NC</p><p>3. Certified that pay and allowances drawan in this bill are due and admissible as per authority in force and deductions where-ever required have been made, as per</p><p>4. RECEIVED THE CONTENTS (In Cash) Rs " + (_memberOuterBill.grossTotal - (_memberOuterBill.total_a + _memberOuterBill.total_b)) + "<br>(In Words) - " + int.Parse((_memberOuterBill.grossTotal - (_memberOuterBill.total_a + _memberOuterBill.total_b)).ToString()).NumbersToWords() + " Only</p><div style=\"text-align: left;float: right;\"><br /><br />(Signature & Designation of DDO) <br>Code No :</div><header>";
            outXml += "<section><span style=\"font-weight:bold;\">___________________________________________________________________________________________________________________________</span>";
            outXml += "<table style=\"width:100%\"><tr>   <td>Pay Rs.</td>   <td>(In words) Rupees</td>    <td>(TO BE USED BY TREASURY OFFICE)</td> </tr> <tr>   <td>Superindendent Treasury</td>   <td>Dated :</td>    <td></td></tr>";
            outXml += " <tr>   <td>Amount To be Classified by T.O</td>   <td>(Treasury Officer)</td>    <td></td> </tr> <tr>   <td>1. Cash</td>   <td></td>    <td></td> </tr> <tr>   <td>House Rent</td>   <td></td>    <td></td> </tr> <tr>   <td>***Total***</td>   <td></td>    <td></td> </tr>  </table><span style=\"font-weight:bold;\">_____________________________________________________________________________________________________________________________</span>";
            outXml += "<table style=\"width:100%\"><tr>  <td>Intitials of supdt A.A.O.in token of</td>   <td> (TO BE USED BY ACCOUNTANT GENERAL OFFICE) </td>  </tr> <tr>   <td>Check of classification of Item above Rs. 500/5000</td>   <td>Admitted Rs.</td>  </tr>   <tr>   <td></td> <td>Objected Rs.</td><td>Auditor</td>	</tr></table><span style=\"font-weight:bold;\">____________________________________________________________________________________________________________________________</span><section>";
            outXml += "<footer><span style=\"text-align:center;\">INSTRUCTION</span><p style=\"text-align:left\">1. A red Line should be drawan right across the sheet after each section of the establishment and GRANT TO TOTALS shown in red.<br>2. All deductions should be schedules in appropriate form<br>3. Recovery of House rent should be supported by rent rolls in duplicate form the PWD/Estate Officer. Dedction adjustable by B.T Should also be supported by duplicate sche<br>4. Due care should be taken to give correct code numbers where-ever specified.</p></footer><br> <br></section>";
            outXml += @"</div></div>";


            //////////////////////New page added for bill break up list
            string establishbillid = "";
            string bid = Convert.ToString(_memberOuterBill.billNO);
            string details = bid + "," + monthId + "," + yearID + "," + ispart + "," + CurrentSession.AssemblyId;
            var EstablishId = (List<mEstablishBill>)Helper.ExecuteService("Budget", "GetEstablishBillIdByBillNo", details);
            foreach (var item in EstablishId)
            {
                string a = Convert.ToString(item.EstablishBillID);
                establishbillid = a + ",";
            }
            var newlistIs = (List<AccountDetail>)Helper.ExecuteService("Budget", "GetAllAccountDetail", establishbillid);
            if (newlistIs != null)
            {

                outXml += "<table style='page-break-after: always; width: 100%; margin-left:1%;'> <tr> <td style='vertical-align: middle; text-align: center'>";
                outXml += "<div style='text-align:center;'><h3>Bill Breakup  " + "-Bill No." + _memberOuterBill.billNO + " of " + _memberOuterBill.Financialyear + "</h3></div>";
                outXml += "<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr>";
                outXml += "<th style='border:1px solid black;border-collapse:collapse;'>Sr. No.</th>";
                outXml += "<th style='border:1px solid black;border-collapse:collapse;'>Name of the Member</th>";
                outXml += "<th style='border:1px solid black;border-collapse:collapse;'>Payee Code</th>";
                outXml += "<th style='border:1px solid black;border-collapse:collapse;'>IFSC Code</th>";
                outXml += "<th style='border:1px solid black;border-collapse:collapse;'>Account Number</th>";
                outXml += "<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>";
                int AccCount = 1;
                decimal TotalSubVoucher = 0;
                foreach (var item in newlistIs)
                {

                    outXml += "<tr>";
                    outXml += "<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>" + AccCount + "</td>";
                    outXml += "<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>" + item.ClaimantName + "</td>";
                    outXml += "<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>" + item.MemberPayeeCode + "</td>";
                    outXml += "<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>" + item.IFSCCode + "</td>";
                    outXml += "<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>" + item.AccountNo + "</td>";
                    string SAmount = item.VoucherAmount.ToString("0.#####");
                    outXml += "<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>" + SAmount + "</td>";
                    outXml += "</tr>";
                    AccCount++;
                    TotalSubVoucher = TotalSubVoucher + item.VoucherAmount;
                }

                outXml += "<tr>";
                outXml += "<td style='border:1px solid black;border-collapse:collapse;'></td>";
                outXml += "<td style='border:1px solid black;border-collapse:collapse;'></td>";
                outXml += "<td style='border:1px solid black;border-collapse:collapse;'></td>";
                outXml += "<td style='border:1px solid black;border-collapse:collapse;'></td>";
                outXml += "<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b></td>";
                string TotalSubVoucherAmounts = TotalSubVoucher.ToString("0.#####");// string.Format("{0:n0}", TotalSubVoucher);
                outXml += "<td style='border:1px solid black;border-collapse:collapse;'>" + TotalSubVoucherAmounts + "</td>";
                outXml += "</tr></table> ";
                outXml += "</body></html>";
            }
            ////////////////////////////

            string htmlStringToConvert = outXml;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Landscape;

            pdfConverter.PdfHeaderOptions.HeaderHeight = 20;
            pdfConverter.PdfFooterOptions.FooterHeight = 40;

            TextElement footerTextElement = new TextElement(0, 20, "Page &p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(390, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            //  stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            string url = "/salaryOuterBills/12/" + _memberOuterBill.monthID + "/" + _memberOuterBill.yearID + "/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), _memberOuterBill.monthID + "_" + _memberOuterBill.yearID + "_CompletesalaryOuterBill.pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();

            DownloadPDF(url);
            return View();
        }

        //public ActionResult generatePDFforOuterBillforspeaker(string designationID, string monthId, string yearID, string mode, int ispart)
        //{
        //    int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
        //    int[] arr = new int[] { int.Parse(designationID), int.Parse(monthId), int.Parse(yearID), assemblyid, ispart };
        //    MemberOuterBill qModel = (MemberOuterBill)Helper.ExecuteService("salaryheads", "generateOuterBill", arr);

        //    qModel.billNO = billNO(int.Parse(designationID) - 1, qModel.BudgetId, 7, int.Parse(monthId), int.Parse(yearID), qModel.grossTotal, qModel.total_a, qModel.memberID, mode, 1);

        //    string msg = string.Empty;

        //    MemoryStream output = new MemoryStream();

        //    string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

        //    outXml += "<table class=\"tg\"   style=\"width: 100%;\">";
        //    outXml += "<tr><th class=\"tg-031e\" colspan=\"8\">A.G.H.P SHIMLA PAY SLIP No :Deposit/MLA/1047-48 <br/> Dated " + new DateTime(1900, Convert.ToInt32(qModel.monthID), 1).ToString("MMMM").ToUpper() + " , " + qModel.yearID + " , Bill NO. : " + qModel.billNO + " " + DateTime.Now.ToString("dd/MM/yyyy") + "</th></tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"8\">Note: Government accepts no responsibility or any fraud or misappropriation in respect of money, cheques or drafts made over to a messenger.<br/><br/></td> </tr>";

        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Name :   <b>" + qModel.memberName + "</b></td><td class=\"tg-031e\" colspan=\"4\"><b><span id=\"Designation\">" + qModel.designationName + "</span></b></td> </tr>";
        //    outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
        //    outXml += "<tr><td colspan=\"3\">1. &nbsp &nbsp  Treasury Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> SML00</td><td colspan=\"3\">2. &nbsp &nbsp  Gazetted/Non Gazetted</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>G</td></tr>";
        //    outXml += "<tr><td colspan=\"3\">3. &nbsp &nbsp  Demand Number</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td><td colspan=\"3\">4. &nbsp &nbsp  Scheme Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>-</td></tr>";
        //    outXml += "<tr><td colspan=\"3\">5. &nbsp &nbsp  DDO Code</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>092</td><td colspan=\"3\">6. &nbsp &nbsp  Object Code</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td></tr>";
        //    outXml += "<tr><td colspan=\"3\">7. &nbsp &nbsp  Major Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>2011</td><td colspan=\"3\">8. &nbsp &nbsp  Voted Charged</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>C</td></tr>";

        //    outXml += "<tr><td colspan=\"3\">9. &nbsp &nbsp  Sub Major Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>02</td><td colspan=\"3\">10. &nbsp &nbsp  Plan/Non Plan</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>N</td></tr>";
        //    outXml += "<tr><td colspan=\"3\">11. &nbsp &nbsp  Minor Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>101</td><td colspan=\"3\"></td><td style=\"text-align: right; padding-right:60px;\"></td></tr>";
        //    outXml += "<tr><td colspan=\"3\">12. &nbsp &nbsp  Sub Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td><td colspan=\"3\"></td><td style=\"text-align: right; padding-right:60px;\"></td></tr>";

        //    outXml += "<tr><td colspan=\"8\"><hr /></td></tr>";
        //    outXml += "<tr><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\"> ";

        //    foreach (var item in qModel.headList)
        //    {
        //        if (item.htype == "cr")
        //        {
        //            string ctext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + "</td></tr>";
        //            outXml += ctext;
        //            // AllowancesText += ctext;
        //        }
        //    }
        //    // AllowancesText += "</table>";
        //    outXml += "</table></td><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\">";
        //    foreach (var item in qModel.headList)
        //    {
        //        if (item.htype == "dr")
        //        {
        //            string dtext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + "</td></tr>";
        //            outXml += dtext;
        //            //Deductions += dtext;
        //        }
        //    }
        //    outXml += " </table> </td> </tr>";
        //    // Deductions += "</table>";

        //    outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
        //    outXml += " <tr><td class=\"tg-031e\"  colspan=\"2\"><b>Gross Amount</b></td><td colspan=\"2\" style=\"text-align: right;  padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.grossTotal + "</b></td><td class=\"tg-031e\"colspan=\"2\"><b>Total Deductions</b></td>";

        //    outXml += "<td colspan=\"2\" style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.total_a + "</b></td></tr><tr><th align=\"text-align:left;\" colspan=\"8\"><b>Net Claim Rs. " + (qModel.grossTotal - qModel.total_a) + "</b></th></tr><tr><th align=\"text-align:left;\"colspan=\"8\"><b>Net Claim (Words)- " + Convert.ToInt32((qModel.grossTotal - qModel.total_a)).NumbersToWords() + " Only</b></th></tr>";

        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\"><br/><br />Signature</td><td class=\"tg-031e\" colspan=\"4\"><br/>Signature</td> </tr>";
        //    outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">For the Use of A.G. Officer</td><td class=\"tg-031e\" colspan=\"4\">For the Use in Treaury</td> </tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Admitted Rs:</td><td class=\"tg-031e\" colspan=\"4\"><br/><br />Signature<br/><br></td> </tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Object Rs:</td><td class=\"tg-031e\" colspan=\"4\"></td> </tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Auditor    supdt.</td><td class=\"tg-031e\" colspan=\"4\">Tresury Officer</td> </tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Gazetted Officer </td><td class=\"tg-031e\" colspan=\"4\">Incorporated in Treasury Accountant</td> </tr>";

        //    outXml += "</table>";

        //    outXml += @"</div></div></body></html>";

        //    string htmlStringToConvert = outXml;
        //    PdfConverter pdfConverter = new PdfConverter();
        //    pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

        //    pdfConverter.PdfDocumentOptions.ShowFooter = true;
        //    pdfConverter.PdfDocumentOptions.ShowHeader = true;
        //    pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;

        //    pdfConverter.PdfHeaderOptions.HeaderHeight = 20;
        //    pdfConverter.PdfFooterOptions.FooterHeight = 30;

        //    TextElement footerTextElement = new TextElement(0, 10, "Page &p; of &P;",
        //            new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //    footerTextElement.TextAlign = HorizontalTextAlign.Center;

        //    pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

        //    string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

        //    ImageElement imageElement1 = new ImageElement(390, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
        //    // imageElement1.KeepAspectRatio = true;
        //    imageElement1.Opacity = 100;
        //    imageElement1.DestHeight = 97f;
        //    imageElement1.DestWidth = 71f;
        //    // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
        //    ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
        //    imageElement2.KeepAspectRatio = false;
        //    imageElement2.Opacity = 5;
        //    imageElement2.DestHeight = 284f;
        //    imageElement2.DestWidth = 388F;

        //    EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
        //    float stampWidth = float.Parse("400");
        //    float stampHeight = float.Parse("400");

        //    // Center the stamp at the top of PDF page
        //    float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
        //    float stampYLocation = 150;

        //    RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

        //    Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
        //    //  stampTemplate.AddElement(imageElement2);
        //    byte[] pdfBytes = null;

        //    try
        //    {
        //        pdfBytes = pdfDocument.Save();
        //    }
        //    finally
        //    {
        //        // close the Document to realease all the resources
        //        pdfDocument.Close();
        //    }

        //    string url = "/salaryOuterBills/12/" + qModel.monthID + "/" + qModel.yearID + "/";

        //    string directory = Server.MapPath(url);

        //    if (!Directory.Exists(directory))
        //    {
        //        Directory.CreateDirectory(directory);
        //    }

        //    string path = Path.Combine(Server.MapPath("~" + url), qModel.monthID + "_" + qModel.yearID + "_CompletesalaryOuterBill.pdf");

        //    FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
        //    System.IO.FileAccess.Write);

        //    _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

        //    // close file stream
        //    _FileStream.Close();

        //    DownloadPDF(url);
        //    return View();
        //}
        public ActionResult generatePDFforOuterBillforspeaker(string designationID, string monthId, string yearID, string mode, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] arr = new int[] { int.Parse(designationID), int.Parse(monthId), int.Parse(yearID), assemblyid, ispart };
            MemberOuterBill qModel = (MemberOuterBill)Helper.ExecuteService("salaryheads", "generateOuterBill", arr);

            qModel.billNO = billNO(int.Parse(designationID) - 1, qModel.BudgetId, 7, int.Parse(monthId), int.Parse(yearID), qModel.grossTotal, qModel.total_a, qModel.memberID, mode, ispart);

            string msg = string.Empty;

            MemoryStream output = new MemoryStream();

            string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

            outXml += "<table class=\"tg\"   style=\"width: 100%;\">";
            outXml += "<tr><th class=\"tg-031e\" colspan=\"8\">A.G.H.P SHIMLA PAY SLIP No :Deposit/MLA/1047-48 <br/> Dated " + new DateTime(1900, Convert.ToInt32(qModel.monthID), 1).ToString("MMMM").ToUpper() + " , " + qModel.yearID + " , Bill NO. : " + qModel.billNO + " " + DateTime.Now.ToString("dd/MM/yyyy") + "</th></tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"8\">Note: Government accepts no responsibility or any fraud or misappropriation in respect of money, cheques or drafts made over to a messenger.<br/><br/></td> </tr>";

            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Name :   <b>" + qModel.memberName + "</b></td><td class=\"tg-031e\" colspan=\"4\"><b><span id=\"Designation\">" + qModel.designationName + "</span></b></td> </tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += "<tr><td colspan=\"3\">1. &nbsp &nbsp  Treasury Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span> SML00</td><td colspan=\"3\">2. &nbsp &nbsp  Gazetted/Non Gazetted</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>G</td></tr>";
            outXml += "<tr><td colspan=\"3\">3. &nbsp &nbsp  Demand Number</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td><td colspan=\"3\">4. &nbsp &nbsp  Scheme Code </td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>-</td></tr>";

            if (int.Parse(designationID) - 1 == 3)
            {
                outXml += "<tr><td colspan=\"3\">5. &nbsp &nbsp  DDO Code</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>126</td><td colspan=\"3\">6. &nbsp &nbsp  Object Code</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td></tr>";
            }
            else
            {
                outXml += "<tr><td colspan=\"3\">5. &nbsp &nbsp  DDO Code</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>092</td><td colspan=\"3\">6. &nbsp &nbsp  Object Code</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td></tr>";
            }
            outXml += "<tr><td colspan=\"3\">7. &nbsp &nbsp  Major Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>2011</td><td colspan=\"3\">8. &nbsp &nbsp  Voted Charged</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>C</td></tr>";

            outXml += "<tr><td colspan=\"3\">9. &nbsp &nbsp  Sub Major Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>02</td><td colspan=\"3\">10. &nbsp &nbsp  Plan/Non Plan</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>N</td></tr>";
            outXml += "<tr><td colspan=\"3\">11. &nbsp &nbsp  Minor Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>101</td><td colspan=\"3\"></td><td style=\"text-align: right; padding-right:60px;\"></td></tr>";
            outXml += "<tr><td colspan=\"3\">12. &nbsp &nbsp  Sub Head</td><td style=\"text-align: right; padding-right:60px;\"><span style=\"float:left;\"> : </span>01</td><td colspan=\"3\"></td><td style=\"text-align: right; padding-right:60px;\"></td></tr>";

            outXml += "<tr><td colspan=\"8\"><hr /></td></tr>";
            outXml += "<tr><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\"> ";

            foreach (var item in qModel.headList)
            {
                if (item.htype == "cr")
                {
                    string ctext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + "</td></tr>";
                    outXml += ctext;
                    // AllowancesText += ctext;
                }
            }
            // AllowancesText += "</table>";
            outXml += "</table></td><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\">";
            foreach (var item in qModel.headList)
            {
                if (item.htype == "dr")
                {
                    string dtext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + "</td></tr>";
                    outXml += dtext;
                    //Deductions += dtext;
                }
            }
            outXml += " </table> </td> </tr>";
            // Deductions += "</table>";

            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += " <tr><td class=\"tg-031e\"  colspan=\"2\"><b>Gross Amount</b></td><td colspan=\"2\" style=\"text-align: right;  padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.grossTotal + "</b></td><td class=\"tg-031e\"colspan=\"2\"><b>Total Deductions</b></td>";

            outXml += "<td colspan=\"2\" style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.total_a + "</b></td></tr><tr><th align=\"text-align:left;\" colspan=\"8\"><b>Net Claim Rs. " + (qModel.grossTotal - qModel.total_a) + "</b></th></tr><tr><th align=\"text-align:left;\"colspan=\"8\"><b>Net Claim (Words)- " + Convert.ToInt32((qModel.grossTotal - qModel.total_a)).NumbersToWords() + " Only</b></th></tr>";

            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\"><br/><br />Signature</td><td class=\"tg-031e\" colspan=\"4\"><br/>Signature</td> </tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">For the Use of A.G. Officer</td><td class=\"tg-031e\" colspan=\"4\">For the Use in Treaury</td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Admitted Rs:</td><td class=\"tg-031e\" colspan=\"4\"><br/><br />Signature<br/><br></td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Object Rs:</td><td class=\"tg-031e\" colspan=\"4\"></td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Auditor    supdt.</td><td class=\"tg-031e\" colspan=\"4\">Tresury Officer</td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Gazetted Officer </td><td class=\"tg-031e\" colspan=\"4\">Incorporated in Treasury Accountant</td> </tr>";

            outXml += "</table>";

            outXml += @"</div></div></body></html>";

            string htmlStringToConvert = outXml;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;

            pdfConverter.PdfHeaderOptions.HeaderHeight = 20;
            pdfConverter.PdfFooterOptions.FooterHeight = 30;

            TextElement footerTextElement = new TextElement(0, 10, "Page &p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(390, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            //  stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            string url = "/salaryOuterBills/12/" + qModel.monthID + "/" + qModel.yearID + "/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), qModel.monthID + "_" + qModel.yearID + "_CompletesalaryOuterBill.pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();

            DownloadPDF(url);
            return View();
        }

        #region

        public ActionResult HouseRentDeductionReport()
        {
            return View();
        }

        public ActionResult HouseRentDeductionReportPartial()
        {
            return PartialView("_HouseRentDeductionReport");
        }

        public ActionResult getHouseRentDeductionList(string monthID, string yearID, string deductionTypeID, string designationID, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] arr = new int[] { int.Parse(designationID), int.Parse(deductionTypeID), int.Parse(monthID), int.Parse(yearID), assemblyid, ispart };
            HouseRentDedReport _list = (HouseRentDedReport)Helper.ExecuteService("salaryheads", "getHouseRentDeductionList", arr);
            List<AccountHeadList> headlist = new List<AccountHeadList>();
            if (deductionTypeID == "9")
            {
                headlist.Add(new AccountHeadList { code = "0216", text = "Housing" });
                headlist.Add(new AccountHeadList { code = "01", text = "Government Residential Building" });
                headlist.Add(new AccountHeadList { code = "106", text = "General Pool Accommodations" });
                headlist.Add(new AccountHeadList { code = "03", text = "Receipt from Estate Office" });
                _list.accountHead = "2011 - 02 - 101 - 03 - vs Members";
            }

            if (deductionTypeID == "11")
            {
                headlist.Add(new AccountHeadList { code = "0000", text = "Medical Reimbursement" });
                headlist.Add(new AccountHeadList { code = "00", text = "" });
                headlist.Add(new AccountHeadList { code = "000", text = "" });
                headlist.Add(new AccountHeadList { code = "00", text = "" });
                _list.accountHead = "2011 - 02 - 101 - 03 - vs Members";
            }
            _list.accountHeadList = headlist;

            return PartialView("_houseRentDed", _list);
        }

        [ValidateInput(false)]
        public ActionResult generatePDFforhouseRentBill(string PDFData, string monthID, string yearID)
        {
            string outXml = "<html><body style=\"font-family:DVOT-Yogesh; font-size: 12px;margin-right:25px;margin-left: 25px;\"><div><div style=\"width: 100%;\">";
            outXml += PDFData;
            outXml += @"</div></div></body></html>";
            MemoryStream output = new MemoryStream();
            string htmlStringToConvert = outXml;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;

            pdfConverter.PdfHeaderOptions.HeaderHeight = 20;
            pdfConverter.PdfFooterOptions.FooterHeight = 40;

            TextElement footerTextElement = new TextElement(0, 30, "Page: &p; of &P;",
             new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            //  stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            string url = "/HouseRentDeductionBill/12/" + monthID + "/" + yearID + "/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), yearID + "_" + monthID + "HouseRentDeductionBill.pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();

            DownloadPDF(url);
            return View();
        }

        #endregion Generate Outer Bill

        public ActionResult MembersSalaryStatement()
        {
            return View();
        }

        public ActionResult MembersSalaryStatementPartial()
        {
            return PartialView("_MembersSalaryStatement");
        }

        public FileResult GenerateMembersReport(string monthID, string yearID, string designationID, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] arr = new int[] { int.Parse(designationID), int.Parse(monthID), int.Parse(yearID), assemblyid, ispart };
            List<membersOuterReport> qModel = (List<membersOuterReport>)Helper.ExecuteService("salaryheads", "generateOuterBillForMembers", arr);
            string msg = string.Empty;

            MemoryStream output = new MemoryStream();

            string outXml = "<html><body style='font-family:DVOT-Yogesh; font:10px; margin-left:80px;  margin-right:10px;'><div><div style='width: 100%;   white-space: nowrap;'>";

            outXml += "<table cellpadding=\"2\"  style=\"width: 100%; font-size: 10px; border-collapse:collapse; font:10px;  white-space: nowrap; \" width='100%'>";
            outXml += "<tr ><td style=\"text-align: center;\" colspan='" + qModel[0].columnName.Count + "'>HIMACHAL PRADESH VIDHAN SHABHA SHIMLA-171004</td></tr>";
            outXml += "<tr ><td colspan='" + qModel[0].columnName.Count + "'>MEMBERS SALARY STATEMENT FOR THE MONTH OF : " + new DateTime(1900, Convert.ToInt32(qModel[0].monthID), 1).ToString("MMMM").ToUpper() + " , " + qModel[0].yearID + "</td></tr>";

            int count = 0;
            int columncount = 0;
            int gsalary = 0;
            int nsalary = 0;
            foreach (var item in qModel)
            {
                //count++;
                count = count + 1;
                columncount = 0;
                string style = (count % 2 == 0) ? "bgcolor='#f9f9f9'" : "bgcolor='#ffffff'";
                outXml += "<tr style='border-bottom: 1px dashed #ccc;'>";
                if (count == 1)
                {
                    foreach (var critem in item.columnName)
                    {
                        columncount++;
                        if (critem == "Gross Salary")
                        {
                            gsalary = columncount;
                        }
                        if (critem == "Net Salary")
                        {
                            nsalary = columncount;
                        }

                        outXml += "<th>" + critem + "</th>";
                    }
                }
                else
                {
                    if (count == qModel.Count)
                    {
                        foreach (var critem in item.columnName)
                        {
                            columncount++;

                            outXml += "<th style='text-align:left;'>" + critem + "</th>";
                        }
                    }
                    else
                    {
                        foreach (var critem in item.columnName)
                        {
                            columncount++;
                            if (columncount == nsalary || columncount == gsalary)
                            {
                                outXml += "<td><b>" + critem + "</b></td>";
                            }
                            else
                            {
                                outXml += "<td>" + critem + "</td>";
                            }
                        }
                    }
                }
                outXml += "</tr>";
            }

            outXml += "</table>";
            outXml += "<div><br /><br /><span style='float:right; padding-right:20px;'><br /><br />(Signature & Designation of DDO)<br />Code No:</span></div>";
            outXml += "</div></div></body></html>";
            string htmlStringToConvert = outXml;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfDocumentOptions.AutoSizePdfPage = true;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Landscape;

            pdfConverter.PdfHeaderOptions.HeaderHeight = 20;
            pdfConverter.PdfFooterOptions.FooterHeight = 40;

            //  pdfConverter.PdfDocumentOptions.FitWidth = true;
            TextElement footerTextElement = new TextElement(0, 20, "&p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");
            ImageElement imageElement1 = new ImageElement(390, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            //   stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            string url = "/salaryOuterBills/12/" + qModel[0].yearID + "/" + qModel[0].monthID + "/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), qModel[0].monthID + "_" + qModel[0].yearID + "_salaryOuterBill.pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();
            DownloadPDF(url);
            return new FileStreamResult(output, "application/pdf");
        }

        public object DownloadPDF(string url)
        {
            try
            {
                //string url = "/QuestionList" + "/DiaryWiseStarred/" + "12/" + "7";

                string directory = Server.MapPath(url);

                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                    //zip.AddDirectoryByName("Files");

                    if (Directory.Exists(directory))
                    {
                        zip.AddDirectory(directory, "salaryBills");
                    }
                    else
                    {
                    }

                    Response.Clear();
                    Response.BufferOutput = false;
                    string zipName = String.Format("{0}.zip", "salaryBill");
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);

                    zip.Save(Response.OutputStream);
                    Response.End();

                    return Response;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return "No File Available!";
            }

#pragma warning disable CS0162 // Unreachable code detected
            return Response;
#pragma warning restore CS0162 // Unreachable code detected
        }

        public ActionResult getSalaryStatement(string designationID, string monthID, string yearID, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] arr = new int[] { Convert.ToInt32(designationID), Convert.ToInt32(monthID), Convert.ToInt32(yearID), assemblyid, ispart };
            List<membersOuterReport> _membersOuterReport = (List<membersOuterReport>)Helper.ExecuteService("salaryheads", "generateOuterBillForMembers", arr);

            return PartialView("_salaryStatement", _membersOuterReport);
        }

        #endregion Generate Outer Bill

        #region schedule report

        public ActionResult ScheduleofRecovery()
        {
            return View();
        }

        public ActionResult ScheduleofRecoveryPartial()
        {
            return PartialView("_ScheduleofRecovery");
        }

        public ActionResult getListofScheduleReport(string designationID, String loanTypeID, string monthID, string yearID, int ispart)
        {
            int Assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] arr = new int[] { int.Parse(designationID), int.Parse(loanTypeID), int.Parse(monthID), int.Parse(yearID), Assemblyid, ispart };
            ScheduleReport _list = (ScheduleReport)Helper.ExecuteService("salaryheads", "getListofScheduleReport", arr);
            List<AccountHeadList> headlist = new List<AccountHeadList>();
            if (loanTypeID == "1")
            {
                headlist.Add(new AccountHeadList { code = "7610", text = "Loans to Government Servants" });
                headlist.Add(new AccountHeadList { code = "00", text = "" });
                headlist.Add(new AccountHeadList { code = "201", text = "House Building Advance" });
                headlist.Add(new AccountHeadList { code = "04", text = "Loan's to MLA's for Construction Of House" });
                _list.accountHead = "2011 - 02 - 101 - 03 - vs Members";
            }
            if (loanTypeID == "2")
            {
                headlist.Add(new AccountHeadList { code = "7610", text = "Loans to Government Servants" });
                headlist.Add(new AccountHeadList { code = "00", text = "" });
                headlist.Add(new AccountHeadList { code = "202", text = "Advance for Purchase of Motor conveyance" });
                headlist.Add(new AccountHeadList { code = "05", text = "Advance to MLA's for purchase of motor vehicle[Non Plan]" });
                _list.accountHead = "2011 - 02 - 101 - 03 - vs Members";
            }
            _list.accountHeadList = headlist;

            return PartialView("_scheduleReport", _list);
        }

        [ValidateInput(false)]
        public ActionResult generatepdfschedulereport(string PDFData, string monthID, string yearID)
        {
            PDFData = PDFData.Replace("font-size: 18px", "font-size: 14px");
            string outXml = "<html><body style=\"font-family:DVOT-Yogesh; font-size: 14px;margin-right:25px;margin-left: 25px;\"><div><div style=\"width: 100%;\">";
            outXml += PDFData;
            outXml += @"</div></div></body></html>";
            MemoryStream output = new MemoryStream();
            string htmlStringToConvert = outXml;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;

            pdfConverter.PdfHeaderOptions.HeaderHeight = 20;
            pdfConverter.PdfFooterOptions.FooterHeight = 40;

            //TextElement footerTextElement = new TextElement(0, 20, "Page: &p; of &P;",
            // new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            //footerTextElement.TextAlign = HorizontalTextAlign.Center;
            //pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;
            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            // stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            string url = "/ScheduleReport/12/" + monthID + "/" + yearID + "/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), yearID + "_" + monthID + "ScheduleReport.pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();

            DownloadPDF(url);
            return View();
        }

        #endregion

        #region Send Email

        public ActionResult sendSMSBYAutoID(string autoID)
        {
            string filepath = string.Empty;
            string SavedPdfPath = string.Empty;
            filepath = Helper.ExecuteService("salaryheads", "getBillFilePath", autoID) as string;
            salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", autoID);
            try
            {
                SavedPdfPath = filepath;
                SalaryNotification sn = new SalaryNotification();
                List<string> emailAddresses = new List<string>();
                List<string> phoneNumbers = new List<string>();
                sn.mailSubject = "testing mail";
                phoneNumbers.Add("9045337778");
                phoneNumbers.Add("8395055554");
                sn.mobileStatus = true;
                sn.emailStatus = false;
                sn.msgBody = "testing msg";
                sn.attachmentUrl = "";
                emailAddresses.Add("durgesh.net4@gmail.com");
                emailAddresses.Add("durgesh@sblsoftware.com");
                sn.mobileList = phoneNumbers;
                sn.emailList = emailAddresses;
                sn.mailBody = "test mail";
                string directory = Server.MapPath(SavedPdfPath);
                // SendEmailDetails(sn, Server.MapPath(SavedPdfPath));
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
            }

            return Json(SavedPdfPath, JsonRequestBehavior.AllowGet);
        }

        public ActionResult sendMAILBYAutoID(string autoID)
        {
            string[] returnResult = null;
            string filepath = string.Empty;
            string SavedPdfPath = string.Empty;

            filepath = Helper.ExecuteService("salaryheads", "getBillFilePath", autoID) as string;

            salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", autoID);
            try
            {
                if (s.designationName == "Speaker" || s.designationName == "Deputy Speaker")
                {
                    returnResult = GeneratePDFforSpeaker(s, false, filepath);
                }
                else
                {
                    returnResult = GeneratePDFforMembers(s, false, filepath);
                }

                SavedPdfPath = filepath;
                SalaryNotification sn = new SalaryNotification();
                List<string> emailAddresses = new List<string>();
                List<string> phoneNumbers = new List<string>();
                sn.mailSubject = "testing mail";
                phoneNumbers.Add("9045337778");
                phoneNumbers.Add("8395055554");

                sn.mobileStatus = false;
                sn.emailStatus = true;
                sn.msgBody = "testing msg";
                sn.attachmentUrl = "";
                emailAddresses.Add("durgesh.net4@gmail.com");
                emailAddresses.Add("durgesh@sblsoftware.com");
                sn.mobileList = phoneNumbers;
                sn.emailList = emailAddresses;
                string mailText = System.IO.File.ReadAllText(Server.MapPath("/SalaryMailer/index.html"));
                mailText = mailText.Replace("__MemberName__", s.memberName);
                mailText = mailText.Replace("__MonthName__ ", new DateTime(1900, Convert.ToInt32(s.monthID), 1).ToString("MMMM"));
                mailText = mailText.Replace("__year__", s.yearId.ToString());
                mailText = mailText.Replace("__Allowances__", returnResult[1]);
                mailText = mailText.Replace("__Deductions__", returnResult[2]);
                mailText = mailText.Replace("__totalAllowances__", s.totalAllowances + "");
                mailText = mailText.Replace("__totalDeductions__", s.totalDeduction + "");
                mailText = mailText.Replace("__grossAmount__", s.netSalary + "");
                mailText = mailText.Replace("__amountinwords__", Convert.ToInt32(s.netSalary).NumbersToWords() + " Only");
                sn.mailBody = mailText;
                string directory = Server.MapPath(SavedPdfPath);
                // SendEmailDetails(sn, Server.MapPath(SavedPdfPath));
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
            }

            return Json(SavedPdfPath, JsonRequestBehavior.AllowGet);
        }

        public static void SendEmailDetails(SalaryNotification EntityModel, string SavedPdfPath)
        {
            #region SMS And Email

            List<string> emailAddresses = new List<string>();
            List<string> phoneNumbers = new List<string>();

            SMSMessageList smsMessage = new SMSMessageList();
            smsMessage.isLocale = true;

            CustomMailMessage emailMessage = new CustomMailMessage();

            //# SMS Settings
            if (EntityModel != null)
            {
                emailAddresses.AddRange(EntityModel.emailList);
                phoneNumbers.AddRange(EntityModel.mobileList);
            }
            if (emailAddresses != null && phoneNumbers != null)
            {
                foreach (var item in phoneNumbers)
                {
                    smsMessage.MobileNo.Add(item);
                }
                foreach (var item in emailAddresses)
                {
                    emailMessage._toList.Add(item);
                }
            }

            smsMessage.SMSText = EntityModel.msgBody;
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = 1;
            emailMessage._isBodyHtml = true;
            emailMessage._subject = EntityModel.mailSubject;
            emailMessage._body = EntityModel.mailBody;
            EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes((SavedPdfPath)) };
            emailMessage._attachments = new List<EAttachment>();
            emailMessage._attachments.Add(ea);
            try
            {
                if (EntityModel.emailList != null || EntityModel.mobileList != null)
                {
                    Notification.Send(EntityModel.mobileStatus, EntityModel.emailStatus, smsMessage, emailMessage);
                }
            }
            catch (Exception exp)
            {
                string errorMsg = exp.Message;

            }

            #endregion SMS And Email
        }

        #endregion Send Email

        #region Nomianl Report

        public ActionResult NominalReport()
        {
            return PartialView("_NominalReport");
        }

        public ActionResult GETNominalReport(string designationID, string Increment_param, string DA_param, string CCA_param, string CAP_param, string LECAmount_param, string LTCAmount_param, string financialYear)
        {

            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            MembersNominalReport report = null;
            if (designationID != "3")
            {
                int[] arr = new int[] { Convert.ToInt32(designationID), assemblyid };
                report = (MembersNominalReport)Helper.ExecuteService("salaryheads", "getNominalReport", arr);
            }
            else
            {
                report = (MembersNominalReport)Helper.ExecuteService("salaryheads", "getNominalReportForStaff", new string[] { Increment_param, DA_param, CCA_param, CAP_param, LECAmount_param, LTCAmount_param, financialYear });
                report.gTotal = Convert.ToInt64(report.Total).NumbersToWords();
            }
            return PartialView("_NominalReportList", report);
        }



        [ValidateInput(false)]
        public ActionResult GenerateNominalReport(string PDFData)
        {
            //  string outXml = "<html><body style='font-family:DVOT-Yogesh; font:8px; margin-left:40px;  margin-right:9px;'><div><div style='width: 100%;   white-space: nowrap;'>";

            //  //  outXml += "<table cellpadding=\"2\"  style=\"width: 100%; font-size: 10px; border-collapse:collapse; font:10px;  white-space: nowrap; \" width='100%'>";
            //  outXml += PDFData;
            //  outXml += @"</div></div></body></html>";
            //  MemoryStream output = new MemoryStream();
            //  string htmlStringToConvert = outXml;
            //  PdfConverter pdfConverter = new PdfConverter();
            //  pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            //  pdfConverter.PdfDocumentOptions.RightMargin = 10;
            //  pdfConverter.PdfDocumentOptions.ShowFooter = true;
            //  pdfConverter.PdfDocumentOptions.ShowHeader = true;
            //  pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Landscape;

            //  pdfConverter.PdfHeaderOptions.HeaderHeight = 10;
            //  pdfConverter.PdfFooterOptions.FooterHeight = 30;

            //  TextElement footerTextElement = new TextElement(0, 20, "Page: &p; of &P;",
            //   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            //  footerTextElement.TextAlign = HorizontalTextAlign.Center;
            //  pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

            //  string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            //  //ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            //  // imageElement1.KeepAspectRatio = true;
            //  //imageElement1.Opacity = 100;
            // // imageElement1.DestHeight = 97f;
            // // imageElement1.DestWidth = 71f;
            //  // pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            // // ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            // // imageElement2.KeepAspectRatio = false;
            ////  imageElement2.Opacity = 5;
            ////  imageElement2.DestHeight = 284f;
            ////  imageElement2.DestWidth = 388F;

            //  EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            //  float stampWidth = float.Parse("400");
            //  float stampHeight = float.Parse("400");

            //  // Center the stamp at the top of PDF page
            //  float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            //  float stampYLocation = 150;

            //  RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            //  Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            //  // stampTemplate.AddElement(imageElement2);
            //  byte[] pdfBytes = null;

            //  try
            //  {
            //      pdfBytes = pdfDocument.Save();
            //  }
            //  finally
            //  {
            //      // close the Document to realease all the resources
            //      pdfDocument.Close();
            //  }

            //string url = "/NominalReport/12/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/";

            //string directory = Server.MapPath(url);

            //if (!Directory.Exists(directory))
            //{
            //    Directory.CreateDirectory(directory);
            //}

            //string path = Path.Combine(Server.MapPath("~" + url), DateTime.Now.Year + "_" + DateTime.Now.Month + "NominalReport.pdf");

            //FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            //System.IO.FileAccess.Write);

            //_FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            //// close file stream
            //_FileStream.Close();

            //DownloadPDF(url);


            string Result = string.Empty;


            Result = PDFData;
            Result = Result.Replace("+", "%2b");
            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=Nominalroll.doc");
            Response.Charset = "";
            Response.ContentType = "application/doc";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            //return new EmptyResult();

            return View();
        }

        #endregion

        #region Bill NO

        //public int billNO(int designationType, int budgetTypeID, int billtype, int monthID, int yearID, double TotalGrossAmount, double TotalDeduction, int? memberID, string Mode, int ispart)
        //{
        //    int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
        //    mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
        //    int billNumber = (int)Helper.ExecuteService("salaryheads", "checkSalaryBill", new int[] { designationType, monthID, yearID, budgetTypeID, assemblyid, ispart });
        //    if (billNumber > 0)
        //    {   //getEbillDetails
        //        int EstablishBillId = (int)Helper.ExecuteService("salaryheads", "getEbillDetails", new int[] { billNumber, monthID, yearID });
        //        mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillId });
        //        EstablishBill.TotalAmount = Convert.ToDecimal(TotalGrossAmount - TotalDeduction);
        //        EstablishBill.TotalGrossAmount = Convert.ToDecimal(TotalGrossAmount);
        //        EstablishBill.Deduction = Convert.ToDecimal(TotalDeduction);
        //        EstablishBill.ModifiedBy = CurrentSession.UserName;
        //        EstablishBill.ModifiedWhen = DateTime.Now;
        //        EstablishBill.CreatedBy = CurrentSession.UserName;
        //        Helper.ExecuteService("Budget", "UpdateEstablishBill", EstablishBill);
        //        //updateBillDetailsInSalary

        //        DataSet dataSet = new DataSet();
        //        var Parameter = new List<KeyValuePair<string, string>>();
        //        Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", EstablishBillId.ToString()));
        //        dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteVoucherByEstablishBillId", Parameter);
        //        if (designationType != 2)
        //        {
        //            mReimbursementBill Bill = new mReimbursementBill();
        //            Bill.SactionAmount = Convert.ToDecimal(TotalGrossAmount - TotalDeduction);
        //            Bill.GrossAmount = Convert.ToDecimal(TotalGrossAmount);

        //            Bill.Deduction = Convert.ToDecimal(TotalDeduction);

        //            Bill.BillFromDate = new DateTime(yearID, monthID, 1);
        //            Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
        //            Bill.Remarks = string.Empty;
        //            Bill.IsRejected = false;
        //            Bill.IsChallanGenerate = true;
        //            Bill.BillType = billtype;
        //            Bill.BudgetId = budgetTypeID;
        //            if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
        //                Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        //            string FinYearValue = SiteSettingsSessn.SettingValue;
        //            Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
        //            Bill.FinancialYear = FinYearValue;
        //            Bill.EstablishBillId = EstablishBillId;
        //            Bill.EstblishBillNumber = billNumber;
        //            Bill.DateOfPresentation = DateTime.Now;
        //            Bill.DesignationS = designationType.ToString();
        //            Bill.ClaimantId = memberID.Value;
        //            Bill.Designation = designationType;
        //            Bill.CreatedBy = CurrentSession.UserName;
        //            int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);
        //            Helper.ExecuteService("salaryheads", "updateBillDetailsInSalary", new int[] { EstablishBillId, monthID, yearID, memberID.Value, assemblyid });
        //        }
        //        else
        //        {
        //            List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "SalaryBillList", new int[] { monthID, yearID, designationType + 1, assemblyid, ispart });
        //            foreach (var item in _slist)
        //            {
        //                mReimbursementBill Bill = new mReimbursementBill();
        //                Bill.SactionAmount = Convert.ToDecimal(item.totalAllowances - item.totalDeductions);
        //                Bill.GrossAmount = Convert.ToDecimal(item.totalAllowances);

        //                Bill.Deduction = Convert.ToDecimal(item.totalDeductions);

        //                Bill.BillFromDate = new DateTime(yearID, monthID, 1);
        //                Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
        //                Bill.Remarks = string.Empty;
        //                Bill.IsRejected = false;
        //                Bill.IsChallanGenerate = true;
        //                Bill.BillType = billtype;
        //                Bill.BudgetId = budgetTypeID;
        //                if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
        //                    Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        //                string FinYearValue = SiteSettingsSessn.SettingValue;
        //                Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
        //                Bill.FinancialYear = FinYearValue;
        //                Bill.EstablishBillId = EstablishBillId;
        //                Bill.EstblishBillNumber = billNumber;
        //                Bill.DateOfPresentation = DateTime.Now;
        //                Bill.DesignationS = designationType.ToString();
        //                Bill.ClaimantId = item.memberID;
        //                Bill.Designation = designationType;
        //                Bill.CreatedBy = CurrentSession.UserName;

        //                int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);
        //                Helper.ExecuteService("salaryheads", "updateBillDetailsInSalary", new int[] { EstablishBillId, monthID, yearID, item.memberID, assemblyid });
        //            }
        //        }

        //        return billNumber;
        //    }
        //    else
        //    {
        //        mEstablishBill LastEstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetLastEstablishBill", new mEstablishBill { FinancialYear = SiteSettingsSessn.SettingValue });
        //        mEstablishBill obj = new mEstablishBill();
        //        obj.Designation = designationType;
        //        obj.BudgetId = budgetTypeID;
        //        obj.SanctionDate = DateTime.Now;
        //        obj.BillType = billtype;
        //        obj.SanctioNumber = "NA";
        //        obj.FinancialYear = SiteSettingsSessn.SettingValue;
        //        obj.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
        //        obj.TotalAmount = Convert.ToDecimal(TotalGrossAmount - TotalDeduction);
        //        obj.TotalGrossAmount = Convert.ToDecimal(TotalGrossAmount);
        //        obj.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        //        obj.IsBillUpdate = true;
        //        obj.sMOnthID = monthID;
        //        obj.sYearID = yearID;
        //        obj.IsChallanGenerate = true;
        //        obj.CreatedBy = CurrentSession.UserName;
        //        obj.AssemblyId = assemblyid;
        //        obj.Deduction = Convert.ToDecimal(TotalDeduction);
        //        obj.ispart = ispart;
        //        if (LastEstablishBill != null && LastEstablishBill.EstablishBillID > 0)
        //        {
        //            if (SiteSettingsSessn.SettingValue == LastEstablishBill.FinancialYear)
        //                obj.BillNumber = LastEstablishBill.BillNumber + 1;
        //            else
        //                obj.BillNumber = 1;
        //        }
        //        else
        //        {
        //            obj.BillNumber = 1;
        //        }
        //        billNumber = obj.BillNumber;
        //        obj.CreatedBy = CurrentSession.UserName;
        //        obj.BillProcessingDate = DateTime.Now;

        //        if (Mode == "Search")
        //        {
        //            if (designationType != 2)
        //            {
        //                mReimbursementBill Bill = new mReimbursementBill();
        //                Bill.SactionAmount = Convert.ToDecimal(TotalGrossAmount - TotalDeduction);
        //                Bill.GrossAmount = Convert.ToDecimal(TotalGrossAmount);

        //                Bill.Deduction = Convert.ToDecimal(TotalDeduction);

        //                Bill.BillFromDate = new DateTime(yearID, monthID, 1);
        //                Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
        //                Bill.Remarks = string.Empty;
        //                Bill.IsRejected = false;
        //                Bill.IsChallanGenerate = true;
        //                Bill.BillType = billtype;
        //                Bill.BudgetId = budgetTypeID;
        //                if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
        //                    Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        //                string FinYearValue = SiteSettingsSessn.SettingValue;
        //                Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
        //                Bill.FinancialYear = FinYearValue;
        //                //  Bill.EstablishBillId = EstablishBillId;
        //                Bill.EstblishBillNumber = billNumber;
        //                Bill.DateOfPresentation = DateTime.Now;
        //                Bill.DesignationS = designationType.ToString();
        //                Bill.ClaimantId = memberID.Value;
        //                Bill.Designation = designationType;
        //                Bill.CreatedBy = CurrentSession.UserName;
        //                // int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);
        //                // Helper.ExecuteService("salaryheads", "updateBillDetailsInSalary", new int[] { EstablishBillId, monthID, yearID, memberID.Value });
        //            }
        //            else
        //            {
        //                List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "SalaryBillList", new int[] { monthID, yearID, designationType + 1, assemblyid, ispart });
        //                foreach (var item in _slist)
        //                {
        //                    mReimbursementBill Bill = new mReimbursementBill();
        //                    Bill.SactionAmount = Convert.ToDecimal(item.totalAllowances - item.totalDeductions);
        //                    Bill.GrossAmount = Convert.ToDecimal(item.totalAllowances);

        //                    Bill.Deduction = Convert.ToDecimal(item.totalDeductions);

        //                    Bill.BillFromDate = new DateTime(yearID, monthID, 1);
        //                    Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
        //                    Bill.Remarks = string.Empty;
        //                    Bill.IsRejected = false;
        //                    Bill.IsChallanGenerate = true;
        //                    Bill.BillType = billtype;
        //                    Bill.BudgetId = budgetTypeID;
        //                    if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
        //                        Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        //                    string FinYearValue = SiteSettingsSessn.SettingValue;
        //                    Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
        //                    Bill.FinancialYear = FinYearValue;
        //                    //        Bill.EstablishBillId = EstablishBillId;
        //                    Bill.EstblishBillNumber = billNumber;
        //                    Bill.DateOfPresentation = DateTime.Now;
        //                    Bill.DesignationS = designationType.ToString();
        //                    Bill.ClaimantId = item.memberID;
        //                    Bill.Designation = designationType;
        //                    Bill.CreatedBy = CurrentSession.UserName;
        //                    //       int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);


        //                }
        //            }
        //        }
        //        else
        //        {
        //            int MainEstablishBillId = (int)Helper.ExecuteService("Budget", "CreateEstablishBill", obj);
        //            DataSet dataSet = new DataSet();
        //            var Parameter = new List<KeyValuePair<string, string>>();
        //            Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", MainEstablishBillId.ToString()));
        //            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteVoucherByEstablishBillId", Parameter);
        //            if (designationType != 2)
        //            {
        //                mReimbursementBill Bill = new mReimbursementBill();
        //                Bill.SactionAmount = Convert.ToDecimal(TotalGrossAmount - TotalDeduction);
        //                Bill.GrossAmount = Convert.ToDecimal(TotalGrossAmount);
        //                Bill.Deduction = Convert.ToDecimal(TotalDeduction);
        //                Bill.BillFromDate = new DateTime(yearID, monthID, 1);
        //                Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
        //                Bill.Remarks = string.Empty;
        //                Bill.IsRejected = false;
        //                Bill.IsChallanGenerate = true;
        //                Bill.BillType = billtype;
        //                Bill.BudgetId = budgetTypeID;
        //                if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
        //                    Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        //                string FinYearValue = SiteSettingsSessn.SettingValue;
        //                Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
        //                Bill.FinancialYear = FinYearValue;
        //                Bill.EstablishBillId = MainEstablishBillId;
        //                Bill.EstblishBillNumber = billNumber;
        //                Bill.DateOfPresentation = DateTime.Now;
        //                Bill.DesignationS = designationType.ToString();
        //                Bill.ClaimantId = memberID.Value;
        //                Bill.Designation = designationType;
        //                Bill.CreatedBy = CurrentSession.UserName;

        //                int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);
        //            }
        //            else
        //            {
        //                List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "SalaryBillList", new int[] { monthID, yearID, designationType + 1, assemblyid, ispart });
        //                foreach (var item in _slist)
        //                {
        //                    mReimbursementBill Bill = new mReimbursementBill();
        //                    Bill.SactionAmount = Convert.ToDecimal(item.totalAllowances - item.totalDeductions);
        //                    Bill.GrossAmount = Convert.ToDecimal(item.totalAllowances);
        //                    Bill.Deduction = Convert.ToDecimal(item.totalDeductions);
        //                    Bill.BillFromDate = new DateTime(yearID, monthID, 1);
        //                    Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
        //                    Bill.Remarks = string.Empty;
        //                    Bill.IsRejected = false;
        //                    Bill.IsChallanGenerate = true;
        //                    Bill.BillType = billtype;
        //                    Bill.BudgetId = budgetTypeID;
        //                    if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
        //                        Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        //                    string FinYearValue = SiteSettingsSessn.SettingValue;
        //                    Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
        //                    Bill.FinancialYear = FinYearValue;
        //                    Bill.EstablishBillId = MainEstablishBillId;
        //                    Bill.EstblishBillNumber = billNumber;
        //                    Bill.DateOfPresentation = DateTime.Now;
        //                    Bill.DesignationS = designationType.ToString();
        //                    Bill.ClaimantId = item.memberID;
        //                    Bill.Designation = designationType;
        //                    Bill.CreatedBy = CurrentSession.UserName;
        //                    int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);
        //                }
        //            }

        //        }
        //    }

        //    return billNumber;
        //}
        public int billNO(int designationType, int budgetTypeID, int billtype, int monthID, int yearID, double TotalGrossAmount, double TotalDeduction, int? memberID, string Mode, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            // int billNumber = 0;
            int billNumber = (int)Helper.ExecuteService("salaryheads", "checkSalaryBill", new int[] { designationType, monthID, yearID, budgetTypeID, assemblyid, ispart });
            if (billNumber > 0)
            {   //getEbillDetails
                int EstablishBillId = (int)Helper.ExecuteService("salaryheads", "getEbillDetails", new int[] { billNumber, monthID, yearID });
                mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillId });
                EstablishBill.TotalAmount = Convert.ToDecimal(TotalGrossAmount - TotalDeduction);
                EstablishBill.TotalGrossAmount = Convert.ToDecimal(TotalGrossAmount);
                EstablishBill.Deduction = Convert.ToDecimal(TotalDeduction);
                EstablishBill.ModifiedBy = CurrentSession.UserName;
                EstablishBill.ModifiedWhen = DateTime.Now;
                EstablishBill.CreatedBy = CurrentSession.UserName;
                Helper.ExecuteService("Budget", "UpdateEstablishBill", EstablishBill);
                //updateBillDetailsInSalary

                DataSet dataSet = new DataSet();
                var Parameter = new List<KeyValuePair<string, string>>();
                Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", EstablishBillId.ToString()));
                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteVoucherByEstablishBillId", Parameter);
                if (designationType != 2)
                {
                    //for Spk/Dy.Spk/C.Whip
                    mReimbursementBill Bill = new mReimbursementBill();
                    Bill.SactionAmount = Convert.ToDecimal(TotalGrossAmount - TotalDeduction);
                    Bill.GrossAmount = Convert.ToDecimal(TotalGrossAmount);

                    Bill.Deduction = Convert.ToDecimal(TotalDeduction);

                    Bill.BillFromDate = new DateTime(yearID, monthID, 1);
                    Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
                    Bill.Remarks = string.Empty;
                    Bill.IsRejected = false;
                    Bill.IsChallanGenerate = true;
                    Bill.BillType = billtype;
                    Bill.BudgetId = budgetTypeID;
                    if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
                        Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                    string FinYearValue = SiteSettingsSessn.SettingValue;
                    Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
                    Bill.FinancialYear = FinYearValue;
                    Bill.EstablishBillId = EstablishBillId;
                    Bill.EstblishBillNumber = billNumber;
                    Bill.DateOfPresentation = DateTime.Now;
                    Bill.DesignationS = designationType.ToString();
                    Bill.ClaimantId = memberID.Value;
                    Bill.Designation = designationType;
                    Bill.CreatedBy = CurrentSession.UserName;
                    int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);
                    Helper.ExecuteService("salaryheads", "updateBillDetailsInSalary", new int[] { EstablishBillId, monthID, yearID, memberID.Value, assemblyid, ispart });
                }
                else
                {
                    //For Members
                    List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "SalaryBillList", new int[] { monthID, yearID, designationType + 1, assemblyid, ispart });
                    foreach (var item in _slist)
                    {
                        mReimbursementBill Bill = new mReimbursementBill();
                        Bill.SactionAmount = Convert.ToDecimal(item.totalAllowances - item.totalDeductions);
                        Bill.GrossAmount = Convert.ToDecimal(item.totalAllowances);

                        Bill.Deduction = Convert.ToDecimal(item.totalDeductions);

                        Bill.BillFromDate = new DateTime(yearID, monthID, 1);
                        Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
                        Bill.Remarks = string.Empty;
                        Bill.IsRejected = false;
                        Bill.IsChallanGenerate = true;
                        Bill.BillType = billtype;
                        Bill.BudgetId = budgetTypeID;
                        if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
                            Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                        string FinYearValue = SiteSettingsSessn.SettingValue;
                        Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
                        Bill.FinancialYear = FinYearValue;
                        Bill.EstablishBillId = EstablishBillId;
                        Bill.EstblishBillNumber = billNumber;
                        Bill.DateOfPresentation = DateTime.Now;
                        Bill.DesignationS = designationType.ToString();
                        Bill.ClaimantId = item.memberID;
                        Bill.Designation = designationType;
                        Bill.CreatedBy = CurrentSession.UserName;

                        int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);
                        Helper.ExecuteService("salaryheads", "updateBillDetailsInSalary", new int[] { EstablishBillId, monthID, yearID, item.memberID, assemblyid, ispart });
                    }
                }

                return billNumber;
            }
            else
            {
                mEstablishBill LastEstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetLastEstablishBill", new mEstablishBill { FinancialYear = SiteSettingsSessn.SettingValue });
                mEstablishBill obj = new mEstablishBill();
                obj.Designation = designationType;
                obj.BudgetId = budgetTypeID;
                obj.SanctionDate = DateTime.Now;
                obj.BillType = billtype;
                obj.SanctioNumber = "NA";
                obj.FinancialYear = SiteSettingsSessn.SettingValue;
                obj.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
                obj.TotalAmount = Convert.ToDecimal(TotalGrossAmount - TotalDeduction);
                obj.TotalGrossAmount = Convert.ToDecimal(TotalGrossAmount);
                obj.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                obj.IsBillUpdate = true;
                obj.sMOnthID = monthID;
                obj.sYearID = yearID;
                obj.IsChallanGenerate = true;
                obj.CreatedBy = CurrentSession.UserName;
                obj.AssemblyId = assemblyid;
                obj.Deduction = Convert.ToDecimal(TotalDeduction);
                obj.ispart = ispart;
                if (LastEstablishBill != null && LastEstablishBill.EstablishBillID > 0)
                {
                    if (SiteSettingsSessn.SettingValue == LastEstablishBill.FinancialYear)
                        obj.BillNumber = LastEstablishBill.BillNumber + 1;
                    else
                        obj.BillNumber = 1;
                }
                else
                {
                    obj.BillNumber = 1;
                }
                billNumber = obj.BillNumber;
                obj.CreatedBy = CurrentSession.UserName;
                obj.BillProcessingDate = DateTime.Now;

                if (Mode == "Search")
                {
                    if (designationType != 2)
                    {
                        mReimbursementBill Bill = new mReimbursementBill();
                        Bill.SactionAmount = Convert.ToDecimal(TotalGrossAmount - TotalDeduction);
                        Bill.GrossAmount = Convert.ToDecimal(TotalGrossAmount);

                        Bill.Deduction = Convert.ToDecimal(TotalDeduction);

                        Bill.BillFromDate = new DateTime(yearID, monthID, 1);
                        Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
                        Bill.Remarks = string.Empty;
                        Bill.IsRejected = false;
                        Bill.IsChallanGenerate = true;
                        Bill.BillType = billtype;
                        Bill.BudgetId = budgetTypeID;
                        if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
                            Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                        string FinYearValue = SiteSettingsSessn.SettingValue;
                        Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
                        Bill.FinancialYear = FinYearValue;
                        //  Bill.EstablishBillId = EstablishBillId;
                        Bill.EstblishBillNumber = billNumber;
                        Bill.DateOfPresentation = DateTime.Now;
                        Bill.DesignationS = designationType.ToString();
                        Bill.ClaimantId = memberID.Value;
                        Bill.Designation = designationType;
                        Bill.CreatedBy = CurrentSession.UserName;
                        // int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);
                        // Helper.ExecuteService("salaryheads", "updateBillDetailsInSalary", new int[] { EstablishBillId, monthID, yearID, memberID.Value });
                    }
                    else
                    {
                        List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "SalaryBillList", new int[] { monthID, yearID, designationType + 1, assemblyid, ispart });
                        foreach (var item in _slist)
                        {
                            mReimbursementBill Bill = new mReimbursementBill();
                            Bill.SactionAmount = Convert.ToDecimal(item.totalAllowances - item.totalDeductions);
                            Bill.GrossAmount = Convert.ToDecimal(item.totalAllowances);

                            Bill.Deduction = Convert.ToDecimal(item.totalDeductions);

                            Bill.BillFromDate = new DateTime(yearID, monthID, 1);
                            Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
                            Bill.Remarks = string.Empty;
                            Bill.IsRejected = false;
                            Bill.IsChallanGenerate = true;
                            Bill.BillType = billtype;
                            Bill.BudgetId = budgetTypeID;
                            if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
                                Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                            string FinYearValue = SiteSettingsSessn.SettingValue;
                            Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
                            Bill.FinancialYear = FinYearValue;
                            //        Bill.EstablishBillId = EstablishBillId;
                            Bill.EstblishBillNumber = billNumber;
                            Bill.DateOfPresentation = DateTime.Now;
                            Bill.DesignationS = designationType.ToString();
                            Bill.ClaimantId = item.memberID;
                            Bill.Designation = designationType;
                            Bill.CreatedBy = CurrentSession.UserName;
                            //       int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);


                        }
                    }
                }
                else
                {
                    int MainEstablishBillId = (int)Helper.ExecuteService("Budget", "CreateEstablishBill", obj);
                    DataSet dataSet = new DataSet();
                    var Parameter = new List<KeyValuePair<string, string>>();
                    Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", MainEstablishBillId.ToString()));
                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteVoucherByEstablishBillId", Parameter);
                    if (designationType != 2)
                    {
                        mReimbursementBill Bill = new mReimbursementBill();
                        Bill.SactionAmount = Convert.ToDecimal(TotalGrossAmount - TotalDeduction);
                        Bill.GrossAmount = Convert.ToDecimal(TotalGrossAmount);
                        Bill.Deduction = Convert.ToDecimal(TotalDeduction);
                        Bill.BillFromDate = new DateTime(yearID, monthID, 1);
                        Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
                        Bill.Remarks = string.Empty;
                        Bill.IsRejected = false;
                        Bill.IsChallanGenerate = true;
                        Bill.BillType = billtype;
                        Bill.BudgetId = budgetTypeID;
                        if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
                            Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                        string FinYearValue = SiteSettingsSessn.SettingValue;
                        Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
                        Bill.FinancialYear = FinYearValue;
                        Bill.EstablishBillId = MainEstablishBillId;
                        Bill.EstblishBillNumber = billNumber;
                        Bill.DateOfPresentation = DateTime.Now;
                        Bill.DesignationS = designationType.ToString();
                        Bill.ClaimantId = memberID.Value;
                        Bill.Designation = designationType;
                        Bill.CreatedBy = CurrentSession.UserName;

                        int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);
                    }
                    else
                    {
                        List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "SalaryBillList", new int[] { monthID, yearID, designationType + 1, assemblyid, ispart });
                        foreach (var item in _slist)
                        {
                            mReimbursementBill Bill = new mReimbursementBill();
                            Bill.SactionAmount = Convert.ToDecimal(item.totalAllowances - item.totalDeductions);
                            Bill.GrossAmount = Convert.ToDecimal(item.totalAllowances);
                            Bill.Deduction = Convert.ToDecimal(item.totalDeductions);
                            Bill.BillFromDate = new DateTime(yearID, monthID, 1);
                            Bill.BillToDate = new DateTime(yearID, monthID, DateTime.DaysInMonth(yearID, monthID));
                            Bill.Remarks = string.Empty;
                            Bill.IsRejected = false;
                            Bill.IsChallanGenerate = true;
                            Bill.BillType = billtype;
                            Bill.BudgetId = budgetTypeID;
                            if (Convert.ToDateTime(Bill.EncashmentDate).ToShortDateString() == "01/01/0001")
                                Bill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                            string FinYearValue = SiteSettingsSessn.SettingValue;
                            Bill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
                            Bill.FinancialYear = FinYearValue;
                            Bill.EstablishBillId = MainEstablishBillId;
                            Bill.EstblishBillNumber = billNumber;
                            Bill.DateOfPresentation = DateTime.Now;
                            Bill.DesignationS = designationType.ToString();
                            Bill.ClaimantId = item.memberID;
                            Bill.Designation = designationType;
                            Bill.CreatedBy = CurrentSession.UserName;
                            int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", Bill);
                        }
                    }

                }
            }

            return billNumber;
        }

        #endregion

        #region checkBillStatus

        public ActionResult checkBillStatus(string designationID, string monthID, string yearID, int ispart)
        {
            int assemblyid = Convert.ToInt16(CurrentSession.AssemblyId);
            int[] arr = { int.Parse(monthID), int.Parse(yearID), int.Parse(designationID), assemblyid, ispart };
            List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "checkSalaryBillList", arr);
            if (_slist.Count > 0)
            {
                return PartialView("_pendingBillList", _slist);
            }
            else
            {
                return Json(new { data = true }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}