﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.HouseCommittee
{
    public class HouseCommitteeAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HouseCommittee";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HouseCommittee_default",
                "HouseCommittee/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
