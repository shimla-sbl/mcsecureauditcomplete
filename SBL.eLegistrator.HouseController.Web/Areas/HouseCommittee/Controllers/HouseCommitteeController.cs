﻿using EvoPdf;
using Newtonsoft.Json.Linq;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.eFile;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Areas.eFile.Models;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SBL.eLegistrator.HouseController.Filters;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Employee;
using Lib.Web.Mvc.JQuery.JqGrid;
using Microsoft.Security.Application;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Extensions;
using SMS.API;
using Email.API;
using SBL.DomainModel.Models.PrintingPress;
using SBL.DomainModel.Models.StaffManagement;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Member;
using Microsoft.Office.Interop.Word;
using SBL.DomainModel.Models.Diaries;
using Spire.Doc;
using Spire.Doc.Documents;
using iTextSharp.text.pdf;
using SBL.DomainModel.Models.CommitteeReport;
using Spire.Doc.Fields;
using SBL.DomainModel.ComplexModel;
using SMS.WebService;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.RecipientGroups;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Net;

namespace SBL.eLegistrator.HouseController.Web.Areas.HouseCommittee.Controllers
{
    public class HouseCommitteeController : Controller
    {
        //
        // GET: /HouseCommittee/HouseCommittee/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult eFileLogin()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            return null;
        }

        public ActionResult _eFileBookDetailPartial(eFileViewModel model)
        {
            return View(model);
        }

        public ActionResult _eFileBookViewByIDPartial(eFileViewModel model)
        {
            try
            {
                eFileAttachment documents = new eFileAttachment();
                List<int> PDFPageNumbers = new List<int>();
                documents.eFileID = model.eFileID;
                documents.DepartmentId = CurrentSession.DeptID;

                ViewBag.Subject = model.eFileSubject;
                ViewBag.eFileNum = model.eFileNumber;
                //model. = (List<string>)Helper.ExecuteService("eFile", "eFileDocumentIndex", model.eFileID);
                model.eFileNotingList = (List<eFileNoting>)Helper.ExecuteService("eFile", "GeteFileNotingDetailsByeFileID", model.eFileID);

            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return View(model);
            //return View(model);
        }

        public ActionResult _eFileViewByID(eFileViewModel model)
        {
            eFileLogin();
            try
            {
                eFileViewModel model2 = new eFileViewModel();

                model2.DepartmentName = model.DepartmentName;
                //line for office
                model2.eFileNumber = model.eFileNumber;
                model2.eFileSubject = model.eFileSubject;
                model2.eFileID = model.eFileID;
                model2.eFileDetail = (SBL.DomainModel.Models.eFile.eFile)Helper.ExecuteService("eFile", "GeteFileDetailsByFileId", model.eFileID);
                model2.NewDescription = model2.eFileDetail.NewDescription;
                model2.DepartmentId = model2.eFileDetail.DepartmentId;
                model2.Officecode = model2.eFileDetail.Officecode;
                return View(model2);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Create a new eFile
        /// </summary>
        /// <returns>eFileViewModel</returns>
        [HttpGet]
        public ActionResult AddeFile(int CommitteeId)
        {
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel();
            model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            model.OldDescription = "PreText";
            model.NewDescription = "FutureText";
            return View(model);
        }

        [HttpPost]
        public JsonResult AddeFile(SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model, int lstOffice)
        {
            eFileLogin();
            SBL.DomainModel.Models.eFile.eFile eFileModel = SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.ModelMapping.ToDomainModel(model);
            eFileModel.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
            eFileModel.CreatedDate = DateTime.Now;
            eFileModel.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
            eFileModel.ModifiedDate = DateTime.Now;
            eFileModel.Officecode = lstOffice;// model.Officecode;
            eFileModel.eFileSubject = model.eFileSubject.Replace("'", "");
            eFileModel.eFileNumber = model.eFileNumber.Replace("'", "");
            //eFileModel.CommitteeId = 1;
            Helper.ExecuteService("eFile", "AddeFile", eFileModel);
            //return RedirectToAction("eFileListAll", "eFile", routeValues: new { CommitteeId = model.CommitteeId });

            return Json("Save Successfully", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddeFileAttachments(string Id)
        {
            eFileLogin();
            SBL.DomainModel.Models.eFile.eFileAttachment model = new SBL.DomainModel.Models.eFile.eFileAttachment();
            //Apply eFileId
            model.eFileID = Convert.ToInt32(1);
            return View(model);
        }

        [HttpPost]
        public ActionResult AddeFileAttachments(SBL.DomainModel.Models.eFile.eFileAttachment model, HttpPostedFileBase file, int CommId = 0)
        {
            try
            {
                if (file != null)
                {
                    model.eFileName = Path.GetFileNameWithoutExtension(file.FileName);
                    if (!Directory.Exists(Server.MapPath("~/eFileAttachments/" + model.eFileID)))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/eFileAttachments/" + model.eFileID));
                    };
                    string destinationPath = Path.Combine(Server.MapPath("~/eFileAttachments/" + model.eFileID), Path.GetFileName(file.FileName));
                    file.SaveAs(destinationPath);

                    //PDF to Image.
                    List<int> PDFPageNumbers = new List<int>();
                    if (!Directory.Exists(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles"));
                        PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);
                    }
                    else
                    {
                        PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);
                    }

                    model.SplitFileCount = PDFPageNumbers.Count;
                    model.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
                    model.ModifiedDate = DateTime.Now;
                    model.eFileNameId = Guid.NewGuid();
                    //model.Approve = 1;
                    model.ParentId = model.ParentId == 0 ? 0 : model.ParentId;
                    var value = Helper.ExecuteService("eFile", "AddeFileAttachments", model);
                }

                //if (model.Type != "FromeFile")
                //{
                //    // var result = Helper.ExecuteService("eFile", "GetDepartmentMembers", null);
                //    List<SBL.DomainModel.Models.Member.DepartmentMembers> deptmembers = new List<DomainModel.Models.Member.DepartmentMembers>();
                //    deptmembers = (List<SBL.DomainModel.Models.Member.DepartmentMembers>)Helper.ExecuteService("eFile", "GetDepartmentMembers", null);
                //    CustomMailMessage emailMessage = new CustomMailMessage();

                //    emailMessage._from = "evidhanmail@gmail.com";
                //    emailMessage._isBodyHtml = false;
                //    emailMessage._subject = "Notice From Committee";
                //    emailMessage._body = "Notice From Committee.Please find the attachment.";
                //    emailMessage._toList.Add("test@gmail.com");
                //    //foreach (var item in deptmembers)
                //    //{
                //    //    emailMessage._toList.Add(item.Email);
                //    //}
                //    emailMessage.ProccessDate = DateTime.Now;
                //    EAttachment ea = new EAttachment { FileName = new FileInfo(Server.MapPath("~/EmailAttachment/Test.txt")).Name, FileContent = System.IO.File.ReadAllBytes(Server.MapPath("~/EmailAttachment/Test.txt")) };
                //    emailMessage._attachments.Add(ea);

                //    SMSMessageList smsMessage = new SMSMessageList();

                //    smsMessage.SMSText = "Notice to department";
                //    smsMessage.ModuleActionID = 1;
                //    smsMessage.UniqueIdentificationID = 2;
                //    smsMessage.MobileNo.Add("12345678");
                //    //foreach (var item in deptmembers)
                //    //{
                //    //    smsMessage.MobileNo.Add(item.Mobile);
                //    //}
                //    //string SendSMS = "1"; string SendEmail = "1";
                //    Notification.Send(true, true, smsMessage, emailMessage);
                //}

                if (model.Type == "FromDepartment")
                {
                    return RedirectToAction("ShowDepartmentNotices", "Committee", new { area = "SuperAdmin" });
                }
                else if (model.Type == "SendNoticetoDept")
                {
                    //return RedirectToAction("SendNoticeToDepartmentList", "Committee", new { area = "SuperAdmin" });
                    return RedirectToAction("eFileListAll", "eFile", new { area = "eFile", CommitteeId = CommId });
                }
                else if (model.Type == "AttachPaper")
                {
                    return RedirectToAction("eFileListAll", "eFile", new { area = "eFile", CommitteeId = CommId });
                }
                else if (model.Type == "Report")
                {
                    return RedirectToAction("eFileListAll", "eFile", new { area = "eFile", CommitteeId = CommId, eFileID = model.eFileID });
                }

                //else
                //{
                //    return RedirectToAction("eFileListAll", "eFile", new { area = "eFile" });
                //}
            }
            catch (Exception ex)
            {
                string exs = ex.ToString();
            }
            //return View();
            return RedirectToAction("eFileListAll", "eFile", new { area = "eFile", CommitteeId = CommId });
        }

        public ActionResult CheckBookViewById(eFileViewModel model)
        {
            eFileAttachment documents = new eFileAttachment();
            model.DocumentType = (List<SBL.DomainModel.Models.Document.mDocumentType>)Helper.ExecuteService("eFile", "eFileDocumentIndex", 1);
            model.eFileAttachment = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)Helper.ExecuteService("eFile", "GetAlleFileAttachments", model.eFileID);
            return View(model);
        }

        [HttpGet]
        public ActionResult EditeFile(int Id)
        {
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel();
            SBL.DomainModel.Models.eFile.eFile eFileModel = (SBL.DomainModel.Models.eFile.eFile)Helper.ExecuteService("eFile", "EditeFile", Id);
            model.eFileID = eFileModel.eFileID;
            model.eFileNumber = eFileModel.eFileNumber;
            model.eFileSubject = eFileModel.eFileSubject;
            model.DepartmentId = eFileModel.DepartmentId;
            model.Status = eFileModel.Status;
            model.OldDescription = eFileModel.OldDescription;
            model.NewDescription = eFileModel.NewDescription;
            model.CommitteeId = eFileModel.CommitteeId;
            model.CreatedBy = eFileModel.CreatedBy;
            model.CreatedDate = eFileModel.CreatedDate;
            model.ModifiedBy = eFileModel.ModifiedBy;
            model.ModifiedDate = eFileModel.ModifiedDate;
            model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            return View("AddeFile", model);
        }

        public ActionResult eFileActive(int eFileId)
        {
            int CommitteeId = (int)Helper.ExecuteService("eFile", "eFileActive", eFileId);

            return Json("eFile Opened Successfully", JsonRequestBehavior.AllowGet);
            //return RedirectToAction("eFileListAll", new { CommitteeId = (int)Helper.ExecuteService("eFile", "eFileActive", eFileId) });
        }

        [HttpGet]
        public ActionResult eFileClose(string CommitteId)
        {
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);
            return PartialView("_eFileListView", eList.eFileLists);
        }

        public JsonResult eFileDeactive(int eFileId)
        {

            int CommitteeId = (int)Helper.ExecuteService("eFile", "eFileDeActive", eFileId);

            return Json("eFile Closed Successfully", JsonRequestBehavior.AllowGet);
            //return RedirectToAction("eFileListAll", new { CommitteeId = (int)Helper.ExecuteService("eFile", "eFileDeActive", eFileId) });
        }

        public ActionResult eFileList()
        {
            eFileLogin();
            List<SBL.DomainModel.Models.eFile.eFileList> eList = new List<DomainModel.Models.eFile.eFileList>();
            eList = (List<SBL.DomainModel.Models.eFile.eFileList>)Helper.ExecuteService("eFile", "GetAlleFile", null);
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            var x = deptList.ToList().Select(c => new SelectListItem { Text = c.deptname, Value = c.deptId }).ToList();
            ViewBag.Departments = x;
            //model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            return View(eList);
        }

        /// <summary>
        /// List all efiles for pericular committees
        /// </summary>
        /// <param name="CommitteeId"></param>
        /// <returns></returns>
        public ActionResult eFileListAll()  //int CommitteeId, int? eFileID
        {
            eFileLogin();
            //List<SBL.DomainModel.Models.eFile.eFileList> eList = new List<DomainModel.Models.eFile.eFileList>();
            //eList = (List<SBL.DomainModel.Models.eFile.eFileList>)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);
            //var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
            //var x = deptList.ToList().Select(c => new SelectListItem { Text = c.deptname, Value = c.deptId }).ToList();
            //ViewBag.Departments = x;
            //ViewBag.CommitteeId = CommitteeId;
            //return View(eList);
            CommitteeList comList = new CommitteeList();
            comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            ViewBag.CommitteeList = comList.CommitteeView.Select(a => new SelectListItem { Text = a.CommitteeName, Value = a.CommitteeId.ToString() }).ToList();

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);
            // var departmentsValues = eList.Departments.ToList().Select(c => new SelectListItem { Text = c.deptname, Value = c.deptId }).ToList();
            // ViewBag.Departments = departmentsValues;
            //ViewBag.CommitteeId = CommitteeId;
            //ViewBag.eFileID = eFileID;
            return View(eList);
        }

        public ActionResult eFileView(string Id)
        {
            SBL.DomainModel.Models.eFile.eFileList efile = (SBL.DomainModel.Models.eFile.eFileList)Helper.ExecuteService("eFile", "GeteFile", Id);
            efile.eAttachments = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)Helper.ExecuteService("eFile", "GetAlleFileAttachments", Id);
            ViewBag.eFileId = Id;
            return View(efile);
        }

        /// <summary>
        /// Get Committee Report
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetCommitteeReport()
        {
            List<SBL.DomainModel.Models.eFile.eFileAttachment> eFilesAttachments = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)Helper.ExecuteService("eFile", "GeteFileReport", null);
            return View("CommitteeReport", eFilesAttachments);
        }

        /// <summary>
        /// Get departments
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GeteFileBaseDept(string departmentId)
        {
            departmentId = "HPD1098";
            List<SBL.DomainModel.Models.eFile.eFile> eFiles = (List<SBL.DomainModel.Models.eFile.eFile>)Helper.ExecuteService("eFile", "GetCommitteeeFile", departmentId);
            return Json(eFiles, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        //[HttpPost]
        public ActionResult GeteFileSearch(string Department, string eFileNo, string Subject, int CommitteeId, string Status)
        {
            try
            {
                SBL.DomainModel.Models.eFile.eFileSearch Search = new eFileSearch();
                Search.Department = Department;
                Search.eFileNo = eFileNo;
                Search.Subject = Subject;
                Search.CommitteeId = CommitteeId;
                Search.Status = Convert.ToBoolean(Convert.ToInt16(Status.ToString()));
                SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
                eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GeteFileSearch", Search);
                ViewBag.Departments = eList.Departments.ToList().Select(c => new SelectListItem { Text = c.deptname, Value = c.deptId }).ToList();
                return PartialView("_eFileListView", eList.eFileLists);
            }
            catch (Exception ex)
            {
                string excep = ex.ToString();
            }
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }
        //[HttpPost]
        //public ActionResult AddeFileAttachments(SBL.DomainModel.Models.eFile.eFileAttachment model, HttpPostedFileBase file, int CommId = 0)
        //{
        //    if (file != null)
        //    {
        //        model.eFileName = Path.GetFileNameWithoutExtension(file.FileName);
        //        if (!Directory.Exists(Server.MapPath("~/eFileAttachments/" + model.eFileID)))
        //        {
        //            Directory.CreateDirectory(Server.MapPath("~/eFileAttachments/" + model.eFileID));

        //        };
        //        string destinationPath = Path.Combine(Server.MapPath("~/eFileAttachments/" + model.eFileID), Path.GetFileName(file.FileName));
        //        file.SaveAs(destinationPath);

        //        //PDF to Image.
        //        List<int> PDFPageNumbers = new List<int>();
        //        if (!Directory.Exists(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles")))
        //        {
        //            Directory.CreateDirectory(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles"));
        //            PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);

        //        }
        //        else
        //        {
        //            PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);
        //        }

        //        model.SplitFileCount = PDFPageNumbers.Count;
        //        model.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
        //        model.CreatedDate = DateTime.Now;
        //        model.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
        //        model.ModifiedDate = DateTime.Now;
        //        model.eFileNameId = Guid.NewGuid();
        //        model.ParentId = model.ParentId == 0 ? 0 : model.ParentId;
        //        var value = Helper.ExecuteService("eFile", "AddeFileAttachments", model);
        //    }

        //    if (model.Type != "FromeFile")
        //    {
        //        // var result = Helper.ExecuteService("eFile", "GetDepartmentMembers", null);
        //        List<SBL.DomainModel.Models.Member.DepartmentMembers> deptmembers = new List<DomainModel.Models.Member.DepartmentMembers>();
        //        deptmembers = (List<SBL.DomainModel.Models.Member.DepartmentMembers>)Helper.ExecuteService("eFile", "GetDepartmentMembers", null);
        //        CustomMailMessage emailMessage = new CustomMailMessage();

        //        emailMessage._from = "evidhanmail@gmail.com";
        //        emailMessage._isBodyHtml = false;
        //        emailMessage._subject = "Notice From Committee";
        //        emailMessage._body = "Notice From Committee.Please find the attachment.";
        //        emailMessage._toList.Add("test@gmail.com");
        //        //foreach (var item in deptmembers)
        //        //{
        //        //    emailMessage._toList.Add(item.Email);
        //        //}
        //        emailMessage.ProccessDate = DateTime.Now;
        //        EAttachment ea = new EAttachment { FileName = new FileInfo(Server.MapPath("~/EmailAttachment/Test.txt")).Name, FileContent = System.IO.File.ReadAllBytes(Server.MapPath("~/EmailAttachment/Test.txt")) };
        //        emailMessage._attachments.Add(ea);

        //        SMSMessageList smsMessage = new SMSMessageList();

        //        smsMessage.SMSText = "Notice to department";
        //        smsMessage.ModuleActionID = 1;
        //        smsMessage.UniqueIdentificationID = 2;
        //        smsMessage.MobileNo.Add("12345678");
        //        //foreach (var item in deptmembers)
        //        //{
        //        //    smsMessage.MobileNo.Add(item.Mobile);
        //        //}
        //        //string SendSMS = "1"; string SendEmail = "1";
        //        Notification.Send(true, true, smsMessage, emailMessage);
        //    }

        //    if (model.Type == "FromDepartment")
        //    {
        //        return RedirectToAction("ShowDepartmentNotices", "Committee", new { area = "SuperAdmin" });
        //    }
        //    else if (model.Type == "CommitteeToDept")
        //    {
        //        return RedirectToAction("SendNoticeToDepartmentList", "Committee", new { area = "SuperAdmin" });
        //    }
        //    else if (model.Type == "FromeFile")
        //    {
        //        return RedirectToAction("eFileListAll", "eFile", new { area = "eFile", CommitteeId = CommId });
        //    }

        //    else
        //    {
        //        return RedirectToAction("eFileListAll", "eFile", new { area = "eFile" });
        //    }
        //}
        public List<int> PDFToImage(string file, string outputPath, int dpi)
        {
            try
            {
                //string sOutputDirectory = Server.MapPath("~/eFileAttachments/" + eFileId + "/SplitedFiles");
                List<int> PDFPagesCount = new List<int>();
                Ghostscript.NET.Rasterizer.GhostscriptRasterizer rasterizer = null;
#pragma warning disable CS0219 // The variable 'img' is assigned but its value is never used
                System.Drawing.Image img = null;
#pragma warning restore CS0219 // The variable 'img' is assigned but its value is never used
                Ghostscript.NET.GhostscriptVersionInfo vesion = new Ghostscript.NET.GhostscriptVersionInfo(new System.Version(0, 0, 0), Server.MapPath("~/bin/gsdll32.dll"), string.Empty, Ghostscript.NET.GhostscriptLicense.GPL);

                using (rasterizer = new Ghostscript.NET.Rasterizer.GhostscriptRasterizer())
                {
                    rasterizer.Open(file, vesion, false);

                    for (int i = 1; i <= rasterizer.PageCount; i++)
                    {
                        PDFPagesCount.Add(i);
                        string pageFilePath = Path.Combine(outputPath, Path.GetFileNameWithoutExtension(file) + "-p" + i.ToString() + ".png");

                        //using (img = rasterizer.GetPage(dpi, dpi, i))
                        //{
                        using (Bitmap bmp = new Bitmap(rasterizer.GetPage(dpi, dpi, i)))
                        {
                            bmp.Save(pageFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                            //bmp.Save(pageFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                            //img.Save(pageFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);//
                        }
                    }

                    rasterizer.Close();
                }
                return PDFPagesCount;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
            }
            return null;
        }

        //public static byte[] ConvertImageToByteArray(Image imageToConvert)
        //{
        //    using (var ms = new MemoryStream())
        //    {
        //        Bitmap bmp = new Bitmap(imageToConvert);
        //        bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        return ms.ToArray();
        //    }
        //}

        //public List<int> PDFToImage(string file, string outputPath, int dpi)
        //{
        //    try
        //    {
        //        //string sOutputDirectory = Server.MapPath("~/eFileAttachments/" + eFileId + "/SplitedFiles");
        //        List<int> PDFPagesCount = new List<int>();
        //        Ghostscript.NET.Rasterizer.GhostscriptRasterizer rasterizer = null;
        //        System.Drawing.Image img = null;
        //        Ghostscript.NET.GhostscriptVersionInfo vesion = new Ghostscript.NET.GhostscriptVersionInfo(new Version(0, 0, 0), Server.MapPath("~/bin/gsdll32.dll"), string.Empty, Ghostscript.NET.GhostscriptLicense.GPL);

        //        using (rasterizer = new Ghostscript.NET.Rasterizer.GhostscriptRasterizer())
        //        {
        //            rasterizer.Open(file, vesion, false);

        //            for (int i = 1; i <= rasterizer.PageCount; i++)
        //            {
        //                PDFPagesCount.Add(i);
        //                string pageFilePath = Path.Combine(outputPath, Path.GetFileNameWithoutExtension(file) + "-p" + i.ToString() + ".png");

        //                //using (img = rasterizer.GetPage(dpi, dpi, i))
        //                //{
        //                using (Bitmap bmp = new Bitmap(rasterizer.GetPage(dpi, dpi, i)))
        //                {
        //                    bmp.Save(pageFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
        //                    //img.Save(pageFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);

        //                }
        //            }

        //            rasterizer.Close();
        //        }
        //        return PDFPagesCount;
        //    }

        //    catch (Exception ex)
        //    {
        //    }
        //    return null;
        //}

        //public void PDFToImage(string file, string outputPath, int dpi)
        //{
        //    string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        //    Ghostscript.NET.Rasterizer.GhostscriptRasterizer rasterizer = null;
        //    Ghostscript.NET.GhostscriptVersionInfo vesion = new Ghostscript.NET.GhostscriptVersionInfo(new Version(0, 0, 0), path + @"\gsdll64.dll", string.Empty, Ghostscript.NET.GhostscriptLicense.GPL);

        //    using (rasterizer = new Ghostscript.NET.Rasterizer.GhostscriptRasterizer())
        //    {
        //        rasterizer.Open(file, vesion, false);

        //        for (int i = 1; i <= rasterizer.PageCount; i++)
        //        {
        //            string pageFilePath = Path.Combine(outputPath, Path.GetFileNameWithoutExtension(file) + "-p" + i.ToString() + ".png");

        //            Image img = rasterizer.GetPage(dpi, dpi, i);
        //            img.Save(pageFilePath, ImageFormat.Jpeg);
        //        }

        //        rasterizer.Close();
        //    }
        //    MessageBox.Show("Done");
        //}

        /// <summary>
        /// Split PDF Files in to single files
        /// </summary>
        /// <param name="sourcePdfPath"></param>
        //public List<int> ExtractPages(string sourcePdfPath,int eFileId)// string outputPdfPath)//, int[] extractThesePages)
        //{
        //    iTextSharp.text.pdf.PdfReader reader = null;
        //    iTextSharp.text.Document sourceDocument = null;
        //    iTextSharp.text.pdf.PdfCopy pdfCopyProvider = null;
        //    iTextSharp.text.pdf.PdfImportedPage importedPage = null;
        //    List<int> PDFPagesCount;

        //    try
        //    {
        //        string sOutputDirectory = Server.MapPath("~/eFileAttachments/"+eFileId+"/SplitedFiles");
        //        if (!Directory.Exists(sOutputDirectory))
        //            Directory.CreateDirectory(sOutputDirectory);

        //        {
        //            //sourceDocument = new iTextSharp.text.Document(new iTextSharp.text.Rectangle(50f, 50f));
        //            reader = new iTextSharp.text.pdf.PdfReader(Server.MapPath("~/eFileAttachments/"+eFileId+"/"+sourcePdfPath));
        //            //pgbarLoadBook.Maximum = reader.NumberOfPages;
        //            PDFPagesCount = new List<int>(reader.NumberOfPages);

        //            //For Image convertion.

        //            //string file = System.IO.Path.Combine(Server.MapPath("~/eFileAttachments/"+eFileId+"/"+"GetAssemblyFilePdf.pdf"));
        //            //doc.LoadFromFile(file);

        //            for (int i = 1; i <= reader.NumberOfPages; i++)
        //            {
        //                PDFPagesCount.Add(i);

        //                //sourceDocument = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER, 10, 10, 10, 10);
        //                sourceDocument = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(1));
        //                //sourceDocument.SetPageSize();
        //                //sourceDocument = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER, 10, 10, 10, 10);
        //                // document and an output file stream:
        //                pdfCopyProvider = new iTextSharp.text.pdf.PdfCopy(sourceDocument,
        //                    new System.IO.FileStream(System.IO.Path.Combine(sOutputDirectory, i.ToString() + ".pdf"), System.IO.FileMode.Create));
        //                sourceDocument.Open();
        //                importedPage = pdfCopyProvider.GetImportedPage(reader, i);
        //                pdfCopyProvider.AddPage(importedPage);
        //                sourceDocument.Close();

        //            }

        //            reader.Close();

        //            //save to images
        //            //for (int i = 0; i < doc.Pages.Count; i++)
        //            //{
        //            //    String fileName = Server.MapPath("~/SplitFiles/" + i + ".png");
        //            //    using (Image image = doc.SaveAsImage(i))
        //            //    {
        //            //        image.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);
        //            //        System.Diagnostics.Process.Start(fileName);
        //            //    }
        //            //}

        //            //doc.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return PDFPagesCount;
        //}
        public ActionResult SendAttachmenttoEfile(eFileViewModel model)
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            //documents.eFileID = model.eFileID;
            documents.Type = model.Type;
            //documents.DepartmentId = model.DepartmentId;
            //documents.OfficeCode = model.Officecode;
            //ViewBag.CommitteeId = model.CommitteeId;
            return View(documents);
        }



        #region Committee Code By Shashi
        public ActionResult GetfFileListDetails(int CommitteeId)
        {
            eFileLogin();
            tCommittee mdl = new tCommittee();
            mdl = (tCommittee)Helper.ExecuteService("Committee", "GetCommitteeDetailsByCommitteeId", CommitteeId);

            ViewBag.CommName = mdl.CommitteeName;
            ViewBag.CommYear = mdl.CommitteeYear;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);
            ViewBag.Mode = "Committee";
            ViewBag.CommID = CommitteeId;
            return PartialView("_eFileListView", eList);
        }
        public ActionResult GetfFileListDetailsLayPaper(int CommitteeId)
        {
            eFileLogin();
            tCommittee mdl = new tCommittee();
            mdl = (tCommittee)Helper.ExecuteService("Committee", "GetCommitteeDetailsByCommitteeId", CommitteeId);

            ViewBag.CommName = mdl.CommitteeName;
            ViewBag.CommYear = mdl.CommitteeYear;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);
            ViewBag.Mode = "Committee";
            ViewBag.CommID = CommitteeId;
            return PartialView("_eFileListViewLayPaper", eList);
        }



        public JsonResult UpdateEFileNewDesc(eFileViewModel mdl)
        {
            eFileLogin();
            SBL.DomainModel.Models.eFile.eFile model = new DomainModel.Models.eFile.eFile();
            model.eFileID = mdl.eFileID;
            model.NewDescription = mdl.NewDescription;
            int res = (int)Helper.ExecuteService("eFile", "UpdateeFileNewDescriptionByFileId", model);

            if (res == 1)
            {
                return Json("New Description Updated Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UpdateeFileDetailsByFileId(SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel mdl, int lstOffice)
        {
            eFileLogin();
            SBL.DomainModel.Models.eFile.eFile model = new DomainModel.Models.eFile.eFile();
            model.eFileID = mdl.eFileID;
            model.NewDescription = mdl.NewDescription;
            model.FromYear = mdl.FromYear;
            model.ToYear = mdl.ToYear;
            model.DepartmentId = mdl.DepartmentId;
            model.Officecode = lstOffice;
            model.eFileNumber = mdl.eFileNumber;
            model.eFileSubject = mdl.eFileSubject;
            model.OldDescription = mdl.OldDescription;

            int res = (int)Helper.ExecuteService("eFile", "UpdateeFileDetailsByFileId", model);

            if (res == 1)
            {
                return Json("New Description Updated Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// For Updateing eFIle by Id- Created By Shubham
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public JsonResult UpdateeFileDetailsByFileIdNew(FormCollection coll)
        {
            eFileLogin();
            SBL.DomainModel.Models.eFile.eFile model = new DomainModel.Models.eFile.eFile();
            model.eFileID = int.Parse(coll["eFileID"]);
            model.NewDescription = coll["NewDescription"];
            model.FromYear = coll["FromYear"];// mdl.FromYear;
            model.ToYear = coll["ToYear"];// mdl.ToYear;
            model.DepartmentId = coll["hdnDepartmentId"];// mdl.DepartmentId;
            model.Officecode = int.Parse(coll["hdnOfficecode"]);// lstOffice;
            model.eFileNumber = coll["eFileNumber"];// mdl.eFileNumber;
            model.eFileSubject = coll["eFileSubject"];// mdl.eFileSubject;
            model.OldDescription = coll["OldDescription"];// mdl.OldDescription;

            int res = (int)Helper.ExecuteService("eFile", "UpdateeFileDetailsByFileId", model);

            if (res == 1)
            {
                return Json("New Description Updated Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            }
        }

        public DateTime YearEarlier(DateTime mydate)
        {
            return mydate.AddYears(-1);
        }

        public PartialViewResult GetReceivePaperList(int pageId = 1, int pageSize = 25, int YearId = 5, int PapersId = 1)
        {
            CurrentSession.SetSessionAlive = null;
            eFileLogin();
            eFileAttachments model = new eFileAttachments();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string Year = YearId.ToString();
            string papers = PapersId.ToString();
            model.CurrentSessionAId = AadharId;
            string[] strngBIDN = new string[2];
            strngBIDN[0] = CurrentSession.BranchId;
            var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = value123[0];
            //str[3] = currtBId;
            str[4] = Year;
            str[5] = papers;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
            //model.eFileAttachemts = ListModel;

            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            //List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            //if (pageId == 1)
            //{
            //    pagedRecord = ListModel.Take(pageSize).ToList();
            //}
            //else
            //{
            //    int r = (pageId - 1) * pageSize;
            //    pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            //}

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            string[] str1 = new string[4];
            string[] strngBID = new string[2];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);

            str1[3] = value[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileAttachemts = ListModel;//pagedRecord commented the pagination
            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            model.eFileLists = eList.eFileLists;

            //for count update draft list 
            string design = CurrentSession.Designation;
            string paperType = PapersId.ToString();
            string[] strDraft = new string[6];
            strDraft[0] = DepartmentId;
            strDraft[1] = Officecode;
            strDraft[2] = AadharId;
            strDraft[3] = currtBId;
            strDraft[4] = design;
            strDraft[5] = paperType;
            List<eFileAttachment> ListModelForDraft = new List<eFileAttachment>();
            ListModelForDraft = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", str);
            model.DraftListCount = ListModelForDraft.Count;


            return PartialView("_receivePaperList", model);
        }

        public ActionResult ToverifyPaper(int pageId = 1, int pageSize = 25, int YearId = 5, int PapersId = 1, int strid = 0)
        {
            //verified logic on Efileattachment on other context cause it was locked.
            string[] strFOrPending = new string[4];
            strFOrPending[0] = CurrentSession.UserName;
            strFOrPending[1] = Convert.ToString(strid);
            strFOrPending[2] = CurrentSession.BranchId;
            //  string[] strngBID = new string[2];
            // strngBID[0] = CurrentSession.BranchId;
            // var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            // strFOrPending[3] = value[0];//passing commId
            strFOrPending[3] = CurrentSession.AadharId;
            Helper.ExecuteService("CommitteReplyPendency", "ToVerifyAttachedPaper", strFOrPending);

            eFileAttachments model = new eFileAttachments();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string Year = YearId.ToString();
            string papers = PapersId.ToString();
            model.CurrentSessionAId = AadharId;
            string[] strngBIDN = new string[2];
            strngBIDN[0] = CurrentSession.BranchId;
            var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = value123[0];
            //str[3] = currtBId;
            str[4] = Year;
            str[5] = papers;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
            //model.eFileAttachemts = ListModel;

            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            string[] str1 = new string[4];
            string[] strngBID = new string[2];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);

            str1[3] = value[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileAttachemts = pagedRecord;
            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            model.eFileLists = eList.eFileLists;


            return PartialView("_receivePaperList", model);
        }
        public ActionResult SaveStatus(int Id, string Remarks)
        {
#pragma warning disable CS0219 // The variable 'strid' is assigned but its value is never used
            int pageId = 1; int pageSize = 25; int YearId = 5; int PapersId = 1; int strid = 0;
#pragma warning restore CS0219 // The variable 'strid' is assigned but its value is never used
            eFileAttachment model1 = new eFileAttachment();
            model1.eFileAttachmentId = Id;
            model1.RejectedRemarks = Remarks;
            model1.VerifiedBy = CurrentSession.UserName;//using verifed by for rejected here 
            model1 = (eFileAttachment)Helper.ExecuteService("CommitteReplyPendency", "ToRejectAttachedPaper", model1);

            eFileAttachments model = new eFileAttachments();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string Year = YearId.ToString();
            string papers = PapersId.ToString();
            model.CurrentSessionAId = AadharId;
            string[] strngBIDN = new string[2];
            strngBIDN[0] = CurrentSession.BranchId;
            var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = value123[0];
            //str[3] = currtBId;
            str[4] = Year;
            str[5] = papers;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
            //model.eFileAttachemts = ListModel;

            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            string[] str1 = new string[4];
            string[] strngBID = new string[2];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);

            str1[3] = value[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileAttachemts = pagedRecord;
            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            model.eFileLists = eList.eFileLists;


            return PartialView("_receivePaperList", model);
        }
        public JsonResult Read_RowData(int eFileattachmentId, int CommitteeId) //pr
        {
            ReadTableData mdl = new ReadTableData();
            mdl.TablePrimaryId = eFileattachmentId;
            mdl.UserAadharId = CurrentSession.AadharId;
            mdl.UserId = CurrentSession.UserID;
            mdl.CommitteeId = Convert.ToString(CommitteeId);
            int res = (int)Helper.ExecuteService("eFile", "Save_ReadDataInTable", mdl);

            //if (res == 1)
            //{
            //    return Json("Updated Successfully", JsonRequestBehavior.AllowGet);
            //}
            return Json("success", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult NewReceivePaper()
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();
            documents.PaperRefNo = GetRefNo();
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);
            //Get Items
            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetFileItemList", null);
            ViewBag.FileItem = new SelectList(_ListItems, "ID", "ItemTypeName", null);
            ///Get eFilePaperNature
            var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList_Receive", null);
            ViewBag.eFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);

            documents.DepartmentId = CurrentSession.DeptID;
            if (documents.DepartmentId == "HPD0001")
            {
                string Did = null;
                documents.DepartmentId = Did;
            }
            else
            {
                string Did = "HPD0001";
                documents.DepartmentId = Did;
            }
            return PartialView("_ReceivePaper", documents);
        }

        public PartialViewResult NewSendPaperCreate()
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();
            documents.PaperRefNo = GetRefNo();
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);
            //Get Items
            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetFileItemList", null);
            ViewBag.FileItem = new SelectList(_ListItems, "ID", "ItemTypeName", null);
            ///Get eFilePaperNature
            var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList_Receive", null);
            ViewBag.eFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);
            documents.DepartmentId = CurrentSession.DeptID;
            if (documents.DepartmentId == "HPD0001")
            {
                string Did = null;
                documents.DepartmentId = Did;
            }
            else
            {
                string Did = "HPD0001";
                documents.DepartmentId = Did;
            }
            return PartialView("_sendPaperForCreate", documents);
        }
        public PartialViewResult NewReceivePaperFromeFileList(int eFileid, string eFileNumber)
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();
            documents.PaperRefNo = GetRefNo();
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            ///Get eFilePaperNature
            var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList_Receive", null);
            ViewBag.eFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);

            documents.eFileID = eFileid;
            ViewBag.eFileNumber = eFileNumber;

            return PartialView("_ReceivePaper", documents);
        }

        public PartialViewResult GetSendPaperList(int pageId = 1, int pageSize = 25, int YearId = 5, int PapersId = 1)
        {
            CurrentSession.SetSessionAlive = null;
            eFileLogin();
            eFileAttachments model = new eFileAttachments();

            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string Year = YearId.ToString();
            string PaperType = PapersId.ToString();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            model.CurrentSessionAId = AadharId;
            var CommitteeTypePermission = (CommitteeTypePermission)Helper.ExecuteService("eFile", "GetCommitteUser", CurrentSession.UserID);
            if (CommitteeTypePermission != null && CommitteeTypePermission.CommitteeTypeId > 0)
                ViewBag.CommitteeTypePermission = "Yes";
            else
                ViewBag.CommitteeTypePermission = "No";

            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = currtBId;
            str[4] = Year;
            str[5] = PaperType;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            // ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendPaperDetailsByDeptId", str);
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendList", str);
            //model.eFileAttachemts = ListModel;
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            //List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            //if (pageId == 1)
            //{
            //    pagedRecord = ListModel.Take(pageSize).ToList();
            //}
            //else
            //{
            //    int r = (pageId - 1) * pageSize;
            //    pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            //}

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            string[] str1 = new string[4];
            string[] strngBID = new string[2];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str1[3] = value[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileAttachemts = ListModel;//pagedRecord
            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            model.eFileLists = eList.eFileLists;

            #region For Draft List Count
            string[] strD = new string[6];
            strD[0] = DepartmentId;
            strD[1] = Officecode;
            strD[2] = AadharId;
            strD[3] = currtBId;
            strD[4] = CurrentSession.Designation;
            strD[5] = "1";
            var DraftListCount = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
            model.DraftListCount = DraftListCount.Count;
            #endregion
            return PartialView("_sendPaperList", model);
        }
        public PartialViewResult NewSendPaper()
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();

            documents.PaperRefNo = GetRefNo();
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            ///Get eFilePaperNature
            var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList", null);
            ViewBag._ListeFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);


            return PartialView("_SendPaper", documents);
        }
        public PartialViewResult NewSendPaperFromeFileList(int eFileid, string eFileNumber, int CommetteId)
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();

            documents.PaperRefNo = GetRefNo();
            if (!string.IsNullOrEmpty(CurrentSession.DeptID))
            {
                var UserDeptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartmentByDeptId", CurrentSession.DeptID);
                ViewBag.UserdeptList = new SelectList(UserDeptList, "deptId", "deptname", null);

            }
            else
            {
                ViewBag.UserdeptList = null;
            }
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            ///Get eFilePaperNature
            var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList", null);
            ViewBag._ListeFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);

            documents.eFileID = eFileid;
            ViewBag.eFileNumber = eFileNumber;
            ViewBag.CommetteId = CommetteId;
            return PartialView("_SendPaper", documents);
        }
        static string GetRefNo()
        {
            Random _no = new Random();
            var _Year = DateTime.Today.Year.ToString();
            var _Month = DateTime.Today.Month.ToString();
            int n = _no.Next(00001, 99999);
            string fullRefNo = "PREF" + "/" + _Year + "/" + _Month + "/" + n.ToString();
            return fullRefNo;
        }

        static int GetFileRandomNo()
        {
            Random ran = new Random();
            int rno = ran.Next(1, 99999);
            return rno;
        }
        [HttpPost]
        public JsonResult SaveReceivePaper(eFileAttachment model, HttpPostedFileBase file, int? lstOffice)
        {
            eFileAttachment mdl = new eFileAttachment();

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            var IsMsgRequired = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "IsMsgRequiredInHouseCommittee", null);
            var IsMailRequired = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "IsMailRequiredInHouseCommittee", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);
            if (CurrentSession.IsUploadPdfFile != null && CurrentSession.IsUploadPdfFile != "" && CurrentSession.IsUploadPdfFile == "Y")
            {
                model.IsPdf = "N";
                if (model.IsPdf == null || model.IsPdf == "" || model.IsPdf == "N")
                {
                    if (Directory.Exists(directory))
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                        if (savedFileName.Length > 0)
                        {
                            string SourceFile = savedFileName[0];
                            foreach (string page in savedFileName)
                            {
                                Guid FileName = Guid.NewGuid();

                                string name = Path.GetFileName(page);

                                string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                                if (!string.IsNullOrEmpty(name))
                                {
                                    if (!string.IsNullOrEmpty(mdl.eFilePath))
                                        System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePath));
                                    System.IO.File.Copy(SourceFile, path, true);
                                    mdl.eFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                                }

                            }

                        }
                        else
                        {
                            mdl.eFilePath = null;
                            // return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                        }


                    }
                }


                else
                {
                    mdl.eFilePath = model.eFilePathPDFN;
                }
            }
            else
            {
                if (model.IsPdf == "Y")
                {
                    mdl.eFilePath = model.eFilePathPDFN;
                }
                else
                {
                    mdl.eFilePath = null;
                    //  return Json("Please select Word file", JsonRequestBehavior.AllowGet);
                }
            }

            #region Upload Word File
            string url1Word = "~/ePaper/FileWord";
            string directory1Word = Server.MapPath(url1Word);
            if (CurrentSession.IsUploadWordFile != null && CurrentSession.IsUploadWordFile != "" && CurrentSession.IsUploadWordFile == "Y")
            {
                model.IsWord = "N";
                if (model.IsWord == null || model.IsWord == "" || model.IsWord == "N")
                {
                    if (Directory.Exists(directory))
                    {
                        string[] savedFileNameWord = Directory.GetFiles(Server.MapPath(url1Word));
                        if (savedFileNameWord.Length > 0)
                        {
                            string SourceFile = savedFileNameWord[0];
                            foreach (string page in savedFileNameWord)
                            {
                                Guid FileName = Guid.NewGuid();

                                string name = Path.GetFileName(page);

                                string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                                if (!string.IsNullOrEmpty(name))
                                {
                                    if (!string.IsNullOrEmpty(mdl.eFilePathWord))
                                        System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePathWord));
                                    System.IO.File.Copy(SourceFile, path, true);
                                    mdl.eFilePathWord = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                                }

                            }

                        }
                        else
                        {
                            mdl.eFilePathWord = null;
                            return Json("Please select Word file", JsonRequestBehavior.AllowGet);
                        }
                    }

                }
                else
                {
                    mdl.eFilePathWord = model.eFilePathWordN;
                }
            }
            else
            {
                if (model.IsWord == "Y")
                {
                    mdl.eFilePathWord = model.eFilePathWordN;
                }
                else
                {
                    mdl.eFilePathWord = null;
                    return Json("Please select Word file", JsonRequestBehavior.AllowGet);
                }
            }
            #endregion
            //  mdl.PaperDraft = model.PaperDraft;
            mdl.SplitFileCount = 0;
            mdl.CreatedBy = new Guid(CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.eFileNameId = Guid.NewGuid();
            model.ParentId = fileID;

            //for sms by robin
            mdl.CommitteeName = model.CommitteeName;
            mdl.DepartmentName = model.DepartmentName;

            // i am receiver
            mdl.ToDepartmentID = CurrentSession.DeptID; //"HPD0001";
            int OfficeId = 0;
            int.TryParse(CurrentSession.OfficeId, out OfficeId);
            mdl.ToOfficeCode = OfficeId;// int.Parse(CurrentSession.OfficeId);// 0;
            ///


            // I am Sender
            mdl.DepartmentId = model.DepartmentId;//CurrentSession.DeptID; AddeFileAttachments
            string _lstOffice = Convert.ToString(lstOffice);
            if (!string.IsNullOrEmpty(_lstOffice))
            {
                mdl.OfficeCode = int.Parse(_lstOffice);
            }
            else
            {
                mdl.OfficeCode = 0;
            }
            //////

            if (model.PType == "4")
            {
                mdl.PType = "R";
            }
            else
            {
                mdl.PType = "O"; //model.PType;
            }


            mdl.DocumentTypeId = model.DocumentTypeId;
            mdl.Description = model.Description;
            mdl.PaperNature = model.PaperNature;
            mdl.PaperNatureDays = model.PaperNatureDays;
            mdl.Title = model.Title;
            mdl.PaperRefNo = model.PaperRefNo;
            mdl.RecvDetails = model.RecvDetails;
            mdl.RevedOption = model.RevedOption;
            mdl.ToRecvDetails = model.RecvDetails;
            mdl.PaperTicketValue = model.PaperTicketValue;
            mdl.PaperNo = model.PaperNo;
            mdl.DispatchtoWhom = model.DispatchtoWhom;
            mdl.eFileID = model.eFileID;
            mdl.CountsAPS = model.CountsAPS;
            mdl.ItemTypeName = model.ItemTypeName;
            mdl.DraftId = model.DraftId;
            mdl.ItemNumber = model.ItemNumber;
            mdl.ReplyRefNo = model.ReplyRefNo;
            if (model.ToDepIds != null)
            {
                mdl.ToDepIds = model.ToDepIds.TrimEnd();
            }
            if (model.CCDepIds != null)
            {
                mdl.CCDepIds = model.CCDepIds.TrimEnd(',');
            }
            if (model.ToMemIds != null)
            {
                mdl.ToMemIds = model.ToMemIds.TrimEnd(',');
            }
            if (model.CCMemIds != null)
            {
                mdl.CCMemIds = model.CCMemIds.TrimEnd(',');
            }
            if (model.ToOtherIds != null)
            {
                mdl.ToOtherIds = model.ToOtherIds.TrimEnd(',');
            }
            if (model.CCOtherIds != null)
            {
                mdl.CCOtherIds = model.CCOtherIds.TrimEnd(',');
            }
            if (model.ToListComplN != null)
            {
                mdl.ToListComplN = model.ToListComplN.TrimEnd(',');
            }

            if (model.CCListComplN != null)
            {
                mdl.CCListComplN = model.CCListComplN.TrimEnd(',');
            }
            if (model.ToListComplIds != null)
            {
                mdl.ToListComplIds = model.ToListComplIds.TrimEnd(',');
            }
            if (model.CCListComplIds != null)
            {
                mdl.CCListComplIds = model.CCListComplIds.TrimEnd(',');
            }
            mdl.CommitteeId = model.CommitteeId;
            mdl.PaperRefrenceManualy = model.PaperRefrenceManualy;

            //getting user details
            mUsers objM = new mUsers();
            Guid newguid = new Guid(CurrentSession.UserID);
            objM.UserId = newguid;
            mdl.IpAddress = (string)Helper.ExecuteService("eFile", "GetIPAddress", "");
            mdl.MacAddress = (string)Helper.ExecuteService("eFile", "GetMACAddress", "");

            var ResultForSend = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserIDSMS", objM);//for getting user phoneno from userid      


            if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
            {
                int res = (int)Helper.ExecuteService("eFile", "UpdateDraftDatabyId", mdl);

                if (res == 1)
                {
                    return Json("Updated Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                mdl.ToDepartmentID = "HPD0001";
                eFileAttachment res = (eFileAttachment)Helper.ExecuteService("eFile", "SendDraftDatabyId", mdl);

                //for sms and mail by robin
                string reffno = res.PaperRefNo;
                var message = ("Kindly Take Necessary action w.r.t e-Vidhan reference no." + reffno + " regarding " + res.Title + " by " + res.DepartmentName + " to " + res.CommitteeName + " on " + res.ReceivedDate).ToLower();

                //to send msg to mupltiple contact groups
                List<string> phoneNumbers = new List<string>();
                var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByDeptInHC", new RecipientGroupMember { CommitteeId = Convert.ToString(res.CommitteeId) }) as List<RecipientGroupMember>;

                if (contactGroupMembers != null)
                {
                    foreach (var item in contactGroupMembers)
                    {
                        if (item.MobileNo.Contains(','))
                        {
                            string[] numbers = item.MobileNo.Split(',');
                            foreach (var mb in numbers)
                            {
                                phoneNumbers.Add(mb);
                            }
                        }
                        else
                            phoneNumbers.Add(item.MobileNo);
                    }
                }
                //if (!string.IsNullOrEmpty(model.PhoneNumbers))
                //    phoneNumbers = model.PhoneNumbers.Split(',').Select(s => s).ToList();


                //code for email
                if (IsMailRequired.SettingValue == "True")
                {
                    if (ResultForSend.EmailId != "" || ResultForSend.EmailId != null)
                    {
                        Email.API.CustomMailMessage emailmsg = new Email.API.CustomMailMessage();
                        emailmsg._body = message;
                        emailmsg._isBodyHtml = true;
                        emailmsg._from = ResultForSend.EmailId;
                        emailmsg._subject = "Pendency Reply";
                        emailmsg._toList.Add(ResultForSend.EmailId);//sending mail to same person
                        Notification.Send(false, true, null, emailmsg);
                    }
                }

                //for sending sms..
                if (IsMsgRequired.SettingValue == "True")
                {
                    //if (ResultForSend.MobileNo != "" || ResultForSend.MobileNo != null) // self check number not required
                    //{
                    SMSMessageList smsMessage = new SMSMessageList();
                    foreach (var item in phoneNumbers)
                    {
                        smsMessage.MobileNo.Add(item);
                    }
                    Service1 smsServiceClient = new Service1();
                    smsMessage.ModuleActionID = -1;
                    smsMessage.UniqueIdentificationID = -1;
                    smsMessage.SMSText = message;
                    //smsMessage.MobileNo.Add(ResultForSend.MobileNo);                    
                    Notification.Send(true, false, smsMessage, null);
                    // }
                }

                if (res != null)//logic changed by robin due to sms requirement
                {
                    return Json("Send Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
                }

            }
            //Helper.ExecuteService("eFile", "AddeFileAttachments", mdl);


            //DataSet dataSet = new DataSet();
            //var methodParameter = new List<KeyValuePair<string, string>>();
            //methodParameter.Add(new KeyValuePair<string, string>("@DocumentTypeId", mdl.DocumentTypeId.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@eFileName", mdl.eFileName));
            //methodParameter.Add(new KeyValuePair<string, string>("@Description", mdl.Description));
            //methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", mdl.DepartmentId));
            //methodParameter.Add(new KeyValuePair<string, string>("@Title", mdl.Title));
            //methodParameter.Add(new KeyValuePair<string, string>("@eFileNameId", mdl.eFileNameId.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@DocType", "ReceivePaperByDept"));
            //methodParameter.Add(new KeyValuePair<string, string>("@SplitFileCount", mdl.SplitFileCount.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@OfficeCode", mdl.OfficeCode.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@PaperNature", mdl.PaperNature.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@PaperNatureDays", mdl.PaperNatureDays));
            //methodParameter.Add(new KeyValuePair<string, string>("@ToDepartmentID", mdl.ToDepartmentID));
            //methodParameter.Add(new KeyValuePair<string, string>("@ToOfficeCode", mdl.ToOfficeCode.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@PaperStatus", "N"));
            //methodParameter.Add(new KeyValuePair<string, string>("@PType", mdl.PType));
            //methodParameter.Add(new KeyValuePair<string, string>("@eFilePath", mdl.eFilePath));
            //methodParameter.Add(new KeyValuePair<string, string>("@PaperRefNo", mdl.PaperRefNo));
            //methodParameter.Add(new KeyValuePair<string, string>("@RecvDetails", mdl.RecvDetails));
            //methodParameter.Add(new KeyValuePair<string, string>("@ToRecvDetails", mdl.ToRecvDetails));
            //methodParameter.Add(new KeyValuePair<string, string>("@RevedOption", mdl.RevedOption));
            //methodParameter.Add(new KeyValuePair<string, string>("@isSend", "0"));
            //methodParameter.Add(new KeyValuePair<string, string>("@TicketValue", mdl.PaperTicketValue));
            //methodParameter.Add(new KeyValuePair<string, string>("@PaperNo", mdl.PaperNo));
            //methodParameter.Add(new KeyValuePair<string, string>("@WhomtoDispatch", mdl.DispatchtoWhom));
            //methodParameter.Add(new KeyValuePair<string, string>("@eFileId", "0"));
            //methodParameter.Add(new KeyValuePair<string, string>("@eFilePathWord", mdl.eFilePathWord));
            //methodParameter.Add(new KeyValuePair<string, string>("@ReFileId", mdl.eFileID.ToString()));

            //dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperEntry", methodParameter);
            //string res = "", serno = "";
            //if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            //{
            //    res = dataSet.Tables[0].Rows[0][0].ToString();
            //    serno = dataSet.Tables[0].Rows[0][1].ToString();
            //}
            //if (res == "Success")
            //{

            //    return Json("Paper Received Successfully. Paper Ref No. : <span class='label label-xlg label-primary arrowed arrowed-right' style='  font-size: 16px;  height: 29px;'><b>" + serno + "</b></span>", JsonRequestBehavior.AllowGet);

            //}
            //else
            //{
            //    return Json("Some Problem Occured. Please Try Again", JsonRequestBehavior.AllowGet);

            //}
            //return Json("Paper Received Successfully. Paper Ref No. : <span class='label label-xlg label-primary arrowed arrowed-right' style='  font-size: 16px;  height: 29px;'><b>"  +"</b></span>", JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult CreateeFile()
        {
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel();
            model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            model.DocumentType = (List<SBL.DomainModel.Models.Document.mDocumentType>)Helper.ExecuteService("eFile", "eFileDocumentIndex", 1);

            //EFile Type List  added on 27 July 2015
            model.eFileTypeList = (List<SBL.DomainModel.Models.eFile.eFileType>)Helper.ExecuteService("eFile", "eFileTypeDetails", null);
            ///********//
            ///
            model.CommitteeId = 0;
            model.Mode = "Add";
            if (!string.IsNullOrEmpty(CurrentSession.DeptID))
            {
                model.DepartmentId = CurrentSession.DeptID;
            }

            int OfficeId = 0;
            int.TryParse(CurrentSession.OfficeId, out OfficeId);
            model.Officecode = OfficeId;// Convert.ToInt32(CurrentSession.OfficeId);
            ViewBag.OffICode = CurrentSession.OfficeId;
            return PartialView("_CreateeFile", model);
        }

        public PartialViewResult eFileListDetails()
        {
            CurrentSession.SetSessionAlive = null;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[3];
            //str[2] = CurrentSession.BranchId;
            //var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            if (str[2] != "")
            {
                eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListR", str);
            }
            else
            {
                eList.AllBrancheList = (List<mBranches>)Helper.ExecuteService("eFile", "GetAllBranchList", null);//for multiple branches dropdown
                ViewBag.ForSecretary = "ForSecr";
            }
            return PartialView("_eFileListView", eList);

        }

        public PartialViewResult eFileListDetailsForSecretary(string BranchId)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = BranchId;
            ViewBag.ForSecretary = "ForSecr";
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListR", str);
            eList.AllBrancheList = (List<mBranches>)Helper.ExecuteService("eFile", "GetAllBranchList", null);//for multiple branches dropdown
            return PartialView("_eFileListView", eList);

        }

        public PartialViewResult eFileListDetailsALLStatus(int CommitteeId, int pageId = 1, int pageSize = 25)
        {
            SBL.DomainModel.Models.eFile.eFile mdl = new DomainModel.Models.eFile.eFile();
            mdl.DepartmentId = CurrentSession.DeptID;
            int OfficeId = 0;
            int.TryParse(CurrentSession.OfficeId, out OfficeId);

            mdl.Officecode = OfficeId;// Convert.ToInt32(CurrentSession.OfficeId);
            mdl.CommitteeId = CommitteeId;
            ViewBag.Mode = "Committee";
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListALLStatus", mdl);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            return PartialView("_eFileListViewAll", eList);

        }
        public PartialViewResult eFileListDetailsActive(int pageId = 1, int pageSize = 25)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();



            var CommitteeTypePermission = (CommitteeTypePermission)Helper.ExecuteService("eFile", "GetCommitteUser", CurrentSession.UserID);
            if (CommitteeTypePermission != null && CommitteeTypePermission.CommitteeTypeId > 0)
                ViewBag.CommitteeTypePermission = "Yes";
            else
                ViewBag.CommitteeTypePermission = "No";

            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            return PartialView("_eFileListViewAll", eList);

        }
        public PartialViewResult eFileListDetailsDeactive(int pageId = 1, int pageSize = 25)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            var CommitteeTypePermission = (CommitteeTypePermission)Helper.ExecuteService("eFile", "GetCommitteUser", CurrentSession.UserID);
            if (CommitteeTypePermission != null && CommitteeTypePermission.CommitteeTypeId > 0)
                ViewBag.CommitteeTypePermission = "Yes";
            else
                ViewBag.CommitteeTypePermission = "No";
            string[] str = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListDeactive", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            return PartialView("_eFileListViewAll", eList);

        }

        public PartialViewResult eFileListDetailsActiveCommAttach(int pageId = 1, int pageSize = 25)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            ViewBag.Mode = "Committee";
            ViewBag.CommAttach = "CommitteeAttach";

            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            //return PartialView("_eFileListViewAll", eList);
            return PartialView("_eFileListViewAllCommitteeAttach", eList);

        }
        public PartialViewResult eFileListDetailsDeactiveCommAttach(int pageId = 1, int pageSize = 25)
        {
            ViewBag.Mode = "Committee";
            ViewBag.CommAttach = "CommitteeAttach";
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListDeactive", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            //return PartialView("_eFileListViewAll", eList);
            return PartialView("_eFileListViewAllCommitteeAttach", eList);

        }

        public PartialViewResult eFileListDetailsActiveLayPaper(int CommitteeId, int pageId = 1, int pageSize = 25)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();


            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CommitteeId.ToString();

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListByCommitteeId", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            ViewBag.Mode = "Committee";
            return PartialView("_eFileListViewAllLayPaper", eList);

        }
        public PartialViewResult eFileListDetailsDeactiveLayPaper(int CommitteeId, int pageId = 1, int pageSize = 25)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CommitteeId.ToString();

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListDeactiveByCommitteeId", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            ViewBag.Mode = "Committee";
            return PartialView("_eFileListViewAllLayPaper", eList);

        }

        [HttpPost]
        public JsonResult SaveSendPaper(eFileAttachment model, int? lstOffice, int? CommetteId, string UserDepartmentId)
        {



            eFileAttachment mdl = new eFileAttachment();
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.eFilePath))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePath));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.eFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");

                        }

                    }

                }
                else
                {
                    return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }


            #region Upload Word File
            // string url1Word = "/ePaper/FileWord";
            string directory1Word = Server.MapPath(url);
            if (!System.IO.Directory.Exists(url))
            {
                System.IO.Directory.CreateDirectory(url);
            }
            if (Directory.Exists(directory))
            {

                string[] savedFileNameWord = Directory.GetFiles(Server.MapPath(url));
                if (savedFileNameWord.Length > 0)
                {
                    string SourceFiles = savedFileNameWord[0];
                    foreach (string page1 in savedFileNameWord)
                    {
                        Guid FileName1 = Guid.NewGuid();

                        string nametwo = Path.GetFileName(page1);

                        string path1 = System.IO.Path.Combine(directory, FileName1 + "_" + nametwo.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(nametwo))
                        {

                            if (!string.IsNullOrEmpty(mdl.eFilePathWord))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePathWord));

                            System.IO.File.Copy(SourceFiles, path1, true);
                            mdl.eFilePathWord = "/ePaper/" + FileName1 + "_" + nametwo.Replace(" ", "");
                        }

                    }

                }
                else
                {
                    mdl.eFilePathWord = null;
                    //return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

                //string[] savedFileNameWord = Directory.GetFiles(Server.MapPath(url1Word));
                //if (savedFileNameWord.Length > 0)
                //{
                //    string SourceFiles = savedFileNameWord[0];
                //    foreach (string page1 in savedFileNameWord)
                //    {
                //        Guid FileName1 = Guid.NewGuid();

                //        string nametwo = Path.GetFileName(page1);

                //        string path1 = System.IO.Path.Combine(directory, FileName1 + "_" + nametwo.Replace(" ", ""));


                //        if (!string.IsNullOrEmpty(nametwo))
                //        {

                //            if (!string.IsNullOrEmpty(mdl.eFilePathWord))
                //                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePathWord));

                //            System.IO.File.Copy(SourceFiles, path1, true);
                //            mdl.eFilePathWord = "/ePaper/" + FileName1 + "_" + nametwo.Replace(" ", "");
                //        }

                //    }

                //}
                //else
                //{
                //    mdl.eFilePathWord = null;
                //    //return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                //}

            }
            #endregion

            //mdl.eFilePathWord = null;
            mdl.SplitFileCount = 0;
            mdl.CreatedBy = new Guid(CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.eFileNameId = Guid.NewGuid();
            model.ParentId = fileID;


            // I am Receiver
            mdl.ToDepartmentID = model.DepartmentId;
            string _lstOffice = Convert.ToString(lstOffice);
            if (!string.IsNullOrEmpty(_lstOffice))
            {
                mdl.ToOfficeCode = int.Parse(_lstOffice);
            }
            else
            {
                mdl.ToOfficeCode = 0;
            }
            //////


            // I am Sender
            mdl.DepartmentId = UserDepartmentId;// CurrentSession.DeptID;
            int officecode = 0;
            int.TryParse(CurrentSession.OfficeId, out officecode);
            mdl.OfficeCode = officecode; //int.Parse(CurrentSession.OfficeId);// 0;

            ////

            if (model.PType == "4")
            {
                mdl.PType = "R";
            }
            else
            {
                mdl.PType = model.PType;
            }

            mdl.DocumentTypeId = model.DocumentTypeId;
            mdl.Description = model.Description;
            mdl.PaperNature = model.PaperNature;
            mdl.PaperNatureDays = model.PaperNatureDays;
            mdl.Title = model.Title;
            mdl.PaperRefNo = model.PaperRefNo;
            mdl.ToRecvDetails = model.ToRecvDetails;
            mdl.RecvDetails = model.ToRecvDetails;
            mdl.RevedOption = model.RevedOption;
            mdl.eFileID = model.eFileID;
            //Helper.ExecuteService("eFile", "AddeFileAttachments", mdl);

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@DocumentTypeId", mdl.DocumentTypeId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileName", mdl.eFileName));
            methodParameter.Add(new KeyValuePair<string, string>("@Description", mdl.Description));
            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", mdl.DepartmentId));
            methodParameter.Add(new KeyValuePair<string, string>("@Title", mdl.Title));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileNameId", mdl.eFileNameId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DocType", "SendPaperToDept"));
            methodParameter.Add(new KeyValuePair<string, string>("@SplitFileCount", mdl.SplitFileCount.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@OfficeCode", mdl.OfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNature", mdl.PaperNature.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNatureDays", mdl.PaperNatureDays));
            methodParameter.Add(new KeyValuePair<string, string>("@ToDepartmentID", mdl.ToDepartmentID));
            methodParameter.Add(new KeyValuePair<string, string>("@ToOfficeCode", mdl.ToOfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperStatus", "N"));
            methodParameter.Add(new KeyValuePair<string, string>("@PType", mdl.PType));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePath", mdl.eFilePath));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperRefNo", mdl.PaperRefNo));
            methodParameter.Add(new KeyValuePair<string, string>("@RecvDetails", mdl.RecvDetails));
            methodParameter.Add(new KeyValuePair<string, string>("@ToRecvDetails", mdl.ToRecvDetails));
            methodParameter.Add(new KeyValuePair<string, string>("@RevedOption", mdl.RevedOption));
            methodParameter.Add(new KeyValuePair<string, string>("@isSend", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileId", mdl.eFileID.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePathWord", mdl.eFilePathWord));
            methodParameter.Add(new KeyValuePair<string, string>("@ReFileId", "0"));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperEntry", methodParameter);

            string res = "", serno = "", eFIleAttachmentId = "";
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                res = dataSet.Tables[0].Rows[0][0].ToString();
                serno = dataSet.Tables[0].Rows[0][1].ToString();
                eFIleAttachmentId = dataSet.Tables[0].Rows[0][2].ToString();
            }
            if (res == "Success")
            {
                if (CommetteId.HasValue && CommetteId.Value > 0)
                    return Json(string.Format("{0},{1}", serno, "Commette"), JsonRequestBehavior.AllowGet);
                else
                    return Json(string.Format("{0},{1}", serno, eFIleAttachmentId), JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json("Some Problem Occured. Please Try Again", JsonRequestBehavior.AllowGet);

            }


        }
        public JsonResult SendPubishedTourSMS(string MobileNos, int FileAttachmentId, string EmailIds)
        {
            string Message = string.Empty;
            string[] mobnos = MobileNos.Split(',');
            if (mobnos.Count() > 0)
            {

                List<string> mobileList = new List<string>();
                List<string> EmailList = new List<string>();

                eFileAttachment File = (eFileAttachment)Helper.ExecuteService("eFile", "GetAlleFileAttachmentsByeFileID", new eFileAttachment { eFileAttachmentId = FileAttachmentId });
                string msgBody = string.Format(" Dear Sir/Madam, \n {0}. \n Secretary HP Vidhan Sabha", File.Title);

                foreach (var item in MobileNos.Split(','))
                {
                    mobileList.Add(item);
                }
                foreach (var item in EmailIds.Split(','))
                {
                    EmailList.Add(item);
                }
                SendEmailDetails(EmailList, mobileList, msgBody, "HP Vidhan Sabha", msgBody, true, true, string.Empty);
                Message = "Sucessfully send email and sms";
            }
            else
            {
                Message = "No Phone Number Found.";
            }

            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        #region Send Email
        public static void SendEmailDetails(List<string> emailList, List<string> mobileList, string msgBody, string mailSubject, string mailBody, bool mobileStatus, bool emailStatus, string SavedPdfPath)
        {
            #region SMS And Email

            List<string> emailAddresses = new List<string>();
            List<string> phoneNumbers = new List<string>();

            SMSMessageList smsMessage = new SMSMessageList();
            CustomMailMessage emailMessage = new CustomMailMessage();

            //# SMS Settings
            if (emailList != null)
            {
                emailAddresses.AddRange(emailList);

            }
            if (mobileList != null)
            {
                phoneNumbers.AddRange(mobileList);
            }
            if (emailAddresses != null && phoneNumbers != null)
            {
                foreach (var item in phoneNumbers)
                {
                    smsMessage.MobileNo.Add(item);
                }
                foreach (var item in emailAddresses)
                {
                    emailMessage._toList.Add(item);
                }
            }

            smsMessage.SMSText = msgBody;
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = 1;
            emailMessage._isBodyHtml = true;
            emailMessage._subject = mailSubject;
            emailMessage._body = mailBody;
            if (!string.IsNullOrEmpty(SavedPdfPath))
            {
                EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes((SavedPdfPath)) };
                emailMessage._attachments = new List<EAttachment>();
                emailMessage._attachments.Add(ea);
            }
            try
            {
                if (emailList != null || mobileList != null)
                {
                    //Notification.Send(mobileStatus, emailStatus, smsMessage, emailMessage);
                }

            }
            catch (Exception exp)
            {
                string errorMsg = exp.Message;
                // throw;
            }

            #endregion SMS And Email
        }

        #endregion
        [HttpPost]
        public JsonResult SaveReceivePaperSingle(int eFileAttachId, int eFielId)
        {
            eFileAttachment model = new eFileAttachment();
            model.eFileAttachmentId = eFileAttachId;
            model.eFileID = eFielId;
            model.ReceivedDate = DateTime.Now;
            model.PaperStatus = "Y";
            int res = (int)Helper.ExecuteService("eFile", "UpdateReceivePaperSingle", model);

            if (res == 0)
            {
                return Json("Paper Received Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult AttachPaperToeFileMultiple(string eFileAttIds, int eFielId)
        {
            //int[] d=new  int[3];
            eFileAttachment model = new eFileAttachment();
            string[] d = eFileAttIds.Split(',');
            int[] brnds = new int[d.Length - 1];

            for (int i = 0; i < d.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(d[i].ToString()))
                {
                    brnds[i] = Convert.ToInt32(d[i]);
                }
            }

            model.eFileAttachmentIds = brnds;
            model.eFileID = eFielId;
            //model.ReceivedDate = DateTime.Now;
            //model.PaperStatus = "Y";
            int res = (int)Helper.ExecuteService("eFile", "AttachPapertoEFileMul", model);

            if (res == 0)
            {
                return Json("Paper Attached Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AttachPaperToeFileMultipleR(string eFileAttIds, int eFielId)
        {
            //int[] d=new  int[3];
            eFileAttachment model = new eFileAttachment();
            string[] d = eFileAttIds.Split(',');
            int[] brnds = new int[d.Length - 1];

            for (int i = 0; i < d.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(d[i].ToString()))
                {
                    brnds[i] = Convert.ToInt32(d[i]);
                }
            }

            model.eFileAttachmentIds = brnds;
            model.ReFileID = eFielId;
            //model.ReceivedDate = DateTime.Now;
            //model.PaperStatus = "Y";
            int res = (int)Helper.ExecuteService("eFile", "AttachPapertoEFileMulR", model);

            if (res == 0)
            {
                return Json("Paper Attached Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult AttachPaperToeFileMultipleDraftR(string eFileAttIds, int eFielId)
        {
            //int[] d=new  int[3];
            eFileAttachment model = new eFileAttachment();
            string[] d = eFileAttIds.Split(',');
            int[] brnds = new int[d.Length - 1];

            for (int i = 0; i < d.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(d[i].ToString()))
                {
                    brnds[i] = Convert.ToInt32(d[i]);
                }
            }

            model.eFileAttachmentIds = brnds;
            model.ReFileID = eFielId;
            //model.ReceivedDate = DateTime.Now;
            //model.PaperStatus = "Y";
            var IsSinglFilerequired = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "IsSingleFileMovement", null);
            //if (IsSinglFilerequired.SettingValue == "True")
            //{
            //}
            model.SettingValue_file = IsSinglFilerequired.SettingValue;
            int res = (int)Helper.ExecuteService("eFile", "AttachPapertoEFileMulDraftR", model);

            if (res == 0)
            {
                return Json("Paper Attached Successfully", JsonRequestBehavior.AllowGet);
            }
            else if (res == 1)
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("This File Is Allready Engaged With Another Drafted Paper,So We Are Unable To Process.", JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult ReplyPaperSingle(int eFileAttachmentId, string PaperRefNo, string DepartmentId, int OfficeCode, int DocumentTypeId, string LinkedRefNo, int PaperNature)
        {
            eFileAttachment documents = new eFileAttachment();
            if (PaperNature == 4 || PaperNature == 5)
            {
                documents.PaperRefNo = LinkedRefNo;
            }
            else
            {
                documents.PaperRefNo = PaperRefNo;
            }
            documents.eFileAttachmentId = eFileAttachmentId;
            documents.DepartmentId = DepartmentId;
            documents.OfficeCode = OfficeCode;
            documents.DocumentTypeId = DocumentTypeId;

            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            ///Get eFilePaperNature
            var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList", null);
            ViewBag.eFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);

            return PartialView("_Reply", documents);
        }

        [HttpPost]
        public JsonResult SaveReplyPaper(eFileAttachment model, HttpPostedFileBase file, int? lstOffice, string ReferenceNo)
        {
            eFileAttachment mdl = new eFileAttachment();
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;
            //string url = "~/ePaper/";
            //string directory = Server.MapPath(url);

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.eFilePath))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePath));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.eFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");

                        }

                    }

                }
                else
                {
                    return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }




            //mdl.eFileName = Path.GetFileNameWithoutExtension(file.FileName);

            //string OnlyFileName = mdl.eFileName + "_" + fileID;
            //string Extension = Path.GetExtension(file.FileName);

            //string FullFileName = OnlyFileName + Extension;


            //if (!Directory.Exists(Server.MapPath("~/ePaper")))
            //{
            //    Directory.CreateDirectory(Server.MapPath("~/ePaper"));
            //};
            //string destinationPath = Path.Combine(Server.MapPath("~/ePaper/"), FullFileName);
            //file.SaveAs(destinationPath);


            //string FilePath = "..\\ePaper" + "\\" + FullFileName;
            mdl.SplitFileCount = 0;
            mdl.CreatedBy = new Guid(CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.eFileNameId = Guid.NewGuid();
            model.ParentId = fileID;


            mdl.ToDepartmentID = model.DepartmentId;  //"HPD0001";
            string _lstOffice = Convert.ToString(lstOffice);
            if (!string.IsNullOrEmpty(_lstOffice))
            {
                mdl.ToOfficeCode = int.Parse(_lstOffice);
            }
            else
            {
                mdl.ToOfficeCode = 0;
            }
            mdl.DepartmentId = CurrentSession.DeptID;//CurrentSession.DeptID; AddeFileAttachments
            int officecode = 0;
            int.TryParse(CurrentSession.OfficeId, out officecode);
            mdl.OfficeCode = officecode; //int.Parse(CurrentSession.Offic
            //   mdl.OfficeCode = Convert.ToInt32(CurrentSession.OfficeId);

            mdl.PType = model.PType;
            mdl.DocumentTypeId = model.DocumentTypeId;
            mdl.Description = model.Description;
            mdl.PaperNature = model.PaperNature;
            mdl.PaperNatureDays = model.PaperNatureDays;
            mdl.Title = model.Title;
            mdl.PaperRefNo = model.PaperRefNo;
            mdl.eFileAttachmentId = model.eFileAttachmentId;
            //Helper.ExecuteService("eFile", "AddeFileAttachments", mdl);

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@DocumentTypeId", mdl.DocumentTypeId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileName", mdl.eFileName));
            methodParameter.Add(new KeyValuePair<string, string>("@Description", mdl.Description));
            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", mdl.DepartmentId));
            methodParameter.Add(new KeyValuePair<string, string>("@Title", mdl.Title));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileNameId", mdl.eFileNameId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DocType", "ReceivePaperByDept"));
            methodParameter.Add(new KeyValuePair<string, string>("@SplitFileCount", mdl.SplitFileCount.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@OfficeCode", mdl.OfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNature", mdl.PaperNature.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNatureDays", mdl.PaperNatureDays));
            methodParameter.Add(new KeyValuePair<string, string>("@ToDepartmentID", mdl.ToDepartmentID));
            methodParameter.Add(new KeyValuePair<string, string>("@ToOfficeCode", mdl.ToOfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperStatus", "N"));
            methodParameter.Add(new KeyValuePair<string, string>("@PType", mdl.PType));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePath", mdl.eFilePath));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperRefNo", ReferenceNo));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileAttachmentId", mdl.eFileAttachmentId.ToString()));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperEntry_Reply", methodParameter);
            string res = "", serno = "";
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                res = dataSet.Tables[0].Rows[0][0].ToString();
                serno = dataSet.Tables[0].Rows[0][1].ToString();
            }
            if (res == "Success")
            {

                return Json("Paper Replied Successfully. Paper Ref No. : <span class='label label-xlg label-primary arrowed arrowed-right' style='  font-size: 16px;  height: 29px;'><b>" + serno + "</b></span>", JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json("Some Problem Occured. Please Try Again", JsonRequestBehavior.AllowGet);

            }

        }


        [HttpPost]
        public JsonResult AttacheFileToCommitteeMultiple(string eFileAttIds, int eFielId)
        {
            //int[] d=new  int[3];
            eFileAttachment model = new eFileAttachment();
            string[] d = eFileAttIds.Split(',');
            int[] brnds = new int[d.Length - 1];

            for (int i = 0; i < d.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(d[i].ToString()))
                {
                    brnds[i] = Convert.ToInt32(d[i]);
                }
            }


            model.eFileAttachmentIds = brnds;
            model.eFileID = eFielId;
            //model.ReceivedDate = DateTime.Now;
            //model.PaperStatus = "Y";
            int res = (int)Helper.ExecuteService("eFile", "AttacheFiletoCommitteeMul", model);

            if (res == 0)
            {
                return Json("eFile attached to Committee Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult AddNewNoting(int eFileId, string eFileNumber)
        {
            eFileNoting model = new eFileNoting();
            model.eFileID = eFileId;
            ViewBag.eFileNum = eFileNumber;
            return PartialView("_AddNoting", model);

        }

        [HttpPost]
        public JsonResult AddNewNotingtoeFile(eFileNoting model, HttpPostedFileBase file, string hdnCheckVal)
        {

            eFileNoting mdl = new eFileNoting();

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/eFileAttachments/";
            string MainDirectory = FileSettings.SettingValue + url;

            string url1 = "~/ePaper/TempFile";
            string directory = Server.MapPath(url1);

            string UrlImage = "~/ePaper/FileWord";
            string directoryImage = Server.MapPath(UrlImage);

            string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
            if (savedFileName.Length > 0)
            {
                string SourceFile = savedFileName[0];
                foreach (string page in savedFileName)
                {
                    Guid FileName = Guid.NewGuid();

                    string name = Path.GetFileName(page);

                    string pathCompress = System.IO.Path.Combine(directory, name);
                    string pathCompressImage = System.IO.Path.Combine(directoryImage, name);
                    Extensions.ImageResizerExtensions imgex = new Extensions.ImageResizerExtensions(600);
                    System.IO.File.Copy(pathCompress, pathCompressImage, true);
                    imgex.Resize(pathCompress, pathCompressImage);

                    System.IO.File.Delete(pathCompress);
                    System.IO.File.Copy(pathCompressImage, pathCompress, true);
                    System.IO.File.Delete(pathCompressImage);




                    string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));

                    string extfile = Path.GetExtension(page);

                    if (extfile == ".jpg")  //|| extfile == ".png" || extfile == ".jpeg" || extfile == ".gif"
                    { }
                    else
                    {
                        return Json("Only Image File Required  '.jpg' Extension File ", JsonRequestBehavior.AllowGet); //or '.png' or 'jpeg' or '.gif'

                    }



                    if (!string.IsNullOrEmpty(name))
                    {
                        if (!string.IsNullOrEmpty(mdl.FilePath))
                            System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.FilePath));

                        model.eNotingFileName = Path.GetFileNameWithoutExtension(name);
                        string fileExt = Path.GetExtension(name);
                        if (!Directory.Exists(MainDirectory + model.eFileID))
                        {
                            Directory.CreateDirectory(MainDirectory + model.eFileID);
                        };
                        string destinationPath = Path.Combine(MainDirectory + model.eFileID, Path.GetFileName(name));
                        //System.IO.File.SaveAs(destinationPath);
                        //System.IO.File.Copy(SourceFile, destinationPath, true);
                        int PageSeq = (int)Helper.ExecuteService("eFile", "GetPNo", model.eFileID);
                        PageSeq += 1;
                        //PDF to Image.
                        List<int> PDFPageNumbers = new List<int>();

                        if (!Directory.Exists(MainDirectory + model.eFileID + "/SplitedFiles"))
                        {
                            Directory.CreateDirectory(MainDirectory + model.eFileID + "/SplitedFiles");


                            //string pageFilePath = Path.Combine(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles"),model.eNotingFileName + "-p" + PageSeq + "" + fileExt);
                            //System.IO.File.Copy(SourceFile, pageFilePath, true);
                            //PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eNotingFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);
                        }
                        //else
                        //{

                        /// Resize Code ////

                        //string basePath = System.IO.Path.Combine(directory, model.eNotingFileName);
                        //Extensions.ImageResizerExtensions imgex = new Extensions.ImageResizerExtensions(600);



                        //////////////////////

                        string pageFilePath = Path.Combine(MainDirectory + model.eFileID + "/SplitedFiles", model.eNotingFileName + "-p" + PageSeq + "" + fileExt);
                        System.IO.File.Copy(SourceFile, pageFilePath, true);
                        //PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eNotingFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);
                        //}


                        model.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
                        model.CreatedDate = DateTime.Now;
                        model.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
                        model.ModifiedDate = DateTime.Now;
                        model.FilePath = url + model.eFileID + "/SplitedFiles/" + model.eNotingFileName + "-p" + PageSeq + "" + fileExt;
                        model.NotingStatus = 1; //1 for active  2 for updated



                        mdl.FilePath = url + model.eFileID + "/" + model.eNotingFileName + "" + fileExt;

                        if (hdnCheckVal == "0")
                        {
                            model.FileUploadSeq = PageSeq;
                            var value = (Boolean)Helper.ExecuteService("eFile", "AddeFileNotings", model);
                            if (value == true)
                            {
                                return Json("Noting Addedd Successfully", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("Some error found.Please Try again", JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            model.FileUploadSeq = (int)Helper.ExecuteService("eFile", "GetPNo", model.eFileID);



                            if (model.FileUploadSeq != 0)
                            {
                                int eNotingID = (int)Helper.ExecuteService("eFile", "GeteNotingIDByeFileIdandSeqNo", model);

                                // Update Noting
                                model.eNotingID = eNotingID;
                                model.NotingStatus = 2;

                                var value = (Boolean)Helper.ExecuteService("eFile", "UpdateLasteFileNotings", model);
                                if (value == true)
                                {
                                    return Json("Noting Updated Successfully", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json("Some error found in Update.Please Try again", JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json("No Noting Details Found to Update", JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    return Json("Some error1 found.Please Try again", JsonRequestBehavior.AllowGet);
                }
                return Json("Some error2 found.Please Try again", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
            }


        }

        public PartialViewResult eFileByCommittee()
        {
            eFileLogin();

            CommitteeList comList = new CommitteeList();
            comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            ViewBag.CommitteeList = comList.CommitteeView.Select(a => new SelectListItem { Text = a.CommitteeName, Value = a.CommitteeId.ToString() }).ToList();

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            return PartialView("_eFileByCommittee", eList);
        }

        public ActionResult eFileIndexDetails(eFileViewModel model)
        {
            try
            {
                eFileAttachment documents = new eFileAttachment();
                List<int> PDFPageNumbers = new List<int>();
                documents.eFileID = model.eFileID;
                documents.DepartmentId = CurrentSession.DeptID;
                int officecode = 0;
                int.TryParse(CurrentSession.OfficeId, out officecode);
                documents.OfficeCode = officecode; //int.Parse(CurrentSession.Offic
                //   documents.OfficeCode = Convert.ToInt32(CurrentSession.OfficeId);
                ViewBag.Subject = model.eFileSubject;
                ViewBag.eFileNum = model.eFileNumber;
                ViewBag.EfileId = model.eFileID;
                //model. = (List<string>)Helper.ExecuteService("eFile", "eFileDocumentIndex", model.eFileID);
                //model.eFileAttachment = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)Helper.ExecuteService("eFile", "GetAlleFileAttachments", model.eFileID);
                //model.eFileAttachment = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)Helper.ExecuteService("eFile", "GetAlleFileAttachmentsByDeptIDandeFileID", documents);

                List<eFileAttachment> ListModel = new List<eFileAttachment>();
                eFileAttachment obj = new eFileAttachment();
                DataSet dataSet = new DataSet();
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@efileid", documents.eFileID.ToString()));
                methodParameter.Add(new KeyValuePair<string, string>("@DeptID", documents.DepartmentId));
                methodParameter.Add(new KeyValuePair<string, string>("@officecode", documents.OfficeCode.ToString()));

                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_GeteFileIndexData", methodParameter);
                if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        obj = new eFileAttachment();
                        obj.eFileAttachmentId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["efileattachmentid"]);
                        obj.PaperRefNo = dataSet.Tables[0].Rows[i]["PaperRefNo"].ToString();
                        obj.CreatedDate = Convert.ToDateTime(dataSet.Tables[0].Rows[i]["PDate"]);
                        obj.mode = dataSet.Tables[0].Rows[i]["PMode"].ToString();
                        obj.Title = dataSet.Tables[0].Rows[i]["Title"].ToString();
                        obj.eFilePath = dataSet.Tables[0].Rows[i]["efilepath"].ToString();
                        obj.LinkedRefNo = dataSet.Tables[0].Rows[i]["linkedrefno"].ToString();
                        obj.DepartmentId = dataSet.Tables[0].Rows[i]["deptid"].ToString();
                        obj.RecvDetails = dataSet.Tables[0].Rows[i]["details"].ToString();
                        obj.Name = dataSet.Tables[0].Rows[i]["FullSubject"].ToString();
                        obj.RevedOption = dataSet.Tables[0].Rows[i]["RevedOption"].ToString();
                        obj.PType = dataSet.Tables[0].Rows[i]["PType"].ToString();
                        obj.ReplyStatus = dataSet.Tables[0].Rows[i]["ReplyStatus"].ToString();
                        obj.eMode = Convert.ToInt32(dataSet.Tables[0].Rows[i]["emode"]);
                        obj.SendStatus = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["sendstatus"]);
                        obj.CommitteeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["CommitteeId"]);
                        obj.DocumentTypeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["DocumentTypeId"]);
                        obj.ReportLayingHouse = Convert.ToInt32(dataSet.Tables[0].Rows[i]["ReportLayingHouse"]);
                        obj.DocuemntPaperType = dataSet.Tables[0].Rows[i]["LinkRecv"].ToString();
                        if (obj.RevedOption == "H")
                        {
                            obj.DepartmentName = dataSet.Tables[0].Rows[i]["DeptName"].ToString() + ", Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();
                        }
                        else
                        {
                            obj.DepartmentName = "OTHER , Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();

                        }
                        obj.PaperRefrenceManualy = dataSet.Tables[0].Rows[i]["PaperRefrenceManualy"].ToString();
                        obj.AnnexPdfPath = dataSet.Tables[0].Rows[i]["AnnexPdfPath"].ToString();
                        obj.eFileNumber = dataSet.Tables[0].Rows[i]["eFileNumber"].ToString();
                        ViewBag.Subject = dataSet.Tables[0].Rows[i]["eFileSubject"].ToString();
                        ViewBag.eFileNum = dataSet.Tables[0].Rows[i]["eFileNumber"].ToString();// model.eFileNumber;
                        ListModel.Add(obj);

                    }
                }

                //List<eFileAttachment> ListModel2 = new List<eFileAttachment>();
                //ListModel2 =(List<eFileAttachment>)( ListModel2.OrderBy(a => a.eFileAttachmentId));

                model.eFileAttachment = ListModel;// (List<eFileAttachment>)(ListModel.OrderBy(a => a.CreatedDate)); ;

                var ListAss = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                ViewBag.AssemblyList = new SelectList(ListAss, "AssemblyCode", "AssemblyName", null);
                documents = (eFileAttachment)Helper.ExecuteService("eFile", "NotingMovementListFiles", documents);
                model.HouseComModel = documents.HouseComModel;
            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }
            // ViewBag.Title1 = "हि0 प्र0 विधान सभा सचिवालय";
            // ViewBag.Title2 = "(टिप्पणी एवं आदेश)";
            return View("_eFileBookViewByIDPartialIndexDet", model);
            //return View("eFileIndex", model.eFileAttachment);
            //return View(model); _eFileBookViewByIDPartialIndexDet
        }
        [HttpGet]
        public ActionResult Pdf_EfileIndexData(string eFileId)
        {
            eFileViewModel model = new eFileViewModel();

            eFileAttachment documents = new eFileAttachment();

            documents.eFileID = Convert.ToInt32(eFileId);

            documents = (eFileAttachment)Helper.ExecuteService("eFile", "NotingMovementListFiles", documents);
            model.HouseComModel = documents.HouseComModel;
            string fileName = "";
            string Subject = "";
            if (model.HouseComModel != null)
            {
                foreach (var item in model.HouseComModel)
                {
                    fileName = item.eFileName;
                    Subject = item.Subject;
                }
            }

            var DocLocation = GeneratePdf(model, fileName, Subject);

            // model.HouseComModel = documents.HouseComModel;


            return Json(DocLocation, JsonRequestBehavior.AllowGet);


        }
        public Object GeneratePdf(eFileViewModel _model, string fileName, string subject)
        {
            try
            {

                int count = 0;
                string outXml = "";

                string savedPDF = string.Empty;
                if (_model.HouseComModel != null)
                {

                    outXml = @"<html>                           
                                <body style='font-family:DVOT-Yogesh;color:#003366;'> <div><div style='width: 100%;'>";
                    outXml += @"<div> 

                    </div>
            

<table style='width:100%'>
 
      <tr>
          <td style='text-align:center;font-size:28px;' >
             <b> हि0 प्र0 विधान सभा सचिवालय </b>
 <br />
                             (टिप्पणी एवं आदेश)  <br />
  </td>
              </tr>
 <tr>
          <td style='text-align:left;font-size:24px;' >
                                नस्ति सं0 :-" + fileName + @" 
                                <br>
                                विषय:-" + subject + @"  <br /> </td>
              </tr>

             
      </table>

<table style='width: 100%; border-collapse: collapse' border: solid 1px black;'>";
                    //sb.Append("<img src=" + @"data:image/jpeg;base64," + GetImage(path) + "' />");​

                    foreach (var item in _model.HouseComModel)
                    {
                        count = count + 1;
                        string style = (count % 2 == 0) ? "odd" : "even";
                        outXml += @"

 <tr>
           <td style='text-align: center;border:1px  solid black;'>" + count + @" </td>  <br>
           <td style='text-align: left;border:1px  solid black; '>" + item.Remarks + @" <br>
              <img style='margin-bottom:5px;' src=" + @"data:image/jpeg;base64," + GetImage(item.SignPath) + " height='100' width='100'/> <br> " +
             Convert.ToDateTime(item.AssignDateTime).ToString("dd/MM/yyyy hh:mm tt") +
                                @"<br> <br>
                               <strong>Marked From : </strong> " + item.AssignfrmName + @"<br> <br>
                             <strong> Marked To :</strong>" + item.MarkedDesignation + "(" + item.MarkedName + ")" + @"</td></tr>

          ";
                    }
                }

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new EvoPdf.Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new EvoPdf.Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/DataPdfFile/";
                // fileName = "QuestionList" + "DateFrom" + "_" + Convert.ToDateTime(model.DateFrom).ToString("dd/MM/yyyy") + "_" + "DateTo" + "_" + Convert.ToDateTime(model.DateTo).ToString("dd/MM/yyyy") + "_" + "S" + ".pdf";
                // fileName = fileName.Replace("/", "-");
                //  HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);
                fileName = "DataRpt.pdf";
                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                //DownloadDateWiseQuestion();
                return url + fileName;


            }

            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }
        public string GetImage(string path)
        {
            string signpath = "";
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            // Update.CompPath = FileSettings.SettingValue;
            if (path == null || path == "")
            {
                // signpath = Server.MapPath("../../Images/SignaturePath/imageedit_33_7349958904.gif"); 
                //Server.MapPath("../../Images/Common/imageedit_33_7349958904.gif");   // http://localhost:50730/Images/SignaturePath/imageedit_33_7349958904.gif;
            }
            else
            {
                // signpath = FileSettings.SettingValue +  path + "";
                signpath = Server.MapPath("../../" + path + "");
                //signpath = Server.MapPath("../../Images/SignaturePath/imageedit_33_7349958904.gif"); 

            }
            if (System.IO.File.Exists(signpath))
            {
                using (Image image = Image.FromFile(signpath))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        image.Save(m, image.RawFormat);
                        byte[] imageBytes = m.ToArray();

                        // Convert byte[] to Base64 String
                        return Convert.ToBase64String(imageBytes);
                    }
                }
            }
            else
            {
                return path;
            }
        }

        public PartialViewResult GetCommitteeDetByCommID(int CommitteeId)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeDetByID", CommitteeId);


            return PartialView("_CommitteeDetailsByCommID", eList);

        }

        public JsonResult GetSessionssByAssemblyId(int AssemblyID)
        {
            List<SBL.DomainModel.Models.Session.mSession> SessLst = new List<SBL.DomainModel.Models.Session.mSession>();
            SBL.DomainModel.Models.Session.mSession model = new DomainModel.Models.Session.mSession();
            model.AssemblyID = AssemblyID;
            SessLst = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", model);

            return Json(SessLst, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSessionDatesByAssemblyIdSessId(int AssemblyID, int SessionID)
        {
            List<SBL.DomainModel.Models.Session.mSessionDate> SessDateLst = new List<SBL.DomainModel.Models.Session.mSessionDate>();
            SBL.DomainModel.Models.Session.mSessionDate mdl = new DomainModel.Models.Session.mSessionDate();
            List<SBL.DomainModel.Models.Session.mSessionDate> SessDateLst1 = new List<SBL.DomainModel.Models.Session.mSessionDate>();
            SBL.DomainModel.Models.Session.mSession model = new DomainModel.Models.Session.mSession();
            model.AssemblyID = AssemblyID;
            model.SessionCode = SessionID;
            SessDateLst = (List<SBL.DomainModel.Models.Session.mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", model);

            foreach (var item in SessDateLst)
            {
                mdl = new DomainModel.Models.Session.mSessionDate();
                mdl.Id = item.Id;
                mdl.SessionDate_Local = Convert.ToDateTime(item.SessionDate).ToString("dd/MM/yyyy");
                SessDateLst1.Add(mdl);
            }


            return Json(SessDateLst1, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SavePaperLaidInHouse(int AssemblyID, int SessionID, int SessionDateId, int eAttchId, int CommitteeId, string Subject, string eFileName, string eFilePath, string Description)
        {



            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            string directory = FileSettings.SettingValue;
            string SourcePath = directory + eFilePath;
            //obj.AssemblyId + "_" + obj.SessionId + "_" + "R" + "_" + tPaper.PaperLaidId + "_V" + model.Count + ext;

            string FilePathTarget = "/PaperLaid/" + CurrentSession.AssemblyId + "/" + CurrentSession.SessionId + "/";

            if (!System.IO.Directory.Exists(directory + FilePathTarget))
            {
                System.IO.Directory.CreateDirectory(directory + FilePathTarget + "/Signed/");
            }




            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@AssemblyId", AssemblyID.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", SessionID.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDateId", SessionDateId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@Title", Subject));
            methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", CurrentSession.DeptID));
            methodParameter.Add(new KeyValuePair<string, string>("@DepartmentName", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@Description", Description));
            methodParameter.Add(new KeyValuePair<string, string>("@EventId", "8"));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeChairmanMemberId", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", CommitteeId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@Version", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@FilePath", FilePathTarget));
            methodParameter.Add(new KeyValuePair<string, string>("@SignedFilePath", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@FileName", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@efileattachmentid", eAttchId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedByUserCode", CurrentSession.UserID.ToString()));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperLaidInHouse", methodParameter);
            string FileName = "";
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                FileName = dataSet.Tables[0].Rows[0][1].ToString();

            }


            string UrlTarget = "/PaperLaid/" + CurrentSession.AssemblyId + "/" + CurrentSession.SessionId + "/Signed/" + FileName;

            string TargetPath = directory + UrlTarget;

            System.IO.File.Copy(SourcePath, TargetPath, true);



            return Json("Paper Laid In the House Successfully", JsonRequestBehavior.AllowGet);
        }

        //Update eFile
        public ActionResult EditeFileDetails(int eFileId)
        {

            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
            SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel();
            model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            model.DocumentType = (List<SBL.DomainModel.Models.Document.mDocumentType>)Helper.ExecuteService("eFile", "eFileDocumentIndex", 1);
            //model.CommitteeId = CommitteeId;
            //EFile Type List  added on 27 July 2015
            model.eFileTypeList = (List<SBL.DomainModel.Models.eFile.eFileType>)Helper.ExecuteService("eFile", "eFileTypeDetails", null);
            ///********//

            model.eFileDetail = (SBL.DomainModel.Models.eFile.eFile)Helper.ExecuteService("eFile", "GeteFileDetailsByFileId", eFileId);
            model.eFileID = eFileId;
            model.eFileNumber = model.eFileDetail.eFileNumber;
            model.CommitteeId = model.eFileDetail.CommitteeId;
            model.eFileSubject = model.eFileDetail.eFileSubject;
            model.FromYear = model.eFileDetail.FromYear;
            model.ToYear = model.eFileDetail.ToYear;
            model.OldDescription = model.eFileDetail.OldDescription;
            model.NewDescription = model.eFileDetail.NewDescription;
            model.DepartmentId = model.eFileDetail.DepartmentId;
            model.Officecode = model.eFileDetail.Officecode;
            model.eFileTypeID = model.eFileDetail.eFileTypeID;
            model.Mode = "Edit";
            ViewBag.OffICode = CurrentSession.OfficeId;
            return PartialView("_CreateeFile", model);
        }


        public PartialViewResult eFileListForCommitteeAttach()
        {
            ViewBag.Mode = "CommitteeAttach";
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            return PartialView("_eFileListViewCommitteeAttach", eList);

        }

        public PartialViewResult eFileListDetailsALLStatusToAttach(int pageId = 1, int pageSize = 25)
        {
            ViewBag.Mode = "Committee";
            ViewBag.CommAttach = "CommitteeAttach";
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            return PartialView("_eFileListViewAllCommitteeAttach", eList);

        }

        public ActionResult eFileIndexDetailsPaperLaid(eFileViewModel model)
        {
            try
            {
                eFileAttachment documents = new eFileAttachment();
                List<int> PDFPageNumbers = new List<int>();
                documents.eFileID = model.eFileID;
                documents.DepartmentId = CurrentSession.DeptID;
                int OfficeId = 0;
                int.TryParse(CurrentSession.OfficeId, out OfficeId);

                documents.OfficeCode = OfficeId;// Convert.ToInt32(CurrentSession.OfficeId);
                ViewBag.eFileID = model.eFileID;
                ViewBag.Subject = model.eFileSubject;
                ViewBag.eFileNum = model.eFileNumber;

                List<eFileAttachment> ListModel = new List<eFileAttachment>();
                eFileAttachment obj = new eFileAttachment();
                DataSet dataSet = new DataSet();
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@efileid", documents.eFileID.ToString()));
                methodParameter.Add(new KeyValuePair<string, string>("@DeptID", documents.DepartmentId));
                methodParameter.Add(new KeyValuePair<string, string>("@officecode", documents.OfficeCode.ToString()));

                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_GeteFileIndexData", methodParameter);
                if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        obj = new eFileAttachment();
                        obj.eFileAttachmentId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["efileattachmentid"]);
                        obj.PaperRefNo = dataSet.Tables[0].Rows[i]["PaperRefNo"].ToString();
                        obj.CreatedDate = Convert.ToDateTime(dataSet.Tables[0].Rows[i]["PDate"]);
                        obj.mode = dataSet.Tables[0].Rows[i]["PMode"].ToString();
                        obj.Title = dataSet.Tables[0].Rows[i]["Title"].ToString();
                        obj.eFilePath = dataSet.Tables[0].Rows[i]["efilepath"].ToString();
                        obj.LinkedRefNo = dataSet.Tables[0].Rows[i]["linkedrefno"].ToString();
                        obj.DepartmentId = dataSet.Tables[0].Rows[i]["deptid"].ToString();
                        obj.RecvDetails = dataSet.Tables[0].Rows[i]["details"].ToString();
                        obj.Name = dataSet.Tables[0].Rows[i]["FullSubject"].ToString();
                        obj.RevedOption = dataSet.Tables[0].Rows[i]["RevedOption"].ToString();
                        obj.PType = dataSet.Tables[0].Rows[i]["PType"].ToString();
                        obj.ReplyStatus = dataSet.Tables[0].Rows[i]["ReplyStatus"].ToString();
                        obj.eMode = Convert.ToInt32(dataSet.Tables[0].Rows[i]["emode"]);
                        obj.SendStatus = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["sendstatus"]);
                        obj.CommitteeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["CommitteeId"]);
                        obj.DocumentTypeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["DocumentTypeId"]);
                        obj.ReportLayingHouse = Convert.ToInt32(dataSet.Tables[0].Rows[i]["ReportLayingHouse"]);
                        obj.DocuemntPaperType = dataSet.Tables[0].Rows[i]["LinkRecv"].ToString();
                        if (obj.RevedOption == "H")
                        {
                            obj.DepartmentName = dataSet.Tables[0].Rows[i]["DeptName"].ToString() + ", Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();
                        }
                        else
                        {
                            obj.DepartmentName = "OTHER , Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();

                        }

                        ListModel.Add(obj);
                    }
                }



                model.eFileAttachment = ListModel;

                var ListAss = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                ViewBag.AssemblyList = new SelectList(ListAss, "AssemblyCode", "AssemblyName", null);


            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return View("_eFileIndexPaperLaid", model.eFileAttachment);
            //return View(model);
        }
        public ActionResult eFileIndexDetailsPaperLaid_OnlyLaidPapers(eFileViewModel model)
        {
            try
            {
                eFileAttachment documents = new eFileAttachment();
                List<int> PDFPageNumbers = new List<int>();
                documents.eFileID = model.eFileID;
                documents.DepartmentId = CurrentSession.DeptID;
                int OfficeId = 0;
                int.TryParse(CurrentSession.OfficeId, out OfficeId);
                documents.OfficeCode = OfficeId;// Convert.ToInt32(CurrentSession.OfficeId);
                ViewBag.eFileID = model.eFileID;
                ViewBag.Subject = model.eFileSubject;
                ViewBag.eFileNum = model.eFileNumber;

                List<eFileAttachment> ListModel = new List<eFileAttachment>();
                eFileAttachment obj = new eFileAttachment();
                DataSet dataSet = new DataSet();
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@efileid", documents.eFileID.ToString()));
                methodParameter.Add(new KeyValuePair<string, string>("@DeptID", documents.DepartmentId));
                methodParameter.Add(new KeyValuePair<string, string>("@officecode", documents.OfficeCode.ToString()));

                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_GeteFileIndexData_OnlyLaidPapers", methodParameter);
                if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        obj = new eFileAttachment();
                        obj.eFileAttachmentId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["efileattachmentid"]);
                        obj.PaperRefNo = dataSet.Tables[0].Rows[i]["PaperRefNo"].ToString();
                        obj.CreatedDate = Convert.ToDateTime(dataSet.Tables[0].Rows[i]["PDate"]);
                        obj.mode = dataSet.Tables[0].Rows[i]["PMode"].ToString();
                        obj.Title = dataSet.Tables[0].Rows[i]["Title"].ToString();
                        obj.eFilePath = dataSet.Tables[0].Rows[i]["efilepath"].ToString();
                        obj.LinkedRefNo = dataSet.Tables[0].Rows[i]["linkedrefno"].ToString();
                        obj.DepartmentId = dataSet.Tables[0].Rows[i]["deptid"].ToString();
                        obj.RecvDetails = dataSet.Tables[0].Rows[i]["details"].ToString();
                        obj.Name = dataSet.Tables[0].Rows[i]["FullSubject"].ToString();
                        obj.RevedOption = dataSet.Tables[0].Rows[i]["RevedOption"].ToString();
                        obj.PType = dataSet.Tables[0].Rows[i]["PType"].ToString();
                        obj.ReplyStatus = dataSet.Tables[0].Rows[i]["ReplyStatus"].ToString();
                        obj.eMode = Convert.ToInt32(dataSet.Tables[0].Rows[i]["emode"]);
                        obj.SendStatus = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["sendstatus"]);
                        obj.CommitteeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["CommitteeId"]);
                        obj.DocumentTypeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["DocumentTypeId"]);
                        obj.ReportLayingHouse = Convert.ToInt32(dataSet.Tables[0].Rows[i]["ReportLayingHouse"]);
                        obj.DocuemntPaperType = dataSet.Tables[0].Rows[i]["LinkRecv"].ToString();
                        if (obj.RevedOption == "H")
                        {
                            obj.DepartmentName = dataSet.Tables[0].Rows[i]["DeptName"].ToString() + ", Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();
                        }
                        else
                        {
                            obj.DepartmentName = "OTHER , Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();

                        }

                        ListModel.Add(obj);
                    }
                }



                model.eFileAttachment = ListModel;

                var ListAss = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                ViewBag.AssemblyList = new SelectList(ListAss, "AssemblyCode", "AssemblyName", null);
                ViewBag.SearchPaper = "L";

            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return View("_eFileIndexPaperLaid", model.eFileAttachment);
            //return View(model);
        }

        #region BulkSMS
        public ActionResult CustomizeSMSEMAILS()
        {
            SBL.DomainModel.ComplexModel.OnlineMemberQmodel model = new SBL.DomainModel.ComplexModel.OnlineMemberQmodel();
            ViewBag.GenericOnly = "GENRICONLY";

            return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_CustomizeSMSEMAIL.cshtml", model);
        }
        #endregion

        #region Committee System
        public ActionResult CommitteeMembers(int ID, string Name)
        {
            CommitteeMembersUpload model = new CommitteeMembersUpload();
            model.CommitteeId = ID;
            ViewBag.CommitteeName = Name;
            return PartialView("_CommitteeMembers", model);
        }
        [HttpPost]
        public JsonResult UploadCommitteeMemnbers(CommitteeMembersUpload model, HttpPostedFileBase file)
        {

            CommitteeMembersUpload mdl = new CommitteeMembersUpload();

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);
                        mdl.CommFileName = name;
                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.CommFilePath))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.CommFilePath));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.CommFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                        }

                    }

                }
                else
                {
                    return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }

            mdl.Remark = model.Remark;
            mdl.CommStatus = 1;

            mdl.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.IsDeleted = false;
            mdl.CommitteeId = model.CommitteeId;

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@CommFileName", mdl.CommFileName));
            methodParameter.Add(new KeyValuePair<string, string>("@CommFilePath", mdl.CommFilePath));
            methodParameter.Add(new KeyValuePair<string, string>("@Remark", mdl.Remark));
            methodParameter.Add(new KeyValuePair<string, string>("@CommStatus", mdl.CommStatus.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", mdl.CommitteeId.ToString()));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_CommitteeMembersUpload", methodParameter);

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                string res = dataSet.Tables[0].Rows[0][0].ToString();

                if (res == "Success")
                {
                    return Json("Pdf Uploaded Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Some problem occured. Please Try again", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("Some error1 found.Please Try again", JsonRequestBehavior.AllowGet);

        }

        public PartialViewResult UploadedCommitteeMemebersPdf(int CommitteeId, string CommitteeName)
        {
            CommitteeMembersUpload model = new CommitteeMembersUpload();
            List<CommitteeMembersUpload> listComm = new List<CommitteeMembersUpload>();
            model.CommitteeId = CommitteeId;
            ViewBag.CommitteeName = CommitteeName;

            listComm = (List<CommitteeMembersUpload>)Helper.ExecuteService("eFile", "GetCommitteeUploadedPdf", model);

            return PartialView("_CommMembersUploadList", listComm);

        }
        public PartialViewResult UploadedGeneratedPdf()
        {

            List<CommitteeMembersUpload> listComm = new List<CommitteeMembersUpload>();


            listComm = (List<CommitteeMembersUpload>)Helper.ExecuteService("eFile", "GetGeneratedPdfList", null);

            return PartialView("_CommMembersUploadList", listComm);

        }

        public ActionResult GetAllNewComitteeList()
        {
            CommitteeList comList = new CommitteeList();
            List<tCommitteeMemberList> committeMember = new List<tCommitteeMemberList>();
            try
            {

                //comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);


                comList.committeeMemberList = (List<tCommitteeMemberList>)Helper.ExecuteService("Committee", "GetAllCommitteeMembers", null);
                comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
                ViewBag.CommitteeList = comList.CommitteeView.Select(a => new SelectListItem { Text = a.CommitteeName, Value = a.CommitteeId.ToString() }).ToList();
                int mode = (int)Helper.ExecuteService("Committee", "CommitteeExisit", null);
                ViewBag.mode = mode;
                //var list=committeMember
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Committee Details";
                return View("AdminErrorPage");
                throw ex;
            }

            return PartialView("_GetAllNewCommitteeList", comList);
        }
        public static SelectList GetCommitteeYear()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Select Committee Year", Value = "0" });
            for (int i = 2000; i <= 2050; i++)
            {
                result.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });
            }
            return new SelectList(result, "Value", "Text");
        }
        public ActionResult CreateNewCommittee()
        {
            SBL.DomainModel.Models.Assembly.mAssembly Amodel = new DomainModel.Models.Assembly.mAssembly();
            var AssemblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyData", Amodel);
            var CommitteTypeList = (List<SBL.DomainModel.Models.Committee.mCommitteeType>)Helper.ExecuteService("Committee", "GetAllCommiteeType", null);
            var CommitteeList = (List<SBL.DomainModel.Models.Committee.CommitteeSearchModel>)Helper.ExecuteService("Committee", "GetCommittees", 2);
            SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models.CommitteeViewModel model = new CommitteeViewModel();
            model.Assembly = new SelectList(AssemblyList, "AssemblyID", "AssemblyName", null);
            model.CommitteeType = new SelectList(CommitteTypeList, "CommitteeTypeId", "CommitteeTypeName", null);
            model.ParentCommittee = new SelectList(CommitteeList, "CommitteeId", "CommitteeName", null);
            ViewBag.CommitteeYear = GetCommitteeYear();
            model.Mode = "Add";
            return PartialView("_CreateCommittee", model);
        }

        [HttpGet]
        public ActionResult EditNewCommittee(int Id)
        {
            SBL.DomainModel.Models.Assembly.mAssembly Amodel = new DomainModel.Models.Assembly.mAssembly();
            tCommittee committeeToEdit = (tCommittee)Helper.ExecuteService("Committee", "GetSubCommitteById", Id);
            var AssemblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyData", Amodel);
            var CommitteTypeList = (List<SBL.DomainModel.Models.Committee.mCommitteeType>)Helper.ExecuteService("Committee", "GetAllCommiteeType", null);
            var CommitteeList = (List<SBL.DomainModel.Models.Committee.CommitteeSearchModel>)Helper.ExecuteService("Committee", "GetCommittees", 2);
            //var CommitteeList = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("Committee", "GetCommittee", committeeToEdit);
            SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models.CommitteeViewModel model = new CommitteeViewModel();
            model.Assembly = new SelectList(AssemblyList, "AssemblyID", "AssemblyName", null);
            model.CommitteeType = new SelectList(CommitteTypeList, "CommitteeTypeId", "CommitteeTypeName", null);
            model.ParentCommittee = new SelectList(CommitteeList, "CommitteeId", "CommitteeName", null);
            model.CommitteeName = committeeToEdit.CommitteeName;
            //model.DateofCreation = committeeToEdit.DateOfCreation;
            model.Description = committeeToEdit.Remark;
            model.IsActive = committeeToEdit.IsActive;
            model.AssemblyID = committeeToEdit.AssemblyID;
            model.CommitteeTypeId = committeeToEdit.CommitteeTypeId;
            model.ParentId = committeeToEdit.ParentId;
            model.CommitteeYear = committeeToEdit.CommitteeYear;
            model.DateofCreation = committeeToEdit.DateOfCreation;
            model.CommitteeId = committeeToEdit.CommitteeId;
            ViewBag.CommitteeYear = GetCommitteeYear();
            model.Mode = "Edit";
            return PartialView("_CreateCommittee", model);
        }


        [HttpPost]
        public ActionResult EditSubCommittee(CommitteeViewModel model, string Depddlmm)
        {
            tCommittee mdl = new tCommittee();
            mdl.CommitteeId = model.CommitteeId;
            mdl.AssemblyID = model.AssemblyID;
            mdl.CommitteeTypeId = model.CommitteeTypeId;
            mdl.ParentId = 0;
            mdl.CommitteeName = model.CommitteeName;
            mdl.DateOfCreation = model.DateofCreation;
            mdl.IsActive = model.IsActive;
            mdl.AutoSchedule = model.AutoSchedule;
            mdl.Remark = model.Remark;
            mdl.CommitteeYear = Depddlmm;
            Helper.ExecuteService("Committee", "UpdateSubCommittee", mdl);

            return Json("Committee Updated Successfully", JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllMembers()
        {


            List<SBL.DomainModel.Models.Member.mMember> Members = new List<DomainModel.Models.Member.mMember>();
            SBL.DomainModel.Models.Member.mMemberAssembly model = new DomainModel.Models.Member.mMemberAssembly();
            model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            //var Members = (List<SBL.DomainModel.Models.Member.mMember>)Helper.ExecuteService("Ministry", "GetAllMembers", null);
            Members = (List<SBL.DomainModel.Models.Member.mMember>)Helper.ExecuteService("eFile", "GetMembersByAssemblyID", model);


            return Json(Members, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ShowCommitteMemberByCommitteeId(string value)
        {
            string[] p = new string[2];
            p[0] = value;
            p[1] = CurrentSession.AssemblyId;
            var Members = (List<tCommitteeMemberList>)Helper.ExecuteService("Committee", "ShowCommitteMemberByCommitteeId", p);
            return Json(Members, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllStaff()
        {
            var Staff = (List<SBL.DomainModel.Models.StaffManagement.mStaff>)Helper.ExecuteService("eFile", "GetStaffDetails", null);
            return Json(Staff, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CreateCommitteMembers(string values, string ChairmanId, string Mode, string CommitteeId, int ChairmanIdComm)
        {

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", CommitteeId));
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_DeleteCommitteeMembers", methodParameter);


            string[] words = values.Split(',');
            tCommitteeMember model = new tCommitteeMember();
            for (int count = 0; count < words.Length; count++)
            {
                var models = new tCommitteeMember
                {
                    MemberId = Convert.ToInt32(words[count]),
                    CommitteeId = Convert.ToInt32(CommitteeId),
                    //IsChairMan = false,
                    RelievedDate = DateTime.Now,
                    JoinedDate = DateTime.Now
                };

                if (ChairmanId == models.MemberId.ToString())
                {
                    models.IsReportChairMan = true;
                }
                else
                {
                    models.IsReportChairMan = false;
                }


                models.StaffID = ChairmanIdComm;
                Helper.ExecuteService("Committee", "AddCommitteeMembers", models);

            }
            return Json("Committee Member Added Successfully", JsonRequestBehavior.AllowGet);
        }




        [HttpPost]
        public JsonResult CreateCommitteeNew(CommitteeViewModel model, string Depddlmm)
        {
            tCommittee mdl = new tCommittee();
            mdl.CommitteeId = model.CommitteeId;
            mdl.AssemblyID = model.AssemblyID;
            mdl.CommitteeTypeId = model.CommitteeTypeId;
            mdl.ParentId = 0;
            mdl.CommitteeName = model.CommitteeName;
            mdl.DateOfCreation = model.DateofCreation;
            mdl.IsActive = model.IsActive;
            mdl.AutoSchedule = model.AutoSchedule;
            mdl.Remark = model.Remark;
            mdl.CommitteeYear = Depddlmm;

            //tCommittee committeeModel = model.ToDomainModel();
            //committeeModel.CommitteeYear = Depddlmm;
            Helper.ExecuteService("Committee", "CreateCommittee", mdl);
            return Json("Committee Created Successfully", JsonRequestBehavior.AllowGet);


            //return RedirectToAction("GetAllNewComittee", "Committee");
        }

        //public static tCommittee ToDomainModel(this CommitteeViewModel model)
        //{
        //	return new tCommittee
        //	{
        //		CommitteeId = Convert.ToInt32(model.CommitteeId),
        //		AssemblyID = model.AssemblyID,
        //		CommitteeTypeId = model.CommitteeTypeId,
        //		ParentId = model.ParentId,
        //		CommitteeName = model.CommitteeName,
        //		DateOfCreation = model.DateofCreation,
        //		IsActive = model.IsActive,
        //		AutoSchedule = model.AutoSchedule,
        //		Remark = model.Remark,
        //		CommitteeYear = model.CommitteeYear
        //	};
        //}

        public ActionResult CommitteeProceeding(int pageId = 1, int pageSize = 25)
        {
            COmmitteeProceeding mdl = new COmmitteeProceeding();
            // mdl.DeptID = CurrentSession.DeptID;
            string UserID = CurrentSession.UserID;
            //mdl.OfficeCode = 0;
            var Members = (List<COmmitteeProceeding>)Helper.ExecuteService("eFile", "GetCommitteeProoceedingByDept", UserID);

            ////////////////////////////Server Side Paging/////////////////////////////////// int pageId = 1, int pageSize = 25
            ViewBag.PageSize = pageSize;
            List<COmmitteeProceeding> pagedRecord = new List<COmmitteeProceeding>();
            if (pageId == 1)
            {
                pagedRecord = Members.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = Members.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(Members.Count, pageId, pageSize);
            ////////////////////////////Server Side Paging///////////////////////////////////
            return PartialView("_CommitteeProceeding", pagedRecord);
        }

        public ActionResult GetMemberPDFList(int CommitteeID)
        {
            CommitteeMembersUpload model = new CommitteeMembersUpload();
            model = (CommitteeMembersUpload)Helper.ExecuteService("Committee", "GetCommitteeMemberPDFListByCommitteeId", CommitteeID);
            return PartialView("_GetMemberPDFList", model);
        }

        public JsonResult DeleteGeneratedMemberPDF(int ID)
        {
            int res = (int)Helper.ExecuteService("Committee", "DeleteGeneratedMemberPDfByID", ID);

            if (res == 1)
            {
                return Json("Deleted Successfully", JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json("Some Problem Occured . Please try again", JsonRequestBehavior.AllowGet);

            }

        }

        public JsonResult MakeActivePDF(int ID)
        {
            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@CommUploadID", ID.ToString()));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_CommitteeMembersUpload_MakeActiveCurrent", methodParameter);
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                string res = dataSet.Tables[0].Rows[0][0].ToString();

                if (res == "Success")
                {
                    return Json("Activate Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Some problem occured. Please Try again", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("Some error1 found.Please Try again", JsonRequestBehavior.AllowGet);

        }

        public ActionResult NewCommittProceeding()
        {
            COmmitteeProceeding model = new COmmitteeProceeding();
            model.CommitteeList = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            //model.DepartmentList = deptList;
            return PartialView("_NewCommittProceeding", model);
        }

        public ActionResult saveCommittProceeding(COmmitteeProceeding committe)
        {
            Helper.ExecuteService("eFile", "saveCommitteeProceeding", committe);
            COmmitteeProceeding model = new COmmitteeProceeding();
            model.CommitteeList = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            //model.DepartmentList = deptList;
            return PartialView("_NewCommittProceeding", model);
        }

        #endregion

        #region Replace Committee Paper Laid

        public ActionResult GetOtherPaperLaidSubmitListGridCommitteeNew(int? QNumber, int? Bname)
        {
            tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

            PaperMovementModel objPaperModel = new PaperMovementModel();

            //added code venkat for dynamic 
            objPaperModel.QuestionNumber = QNumber ?? 0;
            objPaperModel.EventId = Bname ?? 0;
            //end------------------------------------
            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                tPaperLaidDepartmentModel.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", tPaperLaidDepartmentModel);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    objPaperModel.DepartmentId = item.AssociatedDepts;
                }

            }
            else
            {
                objPaperModel.DepartmentId = CurrentSession.DeptID;
            }

            /*End*/

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                tPaperLaidDepartmentModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                objPaperModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                tPaperLaidDepartmentModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                objPaperModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            var result = (List<PaperMovementModel>)Helper.ExecuteService("OtherPaperLaid", "GetSubmittedOtherPaperLaidListCommittee", objPaperModel);

            return PartialView("_GetCommitteePaperLaidList", result);

        }



        public ActionResult ShowOtherPaperLaidDetailByIDCommittee(string paperLaidId, string ReferenceNo)
        {
            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByIDCommittee", movementModel);
                movementModel.evidhanReferenceNumber = ReferenceNo;
                return PartialView("_GetOtherPaperLaidSubmittedListByIdCommittee", movementModel);
            }
            return null;
        }
        public ActionResult GetSubmittedOtherPaperLaidByIDCommittee(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByIDCommittee", movementModel);
                return PartialView("_GetSubmittedOtherPaperLaidByIDCommittee", movementModel);
            }
            return null;
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateOtherPaperLaidFileCommittee(HttpPostedFileBase file, tPaperLaidV tPaper)
        {
            if (CurrentSession.UserID != "")
            {
                tPaperLaidTemp tPaperTemp = new tPaperLaidTemp();
                PaperMovementModel obj = new PaperMovementModel();
                obj.PaperLaidId = tPaper.PaperLaidId;
                obj.actualFilePath = tPaper.FilePath;
                tPaperLaidV model = new tPaperLaidV();
                tPaperLaidV siteSetting = new tPaperLaidV();
                siteSetting = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", siteSetting);

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);

                }


                model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidFileVersionCommittee", tPaper);
                if (model != null)
                {

                    if (file == null)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        //string url = "/ePaper";

                        string FilePathTarget = "/PaperLaid/" + CurrentSession.AssemblyId + "/" + CurrentSession.SessionId + "/";
                        string directory = FileSettings.SettingValue;
                        if (!System.IO.Directory.Exists(directory + FilePathTarget))
                        {
                            System.IO.Directory.CreateDirectory(directory + FilePathTarget + "/Signed/");
                        }




                        Guid FileName = Guid.NewGuid();
                        string name = "";
                        //if (!System.IO.Directory.Exists(directory))
                        //{
                        //	System.IO.Directory.CreateDirectory(directory);
                        //}
                        if (Directory.Exists(directory))
                        {
                            string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                            string SourceFile = savedFileName[0];
                            foreach (string page in savedFileName)
                            {

                                name = Path.GetFileName(page);
                                string nameKey = Path.GetFileNameWithoutExtension(page);
                                string directory1 = Path.GetDirectoryName(page);
                                //
                                // Display the Path strings we extracted.
                                //
                                Console.WriteLine("{0}, {1}, {2}, {3}",
                                page, name, nameKey, directory1);
                                tPaperTemp.FileName = name;
                            }
                            string ext = Path.GetExtension(tPaperTemp.FileName);
                            string fileNam1 = tPaperTemp.FileName.Replace(ext, "");



                            model.Count = model.Count;

                            string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "R" + "_" + tPaper.PaperLaidId + "_V" + model.Count + ext;

                            string UrlTarget = "/PaperLaid/" + CurrentSession.AssemblyId + "/" + CurrentSession.SessionId + "/Signed/" + actualFileName;

                            string TargetPath = directory + UrlTarget;
                            //DirectoryInfo Dir = new DirectoryInfo(FilePathTarget);
                            //if (!Dir.Exists)
                            //{
                            //	Dir.Create();
                            //}

                            /////string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));

                            //string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);

                            System.IO.File.Copy(SourceFile, TargetPath, true);
                            tPaperTemp.FilePath = FilePathTarget;   // "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                            tPaperTemp.SignedFilePath = UrlTarget;
                            tPaperTemp.FileName = actualFileName;


                            tPaperTemp.PaperLaidId = tPaper.PaperLaidId;

                            tPaperTemp.Version = model.Count;

                        }


                        tPaperTemp.Version = model.Count;

                    }

                    DataSet dataSet = new DataSet();
                    var methodParameter = new List<KeyValuePair<string, string>>();

                    methodParameter.Add(new KeyValuePair<string, string>("@PaperLaidID", tPaperTemp.PaperLaidId.ToString()));
                    methodParameter.Add(new KeyValuePair<string, string>("@AssemblyId", CurrentSession.AssemblyId));
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
                    methodParameter.Add(new KeyValuePair<string, string>("@ModifiedByUserCode", CurrentSession.UserID));
                    methodParameter.Add(new KeyValuePair<string, string>("@Version", tPaperTemp.Version.ToString()));
                    methodParameter.Add(new KeyValuePair<string, string>("@FilePath", tPaperTemp.FilePath));
                    methodParameter.Add(new KeyValuePair<string, string>("@SignedFilePath", tPaperTemp.SignedFilePath));
                    methodParameter.Add(new KeyValuePair<string, string>("@FileName", tPaperTemp.FileName));

                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperLaidInHouse_Replace", methodParameter);


                }
            }


            if (Request.IsAjaxRequest())
            {
                tPaperLaidV mod = new tPaperLaidV();
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(tPaper.PaperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByIDCommittee", movementModel);
                return PartialView("/Areas/eFile/Views/eFile/_ShowPrevReplaceLaidPaper.cshtml", movementModel);


                //return Json("Uploaded Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {

            }
            return null;


        }
        #endregion


        #region Pendency Report
        public ActionResult eFilePendencyReport()
        {
            eFileAttachments model = new eFileAttachments();


            CommitteeList comList = new CommitteeList();
            comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            ViewBag.CommitteeList = comList.CommitteeView.Select(a => new SelectListItem { Text = a.CommitteeName, Value = a.CommitteeId.ToString() }).ToList();

            return PartialView("_eFilePendencyReport", model);

        }

        public ActionResult eFilePendencyReportList(string Year, string CommitteeId, string eFileId, string eFileStatus, string PaperOption, string PendingOnDate, string PaperNature, int pageId = 1, int pageSize = 25)
        {
            eFileAttachments model = new eFileAttachments();
            List<eFileAttachment> mdlList = new List<eFileAttachment>();
            ViewBag.Paper = PaperOption;
            eFileAttachment obj;
            string dt = "";
            if (string.IsNullOrEmpty(PendingOnDate))
            {
                dt = DateTime.Today.ToString("dd/MM/yyyy");
            }
            else
            {
                dt = PendingOnDate;
            }

            //if (string.IsNullOrEmpty(CommitteeId))
            //{
            //	eFileId = "All";
            //	eFileStatus = "All";
            //	CommitteeId = "All";
            //}
            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@Year", Year));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", CommitteeId));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileId", eFileId));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileStatus", eFileStatus));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperOption", PaperOption));
            methodParameter.Add(new KeyValuePair<string, string>("@PendingAsOnDate", dt));
            methodParameter.Add(new KeyValuePair<string, string>("@DeptId", CurrentSession.DeptID));
            methodParameter.Add(new KeyValuePair<string, string>("@officecode", CurrentSession.OfficeId));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNature", PaperNature));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PendencyReport", methodParameter);

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    obj = new eFileAttachment();
                    obj.eFileAttachmentId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["efileattachmentid"]);
                    obj.PaperRefNo = dataSet.Tables[0].Rows[i]["PaperRefNo"].ToString();
                    obj.CreatedDate = Convert.ToDateTime(dataSet.Tables[0].Rows[i]["CreatedDate"]);
                    obj.mode = dataSet.Tables[0].Rows[i]["PMode"].ToString();
                    obj.Title = dataSet.Tables[0].Rows[i]["Title"].ToString();
                    obj.eFilePath = dataSet.Tables[0].Rows[i]["efilepath"].ToString();
                    obj.LinkedRefNo = dataSet.Tables[0].Rows[i]["linkedrefno"].ToString();
                    obj.DepartmentId = dataSet.Tables[0].Rows[i]["DepartmentId"].ToString();
                    obj.RecvDetails = dataSet.Tables[0].Rows[i]["RecvDetails"].ToString();
                    obj.RevedOption = dataSet.Tables[0].Rows[i]["RevedOption"].ToString();
                    obj.PType = dataSet.Tables[0].Rows[i]["PType"].ToString();
                    obj.eMode = Convert.ToInt32(dataSet.Tables[0].Rows[i]["emode"]);
                    obj.SendStatus = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["sendstatus"]);
                    obj.CommitteeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["CommitteeId"]);
                    obj.DocumentTypeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["DocumentTypeId"]);
                    obj.ReportLayingHouse = Convert.ToInt32(dataSet.Tables[0].Rows[i]["ReportLayingHouse"]);
                    obj.DeptAbbr = dataSet.Tables[0].Rows[i]["deptabbr"].ToString();
                    obj.eFilePathWord = dataSet.Tables[0].Rows[i]["eFilePathWord"].ToString();
                    obj.RecvdPaperNature = dataSet.Tables[0].Rows[i]["PaperNatureType"].ToString();
                    obj.PaperTicketValue = dataSet.Tables[0].Rows[i]["PendingDay"].ToString();
                    obj.PaperNo = dataSet.Tables[0].Rows[i]["eFileNumber"].ToString();
                    obj.DispatchtoWhom = dataSet.Tables[0].Rows[i]["CommitteeName"].ToString();
                    obj.ReplyStatus = dataSet.Tables[0].Rows[i]["ReplyStatus"].ToString();
                    obj.ReplyDate = Convert.ToDateTime(dataSet.Tables[0].Rows[i]["ReplyDate"]);

                    if (obj.RevedOption == "H")
                    {
                        obj.DepartmentName = dataSet.Tables[0].Rows[i]["DeptName"].ToString() + ", Details : " + dataSet.Tables[0].Rows[i]["RecvDetails"].ToString();
                    }
                    else
                    {
                        obj.DepartmentName = "OTHER";

                    }

                    mdlList.Add(obj);
                }
            }

            model.eFileAttachemts = mdlList;
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = model.eFileAttachemts.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = model.eFileAttachemts.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(model.eFileAttachemts.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////


            return PartialView("_eFilePendencyReportList", model);

        }

        public JsonResult GeteFileByCommitteeId(int CommitteeId)
        {
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);

            return Json(eList.eFileLists, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion Committee Code By Shashi

        #region Generate Pdf Runtime and save on folder
        public JsonResult GeneratePDFCommitteeMembers()
        {

            //string Result = string.Empty;

            //Result = PDFFormat();

            //Result = HttpUtility.UrlDecode(Result);
            //Response.Clear();
            //Response.AddHeader("content-disposition", "attachment;filename=CommitteeMembers.pdf");
            //Response.Charset = "";
            //Response.ContentType = "application/pdf";
            //Response.Write(Result);
            //Response.Flush();
            //Response.End();
            ////return new EmptyResult();
            MemoryStream output = new MemoryStream();
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new EvoPdf.Margins(0, 0, 0, 0);

            EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new EvoPdf.Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = PDFFormat();

            string htmlStringToConvert = outXml;

            HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

            AddElementResult addResult = page.AddElement(htmlToPdfElement);

            byte[] pdfBytes = document1.Save();

            output.Write(pdfBytes, 0, pdfBytes.Length);

            output.Position = 0;

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;
            //string url = "/ePaper/" + "MemberTourPdf/";

            //string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            Guid id = Guid.NewGuid();
            string name = "CommitteeMembers";

            //string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + tourTitle + ".pdf");
            string path = System.IO.Path.Combine(directory, id + "_" + name + ".pdf");
            string FilePath = "/ePaper/" + id + "_" + name + ".pdf";

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@CommFileName", "CommitteeMembers"));
            methodParameter.Add(new KeyValuePair<string, string>("@CommFilePath", FilePath));
            methodParameter.Add(new KeyValuePair<string, string>("@Remark", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@CommStatus", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", CurrentSession.UserID));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", CurrentSession.UserID));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", "0"));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_CommitteeMembersUpload", methodParameter);

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                string res = dataSet.Tables[0].Rows[0][0].ToString();

                if (res == "Success")
                {
                    return Json("Pdf Uploaded Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Some problem occured. Please Try again", JsonRequestBehavior.AllowGet);
                }
            }


            return Json("PDF Generated Successfully", JsonRequestBehavior.AllowGet);
        }

        public static string PDFFormat()
        {
            SBL.DomainModel.Models.SiteSetting.SiteSettings steSett1 = new SiteSettings();
            SBL.DomainModel.Models.SiteSetting.SiteSettings steSett2 = new SiteSettings();
            SBL.DomainModel.Models.SiteSetting.SiteSettings steSett3 = new SiteSettings();
            var FileSettings = (List<SBL.DomainModel.Models.SiteSetting.SiteSettings>)Helper.ExecuteService("SiteSetting", "GetCommitteeMemberPDFSetting", null);

            steSett1 = FileSettings.Single(s => s.SettingValue == "Heading1");
            steSett2 = FileSettings.Single(s => s.SettingValue == "Heading2");
            steSett3 = FileSettings.Single(s => s.SettingValue == "Heading3");

            StringBuilder ReportList = new StringBuilder();
            ReportList.Append("<body>");
            ReportList.Append("<div style='font-family:Calibri;font-size:17px;width:96%;'>");
            ReportList.Append("<center>( हिमाचल प्रदेश सरकार के असाधारण राजपत्र में प्रकाशित किया जायेगा )</center>");
            //ReportList.Append("<center><h2>हिमाचल प्रदेश बारहवीं विधान सभा </h2></center>");
            ReportList.Append("<center><h1>" + steSett1.Description + " </h1></center>");
            ReportList.Append("<center><h4>अधिसूचना </h4></center>");
            //	ReportList.Append("<center><h5>सं0: वि0स0 -विधायन -सिमित गठन/1-13/2013   दिनांक , 30 अप्रैल, 2015 </h5></center>");
            ReportList.Append("<center><h3> " + steSett2.Description + "</h3></center>");
            //ReportList.Append("<center><p style='width:60%;'>हिमाचल प्रदेश विधान  सभा की प्रक्रिया एवं कार्य संचालन नियमावली , 1973 के नियम , 209 और 211 के अनुसरण में माननीय अध्यक्ष महोदय ने वर्ष 2015-16 के लिए गठित सदन की निम्न समितियों हेतु सदस्यों को सभापति तथा सदस्य नामांकित किया है:-</p></center>");

            ReportList.Append("<center><p style='width:60%;'>" + steSett3.Description + "</p></center>");

            ReportList.Append("<br/><br/>");
            //GetAllCommittee
            List<CommitteesListView> modelList = new List<CommitteesListView>();
            modelList = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommittee", null);
            int MNo = 0;
            foreach (var item in modelList)
            {
                MNo++;
                List<tCommitteeMemberList> mdlList = new List<tCommitteeMemberList>();
                mdlList = (List<tCommitteeMemberList>)Helper.ExecuteService("Committee", "GetAllMembersByCommitteeIdPDF", item.CommitteeId.ToString());


                ReportList.Append("<center><h3>" + MNo + " <u>" + item.CommitteeNameLocal + " </u> </h3></center>");


                int No = 0;
                ReportList.Append("<center><table style='font-family:Calibri;font-size:19px;'>");
                foreach (var item1 in mdlList)
                {
                    No++;
                    if (item1.IsChairMan == true)
                    {
                        ReportList.Append("<tr>");
                        ReportList.Append("<td> <b>" + No + ".&nbsp;&nbsp;</b></td> <td><b>" + item1.CommitteMemberName + "</b>&nbsp;&nbsp; </td> <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td><td><b> सभापति</b></td>");
                        ReportList.Append("</tr>");
                    }
                    else
                    {
                        ReportList.Append("<tr>");
                        ReportList.Append("<td>" + No + ".&nbsp;&nbsp;</td> <td>" + item1.CommitteMemberName + "&nbsp;&nbsp; </td> <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td><td> सदस्य </td>");
                        ReportList.Append("</tr>");
                    }

                }
                ReportList.Append("</table></center>");
            }


            ReportList.Append("<span style='float:right;'><b>सचिव,</b></span> <br/>");
            ReportList.Append("<span style='float:right;'>हि0 प्र0 विधान सभा</span>");
            ReportList.Append("</div>");
            ReportList.Append("</body>");
            return ReportList.ToString();
        }

        public ActionResult GetCommitteePDFSettingsList()
        {
            List<SiteSettings> mdlList = new List<SiteSettings>();
            mdlList = (List<SBL.DomainModel.Models.SiteSetting.SiteSettings>)Helper.ExecuteService("SiteSetting", "GetCommitteeMemberPDFSetting", null);
            return PartialView("_CommitteeMemberPDFSettingsList", mdlList);
        }
        public ActionResult GetEditSiteSetting(int SettingId, string SettingName, string SettingValue, string Description)
        {
            SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models.SiteSettingViewModel mdl = new SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models.SiteSettingViewModel();
            mdl.SettingId = SettingId;
            mdl.SettingName = SettingName;
            mdl.SettingValue = SettingValue;
            mdl.Description = Description;

            mdl.Mode = "Update";
            return PartialView("_CommitteeMemberPDFSettings", mdl);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveSiteSettingsCommittee(SiteSettingViewModel model)
        {

            try
            {

                if (ModelState.IsValid)
                {

                    SiteSettings mdlSett = new SiteSettings();
                    mdlSett.SettingId = model.SettingId;
                    mdlSett.SettingName = model.SettingName;
                    mdlSett.SettingValue = model.SettingValue;
                    mdlSett.Description = model.Description;
                    mdlSett.IsActive = true;
                    mdlSett.CreatedBy = CurrentSession.UserName;
                    mdlSett.ModifiedBy = CurrentSession.UserName;

                    //if (model.Mode == "Add")
                    //{

                    //	Helper.ExecuteService("SiteSetting", "CreateSiteSettings", mdlSett);
                    //}
                    //else
                    //{

                    Helper.ExecuteService("SiteSetting", "UpdateSiteSettings", mdlSett);
                    //}
                    return Json("Settings Updated Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Error found", JsonRequestBehavior.AllowGet);
                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("There is a Error While Saving Site Setting Details", JsonRequestBehavior.AllowGet);

            }
        }

        #endregion

        //ashwani
        public ActionResult GetValuesByRefID(string _ReferenceNo)
        {
            eFileAttachment documents = new eFileAttachment();
            var _ObjeFileAttachment = (eFileAttachment)Helper.ExecuteService("eFile", "GetValuesByRefID", _ReferenceNo);
            return Json(_ObjeFileAttachment, JsonRequestBehavior.AllowGet);
        }


        #region UploadFileAsyn
        [HttpPost]
        public ContentResult UploadFiles()
        {
            string url = "~/ePaper/TempFile";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/ePaper/TempFile"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }

        public JsonResult RemovePDFFiles()
        {
            string url = "~/ePaper/TempFile";
            string directory = Server.MapPath(url);

            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ContentResult UploadFilesWord()
        {
            string url = "~/ePaper/FileWord";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/ePaper/FileWord"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }

        [HttpPost]
        public ContentResult UploadFilesWordNew()
        {
            string url = "~/ePaper/";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/ePaper/"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }

        public JsonResult RemovePDFFilesWord()
        {
            string url = "~/ePaper/FileWord";
            string directory = Server.MapPath(url);

            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveWordFilesNew()
        {
            string url = "~/ePaper/";
            string directory = Server.MapPath(url);

            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        #endregion


        public ActionResult GeteBookData(string URL)
        {
            return PartialView("_eBookByID", URL);
        }

        public ActionResult _eFileViewByIDAttached(eFileViewModel model)
        {
            eFileLogin();
            try
            {
                eFileViewModel model2 = new eFileViewModel();

                //model2.DepartmentName = model.DepartmentName;
                //line for office
                model2.eFileNumber = model.eFileNumber;
                model2.eFileSubject = model.eFileSubject;
                model2.eFileID = model.eFileID;
                model2.eFileDetail = (SBL.DomainModel.Models.eFile.eFile)Helper.ExecuteService("eFile", "GeteFileDetailsByFileId", model.eFileID);
                model2.DepartmentName = model2.eFileDetail.DepartmentName;
                model2.NewDescription = model2.eFileDetail.NewDescription;
                model2.DepartmentId = model2.eFileDetail.DepartmentId;
                model2.Officecode = model2.eFileDetail.Officecode;
                ViewBag.Attached = "Y";

                //var ListAss = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                //ViewBag.AssemblyList = new SelectList(ListAss, "AssemblyCode", "AssemblyName", null);


                return View("_eFileViewByID", model2);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public PartialViewResult GetPaperListByRefNo(string PaperRefNo)
        {
            eFileLogin();
            eFileAttachments model = new eFileAttachments();

            string RefNo = PaperRefNo; //"HPD0001";

            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetPaperDetailsByRefNo", RefNo);
            model.eFileAttachemts = ListModel;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }

            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            model.eFileLists = eList.eFileLists;

            return PartialView("_eFileViewByRefNo", model);
        }


        #region ServerSidePaging
        public PartialViewResult GetSendPaperByPaging(int pageId = 1, int pageSize = 25, int PapersId = 1)
        {


            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string PaperType = PapersId.ToString();
            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = currtBId;
            str[5] = PaperType;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            // ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendPaperDetailsByDeptId", str);
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendList", str);
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            eFileAttachments model = new eFileAttachments();
            model.eFileAttachemts = pagedRecord;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string[] str1 = new string[4];
            string[] strngBID = new string[2];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);

            str1[3] = value[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);
            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }

            model.eFileLists = eList.eFileLists;


            return PartialView("_sendPaperList", model);
        }
        public PartialViewResult GetSavedPaperByPaging(int pageId = 1, int pageSize = 25, int PapersId = 1)
        {
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            //  string Year = YearId.ToString();
            string papers = PapersId.ToString();
            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = currtBId;
            str[5] = papers;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            eFileAttachments model = new eFileAttachments();
            model.eFileAttachemts = pagedRecord;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string[] str1 = new string[4];
            string[] strngBID = new string[2];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);

            str1[3] = value[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);
            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileLists = eList.eFileLists;

            return PartialView("_receivePaperList", model);
        }
        public PartialViewResult GeteFileByPaging(int pageId = 1, int pageSize = 25)
        {
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            eFileAttachments model = new eFileAttachments();
            eList.eFileLists = pagedRecord;


            return PartialView("_eFileListViewAll", eList);
        }
        public PartialViewResult GeteFileByPagingCommittee(int CommitteeId, int pageId = 1, int pageSize = 25)
        {

            tCommittee mdl = new tCommittee();
            mdl = (tCommittee)Helper.ExecuteService("Committee", "GetCommitteeDetailsByCommitteeId", CommitteeId);

            ViewBag.CommName = mdl.CommitteeName;
            ViewBag.CommYear = mdl.CommitteeYear;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            eFileAttachments model = new eFileAttachments();
            eList.eFileLists = pagedRecord;
            ViewBag.Mode = "Committee";


            return PartialView("_eFileListViewAll", eList);
        }
        public PartialViewResult GeteFileByPagingAttach(int pageId = 1, int pageSize = 25)
        {
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            eFileAttachments model = new eFileAttachments();
            eList.eFileLists = pagedRecord;
            ViewBag.Mode = "Committee";
            ViewBag.CommAttach = "CommitteeAttach";

            return PartialView("_eFileListViewAllCommitteeAttach", eList);
        }

        public ActionResult GetCommitteeProceedingPaging(int pageId = 1, int pageSize = 25)
        {
            COmmitteeProceeding mdl = new COmmitteeProceeding();
            // mdl.DeptID = CurrentSession.DeptID;
            string UserID = CurrentSession.UserID;
            //mdl.OfficeCode = 0;
            var Members = (List<COmmitteeProceeding>)Helper.ExecuteService("eFile", "GetCommitteeProoceedingByDept", UserID);

            ////////////////////////////Server Side Paging/////////////////////////////////// int pageId = 1, int pageSize = 25
            ViewBag.PageSize = pageSize;
            List<COmmitteeProceeding> pagedRecord = new List<COmmitteeProceeding>();
            if (pageId == 1)
            {
                pagedRecord = Members.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = Members.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(Members.Count, pageId, pageSize);
            ////////////////////////////Server Side Paging///////////////////////////////////
            return PartialView("_CommitteeProceeding", pagedRecord);
        }
        #endregion

        public JsonResult GetTreeViewJSON_DATA()
        {
            TreeViewStructure objTreeViewStructure = new TreeViewStructure();
            var Result = (List<TreeViewStructure>)Helper.ExecuteService("eFile", "GetTreeViewStructure", null);
            List<MyClass> items = new List<MyClass>();
            foreach (var item in Result)
            {
                items.Add(new MyClass(item.Name, Convert.ToInt32(item.AadharID), Convert.ToInt32(item.Parent_AadharID)));
            }

            items.ForEach(item => item.children = items.Where(child => child.ParentID == item.ID)
                                           .ToList());
            MyClass topItems = items.Where(item => item.ParentID == 0).FirstOrDefault();
            //List<MyClass> topItems = items.Where(item => item.ParentID == 0).ToList();
            return new JsonResult { Data = topItems, ContentType = "Json", JsonRequestBehavior = JsonRequestBehavior.AllowGet };


            //JavaScriptSerializer jss = new JavaScriptSerializer();
            // string output = jss.Serialize(topItems1);
            //output.Property("field2").Remove();

        }

        public ActionResult GetTreeViewStructure()
        {
            return PartialView("_TreeViewStructure");
        }



        [HttpPost]
        public ContentResult UploadPdfFiles()
        {

            string url = "~/EFileCommettee/Pdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPrintingTemp>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                string s = ConvertBytesToMegabytes(hpf.ContentLength).ToString("0.00");

                // Convert bytes to megabytes.
                Console.WriteLine("{0} bytes = {1} megabytes",
                    hpf.ContentLength,
                    s);
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/EFileCommettee/Pdf";
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/EFileCommettee/Pdf"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPrintingTemp()
                {
                    Name = hpf.FileName,
                    // Length = hpf.ContentLength,
                    MB = s,
                    Type = hpf.ContentType,

                });
                //int val = 176763 ;
                //
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} MB", r[0].MB) + "\"}", "application/json");
        }

        static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        public JsonResult RemovePDFFilesCom()
        {
            string url = "~/EFileCommettee/Pdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ContentResult DocUploadFiles()
        {

            string url = "~/EFileCommettee/Doc";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPrintingTemp>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                string s = ConvertBytesToMegabytes(hpf.ContentLength).ToString("0.00");

                // Convert bytes to megabytes.
                Console.WriteLine("{0} bytes = {1} megabytes",
                    hpf.ContentLength,
                    s);
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/EFileCommettee/Doc";
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/EFileCommettee/Doc"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPrintingTemp()
                {
                    Name = hpf.FileName,
                    // Length = hpf.ContentLength,
                    MB = s,
                    Type = hpf.ContentType,

                });
                //int val = 176763 ;
                //
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} MB", r[0].MB) + "\"}", "application/json");
        }

        public JsonResult RemoveDocFiles()
        {
            string url = "~/EFileCommettee/Doc";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        //Sameer Code
        public JsonResult DiaryEntryPaper(int EfileId)
        {
            eFileAttachment Update = new eFileAttachment();
            Update.eFileAttachmentId = EfileId;
            Update.Year = DateTime.Now.Year.ToString();
            Update = (eFileAttachment)Helper.ExecuteService("eFile", "CheckUpdateDiary", Update);
            // return PartialView("/Areas/eFile/Views/eFile/_MethodReceive.cshtml");
            // return RedirectToAction("GetReceivePaperList");
            return Json(Update, JsonRequestBehavior.AllowGet);
        }


        //public ActionResult BranchModel(int Id, string Type)
        //{
        //    mBranches modelBranch = new mBranches();
        //    modelBranch.Type = Type;
        //    modelBranch.ID = Id;
        //    modelBranch.BranchList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "BranchsForAasign", modelBranch);
        //    modelBranch.BranchId = (int)Helper.ExecuteService("eFile", "GetBranchId", modelBranch);

        //    return PartialView("_GetBranch", modelBranch);

        //}

        public ActionResult BranchModel(int Id, string Type)
        {
            mBranches modelBranch = new mBranches();
            mStaff model = new mStaff();
            modelBranch.Type = Type;
            modelBranch.ID = Id;
            // modelBranch.CID = CID;
            modelBranch.BranchList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "BranchsForAasign", modelBranch);
            modelBranch.BranchId = (int)Helper.ExecuteService("eFile", "GetBranchId", modelBranch);
            model.BranchIDs = modelBranch.BranchId.ToString();
            modelBranch.EmpList = Helper.ExecuteService("eFile", "GetEmployeeByBranch", model) as List<mStaff>;
            // modelBranch.AadharID = Convert.ToString(0);
            //  modelBranch.StaffName = "Select Employee";
            return PartialView("_GetBranch", modelBranch);

        }

        public JsonResult GetEmployeeByBranch(int BranchId)
        {
            mBranches modelBranch = new mBranches();
            mStaff model = new mStaff();
            model.BranchIDs = BranchId.ToString();
            modelBranch.EmpList = Helper.ExecuteService("eFile", "GetEmployeeByBranch", model) as List<mStaff>;
            //  modelBranch.AadharID = Convert.ToString(0);
            // modelBranch.StaffName = "Select Employee";
            return Json(modelBranch.EmpList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AssignPaperMovement(Updatevalue val)
        {
            mBranches Update = new mBranches();
            Update.ID = val.EfileId;
            Update.efileID = val.RefileId;
            long EID = Convert.ToInt64(val.EmployeeAid);

            if (EID == 0)
            {
                Update.AadharID = null;
            }

            Update.AadharID = val.EmployeeAid;
            Update.AadharID = val.EmployeeAid;
            Update.Remark = val.Remark;
            Update.BranchId = val.BranchId;
            Update.ModifiedDate = DateTime.Now;
            Update.AssignfrmAadharId = CurrentSession.AadharId;
            Update = (mBranches)Helper.ExecuteService("eFile", "UpdateMovements", Update);
            return Json(Update, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BranchInSession(string BranchId)
        {
            CurrentSession.BranchId = BranchId;
            //eFileAttachment Update = new eFileAttachment();
            //Update.eFileAttachmentId = EfileId;
            //Update.Year = DateTime.Now.Year.ToString();
            //Update = (eFileAttachment)Helper.ExecuteService("eFile", "CheckUpdateDiary", Update);
            // return PartialView("/Areas/eFile/Views/eFile/_MethodReceive.cshtml");
            // return RedirectToAction("GetReceivePaperList");
            // return RedirectToAction("Index", "VidhanSabhaDept", new { area = "VidhanSabhaDepartment" });
            return Json(JsonRequestBehavior.AllowGet);
        }

        public ActionResult MovementList(string EfileId)
        {
            tMovementFile Update = new tMovementFile();
            Update.ID = Convert.ToInt32(EfileId);
            Update = (tMovementFile)Helper.ExecuteService("eFile", "MovementList", Update);
            return PartialView("_MovementList", Update);

        }

        [HttpPost]
        public JsonResult DraftPapers(string eFileAttIds)
        {

            eFileAttachment model = new eFileAttachment();
            string[] d = eFileAttIds.Split(',');
            int[] brnds = new int[d.Length - 1];

            for (int i = 0; i < d.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(d[i].ToString()))
                {
                    brnds[i] = Convert.ToInt32(d[i]);
                }
            }

            model.eFileAttachmentIds = brnds;
            model.CurrentEmpAadharID = CurrentSession.AadharId;
            int res = (int)Helper.ExecuteService("eFile", "Draftpapers", model);

            if (res == 0)
            {
                return Json("Paper Added in Draft List Successfully", JsonRequestBehavior.AllowGet);
            }
            else if (res == 2)
            {
                return Json("File Already Exist in Draft List", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DraftPapersSent(string eFileAttIds)
        {

            eFileAttachment model = new eFileAttachment();
            string[] d = eFileAttIds.Split(',');
            int[] brnds = new int[d.Length - 1];

            for (int i = 0; i < d.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(d[i].ToString()))
                {
                    brnds[i] = Convert.ToInt32(d[i]);
                }
            }

            model.eFileAttachmentIds = brnds;
            model.CurrentEmpAadharID = CurrentSession.AadharId;
            int res = (int)Helper.ExecuteService("eFile", "DraftpapersSent", model);

            if (res == 0)
            {
                return Json("Paper Added in Draft List Successfully", JsonRequestBehavior.AllowGet);
            }
            else if (res == 2)
            {
                return Json("File Already Exist in Draft List", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }


        public PartialViewResult GetDraftPaperList(int pageId = 1, int pageSize = 25, int PapersId = 1)
        {
            CurrentSession.SetSessionAlive = null;
            eFileAttachments model = new eFileAttachments();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;//
            string design = CurrentSession.Designation;

            string DepartmentId = CurrentSession.DeptID;

            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string paperType = PapersId.ToString();
            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = currtBId;
            str[4] = design;
            str[5] = paperType;
            model.CurrentSessionAId = AadharId;
            model.Sessiondesign = design;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", str);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            //List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            //if (pageId == 1)
            //{
            //    pagedRecord = ListModel.Take(pageSize).ToList();
            //}
            //else
            //{
            //    int r = (pageId - 1) * pageSize;
            //    pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            //}

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string[] str1 = new string[4];
            string[] strngBID = new string[2];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str1[3] = value[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);
            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "ComSendButtonDisp", null);
            // var AuthorizeEmp = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFile", eFile);
            model.CheckAuthorize = (int)Helper.ExecuteService("eFile", "GetAuthorizeEmp", model);
            //   model.CheckAuthorize =  AuthorizeEmp;
            model.ButtonShow = NewsSettings.SettingValue;
            model.eFileAttachemts = ListModel;//pagedRecord
            model.eFileLists = eList.eFileLists;

            #region For Send List Count
            string[] strS = new string[6];
            strS[0] = DepartmentId;
            strS[1] = Officecode;
            strS[2] = AadharId;
            strS[3] = currtBId;
            strS[4] = "5";
            strS[5] = "1";
            List<eFileAttachment> ListModelForSent = new List<eFileAttachment>();
            ListModelForSent = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendList", strS);
            model.SentPaperCount = ListModelForSent.Count;
            #endregion

            return PartialView("DraftList", model);
        }

        public PartialViewResult GetDraftPaperByPaging(int pageId = 1, int pageSize = 25, int PapersId = 1)
        {
            eFileAttachments model = new eFileAttachments();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            string design = CurrentSession.Designation;

            string DepartmentId = CurrentSession.DeptID;

            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string paperType = PapersId.ToString();
            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = currtBId;
            str[4] = design;
            str[5] = paperType;
            model.CurrentSessionAId = AadharId;
            model.Sessiondesign = design;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", str);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string[] str1 = new string[4];
            string[] strngBID = new string[2];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);

            str1[3] = value[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);
            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileAttachemts = pagedRecord;
            model.eFileLists = eList.eFileLists;
            return PartialView("DraftList", model);
        }


        [HttpPost]
        public JsonResult DeleteDraftPapers(string eFileAttId)
        {

            eFileAttachment model = new eFileAttachment();
            model.eFileAttachmentId = Convert.ToInt32(eFileAttId);

            int res = (int)Helper.ExecuteService("eFile", "DeleteDraftPapers", model);

            if (res == 0)
            {
                return Json("Deleted paper Sucessfully", JsonRequestBehavior.AllowGet);
            }

            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult DraftPaperInfo(string EfileId)
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();
            documents.DraftId = Convert.ToInt32(EfileId);
            documents.PaperRefNo = GetRefNo();
            string Dept = CurrentSession.DeptID;
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            ///Get eFilePaperNature
            if (Dept == "HPD0001")
            {
                var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList_Receive", null);
                ViewBag.eFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);
            }
            else
            {
                var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList_ReceiveDept", null);
                ViewBag.eFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);
            }


            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);
            ///Get Item Type
            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetFileItemList", null);
            ViewBag.FileItem = new SelectList(_ListItems, "ID", "ItemTypeName", null);

            //For Department Users
            mDepartment mdep = new mDepartment();
            mdep.deptId = CurrentSession.DeptID;
            documents.mDepartment = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
            documents = (eFileAttachment)Helper.ExecuteService("eFile", "GetDataList", documents);

            //to get comm id with branch id
            string[] strngBID = new string[2];
            strngBID[0] = documents.CurrentBranchId.ToString();
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            documents.CommitteeId = Convert.ToInt32(value[0]);
            documents.ValueType = "Draft";
            var _Commitee = (List<CommitteeSearchModel>)Helper.ExecuteService("eFile", "GetCommittees", null);
            ViewBag.CommiteeName = new SelectList(_Commitee, "CommitteeId", "CommitteeName", null);
            return PartialView("_DraftPaperInfo", documents);
        }

        public ActionResult GetToPopUp(int Id)
        {

            mBranches documents = new mBranches();
            documents.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            documents.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            documents.DeptList = Helper.ExecuteService("eFile", "GetDepartment", documents) as List<mDepartment>;
            documents.MemberList = Helper.ExecuteService("eFile", "GetMembers", documents) as List<HouseComModel>;
            documents.OtherThanList = Helper.ExecuteService("eFile", "GetOtherThanHP", documents) as List<HouseComModel>;
            documents.Secretary = true;
            documents.HOD = true;
            return PartialView("_GetToPop", documents);

        }

        public ActionResult GetCCPopUp(int Id)
        {

            mBranches documents = new mBranches();
            documents.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            documents.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            documents.DeptList = Helper.ExecuteService("eFile", "GetDepartment", documents) as List<mDepartment>;
            documents.MemberList = Helper.ExecuteService("eFile", "GetMembers", documents) as List<HouseComModel>;
            documents.OtherThanList = Helper.ExecuteService("eFile", "GetOtherThanHP", documents) as List<HouseComModel>;
            documents.Secretary = true;
            documents.HOD = true;
            return PartialView("_GetCCPop", documents);

        }

        public ActionResult Test(string Path, string eFileattachedId)
        {
            try
            {

                tMovementFile Update = new tMovementFile();
                Update.ID = Convert.ToInt32(eFileattachedId);
                Update.PDFPath = Path;
                Update = (tMovementFile)Helper.ExecuteService("eFile", "MovementList", Update);
                return View(Update);
            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return View();
        }
        public ActionResult ForNotAuthorize()
        {

            return View();
        }
        public ActionResult Test_movement(string Path, string eFileattachedId)
        {
            tMovementFile Update = new tMovementFile();
            try
            {

                // tMovementFile Update = new tMovementFile();
                Update.ID = Convert.ToInt32(eFileattachedId);
                Update.PDFPath = Path;
                Update = (tMovementFile)Helper.ExecuteService("eFile", "MovementList", Update);
                return PartialView("MovementList", Update);
            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return PartialView("MovementList", Update);
        }
        public ActionResult Test_movementPDF(string Path, string eFileattachedId)
        {
            tMovementFile Update = new tMovementFile();
            try
            {

                // tMovementFile Update = new tMovementFile();
                Update.ID = Convert.ToInt32(eFileattachedId);
                Update.PDFPath = Path;
                //  Update = (tMovementFile)Helper.ExecuteService("eFile", "MovementList", Update);
                return PartialView("_AuntheticPDf", Update);
            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return PartialView("_AuntheticPDf", Update);
        }
        public ActionResult ForNotAuthorize_New()
        {

            return PartialView("_ForNotAuthorizeNew");
        }


        public JsonResult AssignToHidden(string DeptIds, string MemId, string OtherId)
        {

            ///  mBranches model = new mBranches();
            eFileAttachment model = new eFileAttachment();
            model.ToDepIds = DeptIds;
            model.ToMemIds = MemId;
            model.ToOtherIds = OtherId;
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ToModelPop(string To, string CC)
        {
            mBranches modelBranch = new mBranches();
            modelBranch.BranchName = To;
            modelBranch.deptnameCC = CC;
            return PartialView("_GetToList", modelBranch);

        }

        //public ActionResult CreateNotingPop(int Id, int ReId)
        //{
        //    mBranches modelBranch = new mBranches();
        //    modelBranch.ID = Id;
        //    modelBranch.efileID = ReId;
        //    modelBranch.BranchList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "BranchsForAasign", modelBranch);
        //    modelBranch.BranchId = (int)Helper.ExecuteService("eFile", "GetBranchIdDraftList", modelBranch);
        //    return PartialView("_GetNotingPop", modelBranch);

        //}
        public ActionResult CreateNotingPop(int Id, int ReId)
        {
            mBranches modelBranch = new mBranches();
            mStaff model = new mStaff();
            modelBranch.ID = Id;
            modelBranch.efileID = ReId;
            var IsSinglFilerequired = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "IsSingleFileMovement", null);
            if (IsSinglFilerequired.SettingValue == "True")
            {
                //chk is file already aattached with another draft
                int existdraftid = (int)Helper.ExecuteService("eFile", "check_draftIdinEfile", modelBranch);
#pragma warning disable CS0472 // The result of the expression is always 'false' since a value of type 'int' is never equal to 'null' of type 'int?'
                if (existdraftid == null || existdraftid == 0 || existdraftid == Id)
#pragma warning restore CS0472 // The result of the expression is always 'false' since a value of type 'int' is never equal to 'null' of type 'int?'
                {
                    modelBranch.BranchList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "BranchsForAasign", modelBranch);
                    int branchid = (int)Helper.ExecuteService("eFile", "GetBranchIdDraftList", modelBranch);
                    modelBranch.BranchId = branchid;
                    model.BranchIDs = branchid.ToString();
                    modelBranch.EmpList = Helper.ExecuteService("eFile", "GetEmployeeByBranch", model) as List<mStaff>;
                    modelBranch.AadharID = Convert.ToString(0);
                    modelBranch.StaffName = "Select Employee";
                    modelBranch.Exist_draftId = existdraftid;
                    return PartialView("_GetNotingPop", modelBranch);
                }
                else if (existdraftid == -1)
                {
                    modelBranch.Exist_draftId = -1;
                    return PartialView("_GetNotingPop", modelBranch);
                }
                else
                {
                    modelBranch.Exist_draftId = -2; //with other draft
                    return PartialView("_GetNotingPop", modelBranch);
                }

            }
            else
            {
                modelBranch.Exist_draftId = 0;
                modelBranch.BranchList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "BranchsForAasign", modelBranch);
                int branchid = (int)Helper.ExecuteService("eFile", "GetBranchIdDraftList", modelBranch);
                modelBranch.BranchId = branchid;
                model.BranchIDs = branchid.ToString();
                modelBranch.EmpList = Helper.ExecuteService("eFile", "GetEmployeeByBranch", model) as List<mStaff>;
                modelBranch.AadharID = Convert.ToString(0);
                modelBranch.StaffName = "Select Employee";
                return PartialView("_GetNotingPop", modelBranch);

            }

        }

        public JsonResult AssignPaperNoting(Updatevalue val)
        {
            mBranches Update = new mBranches();
            Update.ID = val.EfileId;
            Update.efileID = val.RefileId;
            long EID = Convert.ToInt64(val.EmployeeAid);

            if (EID == 0)
            {
                Update.AadharID = null;
            }

            Update.AadharID = val.EmployeeAid;
            Update.Remark = val.Remark;
            Update.BranchId = val.BranchId;
            Update.ModifiedDate = DateTime.Now;
            Update.AssignfrmAadharId = CurrentSession.AadharId;
            Update = (mBranches)Helper.ExecuteService("eFile", "UpdateNoting", Update);
            return Json(Update, JsonRequestBehavior.AllowGet);
        }

        public ActionResult NotingList(string EfileId)
        {
            tMovementFile Update = new tMovementFile();
            Update.ID = Convert.ToInt32(EfileId);
            Update = (tMovementFile)Helper.ExecuteService("eFile", "NotingList", Update);
            return PartialView("_NotingList", Update);

        }

        public ActionResult DraftMovementTest(string Path, string eFileattachedId)
        {
            try
            {

                tMovementFile Update = new tMovementFile();
                Update.ID = Convert.ToInt32(eFileattachedId);
                Update.PDFPath = Path;
                Update.AssignfrmAadharId = CurrentSession.AadharId;
                Update = (tMovementFile)Helper.ExecuteService("eFile", "DraftMovementList", Update);
                if (Update.Value == true)
                {
                    return PartialView("Test", Update);
                }
                else if (Update.Value == false)
                {
                    return PartialView("ForNotAuthorize");
                }
            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return View();
        }


        public ActionResult DOCDraftMovement(string Path, string eFileattachedId)
        {
            try
            {

                tMovementFile Update = new tMovementFile();
                Update.ID = Convert.ToInt32(eFileattachedId);
                Update.PDFPath = Path;
                Update.AssignfrmAadharId = CurrentSession.AadharId;
                Update = (tMovementFile)Helper.ExecuteService("eFile", "DraftMovementList", Update);
                if (Update.Value == true)
                {
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                    Update.CompPath = FileSettings.SettingValue;
                    return Json(Update, JsonRequestBehavior.AllowGet);

                }

                else if (Update.Value == false)
                {
                    return PartialView("ForNotAuthorize");
                }
            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return View();
        }


        public ActionResult NotingMovement(string Path, string eFileattachedId)
        {
            try
            {

                tMovementFile Update = new tMovementFile();
                Update.ID = Convert.ToInt32(eFileattachedId);
                Update.PDFPath = Path;
                Update.AssignfrmAadharId = CurrentSession.AadharId;
                Update = (tMovementFile)Helper.ExecuteService("eFile", "NotingMovementList", Update);
                if (Update.Value == true)
                {
                    return PartialView("_NotingWithPdf", Update);
                }
                else if (Update.Value == false)
                {
                    return PartialView("ForNotAuthorize");
                }
            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return View();
        }

        public ActionResult SendToDeptPop(int Id)
        {
            mBranches modelBranch = new mBranches();
            modelBranch.ID = Id;
            return PartialView("_SendPopUp", modelBranch);

        }


        public JsonResult SendToDeptEntry(int Id, bool Approvefile, bool SignDraftpaper)
        {
            try
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog(Convert.ToString(Id));
                eFileAttachment model = new eFileAttachment();
                SBL.DomainModel.Models.eFile.eFile model1 = new DomainModel.Models.eFile.eFile();
                model.eFileAttachmentId = Id;
                model = (eFileAttachment)Helper.ExecuteService("eFile", "GetDataPathsforMerge", model);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                var FileLocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                var IsMsgRequired = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "IsMsgRequiredInHouseCommittee", null);
                var IsMailRequired = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "IsMailRequiredInHouseCommittee", null);
                string filepathDoc = FileLocation.SettingValue + model.eFilePathWordN;
                string strName = Path.GetFileName(filepathDoc).ToLower();
                string filepathPDF = strName;
                string docpath = @" C:\inetpub\e_Vidhan\FileStructure\ePaper\" + strName;
                string strTemp = Path.GetExtension(filepathDoc).ToLower();

                if (strTemp == ".doc" || strTemp == ".docx")
                {
                    filepathPDF = Path.ChangeExtension(filepathPDF, "pdf");
                }

                string outputPath = @" C:\inetpub\e_Vidhan\FileStructure\ePaper\" + filepathPDF;
                string textLine1 = CurrentSession.UserName;

                model.SignFilePath = CurrentSession.UserID.GetSignPath();

                model1.AadharID = CurrentSession.AadharId.ToString();
                string DesigName = (string)Helper.ExecuteService("eFile", "GetDesigNameByAadharId", model1);
                string imageFileLoc = Server.MapPath(model.SignFilePath);


                Spire.Doc.Document doc = new Spire.Doc.Document();

                doc.LoadFromFile(docpath);
                Spire.Doc.Section section = doc.Sections[0];
                Spire.Doc.HeaderFooter footer = section.HeadersFooters.Footer;
                Spire.Doc.Documents.Paragraph footerParagraph = footer.AddParagraph();
                DocPicture footerimage = footerParagraph.AppendPicture(Image.FromFile(imageFileLoc));//    (@"E:\Logo\logo.jpeg"));
                footerimage.Width = 20;
                footerimage.Height = 20;
                footerimage.HorizontalPosition = 50.0F;
                footerParagraph.AppendText("Digitially Signed by: " + CurrentSession.UserName + " ( " + DesigName + " ) On " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt"));
                //Append Picture and Text for Footer Paragraph

                // DocPicture footerimage = footerParagraph.AppendPicture(Image.FromFile(imageFileLoc));//    (@"E:\Logo\logo.jpeg"));
                //footerimage.Width = 20;
                // footerimage.Height = 20;
                ParagraphStyle style = new ParagraphStyle(doc);
                style.Name = "Footer Style";
                style.CharacterFormat.FontName = "Calibri";
                style.CharacterFormat.FontSize = 8;
                style.CharacterFormat.TextColor = Color.Blue;
                doc.Styles.Add(style);
                footerParagraph.ApplyStyle(style.Name);

                footerParagraph.Format.HorizontalAlignment
                    = Spire.Doc.Documents.HorizontalAlignment.Center;

                //Border
                footerParagraph.Format.Borders.Top.BorderType
                    = Spire.Doc.Documents.BorderStyle.Single;
                footerParagraph.Format.Borders.Top.Space = 0.05F;
                doc.SaveToFile(outputPath, FileFormat.PDF);
                string CompleteFile = outputPath;
                model.eFilePath = "/ePaper/" + filepathPDF;

                string[] strngBID = new string[2];
                if (CurrentSession.BranchId == "" || CurrentSession.BranchId == null)
                {
                    CurrentSession.BranchId = Convert.ToString(model.CurrentBranchId);
                }
                strngBID[0] = CurrentSession.BranchId;
                var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
                model.CommitteeId = Convert.ToInt16(value[0]);
                model.CommitteeName = value[1];
                model.IpAddress = (string)Helper.ExecuteService("eFile", "GetIPAddress", "");
                model.MacAddress = (string)Helper.ExecuteService("eFile", "GetMACAddress", "");

                //List<string> myResult = (List<string>)Helper.ExecuteService("eFile", "SentToDept", model);
                List<eFileAttachment> myResult = (List<eFileAttachment>)Helper.ExecuteService("eFile", "SentToDept", model);
                mUsers objM = new mUsers();
                Guid newguid = new Guid(CurrentSession.UserID);
                objM.UserId = newguid;
                var ResultForSend = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserIDSMS", objM);//for getting user phoneno from userid               
                StringBuilder ReffStringAll = new StringBuilder();
                StringBuilder AllDept = new StringBuilder();

                if (myResult != null)
                {
                    if (myResult.Count != 0)
                    {

                        foreach (var item in myResult.LastOrDefault().PaperRefNoList)
                        {
                            ReffStringAll.Append(item);
                            ReffStringAll.Append(",");
                        }

                    }

                }

                //foreach (var item in myResult.FirstOrDefault().DepartmentName)
                //{
                //    AllDept.Append(item.DepartmentName);
                //    AllDept.Append(",");
                //}

                //string[] allReffNo = myResult.ToArray();
                //string.Join(",", allReffNo);
                //committee name // myResult.FirstOrDefault().CommitteeName
                if (myResult != null && IsMsgRequired.SettingValue == "True")
                {
                    if (myResult.Count != 0)
                    {
                        var TextToSend = "Kindly Take Necessary action w.r.t e-Vidhan reference no " + Convert.ToString(ReffStringAll).TrimEnd(',') + " regarding " + myResult.FirstOrDefault().ItemSubject + " SentBy " + model.CommitteeName + " to " + myResult.FirstOrDefault().DepartmentName + " on " + myResult.FirstOrDefault().ReceivedDate;


                        //to send msg to mupltiple contact groups
                        List<string> phoneNumbers = new List<string>();
                        var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByDeptInHC", new RecipientGroupMember { CommitteeId = Convert.ToString(model.CommitteeId) }) as List<RecipientGroupMember>;

                        if (contactGroupMembers != null)
                        {
                            foreach (var item in contactGroupMembers)
                            {
                                if (item.MobileNo.Contains(','))
                                {
                                    string[] numbers = item.MobileNo.Split(',');
                                    foreach (var mb in numbers)
                                    {
                                        phoneNumbers.Add(mb);
                                    }
                                }
                                else
                                    phoneNumbers.Add(item.MobileNo);
                            }
                        }

                        //for email
                        if (IsMailRequired.SettingValue == "True")
                        {
                            if (ResultForSend.EmailId != null || ResultForSend.EmailId != "")
                            {


                                Email.API.CustomMailMessage emailmsg = new Email.API.CustomMailMessage();
                                emailmsg._body = TextToSend;
                                emailmsg._isBodyHtml = true;
                                emailmsg._from = ResultForSend.EmailId;
                                emailmsg._subject = "Pendency Reply";
                                emailmsg._toList.Add(ResultForSend.EmailId);//sending mail to same person
                                Notification.Send(false, true, null, emailmsg);
                            }
                        }
                        if (IsMsgRequired.SettingValue == "True")
                        {
                            //if (ResultForSend.MobileNo != null || ResultForSend.MobileNo != "") //not checking same person number now

                            //condition specially for sec                    
                            if (CurrentSession.SubUserTypeID == "3" && CurrentSession.OfficeLevel == "2")
                            {
                                mUsers objmUsers = new mUsers();
                                objmUsers.UserId = Guid.Parse(CurrentSession.UserID);
                                var PhoneNoOfsec = Helper.ExecuteService("User", "GetUserDeptDetailsByUserID", objmUsers) as mUsers;
                                phoneNumbers.Add(PhoneNoOfsec.MobileNo);
                            }

                            if (phoneNumbers != null && phoneNumbers.Count() != 0)
                            {
                                //for sending sms..
                                SMSMessageList smsMessage = new SMSMessageList();
                                Service1 smsServiceClient = new Service1();
                                foreach (var item in phoneNumbers)
                                {
                                    smsMessage.MobileNo.Add(item);
                                }
                                smsMessage.ModuleActionID = -1;
                                smsMessage.UniqueIdentificationID = -1;
                                smsMessage.SMSText = TextToSend;
                                //smsMessage.MobileNo.Add(ResultForSend.MobileNo);
                                Notification.Send(true, false, smsMessage, null);
                            }

                        }
                    }
                }


                return Json(JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog(ex, "Not Working..");
                throw;
            }

            //   return Json( JsonRequestBehavior.AllowGet);

        }

        //public JsonResult SendToDeptEntry(int Id, bool Approvefile, bool SignDraftpaper)
        //{
        //    try
        //    {
        //        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog(Convert.ToString(Id));
        //        eFileAttachment model = new eFileAttachment();
        //        SBL.DomainModel.Models.eFile.eFile model1 = new DomainModel.Models.eFile.eFile();
        //        model.eFileAttachmentId = Id;
        //        model = (eFileAttachment)Helper.ExecuteService("eFile", "GetDataPathsforMerge", model);
        //       // string paperdraftHTml = model.PaperDraft;
        //        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
        //        //var SaveFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSaveFileSettingLocation", null);
        //        var FileLocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
        //        string textLine1 = CurrentSession.UserName;

        //        var IsMsgRequired = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "IsMsgRequiredInHouseCommittee", null);
        //        var IsMailRequired = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "IsMailRequiredInHouseCommittee", null);
        //        //if (CurrentSession.UserName != null && CurrentSession.UserName != "Sunder Singh Verma")
        //        //{
        //        //    model.SignFilePath = "/Images/SignaturePath/sec.jpg";
        //        //    textLine1 = "Sunder Singh Verma";
        //        //}
        //        //else
        //        //{
        //        //    model.SignFilePath = CurrentSession.UserID.GetSignPath();
        //        //}
        //        string filepathDoc = FileLocation.SettingValue + model.eFilePathWordN;
        //        string strName = Path.GetFileName(filepathDoc).ToLower();
        //        string filepathPDF = strName;
        //        string docpath = @" C:\inetpub\e_Vidhan\FileStructure\ePaper\" + strName;
        //        string strTemp = Path.GetExtension(filepathDoc).ToLower();

        //        if (strTemp == ".doc" || strTemp == ".docx")
        //        {
        //            filepathPDF = Path.ChangeExtension(filepathPDF, "pdf");
        //        }

        //        string outputPath = @" C:\inetpub\e_Vidhan\FileStructure\ePaper\" + filepathPDF;
        //        model.SignFilePath = CurrentSession.UserID.GetSignPath();
        //        model1.AadharID = CurrentSession.AadharId.ToString();
        //        string DesigName = (string)Helper.ExecuteService("eFile", "GetDesigNameByAadharId", model1);
        //        // paper draft html to pdf  25 april
        //        PdfConverter pdfConverter = new PdfConverter();
        //        pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
        //        pdfConverter.PdfDocumentOptions.ShowHeader = true;
        //        pdfConverter.PdfDocumentOptions.ShowFooter = true;
        //        pdfConverter.PdfFooterOptions.FooterHeight = 60;
        //        pdfConverter.PdfHeaderOptions.HeaderHeight = 60;
        //        // pdfConverter.PdfDocumentOptions.FitWidth = true;
        //        // pdfConverter.PdfDocumentOptions.LeftMargin = 20;
        //        // pdfConverter.PdfDocumentOptions.RightMargin = 20;

        //        string imageFileLoc = Server.MapPath(model.SignFilePath);
        //        ImageElement imageElement1 = new ImageElement(0, 30, System.IO.Path.Combine(imageFileLoc));
        //        //  imageElement1.Opacity = 50;
        //        // imageElement1.HorizontalPosition = 50.0F
        //        imageElement1.DestHeight = 20;
        //        imageElement1.DestWidth = 20;
        //        imageElement1.XLocation = 130;
        //        // imageElement1.BackColor = Color.Transparent;
        //        // imageElement1.YLocation = 10;
        //        TextElement footerTextElement = new TextElement(0, 30, "Digitially Signed by: " + CurrentSession.UserName + " ( " + DesigName + " ) On " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt"),
        //                new System.Drawing.Font(new FontFamily("Times New Roman"), 9, GraphicsUnit.Point));
        //        footerTextElement.TextAlign = HorizontalTextAlign.Center;
        //        footerTextElement.ForeColor = Color.Navy;
        //        pdfConverter.PdfFooterOptions.AddElement(imageElement1);
        //        pdfConverter.PdfFooterOptions.AddElement(imageElement1);
        //        pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

        //        //EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(paperdraftHTml);

        //        //byte[] pdfBytes = null;

        //        //try
        //        //{
        //        //    pdfBytes = pdfDocument.Save();
        //        //}
        //        //finally
        //        //{
        //        //    // close the Document to realease all the resources
        //        //    pdfDocument.Close();
        //        //}

        //        //string url = "ePaper";
        //        //string directory = FileLocation.SettingValue + url;

        //        //if (!System.IO.Directory.Exists(directory))
        //        //{
        //        //    System.IO.Directory.CreateDirectory(directory);
        //        //}
        //        //int fileID = GetFileRandomNo();
        //        //Guid id = Guid.NewGuid();
        //        //string name = "PaperDrafthtml";
        //        //string path = System.IO.Path.Combine(directory, id + "_" + name + ".pdf");
        //        //string FilePath = "/ePaper/" + id + "_" + name + ".pdf";
        //        //FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //        //_FileStream.Write(pdfBytes, 0, pdfBytes.Length);
        //        //// close file stream
        //        //_FileStream.Close();



        //        //model.eFilePath = FilePath;
        //        model.eFilePath = outputPath;
        //        string[] strngBID = new string[2];
        //        if (CurrentSession.BranchId == "" || CurrentSession.BranchId == null)
        //        {
        //            CurrentSession.BranchId = Convert.ToString(model.CurrentBranchId);
        //        }
        //        strngBID[0] = CurrentSession.BranchId;
        //        var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
        //        model.CommitteeId = Convert.ToInt16(value[0]);
        //        model.CommitteeName = value[1];
        //        model.IpAddress = (string)Helper.ExecuteService("eFile", "GetIPAddress", "");
        //        model.MacAddress = (string)Helper.ExecuteService("eFile", "GetMACAddress", "");

        //        //List<string> myResult = (List<string>)Helper.ExecuteService("eFile", "SentToDept", model);
        //        List<eFileAttachment> myResult = (List<eFileAttachment>)Helper.ExecuteService("eFile", "SentToDept", model);
        //        mUsers objM = new mUsers();
        //        Guid newguid = new Guid(CurrentSession.UserID);
        //        objM.UserId = newguid;
        //        var ResultForSend = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserIDSMS", objM);//for getting user phoneno from userid               
        //        StringBuilder ReffStringAll = new StringBuilder();
        //        StringBuilder AllDept = new StringBuilder();

        //        if (myResult != null)
        //        {
        //            foreach (var item in myResult.LastOrDefault().PaperRefNoList)
        //            {
        //                ReffStringAll.Append(item);
        //                ReffStringAll.Append(",");
        //            }
        //        }

        //        //foreach (var item in myResult.FirstOrDefault().DepartmentName)
        //        //{
        //        //    AllDept.Append(item.DepartmentName);
        //        //    AllDept.Append(",");
        //        //}

        //        //string[] allReffNo = myResult.ToArray();
        //        //string.Join(",", allReffNo);
        //        //committee name // myResult.FirstOrDefault().CommitteeName
        //        if (myResult != null && IsMsgRequired.SettingValue == "True")
        //        {
        //            var TextToSend = "Kindly Take Necessary action w.r.t e-Vidhan reference no " + Convert.ToString(ReffStringAll).TrimEnd(',') + " regarding " + myResult.FirstOrDefault().ItemSubject + " SentBy " + model.CommitteeName + " to " + myResult.FirstOrDefault().DepartmentName + " on " + myResult.FirstOrDefault().ReceivedDate;


        //            //to send msg to mupltiple contact groups
        //            List<string> phoneNumbers = new List<string>();
        //            var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByDeptInHC", new RecipientGroupMember { CommitteeId = Convert.ToString(model.CommitteeId) }) as List<RecipientGroupMember>;

        //            if (contactGroupMembers != null)
        //            {
        //                foreach (var item in contactGroupMembers)
        //                {
        //                    if (item.MobileNo.Contains(','))
        //                    {
        //                        string[] numbers = item.MobileNo.Split(',');
        //                        foreach (var mb in numbers)
        //                        {
        //                            phoneNumbers.Add(mb);
        //                        }
        //                    }
        //                    else
        //                        phoneNumbers.Add(item.MobileNo);
        //                }
        //            }

        //            //for email
        //            if (IsMailRequired.SettingValue == "True")
        //            {
        //                if (ResultForSend.EmailId != null || ResultForSend.EmailId != "")
        //                {


        //                    Email.API.CustomMailMessage emailmsg = new Email.API.CustomMailMessage();
        //                    emailmsg._body = TextToSend;
        //                    emailmsg._isBodyHtml = true;
        //                    emailmsg._from = ResultForSend.EmailId;
        //                    emailmsg._subject = "Pendency Reply";
        //                    emailmsg._toList.Add(ResultForSend.EmailId);//sending mail to same person
        //                    Notification.Send(false, true, null, emailmsg);
        //                }
        //            }
        //            if (IsMsgRequired.SettingValue == "True")
        //            {
        //                //if (ResultForSend.MobileNo != null || ResultForSend.MobileNo != "") //not checking same person number now

        //                //condition specially for sec                    
        //                if (CurrentSession.SubUserTypeID == "3" && CurrentSession.OfficeLevel == "2")
        //                {
        //                    mUsers objmUsers = new mUsers();
        //                    objmUsers.UserId = Guid.Parse(CurrentSession.UserID);
        //                    var PhoneNoOfsec = Helper.ExecuteService("User", "GetUserDeptDetailsByUserID", objmUsers) as mUsers;
        //                    phoneNumbers.Add(PhoneNoOfsec.MobileNo);
        //                }

        //                if (phoneNumbers != null && phoneNumbers.Count() != 0)
        //                {
        //                    //for sending sms..
        //                    SMSMessageList smsMessage = new SMSMessageList();
        //                    Service1 smsServiceClient = new Service1();
        //                    foreach (var item in phoneNumbers)
        //                    {
        //                        smsMessage.MobileNo.Add(item);
        //                    }
        //                    smsMessage.ModuleActionID = -1;
        //                    smsMessage.UniqueIdentificationID = -1;
        //                    smsMessage.SMSText = TextToSend;
        //                    //smsMessage.MobileNo.Add(ResultForSend.MobileNo);
        //                    Notification.Send(true, false, smsMessage, null);
        //                }

        //            }
        //        }


        //        return Json(JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog(ex, "Not Working..");
        //        throw;
        //    }

        //    //   return Json( JsonRequestBehavior.AllowGet);

        //}




        public static void Word2PDF(string fileName, string outputPath)
        {
            Microsoft.Office.Interop.Word.Application wordApplication = new Microsoft.Office.Interop.Word.Application();
            // ApplicationClass wordApplication = new ApplicationClass();
            Microsoft.Office.Interop.Word.Document wordDocument = null;
            object paramSourceDocPath = fileName;
            object paramMissing = Type.Missing;
            string paramExportFilePath = outputPath;
            WdExportFormat paramExportFormat = WdExportFormat.wdExportFormatPDF;
            bool paramOpenAfterExport = false;

            WdExportOptimizeFor paramExportOptimizeFor = WdExportOptimizeFor.wdExportOptimizeForPrint;

            WdExportRange paramExportRange = WdExportRange.wdExportAllDocument;
            int paramStartPage = 0;

            int paramEndPage = 0;
            WdExportItem paramExportItem = WdExportItem.wdExportDocumentContent;

            bool paramIncludeDocProps = true;
            bool paramKeepIRM = true;

            WdExportCreateBookmarks paramCreateBookmarks =
            WdExportCreateBookmarks.wdExportCreateWordBookmarks;

            bool paramDocStructureTags = true;
            bool paramBitmapMissingFonts = true;

            bool paramUseISO19005_1 = false;

            try
            {
                // Open the source document.
                wordDocument = wordApplication.Documents.Open(
                ref paramSourceDocPath, ref paramMissing, ref paramMissing,
                ref paramMissing, ref paramMissing, ref paramMissing,
                ref paramMissing, ref paramMissing, ref paramMissing,
                ref paramMissing, ref paramMissing, ref paramMissing,
                ref paramMissing, ref paramMissing, ref paramMissing,
                ref paramMissing);

                // Export it in the specified format.
                if (wordDocument != null)
                    wordDocument.ExportAsFixedFormat(paramExportFilePath,
                    paramExportFormat, paramOpenAfterExport,
                    paramExportOptimizeFor, paramExportRange, paramStartPage,
                    paramEndPage, paramExportItem, paramIncludeDocProps,
                    paramKeepIRM, paramCreateBookmarks, paramDocStructureTags,
                    paramBitmapMissingFonts, paramUseISO19005_1,
                    ref paramMissing);
            }
            catch (Exception ex)
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog(ex, "Converting Word to PDF Error..");
                // Respond to the error
            }
            finally
            {
                // Close and release the Document object.
                if (wordDocument != null)
                {
                    wordDocument.Close(ref paramMissing, ref paramMissing,
                    ref paramMissing);
                    wordDocument = null;
                }
                // Quit Word and release the ApplicationClass object.
                if (wordApplication != null)
                {
                    wordApplication.Quit(ref paramMissing, ref paramMissing,
                    ref paramMissing);
                    wordApplication = null;
                }
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }



        public Microsoft.Office.Interop.Word.Document wordDocument { get; set; }


        [HttpPost]
        public JsonResult SaveNewPaperEntry(eFileAttachment model, HttpPostedFileBase file, int? lstOffice)
        {
            eFileAttachment mdl = new eFileAttachment();
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.eFilePath))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePath));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.eFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                        }

                    }

                }
                else
                {
                    return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }


            //#region Upload Word File
            //string url1Word = "~/ePaper/FileWord";
            //string directory1Word = Server.MapPath(url1Word);

            //if (Directory.Exists(directory1Word))
            //{
            //    string[] savedFileNameWord = Directory.GetFiles(Server.MapPath(url1Word));
            //    if (savedFileNameWord.Length > 0)
            //    {
            //        string SourceFile = savedFileNameWord[0];
            //        foreach (string page in savedFileNameWord)
            //        {
            //            Guid FileName = Guid.NewGuid();

            //            string name = Path.GetFileName(page);

            //            string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


            //            if (!string.IsNullOrEmpty(name))
            //            {
            //                if (!string.IsNullOrEmpty(mdl.eFilePathWord))
            //                    System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePathWord));
            //                System.IO.File.Copy(SourceFile, path, true);
            //                mdl.eFilePathWord = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
            //            }

            //        }

            //    }
            //    else
            //    {
            //        ////  mdl.eFilePathWord = null;
            //        return Json("Please select Doc file", JsonRequestBehavior.AllowGet);
            //    }

            //}
            //#endregion

            mdl.SplitFileCount = 0;
            mdl.CreatedBy = new Guid(CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.eFileNameId = Guid.NewGuid();
            model.ParentId = fileID;

            // i am receiver
            mdl.ToDepartmentID = CurrentSession.DeptID; //"HPD0001";
            int OfficeId = 0;
            int.TryParse(CurrentSession.OfficeId, out OfficeId);
            mdl.ToOfficeCode = OfficeId;// int.Parse(CurrentSession.OfficeId);// 0;
            ///


            // I am Sender
            mdl.DepartmentId = model.DepartmentId;//CurrentSession.DeptID; AddeFileAttachments
            string _lstOffice = Convert.ToString(lstOffice);
            if (!string.IsNullOrEmpty(_lstOffice))
            {
                mdl.OfficeCode = int.Parse(_lstOffice);
            }
            else
            {
                mdl.OfficeCode = 0;
            }
            //////

            if (model.PType == "4")
            {
                mdl.PType = "R";
            }
            else
            {
                mdl.PType = "O"; //model.PType;
            }


            mdl.DocumentTypeId = model.DocumentTypeId;
            mdl.Description = model.Description;
            mdl.PaperNature = model.PaperNature;
            mdl.PaperNatureDays = model.PaperNatureDays;
            mdl.Title = model.Title;
            mdl.PaperRefNo = model.PaperRefNo;
            mdl.RecvDetails = model.RecvDetails;
            mdl.RevedOption = model.RevedOption;
            mdl.ToRecvDetails = model.RecvDetails;
            mdl.PaperTicketValue = model.PaperTicketValue;
            mdl.PaperNo = model.PaperNo;
            mdl.DispatchtoWhom = model.DispatchtoWhom;

            mdl.eFileID = model.eFileID;
            mdl.IsBackLog = true;
            mdl.IsVerified = true;
            mdl.VerifiedDate = model.SendDate;
            mdl.VerifiedBy = CurrentSession.UserName;
            mdl.CurrentBranchId = Convert.ToInt32(CurrentSession.BranchId);
            mdl.CurrentEmpAadharID = CurrentSession.AadharId;
            mdl.EmpName = CurrentSession.UserName;
            mdl.Desingnation = CurrentSession.Designation;
            string[] strngBID = new string[2];
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            mdl.CommitteeId = Convert.ToInt16(value[0]);
            mdl.ItemTypeName = model.ItemTypeName;
            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@DocumentTypeId", mdl.DocumentTypeId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileName", mdl.eFileName));
            methodParameter.Add(new KeyValuePair<string, string>("@Description", mdl.Description));
            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", mdl.DepartmentId));
            methodParameter.Add(new KeyValuePair<string, string>("@Title", mdl.Title));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileNameId", mdl.eFileNameId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DocType", "ReceivePaperByDept"));
            methodParameter.Add(new KeyValuePair<string, string>("@SplitFileCount", mdl.SplitFileCount.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@OfficeCode", mdl.OfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNature", mdl.PaperNature.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNatureDays", mdl.PaperNatureDays));
            methodParameter.Add(new KeyValuePair<string, string>("@ToDepartmentID", mdl.ToDepartmentID));
            methodParameter.Add(new KeyValuePair<string, string>("@ToOfficeCode", mdl.ToOfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperStatus", "N"));
            methodParameter.Add(new KeyValuePair<string, string>("@PType", mdl.PType));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePath", mdl.eFilePath));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperRefNo", mdl.PaperRefNo));
            methodParameter.Add(new KeyValuePair<string, string>("@RecvDetails", mdl.RecvDetails));
            methodParameter.Add(new KeyValuePair<string, string>("@ToRecvDetails", mdl.ToRecvDetails));
            methodParameter.Add(new KeyValuePair<string, string>("@RevedOption", mdl.RevedOption));
            methodParameter.Add(new KeyValuePair<string, string>("@isSend", "0"));
            methodParameter.Add(new KeyValuePair<string, string>("@TicketValue", mdl.PaperTicketValue));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNo", mdl.PaperNo));
            methodParameter.Add(new KeyValuePair<string, string>("@WhomtoDispatch", mdl.DispatchtoWhom));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileId", "0"));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePathWord", mdl.eFilePathWord));
            methodParameter.Add(new KeyValuePair<string, string>("@ReFileId", mdl.eFileID.ToString()));

            methodParameter.Add(new KeyValuePair<string, string>("@IsBackLog", mdl.IsBackLog.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@IsVerified", mdl.IsVerified.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@VerifiedDate", mdl.VerifiedDate.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@VerifiedBy", mdl.VerifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CurrentBranchId", mdl.CurrentBranchId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CurrentEmpAadharID", mdl.CurrentEmpAadharID.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@EmpName", mdl.EmpName.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", mdl.CommitteeId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ItemTypeName", mdl.ItemTypeName.ToString()));
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperEntry", methodParameter);
            var Id = dataSet.Tables[0].Rows[0][2].ToString();
            mdl.eFileAttachmentId = Convert.ToInt32(Id);
            int Val = (int)Helper.ExecuteService("eFile", "NotingBacklog", mdl);

            string res = "", serno = "";
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                res = dataSet.Tables[0].Rows[0][0].ToString();
                serno = dataSet.Tables[0].Rows[0][1].ToString();
            }
            if (res == "Success")
            {

                return Json("Paper Received Successfully. Paper Ref No. : <span class='label label-xlg label-primary arrowed arrowed-right' style='  font-size: 16px;  height: 29px;'><b>" + serno + "</b></span>", JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json("Some Problem Occured. Please Try Again", JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult SaveSentPapercreate(eFileAttachment model, HttpPostedFileBase file, int? lstOffice)
        {
            eFileAttachment mdl = new eFileAttachment();
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.eFilePath))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePath));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.eFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                        }

                    }

                }
                else
                {
                    return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }


            //#region Upload Word File
            //string url1Word = "~/ePaper/FileWord";
            //string directory1Word = Server.MapPath(url1Word);

            //if (Directory.Exists(directory1Word))
            //{
            //    string[] savedFileNameWord = Directory.GetFiles(Server.MapPath(url1Word));
            //    if (savedFileNameWord.Length > 0)
            //    {
            //        string SourceFile = savedFileNameWord[0];
            //        foreach (string page in savedFileNameWord)
            //        {
            //            Guid FileName = Guid.NewGuid();

            //            string name = Path.GetFileName(page);

            //            string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


            //            if (!string.IsNullOrEmpty(name))
            //            {
            //                if (!string.IsNullOrEmpty(mdl.eFilePathWord))
            //                    System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePathWord));
            //                System.IO.File.Copy(SourceFile, path, true);
            //                mdl.eFilePathWord = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
            //            }

            //        }

            //    }
            //    else
            //    {
            //        ////  mdl.eFilePathWord = null;
            //        return Json("Please select Doc file", JsonRequestBehavior.AllowGet);
            //    }

            //}
            //#endregion

            mdl.SplitFileCount = 0;
            mdl.CreatedBy = new Guid(CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.eFileNameId = Guid.NewGuid();
            model.ParentId = fileID;

            // i am receiver
            if (model.DepartmentId == null)
            {
                mdl.ToDepartmentID = "HPD0001";  //"HPD0001";
            }
            else
            {
                mdl.ToDepartmentID = model.DepartmentId;  //"HPD0001";
            }

            int OfficeId = 0;
            int.TryParse(CurrentSession.OfficeId, out OfficeId);
            mdl.ToOfficeCode = OfficeId;// int.Parse(CurrentSession.OfficeId);// 0;
            ///


            // I am Sender
            mdl.DepartmentId = CurrentSession.DeptID; //CurrentSession.DeptID; AddeFileAttachments
            string _lstOffice = Convert.ToString(lstOffice);
            if (!string.IsNullOrEmpty(_lstOffice))
            {
                mdl.OfficeCode = int.Parse(_lstOffice);
            }
            else
            {
                mdl.OfficeCode = 0;
            }
            //////

            if (model.PType == "4")
            {
                mdl.PType = "R";
            }
            else
            {
                mdl.PType = "O"; //model.PType;
            }


            mdl.DocumentTypeId = model.DocumentTypeId;
            mdl.Description = model.Description;
            mdl.PaperNature = model.PaperNature;
            mdl.PaperNatureDays = model.PaperNatureDays;
            mdl.Title = model.Title;
            mdl.PaperRefNo = model.PaperRefNo;
            mdl.RecvDetails = model.RecvDetails;
            mdl.RevedOption = model.RevedOption;
            mdl.ToRecvDetails = model.RecvDetails;
            mdl.PaperTicketValue = model.PaperTicketValue;
            mdl.PaperNo = model.PaperNo;
            mdl.DispatchtoWhom = model.DispatchtoWhom;
            mdl.eFileID = model.eFileID;
            mdl.IsBackLog = true;
            mdl.IsVerified = true;
            mdl.VerifiedDate = model.SendDate;
            mdl.VerifiedBy = CurrentSession.UserName;
            mdl.CurrentBranchId = Convert.ToInt32(CurrentSession.BranchId);
            mdl.CurrentEmpAadharID = CurrentSession.AadharId;
            mdl.EmpName = CurrentSession.UserName;
            mdl.Desingnation = CurrentSession.Designation;
            string[] strngBID = new string[2];
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            mdl.CommitteeId = Convert.ToInt16(value[0]);
            mdl.ItemTypeName = model.ItemTypeName;

            //Helper.ExecuteService("eFile", "AddeFileAttachments", mdl);


            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@DocumentTypeId", mdl.DocumentTypeId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileName", mdl.eFileName));
            methodParameter.Add(new KeyValuePair<string, string>("@Description", mdl.Description));
            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", mdl.DepartmentId));
            methodParameter.Add(new KeyValuePair<string, string>("@Title", mdl.Title));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileNameId", mdl.eFileNameId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DocType", "ReceivePaperByDept"));
            methodParameter.Add(new KeyValuePair<string, string>("@SplitFileCount", mdl.SplitFileCount.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@OfficeCode", mdl.OfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNature", mdl.PaperNature.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNatureDays", mdl.PaperNatureDays));
            methodParameter.Add(new KeyValuePair<string, string>("@ToDepartmentID", mdl.ToDepartmentID));
            methodParameter.Add(new KeyValuePair<string, string>("@ToOfficeCode", mdl.ToOfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperStatus", "N"));
            methodParameter.Add(new KeyValuePair<string, string>("@PType", mdl.PType));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePath", mdl.eFilePath));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperRefNo", mdl.PaperRefNo));
            methodParameter.Add(new KeyValuePair<string, string>("@RecvDetails", mdl.RecvDetails));
            methodParameter.Add(new KeyValuePair<string, string>("@ToRecvDetails", mdl.ToRecvDetails));
            methodParameter.Add(new KeyValuePair<string, string>("@RevedOption", mdl.RevedOption));
            methodParameter.Add(new KeyValuePair<string, string>("@isSend", "0"));
            methodParameter.Add(new KeyValuePair<string, string>("@TicketValue", mdl.PaperTicketValue));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNo", mdl.PaperNo));
            methodParameter.Add(new KeyValuePair<string, string>("@WhomtoDispatch", mdl.DispatchtoWhom));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileId", "0"));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePathWord", mdl.eFilePathWord));
            methodParameter.Add(new KeyValuePair<string, string>("@ReFileId", mdl.eFileID.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@IsBackLog", mdl.IsBackLog.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@IsVerified", mdl.IsVerified.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@VerifiedDate", mdl.VerifiedDate.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@VerifiedBy", mdl.VerifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CurrentBranchId", mdl.CurrentBranchId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CurrentEmpAadharID", mdl.CurrentEmpAadharID.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@EmpName", mdl.EmpName.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", mdl.CommitteeId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ItemTypeName", mdl.ItemTypeName.ToString()));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperEntry", methodParameter);
            var Id = dataSet.Tables[0].Rows[0][2].ToString();
            mdl.eFileAttachmentId = Convert.ToInt32(Id);
            int Val = (int)Helper.ExecuteService("eFile", "NotingBacklog", mdl);

            string res = "", serno = "";
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                res = dataSet.Tables[0].Rows[0][0].ToString();
                serno = dataSet.Tables[0].Rows[0][1].ToString();
            }
            if (res == "Success")
            {

                return Json("Paper Received Successfully. Paper Ref No. : <span class='label label-xlg label-primary arrowed arrowed-right' style='  font-size: 16px;  height: 29px;'><b>" + serno + "</b></span>", JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json("Some Problem Occured. Please Try Again", JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult YearsList()
        {
            return PartialView("YearsList");//after words will call GetReceivePaperList --robin
        }
        public ActionResult InboxClickDetail(int pageId = 1, int pageSize = 10, int YearId = 5, int PapersId = 1)
        {
            //  eFileLogin();
            eFileAttachments model = new eFileAttachments();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string Year = YearId.ToString();
            string papers = PapersId.ToString();
            model.CurrentSessionAId = AadharId;
            string[] strngBIDN = new string[2];
            strngBIDN[0] = CurrentSession.BranchId;
            var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = value123[0];
            //str[3] = currtBId;
            str[4] = Year;
            str[5] = papers;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            List<ReadTableData> tableList = new List<ReadTableData>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC_onclick", str);
            //  tableList = (List<ReadTableData>)Helper.ExecuteService("eFile", "GetReadDataTableList", str);
            //   model.DraftListCount = ListModel.Count - tableList.Count;
            //  objModel.CountDraft = ListModelDraft.Count;
            // model.eFileAttachemts = ListModel.ToList() - tableList.ToList();
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            //string[] str1 = new string[4];
            //string[] strngBID = new string[2];
            //str1[0] = DepartmentId;
            //str1[1] = Officecode;
            //str1[2] = CurrentSession.BranchId;
            //strngBID[0] = CurrentSession.BranchId;
            //var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);

            //str1[3] = value[0];
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);

            //var eFileList = eList.eFileLists;
            //if (eFileList.Count > 0)
            //{
            //    ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            //}
            //else
            //{
            //    ViewBag.EFilesListAll = null;
            //}
            model.eFileAttachemts = ListModel;//pagedRecord commented the pagination
            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            //model.eFileLists = eList.eFileLists;

            //for count update draft list 
            //string design = CurrentSession.Designation;
            //string paperType = PapersId.ToString();
            //string[] strDraft = new string[6];
            //strDraft[0] = DepartmentId;
            //strDraft[1] = Officecode;
            //strDraft[2] = AadharId;
            //strDraft[3] = currtBId;
            //strDraft[4] = design;
            //strDraft[5] = paperType;
            //List<eFileAttachment> ListModelForDraft = new List<eFileAttachment>();
            //ListModelForDraft = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", str);
            //model.DraftListCount = ListModelForDraft.Count;

            return PartialView("InboxClickDetail", model);//after words will call GetReceivePaperList --robin
        }
        public ActionResult InboxDraftDetail(int pageId = 1, int pageSize = 25, int PapersId = 1)
        {
            CurrentSession.SetSessionAlive = null;
            eFileAttachments model = new eFileAttachments();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;//
            string design = CurrentSession.Designation;

            string DepartmentId = CurrentSession.DeptID;

            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string paperType = PapersId.ToString();
            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = currtBId;
            str[4] = design;
            str[5] = paperType;
            model.CurrentSessionAId = AadharId;
            model.Sessiondesign = design;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", str);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;


            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //string[] str1 = new string[4];
            //string[] strngBID = new string[2];
            //str1[0] = DepartmentId;
            //str1[1] = Officecode;
            //str1[2] = CurrentSession.BranchId;
            //strngBID[0] = CurrentSession.BranchId;
            //var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            //str1[3] = value[0];
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);
            //var eFileList = eList.eFileLists;
            //if (eFileList.Count > 0)
            //{
            //    ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            //}
            //else
            //{
            //    ViewBag.EFilesListAll = null;
            //}
            //var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "ComSendButtonDisp", null);

            //model.CheckAuthorize = (int)Helper.ExecuteService("eFile", "GetAuthorizeEmp", model);

            //model.ButtonShow = NewsSettings.SettingValue;
            model.eFileAttachemts = ListModel;
            //model.eFileLists = eList.eFileLists;

            //#region For Send List Count
            //string[] strS = new string[6];
            //strS[0] = DepartmentId;
            //strS[1] = Officecode;
            //strS[2] = AadharId;
            //strS[3] = currtBId;
            //strS[4] = "5";
            //strS[5] = "1";
            //List<eFileAttachment> ListModelForSent = new List<eFileAttachment>();
            //ListModelForSent = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendList", strS);
            //model.SentPaperCount = ListModelForSent.Count;
            // #endregion

            return PartialView("_InboxDraftList", model);
        }


        public ActionResult YearsListSent()
        {
            return PartialView("YearsListSent");
        }

        public ActionResult MethodRecieve()
        {
            return PartialView("MethodRecieve");
        }

        public ActionResult MethodSend()
        {
            return PartialView("MethodSend");
        }

        public ActionResult MethodDraft()
        {
            return PartialView("MethodDraft");
        }

        public ActionResult GetDetailsList(int pageId = 1, int pageSize = 25, int DraftId = 0)
        {
            eFileLogin();
            eFileAttachments model = new eFileAttachments();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string SDraftId = DraftId.ToString();
            string[] str2 = new string[3];
            str2[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str2);
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            model.CurrentSessionAId = AadharId;
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            var CommitteeTypePermission = (CommitteeTypePermission)Helper.ExecuteService("eFile", "GetCommitteUser", CurrentSession.UserID);
            if (CommitteeTypePermission != null && CommitteeTypePermission.CommitteeTypeId > 0)
                ViewBag.CommitteeTypePermission = "Yes";
            else
                ViewBag.CommitteeTypePermission = "No";


            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = currtBId;
            str[4] = SDraftId;
            str[5] = value[0];
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            // ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendPaperDetailsByDeptId", str);
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDetailsSentList", str);
            //model.eFileAttachemts = ListModel;
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            string[] str1 = new string[4];
            string[] strngBID = new string[2];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value1 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);

            str1[3] = value1[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileAttachemts = pagedRecord;
            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            model.eFileLists = eList.eFileLists;


            return PartialView("_GetDetailePopUp", model);

        }

        public PartialViewResult NewDraftPaper()
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();
            documents.PaperRefNo = GetRefNo();
            string DeptId = CurrentSession.DeptID;
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            ///Get eFilePaperNature
            if (DeptId == "HPD0001")
            {
                var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList_Receive", null);
                ViewBag.eFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            }
            else
            {
                var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList_ReceiveDeptDraftNew", null);
                ViewBag.eFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            }


            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);

            //Get Items

            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetFileItemList", null);
            ViewBag.FileItem = new SelectList(_ListItems, "ID", "ItemTypeName", null);

            //For Department Users
            mDepartment mdep = new mDepartment();
            mdep.deptId = CurrentSession.DeptID;
            documents.mDepartment = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
            if (mdep.deptId != "HPD0069")
            {
                documents.DepartmentId = "HPD0001";
                documents = (eFileAttachment)Helper.ExecuteService("eFile", "GetDeptfodisply", documents);
            }
            documents.ValueType = "New";
            var _Commitee = (List<CommitteeSearchModel>)Helper.ExecuteService("eFile", "GetCommittees", null);
            ViewBag.CommiteeName = new SelectList(_Commitee, "CommitteeId", "CommitteeName", null);
            return PartialView("_DraftPaperInfo", documents);
        }

        [HttpPost]
        public JsonResult NewDraftSave(eFileAttachment model, HttpPostedFileBase file, int? lstOffice)
        {
            eFileAttachment mdl = new eFileAttachment();
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.eFilePath))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePath));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.eFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                        }

                    }

                }
                else
                {
                    mdl.eFilePath = null;
                    // return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }


            #region Upload Word File
            string url1Word = "~/ePaper/FileWord";
            string directory1Word = Server.MapPath(url1Word);
            if (model.IsWord == "N")
            {
                if (Directory.Exists(directory))
                {
                    string[] savedFileNameWord = Directory.GetFiles(Server.MapPath(url1Word));
                    if (savedFileNameWord.Length > 0)
                    {
                        string SourceFile = savedFileNameWord[0];
                        foreach (string page in savedFileNameWord)
                        {
                            Guid FileName = Guid.NewGuid();

                            string name = Path.GetFileName(page);

                            string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                            if (!string.IsNullOrEmpty(name))
                            {
                                if (!string.IsNullOrEmpty(mdl.eFilePathWord))
                                    System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePathWord));
                                System.IO.File.Copy(SourceFile, path, true);
                                mdl.eFilePathWord = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                            }

                        }

                    }
                    else
                    {
                        mdl.eFilePathWord = null;
                        return Json("Please select Word file", JsonRequestBehavior.AllowGet);
                    }
                }

            }
            #endregion
            // mdl.eFilePathWord = null;
            //  mdl.PaperDraft = model.PaperDraft;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            if (currtBId == "" || currtBId == null)
            {
                currtBId = "0";
            }
            mdl.CreatedDate = DateTime.Now;
            mdl.DraftedBy = AadharId;
            mdl.CurrentBranchId = Convert.ToInt32(currtBId);
            mdl.SplitFileCount = 0;
            mdl.CreatedBy = new Guid(CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.eFileNameId = Guid.NewGuid();
            model.ParentId = fileID;

            // i am receiver
            mdl.ToDepartmentID = CurrentSession.DeptID; //"HPD0001";
            int OfficeId = 0;
            int.TryParse(CurrentSession.OfficeId, out OfficeId);
            mdl.ToOfficeCode = OfficeId;// int.Parse(CurrentSession.OfficeId);// 0;
            ///


            // I am Sender
            mdl.DepartmentId = model.DepartmentId;//CurrentSession.DeptID; AddeFileAttachments
            string _lstOffice = Convert.ToString(lstOffice);
            if (!string.IsNullOrEmpty(_lstOffice))
            {
                mdl.OfficeCode = int.Parse(_lstOffice);
            }
            else
            {
                mdl.OfficeCode = 0;
            }
            //////

            if (model.PType == "4")
            {
                mdl.PType = "R";
            }
            else
            {
                mdl.PType = "O"; //model.PType;
            }


            mdl.DocumentTypeId = model.DocumentTypeId;
            mdl.Description = model.Description;
            mdl.PaperNature = model.PaperNature;
            mdl.PaperNatureDays = model.PaperNatureDays;
            mdl.Title = model.Title;
            mdl.PaperRefNo = model.PaperRefNo;
            mdl.RecvDetails = model.RecvDetails;
            mdl.RevedOption = model.RevedOption;
            mdl.ToRecvDetails = model.RecvDetails;
            mdl.PaperTicketValue = model.PaperTicketValue;
            mdl.PaperNo = model.PaperNo;
            mdl.DispatchtoWhom = model.DispatchtoWhom;
            mdl.eFileID = model.eFileID;
            mdl.CountsAPS = model.CountsAPS;
            mdl.ItemTypeName = model.ItemTypeName;
            if (model.ToDepIds != null)
            {
                mdl.ToDepIds = model.ToDepIds.TrimEnd();
            }
            if (model.CCDepIds != null)
            {
                mdl.CCDepIds = model.CCDepIds.TrimEnd(',');
            }
            if (model.ToMemIds != null)
            {
                mdl.ToMemIds = model.ToMemIds.TrimEnd(',');
            }
            if (model.CCMemIds != null)
            {
                mdl.CCMemIds = model.CCMemIds.TrimEnd(',');
            }
            if (model.ToOtherIds != null)
            {
                mdl.ToOtherIds = model.ToOtherIds.TrimEnd(',');
            }
            if (model.CCOtherIds != null)
            {
                mdl.CCOtherIds = model.CCOtherIds.TrimEnd(',');
            }
            if (model.ToListComplN != null)
            {
                mdl.ToListComplN = model.ToListComplN.TrimEnd(',');
            }

            if (model.CCListComplN != null)
            {
                mdl.CCListComplN = model.CCListComplN.TrimEnd(',');
            }
            if (model.ToListComplIds != null)
            {
                mdl.ToListComplIds = model.ToListComplIds.TrimEnd(',');
            }
            if (model.CCListComplIds != null)
            {
                mdl.CCListComplIds = model.CCListComplIds.TrimEnd(',');
            }
            mdl.PaperRefrenceManualy = model.PaperRefrenceManualy;
            mdl.IpAddress = (string)Helper.ExecuteService("eFile", "GetIPAddress", "");
            mdl.MacAddress = (string)Helper.ExecuteService("eFile", "GetMACAddress", "");

            // mdl.CommitteeId = model.CommitteeId;
            if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
            {
                int res = (int)Helper.ExecuteService("eFile", "NewDraftEntry", mdl);

                if (res == 1)
                {

                    //ViewBag.ForNewDraftCount = 
                    return Json("New Draft Saved Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                mdl.DepartmentId = "HPD0001";
                int res = (int)Helper.ExecuteService("eFile", "NewDeptDraftEntry", mdl);

                if (res == 1)
                {
                    return Json("New Draft Saved Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
                }

            }
            //Helper.ExecuteService("eFile", "AddeFileAttachments", mdl);


            //DataSet dataSet = new DataSet();
            //var methodParameter = new List<KeyValuePair<string, string>>();
            //methodParameter.Add(new KeyValuePair<string, string>("@DocumentTypeId", mdl.DocumentTypeId.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@eFileName", mdl.eFileName));
            //methodParameter.Add(new KeyValuePair<string, string>("@Description", mdl.Description));
            //methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", mdl.DepartmentId));
            //methodParameter.Add(new KeyValuePair<string, string>("@Title", mdl.Title));
            //methodParameter.Add(new KeyValuePair<string, string>("@eFileNameId", mdl.eFileNameId.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@DocType", "ReceivePaperByDept"));
            //methodParameter.Add(new KeyValuePair<string, string>("@SplitFileCount", mdl.SplitFileCount.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@OfficeCode", mdl.OfficeCode.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@PaperNature", mdl.PaperNature.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@PaperNatureDays", mdl.PaperNatureDays));
            //methodParameter.Add(new KeyValuePair<string, string>("@ToDepartmentID", mdl.ToDepartmentID));
            //methodParameter.Add(new KeyValuePair<string, string>("@ToOfficeCode", mdl.ToOfficeCode.ToString()));
            //methodParameter.Add(new KeyValuePair<string, string>("@PaperStatus", "N"));
            //methodParameter.Add(new KeyValuePair<string, string>("@PType", mdl.PType));
            //methodParameter.Add(new KeyValuePair<string, string>("@eFilePath", mdl.eFilePath));
            //methodParameter.Add(new KeyValuePair<string, string>("@PaperRefNo", mdl.PaperRefNo));
            //methodParameter.Add(new KeyValuePair<string, string>("@RecvDetails", mdl.RecvDetails));
            //methodParameter.Add(new KeyValuePair<string, string>("@ToRecvDetails", mdl.ToRecvDetails));
            //methodParameter.Add(new KeyValuePair<string, string>("@RevedOption", mdl.RevedOption));
            //methodParameter.Add(new KeyValuePair<string, string>("@isSend", "0"));
            //methodParameter.Add(new KeyValuePair<string, string>("@TicketValue", mdl.PaperTicketValue));
            //methodParameter.Add(new KeyValuePair<string, string>("@PaperNo", mdl.PaperNo));
            //methodParameter.Add(new KeyValuePair<string, string>("@WhomtoDispatch", mdl.DispatchtoWhom));
            //methodParameter.Add(new KeyValuePair<string, string>("@eFileId", "0"));
            //methodParameter.Add(new KeyValuePair<string, string>("@eFilePathWord", mdl.eFilePathWord));
            //methodParameter.Add(new KeyValuePair<string, string>("@ReFileId", mdl.eFileID.ToString()));

            //dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperEntry", methodParameter);
            //string res = "", serno = "";
            //if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            //{
            //    res = dataSet.Tables[0].Rows[0][0].ToString();
            //    serno = dataSet.Tables[0].Rows[0][1].ToString();
            //}
            //if (res == "Success")
            //{

            //    return Json("Paper Received Successfully. Paper Ref No. : <span class='label label-xlg label-primary arrowed arrowed-right' style='  font-size: 16px;  height: 29px;'><b>" + serno + "</b></span>", JsonRequestBehavior.AllowGet);

            //}
            //else
            //{
            //    return Json("Some Problem Occured. Please Try Again", JsonRequestBehavior.AllowGet);

            //}
            //return Json("Paper Received Successfully. Paper Ref No. : <span class='label label-xlg label-primary arrowed arrowed-right' style='  font-size: 16px;  height: 29px;'><b>"  +"</b></span>", JsonRequestBehavior.AllowGet);
#pragma warning disable CS0162 // Unreachable code detected
            return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
#pragma warning restore CS0162 // Unreachable code detected
        }






        public ActionResult GetDataPendency(string EnterNo, string ItemID, string ReplyNo)
        {
            eFileAttachment documents = new eFileAttachment();
            documents.ItemNumber = Sanitizer.GetSafeHtmlFragment(EnterNo);
            documents.ItemId = Convert.ToInt32(ItemID);
            documents.ReplyRefNo = Sanitizer.GetSafeHtmlFragment(ReplyNo);
            documents = (eFileAttachment)Helper.ExecuteService("eFile", "GetPendencyData", documents);
            return Json(documents, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult SentPaperDept(eFileAttachment model, HttpPostedFileBase file, int? lstOffice)
        {
            eFileAttachment mdl = new eFileAttachment();
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            var IsMsgRequired = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "IsMsgRequiredInHouseCommittee", null);
            var IsMailRequired = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "IsMailRequiredInHouseCommittee", null);

            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.eFilePath))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePath));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.eFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                        }

                    }

                }
                else
                {
                    return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }


            #region Upload Word File
            string url1Word = "~/ePaper/FileWord";
            string directory1Word = Server.MapPath(url1Word);

            if (Directory.Exists(directory))
            {
                string[] savedFileNameWord = Directory.GetFiles(Server.MapPath(url1Word));
                if (savedFileNameWord.Length > 0)
                {
                    string SourceFile = savedFileNameWord[0];
                    foreach (string page in savedFileNameWord)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.eFilePathWord))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePathWord));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.eFilePathWord = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                        }

                    }

                }
                else
                {
                    mdl.eFilePathWord = null;
                    return Json("Please select word file", JsonRequestBehavior.AllowGet);
                }

            }
            #endregion

            mdl.SplitFileCount = 0;
            mdl.CreatedBy = new Guid(CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.eFileNameId = Guid.NewGuid();
            model.ParentId = fileID;

            // i am receiver
            mdl.ToDepartmentID = CurrentSession.DeptID; //"HPD0001";
            int OfficeId = 0;
            int.TryParse(CurrentSession.OfficeId, out OfficeId);
            mdl.ToOfficeCode = OfficeId;// int.Parse(CurrentSession.OfficeId);// 0;
            ///


            // I am Sender
            mdl.DepartmentId = model.DepartmentId;//CurrentSession.DeptID; AddeFileAttachments
            string _lstOffice = Convert.ToString(lstOffice);
            if (!string.IsNullOrEmpty(_lstOffice))
            {
                mdl.OfficeCode = int.Parse(_lstOffice);
            }
            else
            {
                mdl.OfficeCode = 0;
            }
            //////

            if (model.PType == "4")
            {
                mdl.PType = "R";
            }
            else
            {
                mdl.PType = "O"; //model.PType;
            }


            mdl.DocumentTypeId = model.DocumentTypeId;
            mdl.Description = model.Description;
            mdl.PaperNature = model.PaperNature;
            mdl.PaperNatureDays = model.PaperNatureDays;
            mdl.Title = model.Title;
            mdl.PaperRefNo = model.PaperRefNo;
            mdl.RecvDetails = model.RecvDetails;
            mdl.RevedOption = model.RevedOption;
            mdl.ToRecvDetails = model.RecvDetails;
            mdl.PaperTicketValue = model.PaperTicketValue;
            mdl.PaperNo = model.PaperNo;
            mdl.DispatchtoWhom = model.DispatchtoWhom;
            mdl.eFileID = model.eFileID;
            mdl.CountsAPS = model.CountsAPS;
            mdl.ItemTypeName = model.ItemTypeName;
            mdl.DraftId = model.DraftId;
            mdl.ItemNumber = model.ItemNumber;
            mdl.ReplyRefNo = model.ReplyRefNo;
            mdl.CommitteeId = model.CommitteeId;
            mdl.CommitteeName = model.CommitteeName;
            if (model.ToDepIds != null)
            {
                mdl.ToDepIds = model.ToDepIds.TrimEnd();
            }
            if (model.CCDepIds != null)
            {
                mdl.CCDepIds = model.CCDepIds.TrimEnd(',');
            }
            if (model.ToMemIds != null)
            {
                mdl.ToMemIds = model.ToMemIds.TrimEnd(',');
            }
            if (model.CCMemIds != null)
            {
                mdl.CCMemIds = model.CCMemIds.TrimEnd(',');
            }
            if (model.ToOtherIds != null)
            {
                mdl.ToOtherIds = model.ToOtherIds.TrimEnd(',');
            }
            if (model.CCOtherIds != null)
            {
                mdl.CCOtherIds = model.CCOtherIds.TrimEnd(',');
            }
            if (model.ToListComplN != null)
            {
                mdl.ToListComplN = model.ToListComplN.TrimEnd(',');
            }

            if (model.CCListComplN != null)
            {
                mdl.CCListComplN = model.CCListComplN.TrimEnd(',');
            }
            if (model.ToListComplIds != null)
            {
                mdl.ToListComplIds = model.ToListComplIds.TrimEnd(',');
            }
            if (model.CCListComplIds != null)
            {
                mdl.CCListComplIds = model.CCListComplIds.TrimEnd(',');
            }

            mdl.ItemTypeName = model.ItemTypeName;
            mdl.ToDepartmentID = "HPD0001";
            mdl.IpAddress = (string)Helper.ExecuteService("eFile", "GetIPAddress", "");
            mdl.MacAddress = (string)Helper.ExecuteService("eFile", "GetMACAddress", "");

            eFileAttachment res = (eFileAttachment)Helper.ExecuteService("eFile", "SentDept", mdl);
            mUsers objM = new mUsers();
            Guid newguid = new Guid(CurrentSession.UserID);
            objM.UserId = newguid;
            var ResultForSend = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserIDSMS", objM);//for getting user phoneno from userid
            string reffno = res.PaperRefNo;
            var message = "Kindly Take Necessary action w.r.t e-Vidhan reference no." + reffno + " regarding" + res.Description + "by" + res.AllDeptName + "to" + res.CommitteeName + "on" + res.ReceivedDate;

            //to send msg to mupltiple contact groups
            List<string> phoneNumbers = new List<string>();
            var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByNewFixed", new RecipientGroupMember { DepartmentCode = res.DepartmentId }) as List<RecipientGroupMember>;

            if (contactGroupMembers != null)
            {
                foreach (var item in contactGroupMembers)
                {
                    if (item.MobileNo.Contains(','))
                    {
                        string[] numbers = item.MobileNo.Split(',');
                        foreach (var mb in numbers)
                        {
                            phoneNumbers.Add(mb);
                        }
                    }
                    else
                        phoneNumbers.Add(item.MobileNo);
                }
            }
            //if (!string.IsNullOrEmpty(model.PhoneNumbers))
            //    phoneNumbers = model.PhoneNumbers.Split(',').Select(s => s).ToList();


            //code for email
            if (IsMailRequired.SettingValue == "True")
            {
                if (ResultForSend.EmailId != "" || ResultForSend.EmailId != null)
                {
                    Email.API.CustomMailMessage emailmsg = new Email.API.CustomMailMessage();
                    emailmsg._body = message;
                    emailmsg._isBodyHtml = true;
                    emailmsg._from = ResultForSend.EmailId;
                    emailmsg._subject = "Pendency Reply";
                    emailmsg._toList.Add(ResultForSend.EmailId);//sending mail to same person
                    Notification.Send(false, true, null, emailmsg);
                }
            }

            //for sending sms..
            if (IsMsgRequired.SettingValue == "True")
            {
                if (ResultForSend.MobileNo != "" || ResultForSend.MobileNo != null)
                {
                    SMSMessageList smsMessage = new SMSMessageList();
                    foreach (var item in phoneNumbers)
                    {
                        smsMessage.MobileNo.Add(item);
                    }
                    Service1 smsServiceClient = new Service1();
                    smsMessage.ModuleActionID = -1;
                    smsMessage.UniqueIdentificationID = -1;
                    smsMessage.SMSText = message;
                    //smsMessage.MobileNo.Add(ResultForSend.MobileNo);
                    Notification.Send(true, false, smsMessage, null);
                }
            }

            //Notification.Send(true, true, smsMessage, emailmsg);

            if (res != null)//logic changed and temporary ....not logicallycorrect by robin
            {
                return Json("Send Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            }



        }

        public ActionResult GetProceddingslist(int pageId = 1, int pageSize = 25)
        {
            COmmitteeProceeding mdl = new COmmitteeProceeding();
            //mdl.DeptID = CurrentSession.DeptID;
            string UserID = CurrentSession.UserID;
            string[] strngBID = new string[2];
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            string ComitteeId = value[0];
            var cprocedding = (List<COmmitteeProceeding>)Helper.ExecuteService("eFile", "GetCommitteeProoceeding_ByCommittee", ComitteeId);
            ViewBag.PageSize = pageSize;
            List<COmmitteeProceeding> pagedRecord = new List<COmmitteeProceeding>();
            //if (cprocedding != null)
            //{
            //    if (pageId == 1)
            //    {
            //        pagedRecord = cprocedding.Take(pageSize).ToList();
            //    }
            //    else
            //    {
            //        int r = (pageId - 1) * pageSize;
            //        //  pagedRecord = cprocedding.ToList();
            //        pagedRecord = cprocedding.Skip(r).Take(pageSize).ToList();
            //    }

            //}
            pagedRecord = cprocedding.ToList();
            mdl.My_ComProcList = pagedRecord;
            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(cprocedding.Count, pageId, pageSize);
            return PartialView("_ProceedingList", mdl);
        }
        [HttpPost]
        public JsonResult ProceddingFile_ToDraftPapers(string ProcIds)
        {

            // eFileAttachment model = new eFileAttachment();
            COmmitteeProceeding model = new COmmitteeProceeding();
            string[] d = ProcIds.Split(',');
            int[] pcds = new int[d.Length - 1];

            for (int i = 0; i < d.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(d[i].ToString()))
                {
                    pcds[i] = Convert.ToInt32(d[i]);
                }
            }

            model.ProcIds = pcds;
            model.Aadharid = CurrentSession.AadharId;
            model.IpAddress = (string)Helper.ExecuteService("eFile", "GetIPAddress", "");
            model.MacAddress = (string)Helper.ExecuteService("eFile", "GetMACAddress", "");

            //var value = (string[])Helper.ExecuteService("File", "GetBranchByCommitteeId", strngBID);
            //string ComitteeId = value[0];
            int res = (int)Helper.ExecuteService("eFile", "Procedding_Draftpapers", model);

            if (res == 0)
            {
                return Json("Proceeding Added in Draft List Successfully", JsonRequestBehavior.AllowGet);
            }
            else if (res == 2)
            {
                return Json("Proceeding File Exist in Draft List", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }


        //For efile new by Lakshay
        [HttpPost]
        public JsonResult nGeteFileByCommitteeId(string CommitteeId)
        {
            List<eFileList> FileListsAccToComId = new List<eFileList>();

            eFileAttachment pdocuments = new eFileAttachment();
            try
            {
                SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
                eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "nGeteFilesByCommitteeId", CommitteeId);
                FileListsAccToComId.AddRange(eList.eFileLists);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(FileListsAccToComId, JsonRequestBehavior.AllowGet);
        }


        //For Correspondence/Letters New by Lakshay
        public ActionResult NewMethodDraft()
        {
            return PartialView("NewMethodDraft");
        }
        public ActionResult NewMethodDraftDept()
        {
            return PartialView("NewMethodDraftDept");
        }
        //public PartialViewResult GetAllHouseComFiles(int pageId = 1, int pageSize = 25)
        //{
        //    pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
        //    List<tCommittee> CommitteeList = new List<tCommittee>();
        //    eFileAttachment documents = new eFileAttachment();
        //    tPaperLaidV newmodel = new tPaperLaidV();           
        //    mdl.CurrentDeptId = CurrentSession.DeptID;
        //    if (mdl.CurrentDeptId == "HPD0001")
        //    {
        //        mdl.CurrentDeptId = "";
        //    }
        //    List<pHouseCommitteeFiles> ListModel = new List<pHouseCommitteeFiles>();           
        //    ListModel = (List<pHouseCommitteeFiles>)Helper.ExecuteService("eFile", "GetCustomCommFiles", mdl);
        //    if (ListModel != null) 
        //    {
        //        ListModel.OrderBy(a => a.SelectedPaperDate);   
        //    }

        //    mdl.HouseCommAllFilesList = ListModel;
        //    //////////////////////////Server Side Paging GetDraftList///////////////////////////////////
        //    //ViewBag.PageSize = pageSize;
        //    //ViewBag.CurrentPage = pageId;
        //    //ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
        //    //////////////////////////Server Side Paging///////////////////////////////////
        //    return PartialView("_ShowHouseComAllFiles", mdl);


        //}
        public PartialViewResult GetAllHouseComFiles(int pageId = 1, int pageSize = 25)
        {
            pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
            List<tCommittee> CommitteeList = new List<tCommittee>();
            List<pHouseCommitteeFiles> ListModel = new List<pHouseCommitteeFiles>();
            eFileAttachment documents = new eFileAttachment();
            tPaperLaidV newmodel = new tPaperLaidV();
            mdl.CurrentDeptId = CurrentSession.DeptID;
            //mdl.CurrentBranchId =Convert.ToInt32(CurrentSession.BranchId);
            if (mdl.CurrentDeptId == "HPD0001")
            {
                mdl.CurrentDeptId = "";
                List<tCommittee> CommitteeListNew = new List<tCommittee>();
                tPaperLaidV model = new tPaperLaidV();
                mBranches modelBranch = new mBranches();
                modelBranch.UserId = Guid.Parse(CurrentSession.UserID);
                model.BranchesList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "GBranchList", modelBranch);
                foreach (var i in model.BranchesList)
                {
                    string[] strngBID = new string[2];
                    strngBID[0] = Convert.ToString(i.BranchId);
                    List<tCommittee> CommitteeList1 = new List<tCommittee>();
                    CommitteeList1 = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("eFile", "nGetCommiteeIDAndNameByBranchID", strngBID);
                    CommitteeList.AddRange(CommitteeList1);
                }
                mdl.tCommitteeList = CommitteeList;
                ListModel = (List<pHouseCommitteeFiles>)Helper.ExecuteService("eFile", "GetCustomCommFiles", mdl);
            }

            else
            {
                //List<pHouseCommitteeFiles> ListModel = new List<pHouseCommitteeFiles>();
                ListModel = (List<pHouseCommitteeFiles>)Helper.ExecuteService("eFile", "GetCustomCommFiles", mdl);
            }

            if (ListModel != null)
            {
                ListModel.OrderBy(a => a.CreatedDate);
            }

            mdl.HouseCommAllFilesList = ListModel;
            //////////////////////////Server Side Paging GetDraftList///////////////////////////////////
            //ViewBag.PageSize = pageSize;
            //ViewBag.CurrentPage = pageId;
            //ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            return PartialView("_ShowHouseComAllFiles", mdl);


        }

        public JsonResult GetSubFilesByParentId(string ParentId)
        {
            SBL.DomainModel.Models.eFile.pHouseCommitteeFiles mdl = new SBL.DomainModel.Models.eFile.pHouseCommitteeFiles();
            List<SBL.DomainModel.Models.eFile.pHouseCommitteeFiles> pHouseCommitteeFiles = new List<SBL.DomainModel.Models.eFile.pHouseCommitteeFiles>();
            List<SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles> pHouseCommitteeSubFilesList = new List<SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles>();
            mdl.ParentId = Convert.ToInt16(ParentId.Trim());
            mdl.CurrentDeptId = CurrentSession.DeptID;
            if (mdl.CurrentDeptId == "HPD0001")
            {
               
                pHouseCommitteeFiles = (List<SBL.DomainModel.Models.eFile.pHouseCommitteeFiles>)Helper.ExecuteService("eFile", "GetSubCommFilesByParentId", mdl);
            }
            else
            {
                mdl.CurrentDeptId = "";
                pHouseCommitteeFiles = (List<SBL.DomainModel.Models.eFile.pHouseCommitteeFiles>)Helper.ExecuteService("eFile", "GetSubCommFilesByParentId", mdl);
            }

            //pHouseCommitteeSubFilesList = (List<SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles>)Helper.ExecuteService("eFile", "GetSubCommFilesByParentId", mdl);
            
            return Json(pHouseCommitteeFiles, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult ComposeNewLetter()
        {
            List<tCommittee> CommitteeList = new List<tCommittee>();
            tPaperLaidV model = new tPaperLaidV();
            mBranches modelBranch = new mBranches();
            modelBranch.UserId = Guid.Parse(CurrentSession.UserID);
            model.BranchesList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "GBranchList", modelBranch);
            string[] str1 = new string[4];
            str1[0] = CurrentSession.DeptID;
            str1[1] = CurrentSession.OfficeId;
            string CommitteeId = "";
            foreach (var i in model.BranchesList)
            {
                str1[2] = Convert.ToString(i.BranchId);
                string[] strngBID = new string[2];
                strngBID[0] = Convert.ToString(i.BranchId);
                List<tCommittee> CommitteeList1 = new List<tCommittee>();
                CommitteeList1 = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("eFile", "nGetCommiteeIDAndNameByBranchID", strngBID);
                CommitteeList.AddRange(CommitteeList1);

            }
            ViewBag.CommitteeList = new SelectList(CommitteeList, "CommitteeId", "CommitteeName", null);
            foreach (var item in CommitteeList)
            {
                CommitteeId = Convert.ToString(item.CommitteeId);
                str1[3] = CommitteeId;
                SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
                eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "NeFileList", str1);
                var eFileList = eList.eFileLists;
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
                List<mDepartment> DeptList = new List<mDepartment>();
                eFileAttachment documents = new eFileAttachment();
                foreach (var Sdeptid in eList.eFileLists)
                {
                    string SelectedDeptId = Sdeptid.SelectedDeptId;
                    mDepartment mdep = new mDepartment();
                    mdep.deptId = SelectedDeptId;
                    documents.mDepartment = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
                    DeptList.AddRange(documents.mDepartment);

                }
                List<mDepartment> DepartmentList = new List<mDepartment>();
                DepartmentList = DeptList.Distinct().ToList();
                ViewBag.ListOfCodes = new SelectList(DepartmentList, "deptId", "deptname", null);
            }
            ///Get All eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);

            //Get All Items
            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetFileItemList", null);
            ViewBag.FileItem = new SelectList(_ListItems, "ID", "ItemTypeName", null);
            return PartialView("_tComposeNewLetter");
        }
        [HttpPost]
        public JsonResult GetDeptnItemByFileId(string eFileID)
        {
            pHouseCommitteeFiles documents = new pHouseCommitteeFiles();
            List<eFileList> ListsAccToeFileId = new List<eFileList>();

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            try
            {
                eFileViewModel model2 = new eFileViewModel();
                model2.eFileID = Convert.ToInt32(eFileID);
                eList = (eFileListAll)Helper.ExecuteService("eFile", "GetFileDetailsByeFileId", model2.eFileID);

                ListsAccToeFileId.AddRange(eList.eFileLists);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(ListsAccToeFileId, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveInformation(List<pHouseCommitteeFiles> LetterList)
        {
            int count = 0;
            foreach (var item in LetterList)
            {
                pHouseCommitteeFiles obj = new pHouseCommitteeFiles();
                obj.eFileID = item.eFileID;
                obj.eFileName = item.eFileName;
                obj.CommitteeId = item.CommitteeId;
                obj.CommitteeName = item.CommitteeName;
                obj.ByDepartmentId = CurrentSession.DeptID;
                obj.ToDepartmentID = item.ToDepartmentID;
                obj.ToDepartmentName = item.ToDepartmentName;
                obj.ItemId = item.ItemId;
                obj.ItemName = item.ItemName;
                obj.Title = item.Title;
                obj.SendStatus = item.SendStatus;

                obj.SelectedPaperDate = item.SelectedPaperDate;
                obj.CreatedBy = new Guid(CurrentSession.UserID);
                obj.ModifiedBy = new Guid(CurrentSession.UserID);
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                //obj.eFileNameId = Guid.NewGuid();
                //model.Approve = 1;
                obj.ParentId = obj.ParentId == 0 ? 0 : obj.ParentId;
                obj.PaperNature = item.PaperNature;
                obj.DocumentTypeId = 10;

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                string tCommName = obj.CommitteeName.Replace(" ", string.Empty);
                string tItemName = obj.ItemName.Replace(" ", string.Empty);

                string directory = FileSettings.SettingValue + "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Letter/";
                string Dirname = "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Letter/";
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }
                string fileName = item.eFilePath;
                if (item.eFilePath != null)
                {
                    string sourcedirectory = Server.MapPath("~/LOBTemp/");
                    string sourceFile = System.IO.Path.Combine(sourcedirectory, fileName);
                    string destFile = System.IO.Path.Combine(directory, fileName);
                    string FullFileNamePath = System.IO.Path.Combine(Dirname, fileName);
                    System.IO.File.Copy(sourceFile, destFile, true);
                    //Delete File from temp location
                    System.IO.File.Delete(sourceFile);
                    obj.eFilePath = FullFileNamePath;
                }
                string directory1 = FileSettings.SettingValue + "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Annexures/";
                string Dirname1 = "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Annexures/";
                if (!System.IO.Directory.Exists(directory1))
                {
                    System.IO.Directory.CreateDirectory(directory1);
                }
                string FullAnnexNamePath = "";
                if (item.AnnexPdfPath != null)
                {
                    string[] s = item.AnnexPdfPath.Split(',');
                    for (int i = 0; i < s.Length - 1; i++)
                    {
                        string AnnexfileName = s[i].Trim(' ');
                        //string AnnexfileName = item.AnnexPdfPath;
                        string source = Server.MapPath("~/LOBTemp/");
                        string AnnexsourceFile = System.IO.Path.Combine(source, AnnexfileName);
                        string AnnexdestFile = System.IO.Path.Combine(directory1, AnnexfileName);
                        //FullAnnexNamePath = FullAnnexNamePath + System.IO.Path.Combine(Dirname1, AnnexfileName) + ",";
                        FullAnnexNamePath = System.IO.Path.Combine(Dirname1, AnnexfileName) + "," + FullAnnexNamePath;
                        System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
                        System.IO.File.Delete(AnnexsourceFile);
                    }

                }


                obj.AnnexPdfPath = FullAnnexNamePath;
                obj.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                obj.CurrentBranchId = Convert.ToInt32(CurrentSession.BranchId);
                int res = (int)Helper.ExecuteService("eFile", "SaveDraftedPaper", obj);




            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult AddToComposedLetter(string ParentId)
        {

            pHouseCommitteeFiles model = new pHouseCommitteeFiles();
            SBL.DomainModel.Models.eFile.pHouseCommitteeFiles mdl = new SBL.DomainModel.Models.eFile.pHouseCommitteeFiles();
            List<SBL.DomainModel.Models.eFile.pHouseCommitteeFiles> pHouseCommitteeFiles = new List<SBL.DomainModel.Models.eFile.pHouseCommitteeFiles>();
            List<SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles> pHouseCommitteeSubFilesList = new List<SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles>();
            mdl.ParentId = Convert.ToInt16(ParentId.Trim());
            pHouseCommitteeFiles = (List<SBL.DomainModel.Models.eFile.pHouseCommitteeFiles>)Helper.ExecuteService("eFile", "GetDetailsByParentId", mdl);
            foreach (var item in pHouseCommitteeFiles)
            {
                model.CommitteeId = item.CommitteeId;
                model.CurrentDeptId = item.ToDepartmentID;
                model.eFileID = item.eFileID;
                SBL.DomainModel.Models.eFile.eFileList efile = (SBL.DomainModel.Models.eFile.eFileList)Helper.ExecuteService("eFile", "GeteFile", model.eFileID);
                //Removing <p> tag from string
                // string eFileSubject = Regex.Replace(efile.eFileSubject, @"<[^>]+>| ", "").Trim();
                if (efile != null)
                {
                    string ReplaceString = efile.eFileSubject.Replace("<p>", "");
                    string eFileSubject = ReplaceString.Replace("</p>", "");
                    string info = System.Text.RegularExpressions.Regex.Replace(efile.eFileSubject, "<[^>]*>", string.Empty);
                    model.eFileSubject = info;
                }



                model.ItemId = item.ItemId;
                model.Description = item.Title;
            }

            //Get All Committees
            List<tCommittee> CommitteeList = new List<tCommittee>();
            eFileAttachment documents = new eFileAttachment();
            documents.tCommittee = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteList", null);
            model.CommitteeList = documents.tCommittee;

            //Get All eFiles
            List<eFileList> eFileLists = new List<eFileList>();
            eFileListAll eList = new eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "NAlleFileList", null);
            model.eFileLists = eList.eFileLists;

            //Get All Departement
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            model.DepartmentList = deptList;

            ///Get All eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            model.eFilePaperTypeList = _ListeFilePaperType;

            //Get All Items
            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetFileItemList", null);
            model.mCommitteeReplyItemTypeList = _ListItems;
            model.ParentId = Convert.ToInt16(ParentId);
            ViewBag.ParentId = ParentId;
            ViewBag.UserID = CurrentSession.DeptID;
            return PartialView("AddToComposedLetter", model);
        }
        //[HttpPost]
        //public JsonResult SaveToComposedLetter(List<pHouseCommitteeFiles> LetterList)
        //{
        //    int count = 0;
        //    foreach (var item in LetterList)
        //    {
        //        pHouseCommitteeSubFiles obj = new pHouseCommitteeSubFiles();
        //        obj.eFileID = item.eFileID;
        //        obj.eFileName = item.eFileName;
        //        obj.CommitteeId = item.CommitteeId;
        //        obj.CommitteeName = item.CommitteeName;
        //        obj.ByDepartmentId = CurrentSession.DeptID;
        //        obj.ToDepartmentID = item.ToDepartmentID;
        //        obj.ToDepartmentName = item.ToDepartmentName;
        //        obj.ItemId = item.ItemId;
        //        obj.ItemName = item.ItemName;
        //        obj.Title = item.Title;
        //        obj.SendStatus = item.SendStatus;

        //        obj.PendencyIDs = item.PendencyIDs;

        //        obj.SelectedPaperDate = item.SelectedPaperDate;
        //        obj.CreatedBy = new Guid(CurrentSession.UserID);
        //        obj.ModifiedBy = new Guid(CurrentSession.UserID);
        //        obj.CreatedDate = DateTime.Now;
        //        obj.ModifiedDate = DateTime.Now;
        //        //obj.eFileNameId = Guid.NewGuid();
        //        //model.Approve = 1;
        //        obj.ParentId = Convert.ToInt16(item.ParentId);
        //        obj.PaperNature = item.PaperNature;
        //        obj.DocumentTypeId = 10;


        //        //For Letters
        //        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

        //        string tCommName = obj.CommitteeName.Replace(" ", string.Empty);
        //        string tItemName = obj.ItemName.Replace(" ", string.Empty);


        //        string directory = FileSettings.SettingValue + "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Letter/";
        //        string Dirname = "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Letter/";
        //        if (!System.IO.Directory.Exists(directory))
        //        {
        //            System.IO.Directory.CreateDirectory(directory);
        //        }
        //        string fileName = item.eFilePath;
        //        if (item.eFilePath != null)
        //        {
        //            string sourcedirectory = Server.MapPath("~/LOBTemp/");
        //            string sourceFile = System.IO.Path.Combine(sourcedirectory, fileName);
        //            string destFile = System.IO.Path.Combine(directory, fileName);
        //            string FullFileNamePath = System.IO.Path.Combine(Dirname, fileName);
        //            System.IO.File.Copy(sourceFile, destFile, true);
        //            obj.eFilePath = FullFileNamePath;
        //            //Delete File from temp location
        //            System.IO.File.Delete(sourceFile);
        //        }


        //        //For Reply
        //        int ItemIdIs = 0;
        //        if (item.ItemId.HasValue)
        //        {
        //            ItemIdIs = item.ItemId.Value;
        //        }



        //        if (ItemIdIs == 10)
        //        {
        //            if (item.ReplyPdfPath != null)
        //            {
        //                obj.ReplyPdfPath = item.ReplyPdfPath;
        //            }

        //        }

        //        else if (ItemIdIs == 7)
        //        {
        //            if (item.ReplyPdfPath != null)
        //            {
        //                obj.ReplyPdfPath = item.ReplyPdfPath;
        //            }

        //        }
        //        else
        //        // if (ItemIdIs != 10 || ItemIdIs != 7)
        //        //if ((ItemIdIs != 10) || (ItemIdIs != 7))
        //        {
        //            string ndirectory1 = FileSettings.SettingValue + "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Reply/";
        //            string nDirname1 = "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Reply/";
        //            if (!System.IO.Directory.Exists(ndirectory1))
        //            {
        //                System.IO.Directory.CreateDirectory(ndirectory1);
        //            }

        //            if (item.ReplyPdfPath != null)
        //            {
        //                string fileName1 = item.ReplyPdfPath;
        //                string sourcedirectory = Server.MapPath("~/LOBTemp/");
        //                string sourceFile = System.IO.Path.Combine(sourcedirectory, fileName1);
        //                string destFile = System.IO.Path.Combine(ndirectory1, fileName1);
        //                string FullFileNamePath = System.IO.Path.Combine(nDirname1, fileName1);
        //                System.IO.File.Copy(sourceFile, destFile, true);
        //                obj.ReplyPdfPath = FullFileNamePath;
        //                //Delete File from temp location
        //                System.IO.File.Delete(sourceFile);
        //            }

        //        }



        //        //For Annexures
        //        string directory1 = FileSettings.SettingValue + "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Annexures/";
        //        string Dirname1 = "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Annexures/";
        //        if (!System.IO.Directory.Exists(directory1))
        //        {
        //            System.IO.Directory.CreateDirectory(directory1);
        //        }


        //        string FullAnnexNamePath = string.Empty;
        //        if (item.AnnexPdfPath != null)
        //        {
        //            if (CurrentSession.DeptID == "HPD0001")//vs user
        //            {
        //                string[] s = item.AnnexPdfPath.Split(',');
        //                for (int i = 0; i < s.Length - 1; i++)
        //                {
        //                    string AnnexfileName = s[i].Trim(' ');
        //                    // string AnnexfileName = item.AnnexPdfPath;
        //                    string source = Server.MapPath("~/LOBTemp/");
        //                    string AnnexsourceFile = System.IO.Path.Combine(source, AnnexfileName);
        //                    string AnnexdestFile = System.IO.Path.Combine(directory1, AnnexfileName);
        //                    FullAnnexNamePath = System.IO.Path.Combine(Dirname1, AnnexfileName) + "," + FullAnnexNamePath;
        //                    System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
        //                    System.IO.File.Delete(AnnexsourceFile);
        //                }
        //                obj.AnnexPdfPath = FullAnnexNamePath;
        //            }
        //            //New for attaching annexures only while sending information.
        //            else if (CurrentSession.DeptID != "HPD0001")
        //            {
        //                if (ItemIdIs == 10)
        //                {
        //                    if (obj.PaperNature == 3)
        //                    {
        //                        string source = Server.MapPath("~/LOBTemp/");
        //                        string AnnexsourceFile = System.IO.Path.Combine(source, item.AnnexPdfPath);
        //                        string AnnexdestFile = System.IO.Path.Combine(directory1, item.AnnexPdfPath);
        //                        FullAnnexNamePath = System.IO.Path.Combine(Dirname1, item.AnnexPdfPath) + "," + FullAnnexNamePath;
        //                        System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
        //                        System.IO.File.Delete(AnnexsourceFile);
        //                        obj.AnnexPdfPath = FullAnnexNamePath;
        //                    }
        //                    else
        //                    {
        //                        obj.AnnexPdfPath = item.AnnexPdfPath;
        //                    }
        //                }

        //                else if (ItemIdIs == 7)
        //                {
        //                    if (obj.PaperNature == 3)
        //                    {
        //                        string source = Server.MapPath("~/LOBTemp/");
        //                        string AnnexsourceFile = System.IO.Path.Combine(source, item.AnnexPdfPath);
        //                        string AnnexdestFile = System.IO.Path.Combine(directory1, item.AnnexPdfPath);
        //                        FullAnnexNamePath = System.IO.Path.Combine(Dirname1, item.AnnexPdfPath) + "," + FullAnnexNamePath;
        //                        System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
        //                        System.IO.File.Delete(AnnexsourceFile);
        //                        obj.AnnexPdfPath = FullAnnexNamePath;
        //                    }
        //                    else
        //                    {
        //                        obj.AnnexPdfPath = item.AnnexPdfPath;
        //                    }
        //                }
        //                else
        //                {

        //                    string source = Server.MapPath("~/LOBTemp/");
        //                    string AnnexsourceFile = System.IO.Path.Combine(source, item.AnnexPdfPath);
        //                    string AnnexdestFile = System.IO.Path.Combine(directory1, item.AnnexPdfPath);
        //                    FullAnnexNamePath = System.IO.Path.Combine(Dirname1, item.AnnexPdfPath) + "," + FullAnnexNamePath;
        //                    System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
        //                    System.IO.File.Delete(AnnexsourceFile);
        //                    obj.AnnexPdfPath = FullAnnexNamePath;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            obj.AnnexPdfPath = string.Empty;
        //        }

        //        //For Word File
        //        string Docdirectory = FileSettings.SettingValue + "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/DocFiles/";
        //        string DocDirname = "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/DocFiles/";
        //        if (!System.IO.Directory.Exists(Docdirectory))
        //        {
        //            System.IO.Directory.CreateDirectory(Docdirectory);
        //        }
        //        string FullDocNamePath = string.Empty;
        //        if (item.DocFilePath != null)
        //        {
        //            if (CurrentSession.DeptID == "HPD0001")//vs user
        //            {


        //                string[] s = item.DocFilePath.Split(',');
        //                for (int i = 0; i < s.Length - 1; i++)
        //                {
        //                    string DocfileName = s[i].Trim(' ');
        //                    // string AnnexfileName = item.AnnexPdfPath;
        //                    string source = Server.MapPath("~/LOBTemp/");
        //                    string WordsourceFile = System.IO.Path.Combine(source, DocfileName);
        //                    string WorddestFile = System.IO.Path.Combine(Docdirectory, DocfileName);
        //                    FullDocNamePath = System.IO.Path.Combine(DocDirname, DocfileName) + "," + FullDocNamePath;
        //                    System.IO.File.Copy(WordsourceFile, WorddestFile, true);
        //                    System.IO.File.Delete(WordsourceFile);
        //                }
        //                obj.DocFilePath = FullDocNamePath;
        //            }

        //            else
        //            {

        //                if (ItemIdIs == 10)
        //                {
        //                    obj.DocFilePath = item.DocFilePath;
        //                    obj.DocFilesNameByUser = item.DocFilesNameByUser;
        //                }
        //                else if (ItemIdIs == 7)
        //                {
        //                    obj.DocFilePath = item.DocFilePath;
        //                    obj.DocFilesNameByUser = item.DocFilesNameByUser;
        //                }
        //                else
        //                {
        //                    string DocfileName = item.DocFilePath;
        //                    // string AnnexfileName = item.AnnexPdfPath;
        //                    string source = Server.MapPath("~/LOBTemp/");
        //                    string WordsourceFile = System.IO.Path.Combine(source, DocfileName);
        //                    string WorddestFile = System.IO.Path.Combine(Docdirectory, DocfileName);
        //                    FullDocNamePath = System.IO.Path.Combine(DocDirname, DocfileName);
        //                    System.IO.File.Copy(WordsourceFile, WorddestFile, true);
        //                    System.IO.File.Delete(WordsourceFile);
        //                    obj.DocFilePath = FullDocNamePath;
        //                    obj.DocFilesNameByUser = item.DocFilesNameByUser;

        //                }
        //            }



        //        }


        //        //////


        //        //if (FullDocNamePath == "")
        //        //{
        //        //    obj.DocFilePath = null;
        //        //}
        //        //else
        //        //{
        //        //    obj.DocFilePath = FullDocNamePath;
        //        //}
        //        if (item.DocFilesNameByUser != null)
        //        {
        //            string a = "";
        //            string[] SplitIt = item.DocFilesNameByUser.Split(',');
        //            int ai = SplitIt.Length - 2;
        //            for (int i = ai; i >= 0; i--)
        //            {
        //                a += SplitIt[i] + ",";
        //            }
        //            obj.DocFilesNameByUser = a;
        //        }
        //        else
        //        {
        //            obj.DocFilesNameByUser = item.DocFilesNameByUser;
        //        }



        //        //  obj.AnnexPdfPath = FullAnnexNamePath;
        //        if (item.AnnexFilesNameByUser != null)
        //        {
        //            string ab = "";
        //            string[] SplitIt1 = item.AnnexFilesNameByUser.Split(',');
        //            int ai1 = SplitIt1.Length - 2;
        //            for (int i = ai1; i >= 0; i--)
        //            {
        //                ab += SplitIt1[i] + ",";
        //            }
        //            obj.AnnexFilesNameByUser = ab;
        //        }
        //        else
        //        {
        //            obj.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
        //        }



        //        int res = (int)Helper.ExecuteService("eFile", "SaveToComposedLetter", obj);


        //    }
        //    return Json(count, JsonRequestBehavior.AllowGet);
        //}


        [HttpPost]
        public JsonResult SaveToComposedLetter(List<pHouseCommitteeFiles> LetterList)
        {
            int count = 0;
            foreach (var item in LetterList)
            {
                pHouseCommitteeSubFiles obj = new pHouseCommitteeSubFiles();
                obj.eFileID = item.eFileID;
                obj.eFileName = item.eFileName;
                obj.CommitteeId = item.CommitteeId;
                obj.CommitteeName = item.CommitteeName;
                obj.ByDepartmentId = CurrentSession.DeptID;
                obj.ToDepartmentID = item.ToDepartmentID;
                obj.ToDepartmentName = item.ToDepartmentName;
                obj.ItemId = item.ItemId;
                obj.ItemName = item.ItemName;
                obj.Title = item.Title;
                obj.SendStatus = item.SendStatus;

                obj.PendencyIDs = item.PendencyIDs;

                obj.SelectedPaperDate = item.SelectedPaperDate;
                obj.CreatedBy = new Guid(CurrentSession.UserID);
                obj.ModifiedBy = new Guid(CurrentSession.UserID);
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                //obj.eFileNameId = Guid.NewGuid();
                //model.Approve = 1;
                obj.ParentId = Convert.ToInt16(item.ParentId);
                obj.PaperNature = item.PaperNature;
                obj.DocumentTypeId = 10;


                //For Letters
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                string tCommName = obj.CommitteeName.Replace(" ", string.Empty);
                string tItemName = obj.ItemName.Replace(" ", string.Empty);


                string directory = FileSettings.SettingValue + "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Letter/";
                string Dirname = "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Letter/";
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }
                string fileName = item.eFilePath;
                if (item.eFilePath != null)
                {
                    string sourcedirectory = Server.MapPath("~/LOBTemp/");
                    string sourceFile = System.IO.Path.Combine(sourcedirectory, fileName);
                    string destFile = System.IO.Path.Combine(directory, fileName);
                    string FullFileNamePath = System.IO.Path.Combine(Dirname, fileName);
                    System.IO.File.Copy(sourceFile, destFile, true);
                    obj.eFilePath = FullFileNamePath;
                    //Delete File from temp location
                    System.IO.File.Delete(sourceFile);
                }


                //For Reply
                int ItemIdIs = 0;
                if (item.ItemId.HasValue)
                {
                    ItemIdIs = item.ItemId.Value;
                }



                if (ItemIdIs == 10)
                {
                    if (item.ReplyPdfPath != null)
                    {
                        obj.ReplyPdfPath = item.ReplyPdfPath;
                    }

                }

                else if (ItemIdIs == 7)
                {
                    if (item.ReplyPdfPath != null)
                    {
                        obj.ReplyPdfPath = item.ReplyPdfPath;
                    }

                }
                else
                // if (ItemIdIs != 10 || ItemIdIs != 7)
                //if ((ItemIdIs != 10) || (ItemIdIs != 7))
                {
                    string ndirectory1 = FileSettings.SettingValue + "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Reply/";
                    string nDirname1 = "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Reply/";
                    if (!System.IO.Directory.Exists(ndirectory1))
                    {
                        System.IO.Directory.CreateDirectory(ndirectory1);
                    }

                    //if (item.ReplyPdfPath != null)
                    //{
                    //    string fileName1 = item.ReplyPdfPath;
                    //    string sourcedirectory = Server.MapPath("~/LOBTemp/");
                    //    string sourceFile = System.IO.Path.Combine(sourcedirectory, fileName1);
                    //    string destFile = System.IO.Path.Combine(ndirectory1, fileName1);
                    //    string FullFileNamePath = System.IO.Path.Combine(nDirname1, fileName1);
                    //    System.IO.File.Copy(sourceFile, destFile, true);
                    //    obj.ReplyPdfPath = FullFileNamePath;
                    //    //Delete File from temp location
                    //    System.IO.File.Delete(sourceFile);
                    //}

                    if (item.ReplyPdfPath != null)
                    {
                        string FullReplyNamePath = string.Empty;
                        string[] s = item.ReplyPdfPath.Split(',');
                        for (int i = 0; i < s.Length - 1; i++)
                        {
                            string ReplyfileName = s[i].Trim(' ');
                            // string AnnexfileName = item.AnnexPdfPath;
                            string source = Server.MapPath("~/LOBTemp/");
                            string sourceFile = System.IO.Path.Combine(source, ReplyfileName);
                            string destFile = System.IO.Path.Combine(ndirectory1, ReplyfileName);
                            FullReplyNamePath = System.IO.Path.Combine(nDirname1, ReplyfileName) + "," + FullReplyNamePath;
                            System.IO.File.Copy(sourceFile, destFile, true);
                            System.IO.File.Delete(sourceFile);
                        }
                        obj.ReplyPdfPath = FullReplyNamePath;
                    }
                }



                //For Annexures
                string directory1 = FileSettings.SettingValue + "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Annexures/";
                string Dirname1 = "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/Annexures/";
                if (!System.IO.Directory.Exists(directory1))
                {
                    System.IO.Directory.CreateDirectory(directory1);
                }


                string FullAnnexNamePath = string.Empty;
                if (item.AnnexPdfPath != null)
                {
                    if (CurrentSession.DeptID == "HPD0001")//vs user
                    {
                        string[] s = item.AnnexPdfPath.Split(',');
                        for (int i = 0; i < s.Length - 1; i++)
                        {
                            string AnnexfileName = s[i].Trim(' ');
                            // string AnnexfileName = item.AnnexPdfPath;
                            string source = Server.MapPath("~/LOBTemp/");
                            string AnnexsourceFile = System.IO.Path.Combine(source, AnnexfileName);
                            string AnnexdestFile = System.IO.Path.Combine(directory1, AnnexfileName);
                            FullAnnexNamePath = System.IO.Path.Combine(Dirname1, AnnexfileName) + "," + FullAnnexNamePath;
                            System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
                            System.IO.File.Delete(AnnexsourceFile);
                        }
                        obj.AnnexPdfPath = FullAnnexNamePath;
                    }
                    //New for attaching annexures only while sending information.
                    else if (CurrentSession.DeptID != "HPD0001")
                    {
                        if (ItemIdIs == 10)
                        {
                            if (obj.PaperNature == 3)
                            {
                                //string source = Server.MapPath("~/LOBTemp/");
                                //string AnnexsourceFile = System.IO.Path.Combine(source, item.AnnexPdfPath);
                                //string AnnexdestFile = System.IO.Path.Combine(directory1, item.AnnexPdfPath);
                                //FullAnnexNamePath = System.IO.Path.Combine(Dirname1, item.AnnexPdfPath) + "," + FullAnnexNamePath;
                                //System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
                                //System.IO.File.Delete(AnnexsourceFile);
                                //obj.AnnexPdfPath = FullAnnexNamePath;
                                string FullAnnexNamePath1 = string.Empty;
                                string[] s = item.AnnexPdfPath.Split(',');
                                for (int i = 0; i < s.Length - 1; i++)
                                {
                                    string AnnexfileName = s[i].Trim(' ');
                                    // string AnnexfileName = item.AnnexPdfPath;
                                    string source = Server.MapPath("~/LOBTemp/");
                                    string sourceFile = System.IO.Path.Combine(source, AnnexfileName);
                                    string destFile = System.IO.Path.Combine(directory1, AnnexfileName);
                                    FullAnnexNamePath1 = System.IO.Path.Combine(Dirname1, AnnexfileName) + "," + FullAnnexNamePath1;
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    System.IO.File.Delete(sourceFile);
                                }
                                obj.AnnexPdfPath = FullAnnexNamePath1;
                            }
                            else
                            {
                                obj.AnnexPdfPath = item.AnnexPdfPath;
                            }
                        }

                        else if (ItemIdIs == 7)
                        {
                            if (obj.PaperNature == 3)
                            {
                                //string source = Server.MapPath("~/LOBTemp/");
                                //string AnnexsourceFile = System.IO.Path.Combine(source, item.AnnexPdfPath);
                                //string AnnexdestFile = System.IO.Path.Combine(directory1, item.AnnexPdfPath);
                                //FullAnnexNamePath = System.IO.Path.Combine(Dirname1, item.AnnexPdfPath) + "," + FullAnnexNamePath;
                                //System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
                                //System.IO.File.Delete(AnnexsourceFile);
                                //obj.AnnexPdfPath = FullAnnexNamePath;
                                string FullAnnexNamePath1 = string.Empty;
                                string[] s = item.AnnexPdfPath.Split(',');
                                for (int i = 0; i < s.Length - 1; i++)
                                {
                                    string AnnexfileName = s[i].Trim(' ');
                                    // string AnnexfileName = item.AnnexPdfPath;
                                    string source = Server.MapPath("~/LOBTemp/");
                                    string sourceFile = System.IO.Path.Combine(source, AnnexfileName);
                                    string destFile = System.IO.Path.Combine(directory1, AnnexfileName);
                                    FullAnnexNamePath1 = System.IO.Path.Combine(Dirname1, AnnexfileName) + "," + FullAnnexNamePath1;
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    System.IO.File.Delete(sourceFile);
                                }
                                obj.AnnexPdfPath = FullAnnexNamePath1;
                            }
                            else
                            {
                                obj.AnnexPdfPath = item.AnnexPdfPath;
                            }
                        }
                        else
                        {
                            if (item.AnnexPdfPath != null)
                            {
                                string FullAnnexNamePath1 = string.Empty;
                                string[] s = item.AnnexPdfPath.Split(',');
                                for (int i = 0; i < s.Length - 1; i++)
                                {
                                    string AnnexfileName = s[i].Trim(' ');
                                    // string AnnexfileName = item.AnnexPdfPath;
                                    string source = Server.MapPath("~/LOBTemp/");
                                    string sourceFile = System.IO.Path.Combine(source, AnnexfileName);
                                    string destFile = System.IO.Path.Combine(directory1, AnnexfileName);
                                    FullAnnexNamePath1 = System.IO.Path.Combine(Dirname1, AnnexfileName) + "," + FullAnnexNamePath1;
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    System.IO.File.Delete(sourceFile);
                                }
                                obj.AnnexPdfPath = FullAnnexNamePath1;
                            }

                            //string source = Server.MapPath("~/LOBTemp/");
                            //string AnnexsourceFile = System.IO.Path.Combine(source, item.AnnexPdfPath);
                            //string AnnexdestFile = System.IO.Path.Combine(directory1, item.AnnexPdfPath);
                            //FullAnnexNamePath = System.IO.Path.Combine(Dirname1, item.AnnexPdfPath) + "," + FullAnnexNamePath;
                            //System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
                            //System.IO.File.Delete(AnnexsourceFile);
                            //obj.AnnexPdfPath = FullAnnexNamePath;
                        }
                    }
                }
                else
                {
                    obj.AnnexPdfPath = string.Empty;
                }

                //For Word File
                string Docdirectory = FileSettings.SettingValue + "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/DocFiles/";
                string DocDirname = "Correspondence/" + obj.eFileID + "/" + tCommName + "/" + tItemName + "/DocFiles/";
                if (!System.IO.Directory.Exists(Docdirectory))
                {
                    System.IO.Directory.CreateDirectory(Docdirectory);
                }
                string FullDocNamePath = string.Empty;
                if (item.DocFilePath != null)
                {
                    if (CurrentSession.DeptID == "HPD0001")//vs user
                    {


                        string[] s = item.DocFilePath.Split(',');
                        for (int i = 0; i < s.Length - 1; i++)
                        {
                            string DocfileName = s[i].Trim(' ');
                            // string AnnexfileName = item.AnnexPdfPath;
                            string source = Server.MapPath("~/LOBTemp/");
                            string WordsourceFile = System.IO.Path.Combine(source, DocfileName);
                            string WorddestFile = System.IO.Path.Combine(Docdirectory, DocfileName);
                            FullDocNamePath = System.IO.Path.Combine(DocDirname, DocfileName) + "," + FullDocNamePath;
                            System.IO.File.Copy(WordsourceFile, WorddestFile, true);
                            System.IO.File.Delete(WordsourceFile);
                        }
                        obj.DocFilePath = FullDocNamePath;
                    }

                    else
                    {

                        if (ItemIdIs == 10)
                        {
                            obj.DocFilePath = item.DocFilePath;
                            obj.DocFilesNameByUser = item.DocFilesNameByUser;
                        }
                        else if (ItemIdIs == 7)
                        {
                            obj.DocFilePath = item.DocFilePath;
                            obj.DocFilesNameByUser = item.DocFilesNameByUser;
                        }
                        else
                        {
                            string[] s = item.DocFilePath.Split(',');
                            for (int i = 0; i < s.Length - 1; i++)
                            {
                                string DocfileName = s[i].Trim(' ');
                                // string AnnexfileName = item.AnnexPdfPath;
                                string source = Server.MapPath("~/LOBTemp/");
                                string WordsourceFile = System.IO.Path.Combine(source, DocfileName);
                                string WorddestFile = System.IO.Path.Combine(Docdirectory, DocfileName);
                                FullDocNamePath = System.IO.Path.Combine(DocDirname, DocfileName) + "," + FullDocNamePath;
                                System.IO.File.Copy(WordsourceFile, WorddestFile, true);
                                System.IO.File.Delete(WordsourceFile);
                            }
                            obj.DocFilePath = FullDocNamePath;
                            //string DocfileName = item.DocFilePath;
                            //// string AnnexfileName = item.AnnexPdfPath;
                            //string source = Server.MapPath("~/LOBTemp/");
                            //string WordsourceFile = System.IO.Path.Combine(source, DocfileName);
                            //string WorddestFile = System.IO.Path.Combine(Docdirectory, DocfileName);
                            //FullDocNamePath = System.IO.Path.Combine(DocDirname, DocfileName);
                            //System.IO.File.Copy(WordsourceFile, WorddestFile, true);
                            //System.IO.File.Delete(WordsourceFile);
                            //obj.DocFilePath = FullDocNamePath;
                            //obj.DocFilesNameByUser = item.DocFilesNameByUser;

                        }
                    }



                }


                //////


                //if (FullDocNamePath == "")
                //{
                //    obj.DocFilePath = null;
                //}
                //else
                //{
                //    obj.DocFilePath = FullDocNamePath;
                //}
                if (item.DocFilesNameByUser != null)
                {
                    string a = "";
                    string[] SplitIt = item.DocFilesNameByUser.Split(',');
                    int ai = SplitIt.Length - 2;
                    for (int i = ai; i >= 0; i--)
                    {
                        a += SplitIt[i] + ",";
                    }
                    obj.DocFilesNameByUser = a;
                }
                else
                {
                    obj.DocFilesNameByUser = item.DocFilesNameByUser;
                }



                //  obj.AnnexPdfPath = FullAnnexNamePath;
                if (item.AnnexFilesNameByUser != null)
                {
                    string ab = "";
                    string[] SplitIt1 = item.AnnexFilesNameByUser.Split(',');
                    int ai1 = SplitIt1.Length - 2;
                    for (int i = ai1; i >= 0; i--)
                    {
                        ab += SplitIt1[i] + ",";
                    }
                    obj.AnnexFilesNameByUser = ab;
                }
                else
                {
                    obj.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                }



                int res = (int)Helper.ExecuteService("eFile", "SaveToComposedLetter", obj);


            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult UpdateLetterSendStatus(int ParentId, int RecordId)
        {
            if (RecordId == 0)
            {
                int res = (int)Helper.ExecuteService("eFile", "UpdateLetterStatus", ParentId);

            }
            else
            {

                int res = (int)Helper.ExecuteService("eFile", "UpdateChildLetterStatus", RecordId);
            }


            return Json("Sent Succesfully", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UploadDocFiles()
        {
            String pfileName = "";
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    NameValueCollection PFileName = Request.Form;
                    foreach (var key in PFileName.AllKeys)
                    {
                        pfileName = PFileName[key];
                    }
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    HttpPostedFileBase file = files[0];
                    string fname;
                    // Checking for Internet Explorer 
                    fname = file.FileName;
                    string[] s = fname.Split('.');
                    //Code for Assembly Folder File Existense Check
                    pfileName = Convert.ToString(DateTime.Now.ToFileTime()) + "." + s[1];
                    var filepath = System.IO.Path.Combine("\\LOBTemp\\" + pfileName);
                    // Get the complete folder path and store the file inside it.  
                    fname = Path.Combine(Server.MapPath(filepath));
                    file.SaveAs(fname);
                    // }
                    // Returns message that successfully uploaded  
                    return Json(pfileName, "File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
        public FileResult GetDocFile(string parameter)
        {


            //string a = Request.QueryString["parameter"];



            var filepath = System.IO.Path.Combine("\\LOBTemp\\" + parameter.Trim());
            // Get the complete folder path and store the file inside it.  
            string fname = Path.Combine(Server.MapPath(filepath));
            string ReportURL = fname;
            byte[] FileBytes = System.IO.File.ReadAllBytes(ReportURL);
            return File(FileBytes, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        }
        public FileResult GetDocFileFromServer(string parameter)
        {


            //string a = Request.QueryString["parameter"];

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            string CompPath = FileSettings.SettingValue;
            var fname = System.IO.Path.Combine(CompPath + parameter.Trim());
            // Get the complete folder path and store the file inside it.  
            //string fname = Path.Combine(Server.MapPath(filepath));
            string ReportURL = fname;
            byte[] FileBytes = System.IO.File.ReadAllBytes(ReportURL);
            return File(FileBytes, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        }

        [HttpGet]
        public FileResult GetPdf(string parameter)
        {

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            string CompPath = FileSettings.SettingValue;
            var fname = System.IO.Path.Combine(CompPath + parameter.Trim());
            // Get the complete folder path and store the file inside it.  
            //string fname = Path.Combine(Server.MapPath(filepath));
            string ReportURL = fname;
            byte[] FileBytes = System.IO.File.ReadAllBytes(ReportURL);
            return File(FileBytes, "application/pdf");
        }


        public JsonResult GetDeptList()
        {
            List<mDepartment> ListsAccToeFileId = new List<mDepartment>();
            if (CurrentSession.DeptID != "HPD0001")
            {
                var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartmentByDeptId", CurrentSession.DeptID);
                ListsAccToeFileId.AddRange(deptList);
            }
            else
            {
                var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
                ListsAccToeFileId.AddRange(deptList);
            }

            return Json(ListsAccToeFileId, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCommitteeList()
        {
            SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model1 = new SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel();
            List<tCommittee> CommitteeList1 = new List<tCommittee>();
            tPaperLaidV model = new tPaperLaidV();
            string[] str1 = new string[4];
            str1[0] = CurrentSession.DeptID;
            str1[1] = CurrentSession.OfficeId;
            mBranches modelBranch = new mBranches();
            modelBranch.UserId = Guid.Parse(CurrentSession.UserID);
            List<tCommittee> Lists = new List<tCommittee>();

            if (str1[0] != "HPD0001")
            {
                CommitteeList1 = (List<tCommittee>)Helper.ExecuteService("eFile", "GetAllCommitteeList", modelBranch);
                Lists.AddRange(CommitteeList1);
            }
            else
            {
                model.BranchesList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "GBranchList", modelBranch);

                foreach (var i in model.BranchesList)
                {
                    str1[2] = Convert.ToString(i.BranchId);
                    string[] strngBID = new string[2];
                    strngBID[0] = Convert.ToString(i.BranchId);
                    CommitteeList1 = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("eFile", "nGetCommiteeIDAndNameByBranchID", strngBID);
                    Lists.AddRange(CommitteeList1);
                }


            }

            return Json(Lists, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult nGeteFileByCommnDeptId(string CommitteeId, string DeptId)
        {
            List<eFileList> FileListsAccToComId = new List<eFileList>();
            eFileList Cmodel = new eFileList();
            Cmodel.CommitteeId = Convert.ToInt16(CommitteeId);
            Cmodel.DepartmentId = DeptId;
            eFileAttachment pdocuments = new eFileAttachment();
            try
            {
                SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
                eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "nGeteFilesByCommnDeptId", Cmodel);
                FileListsAccToComId.AddRange(eList.eFileLists);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(FileListsAccToComId, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDataBySearchParameter(int ItemTypeId, string CommitteeId, string SelectedDeptID)
        {
            List<pHouseCommitteeFiles> SearchedListModel = new List<pHouseCommitteeFiles>();
            List<pHouseCommitteeFiles> CSearchedListModel = new List<pHouseCommitteeFiles>();
            pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
            string aItemTypeId = Convert.ToString(ItemTypeId);

            string CDeptId = CurrentSession.DeptID;
            string Curr_Dept_Id = CurrentSession.DeptID;
            if (CDeptId != "HPD0001") //check for dept
            {
                if (SelectedDeptID == "0")
                {
                    SelectedDeptID = CDeptId;
                }
            }
          


            /////////////////////////////committee check
            tPaperLaidV model = new tPaperLaidV();
            List<tCommittee> CommitteeList1 = new List<tCommittee>();
            List<tCommittee> Lists = new List<tCommittee>();
            mBranches modelBranch = new mBranches();
            modelBranch.UserId = Guid.Parse(CurrentSession.UserID);
            if (CDeptId == "HPD0001" && CommitteeId == "0") //check for VS
            {
                model.BranchesList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "GBranchList", modelBranch);
                
                foreach (var i in model.BranchesList)
                {
                    string[] strngBID = new string[2];
                    strngBID[0] = Convert.ToString(i.BranchId);
                    CommitteeList1 = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("eFile", "nGetCommiteeIDAndNameByBranchID", strngBID);
                    Lists.AddRange(CommitteeList1);
                }

                foreach (var item in Lists)
                {
                    string nCommId = Convert.ToString(item.CommitteeId);
                    CSearchedListModel = (List<pHouseCommitteeFiles>)Helper.ExecuteService("eFile", "GetSearchedList", new string[] { aItemTypeId, nCommId, SelectedDeptID, Curr_Dept_Id });
                    SearchedListModel.AddRange(CSearchedListModel);
                }
            }
            /////////////////////////////

            else
            {
                SearchedListModel = (List<pHouseCommitteeFiles>)Helper.ExecuteService("eFile", "GetSearchedList", new string[] { aItemTypeId, CommitteeId, SelectedDeptID, Curr_Dept_Id });
            }





            if (SearchedListModel != null)
            {
                SearchedListModel.OrderBy(a => a.CreatedDate);
            }
            mdl.HouseCommAllFilesList = SearchedListModel;
            return PartialView("_ShowHouseComAllFiles", mdl);
            //_nPendencyReplyList

        }
        
        
        [HttpPost]
        public JsonResult AcceptDocuments(int RecordId)
        {
            pHouseCommitteeSubFiles mdl = new pHouseCommitteeSubFiles();
            mdl.RecordId = RecordId;
            mdl.VerifiedBy = CurrentSession.AadharId;
            int res = (int)Helper.ExecuteService("eFile", "UpdateAcceptStatus", mdl);
           return Json("Approved Succesfully", JsonRequestBehavior.AllowGet);
        }

        public ActionResult RejectDocuments(string RecordId, string RejectReason)
        {
            pHouseCommitteeSubFiles mdl = new pHouseCommitteeSubFiles();
            mdl.RecordId =Convert.ToInt16(RecordId);
            mdl.RejectReason = RejectReason;
            mdl.VerifiedBy = CurrentSession.AadharId;
            int res = (int)Helper.ExecuteService("eFile", "RejectDocumentsStatus", mdl);
            //EMailModel model = new EMailModel();
            //model.To = "lakshay.verma18@gmail.com";
            //model.Subject = "HI";
            //model.Message = "Document status";
            //SendEmail1("lakshay.verma18@gmail.com", "HI", "Documents");
            return Json("Succesfully Saved", JsonRequestBehavior.AllowGet);        
        }


       
        public void SendEmail1(string receiver, string subject, string message)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var senderEmail = new MailAddress("visabha-hp@nic.in", "Vidhan Sabha");
                    var receiverEmail = new MailAddress(receiver, "Receiver");
                    var password = "hpsmlvs#04";
                    var sub = subject;
                    var body = message;
                    var smtp = new SmtpClient
                    {
                        Host = "mail.gov.in",
                        Port = 465,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(senderEmail.Address, password)
                    };
                    using (var mess = new System.Net.Mail.MailMessage(senderEmail, receiverEmail)
                    {
                        Subject = subject,
                        Body = body
                    })
                    {
                        smtp.Send(mess);
                    }
                    //return View();

                }
            }
            catch (Exception)
            {
                ViewBag.Error = "Some Error";
            }
           // return View();
        }    

        public string Send_Email(string receiver, string subject, string message)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var senderEmail = new MailAddress("HimachalVS@gmail.com", "Himachal VS");
                    string a = senderEmail.Address;
                    var receiverEmail = new MailAddress(receiver, "Receiver");
                    var password = "HimachalVS2020";
                    var sub = subject;
                    var body = message;
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(senderEmail.Address, password)
                    };
                    using (var mess = new System.Net.Mail.MailMessage(senderEmail, receiverEmail)
                    {
                        IsBodyHtml = true,
                        Subject = subject,
                        Body = body
                    })
                    {
                        smtp.Send(mess);
                    }

                }
                return ("Sucess");
            }
            catch (Exception)
            {
                ViewBag.Error = "Some Error";
                return ("Some Error");
            }

        }
        [HttpPost]
        public ActionResult SendEmail(EMailModel model)
        {
            if (model.To != "" && model.To != null && model.Subject != ""
            && model.Subject != null && model.Message != "" && model.Message != null)
            {
                try
                {
                    Service1 EmailWebServiceClient = new Service1();

                    string[] emailList = model.To.Split(new string[] { "," },
                    StringSplitOptions.RemoveEmptyEntries);

                    var emailGroup = (from email in emailList
                                      select email).Distinct();

                    if (emailList.Length > 0)
                    {
                        CustomMailMessage emailMessage = new CustomMailMessage();

                        emailMessage._isBodyHtml = true;

                        emailMessage._subject = model.Subject;

                        emailMessage._body = model.Message;

                        emailMessage.ModuleActionID = 1;

                        emailMessage.UniqueIdentificationID = 1;

                        foreach (string email in emailGroup)
                        {
                            emailMessage._toList.Add(email);
                        }                        

                        model.SendSMS = false;

                        model.SendEmail = true;

                        Notification.Send(model.SendSMS, model.SendEmail, null, emailMessage);
                       
                    }

                    model.Result = "success";
                }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
                {
                    model.Result = "EMail Sending Failed";
                }
            }

            return View("Index", model);
        }
    }




    public class Updatevalue
    {

        public int EfileId { get; set; }
        public string EmployeeAid { get; set; }
        public string Remark { get; set; }
        public int BranchId { get; set; }
        public int RefileId { get; set; }
    }

}
