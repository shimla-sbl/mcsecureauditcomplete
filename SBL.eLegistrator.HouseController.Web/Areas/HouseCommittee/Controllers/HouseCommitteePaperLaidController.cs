﻿using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.CommitteeReport;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.eFile;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.HouseCommittee.Controllers
{
    public class HouseCommitteePaperLaidController : Controller
    {
        //
        // GET: /HouseCommittee/HouseCommitteePaperLaid/

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult GetSentPaperList(int pageId = 1, int pageSize = 25)
        {

            eFileLogin();
            eFileAttachments model = new eFileAttachments();
            string[] str = new string[3];          
            str[0] = CurrentSession.BranchId;
            str[1] = CurrentSession.AssemblyId;
            str[2] = CurrentSession.SessionId;


            List<CommittePapersLaidModel> ListModel = new List<CommittePapersLaidModel>();
            ListModel = (List<CommittePapersLaidModel>)Helper.ExecuteService("eFileCommiteePaperLaid", "GetPapersLiadListByCommitteeId", str);
           
            //model.eFileAttachemts = ListModel;

            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<CommittePapersLaidModel> pagedRecord = new List<CommittePapersLaidModel>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            
            model.CommittePapersLaidList = pagedRecord;
           

            return PartialView("_SentCommitteePaperLaidList", model);
        }


        public ActionResult NewPaperEntry()
        {
            tPaperLaidV paperLaidModel = new tPaperLaidV();
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                mEvent mEventModel = new mEvent();
                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    paperLaidModel.UserID = new Guid(CurrentSession.UserID);
                }
            
                mDepartment mdep = new mDepartment();
                mdep.deptId = paperLaidModel.DepartmentId;
                paperLaidModel.mDepartment = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", mdep);

                tPaperLaidV ministerDetails = new tPaperLaidV();               
                paperLaidModel.SessionId = int.Parse(CurrentSession.SessionId);
                paperLaidModel.AssemblyCode = int.Parse(CurrentSession.AssemblyId);
                paperLaidModel.AssemblyId = int.Parse(CurrentSession.AssemblyId);
                paperLaidModel.SessionCode = int.Parse(CurrentSession.SessionId);

                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                paperLaidModel.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", paperLaidModel);              

                mEventModel.PaperCategoryTypeId = 3;
                List<mEvent> events = (List<mEvent>)Helper.ExecuteService("Events", "GetEventsListByPaperCategoryID", mEventModel);
                paperLaidModel.mEvents = events;

                paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetMinistersonly", ministerDetails);

                List<eFilePaperType> FilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);
                paperLaidModel.eFilePaperTypeList = FilePaperTypeList.FindAll(a => a.PaperTypeID == 4 || a.PaperTypeID == 7 || a.PaperTypeID == 6);               

                return PartialView("NewPaperEntry", paperLaidModel);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PaperLaidSubmitt(HttpPostedFileBase file, HttpPostedFileBase DocFile, tPaperLaidV obj, string submitButton)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV mdl = new tPaperLaidV();
                tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    mdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    mdl.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

               
                // Save and Send Papers 
                tPaperLaidV billDetails = new tPaperLaidV();
                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                billDetails = obj;


                string[] str = new string[3];
                str[0] = CurrentSession.BranchId;
                int CommitteeID=0;

                CommitteeID =  (int)Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDByBranchID", str);
                obj.CommitteeId=CommitteeID;

                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                mdl.EventId = obj.EventId;
                
                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetFileSaveDetails", mdl);

                string categoryType = mdl.RespectivePaper;             

                if (file == null)
                {
                    string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                    string directory = Server.MapPath(url);
                    if (Directory.Exists(directory))
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            string name = Path.GetFileName(page);
                            string nameKey = Path.GetFileNameWithoutExtension(page);
                            string directory1 = Path.GetDirectoryName(page);
                            //
                            // Display the Path strings we extracted.
                            //
                            Console.WriteLine("{0}, {1}, {2}, {3}",
                            page, name, nameKey, directory1);
                            mdlTemp.FileName = name;
                        }
                        string ext = Path.GetExtension(mdlTemp.FileName);
                        string fileNam1 = mdlTemp.FileName.Replace(ext, "");


                        var vers = obj.FileVersion.GetValueOrDefault() + 1;


                        string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "C" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                        string actualFileNameWExt = obj.AssemblyId + "_" + obj.SessionId + "_" + "C" + "_" + mdl.PaperLaidId + "_V" + vers;


                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                        DirectoryInfo Dir = new DirectoryInfo(Url);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }

                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" , actualFileName);
                        string Signpath = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + "/Signed/", actualFileNameWExt + "_Signed.pdf");
                        
                        System.IO.File.Copy(SourceFile, path, true);
                        System.IO.File.Copy(SourceFile, Signpath, true);

                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                        //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                        string FileStructurePath = Acess.SettingValue;
                        mdlTemp.FileStructurePath = FileStructurePath;
                        mdlTemp.FileName = actualFileName ;
                        mdlTemp.SignedFilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + "Signed/" + actualFileNameWExt + "_Signed.pdf"; 
                        mdlTemp.Version = vers;
                        mdlTemp.CommtSubmittedBy = CurrentSession.UserID;
                        mdlTemp.CommtSubmittedDate = System.DateTime.Now;
                        mdlTemp.DeptSubmittedBy = CurrentSession.UserID;
                        mdlTemp.DeptSubmittedDate = System.DateTime.Now;

                    }
                    else
                    {
                        mdlTemp.PaperLaidId = mdl.PaperLaidId;
                        mdlTemp.Version = mdl.Count;
                        mdlTemp.FileName = mdl.FileName;
                        mdlTemp.DocFileName = mdl.DocFileName;
                        mdlTemp.FilePath = mdl.FilePath;
                    }


                }

                else
                {
                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                    mdlTemp.DeptSubmittedDate = DateTime.Now;
                    mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                }


                if (DocFile == null)
                {

                    string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                    string directory = Server.MapPath(url);
                    if (Directory.Exists(directory))
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID));
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            string name = Path.GetFileName(page);
                            string nameKey = Path.GetFileNameWithoutExtension(page);
                            string directory1 = Path.GetDirectoryName(page);
                            //
                            // Display the Path strings we extracted.
                            //
                            Console.WriteLine("{0}, {1}, {2}, {3}",
                            page, name, nameKey, directory1);
                            mdlTemp.DocFileName = name;
                        }

                        string ext = Path.GetExtension(mdlTemp.DocFileName);

                        var DocVer = obj.DocFileVersion.GetValueOrDefault() + 1;


                        string fileName = mdlTemp.DocFileName.Replace(ext, "");


                        string actualDocFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "C" + "_" + mdl.PaperLaidId + "_V" + DocVer + ext;



                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                        DirectoryInfo Dir = new DirectoryInfo(Url);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }

                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualDocFileName);
                        //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                        System.IO.File.Copy(SourceFile, path, true);
                        //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                        string FileStructurePath = Acess.SettingValue;
                        mdlTemp.FileStructurePath = FileStructurePath;
                        mdlTemp.DocFileName = actualDocFileName;
                        mdlTemp.DocFileVersion = DocVer;
                    }

                    mdlTemp.PaperLaidId = mdl.PaperLaidId;                 

                }
                mdlTemp.AssemblyId = CurrentSession.AssemblyId;
                mdlTemp.SessionId = CurrentSession.SessionId;


                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);

                mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                mdl.ModifiedBy = CurrentSession.UserName;
                mdl.ModifiedWhen = DateTime.Now;
                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntryForBilReplace", mdl);


                tCommitteeReport objComm = new tCommitteeReport();
                objComm.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                objComm.PaperLaidId = mdl.PaperLaidId;
                objComm.CommitteeId = CommitteeID;
                objComm.DateOfLaying = obj.DesireLayingDate;
                objComm.DeptmentID = obj.DepartmentId;
                objComm.PDFPath = mdlTemp.FilePath + mdlTemp.FileName;
                objComm.Title = obj.Title;
                objComm.IsOnline = true;
                objComm.EntryDate = System.DateTime.Now;
                objComm.ReportTypeId = obj.ReportTypeId;
                objComm.SessionID = Convert.ToInt32(CurrentSession.SessionId);
                objComm.DateOfLayingId = obj.DesireLayingDateId;


                objComm = (tCommitteeReport)Helper.ExecuteService("eFileCommiteePaperLaid", "InsertCommitteeDetails", objComm);


              

                if (Request.IsAjaxRequest())                
                {
                   

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {


                }
                return null;
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }



        public ActionResult GetCommitteeSentRecordById(int paperLaidId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV mdl = new tPaperLaidV();
                mdl.PaperLaidId = Convert.ToInt64(paperLaidId);               
                mdl = (tPaperLaidV)Helper.ExecuteService("eFileCommiteePaperLaid", "ShowSentBillById", mdl);
                return PartialView("_ShowCommitteeSentDetailsByID", mdl);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        public ActionResult GetCommitteeSentByID(string paperLaidId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV movementModel = new tPaperLaidV();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (tPaperLaidV)Helper.ExecuteService("eFileCommiteePaperLaid", "GetSubmittedPaperLaidByID", movementModel);
                return PartialView("_GetCommitteeSentByID", movementModel);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateCommitteePaperLaidFile(HttpPostedFileBase file, HttpPostedFileBase Docfile, tPaperLaidV tPaper)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidTemp tPaperTemp = new tPaperLaidTemp();
                tPaperLaidV obj = new tPaperLaidV();
                tPaper = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", tPaper);

                if (file == null)
                {

                    string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                    string directory = Server.MapPath(url);
                    if (Directory.Exists(directory))
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            string name = Path.GetFileName(page);
                            string nameKey = Path.GetFileNameWithoutExtension(page);
                            string directory1 = Path.GetDirectoryName(page);
                            //
                            // Display the Path strings we extracted.
                            //
                            Console.WriteLine("{0}, {1}, {2}, {3}",
                            page, name, nameKey, directory1);
                            tPaperTemp.FileName = name;
                        }
                        string ext = Path.GetExtension(tPaperTemp.FileName);
                        string fileNam1 = tPaperTemp.FileName.Replace(ext, "");



                        tPaper.Count = tPaper.Count;
                        //var vers = tPaper.FileVersion.GetValueOrDefault() + 1;


                        string actualFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + "C" + "_" + tPaper.PaperLaidId + "_V" + tPaper.Count + ext;
                        string actualFileNameWExt = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + "C" + "_" + tPaper.PaperLaidId + "_V" + tPaper.Count;



                        // string actualFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + QType + "_" + QuestionNo + "_V" + tPaper.Count + ext;



                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                        string Url = FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";
                        DirectoryInfo Dir = new DirectoryInfo(Url);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }

                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/", actualFileName);
                        string Signpath = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/" + "/Signed/", actualFileNameWExt + "_Signed.pdf");
                        System.IO.File.Copy(SourceFile, path, true);
                        System.IO.File.Copy(SourceFile, Signpath, true);
                        tPaperTemp.FilePath = "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";

                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                        //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                        string FileStructurePath = Acess.SettingValue;
                        tPaperTemp.FileStructurePath = FileStructurePath;
                        tPaperTemp.FileName = actualFileName;

                        // tPaperTemp.Version = tPaper.Count;
                        tPaperTemp.PaperLaidId = tPaper.PaperLaidId;

                        tPaperTemp.Version = tPaper.Count;
                        tPaperTemp.SignedFilePath = "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/" + "Signed/" + actualFileNameWExt + "_Signed.pdf";
                        tPaperTemp.CommtSubmittedBy = CurrentSession.UserID;
                        tPaperTemp.CommtSubmittedDate = System.DateTime.Now;
                        tPaperTemp.DeptSubmittedBy = CurrentSession.UserID;
                        tPaperTemp.DeptSubmittedDate = System.DateTime.Now;

                    }

                    tPaperTemp.Version = tPaper.Count; 
                }

                if (Docfile == null)
                {

                    string urlDocFile = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                    string directoryDocFile = Server.MapPath(urlDocFile);
                    if (Directory.Exists(directoryDocFile))
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID));
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            string name = Path.GetFileName(page);
                            string nameKey = Path.GetFileNameWithoutExtension(page);
                            string directory1 = Path.GetDirectoryName(page);
                            //
                            // Display the Path strings we extracted.
                            //
                            Console.WriteLine("{0}, {1}, {2}, {3}",
                            page, name, nameKey, directory1);
                            tPaperTemp.DocFileName = name;
                        }
                        string Docext = Path.GetExtension(tPaperTemp.DocFileName);
                        string fileNam1 = tPaperTemp.DocFileName.Replace(Docext, "");

                        //var Docvers = tPaper.DocFileVersion.GetValueOrDefault() + 1;

                        string DocFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + "N" + "_" + tPaper.PaperLaidId + "_V" + tPaper.MainDocCount + Docext;
                        //string actualDocFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + QType + "_" + QuestionNo + "_V" + model.MainDocCount + ext;


                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                        string Url = FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";
                        DirectoryInfo Dir = new DirectoryInfo(Url);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }

                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/", DocFileName);
                        //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                        System.IO.File.Copy(SourceFile, path, true);
                        //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                        tPaperTemp.FilePath = "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";

                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                        string FileStructurePath = Acess.SettingValue;
                        tPaperTemp.FileStructurePath = FileStructurePath;
                        tPaperTemp.DocFileName = DocFileName;

                        tPaperTemp.PaperLaidId = tPaper.PaperLaidId;
                        tPaperTemp.DocFileVersion = tPaper.MainDocCount;
                        tPaperTemp.DocFileName = tPaperTemp.DocFileName;
                    }
                    else
                    {
                        tPaperTemp.DocFileVersion = tPaper.MainDocCount;
                    }

                }

                tPaperTemp.AssemblyId = CurrentSession.AssemblyId;
                tPaperTemp.SessionId = CurrentSession.SessionId;

                tPaperLaidTemp mdlTemp1 = new tPaperLaidTemp();
                mdlTemp1 = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", tPaper);
                tPaperTemp.DocFileName = mdlTemp1.DocFileName;

                tPaperTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateBillPaperLaidFileByID", tPaperTemp);
                tPaper.DeptActivePaperId = tPaperTemp.PaperLaidTempId;

                tPaper.ModifiedBy = CurrentSession.UserName;
                tPaper.ModifiedWhen = DateTime.Now;
                tPaper = (tPaperLaidV)Helper.ExecuteService("Notice", "UpdatePaperLaidEntryForNoticeReplace", tPaper);


                tCommitteeReport objComm = new tCommitteeReport();

                objComm.PDFPath = tPaperTemp.FilePath + tPaperTemp.FileName;                
                objComm.EntryDate = System.DateTime.Now;
                objComm.PaperLaidId = tPaper.PaperLaidId; ;

                objComm = (tCommitteeReport)Helper.ExecuteService("eFileCommiteePaperLaid", "UpdateCommitteePaperVersionFile", objComm);

                if (Request.IsAjaxRequest())
                {
                    tPaperLaidV mod = new tPaperLaidV();

                    return Json(mod, JsonRequestBehavior.AllowGet);
                }
                else
                {


                }
                return null;

                //TempData["lSM"] = "BLL";
                //TempData["Msg"] = "File";
                //return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment");
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        public ActionResult eFileLogin()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            return null;
        }

    }
}
