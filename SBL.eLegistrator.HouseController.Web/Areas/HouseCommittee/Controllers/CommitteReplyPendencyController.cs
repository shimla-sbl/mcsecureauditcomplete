﻿using EvoPdf;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.CommitteeReport;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.eFile;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Trirand.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.HouseCommittee.Models;
using SBL.DomainModel.Models.PaperLaid;
using System.Collections.Specialized;

namespace SBL.eLegistrator.HouseController.Web.Areas.HouseCommittee.Controllers
{
    public class CommitteReplyPendencyController : Controller
    {
        //
        // GET: /HouseCommittee/CommitteReplyPendency/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _NewItemPendencyDashBoard()
        {
            tCommitteReplyPendency model = new tCommitteReplyPendency();
            if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
            {
                ViewBag.loginType = "VidhanSabha";
                model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            }
            else
            {
                ViewBag.loginType = "Department";

                mDepartment mdep = new mDepartment();
                mdep.deptId = CurrentSession.DeptID;
                model.mDepartmentList = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
            }
            model.tCommitteeList = (List<tCommittee>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteList", null);
            model.mCommitteeReplyItemTypeList = (List<mCommitteeReplyItemType>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyItemType", null);
            model.mCommitteeReplyMasterList = (List<tCommitteReplyByMaster>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyByMasterRecord", null);

            //new requirement to show selected committee only..
            string[] str = new string[3];
            str[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);
            model.CommitteeId = Convert.ToInt16(value[0]);
            ViewBag.CommitteeId = value[0];
            ViewBag.CommitteeName = value[1];

            model.eFilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);
            model.eFilePaperNatureList = (List<eFilePaperNature>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperNature", null);
            model.mCommitteeReplyStatusList = (List<mCommitteeReplyStatus>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteReplyStatus", null);
            //return PartialView("_MainDashBoard", model);
            return PartialView("_ShowAllItemPendencyList", model);
            // return PartialView("Test", model);
        }
        public ActionResult _MainDashBoard()
        {
            tCommitteReplyPendency model = new tCommitteReplyPendency();
            if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
            {
                ViewBag.loginType = "VidhanSabha";
                model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            }
            else
            {
                ViewBag.loginType = "Department";

                mDepartment mdep = new mDepartment();
                mdep.deptId = CurrentSession.DeptID;
                model.mDepartmentList = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
            }
            model.tCommitteeList = (List<tCommittee>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteList", null);
            model.mCommitteeReplyItemTypeList = (List<mCommitteeReplyItemType>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyItemType", null);
            model.mCommitteeReplyMasterList = (List<tCommitteReplyByMaster>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyByMasterRecord", null);

            //new requirement to show selected committee only..
            string[] str = new string[3];
            str[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);
            model.CommitteeId = Convert.ToInt16(value[0]);
            ViewBag.CommitteeId = value[0];
            ViewBag.CommitteeName = value[1];

            model.eFilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);
            model.eFilePaperNatureList = (List<eFilePaperNature>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperNature", null);
            model.mCommitteeReplyStatusList = (List<mCommitteeReplyStatus>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteReplyStatus", null);
            return PartialView("_MainDashBoard", model);
        }

        [HttpPost]
        public ActionResult GetFilteredPendencyReplyList(tCommitteReplyPendency model)
        {
            try
            {
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                model.page = 1;
                ViewBag.FileLocation = FileSettings.SettingValue;
                if (model.ItemTypeId == 1)
                {
                    ViewBag.itemtype = "Audit Parah(Finance)";
                }
                else if (model.ItemTypeId == 2)
                {
                    ViewBag.itemtype = "Audit Parah(Revenue)";
                }
                else if (model.ItemTypeId == 3)
                {
                    ViewBag.itemtype = "Audit Parah(Civil)";
                }
                else if (model.ItemTypeId == 4)
                {
                    ViewBag.itemtype = "Report(Original)";
                }
                else if (model.ItemTypeId == 5)
                {
                    ViewBag.itemtype = "Report(Action Taken)";
                }
                else if (model.ItemTypeId == 6)
                {
                    ViewBag.itemtype = "Report(Further Action Taken)";
                }
                else if (model.ItemTypeId == 7)
                {
                    ViewBag.itemtype = "Assurances";
                }
                else if (model.ItemTypeId == 8)
                {
                    ViewBag.itemtype = "Title";
                }
                else if (model.ItemTypeId == 9)
                {
                    ViewBag.itemtype = "Other";
                }

                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {// for vidhan sabha login
                    ViewBag.loginType = "VidhanSabha";
                    if (model.ItemTypeId == 4 || model.ItemTypeId == 5 || model.ItemTypeId == 6)
                    {
                        ViewBag.pendencyby = "Report";
                        //var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForReportType", model);
                        var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemType", model);
                        return PartialView("_PendencyReplyList", modelList);
                    }
                    else
                    {
                        var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemType", model);

                        return PartialView("_PendencyReplyList", modelList);
                    }
                }
                else
                { // for department login
                    ViewBag.loginType = "Department";
                    if (model.ItemTypeId == 4 || model.ItemTypeId == 5 || model.ItemTypeId == 6)
                    {
                        if (model.DepartmentId == null)
                        {
                            //mDepartment mdep = new mDepartment();
                            //mdep.deptId = CurrentSession.DeptID;
                            //model.mDepartmentList = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
                            model.DepartmentId = CurrentSession.DeptID;
                            ViewBag.pendencyby = "Report";
                            //var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForReportTypeFDptWD", model);
                            var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemType", model);
                            return PartialView("_PendencyReplyList", modelList);
                        }
                        else
                        {
                            ViewBag.pendencyby = "Report";
                            //var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForReportTypeFDpt", model);
                            var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemType", model);
                            return PartialView("_PendencyReplyList", modelList);
                        }
                    }
                    else
                    {
                        if (model.DepartmentId == null)
                        {
                            //mDepartment mdep = new mDepartment();
                            //mdep.deptId = CurrentSession.DeptID;
                            //model.mDepartmentList = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
                            model.DepartmentId = CurrentSession.DeptID;
                            //var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemTypeFDptWD", model);
                            var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemType", model);

                            return PartialView("_PendencyReplyList", modelList);
                        }
                        else
                        {
                            //var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemTypeFDpt", model);
                            var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemType", model);
                            return PartialView("_PendencyReplyList", modelList);
                        }
                    }
                }
                //tCommitteReplyPendency obj = new tCommitteReplyPendency();
            }
            catch (Exception)
            {
                return RedirectToAction("SessionTimedOut", "Error", new { @area = "" });
            }
        }

        //public ActionResult NewCommiteeReplyEntryForm()
        //{
        //    tCommitteReplyPendency model = new tCommitteReplyPendency();
        //    model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

        //    //mDepartment mdep = new mDepartment();
        //    //mdep.deptId = CurrentSession.DeptID;
        //    //model.mDepartmentList = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);

        //    model.tCommitteeList = (List<tCommittee>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteList", null);
        //    model.mCommitteeReplyItemTypeList = (List<mCommitteeReplyItemType>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyItemType", null);
        //    model.mCommitteeReplyMasterList = (List<tCommitteReplyByMaster>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyByMasterRecord", null);

        //    //new requirement to show selected committee only..
        //    string[] str = new string[3];
        //    str[0] = CurrentSession.BranchId;
        //    var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);
        //    model.CommitteeId = Convert.ToInt16(value[0]);
        //    ViewBag.CommitteeId = value[0];
        //    ViewBag.CommitteeName = value[1];

        //    model.eFilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);
        //    model.eFilePaperNatureList = (List<eFilePaperNature>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperNature", null);
        //    model.mCommitteeReplyStatusList = (List<mCommitteeReplyStatus>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteReplyStatus", null);

        //    var BranchId = CurrentSession.BranchId;
        //    ViewBag.efileno = (List<fillListGenricInt>)Helper.ExecuteService("CommitteReplyPendency", "eFileWithdepartment", BranchId);

        //    var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
        //    //assmeblyList.Add(new{te})
        //    ViewBag.assmeblyList = assmeblyList;
        //    //Code for SessionList 
        //    SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
        //    data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
        //    var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
        //    ViewBag.sessionList = sessionList;

        //    mSession mSession = new mSession();
        //    mSession.SessionCode = sessionList.FirstOrDefault().SessionCode;
        //    mSession.AssemblyID = data.AssemblyID;
        //    //ViewBag.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
        //    // model.mode = "add"; 
        //    return PartialView("NewCommiteeReplyEntryForm", model);
        //}

        public ActionResult NewCommiteeReplyEntryForm()
        {
            tCommitteReplyPendency model = new tCommitteReplyPendency();
            model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            //mDepartment mdep = new mDepartment();
            //mdep.deptId = CurrentSession.DeptID;
            //model.mDepartmentList = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);

            model.tCommitteeList = (List<tCommittee>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteList", null);
            model.mCommitteeReplyItemTypeList = (List<mCommitteeReplyItemType>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyItemType", null);
            model.mCommitteeReplyMasterList = (List<tCommitteReplyByMaster>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyByMasterRecord", null);

            //new requirement to show selected committee only..
            string[] str = new string[3];
            str[0] = CurrentSession.BranchId;
            if (str[0] != "")
            {
                var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);
                model.CommitteeId = Convert.ToInt16(value[0]);
                ViewBag.CommitteeId = value[0];
                ViewBag.CommitteeName = value[1];
            }
            else
            {
                var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndName", null);
                model.CommitteeId = Convert.ToInt16(value[0]);
                ViewBag.CommitteeId = value[0];
                ViewBag.CommitteeName = value[1];


            }

            model.eFilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);
            model.eFilePaperNatureList = (List<eFilePaperNature>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperNature", null);
            model.mCommitteeReplyStatusList = (List<mCommitteeReplyStatus>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteReplyStatus", null);

            var BranchId = CurrentSession.BranchId;
            ViewBag.efileno = (List<fillListGenricInt>)Helper.ExecuteService("CommitteReplyPendency", "eFileWithdepartment", BranchId);

            var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            //assmeblyList.Add(new{te})
            ViewBag.assmeblyList = assmeblyList;
            //Code for SessionList 
            SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
            data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
            var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
            ViewBag.sessionList = sessionList;

            mSession mSession = new mSession();
            mSession.SessionCode = sessionList.FirstOrDefault().SessionCode;
            mSession.AssemblyID = data.AssemblyID;
            //  ViewBag.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            // model.mode = "add"; 
            return PartialView("NewCommiteeReplyEntryForm", model);
        }


        public ActionResult EditCommiteeReplyEntryForm(int id)
        {
            tCommitteReplyPendency model = new tCommitteReplyPendency();
            model = (tCommitteReplyPendency)Helper.ExecuteService("CommitteReplyPendency", "CommitteReplyPendencyForEdit", id);
            model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            //model.SubjectDeptEdit = model.Subject;
            //mDepartment mdep = new mDepartment();
            //mdep.deptId = CurrentSession.DeptID;
            //model.mDepartmentList = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);

            model.tCommitteeList = (List<tCommittee>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteList", null);
            model.mCommitteeReplyItemTypeList = (List<mCommitteeReplyItemType>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyItemType", null);
            model.mCommitteeReplyMasterList = (List<tCommitteReplyByMaster>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyByMasterRecord", null);

            //new requirement to show selected committee only..
            string[] str = new string[3];
            str[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);
            model.CommitteeId = Convert.ToInt16(value[0]);
            ViewBag.CommitteeId = value[0];
            ViewBag.CommitteeName = value[1];

            model.CheckValue = model.TypeNo;//to check duplicate record
            model.CheckItemTypeId = model.ItemTypeId;//to check duplicate record
            model.CheckValueDept = model.DepartmentId;//to check duplicate record
            model.CheckValueSessionDate = model.SessionDate;//to check duplicate record
            model.CheckValueAssemblyId = Convert.ToString(model.AssemblyID);//to check duplicate record
            if (model.SessionID != null)
            {
                model.CheckValueSessionId = Convert.ToString(model.SessionID);//to check duplicate record
            }
            model.CheckFinancialYear = model.FinancialYear;
            model.eFilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);
            model.eFilePaperNatureList = (List<eFilePaperNature>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperNature", null);
            model.mCommitteeReplyStatusList = (List<mCommitteeReplyStatus>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteReplyStatus", null);

            var BranchId = CurrentSession.BranchId;
            ViewBag.efileno = (List<fillListGenricInt>)Helper.ExecuteService("CommitteReplyPendency", "eFileWithdepartment", BranchId);

            var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            //assmeblyList.Add(new{te})
            ViewBag.assmeblyList = assmeblyList;
            //Code for SessionList 
            SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
            data.AssemblyID = model.AssemblyID;
            var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
            ViewBag.sessionList = sessionList;

            //mSession mSession = new mSession();
            //mSession.SessionCode = sessionList.FirstOrDefault().SessionCode;
            //mSession.AssemblyID = data.AssemblyID;
            ViewBag.EditForm = "EditForm";
            //model.SessionID = mSession.SessionCode;
            if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
            { return PartialView("NewCommiteeReplyEntryForm", model); }
            return PartialView("_ReplyPendencyDeptEdit", model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveCommiteeReplyEntryForm(tCommitteReplyPendency model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string url = "/ReplyPendencyCoverPath/";
                    string directory = FileSettings.SettingValue + url;

                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }

                    string url1 = "~/Committee/TempFile";
                    string directory1 = Server.MapPath(url1);

                    if (Directory.Exists(directory1))
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                        if (savedFileName.Length > 0)
                        {
                            string SourceFile = savedFileName[0];
                            foreach (string page in savedFileName)
                            {
                                Guid FileName = Guid.NewGuid();

                                string name = Path.GetFileName(page);

                                string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));

                                if (!string.IsNullOrEmpty(name))
                                {
                                    if (!string.IsNullOrEmpty(model.CoverLetterPdfPath))
                                        System.IO.File.Delete(System.IO.Path.Combine(directory, model.CoverLetterPdfPath));
                                    System.IO.File.Copy(SourceFile, path, true);
                                    model.CoverLetterPdfPath = "/ReplyPendencyCoverPath/" + FileName + "_" + name.Replace(" ", "");

                                    //logic to delete temp file
                                    if (Directory.Exists(directory1))
                                    {
                                        string[] filePaths = Directory.GetFiles(directory1);
                                        foreach (string filePath in filePaths)
                                        {
                                            System.IO.File.Delete(filePath);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (model.PendencyId != 0)//in case of edit
                    {

                        //edit for dept only..discription and subject
                        if (Convert.ToInt32(CurrentSession.OfficeLevel) == 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) == 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) == 3))
                        {
                            Helper.ExecuteService("CommitteReplyPendency", "SaveByDepartmentOnly", model);
                            return Content("edit");
                        }

                        // model.FinancialYear = Convert.ToString(model.SessionDate.Year);
                        if (model.CheckValue != model.TypeNo || model.CheckValueDept != model.DepartmentId || model.CheckValueAssemblyId != Convert.ToString(model.AssemblyID) || model.CheckValueSessionId != Convert.ToString(model.SessionID) || model.CheckFinancialYear != model.FinancialYear || model.CheckItemTypeId != model.ItemTypeId || model.CheckValueSessionDate != model.SessionDate)
                        {
                            var ItemNoCheck1 = Helper.ExecuteService("CommitteReplyPendency", "ItemNoCheck", model);
                            if (ItemNoCheck1 == null)
                            {
                                if (model.ItemTypeId == 4 || model.ItemTypeId == 5 || model.ItemTypeId == 6) { model.LaidInTheHouse = model.SessionDate; }

                                Helper.ExecuteService("CommitteReplyPendency", "SaveCommiteeReplyEntryForm", model);
                                return Content("edit");
                            }
                            else
                            {
                                return Content("TypeNoError");//type no already exist                       
                            }
                        }
                        else
                        {
                            if (model.ItemTypeId == 4 || model.ItemTypeId == 5 || model.ItemTypeId == 6) { model.LaidInTheHouse = model.SessionDate; }

                            Helper.ExecuteService("CommitteReplyPendency", "SaveCommiteeReplyEntryForm", model);
                            return Content("edit");
                        }
                    }
                    else
                    {// in case of new value



                        var ItemNoCheck1 = Helper.ExecuteService("CommitteReplyPendency", "ItemNoCheck", model);
                        if (ItemNoCheck1 == null)
                        {
                            model.CreatedDate = Convert.ToString(DateTime.Now);
                            model.CreatedBy = CurrentSession.UserName;
                            // model.FinancialYear = Convert.ToString(model.SessionDate.Year);
                            if (model.ItemTypeId == 4 || model.ItemTypeId == 5 || model.ItemTypeId == 6) { model.LaidInTheHouse = model.SessionDate; }

                            Helper.ExecuteService("CommitteReplyPendency", "SaveCommiteeReplyEntryForm", model);
                            return Content("Saved");
                        }
                        else
                        {
                            return Content("TypeNoError");//type no already exist
                        }
                    }
                }
                else
                {
                    //ModelState.AddModelError("Error", ex.Message);
                    return Content("modelError");
                }

            }
            catch (Exception)
            {
                throw;
            }
        }


        public ActionResult NewCommiteeReplyPendencyDetail(int id)
        {
            try
            {
                //tCommitteeReplyPendencyDetail model = new tCommitteeReplyPendencyDetail();
                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {
                    ViewBag.loginType = "VidhanSabha";
                }
                else
                {
                    ViewBag.loginType = "Department";
                }
                ViewBag.pendencyid = id;
                var list = (List<tCommitteeReplyPendencyDetail>)Helper.ExecuteService("CommitteReplyPendency", "ReplyPendencyDetailByPendencyId", id);
                return PartialView("_pendencyReplyListDetails", list);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult DeleteNewCommiteeReplyPendencyDetail(int id)
        {
            try
            {
                //tCommitteeReplyPendencyDetail model = new tCommitteeReplyPendencyDetail();
                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {
                    ViewBag.loginType = "VidhanSabha";
                }
                else
                {
                    ViewBag.loginType = "Department";
                }
                ViewBag.pendencyid = id;
                Helper.ExecuteService("CommitteReplyPendency", "DeletePendencyNewEntryByPendencyId", id);
                return PartialView("_PendencyReplyList");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult CommiteePendencyDetailNewReplies(int Pendencyid)
        {
            try
            {
                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {
                    ViewBag.loginType = "VidhanSabha";
                }
                else
                {
                    ViewBag.loginType = "Department";
                }
                tCommitteeReplyPendencyDetail obj = new tCommitteeReplyPendencyDetail();
                obj.createdBy = CurrentSession.UserName;
                obj.PendencyId = Pendencyid;
                var list = (List<tCommitteeReplyPendencyDetail>)Helper.ExecuteService("CommitteReplyPendency", "CommiteePendencyDetailNewReply", obj);
                return PartialView("_committeeReplyPendencyDetailListinPopup", list);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult CommiteePendencyDetailEditReply(int Pendencyid, int replyId)
        {
            try
            {
                tCommitteeReplyPendencyDetail obj = new tCommitteeReplyPendencyDetail();
                obj.PendencyReplyId = replyId;
                obj.PendencyId = Pendencyid;
                var list = (List<tCommitteeReplyPendencyDetail>)Helper.ExecuteService("CommitteReplyPendency", "CommiteePendencyDetailNewReply", obj);
                return PartialView("_committeeReplyPendencyDetailListinPopup", list);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult ReplyDetailListWithPendencyID(int Pendencyid)
        {
            try
            {
                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {
                    ViewBag.loginType = "VidhanSabha";
                }
                else
                {
                    ViewBag.loginType = "Department";
                }
                ViewBag.Pendencyid = Pendencyid;
                var list = (List<tCommitteeReplyPendencyDetail>)Helper.ExecuteService("CommitteReplyPendency", "ReplyDetailListWithPendencyID", Pendencyid);
                return PartialView("_committeeReplyPendencyDetailListinPopup", list);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult EditDepartmentReply(int Pendencyid, int pendencyReplyId)
        {
            try
            {
                tCommitteeReplyPendencyDetail obj = new tCommitteeReplyPendencyDetail();
                obj.PendencyId = Pendencyid;
                obj.PendencyReplyId = pendencyReplyId;
                var data = (tCommitteeReplyPendencyDetail)Helper.ExecuteService("CommitteReplyPendency", "GetPendencyRecordFromReplyID", obj);
                return PartialView("_EditColumnDepartmentReply", data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult EditReplyQuestionareeForCommittee(int Pendencyid, int pendencyReplyId)
        {
            try
            {
                tCommitteeReplyPendencyDetail obj = new tCommitteeReplyPendencyDetail();
                obj.PendencyId = Pendencyid;
                obj.PendencyReplyId = pendencyReplyId;
                var data = (tCommitteeReplyPendencyDetail)Helper.ExecuteService("CommitteReplyPendency", "GetPendencyRecordFromReplyID", obj);
                return PartialView("_EditColumnQuestionareeForCommittee", data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult EditReplyEntryCommentCommittee(int Pendencyid, int pendencyReplyId)
        {
            try
            {
                tCommitteeReplyPendencyDetail obj = new tCommitteeReplyPendencyDetail();
                obj.PendencyId = Pendencyid;
                obj.PendencyReplyId = pendencyReplyId;
                var data = (tCommitteeReplyPendencyDetail)Helper.ExecuteService("CommitteReplyPendency", "GetPendencyRecordFromReplyID", obj);
                ViewBag.mCommitteeReplyStatusList = (List<mCommitteeReplyStatus>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteReplyStatus", null);
                return PartialView("_EditColumnCommitteeReply", data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [ValidateInput(false)]
        public ActionResult saveEditNewDeptReply(int PendencyId, int PendencyReplyId, string DepartmentReply)
        {
            try
            {
                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {
                    ViewBag.loginType = "VidhanSabha";
                }
                else
                {
                    ViewBag.loginType = "Department";
                }
                tCommitteeReplyPendencyDetail obj = new tCommitteeReplyPendencyDetail();
                obj.PendencyId = PendencyId;
                obj.PendencyReplyId = PendencyReplyId;
                obj.DepartmentReply = DepartmentReply;
                if (PendencyReplyId == 0)
                {
                    obj.createdBy = CurrentSession.Name;
                }
                var list = (List<tCommitteeReplyPendencyDetail>)Helper.ExecuteService("CommitteReplyPendency", "saveEditNewDeptReplyContext", obj);
                return PartialView("_committeeReplyPendencyDetailListinPopup", list);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [ValidateInput(false)]
        public ActionResult saveEditCommentsOfCommittee(int PendencyId, int PendencyReplyId, string CommitteeReply, int Status)
        {
            try
            {
                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {
                    ViewBag.loginType = "VidhanSabha";
                }
                else
                {
                    ViewBag.loginType = "Department";
                }
                tCommitteeReplyPendencyDetail obj = new tCommitteeReplyPendencyDetail();
                obj.PendencyId = PendencyId;
                obj.PendencyReplyId = PendencyReplyId;
                obj.Recomm_CommentsOfCommittee = CommitteeReply;
                obj.Status = Status;
                var list = (List<tCommitteeReplyPendencyDetail>)Helper.ExecuteService("CommitteReplyPendency", "saveEditNewComitteeReplyContext", obj);
                return PartialView("_committeeReplyPendencyDetailListinPopup", list);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [ValidateInput(false)]
        public ActionResult saveEditQuestionareeForCommittee(int PendencyId, int PendencyReplyId, string Questionareee)
        {
            try
            {
                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {
                    ViewBag.loginType = "VidhanSabha";
                }
                else
                {
                    ViewBag.loginType = "Department";
                }
                tCommitteeReplyPendencyDetail obj = new tCommitteeReplyPendencyDetail();
                obj.PendencyId = PendencyId;
                obj.PendencyReplyId = PendencyReplyId;
                obj.QuestionaireForCommittee = Questionareee;
                var list = (List<tCommitteeReplyPendencyDetail>)Helper.ExecuteService("CommitteReplyPendency", "saveEditNewQuesionforComitteeContext", obj);
                return PartialView("_committeeReplyPendencyDetailListinPopup", list);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public ActionResult DeletePendencyReplyDetailsByReplyId(int Pendencyid, int pendencyReplyId)
        {
            try
            {
                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {
                    ViewBag.loginType = "VidhanSabha";
                }
                else
                {
                    ViewBag.loginType = "Department";
                }
                tCommitteeReplyPendencyDetail obj = new tCommitteeReplyPendencyDetail();
                obj.PendencyId = Pendencyid;
                obj.PendencyReplyId = pendencyReplyId;
                var list = (List<tCommitteeReplyPendencyDetail>)Helper.ExecuteService("CommitteReplyPendency", "DeletePendencyReplyDetailsByReplyIdContext", obj);
                return PartialView("_committeeReplyPendencyDetailListinPopup", list);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult NewDepartmentReply(int Pendencyid)
        {
            try
            {
                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {
                    ViewBag.loginType = "VidhanSabha";
                }
                else
                {
                    ViewBag.loginType = "Department";
                }
                tCommitteeReplyPendencyDetail obj = new tCommitteeReplyPendencyDetail();
                obj.PendencyId = Pendencyid;
                obj.PendencyReplyId = 0;
                return PartialView("_EditColumnDepartmentReply", obj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult OldNewPendencyReplyDetail(int Pendencyid)
        {
            try
            {
                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {
                    ViewBag.loginType = "VidhanSabha";
                }
                else
                {
                    ViewBag.loginType = "Department";
                }
                tCommitteeReplyPendencyDetail obj = new tCommitteeReplyPendencyDetail();
                obj.PendencyId = Pendencyid;
                obj.PendencyReplyId = 0;
                ViewBag.mCommitteeReplyStatusList = (List<mCommitteeReplyStatus>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteReplyStatus", null);
                return PartialView("_NewOldReplyPendencyDetailPopUp", obj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult saveOldNewDeptReply(tCommitteeReplyPendencyDetail model)
        {
            try
            {
                if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
                {
                    ViewBag.loginType = "VidhanSabha";
                }
                else
                {
                    ViewBag.loginType = "Department";
                }
                if (model.PendencyReplyId == 0)
                {
                    model.createdBy = CurrentSession.Name;
                }
                var list = (List<tCommitteeReplyPendencyDetail>)Helper.ExecuteService("CommitteReplyPendency", "saveOldNewPendencyReplyContext", model);
                return PartialView("_committeeReplyPendencyDetailListinPopup", list);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public ContentResult UploadFiles()
        {
            string url = "~/Committee/TempFile";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/Committee/TempFile"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }

        public JsonResult RemovePDFFiles()
        {
            string url = "~/Committee/TempFile";
            string directory = Server.MapPath(url);

            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadFile(tCommitteReplyPendency model)
        {
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);
            try
            {
                string Result = string.Empty;
                //Guid FId = Guid.NewGuid();
                //string fileName = FId + "PendingList.pdf";
                //var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemType", model);
                if (model.FinancialYear == "Select")
                {
                    Result = GetFileLIst(model);
                }
                else
                {
                    Result = GetFile(model);
                }

                MemoryStream output = new MemoryStream();
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);
                string htmlStringToConvert = Result;
                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");
                AddElementResult addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();
                output.Write(pdfBytes, 0, pdfBytes.Length);
                output.Position = 0;
                Response.AppendHeader("content-disposition", "attachment; filename=ReplyPendencyFile.pdf");
                return new FileStreamResult(output, "application/pdf");
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                document1.Close();
                // System.IO.File.Delete(path);
            }
        }

        public static string GetFile(tCommitteReplyPendency model)
        {
            var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemType", model);
            if (modelList != null && modelList.Count() > 0)
            {
                StringBuilder ReportList = new StringBuilder();
                ReportList.Append("<html>");
                ReportList.Append("<body>");
                ReportList.Append("<p>Pendency Reply PDF - " + model.FinancialYear + "</p>");

                //if (model.DepartmentName != null)
                //{
                ReportList.Append("<p>" + modelList.FirstOrDefault().DepartmentName + "</p>");
                //}
                //foreach (var item in modelList)
                //{
                //    ReportList.Append("<p>" + item.DepartmentName + "</p>");
                //}
                ReportList.Append("<table class='table table-condensed table-bordered' style='border: 1px dotted #808080;width:1000px;font-size:22px;'");
                ReportList.Append("<tr><td><b>Item No</b></td><td><b>Title</b></td><td><b>Status</b></td></tr>");
                foreach (tCommitteReplyPendency item in modelList)
                {
                    ReportList.Append("<tr><td>" + item.TypeNo + "</td><td>" + item.Subject + "</td><td>" + item.StatusName + "</td></tr>");
                }
                ReportList.Append("</table>");
                ReportList.Append("</body>");
                //ReportList.Append("</html>");
                return ReportList.ToString();
            }
            else
            {
                StringBuilder ReportList = new StringBuilder();
                ReportList.Append("<P>No Result Found</p>");
                return ReportList.ToString();
            }
        }

        public static string GetFileLIst(tCommitteReplyPendency model)
        {
            var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemTypeAll", model);
            if (modelList != null && modelList.Count() > 0)
            {
                StringBuilder ReportList = new StringBuilder();
                ReportList.Append("<html>");
                ReportList.Append("<body>");
                // ReportList.Append("<p>Pendency Reply PDF - " + model.FinancialYear + "</p>");

                //if (model.DepartmentName != null)
                //{
                ReportList.Append("<p>" + modelList.FirstOrDefault().DepartmentName + "</p>");
                //}
                //foreach (var item in modelList)
                //{
                //    ReportList.Append("<p>" + item.DepartmentName + "</p>");
                //}
                ReportList.Append("<table class='table table-condensed table-bordered' style='border: 1px dotted #808080;width:1000px;font-size:22px;'");
                ReportList.Append("<tr><td><b>Item No</b></td><td><b>Financial Year</b></td><td><b>Title</b></td><td><b>Status</b></td></tr>");
                foreach (tCommitteReplyPendency item in modelList)
                {
                    ReportList.Append("<tr><td>" + item.TypeNo + "</td><td>" + item.FinancialYear + "</td><td>" + item.Subject + "</td><td>" + item.StatusName + "</td></tr>");
                }
                ReportList.Append("</table>");
                ReportList.Append("</body>");
                //ReportList.Append("</html>");
                return ReportList.ToString();
            }
            else
            {
                StringBuilder ReportList = new StringBuilder();
                ReportList.Append("<P>No Result Found</p>");
                return ReportList.ToString();
            }
        }

        public ActionResult _VerifyPapersIndex()
        {
            try
            {
                SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
                string DepartmentId = CurrentSession.DeptID;
                string Officecode = CurrentSession.OfficeId;

                //string[] str = new string[3];
                //str[2] = CurrentSession.BranchId;
                //var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);

                string[] str = new string[3];
                str[0] = DepartmentId;
                str[1] = Officecode;
                str[2] = CurrentSession.BranchId;

                eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListR", str);
                return PartialView("_eFileListView", eList);
            }
            catch (Exception)
            {

                throw;
            }
        }

         // public ActionResult GetVerifiedPaperList(int pageId = 1, int pageSize = 25, int YearId = 5, int PapersId = 1, string p)
        public ActionResult GetVerifiedPaperList(int pageId, int pageSize, int YearId, int PapersId, string chkedStatus)
        {
            eFileAttachments model = new eFileAttachments();
            
            
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            //string currtBId = CurrentSession.commit;
            string Year = YearId.ToString();
            string papers = PapersId.ToString();
            model.CurrentSessionAId = AadharId;
            string[] str = new string[8];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = currtBId;
            str[4] = Year;
            str[5] = papers;
           CurrentSession.PaperStatus = chkedStatus;
            string[] strngBID = new string[2];
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[6] = value[0];//passing commId
            str[7] = chkedStatus;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperForVerify", str);           

            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            string[] str1 = new string[4];
            string[] strngBID1 = new string[2];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID1[0] = CurrentSession.BranchId;
            var value1 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID1);
            str1[3] = value1[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileAttachemts = pagedRecord;
            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            model.eFileLists = eList.eFileLists;


           // return PartialView("VerifyPaperNew", model);
            return PartialView("_VerifyPaperIndex", model);
        }

        public ActionResult ToverifyPaper(int pageId = 1, int pageSize = 25, int YearId = 5, int PapersId = 1, int strid = 0)//  used in now  HC MEnu _recieved list
        {
            //verified logic on Efileattachment on other context cause it was locked.
            string[] strFOrPending = new string[4];
            strFOrPending[0] = CurrentSession.UserName;
            strFOrPending[1] = Convert.ToString(strid);
            strFOrPending[2] = CurrentSession.BranchId;
            //  string[] strngBID = new string[2];
            // strngBID[0] = CurrentSession.BranchId;
            // var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            // strFOrPending[3] = value[0];//passing commId
            strFOrPending[3] = CurrentSession.AadharId;
            Helper.ExecuteService("CommitteReplyPendency", "ToVerifyAttachedPaper", strFOrPending);

            eFileAttachments model = new eFileAttachments();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string Year = YearId.ToString();
            string papers = PapersId.ToString();
            model.CurrentSessionAId = AadharId;
            string[] str = new string[8];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = currtBId;
            str[4] = Year;
            str[5] = papers;
            string[] strngBID = new string[2];
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[6] = value[0];//passing commId
            str[7] = "Ac";
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            CurrentSession.PaperStatus = "Ac";
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperForVerify", str);
            //model.eFileAttachemts = ListModel;

            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            string[] str1 = new string[4];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value1 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str1[3] = value1[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileAttachemts = pagedRecord;
            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            model.eFileLists = eList.eFileLists;
            return PartialView("_VerifyPaperIndex", model);
        }

        public ActionResult Grid_PendencyLIst(tCommitteReplyPendency model)
        {
            return PartialView("_GridPendencyRepyList", model);
        }


        public void SetupsearchJQGrid(JQGrid searchGrid)
        {
            JQGridColumn PendencyId = searchGrid.Columns.Find(c => c.DataField == "PendencyId");
            PendencyId.DataType = typeof(Int32);

            JQGridColumn FinancialYear = searchGrid.Columns.Find(c => c.DataField == "FinancialYear");
            FinancialYear.DataType = typeof(string);
            JQGridColumn DepartmentName = searchGrid.Columns.Find(c => c.DataField == "DepartmentName");
            DepartmentName.DataType = typeof(string);

        }

        public JsonResult OnCustomerDataRequested(int page, int rows, string sidx, string sord, string departmentId, int Status, int CommitteeId, string FinancialYear, int ItemTypeId)
        {


            var gridModel = new ComplexReplyPendency();
            tCommitteReplyPendency model = new tCommitteReplyPendency();
            model.page = page;
            model.rows = rows;
            model.sidx = sidx;
            model.sord = sord;
            model.DepartmentId = departmentId;
            model.Status = Status;
            model.CommitteeId = CommitteeId;
            model.FinancialYear = FinancialYear;
            model.ItemTypeId = ItemTypeId;

            //model.Deptid = CurrentSession.DeptID;
            this.SetRequestQueryStringPageToOne();

            if (Convert.ToInt32(CurrentSession.OfficeLevel) != 2 && ((Convert.ToInt32(CurrentSession.SubUserTypeID) != 2) || Convert.ToInt32(CurrentSession.SubUserTypeID) != 3))
            {// for vidhan sabha login
                ViewBag.loginType = "VidhanSabha";

                ViewBag.pendencyby = "Report";
                Int32 totalRecordsCount;
                // var resultSet = (List<tPISEmpSanctionpost1>)Helper.ExecuteService("PISModule", "GetAllSenctionPost_Jqgrid", s);

                var modelList = (List<tCommitteReplyPendency1>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyInGriD_Jquerygrid", model);
                var finalResult = modelList.LastOrDefault();
                totalRecordsCount = finalResult.totalRecordsCount;
                var FinalRecord = modelList.Take(Math.Max(0, modelList.Count() - 1));
                SetupsearchJQGrid(gridModel.PostGrid);
                JsonResult jsonResult = gridModel.PostGrid.DataBind(FinalRecord.AsQueryable());
                this.ModifyJsonResultData(jsonResult.Data, model.page, model.rows, totalRecordsCount);

                return jsonResult;
            }
            else
            { // for department login
                ViewBag.loginType = "Department";
                model.DepartmentId = CurrentSession.DeptID;
                ViewBag.pendencyby = "Report";
                Int32 totalRecordsCount;
                var modelList = (List<tCommitteReplyPendency1>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyInGriD_Jquerygrid", model);
                var finalResult = modelList.LastOrDefault();
                totalRecordsCount = finalResult.totalRecordsCount;
                var FinalRecord = modelList.Take(Math.Max(0, modelList.Count() - 1));
                SetupsearchJQGrid(gridModel.PostGrid);
                JsonResult jsonResult = gridModel.PostGrid.DataBind(FinalRecord.AsQueryable());
                this.ModifyJsonResultData(jsonResult.Data, model.page, model.rows, totalRecordsCount);
                return jsonResult;
            }
        }

        public ActionResult RejectPaperPop(int Id)
        {
            eFileAttachment model = new eFileAttachment();
            model.eFileAttachmentId = Id;


            return PartialView("_Partialreject", model);

        }

        public ActionResult SaveStatus(int Id, string Remarks)
        {
#pragma warning disable CS0219 // The variable 'strid' is assigned but its value is never used
            int pageId = 1; int pageSize = 25; int YearId = 5; int PapersId = 1; int strid = 0;
#pragma warning restore CS0219 // The variable 'strid' is assigned but its value is never used
            eFileAttachment model1 = new eFileAttachment();
            model1.eFileAttachmentId = Id;
            model1.RejectedRemarks = Remarks;
            model1.VerifiedBy = CurrentSession.UserName;//using verifed by for rejected here 
            model1 = (eFileAttachment)Helper.ExecuteService("CommitteReplyPendency", "ToRejectAttachedPaper", model1);

            eFileAttachments model = new eFileAttachments();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string Year = YearId.ToString();
            string papers = PapersId.ToString();
            model.CurrentSessionAId = AadharId;
            string[] str = new string[8];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = currtBId;
            str[4] = Year;
            str[5] = papers;
            string[] strngBID = new string[2];
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[6] = value[0];//passing commId
            str[7] = "Re";
            CurrentSession.PaperStatus = "Re";
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperForVerify", str);
            //model.eFileAttachemts = ListModel;

            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            string[] str1 = new string[4];
            str1[0] = DepartmentId;
            str1[1] = Officecode;
            str1[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value1 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str1[3] = value1[0];
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str1);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileAttachemts = pagedRecord;
            model.eFileLists = eList.eFileLists;
            return PartialView("_VerifyPaperIndex", model);
       
        }  //not used now


        [HttpPost]
        public ActionResult SummaryDetailsByDept(tCommitteReplyPendency model)
        {
            try
            {
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                model.page = 1;
                ViewBag.FileLocation = FileSettings.SettingValue;
                if (model.ItemTypeId == 1)
                {
                    ViewBag.itemtype = "Audit Parah(Finance)";
                }
                else if (model.ItemTypeId == 2)
                {
                    ViewBag.itemtype = "Audit Parah(Revenue)";
                }
                else if (model.ItemTypeId == 3)
                {
                    ViewBag.itemtype = "Audit Parah(Civil)";
                }
                else if (model.ItemTypeId == 4)
                {
                    ViewBag.itemtype = "Report(Original)";
                }
                else if (model.ItemTypeId == 5)
                {
                    ViewBag.itemtype = "Report(Action Taken)";
                }
                else if (model.ItemTypeId == 6)
                {
                    ViewBag.itemtype = "Report(Further Action Taken)";
                }
                else if (model.ItemTypeId == 7)
                {
                    ViewBag.itemtype = "Assurances";
                }
                else if (model.ItemTypeId == 8)
                {
                    ViewBag.itemtype = "Title";
                }
                else if (model.ItemTypeId == 9)
                {
                    ViewBag.itemtype = "Other";
                }
                var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencySummaryCount", model);
                return PartialView("_PendencyReplySummary", modelList);

            }
            catch (Exception)
            {
                return RedirectToAction("SessionTimedOut", "Error", new { @area = "" });
            }
        }

        // New work for item pendency by Lakshay

        public ActionResult GetAllPendencyNew(pHouseCommitteeItemPendency model)
        {
            model.CurrentDeptId = CurrentSession.DeptID;
            ViewBag.CurrentDeptId = CurrentSession.DeptID;
            List<tCommittee> CommitteeList = new List<tCommittee>();
            List<pHouseCommitteeItemPendency> ListModel = new List<pHouseCommitteeItemPendency>();
            if (model.CurrentDeptId == "HPD0001")
            {
                model.CurrentDeptId = "";
                List<tCommittee> CommitteeListNew = new List<tCommittee>();
                tPaperLaidV model1 = new tPaperLaidV();
                mBranches modelBranch = new mBranches();
                modelBranch.UserId = Guid.Parse(CurrentSession.UserID);
                model1.BranchesList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "GBranchList", modelBranch);
                foreach (var i in model1.BranchesList)
                {
                    string[] strngBID = new string[2];
                    strngBID[0] = Convert.ToString(i.BranchId);
                    List<tCommittee> CommitteeList1 = new List<tCommittee>();
                    CommitteeList1 = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("eFile", "nGetCommiteeIDAndNameByBranchID", strngBID);
                    CommitteeList.AddRange(CommitteeList1);
                }
                model.tCommitteeList = CommitteeList;
                ListModel = (List<pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetCustomItemPendency", model);
            }

            else
            {
                //List<pHouseCommitteeFiles> ListModel = new List<pHouseCommitteeFiles>();
                ListModel = (List<pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetCustomItemPendency", model);
            }
            if (ListModel != null)
            {
                ListModel.OrderBy(a => a.CreatedDate);
            }
            pHouseCommitteeItemPendency mdl1 = new pHouseCommitteeItemPendency();
            mdl1.HouseCommitteeItemPendency = ListModel;
            return PartialView("_nPendencyReplyList", mdl1);

        }

        public JsonResult GetSubItemByParentId(string ParentId)
        {
            SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency mdl = new SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency> pHouseCommitteeItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency> pHouseCommitteeSubItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency>();
            mdl.ParentId = Convert.ToInt16(ParentId.Trim());
            mdl.CurrentDeptId = CurrentSession.DeptID;
            if (mdl.CurrentDeptId == "HPD0001")
            {
                mdl.CurrentDeptId = "";
                pHouseCommitteeItemPendency = (List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetItemPendencyByParentId", mdl);
            }
            else
            {
                pHouseCommitteeItemPendency = (List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetItemPendencyByParentId", mdl);
            }

            //pHouseCommitteeSubFilesList = (List<SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles>)Helper.ExecuteService("eFile", "GetSubCommFilesByParentId", mdl);

            return Json(pHouseCommitteeItemPendency, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult ComposeNewPendency()
        {
            List<tCommittee> CommitteeList = new List<tCommittee>();
            tPaperLaidV model = new tPaperLaidV();
            mBranches modelBranch = new mBranches();
            modelBranch.UserId = Guid.Parse(CurrentSession.UserID);
            model.BranchesList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "GBranchList", modelBranch);
            string[] str1 = new string[4];
            str1[0] = CurrentSession.DeptID;
            str1[1] = CurrentSession.OfficeId;
#pragma warning disable CS0219 // The variable 'CommitteeId' is assigned but its value is never used
            string CommitteeId = "";
#pragma warning restore CS0219 // The variable 'CommitteeId' is assigned but its value is never used
            foreach (var i in model.BranchesList)
            {
                str1[2] = Convert.ToString(i.BranchId);
                string[] strngBID = new string[2];
                strngBID[0] = Convert.ToString(i.BranchId);
                List<tCommittee> CommitteeList1 = new List<tCommittee>();
                CommitteeList1 = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("eFile", "nGetCommiteeIDAndNameByBranchID", strngBID);
                CommitteeList.AddRange(CommitteeList1);

            }
            ViewBag.CommitteeList = new SelectList(CommitteeList, "CommitteeId", "CommitteeName", null);
            
            mDepartment mdep = new mDepartment();
            mdep.deptId = str1[0];
            var depsrtmentlist = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
            //List<mDepartment> DepartmentList1 = new List<mDepartment>();
            //DepartmentList1 = depsrtmentlist.Distinct().ToList();
            ViewBag.ListOfCodes = depsrtmentlist;

            ///Get All eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);



            //Get All Items
            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetFileItemList", null);
            ViewBag.FileItem = new SelectList(_ListItems, "ID", "ItemTypeName", null);

            //Get Assembly and Session
            var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            //assmeblyList.Add(new{te})
            ViewBag.assmeblyList = assmeblyList;
            //Code for SessionList 
            SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
            data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
            var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
            ViewBag.sessionList = sessionList;
            mSessionDate mSession = new mSessionDate();
            mSession.AssemblyId = data.AssemblyID;
            mSession.SessionId = sessionList.FirstOrDefault().SessionCode;
            //  var SessionDateList2 = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);

            var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", mSession);
            ViewBag.SessionDateList = sessionDateList;
            return PartialView("_ComposeNewPendency");
        }

        [HttpPost]
        public JsonResult SaveNewPendency(List<pHouseCommitteeItemPendency> LetterList)
        {
            int count = 0;
            foreach (var item in LetterList)
            {
                pHouseCommitteeItemPendency obj = new pHouseCommitteeItemPendency();
                //obj.AssemblyID = item.AssemblyID;
                //obj.SessionID = item.SessionID;
                obj.FinancialYear = item.FinancialYear;
                //obj.CommitteeId = item.CommitteeId;
                //obj.eFileID = item.eFileID;     
                obj.SentByDepartmentId = item.SentByDepartmentId;
                //obj.SessionDate = item.SessionDate;
                //obj.SentOnDateByVS = item.SentOnDateByVS;
                obj.ItemTypeId = item.ItemTypeId;
                obj.ItemTypeName = item.ItemTypeName;
                obj.SubItemTypeId = item.SubItemTypeId;
                obj.SubItemTypeName = item.SubItemTypeName;
                obj.CAGYear = item.CAGYear;
                obj.ItemNumber = item.ItemNumber;
                obj.ItemNatureId = item.ItemNatureId;
                obj.ItemNature = item.ItemNature;
                obj.ItemDescription = item.ItemDescription;
                obj.Status = item.Status;
                obj.CreatedBy = new Guid(CurrentSession.UserID);
                obj.ModifiedBy = new Guid(CurrentSession.UserID);
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string ItemTypeName = obj.ItemTypeName.Replace(" ", "");
                string directory = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Reply/";

                string Dirname = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Reply/";
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }
                string fileName = item.ItemReplyPdfPath;
                if (item.ItemReplyPdfPath != null)
                {
                    string sourcedirectory = Server.MapPath("~/LOBTemp/");
                    string sourceFile = System.IO.Path.Combine(sourcedirectory, fileName);
                    string destFile = System.IO.Path.Combine(directory, fileName);
                    string FullFileNamePath = System.IO.Path.Combine(Dirname, fileName);
                    System.IO.File.Copy(sourceFile, destFile, true);
                    //Delete File from temp location
                    System.IO.File.Delete(sourceFile);
                    obj.ItemReplyPdfPath = FullFileNamePath;
                }
                string directory1 = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Annexures/";
                string Dirname1 = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Annexures/";
                if (!System.IO.Directory.Exists(directory1))
                {
                    System.IO.Directory.CreateDirectory(directory1);
                }
                string FullAnnexNamePath = "";
                if (item.AnnexPdfPath != null)
                {
                    string[] s = item.AnnexPdfPath.Split(',');
                    for (int i = 0; i < s.Length - 1; i++)
                    {
                        string AnnexfileName = s[i].Trim(' ');
                        //string AnnexfileName = item.AnnexPdfPath;
                        string source = Server.MapPath("~/LOBTemp/");
                        string AnnexsourceFile = System.IO.Path.Combine(source, AnnexfileName);
                        string AnnexdestFile = System.IO.Path.Combine(directory1, AnnexfileName);
                        //FullAnnexNamePath = FullAnnexNamePath + System.IO.Path.Combine(Dirname1, AnnexfileName) + ",";
                        FullAnnexNamePath = System.IO.Path.Combine(Dirname1, AnnexfileName);
                        System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
                        System.IO.File.Delete(AnnexsourceFile);
                    }

                }

                //For Word File
                string Wdirectory = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/DocFiles/";
                string WDirname = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/DocFiles/";
                if (!System.IO.Directory.Exists(Wdirectory))
                {
                    System.IO.Directory.CreateDirectory(Wdirectory);
                }
                string Docfilepath = item.DocFilePath;
                if (item.DocFilePath != null)
                {
                    string sourcedirectory = Server.MapPath("~/LOBTemp/");
                    string sourceFile = System.IO.Path.Combine(sourcedirectory, Docfilepath);
                    string destFile = System.IO.Path.Combine(Wdirectory, Docfilepath);
                    string FullFileNamePath = System.IO.Path.Combine(WDirname, Docfilepath);
                    System.IO.File.Copy(sourceFile, destFile, true);
                    //Delete File from temp location
                    System.IO.File.Delete(sourceFile);
                    obj.DocFilePath = FullFileNamePath;
                    obj.DocFileName = item.DocFileName;
                }



                obj.AnnexPdfPath = FullAnnexNamePath;
                obj.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                int res = (int)Helper.ExecuteService("eFile", "SaveNewPendency", obj);

            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult AddToComposedPendency(string PendencyId)
        {

            pHouseCommitteeItemPendency model = new pHouseCommitteeItemPendency();
            model.ParentId = Convert.ToInt16(PendencyId);
            SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency mdl = new SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency> pHouseCommitteeItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>();
            List<SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles> pHouseCommitteeSubFilesList = new List<SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles>();
            mdl.PendencyId = Convert.ToInt16(PendencyId.Trim());
            pHouseCommitteeItemPendency = (List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetDetailsByParentId", mdl);
            foreach (var item in pHouseCommitteeItemPendency)
            {
                // model.CommitteeId = item.CommitteeId;
                //model.CurrentDeptId = item.ToDepartmentID;
                model.ItemNumber = item.ItemNumber;
                model.ItemDescription = item.ItemDescription;
                model.ItemTypeId = item.ItemTypeId;
                model.ItemTypeName = item.ItemTypeName;
                model.SubItemTypeId = item.SubItemTypeId;
                model.SubItemTypeName = item.SubItemTypeName;
                model.CAGYear = item.CAGYear;
                model.ItemNature = item.ItemNature;
                //model.SessionDate = item.SessionDate;
                model.FinancialYear = item.FinancialYear;
                //model.AssemblyID = item.AssemblyID;
                //model.SessionID = item.SessionID;
                model.SentByDepartmentId = item.SentByDepartmentId;
                model.IsLocked = false;
               
            }

           

            ///Get All eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            model.eFilePaperTypeList = _ListeFilePaperType;


            var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            //assmeblyList.Add(new{te})
            model.AssmeblyList = assmeblyList;
            //Code for SessionList 
            SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
            //data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
            //var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
            var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetAllSessionNames", null);
            model.SessionList = sessionList;
            //Get All Items
            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetFileItemList", null);
            model.mCommitteeReplyItemTypeList = _ListItems;

            mDepartment mdep = new mDepartment();
            mdep.deptId = CurrentSession.DeptID;
            var depsrtmentlist = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
            model.mDepartmentList = depsrtmentlist;


            //model.ParentId = Convert.ToInt16(ParentId);
            //ViewBag.ParentId = ParentId;
            return PartialView("AddToComposedPendency", model);
        }

        [HttpPost]
        public JsonResult SaveToComposedPendency(List<pHouseCommitteeItemPendency> LetterList)
        {
            int count = 0;
            foreach (var item in LetterList)
            {
                pHouseCommitteeSubItemPendency obj = new pHouseCommitteeSubItemPendency();
                obj.ParentId = item.ParentId.HasValue ? item.ParentId.Value : 0;               
                obj.FinancialYear = item.FinancialYear;               
                obj.SentByDepartmentId = item.SentByDepartmentId;              
                obj.ItemTypeId = item.ItemTypeId;
                obj.ItemTypeName = item.ItemTypeName;
                obj.ItemNumber = item.ItemNumber;
                obj.SubItemTypeId = item.SubItemTypeId;
                obj.SubItemTypeName = item.SubItemTypeName;
                obj.CAGYear = item.CAGYear;
                obj.ItemNatureId = item.ItemNatureId;
                obj.ItemNature = item.ItemNature;
                obj.ItemDescription = item.ItemDescription;
                obj.Status = item.Status;
                obj.IsLocked = false;
                obj.CreatedBy = new Guid(CurrentSession.UserID);
                obj.ModifiedBy = new Guid(CurrentSession.UserID);
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string ItemTypeName = obj.ItemTypeName.Replace(" ", "");
                string directory = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Reply/";
                string Dirname = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Reply/";
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }
                string fileName = item.ItemReplyPdfPath;
                if (item.ItemReplyPdfPath != null)
                {
                    string sourcedirectory = Server.MapPath("~/LOBTemp/");
                    string sourceFile = System.IO.Path.Combine(sourcedirectory, fileName);
                    string destFile = System.IO.Path.Combine(directory, fileName);
                    string FullFileNamePath = System.IO.Path.Combine(Dirname, fileName);
                    System.IO.File.Copy(sourceFile, destFile, true);
                    //Delete File from temp location
                    System.IO.File.Delete(sourceFile);
                    obj.ItemReplyPdfPath = FullFileNamePath;
                }
                string directory1 = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Annexures/";
                string Dirname1 = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Annexures/";
                if (!System.IO.Directory.Exists(directory1))
                {
                    System.IO.Directory.CreateDirectory(directory1);
                }
                string FullAnnexNamePath = "";
                if (item.AnnexPdfPath != null)
                {
                    string[] s = item.AnnexPdfPath.Split(',');
                    for (int i = 0; i < s.Length - 1; i++)
                    {
                        string AnnexfileName = s[i].Trim(' ');
                        //string AnnexfileName = item.AnnexPdfPath;
                        string source = Server.MapPath("~/LOBTemp/");
                        string AnnexsourceFile = System.IO.Path.Combine(source, AnnexfileName);
                        string AnnexdestFile = System.IO.Path.Combine(directory1, AnnexfileName);
                        //FullAnnexNamePath = FullAnnexNamePath + System.IO.Path.Combine(Dirname1, AnnexfileName) + ",";
                        FullAnnexNamePath = System.IO.Path.Combine(Dirname1, AnnexfileName);
                        System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
                        System.IO.File.Delete(AnnexsourceFile);
                    }

                }
                //For Word File
                string Wdirectory = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/DocFiles/";
                string WDirname = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/DocFiles/";


                //string Wdirectory = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + "DocFiles" + "/" + obj.ItemTypeName + "/ItemPendency/";
                //string WDirname = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + "DocFiles" + "/" + obj.ItemTypeName + "/ItemPendency/";
                if (!System.IO.Directory.Exists(Wdirectory))
                {
                    System.IO.Directory.CreateDirectory(Wdirectory);
                }
                string Docfilepath = item.DocFilePath;
                if (item.DocFilePath != null)
                {
                    string sourcedirectory = Server.MapPath("~/LOBTemp/");
                    string sourceFile = System.IO.Path.Combine(sourcedirectory, Docfilepath);
                    string destFile = System.IO.Path.Combine(Wdirectory, Docfilepath);
                    string FullFileNamePath = System.IO.Path.Combine(WDirname, Docfilepath);
                    System.IO.File.Copy(sourceFile, destFile, true);
                    //Delete File from temp location
                    System.IO.File.Delete(sourceFile);
                    obj.DocFilePath = FullFileNamePath;
                }

                obj.DocFileName = item.DocFileName;
                obj.AnnexPdfPath = FullAnnexNamePath;
                obj.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                int res = (int)Helper.ExecuteService("eFile", "SaveToComposedPendency", obj);

            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        //For Linking
        public JsonResult GetSubItemByParentIdForLinking(string ParentId)
        {
            SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency mdl = new SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency> pHouseCommitteeItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency> pHouseCommitteeSubItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency>();
            mdl.ParentId = Convert.ToInt16(ParentId.Trim());
            mdl.CurrentDeptId = CurrentSession.DeptID;
            if (mdl.CurrentDeptId == "HPD0001")
            {
                mdl.CurrentDeptId = "";
                pHouseCommitteeItemPendency = (List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetItemPendencyByParentIdforLinking", mdl);
            }
            else
            {
                pHouseCommitteeItemPendency = (List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetItemPendencyByParentIdforLinking", mdl);
            }

            //pHouseCommitteeSubFilesList = (List<SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles>)Helper.ExecuteService("eFile", "GetSubCommFilesByParentId", mdl);

            return Json(pHouseCommitteeItemPendency, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult ShowPendencyItemList(string CommitteeName, string SelectedDeptId, string ItemTypeName, string ItemTypeId, string RowIndexId)
        {
            pHouseCommitteeItemPendency model = new pHouseCommitteeItemPendency();
            model.SentByDepartmentId = SelectedDeptId;
            model.CurrentDeptId = CurrentSession.DeptID;
            model.ItemTypeId = Convert.ToInt16(ItemTypeId);
            List<tCommittee> CommitteeList = new List<tCommittee>();
            List<pHouseCommitteeItemPendency> ListModel = new List<pHouseCommitteeItemPendency>();
            if (model.CurrentDeptId == "HPD0001")
            {
                model.CurrentDeptId = "";
                List<tCommittee> CommitteeListNew = new List<tCommittee>();
                tPaperLaidV model1 = new tPaperLaidV();
                mBranches modelBranch = new mBranches();
                modelBranch.UserId = Guid.Parse(CurrentSession.UserID);
                model1.BranchesList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "GBranchList", modelBranch);
                foreach (var i in model1.BranchesList)
                {
                    string[] strngBID = new string[2];
                    strngBID[0] = Convert.ToString(i.BranchId);
                    List<tCommittee> CommitteeList1 = new List<tCommittee>();
                    CommitteeList1 = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("eFile", "nGetCommiteeIDAndNameByBranchID", strngBID);
                    CommitteeList.AddRange(CommitteeList1);
                }
                model.tCommitteeList = CommitteeList;
                ListModel = (List<pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetPendencyListforLinking", model);
            }

            else
            {
                //List<pHouseCommitteeFiles> ListModel = new List<pHouseCommitteeFiles>();
                ListModel = (List<pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetPendencyListforLinking", model);
            }
            if (ListModel != null)
            {
                ListModel.OrderBy(a => a.CreatedDate);
            }
            pHouseCommitteeItemPendency mdl1 = new pHouseCommitteeItemPendency();
            mdl1.HouseCommitteeItemPendency = ListModel;
            mdl1.ItemTypeName = ItemTypeName;
            mdl1.CommitteeName = CommitteeName;
            return PartialView(mdl1);
        }





        [HttpPost]
        public ActionResult UploadDocFiles()
        {
            String pfileName = "";
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    NameValueCollection PFileName = Request.Form;
                    foreach (var key in PFileName.AllKeys)
                    {
                        pfileName = PFileName[key];
                    }
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    HttpPostedFileBase file = files[0];
                    string fname;
                    // Checking for Internet Explorer 
                    fname = file.FileName;
                    string[] s = fname.Split('.');
                    //Code for Assembly Folder File Existense Check
                    pfileName = Convert.ToString(DateTime.Now.ToFileTime()) + "." + s[1];
                    var filepath = System.IO.Path.Combine("\\LOBTemp\\" + pfileName);
                    // Get the complete folder path and store the file inside it.  
                    fname = Path.Combine(Server.MapPath(filepath));
                    file.SaveAs(fname);
                    // }
                    // Returns message that successfully uploaded  
                    return Json(pfileName, "File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
        [HttpGet]
        public FileResult GetDocFile(string parameter)
        {


            //string a = Request.QueryString["parameter"];



            var filepath = System.IO.Path.Combine("\\LOBTemp\\" + parameter.Trim());
            // Get the complete folder path and store the file inside it.  
            string fname = Path.Combine(Server.MapPath(filepath));
            string ReportURL = fname;
            byte[] FileBytes = System.IO.File.ReadAllBytes(ReportURL);
            return File(FileBytes, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        }

        public JsonResult CheckItemExists(string ItemDept, string ItemType, string ItemSubType, string ItemFINYear, string ItemCAGYear, string ItemNumber)
        {
            bool count;
            SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency mdl = new SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency> pHouseCommitteeItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency> pHouseCommitteeSubItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency>();
            mdl.ItemNumber = ItemNumber.Trim();
            mdl.SentByDepartmentId = ItemDept;
            mdl.ItemTypeId = Convert.ToInt16(ItemType);
            if (ItemSubType != "null")
            {
                mdl.SubItemTypeId = Convert.ToInt16(ItemSubType);
            }
            else
            {
                mdl.SubItemTypeId = null;
            }
            mdl.FinancialYear = ItemFINYear;
            mdl.CAGYear = ItemCAGYear;
            var value = (int)Helper.ExecuteService("CommitteReplyPendency", "CheckItemNumberExists", mdl);
            if (value == 1)// Item No exists
            {
                count = true;
            }
            else
            {
                count = false;
            }

            return Json(count, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LockMainPendency(string PendencyId)
        {
            SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency mdl = new SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency> pHouseCommitteeItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency> pHouseCommitteeSubItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency>();
            mdl.PendencyId = Convert.ToInt16(PendencyId);

            var value = (int)Helper.ExecuteService("CommitteReplyPendency", "LockMainPendency", mdl);
            return Json(value, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LockSubPendency(string SubPendencyId)
        {
            SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency mdl = new SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency> pHouseCommitteeItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency>();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency> pHouseCommitteeSubItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency>();
            mdl.SubPendencyId = Convert.ToInt16(SubPendencyId);

            var value = (int)Helper.ExecuteService("CommitteReplyPendency", "LockSubPendency", mdl);
            return Json(value, JsonRequestBehavior.AllowGet);
        }



        public PartialViewResult EditMainPendency(string PendencyId)
        {

            pHouseCommitteeItemPendency model = new pHouseCommitteeItemPendency();
            model.ParentId = Convert.ToInt16(PendencyId);
            SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency mdl = new SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency> pHouseCommitteeItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>();
            List<SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles> pHouseCommitteeSubFilesList = new List<SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles>();
            mdl.PendencyId = Convert.ToInt16(PendencyId.Trim());
            pHouseCommitteeItemPendency = (List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetDetailsByPendencyId", mdl);
            foreach (var item in pHouseCommitteeItemPendency)
            {
                model.PendencyId = Convert.ToInt16(PendencyId);
                model.ItemNumber = item.ItemNumber;
                model.ItemDescription = item.ItemDescription;
                model.ItemTypeId = item.ItemTypeId;
                model.ItemTypeName = item.ItemTypeName;
                model.SubItemTypeId = item.SubItemTypeId;
                model.SubItemTypeName = item.SubItemTypeName;
                model.CAGYear = item.CAGYear;
                model.ItemNature = item.ItemNature;
                model.ItemNatureId = item.ItemNatureId;
                //model.SessionDate = item.SessionDate;
                model.FinancialYear = item.FinancialYear;
                //model.AssemblyID = item.AssemblyID;
                //model.SessionID = item.SessionID;
                model.SentByDepartmentId = item.SentByDepartmentId;
                model.IsLocked = false;
                model.ItemReplyPdfPath = item.ItemReplyPdfPath;
                model.ItemReplyPdfNameByUser = item.ItemReplyPdfNameByUser;
                model.AnnexPdfPath = item.AnnexPdfPath;
                model.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                model.DocFileName = item.DocFileName;
                model.DocFilePath = item.DocFilePath;
               
            }



            ///Get All eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            model.eFilePaperTypeList = _ListeFilePaperType;
            var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            model.AssmeblyList = assmeblyList;

            //Code for SessionList 
            SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
            var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetAllSessionNames", null);
            model.SessionList = sessionList;

            //Get All Items
            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetCustomFileItemList", null);
            model.mCommitteeReplyItemTypeList = _ListItems;

            var _SubItemList = (List<mCommitteeReplySubItemType>)Helper.ExecuteService("eFile", "GetSubItemList", null);
            model.mCommitteeReplySubItemTypeList = _SubItemList;

            mDepartment mdep = new mDepartment();
            mdep.deptId = CurrentSession.DeptID;
            var depsrtmentlist = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
            model.mDepartmentList = depsrtmentlist;
            return PartialView("EditMainPendency", model);
        }


        public PartialViewResult EditSubPendency(string SubPendencyId)
        {

            pHouseCommitteeSubItemPendency model = new pHouseCommitteeSubItemPendency();
            SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency mdl = new SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency();
            List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency> pHouseCommitteeSubItemPendency = new List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency>();

            mdl.SubPendencyId = Convert.ToInt16(SubPendencyId.Trim());
            pHouseCommitteeSubItemPendency = (List<SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetDetailsBySubPendencyId", mdl);
            foreach (var item in pHouseCommitteeSubItemPendency)
            {
                model.SubPendencyId = Convert.ToInt16(SubPendencyId.Trim());
                model.ItemNumber = item.ItemNumber;
                model.ItemDescription = item.ItemDescription;
                model.ItemTypeId = item.ItemTypeId;
                model.ItemTypeName = item.ItemTypeName;
                model.SubItemTypeId = item.SubItemTypeId;
                model.SubItemTypeName = item.SubItemTypeName;
                model.CAGYear = item.CAGYear;
                model.ItemNature = item.ItemNature;
                model.ItemNatureId = item.ItemNatureId;
                //model.SessionDate = item.SessionDate;
                model.FinancialYear = item.FinancialYear;
                //model.AssemblyID = item.AssemblyID;
                //model.SessionID = item.SessionID;
                model.SentByDepartmentId = item.SentByDepartmentId;
                model.IsLocked = false;
                model.ItemReplyPdfPath = item.ItemReplyPdfPath;
                model.ItemReplyPdfNameByUser = item.ItemReplyPdfNameByUser;
                model.AnnexPdfPath = item.AnnexPdfPath;
                model.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                model.DocFileName = item.DocFileName;
                model.DocFilePath = item.DocFilePath;

            }



            ///Get All eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            model.eFilePaperTypeList = _ListeFilePaperType;
           

            //Get All Items

            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetCustomFileItemList", null);
            model.mCommitteeReplyItemTypeList = _ListItems;

            var _SubItemList = (List<mCommitteeReplySubItemType>)Helper.ExecuteService("eFile", "GetSubItemList", null);
            model.mCommitteeReplySubItemTypeList = _SubItemList;

            mDepartment mdep = new mDepartment();
            mdep.deptId = CurrentSession.DeptID;
            var depsrtmentlist = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
            model.mDepartmentList = depsrtmentlist;
            return PartialView("EditSubPendency", model);
        }

        [HttpPost]
        public JsonResult UpdateMainPendency(List<pHouseCommitteeItemPendency> LetterList)
        {
            int count = 0;
            foreach (var item in LetterList)
            {
                pHouseCommitteeItemPendency obj = new pHouseCommitteeItemPendency();
                obj.ParentId = item.ParentId.HasValue ? item.ParentId.Value : 0;
                obj.PendencyId = item.PendencyId;
                obj.FinancialYear = item.FinancialYear;
                obj.SentByDepartmentId = item.SentByDepartmentId;
                obj.ItemTypeId = item.ItemTypeId;
                obj.ItemTypeName = item.ItemTypeName;
                obj.ItemNumber = item.ItemNumber;
                obj.SubItemTypeId = item.SubItemTypeId;
                obj.SubItemTypeName = item.SubItemTypeName;
                obj.CAGYear = item.CAGYear;
                obj.ItemNatureId = item.ItemNatureId;
                obj.ItemNature = item.ItemNature;
                obj.ItemDescription = item.ItemDescription;
                obj.Status = 0;
                obj.IsLocked = false;
                obj.ModifiedBy = new Guid(CurrentSession.UserID);
                obj.ModifiedDate = DateTime.Now;
                obj.ReplyPdfIsChange = item.ReplyPdfIsChange;
                obj.AnnexPdfIsChange = item.AnnexPdfIsChange;
                obj.DocFileIsChange = item.DocFileIsChange;

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string ItemTypeName = obj.ItemTypeName.Replace(" ", "");
                if (item.ReplyPdfIsChange == true) //Reply pdf has been changed,so get from the temp folder
                {
                    string directory = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Reply/";
                    string Dirname = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Reply/";
                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }
                    string fileName = item.ItemReplyPdfPath;
                    if (item.ItemReplyPdfPath != null)
                    {
                        string sourcedirectory = Server.MapPath("~/LOBTemp/");
                        string sourceFile = System.IO.Path.Combine(sourcedirectory, fileName);
                        string destFile = System.IO.Path.Combine(directory, fileName);
                        string FullFileNamePath = System.IO.Path.Combine(Dirname, fileName);
                        System.IO.File.Copy(sourceFile, destFile, true);
                        //Delete File from temp location
                        System.IO.File.Delete(sourceFile);
                        obj.ItemReplyPdfPath = FullFileNamePath;
                    }

                }


                if (item.AnnexPdfIsChange == true) //Anex pdf has been changed,so get from the temp folder
                {
                    string directory1 = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Annexures/";
                    string Dirname1 = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Annexures/";
                    if (!System.IO.Directory.Exists(directory1))
                    {
                        System.IO.Directory.CreateDirectory(directory1);
                    }
                    string FullAnnexNamePath = "";
                    if (item.AnnexPdfPath != null)
                    {
                        string[] s = item.AnnexPdfPath.Split(',');
                        for (int i = 0; i < s.Length - 1; i++)
                        {
                            string AnnexfileName = s[i].Trim(' ');
                            //string AnnexfileName = item.AnnexPdfPath;
                            string source = Server.MapPath("~/LOBTemp/");
                            string AnnexsourceFile = System.IO.Path.Combine(source, AnnexfileName);
                            string AnnexdestFile = System.IO.Path.Combine(directory1, AnnexfileName);
                            //FullAnnexNamePath = FullAnnexNamePath + System.IO.Path.Combine(Dirname1, AnnexfileName) + ",";
                            FullAnnexNamePath = System.IO.Path.Combine(Dirname1, AnnexfileName);
                            System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
                            System.IO.File.Delete(AnnexsourceFile);
                            obj.AnnexPdfPath = FullAnnexNamePath;
                            obj.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                        }

                    }

                }
                //For Word File
                if (item.DocFileIsChange == true) //Word file has been changed,so get from the temp folder
                {
                    string Wdirectory = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/DocFiles/";
                    string WDirname = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/DocFiles/";
                    if (!System.IO.Directory.Exists(Wdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Wdirectory);
                    }
                    string Docfilepath = item.DocFilePath;
                    if (item.DocFilePath != null)
                    {
                        string sourcedirectory = Server.MapPath("~/LOBTemp/");
                        string sourceFile = System.IO.Path.Combine(sourcedirectory, Docfilepath);
                        string destFile = System.IO.Path.Combine(Wdirectory, Docfilepath);
                        string FullFileNamePath = System.IO.Path.Combine(WDirname, Docfilepath);
                        System.IO.File.Copy(sourceFile, destFile, true);
                        //Delete File from temp location
                        System.IO.File.Delete(sourceFile);
                        obj.DocFilePath = FullFileNamePath;
                        obj.DocFileName = item.DocFileName;
                    }
                }
                int res = (int)Helper.ExecuteService("CommitteReplyPendency", "UpdateMainPendency", obj);

            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult UpdateSubPendency(List<pHouseCommitteeSubItemPendency> LetterList)
        {
            int count = 0;
            foreach (var item in LetterList)
            {
                pHouseCommitteeSubItemPendency obj = new pHouseCommitteeSubItemPendency();
                obj.SubPendencyId = item.SubPendencyId;
                obj.FinancialYear = item.FinancialYear;
                obj.SentByDepartmentId = item.SentByDepartmentId;
                obj.ItemTypeId = item.ItemTypeId;
                obj.ItemTypeName = item.ItemTypeName;
                obj.ItemNumber = item.ItemNumber;
                obj.SubItemTypeId = item.SubItemTypeId;
                obj.SubItemTypeName = item.SubItemTypeName;
                obj.CAGYear = item.CAGYear;
                obj.ItemNatureId = item.ItemNatureId;
                obj.ItemNature = item.ItemNature;
                obj.ItemDescription = item.ItemDescription;
                obj.Status = 0;
                obj.IsLocked = false;
                obj.ModifiedBy = new Guid(CurrentSession.UserID);
                obj.ModifiedDate = DateTime.Now;
                obj.ReplyPdfIsChange = item.ReplyPdfIsChange;
                obj.AnnexPdfIsChange = item.AnnexPdfIsChange;
                obj.DocFileIsChange = item.DocFileIsChange;

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string ItemTypeName = obj.ItemTypeName.Replace(" ", "");
                if (item.ReplyPdfIsChange == true) //Reply pdf has been changed,so get from the temp folder
                {
                    string directory = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Reply/";
                    string Dirname = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Reply/";
                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }
                    string fileName = item.ItemReplyPdfPath;
                    if (item.ItemReplyPdfPath != null)
                    {
                        string sourcedirectory = Server.MapPath("~/LOBTemp/");
                        string sourceFile = System.IO.Path.Combine(sourcedirectory, fileName);
                        string destFile = System.IO.Path.Combine(directory, fileName);
                        string FullFileNamePath = System.IO.Path.Combine(Dirname, fileName);
                        System.IO.File.Copy(sourceFile, destFile, true);
                        //Delete File from temp location
                        System.IO.File.Delete(sourceFile);
                        obj.ItemReplyPdfPath = FullFileNamePath;
                    }

                }


                if (item.AnnexPdfIsChange == true) //Anex pdf has been changed,so get from the temp folder
                {
                    string directory1 = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Annexures/";
                    string Dirname1 = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/Annexures/";
                    if (!System.IO.Directory.Exists(directory1))
                    {
                        System.IO.Directory.CreateDirectory(directory1);
                    }
                    string FullAnnexNamePath = "";
                    if (item.AnnexPdfPath != null)
                    {
                        string[] s = item.AnnexPdfPath.Split(',');
                        for (int i = 0; i < s.Length - 1; i++)
                        {
                            string AnnexfileName = s[i].Trim(' ');
                            //string AnnexfileName = item.AnnexPdfPath;
                            string source = Server.MapPath("~/LOBTemp/");
                            string AnnexsourceFile = System.IO.Path.Combine(source, AnnexfileName);
                            string AnnexdestFile = System.IO.Path.Combine(directory1, AnnexfileName);
                            //FullAnnexNamePath = FullAnnexNamePath + System.IO.Path.Combine(Dirname1, AnnexfileName) + ",";
                            FullAnnexNamePath = System.IO.Path.Combine(Dirname1, AnnexfileName);
                            System.IO.File.Copy(AnnexsourceFile, AnnexdestFile, true);
                            System.IO.File.Delete(AnnexsourceFile);
                            obj.AnnexPdfPath = FullAnnexNamePath;
                            obj.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                        }

                    }

                }
                //For Word File
                if (item.DocFileIsChange == true) //Word file has been changed,so get from the temp folder
                {
                    string Wdirectory = FileSettings.SettingValue + "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/DocFiles/";
                    string WDirname = "ItemPendency/" + "/" + obj.SentByDepartmentId + "/" + obj.FinancialYear + "/" + ItemTypeName + "/DocFiles/";
                    if (!System.IO.Directory.Exists(Wdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Wdirectory);
                    }
                    string Docfilepath = item.DocFilePath;
                    if (item.DocFilePath != null)
                    {
                        string sourcedirectory = Server.MapPath("~/LOBTemp/");
                        string sourceFile = System.IO.Path.Combine(sourcedirectory, Docfilepath);
                        string destFile = System.IO.Path.Combine(Wdirectory, Docfilepath);
                        string FullFileNamePath = System.IO.Path.Combine(WDirname, Docfilepath);
                        System.IO.File.Copy(sourceFile, destFile, true);
                        //Delete File from temp location
                        System.IO.File.Delete(sourceFile);
                        obj.DocFilePath = FullFileNamePath;
                        obj.DocFileName = item.DocFileName;
                    }
                }
                int res = (int)Helper.ExecuteService("CommitteReplyPendency", "UpdateSubPendency", obj);

            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }



        public PartialViewResult ShowLinkedRecordList(int Linked_RecordId)
        {
            pHouseCommitteeSubFiles model = new pHouseCommitteeSubFiles();
            model.RecordId = Linked_RecordId;
            List<pHouseCommitteeSubFiles> ListModel = new List<pHouseCommitteeSubFiles>();
            ListModel = (List<pHouseCommitteeSubFiles>)Helper.ExecuteService("CommitteReplyPendency", "GetDetailsByRecordId", model);

            if (ListModel != null)
            {
                ListModel.OrderBy(a => a.CreatedDate);
            }
            //return PartialView(ListModel);

            pHouseCommitteeSubFiles mdl1 = new pHouseCommitteeSubFiles();
            mdl1.HouseCommAllSubFilesList = ListModel;

            foreach (var item in ListModel)
            {
                mdl1.ItemName = item.ItemName;
                mdl1.CommitteeName = item.CommitteeName;
                mDepartment mdep = new mDepartment();
                mdep.deptId = item.ToDepartmentID;
                List<mDepartment> deptlist = new List<mDepartment>();
                deptlist = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
                foreach (var dep in deptlist)
                {
                    mdl1.ToDepartmentName = dep.deptname;

                }
            }


            //mdl1.ItemName = ListModel.;
            //mdl1.CommitteeName = CommitteeName;
            return PartialView("ShowLinkedRecordList", mdl1);

        }


        public ActionResult GetDataBySearchParam(int ItemTypeId, int SubItemTypeId, string FinancialYearID, string SelectedDeptID)
        {
            pHouseCommitteeItemPendency model = new pHouseCommitteeItemPendency();
            model.CurrentDeptId = CurrentSession.DeptID;
            model.ItemTypeId = ItemTypeId;
            model.SubItemTypeId = SubItemTypeId;
            model.FinancialYear = FinancialYearID;
            model.SentByDepartmentId = SelectedDeptID;
            List<tCommittee> CommitteeList = new List<tCommittee>();
            List<pHouseCommitteeItemPendency> ListModel = new List<pHouseCommitteeItemPendency>();
            ListModel = (List<pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetDataBySearchParam", model);

            if (ListModel != null)
            {
                ListModel.OrderBy(a => a.CreatedDate);
            }
            pHouseCommitteeItemPendency mdl1 = new pHouseCommitteeItemPendency();
            mdl1.HouseCommitteeItemPendency = ListModel;
            return PartialView("_nPendencyReplyList", mdl1);

        }


        public ActionResult GetDataBySearchParameter(int ItemTypeId, int SubItemTypeId, string FinancialYearID, string SelectedDeptID)
        {
            pHouseCommitteeItemPendency model = new pHouseCommitteeItemPendency();
           
            string aItemTypeId = Convert.ToString(ItemTypeId);
            string aSubItemTypeId = Convert.ToString(SubItemTypeId);
            ViewBag.CurrentDeptId = CurrentSession.DeptID;
            List<tCommittee> CommitteeList = new List<tCommittee>();
            List<pHouseCommitteeItemPendency> ListModel = new List<pHouseCommitteeItemPendency>();
            ListModel = (List<pHouseCommitteeItemPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetDataBySearchParameter", new string[] { aItemTypeId, aSubItemTypeId, FinancialYearID, SelectedDeptID });
            if (ListModel != null)
            {
                ListModel.OrderBy(a => a.CreatedDate);
            }

            pHouseCommitteeItemPendency mdl1 = new pHouseCommitteeItemPendency();
            mdl1.HouseCommitteeItemPendency = ListModel;
            return PartialView("_nPendencyReplyList", mdl1);
        }




        //Searching Methods
        public JsonResult GetAllItemType()
        {
            List<mCommitteeReplyItemType> modellist = new List<mCommitteeReplyItemType>();
            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetFileItemList", null);
            modellist = _ListItems.ToList();
            return Json(modellist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSubItemType()
        {
            List<mCommitteeReplySubItemType> modellist = new List<mCommitteeReplySubItemType>();
            var _SubItemList = (List<mCommitteeReplySubItemType>)Helper.ExecuteService("eFile", "GetSubItemList", null);
            modellist = _SubItemList.ToList();
            return Json(modellist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCurrentDeptList()
        {
            List<mDepartment> modellist = new List<mDepartment>();
            mDepartment mdep = new mDepartment();
            mdep.deptId = CurrentSession.DeptID;
            if (mdep.deptId.Contains(','))
            {
                string[] str1 = new string[4];
                str1 = mdep.deptId.Split(',');
                foreach (var item in str1)
                {
                    mDepartment mdep1 = new mDepartment();
                    mdep1.deptId = item;
                    var list = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep1);
                    modellist.AddRange(list);
                }
            }

            else
            {
                if (CurrentSession.DeptID != "HPD0001")
                {
                    modellist = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
                }
                else 
                {
                    modellist = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
                }                
            }
            return Json(modellist, JsonRequestBehavior.AllowGet);
        }

    }
}
