﻿using Ionic.Zip;
using SBL.DomainModel.Models.CommitteeReport;
using SBL.DomainModel.Models.eFile;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.HouseCommittee.Controllers
{
    public class HouseCommitteePapersMeetingsController : Controller
    {
        //
        // GET: /HouseCommittee/HouseCommitteePapersMeetings/

        public ActionResult Index()
        {
            return View();
        }


        public PartialViewResult GetReceiveSentPaperList(int pageId = 1, int pageSize = 25, string PType = "Recieved", string PTypePending = "Pending")
        {

            eFileLogin();
            eFileAttachments model = new eFileAttachments();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[4];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            str[3] = PTypePending;

            ViewBag.PapersType = PType;
            List<CommitteMeetingPapersModel> ListModel = new List<CommitteMeetingPapersModel>();
            if (!string.IsNullOrEmpty(PType) && PType == "Recieved")
            {

                ListModel = (List<CommitteMeetingPapersModel>)Helper.ExecuteService("eFileCommiteeMeetings", "GetReceivePapersListByDeptIdForMeeting", str);
            }
            else
            {
                ListModel = (List<CommitteMeetingPapersModel>)Helper.ExecuteService("eFileCommiteeMeetings", "GetSentPapersListByDeptIdForMeeting", str);
            }


            model.CommitteMeetingPapersList = ListModel;  
            return PartialView("_receiveSentPaperList", model);
        }


        public JsonResult getDateByYear(string selectedYear)
        {
            try
            {
                string[] str = new string[2];
                str[0] = selectedYear;
                str[1] = CurrentSession.BranchId;  
                 eFileAttachments model = new eFileAttachments();
                 model.CommitteMeetingPapersList = (List<CommitteMeetingPapersModel>)Helper.ExecuteService("eFileCommiteeMeetings", "GetCommiteeMettingAllDataByYear", str);
                 return Json(model, JsonRequestBehavior.AllowGet);
               
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public PartialViewResult GetReceiveSentPaperListForSelection()
        {
            return PartialView("_receiveSentPaperListSelection");
        }

        public PartialViewResult GetMeetingPapersSelection()
        {
            return PartialView("_GetMeetingPapersSelection");
        }

        public PartialViewResult GetSelectedPaperList(string papersIds)
        {
            eFileAttachments model = new eFileAttachments();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];

            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = papersIds.Trim();
            List<eFileAttachment> ListModel = new List<eFileAttachment>();

          

            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFileCommiteeMeetings", "GetReceiveSentPaperDetailsByPaperstIds", str);

            if (ListModel != null)
            {
                model.eFileAttachemts = ListModel;
                model.papersIds = papersIds;

            }           
          
            return PartialView("_GetSelectedPaperList",model);
        }

        public object GetSelectedPaperListForTransfer(string papersIds, string PaperType)
        {
            eFileAttachments model = new eFileAttachments();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = papersIds.Trim();
            str[3] = PaperType;
            str[4] = CurrentSession.AadharId;
            str[5] = CurrentSession.BranchId;

            string Msg = (string)Helper.ExecuteService("eFileCommiteeMeetings", "TransferReceiveSentPaperByPaperstIds", str);

            return Msg;
        }


        //SaveNewDateForMeeting
        public PartialViewResult SaveNewDateForMeeting(string papersIds, string DateSelected)
        {
            eFileAttachments model = new eFileAttachments();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[4];

            str[0] = DateSelected;
            str[1] = papersIds.Trim();
            str[2] = CurrentSession.AadharId;
            str[3] = CurrentSession.BranchId;

         

            //SaveNewDateForMeetingByPaperstIds

            model.CommitteMeetingPapersList = (List<CommitteMeetingPapersModel>)Helper.ExecuteService("eFileCommiteeMeetings", "SaveNewDateForMeetingByPaperstIds", str);

            //Save Papers to Particular Ids
            if (model.CommitteMeetingPapersList.Count > 0)
            {
                string day = DateSelected.Substring(0, 2);
                string month = DateSelected.Substring(3, 2);
                string year = DateSelected.Substring(6, 4);
                if (day.Length == 1)
                    day = "0" + day;
                if (month.Length == 1)
                    month = "0" + month;   


                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                var GetFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                string url = "/ePaper/MeetingPapers";
                string Date = "/" + day + " " + month + " " + year; 
                string directory = FileSettings.SettingValue + url + Date + "/";
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }               

                foreach (var item in model.CommitteMeetingPapersList)
                {
                    if (Directory.Exists(directory))
                    {
                        string SourceUrl = FileSettings.SettingValue + item.PdfPath;
                        string fileName = Path.GetFileName(SourceUrl);

                        var path = System.IO.Path.Combine(directory, fileName);
                        string From = System.IO.Path.Combine(FileSettings.SettingValue + "/ePaper/" + item.PdfPath);

                        string copyPath = From;
                        string pastePath = path;

                      
                        if (FileExists(From))
                        {
                            System.IO.File.Copy(copyPath, pastePath, true);                           
                        }

                    }                    
                }
            }

            return PartialView("_GetSavedSelectedPaperList", model);
        }

        //PhosponedDateForMeeting
        public PartialViewResult PhosponedDateForMeeting(string papersIds, string DateSelected)
        {
            eFileAttachments model = new eFileAttachments();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];

            str[0] = DateSelected;
            str[1] = papersIds.Trim();
            str[2] = CurrentSession.AadharId;

            //SaveNewDateForMeetingByPaperstIds

            model.CommitteMeetingPapersList = (List<CommitteMeetingPapersModel>)Helper.ExecuteService("eFileCommiteeMeetings", "PhosponedDateForMeetingByPaperstIds", str);
                       

            return PartialView("_GetSavedSelectedPaperList", model);
        }

        //GetDateDataForMeeting
        public PartialViewResult GetDateDataForMeeting(string DateSelected)
        {
            eFileAttachments model = new eFileAttachments();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];

            str[0] = DateSelected;           
            str[1] = CurrentSession.AadharId;
            str[2] = CurrentSession.BranchId;

            //SaveNewDateForMeetingByPaperstIds

            model.CommitteMeetingPapersList = (List<CommitteMeetingPapersModel>)Helper.ExecuteService("eFileCommiteeMeetings", "GetDateDataForMeeting", str);           

            return PartialView("_GetSavedSelectedPaperList", model);
        }

        //Delete Paper(S) from Meeting Date
        public PartialViewResult DeleteItemsForMeetingbyDate(string papersIds)
        {
            eFileAttachments model = new eFileAttachments();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[2];            
            str[0] = papersIds.Trim();
            str[1] = CurrentSession.AadharId;        

            model.CommitteMeetingPapersList = (List<CommitteMeetingPapersModel>)Helper.ExecuteService("eFileCommiteeMeetings", "DeleteItemsForMeetingbyDate", str);


            return PartialView("_GetSavedSelectedPaperList", model);
        }

        public PartialViewResult CancelItemsForMeetingbyDate(string papersIds, string DateSelected)
        {
            eFileAttachments model = new eFileAttachments();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];

            str[0] = DateSelected;
            str[1] = papersIds.Trim();
            str[2] = CurrentSession.AadharId;

            if (DateSelected != null && DateSelected != "")
            {
                int date = (Convert.ToInt32(DateSelected.Substring(0, 2)));
                int month = (Convert.ToInt32(DateSelected.Substring(3, 2)));
                int year = (Convert.ToInt32(DateSelected.Substring(6, 4)));
            }

            //SaveNewDateForMeetingByPaperstIds

            model.CommitteMeetingPapersList = (List<CommitteMeetingPapersModel>)Helper.ExecuteService("eFileCommiteeMeetings", "CancelItemsForMeetingbyDate", str);




            return PartialView("_GetSavedSelectedPaperList", model);
        }

        //Download Paper(S) from Meeting Date
        public object DownloadItemsForMeetingbyDate(string papersIds, string DateSelected)
        {
            eFileAttachments model = new eFileAttachments();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];

            str[0] = DateSelected;
            str[1] = papersIds.Trim();
            str[2] = CurrentSession.AadharId;

            string day = DateSelected.Substring(0, 2);
            string month = DateSelected.Substring(3, 2);
            string year = DateSelected.Substring(6, 4);
            if (day.Length == 1)
                day = "0" + day;
            if (month.Length == 1)
                month = "0" + month;

            model.CommitteMeetingPapersList = (List<CommitteMeetingPapersModel>)Helper.ExecuteService("eFileCommiteeMeetings", "GetItemsForMeetingbyDate", str);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url1 = "/ePaper/MeetingPapers";
            string Date = "/" + day + " " + month + " " + year;
            string directory = FileSettings.SettingValue + url1 + Date + "/";

            //create temporary directory for downloading selected files
            string Tempdirectory = FileSettings.SettingValue + url1 + Date + "/" + "Temp" + "/";            
            if (!System.IO.Directory.Exists(Tempdirectory))
            {
                System.IO.Directory.CreateDirectory(Tempdirectory);
            }
            else
            {
                DirectoryInfo dir = new DirectoryInfo(Tempdirectory);
                foreach (FileInfo fi in dir.GetFiles())
                {
                    fi.IsReadOnly = false;
                    fi.Delete();
                }
            }
            

            if (model.CommitteMeetingPapersList.Count > 0)
            {
                foreach (var item in model.CommitteMeetingPapersList)
                {
                    string SourceUrl = FileSettings.SettingValue + item.PdfPath;
                    string fileName = Path.GetFileName(SourceUrl);

                    var path = System.IO.Path.Combine(Tempdirectory, fileName);
                    string From = System.IO.Path.Combine(FileSettings.SettingValue + url1 + Date + "/" + item.PdfPath);

                    string copyPath = From;
                    string pastePath = path;

                    if (System.IO.File.Exists(copyPath)==true)
                    {
                        System.IO.File.Copy(copyPath, pastePath, true);
                    }

                    
                }


            }

            

            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                //zip.AddDirectoryByName("Files");

                if (Directory.Exists(Tempdirectory))
                {
                    zip.AddDirectory(Tempdirectory, "CommitteeMeeting");

                    //zip.Save(Tempdirectory + "CommitteeMeeting.zip");
                    //// Read bytes from disk
                    //byte[] fileBytes = System.IO.File.ReadAllBytes(Tempdirectory + "CommitteeMeeting.zip");
                    //string fileName = "CommitteeMeeting.zip";
                    //// Return bytes as stream for download
                    //return File(fileBytes, "application/zip", fileName);
                }
                else
                {

                }



                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("{0}.zip", "CommitteeMeeting_" + Date + ".zip");
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);
                Response.End();

               

                return Response;
            }
            
            
        }

        //public FileResult download()
        //{
        //    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
        //    string url1 = "/ePaper/MeetingPapers";
        //    string Date = "/" + day + " " + month + " " + year;
        //    string directory = FileSettings.SettingValue + url1 + Date + "/";

        //    //create temporary directory for downloading selected files
        //    string Tempdirectory = FileSettings.SettingValue + url1 + Date + "/" + "Temp" + "/";
        //    return File("~/" + path, "application/pdf", string.Format(fileName));
        //}
        //SaveNewDateForMeeting
        public PartialViewResult SaveManualsValuesForMeetings(string DateSelected, string Subject)
        {
            eFileAttachments model = new eFileAttachments();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string UploadedfilePathName = "";            

            string url = "~/ePaper/TempFile" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            string[] filePaths = Directory.GetFiles(directory);
            foreach (var item in filePaths)
            {
                UploadedfilePathName = item;
                
            }
            string fileName = Path.GetFileName(UploadedfilePathName);
            fileName = CurrentSession.UserID + "_" + fileName;

            string[] str = new string[5];

            str[0] = DateSelected;
            str[1] = Subject;
            str[2] = CurrentSession.AadharId;
            str[3] = fileName;
            str[4] = CurrentSession.BranchId;

            //SaveNewDateForMeetingByPaperstIds

            model.CommitteMeetingPapersList = (List<CommitteMeetingPapersModel>)Helper.ExecuteService("eFileCommiteeMeetings", "SaveManualsValuesForMeetingsByDate", str);

            //Save Papers to Particular Ids
            if (model.CommitteMeetingPapersList.Count > 0)
            {
                string day = DateSelected.Substring(0, 2);
                string month = DateSelected.Substring(3, 2);
                string year = DateSelected.Substring(6, 4);
                if (day.Length == 1)
                    day = "0" + day;
                if (month.Length == 1)
                    month = "0" + month;


                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                var GetFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                string url1 = "/ePaper/MeetingPapers";
                string Date = "/" + day + " " + month + " " + year;
                string directory1 = FileSettings.SettingValue + url1 + Date + "/";
                if (!System.IO.Directory.Exists(directory1))
                {
                    System.IO.Directory.CreateDirectory(directory1);
                }

                if (Directory.Exists(directory1))
                {
                    string SourceUrl = fileName;
                    string fileName1 = Path.GetFileName(SourceUrl);

                    var path = System.IO.Path.Combine(directory1, fileName);                    

                    string copyPath = UploadedfilePathName;
                    string pastePath = path;


                    if (FileExists(copyPath))
                    {
                        System.IO.File.Copy(copyPath, pastePath, true);
                        System.IO.File.Delete(copyPath);
                    }

                }



                
            }

            return PartialView("_GetSavedSelectedPaperList", model);
        }

        [HttpPost]
        public ContentResult UploadMainPdfFiles()
        {
            string url = "~/ePaper/TempFile" + "/" + CurrentSession.UserID;            
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<CommitteMeetingPapersModel>();
            
            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                // string Fname=  hpf.FileName;
                string ext = Path.GetExtension(hpf.FileName);
                if (ext == ".pdf")
                {
                    if (hpf.ContentLength == 0)
                        continue;
                    string url1 = "~/ePaper/TempFile" + "/" + CurrentSession.UserID;
                    string directory1 = Server.MapPath(url1);
                    if (!Directory.Exists(directory1))
                    {
                        Directory.CreateDirectory(directory1);
                    }
                    string savedFileName = Path.Combine(Server.MapPath("~/ePaper/TempFile" + "/" + CurrentSession.UserID), Path.GetFileName(hpf.FileName));
                    hpf.SaveAs(savedFileName);

                    r.Add(new CommitteMeetingPapersModel()
                    {
                        Name = hpf.FileName,
                        Length = hpf.ContentLength,
                        Type = hpf.ContentType


                    });
                    return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
                }
                else
                {
                    r.Add(new CommitteMeetingPapersModel()
                    {
                        // Name = "only .pdf Allowed",
                        Length = hpf.ContentLength,
                        Type = hpf.ContentType


                    });
                    // string Name = "only .pdf Allowed";
                    return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
                }
            }


            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult RemoveMainFiles()
        {
            string url = "~/ePaper/TempFile" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public ActionResult eFileLogin()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            return null;
        }

        public bool FileExists(string path)
        {
            string curFile = @"" + path; ;
            bool valu = System.IO.File.Exists(curFile) ? true : false;
            return valu;
        }

    }
}
