﻿using SBL.DomainModel.Models.Member;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SalaryClerks.Extensions;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.SalaryClerks.Controllers
{ 
    public class MemberAccountDetailsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }//Index not implemented

        #region All members Account details - Dashboard
        public ActionResult AllMembersAccountDetails()
        {
            try
            {
                var Content = (List<mMemberAccountsDetails>)Helper.ExecuteService("MemberAccountDetails", "GetAllMembersDetails", null);
                var model = Content.ToViewModel();
                ViewBag.SaveData = TempData["Message"];//to show successful message in case of saving 
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Getting all Members Account Details";
                return View("HrErrorPage");
            }
        }
        #endregion

        #region Current members Account details - Dashboard
        public ActionResult CurrentMembersAccountDetails()
        {
            try
            {
                //if (string.IsNullOrEmpty(CurrentSession.UserID))
                //{
                //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                //}
                var Content = (List<mMemberAccountsDetails>)Helper.ExecuteService("MemberAccountDetails", "GetCurrentMembersDetails", null);
                var model = Content.ToViewModel();
                ViewBag.SaveData = TempData["Message"];//to show successful message in case of saving 
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Getting all Members Account Details";
                return View("HrErrorPage");
            }
        }
        #endregion

        #region Ex Members Account details - Dashboard
        public ActionResult ExMembersAccountDetails()
        {
            try
            {
                //if (string.IsNullOrEmpty(CurrentSession.UserID))
                //{
                //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                //}
                var Content = (List<mMemberAccountsDetails>)Helper.ExecuteService("MemberAccountDetails", "GetExMembersDetails", null);
                var model = Content.ToViewModel();
                ViewBag.SaveData = TempData["Message"];//to show successful message in case of saving 
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Getting all Members Account Details";
                return View("HrErrorPage");
            }
        }
        #endregion

        #region Edit Member Account Details
        public ActionResult EditAccountDetails(int id)
        {
            try
            {
                //if (string.IsNullOrEmpty(CurrentSession.UserID))
                //{
                //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                //}
                var Content = (mMemberAccountsDetails)Helper.ExecuteService("MemberAccountDetails", "GetMemberAccountDetailsById", new mMemberAccountsDetails { MemberCode = id });

                var model = Content.EditModel();
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Getting all Members Account Details";
                return View("HrErrorPage");
            }
        }
        #endregion

        #region Save Account Details
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveContent(mMemberAccountsDetails model)
        {
            try
            {
                //if (string.IsNullOrEmpty(CurrentSession.UserID))
                //{
                //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                //}
                if (ModelState.IsValid)
                {
                    var Content = (mMemberAccountsDetails)Helper.ExecuteService("MemberAccountDetails", "SaveMembersAccountDetails", model);
                    TempData["Message"] = "Data saved successfully";

                    return RedirectToAction("CurrentMembersAccountDetails", "MemberAccountDetails", new { @area = "SalaryClerks" });
                }
                else
                {
                    var memberData = (mMemberAccountsDetails)Helper.ExecuteService("MemberAccountDetails", "GetMemberAccountDetailsById", new mMemberAccountsDetails { MemberCode = model.MemberCode });
                    var passObj = memberData.EditModel();
                    return View("EditAccountDetails", passObj);
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Content Details";
                return View("AdminErrorPage");
            }
        }
        #endregion

        #region Nominee page
        public ActionResult SelectNominee(int id)
        {
            try
            {
                var Content = (List<mMemberNominee>)Helper.ExecuteService("MemberAccountDetails", "NomineeDetails", id);
                return PartialView("_MembersNomineeList");
            }
            catch (Exception)
            {
                
                throw;
            }           
        }
        #endregion

        #region Create Nominee
        public ActionResult NewNominee()
        {
            return PartialView("_MemberNewNominee");
        }
        #endregion

    }
}
