﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SalaryClerks
{
    public class SalaryClerksRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SalaryClerks";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SalaryClerks_default",
                "SalaryClerks/{controller}/{action}/{id}",
                new { action = "AllMembersAccountDetails", Controller="MemberAccountDetails", id = UrlParameter.Optional }
            );
        }
    }
}
