﻿using SBL.DomainModel.Models.Member;
using SBL.eLegistrator.HouseController.Web.Areas.SalaryClerks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SalaryClerks.Extensions
{
    public static class ModelMapping
    {
        //mapping of all former members
        public static IEnumerable<MemberAccountDetailsViewModel> ToViewModel(this IEnumerable<mMemberAccountsDetails> content)
        {
            var ViewModel = content.Select(contentItem => new MemberAccountDetailsViewModel()
            {
                MemberCode = contentItem.MemberCode,
                MemberName = contentItem.MemberName,
                PhotoCode = contentItem.PhotoCode,
                BankName = contentItem.BankName,
                AccountNo = contentItem.AccountNo,
                NomineeName = contentItem.NomineeName,
                ConstituencyName = contentItem.ConstituencyName
            });
            return ViewModel;
        }

        //mapping of single member
        public static MemberAccountDetailsViewModel EditModel(this mMemberAccountsDetails content)
        {
            var viewmodel = new MemberAccountDetailsViewModel()
            {                
                MemberCode = content.MemberCode,
                MemberName = content.MemberName,
                MembersDetails=content.MemberDetail,
                EPABXNo = content.EPABXNo,
                HBAAccountNo = content.HBAAccountNo,
                MCAAccountNo = content.MCAAccountNo,
                BankName = content.BankName,
                AccountNo = content.AccountNo,
                IFSCCode = content.IFSCCode,
                NomineeActive = content.NomineeActive,
                NomineeRelationShip = content.NomineeRelationShip,
                NomineeIFSCCode = content.NomineeIFSCCode,
                NomineeName = content.NomineeName,
                NomineeAccountNo = content.NomineeAccountNo,
                NomineeBankName = content.NomineeBankName,
                PhotoCode = content.PhotoCode,
                MemberAddress = content.MemberAddress,
                MobileNumber = content.MobileNumber,
                ConstituencyName = content.ConstituencyName
            };
            return viewmodel;
        }
    }
}