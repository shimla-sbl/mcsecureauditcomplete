﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SalaryClerks.Models
{
    public class MemberAccountDetailsViewModel
    {
        public int MemberCode { get; set; }

        [Display(Name = "Member Name")]
        public string MemberName { get; set; }

        public string MemberAddress { get; set; }

        public string MobileNumber { get; set; }

        public int? PhotoCode { get; set; }

        public int? RoomNo { get; set; }

        public string ConstituencyName { get; set; }

        public string MembersDetails { get; set; }

        [Display(Name = "EPABX No")]
        //[Required(ErrorMessage = "Please Enter EPABX number")]
        public long? EPABXNo { get; set; }

        [Display(Name = "HBA AccountNo")]
        //[Required(ErrorMessage = "Please Enter HBA AccountNo ")]
        public long? HBAAccountNo { get; set; }

        [Display(Name = "MCA AccountNo")]
        //[Required(ErrorMessage = "Please Enter MCA AccountNo ")]
        public long? MCAAccountNo { get; set; }

        [Display(Name = "Bank Name")]
        //[Required(ErrorMessage = "Please Enter Bank Name")]
        public string BankName { get; set; }

        [Display(Name = "Account Number")]
        //[Required(ErrorMessage = "Please Enter Bank Account Number")]
        public long? AccountNo { get; set; }

        [Display(Name = "IFSC Code")]
        //[Required(ErrorMessage = "Please Enter Bank IFSC Code")]
        public string IFSCCode { get; set; }

        public bool NomineeActive { get; set; }

        [Display(Name = "Nominee Name")]
        //[Required(ErrorMessage = "Please Enter Nominee Name")]
        public string NomineeName { get; set; }

        [Display(Name = "Nominee's RelationShip")]
        //[Required(ErrorMessage = "Please Select Relationship with Nominee")]
        public string NomineeRelationShip { get; set; }

        [Display(Name = "Nominee Bank Name")]
        //[Required(ErrorMessage = "Please Enter Nominee Bank Name")]
        public string NomineeBankName { get; set; }

        [Display(Name = "Nominee's Bank Account Number")]
        //[Required(ErrorMessage = "Please Enter Nominee Account No")]
        public long? NomineeAccountNo { get; set; }

        [Display(Name = "Nominee's Bank IFSC Code")]
       // [Required(ErrorMessage = "Please Enter Nominee's Bank IFSC Code")]
        public string NomineeIFSCCode { get; set; }
    }
}