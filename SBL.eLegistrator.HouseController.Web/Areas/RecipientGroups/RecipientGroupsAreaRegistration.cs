﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups
{
    public class RecipientGroupsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "RecipientGroups";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "RecipientGroups_default",
                "RecipientGroups/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
