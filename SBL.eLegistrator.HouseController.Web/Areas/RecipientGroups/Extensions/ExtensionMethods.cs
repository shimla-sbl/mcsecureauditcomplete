﻿using SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SBL.DomainModel.Models.RecipientGroups;
using System.Web.Mvc;
using System.Data;
using System.Reflection;
using SBL.eLegistrator.HouseController.Web.Utility;


namespace SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Extensions
{
    public static class ExtensionMethods
    {
        public static SelectList GetGenderList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Male", Value = "Male" });
            result.Add(new SelectListItem() { Text = "Female", Value = "Female" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetRemainderTypes()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Periodic", Value = "1" });
            result.Add(new SelectListItem() { Text = "Specific", Value = "2" });

            return new SelectList(result, "Value", "Text");
        }

        #region Contact Groups
        public static RecipientGroup ToEntity(this RecipientGroupModel Model)
        {
            RecipientGroup entity = new RecipientGroup()
            {
                GroupID = Model.GroupID,
                Name = Model.Name,
                Description = Model.Description,
                IsPublic = Model.IsPublic,
                CreatedBy = Model.CreatedBy,
                CreatedDate = Model.CreatedDate,               
                IsActive = Model.IsActive,
                DepartmentCode = Model.DepartmentCode,
                PancayatCode = Model.PancayatCode,
                ConstituencyCode = Model.ConstituencyCode,
                GroupTypeID = Model.GroupTypeID,
				IsGeneric=Model.IsGenericGroup,
				RankingOrder=Model.RankingOrder
             
            };
            return entity;
        }

        public static RecipientGroupModel ToModel(this RecipientGroup entity)
        {
            var Model = new RecipientGroupModel()
            {
                GroupID = entity.GroupID,
                Name = entity.Name,
                Description = entity.Description,
                IsPublic = entity.IsPublic,
                CreatedBy = entity.CreatedBy,
                CreatedDate = entity.CreatedDate,
                IsActive = entity.IsActive,
                DepartmentCode = entity.DepartmentCode,
                PancayatCode = entity.PancayatCode,
                ConstituencyCode = entity.ConstituencyCode,
                GroupTypeID = entity.GroupTypeID,
				RankingOrder=entity.RankingOrder,

            };
            return Model;
        }

        public static List<RecipientGroupModel> ToModelList(this List<RecipientGroup> entityList)
        {
            var ModelList = entityList.Select(entity => new RecipientGroupModel()
            {
                GroupID = entity.GroupID,
                Name = entity.Name,
                Description = entity.Description,
                IsPublic = entity.IsPublic,
                CreatedBy = entity.CreatedBy,
                CreatedDate = entity.CreatedDate,
                IsActive = entity.IsActive,
                DepartmentCode = entity.DepartmentCode,
                PancayatCode = entity.PancayatCode,
                ConstituencyCode = entity.ConstituencyCode,
                GroupTypeID = entity.GroupTypeID,
				RankingOrder=entity.RankingOrder,
                
            });
            return ModelList.ToList();
        }
        #endregion

        #region Contact Group Member

        public static RecipientGroupMember ToEntity(this RecipientGroupMemberModel Model)
        {
            RecipientGroupMember entity = new RecipientGroupMember()
            {
                GroupMemberID = Model.GroupMemberID,
                GroupID = Model.GroupID,
                Name = Model.Name,
                Gender = Model.Gender,
                Designation = Model.Designation,
                Email = Model.Email,
                MobileNo = Model.MobileNo,
                Address = Model.Address,
               
                PinCode = Model.PinCode,
                IsActive = Model.IsActive,
                CreatedBy = Model.CreatedBy,
                CreatedDate = Model.CreatedDate,
                DepartmentCode = Model.DepartmentCode,
				RankingOrder=Model.RankingOrder,
				LandLineNumber=Model.LandLineNumber,
                LandLineNumberVS = Model.LandLineNumberVS,
                AadharId = Model.AadharId,
                WardName =Model.WardName ,
            };
            return entity;
        }

        public static List<RecipientGroupMember> ToEntityList(this List<RecipientGroupMemberModel> ModelList, Int32 groupID)
        {
            var entityList = ModelList.Select(Model => new RecipientGroupMember()
            {
                GroupMemberID = Model.GroupMemberID,
                GroupID = groupID,
                Name = Model.Name,
                Gender = Model.Gender,
                Designation = Model.Designation,
                Email = Model.Email,
                MobileNo = Model.MobileNo,
                Address = Model.Address,
                PinCode = Model.PinCode,
                IsActive = Model.IsActive,
                CreatedBy = CurrentSession.UserName,
                CreatedDate = DateTime.Now,
                DepartmentCode = Model.DepartmentCode,
                LandLineNumberVS= Model.LandLineNumberVS,
                WardName =Model.WardName ,
            });
            return entityList.ToList();
        }

        public static RecipientGroupMemberModel ToModel(this RecipientGroupMember entity)
        {
            var Model = new RecipientGroupMemberModel()
            {
                GroupMemberID = entity.GroupMemberID,
                GroupID = entity.GroupID,
                Name = entity.Name,
                Gender = entity.Gender,
                Designation = entity.Designation,
                Email = entity.Email,
                MobileNo = entity.MobileNo,
                Address = entity.Address,
               
                PinCode = entity.PinCode,
                IsActive = entity.IsActive,
				DepartmentCode=entity.DepartmentCode,
				RankingOrder=entity.RankingOrder,
				LandLineNumber=entity.LandLineNumber,
                LandLineNumberVS = entity.LandLineNumberVS,
                AadharId =entity.AadharId,
                WardName=entity.WardName ,
            };
            return Model;
        }

        public static List<RecipientGroupMemberModel> ToModelList(this List<RecipientGroupMember> entityList)
        {
            var ModelList = entityList.Select(entity => new RecipientGroupMemberModel()
            {
                GroupMemberID = entity.GroupMemberID,
                GroupID = entity.GroupID,
                Name = entity.Name,
                Gender = entity.Gender,
                Designation = entity.Designation,
                Email = entity.Email,
                MobileNo = entity.MobileNo,
                Address = entity.Address,
                PinCode = entity.PinCode,
                IsActive = entity.IsActive,
                DepartmentCode = entity.DepartmentCode,
                LandLineNumberVS = entity.LandLineNumberVS,
                WardName =entity .WardName ,
            });
            return ModelList.ToList();
        }

        #endregion

        public static List<T> DataTableToList<T>(this DataTable datatable) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in datatable.AsEnumerable())
                {
                    T obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }

        }
    }       
}