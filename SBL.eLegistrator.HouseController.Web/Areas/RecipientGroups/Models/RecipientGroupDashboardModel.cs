﻿using SBL.DomainModel.Models.RecipientGroups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models
{
    public class RecipientGroupDashboardModel
    {
        public RecipientGroupDashboardModel()
        {
            this.RecipientGroupModelList = new List<RecipientGroupModel>();
        }
        public List<RecipientGroupModel> RecipientGroupModelList { get; set; }

        public Int32 GroupID { get; set; }
        public string GroupName { get; set; }

        public bool IsGenericGroup { get; set; }
        public bool IsPanchayatGroup { get; set; }
        public bool IsConstituencyGroup { get; set; }
    }

    public class RecipientGroupMemDashboardModel
    {
        public RecipientGroupMemDashboardModel()
        {
            this.RecipientGroupMemModelList = new List<RecipientGroupMemberModel>();
            this.RecipientGroupMemberList = new List<RecipientGroupMember>();
        }

        public int GroupID { get; set; }

        public List<RecipientGroupMemberModel> RecipientGroupMemModelList { get; set; }
        public List<RecipientGroupMember> RecipientGroupMemberList { get; set; }
        
        //public SelectList GroupList { get; set; }

        public List<RecipientGroup> GroupList { get; set; }
        public List<RecipientGroup> GroupListAll { get; set; }
    }
}