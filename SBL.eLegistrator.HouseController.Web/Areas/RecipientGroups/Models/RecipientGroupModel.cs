﻿using SBL.DomainModel.Models.Department;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SBL.DomainModel.Models.Constituency;

namespace SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models
{
    public class RecipientGroupModel
    {
        public int GroupID { get; set; }

        [Required(ErrorMessage = "Please Enter Group Name")]
        public string Name { get; set; }

        public string Description { get; set; }
        public bool IsPublic { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }

        public string DepartmentCode { get; set; }
        public int? PancayatCode { get; set; }
        public int? ConstituencyCode { get; set; }

        public string Mode { get; set; }
        public bool SaveCreateNew { get; set; }

        //Hide and show the appropriate Controls on the View with following properties.
        public bool IsGenericGroup { get; set; }
        public bool IsPanchayatGroup { get; set; }
        public bool IsConstituencyGroup { get; set; }
        public int GroupTypeID { get; set; }

        public List<mDepartment> mDepartmentList { get; set; }
        public List<mPanchayat> mPanchayatList { get; set; }
        public List<mConstituency> mConstituencyList { get; set; }

		public int? RankingOrder { get; set; }
    }
}