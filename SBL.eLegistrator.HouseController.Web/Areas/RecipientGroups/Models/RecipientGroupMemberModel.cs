﻿using SBL.DomainModel.Models.Department;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models
{
    public class RecipientGroupMemberModel
    {
        public int GroupMemberID { get; set; }

        public int GroupID { get; set; }

        [Required(ErrorMessage = "Please Enter Name")]
		//[StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
		//[RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$", ErrorMessage =
		//	"Numbers and special characters are not allowed")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Gender is required")]
        public string Gender { get; set; }

        public string Designation { get; set; }

		//[EmailAddress(ErrorMessage = "Invalid Email Address")]
		//[Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        [StringLength(10,MinimumLength=10, ErrorMessage = "Maximum 10 characters allowed")]
        [RegularExpression(@"^[0-9\.\+\-\/]+$", ErrorMessage = "Invalid phone number")]
        [Required(ErrorMessage = "Mobile Number is required")]
        public string MobileNo { get; set; }

        public string Address { get; set; }

        //public string AddressVS { get; set; }

        //public string RoomNoVS { get; set; }

        public string DepartmentCode { get; set; }

        [StringLength(15, ErrorMessage = "Maximum 10 characters allowed")]
        [RegularExpression(@"^[0-9\.\+\-\/]+$", ErrorMessage = "Invalid Pin Code")]        
        public string PinCode { get; set; }

        public string WardName { get; set; }
        

        public bool IsActive { get; set; }

        public string Mode { get; set; }
        public bool SaveCreateNew { get; set; }

        public SelectList GenderList { get; set; }
        public string GroupName { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public List<mDepartment> mDepartmentList { get; set; }
		public int? RankingOrder { get; set; }
		public string LandLineNumber { get; set; }
        public string LandLineNumberVS { get; set; }
        public string AadharId { get; set; }
    }
}