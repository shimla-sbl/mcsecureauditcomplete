﻿

namespace SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models
{
    #region Namespace reffrences
    using SBL.DomainModel.Models.RecipientGroups;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    #endregion

    public class SendNotificationsModel
    {
        #region Constrctor Block
        public SendNotificationsModel()
        {
            this.availableRecipientGroups = new List<RecipientGroup>();
            this.NotificationAlreadySentTo = new List<RecipientGroupMember>();
        }
        #endregion

        public string AssociatedContactGroups { get; set; }

        [StringLength(160, ErrorMessage = "Maximum 160 characters allowed")]
        public string SMSTemplate { get; set; }

        public string EmailTemplate { get; set; }

        public bool SendSMS { get; set; }

        public bool SendEmail { get; set; }

        public List<RecipientGroup> availableRecipientGroups { get; set; }

        public List<RecipientGroup> selectedRecipientGroups { get; set; }

        public PostedRecipientGroups PostedRecipientGroups { get; set; }

        //IEnumerable<SBL.DomainModel.Models.RecipientGroups.RecipientGroupMember>
        public List<RecipientGroupMember> NotificationAlreadySentTo { get; set; }

        public string PhoneNumbers { get; set; }

        public string EmailAddresses { get; set; }

        public int RemainderTypeID { get; set; }

        public SelectList RemainderTypesList { get; set; }

        public string PeriodicTillDate { get; set; }
    }

    public class PostedRecipientGroups
    {
        public string[] RecipientGroupIDs { get; set; }
    } 
}