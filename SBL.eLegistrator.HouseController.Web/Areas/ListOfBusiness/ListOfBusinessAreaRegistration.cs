﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness
{
    public class ListOfBusinessAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ListOfBusiness";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ListOfBusiness_default",
                "ListOfBusiness/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
