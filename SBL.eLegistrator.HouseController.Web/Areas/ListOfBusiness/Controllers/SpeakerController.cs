﻿using EvoPdf;
using SBL.DomainModel.Models.LOB;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    public class SpeakerController : Controller
    {
        //
        // GET: /ListOfBusiness/Speaker/

        public ActionResult Index()
        {
            return View();
        }


        #region SpeakerPad

        /// <summary>
        /// Will render the SpeakerPad View Will Three Tabs Include ,Pending,Submitted
        /// Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult SpeakerPad()
        {

            return View();
        }

        #endregion


        #region PartialInclude

        /// <summary>
        /// Will render the PartialInclude PartialView
        /// Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialInclude(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();
            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSpeakerPendingLOBForInclude]", methodParameter);

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        //    objLOB.SubmittedDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }
                        listLOBForDay.Add(objLOB);
                    }

                    objPendingLOB.ListLOBBYId = listLOBForDay;
                }

                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("PartialInclude", objPendingLOB);

        }

        #endregion


        #region IncludeLineGrid

        public ActionResult IncludeLineGrid(string LOBId)
        {
            AddLOBModel addLOBModel = new AddLOBModel();
            try
            {
                if (LOBId != null && LOBId != "")
                {
                    List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                    //methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                    //methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                    DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectApporvedLOBByIdForSpeakerLOB", methodParameter);
                    List<AddLOBModel> ListLOB = new List<AddLOBModel>();
                    string IncludeALL = "";
                    for (int i = 0; i < dataSetLOB.Tables[0].Rows.Count; i++)
                    {
                        AddLOBModel addLOBModel1 = new AddLOBModel();
                        string SrNo1 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo1"]);
                        if (SrNo1 != "")
                        {
                            addLOBModel1.SrNo11 = SrNo1;
                            SrNo1 = SrNo1 + ".";
                        }
                        string SrNo2 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo2"]);
                        if (SrNo2 != "")
                        {
                            addLOBModel1.SrNo22 = SrNo2;
                            SrNo2 = "(" + SrNo2 + ")";
                        }
                        string SrNo3 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo3"]);
                        if (SrNo3 != "")
                        {
                            addLOBModel1.SrNo33 = SrNo3;
                            SrNo3 = "(" + ToRoman(Convert.ToInt16(SrNo3)) + ")";
                        }
                        addLOBModel1.SrNo1 = SrNo1;
                        addLOBModel1.SrNo2 = SrNo2;
                        addLOBModel1.SrNo3 = SrNo3;
                        addLOBModel1.Sessiondate = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SessionDate"]);
                        addLOBModel1.SessionName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SessionName"]);
                        addLOBModel1.AssemblyName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["AssemblyName"]);
                        addLOBModel1.RecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                        addLOBModel1.LOBId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["LOBId"]);
                        addLOBModel1.TextLOB = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TextLOB"]);
                        addLOBModel1.TextSpeaker = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TextSpeaker"]);
                        if (addLOBModel1.TextSpeaker == "" || addLOBModel1.TextSpeaker == null)
                        {
                            IncludeALL = "false";
                        }
                        //addLOBModel1.ConcernedMemberName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["ConcernedMemberName"]);
                        //addLOBModel1.ConcernedDeptName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["ConcernedDeptName"]);
                        //addLOBModel1.ConcernedCommitteeName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["ConcernedCommitteeName"]);
                        //if (i == 0)
                        //{
                        //    addLOBModel.ResultCount = Convert.ToInt32(Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TotalRecords"]));
                        //}
                        ListLOB.Add(addLOBModel1);
                    }
                    addLOBModel.LOBList = ListLOB;

                    //if (PageNumber != null && PageNumber != "")
                    //{
                    //    addLOBModel.PageNumber = Convert.ToInt32(PageNumber);
                    //}
                    //else
                    //{
                    //    addLOBModel.PageNumber = Convert.ToInt32("1");
                    //}

                    //if (RowsPerPage != null && RowsPerPage != "")
                    //{
                    //    addLOBModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                    //}
                    //else
                    //{
                    //    addLOBModel.RowsPerPage = Convert.ToInt32("10");
                    //}
                    //if (PageNumber != null && PageNumber != "")
                    //{
                    //    addLOBModel.selectedPage = Convert.ToInt32(PageNumber);
                    //}
                    //else
                    //{
                    //    addLOBModel.selectedPage = Convert.ToInt32("1");
                    //}

                    //if (loopStart != null && loopStart != "")
                    //{
                    //    addLOBModel.loopStart = Convert.ToInt32(loopStart);
                    //}
                    //else
                    //{
                    //    addLOBModel.loopStart = Convert.ToInt32("1");
                    //}

                    //if (loopEnd != null && loopEnd != "")
                    //{
                    //    addLOBModel.loopEnd = Convert.ToInt32(loopEnd);
                    //}
                    //else
                    //{
                    //    addLOBModel.loopEnd = Convert.ToInt32("5");
                    //}
                    if (IncludeALL == "false")
                    {
                        addLOBModel.PageBreak = false;
                    }
                    else
                    {
                        addLOBModel.PageBreak = true;
                    }


                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("IncludeLineGrid", addLOBModel);
        }

        #endregion

        #region AddIncludeLine

        public JsonResult AddIncludeLine(string RecordId)
        {
            if (RecordId != null && RecordId != "")
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@RecordId", RecordId));
                DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_IncludeLOBlineForSpeakerPad", methodParameter);
            }
            var res2 = Json("teste", JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion


        #region RemoveIncludeLine

        public JsonResult RemoveIncludeLine(string RecordId)
        {
            if (RecordId != null && RecordId != "")
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@RecordId", RecordId));
                DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_RemoveLOBlineForSpeakerPad", methodParameter);
            }
            var res2 = Json("teste", JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion


        #region PartialPending

        /// <summary>
        /// Will render the PartialCreateLOB PartialView
        /// Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialPending(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();
            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSpeakerPendingLOB]", methodParameter);

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        //    objLOB.SubmittedDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }
                        listLOBForDay.Add(objLOB);
                    }

                    objPendingLOB.ListLOBBYId = listLOBForDay;
                }

                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            return PartialView("PartialPending", objPendingLOB);
        }

        #endregion


        #region UpdateLinesGrid

        public ActionResult UpdateLinesGrid(string LOBId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            AddLOBModel addLOBModel = new AddLOBModel();
            try
            {
                if (LOBId != null && LOBId != "")
                {
                    List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                    methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                    methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                    DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectApporvedLOBByIdForSpeakerLOBUpdate", methodParameter);
                    List<AddLOBModel> ListLOB = new List<AddLOBModel>();
                    for (int i = 0; i < dataSetLOB.Tables[0].Rows.Count; i++)
                    {
                        AddLOBModel addLOBModel1 = new AddLOBModel();
                        string SrNo1 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo1"]);
                        if (SrNo1 != "")
                        {
                            addLOBModel1.SrNo11 = SrNo1;
                            SrNo1 = SrNo1 + ".";
                        }
                        string SrNo2 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo2"]);
                        if (SrNo2 != "")
                        {
                            addLOBModel1.SrNo22 = SrNo2;
                            SrNo2 = "(" + SrNo2 + ")";
                        }
                        string SrNo3 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo3"]);
                        if (SrNo3 != "")
                        {
                            addLOBModel1.SrNo33 = SrNo3;
                            SrNo3 = "(" + ToRoman(Convert.ToInt16(SrNo3)) + ")";
                        }
                        addLOBModel1.SrNo1 = SrNo1;
                        addLOBModel1.SrNo2 = SrNo2;
                        addLOBModel1.SrNo3 = SrNo3;
                        addLOBModel1.LOBId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["LOBId"]);
                        addLOBModel1.Sessiondate = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SessionDate"]);
                        addLOBModel1.SessionName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SessionName"]);
                        addLOBModel1.AssemblyName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["AssemblyName"]);
                        if (Convert.ToString(dataSetLOB.Tables[0].Rows[i]["IsSpeakerPageBreak"]) != "")
                        {
                            addLOBModel1.IsSpeakerPageBreak = Convert.ToBoolean(dataSetLOB.Tables[0].Rows[i]["IsSpeakerPageBreak"]);
                        }
                        else
                        {
                            addLOBModel1.IsSpeakerPageBreak = false;
                        }

                        addLOBModel1.RecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                        addLOBModel1.TextLOB = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TextLOB"]);
                        addLOBModel1.TextSpeaker = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TextSpeaker"]);
                        //addLOBModel1.ConcernedMemberName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["ConcernedMemberName"]);
                        //addLOBModel1.ConcernedDeptName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["ConcernedDeptName"]);
                        //addLOBModel1.ConcernedCommitteeName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["ConcernedCommitteeName"]);
                        if (i == 0)
                        {
                            addLOBModel.ResultCount = Convert.ToInt32(Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TotalRecords"]));
                        }
                        ListLOB.Add(addLOBModel1);
                    }
                    addLOBModel.LOBList = ListLOB;

                    if (PageNumber != null && PageNumber != "")
                    {
                        addLOBModel.PageNumber = Convert.ToInt32(PageNumber);
                    }
                    else
                    {
                        addLOBModel.PageNumber = Convert.ToInt32("1");
                    }

                    if (RowsPerPage != null && RowsPerPage != "")
                    {
                        addLOBModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                    }
                    else
                    {
                        addLOBModel.RowsPerPage = Convert.ToInt32("10");
                    }
                    if (PageNumber != null && PageNumber != "")
                    {
                        addLOBModel.selectedPage = Convert.ToInt32(PageNumber);
                    }
                    else
                    {
                        addLOBModel.selectedPage = Convert.ToInt32("1");
                    }

                    if (loopStart != null && loopStart != "")
                    {
                        addLOBModel.loopStart = Convert.ToInt32(loopStart);
                    }
                    else
                    {
                        addLOBModel.loopStart = Convert.ToInt32("1");
                    }

                    if (loopEnd != null && loopEnd != "")
                    {
                        addLOBModel.loopEnd = Convert.ToInt32(loopEnd);
                    }
                    else
                    {
                        addLOBModel.loopEnd = Convert.ToInt32("5");
                    }



                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("UpdateLinesGrid", addLOBModel);
        }

        #endregion

        #region PartialUpdateSpeakerLine
         [ValidateAntiForgeryToken]
        public ActionResult PartialUpdateSpeakerLine(string RecordId)
        {
            AddLOBModel addLOBModel = new AddLOBModel();

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@Id", RecordId));

            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLineRecordByRecordIdFromAdmin", methodParameter);
            if (dataSetLOB != null)
            {
                addLOBModel.RecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["Id"]);
                addLOBModel.LOBId = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["LOBId"]);
                addLOBModel.TextSpeaker = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["TextSpeaker"]);
                if (dataSetLOB.Tables[0].Rows[0]["IsSpeakerPageBreak"] != null && Convert.ToString(dataSetLOB.Tables[0].Rows[0]["IsSpeakerPageBreak"]) != "")
                {
                    addLOBModel.IsSpeakerPageBreak = Convert.ToBoolean(dataSetLOB.Tables[0].Rows[0]["IsSpeakerPageBreak"]);
                }
                else
                {
                    addLOBModel.IsSpeakerPageBreak = false;
                }
            }
            //addLOBModel.RecordId = RecordId;
            //addLOBModel.LOBId = LOBId;
            //addLOBModel.TextSpeaker = TextSpeaker;

            return PartialView("PartialUpdateSpeakerLine", addLOBModel);
        }

        public JsonResult UpdateSpeakerText(AddLOBModel addLOBModel)
        {

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@RecordId", addLOBModel.RecordId));
            methodParameter.Add(new KeyValuePair<string, string>("@TextSpeaker", addLOBModel.TextSpeaker));
            methodParameter.Add(new KeyValuePair<string, string>("@IsSpeakerPageBreak", Convert.ToString(addLOBModel.IsSpeakerPageBreak)));

            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateSpeakerTextById", methodParameter);

            var res2 = Json(@Resources.LOB.SavedSuccessfully, JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion


        #region "SubmitSpeakerPad"

        /// <summary>
        /// It will submit an Draft SpeakerLOB for Approval
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public JsonResult SubmitSpeakerPad(string LOBId)
        {
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            if (Utility.CurrentSession.UserID != null)
            {
                methodParameter.Add(new KeyValuePair<string, string>("@SubmittedBy", Utility.CurrentSession.UserID));
                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SubmitSpeakerPadById", methodParameter);
                GenerateLOBPdf(LOBId, true, "Draft");
            }
            else
            {
                var res2 = Json(false, JsonRequestBehavior.AllowGet);
                return res2;
            }

            var res1 = Json(true, JsonRequestBehavior.AllowGet);
            return res1;
        }

        #endregion

        #region "IncludeAll"
        public JsonResult IncludeAll(String LOBId)
        {

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));

            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_IncludeAllLOBLinesForSpeaker", methodParameter);

            var res2 = Json(@Resources.LOB.SavedSuccessfully, JsonRequestBehavior.AllowGet);
            return res2;
        }
        #endregion

        #region "ExcludeAll"
        public JsonResult ExcludeAll(String LOBId)
        {

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));

            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_ExcludeAllLOBLinesForSpeaker", methodParameter);

            var res2 = Json(@Resources.LOB.SavedSuccessfully, JsonRequestBehavior.AllowGet);
            return res2;
        }
        #endregion


        #region PartialSubmitted

        /// <summary>
        /// Will render the PartialCreateLOB PartialView
        /// Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialSubmitted(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();
            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSpeakerSubmittedLOB]", methodParameter);

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        objLOB.SubmittedDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SpeakerSubmittedDate"]);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }
                        listLOBForDay.Add(objLOB);
                    }

                    objPendingLOB.ListLOBBYId = listLOBForDay;
                }

                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            return PartialView("PartialSubmitted", objPendingLOB);
        }

        #endregion

        #region "ShowSubmittedSpeakerPad"
        /// <summary>
        /// Get and Show Submitted Draft Speaker Pad from FileSystem
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public ActionResult ShowSubmittedSpeakerPad(string LOBId)
        {

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", "10"));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectAdminSpeakerPadById", methodParameter);
            string path = "";
            if (dataSetLOB != null)
            {
                if (dataSetLOB.Tables[0].Rows.Count > 0)
                {
                    path = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["SpeakerSubmittedLOBPath"]);
                }
            }

            if (path != null && path != "")
            {
                return File(path, "application/pdf");
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region ApprovalSpeakerPad

        /// <summary>
        /// Will render the SpeakerPad View Will two Tabs Pending,Approved
        /// Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult ApprovalSpeakerPad()
        {

            return View();
        }

        #endregion

        #region PartialPendingApproval

        /// <summary>
        /// Will render the PartialCreateLOB PartialView
        /// Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialPendingApproval(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();
            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSpeakerPendingApprovalLOB]", methodParameter);

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        objLOB.SubmittedDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SpeakerSubmittedDate"]);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }
                        listLOBForDay.Add(objLOB);
                    }

                    objPendingLOB.ListLOBBYId = listLOBForDay;
                }

                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            return PartialView("PartialPendingApproval", objPendingLOB);
        }

        #endregion

        #region UpdatePageBreak

        public ActionResult UpdatePageBreak(string LOBId)
        {
            AddLOBModel addLOBModel = new AddLOBModel();
            try
            {
                if (LOBId != null && LOBId != "")
                {
                    List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));

                    DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectApprovedLOBRecordByLOBIdForSpeaker", methodParameter);
                    List<AddLOBModel> ListLOB = new List<AddLOBModel>();
#pragma warning disable CS0219 // The variable 'IncludeALL' is assigned but its value is never used
                    string IncludeALL = "";
#pragma warning restore CS0219 // The variable 'IncludeALL' is assigned but its value is never used
                    for (int i = 0; i < dataSetLOB.Tables[0].Rows.Count; i++)
                    {
                        AddLOBModel addLOBModel1 = new AddLOBModel();
                        string SrNo1 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo1"]);
                        if (SrNo1 != "")
                        {
                            addLOBModel1.SrNo11 = SrNo1;
                            SrNo1 = SrNo1 + ".";
                        }
                        string SrNo2 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo2"]);
                        if (SrNo2 != "")
                        {
                            addLOBModel1.SrNo22 = SrNo2;
                            SrNo2 = "(" + SrNo2 + ")";
                        }
                        string SrNo3 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo3"]);
                        if (SrNo3 != "")
                        {
                            addLOBModel1.SrNo33 = SrNo3;
                            SrNo3 = "(" + ToRoman(Convert.ToInt16(SrNo3)) + ")";
                        }
                        addLOBModel1.SrNo1 = SrNo1;
                        addLOBModel1.SrNo2 = SrNo2;
                        addLOBModel1.SrNo3 = SrNo3;
                        addLOBModel1.Sessiondate = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SessionDate"]);
                        addLOBModel1.SessionName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SessionName"]);
                        addLOBModel1.AssemblyName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["AssemblyName"]);
                        addLOBModel1.RecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                        addLOBModel1.LOBId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["LOBId"]);
                        addLOBModel1.TextLOB = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TextLOB"]);
                        addLOBModel1.TextSpeaker = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TextSpeaker"]);
                        if (Convert.ToString(dataSetLOB.Tables[0].Rows[i]["IsSpeakerPageBreak"]) != "")
                        {
                            addLOBModel1.PageBreak = Convert.ToBoolean(dataSetLOB.Tables[0].Rows[i]["IsSpeakerPageBreak"]);
                        }
                        else
                        {
                            addLOBModel1.PageBreak = false;
                        }
                        //if (addLOBModel1.TextSpeaker == "" || addLOBModel1.TextSpeaker == null)
                        //{
                        //    IncludeALL = "false";
                        //}

                        ListLOB.Add(addLOBModel1);
                    }
                    addLOBModel.LOBList = ListLOB;


                    //if (IncludeALL == "false")
                    //{
                    //    addLOBModel.PageBreak = false;
                    //}
                    //else
                    //{
                    //    addLOBModel.PageBreak = true;
                    //}


                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("UpdatePageBreak", addLOBModel);
        }

        #endregion


        #region "AddPageBreake"

        public JsonResult AddPageBreake(string RecordId)
        {
            if (RecordId != null && RecordId != "")
            {
                AdminLOB model = new AdminLOB();
                model.Id = Convert.ToInt16(RecordId);
                Helper.ExecuteService("LOB", "AddPageBreakeSpeaker", model);
            }
            var res2 = Json("teste", JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region "RemovePageBreake"

        public JsonResult RemovePageBreake(string RecordId)
        {
            if (RecordId != null && RecordId != "")
            {
                AdminLOB model = new AdminLOB();
                model.Id = Convert.ToInt16(RecordId);
                Helper.ExecuteService("LOB", "RemovePageBreakeSpeaker", model);
            }
            var res2 = Json("teste", JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region "ApproveSpeakerPad"

        /// <summary>
        /// It will submit an Draft SpeakerLOB for Approval
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public JsonResult ApproveSpeakerPad(string LOBId)
        {
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            if (Utility.CurrentSession.UserID != null)
            {
                methodParameter.Add(new KeyValuePair<string, string>("@SpeakerApprovedBy", Utility.CurrentSession.UserID));
                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_ApproveSpeakerPadById", methodParameter);
                GenerateLOBPdf(LOBId, true, "Approved");
            }
            else
            {
                var res2 = Json(false, JsonRequestBehavior.AllowGet);
                return res2;
            }

            var res1 = Json(true, JsonRequestBehavior.AllowGet);
            return res1;
        }

        #endregion




        #region PartialApprovedGrid

        /// <summary>
        /// Will render the 
        /// Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialApprovedGrid(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();
            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectApprovedSpeakerPad]", methodParameter);

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        objLOB.ApproverdDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SpeakerApprovedDate"]);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }
                        listLOBForDay.Add(objLOB);
                    }

                    objPendingLOB.ListLOBBYId = listLOBForDay;
                }

                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            return PartialView("PartialApprovedGrid", objPendingLOB);
        }

        #endregion

        #region "ShowApprovedSpeakerPad"
        /// <summary>
        /// Get and Show Approved Speaker Pad from FileSystem
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public ActionResult ShowApprovedSpeakerPad(string LOBId)
        {

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", "10"));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectAdminSpeakerPadById", methodParameter);
            string path = "";
            if (dataSetLOB != null)
            {
                if (dataSetLOB.Tables[0].Rows.Count > 0)
                {
                    path = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["SpeakerApprovedLOBPath"]);
                }
            }

            if (path != null && path != "")
            {
                return File(path, "application/pdf");
            }
            else
            {
                return null;
            }
        }
        #endregion


        #region "GenerateLOBPdf"
        public FileStreamResult GenerateLOBPdf(string LOBId, bool download, string LOBType)
        {
            string LOBName = "20Dec2013_1_LOB";
            string CurrentSecretery = "";
            DateTime SessionDate = new DateTime();
            string savedPDF = string.Empty;

            MemoryStream output = new MemoryStream();
            if (LOBId != null && LOBId != "")
            {
                LOBModel objLOBModel = new LOBModel();
                DataSet SummaryDataSet = new DataSet();
                string sessiondate = "";
                string outXml = "";
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftSpeakerPadByLOBId]", methodParameter);


                //Getting Current Secretary
                DataSet SiteSettingDataSet = new DataSet();
                var methodParameter1 = new List<KeyValuePair<string, string>>();
                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
                {
                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
                    if (dtSiteSetting != null)
                    {
                        IEnumerable<DataRow> siteSettingQuery =
                                from siteSetting in dtSiteSetting.AsEnumerable()
                                select siteSetting;
                        IEnumerable<DataRow> curtSecretery =
                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
                        }
                    }
                }

                string SubittedDate = "";
#pragma warning disable CS0219 // The variable 'SubittedTime' is assigned but its value is never used
                string SubittedTime = "";
#pragma warning restore CS0219 // The variable 'SubittedTime' is assigned but its value is never used
                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {

                    outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    string SrNo1 = "";
                    string SrNo2 = "";
                    string SrNo3 = "";
                    int SrNo1Count = 1;
                    int SrNo2Count = 1;
                    int SrNo3Count = 1;
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {

                        if (i == 0)
                        {
                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            string date = ConvertSQLDate(SessionDate);
                            date = date.Replace("/", "");
                            date = date.Replace("-", "");
                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


                            outXml += @"<tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
                                                         हिमाचल प्रदेश&nbsp;" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
                                                    </td>
                                                </tr>
                                              
                                                <tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold'>
                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold; height: 30px;'>                
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;'>               
                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;text-decoration: underline;'>               
                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
                                                    </td>
                                                </tr>";

                            if (LOBType == "Draft")
                            {
                                if (SummaryDataSet.Tables[0].Rows[i]["SpeakerSubmittedDate"] != null && Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SpeakerSubmittedDate"]) != "")
                                {
                                    SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SpeakerSubmittedDate"]).ToString("D", new CultureInfo("hi")); ;
                                }
                                else
                                {
                                    SubittedDate = System.DateTime.Now.ToString("dd/MM/yyyy");
                                }

                            }
                            else
                            {

                                SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SpeakerApprovedDate"]).ToString("D", new CultureInfo("hi"));

                            }

                        }


                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
                            outXml += @"<tr><td style='text-align: justify;width:100%; font-size: 24px;'>
                                      <br/>   <div style='padding-left:10px;width:100%;text-align:justify;'>
                                    " + SrNo1 + @"&nbsp;&nbsp;&nbsp; <div style='padding-left:20px;width:100%; margin: -35px 0 0 2%;'>";
                            SrNo1Count = SrNo1Count + 1;
                            SrNo2Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                            outXml += @"<tr><td style='text-align: justify;width:100%; font-size: 22px;'><div style='padding-left:33px;text-align:justify;width:100%;'>
                                    " + SrNo2 + @"&nbsp;&nbsp;&nbsp;<div style='padding-left:15px; margin: -30px 0 0 4.5%;text-align:justify;width:100%;'>";
                            SrNo2Count = SrNo2Count + 1;
                            SrNo3Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
                        {

                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                            outXml += @"<tr><td style='text-align: justify;width:100%; font-size: 22px;'><div style='padding-left:60px;text-align:justify;width:100%;'>
                                    " + SrNo3 + @"&nbsp;&nbsp;&nbsp; <div style='padding-left:15px; margin: -33px 0 0 6%;text-align:justify;width:100%;'>";
                            SrNo3Count = SrNo3Count + 1;

                        }

                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextSpeaker"]);
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                            LOBText = LOBText.Replace("^^", "");
                        }

                        //string Member = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberDesignationLocal"]) + ")";
                        //string Minister = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterDesignationLocal"]) + ")";
                        //string Department = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedDeptNameLocal"]);
                        //string Committee = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedCommitteeNameLocal"]);
                        //string Rule = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedRuleNameLocal"]);
                        outXml += @"" + LOBText + "</div></div> ";
                        //if (Member != " ()")
                        //{
                        //    outXml += @"<b>  सदस्य: </b>" + Member;
                        //}
                        //if (Department != "")
                        //{
                        //    outXml += @"<b>  विभाग: </b> " + Department;
                        //    if (Minister != "")
                        //    {
                        //        outXml += @"<b>  मंत्री: </b> " + Minister;
                        //    }
                        //}
                        //if (Committee != "")
                        //{
                        //    outXml += @"<b>  समिति: </b> " + Committee;
                        //}
                        //if (Rule != "")
                        //{
                        //    outXml += @"<b>  नियम: </b> " + Rule;
                        //}

                        outXml += @"<br/> </td></tr>";


                    }


                    //Bottom Section
                    outXml += @" <tr><td colspan='2'><br /><br /><br /><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:70%;float:left;font-size: 24px;'>
                                                शिमला-171004
                                                </td>
                                                <td style='width:30%;float:left;font-size: 24px;'>
                                                 " + CurrentSecretery + @"  
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:70%;float:left;font-size: 24px;'>
                                                " + SubittedDate + @"
                                                </td>
                                                <td style='width:30%;float:left;font-size: 24px;'>
                                                सचिव।
                                                </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                    outXml += "</table></div></div> </body> </html>";

                }

                // convert HTML to PDF
                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(40, 20, 20, 20);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(10, 10, 55, 55), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(40, 20, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }


                ///To get File Path
                ///
                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string CurrentSession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        CurrentSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";


                sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                sessiondate = sessiondate.Replace('/', ' ');

                if (download)
                {
                    if (LOBType == "Draft")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=SpeakerDraft_" + LOBId + ".pdf");

                        string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + sessiondate + "/";
                        string directory = Server.MapPath(url);
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);
                        }
                        fileName = "SpeakerDraft_" + LOBId + ".pdf";
                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");

                        #endregion
                        methodParameter = new List<KeyValuePair<string, string>>();
                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedSpeakerPadPath]", methodParameter);
                        return null;
                    }
                    else if (LOBType == "Approved")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=SpeakerApproved_" + LOBId + ".pdf");
                        string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + sessiondate + "/";
                        string directory = Server.MapPath(url);
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);
                        }
                        fileName = "SpeakerApproved_" + LOBId + ".pdf";
                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion
                        methodParameter = new List<KeyValuePair<string, string>>();
                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedApprovedSpeakerPath]", methodParameter);
                        return null;
                    }
                    else if (LOBType == "Published")
                    {

                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=SpeakerPublished_" + LOBId + ".pdf");
                        string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + sessiondate + "/";
                        string directory = Server.MapPath(url);
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);
                        }
                        fileName = "SpeakerPublished_" + LOBId + ".pdf";
                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion
                        methodParameter = new List<KeyValuePair<string, string>>();
                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@PublishLOBPath", url + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_PublishedLOBPath]", methodParameter);
                        return null;
                    }
                }
            }

            return File(output, "application/pdf");
        }

        #endregion

        #region "GenerateXMLlob"
        public ActionResult GenerateXMLlob(string LOBId)
        {
            //  string test1 = CreateQuesXML("2", "20-12-2013");
            // string test2 = CreateQuesXML("1", "20-12-2013");
            string test = CreateXML(LOBId, "Draft");
            //List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            //methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            //methodParameter.Add(new KeyValuePair<string, string>("@DraftLOBxmlLocation", test));

            //DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDraftLOBxmlPathLOB", methodParameter);
            return File(test, "application/xml", "DraftSpeakerFile.xml");
        }
        #endregion


        #region "GenerateXMLApprovedlob"
        public ActionResult GenerateXMLApprovedlob(string LOBId)
        {
            string test = CreateXML(LOBId, "Approved");


            //List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            //methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            //methodParameter.Add(new KeyValuePair<string, string>("@LOBxmlLocation", test));

            //DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBxmlPathLOB", methodParameter);
            return File(test, "application/xml", "SpeakerFile.xml");
        }
        #endregion



        #region "CreateXML"

        public string CreateXML(string LOBId, string Form)
        {
            var methodParameter = new List<KeyValuePair<string, string>>();
            DataSet dataset = null;
            if (Form == "Draft")
            {
                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));
                dataset = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftSpeakerPadForXML]", methodParameter);
            }
            else
            {
                dataset = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectCurrentSpeakerPadForXML]", methodParameter);
            }



            if (dataset != null)
            {

                XElement LOB = new System.Xml.Linq.XElement("LOB");
                XElement pages = new System.Xml.Linq.XElement("pages");
                LOB.Add(pages);
                XElement header = new XElement("header-page");
                //header.Attribute("id").Value="header_page";
                header.SetAttributeValue("id", "header_page");
                pages.Add(header);
                XElement content = new XElement("content");
                header.Add(content);



                DateTime SessionDate = Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]);
                string sessiondate = Convert.ToString(dataset.Tables[0].Rows[0]["SessionDate"]);
                string date = ConvertSQLDate(SessionDate);
                date = date.Replace("/", "");
                date = date.Replace("-", "");
                string LOBName = date + "_" + "LOB" + "_" + Convert.ToString(dataset.Tables[0].Rows[0]["LOBId"]);

                content.ReplaceNodes(new XCData(@"<h2 style='text-align: center;font-family:DVOT-Yogesh;'>" + Convert.ToString(dataset.Tables[0].Rows[0]["AssemblyNameLocal"]) + @"</h2>
                    <h4 style='text-align: center;font-family:DVOT-Yogesh;'>कार्य - सूची</h4>
                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionNameLocal"]) + @"  </h4>
                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionDateLocal"]) + @"  " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionTimeLocal"]) + @" बजे शुरू</h4>"));

                LOB.SetAttributeValue("AssemblyId", Convert.ToString(dataset.Tables[0].Rows[0]["AssemblyId"]));
                LOB.SetAttributeValue("SessionId", Convert.ToString(dataset.Tables[0].Rows[0]["SessionId"]));
                LOB.SetAttributeValue("SessionDate", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd/MM/yyyy"));


                //page
                int idCount = 2;
                string SrNo1 = "";
                string SrNo2 = "";
                string SrNo3 = "";
                int SrNo1Count = 1;
                int SrNo2Count = 1;
                int SrNo3Count = 1;


                string outXml = "";

                //XElement page = new XElement("page");
                //XElement cont = new XElement("content");

                XElement page1 = new XElement("page");
                // XElement cont1 = new XElement("content");
                XElement business = new XElement("business");
                //  page1.Add(business);
                //Index
                XElement index = new XElement("index-page");
                index.SetAttributeValue("id", "index_page");
                pages.Add(index);
                XElement indexContent = new XElement("content");
                StringBuilder sb = new StringBuilder();

                XElement indexOne = new XElement("index");


                for (int count = 0; count < dataset.Tables[0].Rows.Count; count++)
                {
                    business = new XElement("business");

                    if (count == 0)
                    {
                        page1.SetAttributeValue("id", 2);
                        page1.SetAttributeValue("page-des", "first-page");



                    }
                    else
                    {
                        if (Convert.ToInt32(dataset.Tables[0].Rows[count - 1]["SrNo1"]) != Convert.ToInt32(dataset.Tables[0].Rows[count]["SrNo1"]))
                        {

                        }

                    }




                    if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == true && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == true)
                    {
                        if (count != 0)
                        {

                            //   page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            // business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);
                        }


                        string LOBText1 = Convert.ToString(dataset.Tables[0].Rows[count]["TextSpeaker"]);
                        if (LOBText1 != "")
                        {
                            LOBText1 = LOBText1.Replace("<p>", "");
                            LOBText1 = LOBText1.Replace("</p>", "");
                        }
                        sb.Append("<li>" + LOBText1 + "</li>");
                        XElement subIndex1 = new XElement("index");
                        subIndex1.SetAttributeValue("id", idCount);
                        LOBText1 = StripHTML(LOBText1);
                        subIndex1.SetAttributeValue("description", LOBText1);
                        subIndex1.SetAttributeValue("command", "test_command");
                        subIndex1.SetAttributeValue("enabled", "true");
                        indexOne.Add(subIndex1);


                        /// For contant
                        outXml = "";
                        SrNo1 = Convert.ToString(SrNo1Count) + ". ";
                        //                        outXml += @" <p style='font-size:20px; font-family:DVOT-Yogesh;text-align:justify;'><span>
                        //                                    
                        //                                    " + SrNo1 + @"";
                        string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextSpeaker"]);

                        int indexof = LOBText.IndexOf("^^");
                        if (indexof != -1)
                        {
                            string[] subqestion = LOBText.Split(new string[] { "^^" }, StringSplitOptions.None);
                            for (int j = 0; j < subqestion.Count(); j++)
                            {
                                if (j == 0)
                                {
                                    XElement contentOne = new XElement("content");
                                    var conten = subqestion[j];
                                    if (subqestion.Count() > 1)
                                    {
                                        if (conten != "")
                                        {
                                            conten = conten.Replace("<p>", "");
                                            conten = conten.Replace("</p>", "");
                                        }
                                        outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                                        outXml += conten + "</span></p>";

                                        outXml += "<br/>";
                                    }
                                    contentOne.ReplaceNodes(new XCData(outXml));
                                    outXml = "";
                                    business.Add(contentOne);
                                }
                                else
                                {
                                    page1.Add(business);
                                    pages.Add(page1);
                                    idCount++;
                                    page1 = new XElement("page");
                                    business = new XElement("business");
                                    page1.SetAttributeValue("id", idCount);

                                    var conten = subqestion[j];
                                    if (j == subqestion.Count() - 1)
                                    {
                                        if (conten != "")
                                        {
                                            conten = conten.Replace("<p>", "");
                                            conten = conten.Replace("</p>", "");
                                        }
                                        outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                                        outXml += conten + "</span></p>";

                                        outXml += "<br/>";
                                    }
                                    else
                                    {
                                        if (conten != "")
                                        {
                                            conten = conten.Replace("<p>", "");
                                            conten = conten.Replace("</p>", "");
                                        }
                                        outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                                        outXml += conten + "</span></p>";

                                        outXml += "<br/>";
                                    }

                                    XElement contentOne = new XElement("content");
                                    contentOne.ReplaceNodes(new XCData(outXml));
                                    outXml = "";
                                    business.Add(contentOne);

                                }
                            }
                        }
                        else
                        {
                            if (LOBText != "")
                            {
                                LOBText = LOBText.Replace("<p>", "");
                                LOBText = LOBText.Replace("</p>", "");
                            }
                            outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                            outXml += LOBText + "</span></p>";

                            outXml += "<br/>";

                            XElement contentOne = new XElement("content");

                            contentOne.ReplaceNodes(new XCData(outXml));
                            outXml = "";
                            business.Add(contentOne);

                        }

                        //if (LOBText != "")
                        //{
                        //    LOBText = LOBText.Replace("<p>", "");
                        //    LOBText = LOBText.Replace("</p>", "");
                        //}

                        //outXml += LOBText + "</span></p>";



                        //XElement contentOne = new XElement("content");

                        //contentOne.ReplaceNodes(new XCData(outXml));

                        //business.Add(contentOne);


                        SrNo1Count = SrNo1Count + 1;
                        SrNo2Count = 1;

                        //for page break
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["IsSpeakerPageBreak"]) == "True")
                        {
                            page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);
                        }
                        else
                        {
                            page1.Add(business);

                        }

                    }
                    else if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == true)
                    {
                        outXml = "";
                        SrNo2 = " (" + Convert.ToString(SrNo2Count) + "). ";

                        string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextSpeaker"]);


                        int indexof = LOBText.IndexOf("^^");
                        if (indexof != -1)
                        {
                            string[] subqestion = LOBText.Split(new string[] { "^^" }, StringSplitOptions.None);
                            for (int j = 0; j < subqestion.Count(); j++)
                            {
                                if (j == 0)
                                {
                                    XElement contentOne = new XElement("content");
                                    var conten = subqestion[j];
                                    if (subqestion.Count() > 1)
                                    {
                                        if (conten != "")
                                        {
                                            conten = conten.Replace("<p>", "");
                                            conten = conten.Replace("</p>", "");
                                        }
                                        outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                                        outXml += conten + "</span></p>";

                                        outXml += "<br/>";
                                    }
                                    contentOne.ReplaceNodes(new XCData(outXml));
                                    outXml = "";
                                    business.Add(contentOne);
                                }
                                else
                                {
                                    page1.Add(business);
                                    pages.Add(page1);
                                    idCount++;
                                    page1 = new XElement("page");
                                    business = new XElement("business");
                                    page1.SetAttributeValue("id", idCount);

                                    var conten = subqestion[j];
                                    if (j == subqestion.Count() - 1)
                                    {
                                        if (conten != "")
                                        {
                                            conten = conten.Replace("<p>", "");
                                            conten = conten.Replace("</p>", "");
                                        }
                                        outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                                        outXml += conten + "</span></p>";

                                        outXml += "<br/>";
                                    }
                                    else
                                    {
                                        if (conten != "")
                                        {
                                            conten = conten.Replace("<p>", "");
                                            conten = conten.Replace("</p>", "");
                                        }
                                        outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                                        outXml += conten + "</span></p>";

                                        outXml += "<br/>";
                                    }

                                    XElement contentOne = new XElement("content");
                                    contentOne.ReplaceNodes(new XCData(outXml));
                                    outXml = "";
                                    business.Add(contentOne);

                                }
                            }
                        }
                        else
                        {
                            if (LOBText != "")
                            {
                                LOBText = LOBText.Replace("<p>", "");
                                LOBText = LOBText.Replace("</p>", "");
                            }
                            outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                            outXml += LOBText + "</span></p>";

                            outXml += "<br/>";

                            XElement contentOne = new XElement("content");

                            contentOne.ReplaceNodes(new XCData(outXml));
                            outXml = "";
                            business.Add(contentOne);

                        }

                        SrNo2Count = SrNo2Count + 1;
                        SrNo3Count = 1;
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["IsSpeakerPageBreak"]) == "True")
                        {
                            page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);
                        }
                        else
                        {
                            page1.Add(business);

                        }

                    }
                    else if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == false)
                    {
                        outXml = "";
                        SrNo3 = ToRoman(Convert.ToInt16(Convert.ToString(SrNo3Count))) + ". ";
                        //                        outXml += @"<p style='text-align: justify;padding-left: 50px;font-family:DVOT-Yogesh;font-size:18px'><span >  
                        //                                    " + "" + @"";
                        string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextSpeaker"]);



                        int indexof = LOBText.IndexOf("^^");
                        if (indexof != -1)
                        {
                            string[] subqestion = LOBText.Split(new string[] { "^^" }, StringSplitOptions.None);
                            for (int j = 0; j < subqestion.Count(); j++)
                            {
                                if (j == 0)
                                {
                                    XElement contentOne = new XElement("content");
                                    var conten = subqestion[j];
                                    if (subqestion.Count() > 1)
                                    {
                                        if (conten != "")
                                        {
                                            conten = conten.Replace("<p>", "");
                                            conten = conten.Replace("</p>", "");
                                        }
                                        outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                                        outXml += conten + "</span></p>";

                                        outXml += "<br/>";
                                    }
                                    contentOne.ReplaceNodes(new XCData(outXml));
                                    outXml = "";
                                    business.Add(contentOne);
                                }
                                else
                                {
                                    page1.Add(business);
                                    pages.Add(page1);
                                    idCount++;
                                    page1 = new XElement("page");
                                    business = new XElement("business");
                                    page1.SetAttributeValue("id", idCount);

                                    var conten = subqestion[j];
                                    if (j == subqestion.Count() - 1)
                                    {
                                        if (conten != "")
                                        {
                                            conten = conten.Replace("<p>", "");
                                            conten = conten.Replace("</p>", "");
                                        }
                                        outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                                        outXml += conten + "</span></p>";

                                        outXml += "<br/>";
                                    }
                                    else
                                    {
                                        if (conten != "")
                                        {
                                            conten = conten.Replace("<p>", "");
                                            conten = conten.Replace("</p>", "");
                                        }
                                        outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                                        outXml += conten + "</span></p>";

                                        outXml += "<br/>";
                                    }

                                    XElement contentOne = new XElement("content");
                                    contentOne.ReplaceNodes(new XCData(outXml));
                                    outXml = "";
                                    business.Add(contentOne);

                                }
                            }
                        }
                        else
                        {
                            if (LOBText != "")
                            {
                                LOBText = LOBText.Replace("<p>", "");
                                LOBText = LOBText.Replace("</p>", "");
                            }

                            outXml += @"<p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:18px;padding-left: 25px'><span>";

                            outXml += LOBText + "</span></p>";

                            outXml += "<br/>";

                            XElement contentOne = new XElement("content");

                            contentOne.ReplaceNodes(new XCData(outXml));
                            outXml = "";
                            business.Add(contentOne);

                        }




                        ///for the action tab for pdf link


                        SrNo3Count = SrNo3Count + 1;
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["IsSpeakerPageBreak"]) == "True")
                        {
                            page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);
                        }
                        else
                        {
                            page1.Add(business);

                        }
                    }




                    if (count == dataset.Tables[0].Rows.Count - 1)
                    {

                        // page1.Add(business);
                        pages.Add(page1);
                        ///Last page
                        page1 = new XElement("page");
                        business = new XElement("business");
                        page1.SetAttributeValue("id", idCount + 1);
                        page1.SetAttributeValue("page-des", "last-page");
                        page1.Add(business);
                        pages.Add(page1);

                    }
                }
                //indexContent.SetValue(sb);
                indexContent.ReplaceNodes(new XCData("<h5><b>Today's list of business:</b></h5><ol>" + sb + "<ol>"));
                index.Add(indexContent);

                index.Add(indexOne);

                ///Save XML File
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(LOB.ToString());


                methodParameter = new List<KeyValuePair<string, string>>();

                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string CurrentSession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        CurrentSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";


                sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                sessiondate = sessiondate.Replace('/', ' ');
                if (Form == "Draft")
                {
                    fileName = "DraftSpeakerFile.xml";
                    string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + sessiondate + "/";
                    string directory = Server.MapPath(url);
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = System.IO.Path.Combine(Server.MapPath("~" + url), fileName);
                    //  file.SaveAs(path);
                    xml.Save(path);
                    return "~" + url + fileName;
                }
                else
                {
                    fileName = "SpeakerFile.xml";
                    string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + sessiondate + "/";
                    string directory = Server.MapPath(url);
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = System.IO.Path.Combine(Server.MapPath("~" + url), fileName);
                    //  file.SaveAs(path);
                    xml.Save(path);
                    return "~" + url + fileName;
                    //xml.Save("C:/eVidhan/LOB/LOBFile.xml");
                }

#pragma warning disable CS0162 // Unreachable code detected
                return "";
#pragma warning restore CS0162 // Unreachable code detected
            }
            return "";
        }


        private string StripHTML(string source)
        {
            try
            {
                string result;

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = source.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove all others. More can be added, see

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // for testing
                //System.Text.RegularExpressions.Regex.Replace(result,
                //       this.txtRegex.Text,string.Empty,
                //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // make line breaking consistent
                result = result.Replace("\n", "\r");

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4.
                // Prepare first to remove any whitespaces in between
                // the escaped characters and remove redundant tabs in between line breaks
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove multiple tabs following a line break with just one tab
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Initial replacement target string for line breaks
                string breaks = "\r\r\r";
                // Initial replacement target string for tabs
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }

                // That's it.
                return result;
            }
            catch
            {

                return source;
            }
        }
        #endregion

        #region "Comman Methods"

        /// <summary>
        /// convert the number in the Roman
        /// Created by : Sujeet Kumar
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "m" + ToRoman(number - 1000);
            if (number >= 900) return "cm" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "d" + ToRoman(number - 500);
            if (number >= 400) return "cd" + ToRoman(number - 400);
            if (number >= 100) return "c" + ToRoman(number - 100);
            if (number >= 90) return "xc" + ToRoman(number - 90);
            if (number >= 50) return "l" + ToRoman(number - 50);
            if (number >= 40) return "xl" + ToRoman(number - 40);
            if (number >= 10) return "x" + ToRoman(number - 10);
            if (number >= 9) return "ix" + ToRoman(number - 9);
            if (number >= 5) return "v" + ToRoman(number - 5);
            if (number >= 4) return "iv" + ToRoman(number - 4);
            if (number >= 1) return "i" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        public string ConvertSQLDate(DateTime? dt)
        {
            DateTime ConvtDate = Convert.ToDateTime(dt);
            string month = ConvtDate.Month.ToString();
            if (month.Length < 2)
            {
                month = "0" + month;
            }
            string day = ConvtDate.Day.ToString();
            string year = ConvtDate.Year.ToString();
            string Date = day + "/" + month + "/" + year;
            return Date;
        }

        #endregion
    }
}
