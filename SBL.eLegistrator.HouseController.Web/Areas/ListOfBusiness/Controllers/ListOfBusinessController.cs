﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models;
using System.Data;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.tool.xml;
using System.Globalization;
using System.Xml.Serialization;
using System.Xml;
using EvoPdf;
using System.Xml.Linq;
using System.Text;
using SBL.DomainModel.Models.LOB;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.PaperLaid;
using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Filters;
using System.Text.RegularExpressions;

namespace SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Controllers
{
    [SBLAuthorize(Allow = "Authenticated")]
    [Audit]
    public class ListOfBusinessController : Controller
    {
        //
        // GET: /ListOfBusiness/ListOfBusiness/
        LOBModel objPendingLOB1 = new LOBModel();
        public ActionResult Index()
        {
            return View();
        }

        #region CreateLOB

        /// <summary>
        /// Will render the CreateLOB View Will Three Tabs CreateLOB,PendingLOB,SubmittedLOB
        /// Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateLOB()
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            //string test = CreateQuesXML("1");
            // string test1 = CreateQuesXML("2","20-12-2013");
            // string test2 = CreateQuesXML("1", "20-12-2013");
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
            string CurrentAssembly = "";
            string Currentsession = "";


            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                {
                    CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                {
                    Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
            }
            //AddLOB.SessionId = Currentsession;
            //AddLOB.AssemblyId = CurrentAssembly;
            tPaperLaidV mdl = new tPaperLaidV();
            mdl.SessionName = Currentsession;
            mdl.AssesmblyName = CurrentAssembly;

            //string test = CreateXML();
            return View(mdl);
        }

        #endregion

        #region PartialCreateLOB

        /// <summary>
        /// Will render the PartialCreateLOB PartialView
        /// Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialCreateLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            AddLOBModel AddLOB = new AddLOBModel();
            try
            {
                ////Getting the Current assembly AND session.
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";

                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }
                AddLOB.SessionId = Currentsession;
                AddLOB.AssemblyId = CurrentAssembly;
                //addLines.mDepartmentList = DepartmentList;

                ///getting the assembly Information
                methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@AssemblyID", CurrentAssembly));
                DataSet dataSetAssembly = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectAssemblyInfoById", methodParameter);
                AddLOB.AssemblyName = Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyName"]);

                ///getting the session information
                methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@SessionId", Currentsession));
                methodParameter.Add(new KeyValuePair<string, string>("@AssemblyID", CurrentAssembly));
                DataSet dataSetSession = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionInfoById", methodParameter);

                List<mSession> ListSession = new List<mSession>();

                mSession Session1 = new mSession();
                Session1.SessionDate = "Select";
                ListSession.Add(Session1);

                for (int i = 0; i < dataSetSession.Tables[0].Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        AddLOB.SessionName = Convert.ToString(dataSetSession.Tables[0].Rows[i]["SessionName"]);
                        AddLOB.SessionTime = Convert.ToString(dataSetSession.Tables[0].Rows[i]["SessionTimeF"]);
                    }
                    mSession Session = new mSession();
                    Session.SessionDate = Convert.ToDateTime(Convert.ToString(dataSetSession.Tables[0].Rows[i]["SessionDate"])).ToString("dd/MM/yyyy");
                    ListSession.Add(Session);
                }
                AddLOB.mSessionList = ListSession;

                if (LOBId != null && LOBId != "")
                {
                    AddLOB.LOBId = LOBId;
                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                    methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", "1"));
                    methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", "10"));
                    DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDraftLOBByIdForLineRecord", methodParameter);
                    if (dataSetLOB != null)
                    {
                        ListSession = new List<mSession>();
                        if (dataSetLOB.Tables[0].Rows.Count > 0)
                        {
                            mSession Session = new mSession();
                            Session.SessionDate = Convert.ToDateTime(Convert.ToString(dataSetLOB.Tables[0].Rows[0]["SessionDate"])).ToString("dd/MM/yyyy");
                            ListSession.Add(Session);
                            AddLOB.Sessiondate = Convert.ToDateTime(Convert.ToString(dataSetLOB.Tables[0].Rows[0]["SessionDate"])).ToString("dd/MM/yyyy");

                            TimeSpan interval = TimeSpan.Parse(Convert.ToString(dataSetLOB.Tables[0].Rows[0]["SessionTime"]));
                            DateTime time = DateTime.Today.Add(interval);
                            string displayTime = time.ToString("hh:mm tt");
                            AddLOB.SessionTime = displayTime;
                        }
                        AddLOB.mSessionList = ListSession;
                    }
                }

                return PartialView("PartialCreateLOB", AddLOB);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return PartialView("PartialCreateLOB", AddLOB);
            }
        }

        /// <summary>
        /// Get session time using session date
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="Sessiondate"></param>
        /// <returns></returns>
        public JsonResult GetSessionTime(string Sessiondate)
        {
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
            DataSet dataSetSession = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionDateInfo", methodParameter);
            string result = "";
            List<mSession> ListSession = new List<mSession>();
            result = Convert.ToString(dataSetSession.Tables[0].Rows[0]["SessionTimeF"]);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Check weather LOB is created for given Session date
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="Sessiondate"></param>
        /// <returns></returns>
        public JsonResult CheckSessionDateLOB(string Sessiondate)
        {
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLOBFromSessionDate", methodParameter);
            bool result = false;
            List<mSession> ListSession = new List<mSession>();

            if (dataSetLOB.Tables[0].Rows.Count > 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "PartialLineEntryForm"

        /// <summary>
        /// It will render the Line Entry For enter lines of the LOB
        /// Created By:Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <param name="RecordId"></param>
        /// <returns></returns>
        public ActionResult PartialLineEntryForm(string LOBId, string RecordId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }

            AddLOBModel addLines = new AddLOBModel();
            try
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetDept = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDepartment", methodParameter);
                List<mDepartment> DepartmentList = new List<mDepartment>();
                mDepartment Department2 = new mDepartment();
                Department2.DeptId = "0";
                Department2.DeptName = "Select";
                DepartmentList.Add(Department2);

                for (int i = 0; i < dataSetDept.Tables[0].Rows.Count; i++)
                {
                    mDepartment Department = new mDepartment();
                    Department.DeptId = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptId"]);
                    Department.DeptName = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptname"]);
                    DepartmentList.Add(Department);
                }

                addLines.mDepartmentList = DepartmentList;



                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetCommittee = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommittee", methodParameter);
                List<mCommittee> CommitteeList = new List<mCommittee>();
                mCommittee Committee2 = new mCommittee();
                Committee2.CommitteeId = "0";
                Committee2.CommitteeName = "Select";
                CommitteeList.Add(Committee2);

                for (int i = 0; i < dataSetCommittee.Tables[0].Rows.Count; i++)
                {
                    mCommittee Committee = new mCommittee();
                    Committee.CommitteeId = Convert.ToString(dataSetCommittee.Tables[0].Rows[i]["CommitteeId"]);
                    Committee.CommitteeName = Convert.ToString(dataSetCommittee.Tables[0].Rows[i]["CommitteeName"]);
                    CommitteeList.Add(Committee);
                }
                addLines.mCommitteeList = CommitteeList;

                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetCommitteeRepTyp = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommitteeRepType", methodParameter);
                List<mCommitteeRepType> CommitteeRepTyp = new List<mCommitteeRepType>();
                mCommitteeRepType CommitteeRepType = new mCommitteeRepType();
                CommitteeRepType.ReportTypeId = "0";
                CommitteeRepType.ReportTypeName = "Select";
                CommitteeRepTyp.Add(CommitteeRepType);

                for (int i = 0; i < dataSetCommitteeRepTyp.Tables[0].Rows.Count; i++)
                {
                    mCommitteeRepType CommitteeRepType1 = new mCommitteeRepType();
                    CommitteeRepType1.ReportTypeId = Convert.ToString(dataSetCommitteeRepTyp.Tables[0].Rows[i]["ReportTypeId"]);
                    CommitteeRepType1.ReportTypeName = Convert.ToString(dataSetCommitteeRepTyp.Tables[0].Rows[i]["ReportTypeName"]);
                    CommitteeRepTyp.Add(CommitteeRepType1);
                }
                addLines.mCommitteeRepTypeList = CommitteeRepTyp;



                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetEvent = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEventsLOB", methodParameter);
                List<mEvent> EventList = new List<mEvent>();
                mEvent Event1 = new mEvent();
                Event1.EventId = "0";
                Event1.EventName = "Select";
                EventList.Add(Event1);

                for (int i = 0; i < dataSetEvent.Tables[0].Rows.Count; i++)
                {
                    mEvent Event2 = new mEvent();
                    Event2.EventId = Convert.ToString(dataSetEvent.Tables[0].Rows[i]["EventID"]);
                    Event2.EventName = Convert.ToString(dataSetEvent.Tables[0].Rows[i]["EventName"]);
                    EventList.Add(Event2);
                }

                addLines.mEventList = EventList;
                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";

                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                List<SelectListItem> years = new List<SelectListItem>();
                int currentYear = DateTime.Now.Year;

                for (int i = currentYear - 70; i < currentYear; i++)
                {
                    SelectListItem year = new SelectListItem
                    {
                        Text =
                            i.ToString(),
                        Value = i.ToString()
                    };

                    years.Add(year);
                }
                for (int i = currentYear; i < currentYear + 5; i++)
                {
                    SelectListItem year = new SelectListItem();
                    if (i == DateTime.Now.Year)
                    {
                        year = new SelectListItem
                        {
                            Text =
                                i.ToString(),
                            Value = i.ToString(),
                            Selected = true
                        };
                    }
                    else
                    {
                        year = new SelectListItem
                        {
                            Text =
                                i.ToString(),
                            Value = i.ToString()
                        };
                    }
                    years.Add(year);
                }
                addLines.yearList = years;
                //methodParameter = new List<KeyValuePair<string, string>>();
                //methodParameter.Add(new KeyValuePair<string, string>("@AssemblyId", CurrentAssembly));
                //DataSet dataSetMemebers = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectMembers", methodParameter);

                //List<mMinister> ministerList = new List<mMinister>();

                //mMinister Minister2 = new mMinister();
                //Minister2.MinisterId = "0";
                //Minister2.MinisterName = "Select";
                //ministerList.Add(Minister2);


                //for (int i = 0; i < dataSetMemebers.Tables[0].Rows.Count; i++)
                //{
                //    mMinister Minister = new mMinister();
                //    Minister.MinisterId = Convert.ToString(dataSetMemebers.Tables[0].Rows[i]["MemberID"]);
                //    Minister.MinisterName = Convert.ToString(dataSetMemebers.Tables[0].Rows[i]["Prefix"]) + " " + Convert.ToString(dataSetMemebers.Tables[0].Rows[i]["Name"]) + "         (" + Convert.ToString(dataSetMemebers.Tables[0].Rows[i]["Designation"]) + ")";
                //    ministerList.Add(Minister);
                //}
                //addLines.mMinisterList = ministerList;





                //methodParameter = new List<KeyValuePair<string, string>>();

                //DataSet dataSetRule = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectRules", methodParameter);

                //List<mRule> RuleList = new List<mRule>();

                //mRule Rule2 = new mRule();
                //Rule2.RuleId = "0";
                //Rule2.RuleName = "Select";
                //RuleList.Add(Rule2);


                //for (int i = 0; i < dataSetRule.Tables[0].Rows.Count; i++)
                //{
                //    mRule Rule = new mRule();
                //    Rule.RuleId = Convert.ToString(dataSetRule.Tables[0].Rows[i]["RuleId"]);
                //    Rule.RuleName = Convert.ToString(dataSetRule.Tables[0].Rows[i]["RuleName"]);
                //    RuleList.Add(Rule);
                //}
                //addLines.mRuleList = RuleList;

                methodParameter = new List<KeyValuePair<string, string>>();

                //DataSet dataSetResolution = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectResolution", methodParameter);

                //List<mResolution> ListResolution = new List<mResolution>();

                //for (int i = 0; i < dataSetResolution.Tables[0].Rows.Count; i++)
                //{
                //    mResolution Resolution1 = new mResolution();
                //    Resolution1.ResolutionId = Convert.ToInt16(dataSetResolution.Tables[0].Rows[i]["ResolutionId"]);
                //    Resolution1.Resolution = Convert.ToString(dataSetResolution.Tables[0].Rows[i]["Resolution"]);
                //    ListResolution.Add(Resolution1);
                //}
                //addLines.mResolution = ListResolution;

                if (RecordId != null && RecordId != "")
                {
                    addLines.RecordId = RecordId;
                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@Id", RecordId));
                    DataSet dataSetRecord = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLineRecordByRecordId", methodParameter);
                    if (dataSetRecord != null)
                    {
                        if (dataSetRecord.Tables[0].Rows.Count > 0)
                        {
                            addLines.SrNo1 = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["SrNo1"]);
                            addLines.SrNo2 = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["SrNo2"]);
                            addLines.SrNo3 = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["SrNo3"]);
                            addLines.ConcernedEventId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["ConcernedEventId"]);
                            addLines.PageBreak = Convert.ToBoolean(dataSetRecord.Tables[0].Rows[0]["PageBreak"]);
                            addLines.ConcernedCommitteeId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["CommitteeId"]);
                            List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                            methodParameter1.Add(new KeyValuePair<string, string>("@CommitteeId", addLines.ConcernedCommitteeId));
                            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommitteeInfoById", methodParameter1);

                            addLines.CommitteeReportTitle = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["CommitteeTitle"]);
                            addLines.ConcernedCommitteeRepTypId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["CommitteeRepTypId"]);
                            addLines.ConcernedDeptId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["DeptId"]);

                            //addLines.ConcernedDeptId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["ConcernedDeptId"]);
                            //addLines.ConcernedMemberId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["ConcernedMemberId"]);
                            //addLines.ConcernedRuleId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["ConcernedRuleId"]);
                            addLines.TextLOB = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["TextLOB"]);
                            addLines.BillTextNumber = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["BillNo"]);



                            if (addLines.BillTextNumber != null && addLines.BillTextNumber != "")
                            {
                                string[] bill = Regex.Split(addLines.BillTextNumber, @"of");//addLines.BillTextNumber.Split("o");
                                string part1 = bill[0].Trim();
                                string part2 = bill[1].Trim();
                                addLines.BillTextNumber = part1;
                                addLines.BillNumberYear = Convert.ToInt32(part2);

                                mBills Updatemodel = new mBills();
                                Updatemodel.BillNo = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["BillNo"]);
                                Updatemodel = (mBills)Helpers.Helper.ExecuteService("BillPaperLaid", "GetBillInfoByBillNo", Updatemodel);
                                if (Updatemodel != null)
                                {
                                    addLines.BillTextTitle = Updatemodel.BillTitle;
                                }

                            }
                            //addLines.ConcernedEventName = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["ConcernedEventName"]);
                            //string extra = "\r\n";
                            //if (addLines.ConcernedEventName.Contains(extra))
                            //{
                            //    string remove = addLines.ConcernedEventName.Trim();
                            //    addLines.ConcernedEventName = remove;
                            //}
                        }
                    }

                }
                //  addLines.RecordId="20";
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            return PartialView("PartialLineEntryForm", addLines);
        }

        /// <summary>
        /// it will save the Line Record information according to the LOB id
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="addLines"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        /// 
        //public string CreateLobFolder()
        //{
        //    List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
        //    methodParameter = new List<KeyValuePair<string, string>>();

        //    DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
        //    string CurrentAssembly = "";
        //    string Currentsession = "";

        //    for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
        //    {
        //        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
        //        {
        //            CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
        //        }
        //        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
        //        {
        //            Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
        //        }
        //    }
        //    string Sessiondate = "";
        //    if (TempData["addLines"] != null)
        //    {

        //        string addLines = (string)TempData["addLines"];
        //        Sessiondate = addLines;

        //    }
        //    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
        //    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

        //    string DOC = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";
        //    DirectoryInfo Dir = new DirectoryInfo(DOC);
        //            if (!Dir.Exists)
        //            {
        //                Dir.Create();
        //            }
        //    string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";

        //    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
        //    return directory;



        //}
        [ValidateAntiForgeryToken]
        public ActionResult SaveLines(AddLOBModel addLines, object file)
        {


            try
            {
                if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }

                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

                string Sdate = addLines.Sessiondate;
                Sdate = Sdate.Replace('/', ' ');
                //TempData["addLines"] = Sdate;

                if (addLines.RecordId == null || addLines.RecordId == "")
                {
                    if (addLines.LOBId == null || addLines.LOBId == "")
                    {
                        methodParameter = new List<KeyValuePair<string, string>>();
                        DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectMaxLOBId", methodParameter);
                        if (dataSetLOB != null)
                        {
                            if (Convert.ToString(dataSetLOB.Tables[0].Rows[0][0]) != "")
                            {
                                addLines.LOBId = Convert.ToString(Convert.ToInt16(dataSetLOB.Tables[0].Rows[0][0]) + 1);
                            }
                            else
                            {
                                addLines.LOBId = "1";
                            }
                        }
                    }
                }

                methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBId", addLines.LOBId));
                string extra = "<b>";
                if (addLines.TextLOB.Contains(extra))
                {
                    string a = addLines.TextLOB.Replace("<b>", "<strong>");
                    string b = a.Replace("</b>", "</strong>&nbsp;");
                    // addLines.TextLOB = a;
                    addLines.TextLOB = b;
                }
                if (addLines.AssemblyId != null && addLines.AssemblyId != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@AssemblyId", addLines.AssemblyId));
                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyID", addLines.AssemblyId));
                    DataSet dataSetAssembly = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectAssemblyInfoById", methodParameter1);

                    if (dataSetAssembly != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@AssemblyName", Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyName"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@AssemblyNameLocal", Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyNameLocal"])));
                    }
                }

                if (addLines.SessionId != null && addLines.SessionId != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionId", addLines.SessionId));
                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionId", addLines.SessionId));
                    methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyID", addLines.AssemblyId));
                    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionInfoById", methodParameter1);

                    if (dataSet != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionName", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionName"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionNameLocal"])));
                    }
                }
                //   string Sessiondate="";
                if (addLines.Sessiondate != null && addLines.Sessiondate != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", addLines.Sessiondate));
                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionDate", addLines.Sessiondate));
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
                    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionDateInfo", methodParameter1);

                    if (dataSet != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionDateLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionDateLocal"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionTime", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionTime"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionTimeLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionTimeLocal"])));
                    }
                }

                if (addLines.SrNo1 != null && addLines.SrNo1 != "Select" && addLines.SrNo1 != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", addLines.SrNo1));
                }
                if (addLines.SrNo2 != null && addLines.SrNo2 != "Select" && addLines.SrNo2 != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", addLines.SrNo2));
                }
                if (addLines.SrNo3 != null && addLines.SrNo3 != "Select" && addLines.SrNo3 != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo3", addLines.SrNo3));
                }

                if (addLines.TextLOB != null && addLines.TextLOB != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@TextLOB", addLines.TextLOB));
                }

                #region Commented code for Concerned Data

                //if (addLines.ConcernedMemberId != null && addLines.ConcernedMemberId != "0")
                //{
                //    //// getting member Info
                //    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMemberId", addLines.ConcernedMemberId));


                //    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                //    methodParameter1.Add(new KeyValuePair<string, string>("@MemberId", addLines.ConcernedMemberId));
                //    methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyId", addLines.AssemblyId));
                //    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectMemberInfoById", methodParameter1);

                //    if (dataSet != null)
                //    {
                //        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMemberDesignationId", Convert.ToString(dataSet.Tables[0].Rows[0]["MinisteryID"])));
                //        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMemberDesignation", Convert.ToString(dataSet.Tables[0].Rows[0]["MinisteryName"])));
                //        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMemberDesignationLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["MinisteryNameLocal"])));
                //        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMemberName", Convert.ToString(dataSet.Tables[0].Rows[0]["Name"])));
                //        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMemberNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["Name_Hindi"])));
                //    }
                //}

                //if (addLines.ConcernedDeptId != null && addLines.ConcernedDeptId != "0")
                //{
                //    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedDeptId", addLines.ConcernedDeptId));

                //    ////Getting Dept Information
                //    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                //    methodParameter1.Add(new KeyValuePair<string, string>("@DeptId", addLines.ConcernedDeptId));
                //    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDepartmentInfoById", methodParameter1);

                //    if (dataSet != null)
                //    {
                //        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedDeptName", Convert.ToString(dataSet.Tables[0].Rows[0]["deptname"])));
                //        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedDeptNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["deptname_hindi"])));
                //    }

                //    ////Getting Dept Minister
                //    List<KeyValuePair<string, string>> methodParameter2 = new List<KeyValuePair<string, string>>();
                //    methodParameter2.Add(new KeyValuePair<string, string>("@DeptId", addLines.ConcernedDeptId));
                //    DataSet dataSet1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDeptMinister", methodParameter2);

                //    if (dataSet1 != null)
                //    {
                //        ////Getting minister Info
                //        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMinisterId", Convert.ToString(dataSet1.Tables[0].Rows[0]["MemberID"])));
                //        List<KeyValuePair<string, string>> methodParameter3 = new List<KeyValuePair<string, string>>();
                //        methodParameter3.Add(new KeyValuePair<string, string>("@MemberId", Convert.ToString(dataSet1.Tables[0].Rows[0]["MemberID"])));
                //        methodParameter3.Add(new KeyValuePair<string, string>("@AssemblyId", addLines.AssemblyId));
                //        DataSet dataSet2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectMemberInfoById", methodParameter3);

                //        if (dataSet2 != null)
                //        {
                //            methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMinisterDesignationId", Convert.ToString(dataSet2.Tables[0].Rows[0]["MinisteryID"])));
                //            methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMinisterDesignation", Convert.ToString(dataSet2.Tables[0].Rows[0]["MinisteryName"])));
                //            methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMinisterDesignationLocal", Convert.ToString(dataSet2.Tables[0].Rows[0]["MinisteryNameLocal"])));
                //            methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMinisterName", Convert.ToString(dataSet2.Tables[0].Rows[0]["Name"])));
                //            methodParameter.Add(new KeyValuePair<string, string>("@ConcernedMinisterNameLocal", Convert.ToString(dataSet2.Tables[0].Rows[0]["Name_Hindi"])));
                //        }
                //    }



                //}



                //if (addLines.ConcernedRuleId != null && addLines.ConcernedRuleId != "0")
                //{
                //    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedRuleId", addLines.ConcernedRuleId));


                //    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                //    methodParameter1.Add(new KeyValuePair<string, string>("@RuleId", addLines.ConcernedRuleId));
                //    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectRuleInfoById", methodParameter1);

                //    if (dataSet != null)
                //    {
                //        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedRuleName", Convert.ToString(dataSet.Tables[0].Rows[0]["RuleName"])));
                //        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedRuleNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["RuleNameLocal"])));
                //    }
                //}

                #endregion
                if (addLines.ConcernedCommitteeId != null && addLines.ConcernedCommitteeId != "0")
                {


                    /// Getting Committee Info
                    //  List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    //  methodParameter1.Add(new KeyValuePair<string, string>("@CommitteeId", addLines.ConcernedCommitteeId));
                    // DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommitteeInfoById", methodParameter1);

                    //if (dataSet != null)
                    //{
                    //   // methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeName", Convert.ToString(dataSet.Tables[0].Rows[0]["CommitteeName"])));
                    //    //methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["deptname_hindi"])));
                    //}
                    // string CommId = addLines.ConcernedCommitteeId;
                    //string CommTitle = addLines.CommitteeReportTitle;

                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeId", addLines.ConcernedCommitteeId));
                    methodParameter.Add(new KeyValuePair<string, string>("@CommitteeReportTitle", addLines.CommitteeReportTitle));

                }

                if (addLines.ConcernedCommitteeRepTypId != null && addLines.ConcernedCommitteeRepTypId != "0")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeRepTypId", addLines.ConcernedCommitteeRepTypId));

                    //List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    //methodParameter1.Add(new KeyValuePair<string, string>("@ReportTypeId", addLines.ConcernedCommitteeRepTypId));
                    //DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommitteeRepTypeInfoById", methodParameter1);

                    //if (dataSet != null)
                    //{
                    //   methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeRepTypId", Convert.ToString(dataSet.Tables[0].Rows[0]["ReportTypeId"])));
                    //   methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeRepTypName", Convert.ToString(dataSet.Tables[0].Rows[0]["ReportTypeName"])));
                    //}

                }

                if (addLines.ConcernedDeptId != null && addLines.ConcernedDeptId != "0")
                {



                    ////Getting Dept Information
                    //List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    //methodParameter1.Add(new KeyValuePair<string, string>("@DeptId", addLines.ConcernedDeptId));
                    //DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDepartmentInfoById1", methodParameter1);

                    // if (dataSet != null)
                    // {
                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedDeptId", addLines.ConcernedDeptId));
                    // methodParameter.Add(new KeyValuePair<string, string>("@ConcernedDeptName", Convert.ToString(dataSet.Tables[0].Rows[0]["deptname"])));
                    //}
                    // methodParameter.Add(new KeyValuePair<string, string>("@ConcernedDeptName", addLines.ConcernedDeptName));
                }





                if (addLines.ConcernedEventId != null && addLines.ConcernedEventId != "0")
                {
                    //// getting member Info
                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedEventId", addLines.ConcernedEventId));

                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@EventId", addLines.ConcernedEventId));
                    // methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyId", addLines.AssemblyId));
                    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEventInfoById", methodParameter1);

                    if (dataSet != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedEventName", Convert.ToString(dataSet.Tables[0].Rows[0]["EventName"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedEventNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["EventNameLocal"])));
                    }
                }

                methodParameter.Add(new KeyValuePair<string, string>("@EVoting", Convert.ToString(addLines.IsEVoting)));
                methodParameter.Add(new KeyValuePair<string, string>("@PageBreak", Convert.ToString(addLines.PageBreak)));

                DataSet dataSetLine = null;
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                if ((addLines.PaperLaidIdTemp != null && addLines.PaperLaidIdTemp != "" && addLines.PaperLaidId != null && addLines.PaperLaidId != "") && (addLines.BillTextNumber != null && addLines.BillTextNumber != "" && addLines.BillNumberYear != null && addLines.BillNumberYear != 0))
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                {
                    if (addLines.BillTextNumber != null)
                    {
                        addLines.BillTextNumber = addLines.BillTextNumber.Trim() + " of " + addLines.BillNumberYear;
                        methodParameter.Add(new KeyValuePair<string, string>("@BillNo", Convert.ToString(addLines.BillTextNumber)));
                    }
                }
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                if ((addLines.PaperLaidIdTemp == null || addLines.PaperLaidIdTemp == "" && addLines.PaperLaidId == null || addLines.PaperLaidId == "") && (addLines.BillTextNumber != null && addLines.BillTextNumber != "" && addLines.BillNumberYear != null && addLines.BillNumberYear != 0))
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                {
                    if (addLines.BillTextNumber != null)
                    {
                        addLines.BillTextNumber = addLines.BillTextNumber.Trim() + " of " + addLines.BillNumberYear;
                        methodParameter.Add(new KeyValuePair<string, string>("@BillNo", Convert.ToString(addLines.BillTextNumber)));
                    }
                }
                if (addLines.RecordId == null || addLines.RecordId == "")
                {
                    if (addLines.TextLOB != "" && addLines.TextLOB != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", Utility.CurrentSession.UserID));
                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedDate", Convert.ToString(System.DateTime.Now)));
                        dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLOBLineRecord", methodParameter);
                    }
                }
                else
                {
                    if (addLines.TextLOB != "" && addLines.TextLOB != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));
                        methodParameter.Add(new KeyValuePair<string, string>("@Id", addLines.RecordId));
                        dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBLineRecord", methodParameter);
                    }
                }

                if (addLines.PaperLaidIdTemp != null && addLines.PaperLaidIdTemp != "" && addLines.PaperLaidId != null && addLines.PaperLaidId != "")
                {

                    tPaperLaidTemp PaperLaid = new tPaperLaidTemp();
                    PaperLaid.PaperLaidTempId = Convert.ToInt16(addLines.PaperLaidIdTemp);

                    PaperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", PaperLaid);
                    string Location = "";



                    methodParameter = new List<KeyValuePair<string, string>>();

                    DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                    string CurrentAssembly = "";
                    string Currentsession = "";

                    for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                        {
                            CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                        }
                        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                        {
                            Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                        }
                    }

                    string Sessiondate = addLines.Sessiondate;
                    Sessiondate = Sessiondate.Replace('/', ' ');

                    string sourcePath = PaperLaid.SignedFilePath;
                    Location = PaperLaid.SignedFilePath;
                    int index = Location.LastIndexOf(@"/");
                    Location = Location.Substring(index + 1);
                    sourcePath = sourcePath.Substring(0, index);

                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";
                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }

                    string sourcedirectory = FileSettings.SettingValue + sourcePath;

                    string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                    if (addLines.SrNo1 != null && addLines.SrNo1 != "Select")
                    {
                        fileName = addLines.SrNo1;
                    }
                    if (addLines.SrNo2 != null && addLines.SrNo2 != "Select")
                    {
                        fileName = fileName + "_" + addLines.SrNo2;
                    }
                    if (addLines.SrNo3 != null && addLines.SrNo3 != "Select")
                    {
                        fileName = fileName + "_" + addLines.SrNo3;
                    }
                    fileName = fileName + ".pdf";
                    string sourceFile = System.IO.Path.Combine(sourcedirectory, Location);
                    string destFile = System.IO.Path.Combine(directory, fileName);

                    // To copy a folder's contents to a new location: 
                    // Create a new target folder, if necessary. 
                    //if (!System.IO.Directory.Exists(directory))
                    //{
                    //    System.IO.Directory.CreateDirectory(directory);
                    //}

                    // To copy a file to another location and  
                    // overwrite the destination file if it already exists.
                    System.IO.File.Copy(sourceFile, destFile, true);

                    string recordId = addLines.RecordId;

                    if (recordId == null || recordId == "")
                    {
                        recordId = Convert.ToString(dataSetLine.Tables[0].Rows[0][0]);
                    }
                    string savepath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";
                    string File = System.IO.Path.Combine(savepath, fileName);

                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@PDFLocation", File));
                    methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", addLines.LOBId));
                    methodParameter.Add(new KeyValuePair<string, string>("@Id", recordId));

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentPDF", methodParameter);
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    provider = new CultureInfo("fr-FR");
                    var Date = DateTime.ParseExact(addLines.Sessiondate, "d", provider);

                    tPaperLaidV PaperLaidV = new tPaperLaidV();
                    PaperLaidV.DesireLayingDate = Convert.ToDateTime(Date);
                    PaperLaidV.PaperLaidId = Convert.ToInt16(addLines.PaperLaidId);
                    PaperLaidV.LOBRecordId = Convert.ToInt16(recordId);
                    PaperLaidV.DesireLayingDate = Convert.ToDateTime(Date);
                    PaperLaidV.LOBPaperTempId = Convert.ToInt16(addLines.PaperLaidIdTemp);
                    Helpers.Helper.ExecuteService("PaperLaid", "UpdateLOBRecordIdIntPaperLaidVS", PaperLaidV);


                    if (addLines.BillTextNumber != "" && addLines.BillTextNumber != null)
                    {
                        UpdateBillsEntry(addLines, sourceFile);
                    }
                }
                if ((addLines.PaperLaidIdTemp == null || addLines.PaperLaidIdTemp == "" && addLines.PaperLaidId == null || addLines.PaperLaidId == ""))
                {


                    methodParameter = new List<KeyValuePair<string, string>>();

                    DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                    string CurrentAssembly = "";
                    string Currentsession = "";

                    for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                        {
                            CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                        }
                        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                        {
                            Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                        }
                    }

                    string Sessiondate = addLines.Sessiondate;
                    Sessiondate = Sessiondate.Replace('/', ' ');

                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";

                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }

                    string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                    if (addLines.SrNo1 != null && addLines.SrNo1 != "Select")
                    {
                        fileName = addLines.SrNo1;
                    }
                    if (addLines.SrNo2 != null && addLines.SrNo2 != "Select")
                    {
                        fileName = fileName + "_" + addLines.SrNo2;
                    }
                    if (addLines.SrNo3 != null && addLines.SrNo3 != "Select")
                    {
                        fileName = fileName + "_" + addLines.SrNo3;
                    }
                    fileName = fileName + ".pdf";
                    // string sourceFile = System.IO.Path.Combine(sourcedirectory, Location);
                    //string directory = FolderCreate();
                    string sourceFile = System.IO.Path.Combine(directory, fileName);


                    if (addLines.BillTextNumber != "" && addLines.BillTextNumber != null)
                    {
                        UpdateBillsEntry(addLines, sourceFile);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // var data=true;
            var res2 = Json(addLines.LOBId, JsonRequestBehavior.AllowGet);
            return res2;
            // Now you can do whatever you want with your model
        }
        /// <summary>
        /// added by dharmendra for interting data to mBills
        /// </summary>
        /// <param name="ConcernedEventId"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        public void UpdateBillsEntry(AddLOBModel model, string sourceFile)
        {
            List<KeyValuePair<string, string>> methodParameterAss = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameterAss);
            string CurrAssembly = "";

            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                {
                    CurrAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }

            }
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            DataSet dataSetLine = null;
            //BillWorkModel mdl = new BillWorkModel();
            mBills Updatemodel = new mBills();
            mEvent eventmodel = new mEvent();
            string EventID = model.ConcernedEventId;

            string space1 = " ";
            string[] Split1 = model.BillTextNumber.Split();
            Split1 = Split1.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            string b_textNumber1 = Split1[0] + space1 + Split1[1] + space1 + Split1[2];
            Updatemodel.BillNo = b_textNumber1;




            methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@EventId", EventID));
            dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_GetEventNameByID", methodParameter);
            //eventmodel = (mEvent)Helpers.Helper.ExecuteService("BillPaperLaid", "GetEventNameByID", eventmodel);
            // model.ConcernedEventName = eventmodel.EventName;
            bool checkBillNoExist = (bool)Helpers.Helper.ExecuteService("BillPaperLaid", "CheckBillNUmberExist", Updatemodel);
            if (checkBillNoExist == true)
            {
                if (EventID == "5" || EventID == "72")
                {
                    string DBurl = "/IntroductionPdf/";
                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string serverpath = System.IO.Path.Combine(FileSettings.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/IntroductionPdf/");
                    if (!System.IO.Directory.Exists(serverpath))
                    {
                        System.IO.Directory.CreateDirectory(serverpath);
                    }

                    string LastName = model.BillTextNumber.Replace(" ", String.Empty);
                    string ChangeIntroFileName = LastName + "_" + "Introduction.pdf";

                    string destFile = System.IO.Path.Combine(serverpath, ChangeIntroFileName);
                    string SaveIntroPath = "/IntroductionPdf/" + ChangeIntroFileName;
                    //System.IO.File.Copy(SourceFile, path, true);
                    System.IO.File.Copy(sourceFile, destFile, true);
                    Updatemodel.BillTitle = model.BillTextTitle;
                    Updatemodel.IntroductionFilePath = System.IO.Path.Combine(DBurl, ChangeIntroFileName);
                    Updatemodel.Type = "Introduce";
                    Helpers.Helper.ExecuteService("BillPaperLaid", "UpdatemBillsRecord", Updatemodel);
                }
                else if (EventID == "36")
                {
                    string DBurl = "/PassedPdf/";
                    //string url = "/Bills/PassedPdf/";
                    //string Destinationdirectory = Server.MapPath(url);
                    //if (!System.IO.Directory.Exists(Destinationdirectory))
                    //{
                    //    System.IO.Directory.CreateDirectory(Destinationdirectory);
                    //}
                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string serverpath = System.IO.Path.Combine(FileSettings.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/PassedPdf/");
                    if (!System.IO.Directory.Exists(serverpath))
                    {
                        System.IO.Directory.CreateDirectory(serverpath);
                    }
                    string LastName = model.BillTextNumber.Replace(" ", String.Empty);
                    string ChangeIntroFileName = LastName + "_" + "Passed.pdf";
                    string destFile = System.IO.Path.Combine(serverpath, ChangeIntroFileName);
                    System.IO.File.Copy(sourceFile, destFile, true);
                    Updatemodel.BillTitle = model.BillTextTitle;
                    Updatemodel.PassingFilePath = System.IO.Path.Combine(DBurl, ChangeIntroFileName);
                    Updatemodel.Type = "Passed";
                    Helpers.Helper.ExecuteService("BillPaperLaid", "UpdatemBillsRecord", Updatemodel);
                }
                else if (EventID == "31")
                {
                    string DBurl = "/AssentedPdf/";
                    //string url = "/Bills/AssentedPdf/";
                    //string Destinationdirectory = Server.MapPath(url);
                    //if (!System.IO.Directory.Exists(Destinationdirectory))
                    //{
                    //    System.IO.Directory.CreateDirectory(Destinationdirectory);
                    //}
                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string serverpath = System.IO.Path.Combine(FileSettings.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/AssentedPdf/");
                    if (!System.IO.Directory.Exists(serverpath))
                    {
                        System.IO.Directory.CreateDirectory(serverpath);
                    }
                    string LastName = model.BillTextNumber.Replace(" ", String.Empty);
                    string ChangeIntroFileName = LastName + "_" + "Assented.pdf";
                    string destFile = System.IO.Path.Combine(serverpath, ChangeIntroFileName);
                    System.IO.File.Copy(sourceFile, destFile, true);
                    Updatemodel.BillTitle = model.BillTextTitle;
                    Updatemodel.AccentedFilePath = System.IO.Path.Combine(DBurl, ChangeIntroFileName);
                    Updatemodel.Type = "Assent";
                    Helpers.Helper.ExecuteService("BillPaperLaid", "UpdatemBillsRecord", Updatemodel);
                }


            }
            else
            {
                DataSet dataSetLine2 = null;
                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                dataSetLine2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GelLastSNo", methodParameter1);
                if (EventID == "5" || EventID == "72")
                {
                    string DBurl = "/IntroductionPdf/";
                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string serverpath = System.IO.Path.Combine(FileSettings.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/IntroductionPdf/");
                    if (!System.IO.Directory.Exists(serverpath))
                    {
                        System.IO.Directory.CreateDirectory(serverpath);
                    }

                    string LastName = model.BillTextNumber.Replace(" ", String.Empty);
                    string ChangeIntroFileName = LastName + "_" + "Introduction.pdf";

                    string destFile = System.IO.Path.Combine(serverpath, ChangeIntroFileName);
                    string SaveIntroPath = "/IntroductionPdf/" + ChangeIntroFileName;
                    System.IO.File.Copy(sourceFile, destFile, true);

                    Updatemodel.IntroductionFilePath = System.IO.Path.Combine(DBurl, ChangeIntroFileName);
                    Updatemodel.BillTitle = model.BillTextTitle;




                    //string space =" ";
                    //string[] Split = model.BillTextNumber.Split();
                    //Split = Split.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    //string b_textNumber = Split[0] + space + Split[1] + space + Split[2];
                    //if (Updatemodel.BillNo.Contains(space))
                    //{

                    //}


                    // Updatemodel.BillNo = b_textNumber1;
                    Updatemodel.IntroductionDate = DateTime.Now;
                    Updatemodel.SNO = Convert.ToInt32(dataSetLine2.Tables[0].Rows[0]["ID"].ToString());
                    Updatemodel.AssemblyId = Convert.ToInt32(model.AssemblyId);
                    Updatemodel.SessionId = Convert.ToInt32(model.SessionId);

                    Helpers.Helper.ExecuteService("BillPaperLaid", "InsertmBillsRecord", Updatemodel);
                }
                else if (EventID == "36")
                {
                    string DBurl = "/PassedPdf/";
                    //string url = "/Bills/PassedPdf/";
                    //string Destinationdirectory = Server.MapPath(url);
                    //if (!System.IO.Directory.Exists(Destinationdirectory))
                    //{
                    //    System.IO.Directory.CreateDirectory(Destinationdirectory);
                    //}
                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string serverpath = System.IO.Path.Combine(FileSettings.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/PassedPdf/");
                    if (!System.IO.Directory.Exists(serverpath))
                    {
                        System.IO.Directory.CreateDirectory(serverpath);
                    }
                    string LastName = model.BillTextNumber.Replace(" ", String.Empty);
                    string ChangeIntroFileName = LastName + "_" + "Passed.pdf";
                    string destFile = System.IO.Path.Combine(serverpath, ChangeIntroFileName);
                    //string destFile = System.IO.Path.Combine(Destinationdirectory, ChangeIntroFileName);
                    System.IO.File.Copy(sourceFile, destFile, true);
                    Updatemodel.PassingFilePath = System.IO.Path.Combine(DBurl, ChangeIntroFileName);
                    Updatemodel.BillTitle = model.BillTextTitle;



                    Updatemodel.BillNo = b_textNumber1;
                    Updatemodel.PassingDate = DateTime.Now;
                    Updatemodel.SNO = Convert.ToInt32(dataSetLine2.Tables[0].Rows[0]["ID"].ToString());
                    Updatemodel.AssemblyId = Convert.ToInt32(model.AssemblyId);
                    Updatemodel.SessionId = Convert.ToInt32(model.SessionId);
                    Helpers.Helper.ExecuteService("BillPaperLaid", "InsertmBillsRecord", Updatemodel);
                }
                else if (EventID == "31")
                {
                    string DBurl = "/AssentedPdf/";
                    //string url = "/Bills/AssentedPdf/";
                    //string Destinationdirectory = Server.MapPath(url);
                    //if (!System.IO.Directory.Exists(Destinationdirectory))
                    //{
                    //    System.IO.Directory.CreateDirectory(Destinationdirectory);
                    //}
                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string serverpath = System.IO.Path.Combine(FileSettings.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/AssentedPdf/");
                    if (!System.IO.Directory.Exists(serverpath))
                    {
                        System.IO.Directory.CreateDirectory(serverpath);
                    }
                    string LastName = model.BillTextNumber.Replace(" ", String.Empty);
                    string ChangeIntroFileName = LastName + "_" + "Assented.pdf";
                    string destFile = System.IO.Path.Combine(serverpath, ChangeIntroFileName);
                    //string destFile = System.IO.Path.Combine(Destinationdirectory, ChangeIntroFileName);
                    System.IO.File.Copy(sourceFile, destFile, true);
                    Updatemodel.AccentedFilePath = System.IO.Path.Combine(DBurl, ChangeIntroFileName);
                    Updatemodel.BillTitle = model.BillTextTitle;
                    Updatemodel.BillNo = b_textNumber1;
                    Updatemodel.AssesntDate = DateTime.Now;
                    Updatemodel.SNO = Convert.ToInt32(dataSetLine2.Tables[0].Rows[0]["ID"].ToString());
                    Updatemodel.AssemblyId = Convert.ToInt32(model.AssemblyId);
                    Updatemodel.SessionId = Convert.ToInt32(model.SessionId);
                    Helpers.Helper.ExecuteService("BillPaperLaid", "InsertmBillsRecord", Updatemodel);
                }


            }


            //return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "getPaperLaidListByEventId"

        public ActionResult PartialShowPapers(string ConcernedEventId)
        {
            ConcernedEventId = Sanitizer.GetSafeHtmlFragment(ConcernedEventId);
            tPaperLaidV PaperLaid = new tPaperLaidV();

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            DataSet dataSetDept = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "SelectDepartment", methodParameter);
            List<SBL.DomainModel.Models.Department.mDepartment> DepartmentList = new List<SBL.DomainModel.Models.Department.mDepartment>();
            SBL.DomainModel.Models.Department.mDepartment Department2 = new SBL.DomainModel.Models.Department.mDepartment();
            Department2.deptId = "0";
            Department2.deptname = "Select";

            DepartmentList.Add(Department2);

            for (int i = 0; i < dataSetDept.Tables[0].Rows.Count; i++)
            {
                SBL.DomainModel.Models.Department.mDepartment Department = new SBL.DomainModel.Models.Department.mDepartment();
                Department.deptId = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptId"]);
                //Department.deptname = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptname"]);
                string Dept = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptname"]);

                if (Dept.Contains(">"))
                {

                    Department.deptname = Dept.Replace(">", "");
                }
                else
                {
                    Department.deptname = Dept;
                }
                DepartmentList.Add(Department);
            }

            List<KeyValuePair<string, string>> mD = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", mD);
#pragma warning disable CS0219 // The variable 'CurrentAssembly' is assigned but its value is never used
            string CurrentAssembly = "";
#pragma warning restore CS0219 // The variable 'CurrentAssembly' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Currentsession' is assigned but its value is never used
            string Currentsession = "";
#pragma warning restore CS0219 // The variable 'Currentsession' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'OldSession' is assigned but its value is never used
            string OldSession = "";
#pragma warning restore CS0219 // The variable 'OldSession' is assigned but its value is never used
            string PreviousSession = "";
            String PreviousAssembly = "";

            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {
                //if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                //{
                //    CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                //}
                //if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                //{
                //    Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                //}
                //if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "OldSession")
                //{
                //    OldSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                //}
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "PreviousSession")
                {
                    PreviousSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "PreviousAssembly")
                {
                    PreviousAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
            }


            PaperLaid.mDepartmentList = DepartmentList;

            List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
            mParameter.Add(new KeyValuePair<string, string>("@Assembly", PreviousAssembly));
            mParameter.Add(new KeyValuePair<string, string>("@session", PreviousSession));
            DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "LastSessionDates", mParameter);
            string Date = Convert.ToString(dataDate.Tables[0].Rows[0]["SessionDate"]);
            DateTime endDate = Convert.ToDateTime(Date);
            if (endDate == DateTime.Now)
            {
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");

                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;

                string LastSessionDate = StartDay;

            }
            else
            {
                endDate = endDate.AddDays(1);
                string StartDay = (endDate).ToString("dd/MM/yyyy");

                //string[] dda = StartDay.Split('/');
                //string StartDayis = dda[1] + '/' + dda[0] + '/' + dda[2];

                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");

                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;





                string LastSessionDate = StartDay;

            }
            ConcernedEventId = Sanitizer.GetSafeHtmlFragment(ConcernedEventId);
            PaperLaid.EventId = Convert.ToInt16(ConcernedEventId);
            List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
            methodParameter1.Add(new KeyValuePair<string, string>("@EventId", ConcernedEventId));

            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEventInfoById", methodParameter1);

            if (dataSet != null)
            {
                PaperLaid.EventName = Convert.ToString(dataSet.Tables[0].Rows[0]["EventName"]);
                string extra = "\r\n";
                if (PaperLaid.EventName.Contains(extra))
                {
                    string remove = PaperLaid.EventName.Trim();
                    PaperLaid.EventName = remove;
                }
            }
            return PartialView(PaperLaid);
        }
        ////added by dharmendra

        public ActionResult PartialShowIntimationPapers(string ConcernedEventId)
        {
            ConcernedEventId = Sanitizer.GetSafeHtmlFragment(ConcernedEventId);
            tPaperLaidV PaperLaid = new tPaperLaidV();

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            DataSet dataSetDept = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "SelectDepartment", methodParameter);
            List<SBL.DomainModel.Models.Department.mDepartment> DepartmentList = new List<SBL.DomainModel.Models.Department.mDepartment>();
            SBL.DomainModel.Models.Department.mDepartment Department2 = new SBL.DomainModel.Models.Department.mDepartment();
            Department2.deptId = "0";
            Department2.deptname = "Select";

            DepartmentList.Add(Department2);

            for (int i = 0; i < dataSetDept.Tables[0].Rows.Count; i++)
            {
                SBL.DomainModel.Models.Department.mDepartment Department = new SBL.DomainModel.Models.Department.mDepartment();
                Department.deptId = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptId"]);
                //Department.deptname = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptname"]);
                string Dept = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptname"]);

                if (Dept.Contains(">"))
                {

                    Department.deptname = Dept.Replace(">", "");
                }
                else
                {
                    Department.deptname = Dept;
                }
                DepartmentList.Add(Department);
            }

            List<KeyValuePair<string, string>> mD = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", mD);

            string PreviousSession = "";
            String PreviousAssembly = "";

            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {

                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "PreviousSession")
                {
                    PreviousSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "PreviousAssembly")
                {
                    PreviousAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
            }


            PaperLaid.mDepartmentList = DepartmentList;

            List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
            mParameter.Add(new KeyValuePair<string, string>("@Assembly", PreviousAssembly));
            mParameter.Add(new KeyValuePair<string, string>("@session", PreviousSession));
            DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "LastSessionDates", mParameter);
            string Date = Convert.ToString(dataDate.Tables[0].Rows[0]["SessionDate"]);
            DateTime endDate = Convert.ToDateTime(Date);
            if (endDate == DateTime.Now)
            {
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");

                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;

                string LastSessionDate = StartDay;

            }
            else
            {
                endDate = endDate.AddDays(1);
                string StartDay = (endDate).ToString("dd/MM/yyyy");

                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");

                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;

                string LastSessionDate = StartDay;

            }
            ConcernedEventId = Sanitizer.GetSafeHtmlFragment(ConcernedEventId);
            PaperLaid.EventId = Convert.ToInt16(ConcernedEventId);
            List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
            methodParameter1.Add(new KeyValuePair<string, string>("@EventId", ConcernedEventId));

            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEventInfoById", methodParameter1);

            if (dataSet != null)
            {
                PaperLaid.EventName = Convert.ToString(dataSet.Tables[0].Rows[0]["EventName"]);
                string extra = "\r\n";
                if (PaperLaid.EventName.Contains(extra))
                {
                    string remove = PaperLaid.EventName.Trim();
                    PaperLaid.EventName = remove;
                }
            }
            return PartialView("PartialShowIntimationPapers", PaperLaid);
        }
        public ActionResult PatialgetIntimationPaperLaidListByEventId(string ConcernedEventId, string PaperId, string ConcernedDeptId, string Datefrom, string Dateto)
        {
            string AssemblyCode = null;
            string SessionCode = null;
            DateTime? DateFrm = new DateTime();
            DateTime? DateTo = new DateTime();
            string EventId = "";
            string PaperType = "";
            string DepartmentId = "";
            tPaperLaidV model = new tPaperLaidV();
            if (string.IsNullOrEmpty(ConcernedEventId))
            {
                EventId = null;
            }
            else
            {
                EventId = ConcernedEventId;
            }
            if (string.IsNullOrEmpty(PaperId))
            {
                PaperType = null;
            }
            else
            {
                PaperType = PaperId;
            }
            if (string.IsNullOrEmpty(ConcernedDeptId) || ConcernedDeptId == "0")
            {
                DepartmentId = null;
            }
            else
            {
                DepartmentId = ConcernedDeptId;
            }
            ////Getting the Current assembly AND session.
            List<KeyValuePair<string, string>> methodParameters = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameters);
            //string CurrentAssembly = "";
            //string Currentsession = "";


            //for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            //{
            //    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
            //    {
            //        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
            //    }
            //    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
            //    {
            //        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
            //    }
            //}
            //AssemblyCode = CurrentAssembly;
            //SessionCode = Currentsession;
            if (!string.IsNullOrEmpty(Datefrom) && !string.IsNullOrEmpty(Dateto))
            {

                DateFrm = new DateTime(Convert.ToInt32(Datefrom.Split('/')[2]), Convert.ToInt32(Datefrom.Split('/')[1]), Convert.ToInt32(Datefrom.Split('/')[0]));
                DateTo = new DateTime(Convert.ToInt32(Dateto.Split('/')[2]), Convert.ToInt32(Dateto.Split('/')[1]), Convert.ToInt32(Dateto.Split('/')[0]));
                DataSet SummaryDataSet = new DataSet();
                List<tPaperLaidV> _list = new List<tPaperLaidV>();

                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@AssemblyCode", AssemblyCode));
                methodParameter.Add(new KeyValuePair<string, string>("@SessionCode", SessionCode));
                methodParameter.Add(new KeyValuePair<string, string>("@DateFrom", DateFrm.ToString()));
                methodParameter.Add(new KeyValuePair<string, string>("@DateTo", DateTo.ToString()));
                methodParameter.Add(new KeyValuePair<string, string>("@EventId", EventId));
                methodParameter.Add(new KeyValuePair<string, string>("@PaperType", PaperType));
                methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", DepartmentId));

                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[GetFilteredPaperLaidRecord]", methodParameter);
                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {

                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        tPaperLaidV mel = new tPaperLaidV();
                        mel.DeparmentName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["DeparmentName"]);
                        mel.Title = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["Title"]);
                        mel.PaperLaidId = Convert.ToInt16(SummaryDataSet.Tables[0].Rows[i]["PaperLaidId"]);
                        mel.actualFilePath = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SignedFilePath"]);
                        mel.PaperLaidTempId = Convert.ToInt16(SummaryDataSet.Tables[0].Rows[i]["PaperLaidTempId"]);
                        mel.FileVersion = Convert.ToInt16(SummaryDataSet.Tables[0].Rows[i]["Version"]);
                        mel.DeptActivePaperId = Convert.ToInt16(SummaryDataSet.Tables[0].Rows[i]["DeptActivePaperId"]);
                        string ddd = SummaryDataSet.Tables[0].Rows[i]["DeptSubmittedDate"].ToString();
                        if (!string.IsNullOrEmpty(ddd))
                        {
                            mel.DeptSubmittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["DeptSubmittedDate"]);
                        }
                        _list.Add(mel);
                    }
                }
                model.ListtPaperLaidV = _list;
                model.ListtPaperLaidV = model.ListtPaperLaidV.OrderBy(z => z.PaperLaidId).GroupBy(x => x.PaperLaidId).Select(y => y.First()).ToList();
                List<tPaperLaidV> testlist1 = new List<tPaperLaidV>();
                testlist1 = _list.OrderByDescending(s => s.PaperLaidTempId).ToList();
                foreach (var val in model.ListtPaperLaidV)
                {
                    List<tPaperLaidV> _Sublist = new List<tPaperLaidV>();
                    foreach (var val1 in testlist1)
                    {
                        if (val1.PaperLaidId == val.PaperLaidId)
                        {
                            tPaperLaidV mel = new tPaperLaidV();
                            mel.PaperLaidId = val1.PaperLaidId;
                            mel.actualFilePath = val1.actualFilePath;
                            mel.PaperLaidTempId = val1.PaperLaidTempId;
                            mel.FileVersion = val1.FileVersion;
                            mel.DeptActivePaperId = val1.DeptActivePaperId;
                            mel.DeptSubmittedDate = val1.DeptSubmittedDate;
                            _Sublist.Add(mel);
                        }
                    }
                    val.ListtPaperLaidVSub = _Sublist;

                }

            }

            // return PartialView("_getListEventId", PaperLaid);
            return PartialView("PatialgetIntimationPaperLaidListByEventId", model);
        }
        public ActionResult getPaperLaidListByEventId(string ConcernedEventId, string Priority, string PaperId, string ConcernedDeptId, string Datefrom, string Dateto)
        {

            DateTime DateFromis = DateTime.Now;
            DateTime DateTois = DateTime.Now;
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            tPaperLaidV PaperLaid = new tPaperLaidV();
            ConcernedEventId = Sanitizer.GetSafeHtmlFragment(ConcernedEventId);

            if (ConcernedEventId == "3" || ConcernedEventId == "4")
            {
                PaperLaid.ListtPaperLaidV = null;
            }
            else
            {
                if (PaperId.Equals("Pending Papers"))
                {
                    PaperLaid.IsLaid = false;
                }
                else if (PaperId.Equals("Paper Laid"))
                {
                    PaperLaid.IsLaid = true;
                }
                else
                {
                    //  PaperLaid.IsLaid = false || true;
                }

                PaperLaid.DepartmentId = ConcernedDeptId;
                PaperLaid.EventId = Convert.ToInt16(ConcernedEventId);

                if (Priority == "ALL" && PaperId == "Pending Papers" && ConcernedDeptId == "0")
                {
                    PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByDeptId", PaperLaid);
                }

                else if (Priority == "ALL")
                {
                    PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByEventId", PaperLaid);
                }
                else if (Priority == "NotSet")
                {
                    PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByEventIdWithNoPriority", PaperLaid);
                }
                else
                {
                    PaperLaid.PaperlaidPriority = Convert.ToInt16(Priority);
                    PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByEventIdAndPriorty", PaperLaid);

                }

                tPaperLaidTemp paperLaid1 = new tPaperLaidTemp();
                string DeptSubDate = Convert.ToDateTime(paperLaid1.DeptSubmittedDate).ToString("dd/MM/yyyy");
                DateTime DeptSubDateis = Convert.ToDateTime(DeptSubDate);
                //string DateFrom = string.Format("{0:dd/MM/yyyy}", Datefrom);
                // string DateTo = string.Format("{0:dd/MM/yyyy}", Dateto);
                // DateFromis = Convert.ToDateTime(Datefrom, CultureInfo.GetCultureInfo("en-IN"));

                DateFromis = Convert.ToDateTime(Datefrom, CultureInfo.GetCultureInfo("en-IN"));
                DateTois = Convert.ToDateTime(Dateto, CultureInfo.GetCultureInfo("en-IN"));


                //   int result = DateTime.Compare(DateFromis, DateTois);



                if (PaperLaid.ListtPaperLaidV != null)
                {

                    for (int i = 0; i < PaperLaid.ListtPaperLaidV.Count; i++)
                    {

                        SBL.DomainModel.Models.Event.mEvent Event = new SBL.DomainModel.Models.Event.mEvent();
                        Event.EventId = PaperLaid.ListtPaperLaidV[i].EventId;
                        Event = (SBL.DomainModel.Models.Event.mEvent)Helpers.Helper.ExecuteService("Events", "GetEventsById", Event);
                        PaperLaid.ListtPaperLaidV[i].EventName = Event.EventName;
                        PaperLaid.EventName = Event.EventName;
                        tPaperLaidTemp paperLaid = new tPaperLaidTemp();
                        paperLaid.PaperLaidTempId = (long)PaperLaid.ListtPaperLaidV[i].DeptActivePaperId;
                        paperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", paperLaid);
                        if (paperLaid != null)
                        {
                            if (ConcernedEventId == "8")
                            {
                                PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.CommtSubmittedDate).Date;
                                PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.CommtSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                            }
                            else
                            {
                                PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                                PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                            }
                           // PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                           // PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                            PaperLaid.ListtPaperLaidV[i].actualFilePath = paperLaid.SignedFilePath;
                        }


                    }
                }

            }
            List<tPaperLaidV> _list = new List<tPaperLaidV>();
            if (PaperLaid.ListtPaperLaidV != null)
            {

                if (DateFromis.Date == DateTois.Date)
                {
                    foreach (var item in PaperLaid.ListtPaperLaidV)
                    {
                        if (item.DeptSubmittedDate == DateFromis.Date)
                        {
                            _list.Add(item);
                        }
                    }

                }


                else
                {
                    foreach (var item in PaperLaid.ListtPaperLaidV)
                    {

                        //item.MinisterSubmittedDate == Datefrom && item.MinisterSubmittedDate == Dateto)
                        if (item.DeptSubmittedDate >= DateFromis.Date && item.DeptSubmittedDate <= DateTois.Date)
                        {

                            _list.Add(item);
                        }



                    }
                }
            }

            PaperLaid.ListtPaperLaidV = _list;


            // return PartialView("_getListEventId", PaperLaid);
            return PartialView("PatialgetPaperLaidListByEventId", PaperLaid);
        }

        public ActionResult GetListByPriorityIdAndPaperId(string ConcernedEventId, string Priority, string PaperId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            tPaperLaidV PaperLaid = new tPaperLaidV();
            AdminLOB admnLOB = new AdminLOB();
            ConcernedEventId = Sanitizer.GetSafeHtmlFragment(ConcernedEventId);
            if (ConcernedEventId == "3" || ConcernedEventId == "4")
            {
                PaperLaid.ListtPaperLaidV = null;
            }
            else
            {
                PaperLaid.EventId = Convert.ToInt16(ConcernedEventId);

                if (Priority == "ALL")
                {
                    if (PaperId == "ALL")
                    {
                        PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByEventId", PaperLaid);
                    }
                    else if (PaperId == "Pending Papers")
                    {
                        PaperLaid.IsLaid = false;
                        PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperByPaperIdAndPriority", PaperLaid);

                    }
                    else        //paper laid
                    {
                        PaperLaid.IsLaid = true;
                        PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByPaperIdAndPriority", PaperLaid);

                    }
                    //  PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByEventId", PaperLaid);
                }


                else if (Priority == "NotSet")
                {
                    if (PaperId == "ALL")
                    {
                        PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByEventIdWithNoPriority", PaperLaid);
                    }
                    else if (PaperId == "Pending Papers")
                    {
                        PaperLaid.IsLaid = false;
                        PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperByPaperIdWithNoPriority", PaperLaid);

                    }
                    else        //paper laid
                    {
                        PaperLaid.IsLaid = true;
                        PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByPaperIdAndWithNoPriority", PaperLaid);

                    }
                    // PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByEventIdWithNoPriority", PaperLaid);
                }
                else
                {
                    PaperLaid.PaperlaidPriority = Convert.ToInt16(Priority);

                    if (PaperId == "ALL")
                    {
                        PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByEventIdAndPriorty", PaperLaid);
                    }
                    else if (PaperId == "Pending Papers")
                    {
                        //PaperLaid.PaperlaidPriority = Convert.ToInt16(Priority);
                        PaperLaid.IsLaid = false;
                        PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperByPaperIdAndPriority", PaperLaid);

                    }
                    else        //paper laid
                    {
                        //PaperLaid.PaperlaidPriority = Convert.ToInt16(Priority);
                        PaperLaid.IsLaid = true;
                        PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByPaperIdAndPriority", PaperLaid);

                    }

                }


                for (int i = 0; i < PaperLaid.ListtPaperLaidV.Count; i++)
                {

                    SBL.DomainModel.Models.Event.mEvent Event = new SBL.DomainModel.Models.Event.mEvent();
                    Event.EventId = PaperLaid.ListtPaperLaidV[i].EventId;
                    Event = (SBL.DomainModel.Models.Event.mEvent)Helpers.Helper.ExecuteService("Events", "GetEventsById", Event);
                    PaperLaid.ListtPaperLaidV[i].EventName = Event.EventName;
                    PaperLaid.EventName = Event.EventName;
                    tPaperLaidTemp paperLaid = new tPaperLaidTemp();
                    paperLaid.PaperLaidTempId = (long)PaperLaid.ListtPaperLaidV[i].MinisterActivePaperId;
                    paperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", paperLaid);
                    PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy");
                    PaperLaid.ListtPaperLaidV[i].actualFilePath = paperLaid.SignedFilePath;

                }
            }
            return PartialView("PatialgetPaperLaidListByEventId", PaperLaid);
        }


        ///////////added by dharmendra for bill////////////////////////

        public ActionResult getPartialBillPaperLaidListByEventId(string ConcernedEventId, string Priority)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            tPaperLaidV PaperLaid = new tPaperLaidV();
            ConcernedEventId = Sanitizer.GetSafeHtmlFragment(ConcernedEventId);
            if (ConcernedEventId == "3" || ConcernedEventId == "4")
            {
                PaperLaid.ListtPaperLaidV = null;
            }
            else
            {
                PaperLaid.EventId = Convert.ToInt16(ConcernedEventId);

                if (Priority == "ALL")
                {
                    PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByEventId", PaperLaid);
                }
                else if (Priority == "NotSet")
                {
                    PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByEventIdWithNoPriority", PaperLaid);
                }
                else
                {
                    PaperLaid.PaperlaidPriority = Convert.ToInt16(Priority);
                    PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByEventIdAndPriorty", PaperLaid);
                }

                List<SelectListItem> years = new List<SelectListItem>();
                int currentYear = DateTime.Now.Year;

                for (int i = currentYear - 70; i < currentYear; i++)
                {
                    SelectListItem year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };

                    years.Add(year);
                }
                for (int i = currentYear; i < currentYear + 5; i++)
                {
                    SelectListItem year = new SelectListItem();
                    if (i == DateTime.Now.Year)
                    {
                        year = new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = true };
                    }
                    else
                    {
                        year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };
                    }
                    years.Add(year);
                }

                //if (item.BillNo != "")
                //{
                //    model.BillNo = item.BillNo;
                //    string[] parts = model.BillNo.Split('o', 'f');
                //    string fist = parts[0].ToString();
                //    string Second = parts[2].ToString();
                //    model.BillYearS = Convert.ToInt32(Second);
                //    model.BillNoS = Convert.ToInt32(fist);
                //}
                for (int i = 0; i < PaperLaid.ListtPaperLaidV.Count; i++)
                {

                    SBL.DomainModel.Models.Event.mEvent Event = new SBL.DomainModel.Models.Event.mEvent();
                    Event.EventId = PaperLaid.ListtPaperLaidV[i].EventId;
                    Event = (SBL.DomainModel.Models.Event.mEvent)Helpers.Helper.ExecuteService("Events", "GetEventsById", Event);
                    PaperLaid.ListtPaperLaidV[i].EventName = Event.EventName;
                    PaperLaid.EventName = Event.EventName;
                    PaperLaid.yearList = years;
                    tPaperLaidTemp paperLaid = new tPaperLaidTemp();
                    paperLaid.PaperLaidTempId = (long)PaperLaid.ListtPaperLaidV[i].MinisterActivePaperId;
                    paperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", paperLaid);
                    PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.MinisterSubmittedDate).ToString("dd/MM/yyyy");
                    PaperLaid.ListtPaperLaidV[i].actualFilePath = paperLaid.SignedFilePath;

                }
            }
            return PartialView("PartialBillPaperLaidListByEventId", PaperLaid);
        }

        ////////////////end//////////////////////////



        /// <summary>
        /// get and show paperLaid for a select Line
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="Location"></param>
        /// <returns></returns>
        public ActionResult ViewPaperLaid(string PaperLaidTempId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            PaperLaidTempId = Sanitizer.GetSafeHtmlFragment(PaperLaidTempId);
            if (PaperLaidTempId != null && PaperLaidTempId != "")
            {
                tPaperLaidTemp PaperLaid = new tPaperLaidTemp();
                PaperLaid.PaperLaidTempId = Convert.ToInt16(PaperLaidTempId);

                PaperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", PaperLaid);
                string Location = "";

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                Location = System.IO.Path.Combine(FileSettings.SettingValue + PaperLaid.SignedFilePath);
                //int indx = addLOBModel1.PDFLocation.IndexOf("~");
                //if (indx != -1)
                //{

                //    //string filstring = addLOBModel1.PDFLocation.TrimStart('~');
                //    Location = System.IO.Path.Combine(FileSettings.SettingValue + PaperLaid.SignedFilePath);

                //}
                //foreach (var item in PaperLaid.ListPaperLaidTemp)
                //{
                //    Location = item.FilePath;
                //}
                //Location = PaperLaid.FilePath+PaperLaid.FileName;
                //Location = PaperLaid.SignedFilePath;
                return File(Location, "application/pdf");
                // return RedirectToAction("LoadAction", "Module", new { area = "", ActionName = "LoadPDF", ControllerName = "PDFViewer", ModuleName = "PDFViewer", PDFPath = path });
            }
            else
            {
                return null;
            }
        }



        public JsonResult GetFileNamePaperLaid(string PaperLaidTempId, int PaperLaidId)
        {
            PaperLaidTempId = Sanitizer.GetSafeHtmlFragment(PaperLaidTempId);
            if (PaperLaidTempId != null && PaperLaidTempId != "")
            {
                tPaperLaidTemp PaperLaid = new tPaperLaidTemp();
                //mBills tBills = new mBills();
                PaperLaid.PaperLaidTempId = Convert.ToInt16(PaperLaidTempId);
                PaperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", PaperLaid);

                tBillRegister PaperLaidNumber = new tBillRegister();
                PaperLaidNumber.PaperLaidId = PaperLaidId;
                PaperLaidNumber = (tBillRegister)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidNumberById", PaperLaidNumber);
                string BillNumber = "";
                //string BillTitle="";
                if (PaperLaidNumber != null)
                {
                    BillNumber = PaperLaidNumber.BillNumber;
                    Session["BillNumber"] = BillNumber;
                }
                //tBills.BillNo = BillNumber;
                //tBills = (mBills)Helpers.Helper.ExecuteService("PaperLaid", "GetBillDetails", tBills);
                //if (tBills != null)
                //{
                //    BillTitle = tBills.BillTitle;
                //}
                string Location = "";

                //foreach (var item in PaperLaid.ListPaperLaidTemp)
                //{
                //    Location = item.SignedFilePath;
                //}
                Location = PaperLaid.SignedFilePath;
                int index = Location.LastIndexOf(@"/");
                Location = Location.Substring(index + 1);
                string Result = Location + "," + BillNumber;
                return Json(Result, JsonRequestBehavior.AllowGet);
                // return RedirectToAction("LoadAction", "Module", new { area = "", ActionName = "LoadPDF", ControllerName = "PDFViewer", ModuleName = "PDFViewer", PDFPath = path });
            }
            else
            {
                return null;
            }
        }

        public JsonResult GetPaperLaidDescription(string PaperLaidId)
        {
            PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);
            if (PaperLaidId != null && PaperLaidId != "")
            {
                tPaperLaidV PaperLaid = new tPaperLaidV();
                PaperLaid.PaperLaidId = Convert.ToInt16(PaperLaidId);

                PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidById", PaperLaid);
                string Description = "";

                foreach (var item in PaperLaid.ListtPaperLaidV)
                {
                    Description = item.Description;
                }


                return Json(Description, JsonRequestBehavior.AllowGet);
                // return RedirectToAction("LoadAction", "Module", new { area = "", ActionName = "LoadPDF", ControllerName = "PDFViewer", ModuleName = "PDFViewer", PDFPath = path });
            }
            else
            {
                return null;
            }
        }




        #endregion

        #region SrNoChecks

        /// <summary>
        /// Check weather given SrNo exist or not
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <param name="SrNo1"></param>
        /// <param name="SrNo2"></param>
        /// <param name="SrNo3"></param>
        /// <returns></returns>
        public JsonResult checkSrNo(string LOBId, string SrNo1, string SrNo2, string SrNo3)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            bool result = true;

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
            if (SrNo2 != "Select" && SrNo2 != null && SrNo2 != "")
            {
                methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));
            }
            if (SrNo3 != "Select" && SrNo3 != null && SrNo3 != "")
            {
                methodParameter.Add(new KeyValuePair<string, string>("@SrNo3", SrNo3));
            }
            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_GetPRecordBySrNo", methodParameter);
            if (dataSet != null)
            {
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    result = false;
                }
            }

            var res2 = Json(result, JsonRequestBehavior.AllowGet);
            return res2;
        }

        /// <summary>
        /// Validate the given Sr.No combination
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <param name="SrNo1"></param>
        /// <param name="SrNo2"></param>
        /// <param name="SrNo3"></param>
        /// <returns></returns>
        public ActionResult ValidateRecord(string LOBId, string SrNo1, string SrNo2, string SrNo3)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            bool result = false;
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
            if (SrNo2 != "Select")
            {
                methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));
            }
            if (SrNo3 != "Select")
            {
                methodParameter.Add(new KeyValuePair<string, string>("@SrNo3", SrNo3));
            }
            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_GetRecordInfoByIdSrNo", methodParameter);
            if (dataSet != null)
            {
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    result = true;
                }
            }

            var res2 = Json(result, JsonRequestBehavior.AllowGet);
            return res2;
        }


        #endregion

        #region PartialLineGrid


        /// <summary>
        /// Will get the list of line in a LOB PartialView
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialLineGrid(string LOBId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            AddLOBModel addLOBModel = new AddLOBModel();
            try
            {
                if (LOBId != null && LOBId != "")
                {
                    List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                    methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                    methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                    DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDraftLOBByIdForLineRecord", methodParameter);
                    List<AddLOBModel> ListLOB = new List<AddLOBModel>();
                    for (int i = 0; i < dataSetLOB.Tables[0].Rows.Count; i++)
                    {
                        AddLOBModel addLOBModel1 = new AddLOBModel();
                        string SrNo1 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo1"]);
                        if (SrNo1 != "")
                        {
                            addLOBModel1.SrNo11 = SrNo1;
                            SrNo1 = SrNo1 + ".";
                        }
                        string SrNo2 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo2"]);
                        if (SrNo2 != "")
                        {
                            addLOBModel1.SrNo22 = SrNo2;
                            SrNo2 = "(" + SrNo2 + ")";
                        }
                        string SrNo3 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo3"]);
                        if (SrNo3 != "")
                        {
                            addLOBModel1.SrNo33 = SrNo3;
                            SrNo3 = "(" + ToRoman(Convert.ToInt16(SrNo3)) + ")";
                        }
                        addLOBModel1.SrNo1 = SrNo1;
                        addLOBModel1.SrNo2 = SrNo2;
                        addLOBModel1.SrNo3 = SrNo3;
                        addLOBModel1.Sessiondate = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SessionDate"]);
                        addLOBModel1.RecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                        addLOBModel1.TextLOB = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TextLOB"]);
                        addLOBModel1.PDFLocation = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PDFLocation"]);
                        //addLOBModel1.IsEVoting = Convert.ToBoolean(dataSetLOB.Tables[0].Rows[i]["IsEVoting"]);
                        if (Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PageBreak"]) != "")
                        {
                            addLOBModel1.PageBreak = Convert.ToBoolean(dataSetLOB.Tables[0].Rows[i]["PageBreak"]);
                        }
                        else
                        {
                            addLOBModel1.PageBreak = false;
                        }
                        if (i == 0)
                        {
                            addLOBModel.ResultCount = Convert.ToInt32(Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TotalRecords"]));
                        }
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                        int indx = addLOBModel1.PDFLocation.IndexOf("~");
                        if (indx != -1)
                        {

                            string filstring = addLOBModel1.PDFLocation.TrimStart('~');
                            addLOBModel1.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + filstring);

                        }
                        else if (addLOBModel1.PDFLocation != null && addLOBModel1.PDFLocation != "")
                        {
                            addLOBModel1.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + addLOBModel1.PDFLocation);
                        }


                        ListLOB.Add(addLOBModel1);
                    }
                    addLOBModel.LOBList = ListLOB;

                    if (PageNumber != null && PageNumber != "")
                    {
                        addLOBModel.PageNumber = Convert.ToInt32(PageNumber);
                    }
                    else
                    {
                        addLOBModel.PageNumber = Convert.ToInt32("1");
                    }

                    if (RowsPerPage != null && RowsPerPage != "")
                    {
                        addLOBModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                    }
                    else
                    {
                        addLOBModel.RowsPerPage = Convert.ToInt32("15");
                    }
                    if (PageNumber != null && PageNumber != "")
                    {
                        addLOBModel.selectedPage = Convert.ToInt32(PageNumber);
                    }
                    else
                    {
                        addLOBModel.selectedPage = Convert.ToInt32("1");
                    }

                    if (loopStart != null && loopStart != "")
                    {
                        addLOBModel.loopStart = Convert.ToInt32(loopStart);
                    }
                    else
                    {
                        addLOBModel.loopStart = Convert.ToInt32("1");
                    }

                    if (loopEnd != null && loopEnd != "")
                    {
                        addLOBModel.loopEnd = Convert.ToInt32(loopEnd);
                    }
                    else
                    {
                        addLOBModel.loopEnd = Convert.ToInt32("5");
                    }



                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("PartialLineGrid", addLOBModel);
        }
        //public ActionResult PartialLineGrid(string LOBId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        //{
        //    if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
        //    AddLOBModel addLOBModel = new AddLOBModel();
        //    try
        //    {
        //        if (LOBId != null && LOBId != "")
        //        {
        //            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
        //            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
        //            methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
        //            methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
        //            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDraftLOBByIdForLineRecord", methodParameter);
        //            List<AddLOBModel> ListLOB = new List<AddLOBModel>();
        //            for (int i = 0; i < dataSetLOB.Tables[0].Rows.Count; i++)
        //            {
        //                AddLOBModel addLOBModel1 = new AddLOBModel();
        //                string SrNo1 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo1"]);
        //                if (SrNo1 != "")
        //                {
        //                    addLOBModel1.SrNo11 = SrNo1;
        //                    SrNo1 = SrNo1 + ".";
        //                }
        //                string SrNo2 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo2"]);
        //                if (SrNo2 != "")
        //                {
        //                    addLOBModel1.SrNo22 = SrNo2;
        //                    SrNo2 = "(" + SrNo2 + ")";
        //                }
        //                string SrNo3 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo3"]);
        //                if (SrNo3 != "")
        //                {
        //                    addLOBModel1.SrNo33 = SrNo3;
        //                    SrNo3 = "(" + ToRoman(Convert.ToInt16(SrNo3)) + ")";
        //                }
        //                addLOBModel1.SrNo1 = SrNo1;
        //                addLOBModel1.SrNo2 = SrNo2;
        //                addLOBModel1.SrNo3 = SrNo3;
        //                addLOBModel1.Sessiondate = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SessionDate"]);
        //                addLOBModel1.RecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
        //                addLOBModel1.TextLOB = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TextLOB"]);
        //                addLOBModel1.PDFLocation = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PDFLocation"]);
        //                //addLOBModel1.IsEVoting = Convert.ToBoolean(dataSetLOB.Tables[0].Rows[i]["IsEVoting"]);
        //                if (Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PageBreak"]) != "")
        //                {
        //                    addLOBModel1.PageBreak = Convert.ToBoolean(dataSetLOB.Tables[0].Rows[i]["PageBreak"]);
        //                }
        //                else
        //                {
        //                    addLOBModel1.PageBreak = false;
        //                }
        //                if (i == 0)
        //                {
        //                    addLOBModel.ResultCount = Convert.ToInt32(Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TotalRecords"]));
        //                }
        //                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
        //                int indx = addLOBModel1.PDFLocation.IndexOf("~");
        //                if (indx != -1)
        //                {

        //                    string filstring = addLOBModel1.PDFLocation.TrimStart('~');
        //                    addLOBModel1.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + filstring);

        //                }
        //                else if (addLOBModel1.PDFLocation != null && addLOBModel1.PDFLocation != "")
        //                {
        //                    addLOBModel1.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + addLOBModel1.PDFLocation);
        //                }


        //                ListLOB.Add(addLOBModel1);
        //            }
        //            addLOBModel.LOBList = ListLOB;

        //            if (PageNumber != null && PageNumber != "")
        //            {
        //                addLOBModel.PageNumber = Convert.ToInt32(PageNumber);
        //            }
        //            else
        //            {
        //                addLOBModel.PageNumber = Convert.ToInt32("1");
        //            }

        //            if (RowsPerPage != null && RowsPerPage != "")
        //            {
        //                addLOBModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
        //            }
        //            else
        //            {
        //                addLOBModel.RowsPerPage = Convert.ToInt32("15");
        //            }
        //            if (PageNumber != null && PageNumber != "")
        //            {
        //                addLOBModel.selectedPage = Convert.ToInt32(PageNumber);
        //            }
        //            else
        //            {
        //                addLOBModel.selectedPage = Convert.ToInt32("1");
        //            }

        //            if (loopStart != null && loopStart != "")
        //            {
        //                addLOBModel.loopStart = Convert.ToInt32(loopStart);
        //            }
        //            else
        //            {
        //                addLOBModel.loopStart = Convert.ToInt32("1");
        //            }

        //            if (loopEnd != null && loopEnd != "")
        //            {
        //                addLOBModel.loopEnd = Convert.ToInt32(loopEnd);
        //            }
        //            else
        //            {
        //                addLOBModel.loopEnd = Convert.ToInt32("5");
        //            }



        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return PartialView("PartialLineGrid_1", addLOBModel);
        //}
        #endregion

        #region DeleteLineRecord

        /// <summary>
        /// Delete Line record for a LOB 
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="RecordId"></param>
        /// <returns></returns>
        public JsonResult DeleteLineRecord(string SessionDate, string SrNo1, string SrNo2, string SrNo3)
        {

            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string Message = "";
            try
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@sessiondate", SessionDate));
                DataSet dataSetLine = null;


                if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) == "" && Convert.ToString(SrNo3) == "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_DeleteLineAboveSrNo1", methodParameter);

                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    string filepath = "";

                    if (dataSetLine != null)
                    {
                        Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["AssemblyId"]);
                        Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["SessionId"]);

                    }
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (SNo1 != "" && SNo2 != "" && SNo3 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            else if (SNo1 != "" && SNo2 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            else
                            {
                                if (FileExists(directory + SNo1 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.DeletedSuccessfully;
                }
                else if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) != "" && Convert.ToString(SrNo3) == "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_DeleteLineAboveSrNo2", methodParameter);
                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    string filepath = "";
                    if (dataSetLine != null)
                    {
                        Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["AssemblyId"]);
                        Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["SessionId"]);

                    }
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (SNo1 != "" && SNo2 != "" && SNo3 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            else
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                              
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.DeletedSuccessfully;
                }
                else if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) != "" && Convert.ToString(SrNo3) != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo3", SrNo3));

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_DeleteLineAboveSrNo3", methodParameter);

                    string PdfPath = "";
                    string filepath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    if (dataSetLine != null)
                    {
                        Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["AssemblyId"]);
                        Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["SessionId"]);

                    }
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                            {
                                System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                            }
                            if (FileExists(Tempdirectory + FileName) == true)
                            {
                                System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                            }
                            else
                            {
                                filepath = "";
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }

                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.DeletedSuccessfully;
                }
                else
                {
                    Message = Resources.LOB.UnableToDelete;
                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                Message = Resources.LOB.UnableToDelete;
            }

            var res2 = Json(Message, JsonRequestBehavior.AllowGet);
            return res2;
        }
        public bool FileExists(string path)
        {
            string curFile = @"" + path; ;
            bool valu = System.IO.File.Exists(curFile) ? true : false;
            return valu;
        }
        #endregion

        #region "GenerateXMLlob"
        public ActionResult GenerateXMLlob(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            //  string test1 = CreateQuesXML("2", "20-12-2013");
            // string test2 = CreateQuesXML("1", "20-12-2013");
            string test = CreateXML(LOBId, "Draft");
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            methodParameter.Add(new KeyValuePair<string, string>("@DraftLOBxmlLocation", test));

            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDraftLOBxmlPathLOB", methodParameter);
            return File(test, "application/xml", "DraftLOBxml.xml");
        }
        #endregion

        #region "GenerateXMLApprovedlob"
        public ActionResult GenerateXMLApprovedlob(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string test = CreateXML(LOBId, "Approved");


            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            methodParameter.Add(new KeyValuePair<string, string>("@LOBxmlLocation", test));

            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBxmlPathLOB", methodParameter);
            return File(test, "application/xml", "LOBFile.xml");
        }
        #endregion

        #region "GenerateXMLSQuestions"
        public ActionResult GenerateXMLSQuestions(string SessionDate, string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            if (SessionDate != null)
            {
                string test = CreateQuesXML("1", Convert.ToDateTime(SessionDate).ToString("dd-MM-yyyy"));


                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                methodParameter.Add(new KeyValuePair<string, string>("@SQuestionsxmlLocation", test));

                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateSxmlPathLOB", methodParameter);
                return File(test, "application/xml", "QuestingSFile.xml");
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region "GenerateXMLUSQuestions"
        public ActionResult GenerateXMLUSQuestions(string SessionDate, string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            if (SessionDate != null)
            {
                string test = CreateQuesXML("2", Convert.ToDateTime(SessionDate).ToString("dd-MM-yyyy"));


                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                methodParameter.Add(new KeyValuePair<string, string>("@USQuestionsxmlLocation", test));

                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateUSxmlPathLOB", methodParameter);
                return File(test, "application/xml", "QuestingUSFile.xml");
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region "PartialCorrigendumLOB"
        /// <summary>
        /// Will render the PartialCreateLOB PartialView
        /// Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialCorrigendumLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            AddLOBModel AddLOB = new AddLOBModel();
            try
            {



                ////Getting the Current assembly AND session.
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();

                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }
                AddLOB.SessionId = Currentsession;
                AddLOB.AssemblyId = CurrentAssembly;
                //addLines.mDepartmentList = DepartmentList;

                ///getting the assembly Information
                methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@AssemblyID", CurrentAssembly));
                DataSet dataSetAssembly = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectAssemblyInfoById", methodParameter);
                AddLOB.AssemblyName = Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyName"]);


                ///getting the session information
                methodParameter1 = new List<KeyValuePair<string, string>>();
                methodParameter1.Add(new KeyValuePair<string, string>("@SessionId", Currentsession));
                methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyID", CurrentAssembly));
                DataSet dataSetSession = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionInfoById", methodParameter1);

                List<mSession> ListSession = new List<mSession>();

                mSession Session1 = new mSession();
                Session1.SessionDate = "Select";
                ListSession.Add(Session1);

                for (int i = 0; i < dataSetSession.Tables[0].Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        AddLOB.SessionName = Convert.ToString(dataSetSession.Tables[0].Rows[i]["SessionName"]);
                        AddLOB.SessionTime = Convert.ToString(dataSetSession.Tables[0].Rows[i]["SessionTimeF"]);
                    }
                    mSession Session = new mSession();
                    Session.SessionDate = Convert.ToDateTime(Convert.ToString(dataSetSession.Tables[0].Rows[i]["SessionDate"])).ToString("dd/MM/yyyy");
                    ListSession.Add(Session);
                }
                AddLOB.mSessionList = ListSession;

                if (LOBId != null && LOBId != "")
                {
                    AddLOB.LOBId = LOBId;
                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                    methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", "1"));
                    methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", "10"));
                    DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDraftLOBByIdForLineRecord", methodParameter);
                    if (dataSetLOB != null)
                    {
                        ListSession = new List<mSession>();
                        if (dataSetLOB.Tables[0].Rows.Count > 0)
                        {
                            mSession Session = new mSession();
                            Session.SessionDate = Convert.ToDateTime(Convert.ToString(dataSetLOB.Tables[0].Rows[0]["SessionDate"])).ToString("dd/MM/yyyy");
                            ListSession.Add(Session);
                            AddLOB.Sessiondate = Convert.ToDateTime(Convert.ToString(dataSetLOB.Tables[0].Rows[0]["SessionDate"])).ToString("dd/MM/yyyy");

                            TimeSpan interval = TimeSpan.Parse(Convert.ToString(dataSetLOB.Tables[0].Rows[0]["SessionTime"]));
                            DateTime time = DateTime.Today.Add(interval);
                            string displayTime = time.ToString("hh:mm tt");
                            AddLOB.SessionTime = displayTime;
                        }
                        AddLOB.mSessionList = ListSession;
                    }
                    DraftLOB model = new DraftLOB();
                    model.LOBId = Convert.ToInt16(LOBId);
                    var result = (CorrigendumLOB)Helper.ExecuteService("LOB", "CorrigendumLOBByLOBId", model);
                    if (result != null)
                    {
                        if (result.IsApproved == false && result.IsSubmitted == false)
                        {
                            AddLOB.CorrigendumId = result.CorrigendumId;
                        }
                    }
                }


                return PartialView("PartialCorrigendumLOB", AddLOB);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return PartialView("PartialCorrigendumLOB", AddLOB);
            }
        }
        #endregion

        #region "PartialLineEntryFormCorrigendum"

        public ActionResult PartialLineEntryFormCorrigendum(string LOBId, string RecordId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }

            AddLOBModel addLines = new AddLOBModel();
            try
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

                DataSet dataSetDept = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDepartment", methodParameter);

                List<mDepartment> DepartmentList = new List<mDepartment>();

                mDepartment Department2 = new mDepartment();
                Department2.DeptId = "0";
                Department2.DeptName = "Select";
                DepartmentList.Add(Department2);

                for (int i = 0; i < dataSetDept.Tables[0].Rows.Count; i++)
                {
                    mDepartment Department = new mDepartment();
                    Department.DeptId = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptId"]);
                    Department.DeptName = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptname"]);
                    DepartmentList.Add(Department);
                }
                addLines.mDepartmentList = DepartmentList;

                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetCommittee = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommittee", methodParameter);
                List<mCommittee> CommitteeList = new List<mCommittee>();
                mCommittee Committee2 = new mCommittee();
                Committee2.CommitteeId = "0";
                Committee2.CommitteeName = "Select";
                CommitteeList.Add(Committee2);

                for (int i = 0; i < dataSetCommittee.Tables[0].Rows.Count; i++)
                {
                    mCommittee Committee = new mCommittee();
                    Committee.CommitteeId = Convert.ToString(dataSetCommittee.Tables[0].Rows[i]["CommitteeId"]);
                    Committee.CommitteeName = Convert.ToString(dataSetCommittee.Tables[0].Rows[i]["CommitteeName"]);
                    CommitteeList.Add(Committee);
                }
                addLines.mCommitteeList = CommitteeList;

                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetCommitteeRepTyp = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommitteeRepType", methodParameter);
                List<mCommitteeRepType> CommitteeRepTyp = new List<mCommitteeRepType>();
                mCommitteeRepType CommitteeRepType = new mCommitteeRepType();
                CommitteeRepType.ReportTypeId = "0";
                CommitteeRepType.ReportTypeName = "Select";
                CommitteeRepTyp.Add(CommitteeRepType);

                for (int i = 0; i < dataSetCommitteeRepTyp.Tables[0].Rows.Count; i++)
                {
                    mCommitteeRepType CommitteeRepType1 = new mCommitteeRepType();
                    CommitteeRepType1.ReportTypeId = Convert.ToString(dataSetCommitteeRepTyp.Tables[0].Rows[i]["ReportTypeId"]);
                    CommitteeRepType1.ReportTypeName = Convert.ToString(dataSetCommitteeRepTyp.Tables[0].Rows[i]["ReportTypeName"]);
                    CommitteeRepTyp.Add(CommitteeRepType1);
                }
                addLines.mCommitteeRepTypeList = CommitteeRepTyp;




                methodParameter = new List<KeyValuePair<string, string>>();

                DataSet dataSetEvent = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEventsLOB", methodParameter);

                List<mEvent> EventList = new List<mEvent>();

                mEvent Event1 = new mEvent();
                Event1.EventId = "0";
                Event1.EventName = "Select";
                EventList.Add(Event1);

                for (int i = 0; i < dataSetEvent.Tables[0].Rows.Count; i++)
                {
                    mEvent Event2 = new mEvent();
                    Event2.EventId = Convert.ToString(dataSetEvent.Tables[0].Rows[i]["EventID"]);
                    Event2.EventName = Convert.ToString(dataSetEvent.Tables[0].Rows[i]["EventName"]);
                    EventList.Add(Event2);
                }
                addLines.mEventList = EventList;


                methodParameter = new List<KeyValuePair<string, string>>();

                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }


                methodParameter = new List<KeyValuePair<string, string>>();

                //DataSet dataSetResolution = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectResolution", methodParameter);

                //List<mResolution> ListResolution = new List<mResolution>();

                //for (int i = 0; i < dataSetResolution.Tables[0].Rows.Count; i++)
                //{
                //    mResolution Resolution1 = new mResolution();
                //    Resolution1.ResolutionId = Convert.ToInt16(dataSetResolution.Tables[0].Rows[i]["ResolutionId"]);
                //    Resolution1.Resolution = Convert.ToString(dataSetResolution.Tables[0].Rows[i]["Resolution"]);
                //    ListResolution.Add(Resolution1);
                //}
                //addLines.mResolution = ListResolution;

                List<SelectListItem> years = new List<SelectListItem>();
                int currentYear = DateTime.Now.Year;

                for (int i = currentYear - 70; i < currentYear; i++)
                {
                    SelectListItem year = new SelectListItem
                    {
                        Text =
                            i.ToString(),
                        Value = i.ToString()
                    };

                    years.Add(year);
                }
                for (int i = currentYear; i < currentYear + 5; i++)
                {
                    SelectListItem year = new SelectListItem();
                    if (i == DateTime.Now.Year)
                    {
                        year = new SelectListItem
                        {
                            Text =
                                i.ToString(),
                            Value = i.ToString(),
                            Selected = true
                        };
                    }
                    else
                    {
                        year = new SelectListItem
                        {
                            Text =
                                i.ToString(),
                            Value = i.ToString()
                        };
                    }
                    years.Add(year);
                }
                addLines.yearList = years;
                if (RecordId != null && RecordId != "")
                {
                    addLines.RecordId = RecordId;
                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@Id", RecordId));
                    DataSet dataSetRecord = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLineRecordByRecordId", methodParameter);
                    if (dataSetRecord != null)
                    {
                        if (dataSetRecord.Tables[0].Rows.Count > 0)
                        {
                            addLines.SrNo1 = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["SrNo1"]);
                            addLines.SrNo2 = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["SrNo2"]);
                            addLines.SrNo3 = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["SrNo3"]);

                            // addLines.ConcernedCommitteeId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["CommitteeId"]);
                            //  List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                            //  methodParameter1.Add(new KeyValuePair<string, string>("@CommitteeId", addLines.ConcernedCommitteeId));
                            //  DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommitteeInfoById", methodParameter1);
                            addLines.ConcernedEventId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["ConcernedEventId"]);

                            addLines.CommitteeReportTitle = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["CommitteeTitle"]);
                            addLines.ConcernedCommitteeId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["CommitteeId"]);
                            addLines.ConcernedDeptId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["DeptId"]);
                            addLines.ConcernedCommitteeRepTypId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["CommitteeRepTypId"]);

                            //addLines.ConcernedDeptId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["ConcernedDeptId"]);
                            //addLines.ConcernedMemberId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["ConcernedMemberId"]);
                            //addLines.ConcernedRuleId = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["ConcernedRuleId"]);


                            if (Convert.ToString(dataSetRecord.Tables[0].Rows[0]["PageBreak"]) != "")
                            {

                                addLines.PageBreak = Convert.ToBoolean(dataSetRecord.Tables[0].Rows[0]["PageBreak"]);
                            }
                            else
                            {
                                addLines.PageBreak = false;
                            }




                            //string a =Convert.ToString(dataSetRecord.Tables[0].Rows[0]["PageBreak"]);
                            //if (a == null || a== "")
                            //{
                            //    bool b = false;
                            //    addLines.PageBreak = b;
                            //}
                            //else 
                            //{ 


                            //addLines.PageBreak = Convert.ToBoolean(dataSetRecord.Tables[0].Rows[0]["PageBreak"]);

                            //}
                            addLines.TextLOB = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["TextLOB"]);

                            addLines.BillTextNumber = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["BillNo"]);
                            if (addLines.BillTextNumber != null && addLines.BillTextNumber != "")
                            {
                                string[] bill = Regex.Split(addLines.BillTextNumber, @"of");//addLines.BillTextNumber.Split("o");
                                string part1 = bill[0].Trim();
                                string part2 = bill[1].Trim();
                                addLines.BillTextNumber = part1;
                                addLines.BillNumberYear = Convert.ToInt32(part2);

                                mBills Updatemodel = new mBills();
                                Updatemodel.BillNo = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["BillNo"]);
                                Updatemodel = (mBills)Helpers.Helper.ExecuteService("BillPaperLaid", "GetBillInfoByBillNo", Updatemodel);
                                if (Updatemodel != null)
                                {
                                    addLines.BillTextTitle = Updatemodel.BillTitle;
                                }

                            }

                            //addLines.ConcernedEventName = Convert.ToString(dataSetRecord.Tables[0].Rows[0]["ConcernedEventName"]);

                            //string extra = "\r\n";
                            //if (addLines.ConcernedEventName.Contains(extra))
                            //{
                            //    string remove = addLines.ConcernedEventName.Trim();
                            //    addLines.ConcernedEventName = remove;
                            //}
                        }
                    }

                }


                //  addLines.RecordId="20";


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            return PartialView("PartialLineEntryFormCorrigendum", addLines);
        }

        #endregion

        #region "SaveCorrigendumLines"
        [ValidateAntiForgeryToken]
        public ActionResult SaveCorrigendumLines(AddLOBModel addLines, object file)
        {
            int CorrigendumId = 0;
            try
            {
                if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }

                CorrigendumDetails corrigendumDetails = new CorrigendumDetails();

                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

                string Sdate = addLines.Sessiondate;
                Sdate = Sdate.Replace('/', ' ');
                //TempData["addLines"] = Sdate;

                if (addLines.CorrigendumId == null || addLines.CorrigendumId == 0)
                {
                    CorrigendumLOB corrigendumLOB = new CorrigendumLOB();
                    corrigendumLOB.LOBId = Convert.ToInt16(addLines.LOBId);
                    corrigendumLOB.IsSubmitted = false;
                    corrigendumLOB.IsApproved = false;
                    corrigendumLOB = (CorrigendumLOB)Helper.ExecuteService("LOB", "InsertCorrigendumLOB", corrigendumLOB);
                    CorrigendumId = Convert.ToInt16(corrigendumLOB.CorrigendumId);
                }
                else
                {
                    CorrigendumId = Convert.ToInt16(addLines.CorrigendumId);
                }

                corrigendumDetails.CorrigendumId = CorrigendumId;

                methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBId", addLines.LOBId));
                corrigendumDetails.LOBId = Convert.ToInt16(addLines.LOBId);
                if (addLines.AssemblyId != null && addLines.AssemblyId != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@AssemblyId", addLines.AssemblyId));
                    corrigendumDetails.AssemblyId = Convert.ToInt32(addLines.AssemblyId);

                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyID", addLines.AssemblyId));
                    DataSet dataSetAssembly = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectAssemblyInfoById", methodParameter1);

                    if (dataSetAssembly != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@AssemblyName", Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyName"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@AssemblyNameLocal", Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyNameLocal"])));

                        corrigendumDetails.AssemblyName = Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyName"]);
                        corrigendumDetails.AssemblyNameLocal = Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyNameLocal"]);
                    }

                }

                if (addLines.SessionId != null && addLines.SessionId != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionId", addLines.SessionId));

                    corrigendumDetails.SessionId = Convert.ToInt32(addLines.SessionId);

                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionId", addLines.SessionId));
                    methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyID", addLines.AssemblyId));
                    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionInfoById", methodParameter1);

                    if (dataSet != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionName", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionName"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionNameLocal"])));
                        corrigendumDetails.SessionName = Convert.ToString(dataSet.Tables[0].Rows[0]["SessionName"]);
                        corrigendumDetails.SessionNameLocal = Convert.ToString(dataSet.Tables[0].Rows[0]["SessionNameLocal"]);
                    }

                }
                //   string Sessiondate="";
                if (addLines.Sessiondate != null && addLines.Sessiondate != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", addLines.Sessiondate));


                    if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageCulture == "hi-IN")
                    {

                        CultureInfo provider = CultureInfo.InvariantCulture;
                        provider = new CultureInfo("hi-IN");
                        var Date = DateTime.ParseExact(addLines.Sessiondate, "d", provider);
                        corrigendumDetails.SessionDate = Convert.ToDateTime(Date);

                    }
                    else
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        provider = new CultureInfo("fr-FR");
                        var Date = DateTime.ParseExact(addLines.Sessiondate, "d", provider);
                        corrigendumDetails.SessionDate = Convert.ToDateTime(Date);
                    }



                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionDate", addLines.Sessiondate));
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
                    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionDateInfo", methodParameter1);

                    if (dataSet != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionDateLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionDateLocal"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionTime", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionTime"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionTimeLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionTimeLocal"])));

                        corrigendumDetails.SessionDateLocal = Convert.ToString(dataSet.Tables[0].Rows[0]["SessionDateLocal"]);
                        corrigendumDetails.SessionTime = Convert.ToString(dataSet.Tables[0].Rows[0]["SessionTime"]);
                        corrigendumDetails.SessionTimeLocal = Convert.ToString(dataSet.Tables[0].Rows[0]["SessionTimeLocal"]);

                    }

                }
                if (addLines.SrNo1 != null && addLines.SrNo1 != "Select" && addLines.SrNo1 != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", addLines.SrNo1));
                    corrigendumDetails.SrNo1 = Convert.ToInt16(addLines.SrNo1);
                }
                if (addLines.SrNo2 != null && addLines.SrNo2 != "Select" && addLines.SrNo2 != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", addLines.SrNo2));
                    corrigendumDetails.SrNo2 = Convert.ToInt16(addLines.SrNo2);
                }
                if (addLines.SrNo3 != null && addLines.SrNo3 != "Select" && addLines.SrNo3 != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo3", addLines.SrNo3));
                    corrigendumDetails.SrNo3 = Convert.ToInt16(addLines.SrNo3);
                }

                if (addLines.TextLOB != null && addLines.TextLOB != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@TextLOB", addLines.TextLOB));
                    corrigendumDetails.TextLOB = addLines.TextLOB;
                }

                if (addLines.ConcernedCommitteeId != null && addLines.ConcernedCommitteeId != "0")
                {


                    /// Getting Committee Info
                    //List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    //methodParameter1.Add(new KeyValuePair<string, string>("@CommitteeId", addLines.ConcernedCommitteeId));
                    //DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommitteeInfoById", methodParameter1);

                    //if (dataSet != null)
                    //{
                    //   // methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeName", Convert.ToString(dataSet.Tables[0].Rows[0]["CommitteeName"])));
                    //    //methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["deptname_hindi"])));
                    //}
                    string CommId = addLines.ConcernedCommitteeId;
                    string CommTitle = addLines.CommitteeReportTitle;

                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeId", addLines.ConcernedCommitteeId));
                    methodParameter.Add(new KeyValuePair<string, string>("@CommitteeReportTitle", addLines.CommitteeReportTitle));
                    corrigendumDetails.CommitteeId = Convert.ToInt16(addLines.ConcernedCommitteeId);
                    corrigendumDetails.CommitteeTitle = addLines.CommitteeReportTitle;

                }

                if (addLines.ConcernedCommitteeRepTypId != null && addLines.ConcernedCommitteeRepTypId != "0")
                {
                    //List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    //methodParameter1.Add(new KeyValuePair<string, string>("@ReportTypeId", addLines.ConcernedCommitteeRepTypId));
                    //DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommitteeRepTypeInfoById", methodParameter1);

                    //if (dataSet != null)
                    //{
                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeRepTypId", addLines.ConcernedCommitteeRepTypId));
                    //   methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeRepTypName", Convert.ToString(dataSet.Tables[0].Rows[0]["ReportTypeName"])));
                    corrigendumDetails.CommitteeRepTypId = Convert.ToInt16(addLines.ConcernedCommitteeRepTypId);

                    // }

                }

                if (addLines.ConcernedDeptId != null && addLines.ConcernedDeptId != "0")
                {
                    ////Getting Dept Information
                    //List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    //methodParameter1.Add(new KeyValuePair<string, string>("@DeptId", addLines.ConcernedDeptId));
                    //DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDepartmentInfoById1", methodParameter1);

                    //if (dataSet != null)
                    //{
                    //    //methodParameter.Add(new KeyValuePair<string, string>("@ConcernedDeptId", addLines.ConcernedDeptId));
                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedDeptId", addLines.ConcernedDeptId));
                    //  methodParameter.Add(new KeyValuePair<string, string>("@ConcernedDeptName", Convert.ToString(dataSet.Tables[0].Rows[0]["deptname"])));
                    corrigendumDetails.DeptId = addLines.ConcernedDeptId;
                    // corrigendumDetails.DeptName = Convert.ToString(dataSet.Tables[0].Rows[0]["deptname"]);
                    // }
                    //string DeptId = addLines.ConcernedDeptId;
                    //string DeptName = addLines.ConcernedDeptName;
                    //corrigendumDetails.DeptId = DeptId;
                    //corrigendumDetails.DeptName = DeptName;

                }

                if (addLines.ConcernedEventId != null && addLines.ConcernedEventId != "0")
                {
                    //// getting member Info
                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedEventId", addLines.ConcernedEventId));
                    corrigendumDetails.ConcernedEventId = Convert.ToInt16(addLines.ConcernedEventId);

                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@EventId", addLines.ConcernedEventId));
                    // methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyId", addLines.AssemblyId));
                    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEventInfoById", methodParameter1);

                    if (dataSet != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedEventName", Convert.ToString(dataSet.Tables[0].Rows[0]["EventName"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedEventNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["EventNameLocal"])));
                        corrigendumDetails.ConcernedEventName = Convert.ToString(dataSet.Tables[0].Rows[0]["EventName"]);
                        corrigendumDetails.ConcernedEventNameLocal = Convert.ToString(dataSet.Tables[0].Rows[0]["EventNameLocal"]);

                    }
                }

                methodParameter.Add(new KeyValuePair<string, string>("@EVoting", Convert.ToString(addLines.IsEVoting)));
                methodParameter.Add(new KeyValuePair<string, string>("@PageBreak", Convert.ToString(addLines.PageBreak)));

                corrigendumDetails.IsEVoting = addLines.IsEVoting;
                corrigendumDetails.PageBreak = addLines.PageBreak;

                DataSet dataSetLine = null;

#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                if ((addLines.PaperLaidIdTemp != null && addLines.PaperLaidIdTemp != "" && addLines.PaperLaidId != null && addLines.PaperLaidId != "") && (addLines.BillTextNumber != null && addLines.BillTextNumber != "" && addLines.BillNumberYear != null && addLines.BillNumberYear != 0))
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                {
                    if (addLines.BillTextNumber != null)
                    {

                        addLines.BillTextNumber = addLines.BillTextNumber.Trim() + " of " + addLines.BillNumberYear;
                        methodParameter.Add(new KeyValuePair<string, string>("@BillNo", Convert.ToString(addLines.BillTextNumber)));
                    }
                }
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                if ((addLines.PaperLaidIdTemp == null || addLines.PaperLaidIdTemp == "" && addLines.PaperLaidId == null || addLines.PaperLaidId == "") && (addLines.BillTextNumber != null && addLines.BillTextNumber != "" && addLines.BillNumberYear != null && addLines.BillNumberYear != 0))
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                {
                    if (addLines.BillTextNumber != null)
                    {

                        addLines.BillTextNumber = addLines.BillTextNumber.Trim() + " of " + addLines.BillNumberYear;
                        methodParameter.Add(new KeyValuePair<string, string>("@BillNo", Convert.ToString(addLines.BillTextNumber)));
                    }
                }

                if (addLines.RecordId == null || addLines.RecordId == "")
                {
                    if (addLines.TextLOB != "" && addLines.TextLOB != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", Utility.CurrentSession.UserID));

                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedDate", Convert.ToString(System.DateTime.Now)));
                        corrigendumDetails.CreatedBy = Utility.CurrentSession.UserID;
                        corrigendumDetails.CreatedDate = System.DateTime.Now;

                        dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLOBLineRecord", methodParameter);
                        corrigendumDetails.RecordId = Convert.ToInt16(dataSetLine.Tables[0].Rows[0][0]);
                        corrigendumDetails.IsPublished = true;
                    }
                }
                else
                {
                    if (addLines.TextLOB != "" && addLines.TextLOB != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));
                        methodParameter.Add(new KeyValuePair<string, string>("@Id", addLines.RecordId));
                        corrigendumDetails.ModifiedBy = Utility.CurrentSession.UserID;
                        corrigendumDetails.RecordId = Convert.ToInt16(addLines.RecordId);
                        corrigendumDetails.ModifiedDate = System.DateTime.Now;
                        dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBLineRecord", methodParameter);
                    }
                }


                Helper.ExecuteService("LOB", "InsertUpdatedCorrigendumDetails", corrigendumDetails);


                if (addLines.PaperLaidIdTemp != null && addLines.PaperLaidIdTemp != "" && addLines.PaperLaidId != null && addLines.PaperLaidId != "")
                {

                    tPaperLaidTemp PaperLaid = new tPaperLaidTemp();
                    PaperLaid.PaperLaidTempId = Convert.ToInt16(addLines.PaperLaidIdTemp);

                    PaperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", PaperLaid);
                    string Location = "";

                    //foreach (var item in PaperLaid.ListPaperLaidTemp)
                    //{
                    //    Location = item.FilePath;
                    //}


                    methodParameter = new List<KeyValuePair<string, string>>();

                    DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                    string CurrentAssembly = "";
                    string Currentsession = "";


                    for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                        {
                            CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                        }
                        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                        {
                            Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                        }
                    }

                    string Sessiondate = addLines.Sessiondate;
                    Sessiondate = Sessiondate.Replace('/', ' ');


                    string sourcePath = PaperLaid.SignedFilePath;
                    Location = PaperLaid.SignedFilePath;
                    int index = Location.LastIndexOf(@"/");
                    Location = Location.Substring(index + 1);
                    sourcePath = sourcePath.Substring(0, index);


                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";
                    DirectoryInfo Dir = new DirectoryInfo(directory);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }

                    string sourcedirectory = FileSettings.SettingValue + sourcePath;
                    //string directory = FolderCreate();

                    string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                    if (addLines.SrNo1 != null && addLines.SrNo1 != "Select")
                    {
                        fileName = addLines.SrNo1;
                    }
                    if (addLines.SrNo2 != null && addLines.SrNo2 != "Select")
                    {
                        fileName = fileName + "_" + addLines.SrNo2;
                    }
                    if (addLines.SrNo3 != null && addLines.SrNo3 != "Select")
                    {
                        fileName = fileName + "_" + addLines.SrNo3;
                    }
                    fileName = fileName + ".pdf";
                    string sourceFile = System.IO.Path.Combine(sourcedirectory, Location);
                    string destFile = System.IO.Path.Combine(directory, fileName);
                    string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";
                    // To copy a folder's contents to a new location: 
                    // Create a new target folder, if necessary. 
                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }

                    // To copy a file to another location and  
                    // overwrite the destination file if it already exists.
                    System.IO.File.Copy(sourceFile, destFile, true);

                    string recordId = addLines.RecordId;
                    if (recordId == null || recordId == "")
                    {
                        recordId = Convert.ToString(dataSetLine.Tables[0].Rows[0][0]);
                    }

                    string File = System.IO.Path.Combine(dbpath, fileName);


                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@PDFLocation", File));
                    methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", addLines.LOBId));
                    methodParameter.Add(new KeyValuePair<string, string>("@Id", recordId));
                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentPDF", methodParameter);

                    tPaperLaidV PaperLaidV = new tPaperLaidV();

                    if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageCulture == "hi-IN")
                    {

                        CultureInfo provider = CultureInfo.InvariantCulture;
                        provider = new CultureInfo("hi-IN");
                        var Date = DateTime.ParseExact(addLines.Sessiondate, "d", provider);
                        PaperLaidV.DesireLayingDate = Convert.ToDateTime(Date);

                    }
                    else
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        provider = new CultureInfo("fr-FR");
                        var Date = DateTime.ParseExact(addLines.Sessiondate, "d", provider);
                        PaperLaidV.DesireLayingDate = Convert.ToDateTime(Date);
                    }





                    PaperLaidV.PaperLaidId = Convert.ToInt16(addLines.PaperLaidId);
                    PaperLaidV.LOBRecordId = Convert.ToInt16(recordId);
                    //PaperLaidV.DesireLayingDate = Convert.ToDateTime(Date);
                    PaperLaidV.LOBPaperTempId = Convert.ToInt16(addLines.PaperLaidIdTemp);
                    Helpers.Helper.ExecuteService("PaperLaid", "UpdateLOBRecordIdIntPaperLaidVS", PaperLaidV);

                    if (addLines.BillTextNumber != "" && addLines.BillTextNumber != null)
                    {
                        UpdateBillsEntry(addLines, sourceFile);
                    }
                }
                if ((addLines.PaperLaidIdTemp == null || addLines.PaperLaidIdTemp == "" && addLines.PaperLaidId == null || addLines.PaperLaidId == ""))
                {


                    methodParameter = new List<KeyValuePair<string, string>>();

                    DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                    string CurrentAssembly = "";
                    string Currentsession = "";

                    for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                    {
                        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                        {
                            CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                        }
                        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                        {
                            Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                        }
                    }

                    string Sessiondate = addLines.Sessiondate;
                    Sessiondate = Sessiondate.Replace('/', ' ');

                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";

                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }

                    string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                    if (addLines.SrNo1 != null && addLines.SrNo1 != "Select")
                    {
                        fileName = addLines.SrNo1;
                    }
                    if (addLines.SrNo2 != null && addLines.SrNo2 != "Select")
                    {
                        fileName = fileName + "_" + addLines.SrNo2;
                    }
                    if (addLines.SrNo3 != null && addLines.SrNo3 != "Select")
                    {
                        fileName = fileName + "_" + addLines.SrNo3;
                    }
                    fileName = fileName + ".pdf";
                    // string sourceFile = System.IO.Path.Combine(sourcedirectory, Location);
                    //string directory = FolderCreate();
                    string sourceFile = System.IO.Path.Combine(directory, fileName);


                    if (addLines.BillTextNumber != "" && addLines.BillTextNumber != null)
                    {
                        UpdateBillsEntry(addLines, sourceFile);
                    }
                }


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            var data = addLines.LOBId + "," + CorrigendumId;
            var res2 = Json(data, JsonRequestBehavior.AllowGet);
            return res2;
            // Now you can do whatever you want with your model
        }

        #endregion

        #region "PartialLineGridCorrigendum"

        public ActionResult PartialLineGridCorrigendum(string LOBId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            AddLOBModel addLOBModel = new AddLOBModel();
            try
            {
                if (LOBId != null && LOBId != "")
                {
                    List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                    methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                    methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                    DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDraftLOBByIdForLineRecord", methodParameter);
                    List<AddLOBModel> ListLOB = new List<AddLOBModel>();
                    for (int i = 0; i < dataSetLOB.Tables[0].Rows.Count; i++)
                    {
                        AddLOBModel addLOBModel1 = new AddLOBModel();
                        string SrNo1 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo1"]);
                        if (SrNo1 != "")
                        {
                            addLOBModel1.SrNo11 = SrNo1;
                            SrNo1 = SrNo1 + ".";
                        }
                        string SrNo2 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo2"]);
                        if (SrNo2 != "")
                        {
                            addLOBModel1.SrNo22 = SrNo2;
                            SrNo2 = "(" + SrNo2 + ")";
                        }
                        string SrNo3 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo3"]);
                        if (SrNo3 != "")
                        {
                            addLOBModel1.SrNo33 = SrNo3;
                            SrNo3 = "(" + ToRoman(Convert.ToInt16(SrNo3)) + ")";
                        }
                        addLOBModel1.SrNo1 = SrNo1;
                        addLOBModel1.SrNo2 = SrNo2;
                        addLOBModel1.SrNo3 = SrNo3;
                        addLOBModel1.Sessiondate = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SessionDate"]);
                        addLOBModel1.RecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                        addLOBModel1.TextLOB = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TextLOB"]);
                        addLOBModel1.PDFLocation = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PDFLocation"]);
                        //addLOBModel1.IsEVoting = Convert.ToBoolean(dataSetLOB.Tables[0].Rows[i]["IsEVoting"]);
                        if (Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PageBreak"]) != "")
                        {
                            addLOBModel1.PageBreak = Convert.ToBoolean(dataSetLOB.Tables[0].Rows[i]["PageBreak"]);
                        }
                        else
                        {
                            addLOBModel1.PageBreak = false;
                        }
                        if (i == 0)
                        {
                            addLOBModel.ResultCount = Convert.ToInt32(Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TotalRecords"]));
                        }

                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                        int indx = addLOBModel1.PDFLocation.IndexOf("~");
                        if (indx != -1)
                        {

                            string filstring = addLOBModel1.PDFLocation.TrimStart('~');
                            addLOBModel1.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + filstring);

                        }
                        else if (addLOBModel1.PDFLocation != null && addLOBModel1.PDFLocation != "")
                        {
                            addLOBModel1.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + addLOBModel1.PDFLocation);
                        }
                        ListLOB.Add(addLOBModel1);
                    }
                    addLOBModel.LOBList = ListLOB;

                    if (PageNumber != null && PageNumber != "")
                    {
                        addLOBModel.PageNumber = Convert.ToInt32(PageNumber);
                    }
                    else
                    {
                        addLOBModel.PageNumber = Convert.ToInt32("1");
                    }

                    if (RowsPerPage != null && RowsPerPage != "")
                    {
                        addLOBModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                    }
                    else
                    {
                        addLOBModel.RowsPerPage = Convert.ToInt32("15");
                    }
                    if (PageNumber != null && PageNumber != "")
                    {
                        addLOBModel.selectedPage = Convert.ToInt32(PageNumber);
                    }
                    else
                    {
                        addLOBModel.selectedPage = Convert.ToInt32("1");
                    }

                    if (loopStart != null && loopStart != "")
                    {
                        addLOBModel.loopStart = Convert.ToInt32(loopStart);
                    }
                    else
                    {
                        addLOBModel.loopStart = Convert.ToInt32("1");
                    }

                    if (loopEnd != null && loopEnd != "")
                    {
                        addLOBModel.loopEnd = Convert.ToInt32(loopEnd);
                    }
                    else
                    {
                        addLOBModel.loopEnd = Convert.ToInt32("5");
                    }



                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("PartialLineGridCorrigendum", addLOBModel);
        }

        #endregion

        #region "DeleteLineRecordCorrigendum"

        public JsonResult DeleteLineRecordCorrigendum(string SessionDate, string SrNo1, string SrNo2, string SrNo3, string CorrigendumId, string LOBId)
        {


            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string Message = "";
            try
            {

                if (CorrigendumId == null || CorrigendumId == "")
                {
                    CorrigendumLOB corrigendumLOB = new CorrigendumLOB();
                    corrigendumLOB.LOBId = Convert.ToInt16(LOBId);
                    corrigendumLOB.IsSubmitted = false;
                    corrigendumLOB.IsApproved = false;
                    corrigendumLOB = (CorrigendumLOB)Helper.ExecuteService("LOB", "InsertCorrigendumLOB", corrigendumLOB);
                    CorrigendumId = Convert.ToString(corrigendumLOB.CorrigendumId);
                }

                CorrigendumDetails corrigendumDetails = new CorrigendumDetails();
                corrigendumDetails.CorrigendumId = Convert.ToInt16(CorrigendumId);



                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@sessiondate", SessionDate));
                DataSet dataSetLine = null;

                corrigendumDetails.SessionDate = Convert.ToDateTime(SessionDate);



                if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) == "" && Convert.ToString(SrNo3) == "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));

                    corrigendumDetails.SrNo1 = Convert.ToInt16(SrNo1);
                    Helper.ExecuteService("LOB", "DeleteLineAboveSrNo1Corrigendum", corrigendumDetails);
                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_DeleteLineAboveSrNo1", methodParameter);
                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    string filepath = "";
                    if (dataSetLine != null)
                    {
                        Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["AssemblyId"]);
                        Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["SessionId"]);

                    }
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (SNo1 != "" && SNo2 != "" && SNo3 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                              
                            }
                            else if (SNo1 != "" && SNo2 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            else
                            {
                                if (FileExists(directory + SNo1 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                              
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.DeletedSuccessfully;
                }
                else if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) != "" && Convert.ToString(SrNo3) == "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));

                    corrigendumDetails.SrNo1 = Convert.ToInt16(SrNo1);
                    corrigendumDetails.SrNo2 = Convert.ToInt16(SrNo2);
                    Helper.ExecuteService("LOB", "DeleteLineAboveSrNo2Corrigendum", corrigendumDetails);
                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_DeleteLineAboveSrNo2", methodParameter);
                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    string filepath = "";
                    if (dataSetLine != null)
                    {
                        Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["AssemblyId"]);
                        Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["SessionId"]);

                    }
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (SNo1 != "" && SNo2 != "" && SNo3 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            else
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                              
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.DeletedSuccessfully;
                }
                else if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) != "" && Convert.ToString(SrNo3) != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo3", SrNo3));

                    corrigendumDetails.SrNo1 = Convert.ToInt16(SrNo1);
                    corrigendumDetails.SrNo2 = Convert.ToInt16(SrNo2);
                    corrigendumDetails.SrNo3 = Convert.ToInt16(SrNo3);
                    Helper.ExecuteService("LOB", "DeleteLineAboveSrNo3Corrigendum", corrigendumDetails);
                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_DeleteLineAboveSrNo3", methodParameter);
                    string PdfPath = "";
                    string FileName = "";
                    string filepath = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    if (dataSetLine != null)
                    {
                        Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["AssemblyId"]);
                        Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["SessionId"]);

                    }
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                            {
                                System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                            }
                            if (FileExists(Tempdirectory + FileName) == true)
                            {
                                System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                            }
                            else
                            {
                                filepath = "";
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }

                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.DeletedSuccessfully;
                }
                else
                {
                    Message = Resources.LOB.UnableToDelete;
                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                Message = Resources.LOB.UnableToDelete;
            }

            var res2 = Json(Message + "," + CorrigendumId, JsonRequestBehavior.AllowGet);
            return res2;
        }
        #endregion

        #region AddAboveLineCorrigendum

        /// <summary>
        /// Add Above Line for a LOB 
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="RecordId"></param>
        /// <returns></returns>
        public JsonResult AddAboveLineCorrigendum(string SessionDate, string SrNo1, string SrNo2, string SrNo3, string CorrigendumId, string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string Message = "";
            try
            {

                if (CorrigendumId == null || CorrigendumId == "")
                {
                    CorrigendumLOB corrigendumLOB = new CorrigendumLOB();
                    corrigendumLOB.LOBId = Convert.ToInt16(LOBId);
                    corrigendumLOB.IsSubmitted = false;
                    corrigendumLOB.IsApproved = false;
                    corrigendumLOB = (CorrigendumLOB)Helper.ExecuteService("LOB", "InsertCorrigendumLOB", corrigendumLOB);
                    CorrigendumId = Convert.ToString(corrigendumLOB.CorrigendumId);
                }

                CorrigendumDetails corrigendumDetails = new CorrigendumDetails();
                corrigendumDetails.CorrigendumId = Convert.ToInt16(CorrigendumId);


                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@sessiondate", SessionDate));
                DataSet dataSetLine = null;

                corrigendumDetails.SessionDate = Convert.ToDateTime(SessionDate);

                if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) == "" && Convert.ToString(SrNo3) == "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));

                    corrigendumDetails.SrNo1 = Convert.ToInt16(SrNo1);

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLineAboveSrNo1", methodParameter);
                    Helper.ExecuteService("LOB", "InsertLineAboveSrNo1Corrigendum", corrigendumDetails);

                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    string filepath = "";
                    if (dataSetLine!=null)
                    {
                        Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["AssemblyId"]);
                        Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["SessionId"]);
                      
                    }
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }
                      
                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (SNo1 != "" && SNo2 != "" && SNo3 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            else if (SNo1 != "" && SNo2 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            else
                            {
                                if (FileExists(directory + SNo1 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            if(!string.IsNullOrEmpty(filepath))
                            {
                            List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                            methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                            methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                            ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.AddSuccessfully;
                }
                else if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) != "" && Convert.ToString(SrNo3) == "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));

                    corrigendumDetails.SrNo1 = Convert.ToInt16(SrNo1);
                    corrigendumDetails.SrNo2 = Convert.ToInt16(SrNo2);


                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLineAboveSrNo2", methodParameter);
                    Helper.ExecuteService("LOB", "InsertLineAboveSrNo2Corrigendum", corrigendumDetails);

                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    string filepath = "";
                    if (dataSetLine != null)
                    {
                        Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["AssemblyId"]);
                        Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["SessionId"]);

                    }
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }
                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (SNo1 != "" && SNo2 != "" && SNo3 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            else
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.AddSuccessfully;
                }
                else if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) != "" && Convert.ToString(SrNo3) != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo3", SrNo3));

                    corrigendumDetails.SrNo1 = Convert.ToInt16(SrNo1);
                    corrigendumDetails.SrNo2 = Convert.ToInt16(SrNo2);
                    corrigendumDetails.SrNo3 = Convert.ToInt16(SrNo3);

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLineAboveSrNo3", methodParameter);
                    Helper.ExecuteService("LOB", "InsertLineAboveSrNo3Corrigendum", corrigendumDetails);

                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string filepath = "";
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    if (dataSetLine != null)
                    {
                        Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["AssemblyId"]);
                        Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[0]["SessionId"]);

                    }
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }
                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                            {
                                System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                            }
                            if (FileExists(Tempdirectory + FileName) == true)
                            {
                                System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                 filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                            }
                            else
                            {
                                filepath = "";
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.AddSuccessfully;
                }
                else
                {
                    Message = Resources.LOB.UnableToAdd;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                Message = Resources.LOB.UnableToAdd;
            }

            var res2 = Json(Message + "," + CorrigendumId, JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region "SubmitLOBCorrigendum"


        /// <summary>
        /// It will submit an Draft LOB for Approval
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public JsonResult SubmitLOBCorrigendum(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }

            DraftLOB draftLOB = new DraftLOB();
            draftLOB.LOBId = Convert.ToInt16(LOBId);
            draftLOB.IsSubmitted = true;
            draftLOB.SubmittedDate = System.DateTime.Now;
            draftLOB.SubmittedTime = System.DateTime.Now.TimeOfDay;


            int corrigendumId = 0;

            var result = (CorrigendumLOB)Helper.ExecuteService("LOB", "CorrigendumLOBByLOBId", draftLOB);
            if (result.IsApproved || result.IsSubmitted)
            {

            }
            else
            {
                corrigendumId = result.CorrigendumId;
            }

            Helper.ExecuteService("LOB", "SubmitCorrigendumLOB", draftLOB);


            //List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            //methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            //DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SubmitLOBById", methodParameter);
            string Path = Convert.ToString(GenerateLOBPdfCorrigendum(LOBId, true, "Draft", corrigendumId));
            draftLOB.SubmittedLOBPath = Path;

            Helper.ExecuteService("LOB", "UpdateSubmitCorrigendumLOBPath", draftLOB);

            var res2 = Json(true, JsonRequestBehavior.AllowGet);
            return res2;
        }




        #endregion

        #region "ApproveLOBCorrigendum"

        /// <summary>
        /// It will Approve a Corrigendum
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public JsonResult ApproveLOBCorrigendum(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }

            AdminLOB draftLOB = new AdminLOB();
            draftLOB.LOBId = Convert.ToInt16(LOBId);
            draftLOB.ApprovedBy = CurrentSession.UserID;
            draftLOB.ApprovedDate = System.DateTime.Now;
            draftLOB.ApprovedTime = System.DateTime.Now.TimeOfDay;

            Helper.ExecuteService("LOB", "ApproveCorrigendumLOB", draftLOB);

            int corrigendumId = 0;

            DraftLOB draftLOB1 = new DraftLOB();
            draftLOB1.LOBId = Convert.ToInt16(LOBId);

            var result = (CorrigendumLOB)Helper.ExecuteService("LOB", "CorrigendumLOBByLOBId", draftLOB1);

            if (result.IsApproved && result.IsSubmitted)
            {
                corrigendumId = result.CorrigendumId;
            }
            else
            {

            }

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));

            DataSet dataSet1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionDateByLobId", methodParameter);
            if (dataSet1 != null)
            {
                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                methodParameter1.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                methodParameter1.Add(new KeyValuePair<string, string>("@SessionDate", Convert.ToString(dataSet1.Tables[0].Rows[0]["SessionDate"])));
                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertApprovedLOBCorr", methodParameter1);

            }



            //GenerateLOBPdfCorrigendum(LOBId, true, "Approved", corrigendumId);
            string Path = Convert.ToString(GenerateLOBPdfCorrigendum(LOBId, true, "Approved", corrigendumId));
            draftLOB.LOBPath = Path;

            Helper.ExecuteService("LOB", "UpdateApproveCorrigendumLOBPath", draftLOB);

            var res2 = Json(true, JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region "ReturnDraftLOBCorrigendum"

        /// <summary>
        /// It will submit an Draft LOB for Approval
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public JsonResult ReturnDraftLOBCorrigendum(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }

            DraftLOB draftLOB = new DraftLOB();
            draftLOB.LOBId = Convert.ToInt16(LOBId);
            draftLOB.IsSubmitted = false;
            draftLOB.SubmittedDate = null;
            draftLOB.SubmittedTime = null;

            Helper.ExecuteService("LOB", "ReturnCorrigendumLOB", draftLOB);


            //List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            //methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            ////DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SubmitLOBById", methodParameter);
            //string Path = Convert.ToString(GenerateLOBPdfCorrigendum(LOBId, true, "Draft", corrigendumId));
            //draftLOB.SubmittedLOBPath = Path;

            //Helper.ExecuteService("LOB", "UpdateSubmitCorrigendumLOBPath", draftLOB);

            var res2 = Json(true, JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region AddAboveLine

        /// <summary>
        /// Add Above Line for a LOB 
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="RecordId"></param>
        /// <returns></returns>
        public JsonResult AddAboveLine(string SessionDate, string SrNo1, string SrNo2, string SrNo3)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string Message = "";
            try
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@sessiondate", SessionDate));
                DataSet dataSetLine = null;


                if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) == "" && Convert.ToString(SrNo3) == "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLineAboveSrNo1", methodParameter);

                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    string filepath = "";
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    foreach (var file in Directory.GetFiles(directory))
                       System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (SNo1 != "" && SNo2 != "" && SNo3 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                              
                            }
                            else if (SNo1 != "" && SNo2 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                              
                            }
                            else
                            {
                                if (FileExists(directory + SNo1 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.AddSuccessfully;
                }
                else if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) != "" && Convert.ToString(SrNo3) == "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLineAboveSrNo2", methodParameter);

                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    string filepath = "";
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (SNo1 != "" && SNo2 != "" && SNo3 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(directory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            else
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(directory + FileName, directory + SNo1 + "_" + SNo2 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }
                               
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.AddSuccessfully;
                }
                else if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) != "" && Convert.ToString(SrNo3) != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo3", SrNo3));

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLineAboveSrNo3", methodParameter);

                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    string filepath = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                            {
                                System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                            }
                            if (FileExists(Tempdirectory + FileName) == true)
                            {
                                System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                 filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                            }
                            else
                            {
                                filepath = "";
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.AddSuccessfully;
                }
                else
                {
                    Message = Resources.LOB.UnableToAdd;
                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                Message = Resources.LOB.UnableToAdd;
            }

            var res2 = Json(Message, JsonRequestBehavior.AllowGet);
            return res2;
        }
        public JsonResult AddFirstLine(string SessionDate, string SrNo1, string SrNo2, string SrNo3)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string Message = "";
            try
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@sessiondate", SessionDate));
                if (CurrentSession.AssemblyId != null && CurrentSession.AssemblyId != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@AssemblyId", CurrentSession.AssemblyId));
                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyID", CurrentSession.AssemblyId));
                    DataSet dataSetAssembly = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectAssemblyInfoById", methodParameter1);

                    if (dataSetAssembly != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@AssemblyName", Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyName"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@AssemblyNameLocal", Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyNameLocal"])));
                    }
                }

                if (CurrentSession.SessionId != null && CurrentSession.SessionId != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
                    methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyID", CurrentSession.SessionId));
                    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionInfoById", methodParameter1);

                    if (dataSet != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionName", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionName"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionNameLocal"])));
                    }
                }
                //   string Sessiondate="";
                if (SessionDate != null && SessionDate != "")
                {
                   // methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", addLines.Sessiondate));
                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionDate", SessionDate));
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
                    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionDateInfo", methodParameter1);

                    if (dataSet != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionDateLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionDateLocal"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionTime", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionTime"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionTimeLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionTimeLocal"])));
                    }
                }

                methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", Utility.CurrentSession.UserID));
                methodParameter.Add(new KeyValuePair<string, string>("@CreatedDate", Convert.ToString(System.DateTime.Now)));

                DataSet dataSetLine = null;


                if (Convert.ToString(SrNo1) == "0" && Convert.ToString(SrNo2) == "0" && Convert.ToString(SrNo3) == "0")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertFirstLineSrNo1", methodParameter);

                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    string filepath = "";
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    string lobid = "";
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                         lobid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["LOBID"]); 
                       if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (SNo1 != "" && SNo2 != "" && SNo3 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }

                            }
                            else if (SNo1 != "" && SNo2 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }

                            }
                            else
                            {
                                if (FileExists(directory + SNo1 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }

                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    //Message = lobid + "-" + Resources.LOB.AddSuccessfully;
                    Message = lobid;
                }

                    ////////////////////////////-----
                else if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) != "" && Convert.ToString(SrNo3) == "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLineAboveSrNo2", methodParameter);

                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    string filepath = "";
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (SNo1 != "" && SNo2 != "" && SNo3 != "")
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(directory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }

                            }
                            else
                            {
                                if (FileExists(directory + SNo1 + "_" + SNo2 + ".pdf") == true)
                                {
                                    System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + ".pdf");
                                }
                                if (FileExists(Tempdirectory + FileName) == true)
                                {
                                    System.IO.File.Move(directory + FileName, directory + SNo1 + "_" + SNo2 + ".pdf");
                                    filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + ".pdf";
                                }
                                else
                                {
                                    filepath = "";
                                }

                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.AddSuccessfully;
                }
                else if (Convert.ToString(SrNo1) != "" && Convert.ToString(SrNo2) != "" && Convert.ToString(SrNo3) != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", SrNo1));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", SrNo2));
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo3", SrNo3));

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLineAboveSrNo3", methodParameter);

                    string PdfPath = "";
                    string FileName = "";
                    string Assemblyid = "";
                    string Sessionid = "";
                    string filepath = "";
                    DateTime SessionDateString;
                    string ddd = "";
                    string SNo1 = "";
                    string SNo2 = "";
                    string SNo3 = "";
                    SessionDateString = Convert.ToDateTime(SessionDate);
                    ddd = String.Format("{0:dd/MM/yyyy}", SessionDateString);
                    ddd = ddd.Replace('/', ' ');
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/";
                    string Tempdirectory = FileSettings.SettingValue + "/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/Temp/";
                    if (!System.IO.Directory.Exists(Tempdirectory))
                    {
                        System.IO.Directory.CreateDirectory(Tempdirectory);
                    }
                    else
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo1 = new DirectoryInfo(Tempdirectory);
                        foreach (FileInfo file in downloadedMessageInfo1.GetFiles())
                        {
                            file.Delete();
                        }

                    }
                    foreach (var file in Directory.GetFiles(directory))
                        System.IO.File.Copy(file, Path.Combine(Tempdirectory, Path.GetFileName(file)), true);
                    for (int i = 0; i < dataSetLine.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetLine.Tables[0].Rows[i]["PDFLocation"] != null && Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]) != "")
                        {
                            PdfPath = Convert.ToString(dataSetLine.Tables[0].Rows[i]["PDFLocation"]);
                            SNo1 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo1"]);
                            SNo2 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo2"]);
                            SNo3 = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SrNo3"]);
                            Assemblyid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["AssemblyId"]);
                            Sessionid = Convert.ToString(dataSetLine.Tables[0].Rows[i]["SessionId"]);
                            string id = Convert.ToString(dataSetLine.Tables[0].Rows[i]["Id"]);
                            int index = PdfPath.LastIndexOf(@"/");
                            FileName = PdfPath.Substring(index + 1);

                            if (FileExists(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf") == true)
                            {
                                System.IO.File.Delete(directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                            }
                            if (FileExists(Tempdirectory + FileName) == true)
                            {
                                System.IO.File.Move(Tempdirectory + FileName, directory + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf");
                                filepath = "~/LOB/" + Assemblyid + "/" + Sessionid + "/" + ddd + "/Documents/" + SNo1 + "_" + SNo2 + "_" + SNo3 + ".pdf";
                            }
                            else
                            {
                                filepath = "";
                            }
                            if (!string.IsNullOrEmpty(filepath))
                            {
                                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                                methodParameter1.Add(new KeyValuePair<string, string>("@PDFLocation", filepath));
                                methodParameter1.Add(new KeyValuePair<string, string>("@id", id));
                                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDocName", methodParameter1);
                            }
                        }

                    }
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(Tempdirectory);
                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    System.IO.Directory.Delete(Tempdirectory);
                    Message = Resources.LOB.AddSuccessfully;
                }
                else
                {
                    Message = Resources.LOB.UnableToAdd;
                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                Message = Resources.LOB.UnableToAdd;
            }

            var res2 = Json(Message, JsonRequestBehavior.AllowGet);
            return res2;
        }
        #endregion

        #region UploadDocument

        /// <summary>
        /// For Upload the Document related to an LOB
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="file"></param>
        /// <param name="NatureOfDocument"></param>
        /// <param name="ActionDocumentType"></param>
        /// <param name="LOBId"></param>
        /// <param name="RecordId"></param>
        /// <returns></returns>
        public ActionResult UploadDocument(HttpPostedFileBase file, string NatureOfDocument, string ActionDocumentType, string LOBId, string RecordId, string Sr, string Sessiondate)
        {
            string pdfPath = "";
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }

            try
            {



                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                string FilePath = "";

                if (RecordId == null || RecordId == "")
                {
                    if (LOBId == null || LOBId == "")
                    {
                        methodParameter = new List<KeyValuePair<string, string>>();
                        DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectMaxLOBId", methodParameter);
                        if (dataSetLOB != null)
                        {
                            if (Convert.ToString(dataSetLOB.Tables[0].Rows[0][0]) != "")
                            {
                                LOBId = Convert.ToString(Convert.ToInt16(dataSetLOB.Tables[0].Rows[0][0]) + 1);
                            }
                            else
                            {
                                LOBId = "1";
                            }
                        }
                    }
                }
                methodParameter = new List<KeyValuePair<string, string>>();

                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }
                if (file != null)
                {
                    string fileName = "";
                    //if (NatureOfDocument == "PPT")
                    //{
                    //    fileName = LOBId + "_PPT_" + System.IO.Path.GetFileName(file.FileName);
                    //}
                    //if (NatureOfDocument == "PDF")
                    //{
                    //    fileName = LOBId + "_PDF_" + System.IO.Path.GetFileName(file.FileName);
                    //}
                    //if (NatureOfDocument == "Video")
                    //{
                    //    fileName = LOBId + "_Video_" + System.IO.Path.GetFileName(file.FileName);
                    //}
                    int index = file.FileName.LastIndexOf(".");
                    string exten = file.FileName.Substring(index);
                    Sessiondate = Sessiondate.Replace('/', ' ');
                    //TempData["addLines"] = Sessiondate;
                    fileName = Sr + exten;

                    //string directory = FolderCreate();
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";
                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }

                    //string sourcedirectory = FileSettings.SettingValue + sourcePath;
                    /*  comment Sanjay 
                    string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";
                     string directory = Server.MapPath(url);
                   if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    */
                    string savepath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";
                    string path = System.IO.Path.Combine(directory, fileName);
                    file.SaveAs(path);

                    if (NatureOfDocument != "Video")
                    {

                        #region albumthumb save

                        int fileLength = file.ContentLength;

                        byte[] myData = new byte[fileLength];
                        file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(myData.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, myData);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        //   ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, fileLength, LOBId + "/Documents", "LOB");
                    }


                        #endregion


                    FilePath = "~" + savepath + fileName;
                }

                DataSet dataSetLine = null;
                DataSet dataSetLineNew = null;
                if (RecordId == null || RecordId == "")
                {

                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));

                    if (NatureOfDocument == "PPT")
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@PPTLocation", FilePath));
                        methodParameter.Add(new KeyValuePair<string, string>("@ActionDocumentTypePPT", ActionDocumentType));
                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", Utility.CurrentSession.UserID));
                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedDate", Convert.ToString(System.DateTime.Now)));
                        if (file != null)
                        {
                            dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLOBDocumentPPT", methodParameter);
                        }
                    }
                    if (NatureOfDocument == "PDF")
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@PDFLocation", FilePath));
                        methodParameter.Add(new KeyValuePair<string, string>("@ActionDocumentTypePDF", ActionDocumentType));
                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", Utility.CurrentSession.UserID));
                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedDate", Convert.ToString(System.DateTime.Now)));
                        if (file != null)
                        {
                            dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLOBDocumentPDF", methodParameter);
                        }
                    }
                    if (NatureOfDocument == "Video")
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@VideoLocation", FilePath));
                        methodParameter.Add(new KeyValuePair<string, string>("@ActionDocumentTypeVideo", ActionDocumentType));
                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", Utility.CurrentSession.UserID));
                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedDate", Convert.ToString(System.DateTime.Now)));
                        if (file != null)
                        {
                            dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLOBDocumentVideo", methodParameter);
                        }
                    }
                }
                else
                {
                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                    methodParameter.Add(new KeyValuePair<string, string>("@Id", RecordId));

                    if (NatureOfDocument == "PPT")
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@PPTLocation", FilePath));
                        methodParameter.Add(new KeyValuePair<string, string>("@ActionDocumentTypePPT", ActionDocumentType));
                        methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));
                        if (file != null)
                        {
                            dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentPPT", methodParameter);
                        }
                    }
                    if (NatureOfDocument == "PDF")
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@PDFLocation", FilePath));
                        methodParameter.Add(new KeyValuePair<string, string>("@ActionDocumentTypePDF", ActionDocumentType));
                        methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));
                        if (file != null)
                        {
                            dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentPDF", methodParameter);

                        }
                    }
                    if (NatureOfDocument == "Video")
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@VideoLocation", FilePath));
                        methodParameter.Add(new KeyValuePair<string, string>("@ActionDocumentTypeVideo", ActionDocumentType));
                        methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));
                        if (file != null)
                        {
                            dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentVideo", methodParameter);
                        }
                    }
                }
                List<KeyValuePair<string, string>> methodParameterNew = new List<KeyValuePair<string, string>>();
                if (RecordId == null || RecordId == "")
                {
                    RecordId = Convert.ToString(dataSetLine.Tables[0].Rows[0][0]);
                    methodParameterNew.Add(new KeyValuePair<string, string>("@Id", RecordId));
                    dataSetLineNew = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentPDFReturnPath", methodParameterNew);
                    pdfPath = Convert.ToString(dataSetLineNew.Tables[0].Rows[0][0]);
                }
                else
                {

                    methodParameterNew.Add(new KeyValuePair<string, string>("@Id", RecordId));
                    dataSetLineNew = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentPDFReturnPath", methodParameterNew);
                    pdfPath = Convert.ToString(dataSetLineNew.Tables[0].Rows[0][0]);
                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            var data = RecordId + "," + LOBId + "," + pdfPath;
            var res2 = Json(data, JsonRequestBehavior.AllowGet);
            return res2;
        }
        public ActionResult GetAttachedPDFPath(string RecordId)
        {
            string pdfPath = "";
            try
            {

                DataSet dataSetLineNew = null;
                List<KeyValuePair<string, string>> methodParameterNew = new List<KeyValuePair<string, string>>();


                methodParameterNew.Add(new KeyValuePair<string, string>("@Id", RecordId));
                dataSetLineNew = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentPDFReturnPath", methodParameterNew);
                pdfPath = Convert.ToString(dataSetLineNew.Tables[0].Rows[0][0]);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            var data = pdfPath;
            var res2 = Json(data, JsonRequestBehavior.AllowGet);
            return res2;
        }
        #endregion

        #region PartialDocumentGrid


        /// <summary>
        /// Will get the list of Document for a line in (Line Data enry PartialView)
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialDocumentGrid(string RecordId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            AddLOBModel addLOBModel = new AddLOBModel();
            try
            {
                if (RecordId != null && RecordId != "")
                {
                    List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@Id", RecordId));
                    DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLineDocumentById", methodParameter);
                    List<AddLOBModel> ListLOB = new List<AddLOBModel>();


                    if (Convert.ToString(dataSetLOB.Tables[0].Rows[0]["PPTLocation"]) != "")
                    {
                        AddLOBModel addLOBModel1 = new AddLOBModel();
                        addLOBModel1.PDFLocation = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["PPTLocation"]);
                        string Location = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["PPTLocation"]);
                        int index = Location.LastIndexOf('/');
                        string substr = Location.Substring(index + 1);
                        int secIndex = substr.LastIndexOf('_');
                        string filename = substr.Substring(secIndex + 1);
                        addLOBModel1.SrNo1 = filename;
                        addLOBModel1.NatureOfDocument = "PPT";
                        addLOBModel1.ActionDocumentType = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["ActionDocumentTypePPT"]);
                        addLOBModel1.RecordId = RecordId;
                        ListLOB.Add(addLOBModel1);
                    }




                    if (Convert.ToString(dataSetLOB.Tables[0].Rows[0]["VideoLocation"]) != "")
                    {
                        AddLOBModel addLOBModel3 = new AddLOBModel();
                        addLOBModel3.PDFLocation = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["VideoLocation"]);
                        string Location = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["VideoLocation"]);
                        int index = Location.LastIndexOf('/');
                        string substr = Location.Substring(index + 1);
                        int secIndex = substr.LastIndexOf('_');
                        string filename = substr.Substring(secIndex + 1);
                        addLOBModel3.SrNo1 = filename;
                        addLOBModel3.NatureOfDocument = "Video";
                        addLOBModel3.ActionDocumentType = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["ActionDocumentTypeVideo"]);
                        addLOBModel3.RecordId = RecordId;
                        ListLOB.Add(addLOBModel3);
                    }



                    addLOBModel.LOBList = ListLOB;


                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("PartialDocumentGrid", addLOBModel);
        }


        /// <summary>
        /// get and show PDF for a LOB Line
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="Location"></param>
        /// <returns></returns>
        public ActionResult ViewPdfFromServer(string Location)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            if (Location != null && Location != "")
            {
                return File(Location, "application/pdf", "PDF.pdf");
                // return RedirectToAction("LoadAction", "Module", new { area = "", ActionName = "LoadPDF", ControllerName = "PDFViewer", ModuleName = "PDFViewer", PDFPath = path });
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// get and show PPT for a LOB Line
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="Location"></param>
        /// <returns></returns>
        public ActionResult ViewPptFromServer(string Location)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            if (Location != null && Location != "")
            {
                return File(Location, "application/ppt", "PPT.ppt");
                // return RedirectToAction("LoadAction", "Module", new { area = "", ActionName = "LoadPDF", ControllerName = "PDFViewer", ModuleName = "PDFViewer", PDFPath = path });
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// get and show Video for a LOB Line
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <param name="Location"></param>
        /// <returns></returns>
        public ActionResult ViewVideoFromServer(string Location)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            if (Location != null && Location != "")
            {
                return File(Location, "application/mp4", "Video.mp4");
                // return RedirectToAction("LoadAction", "Module", new { area = "", ActionName = "LoadPDF", ControllerName = "PDFViewer", ModuleName = "PDFViewer", PDFPath = path });
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region PartialDocumentGridPDF


        /// <summary>
        /// Will get the list of Document for a line in (Line Data enry PartialView)
        ///  Created By:Himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialDocumentGridPDF(string RecordId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            AddLOBModel addLOBModel = new AddLOBModel();
            try
            {
                if (RecordId != null && RecordId != "")
                {
                    List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@Id", RecordId));
                    DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLineDocumentById", methodParameter);
                    List<AddLOBModel> ListLOB = new List<AddLOBModel>();




                    if (Convert.ToString(dataSetLOB.Tables[0].Rows[0]["PDFLocation"]) != "")
                    {
                        AddLOBModel addLOBModel2 = new AddLOBModel();
                        addLOBModel2.PDFLocation = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["PDFLocation"]);
                        string Location = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["PDFLocation"]);
                        int index = Location.LastIndexOf('/');
                        string substr = Location.Substring(index + 1);
                        //int secIndex = substr.LastIndexOf('_');
                        //string filename = substr.Substring(secIndex + 1);
                        addLOBModel2.SrNo1 = substr;
                        addLOBModel2.NatureOfDocument = "PDF";
                        addLOBModel2.ActionDocumentType = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["ActionDocumentTypePDF"]);
                        addLOBModel2.RecordId = RecordId;
                        ListLOB.Add(addLOBModel2);
                    }





                    addLOBModel.LOBList = ListLOB;


                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("PartialDocumentGridPDF", addLOBModel);
        }




        #endregion

        #region DeleteDocumentRecord

        /// <summary>
        /// delete select Document for the LOB Line Record
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="Nature"></param>
        /// <param name="RecordId"></param>
        /// <returns></returns>
        public JsonResult DeleteDocumentRecord(string Nature, string RecordId)
        {

            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string Message = "";
            try
            {
                DataSet dataSetLine = new DataSet();
                if (RecordId != null && RecordId != "")
                {
                    List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", ""));
                    methodParameter.Add(new KeyValuePair<string, string>("@Id", RecordId));

                    if (Nature.Trim() == "PPT")
                    {
                        //   methodParameter.Add(new KeyValuePair<string, string>("@PPTLocation", ""));
                        //   methodParameter.Add(new KeyValuePair<string, string>("@ActionDocumentTypePPT", ""));
                        methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));

                        dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentPPT", methodParameter);
                        Message = Resources.LOB.DeletedSuccessfully;
                    }
                    else if (Nature.Trim() == "PDF")
                    {
                        //  methodParameter.Add(new KeyValuePair<string, string>("@PDFLocation", ""));
                        //  methodParameter.Add(new KeyValuePair<string, string>("@ActionDocumentTypePDF", ""));
                        methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));

                        dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentPDF", methodParameter);
                        Message = Resources.LOB.DeletedSuccessfully;
                    }
                    else if (Nature.Trim() == "Video")
                    {
                        //  methodParameter.Add(new KeyValuePair<string, string>("@VideoLocation", ""));
                        ///  methodParameter.Add(new KeyValuePair<string, string>("@ActionDocumentTypeVideo", ""));
                        methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));

                        dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentVideo", methodParameter);
                        Message = Resources.LOB.DeletedSuccessfully;
                    }
                    else
                    {
                        Message = Resources.LOB.UnableToDelete;
                    }
                }
                else
                {
                    Message = Resources.LOB.UnableToDelete;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                Message = Resources.LOB.UnableToDelete;
            }

            var res2 = Json(Message, JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region "DeleteLOB"

        /// <summary>
        /// Delete the LOB from Draft Table
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public JsonResult DeleteLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string result = "";
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_DeleteLOBById", methodParameter);
            var res2 = Json(result, JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region "SubmitLOB"

        /// <summary>
        /// It will submit an Draft LOB for Approval
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public JsonResult SubmitLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SubmitLOBById", methodParameter);
            GenerateLOBPdf(LOBId, true, "Draft");
            var res2 = Json(true, JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region ShowSubmittedLOB

        /// <summary>
        /// Get and Show Submitted Draft LOB from FileSystem
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public ActionResult ShowSubmittedLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", "10"));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDraftLOBByIdForLineRecord", methodParameter);
            string path = "";
            if (dataSetLOB != null)
            {
                if (dataSetLOB.Tables[0].Rows.Count > 0)
                {
                    path = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["SubmittedLOBPath"]);
                }
            }

            if (path != null && path != "")
            {
                return File(path, "application/pdf");
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region ShowSubmittedApprovedLOB

        /// <summary>
        /// Get and Show Approved LOB from File System
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public ActionResult ShowSubmittedApprovedLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", "10"));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectApprovedLOBById", methodParameter);
            string path = "";
            if (dataSetLOB != null)
            {
                if (dataSetLOB.Tables[0].Rows.Count > 0)
                {
                    path = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["LOBPath"]);
                }
            }

            if (path != null && path != "")
            {
                return File(path, "application/pdf");
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region ShowPublishedLOB

        /// <summary>
        /// Get and Show Published LOB from File system
        /// Created By: Himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public ActionResult ShowPublishedLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", "10"));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectPublishedLOBById", methodParameter);
            string path = "";
            if (dataSetLOB != null)
            {
                if (dataSetLOB.Tables[0].Rows.Count > 0)
                {
                    path = Convert.ToString(dataSetLOB.Tables[0].Rows[0]["PublishLOBPath"]);
                }
            }
            if (path != null && path != "")
            {
                return File(path, "application/pdf");
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region PartialPendingLOB

        /// <summary>
        /// Will render the PartialPendingLOB PartialView
        /// Created By : himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialPendingLOB(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();

            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectPendingLOB]", methodParameter);
                List<LOBModel> listLOBForDay = new List<LOBModel>();
                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }
                        listLOBForDay.Add(objLOB);


                        //if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["IsCorrigendum"]) != "")
                        //{
                        //    objLOB.IsCorrigendum = Convert.ToBoolean(SummaryDataSet.Tables[0].Rows[i]["IsCorrigendum"]);
                        //}
                        //else
                        //{
                        //    objLOB.IsCorrigendum = false;
                        //}
                    }

                    var result = (List<DraftLOB>)Helper.ExecuteService("LOB", "GetCorrigendumPendingForSubmit", null);

                    if (result != null)
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            LOBModel model = new LOBModel();
                            model.LOBId = Convert.ToString(result[i].LOBId);
                            model.AssemblyName = result[i].AssemblyName;
                            model.AssemblyNameLocal = result[i].AssemblyNameLocal;
                            model.SessionName = result[i].SessionName;
                            model.SessionNameLocal = result[i].SessionNameLocal;
                            model.SessionDate = Convert.ToString(result[i].SessionDate);
                            model.SessionDateLocal = result[i].SessionDateLocal;
                            model.SessionNameLocal = result[i].SessionNameLocal;
                            model.SessionNameLocal = result[i].SessionNameLocal;
                            model.IsCorrisandumPending = true;
                            listLOBForDay.Add(model);
                        }
                    }

                    var newList = listLOBForDay.OrderByDescending(x => x.SessionDate).ToList();
                    objPendingLOB.ListLOBBYId = newList;

                }








                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView(objPendingLOB);
        }

        #endregion

        #region PartialSubmittedLOB

        /// <summary>
        /// Will render the PartialSubmittedLOB PartialView
        /// Created By : himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialSubmittedLOB(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();
            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSubmittedLOB]", methodParameter);

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        objLOB.SubmittedDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]);
                        objLOB.PDFLocation = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedLOBPath"]);

                        if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["IsApproved"]) != "")
                        {
                            objLOB.IsApproved = Convert.ToBoolean(SummaryDataSet.Tables[0].Rows[i]["IsApproved"]);
                            if (objLOB.IsApproved == true)
                            {
                                DataSet ApprovedDataSet = new DataSet();
                                var SelectedmethodParameter = new List<KeyValuePair<string, string>>();

                                SelectedmethodParameter.Add(new KeyValuePair<string, string>("@LOBID", objLOB.LOBId));
                                ApprovedDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSubmittedApprovedLOBPath]", SelectedmethodParameter);
                                if (ApprovedDataSet != null)
                                {
                                    if (ApprovedDataSet.Tables[0].Rows.Count >= 1)
                                    {
                                        objLOB.PDFLocation = ApprovedDataSet.Tables[0].Rows[0]["LOBPath"].ToString();
                                        //if (objLOB.PDFLocation == null || objLOB.PDFLocation == "")
                                        //{
                                        //    objLOB.PDFLocation = ApprovedDataSet.Tables[0].Rows[1]["LOBPath"].ToString();
                                        //}
                                    }
                                }
                            }
                        }
                        else
                        {
                            objLOB.IsApproved = false;
                        }
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                        int indx = objLOB.PDFLocation.IndexOf("~");
                        if (indx != -1)
                        {

                            string filstring = objLOB.PDFLocation.TrimStart('~');

                            objLOB.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + filstring);

                        }
                        else if (objLOB.PDFLocation != null && objLOB.PDFLocation != "")
                        {
                            objLOB.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + objLOB.PDFLocation);
                        }
                        //if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["IsCorrigendum"]) != "")
                        //{
                        //    objLOB.IsCorrigendum = Convert.ToBoolean(SummaryDataSet.Tables[0].Rows[i]["IsCorrigendum"]);
                        //}
                        //else
                        //{
                        //    objLOB.IsCorrigendum = false;
                        //}

                        if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]) != "")
                        {
                            TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]));
                            DateTime time = DateTime.Today.Add(interval);
                            string displayTime = time.ToString("hh:mm tt");

                            objLOB.SubmittedTime = displayTime;
                        }
                        else
                        {
                            objLOB.SubmittedTime = "";
                        }

                        int indexof = objLOB.PDFLocation.LastIndexOf('/');
                        objLOB.PPTLocation = objLOB.PDFLocation.Substring(indexof + 1);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }

                        DraftLOB model = new DraftLOB();
                        model.LOBId = Convert.ToInt16(objLOB.LOBId);
                        var result1 = (CorrigendumLOB)Helper.ExecuteService("LOB", "CorrigendumLOBByLOBId", model);
                        if (result1 != null)
                        {
                            if (result1.IsApproved == true && result1.IsSubmitted == true)
                            {
                                objLOB.IsCorrisandumPending = true;
                            }
                            else
                            {
                                objLOB.IsCorrisandumPending = false;
                            }
                        }
                        else
                        {
                            if (Convert.ToBoolean(objLOB.IsApproved))
                            {
                                objLOB.IsCorrisandumPending = true;
                            }
                        }
                        listLOBForDay.Add(objLOB);
                    }

                    var result = (List<DraftLOB>)Helper.ExecuteService("LOB", "GetCorrigendumSubmitted", null);

                    if (result != null)
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            LOBModel model = new LOBModel();
                            model.LOBId = Convert.ToString(result[i].LOBId);
                            model.AssemblyName = result[i].AssemblyName;
                            model.AssemblyNameLocal = result[i].AssemblyNameLocal;
                            model.SessionName = result[i].SessionName;
                            model.SessionNameLocal = result[i].SessionNameLocal;
                            model.SessionDate = Convert.ToString(result[i].SessionDate);
                            model.SessionDateLocal = result[i].SessionDateLocal;
                            model.SubmittedDate = Convert.ToString(result[i].SubmittedDate);
                            model.PDFLocation = result[i].SubmittedLOBPath;
                            model.IsApproved = result[i].IsApproved;
                            if (model.IsApproved == true)
                            {
                                DataSet ApprovedDataSet = new DataSet();
                                var SelectedmethodParameter = new List<KeyValuePair<string, string>>();

                                SelectedmethodParameter.Add(new KeyValuePair<string, string>("@LOBID", model.LOBId));
                                ApprovedDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSubmittedApprovedLOBPathCorri]", SelectedmethodParameter);
                                if (ApprovedDataSet != null)
                                {
                                    if (ApprovedDataSet.Tables[0].Rows.Count >= 1)
                                    {
                                        model.PDFLocation = ApprovedDataSet.Tables[0].Rows[0]["LOBPath"].ToString();
                                        if (model.PDFLocation == null || model.PDFLocation == "")
                                        {
                                            model.PDFLocation = ApprovedDataSet.Tables[0].Rows[1]["LOBPath"].ToString();
                                        }
                                    }
                                }
                            }
                            if (Convert.ToString(result[i].SubmittedTime) != "")
                            {
                                TimeSpan interval = TimeSpan.Parse(Convert.ToString(result[i].SubmittedTime));
                                DateTime time = DateTime.Today.Add(interval);
                                string displayTime = time.ToString("hh:mm tt");

                                model.SubmittedTime = displayTime;
                            }
                            else
                            {
                                model.SubmittedTime = "";
                            }
                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                            if (model.PDFLocation != null)
                            {
                                int indx = model.PDFLocation.IndexOf("~");
                                if (indx != -1)
                                {

                                    string filstring = model.PDFLocation.TrimStart('~');
                                    //addLOBModel1.PDFLocation = filstring;
                                    model.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + filstring);

                                }
                                else if (model.PDFLocation != null && model.PDFLocation != "")
                                {
                                    model.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + model.PDFLocation);
                                }
                            }
                            DraftLOB model1 = new DraftLOB();
                            model1.LOBId = Convert.ToInt16(model.LOBId);
                            var result1 = (CorrigendumLOB)Helper.ExecuteService("LOB", "CorrigendumLOBByLOBId", model1);
                            if (result1 != null)
                            {
                                if (result1.IsApproved == true && result1.IsSubmitted == true)
                                {
                                    model.IsCorrisandumPending = true;
                                }
                                else
                                {
                                    model.IsCorrisandumPending = false;
                                }
                            }
                            else
                            {
                                if (Convert.ToBoolean(model.IsApproved))
                                {
                                    model.IsCorrisandumPending = true;
                                }
                            }

                            model.IsCorrigendum = true;

                            listLOBForDay.Add(model);
                        }
                    }


                    var newList = listLOBForDay.OrderByDescending(x => x.SessionDate).ThenByDescending(x => x.SubmittedDate).ThenByDescending(x => x.SubmittedTime).ToList();
                    objPendingLOB.ListLOBBYId = newList;
                }

                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }



            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("PartialSubmittedLOB", objPendingLOB);

        }

        #endregion

        #region Approval

        /// <summary>
        /// It will render the Approval View with two tabs Pending and Approved LOB
        /// Created By : himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult Approval()
        {
            CurrentSession.SetSessionAlive = null;
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            return View();
        }

        #endregion

        #region PartialPendingApprovedLOB

        /// <summary>
        /// It will render the PArtiaPendingApprovedLOB PartialView with the grid of Pending LOB for the Approval
        /// Created By : himanshu Gupta
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="RowsPerPage"></param>
        /// <param name="loopStart"></param>
        /// <param name="loopEnd"></param>
        /// <returns></returns>
        public ActionResult PartialPendingApprovedLOB(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();
            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectPendingApproveLOB]", methodParameter);

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        objLOB.SubmittedDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }

                        objLOB.PDFLocation = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedLOBPath"]);


                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                        int indx = objLOB.PDFLocation.IndexOf("~");
                        if (indx != -1)
                        {

                            string filstring = objLOB.PDFLocation.TrimStart('~');

                            objLOB.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + filstring);

                        }
                        else if (objLOB.PDFLocation != null && objLOB.PDFLocation != "")
                        {
                            objLOB.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + objLOB.PDFLocation);
                        }
                        //if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["IsCorrigendum"]) != "")
                        //{
                        //    objLOB.IsCorrigendum = Convert.ToBoolean(SummaryDataSet.Tables[0].Rows[i]["IsCorrigendum"]);
                        //}
                        //else
                        //{
                        //    objLOB.IsCorrigendum = false;
                        //}

                        if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]) != "")
                        {
                            TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]));
                            DateTime time = DateTime.Today.Add(interval);
                            string displayTime = time.ToString("hh:mm tt");

                            objLOB.SubmittedTime = displayTime;
                        }
                        else
                        {
                            objLOB.SubmittedTime = "";
                        }

                        int indexof = objLOB.PDFLocation.LastIndexOf('/');
                        objLOB.PPTLocation = objLOB.PDFLocation.Substring(indexof + 1);


                        listLOBForDay.Add(objLOB);
                    }


                    var result = (List<DraftLOB>)Helper.ExecuteService("LOB", "GetCorrigendumPendingApproval", null);

                    if (result != null)
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            LOBModel model = new LOBModel();
                            model.LOBId = Convert.ToString(result[i].LOBId);
                            model.AssemblyName = result[i].AssemblyName;
                            model.AssemblyNameLocal = result[i].AssemblyNameLocal;
                            model.SessionName = result[i].SessionName;
                            model.SessionNameLocal = result[i].SessionNameLocal;
                            model.SessionDate = Convert.ToString(result[i].SessionDate);
                            model.SessionDateLocal = result[i].SessionDateLocal;
                            model.SubmittedDate = Convert.ToString(result[i].SubmittedDate);
                            model.PDFLocation = result[i].SubmittedLOBPath;
                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                            int indx = model.PDFLocation.IndexOf("~");
                            if (indx != -1)
                            {

                                string filstring = model.PDFLocation.TrimStart('~');

                                model.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + filstring);

                            }
                            else if (model.PDFLocation != null && model.PDFLocation != "")
                            {
                                model.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + model.PDFLocation);
                            }

                            if (Convert.ToString(result[i].SubmittedTime) != "")
                            {
                                TimeSpan interval = TimeSpan.Parse(Convert.ToString(result[i].SubmittedTime));
                                DateTime time = DateTime.Today.Add(interval);
                                string displayTime = time.ToString("hh:mm tt");

                                model.SubmittedTime = displayTime;
                            }
                            else
                            {
                                model.SubmittedTime = "";
                            }

                            model.IsCorrigendum = true;

                            listLOBForDay.Add(model);
                        }
                    }


                    var newList = listLOBForDay.OrderByDescending(x => x.SessionDate).ThenByDescending(x => x.ApproverdDate).ThenByDescending(x => x.SubmittedTime).ToList();
                    objPendingLOB.ListLOBBYId = newList;
                }

                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("PartialPendingApprovedLOB", objPendingLOB);

        }

        #endregion

        #region PartialSubmittedApprovedLOB

        /// <summary>
        /// It will render the grid for approved LOB
        /// Created By : himanshu Gupta
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="RowsPerPage"></param>
        /// <param name="loopStart"></param>
        /// <param name="loopEnd"></param>
        /// <returns></returns>
        public ActionResult PartialSubmittedApprovedLOB(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();
            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSubmittedApprovedLOB]", methodParameter);

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        objLOB.ApproverdDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }


                        objLOB.PDFLocation = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBPath"]);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                        int indx = objLOB.PDFLocation.IndexOf("~");
                        if (indx != -1)
                        {

                            string filstring = objLOB.PDFLocation.TrimStart('~');

                            objLOB.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + filstring);

                        }
                        else if (objLOB.PDFLocation != null && objLOB.PDFLocation != "")
                        {
                            objLOB.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + objLOB.PDFLocation);
                        }
                        //if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["IsCorrigendum"]) != "")
                        //{
                        //    objLOB.IsCorrigendum = Convert.ToBoolean(SummaryDataSet.Tables[0].Rows[i]["IsCorrigendum"]);
                        //}
                        //else
                        //{
                        //    objLOB.IsCorrigendum = false;
                        //}

                        if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]) != "")
                        {
                            TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]));
                            DateTime time = DateTime.Today.Add(interval);
                            string displayTime = time.ToString("hh:mm tt");

                            objLOB.SubmittedTime = displayTime;
                        }
                        else
                        {
                            objLOB.SubmittedTime = "";
                        }

                        int indexof = objLOB.PDFLocation.LastIndexOf('/');
                        objLOB.PPTLocation = objLOB.PDFLocation.Substring(indexof + 1);

                        listLOBForDay.Add(objLOB);
                    }

                    var result = (List<AdminLOB>)Helper.ExecuteService("LOB", "GetCorrigendumSubmittedApprove", null);

                    if (result != null)
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            LOBModel model = new LOBModel();
                            model.LOBId = Convert.ToString(result[i].LOBId);
                            model.AssemblyName = result[i].AssemblyName;
                            model.AssemblyNameLocal = result[i].AssemblyNameLocal;
                            model.SessionName = result[i].SessionName;
                            model.SessionNameLocal = result[i].SessionNameLocal;
                            model.SessionDate = Convert.ToString(result[i].SessionDate);
                            model.SessionDateLocal = result[i].SessionDateLocal;
                            model.ApproverdDate = Convert.ToString(result[i].ApprovedDate);
                            model.PDFLocation = result[i].LOBPath;

                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);

                            if (model.PDFLocation != null)
                            {
                                int indx = model.PDFLocation.IndexOf("~");

                                if (indx != -1)
                                {

                                    string filstring = model.PDFLocation.TrimStart('~');

                                    model.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + filstring);

                                }
                                else if (model.PDFLocation != null && model.PDFLocation != "")
                                {
                                    model.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + model.PDFLocation);
                                }
                            }
                            if (Convert.ToString(result[i].ApprovedTime) != "")
                            {
                                TimeSpan interval = TimeSpan.Parse(Convert.ToString(result[i].ApprovedTime));
                                DateTime time = DateTime.Today.Add(interval);
                                string displayTime = time.ToString("hh:mm tt");

                                model.SubmittedTime = displayTime;
                            }
                            else
                            {
                                model.SubmittedTime = "";
                            }

                            model.IsCorrigendum = true;

                            listLOBForDay.Add(model);
                        }
                    }

                    var newList = listLOBForDay.OrderByDescending(x => x.SessionDate).ThenByDescending(x => x.ApproverdDate).ThenByDescending(x => x.SubmittedTime).ToList();
                    objPendingLOB.ListLOBBYId = newList;
                }

                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            return PartialView("PartialSubmittedApprovedLOB", objPendingLOB);

        }

        #endregion

        #region UpdatePageBreak

        public ActionResult UpdatePageBreak(string LOBId)
        {
            AddLOBModel addLOBModel = new AddLOBModel();
            try
            {
                if (LOBId != null && LOBId != "")
                {
                    List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));

                    DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDraftLOBReportByLOBId", methodParameter);
                    List<AddLOBModel> ListLOB = new List<AddLOBModel>();
#pragma warning disable CS0219 // The variable 'IncludeALL' is assigned but its value is never used
                    string IncludeALL = "";
#pragma warning restore CS0219 // The variable 'IncludeALL' is assigned but its value is never used
                    for (int i = 0; i < dataSetLOB.Tables[0].Rows.Count; i++)
                    {
                        AddLOBModel addLOBModel1 = new AddLOBModel();
                        string SrNo1 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo1"]);
                        if (SrNo1 != "")
                        {
                            addLOBModel1.SrNo11 = SrNo1;
                            SrNo1 = SrNo1 + ".";
                        }
                        string SrNo2 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo2"]);
                        if (SrNo2 != "")
                        {
                            addLOBModel1.SrNo22 = SrNo2;
                            SrNo2 = "(" + SrNo2 + ")";
                        }
                        string SrNo3 = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SrNo3"]);
                        if (SrNo3 != "")
                        {
                            addLOBModel1.SrNo33 = SrNo3;
                            SrNo3 = "(" + ToRoman(Convert.ToInt16(SrNo3)) + ")";
                        }
                        addLOBModel1.SrNo1 = SrNo1;
                        addLOBModel1.SrNo2 = SrNo2;
                        addLOBModel1.SrNo3 = SrNo3;
                        addLOBModel1.Sessiondate = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SessionDate"]);
                        addLOBModel1.SessionName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["SessionName"]);
                        addLOBModel1.AssemblyName = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["AssemblyName"]);
                        addLOBModel1.RecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                        addLOBModel1.LOBId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["LOBId"]);
                        addLOBModel1.TextLOB = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TextLOB"]);
                        addLOBModel1.TextSpeaker = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["TextSpeaker"]);
                        if (Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PageBreak"]) != "")
                        {
                            addLOBModel1.PageBreak = Convert.ToBoolean(dataSetLOB.Tables[0].Rows[i]["PageBreak"]);
                        }
                        else
                        {
                            addLOBModel1.PageBreak = false;
                        }
                        //if (addLOBModel1.TextSpeaker == "" || addLOBModel1.TextSpeaker == null)
                        //{
                        //    IncludeALL = "false";
                        //}

                        ListLOB.Add(addLOBModel1);
                    }
                    addLOBModel.LOBList = ListLOB;


                    //if (IncludeALL == "false")
                    //{
                    //    addLOBModel.PageBreak = false;
                    //}
                    //else
                    //{
                    //    addLOBModel.PageBreak = true;
                    //}


                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("UpdatePageBreak", addLOBModel);
        }

        #endregion

        #region "AddPageBreake"

        public JsonResult AddPageBreake(string RecordId)
        {
            if (RecordId != null && RecordId != "")
            {
                DraftLOB model = new DraftLOB();
                model.Id = Convert.ToInt16(RecordId);
                Helper.ExecuteService("LOB", "AddPageBreake", model);
            }
            var res2 = Json("teste", JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region "RemovePageBreake"

        public JsonResult RemovePageBreake(string RecordId)
        {
            if (RecordId != null && RecordId != "")
            {
                DraftLOB model = new DraftLOB();
                model.Id = Convert.ToInt16(RecordId);
                Helper.ExecuteService("LOB", "RemovePageBreake", model);
            }
            var res2 = Json("teste", JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region ReturnDraftLOB

        /// <summary>
        /// it will retuen the Dreft LOB back to dataentry
        /// Created By : himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public JsonResult ReturnDraftLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string result = "";
            try
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_ReturnDraftLOBById", methodParameter);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            var res2 = Json(result, JsonRequestBehavior.AllowGet);
            return res2;
        }


        #endregion

        #region ApproveDraftLOB

        /// <summary>
        /// For approve a LOB By LOBId
        /// Created By : himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public JsonResult ApproveDraftLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string result = "";
            try
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));

                DataSet dataSet1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionDateByLobId", methodParameter);
                if (dataSet1 != null)
                {
                    String sessdate = Convert.ToString(dataSet1.Tables[0].Rows[0]["SessionDate"]);
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", sessdate));

                }

                methodParameter.Add(new KeyValuePair<string, string>("@ApprovedBy", Utility.CurrentSession.UserID));
                methodParameter.Add(new KeyValuePair<string, string>("@ApprovedDate", Convert.ToString(System.DateTime.Now)));
                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertApprovedLOB", methodParameter);
                GenerateLOBPdf(LOBId, true, "Approved");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            var res2 = Json(result, JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region Publish

        /// <summary>
        /// render the Publish View with two tabs Pending and Published
        /// Created By : himanshu Gupta
        /// </summary>
        /// <returns></returns>
        public ActionResult Publish()
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            return View();
        }

        #endregion

        #region PartialPendingPublishLOB

        /// <summary>
        /// render pending publish LOB View with grid of pending LOB for the Publishing
        /// Created By : himanshu Gupta
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="RowsPerPage"></param>
        /// <param name="loopStart"></param>
        /// <param name="loopEnd"></param>
        /// <returns></returns>
        public ActionResult PartialPendingPublishLOB(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();
            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectPendingPublishLOB]", methodParameter);

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        objLOB.ApproverdDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }
                        listLOBForDay.Add(objLOB);
                    }

                    objPendingLOB.ListLOBBYId = listLOBForDay;
                }

                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }

            }
            catch
            {

            }

            return PartialView("PartialPendingPublishLOB", objPendingLOB);

        }

        #endregion

        #region ReturnApprovedLOB

        /// <summary>
        /// for return an approved LOB by Publishing User
        /// Created By : himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public JsonResult ReturnApprovedLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string result = "";
            try
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_ReturnApprovedLOBById", methodParameter);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            var res2 = Json(result, JsonRequestBehavior.AllowGet);
            return res2;
        }


        #endregion

        #region PublishApprovedLOB

        /// <summary>
        /// For publish the approved LOB 
        /// Created By : himanshu Gupta
        /// </summary>
        /// <param name="LOBId"></param>
        /// <returns></returns>
        public JsonResult PublishApprovedLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string result = "";
            try
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                methodParameter.Add(new KeyValuePair<string, string>("@PublishedBy", Utility.CurrentSession.UserID));
                methodParameter.Add(new KeyValuePair<string, string>("@PublishedDate", Convert.ToString(System.DateTime.Now)));
                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertPublishLOB", methodParameter);
                GenerateLOBPdf(LOBId, true, "Published");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            var res2 = Json(result, JsonRequestBehavior.AllowGet);
            return res2;
        }

        #endregion

        #region PartialPublishedLOB

        /// <summary>
        /// Render the grid with Published LOB
        /// 
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="RowsPerPage"></param>
        /// <param name="loopStart"></param>
        /// <param name="loopEnd"></param>
        /// <returns></returns>
        public ActionResult PartialPublishedLOB(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();
            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectPublishedLOB]", methodParameter);

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        objLOB.ApproverdDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }
                        listLOBForDay.Add(objLOB);
                    }

                    objPendingLOB.ListLOBBYId = listLOBForDay;
                }

                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }

            return PartialView("PartialPublishedLOB", objPendingLOB);

        }

        #endregion

        #region "LOB As Pdf"
        public ActionResult LOBAsPdf(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            if (LOBId != null && LOBId != "")
            {
                GeneratePdf(LOBId);
            }
            else
            {
                GeneratePdf("1");
            }

            return View();
        }

        [HttpGet]
       
        public FileStreamResult GenerateLOBPdf(string LOBId, bool download, string LOBType)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string LOBName = "20Dec2013_1_LOB";
            string CurrentSecretery = "";
            string CurrentSpeaker = "";
            string CurrenttSessionPlace = "";
            DateTime SessionDate = new DateTime();
            string savedPDF = string.Empty;


            MemoryStream output = new MemoryStream();
            if (LOBId != null && LOBId != "")
            {
                LOBModel objLOBModel = new LOBModel();
                DataSet SummaryDataSet = new DataSet();
                string sessiondate = "";
                string outXml = "";
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));

                if (LOBType == "Draft")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }
                else if (LOBType == "Approved")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
                }
                else
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }


                //Getting Current Secretary
                DataSet SiteSettingDataSet = new DataSet();
                var methodParameter1 = new List<KeyValuePair<string, string>>();
                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
                {
                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
                    if (dtSiteSetting != null)
                    {
                        IEnumerable<DataRow> siteSettingQuery = from siteSetting in dtSiteSetting.AsEnumerable() select siteSetting;


                        IEnumerable<DataRow> curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
                        }

                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSpeaker");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSpeaker = obj.Field<string>("SettingValueLocal");
                        }

                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "SessionPlace");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrenttSessionPlace = obj.Field<string>("SettingValueLocal");
                        }
                    }
                }
#pragma warning disable CS0219 // The variable 'sessionplace' is assigned but its value is never used
                string sessionplace = "";
#pragma warning restore CS0219 // The variable 'sessionplace' is assigned but its value is never used
                string SubittedDate = "";
                string SubittedTime = "";
                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {

                    outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    string SrNo1 = "";
                    string SrNo2 = "";
                    string SrNo3 = "";
                    int SrNo1Count = 1;
                    int SrNo2Count = 1;
                    int SrNo3Count = 1;
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {

                        if (i == 0)
                        {
                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            string date = ConvertSQLDate(SessionDate);
                            date = date.Replace("/", "");
                            date = date.Replace("-", "");
                            //SessionDate = DateTime.ParseExact(sessiondate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                            sessiondate = sessiondate.Replace('/', ' ');
                            // SessionDate = SessionDate.Replace('/', ' ');                     
                            //TempData["addLines"] = sessiondate;

                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


                            outXml += @"<tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
                                                         हिमाचल प्रदेश&nbsp;" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
                                                          " + "कार्यसूची" + @"    
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold'>
                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;'>               
                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;text-decoration: underline;'>               
                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
                                                    </td>
                                                </tr>";

                            if (LOBType == "Draft")
                            {
                                if (SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"] != null && Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]) != "")
                                {
                                    SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]).ToString("D", new CultureInfo("hi")); ;
                                }
                                else
                                {
                                    SubittedDate = System.DateTime.Now.ToString("dd/MM/yyyy");
                                }

                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]) != "")
                                {
                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]));
                                    DateTime time = DateTime.Today.Add(interval);
                                    string displayTime = time.ToString("hh:mm tt");

                                    SubittedTime = displayTime;
                                }
                                else
                                {
                                    SubittedTime = "";
                                }
                            }
                            else
                            {

                                SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]).ToString("D", new CultureInfo("hi"));
                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]) != "")
                                {
                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]));
                                    DateTime time = DateTime.Today.Add(interval);
                                    string displayTime = time.ToString("hh:mm tt");

                                    SubittedTime = displayTime;
                                }
                                else
                                {
                                    SubittedTime = "";
                                }
                            }

                        }


                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
                            outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
                                     <div style='padding-left:10px;text-align:justify;'>
                                  <b>  " + SrNo1 + @" </b> &nbsp;&nbsp;&nbsp; <div style='padding-left:20px; margin: -34px 0 0 2%;text-align:justify;'>";
                            SrNo1Count = SrNo1Count + 1;
                            SrNo2Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:30px;text-align:justify;'>
                                    <b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px; margin: -31px 0 0 4.5%;text-align:justify;'>";
                            SrNo2Count = SrNo2Count + 1;
                            SrNo3Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
                        {

                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:60px;text-align:justify;'>
                                    <b>" + SrNo3 + @"</b>&nbsp;&nbsp;&nbsp; <div style='padding-left:15px; margin: -31px 0 0 6%;text-align:justify;'>";
                            SrNo3Count = SrNo3Count + 1;

                        }

                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        //string Member = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberDesignationLocal"]) + ")";
                        //string Minister = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterDesignationLocal"]) + ")";
                        //string Department = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedDeptNameLocal"]);
                        //string Committee = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedCommitteeNameLocal"]);
                        //string Rule = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedRuleNameLocal"]);
                        outXml += @"" + LOBText + "</div></div> ";
                        //if (Member != " ()")
                        //{
                        //    outXml += @"<b>  सदस्य: </b>" + Member;
                        //}
                        //if (Department != "")
                        //{
                        //    outXml += @"<b>  विभाग: </b> " + Department;
                        //    if (Minister != "")
                        //    {
                        //        outXml += @"<b>  मंत्री: </b> " + Minister;
                        //    }
                        //}
                        //if (Committee != "")
                        //{
                        //    outXml += @"<b>  समिति: </b> " + Committee;
                        //}
                        //if (Rule != "")
                        //{
                        //    outXml += @"<b>  नियम: </b> " + Rule;
                        //}

                        outXml += @"<br/> </td></tr>";


                    }

                    if (LOBType == "Draft")
                    {
                        //Bottom Section
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;'>
                                               <b> (" + CurrentSpeaker + @"),</b>
                                                </td>
                                                <td style='width:25%;float:left;font-size: 24px;'>
                                                <b> (" + CurrentSecretery + @"),  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;padding-left:15%;'>
                                               <b> अध्यक्ष ।</b>
                                                </td>
                                                <td style='width:25%;float:right;text-align:right;padding-right:80px;font-size: 24px;'>
                                               <b>  सचिव।</b>
                                                </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }
                    else if (LOBType == "Approved")
                    {
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;'>
                                     
                                                    <b> " + CurrenttSessionPlace + @",</b>
                                                </td>
                                                <td style='width:25%;float:left;font-size: 24px;'>
                                                <b> " + CurrentSecretery + @",  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;'>
                                               <b> दिनांक: " + SubittedDate + @"</b>
                                               <b> &nbsp;&nbsp;" + SubittedTime + @"</b>
                                                </td>
                                                <td style='width:25%;float:right;text-align:right;padding-right:85px;font-size: 24px;'>
                                               <b>  सचिव।</b>
                                                </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }
                }

                // convert HTML to PDF
                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(40, 45, 20, 20);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(10, 45, 55, 55), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(40, 20, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }



                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msInput, null);
                //document.Close();

                //byte[] file = ms.ToArray();
                //output.Write(file, 0, file.Length);
                //output.Position = 0;


                ///To get File Path

                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";


                //sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                //sessiondate = sessiondate.Replace('/', ' ');



                if (download)
                {
                    if (LOBType == "Draft")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_LOB.pdf");
                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        // string directory = Server.MapPath(url);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(directory);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Draft_LOB.pdf";
                        //  var path = Path.Combine(Server.MapPath("~" + url), fileName);//sanjay for FileStructure
                        var path = Path.Combine(directory, fileName);
                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");

                        #endregion

                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", dbpath + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedLOBPath]", methodParameter);
                        return null;
                    }
                    else if (LOBType == "Approved")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Approved_LOB.pdf");
                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        // string directory = Server.MapPath(url);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(directory);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Approved_LOB.pdf";
                        var path = Path.Combine(directory, fileName);
                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion

                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", dbpath + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedApprovedLOBPath]", methodParameter);

                        //string pathfileforsign = url + fileName;
                        //string imageFileLoc = Server.MapPath("/SignaturePath/246639196407.gif");
                        //fileName = "Approved_LOB_Sign.pdf";
                        //string newFileLocation = url + fileName;

                        //PDFExtensions.InsertImageToPdf(Server.MapPath(pathfileforsign), imageFileLoc, Server.MapPath(newFileLocation), 4, 4);


                        return null;
                    }
                    else if (LOBType == "Published")
                    {

                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Published_" + LOBId + ".pdf");
                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(directory);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Published_" + LOBId + ".pdf";
                        var path = Path.Combine(directory, fileName);
                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion

                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@PublishLOBPath", dbpath + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_PublishedLOBPath]", methodParameter);
                        return null;
                    }
                }
            }

            return File(output, "application/pdf");
        }


        //old code commented
//        public FileStreamResult GenerateLOBPdf(string LOBId, bool download, string LOBType)
//        {
//            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
//            string LOBName = "20Dec2013_1_LOB";
//            string CurrentSecretery = "";
//            string CurrentSpeaker = "";

//            DateTime SessionDate = new DateTime();
//            string savedPDF = string.Empty;


//            MemoryStream output = new MemoryStream();
//            if (LOBId != null && LOBId != "")
//            {
//                LOBModel objLOBModel = new LOBModel();
//                DataSet SummaryDataSet = new DataSet();
//                string sessiondate = "";
//                string outXml = "";
//                var methodParameter = new List<KeyValuePair<string, string>>();
//                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));

//                if (LOBType == "Draft")
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
//                }
//                else if (LOBType == "Approved")
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
//                }
//                else
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
//                }


//                //Getting Current Secretary
//                DataSet SiteSettingDataSet = new DataSet();
//                var methodParameter1 = new List<KeyValuePair<string, string>>();
//                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
//                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
//                {
//                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
//                    if (dtSiteSetting != null)
//                    {
//                        IEnumerable<DataRow> siteSettingQuery =
//                                from siteSetting in dtSiteSetting.AsEnumerable()
//                                select siteSetting;
//                        IEnumerable<DataRow> curtSecretery =
//                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
//                        foreach (DataRow obj in curtSecretery)
//                        {
//                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
//                        }
//                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSpeaker");
//                        foreach (DataRow obj in curtSecretery)
//                        {
//                            CurrentSpeaker = obj.Field<string>("SettingValueLocal");
//                        }
//                    }
//                }

//                string SubittedDate = "";
//                string SubittedTime = "";
//                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
//                {

//                    outXml = @"<html>                           
//                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

//                    List<LOBModel> listLOBForDay = new List<LOBModel>();
//                    string SrNo1 = "";
//                    string SrNo2 = "";
//                    string SrNo3 = "";
//                    int SrNo1Count = 1;
//                    int SrNo2Count = 1;
//                    int SrNo3Count = 1;
//                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
//                    {

//                        if (i == 0)
//                        {
//                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
//                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
//                            string date = ConvertSQLDate(SessionDate);
//                            date = date.Replace("/", "");
//                            date = date.Replace("-", "");
//                            //SessionDate = DateTime.ParseExact(sessiondate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
//                            sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
//                            sessiondate = sessiondate.Replace('/', ' ');
//                            // SessionDate = SessionDate.Replace('/', ' ');                     
//                            //TempData["addLines"] = sessiondate;

//                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


//                            outXml += @"<tr>
//                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
//                                                         हिमाचल प्रदेश&nbsp;" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
//                                                    </td>
//                                                </tr>
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
//                                                          " + "कार्यसूची" + @"    
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold'>
//                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
//                                                    </td>
//                                                </tr>
//
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;'>               
//                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;text-decoration: underline;'>               
//                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
//                                                    </td>
//                                                </tr>";

//                            if (LOBType == "Draft")
//                            {
//                                if (SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"] != null && Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]) != "")
//                                {
//                                    SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]).ToString("D", new CultureInfo("hi")); ;
//                                }
//                                else
//                                {
//                                    SubittedDate = System.DateTime.Now.ToString("dd/MM/yyyy");
//                                }

//                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]) != "")
//                                {
//                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]));
//                                    DateTime time = DateTime.Today.Add(interval);
//                                    string displayTime = time.ToString("hh:mm tt");

//                                    SubittedTime = displayTime;
//                                }
//                                else
//                                {
//                                    SubittedTime = "";
//                                }
//                            }
//                            else
//                            {

//                                SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]).ToString("D", new CultureInfo("hi"));
//                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]) != "")
//                                {
//                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]));
//                                    DateTime time = DateTime.Today.Add(interval);
//                                    string displayTime = time.ToString("hh:mm tt");

//                                    SubittedTime = displayTime;
//                                }
//                                else
//                                {
//                                    SubittedTime = "";
//                                }
//                            }

//                        }


//                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
//                        {

//                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
//                            outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
//                                     <div style='padding-left:10px;text-align:justify;'>
//                                  <b>  " + SrNo1 + @" </b> &nbsp;&nbsp;&nbsp; <div style='padding-left:20px; margin: -34px 0 0 2%;text-align:justify;'>";
//                            SrNo1Count = SrNo1Count + 1;
//                            SrNo2Count = 1;


//                        }
//                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
//                        {

//                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
//                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:30px;text-align:justify;'>
//                                    <b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px; margin: -31px 0 0 4.5%;text-align:justify;'>";
//                            SrNo2Count = SrNo2Count + 1;
//                            SrNo3Count = 1;


//                        }
//                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
//                        {

//                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
//                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:60px;text-align:justify;'>
//                                    <b>" + SrNo3 + @"</b>&nbsp;&nbsp;&nbsp; <div style='padding-left:15px; margin: -31px 0 0 6%;text-align:justify;'>";
//                            SrNo3Count = SrNo3Count + 1;

//                        }

//                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
//                        if (LOBText != "")
//                        {
//                            LOBText = LOBText.Replace("<p>", "");
//                            LOBText = LOBText.Replace("</p>", "");
//                        }

//                        //string Member = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberDesignationLocal"]) + ")";
//                        //string Minister = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterDesignationLocal"]) + ")";
//                        //string Department = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedDeptNameLocal"]);
//                        //string Committee = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedCommitteeNameLocal"]);
//                        //string Rule = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedRuleNameLocal"]);
//                        outXml += @"" + LOBText + "</div></div> ";
//                        //if (Member != " ()")
//                        //{
//                        //    outXml += @"<b>  सदस्य: </b>" + Member;
//                        //}
//                        //if (Department != "")
//                        //{
//                        //    outXml += @"<b>  विभाग: </b> " + Department;
//                        //    if (Minister != "")
//                        //    {
//                        //        outXml += @"<b>  मंत्री: </b> " + Minister;
//                        //    }
//                        //}
//                        //if (Committee != "")
//                        //{
//                        //    outXml += @"<b>  समिति: </b> " + Committee;
//                        //}
//                        //if (Rule != "")
//                        //{
//                        //    outXml += @"<b>  नियम: </b> " + Rule;
//                        //}

//                        outXml += @"<br/> </td></tr>";


//                    }

//                    if (LOBType == "Draft")
//                    {
//                        //Bottom Section
//                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>
//
//                                                <tr><td>
//                                                <table style='width:100%;'>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;'>
//                                               <b> (" + CurrentSpeaker + @"),</b>
//                                                </td>
//                                                <td style='width:25%;float:left;font-size: 24px;'>
//                                                <b> (" + CurrentSecretery + @"),  </b>
//                                                </td>
//                                                </tr>
//                                                <br/>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;padding-left:15%;'>
//                                               <b> अध्यक्ष ।</b>
//                                                </td>
//                                                <td style='width:25%;float:right;text-align:right;padding-right:80px;font-size: 24px;'>
//                                               <b>  सचिव।</b>
//                                                </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
//                                                          " + "******************" + @"    
//                                                    </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
//                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
//                                                    </td>
//                                                </tr>
//                                                </table>           
//                                                    </td>
//                                                    </tr>";

//                        outXml += "</table></div></div> </body> </html>";
//                    }
//                    else if (LOBType == "Approved")
//                    {
//                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>
//
//                                                <tr><td>
//                                                <table style='width:100%;'>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;'>
//                                               <b> शिमला-171004</b>
//                                                </td>
//                                                <td style='width:25%;float:left;font-size: 24px;'>
//                                                <b> " + CurrentSecretery + @",  </b>
//                                                </td>
//                                                </tr>
//                                                <br/>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;'>
//                                               <b> दिनांक: " + SubittedDate + @"</b>
//                                               <b> &nbsp;&nbsp;" + SubittedTime + @"</b>
//                                                </td>
//                                                <td style='width:25%;float:right;text-align:right;padding-right:85px;font-size: 24px;'>
//                                               <b>  सचिव।</b>
//                                                </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
//                                                          " + "******************" + @"    
//                                                    </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
//                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
//                                                    </td>
//                                                </tr>
//                                                </table>           
//                                                    </td>
//                                                    </tr>";

//                        outXml += "</table></div></div> </body> </html>";
//                    }
//                }

//                // convert HTML to PDF
//                EvoPdf.Document document1 = new EvoPdf.Document();

//                // set the license key
//                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
//                document1.CompressionLevel = PdfCompressionLevel.Best;
//                document1.Margins = new Margins(40, 45, 20, 20);
//                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(10, 45, 55, 55), PdfPageOrientation.Portrait);

//                AddElementResult addResult;

//                HtmlToPdfElement htmlToPdfElement;
//                string htmlStringToConvert = outXml;
//                string baseURL = "";
//                htmlToPdfElement = new HtmlToPdfElement(40, 20, 0, 0, htmlStringToConvert, baseURL);

//                addResult = page.AddElement(htmlToPdfElement);
//                byte[] pdfBytes = document1.Save();

//                try
//                {
//                    output.Write(pdfBytes, 0, pdfBytes.Length);
//                    output.Position = 0;

//                }
//                finally
//                {
//                    // close the PDF document to release the resources
//                    document1.Close();
//                }



//                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msInput, null);
//                //document.Close();

//                //byte[] file = ms.ToArray();
//                //output.Write(file, 0, file.Length);
//                //output.Position = 0;


//                ///To get File Path

//                methodParameter = new List<KeyValuePair<string, string>>();
//                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
//                string CurrentAssembly = "";
//                string Currentsession = "";


//                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
//                {
//                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
//                    {
//                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
//                    }
//                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
//                    {
//                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
//                    }
//                }

//                string fileName = "";


//                //sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
//                //sessiondate = sessiondate.Replace('/', ' ');



//                if (download)
//                {
//                    if (LOBType == "Draft")
//                    {
//                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_LOB.pdf");
//                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        // string directory = Server.MapPath(url);
//                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
//                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        DirectoryInfo Dir = new DirectoryInfo(directory);
//                        if (!Dir.Exists)
//                        {
//                            Dir.Create();
//                        }
//                        fileName = "Draft_LOB.pdf";
//                        //  var path = Path.Combine(Server.MapPath("~" + url), fileName);//sanjay for FileStructure
//                        var path = Path.Combine(directory, fileName);
//                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
//                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

//                        // close file stream
//                        _FileStream.Close();


//                        #region FileTransfer save

//                        //int fileLength = _FileStream.ContentLength;

//                        //byte[] myData = new byte[fileLength];
//                        //file.InputStream.Read(myData, 0, fileLength);
//                        // Gor in Binary Data
//                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
//                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
//                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
//                        sers.Serialize(wr, pdfBytes);
//                        XmlDocument doc = new XmlDocument();
//                        doc.LoadXml(sb.ToString());

//                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");

//                        #endregion

//                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
//                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", dbpath + fileName));
//                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedLOBPath]", methodParameter);
//                        return null;
//                    }
//                    else if (LOBType == "Approved")
//                    {
//                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Approved_LOB.pdf");
//                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        // string directory = Server.MapPath(url);
//                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
//                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        DirectoryInfo Dir = new DirectoryInfo(directory);
//                        if (!Dir.Exists)
//                        {
//                            Dir.Create();
//                        }
//                        fileName = "Approved_LOB.pdf";
//                        var path = Path.Combine(directory, fileName);
//                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
//                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

//                        // close file stream
//                        _FileStream.Close();


//                        #region FileTransfer save

//                        //int fileLength = _FileStream.ContentLength;

//                        //byte[] myData = new byte[fileLength];
//                        //file.InputStream.Read(myData, 0, fileLength);
//                        // Gor in Binary Data
//                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
//                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
//                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
//                        sers.Serialize(wr, pdfBytes);
//                        XmlDocument doc = new XmlDocument();
//                        doc.LoadXml(sb.ToString());

//                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
//                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

//                        #endregion

//                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
//                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", dbpath + fileName));
//                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedApprovedLOBPath]", methodParameter);

//                        //string pathfileforsign = url + fileName;
//                        //string imageFileLoc = Server.MapPath("/SignaturePath/246639196407.gif");
//                        //fileName = "Approved_LOB_Sign.pdf";
//                        //string newFileLocation = url + fileName;

//                        //PDFExtensions.InsertImageToPdf(Server.MapPath(pathfileforsign), imageFileLoc, Server.MapPath(newFileLocation), 4, 4);


//                        return null;
//                    }
//                    else if (LOBType == "Published")
//                    {

//                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Published_" + LOBId + ".pdf");
//                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
//                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        DirectoryInfo Dir = new DirectoryInfo(directory);
//                        if (!Dir.Exists)
//                        {
//                            Dir.Create();
//                        }
//                        fileName = "Published_" + LOBId + ".pdf";
//                        var path = Path.Combine(directory, fileName);
//                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
//                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

//                        // close file stream
//                        _FileStream.Close();


//                        #region FileTransfer save

//                        //int fileLength = _FileStream.ContentLength;

//                        //byte[] myData = new byte[fileLength];
//                        //file.InputStream.Read(myData, 0, fileLength);
//                        // Gor in Binary Data
//                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
//                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
//                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
//                        sers.Serialize(wr, pdfBytes);
//                        XmlDocument doc = new XmlDocument();
//                        doc.LoadXml(sb.ToString());

//                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
//                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

//                        #endregion

//                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
//                        methodParameter.Add(new KeyValuePair<string, string>("@PublishLOBPath", dbpath + fileName));
//                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_PublishedLOBPath]", methodParameter);
//                        return null;
//                    }
//                }
//            }

//            return File(output, "application/pdf");
//        }

        ///new method generated by dharmendra for lob as Doc
        ///



        [HttpGet]
        public ActionResult GenerateLOBDoc(string LOBId, bool download, string LOBType)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string LOBName = "20Dec2013_1_LOB";
            string CurrentSecretery = "";
            string CurrentSpeaker = "";
            string CurrenttSessionPlace = "";
            DateTime SessionDate = new DateTime();
            string savedPDF = string.Empty;


            MemoryStream output = new MemoryStream();
            if (LOBId != null && LOBId != "")
            {
                LOBModel objLOBModel = new LOBModel();
                DataSet SummaryDataSet = new DataSet();
                string sessiondate = "";
                string outXml = "";
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));

                if (LOBType == "Draft")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }
                else if (LOBType == "Approved")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
                }
                else
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }


                //Getting Current Secretary
                DataSet SiteSettingDataSet = new DataSet();
                var methodParameter1 = new List<KeyValuePair<string, string>>();
                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
                {
                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
                    if (dtSiteSetting != null)
                    {
                        IEnumerable<DataRow> siteSettingQuery =
                                from siteSetting in dtSiteSetting.AsEnumerable()
                                select siteSetting;
                        IEnumerable<DataRow> curtSecretery =
                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
                        }
                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSpeaker");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSpeaker = obj.Field<string>("SettingValueLocal");
                        }
                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "SessionPlace");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrenttSessionPlace = obj.Field<string>("SettingValueLocal");
                        }
                    }
                }

                string SubittedDate = "";
                string SubittedTime = "";
                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {

                    outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 80%;margin-left: 10%;margin-right: 10%;'><table style='width: 100%;'>";

                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    string SrNo1 = "";
                    string SrNo2 = "";
                    string SrNo3 = "";
                    int SrNo1Count = 1;
                    int SrNo2Count = 1;
                    int SrNo3Count = 1;
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {

                        if (i == 0)
                        {
                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            string date = ConvertSQLDate(SessionDate);
                            date = date.Replace("/", "");
                            date = date.Replace("-", "");
                            //SessionDate = DateTime.ParseExact(sessiondate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                            sessiondate = sessiondate.Replace('/', ' ');
                            // SessionDate = SessionDate.Replace('/', ' ');                     
                            //TempData["addLines"] = sessiondate;

                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


                            outXml += @"<tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
                                                         हिमाचल प्रदेश&nbsp;" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
                                                          " + "कार्यसूची" + @"    
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold'>
                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;'>               
                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;text-decoration: underline;'>               
                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
                                                    </td>
                                                </tr>";

                            if (LOBType == "Draft")
                            {
                                if (SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"] != null && Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]) != "")
                                {
                                    SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]).ToString("D", new CultureInfo("hi")); ;
                                }
                                else
                                {
                                    SubittedDate = System.DateTime.Now.ToString("dd/MM/yyyy");
                                }

                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]) != "")
                                {
                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]));
                                    DateTime time = DateTime.Today.Add(interval);
                                    string displayTime = time.ToString("hh:mm tt");

                                    SubittedTime = displayTime;
                                }
                                else
                                {
                                    SubittedTime = "";
                                }
                            }
                            else
                            {

                                SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]).ToString("D", new CultureInfo("hi"));
                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]) != "")
                                {
                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]));
                                    DateTime time = DateTime.Today.Add(interval);
                                    string displayTime = time.ToString("hh:mm tt");

                                    SubittedTime = displayTime;
                                }
                                else
                                {
                                    SubittedTime = "";
                                }
                            }

                        }


                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
                            outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
                                     <div style='padding-left:10px;text-align:justify;'>
                                  <b>  " + SrNo1 + @" </b>";
                            SrNo1Count = SrNo1Count + 1;
                            SrNo2Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo2 = "" + "(" + Convert.ToString(SrNo2Count) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:30px;text-align:justify;'>
                                    &nbsp; &nbsp; &nbsp;<b>" + SrNo2 + @"</b>";
                            SrNo2Count = SrNo2Count + 1;
                            SrNo3Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
                        {

                            SrNo3 = "" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:60px;text-align:justify;'>
                                     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>" + SrNo3 + @"</b> ";
                            SrNo3Count = SrNo3Count + 1;

                        }

                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }


                        outXml += @"" + LOBText + "</div> ";


                        outXml += @"<br/> </td></tr>";


                    }

                    if (LOBType == "Draft")
                    {
                        //Bottom Section
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;'>
                                               <b> (" + CurrentSpeaker + @"),</b>
                                                </td>
                                                <td style='width:25%;float:left;font-size: 24px;'>
                                                <b> (" + CurrentSecretery + @"),  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;padding-left:15%;'>
                                               <b> अध्यक्ष ।</b>
                                                </td>
                                                <td style='width:25%;float:right;text-align:right;padding-right:80px;font-size: 24px;'>
                                               <b>  सचिव।</b>
                                                </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }
                    else if (LOBType == "Approved")
                    {
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;'>
                                            <b> " + CurrenttSessionPlace + @",</b>
                                                </td>
                                                <td style='width:25%;float:left;font-size: 24px;'>
                                                <b> " + CurrentSecretery + @",  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;'>
                                               <b> दिनांक: " + SubittedDate + @"</b>
                                               <b> &nbsp;&nbsp;" + SubittedTime + @"</b>
                                                </td>
                                                <td style='width:25%;float:right;text-align:right;padding-right:85px;font-size: 24px;'>
                                               <b>  सचिव।</b>
                                                </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }
                }


                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";

                ViewData["HTMLData"] = outXml;

                if (LOBType == "Draft")
                {
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                    DirectoryInfo Dir = new DirectoryInfo(directory);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    fileName = "Draft_LOB.doc";
                    var path = Path.Combine(directory, fileName);
                    string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                    FileStream fStream = System.IO.File.Create(path);
                    fStream.Close();
                    StreamWriter sWriter = new StreamWriter(path);
                    sWriter.Write(outXml);
                    sWriter.Close();

                    outXml = HttpUtility.UrlDecode(outXml);
                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment;filename=Draft_LOB.doc");
                    Response.Charset = "";
                    Response.ContentType = "application/doc";
                    Response.Write(outXml);
                    Response.Flush();
                    Response.End();
                }
                else if (LOBType == "Approved")
                {

                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                    DirectoryInfo Dir = new DirectoryInfo(directory);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    fileName = "Approved_LOB.doc";
                    var path = Path.Combine(directory, fileName);
                    string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                    FileStream fStream = System.IO.File.Create(path);
                    fStream.Close();
                    StreamWriter sWriter = new StreamWriter(path);
                    sWriter.Write(outXml);
                    sWriter.Close();
                    outXml = HttpUtility.UrlDecode(outXml);
                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment;filename=Approved_LOB.doc");
                    Response.Charset = "";
                    Response.ContentType = "application/doc";
                    Response.Write(outXml);
                    Response.Flush();
                    Response.End();
                }

            }

            return null;//File(output, "application/doc");
        }
//old code commented
//        public ActionResult GenerateLOBDoc(string LOBId, bool download, string LOBType)
//        {
//            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
//            string LOBName = "20Dec2013_1_LOB";
//            string CurrentSecretery = "";
//            string CurrentSpeaker = "";

//            DateTime SessionDate = new DateTime();
//            string savedPDF = string.Empty;


//            MemoryStream output = new MemoryStream();
//            if (LOBId != null && LOBId != "")
//            {
//                LOBModel objLOBModel = new LOBModel();
//                DataSet SummaryDataSet = new DataSet();
//                string sessiondate = "";
//                string outXml = "";
//                var methodParameter = new List<KeyValuePair<string, string>>();
//                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));

//                if (LOBType == "Draft")
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
//                }
//                else if (LOBType == "Approved")
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
//                }
//                else
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
//                }


//                //Getting Current Secretary
//                DataSet SiteSettingDataSet = new DataSet();
//                var methodParameter1 = new List<KeyValuePair<string, string>>();
//                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
//                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
//                {
//                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
//                    if (dtSiteSetting != null)
//                    {
//                        IEnumerable<DataRow> siteSettingQuery =
//                                from siteSetting in dtSiteSetting.AsEnumerable()
//                                select siteSetting;
//                        IEnumerable<DataRow> curtSecretery =
//                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
//                        foreach (DataRow obj in curtSecretery)
//                        {
//                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
//                        }
//                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSpeaker");
//                        foreach (DataRow obj in curtSecretery)
//                        {
//                            CurrentSpeaker = obj.Field<string>("SettingValueLocal");
//                        }
//                    }
//                }

//                string SubittedDate = "";
//                string SubittedTime = "";
//                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
//                {

//                    outXml = @"<html>                           
//                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 80%;margin-left: 10%;margin-right: 10%;'><table style='width: 100%;'>";

//                    List<LOBModel> listLOBForDay = new List<LOBModel>();
//                    string SrNo1 = "";
//                    string SrNo2 = "";
//                    string SrNo3 = "";
//                    int SrNo1Count = 1;
//                    int SrNo2Count = 1;
//                    int SrNo3Count = 1;
//                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
//                    {

//                        if (i == 0)
//                        {
//                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
//                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
//                            string date = ConvertSQLDate(SessionDate);
//                            date = date.Replace("/", "");
//                            date = date.Replace("-", "");
//                            //SessionDate = DateTime.ParseExact(sessiondate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
//                            sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
//                            sessiondate = sessiondate.Replace('/', ' ');
//                            // SessionDate = SessionDate.Replace('/', ' ');                     
//                            //TempData["addLines"] = sessiondate;

//                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


//                            outXml += @"<tr>
//                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
//                                                         हिमाचल प्रदेश&nbsp;" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
//                                                    </td>
//                                                </tr>
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
//                                                          " + "कार्यसूची" + @"    
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold'>
//                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
//                                                    </td>
//                                                </tr>
//
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;'>               
//                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;text-decoration: underline;'>               
//                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
//                                                    </td>
//                                                </tr>";

//                            if (LOBType == "Draft")
//                            {
//                                if (SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"] != null && Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]) != "")
//                                {
//                                    SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]).ToString("D", new CultureInfo("hi")); ;
//                                }
//                                else
//                                {
//                                    SubittedDate = System.DateTime.Now.ToString("dd/MM/yyyy");
//                                }

//                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]) != "")
//                                {
//                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]));
//                                    DateTime time = DateTime.Today.Add(interval);
//                                    string displayTime = time.ToString("hh:mm tt");

//                                    SubittedTime = displayTime;
//                                }
//                                else
//                                {
//                                    SubittedTime = "";
//                                }
//                            }
//                            else
//                            {

//                                SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]).ToString("D", new CultureInfo("hi"));
//                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]) != "")
//                                {
//                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]));
//                                    DateTime time = DateTime.Today.Add(interval);
//                                    string displayTime = time.ToString("hh:mm tt");

//                                    SubittedTime = displayTime;
//                                }
//                                else
//                                {
//                                    SubittedTime = "";
//                                }
//                            }

//                        }


//                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
//                        {

//                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
//                            outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
//                                     <div style='padding-left:10px;text-align:justify;'>
//                                  <b>  " + SrNo1 + @" </b>";
//                            SrNo1Count = SrNo1Count + 1;
//                            SrNo2Count = 1;


//                        }
//                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
//                        {

//                            SrNo2 = "" + "(" + Convert.ToString(SrNo2Count) + ")";
//                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:30px;text-align:justify;'>
//                                    &nbsp; &nbsp; &nbsp;<b>" + SrNo2 + @"</b>";
//                            SrNo2Count = SrNo2Count + 1;
//                            SrNo3Count = 1;


//                        }
//                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
//                        {

//                            SrNo3 = "" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
//                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:60px;text-align:justify;'>
//                                     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>" + SrNo3 + @"</b> ";
//                            SrNo3Count = SrNo3Count + 1;

//                        }

//                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
//                        if (LOBText != "")
//                        {
//                            LOBText = LOBText.Replace("<p>", "");
//                            LOBText = LOBText.Replace("</p>", "");
//                        }


//                        outXml += @"" + LOBText + "</div> ";


//                        outXml += @"<br/> </td></tr>";


//                    }

//                    if (LOBType == "Draft")
//                    {
//                        //Bottom Section
//                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>
//
//                                                <tr><td>
//                                                <table style='width:100%;'>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;'>
//                                               <b> (" + CurrentSpeaker + @"),</b>
//                                                </td>
//                                                <td style='width:25%;float:left;font-size: 24px;'>
//                                                <b> (" + CurrentSecretery + @"),  </b>
//                                                </td>
//                                                </tr>
//                                                <br/>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;padding-left:15%;'>
//                                               <b> अध्यक्ष ।</b>
//                                                </td>
//                                                <td style='width:25%;float:right;text-align:right;padding-right:80px;font-size: 24px;'>
//                                               <b>  सचिव।</b>
//                                                </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
//                                                          " + "******************" + @"    
//                                                    </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
//                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
//                                                    </td>
//                                                </tr>
//                                                </table>           
//                                                    </td>
//                                                    </tr>";

//                        outXml += "</table></div></div> </body> </html>";
//                    }
//                    else if (LOBType == "Approved")
//                    {
//                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>
//
//                                                <tr><td>
//                                                <table style='width:100%;'>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;'>
//                                               <b> शिमला-171004</b>
//                                                </td>
//                                                <td style='width:25%;float:left;font-size: 24px;'>
//                                                <b> " + CurrentSecretery + @",  </b>
//                                                </td>
//                                                </tr>
//                                                <br/>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;'>
//                                               <b> दिनांक: " + SubittedDate + @"</b>
//                                               <b> &nbsp;&nbsp;" + SubittedTime + @"</b>
//                                                </td>
//                                                <td style='width:25%;float:right;text-align:right;padding-right:85px;font-size: 24px;'>
//                                               <b>  सचिव।</b>
//                                                </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
//                                                          " + "******************" + @"    
//                                                    </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
//                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
//                                                    </td>
//                                                </tr>
//                                                </table>           
//                                                    </td>
//                                                    </tr>";

//                        outXml += "</table></div></div> </body> </html>";
//                    }
//                }


//                methodParameter = new List<KeyValuePair<string, string>>();
//                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
//                string CurrentAssembly = "";
//                string Currentsession = "";


//                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
//                {
//                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
//                    {
//                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
//                    }
//                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
//                    {
//                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
//                    }
//                }

//                string fileName = "";

//                ViewData["HTMLData"] = outXml;

//                if (LOBType == "Draft")
//                {
//                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
//                    string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                    DirectoryInfo Dir = new DirectoryInfo(directory);
//                    if (!Dir.Exists)
//                    {
//                        Dir.Create();
//                    }
//                    fileName = "Draft_LOB.doc";
//                    var path = Path.Combine(directory, fileName);
//                    string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                    FileStream fStream = System.IO.File.Create(path);
//                    fStream.Close();
//                    StreamWriter sWriter = new StreamWriter(path);
//                    sWriter.Write(outXml);
//                    sWriter.Close();

//                    outXml = HttpUtility.UrlDecode(outXml);
//                    Response.Clear();
//                    Response.AddHeader("content-disposition", "attachment;filename=Draft_LOB.doc");
//                    Response.Charset = "";
//                    Response.ContentType = "application/doc";
//                    Response.Write(outXml);
//                    Response.Flush();
//                    Response.End();
//                }
//                else if (LOBType == "Approved")
//                {

//                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
//                    string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                    DirectoryInfo Dir = new DirectoryInfo(directory);
//                    if (!Dir.Exists)
//                    {
//                        Dir.Create();
//                    }
//                    fileName = "Approved_LOB.doc";
//                    var path = Path.Combine(directory, fileName);
//                    string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                    FileStream fStream = System.IO.File.Create(path);
//                    fStream.Close();
//                    StreamWriter sWriter = new StreamWriter(path);
//                    sWriter.Write(outXml);
//                    sWriter.Close();
//                    outXml = HttpUtility.UrlDecode(outXml);
//                    Response.Clear();
//                    Response.AddHeader("content-disposition", "attachment;filename=Approved_LOB.doc");
//                    Response.Charset = "";
//                    Response.ContentType = "application/doc";
//                    Response.Write(outXml);
//                    Response.Flush();
//                    Response.End();
//                }

//            }

//            return null;//File(output, "application/doc");
//        }
        //new method for generating html of LOB
        //old code commented
//        public ActionResult GenerateLOBhtml(string LOBId, bool download, string LOBType)
//        {
//            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
//            string LOBName = "20Dec2013_1_LOB";
//            string CurrentSecretery = "";
//            string CurrentSpeaker = "";

//            DateTime SessionDate = new DateTime();
//            string savedPDF = string.Empty;

//            string outXml = "";
//            MemoryStream output = new MemoryStream();
//            if (LOBId != null && LOBId != "")
//            {
//                LOBModel objLOBModel = new LOBModel();
//                DataSet SummaryDataSet = new DataSet();
//                string sessiondate = "";

//                var methodParameter = new List<KeyValuePair<string, string>>();
//                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));

//                if (LOBType == "Draft")
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
//                }
//                else if (LOBType == "Approved")
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
//                }
//                else
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
//                }


//                //Getting Current Secretary
//                DataSet SiteSettingDataSet = new DataSet();
//                var methodParameter1 = new List<KeyValuePair<string, string>>();
//                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
//                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
//                {
//                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
//                    if (dtSiteSetting != null)
//                    {
//                        IEnumerable<DataRow> siteSettingQuery =
//                                from siteSetting in dtSiteSetting.AsEnumerable()
//                                select siteSetting;
//                        IEnumerable<DataRow> curtSecretery =
//                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
//                        foreach (DataRow obj in curtSecretery)
//                        {
//                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
//                        }
//                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSpeaker");
//                        foreach (DataRow obj in curtSecretery)
//                        {
//                            CurrentSpeaker = obj.Field<string>("SettingValueLocal");
//                        }
//                    }
//                }

//                string SubittedDate = "";
//                string SubittedTime = "";
//                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
//                {

//                    outXml = @"<html>                           
//                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

//                    List<LOBModel> listLOBForDay = new List<LOBModel>();
//                    string SrNo1 = "";
//                    string SrNo2 = "";
//                    string SrNo3 = "";
//                    int SrNo1Count = 1;
//                    int SrNo2Count = 1;
//                    int SrNo3Count = 1;
//                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
//                    {

//                        if (i == 0)
//                        {
//                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
//                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
//                            string date = ConvertSQLDate(SessionDate);
//                            date = date.Replace("/", "");
//                            date = date.Replace("-", "");
//                            //SessionDate = DateTime.ParseExact(sessiondate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
//                            sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
//                            sessiondate = sessiondate.Replace('/', ' ');
//                            // SessionDate = SessionDate.Replace('/', ' ');                     
//                            //TempData["addLines"] = sessiondate;

//                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


//                            outXml += @"<tr>
//                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
//                                                         हिमाचल प्रदेश&nbsp;" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
//                                                    </td>
//                                                </tr>
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
//                                                          " + "कार्यसूची" + @"    
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold'>
//                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
//                                                    </td>
//                                                </tr>
//
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;'>               
//                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;text-decoration: underline;'>               
//                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
//                                                    </td>
//                                                </tr>";

//                            if (LOBType == "Draft")
//                            {
//                                if (SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"] != null && Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]) != "")
//                                {
//                                    SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]).ToString("D", new CultureInfo("hi")); ;
//                                }
//                                else
//                                {
//                                    SubittedDate = System.DateTime.Now.ToString("dd/MM/yyyy");
//                                }

//                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]) != "")
//                                {
//                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]));
//                                    DateTime time = DateTime.Today.Add(interval);
//                                    string displayTime = time.ToString("hh:mm tt");

//                                    SubittedTime = displayTime;
//                                }
//                                else
//                                {
//                                    SubittedTime = "";
//                                }
//                            }
//                            else
//                            {

//                                SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]).ToString("D", new CultureInfo("hi"));
//                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]) != "")
//                                {
//                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]));
//                                    DateTime time = DateTime.Today.Add(interval);
//                                    string displayTime = time.ToString("hh:mm tt");

//                                    SubittedTime = displayTime;
//                                }
//                                else
//                                {
//                                    SubittedTime = "";
//                                }
//                            }

//                        }


//                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
//                        {

//                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
//                            outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
//                                     <div style='padding-left:10px;text-align:justify;'>
//                                  <b>  " + SrNo1 + @" </b> &nbsp;&nbsp;&nbsp; <div style='padding-left:20px; margin: -34px 0 0 2%;text-align:justify;'>";
//                            SrNo1Count = SrNo1Count + 1;
//                            SrNo2Count = 1;


//                        }
//                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
//                        {

//                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
//                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:30px;text-align:justify;'>
//                                    <b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px; margin: -31px 0 0 4.5%;text-align:justify;'>";
//                            SrNo2Count = SrNo2Count + 1;
//                            SrNo3Count = 1;


//                        }
//                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
//                        {

//                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
//                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:60px;text-align:justify;'>
//                                    <b>" + SrNo3 + @"</b>&nbsp;&nbsp;&nbsp; <div style='padding-left:15px; margin: -31px 0 0 6%;text-align:justify;'>";
//                            SrNo3Count = SrNo3Count + 1;

//                        }

//                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
//                        if (LOBText != "")
//                        {
//                            LOBText = LOBText.Replace("<p>", "");
//                            LOBText = LOBText.Replace("</p>", "");
//                        }

//                        //string Member = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberDesignationLocal"]) + ")";
//                        //string Minister = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterDesignationLocal"]) + ")";
//                        //string Department = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedDeptNameLocal"]);
//                        //string Committee = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedCommitteeNameLocal"]);
//                        //string Rule = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedRuleNameLocal"]);
//                        outXml += @"" + LOBText + "</div></div> ";
//                        //if (Member != " ()")
//                        //{
//                        //    outXml += @"<b>  सदस्य: </b>" + Member;
//                        //}
//                        //if (Department != "")
//                        //{
//                        //    outXml += @"<b>  विभाग: </b> " + Department;
//                        //    if (Minister != "")
//                        //    {
//                        //        outXml += @"<b>  मंत्री: </b> " + Minister;
//                        //    }
//                        //}
//                        //if (Committee != "")
//                        //{
//                        //    outXml += @"<b>  समिति: </b> " + Committee;
//                        //}
//                        //if (Rule != "")
//                        //{
//                        //    outXml += @"<b>  नियम: </b> " + Rule;
//                        //}

//                        outXml += @"<br/> </td></tr>";


//                    }

//                    if (LOBType == "Draft")
//                    {
//                        //Bottom Section
//                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>
//
//                                                <tr><td>
//                                                <table style='width:100%;'>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;'>
//                                               <b> (" + CurrentSpeaker + @"),</b>
//                                                </td>
//                                                <td style='width:25%;float:left;font-size: 24px;'>
//                                                <b> (" + CurrentSecretery + @"),  </b>
//                                                </td>
//                                                </tr>
//                                                <br/>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;padding-left:15%;'>
//                                               <b> अध्यक्ष ।</b>
//                                                </td>
//                                                <td style='width:25%;float:right;text-align:right;padding-right:80px;font-size: 24px;'>
//                                               <b>  सचिव।</b>
//                                                </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
//                                                          " + "******************" + @"    
//                                                    </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
//                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
//                                                    </td>
//                                                </tr>
//                                                </table>           
//                                                    </td>
//                                                    </tr>";

//                        outXml += "</table></div></div> </body> </html>";
//                    }
//                    else if (LOBType == "Approved")
//                    {
//                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>
//
//                                                <tr><td>
//                                                <table style='width:100%;'>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;'>
//                                               <b> शिमला-171004</b>
//                                                </td>
//                                                <td style='width:25%;float:left;font-size: 24px;'>
//                                                <b> " + CurrentSecretery + @",  </b>
//                                                </td>
//                                                </tr>
//                                                <br/>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;'>
//                                               <b> दिनांक: " + SubittedDate + @"</b>
//                                               <b> &nbsp;&nbsp;" + SubittedTime + @"</b>
//                                                </td>
//                                                <td style='width:25%;float:right;text-align:right;padding-right:85px;font-size: 24px;'>
//                                               <b>  सचिव।</b>
//                                                </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
//                                                          " + "******************" + @"    
//                                                    </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
//                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
//                                                    </td>
//                                                </tr>
//                                                </table>           
//                                                    </td>
//                                                    </tr>";

//                        outXml += "</table></div></div> </body> </html>";
//                    }
//                }


//            }
//            //outXml=   Regex.Replace(outXml, @"\s*(<[^>]+>)\s*", "$1", RegexOptions.Singleline);
//            //ViewBag.HTMLData = HttpUtility.HtmlEncode(outXml);
//            // ViewBag.HTMLData = HttpUtility.HtmlEncode(outXml);
//            ViewData["HTMLData"] = outXml;
//            return View("Test");
//        }
        //Old Code Commented
//        [HttpGet]
//        public object GenerateLOBPdfCorrigendum(string LOBId, bool download, string LOBType, int CorrigendumId)
//        {
//            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
//            string LOBName = "20Dec2013_1_LOB";
//            string CurrentSecretery = "";
//            string CurrentSpeaker = "";

//            DateTime SessionDate = new DateTime();
//            string savedPDF = string.Empty;

//            MemoryStream output = new MemoryStream();
//            if (LOBId != null && LOBId != "")
//            {
//                LOBModel objLOBModel = new LOBModel();
//                DataSet SummaryDataSet = new DataSet();
//                string sessiondate = "";
//                string outXml = "";
//                var methodParameter = new List<KeyValuePair<string, string>>();
//                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));

//                if (LOBType == "Draft")
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
//                }
//                else if (LOBType == "Approved")
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
//                }
//                else
//                {
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
//                }


//                //Getting Current Secretary
//                DataSet SiteSettingDataSet = new DataSet();
//                var methodParameter1 = new List<KeyValuePair<string, string>>();
//                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
//                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
//                {
//                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
//                    if (dtSiteSetting != null)
//                    {
//                        IEnumerable<DataRow> siteSettingQuery =
//                                from siteSetting in dtSiteSetting.AsEnumerable()
//                                select siteSetting;
//                        IEnumerable<DataRow> curtSecretery =
//                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
//                        foreach (DataRow obj in curtSecretery)
//                        {
//                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
//                        }
//                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSpeaker");
//                        foreach (DataRow obj in curtSecretery)
//                        {
//                            CurrentSpeaker = obj.Field<string>("SettingValueLocal");
//                        }
//                    }
//                }


//                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
//                {

//                    outXml = @"<html>                           
//                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

//                    List<LOBModel> listLOBForDay = new List<LOBModel>();
//                    string SrNo1 = "";
//                    string SrNo2 = "";
//                    string SrNo3 = "";
//                    int SrNo1Count = 1;
//                    int SrNo2Count = 1;
//                    int SrNo3Count = 1;
//                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
//                    {

//                        if (i == 0)
//                        {
//                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
//                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
//                            string date = ConvertSQLDate(SessionDate);
//                            date = date.Replace("/", "");
//                            date = date.Replace("-", "");
//                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


//                            outXml += @"<tr>
//                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
//                                                         हिमाचल प्रदेश&nbsp; " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
//                                                    </td>
//                                                </tr>
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
//                                                          " + "कार्यसूची" + @"    
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold'>
//                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
//                                                    </td>
//                                                </tr>
//
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;'>               
//                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;text-decoration: underline;'>               
//                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
//                                                    </td>
//                                                </tr>";

//                        }


//                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
//                        {

//                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
//                            //                            outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
//                            //                                      <br/>  
//                            //                                    " + SrNo1 + @"";
//                            //                            SrNo1Count = SrNo1Count + 1;
//                            //                            SrNo2Count = 1;
//                            outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
//                                      <div style='padding-left:10px;text-align:justify;'>
//                                   <b> " + SrNo1 + @"</b>&nbsp;&nbsp;&nbsp; <div style='padding-left:20px; margin: -34px 0 0 2%;text-align:justify;'>";
//                            SrNo1Count = SrNo1Count + 1;
//                            SrNo2Count = 1;

//                        }
//                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
//                        {

//                            //                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
//                            //                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'>
//                            //                                    " + SrNo2 + @"";
//                            //                            SrNo2Count = SrNo2Count + 1;
//                            //                            SrNo3Count = 1;
//                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
//                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:30px;text-align:justify;'>
//                                    <b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px; margin: -31px 0 0 4.5%;text-align:justify;'>";
//                            SrNo2Count = SrNo2Count + 1;
//                            SrNo3Count = 1;

//                        }
//                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
//                        {

//                            //                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
//                            //                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'>
//                            //                                    " + SrNo3 + @"";
//                            //                            SrNo3Count = SrNo3Count + 1;
//                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
//                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:60px;text-align:justify;'>
//                                    <b>" + SrNo3 + @"</b>&nbsp;&nbsp;&nbsp; <div style='padding-left:15px; margin: -31px 0 0 6%;text-align:justify;'>";
//                            SrNo3Count = SrNo3Count + 1;
//                        }

//                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
//                        if (LOBText != "")
//                        {
//                            LOBText = LOBText.Replace("<p>", "");
//                            LOBText = LOBText.Replace("</p>", "");
//                        }

//                        //string Member = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberDesignationLocal"]) + ")";
//                        //string Minister = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterDesignationLocal"]) + ")";
//                        //string Department = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedDeptNameLocal"]);
//                        //string Committee = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedCommitteeNameLocal"]);
//                        //string Rule = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedRuleNameLocal"]);
//                        outXml += @"" + LOBText + "</div></div> ";
//                        //if (Member != " ()")
//                        //{
//                        //    outXml += @"<b>  सदस्य: </b>" + Member;
//                        //}
//                        //if (Department != "")
//                        //{
//                        //    outXml += @"<b>  विभाग: </b> " + Department;
//                        //    if (Minister != "")
//                        //    {
//                        //        outXml += @"<b>  मंत्री: </b> " + Minister;
//                        //    }
//                        //}
//                        //if (Committee != "")
//                        //{
//                        //    outXml += @"<b>  समिति: </b> " + Committee;
//                        //}
//                        //if (Rule != "")
//                        //{
//                        //    outXml += @"<b>  नियम: </b> " + Rule;
//                        //}

//                        outXml += @"<br/> </td></tr>";


//                    }
//                    string SessionTime = "";
//                    string date12 = "";


//                    CorrigendumLOB corri = new CorrigendumLOB();
//                    corri.CorrigendumId = Convert.ToInt16(CorrigendumId);
//                    corri = (CorrigendumLOB)Helper.ExecuteService("LOB", "GetCorrigendumById", corri);
//                    if (corri != null)
//                    {
//                        if (LOBType == "Draft")
//                        {
//                            if (Convert.ToString(corri.SubmittedTime) != "")
//                            {
//                                TimeSpan interval = TimeSpan.Parse(Convert.ToString(corri.SubmittedTime));
//                                DateTime time = DateTime.Today.Add(interval);
//                                string displayTime = time.ToString("hh:mm tt");

//                                SessionTime = displayTime;
//                            }
//                            else
//                            {
//                                SessionTime = "";
//                            }
//                            date12 = Convert.ToDateTime(corri.SubmittedDate).ToString("D", new CultureInfo("hi"));
//                        }
//                        else
//                        {
//                            if (Convert.ToString(corri.ApprovedTime) != "")
//                            {
//                                TimeSpan interval = TimeSpan.Parse(Convert.ToString(corri.ApprovedTime));
//                                DateTime time = DateTime.Today.Add(interval);
//                                string displayTime = time.ToString("hh:mm tt");

//                                SessionTime = displayTime;
//                            }
//                            else
//                            {
//                                SessionTime = "";
//                            }
//                            date12 = Convert.ToDateTime(corri.ApprovedDate).ToString("D", new CultureInfo("hi"));
//                        }
//                    }
//                    //Bottom Section
//                    if (LOBType == "Draft")
//                    {
//                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>
//
//                                                <tr><td>
//                                                <table style='width:100%;'>
//                                                <tr>
//                                                <td style='width:75%;float:left;font-size: 24px;'>
//                                               <b> (" + CurrentSpeaker + @"),</b>
//                                                </td>
//                                                <td style='width:25%;float:left;font-size: 24px;'>
//                                                <b> (" + CurrentSecretery + @"),  </b>
//                                                </td>
//                                                </tr>
//                                                <br/>
//                                                <tr>
//                                                 <td style='width:75%;float:left;font-size: 24px;padding-left:15%;'>
//                                               <b> अध्यक्ष ।</b>
//                                                </td>
//                                                <td style='width:25%;float:right;text-align:right;padding-right:80px;font-size: 24px;'>
//                                               <b>  सचिव।</b>
//                                                </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
//                                                          " + "******************" + @"    
//                                                    </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
//                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
//                                                    </td>
//                                                </tr>
//                                                </table>           
//                                                    </td>
//                                                    </tr>";

//                        outXml += "</table></div></div> </body> </html>";

//                    }
//                    else if (LOBType == "Approved")
//                    {
//                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>
//
//                                                <tr><td>
//                                                <table style='width:100%;'>
//                                                <tr>
//                                                <td style='width:70%;float:left;font-size: 24px;'>
//                                                <b>शिमला-171004</b>
//                                                </td>
//                                                <td style='width:30%;float:left;font-size: 24px;'>
//                                                <b> " + CurrentSecretery + @",  </b>
//                                                </td>
//                                                </tr>
//                                                <br/>
//                                                <tr>
//                                                <td style='width:70%;float:left;font-size: 24px;'>
//                                                <b> दिनांक: " + date12 + @"</b>
//                                               <b> &nbsp;&nbsp; " + SessionTime + @"</b>
//                                                </td>
//                                             <td style='width:25%;float:right;text-align:right;padding-right:85px;font-size: 24px;'>
//                                               <b>  सचिव।</b>
//                                                </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center;font-size: 24px; ' colspan='2'>               
//                                                          " + "******************" + @"    
//                                                    </td>
//                                                </tr>
//<tr>
//                                                    <td style='text-align: center;font-size: 22px; ' colspan='2'>               
//                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
//                                                    </td>
//                                                </tr>
//                                                </table>           
//                                                    </td>
//                                                    </tr>";

//                        outXml += "</table></div></div> </body> </html>";
//                    }
//                }

//                // convert HTML to PDF
//                EvoPdf.Document document1 = new EvoPdf.Document();

//                // set the license key
//                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
//                document1.CompressionLevel = PdfCompressionLevel.Best;
//                document1.Margins = new Margins(40, 45, 20, 20);
//                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(10, 45, 55, 55), PdfPageOrientation.Portrait);

//                AddElementResult addResult;

//                HtmlToPdfElement htmlToPdfElement;
//                string htmlStringToConvert = outXml;
//                string baseURL = "";
//                htmlToPdfElement = new HtmlToPdfElement(40, 20, 0, 0, htmlStringToConvert, baseURL);



//                addResult = page.AddElement(htmlToPdfElement);
//                byte[] pdfBytes = document1.Save();

//                try
//                {
//                    output.Write(pdfBytes, 0, pdfBytes.Length);
//                    output.Position = 0;

//                }
//                finally
//                {
//                    // close the PDF document to release the resources
//                    document1.Close();
//                }



//                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msInput, null);
//                //document.Close();

//                //byte[] file = ms.ToArray();
//                //output.Write(file, 0, file.Length);
//                //output.Position = 0;


//                ///To get File Path

//                methodParameter = new List<KeyValuePair<string, string>>();
//                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
//                string CurrentAssembly = "";
//                string Currentsession = "";


//                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
//                {
//                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
//                    {
//                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
//                    }
//                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
//                    {
//                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
//                    }
//                }

//                string fileName = "";


//                sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
//                sessiondate = sessiondate.Replace('/', ' ');



//                if (download)
//                {
//                    if (LOBType == "Draft")
//                    {
//                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_LOB_" + CorrigendumId + ".pdf");
//                        string Returl = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        //string directory = Server.MapPath(url);
//                        //if (!Directory.Exists(directory))
//                        //{
//                        //    Directory.CreateDirectory(directory);
//                        //}

//                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
//                        string url = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        DirectoryInfo Dir = new DirectoryInfo(url);
//                        if (!Dir.Exists)
//                        {
//                            Dir.Create();
//                        }
//                        fileName = "Draft_LOB_" + CorrigendumId + ".pdf";
//                        var path = Path.Combine(url, fileName);
//                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
//                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

//                        // close file stream
//                        _FileStream.Close();


//                        #region FileTransfer save

//                        //int fileLength = _FileStream.ContentLength;

//                        //byte[] myData = new byte[fileLength];
//                        //file.InputStream.Read(myData, 0, fileLength);
//                        // Gor in Binary Data
//                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
//                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
//                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
//                        sers.Serialize(wr, pdfBytes);
//                        XmlDocument doc = new XmlDocument();
//                        doc.LoadXml(sb.ToString());

//                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");

//                        #endregion

//                        return Returl + fileName;

//                        //methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
//                        //methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + "/" + fileName));
//                        //ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedLOBPath]", methodParameter);
//                        //return null;
//                    }
//                    else if (LOBType == "Approved")
//                    {
//                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Approved_LOB_" + CorrigendumId + ".pdf");
//                        string Returl = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        //string directory = Server.MapPath(url);
//                        //if (!Directory.Exists(directory))
//                        //{
//                        //    Directory.CreateDirectory(directory);
//                        //}
//                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
//                        string url = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        DirectoryInfo Dir = new DirectoryInfo(url);
//                        if (!Dir.Exists)
//                        {
//                            Dir.Create();
//                        }
//                        fileName = "Approved_LOB_" + CorrigendumId + ".pdf";
//                        var path = Path.Combine(url, fileName);
//                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
//                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

//                        // close file stream
//                        _FileStream.Close();


//                        #region FileTransfer save

//                        //int fileLength = _FileStream.ContentLength;

//                        //byte[] myData = new byte[fileLength];
//                        //file.InputStream.Read(myData, 0, fileLength);
//                        // Gor in Binary Data
//                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
//                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
//                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
//                        sers.Serialize(wr, pdfBytes);
//                        XmlDocument doc = new XmlDocument();
//                        doc.LoadXml(sb.ToString());

//                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
//                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

//                        #endregion

//                        return Returl + fileName;

//                        //methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
//                        //methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + "/" + fileName));
//                        //ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedApprovedLOBPath]", methodParameter);
//                        //return null;
//                    }
//                    else if (LOBType == "Published")
//                    {

//                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Published_" + LOBId + ".pdf");
//                        string Returl = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        //string directory = Server.MapPath(url);
//                        //if (!Directory.Exists(directory))
//                        //{
//                        //    Directory.CreateDirectory(directory);
//                        //}
//                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
//                        string url = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
//                        DirectoryInfo Dir = new DirectoryInfo(url);
//                        if (!Dir.Exists)
//                        {
//                            Dir.Create();
//                        }
//                        fileName = "Published_" + LOBId + ".pdf";
//                        var path = Path.Combine(url, fileName);
//                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
//                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

//                        // close file stream
//                        _FileStream.Close();


//                        #region FileTransfer save

//                        //int fileLength = _FileStream.ContentLength;

//                        //byte[] myData = new byte[fileLength];
//                        //file.InputStream.Read(myData, 0, fileLength);
//                        // Gor in Binary Data
//                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
//                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
//                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
//                        sers.Serialize(wr, pdfBytes);
//                        XmlDocument doc = new XmlDocument();
//                        doc.LoadXml(sb.ToString());

//                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
//                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

//                        #endregion

//                        return Returl + fileName;

//                        //methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
//                        //methodParameter.Add(new KeyValuePair<string, string>("@PublishLOBPath", url + "/" + fileName));
//                        //ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_PublishedLOBPath]", methodParameter);
//                        //return null;
//                    }
//                }
//            }

//            return File(output, "application/pdf");
//        }

        public ActionResult GenerateLOBhtml(string LOBId, bool download, string LOBType)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string LOBName = "20Dec2013_1_LOB";
            string CurrentSecretery = "";
            string CurrentSpeaker = "";
            string CurrenttSessionPlace = "";
            DateTime SessionDate = new DateTime();
            string savedPDF = string.Empty;

            string outXml = "";
            MemoryStream output = new MemoryStream();
            if (LOBId != null && LOBId != "")
            {
                LOBModel objLOBModel = new LOBModel();
                DataSet SummaryDataSet = new DataSet();
                string sessiondate = "";

                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));

                if (LOBType == "Draft")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }
                else if (LOBType == "Approved")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
                }
                else
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }


                //Getting Current Secretary
                DataSet SiteSettingDataSet = new DataSet();
                var methodParameter1 = new List<KeyValuePair<string, string>>();
                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
                {
                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
                    if (dtSiteSetting != null)
                    {
                        IEnumerable<DataRow> siteSettingQuery =
                                from siteSetting in dtSiteSetting.AsEnumerable()
                                select siteSetting;
                        IEnumerable<DataRow> curtSecretery =
                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
                        }
                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSpeaker");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSpeaker = obj.Field<string>("SettingValueLocal");
                        }
                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "SessionPlace");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrenttSessionPlace = obj.Field<string>("SettingValueLocal");
                        }
                    }
                }

                string SubittedDate = "";
                string SubittedTime = "";
                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {

                    outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    string SrNo1 = "";
                    string SrNo2 = "";
                    string SrNo3 = "";
                    int SrNo1Count = 1;
                    int SrNo2Count = 1;
                    int SrNo3Count = 1;
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {

                        if (i == 0)
                        {
                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            string date = ConvertSQLDate(SessionDate);
                            date = date.Replace("/", "");
                            date = date.Replace("-", "");
                            //SessionDate = DateTime.ParseExact(sessiondate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                            sessiondate = sessiondate.Replace('/', ' ');
                            // SessionDate = SessionDate.Replace('/', ' ');                     
                            //TempData["addLines"] = sessiondate;

                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


                            outXml += @"<tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
                                                         हिमाचल प्रदेश&nbsp;" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
                                                          " + "कार्यसूची" + @"    
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold'>
                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;'>               
                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;text-decoration: underline;'>               
                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
                                                    </td>
                                                </tr>";

                            if (LOBType == "Draft")
                            {
                                if (SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"] != null && Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]) != "")
                                {
                                    SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]).ToString("D", new CultureInfo("hi")); ;
                                }
                                else
                                {
                                    SubittedDate = System.DateTime.Now.ToString("dd/MM/yyyy");
                                }

                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]) != "")
                                {
                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]));
                                    DateTime time = DateTime.Today.Add(interval);
                                    string displayTime = time.ToString("hh:mm tt");

                                    SubittedTime = displayTime;
                                }
                                else
                                {
                                    SubittedTime = "";
                                }
                            }
                            else
                            {

                                SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]).ToString("D", new CultureInfo("hi"));
                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]) != "")
                                {
                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]));
                                    DateTime time = DateTime.Today.Add(interval);
                                    string displayTime = time.ToString("hh:mm tt");

                                    SubittedTime = displayTime;
                                }
                                else
                                {
                                    SubittedTime = "";
                                }
                            }

                        }


                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
                            outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
                                     <div style='padding-left:10px;text-align:justify;'>
                                  <b>  " + SrNo1 + @" </b> &nbsp;&nbsp;&nbsp; <div style='padding-left:20px; margin: -34px 0 0 2%;text-align:justify;'>";
                            SrNo1Count = SrNo1Count + 1;
                            SrNo2Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:30px;text-align:justify;'>
                                    <b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px; margin: -31px 0 0 4.5%;text-align:justify;'>";
                            SrNo2Count = SrNo2Count + 1;
                            SrNo3Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
                        {

                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:60px;text-align:justify;'>
                                    <b>" + SrNo3 + @"</b>&nbsp;&nbsp;&nbsp; <div style='padding-left:15px; margin: -31px 0 0 6%;text-align:justify;'>";
                            SrNo3Count = SrNo3Count + 1;

                        }

                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        //string Member = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberDesignationLocal"]) + ")";
                        //string Minister = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterDesignationLocal"]) + ")";
                        //string Department = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedDeptNameLocal"]);
                        //string Committee = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedCommitteeNameLocal"]);
                        //string Rule = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedRuleNameLocal"]);
                        outXml += @"" + LOBText + "</div></div> ";
                        //if (Member != " ()")
                        //{
                        //    outXml += @"<b>  सदस्य: </b>" + Member;
                        //}
                        //if (Department != "")
                        //{
                        //    outXml += @"<b>  विभाग: </b> " + Department;
                        //    if (Minister != "")
                        //    {
                        //        outXml += @"<b>  मंत्री: </b> " + Minister;
                        //    }
                        //}
                        //if (Committee != "")
                        //{
                        //    outXml += @"<b>  समिति: </b> " + Committee;
                        //}
                        //if (Rule != "")
                        //{
                        //    outXml += @"<b>  नियम: </b> " + Rule;
                        //}

                        outXml += @"<br/> </td></tr>";


                    }

                    if (LOBType == "Draft")
                    {
                        //Bottom Section
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;'>
                                               <b> (" + CurrentSpeaker + @"),</b>
                                                </td>
                                                <td style='width:25%;float:left;font-size: 24px;'>
                                                <b> (" + CurrentSecretery + @"),  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;padding-left:15%;'>
                                               <b> अध्यक्ष ।</b>
                                                </td>
                                                <td style='width:25%;float:right;text-align:right;padding-right:80px;font-size: 24px;'>
                                               <b>  सचिव।</b>
                                                </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }
                    else if (LOBType == "Approved")
                    {
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;'>
                                                   <b> " + CurrenttSessionPlace + @",</b>
                                                </td>
                                                <td style='width:25%;float:left;font-size: 24px;'>
                                                <b> " + CurrentSecretery + @",  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;'>
                                               <b> दिनांक: " + SubittedDate + @"</b>
                                               <b> &nbsp;&nbsp;" + SubittedTime + @"</b>
                                                </td>
                                                <td style='width:25%;float:right;text-align:right;padding-right:85px;font-size: 24px;'>
                                               <b>  सचिव।</b>
                                                </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }
                }


            }
            //outXml=   Regex.Replace(outXml, @"\s*(<[^>]+>)\s*", "$1", RegexOptions.Singleline);
            //ViewBag.HTMLData = HttpUtility.HtmlEncode(outXml);
            // ViewBag.HTMLData = HttpUtility.HtmlEncode(outXml);
            ViewData["HTMLData"] = outXml;
            return View("Test");
        }

        [HttpGet]
        public object GenerateLOBPdfCorrigendum(string LOBId, bool download, string LOBType, int CorrigendumId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string LOBName = "20Dec2013_1_LOB";
            string CurrentSecretery = "";
            string CurrentSpeaker = "";
            string CurrenttSessionPlace = "";
            DateTime SessionDate = new DateTime();
            string savedPDF = string.Empty;

            MemoryStream output = new MemoryStream();
            if (LOBId != null && LOBId != "")
            {
                LOBModel objLOBModel = new LOBModel();
                DataSet SummaryDataSet = new DataSet();
                string sessiondate = "";
                string outXml = "";
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));

                if (LOBType == "Draft")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }
                else if (LOBType == "Approved")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
                }
                else
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }


                //Getting Current Secretary
                DataSet SiteSettingDataSet = new DataSet();
                var methodParameter1 = new List<KeyValuePair<string, string>>();
                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
                {
                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
                    if (dtSiteSetting != null)
                    {
                        IEnumerable<DataRow> siteSettingQuery =
                                from siteSetting in dtSiteSetting.AsEnumerable()
                                select siteSetting;
                        IEnumerable<DataRow> curtSecretery =
                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
                        }
                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSpeaker");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSpeaker = obj.Field<string>("SettingValueLocal");
                        }
                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "SessionPlace");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrenttSessionPlace = obj.Field<string>("SettingValueLocal");
                        }
                    }
                }


                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {

                    outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    string SrNo1 = "";
                    string SrNo2 = "";
                    string SrNo3 = "";
                    int SrNo1Count = 1;
                    int SrNo2Count = 1;
                    int SrNo3Count = 1;
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {

                        if (i == 0)
                        {
                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            string date = ConvertSQLDate(SessionDate);
                            date = date.Replace("/", "");
                            date = date.Replace("-", "");
                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


                            outXml += @"<tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
                                                         हिमाचल प्रदेश&nbsp; " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
                                                          " + "कार्यसूची" + @"    
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold'>
                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;'>               
                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;text-decoration: underline;'>               
                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
                                                    </td>
                                                </tr>";

                        }


                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
                            //                            outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
                            //                                      <br/>  
                            //                                    " + SrNo1 + @"";
                            //                            SrNo1Count = SrNo1Count + 1;
                            //                            SrNo2Count = 1;
                            outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
                                      <div style='padding-left:10px;text-align:justify;'>
                                   <b> " + SrNo1 + @"</b>&nbsp;&nbsp;&nbsp; <div style='padding-left:20px; margin: -34px 0 0 2%;text-align:justify;'>";
                            SrNo1Count = SrNo1Count + 1;
                            SrNo2Count = 1;

                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            //                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                            //                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'>
                            //                                    " + SrNo2 + @"";
                            //                            SrNo2Count = SrNo2Count + 1;
                            //                            SrNo3Count = 1;
                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:30px;text-align:justify;'>
                                    <b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px; margin: -31px 0 0 4.5%;text-align:justify;'>";
                            SrNo2Count = SrNo2Count + 1;
                            SrNo3Count = 1;

                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
                        {

                            //                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                            //                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'>
                            //                                    " + SrNo3 + @"";
                            //                            SrNo3Count = SrNo3Count + 1;
                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:60px;text-align:justify;'>
                                    <b>" + SrNo3 + @"</b>&nbsp;&nbsp;&nbsp; <div style='padding-left:15px; margin: -31px 0 0 6%;text-align:justify;'>";
                            SrNo3Count = SrNo3Count + 1;
                        }

                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        //string Member = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberDesignationLocal"]) + ")";
                        //string Minister = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterDesignationLocal"]) + ")";
                        //string Department = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedDeptNameLocal"]);
                        //string Committee = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedCommitteeNameLocal"]);
                        //string Rule = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedRuleNameLocal"]);
                        outXml += @"" + LOBText + "</div></div> ";
                        //if (Member != " ()")
                        //{
                        //    outXml += @"<b>  सदस्य: </b>" + Member;
                        //}
                        //if (Department != "")
                        //{
                        //    outXml += @"<b>  विभाग: </b> " + Department;
                        //    if (Minister != "")
                        //    {
                        //        outXml += @"<b>  मंत्री: </b> " + Minister;
                        //    }
                        //}
                        //if (Committee != "")
                        //{
                        //    outXml += @"<b>  समिति: </b> " + Committee;
                        //}
                        //if (Rule != "")
                        //{
                        //    outXml += @"<b>  नियम: </b> " + Rule;
                        //}

                        outXml += @"<br/> </td></tr>";


                    }
                    string SessionTime = "";
                    string date12 = "";


                    CorrigendumLOB corri = new CorrigendumLOB();
                    corri.CorrigendumId = Convert.ToInt16(CorrigendumId);
                    corri = (CorrigendumLOB)Helper.ExecuteService("LOB", "GetCorrigendumById", corri);
                    if (corri != null)
                    {
                        if (LOBType == "Draft")
                        {
                            if (Convert.ToString(corri.SubmittedTime) != "")
                            {
                                TimeSpan interval = TimeSpan.Parse(Convert.ToString(corri.SubmittedTime));
                                DateTime time = DateTime.Today.Add(interval);
                                string displayTime = time.ToString("hh:mm tt");

                                SessionTime = displayTime;
                            }
                            else
                            {
                                SessionTime = "";
                            }
                            date12 = Convert.ToDateTime(corri.SubmittedDate).ToString("D", new CultureInfo("hi"));
                        }
                        else
                        {
                            if (Convert.ToString(corri.ApprovedTime) != "")
                            {
                                TimeSpan interval = TimeSpan.Parse(Convert.ToString(corri.ApprovedTime));
                                DateTime time = DateTime.Today.Add(interval);
                                string displayTime = time.ToString("hh:mm tt");

                                SessionTime = displayTime;
                            }
                            else
                            {
                                SessionTime = "";
                            }
                            date12 = Convert.ToDateTime(corri.ApprovedDate).ToString("D", new CultureInfo("hi"));
                        }
                    }
                    //Bottom Section
                    if (LOBType == "Draft")
                    {
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 24px;'>
                                               <b> (" + CurrentSpeaker + @"),</b>
                                                </td>
                                                <td style='width:25%;float:left;font-size: 24px;'>
                                                <b> (" + CurrentSecretery + @"),  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                 <td style='width:75%;float:left;font-size: 24px;padding-left:15%;'>
                                               <b> अध्यक्ष ।</b>
                                                </td>
                                                <td style='width:25%;float:right;text-align:right;padding-right:80px;font-size: 24px;'>
                                               <b>  सचिव।</b>
                                                </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";

                    }
                    else if (LOBType == "Approved")
                    {
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:70%;float:left;font-size: 24px;'>
                                               <b> " + CurrenttSessionPlace + @",</b>
                                                </td>
                                                <td style='width:30%;float:left;font-size: 24px;'>
                                                <b> " + CurrentSecretery + @",  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:70%;float:left;font-size: 24px;'>
                                                <b> दिनांक: " + date12 + @"</b>
                                               <b> &nbsp;&nbsp; " + SessionTime + @"</b>
                                                </td>
                                             <td style='width:25%;float:right;text-align:right;padding-right:85px;font-size: 24px;'>
                                               <b>  सचिव।</b>
                                                </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center;font-size: 24px; ' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center;font-size: 22px; ' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }
                }

                // convert HTML to PDF
                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(40, 45, 20, 20);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(10, 45, 55, 55), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(40, 20, 0, 0, htmlStringToConvert, baseURL);



                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }



                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msInput, null);
                //document.Close();

                //byte[] file = ms.ToArray();
                //output.Write(file, 0, file.Length);
                //output.Position = 0;


                ///To get File Path

                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";


                sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                sessiondate = sessiondate.Replace('/', ' ');



                if (download)
                {
                    if (LOBType == "Draft")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_LOB_" + CorrigendumId + ".pdf");
                        string Returl = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        //string directory = Server.MapPath(url);
                        //if (!Directory.Exists(directory))
                        //{
                        //    Directory.CreateDirectory(directory);
                        //}

                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string url = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(url);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Draft_LOB_" + CorrigendumId + ".pdf";
                        var path = Path.Combine(url, fileName);
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");

                        #endregion

                        return Returl + fileName;

                        //methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        //methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + "/" + fileName));
                        //ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedLOBPath]", methodParameter);
                        //return null;
                    }
                    else if (LOBType == "Approved")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Approved_LOB_" + CorrigendumId + ".pdf");
                        string Returl = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        //string directory = Server.MapPath(url);
                        //if (!Directory.Exists(directory))
                        //{
                        //    Directory.CreateDirectory(directory);
                        //}
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string url = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(url);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Approved_LOB_" + CorrigendumId + ".pdf";
                        var path = Path.Combine(url, fileName);
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion

                        return Returl + fileName;

                        //methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        //methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + "/" + fileName));
                        //ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedApprovedLOBPath]", methodParameter);
                        //return null;
                    }
                    else if (LOBType == "Published")
                    {

                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Published_" + LOBId + ".pdf");
                        string Returl = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        //string directory = Server.MapPath(url);
                        //if (!Directory.Exists(directory))
                        //{
                        //    Directory.CreateDirectory(directory);
                        //}
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string url = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(url);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Published_" + LOBId + ".pdf";
                        var path = Path.Combine(url, fileName);
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion

                        return Returl + fileName;

                        //methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        //methodParameter.Add(new KeyValuePair<string, string>("@PublishLOBPath", url + "/" + fileName));
                        //ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_PublishedLOBPath]", methodParameter);
                        //return null;
                    }
                }
            }

            return File(output, "application/pdf");
        }



        #region Commented Code
        //        [HttpGet]
        //        public FileStreamResult GenerateLOBPdf(string LOBId, bool download, string LOBType)
        //        {
        //            string LOBName = "20Dec2013_1_LOB";
        //            string CurrentSecretery = "";
        //            DateTime SessionDate = new DateTime();
        //            string savedPDF = string.Empty;


        //            BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        //            iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

        //            iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
        //            MemoryStream output = new MemoryStream();
        //            if (LOBId != null && LOBId != "")
        //            {
        //                LOBModel objLOBModel = new LOBModel();
        //                DataSet SummaryDataSet = new DataSet();
        //                string outXml = "";
        //                var methodParameter = new List<KeyValuePair<string, string>>();
        //                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));
        //                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);


        //                //Getting Current Secretary
        //                DataSet SiteSettingDataSet = new DataSet();
        //                var methodParameter1 = new List<KeyValuePair<string, string>>();
        //                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
        //                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
        //                {
        //                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
        //                    if (dtSiteSetting != null)
        //                    {
        //                        IEnumerable<DataRow> siteSettingQuery =
        //                                from siteSetting in dtSiteSetting.AsEnumerable()
        //                                select siteSetting;
        //                        IEnumerable<DataRow> curtSecretery =
        //                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
        //                        foreach (DataRow obj in curtSecretery)
        //                        {
        //                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
        //                        }
        //                    }
        //                }


        //                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
        //                {

        //                    outXml = @"<html>
        //                            <head> 
        //                            <style>
        //                            /* Style Definitions */
        //                            p.MsoNormal, li.MsoNormal, div.MsoNormal {
        //                                margin: 0in;
        //                                margin-bottom: .0001pt;
        //                                font-size: 12.0pt;
        //                                font-family: 'Times New Roman','serif';
        //                                #menu
        //                                {
        //                                    position: absolute;
        //                                    border-right: 1px solid black;
        //                                    height: 70%;
        //                                    color:#5779b6
        //                                }
        //
        //                            } </style>                               
        //                            </head>
        //                    <body encoding=" + Hindi + " style='font-family:mangal;font-size:12;'> <div><div style='width: 100%;'><table style='width: 100%;'>";



        //                    List<LOBModel> listLOBForDay = new List<LOBModel>();
        //                    string SrNo1 = "";
        //                    string SrNo2 = "";
        //                    string SrNo3 = "";
        //                    int SrNo1Count = 1;
        //                    int SrNo2Count = 1;
        //                    int SrNo3Count = 1;
        //                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
        //                    {

        //                        if (i == 0)
        //                        {
        //                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
        //                            string date = ConvertSQLDate(SessionDate);
        //                            date = date.Replace("/", "");
        //                            date = date.Replace("-", "");
        //                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


        //                            outXml += @"<tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
        //                                                          " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
        //                                                    </td>
        //                                                </tr>
        //                                                <tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>
        //                                                            कार्यसूची               
        //                                                    </td>
        //                                                </tr>
        //
        //                                                <tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold'>
        //                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
        //                                                    </td>
        //                                                </tr>
        //
        //                                                <tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold; height: 30px;'>                
        //                                                    </td>
        //                                                </tr>
        //
        //                                                <tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
        //                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
        //                                                    </td>
        //                                                </tr>
        //
        //                                                <tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>               
        //                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
        //                                                    </td>
        //                                                </tr>";
        //                        }


        //                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
        //                        {

        //                            SrNo1 = Convert.ToString(SrNo1Count) + ".";



        //                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
        //                                      <br/>  
        //                                    " + SrNo1 + @"";
        //                            SrNo1Count = SrNo1Count + 1;
        //                            SrNo2Count = 1;


        //                        }
        //                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
        //                        {

        //                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
        //                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
        //                                    " + SrNo2 + @"";
        //                            SrNo2Count = SrNo2Count + 1;
        //                            SrNo3Count = 1;


        //                        }
        //                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
        //                        {

        //                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
        //                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
        //                                    " + SrNo3 + @"";
        //                            SrNo3Count = SrNo3Count + 1;

        //                        }

        //                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
        //                        if (LOBText != "")
        //                        {
        //                            LOBText = LOBText.Replace("<p>", "");
        //                            LOBText = LOBText.Replace("</p>", "");
        //                        }
        //                        string Member = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberDesignationLocal"]) + ")";
        //                        string Department = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedDeptNameLocal"]);
        //                        string Committee = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedCommitteeNameLocal"]);
        //                        string Rule = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedRuleNameLocal"]);
        //                        outXml += @" 
        //                                                     " + LOBText;
        //                        if (Member != " ()")
        //                        {
        //                            outXml += @"<b> सदस्य: </b>" + Member;
        //                        }
        //                        if (Department != "")
        //                        {
        //                            outXml += @"<b> विभाग: </b> " + Department;
        //                        }
        //                        if (Committee != "")
        //                        {
        //                            outXml += @"<b> समिति: </b> " + Committee;
        //                        }
        //                        if (Rule != "")
        //                        {
        //                            outXml += @"<b> नियम: </b> " + Rule;
        //                        }

        //                        outXml += @"<br/> </td></tr>";


        //                    }


        //                    //Bottom Section
        //                    outXml += @" <tr><td colspan='2'><br /><br /><br /><br /><br /></td></tr>
        //
        //                                                <tr><td>
        //                                                <table style='width:100%;'>
        //                                                <tr>
        //                                                <td style='width:70%;float:left;'>
        //                                                शिमला-171004
        //                                                </td>
        //                                                <td style='width:30%;float:left;'>
        //                                                 " + CurrentSecretery + @"  
        //                                                </td>
        //                                                </tr>
        //                                                <br/>
        //                                                <tr>
        //                                                <td style='width:70%;float:left;'>
        //                                                " + SessionDate.ToString("D", new CultureInfo("hi")) + @"
        //                                                </td>
        //                                                <td style='width:30%;float:left;'>
        //                                                सचिव
        //                                                </td>
        //                                                </tr>
        //                                                </table>           
        //                                                    </td>
        //                                                    </tr>";

        //                    outXml += "</table></div></div> </body> </html>";

        //                }

        //                FontFactory.Register("c:/windows/fonts/ARIALUNI.TTF");
        //                StyleSheet style = new StyleSheet();
        //                style.LoadTagStyle("body", "face", "Arial Unicode MS");
        //                style.LoadTagStyle("body", "encoding", BaseFont.IDENTITY_H);

        //                MemoryStream ms = new MemoryStream();
        //                byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(outXml);
        //                MemoryStream msInput = new MemoryStream(byteArray);

        //                PdfWriter writer = PdfWriter.GetInstance(document, ms);
        //                document.Open();

        //                // convert HTML to PDF
        //                EvoPdf.Document document1 = new EvoPdf.Document();

        //                // set the license key
        //                document1.LicenseKey = "B4mYiJubiJiInIaYiJuZhpmahpGRkZE=";
        //                document1.CompressionLevel = PdfCompressionLevel.Normal;
        //                document1.Margins = new Margins(10, 10, 0, 0);
        //                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(10, 10, 0, 0), PdfPageOrientation.Portrait);

        //                EvoPdf.PdfFont font1 = document1.Fonts.Add(new System.Drawing.Font(new System.Drawing.FontFamily("Times New Roman"), 10,
        //                        System.Drawing.GraphicsUnit.Point));
        //                AddElementResult addResult;

        //                HtmlToPdfElement htmlToPdfElement;
        //                string htmlStringToConvert = outXml;
        //                string baseURL = "";
        //                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);


        //                addResult = page.AddElement(htmlToPdfElement);
        //                byte[] pdfBytes = document1.Save();

        //                try
        //                {

        //                    output.Write(pdfBytes, 0, pdfBytes.Length);
        //                    output.Position = 0;



        //                }
        //                finally
        //                {
        //                    // close the PDF document to release the resources
        //                    document1.Close();
        //                }



        //                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msInput, null);
        //                //document.Close();

        //                //byte[] file = ms.ToArray();
        //                //output.Write(file, 0, file.Length);
        //                //output.Position = 0;

        //                if (download)
        //                {
        //                    if (LOBType == "Draft")
        //                    {
        //                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_" + LOBId + ".pdf");
        //                        string url = "/LOB/" + LOBId;
        //                        string directory = Server.MapPath(url);
        //                        if (!Directory.Exists(directory))
        //                        {
        //                            Directory.CreateDirectory(directory);
        //                        }
        //                        string fileName = "Draft_" + LOBId + ".pdf";
        //                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
        //                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

        //                        // close file stream
        //                        _FileStream.Close();


        //                        #region FileTransfer save

        //                        //int fileLength = _FileStream.ContentLength;

        //                        //byte[] myData = new byte[fileLength];
        //                        //file.InputStream.Read(myData, 0, fileLength);
        //                        // Gor in Binary Data
        //                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
        //                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
        //                        sers.Serialize(wr, pdfBytes);
        //                        XmlDocument doc = new XmlDocument();
        //                        doc.LoadXml(sb.ToString());

        //                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");

        //                        #endregion


        //                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + "/" + fileName));
        //                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedLOBPath]", methodParameter);
        //                        return null;
        //                    }
        //                    else if (LOBType == "Approved")
        //                    {
        //                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Approved_" + LOBId + ".pdf");
        //                        string url = "/LOB/" + LOBId;
        //                        string directory = Server.MapPath(url);
        //                        if (!Directory.Exists(directory))
        //                        {
        //                            Directory.CreateDirectory(directory);
        //                        }
        //                        string fileName = "Approved_" + LOBId + ".pdf";
        //                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
        //                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

        //                        // close file stream
        //                        _FileStream.Close();


        //                        #region FileTransfer save

        //                        //int fileLength = _FileStream.ContentLength;

        //                        //byte[] myData = new byte[fileLength];
        //                        //file.InputStream.Read(myData, 0, fileLength);
        //                        // Gor in Binary Data
        //                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
        //                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
        //                        sers.Serialize(wr, pdfBytes);
        //                        XmlDocument doc = new XmlDocument();
        //                        doc.LoadXml(sb.ToString());

        //                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
        //                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

        //                        #endregion


        //                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + "/" + fileName));
        //                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedApprovedLOBPath]", methodParameter);
        //                        return null;
        //                    }
        //                    else if (LOBType == "Published")
        //                    {

        //                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Published_" + LOBId + ".pdf");
        //                        string url = "/LOB/" + LOBId;
        //                        string directory = Server.MapPath(url);
        //                        if (!Directory.Exists(directory))
        //                        {
        //                            Directory.CreateDirectory(directory);
        //                        }
        //                        string fileName = "Published_" + LOBId + ".pdf";
        //                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
        //                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

        //                        // close file stream
        //                        _FileStream.Close();


        //                        #region FileTransfer save

        //                        //int fileLength = _FileStream.ContentLength;

        //                        //byte[] myData = new byte[fileLength];
        //                        //file.InputStream.Read(myData, 0, fileLength);
        //                        // Gor in Binary Data
        //                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
        //                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
        //                        sers.Serialize(wr, pdfBytes);
        //                        XmlDocument doc = new XmlDocument();
        //                        doc.LoadXml(sb.ToString());

        //                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
        //                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

        //                        #endregion


        //                        methodParameter.Add(new KeyValuePair<string, string>("@PublishLOBPath", url + "/" + fileName));
        //                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_PublishedLOBPath]", methodParameter);
        //                        return null;
        //                    }
        //                }
        //            }

        //            return File(output, "application/pdf");
        //        }

        #endregion

        #region "commented Code for PDF Generation with itext sharp"
        //        [HttpGet]
        //        public FileStreamResult GenerateLOBPdf(string LOBId, bool download, string LOBType)
        //        {
        //            string LOBName = "20Dec2013_1_LOB";
        //            string CurrentSecretery = "";
        //            DateTime SessionDate = new DateTime();
        //            string savedPDF = string.Empty;


        //            BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        //            iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

        //            Document document = new Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
        //            MemoryStream output = new MemoryStream();
        //            if (LOBId != null && LOBId != "")
        //            {
        //                LOBModel objLOBModel = new LOBModel();
        //                DataSet SummaryDataSet = new DataSet();
        //                string outXml = "";
        //                var methodParameter = new List<KeyValuePair<string, string>>();
        //                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));
        //                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);


        //                //Getting Current Secretary
        //                DataSet SiteSettingDataSet = new DataSet();
        //                var methodParameter1 = new List<KeyValuePair<string, string>>();
        //                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
        //                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
        //                {
        //                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
        //                    if (dtSiteSetting != null)
        //                    {
        //                        IEnumerable<DataRow> siteSettingQuery =
        //                                from siteSetting in dtSiteSetting.AsEnumerable()
        //                                select siteSetting;
        //                        IEnumerable<DataRow> curtSecretery =
        //                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
        //                        foreach (DataRow obj in curtSecretery)
        //                        {
        //                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
        //                        }
        //                    }
        //                }


        //                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
        //                {

        //                    outXml = @"<html>
        //                            <head> 
        //                            <style>
        //                            /* Style Definitions */
        //                            p.MsoNormal, li.MsoNormal, div.MsoNormal {
        //                                margin: 0in;
        //                                margin-bottom: .0001pt;
        //                                font-size: 12.0pt;
        //                                font-family: 'Times New Roman','serif';
        //                                #menu
        //                                {
        //                                    position: absolute;
        //                                    border-right: 1px solid black;
        //                                    height: 70%;
        //                                    color:#5779b6
        //                                }
        //
        //                            } </style>                               
        //                            </head>
        //                    <body encoding=" + Hindi + " style='font-family:mangal;font-size:12;'> <div><div style='width: 100%;'><table style='width: 100%;'>";



        //                    List<LOBModel> listLOBForDay = new List<LOBModel>();
        //                    string SrNo1 = "";
        //                    string SrNo2 = "";
        //                    string SrNo3 = "";
        //                    int SrNo1Count = 1;
        //                    int SrNo2Count = 1;
        //                    int SrNo3Count = 1;
        //                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
        //                    {

        //                        if (i == 0)
        //                        {
        //                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
        //                            string date = ConvertSQLDate(SessionDate);
        //                            date = date.Replace("/", "");
        //                            date = date.Replace("-", "");
        //                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


        //                            outXml += @"<tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
        //                                                          " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
        //                                                    </td>
        //                                                </tr>
        //                                                <tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>
        //                                                            कार्यसूची               
        //                                                    </td>
        //                                                </tr>
        //
        //                                                <tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold'>
        //                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
        //                                                    </td>
        //                                                </tr>
        //
        //                                                <tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold; height: 30px;'>                
        //                                                    </td>
        //                                                </tr>
        //
        //                                                <tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
        //                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
        //                                                    </td>
        //                                                </tr>
        //
        //                                                <tr>
        //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>               
        //                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
        //                                                    </td>
        //                                                </tr>@";
        //                        }


        //                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
        //                        {

        //                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
        //                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
        //                                      <br/>  
        //                                    " + SrNo1 + @"";
        //                            SrNo1Count = SrNo1Count + 1;
        //                            SrNo2Count = 1;


        //                        }
        //                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
        //                        {

        //                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
        //                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
        //                                    " + SrNo2 + @"";
        //                            SrNo2Count = SrNo2Count + 1;
        //                            SrNo3Count = 1;


        //                        }
        //                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
        //                        {

        //                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
        //                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
        //                                    " + SrNo3 + @"";
        //                            SrNo3Count = SrNo3Count + 1;

        //                        }

        //                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
        //                        if (LOBText != "")
        //                        {
        //                            LOBText = LOBText.Replace("<p>", "");
        //                            LOBText = LOBText.Replace("</p>", "");
        //                        }

        //                        outXml += @" 
        //                                                     " + LOBText + @"  
        //                                                    <br/>            
        //                                                    </td>
        //                                                    </tr>";


        //                    }


        //                    //Bottom Section
        //                    outXml += @" <tr><td colspan='2'><br /><br /><br /><br /><br /></td></tr>
        //
        //                                                <tr><td>
        //                                                <table style='width:100%;'>
        //                                                <tr>
        //                                                <td style='width:70%;float:left;'>
        //                                                शिमला-171004
        //                                                </td>
        //                                                <td style='width:30%;float:left;'>
        //                                                 " + CurrentSecretery + @"  
        //                                                </td>
        //                                                </tr>
        //                                                <br/>
        //                                                <tr>
        //                                                <td style='width:70%;float:left;'>
        //                                                " + SessionDate.ToString("D", new CultureInfo("hi")) + @"
        //                                                </td>
        //                                                <td style='width:30%;float:left;'>
        //                                                सचिव
        //                                                </td>
        //                                                </tr>
        //                                                </table>           
        //                                                    </td>
        //                                                    </tr>";

        //                    outXml += "</table></div></div> </body> </html>";

        //                }

        //                FontFactory.Register("c:/windows/fonts/ARIALUNI.TTF");
        //                StyleSheet style = new StyleSheet();
        //                style.LoadTagStyle("body", "face", "Arial Unicode MS");
        //                style.LoadTagStyle("body", "encoding", BaseFont.IDENTITY_H);

        //                MemoryStream ms = new MemoryStream();
        //                byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(outXml);
        //                MemoryStream msInput = new MemoryStream(byteArray);

        //                PdfWriter writer = PdfWriter.GetInstance(document, ms);
        //                document.Open();

        //                XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msInput, null);
        //                document.Close();

        //                byte[] file = ms.ToArray();
        //                output.Write(file, 0, file.Length);
        //                output.Position = 0;

        //                if (download)
        //                {
        //                    if (LOBType == "Draft")
        //                    {
        //                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_" + LOBId + ".pdf");
        //                        string url = "/LOB/" + LOBId;
        //                        string directory = Server.MapPath(url);
        //                        if (!Directory.Exists(directory))
        //                        {
        //                            Directory.CreateDirectory(directory);
        //                        }
        //                        string fileName = "Draft_" + LOBId + ".pdf";
        //                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
        //                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                        _FileStream.Write(file, 0, file.Length);

        //                        // close file stream
        //                        _FileStream.Close();


        //                        #region FileTransfer save

        //                        //int fileLength = _FileStream.ContentLength;

        //                        //byte[] myData = new byte[fileLength];
        //                        //file.InputStream.Read(myData, 0, fileLength);
        //                        // Gor in Binary Data
        //                        XmlSerializer sers = new XmlSerializer(file.GetType());
        //                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
        //                        sers.Serialize(wr, file);
        //                        XmlDocument doc = new XmlDocument();
        //                        doc.LoadXml(sb.ToString());

        //                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, LOBId, "LOB");

        //                        #endregion


        //                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + "/" + fileName));
        //                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedLOBPath]", methodParameter);
        //                        return null;
        //                    }
        //                    else if (LOBType == "Approved")
        //                    {
        //                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Approved_" + LOBId + ".pdf");
        //                        string url = "/LOB/" + LOBId;
        //                        string directory = Server.MapPath(url);
        //                        if (!Directory.Exists(directory))
        //                        {
        //                            Directory.CreateDirectory(directory);
        //                        }
        //                        string fileName = "Approved_" + LOBId + ".pdf";
        //                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
        //                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                        _FileStream.Write(file, 0, file.Length);

        //                        // close file stream
        //                        _FileStream.Close();


        //                        #region FileTransfer save

        //                        //int fileLength = _FileStream.ContentLength;

        //                        //byte[] myData = new byte[fileLength];
        //                        //file.InputStream.Read(myData, 0, fileLength);
        //                        // Gor in Binary Data
        //                        XmlSerializer sers = new XmlSerializer(file.GetType());
        //                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
        //                        sers.Serialize(wr, file);
        //                        XmlDocument doc = new XmlDocument();
        //                        doc.LoadXml(sb.ToString());

        //                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, LOBId, "LOB");
        //                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

        //                        #endregion


        //                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + "/" + fileName));
        //                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedApprovedLOBPath]", methodParameter);
        //                        return null;
        //                    }
        //                    else if (LOBType == "Published")
        //                    {

        //                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Published_" + LOBId + ".pdf");
        //                        string url = "/LOB/" + LOBId;
        //                        string directory = Server.MapPath(url);
        //                        if (!Directory.Exists(directory))
        //                        {
        //                            Directory.CreateDirectory(directory);
        //                        }
        //                        string fileName = "Published_" + LOBId + ".pdf";
        //                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
        //                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                        _FileStream.Write(file, 0, file.Length);

        //                        // close file stream
        //                        _FileStream.Close();


        //                        #region FileTransfer save

        //                        //int fileLength = _FileStream.ContentLength;

        //                        //byte[] myData = new byte[fileLength];
        //                        //file.InputStream.Read(myData, 0, fileLength);
        //                        // Gor in Binary Data
        //                        XmlSerializer sers = new XmlSerializer(file.GetType());
        //                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
        //                        sers.Serialize(wr, file);
        //                        XmlDocument doc = new XmlDocument();
        //                        doc.LoadXml(sb.ToString());

        //                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, LOBId, "LOB");
        //                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

        //                        #endregion


        //                        methodParameter.Add(new KeyValuePair<string, string>("@PublishLOBPath", url + "/" + fileName));
        //                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_PublishedLOBPath]", methodParameter);
        //                        return null;
        //                    }
        //                }
        //            }

        //            return File(output, "application/pdf");
        //        }
        #endregion
//        Old code commented 
//        public void GeneratePdf(string LOBId)
//        {
//            try
//            {
//                string LOBName = "20Dec2013_1_LOB";
//                string CurrentSecretery = "";
//                DateTime SessionDate = new DateTime();
//                string fileName = "";
//                string savedPDF = string.Empty;
//                string url = "/HOUSE_LOB/CurrentAssembly/CurrentSession/CurrentLOB/";

//                BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//                iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

//                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
//                if (LOBId != null && LOBId != "")
//                {
//                    LOBModel objLOBModel = new LOBModel();
//                    DataSet SummaryDataSet = new DataSet();
//                    string outXml = "";
//                    var methodParameter = new List<KeyValuePair<string, string>>();
//                    methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));
//                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectPublishedLOBReportByLOBId]", methodParameter);


//                    //Getting Current Secretary
//                    DataSet SiteSettingDataSet = new DataSet();
//                    var methodParameter1 = new List<KeyValuePair<string, string>>();
//                    SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
//                    if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
//                    {
//                        DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
//                        if (dtSiteSetting != null)
//                        {
//                            IEnumerable<DataRow> siteSettingQuery =
//                                    from siteSetting in dtSiteSetting.AsEnumerable()
//                                    select siteSetting;
//                            IEnumerable<DataRow> curtSecretery =
//                                                 siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
//                            foreach (DataRow obj in curtSecretery)
//                            {
//                                CurrentSecretery = obj.Field<string>("SettingValueLocal");
//                            }
//                        }
//                    }


//                    if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
//                    {

//                        outXml = @"<html>
//                            <head> 
//                            <style>
//                            /* Style Definitions */
//                            p.MsoNormal, li.MsoNormal, div.MsoNormal {
//                                margin: 0in;
//                                margin-bottom: .0001pt;
//                                font-size: 12.0pt;
//                                font-family: 'Times New Roman','serif';
//                                #menu
//                                {
//                                    position: absolute;
//                                    border-right: 1px solid black;
//                                    height: 70%;
//                                    color:#5779b6
//                                }
//
//                            } </style>                               
//                            </head>
//                    <body encoding=" + Hindi + " style='font-family:mangal;font-size:12;'> <div><div style='width: 100%;'><table style='width: 100%;'>";



//                        List<LOBModel> listLOBForDay = new List<LOBModel>();
//                        string SrNo1 = "";
//                        string SrNo2 = "";
//                        string SrNo3 = "";
//                        int SrNo1Count = 1;
//                        int SrNo2Count = 1;
//                        int SrNo3Count = 1;
//                        for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
//                        {

//                            if (i == 0)
//                            {
//                                SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
//                                string date = ConvertSQLDate(SessionDate);
//                                date = date.Replace("/", "");
//                                date = date.Replace("-", "");
//                                LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


//                                outXml += @"<tr>
//                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
//                                                          " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
//                                                    </td>
//                                                </tr>
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>
//                                                            कार्यसूची               
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 25px; font-weight: bold'>
//                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 25px; font-weight: bold; height: 30px;'>                
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
//                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
//                                                    </td>
//                                                </tr>
//
//                                                <tr>
//                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>               
//                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
//                                                    </td>
//                                                </tr>@";
//                            }


//                            if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
//                            {

//                                SrNo1 = Convert.ToString(SrNo1Count) + ".";
//                                outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
//                                    " + SrNo1 + @"";
//                                SrNo1Count = SrNo1Count + 1;
//                                SrNo2Count = 1;


//                            }
//                            else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
//                            {

//                                SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
//                                outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
//                                    " + SrNo2 + @"";
//                                SrNo2Count = SrNo2Count + 1;
//                                SrNo3Count = 1;


//                            }
//                            else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
//                            {

//                                SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
//                                outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
//                                    " + SrNo3 + @"";
//                                SrNo3Count = SrNo3Count + 1;

//                            }

//                            string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
//                            if (LOBText != "")
//                            {
//                                LOBText = LOBText.Replace("<p>", "");
//                                LOBText = LOBText.Replace("</p>", "");
//                            }

//                            outXml += @" 
//                                                     " + LOBText + @"  
//                                                    <br/>            
//                                                    </td>
//                                                    </tr>";


//                        }


//                        //Bottom Section
//                        outXml += @" <tr><td colspan='2'><br /><br /><br /><br /><br /></td></tr>
//
//                                                <tr><td>
//                                                <table style='width:100%;'>
//                                                <tr>
//                                                <td style='width:70%;float:left;'>
//                                                शिमला-171004
//                                                </td>
//                                                <td style='width:30%;float:left;'>
//                                                 " + CurrentSecretery + @"  
//                                                </td>
//                                                </tr>
//                                                <br/>
//                                                <tr>
//                                                <td style='width:70%;float:left;'>
//                                                " + SessionDate.ToString("D", new CultureInfo("hi")) + @"
//                                                </td>
//                                                <td style='width:30%;float:left;'>
//                                                सचिव
//                                                </td>
//                                                </tr>
//                                                </table>           
//                                                    </td>
//                                                    </tr>";

//                        outXml += "</table></div></div> </body> </html>";

//                    }

//                    MemoryStream ms = new MemoryStream();
//                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(outXml);
//                    MemoryStream msInput = new MemoryStream(byteArray);
//                    PdfWriter Pwriter = PdfWriter.GetInstance(document, ms);
//                    document.Open();

//                    XMLWorkerHelper.GetInstance().ParseXHtml(Pwriter, document, msInput, null);
//                    document.Close();

//                    byte[] file = ms.ToArray();
//                    MemoryStream output = new MemoryStream();
//                    output.Write(file, 0, file.Length);
//                    output.Position = 0;


//                    string directory = Server.MapPath(url);
//                    if (!Directory.Exists(directory))
//                    {
//                        Directory.CreateDirectory(directory);
//                    }
//                    fileName = "eVidhan_" + LOBName.Replace("/", "_") + ".pdf";
//                    var path = Path.Combine(Server.MapPath("~" + url), fileName);

//                    if (System.IO.File.Exists(path) == true)
//                    {
//                        savedPDF = url + fileName;
//                        ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(savedPDF);
//                        //System.IO.File.Delete(path);
//                    }
//                    else
//                    {
//                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
//                        _FileStream.Write(file, 0, file.Length);
//                        _FileStream.Close();

//                        PdfWriter.GetInstance(document, new FileStream(Request.PhysicalApplicationPath + @"PDFLOB" + fileName + "", FileMode.Create));
//                        savedPDF = Path.Combine(url, fileName);

//                        output.Close();
//                        Pwriter.Close();

//                        string url1 = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority;
//                        string pdfurl = string.Empty;

//                        if (savedPDF.Contains(url1) == true)
//                        {
//                            pdfurl = savedPDF.Replace(url, "");
//                        }
//                        else
//                        {
//                            pdfurl = savedPDF;
//                        }
//                        ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(pdfurl);

//                    }



//                }





//            }
//            catch (Exception ex)
//            {

//                throw ex;
//            }
//            finally
//            {

//            }


//        }
        public void GeneratePdf(string LOBId)
        {
            try
            {
                string LOBName = "20Dec2013_1_LOB";
                string CurrentSecretery = "";
                DateTime SessionDate = new DateTime();
                string CurrenttSessionPlace = "";
                string fileName = "";
                string savedPDF = string.Empty;
                string url = "/HOUSE_LOB/CurrentAssembly/CurrentSession/CurrentLOB/";

                BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                if (LOBId != null && LOBId != "")
                {
                    LOBModel objLOBModel = new LOBModel();
                    DataSet SummaryDataSet = new DataSet();
                    string outXml = "";
                    var methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectPublishedLOBReportByLOBId]", methodParameter);


                    //Getting Current Secretary
                    DataSet SiteSettingDataSet = new DataSet();
                    var methodParameter1 = new List<KeyValuePair<string, string>>();
                    SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
                    if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
                    {
                        DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
                        if (dtSiteSetting != null)
                        {
                            IEnumerable<DataRow> siteSettingQuery =
                                    from siteSetting in dtSiteSetting.AsEnumerable()
                                    select siteSetting;
                            IEnumerable<DataRow> curtSecretery =
                                                 siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
                            foreach (DataRow obj in curtSecretery)
                            {
                                CurrentSecretery = obj.Field<string>("SettingValueLocal");
                            }
                            curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "SessionPlace");
                            foreach (DataRow obj in curtSecretery)
                            {
                                CurrenttSessionPlace = obj.Field<string>("SettingValueLocal");
                            }
                        }
                    }


                    if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                    {

                        outXml = @"<html>
                            <head> 
                            <style>
                            /* Style Definitions */
                            p.MsoNormal, li.MsoNormal, div.MsoNormal {
                                margin: 0in;
                                margin-bottom: .0001pt;
                                font-size: 12.0pt;
                                font-family: 'Times New Roman','serif';
                                #menu
                                {
                                    position: absolute;
                                    border-right: 1px solid black;
                                    height: 70%;
                                    color:#5779b6
                                }

                            } </style>                               
                            </head>
                    <body encoding=" + Hindi + " style='font-family:mangal;font-size:12;'> <div><div style='width: 100%;'><table style='width: 100%;'>";



                        List<LOBModel> listLOBForDay = new List<LOBModel>();
                        string SrNo1 = "";
                        string SrNo2 = "";
                        string SrNo3 = "";
                        int SrNo1Count = 1;
                        int SrNo2Count = 1;
                        int SrNo3Count = 1;
                        for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                        {

                            if (i == 0)
                            {
                                SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                                string date = ConvertSQLDate(SessionDate);
                                date = date.Replace("/", "");
                                date = date.Replace("-", "");
                                LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


                                outXml += @"<tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
                                                          " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>
                                                            कार्यसूची               
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold'>
                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold; height: 30px;'>                
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>               
                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
                                                    </td>
                                                </tr>@";
                            }


                            if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                            {

                                SrNo1 = Convert.ToString(SrNo1Count) + ".";
                                outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                    " + SrNo1 + @"";
                                SrNo1Count = SrNo1Count + 1;
                                SrNo2Count = 1;


                            }
                            else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                            {

                                SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                                outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                    " + SrNo2 + @"";
                                SrNo2Count = SrNo2Count + 1;
                                SrNo3Count = 1;


                            }
                            else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
                            {

                                SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                                outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                    " + SrNo3 + @"";
                                SrNo3Count = SrNo3Count + 1;

                            }

                            string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                            if (LOBText != "")
                            {
                                LOBText = LOBText.Replace("<p>", "");
                                LOBText = LOBText.Replace("</p>", "");
                            }

                            outXml += @" 
                                                     " + LOBText + @"  
                                                    <br/>            
                                                    </td>
                                                    </tr>";


                        }


                        //Bottom Section
                        outXml += @" <tr><td colspan='2'><br /><br /><br /><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:70%;float:left;'>
                                                 " + CurrenttSessionPlace + @"
                                                </td>
                                                <td style='width:30%;float:left;'>
                                                 " + CurrentSecretery + @"  
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:70%;float:left;'>
                                                " + SessionDate.ToString("D", new CultureInfo("hi")) + @"
                                                </td>
                                                <td style='width:30%;float:left;'>
                                                सचिव
                                                </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";

                    }

                    MemoryStream ms = new MemoryStream();
                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(outXml);
                    MemoryStream msInput = new MemoryStream(byteArray);
                    PdfWriter Pwriter = PdfWriter.GetInstance(document, ms);
                    document.Open();

                    XMLWorkerHelper.GetInstance().ParseXHtml(Pwriter, document, msInput, null);
                    document.Close();

                    byte[] file = ms.ToArray();
                    MemoryStream output = new MemoryStream();
                    output.Write(file, 0, file.Length);
                    output.Position = 0;


                    string directory = Server.MapPath(url);
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    fileName = "eVidhan_" + LOBName.Replace("/", "_") + ".pdf";
                    var path = Path.Combine(Server.MapPath("~" + url), fileName);

                    if (System.IO.File.Exists(path) == true)
                    {
                        savedPDF = url + fileName;
                        ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(savedPDF);
                        //System.IO.File.Delete(path);
                    }
                    else
                    {
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(file, 0, file.Length);
                        _FileStream.Close();

                        PdfWriter.GetInstance(document, new FileStream(Request.PhysicalApplicationPath + @"PDFLOB" + fileName + "", FileMode.Create));
                        savedPDF = Path.Combine(url, fileName);

                        output.Close();
                        Pwriter.Close();

                        string url1 = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority;
                        string pdfurl = string.Empty;

                        if (savedPDF.Contains(url1) == true)
                        {
                            pdfurl = savedPDF.Replace(url, "");
                        }
                        else
                        {
                            pdfurl = savedPDF;
                        }
                        ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(pdfurl);

                    }



                }





            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }

        #endregion


      

        #region "LOB Reports"

        public ActionResult LOBReportDayWise()
        {

            LOBModel objLOBModel = new LOBModel();
            string CurrentAssembly = "";
            string CurrentSession = "";

            //Getting Current Settings            
            DataSet SiteSettingDataSet = new DataSet();
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            var methodParameter1 = new List<KeyValuePair<string, string>>();
            SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
            if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
            {
                DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
                if (dtSiteSetting != null)
                {
                    IEnumerable<DataRow> siteSettingQuery =
                            from siteSetting in dtSiteSetting.AsEnumerable()
                            select siteSetting;
                    //Current Assembly
                    IEnumerable<DataRow> curtAssebly =
                                         siteSettingQuery.Where(p => p.Field<string>("SettingName") == "Assembly");
                    foreach (DataRow obj in curtAssebly)
                    {
                        CurrentAssembly = obj.Field<string>("SettingValue");
                    }
                    //Current Session
                    IEnumerable<DataRow> curtSession =
                                         siteSettingQuery.Where(p => p.Field<string>("SettingName") == "Session");
                    foreach (DataRow obj in curtSession)
                    {
                        CurrentSession = obj.Field<string>("SettingValue");
                    }
                    objLOBModel.SessionId = CurrentSession;
                    objLOBModel.AssemblyId = CurrentAssembly;

                }
                else
                {
                    objLOBModel.SessionId = "0";
                    objLOBModel.AssemblyId = "0";
                }


                ///getting the assembly Information
                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetAssembly = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectAssemblyInfoById", methodParameter);
                List<mAssembly> ListAssembly = new List<mAssembly>();
                if (dataSetAssembly != null && dataSetAssembly.Tables.Count > 0)
                {
                    for (int i = 0; i < dataSetAssembly.Tables[0].Rows.Count; i++)
                    {
                        mAssembly obj = new mAssembly();
                        obj.AssemblyID = Convert.ToString(dataSetAssembly.Tables[0].Rows[i]["AssemblyID"]);
                        obj.AssemblyName = Convert.ToString(dataSetAssembly.Tables[0].Rows[i]["AssemblyName"]);
                        ListAssembly.Add(obj);
                    }
                    objLOBModel.mAssemblyList = ListAssembly;
                }


                ///getting the session information
                methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@AssemblyId", CurrentAssembly));
                DataSet dataSetSession = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionInfoByAssemblyId", methodParameter);

                if (dataSetSession != null && dataSetSession.Tables.Count > 0)
                {
                    List<mSession> ListSession = new List<mSession>();
                    for (int i = 0; i < dataSetSession.Tables[0].Rows.Count; i++)
                    {
                        mSession obj = new mSession();
                        obj.SessionId = Convert.ToString(dataSetSession.Tables[0].Rows[i]["SessionID"]);
                        obj.Name = Convert.ToString(dataSetSession.Tables[0].Rows[i]["Name"]);
                        ListSession.Add(obj);
                    }
                    objLOBModel.mSessionList = ListSession;
                }


                //Getting Department Information
                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetDept = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectDepartment", methodParameter);

                if (dataSetDept != null && dataSetDept.Tables.Count > 0)
                {
                    List<mDepartment> DepartmentList = new List<mDepartment>();
                    mDepartment Department2 = new mDepartment();
                    Department2.DeptId = "0";
                    Department2.DeptName = "Select";
                    DepartmentList.Add(Department2);
                    for (int i = 0; i < dataSetDept.Tables[0].Rows.Count; i++)
                    {
                        mDepartment Department = new mDepartment();
                        Department.DeptId = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptId"]);
                        Department.DeptName = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptname"]);
                        DepartmentList.Add(Department);
                    }
                    objLOBModel.mDepartmentList = DepartmentList;
                }


                // Getting Minister Informations
                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetMemebers = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectMembers", methodParameter);

                if (dataSetMemebers != null && dataSetMemebers.Tables.Count > 0)
                {
                    List<mMinister> ministerList = new List<mMinister>();
                    mMinister Minister2 = new mMinister();
                    Minister2.MinisterId = "0";
                    Minister2.MinisterName = "Select";
                    ministerList.Add(Minister2);
                    for (int i = 0; i < dataSetMemebers.Tables[0].Rows.Count; i++)
                    {
                        mMinister Minister = new mMinister();
                        Minister.MinisterId = Convert.ToString(dataSetMemebers.Tables[0].Rows[i]["MemberID"]);
                        Minister.MinisterName = Convert.ToString(dataSetMemebers.Tables[0].Rows[i]["Name"]);
                        ministerList.Add(Minister);
                    }
                    objLOBModel.mMinisterList = ministerList;
                }

                // Getting Committee Informations
                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetCommittee = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommittee", methodParameter);

                if (dataSetCommittee != null && dataSetCommittee.Tables.Count > 0)
                {

                    List<mCommittee> CommitteeList = new List<mCommittee>();
                    mCommittee Committee2 = new mCommittee();
                    Committee2.CommitteeId = "0";
                    Committee2.CommitteeName = "Select";
                    CommitteeList.Add(Committee2);
                    for (int i = 0; i < dataSetCommittee.Tables[0].Rows.Count; i++)
                    {
                        mCommittee Committee = new mCommittee();
                        Committee.CommitteeId = Convert.ToString(dataSetCommittee.Tables[0].Rows[i]["CommitteeId"]);
                        Committee.CommitteeName = Convert.ToString(dataSetCommittee.Tables[0].Rows[i]["CommitteeName"]);
                        CommitteeList.Add(Committee);
                    }
                    objLOBModel.mCommitteeList = CommitteeList;
                }

                //Getting Session Date Information
                ///getting the session information
                methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession));
                DataSet dataSetSessionDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionInfoById", methodParameter);

                List<mSession> ListSessionDate = new List<mSession>();
                for (int i = 0; i < dataSetSessionDate.Tables[0].Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        objLOBModel.SessionName = Convert.ToString(dataSetSessionDate.Tables[0].Rows[i]["Name"]);
                        objLOBModel.SessionTime = Convert.ToString(dataSetSessionDate.Tables[0].Rows[i]["SessionTimeF"]);
                    }
                    mSession Session = new mSession();
                    Session.SessionDate = Convert.ToDateTime(Convert.ToString(dataSetSessionDate.Tables[0].Rows[i]["SessionDate"])).ToString("dd/MM/yyyy");
                    ListSessionDate.Add(Session);
                }
                objLOBModel.mSessionDateList = ListSessionDate;


            }
            return View(objLOBModel);
        }

        /// <summary>
        /// Will render the PartialPendingLOB PartialView
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialDaysLOBReport(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            DataSet SummaryDataSet = new DataSet();
            LOBModel objPendingLOB = new LOBModel();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
            methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
            SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectLOBByParameters]", methodParameter);

            if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
            {
                List<LOBModel> listLOBForDay = new List<LOBModel>();
                for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                {
                    LOBModel objLOB = new LOBModel();
                    objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                    objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                    objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                    objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                    objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                    objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                    objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                    //objLOB.ConcernedCommitteeName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedCommitteeName"]);
                    //objLOB.ConcernedMemberName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberName"]);
                    //objLOB.ConcernedDeptName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedDeptName"]);
                    if (i == 0)
                    {
                        objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                    }
                    listLOBForDay.Add(objLOB);
                }

                objPendingLOB.ListLOBBYId = listLOBForDay;
            }

            if (PageNumber != null && PageNumber != "")
            {
                objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                objPendingLOB.PageNumber = Convert.ToInt32("1");
            }

            if (RowsPerPage != null && RowsPerPage != "")
            {
                objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                objPendingLOB.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                objPendingLOB.selectedPage = Convert.ToInt32("1");
            }

            if (loopStart != null && loopStart != "")
            {
                objPendingLOB.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                objPendingLOB.loopStart = Convert.ToInt32("1");
            }

            if (loopEnd != null && loopEnd != "")
            {
                objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                objPendingLOB.loopEnd = Convert.ToInt32("5");
            }


            return PartialView("PartialDaysLOBReport", objPendingLOB);
        }


        #endregion

        #region "Comman Methods"

        /// <summary>
        /// convert the number in the Roman
        /// Created by : Sujeet Kumar
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "m" + ToRoman(number - 1000);
            if (number >= 900) return "cm" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "d" + ToRoman(number - 500);
            if (number >= 400) return "cd" + ToRoman(number - 400);
            if (number >= 100) return "c" + ToRoman(number - 100);
            if (number >= 90) return "xc" + ToRoman(number - 90);
            if (number >= 50) return "l" + ToRoman(number - 50);
            if (number >= 40) return "xl" + ToRoman(number - 40);
            if (number >= 10) return "x" + ToRoman(number - 10);
            if (number >= 9) return "ix" + ToRoman(number - 9);
            if (number >= 5) return "v" + ToRoman(number - 5);
            if (number >= 4) return "iv" + ToRoman(number - 4);
            if (number >= 1) return "i" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        public string ConvertSQLDate(DateTime? dt)
        {
            DateTime ConvtDate = Convert.ToDateTime(dt);
            string month = ConvtDate.Month.ToString();
            if (month.Length < 2)
            {
                month = "0" + month;
            }
            string day = ConvtDate.Day.ToString();
            string year = ConvtDate.Year.ToString();
            string Date = day + "/" + month + "/" + year;
            return Date;
        }

        #endregion

        private string StripHTML(string source)
        {
            try
            {
                string result;

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = source.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove all others. More can be added, see

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // for testing
                //System.Text.RegularExpressions.Regex.Replace(result,
                //       this.txtRegex.Text,string.Empty,
                //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // make line breaking consistent
                result = result.Replace("\n", "\r");

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4.
                // Prepare first to remove any whitespaces in between
                // the escaped characters and remove redundant tabs in between line breaks
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove multiple tabs following a line break with just one tab
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Initial replacement target string for line breaks
                string breaks = "\r\r\r";
                // Initial replacement target string for tabs
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }

                // That's it.
                return result;
            }
            catch
            {

                return source;
            }
        }

        public ActionResult Test()
        {
            return View();
        }

        #region "Create XML"

        public string CreateXML(string LOBId, string Form)
        {
            var methodParameter = new List<KeyValuePair<string, string>>();
            DataSet dataset = null;
            methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));
            if (Form == "Draft")
            {

                dataset = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBForXML]", methodParameter);
            }
            else
            {
                dataset = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
            }



            if (dataset != null)
            {

                XElement LOB = new System.Xml.Linq.XElement("LOB");
                XElement pages = new System.Xml.Linq.XElement("pages");
                LOB.Add(pages);
                XElement header = new XElement("header-page");
                //header.Attribute("id").Value="header_page";
                header.SetAttributeValue("id", "header_page");
                pages.Add(header);
                XElement content = new XElement("content");
                header.Add(content);



                DateTime SessionDate = Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]);
                string sessiondate = Convert.ToString(dataset.Tables[0].Rows[0]["SessionDate"]);
                string date = ConvertSQLDate(SessionDate);
                date = date.Replace("/", "");
                date = date.Replace("-", "");
                string LOBName = date + "_" + "LOB" + "_" + Convert.ToString(dataset.Tables[0].Rows[0]["LOBId"]);

                content.ReplaceNodes(new XCData(@"<h2 style='text-align: center;font-family:DVOT-Yogesh;'>" + Convert.ToString(dataset.Tables[0].Rows[0]["AssemblyNameLocal"]) + @"</h2>
                    <h4 style='text-align: center;font-family:DVOT-Yogesh;'>कार्य - सूची</h4>
                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionNameLocal"]) + @"  </h4>
                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionDateLocal"]) + @"  " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionTimeLocal"]) + @" बजे शुरू</h4>"));


                LOB.SetAttributeValue("AssemblyId", Convert.ToString(dataset.Tables[0].Rows[0]["AssemblyId"]));
                LOB.SetAttributeValue("SessionId", Convert.ToString(dataset.Tables[0].Rows[0]["SessionId"]));
                LOB.SetAttributeValue("SessionDate", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd/MM/yyyy"));

                //page
                int idCount = 2;
                string SrNo1 = "";
                string SrNo2 = "";
                string SrNo3 = "";
                int SrNo1Count = 1;
                int SrNo2Count = 1;
                int SrNo3Count = 1;
                int MainEventId = 2;
                string ConcernedEventId = "";
                string outXml = "";

                //XElement page = new XElement("page");
                //XElement cont = new XElement("content");

                XElement page1 = new XElement("page");
                // XElement cont1 = new XElement("content");
                XElement business = new XElement("business");
                //  page1.Add(business);
                //Index
                XElement index = new XElement("index-page");
                index.SetAttributeValue("id", "index_page");
                pages.Add(index);
                XElement indexContent = new XElement("content");
                StringBuilder sb = new StringBuilder();

                XElement indexOne = new XElement("index");
                int businessNo = 1;

                for (int count = 0; count < dataset.Tables[0].Rows.Count; count++)
                {
                    ConcernedEventId = Convert.ToString(dataset.Tables[0].Rows[count]["ConcernedEventId"]);
                    business = new XElement("business");

                    if (count == 0)
                    {
                        page1.SetAttributeValue("id", 2);
                        page1.SetAttributeValue("page-des", "first-page");
                        page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));
                        businessNo = 1;


                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    XElement subactions = new XElement("actions");
                        //    page1.Add(subactions);
                        //    XElement subactionOne = new XElement("action");
                        //    subactionOne.SetAttributeValue("id", "PDF");
                        //    subactionOne.SetAttributeValue("command", "OPEN_PDF");
                        //    subactionOne.SetAttributeValue("commandvalue", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //    subactionOne.SetAttributeValue("image", "place.png");
                        //    subactionOne.SetAttributeValue("enabled", "true");
                        //    subactions.Add(subactionOne);
                        //}


                    }
                    else
                    {
                        if (Convert.ToInt32(dataset.Tables[0].Rows[count - 1]["SrNo1"]) != Convert.ToInt32(dataset.Tables[0].Rows[count]["SrNo1"]))
                        {
                            // cont1.ReplaceNodes(new XCData(outXml));
                            //    page1.Add(business);
                            //   pages.Add(page1);

                            //     outXml = "";
                        }

                    }




                    if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == true && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == true)
                    {

                        if (count != 0)
                        {
                            MainEventId++;
                            businessNo = 1;
                            //  page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            /// business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);
                            page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));
                        }


                        ///For Index
                        string LOBText1 = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string FinalText = LOBText1.Replace("<i>", "<span>&nbsp;");
                        string FinalTt = FinalText.Replace("</i>", "</span>");
                        if (LOBText1 != "")
                        {
                            FinalTt = LOBText1.Replace("<p>", "");
                            FinalTt = LOBText1.Replace("</p>", "");
                            FinalTt = @"<span style='font-family:DVOT-Yogesh;'>" + LOBText1 + "</span>";

                        }
                        sb.Append("<li>" + LOBText1 + "</li>");
                        XElement subIndex1 = new XElement("index");
                        subIndex1.SetAttributeValue("id", idCount);

                        LOBText1 = StripHTML(LOBText1);
                        string Lobtext1 = LOBText1.Replace("\r\r", "");
                        subIndex1.SetAttributeValue("description", Lobtext1);
                        subIndex1.SetAttributeValue("command", "test_command");
                        subIndex1.SetAttributeValue("commandvalue", MainEventId);
                        subIndex1.SetAttributeValue("enabled", "true");
                        indexOne.Add(subIndex1);




                        // }


                        /////For first page Index
                        //string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        //if (LOBText != "")
                        //{
                        //    LOBText = LOBText.Replace("<p>", "");
                        //    LOBText = LOBText.Replace("</p>", "");
                        //}

                        //sb.Append("<li>" + LOBText + "</li>");
                        //XElement subIndex = new XElement("index");
                        //subIndex.SetAttributeValue("id", 1);

                        //LOBText = StripHTML(LOBText);
                        //subIndex.SetAttributeValue("description", LOBText);
                        //subIndex.SetAttributeValue("command", "test_command");
                        //subIndex.SetAttributeValue("enabled", "true");
                        //indexOne.Add(subIndex);


                        /// For contant
                        outXml = "";
                        SrNo1 = Convert.ToString(SrNo1Count) + ". ";
                        outXml += @" <p style='font-size:24px;text-align:justify;'><span style='font-family:DVOT-Yogesh;'>
                                    
                                    " + SrNo1 + @"";
                        string LOBTexts1 = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string FinalTexts1 = LOBTexts1.Replace("<i>", "<span>&nbsp;");
                        string LOBText = FinalTexts1.Replace("</i>", "</span>");
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        outXml += LOBText + "</span></p>";
                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) == "")
                        //{
                        //    outXml += "<br/>";
                        //}


                        XElement contentOne = new XElement("content");
                        //contentOne.SetAttributeValue("id", SrNo1);
                        //contentOne.SetAttributeValue("level", "1");
                        //contentOne.SetAttributeValue("Set", SrNo1);
                        //  contentOne.SetAttributeValue("value", LOBText);




                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    contentOne.SetAttributeValue("action", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //}
                        contentOne.ReplaceNodes(new XCData(outXml));

                        business.SetAttributeValue("id", MainEventId + "." + businessNo);
                        business.SetAttributeValue("MainEventId", MainEventId);

                        business.SetAttributeValue("ConcernedEventId", ConcernedEventId);
                        var methodParameter3 = new List<KeyValuePair<string, string>>();
                        methodParameter3.Add(new KeyValuePair<string, string>("@XmlEventId", MainEventId + "." + businessNo.ToString()));
                        methodParameter3.Add(new KeyValuePair<string, string>("@Id", dataset.Tables[0].Rows[count]["Id"].ToString()));
                        methodParameter3.Add(new KeyValuePair<string, string>("@LobType", Form));
                        ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_UpdateXmlEventId]", methodParameter3);



                        string EventId = Convert.ToString(dataset.Tables[0].Rows[count]["ConcernedEventId"]);

                        DataSet dataset2 = null;
                        var methodParameter4 = new List<KeyValuePair<string, string>>();
                        methodParameter4.Add(new KeyValuePair<string, string>("@Id", EventId));
                        dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetPaperTypeByEventId]", methodParameter4);
                        if (dataset2 != null)
                        {
                            if (!DBNull.Value.Equals(dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"]))
                            {
                                string papertypeid = dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"].ToString();
                                business.SetAttributeValue("PaperCatTypeId", papertypeid);
                            }
                        }
                        string SNo = dataset.Tables[0].Rows[count]["SrNo1"].ToString();
                        business.SetAttributeValue("SerialNumber", SNo);
                        string pdflocation = dataset.Tables[0].Rows[count]["PDFLocation"].ToString();
                        if (pdflocation != "" && pdflocation != null)
                        {
                            if (Form == "Draft")
                            {

                                var methodParameter5 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["Id"].ToString();
                                methodParameter5.Add(new KeyValuePair<string, string>("@Id", draftid));

                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter5);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                            else
                            {

                                var methodParameter5 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["DraftRecordId"].ToString();
                                methodParameter5.Add(new KeyValuePair<string, string>("@Id", draftid));
                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter5);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                        }

                        if (EventId == "3" || EventId == "4")
                        {
                            business.SetAttributeValue("Question", "True");
                        }
                        else
                        {
                            business.SetAttributeValue("Question", "False");
                        }

                        business.Add(contentOne);
                        businessNo++;
                        ///For Start and Stop Event Actions
                        XElement subactions = new XElement("actions");
                        business.Add(subactions);
                        XElement subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STARTEVENT");
                        subactionOne.SetAttributeValue("command", "START_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "start.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STOPEVENT");
                        subactionOne.SetAttributeValue("command", "STOP_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "stop.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        ///For show Atteched PDF action
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        {
                            string name = Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]);
                            int index1 = name.LastIndexOf('/');
                            name = name.Substring(index1 + 1);

                            subactionOne = new XElement("action");
                            subactionOne.SetAttributeValue("id", "ATTACHED_PDF");
                            subactionOne.SetAttributeValue("command", "OPEN_PDF");
                            subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                            subactionOne.SetAttributeValue("commandvalue", name);
                            subactionOne.SetAttributeValue("image", "place.png");
                            subactionOne.SetAttributeValue("concernedmember", "");
                            subactionOne.SetAttributeValue("enabled", "true");
                            subactions.Add(subactionOne);
                        }

                        SrNo1Count = SrNo1Count + 1;
                        SrNo2Count = 1;

                        //for page break
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PageBreak"]) == "True")
                        {
                            page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);

                            page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));

                        }
                        else
                        {
                            page1.Add(business);

                        }

                    }
                    else if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == true)
                    {
                        outXml = "";
                        SrNo2 = " (" + Convert.ToString(SrNo2Count) + ") ";
                        outXml += @" <p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:22px;padding-left: 25px'><span>
                                    " + SrNo2 + @" ";
                        //string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string LOBTexts2 = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string FinalTexts2 = LOBTexts2.Replace("<i>", "<span>&nbsp;");
                        string LOBText = FinalTexts2.Replace("</i>", "</span>");

                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        outXml += LOBText + "</span></p>";
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) == "")
                        {
                            outXml += "<br/>";
                        }
                        XElement contentOne = new XElement("content");
                        //contentOne.SetAttributeValue("id", Convert.ToString(SrNo2Count));
                        //contentOne.SetAttributeValue("level", "2");
                        //contentOne.SetAttributeValue("Set", SrNo1 + "." + SrNo2);
                        //  contentOne.SetAttributeValue("value", LOBText);
                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    contentOne.SetAttributeValue("action", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //}
                        contentOne.ReplaceNodes(new XCData(outXml));

                        business.SetAttributeValue("id", MainEventId + "." + businessNo);

                        business.SetAttributeValue("MainEventId", MainEventId);
                        business.SetAttributeValue("ConcernedEventId", ConcernedEventId);
                        var methodParameter4 = new List<KeyValuePair<string, string>>();
                        methodParameter4.Add(new KeyValuePair<string, string>("@XmlEventId", MainEventId + "." + businessNo.ToString()));
                        methodParameter4.Add(new KeyValuePair<string, string>("@Id", dataset.Tables[0].Rows[count]["Id"].ToString()));
                        methodParameter4.Add(new KeyValuePair<string, string>("@LobType", Form));
                        ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_UpdateXmlEventId]", methodParameter4);

                        string EventId = Convert.ToString(dataset.Tables[0].Rows[count]["ConcernedEventId"]);

                        DataSet dataset2 = null;
                        var methodParameter5 = new List<KeyValuePair<string, string>>();
                        methodParameter5.Add(new KeyValuePair<string, string>("@Id", EventId));
                        dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetPaperTypeByEventId]", methodParameter5);
                        if (dataset2 != null)
                        {
                            if (!DBNull.Value.Equals(dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"]))
                            {
                                string papertypeid = dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"].ToString();
                                business.SetAttributeValue("PaperCatTypeId", papertypeid);
                            }
                        }
                        string SNo1 = dataset.Tables[0].Rows[count]["SrNo1"].ToString();
                        string SNo2 = dataset.Tables[0].Rows[count]["SrNo2"].ToString();
                        business.SetAttributeValue("SerialNumber", SNo1 + "." + SNo2);
                        string pdflocation = dataset.Tables[0].Rows[count]["PDFLocation"].ToString();
                        if (pdflocation != "" && pdflocation != null)
                        {
                            if (Form == "Draft")
                            {
                                var methodParameter6 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["Id"].ToString();
                                methodParameter6.Add(new KeyValuePair<string, string>("@Id", draftid));

                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter6);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                            else
                            {

                                var methodParameter6 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["DraftRecordId"].ToString();
                                methodParameter6.Add(new KeyValuePair<string, string>("@Id", draftid));
                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter6);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                        }
                        if (EventId == "3" || EventId == "4")
                        {
                            business.SetAttributeValue("Question", "True");
                        }
                        else
                        {
                            business.SetAttributeValue("Question", "False");
                        }

                        business.Add(contentOne);
                        businessNo++;


                        ///For Start and Stop Event Actions
                        XElement subactions = new XElement("actions");
                        business.Add(subactions);
                        XElement subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STARTEVENT");
                        subactionOne.SetAttributeValue("command", "START_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "start.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STOPEVENT");
                        subactionOne.SetAttributeValue("command", "STOP_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "stop.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        ///for the action tab for pdf link
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        {
                            string name = Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]);
                            int index1 = name.LastIndexOf('/');
                            name = name.Substring(index1 + 1);

                            subactionOne = new XElement("action");
                            subactionOne.SetAttributeValue("id", "ATTACHED_PDF");
                            subactionOne.SetAttributeValue("command", "OPEN_PDF");
                            subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                            subactionOne.SetAttributeValue("commandvalue", name);
                            subactionOne.SetAttributeValue("image", "place.png");
                            subactionOne.SetAttributeValue("concernedmember", "");
                            subactionOne.SetAttributeValue("enabled", "true");
                            subactions.Add(subactionOne);
                        }

                        SrNo2Count = SrNo2Count + 1;
                        SrNo3Count = 1;
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PageBreak"]) == "True")
                        {
                            page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);

                            page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));
                            // businessNo = 1;
                        }
                        else
                        {
                            page1.Add(business);

                        }

                    }
                    else if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == false)
                    {
                        outXml = "";
                        SrNo3 = "(" + ToRoman(Convert.ToInt16(Convert.ToString(SrNo3Count))) + ") ";
                        outXml += @"<p style='text-align:justify;padding-left: 50px;font-family:DVOT-Yogesh;font-size:22px'><span >  
                                    " + SrNo3 + @"";
                        //string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string LOBTexts3 = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string FinalTexts3 = LOBTexts3.Replace("<i>", "<span>&nbsp;");
                        string LOBText = FinalTexts3.Replace("</i>", "</span>");
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        outXml += LOBText + "</span></p>";
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) == "")
                        {
                            outXml += "<br/>";
                        }

                        XElement contentOne = new XElement("content");
                        //contentOne.SetAttributeValue("id", Convert.ToString(SrNo3Count));
                        //contentOne.SetAttributeValue("level", "3");
                        //contentOne.SetAttributeValue("Set", SrNo1 + "." + SrNo2 + "." + SrNo3);
                        //   contentOne.SetAttributeValue("value", LOBText);
                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    contentOne.SetAttributeValue("action", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //}
                        contentOne.ReplaceNodes(new XCData(outXml));

                        business.SetAttributeValue("id", MainEventId + "." + businessNo);


                        business.SetAttributeValue("MainEventId", MainEventId);
                        business.SetAttributeValue("ConcernedEventId", ConcernedEventId);
                        var methodParameter5 = new List<KeyValuePair<string, string>>();
                        methodParameter5.Add(new KeyValuePair<string, string>("@XmlEventId", MainEventId + "." + businessNo.ToString()));
                        methodParameter5.Add(new KeyValuePair<string, string>("@Id", dataset.Tables[0].Rows[count]["Id"].ToString()));
                        methodParameter5.Add(new KeyValuePair<string, string>("@LobType", Form));
                        ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_UpdateXmlEventId]", methodParameter5);


                        string EventId = Convert.ToString(dataset.Tables[0].Rows[count]["ConcernedEventId"]);
                        DataSet dataset2 = null;
                        var methodParameter4 = new List<KeyValuePair<string, string>>();
                        methodParameter4.Add(new KeyValuePair<string, string>("@Id", EventId));
                        dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetPaperTypeByEventId]", methodParameter4);
                        if (dataset2 != null)
                        {
                            if (!DBNull.Value.Equals(dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"]))
                            {
                                string papertypeid = dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"].ToString();
                                business.SetAttributeValue("PaperCatTypeId", papertypeid);
                            }
                        }
                        string SNo1 = dataset.Tables[0].Rows[count]["SrNo1"].ToString();
                        string SNo2 = dataset.Tables[0].Rows[count]["SrNo2"].ToString();
                        string SNo3 = dataset.Tables[0].Rows[count]["SrNo3"].ToString();
                        business.SetAttributeValue("SerialNumber", SNo1 + "." + SNo2 + "." + SNo3);
                        string pdflocation = dataset.Tables[0].Rows[count]["PDFLocation"].ToString();
                        if (pdflocation != "" && pdflocation != null)
                        {
                            if (Form == "Draft")
                            {
                                var methodParameter6 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["Id"].ToString();
                                methodParameter6.Add(new KeyValuePair<string, string>("@Id", draftid));

                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter6);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                            else
                            {

                                var methodParameter6 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["DraftRecordId"].ToString();
                                methodParameter6.Add(new KeyValuePair<string, string>("@Id", draftid));
                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter6);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                        }
                        if (EventId == "3" || EventId == "4")
                        {
                            business.SetAttributeValue("Question", "True");
                        }
                        else
                        {
                            business.SetAttributeValue("Question", "False");
                        }

                        business.Add(contentOne);
                        businessNo++;


                        ///For Start and Stop Event Actions
                        XElement subactions = new XElement("actions");
                        business.Add(subactions);
                        XElement subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STARTEVENT");
                        subactionOne.SetAttributeValue("command", "START_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "start.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STOPEVENT");
                        subactionOne.SetAttributeValue("command", "STOP_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "stop.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        ///for the action tab for pdf link
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        {
                            string name = Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]);
                            int index1 = name.LastIndexOf('/');
                            name = name.Substring(index1 + 1);

                            subactionOne = new XElement("action");
                            subactionOne.SetAttributeValue("id", "ATTACHED_PDF");
                            subactionOne.SetAttributeValue("command", "OPEN_PDF");
                            subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                            subactionOne.SetAttributeValue("commandvalue", name);
                            subactionOne.SetAttributeValue("image", "place.png");
                            subactionOne.SetAttributeValue("concernedmember", "");
                            subactionOne.SetAttributeValue("enabled", "true");
                            subactions.Add(subactionOne);
                        }

                        SrNo3Count = SrNo3Count + 1;
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PageBreak"]) == "True")
                        {
                            page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);
                            page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));
                            //    businessNo = 1;
                        }
                        else
                        {
                            page1.Add(business);

                        }
                    }




                    if (count == dataset.Tables[0].Rows.Count - 1)
                    {

                        //businessNo = 1;
                        //business = new XElement("business");
                        //page1.Add(business);
                        //pages.Add(page1);

                        //page1.SetAttributeValue("id", idCount);
                        //page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));
                        //// cont1.ReplaceNodes(new XCData(outXml));
                        //// page1.Add(business);
                        //pages.Add(page1);
                        //business = new XElement("business");

                        pages.Add(page1);
                        ///Last page
                        page1 = new XElement("page");
                        //content = new XElement("content");
                        business = new XElement("business");
                        page1.SetAttributeValue("id", idCount + 1);
                        page1.SetAttributeValue("page-des", "last-page");
                        //content.ReplaceNodes(new XCData("<p style='font-size:22px;'>The End</P>"));
                        //business.Add(content);
                        page1.Add(business);
                        pages.Add(page1);

                    }
                }
                //indexContent.SetValue(sb);
                indexContent.ReplaceNodes(new XCData("<h5><b>Today's list of business:</b></h5><ol>" + sb + "<ol>"));
                index.Add(indexContent);

                index.Add(indexOne);

                ///Save XML File
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(LOB.ToString());


                methodParameter = new List<KeyValuePair<string, string>>();

                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string CurrentSession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        CurrentSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";


                sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                sessiondate = sessiondate.Replace('/', ' ');
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                if (Form == "Draft")
                {
                    fileName = "DraftLOBFile.xml";
                    string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + sessiondate + "/";
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = System.IO.Path.Combine(directory, fileName);
                    //  file.SaveAs(path);
                    xml.Save(path);
                    return path;
                }
                else
                {
                    fileName = "LOBFile.xml";
                    string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + sessiondate + "/";
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = System.IO.Path.Combine(directory, fileName);
                    //  file.SaveAs(path);
                    xml.Save(path);
                    return path;
                    //xml.Save("C:/eVidhan/LOB/LOBFile.xml");
                }

#pragma warning disable CS0162 // Unreachable code detected
                return "";
#pragma warning restore CS0162 // Unreachable code detected
            }
            return "";
        }


        public string CreateQuesXML(string QuestionType, string sessionDate)
        {
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", sessionDate));
            methodParameter.Add(new KeyValuePair<string, string>("@QuesType", QuestionType));

            DataSet dataset = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectQuestionBySessionDate]", methodParameter);

            if (dataset != null)
            {

                XElement LOB = new System.Xml.Linq.XElement("LOB");
                XElement pages = new System.Xml.Linq.XElement("pages");
                LOB.Add(pages);
                //XElement header = new XElement("header-page");
                ////header.Attribute("id").Value="header_page";
                //header.SetAttributeValue("id", "header_page");
                //pages.Add(header);
                //XElement content = new XElement("content");
                //header.Add(content);

                //                content.ReplaceNodes(new XCData(@"<h2 style='text-align: center;font-family:DVOT-Yogesh;'>" + Convert.ToString(dataset.Tables[0].Rows[0]["AssemblyNameLocal"]) + @"</h2>
                //                    <h4 style='text-align: center;font-family:DVOT-Yogesh;'>????? - ????</h4>
                //                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionNameLocal"]) + @"  </h4>
                //                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionDateLocal"]) + @"  " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionTimeLocal"]) + @" ??? ????</h4>"));




                //page
                int idCount = 2;
#pragma warning disable CS0219 // The variable 'SrNo1' is assigned but its value is never used
                string SrNo1 = "";
#pragma warning restore CS0219 // The variable 'SrNo1' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo2' is assigned but its value is never used
                string SrNo2 = "";
#pragma warning restore CS0219 // The variable 'SrNo2' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo3' is assigned but its value is never used
                string SrNo3 = "";
#pragma warning restore CS0219 // The variable 'SrNo3' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo1Count' is assigned but its value is never used
                int SrNo1Count = 1;
#pragma warning restore CS0219 // The variable 'SrNo1Count' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo2Count' is assigned but its value is never used
                int SrNo2Count = 1;
#pragma warning restore CS0219 // The variable 'SrNo2Count' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo3Count' is assigned but its value is never used
                int SrNo3Count = 1;
#pragma warning restore CS0219 // The variable 'SrNo3Count' is assigned but its value is never used


#pragma warning disable CS0219 // The variable 'outXml' is assigned but its value is never used
                string outXml = "";
#pragma warning restore CS0219 // The variable 'outXml' is assigned but its value is never used
                DateTime SessionDate = new DateTime();
                if (dataset.Tables[0].Rows.Count > 0)
                {
                    SessionDate = Convert.ToDateTime(dataset.Tables[0].Rows[0]["IsFixedDate"]);
                }
                string date = SessionDate.ToString("dd/MM/yyyy");
                date = date.Replace("/", "-");
                XElement page1 = new XElement("page");
                int IndexCount = 0;
                XElement index = new XElement("index-page");
                index.SetAttributeValue("id", "index_page");
                index.SetAttributeValue("Date", date);

                pages.Add(index);
                XElement indexContent = new XElement("content");
                XElement indexOne = new XElement("index");

                StringBuilder sb = new StringBuilder();



                //if (dataset.Tables[0].Rows.Count > 0)
                //{
                //    string sessiondate = null;
                //}
                //else
                //{
                //    string sessiondate = sessionDate;  
                //}

                for (int count = 0; count < dataset.Tables[0].Rows.Count; count++)
                {


                    sessionDate = Convert.ToString(dataset.Tables[0].Rows[0]["IsFixedDate"]);

                    string IsHindi = "False";

                    if (Convert.ToString(dataset.Tables[0].Rows[count]["IsHindi"]) == "True")
                    {
                        IsHindi = "True";

                    }
                    else
                    {
                        IsHindi = "False";
                    }


                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@DeptId", Convert.ToString(dataset.Tables[0].Rows[count]["DepartmentID"])));
                    methodParameter.Add(new KeyValuePair<string, string>("@MinistryId", Convert.ToString(dataset.Tables[0].Rows[count]["MinistryId"])));
                    DataSet datasetDept = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDepartmentInfoById]", methodParameter);
                    string DepartmentName = "";
                    string DepartmentNameLocal = "";
                    if (datasetDept != null)
                    {
                        if (datasetDept.Tables[0].Rows[0] != null)
                        {
                            DepartmentName = Convert.ToString(datasetDept.Tables[0].Rows[0]["deptname"]).Trim();
                            DepartmentNameLocal = Convert.ToString(datasetDept.Tables[0].Rows[0]["deptnameLocal"]).Trim();
                        }
                    }


                    string QuesNo = Convert.ToString(dataset.Tables[0].Rows[count]["QuestionNumber"]);
                    string Subject = Convert.ToString(dataset.Tables[0].Rows[count]["Subject"]);

                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@QuestionType", Convert.ToString(dataset.Tables[0].Rows[0]["QuestionType"])));
                    DataSet datasetQuestion = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectQuestionTypeById]", methodParameter);
                    string QuesType = "";
                    if (datasetQuestion != null)
                    {
                        if (datasetQuestion.Tables[0].Rows[0] != null)
                        {
                            QuesType = Convert.ToString(datasetQuestion.Tables[0].Rows[0]["QuestionTypeName"]);
                        }
                    }

                    string memberName = "";
                    string memberNameLocal = "";
                    string memberCode = "";
                    if (Convert.ToString(dataset.Tables[0].Rows[count]["IsClubbed"]) != "")
                    {
                        if (Convert.ToBoolean(dataset.Tables[0].Rows[count]["IsClubbed"]))
                        {
                            memberCode = dataset.Tables[0].Rows[count]["ReferenceMemberCode"].ToString();
                            string[] members = Convert.ToString(dataset.Tables[0].Rows[count]["ReferenceMemberCode"]).Split(',');
                            members = members.Distinct().ToArray();
                            for (int j = 0; j < members.Count(); j++)
                            {
                                if (j == members.Count() - 1)
                                {
                                    methodParameter = new List<KeyValuePair<string, string>>();
                                    methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", members[j]));
                                    DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);

                                    if (datasetMember != null)
                                    {
                                        if (datasetMember.Tables[0].Rows[0] != null)
                                        {
                                            string Prefix = Convert.ToString(datasetMember.Tables[0].Rows[0]["Prefix"]).Trim();
                                            if (Prefix.Contains("Sh."))
                                            {
                                                Prefix = Prefix.Replace("Sh.", "Shri");
                                            }
                                            memberName = memberName + Prefix + " " + Convert.ToString(datasetMember.Tables[0].Rows[0]["Name"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ")";
                                            memberNameLocal = memberNameLocal + Convert.ToString(datasetMember.Tables[0].Rows[0]["NameLocal"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName_Local"]) + ")";
                                        }
                                    }
                                }
                                else
                                {
                                    methodParameter = new List<KeyValuePair<string, string>>();
                                    methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", members[j]));
                                    DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);

                                    if (datasetMember != null)
                                    {
                                        if (datasetMember.Tables[0].Rows[0] != null)
                                        {
                                            string Prefix = Convert.ToString(datasetMember.Tables[0].Rows[0]["Prefix"]).Trim();
                                            if (Prefix.Contains("Sh."))
                                            {
                                                Prefix = Prefix.Replace("Sh.", "Shri");
                                            }
                                            memberName = memberName + Prefix + " " + Convert.ToString(datasetMember.Tables[0].Rows[0]["Name"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ") ,";
                                            memberNameLocal = memberNameLocal + Convert.ToString(datasetMember.Tables[0].Rows[0]["NameLocal"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName_Local"]) + ") ,";
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            methodParameter = new List<KeyValuePair<string, string>>();
                            methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", Convert.ToString(dataset.Tables[0].Rows[count]["MemberID"])));
                            DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);

                            if (datasetMember != null)
                            {
                                if (datasetMember.Tables[0].Rows[0] != null)
                                {
                                    string Prefix = Convert.ToString(datasetMember.Tables[0].Rows[0]["Prefix"]).Trim();
                                    if (Prefix.Contains("Sh."))
                                    {
                                        Prefix = Prefix.Replace("Sh.", "Shri");
                                    }
                                    memberCode = datasetMember.Tables[0].Rows[0]["MemberCode"].ToString();
                                    memberName = Prefix + " " + Convert.ToString(datasetMember.Tables[0].Rows[0]["Name"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ")";
                                    memberNameLocal = Convert.ToString(datasetMember.Tables[0].Rows[0]["NameLocal"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName_Local"]) + ")";
                                }
                            }
                        }
                    }
                    else
                    {

                        methodParameter = new List<KeyValuePair<string, string>>();
                        methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", Convert.ToString(dataset.Tables[0].Rows[count]["MemberID"])));
                        DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);

                        if (datasetMember != null)
                        {
                            if (datasetMember.Tables[0].Rows[0] != null)
                            {
                                string Prefix = Convert.ToString(datasetMember.Tables[0].Rows[0]["Prefix"]).Trim();
                                if (Prefix.Contains("Sh."))
                                {
                                    Prefix = Prefix.Replace("Sh.", "Shri");
                                }
                                memberCode = datasetMember.Tables[0].Rows[0]["MemberCode"].ToString();
                                memberName = Prefix + " " + Convert.ToString(datasetMember.Tables[0].Rows[0]["Name"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ")";
                                memberNameLocal = Convert.ToString(datasetMember.Tables[0].Rows[0]["NameLocal"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName_Local"]) + ")";
                            }
                        }

                    }

                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@MinistryID", Convert.ToString(dataset.Tables[0].Rows[count]["MinistryId"])));
                    DataSet datasetMember1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMinistryInfoByMinistryID]", methodParameter);
                    string MinisterName = "";
                    string MinisterNameLocal = "";
                    string MinistersName = "";

                    mMinistry Model = new mMinistry();
                    Model.MinistryID = Convert.ToInt16(dataset.Tables[0].Rows[count]["MinistryId"]);
                    List<mMinsitryMinister> result = (List<mMinsitryMinister>)Helper.ExecuteService("MinistryMinister", "GetMinisterInfoByMinistryId", Model);
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (i == 0)
                        {
                            MinistersName = result[0].MinisterName;
                            methodParameter = new List<KeyValuePair<string, string>>();
                            methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", Convert.ToString(result[0].MemberCode)));
                            DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);
                            if (datasetMember != null)
                            {
                                if (datasetMember.Tables[0].Rows[0] != null)
                                {
                                    MinistersName = MinistersName + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ")";
                                }
                            }
                        }
                        else
                        {
                            MinistersName = MinistersName + ", " + result[i].MinisterName;
                            methodParameter = new List<KeyValuePair<string, string>>();
                            methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", Convert.ToString(result[0].MemberCode)));
                            DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);
                            if (datasetMember != null)
                            {
                                if (datasetMember.Tables[0].Rows[0] != null)
                                {
                                    MinistersName = MinistersName + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ")";
                                }
                            }
                        }
                    }
                    if (datasetMember1 != null)
                    {
                        if (datasetMember1.Tables[0].Rows[0] != null)
                        {
                            MinisterName = Convert.ToString(datasetMember1.Tables[0].Rows[0]["MinistryName"]);
                            MinisterNameLocal = Convert.ToString(datasetMember1.Tables[0].Rows[0]["MinistryNameLocal"]);
                        }
                    }


                    if (count == 0)
                    {
                        page1.SetAttributeValue("id", 2);
                        page1.SetAttributeValue("page-des", "first-page");
                        page1.SetAttributeValue("Date", date);

                    }
                    else
                    {
                        pages.Add(page1);
                        idCount++;
                        page1 = new XElement("page");
                        page1.SetAttributeValue("id", idCount);
                        page1.SetAttributeValue("Date", date);
                    }

                    IndexCount = IndexCount + 1;
                    if (IndexCount == 11)
                    {
                        index.Add(indexOne);
                        IndexCount = 0;
                        index = new XElement("index-page");
                        index.SetAttributeValue("id", "index_page");
                        index.SetAttributeValue("Date", date);

                        pages.Add(index);
                        indexContent = new XElement("content");
                        indexOne = new XElement("index");
                    }
                    ///For Indexing
                    ///
                    if (IsHindi == "True")
                    {
                        // Subject = Subject;
                    }
                    else
                    {
                        Subject = Subject.ToUpper();
                    }

                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, "&lt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&amp;&zwd;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                    memberNameLocal = memberNameLocal.Replace("\r\n", "");

                    string ind = QuesNo + "(" + Subject + ")";
                    sb.Append("<li>" + ind + "</li>");
                    XElement subIndex1 = new XElement("index");
                    subIndex1.SetAttributeValue("id", idCount);
                    ind = StripHTML(ind);
                    ind = System.Text.RegularExpressions.Regex.Replace(ind, "\r\r", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    subIndex1.SetAttributeValue("description", ind);
                    subIndex1.SetAttributeValue("MemberCodes", memberCode.Trim());
                    subIndex1.SetAttributeValue("Member", memberName.ToUpper());
                    subIndex1.SetAttributeValue("MemberLocal", memberNameLocal);
                    subIndex1.SetAttributeValue("MinisterConcerned", MinistersName.ToUpper());
                    subIndex1.SetAttributeValue("IsHindi", IsHindi);

                    subIndex1.SetAttributeValue("command", "test_command");
                    subIndex1.SetAttributeValue("enabled", "true");
                    subIndex1.ReplaceNodes(new XCData(@"<p style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'>" + ind + "</P>"));
                    indexOne.Add(subIndex1);


                    XElement contentOne = new XElement("content");
                    contentOne.SetAttributeValue("Department", DepartmentName);
                    contentOne.SetAttributeValue("DepartmentLocal", DepartmentNameLocal);
                    contentOne.SetAttributeValue("QNo", QuesNo);
                    contentOne.SetAttributeValue("Type", QuesType);
                    contentOne.SetAttributeValue("LayingDate", date);

                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"<p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"</p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, "&amp;", "$M", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&nbsp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"\r\n", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&amp;&zwd;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&amp;&zwj;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"nbsp;", "T", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"$MT", "&nbsp;", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"$M", "&amp;", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"T", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&T", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                    contentOne.SetAttributeValue("Subject", Subject);

                    contentOne.SetAttributeValue("MemberCodes", memberCode.Trim());
                    contentOne.SetAttributeValue("Member", memberName.ToUpper());
                    contentOne.SetAttributeValue("MemberLocal", memberNameLocal);
                    contentOne.SetAttributeValue("Minister", MinisterName.ToUpper());
                    contentOne.SetAttributeValue("MinisterLocal", MinisterNameLocal);
                    contentOne.SetAttributeValue("MinisterConcerned", MinistersName.ToUpper());
                    contentOne.SetAttributeValue("IsHindi", IsHindi);
                    page1.Add(contentOne);


                    String Questions = Convert.ToString(dataset.Tables[0].Rows[count]["MainQuestion"]);

                    ////  remove unwanted tag from main Question
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<( )*tr([^>])*>", "<p>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<( )*p([^>])*>", "<p>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<( )*span([^>])*>", "<span>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<span>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"</span>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p>&lt;br/&gt;</p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p>&nbsp;</p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p><br/></p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p>", "<p style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'><span style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"</p>", "</span></p>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&nbsp;", " &nbsp; ", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&zwj;", string.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"zwj;", string.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&#xD;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&#xA;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&#xD;&#xA;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&lt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"p&gt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&amp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&amp;&zwd;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&amp;&zwj;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"nbsp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"lt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"/p&gt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&nbsp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    // Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"  nbsp", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"nbsp", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'><span style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'>nbsp</span></p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'><span style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'>  nbsp</span></p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'><span style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'></span></p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"#xD;&#xA;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"xD;&#xA;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&#", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    // if (Questions != "")
                    //{
                    //    Questions = Questions.Replace("<p>", "");
                    //    Questions = Questions.Replace("</p>", "");
                    //}

                    //string[] subqestion = Regex.Split(Questions, "^^");
                    string[] subqestion = Questions.Split(new string[] { "^^" }, StringSplitOptions.None);
                    for (int j = 0; j < subqestion.Count(); j++)
                    {
                        if (j == 0)
                        {
                            XElement Question = new XElement("Question");
                            var conten = subqestion[j];
                            if (subqestion.Count() > 1)
                            {
                                conten = conten + "</p>";
                            }
                            Question.ReplaceNodes(new XCData(conten));
                            page1.Add(Question);
                        }
                        else
                        {
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            page1.SetAttributeValue("id", idCount);
                            page1.SetAttributeValue("Date", date);
                            var conten = subqestion[j];
                            if (j == subqestion.Count() - 1)
                            {
                                conten = "<p>" + conten;
                            }
                            else
                            {
                                conten = "<p>" + conten + "</p>";
                            }

                            XElement Question = new XElement("Question");
                            Question.ReplaceNodes(new XCData(conten));
                            page1.Add(Question);

                        }
                    }



                    //methodParameter = new List<KeyValuePair<string, string>>();
                    //methodParameter.Add(new KeyValuePair<string, string>("@QuestionNo", Convert.ToString(dataset.Tables[0].Rows[count]["QuestionNumber"])));
                    //DataSet datasetSubQuestions = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSubQuestionByQuestionNo]", methodParameter);


                    //for (int i = 0; i < datasetSubQuestions.Tables[0].Rows.Count; i++)
                    //{
                    //    XElement Question = new XElement("Question");
                    //    Question.ReplaceNodes(new XCData(Convert.ToString(datasetSubQuestions.Tables[0].Rows[i]["SubQuestion"])));
                    //    page1.Add(Question);
                    //    XElement Answer = new XElement("Answer");
                    //    Answer.ReplaceNodes(new XCData(Convert.ToString(datasetSubQuestions.Tables[0].Rows[i]["SubAnswer"])));
                    //    page1.Add(Answer);
                    //    ////for page break
                    //    //if (Convert.ToString(dataset.Tables[0].Rows[count]["PageBreak"]) == "True")
                    //    //{
                    //    //    //page1.Add(business);
                    //    //    pages.Add(page1);
                    //    //    idCount++;
                    //    //    page1 = new XElement("page");
                    //    //    //business = new XElement("business");
                    //    //    page1.SetAttributeValue("id", idCount);
                    //    //}

                    //}

                    //if (dataset.Tables[0].Rows[count]["AnswerAttachLocation"] != null && Convert.ToString(dataset.Tables[0].Rows[count]["AnswerAttachLocation"]) != "")
                    //{
                    string name = Convert.ToString(dataset.Tables[0].Rows[count]["AnswerAttachLocation"]);
                    int index1 = name.LastIndexOf('/');
                    name = name.Substring(index1 + 1);
                    XElement subactions = new XElement("actions");
                    page1.Add(subactions);
                    XElement subactionOne = new XElement("action");
                    subactionOne.SetAttributeValue("id", "PDF");
                    if (QuestionType == "2")
                    {
                        subactionOne.SetAttributeValue("command", "OPEN_PDF_UQ");
                    }
                    else if (QuestionType == "1")
                    {
                        subactionOne.SetAttributeValue("command", "OPEN_PDF_QA");
                    }
                    subactionOne.SetAttributeValue("commandvalue", QuesNo + ".pdf");
                    subactionOne.SetAttributeValue("image", "QAndA.png");
                    subactionOne.SetAttributeValue("enabled", "true");
                    subactions.Add(subactionOne);
                    //}

                    //string  LOBText = "";
                    //  XElement contentOne = new XElement("content");
                    //  contentOne.SetAttributeValue("id", SrNo1);
                    //  contentOne.SetAttributeValue("level", "1");
                    //  contentOne.SetAttributeValue("Set", SrNo1);
                    //  contentOne.SetAttributeValue("value", LOBText);
                    //  page1.Add(contentOne);

                    //  XElement subactions = new XElement("actions");
                    //  page1.Add(subactions);
                    //  XElement subactionOne = new XElement("action");
                    //  subactionOne.SetAttributeValue("id", "PDF");
                    //  subactionOne.SetAttributeValue("command", "OPEN_PDF");
                    //  subactionOne.SetAttributeValue("commandvalue", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                    //  subactionOne.SetAttributeValue("image", "place.png");
                    //  subactionOne.SetAttributeValue("enabled", "true");
                    //subactions.Add(subactionOne);

                    //if (count == dataset.Tables[0].Rows.Count - 1)
                    //{
                    //    pages.Add(page1);
                    //}

                    if (count == dataset.Tables[0].Rows.Count - 1)
                    {


                        //pages.Add(page1);
                        ///Last page
                        ///
                        pages.Add(page1);
                        idCount++;
                        page1 = new XElement("page");
                        page1.SetAttributeValue("id", idCount);
                        page1.SetAttributeValue("Date", date);
                        XElement content = new XElement("content");
                        content.SetAttributeValue("Department", "");
                        content.SetAttributeValue("DepartmentLocal", "");
                        content.SetAttributeValue("QNo", "");
                        content.SetAttributeValue("Type", "");
                        content.SetAttributeValue("LayingDate", "");

                        content.SetAttributeValue("Subject", "");


                        content.SetAttributeValue("Member", "");
                        content.SetAttributeValue("MemberLocal", "");
                        content.SetAttributeValue("Minister", "");
                        content.SetAttributeValue("MinisterLocal", "");
                        content.SetAttributeValue("MinisterConcerned", "");
                        content.SetAttributeValue("IsHindi", "False");


                        XElement Question = new XElement("Question");

                        page1.Add(content);
                        page1.Add(Question);
                        pages.Add(page1);

                    }

                    //idCount++;
                    //page1 = new XElement("page");
                    //// business = new XElement("business");
                    //page1.SetAttributeValue("id", idCount);


                }
                indexContent.ReplaceNodes(new XCData("<h5><b>Today's list of Questions:</b></h5><ol>" + sb + "<ol>"));
                index.Add(indexContent);
                index.Add(indexOne);

                ///Save XML File
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(LOB.ToString());



                methodParameter = new List<KeyValuePair<string, string>>();

                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string CurrentSession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        CurrentSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }
                string fileName = "";

                //sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                date = date.Replace('-', ' ');


                if (QuestionType == "1")
                {
                    fileName = "QuestingSFile.xml";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + date + "/";
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    // string directory = Server.MapPath(url);
                    if (!Directory.Exists(directory))
                    {

                        Directory.CreateDirectory(directory);
                    }

                    string path = System.IO.Path.Combine(directory, fileName);
                    //  file.SaveAs(path);
                    xml.Save(path);
                    return path;
                }
                else
                {
                    fileName = "QuestingUSFile.xml";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + date + "/";
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);

                    // string directory = Server.MapPath(url);
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = System.IO.Path.Combine(directory, fileName);
                    //  file.SaveAs(path);
                    xml.Save(path);
                    return path;
                    //xml.Save("C:/eVidhan/LOB/LOBFile.xml");
                }


                //if (QuestionType == "1")
                //{
                //    xml.Save("C:/eVidhan/LOB/QuestingSFile.xml");
                //}
                //else if (QuestionType == "2")
                //{
                //    xml.Save("C:/eVidhan/LOB/QuestingUnFile.xml");
                //}
#pragma warning disable CS0162 // Unreachable code detected
                return "";
#pragma warning restore CS0162 // Unreachable code detected
            }
            return "";
        }

        #endregion

        #region "LOB Preview"

        public string CreateLOBPreview(string LOBId, string Resolution)
        {
            var methodParameter = new List<KeyValuePair<string, string>>();

            DataSet dataset = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);

            if (dataset != null)
            {

                XElement LOB = new System.Xml.Linq.XElement("LOB");
                XElement pages = new System.Xml.Linq.XElement("pages");
                LOB.Add(pages);
                XElement header = new XElement("header-page");
                //header.Attribute("id").Value="header_page";
                header.SetAttributeValue("id", "header_page");
                pages.Add(header);
                XElement content = new XElement("content");
                header.Add(content);



                DateTime SessionDate = Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]);
                string date = ConvertSQLDate(SessionDate);
                date = date.Replace("/", "");
                date = date.Replace("-", "");
                string LOBName = date + "_" + "LOB" + "_" + Convert.ToString(dataset.Tables[0].Rows[0]["LOBId"]);

                content.ReplaceNodes(new XCData(@"<h2 style='text-align: center;font-family:DVOT-Yogesh;'>" + Convert.ToString(dataset.Tables[0].Rows[0]["AssemblyNameLocal"]) + @"</h2>
                    <h4 style='text-align: center;font-family:DVOT-Yogesh;'>कार्य - सूची</h4>
                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionNameLocal"]) + @"  </h4>
                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionDateLocal"]) + @"  " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionTimeLocal"]) + @" बजे शुरू</h4>"));




                //page
                int idCount = 2;
                string SrNo1 = "";
                string SrNo2 = "";
                string SrNo3 = "";
                int SrNo1Count = 1;
                int SrNo2Count = 1;
                int SrNo3Count = 1;


                string outXml = "";

                //XElement page = new XElement("page");
                //XElement cont = new XElement("content");

                XElement page1 = new XElement("page");
                // XElement cont1 = new XElement("content");
                //XElement business = new XElement("business");
                //  page1.Add(business);
                //Index
                XElement index = new XElement("index-page");
                index.SetAttributeValue("id", "index_page");
                pages.Add(index);
                XElement indexContent = new XElement("content");
                StringBuilder sb = new StringBuilder();

                XElement indexOne = new XElement("index");


                for (int count = 0; count < dataset.Tables[0].Rows.Count; count++)
                {


                    if (count == 0)
                    {
                        page1.SetAttributeValue("id", 2);
                        page1.SetAttributeValue("page-des", "first-page");



                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    XElement subactions = new XElement("actions");
                        //    page1.Add(subactions);
                        //    XElement subactionOne = new XElement("action");
                        //    subactionOne.SetAttributeValue("id", "PDF");
                        //    subactionOne.SetAttributeValue("command", "OPEN_PDF");
                        //    subactionOne.SetAttributeValue("commandvalue", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //    subactionOne.SetAttributeValue("image", "place.png");
                        //    subactionOne.SetAttributeValue("enabled", "true");
                        //    subactions.Add(subactionOne);
                        //}


                    }
                    else
                    {
                        if (Convert.ToInt32(dataset.Tables[0].Rows[count - 1]["SrNo1"]) != Convert.ToInt32(dataset.Tables[0].Rows[count]["SrNo1"]))
                        {
                            // cont1.ReplaceNodes(new XCData(outXml));
                            //    page1.Add(business);
                            //   pages.Add(page1);

                            //     outXml = "";
                        }

                    }




                    if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == true && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == true)
                    {

                        //  if (count != 0)
                        //  {
                        ///For Index
                        string LOBText1 = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        if (LOBText1 != "")
                        {
                            LOBText1 = LOBText1.Replace("<p>", "");
                            LOBText1 = LOBText1.Replace("</p>", "");
                        }
                        sb.Append("<li>" + LOBText1 + "</li>");
                        XElement subIndex1 = new XElement("index");
                        subIndex1.SetAttributeValue("id", idCount);
                        LOBText1 = StripHTML(LOBText1);
                        subIndex1.SetAttributeValue("description", LOBText1);
                        subIndex1.SetAttributeValue("command", "test_command");
                        subIndex1.SetAttributeValue("enabled", "true");
                        indexOne.Add(subIndex1);

                        // }


                        /////For first page Index
                        //string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        //if (LOBText != "")
                        //{
                        //    LOBText = LOBText.Replace("<p>", "");
                        //    LOBText = LOBText.Replace("</p>", "");
                        //}

                        //sb.Append("<li>" + LOBText + "</li>");
                        //XElement subIndex = new XElement("index");
                        //subIndex.SetAttributeValue("id", 1);

                        //LOBText = StripHTML(LOBText);
                        //subIndex.SetAttributeValue("description", LOBText);
                        //subIndex.SetAttributeValue("command", "test_command");
                        //subIndex.SetAttributeValue("enabled", "true");
                        //indexOne.Add(subIndex);


                        /// For contant
                        outXml = "";
                        SrNo1 = Convert.ToString(SrNo1Count) + ". ";
                        outXml += @"
                                    
                                    " + SrNo1 + @"";
                        string LOBText = SrNo1 + Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        outXml += LOBText + "";



                        XElement contentOne = new XElement("content");
                        //contentOne.SetAttributeValue("id", SrNo1);
                        //contentOne.SetAttributeValue("level", "1");
                        //contentOne.SetAttributeValue("Set", SrNo1);
                        //  contentOne.SetAttributeValue("value", LOBText);




                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    contentOne.SetAttributeValue("action", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //}
                        contentOne.ReplaceNodes(new XCData(LOBText));

                        page1.Add(contentOne);
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        {
                            XElement subactions = new XElement("actions");
                            page1.Add(subactions);
                            XElement subactionOne = new XElement("action");
                            subactionOne.SetAttributeValue("id", "PDF");
                            subactionOne.SetAttributeValue("command", "OPEN_PDF");
                            subactionOne.SetAttributeValue("commandvalue", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                            subactionOne.SetAttributeValue("image", "place.png");
                            subactionOne.SetAttributeValue("enabled", "true");
                            subactions.Add(subactionOne);
                        }

                        SrNo1Count = SrNo1Count + 1;
                        SrNo2Count = 1;

                        //for page break
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PageBreak"]) == "True")
                        {
                            //page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            //business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);
                        }

                    }
                    else if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == true)
                    {
                        outXml = "";
                        SrNo2 = "(" + Convert.ToString(SrNo2Count) + "). ";
                        outXml += @" <p style='line-height:4; font-family:DVOT-Yogesh;text-align:left;'><span>
                                    " + SrNo2 + @" ";
                        string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        outXml += LOBText + "</span></p>";

                        XElement contentOne = new XElement("content");
                        //contentOne.SetAttributeValue("id", Convert.ToString(SrNo2Count));
                        //contentOne.SetAttributeValue("level", "2");
                        //contentOne.SetAttributeValue("Set", SrNo1 + "." + SrNo2);
                        //  contentOne.SetAttributeValue("value", LOBText);
                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    contentOne.SetAttributeValue("action", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //}
                        contentOne.ReplaceNodes(new XCData(outXml));

                        page1.Add(contentOne);
                        ///for the action tab for pdf link
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        {
                            XElement subactions = new XElement("actions");
                            page1.Add(subactions);
                            XElement subactionOne = new XElement("action");
                            subactionOne.SetAttributeValue("id", "PDF");
                            subactionOne.SetAttributeValue("command", "OPEN_PDF");
                            subactionOne.SetAttributeValue("commandvalue", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                            subactionOne.SetAttributeValue("image", "place.png");
                            subactionOne.SetAttributeValue("enabled", "true");
                            subactions.Add(subactionOne);
                        }

                        SrNo2Count = SrNo2Count + 1;
                        SrNo3Count = 1;
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PageBreak"]) == "True")
                        {
                            //page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            // business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);
                        }

                    }
                    else if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == false)
                    {
                        outXml = "";
                        SrNo3 = ToRoman(Convert.ToInt16(Convert.ToString(SrNo3Count))) + ". ";
                        outXml += @"<p style='text-align: left;padding-left: 50px;font-family:DVOT-Yogesh;'><span >  
                                    " + SrNo3 + @"";
                        string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        outXml += LOBText + "</span></p>";

                        XElement contentOne = new XElement("content");
                        //contentOne.SetAttributeValue("id", Convert.ToString(SrNo3Count));
                        //contentOne.SetAttributeValue("level", "3");
                        //contentOne.SetAttributeValue("Set", SrNo1 + "." + SrNo2 + "." + SrNo3);
                        //   contentOne.SetAttributeValue("value", LOBText);
                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    contentOne.SetAttributeValue("action", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //}
                        contentOne.ReplaceNodes(new XCData(outXml));

                        page1.Add(contentOne);
                        ///for the action tab for pdf link
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        {
                            XElement subactions = new XElement("actions");
                            page1.Add(subactions);
                            XElement subactionOne = new XElement("action");
                            subactionOne.SetAttributeValue("id", "PDF");
                            subactionOne.SetAttributeValue("command", "OPEN_PDF");
                            subactionOne.SetAttributeValue("commandvalue", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                            subactionOne.SetAttributeValue("image", "place.png");
                            subactionOne.SetAttributeValue("enabled", "true");
                            subactions.Add(subactionOne);
                        }

                        SrNo3Count = SrNo3Count + 1;
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PageBreak"]) == "True")
                        {
                            // page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            //   business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);
                        }
                    }




                    if (count == dataset.Tables[0].Rows.Count - 1)
                    {


                        // cont1.ReplaceNodes(new XCData(outXml));
                        // page1.Add(business);
                        pages.Add(page1);


                        ///Last page
                        page1 = new XElement("page");
                        content = new XElement("content");
                        page1.SetAttributeValue("id", idCount + 1);
                        page1.SetAttributeValue("page-des", "last-page");
                        content.ReplaceNodes(new XCData(""));
                        page1.Add(content);
                        pages.Add(page1);

                    }
                }
                //indexContent.SetValue(sb);
                indexContent.ReplaceNodes(new XCData("<h5><b>Today's list of business:</b></h5><ol>" + sb + "<ol>"));
                index.Add(indexContent);

                index.Add(indexOne);

                ///Save XML File
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(LOB.ToString());
                xml.Save("C:/e_Vidhan/LOB/LOBFile.xml");
                return "";


                #region For CreatePDF


#pragma warning disable CS0162 // Unreachable code detected
                MemoryStream output = new MemoryStream();
#pragma warning restore CS0162 // Unreachable code detected
                // convert HTML to PDF

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                //document1.LicenseKey = "B4mYiJubiJiInIaYiJuZhpmahpGRkZE=";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(10, 10, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(10, 10, 0, 0), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }

                #endregion
            }
            return "";
        }



        public ActionResult GeneratePdfLOBPreview(string LOBId, string Resolution)
        {
#pragma warning disable CS0219 // The variable 'LOBName' is assigned but its value is never used
            string LOBName = "20Dec2013_1_LOB";
#pragma warning restore CS0219 // The variable 'LOBName' is assigned but its value is never used
            string CurrentSecretery = "";
#pragma warning disable CS0219 // The variable 'SessionDate' is assigned but its value is never used
            DateTime SessionDate = new DateTime();
#pragma warning restore CS0219 // The variable 'SessionDate' is assigned but its value is never used
            string savedPDF = string.Empty;

            MemoryStream output = new MemoryStream();
            if (LOBId != null && LOBId != "")
            {
                LOBModel objLOBModel = new LOBModel();
                DataSet SummaryDataSet = new DataSet();
                string outXml = "";
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);


                //Getting Current Secretary
                DataSet SiteSettingDataSet = new DataSet();
                var methodParameter1 = new List<KeyValuePair<string, string>>();
                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
                {
                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
                    if (dtSiteSetting != null)
                    {
                        IEnumerable<DataRow> siteSettingQuery =
                                from siteSetting in dtSiteSetting.AsEnumerable()
                                select siteSetting;
                        IEnumerable<DataRow> curtSecretery =
                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
                        }
                    }
                }

                // convert HTML to PDF
                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                //document1.LicenseKey = "B4mYiJubiJiInIaYiJuZhpmahpGRkZE=";
                document1.CompressionLevel = PdfCompressionLevel.Best;

                methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@ResolutionId", Resolution));
                DataSet SummaryDataResolution = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectResolutionById]", methodParameter);


                float Height = float.Parse(Convert.ToString(SummaryDataResolution.Tables[0].Rows[0]["PageSizeHeight"]), CultureInfo.InvariantCulture.NumberFormat);
                float Width = float.Parse(Convert.ToString(SummaryDataResolution.Tables[0].Rows[0]["PageSizeWidth"]), CultureInfo.InvariantCulture.NumberFormat);

                float HeightEle = Height + 100;
                float WidthEle = Width - 20;


                PdfPageSize abc = new PdfPageSize(Width, Height);
                document1.Margins = new Margins(10, 10, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(abc, new Margins(10, 10, 0, 0));
                page.Orientation = PdfPageOrientation.Portrait;


                page.PageSize.Height = Height;
                page.PageSize.Width = Width;
                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = "";
                string baseURL = "";

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {

                    outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    string SrNo1 = "";
                    string SrNo2 = "";
                    string SrNo3 = "";
                    int SrNo1Count = 1;
                    int SrNo2Count = 1;
                    int SrNo3Count = 1;


                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {




                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                      <br/>  
                                    " + SrNo1 + @"";
                            SrNo1Count = SrNo1Count + 1;
                            SrNo2Count = 1;

                            string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                            if (LOBText != "")
                            {
                                LOBText = LOBText.Replace("<p>", "");
                                LOBText = LOBText.Replace("</p>", "");
                            }


                            outXml += @"" + LOBText + "<br/>";


                            outXml += @"<br/> </td></tr>";

                            if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["PageBreak"]) == "True")
                            {
                                htmlStringToConvert = outXml;
                                baseURL = "";
                                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL, Convert.ToInt16(WidthEle), Convert.ToInt16(HeightEle));
                                htmlToPdfElement.DownloadAllResources = true;
                                //htmlToPdfElement.Width = Height;
                                page.AddElement(htmlToPdfElement);

                                page = document1.Pages.AddNewPage(abc, new Margins(10, 10, 0, 0));

                                page.Orientation = PdfPageOrientation.Portrait;
                                page.PageSize.Height = Height;
                                page.PageSize.Width = Width;
                                outXml = "";
                            }


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                    " + SrNo2 + @"";
                            SrNo2Count = SrNo2Count + 1;
                            SrNo3Count = 1;

                            string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                            if (LOBText != "")
                            {
                                LOBText = LOBText.Replace("<p>", "");
                                LOBText = LOBText.Replace("</p>", "");
                            }


                            outXml += @"" + LOBText + "<br/>";


                            outXml += @"<br/> </td></tr>";

                            if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["PageBreak"]) == "True")
                            {
                                htmlStringToConvert = outXml;
                                baseURL = "";
                                EvoPdf.Document docu = new EvoPdf.Document();
                                docu.AddPage(page);
                                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL, Convert.ToInt16(WidthEle), Convert.ToInt16(HeightEle));
                                htmlToPdfElement.DownloadAllResources = true;
                                page.AddElement(htmlToPdfElement);

                                page = document1.Pages.AddNewPage(abc, new Margins(10, 10, 0, 0));
                                page.Orientation = PdfPageOrientation.Portrait;
                                page.PageSize.Height = Height;
                                page.PageSize.Width = Width;
                                outXml = "";
                            }
                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
                        {

                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                    " + SrNo3 + @"";
                            SrNo3Count = SrNo3Count + 1;


                            string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                            if (LOBText != "")
                            {
                                LOBText = LOBText.Replace("<p>", "");
                                LOBText = LOBText.Replace("</p>", "");
                            }


                            outXml += @"" + LOBText + "<br/>";


                            outXml += @"<br/> </td></tr>";

                            if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["PageBreak"]) == "True")
                            {
                                htmlStringToConvert = outXml;
                                baseURL = "";
                                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL, Convert.ToInt16(WidthEle), Convert.ToInt16(HeightEle));

                                htmlToPdfElement.DownloadAllResources = true;
                                page.AddElement(htmlToPdfElement);

                                page = document1.Pages.AddNewPage(abc, new Margins(10, 10, 0, 0));
                                page.Orientation = PdfPageOrientation.Portrait;
                                page.PageSize.Height = Height;
                                page.PageSize.Width = Width;
                                outXml = "";
                            }
                        }




                    }




                    outXml += "</table></div></div> </body> </html>";

                }


                htmlStringToConvert = outXml;
                baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL, Convert.ToInt16(Width), Convert.ToInt16(Height));
                htmlToPdfElement.DownloadAllResources = true;
                page.AddElement(htmlToPdfElement);


                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }


                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msInput, null);
                //document.Close();

                //byte[] file = ms.ToArray();
                //output.Write(file, 0, file.Length);
                //output.Position = 0;


                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_" + LOBId + ".pdf");

                string url = "/LOB1/" + LOBId;
                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                string fileName = "Draft_" + LOBId + ".pdf";
                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

            }

            return File(output, "application/pdf");
        }

        #endregion
    }
}
