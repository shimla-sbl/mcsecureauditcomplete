﻿using SBL.DomainModel.Models.Role;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserAction;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions
{
    public static class Extensions
    {
        public static List<mUserModules> ToModelList(this List<mUserType> entityList)
        {
            var ModelList = entityList.Select(entity => new mUserModules()
            {
                UserTypeName = entity.UserTypeName,
                UserTypeID = entity.UserTypeID
            });
            return ModelList.ToList();
        }

        public static List<mUserSubModules> ToSubAccessList(this List<mUserModules> entityList)
        {
            var ModelList = entityList.Select(entity => new mUserSubModules()
            {
                ModuleName = entity.ModuleName,
                ModuleId = entity.ModuleId
            });
            return ModelList.ToList();
        }

        public static List<mSubUserType> ToSubUserTypeList(this List<mUserType> entityList)
        {
            var ModelList = entityList.Select(entity => new mSubUserType()
            {
                UserTypeName = entity.UserTypeName,
                UserTypeID = entity.UserTypeID
            });
            return ModelList.ToList();
        }
       
        public static List<mRoles> TomroleList(this List<mRoles> entityList)
        {
            var ModelList = entityList.Select(entity => new mRoles()
            {
                RoleName = entity.RoleName,
                RoleId = entity.RoleId
            });
            return ModelList.ToList();
        }
        public static List<mUserSubModules> ToViewUserSubModel(this IEnumerable<mUserSubModules> model)
        {

            var ViewModel = model.Select(part => new mUserSubModules()
            {
                ModuleId = part.ModuleId,
                SubModuleName = part.SubModuleName,
                SubModuleId = part.SubModuleId,
                SubModuleDescription = part.SubModuleDescription,
                ModifiedWhen = part.ModifiedWhen,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                ModuleName=part.ModuleName,
            });
            return ViewModel.ToList();
        }

        public static mUserSubModules ToSubModuleModel(this mUserSubModules model)
        {
            return new mUserSubModules
            {
                ModuleId = model.ModuleId,
                ModuleName = model.ModuleName,
                SubModuleId = model.SubModuleId,
                SubModuleName = model.SubModuleName,
                SubModuleNameLocal = model.SubModuleNameLocal,
                SubModuleDescription = model.SubModuleDescription,
                Isactive = model.Isactive,
                CreatedBy = CurrentSession.UserName,
                SubModuleOrder = model.SubModuleOrder,

            };
        }

        public static mSubUserType ToSubUserTypeModel(this mSubUserType model)
        {
            return new mSubUserType
            {
                UserTypeID = model.UserTypeID,
                UserTypeName = model.UserTypeName,
                SubUserTypeID = model.SubUserTypeID,
                SubUserTypeName = model.SubUserTypeName,
                SubUserTypeNameLocal = model.SubUserTypeNameLocal,
                SubUserTypeDescription = model.SubUserTypeDescription,
                ModifiedWhen = model.ModifiedWhen,
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,

            };
        }

        public static mUserSubModules ToViewSubModuleModel(this mUserSubModules model, string Mode)
        {
            return new mUserSubModules
            {
                ModuleId = model.ModuleId,
                SubModuleDescription = model.SubModuleDescription,
                SubModuleName = model.SubModuleName,
                ModuleName = model.ModuleName,
                Isactive = model.Isactive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }

        public static List<mUserActions> ToUserActionList(this List<mUserType> entityList)
        {
            var ModelList = entityList.Select(entity => new mUserActions()
            {
                UserTypeName = entity.UserTypeName,
                UserTypeID = entity.UserTypeID
            });
            return ModelList.ToList();
        }

        public static List<tUserAccessActions> ToUserAccessActionList(this List<mUserType> entityList)
        {
            var ModelList = entityList.Select(entity => new tUserAccessActions()
            {
                UserTypeName = entity.UserTypeName,
                UserTypeID = entity.UserTypeID
            });
            return ModelList.ToList();
        }


        public static List<tUserAccessActions> ToSubUserTypeListForAccessAction(this List<mSubUserType> entityList)
        {
            var ModelList = entityList.Select(entity => new tUserAccessActions()
            {
                SubUserTypeName = entity.SubUserTypeName,
                SubUserTypeID = entity.SubUserTypeID
            });
            return ModelList.ToList();
        }


        public static List<mUserModules> ToSubUserTypeListForAccess(this List<mSubUserType> entityList)
        {
            var ModelList = entityList.Select(entity => new mUserModules()
            {
                SubUserTypeName = entity.SubUserTypeName,
                SubUserTypeID = entity.SubUserTypeID
            });
            return ModelList.ToList();
        }

        public static List<tUserAccessActions> ToAccessList(this List<mUserModules> entityList)
        {
            var ModelList = entityList.Select(entity => new tUserAccessActions()
            {
                ModuleName = entity.ModuleName,
                ModuleId = entity.ModuleId
            });
            return ModelList.ToList();
        }
        public static List<tUserAccessActions> ToSubAccessList(this List<mUserSubModules> entityList)
        {
            var ModelList = entityList.Select(entity => new tUserAccessActions()
            {
                SubModuleName = entity.SubModuleName,
                SubModuleId = entity.SubModuleId
            });
            return ModelList.ToList();
        }
        public static List<tUserAccessActions> ToActionList(this List<mUserActions> entityList)
        {
            var ModelList = entityList.Select(entity => new tUserAccessActions()
            {
                ActionName = entity.ActionName,
                ActionId = entity.ActionId
            });
            return ModelList.ToList();
        }
        
        public static List<mRoles> ToViewUserRole(this IEnumerable<mRoles> model)
        {

            var ViewModel = model.Select(part => new mRoles()
            {
                RoleId = part.RoleId,
                RoleName = part.RoleName,
                RoleNameLocal =part.RoleNameLocal,
                RoleDescription = part.RoleDescription,
                ModifiedDate = part.ModifiedDate,
                CreatedBy = CurrentSession.UserName,

            });
            return ViewModel.ToList();
        }

        public static List<mUserActions> ToViewUserAction(this IEnumerable<mUserActions> model)
        {

            var ViewModel = model.Select(part => new mUserActions()
            {
                ActionId = part.ActionId,
                ActionName = part.ActionName,
                ActionDescription = part.ActionDescription,
                ModifiedWhen = part.ModifiedWhen,
                CreatedBy = CurrentSession.UserName,

            });
            return ViewModel.ToList();
        }


        public static List<mUserModules> ToViewUserModel(this IEnumerable<mUserModules> model)
        {

            var ViewModel = model.Select(part => new mUserModules()
            {
                ModuleId = part.ModuleId,
                ModuleName = part.ModuleName,
                ModuleDescription = part.ModuleDescription,
                ModifiedWhen = part.ModifiedWhen,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }

        public static mRoles ToDomainModel(this mRoles model)
        {
            return new mRoles
            {

                RoleId = model.RoleId,

                RoleName = model.RoleName,
                RoleNameLocal = model.RoleNameLocal,
                RoleDescription = model.RoleDescription,
                ModifiedDate = model.ModifiedDate,
                Isactive = model.Isactive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,

            };
        }

        public static mUserModules ToModuleModel(this mUserModules model)
        {
            return new mUserModules
            {
                ModuleId = model.ModuleId,
                ModuleName = model.ModuleName,
                SubUserTypeID = model.SubUserTypeID,
                SubUserTypeName = model.SubUserTypeName,
                ModuleNameLocal = model.ModuleNameLocal,
                ModuleDescription = model.ModuleDescription,
                Isactive = model.Isactive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                ModifiedWhen = DateTime.Now,
                ModuleOrder=model.ModuleOrder
            };
        }

        public static mRoles ToViewModel1(this mRoles model, string Mode)
        {
            return new mRoles
            {
                RoleId = model.RoleId,
                RoleDescription = model.RoleDescription,
                RoleName = model.RoleName,
                Isactive = model.Isactive,
                CreatedBy = CurrentSession.UserName,
                RoleNameLocal = model.RoleNameLocal,
                //ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }

        public static mUserModules ToViewModuleModel(this mUserModules model, string Mode)
        {
            return new mUserModules
            {
                ModuleId = model.ModuleId,
                SubUserTypeID = model.SubUserTypeID,
                SubUserTypeName = model.SubUserTypeName,
                ModuleDescription = model.ModuleDescription,
                ModuleName = model.ModuleName,
                ModuleNameLocal = model.ModuleNameLocal,
                Isactive = model.Isactive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode,
                ModuleOrder=model.ModuleOrder

            };
        }

        public static List<mUserType> ToViewUserType(this IEnumerable<mUserType> model)
        {

            var ViewModel = model.Select(part => new mUserType()
            {
                UserTypeID = part.UserTypeID,
                UserTypeName = part.UserTypeName,
                ModifiedWhen = DateTime.Now,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }

        public static mUserType ToUserTypeModel(this mUserType model)
        {
            return new mUserType
            {
                UserTypeID = model.UserTypeID,
                UserTypeName = model.UserTypeName,
                IsActive = model.IsActive,
                ModifiedBy = CurrentSession.UserName,

            };
        }

        public static mUserType ToViewUserTypeModel(this mUserType model, string Mode)
        {
            return new mUserType
            {
                UserTypeID = model.UserTypeID,
                UserTypeName = model.UserTypeName,
                IsActive = model.IsActive,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }

        public static mUserActions ToSaveAction(this mUserActions model)
        {
            return new mUserActions
            {
                ActionId = model.ActionId,
                ActionName = model.ActionName,
                ActionDescription = model.ActionDescription,
               ActionNameLocal = model.ActionNameLocal,
                Isactive = model.Isactive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,

            };
        }

        public static tUserAccessActions ToSaveUserAccessAction(this tUserAccessActions model)
        {
            return new tUserAccessActions
            {
                UserTypeID =  model.UserTypeID,
                SubUserTypeID = model.SubUserTypeID,
                SubUserTypeName = model.SubUserTypeName,
                ModuleId =model.ModuleId,
                ActionId = model.ActionId,
                ActionName = model.ActionName,
                UserTypeName = model.UserTypeName,
                ModifiedWhen = model.ModifiedWhen,
               UserAccessActionsId = model.UserAccessActionsId,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                AccessActionOrder=model.AccessActionOrder

            };
        }

        public static mUserActions ToViewActionModel(this mUserActions model, string Mode)
        {
            return new mUserActions
            {
                ActionId = model.ActionId,
                ActionName = model.ActionName,
                ActionDescription = model.ActionDescription,
               ActionNameLocal = model.ActionNameLocal,
                Isactive = model.Isactive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }

        public static tUserAccessActionsModel ToViewUserAccessActionModel(this tUserAccessActionsModel model, string Mode)
        {
            return new tUserAccessActionsModel
            {
                UserAccessActionsId = model.UserAccessActionsId,
                ActionId = model.ActionId,
                SubUserTypeID = model.SubUserTypeID,
                SubUserTypeName = model.SubUserTypeName,
                ActionName = model.ActionName,
                UserTypeID = model.UserTypeID,
                UserTypeName = model.UserTypeName,
                ModuleId = model.ModuleId,
                ModuleName = model.ModuleName,
                MergeSubModuleId = model.MergeSubModuleId,//by ashwani
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode,
                AccessActionOrder = model.AccessActionOrder

            };
        }


        //public static IEnumerable<MinistryDepartmentViewModel> ToViewModel(this IEnumerable<mMinistryDepartment> model)
        //{

        //    var ViewModel = model.Select(ministryDept => new MinistryDepartmentViewModel()
        //    {
        //        AssemblyID = ministryDept.AssemblyID,
        //        MinistryID = ministryDept.MinistryID,
        //        MinistryDepartmentsID = ministryDept.MinistryDepartmentsID,
        //        DeptID = ministryDept.DeptID,
        //        IsActive = ministryDept.IsActive ?? false,
        //        GetAssemblyName = ministryDept.GetAssemblyName,
        //        GetDeptName = ministryDept.GetDeptName,
        //        GetMinistryName = ministryDept.GetMinistryName,
        //        OrderID = ministryDept.OrderID

        //    });
        //    return ViewModel;
        //}

        public static List<tUserAccessActions> ToViewUserAccessAction(this IEnumerable<tUserAccessActions> model)
        {

            var ViewModel = model.Select(part => new tUserAccessActions()
            {
                UserTypeID = part.UserTypeID,
                ModuleId = part.ModuleId,
                ActionId = part.ActionId,
                UserTypeName = part.UserTypeName,
                ModuleName = part.ModuleName,
                ActionName=part.ActionName,
                UserAccessActionsId = part.UserAccessActionsId,
                ModifiedWhen = DateTime.Now,
                CreatedBy = CurrentSession.UserName,

            });
            return ViewModel.ToList();
        }


        public static tUserAccessRequestModel ToAcceptReject(this tUserAccessRequestModel model)
        {
            return new tUserAccessRequestModel
            {
                ID = model.ID,
                UserID = model.UserID,
                AccessID = model.AccessID,
               
                ModifiedBy = CurrentSession.UserName,

            };
        }
    }
}