﻿using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;
using System.Net;

namespace SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Controllers
{
    public class AcceptRejectUserController : Controller
    {
        //
        // GET: /UserManagement/AcceptRejectUser/

        public ActionResult Index()
        {
           
          
            return View();
        }

        public ActionResult UserManagement()
        {
            return View();
        }

        public ActionResult LeftNavigationMenu()
        {
            return PartialView("_LeftNavigationMenu");
        }
        public ActionResult AcceptRejectUser()
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            List<mUsers> ListSiteSettingsVS = new List<mUsers>();
            tUserAccessRequest objmodel = new tUserAccessRequest();
            mUsers model1 = new mUsers();

            model.mUsersList = (ICollection<mUsers>)Helper.ExecuteService("Module", "GetUsers", model1);

            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetUSerAccessRequestrGrid", objmodel);
           
            return View(model);
        }



        public ActionResult UpdateAcceptUser(string SIds)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            
            var dist = model.ToAcceptReject();

            Helper.ExecuteService("Module", "UpdateAcceptRequest", SIds);
               
               return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
           
        }

        public ActionResult UpdateRejectUser(string SIds)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();

            var dist = model.ToAcceptReject();

            Helper.ExecuteService("Module", "UpdateRejectRequest", SIds);

            return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });

        }
        
    }
}
