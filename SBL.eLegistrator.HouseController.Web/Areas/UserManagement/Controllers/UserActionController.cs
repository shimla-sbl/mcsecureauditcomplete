﻿using SBL.DomainModel.Models.UserAction;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;
using SBL.DomainModel.Models.User;

namespace SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Controllers
{
    public class UserActionController : Controller
    {
        //
        // GET: /UserManagement/UserAction/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetUserAction()
        {
            var Modules = (List<mUserActions>)Helper.ExecuteService("Module", "GetUserAction", null);
            var model1 = Modules.ToViewUserAction();

            return PartialView("_GetUserAction", model1);
        }

        public ActionResult CreateNewUserAction()
        {
            mUserType model = new mUserType();
            var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
            var returnedResultNew = returnedResult.ToUserActionList();


            var model1 = new mUserActions()
            {
                Mode = "Add",
                objList = returnedResultNew
            };

            return PartialView("_CreateNewAction", model1);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveAction(mUserActions model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToSaveAction();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "SaveUserAction", dist);
                }
                else
                {
                    //Helper.ExecuteService("District", "UpdateDistrict", dist);
                }

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }
            else
            {

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateAction(mUserActions model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToSaveAction();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "SaveUserAction", dist);
                }
                else
                {
                    Helper.ExecuteService("Module", "UpdateEntryAction", dist);
                }

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }
            else
            {

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }

        }


        public ActionResult EditUserAction(int ActionId)
        {

            mUserActions ActionToEdit = (mUserActions)Helper.ExecuteService("Module", "GetActionDataById", new mUserActions { ActionId = ActionId });

            var model = ActionToEdit.ToViewActionModel("Edit");
            model.ActionId = ActionId;
            model.ActionName = model.ActionName;
            return View("_UpdateUserAction", model);
        }

        //public ActionResult EditUserAction(int ActionId)
        //{

        //    mUserType model = new mUserType();
        //  //  var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
        //   // var returnedResultNew = returnedResult.ToUserActionList();


        //    var model1 = new mUserActions()
        //    {

        //        objList = returnedResultNew
        //    };

        //    mUserActions ActionToEdit = (mUserActions)Helper.ExecuteService("Module", "GetActionDataById", new mUserActions { ActionId = ActionId });

        //    var model2 = ActionToEdit.ToViewActionModel("Edit");
        //    model1.ActionId = ActionId;
        //    model1.ActionName = model2.ActionName;
          
        //    model1.ActionDescription = model2.ActionDescription;
        //    model1.Isactive = model2.Isactive;
        //    return View("_UpdateUserAction", model1);
        //}

        public JsonResult UpdateActionEntry(value model)
        {
            mUserActions objModel = new mUserActions();
            objModel.ActionId = model.ActionId;
            objModel.ActionName = model.ActionName;
            objModel.ActionDescription = model.ActionDescription;
          
            objModel.Isactive = model.Isactive;
            objModel = Helper.ExecuteService("Module", "UpdateEntryAction", objModel) as mUserActions;
            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public class value
        {
            [AllowHtml]
            public string ActionName { get; set; }
            [AllowHtml]
            public string ActionDescription { get; set; }
            public int ActionId { get; set; }
            public bool Isactive { get; set; }
            public int UserTypeID { get; set; }
            public string UserTypeName { get; set; }

        }

        public JsonResult DeleteAction(int ActionId)
        {
            mUserActions ActionToDelete = (mUserActions)Helper.ExecuteService("Module", "GetActionDataById", new mUserActions { ActionId = ActionId });
            Helper.ExecuteService("Module", "DeleteAction", ActionToDelete);
            return Json("Update.Message", JsonRequestBehavior.AllowGet);

        }

        public JsonResult CheckUserAction(string UserActionName)
        {
            try
            {
                var mdl = (bool)Helper.ExecuteService("Module", "CheckUserActionExist", UserActionName);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }

    }
}
