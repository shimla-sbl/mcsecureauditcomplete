﻿using SBL.DomainModel.Models.UserAction;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;

namespace SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Controllers
{
    public class UserAccessActionController : Controller
    {
        //
        // GET: /UserManagement/UserAccessAction/

        public ActionResult Index()
        {
            return View();
        }

       

        public ActionResult GetUserAccessAction()
        {
            tUserAccessActions model = new tUserAccessActions();

            tUserAccessActionsModel objModel = new tUserAccessActionsModel();

            objModel = (tUserAccessActionsModel)Helper.ExecuteService("Module", "GetUserAccessAction", objModel);
            
            return PartialView("_GetUserAccessAction", objModel);
        }

        public ActionResult CreateNewUserAccessAction()
        {
            // For User Type
            mUserType model = new mUserType();
            var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
            var returnedResultNew = returnedResult.ToUserAccessActionList();


            //For User Access
            mUserModules Accessmodel = new mUserModules();
            var returnedAccessmodel = Helper.ExecuteService("Module", "GetUserAccessDropDown", model) as List<mUserModules>;
            var returnedAccessmodelNew = returnedAccessmodel.ToAccessList();


            //For User Action

            mUserActions Actionmodel = new mUserActions();
            var returnedActionmodel = Helper.ExecuteService("Module", "GetUserActionDropDown", model) as List<mUserActions>;
            var returnedActionmodelNew = returnedActionmodel.ToActionList();

            var model1 = new tUserAccessActions()
            {
                Mode = "Add",
                objList = returnedResultNew,
                AccessList = returnedAccessmodelNew,
                ActionList = returnedActionmodelNew,

            };


            return PartialView("_CreateNewUserAccessAction", model1);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveUserAccessAction(tUserAccessActions model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToSaveUserAccessAction();
                if (model.Mode == "Add")
                {

                    foreach (var item in model.ActionIds)
                    {

                        string ResultString = string.Join(",", model.ActionIds.ToArray());
                        dist.ActionComma = ResultString;
                    }

                    Helper.ExecuteService("Module", "SaveUserAccessAction", dist);
                }
                else
                {
                    //Helper.ExecuteService("District", "UpdateDistrict", dist);
                }

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }
            else
            {

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }

        }



        public ActionResult GetEditUserAccsessAction(int UserAccessActionsId)
        {

            // For User Type
            mUserType model = new mUserType();
            var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
            var returnedResultNew = returnedResult.ToUserAccessActionList();


            //For User Access
            mUserModules Accessmodel = new mUserModules();
            var returnedAccessmodel = Helper.ExecuteService("Module", "GetUserAccessDropDown", model) as List<mUserModules>;
            var returnedAccessmodelNew = returnedAccessmodel.ToAccessList();


            //For User Action

            mUserActions Actionmodel = new mUserActions();
            var returnedActionmodel = Helper.ExecuteService("Module", "GetUserActionDropDown", model) as List<mUserActions>;
            var returnedActionmodelNew = returnedActionmodel.ToActionList();

            var model1 = new tUserAccessActionsModel()
            {
                //Mode = "Add",
                objList = returnedResultNew,
                AccessList = returnedAccessmodelNew,
                ActionList = returnedActionmodelNew
            };

            tUserAccessActionsModel UserAccessActionToEdit = (tUserAccessActionsModel)Helper.ExecuteService("Module", "GetUserAccessActionDataById", new tUserAccessActionsModel { UserAccessActionsId = UserAccessActionsId });

            var model2 = UserAccessActionToEdit.ToViewUserAccessActionModel("Edit");
            model1.UserTypeID = model2.UserTypeID;
            model1.UserTypeName = model2.UserTypeName;
            model1.ActionId = model2.ActionId;
            model1.ActionName = model2.ActionName;
            model1.ModuleId = model2.ModuleId;
            model1.ModuleName = model2.ModuleName;
            model1.MergeActionId = UserAccessActionToEdit.MergeActionId;
            string s = model1.MergeActionId;
            if (s != null && s != "")
            {
                string[] values = s.Split(',');
                model1.ActionIds = new List<string>();
                for (int i = 0; i < values.Length; i++)
                {
                    model1.ActionIds.Add(values[i]);
                }

            }
            return View("_UpdateNewUserAccessAction", model1);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUserAccessAction(tUserAccessActions model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToSaveUserAccessAction();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "UpdateUserAccessAction", dist);
                }
                else
                {

                    foreach (var item in model.ActionIds)
                    {

                        string ResultString = string.Join(",", model.ActionIds.ToArray());
                        dist.ActionComma = ResultString;
                    }

                    Helper.ExecuteService("Module", "UpdateUserAccessAction", dist);
                }

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }
            else
            {

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }

        }


        public JsonResult DeleteUserAccessAction(int UserAccessActionsId)
        {
            tUserAccessActions obj = new tUserAccessActions();
            tUserAccessActionsModel UserAccessActionToDelete = (tUserAccessActionsModel)Helper.ExecuteService("Module", "GetUserAccessActionDataById", new tUserAccessActionsModel { UserAccessActionsId = UserAccessActionsId });

            //tUserAccessActions UserAccessActionToDelete = (tUserAccessActions)Helper.ExecuteService("Module", "GetUserAccessActionDataById", new tUserAccessActions { UserAccessActionsId = UserAccessActionsId });
            obj.UserAccessActionsId = UserAccessActionToDelete.UserAccessActionsId;

            Helper.ExecuteService("Module", "DeleteUserAccessAction", obj);
            return Json("Update.Message", JsonRequestBehavior.AllowGet);

        }

        public JsonResult CheckUserAccessAction(int UserType, int UserAccess, String UserAction)
        {
            tUserAccessActions model = new tUserAccessActions();

            model.UserTypeID = UserType;
            model.ModuleId = UserAccess;
            model.MergeActionId = UserAction;

            try
            {
                var mdl = (bool)Helper.ExecuteService("Module", "CheckUserAccessActionExist", model);
                //var mdl1 = (bool)Helper.ExecuteService("Module", "CheckUserAccessIdExist", UserAccess);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }

#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }

        public JsonResult CheckUserAccessIdExist(int UserAccess)
        {
            try
            {
                var mdl = (bool)Helper.ExecuteService("Module", "CheckUserAccessIdExist", UserAccess);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }

    }
}
