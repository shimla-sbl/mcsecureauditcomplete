﻿using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.UserAction;
using SBL.DomainModel.Models.UserModule;
using SBL.DomainModel.Models.Secretory;
using SBL.DomainModel.ComplexModel;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.HOD;

namespace SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Controllers
{
    public class UserRequestController : Controller
    {
        //
        // GET: /UserManagement/UserRequest/
        List<tUserAccessActions> actionlist = new List<tUserAccessActions>();
        public ActionResult Index()
        {
            try
            {
                mUsers user = new mUsers();
                if (!string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    user.UserId = new Guid(CurrentSession.UserID);
                }
                else
                {
                    return RedirectToAction("Index", "Home", new { @area = "" });
                }

                user.UserName = CurrentSession.UserName;

                tUserAccessActions obj = new tUserAccessActions();
                user = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", user);
                CurrentSession.SubUserTypeID = user.UserType.ToString();
                obj.SubUserTypeID =Convert.ToInt32( user.UserType);
                if (user.IsSecretoryId == true && user.UserType == 2)
                {
                    mSecretory secretory = new mSecretory();
                    Int32 secid = Convert.ToInt32(user.SecretoryId);
                    secretory.SecretoryID = secid;
                    secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretaryByEmpID", secretory);
                    user.SecretoryName = secretory.SecretoryName;
                    user.SecretoryId = secretory.SecretoryID;
                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {
                        if (itm.SubUserTypeName == "Administrative Secretary (Head)")
                        {
                            obj.SubUserTypeID = itm.SubUserTypeID;
                            user.usertypeid = itm.SubUserTypeID;
                            user.usertypename = itm.SubUserTypeName;
                        }
                        if (itm.SubUserTypeName == "Web Administrator")
                        {
                            obj.UserTypeID = itm.SubUserTypeID;
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = itm.SubUserTypeName;
                            //user.SecretoryDepartmentModel
                        }

                    }

                }
                else if (user.IsSecretoryId == true && user.UserType == 4)
                {
                    mSecretory secretory = new mSecretory();
                    Int32 secid = Convert.ToInt32(user.SecretoryId);
                    secretory.SecretoryID = secid;
                    secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretaryByEmpID", secretory);
                    user.SecretoryName = secretory.SecretoryName;
                    user.SecretoryId = secretory.SecretoryID;
                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {
                        if (itm.SubUserTypeName == "Vidhan Shabha Secretary")
                        {
                            obj.SubUserTypeID = itm.SubUserTypeID;
                            user.usertypeid = itm.SubUserTypeID;
                            user.usertypename = itm.SubUserTypeName;
                        }
                        if (itm.SubUserTypeName == "Web Administrator")
                        {
                            obj.UserTypeID = itm.SubUserTypeID;
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = itm.SubUserTypeName;
                            //user.SecretoryDepartmentModel
                        }

                    }

                }
                else if (user.IsHOD == true && user.UserType == 15)
                {
                    mHOD hods = new mHOD();
                    Int32 secid = Convert.ToInt32(user.SecretoryId);
                    hods.HODID = secid;
                    hods = (mHOD)Helper.ExecuteService("HOD", "GetHODById", hods);

                    user.SecretoryName = hods.HODName;
                    user.SecretoryId = hods.HODID;
                    user.IsHOD = true;
                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {
                        if (itm.SubUserTypeName == "Head Of Department")
                        {
                            obj.SubUserTypeID = itm.SubUserTypeID;
                            user.usertypeid = itm.SubUserTypeID;
                            user.usertypename = itm.SubUserTypeName;
                        }
                        if (itm.SubUserTypeName == "Web Administrator")
                        {
                            obj.UserTypeID = itm.SubUserTypeID;
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = itm.SubUserTypeName;
                            //user.SecretoryDepartmentModel
                        }

                    }

                }
                else if (user.IsMember.ToUpper() == "TRUE" && user.UserType == 17)
                {

                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {
                        if (itm.SubUserTypeName == "Member Only")
                        {
                            obj.SubUserTypeID = itm.SubUserTypeID;
                            user.usertypeid = itm.SubUserTypeID;
                            user.usertypename = itm.SubUserTypeName;
                        }
                        if (itm.SubUserTypeName == "Web Administrator")
                        {
                            obj.UserTypeID = itm.SubUserTypeID;
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = itm.SubUserTypeName;
                            //user.SecretoryDepartmentModel
                        }

                    }

                }
                else if (user.IsMember.ToUpper() == "TRUE" && user.UserType == 18)
                {

                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {
                        if (itm.SubUserTypeName == "Speaker")
                        {
                            obj.SubUserTypeID = itm.SubUserTypeID;
                            user.usertypeid = itm.SubUserTypeID;
                            user.usertypename = itm.SubUserTypeName;
                        }
                        if (itm.SubUserTypeName == "Web Administrator")
                        {
                            obj.UserTypeID = itm.SubUserTypeID;
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = itm.SubUserTypeName;
                            //user.SecretoryDepartmentModel
                        }

                    }

                }
                else if (user.IsMember.ToUpper() == "TRUE" && user.UserType == 19)
                {

                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {
                        if (itm.SubUserTypeName == "Minister")
                        {
                            obj.SubUserTypeID = itm.SubUserTypeID;
                            user.usertypeid = itm.SubUserTypeID;
                            user.usertypename = itm.SubUserTypeName;
                        }
                        if (itm.SubUserTypeName == "Web Administrator")
                        {
                            obj.UserTypeID = itm.SubUserTypeID;
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = itm.SubUserTypeName;
                            //user.SecretoryDepartmentModel
                        }

                    }

                }
                else if (user.IsMember.ToUpper() == "TRUE" && user.UserType == 20)
                {

                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {
                        if (itm.SubUserTypeName == "Chief Parliamentary Secretary")
                        {
                            obj.SubUserTypeID = itm.SubUserTypeID;
                            user.usertypeid = itm.SubUserTypeID;
                            user.usertypename = itm.SubUserTypeName;
                        }
                        if (itm.SubUserTypeName == "Web Administrator")
                        {
                            obj.UserTypeID = itm.SubUserTypeID;
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = itm.SubUserTypeName;
                            //user.SecretoryDepartmentModel
                        }

                    }

                }
                else if (user.IsMember.ToUpper() == "TRUE" && user.UserType == 21)
                {

                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {
                        if (itm.SubUserTypeName == "Committee Chairperson")
                        {
                            obj.SubUserTypeID = itm.SubUserTypeID;
                            user.usertypeid = itm.SubUserTypeID;
                            user.usertypename = itm.SubUserTypeName;
                        }
                        if (itm.SubUserTypeName == "Web Administrator")
                        {
                            obj.UserTypeID = itm.SubUserTypeID;
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = itm.SubUserTypeName;
                            //user.SecretoryDepartmentModel
                        }

                    }

                }
                else if (user.IsMember.ToUpper() == "TRUE" && user.UserType == 36)
                {

                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {
                        if (itm.SubUserTypeName == "Dupty Speaker")
                        {
                            obj.SubUserTypeID = itm.SubUserTypeID;
                            user.usertypeid = itm.SubUserTypeID;
                            user.usertypename = itm.SubUserTypeName;
                        }
                        if (itm.SubUserTypeName == "Web Administrator")
                        {
                            obj.UserTypeID = itm.SubUserTypeID;
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = itm.SubUserTypeName;
                            //user.SecretoryDepartmentModel
                        }

                    }

                }

                else if (user.UserType == 3)
                {
                    mSecretory secretory = new mSecretory();
                    secretory.SecretoryID = Convert.ToInt32(user.SecretoryId);
                    secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretaryByEmpID", secretory);
                    user.SecretoryName = secretory.SecretoryName;
                    user.SecretoryId = secretory.SecretoryID;
                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {

                        if (itm.SubUserTypeName == "Administrative Secretary (Head)")
                        {
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = user.SecretoryName + "(Administrative Secretary)";
                            obj.UserTypeID = itm.UserTypeID;
                            obj.SubUserTypeID = Convert.ToInt16(user.UserType);
                            user.usertypename = user.Name;
                            user.IsSecretoryId = true;
                        }

                    }

                }
                else if (user.UserType == 5)
                {
                    mSecretory secretory = new mSecretory();
                    secretory.SecretoryID = Convert.ToInt32(user.SecretoryId);
                    secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretaryByEmpID", secretory);
                    if (secretory != null)
                    {
                        user.SecretoryName = secretory.SecretoryName;
                        user.SecretoryId = secretory.SecretoryID;
                    }
                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {

                        if (itm.SubUserTypeName == "Vidhan Shabha Secretary")
                        {
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = user.SecretoryName + "(Vidhan Shabha Secreatory)";
                            obj.UserTypeID = itm.SubUserTypeID;
                            obj.SubUserTypeID = Convert.ToInt16(user.UserType);
                            user.usertypename = user.Name;
                            user.IsSecretoryId = true;
                        }

                    }

                }
                else if (user.UserType == 16)
                {
                    mHOD hods = new mHOD();
                    Int32 secid = Convert.ToInt32(user.SecretoryId);
                    hods.HODID = secid;
                    hods = (mHOD)Helper.ExecuteService("HOD", "GetHODById", hods);

                    user.SecretoryName = hods.HODName;
                    user.SecretoryId = hods.HODID;

                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    foreach (var itm in mdltype.UserTypeList)
                    {

                        if (itm.SubUserTypeName == "Head Of Department")
                        {
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = user.SecretoryName + "(Head Of Department)";
                            obj.UserTypeID = Convert.ToInt16(user.UserType);
                            obj.SubUserTypeID = Convert.ToInt16(user.UserType);
                            user.usertypename = user.Name;
                            user.IsHOD = true;
                        }

                    }

                }
                else if (user.UserType == 22)
                {
                    // GetMemberById
                    mMember member = new mMember();
                    mUsers subtypeModel = new mUsers();

                    member.MemberCode = Convert.ToInt32(user.UserName);
                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);
                    member = (mMember)Helper.ExecuteService("Member", "GetMemberById", member);
                    subtypeModel.UserName = member.MemberCode.ToString();
                    subtypeModel.IsMember = "True";
                    subtypeModel = (mUsers)Helper.ExecuteService("Member", "GetMemberByUserSubType", subtypeModel);
                    foreach (var itm in mdltype.UserTypeList)
                    {

                        if (itm.SubUserTypeID == Convert.ToInt32(subtypeModel.UserType))
                        {
                            user.domainAdminID = itm.SubUserTypeID;
                            user.domainAdminName = member.Name + " (Member Administrator)";
                            obj.UserTypeID = itm.UserTypeID;
                            obj.SubUserTypeID = Convert.ToInt16(user.UserType);
                            user.usertypename = user.Name;
                            user.IsMember = "True";

                        }

                    }

                }
                else if (user.UserType == 37)
                {

                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);

                    foreach (var itm in mdltype.UserTypeList)
                    {

                        if (itm.SubUserTypeID == Convert.ToInt32(user.UserType))
                        {
                            user.domainAdminID = 0;
                            user.domainAdminName = "Concerned Constituency Minister";
                            obj.UserTypeID = itm.UserTypeID;
                            obj.SubUserTypeID = Convert.ToInt16(user.UserType);
                            user.usertypename = user.Name;
                            //user.IsMember = "True";

                        }

                    }

                }
                else if (user.UserType == 38)
                {
                   
                    mUsers subtypeModel = new mUsers();
                    string memberNames = (string)Helper.ExecuteService("Member", "GetMemberNameByIds", new mUsers{UserName=user.UserName});
                    user.domainAdminID = 17;
                    user.domainAdminName = memberNames + " (Member Administrator)";
                    obj.UserTypeID = 17;
                    obj.SubUserTypeID = 38;
                    user.usertypename = user.Name;
                    //user.IsMember = "True";

                }
                else if (user.UserType == 40)
                {
                    mSubUserType mdltype = new mSubUserType();
                    mdltype.UserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeDropDown", null);

                    foreach (var itm in mdltype.UserTypeList)
                    {

                        if (itm.SubUserTypeID == Convert.ToInt32(user.UserType))
                        {
                            user.domainAdminID = 1;
                            user.domainAdminName = "SDM";
                            obj.UserTypeID = itm.UserTypeID;
                            obj.SubUserTypeID = Convert.ToInt16(user.UserType);
                            user.usertypename = user.Name;
                            //user.IsMember = "True";

                        }

                    }
                }
                user.UserAsscessActionlist = (List<tUserAccessActions>)Helper.ExecuteService("Module", "GetUserAccessActionByUserType", obj);
                tUserAccessRequest ustmdl = new tUserAccessRequest();
                ustmdl.UserID = user.UserId;

                user.UserAsscessRequestlist = (List<tUserAccessRequest>)Helper.ExecuteService("Module", "GetUserAccessRequestByID", ustmdl);

                foreach (var itm in user.UserAsscessRequestlist)
                {
                    string csv = itm.MergeActionId;
                    itm.SelectActionListKey = getSelectedActionlist(csv);

                }

                foreach (var itm in user.UserAsscessActionlist)
                {
                    //mUserModules obj = new mUserModules();
                    string csv = itm.MergeActionId;
                    itm.ActionListKey = getactionlist(csv);

                }


                user.RequestedDeptList = getAssocitedDepartmentlist(user.DepartmentIDs);
                user.ApprovedDeptList = getAssocitedDepartmentlist(user.DeptId);
                user.RequestedAdditionalDeptList = getAssocitedDepartmentlist(user.RequestedAdditionalDept);
                user.ApprovedAdditionalDeptList = getAssocitedDepartmentlist(user.ApprovedAdditionalDept);



                mSecretory secretory1 = new mSecretory();
                user.mSecretory = (ICollection<mSecretory>)Helper.ExecuteService("Secretory", "GetAllSecratoryDetails", secretory1);
                if (user.DepartmentIDs != null)
                {
                    user.AllDepartmentIds = user.DepartmentIDs.Split(',').ToList();
                }
                user.UserTypeList = (ICollection<mUserType>)Helper.ExecuteService("Module", "GetUserTypeDropDown", null);
                user.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", user);
                user.MembersList = (ICollection<mMember>)Helper.ExecuteService("Member", "GetAllMembersList", null);



                return View(user);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  List";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        public List<KeyValuePair<int, string>> getactionlist(string ids)
        {
            var radioButtonList = new List<KeyValuePair<int, string>>();
            if (ids != null && ids.Length > 0)
            {
                IEnumerable<string> idss = ids.Split(',').Select(str => str);
                mUserActions user = new mUserActions();

                foreach (var itm2 in idss)
                {
                    user.ActionId = Convert.ToInt32(itm2);
                    user = (mUserActions)Helper.ExecuteService("Module", "GetActionDataById", user);
                    if (user != null)
                    {
                        radioButtonList.Insert(0, new KeyValuePair<Int32, string>(user.ActionId, user.ActionName));
                    }
                }
            }
            return radioButtonList;
        }
        public List<KeyValuePair<int, string>> getSelectedActionlist(string ids)
        {
            var radioButtonList = new List<KeyValuePair<int, string>>();
            if (ids != null && ids.Length > 0)
            {
                IEnumerable<string> idss = ids.Split(',').Select(str => str);
                mUserActions user = new mUserActions();

                foreach (var itm2 in idss)
                {
                    user.ActionId = Convert.ToInt32(itm2);
                    user = (mUserActions)Helper.ExecuteService("Module", "GetActionDataById", user);
                    if (user != null)
                    {
                        radioButtonList.Insert(0, new KeyValuePair<Int32, string>(user.ActionId, user.ActionName));
                    }
                }
            }
            return radioButtonList;
        }
        public ActionResult GetUserSubTypeDrp(string usertypeid)
        {
            mSubUserType Usbuser = new mSubUserType();
            Usbuser.UserTypeID = Convert.ToInt32(usertypeid);
            mUsers user = new mUsers();
            user.SubUserTypeList = (ICollection<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeByUserTypeId", Usbuser);


            return PartialView("GetUserSubTypeDrp", user);

        }
        public ActionResult GetHodDrpList(int usertypeid)
        {
            mUsers user = new mUsers();
            mHOD hods = new mHOD();
            //if (usertypeid != null)
            //{
            user.HodsList = (ICollection<mHOD>)Helper.ExecuteService("HOD", "GetAllHODList", hods);
            //}


            return PartialView("_HodsList", user);
        }
        public ActionResult GetSecretaryDrpList(int usertypeid)
        {
            mUsers user = new mUsers();
            mSecretory secretory = new mSecretory();
            if (usertypeid != 3)
            {
                user.mSecretory = (ICollection<mSecretory>)Helper.ExecuteService("Secretory", "GetAllSecratoryDetails", secretory);
            }
            else
            {
                secretory.SecretoryID = 21;
                secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretoryDetailsById", secretory);
                user.SecretoryId = secretory.SecretoryID;
                user.SecretoryName = secretory.SecretoryName;
            }

            return PartialView("_SecretaryList", user);
        }
        public ActionResult GetDepartmentDrpList(int usertypeid)
        {
            mUsers user = new mUsers();
            user.UserId = new Guid(CurrentSession.UserID);
            user.IsMember = CurrentSession.IsMember;

            user = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", user);
            if (user.DepartmentIDs != null)
            {
                user.AllDepartmentIds = user.DepartmentIDs.Split(',').ToList();
            }
            mDepartment depart = new mDepartment();

            if (usertypeid != 3)
            {
                user.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", user);
            }
            else
            {
                depart.deptId = "HPD0001";
                depart = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentDetailsById", depart);
                user.DepartmentIDs = depart.deptId;
                user.DepartmentName = depart.deptname;
            }

            return PartialView("_SecretoryDepartment", user);
        }
        public ActionResult SendDepartmentRequest(mUsers model)
        {
            try
            {
                mUsers mdl = model as mUsers;
                //mdl.DepartmentIDs = null;

                //if (mdl.DeptId != null && mdl.DeptId != "" && (mdl.DepartmentIDs == "" || mdl.DepartmentIDs == null))
                //{
                //    mdl.DepartmentIDs = null;
                //    if (!mdl.DeptId.Contains(","))
                //    {
                //        mdl.DeptId = mdl.DeptId + ",";
                //    }

                //    if (mdl.DeptId.IndexOf(",") != -1)
                //    {
                //        string[] arr = mdl.DeptId.Split(',');
                //        foreach (var item in mdl.AllDepartmentIds)
                //        {

                //            if (arr.Contains(item))
                //            {
                //            }
                //            else
                //            {
                //                mdl.DepartmentIDs += item + ",";
                //            }

                //        }
                //        if (mdl.DepartmentIDs != null && mdl.DepartmentIDs != "")
                //        {
                //            bool flag = mdl.DepartmentIDs.EndsWith(",");
                //            if (flag)
                //            {
                //                mdl.DepartmentIDs = mdl.DepartmentIDs.Substring(0, mdl.DepartmentIDs.Length - 1);
                //            }
                //        }
                //    }
                //}
                //else if (mdl.AllDepartmentIds.Count>0)
                //{
                //    mdl.DepartmentIDs = null;
                //    foreach (var dept in mdl.AllDepartmentIds)
                //    {
                //        mdl.DepartmentIDs += dept + ",";
                //    }
                //    bool flag = mdl.DepartmentIDs.EndsWith(",");
                //    if (flag)
                //    {
                //        mdl.DepartmentIDs = mdl.DepartmentIDs.Substring(0, mdl.DepartmentIDs.Length - 1);
                //    }
                //}


                //changed code
                if (mdl.AllDepartmentIds != null && mdl.AllDepartmentIds.Count > 0)
                {
                    mdl.DepartmentIDs = null;

                    foreach (var item in mdl.AllDepartmentIds)
                    {

                        mdl.DepartmentIDs += item + ",";

                    }
                    if (mdl.DepartmentIDs != null && mdl.DepartmentIDs != "")
                    {
                        bool flag = mdl.DepartmentIDs.EndsWith(",");
                        if (flag)
                        {
                            mdl.DepartmentIDs = mdl.DepartmentIDs.Substring(0, mdl.DepartmentIDs.Length - 1);
                        }
                    }

                }



                #region DeptValidation
                if (CurrentSession.SubUserTypeID == "3" && model.OfficeLevel == "2")
                {
                    mUsers _ObjmUsers = new mUsers();
                    _ObjmUsers.SecretoryId = model.SecretoryId;
                    var UserDetsils = (mUsers)Helper.ExecuteService("User", "GetUserDetailsBySecretoryID", _ObjmUsers);

                    if (UserDetsils != null)
                    {
                        if (UserDetsils.DeptId != null)
                        {
                            if (!UserDetsils.DeptId.Contains(","))
                            {
                                UserDetsils.DeptId = UserDetsils.DeptId + ",";
                            }

                            else
                            {

                            }
                        }
                    }
                    string[] SecDepIDs = UserDetsils.DeptId.Split(',');

                    string temp = "";
                    if (mdl.DepartmentIDs != null && mdl.DepartmentIDs != "")
                    {
                        if (!mdl.DepartmentIDs.Contains(","))
                        {
                            temp = mdl.DepartmentIDs + ",";
                        }

                        else
                        {
                            temp = mdl.DepartmentIDs;
                        }
                    }

                    string[] PendingDepIDs = temp.Split(',');


                    string FinalDeptList = "";
                    foreach (var item in PendingDepIDs)
                    {
                        if (item != "" && item != null)
                        {
                            if (!SecDepIDs.Contains(item.ToString()))
                            {
                                FinalDeptList += item.ToString() + ",";
                            }
                        }
                    }

                    if (FinalDeptList != null && FinalDeptList != "" && FinalDeptList.Length > 0)
                    {
                        //return Content("<script>alert('This department not belong to your department.)</script>");
                        return Content("0");
                    }
                    else
                    {
                        mdl.IsSecChange = false;
                    }


                }
                #endregion

                var mdls = (mUsers)Helper.ExecuteService("User", "UpdateDepatmentAndSecretary", mdl);
                //return RedirectToAction("Index");
                return Content("1");
            }
            catch
            {
            }
            return null;
        }
        public string getAssocitedDepartmentlist(string ids)
        {
            string stringlist = "";
            if (ids != null && ids.Length > 0)
            {
                IEnumerable<string> idss = ids.Split(',').Select(str => str);
                mDepartment user = new mDepartment();

                foreach (var itm2 in idss)
                {
                    user.deptId = itm2;

                    user = (mDepartment)Helper.ExecuteService("Module", "GetAssiciatedDepartmentDataById", user);
                    if (user!=null)
                    {
                        stringlist += user.deptname + ",";
                    }
                    
                    //radioButtonList.Insert(0, new KeyValuePair<string, string>(user.deptId, user.deptname));
                    //radioButtonList.Insert(0, new KeyValuePair<Int32, string>(i, user.deptname));
                }
            }
            if (stringlist != "")
            {
                bool flag = stringlist.EndsWith(",");
                if (flag)
                {
                    stringlist = stringlist.Substring(0, stringlist.Length - 1);
                }
            }
            return stringlist;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendUserAcccessRequest(mUsers model, string hdnUserAccessAccessIds, string hdnActionAllIDS)
        {
           
            try
            {
                bool isSubmitted = false;
                tUserAccessRequest mdl = new tUserAccessRequest();
                // mdl.SecreataryId = Convert.ToInt32(model.SecretoryId);
                if (model.DeptId != null)
                {
                    mdl.AssociateDepartmentId = model.DeptId;
                    mdl.SelectedAssociateDepartmentId = model.DeptId;
                }
                else
                {
                    mdl.AssociateDepartmentId = model.DepartmentIDs;
                    mdl.SelectedAssociateDepartmentId = model.DepartmentIDs;
                }
                mdl.UserID = new Guid(CurrentSession.UserID);
                mdl.TypedDomainId = Convert.ToInt32(CurrentSession.SubUserTypeID);
                mdl.SubUserTypeID = Convert.ToInt32(CurrentSession.SubUserTypeID);
                mdl.DomainAdminstratorId = Convert.ToInt32(model.domainAdminID);
                if (model.IsMember.ToUpper() == "TRUE")
                {
                    mdl.MemberId = Convert.ToInt32(model.UserName);
                    mdl.SecreataryId = 0;
                    mdl.HODId = 0;
                    mdl.OfficeID = 0;
                }
                else if (model.IsHOD == true)
                {
                    mdl.HODId = Convert.ToInt32(model.SecretoryId);
                    mdl.SecreataryId = 0;
                    mdl.MemberId = 0;
                    mdl.OfficeID = 0;
                }
                else if (model.IsSecretoryId == true)
                {
                    mdl.SecreataryId = Convert.ToInt32(model.SecretoryId);
                    mdl.HODId = 0;
                    mdl.MemberId = 0;
                    mdl.OfficeID = 0;
                }
                else if (mdl.SubUserTypeID == 37)
                {
                    mdl.SecreataryId = 0;
                    mdl.HODId = 0;
                    mdl.MemberId = 0;
                    mdl.OfficeID = Convert.ToInt32(model.OfficeId);
                }
                else if (mdl.SubUserTypeID == 38)
                {
                    mdl.SecreataryId = 0;
                    mdl.HODId = 0;
                    mdl.OfficeID = 0;
                }

                mdl.DesignationId = 0;
                if (hdnUserAccessAccessIds.Length > 0)
                {
                    hdnUserAccessAccessIds = hdnUserAccessAccessIds.Substring(2, hdnUserAccessAccessIds.Length - 2);
                    hdnActionAllIDS = hdnActionAllIDS.Substring(1, hdnActionAllIDS.Length - 1);
                    string[] allidsarr = hdnActionAllIDS.Split('#');
                    string[] arr = hdnUserAccessAccessIds.Split('#');
                    string allActionids = "";
                    for (int i = 0; arr.Length > i; i++)
                    {
                        string value = arr[i].ToString();
                        if (value.Length > 0)
                        {
                            value = value.Substring(1, value.Length - 1);
                            string[] tparr = value.Split(',');

                            string moduleid = tparr[0]; //value.Substring(0, 1);
                            for (int j = 0; allidsarr.Length > j; j++)
                            {

                                string tvalue1 = allidsarr[j].ToString();
                                string[] allidsrrr = tvalue1.Split(',');
                                string mdlid = allidsrrr[0];
                                tvalue1 = tvalue1.Substring(1, tvalue1.Length - 1);

                                if (mdlid == moduleid)
                                {

                                    int indx1 = tvalue1.IndexOf(",");
                                    allActionids = tvalue1.Substring(indx1, tvalue1.Length - indx1);
                                    allActionids = allActionids.Substring(1, allActionids.Length - 1);
                                }
                            }


                            int indx = value.IndexOf(",");
                            string actionids = value.Substring(indx, value.Length - indx);
                            actionids = actionids.Substring(1, actionids.Length - 1);
                            mdl.AccessID = Convert.ToInt32(moduleid);
                            mdl.MergeActionId = actionids;
                            mdl.ActionControlId = allActionids;
                            mdl.Modifiedwhen = System.DateTime.Now;

                            if (model.UserType == 38)
                            {

                                if (!string.IsNullOrEmpty(model.UserName))
                                {
                                    string[] NameArr = model.UserName.Split(',').ToArray();
                                    foreach (var ts in NameArr)
                                    {
                                        mdl.MemberId = Convert.ToInt32(ts);
                                        bool vtr = (bool)Helper.ExecuteService("Module", "ExistUserAccessRequestByThreeId", mdl);
                                        if (vtr == true)
                                        {

                                            Helper.ExecuteService("Module", "UpdateSDMRequest", mdl);
                                        }
                                        else
                                        {

                                            Helper.ExecuteService("Module", "SaveUserAccessRequest", mdl);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                bool vtr = (bool)Helper.ExecuteService("Module", "ExistUserAccessRequestByTwoId", mdl);
                                if (vtr == true)
                                {
                                    Helper.ExecuteService("Module", "UpdateMeargeActionID", mdl);
                                }
                                else
                                {
                                    Helper.ExecuteService("Module", "SaveUserAccessRequest", mdl);
                                }
                            }

                            isSubmitted = true;
                        }
                        //string[] arr1 = value.Split(',');
                    }
                }
                if (isSubmitted)
                {
                    TempData["IsSubmitted"] = "Submitted";
                }
                else
                {
                    TempData["IsSubmitted"] = "";
                }

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  List";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;  
            }
           
        }

        public ActionResult UpdateAccessOrder(string ModuleAction, string AccessOrder)
        {
            try
            {
                if (ModuleAction != null && ModuleAction != "" && AccessOrder != null && AccessOrder != "")
                {
                    bool flag = ModuleAction.EndsWith(",");
                    if (flag)
                    {
                        ModuleAction = ModuleAction.Substring(0, ModuleAction.Length - 1);
                    }
                    string[] Final_ModuleAction = ModuleAction.Split(',');

                    bool flag2 = AccessOrder.EndsWith(",");
                    if (flag)
                    {
                        AccessOrder = AccessOrder.Substring(0, AccessOrder.Length - 1);
                    }
                    string[] Final_AccessOrder = AccessOrder.Split(',');
                    for (int i = 0; i < Final_ModuleAction.Count(); i++)
                    {
                        for (int j = i; j == i; j++)
                        {
                              tUserAccessActions _ObjtUserAccessActions = new tUserAccessActions();
                            _ObjtUserAccessActions.UserAccessActionsId = int.Parse(Final_ModuleAction[i]);
                            _ObjtUserAccessActions.AccessActionOrder = Convert.ToInt32(Final_AccessOrder[j]);
                            Helper.ExecuteService("Module", "UpdateAccessActionOrder", _ObjtUserAccessActions);
                          
                            //tUserAccessRequest _ObjtUserAccessRequest = new tUserAccessRequest();
                            //_ObjtUserAccessRequest.ID = int.Parse(Final_ModuleAction[i]);
                            //_ObjtUserAccessRequest.AccessOrder = Convert.ToInt32(Final_AccessOrder[j]);
                            //Helper.ExecuteService("Module", "UpdateAccessOrder", _ObjtUserAccessRequest);
                        }
                    }


                    //foreach (var item in Final_ModuleAction)
                    //{
                    //    foreach (var item2 in Final_AccessOrder)
                    //    {
                    //        tUserAccessRequest _ObjtUserAccessRequest = new tUserAccessRequest();
                    //        _ObjtUserAccessRequest.ID = int.Parse(item);
                    //        _ObjtUserAccessRequest.AccessOrder = item2;
                    //        Helper.ExecuteService("Module", "UpdateAccessOrder", _ObjtUserAccessRequest); 
                    //    }
                    //    break;
                    //}
                }
                return Json("Update Successfully", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult SendAdditionalDeptRequest(mUsers model)
        {
            try
            {
                if (model.AllAdditionalDepartmentIds != null && model.AllAdditionalDepartmentIds.Count > 0)
                {
                    model.RequestedAdditionalDept = null;

                    foreach (var item in model.AllAdditionalDepartmentIds)
                    {

                        model.RequestedAdditionalDept += item + ",";

                    }
                    if (model.RequestedAdditionalDept != null && model.RequestedAdditionalDept != "")
                    {
                        bool flag = model.RequestedAdditionalDept.EndsWith(",");
                        if (flag)
                        {
                            model.RequestedAdditionalDept = model.RequestedAdditionalDept.Substring(0, model.RequestedAdditionalDept.Length - 1);
                        }
                    }
                }
                Helper.ExecuteService("User", "UpdateAdditionalDepatment", model);
               // return Json("Update Successfully", JsonRequestBehavior.AllowGet);
                return Content("1"); 
            }
            catch
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }
        
    }
}
