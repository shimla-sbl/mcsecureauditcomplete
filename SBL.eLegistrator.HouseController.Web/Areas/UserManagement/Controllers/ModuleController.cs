﻿using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;

namespace SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Controllers
{
    public class ModuleController : Controller
    {
        //
        // GET: /UserManagement/Module/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserManagement()
        {
            return View();
        }

        public ActionResult GetUserModules()
        {
            var Modules = (List<mUserModules>)Helper.ExecuteService("Module", "GetUserModules", null);
            var model1 = Modules.ToViewUserModel();

            return PartialView("_GetUserModule", model1);
        }


        public ActionResult CreateNewModule()
        {
           mUserType model = new mUserType();
            var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
            var returnedResultNew = returnedResult.ToModelList();
              

            var model1 = new mUserModules()
            {
                Mode = "Add",
                objList = returnedResultNew
            };
                   
            return PartialView("_CreateNewModule", model1);

        }        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveModule(mUserModules model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToModuleModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "CreateModule", dist);
                }
                else
                {
                    Helper.ExecuteService("District", "UpdateDistrict", dist);
                }
              
                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }
            else
            {
               
                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateModule(mUserModules model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToModuleModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "CreateModule", dist);
                }
                else
                {
                    Helper.ExecuteService("Module", "UpdateEntryModules", dist);
                }

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }
            else
            {

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }

        }

        public ActionResult EditUserModule(int ModuleId)
        {

            mUserModules ModuleToEdit = (mUserModules)Helper.ExecuteService("Module", "GetModuleDataById", new mUserModules { ModuleId = ModuleId });

            var model = ModuleToEdit.ToViewModuleModel("Edit");
            model.ModuleId = ModuleId;
            model.ModuleName = model.ModuleName;

            model.ModuleDescription = model.ModuleDescription;
            return View("_UpdateUserModule", model);
        }

        //public ActionResult EditUserModule(int ModuleId)
        //{

        //    mUserType model = new mUserType();
        //    var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
        //    var returnedResultNew = returnedResult.ToModelList();


        //    var model1 = new mUserModules()
        //    {
               
        //        objList = returnedResultNew
        //    };

        //    mUserModules ModuleToEdit = (mUserModules)Helper.ExecuteService("Module", "GetModuleDataById", new mUserModules { ModuleId = ModuleId });

        //    var model2 = ModuleToEdit.ToViewModuleModel("Edit");
        //    model1.ModuleId = ModuleId;
        //    model1.ModuleName = model2.ModuleName;
           
        //     model1.ModuleDescription=model2.ModuleDescription ;
        //    return View("_UpdateUserModule", model1);
        //}

        public JsonResult UpdateEntry(value model)
        {
            mUserModules objModel = new mUserModules();
            objModel.ModuleId = model.ModuleId;
            objModel.ModuleName = model.ModuleName;
            objModel.ModuleDescription = model.ModuleDescription;
           
            objModel.Isactive = model.Isactive;
            objModel = Helper.ExecuteService("Module", "UpdateEntryModules", objModel) as mUserModules;
            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public class value
        {
            [AllowHtml]
            public string ModuleName { get; set; }
            [AllowHtml]
            public string ModuleDescription { get; set; }
            public int ModuleId { get; set; }
            public bool Isactive { get; set; }
            public int UserTypeID { get; set; }
                     
            public string UserTypeName { get; set; }


        }


        public JsonResult DeleteModule(int ModuleId)
        {
            mUserModules moduleToDelete = (mUserModules)Helper.ExecuteService("Module", "GetModuleDataById", new mUserModules { ModuleId = ModuleId });
            Helper.ExecuteService("Module", "DeleteModule", moduleToDelete);
            return Json("Update.Message", JsonRequestBehavior.AllowGet);

        }

        public JsonResult CheckUserAccess(string UserAccessName)
        {
            try
            {
                var mdl = (bool)Helper.ExecuteService("Module", "CheckUserAccessExist", UserAccessName);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }

      
    }
}
