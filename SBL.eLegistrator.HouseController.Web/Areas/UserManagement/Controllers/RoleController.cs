﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Role;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;

using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Assembly;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;


namespace SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Controllers
{
    public class RoleController : Controller
    {
        //
        // GET: /UserManagement/Role/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserManagement()
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }
            mRoles model = new mRoles();
            //Get the Total count of All Type of question.
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            model.SessionCode = model.SessionId = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);


            mSession Mdl = new mSession();
            Mdl.SessionCode = model.SessionId;
            Mdl.AssemblyID = model.AssemblyCode;
            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = model.AssemblyCode;

            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
            {
                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

            }
            else
            {
                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameLocalBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameLocalByAssemblyCode", assmblyMdl);


               
            }

            if (CurrentSession.UserID != null)
            {
                model.ModifiedBy = !string.IsNullOrEmpty(CurrentSession.UserName) ? CurrentSession.UserName : null;
                model.ModifiedDate = DateTime.Now;
            }
            return View(model);
        }


        public ActionResult LeftNavigationMenu()
        {
            return PartialView("_LeftNavigationMenu");
        }


       

        //public ActionResult GetUserRole()
        //{
        //    mRolesModel model = new mRolesModel();

        //    model = (mRolesModel)Helper.ExecuteService("Role", "GetUserRole", model);

        //    return PartialView("_GetUserRole", model);
        //}




        public ActionResult GetUserRole()
        {
            mRoles model1 = new mRoles();
            if (CurrentSession.UserID != null)
            {
               
                model1.ModifiedBy = !string.IsNullOrEmpty(CurrentSession.UserName) ? CurrentSession.UserName : null;
                model1.ModifiedDate = DateTime.Now;
            }
           
            var Roles = (List<mRoles>)Helper.ExecuteService("Role", "GetUserRole", null);

            var model = Roles.ToViewUserRole();

            return PartialView("_GetUserRole", model);
        }


        public ActionResult CreateNewRole()
        {

            var model = new mRoles()
            {
                Mode = "Add",

            };
            model.Mode = "Add";
           
            return PartialView("_CreateNewRole", model);
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveRole(mRoles model)
        {

            if (CurrentSession.UserID != null)
            {
                model.ModifiedBy = !string.IsNullOrEmpty(CurrentSession.UserName) ? CurrentSession.UserName : null;
                model.ModifiedDate = DateTime.Now;
            }
            if (ModelState.IsValid)
            {
                var dist = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Role", "CreateRole", dist);
                }
                else
                {
                    Helper.ExecuteService("District", "UpdateDistrict", dist);
                }
                return RedirectToAction("UserManagement");
            }
            else
            {
                return RedirectToAction("UserManagement");
            }

        }

        public ActionResult EditUserRole(System.Guid RoleId)
        {

            mRoles RoleToEdit = (mRoles)Helper.ExecuteService("Role", "GetRoleDataById", new mRoles { RoleId = RoleId });
           
            var model = RoleToEdit.ToViewModel1("Edit");
            model.RoleId = RoleId;
            return View("_UpdateUserRole", model);
        }

        public JsonResult DeleteRole(System.Guid RoleId)
        {
            mRoles rolesToDelete = (mRoles)Helper.ExecuteService("Role", "GetRoleDataById", new mRoles { RoleId = RoleId });
            Helper.ExecuteService("Role", "DeleteRoles", rolesToDelete);
            return Json("Update.Message", JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateRole(mRoles model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Role", "CreateRole", dist);
                }
                else
                {
                    Helper.ExecuteService("Role", "UpdateEntryModules", dist);
                }
                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }
            else
            {
                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }

        }

      

        public JsonResult UpdateEntry(value model)
        {
             mRoles objModel = new mRoles();
            
            objModel.RoleId = model.RoleId;
            objModel.RoleName = model.RoleName;
            objModel.RoleDescription = model.RoleDescription;
            objModel.Isactive = model.Isactive;
            objModel = Helper.ExecuteService("Role", "UpdateEntryModules", objModel) as mRoles;
            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public class value
        {
            [AllowHtml]
            public string RoleName { get; set; }
            [AllowHtml]
            public string RoleDescription { get; set; }
            public System.Guid RoleId { get; set; }
            public bool Isactive { get; set; }  

        }


        public ActionResult CheckRoleId(System.Guid RoleId)
        {
            var mdl = (bool)Helper.ExecuteService("Role", "CheckRoleIDExist", RoleId);

            return Json(mdl, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckRoleName(string RoleName)
        {
            try
            {
                var mdl = (bool)Helper.ExecuteService("Role", "CheckUserRoleExist", RoleName);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }

    }
}
