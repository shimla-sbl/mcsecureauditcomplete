﻿using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;


namespace SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Controllers
{
    public class UserTypeController : Controller
    {
        //
        // GET: /UserManagement/UserType/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserManagement()
        {
            return View();
        }

        public ActionResult GetUserType()
        {
            var Modules = (List<mUserType>)Helper.ExecuteService("Module", "GetUserType", null);
            var model = Modules.ToViewUserType();

            return PartialView("_GetUserType", model);
        }

        public ActionResult CreateNewUserType()
        {

            var model = new mUserType()
            {
                Mode = "Add",

            };
            model.Mode = "Add";

            return PartialView("_CreateNewUserType", model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveUserType(mUserType model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToUserTypeModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "CreateUserType", dist);
                }
                else
                {
                   // Helper.ExecuteService("District", "UpdateDistrict", dist);
                }

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }
            else
            {

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }

        }


        public ActionResult EditUserType(int UserTypeID)
        {

            mUserType UserTypeToEdit = (mUserType)Helper.ExecuteService("Module", "GetUserTypeDataById", new mUserType { UserTypeID = UserTypeID });

            var model = UserTypeToEdit.ToViewUserTypeModel("Edit");
            model.UserTypeID = UserTypeID;
            return View("_UpdateUserType", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUserType(mUserType model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToUserTypeModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "CreateUserType", dist);
                }
                else
                {
                    Helper.ExecuteService("Module", "UpdateEntryUserType", dist);
                }

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }
            else
            {

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }

        }

        public JsonResult UpdateUserTypeEntry(value model)
        {
            mUserType objModel = new mUserType();
            objModel.UserTypeID = model.UserTypeId;
            objModel.UserTypeName = model.UserTypeName;
           
            objModel.IsActive = model.Isactive;
            objModel = Helper.ExecuteService("Module", "UpdateEntryUserType", objModel) as mUserType;
            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public class value
        {
            [AllowHtml]
            public string UserTypeName { get; set; }
           
            public int UserTypeId { get; set; }
            public bool Isactive { get; set; }

        }


        public JsonResult DeleteUserType(int UserTypeId)
        {
            mUserType UserTypeToDelete = (mUserType)Helper.ExecuteService("Module", "GetUserTypeDataById", new mUserType { UserTypeID = UserTypeId });
            Helper.ExecuteService("Module", "DeleteUserType", UserTypeToDelete);
            return Json("Update.Message", JsonRequestBehavior.AllowGet);

        }

        public JsonResult CheckUserName(string UserTypeName)
        {
            try
            {
               
                var mdl = (bool)Helper.ExecuteService("Role", "CheckUserTypeExist", UserTypeName);
                
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                
                throw;
            }
        }
    }
}
