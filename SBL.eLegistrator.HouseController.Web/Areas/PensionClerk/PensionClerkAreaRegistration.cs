﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PensionClerk
{
    public class PensionClerkAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PensionClerk";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PensionClerk_default",
                "PensionClerk/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
