﻿using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Assembly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Session;
using Microsoft.Security.Application;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Ministery;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Minister.Controllers
{
    [SBLAuthorize(Allow = "Authenticated")]
    [Audit]
    [NoCache]
    public class MinisterController : Controller
    {
        //
        // GET: /Minister/Minister/

        #region Minister By Uday

        public ActionResult MinisterDashboard()
        {
            tPaperLaidV model = new tPaperLaidV();
           
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                Obj.LoginId = Utility.CurrentSession.UserName;
                List<mSiteSettingsVS> ListSiteSettingsVS = new List<mSiteSettingsVS>();
                mSiteSettingsVS SiteSettingsAssem = new mSiteSettingsVS();
                mSiteSettingsVS SiteSettingsSessn = new mSiteSettingsVS();
                model.DepartmentId = CurrentSession.DeptID;
              
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
                model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);
                Obj.SessionCode = model.SessionCode;
                Obj.AssemblyCode = model.AssemblyCode;

                mDepartment deptInfo = new mDepartment();
                deptInfo.deptId = CurrentSession.DeptID;
                deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
                model.DeparmentName = deptInfo.deptname;

                Obj.DeparmentName = model.DeparmentName;

                model.CurrentUserName = CurrentSession.UserName;
                model.UserDesignation = CurrentSession.MemberDesignation;

                Obj.CurrentUserName = model.CurrentUserName;
                Obj.UserDesignation = model.UserDesignation;
           
                model.SessionId = Convert.ToInt32(siteSettingMod.SessionCode);
                model.AssemblyCode = Convert.ToInt32(siteSettingMod.AssemblyCode);
                Obj.SessionId = model.SessionId;
                Obj.AssemblyCode = model.AssemblyCode;

                mSession Mdl = new mSession();
                Mdl.SessionCode = model.SessionId;
                Mdl.AssemblyID = model.AssemblyCode;
                mAssembly assmblyMdl = new mAssembly();
                assmblyMdl.AssemblyID = model.AssemblyCode;
            

                Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", Obj);

                Obj.MinisterName = Obj.ComModel.FirstOrDefault().MinisterName;
                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
                Obj.SessionName = model.SessionName;
                Obj.AssesmblyName = model.AssesmblyName;

                Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "GetInitialCount", Obj);

                ListSiteSettingsVS = Helper.ExecuteService("User", "GetSettings", SiteSettingsAssem) as List<mSiteSettingsVS>;


                SiteSettingsAssem = ListSiteSettingsVS.First(x => x.SettingName == "Assembly");
                if (SiteSettingsAssem != null)
                {
                    Obj.AssemblyId = Convert.ToInt16(SiteSettingsAssem.SettingValue);

                    SiteSettingsSessn = ListSiteSettingsVS.First(x => x.SettingName == "Session");
                    if (SiteSettingsSessn != null)
                    {
                        Obj.SessionId = Convert.ToInt16(SiteSettingsSessn.SettingValue);
                    }
                }

            }

            return View(Obj);
        }

        public ActionResult GetPendings()
        {
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                Obj.LoginId = Utility.CurrentSession.UserName;
                Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "GetPendings", Obj);
            }
            return PartialView("_GetMinisterPendingList", Obj);
        }

        public ActionResult GetSubmitted()
        {
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (Utility.CurrentSession.MemberID != null && Utility.CurrentSession.MemberID != "")
            {
                Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "GetSubmitted", Obj);
            }
            return PartialView("_GetMinisterSubmittedList", Obj);
        }

        public ActionResult SearchByCategory(int CategId, string Tabtype, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {

                Obj.LoginId = Utility.CurrentSession.UserName;
                Obj.EventId = CategId;
                Obj.ActionType = Tabtype;
                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

                Obj.loopStart = Convert.ToInt16(loopStart);
                Obj.loopEnd = Convert.ToInt16(loopEnd);
                Obj.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
                Obj.PageIndex = Convert.ToInt16(PageNumber);

                Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "SearchByCategory", Obj);
                if (PageNumber != null && PageNumber != "")
                {
                    Obj.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    Obj.PageNumber = Convert.ToInt32("1");
                }
                if (RowsPerPage != null && RowsPerPage != "")
                {
                    Obj.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    Obj.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    Obj.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    Obj.selectedPage = Convert.ToInt32("1");
                }
                if (loopStart != null && loopStart != "")
                {
                    Obj.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    Obj.loopStart = Convert.ToInt32("1");
                }
                if (loopEnd != null && loopEnd != "")
                {
                    Obj.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    Obj.loopEnd = Convert.ToInt32("5");
                }
            }
            if (Tabtype == "Pending")
            {
                return PartialView("_GetMinisterPendingSearchList", Obj);
            }

            else
            {
                return PartialView("_GetMinisterSubmitedSearchList", Obj);
            }
        }

        [HttpPost]
        public JsonResult GetCategoryByPaperCategoryTypeId(int PaperCategoryTypeId)
        {
            tPaperLaidV model = new tPaperLaidV();
            mEvent objEvent = new mEvent();

            objEvent.PaperCategoryTypeId = PaperCategoryTypeId;

            objEvent = Helpers.Helper.ExecuteService("Minister", "GetCategoryByPaperCategoryTypeId", objEvent) as mEvent;

            return Json(objEvent.EventList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewPdfFromServerSignIntention(string DeptActivePaperId)
        {
            DeptActivePaperId = Sanitizer.GetSafeHtmlFragment(DeptActivePaperId);
            if (DeptActivePaperId != null && DeptActivePaperId != "")
            {
                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
                int deptactrivepaperId = Convert.ToInt16(DeptActivePaperId);
                PaperLaidAttachmentVS.DeptActivePaperId = deptactrivepaperId;

                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachment", deptactrivepaperId);

                string path = "";
                foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
                {
                    path = item.FilePath;
                }

                if (path == "")
                {
                    return null;
                }
                return File(path, "application/pdf");
            }
            else
            {
                return null;
            }
        }

        public ActionResult ViewSentPdfMinister(string DeptActivePaperId)
        {
            DeptActivePaperId = Sanitizer.GetSafeHtmlFragment(DeptActivePaperId);
            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
            if (DeptActivePaperId != null && DeptActivePaperId != "")
            {
                string noteid = DeptActivePaperId;
                PaperLaidAttachmentVS.DeptActivePaperId = Convert.ToInt16(noteid);
                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachment", DeptActivePaperId);

                string savedPDF = "";
                foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
                {
                    if (item.DeptActivePaperId > 0)
                    {
                        savedPDF = item.FilePath;
                        ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(savedPDF);
                    }
                }
            }

            return PartialView("_ViewSignSentFile", PaperLaidAttachmentVS);
        }

        public ActionResult PartialSignMinisterFile(string DeptActivePaperId)
        {
            DeptActivePaperId = Sanitizer.GetSafeHtmlFragment(DeptActivePaperId);
            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
            if (DeptActivePaperId != null && DeptActivePaperId != "")
            {
                string noteid = DeptActivePaperId;
                PaperLaidAttachmentVS.DeptActivePaperId = Convert.ToInt16(noteid);
                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachment", mMinisteryMinisterModel);
                string path = "";

                foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
                {
                    path = item.FilePath;
                }

                path = path.Replace(" ", "%20");
                var test = Request.Url.AbsoluteUri;
                string[] abc = test.Split('/');
                string Prifix = abc[0] + "//" + abc[2];
                mMinisteryMinisterModel.FilePath = Prifix + path;

                mMinisteryMinisterModel.Message = Prifix + "/Minister/Minister/SubmitedSignIntention?DeptActivePaperId=" + DeptActivePaperId;

                string session = Convert.ToString(Session["DeptActivePaperId"]);
            }
            return PartialView("_SignMinisterFile", mMinisteryMinisterModel);
        }

        [HttpGet]
        public ActionResult ChangedPaperDetails(string PaperLaidId, string DeptActivePaperId)
        {
            PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);

            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
            if (PaperLaidId != null && PaperLaidId != "")
            {

                string PaperLaidID = PaperLaidId;
                mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);
                PaperLaidAttachmentVS.PaperLaidId = Convert.ToInt16(PaperLaidID);                
                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetChangedPaperDetails", PaperLaidId);
            }

            return PartialView("_ChangedPaperDetails", PaperLaidAttachmentVS);
        }

        [HttpGet]
        public ActionResult GetSubmitChangedPaperDetails(string PaperLaidId, string DeptActivePaperId)
        {
            PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);

            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
            if (PaperLaidId != null && PaperLaidId != "")
            {
                string PaperLaidID = PaperLaidId;
                mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);
                PaperLaidAttachmentVS.PaperLaidId = Convert.ToInt16(PaperLaidID);
                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetSubmitChangedPaperDetails", PaperLaidId);
            }

            return PartialView("_SubmittedChangedPaperLaid", PaperLaidAttachmentVS);
        }

        [HttpGet]
        public ActionResult MinisterPaperLaidDetails(string PaperLaidId)
        {
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
            mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);
            mMinisteryMinisterModel.MinisterName = CurrentSession.MemberID;
            mMinisteryMinisterModel = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "MinisterPaperLaidDetails", mMinisteryMinisterModel);
            return PartialView("_MinisterPaperLaidDetails", mMinisteryMinisterModel);
        }

        [HttpGet]
        public ActionResult SubmittedMinisterPaperLaidDetails(string PaperLaidId)
        {
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
            mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);
            mMinisteryMinisterModel.MinisterName = CurrentSession.MemberID;
            mMinisteryMinisterModel = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "MinisterPaperLaidDetails", mMinisteryMinisterModel);
            return PartialView("_SubmittedMinisterPaperLaidDetails", mMinisteryMinisterModel);
        }

        public ActionResult ChangedViewPdfFromServerSignIntention(string PaperLaidTempId)
        {
            PaperLaidTempId = Sanitizer.GetSafeHtmlFragment(PaperLaidTempId);
            if (PaperLaidTempId != null && PaperLaidTempId != "")
            {
                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
                int paperlaidid = Convert.ToInt16(PaperLaidTempId);
                PaperLaidAttachmentVS.PaperLaidId = paperlaidid;
                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "ChangedGetMinisterPaperAttachment", paperlaidid);

                string path = "";
                foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
                {
                    path = item.SignedFilePath;
                }
                if (path == "")
                {
                    return null;
                }
                return File(path, "application/pdf");
            }
            else
            {
                return null;
            }
        }

        public ActionResult SubmitSelectedPaper(string Ids)
        {
            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

            if (Ids != null && Ids != "")
            {
                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

                string path = "";
                string Files = "";
                string Prifix = "";
                foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
                {
                    path = item.FilePath + item.FileName;
                    path = path.Replace(" ", "%20");
                    var test = Request.Url.AbsoluteUri;
                    string[] abc = test.Split('/');
                    Prifix = abc[0] + "//" + abc[2];
                    if (Files == "")
                    {
                        Files = Prifix + path;
                    }
                    else
                    {
                        Files = Files + "," + Prifix + path;
                    }
                }

                mMinisteryMinisterModel.FilePath = Files;
                mMinisteryMinisterModel.ForSave = Prifix + "/Minister/Minister/SubmitedSignIntention?PaperLaidId=" + Ids;

                mMinisteryMinisterModel.Message = Prifix + "/Minister/Minister/UpdateInfoSignPDF?PaperLaidId=" + Ids;

                string session = Convert.ToString(Session["Ids"]);
            }
            return PartialView("_SignMinisterFile", mMinisteryMinisterModel);
        }

        public ActionResult SubmitedSignIntention(HttpPostedFileBase file, string PaperLaidId)
        {
            PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);
            if (file != null)
            {
                string filename = System.IO.Path.GetFileName(file.FileName);
                bool folderExists = Directory.Exists(Server.MapPath(file.FileName));

                List<mSiteSettingsVS> ListSiteSettingsVS = new List<mSiteSettingsVS>();
                mSiteSettingsVS SiteSettingsAssem = new mSiteSettingsVS();
                mSiteSettingsVS SiteSettingsSessn = new mSiteSettingsVS();
                ListSiteSettingsVS = Helper.ExecuteService("User", "GetSettings", SiteSettingsAssem) as List<mSiteSettingsVS>;
                string AssemblyId = "";
                string SessionId = "";
                SiteSettingsAssem = ListSiteSettingsVS.First(x => x.SettingName == "Assembly");
                if (SiteSettingsAssem != null)
                {
                    AssemblyId = Convert.ToString(SiteSettingsAssem.SettingValue);

                    SiteSettingsSessn = ListSiteSettingsVS.First(x => x.SettingName == "Session");
                    if (SiteSettingsSessn != null)
                    {
                        SessionId = Convert.ToString(SiteSettingsSessn.SettingValue);
                    }
                }

                string path = System.IO.Path.Combine(Server.MapPath("~/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/"), filename);

                string directory = Server.MapPath(path);
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }

                file.SaveAs(path);

                tPaperLaidTemp PaperLaidAttachmentVS = new tPaperLaidTemp();

                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

                return RedirectToAction("MinisterDashboard", "Minister", new { area = "Minister", message = "Sign completed" });
            }

            return RedirectToAction("MinisterDashboard", "Minister", new { area = "Minister", message = "Sign completed" });
        }

        public ActionResult UpdateInfoSignPDF(string PaperLaidId)
        {
            PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);

            tPaperLaidTemp pT = new tPaperLaidTemp();
            pT.FilePath = PaperLaidId;
            pT.MinisterSubmittedDate = DateTime.Now;
            pT.MinisterSubmittedBy = Convert.ToInt16(Utility.CurrentSession.MemberID);
            var modelReturned = Helper.ExecuteService("Minister", "InsertSignPathAttachment", pT) as tPaperLaidTemp;

            var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateMinisterActivePaperId", PaperLaidId) as tPaperLaidTemp;

            return RedirectToAction("MinisterDashboard", "Minister", new { area = "Minister", message = "Sign completed" });
        }      
        #endregion      
    }
}
