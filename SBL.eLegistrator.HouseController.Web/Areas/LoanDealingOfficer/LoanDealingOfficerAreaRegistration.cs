﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.LoanDealingOfficer
{
    public class LoanDealingOfficerAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LoanDealingOfficer";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LoanDealingOfficer_default",
                "LoanDealingOfficer/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
