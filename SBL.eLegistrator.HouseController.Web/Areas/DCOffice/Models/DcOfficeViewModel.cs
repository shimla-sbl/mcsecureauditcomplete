﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.DCOffice.Models
{
    public class DcOfficeViewModel
    {
        public string server { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AssemblyName { get; set; }
        public string SessionName { get; set; }
    }
}