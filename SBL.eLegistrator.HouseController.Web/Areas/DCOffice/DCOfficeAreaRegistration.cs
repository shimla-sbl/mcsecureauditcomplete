﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.DCOffice
{
    public class DCOfficeAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DCOffice";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DCOffice_default",
                "DCOffice/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
