﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Areas.DCOffice.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.DCOffice.Controllers
{
    public class DCOfficeController : Controller
    {
        //
        // GET: /DCOffice/DCOffice/

        public ActionResult Index()
        {
            DcOfficeViewModel model = new DcOfficeViewModel();
            mSession Sessionmdl = new mSession();
            mAssembly Assembly = new mAssembly();
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            Sessionmdl.AssemblyID = siteSettingMod.AssemblyCode;
            Sessionmdl.SessionCode = siteSettingMod.SessionCode;
            Assembly.AssemblyID = siteSettingMod.AssemblyCode;
            var SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Sessionmdl);
            var AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", Assembly);
            model.SessionName = SessionName;
            model.AssemblyName = AssesmblyName;

            return View(model);
        }

        public ActionResult CreateConnection()
        {
            DcOfficeViewModel model = new DcOfficeViewModel();
            mSession Sessionmdl = new mSession();
            mAssembly Assembly = new mAssembly();
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            try
            {

                //if (string.IsNullOrEmpty(CurrentSession.UserID))
                //{
                //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                //}
                string dir = "/ConnectionFile" + "/";
                string Path = Server.MapPath(dir) + "/connectionString.txt";
                using (StreamReader sr = new StreamReader(Path))
                {
                    String line = sr.ReadToEnd();
                    string[] ConLine = line.Split(':');
                    model.server = ConLine[0];
                    model.UserName = ConLine[1];
                    model.Password = ConLine[2];
                }
                Sessionmdl.SessionCode = siteSettingMod.SessionCode;
                Sessionmdl.AssemblyID = siteSettingMod.AssemblyCode;
                Assembly.AssemblyID = siteSettingMod.AssemblyCode;
                var SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Sessionmdl);
                var AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", Assembly);
                model.SessionName = SessionName;
                model.AssemblyName = AssesmblyName;
                return PartialView("_ManageConnection", model);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                ViewBag.ErrorMessage = "Their is a Error While Creating DC Office";
                return PartialView("_ManageConnection", model);
            }
        }

        public ActionResult UpdateConnection(DcOfficeViewModel model)
        {
            try
            {
                string ConnectionValue = model.server + ":" + model.UserName + ":" + model.Password;
                string dir = "/ConnectionFile" + "/";
                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(dir));
                if (!Dir.Exists)
                {
                    Dir.Create();
                }

                string Path = Dir + "/connectionString.txt";
                System.IO.File.WriteAllText(Path, ConnectionValue);

                return RedirectToAction("CreateConnection");

            }
#pragma warning disable CS0168 // The variable 'ee' is declared but never used
            catch (Exception ee)
#pragma warning restore CS0168 // The variable 'ee' is declared but never used
            {

                ViewBag.ErrorMessage = "Their is a Error While Save DC Office Details";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "error";
                return RedirectToAction("CreateConnection");
            }
        }


    }
}
