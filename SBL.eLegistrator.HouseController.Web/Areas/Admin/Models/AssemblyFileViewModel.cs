﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.AssemblyFileSystem;
using System.Web.Mvc;
using SBL.DomainModel.Models.Event;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    [Serializable]
    public class AssemblyFileViewModel
    {
        public int VAssemblyFileId { get; set; }

        [Range(1, 300, ErrorMessage = "Please Select Assembly")]
        public int VAssemblyId { get; set; }

        [Range(1, 300, ErrorMessage = "Please Select SessionId")]

        public int VSessionId { get; set; }

        public int? VSessionDateId { get; set; }

        public int? VTypeofDocumentId { get; set; }


        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string VDescription { get; set; }

        public string VTitle { get; set; }


        public string VUploadFile { get; set; }

        public string VCreatedBy { get; set; }

        public DateTime? VCreatedDate { get; set; }

        public bool VStatus { get; set; }

        public int EventID { get; set; }

        public string EventName { get; set; }

        public DateTime? VModifiedDate { get; set; }

        public string VModifiedBy { get; set; }

        public string VAssemblyName { get; set; }

        public string VSessionName { get; set; }

        public string VSessionDate { get; set; }

        public string VAssemblyDocTypeName { get; set; }

        public string FileAcessPath { get; set; }

        public string Mode { get; set; }

        public List<mAssembly> AssemblyList { get; set; }

        public List<mSession> SessionList { get; set; }

        public List<mSessionDate> SessionDateList { get; set; }

        public List<mEvent> EventList { get; set; }

        public List<mAssemblyTypeofDocuments> AssemblyDocumentTypeList { get; set; }

        public SelectList StatusTypeList { get; set; }

        public bool IsCreatNew { get; set; }




    }
}