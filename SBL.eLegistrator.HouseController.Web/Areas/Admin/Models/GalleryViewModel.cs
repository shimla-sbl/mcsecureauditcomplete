﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class GalleryViewModel
    {
       
        public int GalleryId { get; set; }

       [Required(ErrorMessage = "Gallery Title is Required")]
        public string Title { get; set; }

        public string LocalTitle { get; set; }

        [AllowHtml]
        public string Descreption { get; set; }

        public string FullImage { get; set; }

        public string ThumbImage { get; set; }

        public bool IsActive { get; set; }
  
        public string Mode { get; set; }

        public SelectList TypeList { get; set; }

        public byte Status { get; set; }

        [Required(ErrorMessage = "Please Select Gallery Categroy")]
        public int CategoryId { get; set; }

        public DateTime CreatedOn { get; set; }

        public SelectList ImagesList { get; set; }

        public SelectList CategoryList { get; set; }

        public string FileLocation { get; set; }

        public string CategoryName { get; set; }

        public byte GalleryType { get; set; }

        [AllowHtml]
        public string HindiDescription { get; set; }

        public SelectList  GalleryTypeList { get; set; }

        public bool IsCreatNew { get; set; }
       // public DateTime Publish_date { get; set; }
        //public int CategoryID { get; set; }

        public string ImageLocation { get; set; }

        public int Hits { get; set; }
        public string ThumbName { get; set; }
        

    }
}