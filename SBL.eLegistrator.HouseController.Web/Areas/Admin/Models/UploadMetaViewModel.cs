﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Bill;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel;
using SBL.DomainModel.Models.Passes;
namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class UploadMetaViewModel
    {
        public UploadMetaViewModel()
        {
            this.AssemblyList = new List<mAssembly>();
            this.SessionList = new List<mSession>();
            this.SessionDateList = new List<mSessionDate>();
            this.DepartmentList = new List<mDepartment>();
            this.CommiteeList = new List<tCommittee>();
            this.PassCategoryList = new List<PassCategory>();

        }
        public int AssemblyId { get; set; }

        public int SessionId { get; set; }

        public string SessionDateId { get; set; }

        public string DocumentTypeId { get; set; }
        public string CommitteeId { get; set; }
        public string DepartmentId { get; set; }
        public string CommitteeTypeId { get; set; }
        public string BillId { get; set; }

        public string SpeechId { get; set; }
      
        public string SpeechType { get; set; }

        public string QuestionId { get; set; }

        public string PdfFile { get; set; }

        public string Image { get; set; }

        public DateTime CreatedDate { get; set; }

        public List<UploadMetaViewModel>  MetaDataList { get; set; }

        public List<mAssembly> AssemblyList { get; set; }

        public List<mSession> SessionList { get; set; }

        public List<mSessionDate> SessionDateList { get; set; }

        public List<mDepartment> DepartmentList { get; set; }
        public List<tCommittee> CommiteeList { get; set; }

        public List<PassCategory> PassCategoryList { get; set; }
        public List<mBills> BilltypeList { get; set; }
        public DateTime? SessionDate { get; set; }

        public List<tStarredQuestionsHistory> StarredQuestionsHistoryList { get; set; }
        public List<tUnStarredQuestionsHistory> UnStarredQuestionsHistoryList { get; set; }
        public List<ReporterDescriptionsLog> ReporterDescriptionsLogList { get; set; }

        //for pass count list
        public List<VisitorData> VisitorDataList { get; set; }
        public string PassCategoryID { get; set; }
        public string PassCategory { get; set; }
        public string AllowPassCount { get; set; }
        public string InvalidPassCount { get; set; }
    }
}