﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class CategoryViewModel
    {
        public Guid CreatedBy { get; set; }

        public Guid ModifiedBy { get; set; }

        public int CategoryID { get; set; }

        [Required(ErrorMessage="Category Name is Required")]
        public string CategoryName { get; set; }

        public string CategoryDescription { get; set; }

        public string LocalCategoryName { get; set; }

        public string LocalCategoryDescription { get; set; }

        //[Range(1,300,ErrorMessage="Please Select Category Type")]
        public int CategoryType { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public byte Status { get; set; }
      
        public string Mode { get; set; }

        public SelectList TypeList { get; set; }

        public SelectList ImagesList { get; set; }

        public SelectList CategoryTypeList { get; set; }

        public SelectList SpeechTypeList { get; set; }

        public bool IsCreatNew { get; set; }

        public string GalleryThumbImageUrl { get; set; }

        public int? GalleryImageId { get; set; }

        public SelectList GalleryImageList { get; set; }
        public SelectList Dropdownlist { get; set; }

        public int AlbumCategoryId { get; set; }

    }
}