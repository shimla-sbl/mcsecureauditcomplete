﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class NoticeViewModel
    {

        public int NoticeID { get; set; }

        //public Guid RoleId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ? Publish_date { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ? PublishEnd_date { get; set; }

        //[Required(ErrorMessage = "NoticeType is Required")]
        public string NoticeType { get; set; }

        [Required(ErrorMessage = "Notice Title is Required")]
        public string NoticeTitle { get; set; }

        public string LocalNoticeTitle { get; set; }

        //[Required(ErrorMessage = "Description is Required")]
        //[AllowHtml]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string NoticeDescription { get; set; }

        [AllowHtml]
        public string LocalNoticeDescription { get; set; }

       // [Required(ErrorMessage = "Image is Required")]
        public string FileLocation { get; set; }
       

        public DateTime ModifiedBy { get; set; }

        public DateTime ModifiedWhen { get; set; }


        //[Required(ErrorMessage = "Status is Required")]
        public byte Status { get; set; }

        //[Required(ErrorMessage = "Category is Required")]
        public string Category { get; set; }

        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Please Select Notice Categroy")]
        public int CategoryID { get; set; }


        public string Mode { get; set; }

        public SelectList TypeList { get; set; }

        public SelectList CategoryList { get; set; }

        public SelectList NoticeTypeList { get; set; }

        public bool IsCreatNew { get; set; }

        public string ImageLocation { get; set; }

        public int Hits { get; set; }

        public int AssemblyId { get; set; }

        public int SessionId { get; set; }

        public SelectList AssemblyList { get; set; }


        public SelectList DeptList { get; set; }

        public SelectList SessionList { get; set; }
        [Required(ErrorMessage = "Please Select Department")]
        public string DeptId
        {

            get;
            set;
        }
    }
}