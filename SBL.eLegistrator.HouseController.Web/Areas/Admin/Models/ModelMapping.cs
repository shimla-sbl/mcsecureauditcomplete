﻿using SBL.DomainModel.Models.AssemblyFileSystem;
using SBL.DomainModel.Models.SessionDateSignature;
using SBL.DomainModel.Models.Category;
using SBL.DomainModel.Models.Content;
using SBL.DomainModel.Models.Gallery;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.News;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Speech;
using SBL.DomainModel.Models.Tour;
using SBL.DomainModel.Models.ContactUs;
using SBL.DomainModel.Models.Forms;
using SBL.DomainModel.Models.FooterPublicData;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.AlbumCategory;
using SBL.DomainModel.Models.UploadMetaFile;
using SBL.DomainModel.Models.References;
using SBL.DomainModel.ComplexModel;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public static class ModelMapping
    {
        #region Gallery
        public static GalleryViewModel ToViewModel(this tGallery gallery, string Mode)
        {
            var ViewModel = new GalleryViewModel()
            {
                GalleryId = gallery.GalleryId,
                Title = gallery.GalleryTitle,
                Descreption = gallery.GalleryDescription,
                FullImage = gallery.FileName,
                ThumbImage = gallery.FileName,
                IsActive = true,
                Mode = Mode,
                FileLocation = gallery.FilePath,
                TypeList = GetEditStatusTypes(),
                Status = gallery.Status,
                GalleryType = gallery.GalleryType,
                GalleryTypeList = GetGalleryType(),
                //   CreatedOn = gallery.CreatedOn,
                CategoryId = gallery.CategoryID,
                LocalTitle = gallery.LocalGalleryTitle,
                HindiDescription = gallery.LocalGalleryDescription,
                Hits = gallery.Hits
                //Publish_date = gallery.Publish_date
            };
            return ViewModel;
        }
        public static IEnumerable<GalleryViewModel> ToViewModel(this IEnumerable<tGallery> galleryList)
        {
            var ViewModel = galleryList.Select(gallery => new GalleryViewModel()
            {
                GalleryId = gallery.GalleryId,
                Title = gallery.GalleryTitle,
                Descreption = gallery.GalleryDescription,
                FullImage = gallery.FileName,
                ThumbImage = gallery.FileName,
                IsActive = true,
                FileLocation = gallery.ImageAcessurl,
                Status = gallery.Status,
                // CreatedOn = gallery.CreatedOn,
                CategoryId = gallery.CategoryID,
                LocalTitle = gallery.LocalGalleryTitle,
                HindiDescription = gallery.LocalGalleryDescription,
                CreatedOn = gallery.CreatedDate,
                CategoryName = gallery.CategoryName,
                ImageLocation = gallery.ImageAcessurl,
                Hits = gallery.Hits
                // Publish_date = gallery.Publish_date

            });
            return ViewModel;
        }
        public static tGallery ToDomainModel(this GalleryViewModel gallery)
        {
            var DomainModel = new tGallery()
            {
                GalleryId = gallery.GalleryId,
                GalleryTitle = gallery.Title,
                GalleryDescription = gallery.Descreption,
                FileName = gallery.FullImage,
                GalleryType = gallery.GalleryType,
                FilePath = gallery.FileLocation,
                Status = gallery.Status,
                ThumbName=gallery.ThumbName,
                //  CreatedOn = gallery.CreatedOn,
                CategoryID = gallery.CategoryId,
                LocalGalleryDescription = gallery.HindiDescription,
                LocalGalleryTitle = gallery.LocalTitle,


                //Publish_date = gallery.Publish_date


            };
            return DomainModel;
        }
        #endregion

        #region News
        public static NewsViewModel ToViewModel(this mNews news, string Mode)
        {
            var ViewModel = new NewsViewModel()
            {
                LatestUpdateID = news.LatestNewsID,
                //Name=news.FileName,

                Date_Of_Event = news.CreatedDate,
                Publish_date = news.StartPublish,
                FullImage = news.FileName,
                ThumbImage = news.FileName,
                FileLocation = news.Images,

                FullImage1 = news.FileName1,
                ThumbImage1 = news.FileName1,
                FileLocation1 = news.Images1,


                News_Title = news.NewsTitle,
                News_Description = news.NewsDescription,
                CategoryName = news.CategoryTypeName,
                CategoryId = news.CategoryID,
                // IsActive = news.IsActive,
                Status = news.Status,
                //FilePath = news.Images,
                //FileLocation = gallery.FilePath,
                mode = Mode,
                TypeList = GetEditStatusTypes(),
                //FileName = news.FileName,
                HindiDescription = news.LocalNewsDescription,
                Local_News_Title = news.LocalNewsTitle,
                NewsEventDate = news.NewsEventDate != null ? news.NewsEventDate.Value.ToString("dd/MM/yyyy") : "",
                Hits = news.Hits,
                isFlashNews=news.isFlashNews

            };
            return ViewModel;
        }

        public static IEnumerable<NewsViewModel> ToViewModel(this IEnumerable<mNews> newsList)
        {
            var ViewModel = newsList.Select(n => new NewsViewModel()
            {
                LatestUpdateID = n.LatestNewsID,

                Date_Of_Event = n.CreatedDate,
                Publish_date = n.StartPublish,
                FullImage = n.FileName,
                ThumbImage = n.FileName,
                ImageLocation = n.ImageAcessurl,
                FileLocation = n.ImageAcessurl,

                FullImage1 = n.FileName1,
                ThumbImage1 = n.FileName1,
                ImageLocation1 = n.ImageAcessurl1,
                FileLocation1 = n.ImageAcessurl1,


                News_Title = n.NewsTitle,
                News_Description = n.NewsDescription,
                CategoryName = n.CategoryTypeName,
                CategoryId = n.CategoryID,
                FilePath = n.Images,

                FilePath1 = n.Images1,

                Status = n.Status,
                //FileName = n.FileName,
                HindiDescription = n.LocalNewsDescription,
                Local_News_Title = n.LocalNewsTitle,
                NewsEventDate = n.NewsEventDate != null ? n.NewsEventDate.Value.ToString("dd/MM/yyyy") : "",
                Hits = n.Hits,
                isFlashNews = n.isFlashNews




            });
            return ViewModel;
        }

        public static mNews ToDomainModel(this NewsViewModel news)
        {
            var DomainModel = new mNews()
            {
                LatestNewsID = news.LatestUpdateID,
                CreatedDate = news.Date_Of_Event,
                StartPublish = news.Publish_date,
                NewsTitle = news.News_Title,
                NewsDescription = news.News_Description,
                CategoryID = news.CategoryId,
                CreatedBy = Guid.Parse(CurrentSession.UserID),
                ModifiedBy = Guid.Parse(CurrentSession.UserID),
                Status = news.Status,
                Images = news.FileLocation,

                Images1 = news.FileLocation1,

                //FileName = news.FileName,
                LocalNewsDescription = news.HindiDescription,
                LocalNewsTitle = news.Local_News_Title,
                NewsEventDate = string.IsNullOrEmpty(news.NewsEventDate) ? DateTime.Now : DateTime.ParseExact(news.NewsEventDate, "dd/MM/yyyy", null),



                FileName = news.FullImage,
                ThumbName = news.ThumbName,


                FileName1 = news.FullImage1,
                ThumbName1 = news.ThumbName1,
                isFlashNews = news.isFlashNews

            };
            return DomainModel;
        }
        #endregion


        #region Notice
        public static NoticeViewModel ToViewModel1(this mNotice notice, string Mode)
        {
            var ViewModel = new NoticeViewModel()
            {
                NoticeID = notice.NoticeID,
                AssemblyId = notice.AssemblyId,
                SessionId = notice.SessionId,
                Publish_date = notice.StartPublish,
                NoticeTitle = notice.NoticeTitle,
                NoticeDescription = notice.NoticeDescription,
                FileLocation = notice.Attachments,
                Status = notice.Status,
                CategoryID = notice.CategoryID,
                Mode = Mode,
                TypeList = GetEditStatusTypes(),
                LocalNoticeDescription = notice.LocalNoticeDescription,
                LocalNoticeTitle = notice.LocalNoticeTitle,
                Hits = notice.Hits
            };
            return ViewModel;
        }

        public static IEnumerable<NoticeViewModel> ToViewModel1(this IEnumerable<mNotice> noticeList)
        {
            var ViewModel = noticeList.Select(notice => new NoticeViewModel()
            {
                NoticeID = notice.NoticeID,
                Publish_date = notice.StartPublish,
                NoticeTitle = notice.NoticeTitle,
                NoticeDescription = notice.NoticeDescription,
                Category = notice.CategoryName,
                FileLocation = notice.Attachments,
                Status = notice.Status,
                CategoryID = notice.CategoryID,
                LocalNoticeDescription = notice.LocalNoticeDescription,
                LocalNoticeTitle = notice.LocalNoticeTitle,
                Hits = notice.Hits
                // IsActive = notice.IsActive,
            });
            return ViewModel;
        }

        public static mNotice ToDomainModel(this NoticeViewModel notice)
        {
            var DomainModel = new mNotice()
            {
                NoticeID = notice.NoticeID,
                AssemblyId = notice.AssemblyId,
                SessionId = notice.SessionId,
                //  NoticeType = notice.NoticeType,
                StartPublish =Convert.ToDateTime( notice.Publish_date),
                StopPublish = Convert.ToDateTime(notice.PublishEnd_date),
                NoticeTitle = notice.NoticeTitle,
                NoticeDescription = notice.NoticeDescription,
                Attachments = notice.FileLocation,
                Status = notice.Status,
                CategoryID = notice.CategoryID,
                LocalNoticeTitle = notice.LocalNoticeTitle,
                LocalNoticeDescription = notice.LocalNoticeDescription,
                deptId=notice.DeptId
                // IsActive = notice.IsActive,
            };
            return DomainModel;
        }

        #endregion

        #region Category
        public static CategoryViewModel ToViewModel(this Category Category, string Mode)
        {
            var ViewModel = new CategoryViewModel()
            {
                AlbumCategoryId=Category.AlbumCategoryId,
                CategoryID = Category.CategoryID,
                CategoryName = Category.CategoryName,
                CategoryDescription = Category.CategoryDescription,
                CategoryType = Category.CategoryType,
                CreatedDate = Category.CreatedDate,
                ModifiedDate = Category.ModifiedDate,
                Mode = Mode,
                Status = Category.Status,
                TypeList = GetEditStatusTypes(),
                CategoryTypeList = GetCategoryTypes(),
                LocalCategoryDescription = Category.LocalCategoryDescription,
                LocalCategoryName = Category.LocalCategoryName
                

            };
            return ViewModel;
        }
        public static IEnumerable<CategoryViewModel> ToViewModel(this IEnumerable<Category> galleryList)
        {
            var ViewModel = galleryList.Select(Category => new CategoryViewModel()
            {
                CategoryID = Category.CategoryID,
                CategoryName = Category.CategoryName,
                CategoryDescription = Category.CategoryDescription != null ? Category.CategoryDescription.Length > 100 ? Category.CategoryDescription.Substring(0, 100) + ".." : Category.CategoryDescription : null,
                CategoryType = Category.CategoryType,
                CreatedDate = Category.CreatedDate,
                ModifiedDate = Category.ModifiedDate,
                Status = Category.Status,
                LocalCategoryDescription = Category.LocalCategoryDescription != null ? Category.LocalCategoryDescription.Length > 100 ? Category.LocalCategoryDescription.Substring(0, 100) + ".." : Category.LocalCategoryDescription : null,
                LocalCategoryName = Category.LocalCategoryName,
                GalleryImageId = Category.GalleryId,
                GalleryThumbImageUrl = Category.GalleryThmImgUrl
                //  Status = Category.Status,

            });
            return ViewModel;
        }
        public static Category ToDomainModel(this CategoryViewModel Category)
        {
            var DomainModel = new Category()
            {
                AlbumCategoryId=Category.AlbumCategoryId,
                CategoryID = Category.CategoryID,
                CategoryName = Category.CategoryName,
                CategoryDescription = Category.CategoryDescription,
                CategoryType = Category.CategoryType,
                CreatedDate = Category.CreatedDate,
                ModifiedDate = Category.ModifiedDate,
                Status = Category.Status,
                CreatedBy = Category.CreatedBy,
                ModifiedBy = Category.ModifiedBy,
                LocalCategoryDescription = Category.LocalCategoryDescription,
                LocalCategoryName = Category.LocalCategoryName

            };
            return DomainModel;
        }
        #endregion


        public static SelectList GetStatusTypes()
        {
            var result = new List<SelectListItem>();
            // result.Add(new SelectListItem() { Text = "Select Status Type", Value = "0" });
            result.Add(new SelectListItem() { Text = "Published", Value = "1" });
            result.Add(new SelectListItem() { Text = "Unpublished ", Value = "0" });
            //  result.Add(new SelectListItem() { Text = "Archived", Value = "3" });
            //  result.Add(new SelectListItem() { Text = "Trashed", Value = "4" });


            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetEditStatusTypes()
        {
            var result = new List<SelectListItem>();
            //  result.Add(new SelectListItem() { Text = "Select Status Type", Value = "0" });
            result.Add(new SelectListItem() { Text = "Published", Value = "1" });
            result.Add(new SelectListItem() { Text = "Unpublished ", Value = "0" });
            result.Add(new SelectListItem() { Text = "Archived", Value = "2" });
            result.Add(new SelectListItem() { Text = "Trashed", Value = "-2" });


            return new SelectList(result, "Value", "Text");
        }


        public static SelectList GetCategoryTypes()
        {
            var result = new List<SelectListItem>();
            result.Add(new SelectListItem() { Text = "Select Category Type", Value = "0" });
            result.Add(new SelectListItem() { Text = "News", Value = "1" });
            result.Add(new SelectListItem() { Text = "Notices ", Value = "2" });
            result.Add(new SelectListItem() { Text = "Gallery", Value = "3" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetGalleryImages()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "picture1.lpg", Value = "picture1.lpg" });
            result.Add(new SelectListItem() { Text = "picture2.lpg ", Value = "picture2.lpg" });


            return new SelectList(result, "Value", "Text");
        }



        public static SelectList GetNewsCategory()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Industries", Value = "Industries" });
            result.Add(new SelectListItem() { Text = "Labour and Employment ", Value = "Labour and Employment" });
            result.Add(new SelectListItem() { Text = "eVidhan ", Value = "eVidhan" });


            return new SelectList(result, "Value", "Text");
        }
        public static SelectList GetNoticeType()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Excel", Value = "Excel" });
            result.Add(new SelectListItem() { Text = "PDF ", Value = "PDF" });
            result.Add(new SelectListItem() { Text = "Others ", Value = "Others" });


            return new SelectList(result, "Value", "Text");
        }
        public static SelectList GetGalleryType()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Image", Value = "1" });
            result.Add(new SelectListItem() { Text = "Video ", Value = "2" });


            return new SelectList(result, "Value", "Text");
        }


        #region Speech


        public static SelectList GetSpeechType()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Audio", Value = "0" });
            result.Add(new SelectListItem() { Text = "Video ", Value = "1" });
            result.Add(new SelectListItem() { Text = "Image ", Value = "2" });
            result.Add(new SelectListItem() { Text = "Pdf ", Value = "3" });
            result.Add(new SelectListItem() { Text = "doc ", Value = "4" });

            return new SelectList(result, "Value", "Text");
        }

        public static tSpeech ToDomainModel(this SpeechViewModel speech)
        {
            var DomainModel = new tSpeech()
            {
                SpeechID = speech.VSpeechID,
                StartPublish = speech.VStartPublish,
                SpeechTitle = speech.VSpeechTitle,
                SpeechDescription = speech.VSpeechDescription,
                FilePath = speech.VFilePath,
                FileName = speech.VFileName,
                FilePath1 = speech.VFilePath1,
                FileName1 = speech.VFileName1,
                Status = speech.VStatus,
                CategoryID = speech.VCategoryId,
                LocalSpeechDescription = speech.VLocalSpeechDescription,
                SpeechType = speech.VSpeechType,
                LocalSpeechTitle = speech.VLocalSpeechTitle,
                SessionID = speech.VSessionId,
                AssemblyId = speech.VAssemblyId,
                Date = speech.VDate,
                SessionDateId = speech.VSessionDateId
               

            };
            return DomainModel;
        }


        public static IEnumerable<SpeechViewModel> ToViewModel1(this IEnumerable<tSpeech> speech)
        {

            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            var file = fileAcessingSettings.SettingValue;
            var ViewModel = speech.Select(speechItem => new SpeechViewModel()
            {
                VSpeechID = speechItem.SpeechID,
                VStartPublish = speechItem.StartPublish,
                VSpeechTitle = speechItem.SpeechTitle,
                VSpeechDescription = speechItem.SpeechDescription,
                VFilePath = speechItem.FilePath,
                VFilePath1 = speechItem.FilePath1,
                VFileName = speechItem.FileName,
                VFileName1 = speechItem.FileName1,
                VStatus = speechItem.Status,
                VCategoryId = speechItem.CategoryID,
                VLocalSpeechDescription = speechItem.LocalSpeechDescription,
                VSpeechType = speechItem.SpeechType,
                VLocalSpeechTitle = speechItem.LocalSpeechTitle,
                CategoryName = speechItem.CategoryName,
                VHits = speechItem.Hits,
                VSessionId = speechItem.SessionID,
                VAssemblyId = speechItem.AssemblyId,
                VDate = speechItem.Date,
                VSessionDateId = speechItem.SessionDateId,
                VSessionName = speechItem.SessionName,
                VAssemblyName = speechItem.AssemblyName,
                VSessionDate = speechItem.SessionDate,
                ImageLocation = file,
                ImageLocation1 = file,

            });
            return ViewModel;
        }

        public static SpeechViewModel ToViewModel1(this tSpeech speech, string Mode)
        {
            var ViewModel = new SpeechViewModel()
            {
                VSpeechID = speech.SpeechID,
                VStartPublish = speech.StartPublish,
                VSpeechTitle = speech.SpeechTitle,
                VSpeechDescription = speech.SpeechDescription,
                VFilePath = speech.FilePath,
                VFilePath1 = speech.FilePath1,
                VSpeechType = speech.SpeechType,
                VCategoryId = speech.CategoryID,
                Mode = Mode,
                VStatus = speech.Status,
                VLocalSpeechTitle = speech.LocalSpeechTitle,
                VLocalSpeechDescription = speech.LocalSpeechDescription,
                SpeechTypeList = GetSpeechType(),
                StatusTypeList = GetEditStatusTypes(),
                CategoryName = speech.CategoryName,
                VFileName = speech.FileName,
                VFileName1 = speech.FileName1,
                VHits = speech.Hits,
                VSessionId = speech.SessionID,
                VAssemblyId = speech.AssemblyId,
                VDate = speech.Date,
                VSessionDateId = speech.SessionDateId,
                VSessionName = speech.SessionName,
                VAssemblyName = speech.AssemblyName,
                VSessionDate = speech.SessionDate,
            };
            return ViewModel;
        }

        #endregion

        #region Content

        public static Content ToDomainModel(this ContentViewModel content)
        {
            var DomainModel = new Content()
            {
                ContentID = content.VContentID,
                StartPublish = content.VStartPublish,
                ContentTitle = content.VContentTitle,
                ContentDescription = content.VContentDescription,
                Status = content.VStatus,
                CategoryID = content.VCategoryID,
                ContentLocalDescription = content.VContentLocalDescription,
                ContentLocalTitle = content.VContentLocalTitle
            };
            return DomainModel;
        }


        public static IEnumerable<ContentViewModel> ToViewModel1(this IEnumerable<Content> content)
        {
            var ViewModel = content.Select(contentItem => new ContentViewModel()
            {
                VContentID = contentItem.ContentID,
                VCreatedDate = contentItem.CreatedDate,
                VContentTitle = contentItem.ContentTitle,
                VContentDescription = contentItem.ContentDescription,
                VStatus = contentItem.Status,
                VCategoryID = contentItem.CategoryID,
                VContentLocalDescription = contentItem.ContentLocalDescription,
                VContentLocalTitle = contentItem.ContentLocalTitle,
                VCategoryTypeName = contentItem.CategoryTypeName,
                VHits = contentItem.Hits
            });
            return ViewModel;
        }

        public static ContentViewModel ToViewModel1(this Content contentItem, string Mode)
        {
            var ViewModel = new ContentViewModel()
            {
                VContentID = contentItem.ContentID,
                VStartPublish = contentItem.StartPublish,
                VContentTitle = contentItem.ContentTitle,
                VContentDescription = contentItem.ContentDescription,
                VStatus = contentItem.Status,
                VCategoryID = contentItem.CategoryID,
                VContentLocalDescription = contentItem.ContentLocalDescription,
                VContentLocalTitle = contentItem.ContentLocalTitle,
                VCategoryTypeName = contentItem.CategoryTypeName,
                StatusTypeList = GetEditStatusTypes(),
                VHits = contentItem.Hits

            };
            return ViewModel;
        }



        #endregion


        #region Tour


        public static tMemberTour ToDomainModel(this TourViewModel tour)
        {
            // IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);
            var DomainModel = new tMemberTour()
            {
                TourId = tour.TourId,
                Attachement = tour.Attachement,
                AliasAttachment = tour.AliasAttachment,
                LocalTourTitle = tour.LocalTourTitle,
                TourDescrption = tour.TourDescrption,
                LocalTourDescription = tour.LocalTourDescription,
                TourTitle = tour.TourTitle,
                TourFromDateTime = !string.IsNullOrEmpty(tour.TourFromDateTime) ? DateTime.ParseExact(tour.TourFromDateTime, "dd/MM/yyyy", null) : default(DateTime),   //Convert.ToDateTime(tour.VTourFromDateTime),
                TourToDateTime = !string.IsNullOrEmpty(tour.TourToDateTime) ? DateTime.ParseExact(tour.TourToDateTime, "dd/MM/yyyy", null) : default(DateTime),
                CreatedBy = tour.CreatedBy,
                MemberId = tour.MemberId,
                //TourLocation = tour.VTourLocation,
                AssemblyId = tour.AssemblyId,
                mode = tour.mode,
                Purpose = tour.Purpose,
                Departure = tour.Departure,
                Arrival = tour.Arrival,
                DepartureTime = tour.DepartureTime,
                ArrivalTime = tour.ArrivalTime,
                IsPublished = tour.IsPublished,
                LastUpdatedDate = tour.LastUpdatedDate,
                isLocale = tour.isLocale
                //NotifyDate = tour.NotifyDate,
                //NotifyTime = tour.NotifyTime
            };
            return DomainModel;
        }


        public static IEnumerable<TourViewModel> ToViewModel1(this IEnumerable<tMemberTour> tour)
        {
            var ViewModel = tour.Select(tourItem => new TourViewModel()
            {
                TourId = tourItem.TourId,
                Attachement = tourItem.Attachement,
                LocalTourTitle = tourItem.LocalTourTitle,
                TourDescrption = tourItem.TourDescrption,
                LocalTourDescription = tourItem.LocalTourDescription,
                TourTitle = tourItem.TourTitle,
                TourFromDateTime = tourItem.TourFromDateTime != null ? tourItem.TourFromDateTime.ToString("dd/MM/yyyy") : "",
                TourToDateTime = tourItem.TourToDateTime != null ? tourItem.TourToDateTime.ToString("dd/MM/yyyy") : "",
                CreatedBy = tourItem.CreatedBy,
                MemberId = tourItem.MemberId,
                //VTourLocation = tourItem.TourLocation,
                AssemblyId = tourItem.AssemblyId,
                Hits = tourItem.Hits,
                MemberName = tourItem.MemberName,
                mode = tourItem.mode,
                Purpose = tourItem.Purpose,
                EditMode = tourItem.EditMode,
                Departure = tourItem.Departure,
                Arrival = tourItem.Arrival,
                DepartureTime = tourItem.DepartureTime,
                ArrivalTime = tourItem.ArrivalTime,
                IsPublished = tourItem.IsPublished,
                LastUpdatedDate = tourItem.LastUpdatedDate
            });
            return ViewModel;
        }

        public static TourViewModel ToViewModel1(this tMemberTour tourItem, string Mode)
        {
            var ViewModel = new TourViewModel()
            {
                TourId = tourItem.TourId,
                Attachement = tourItem.Attachement,
                AliasAttachment = tourItem.AliasAttachment,
                LocalTourTitle = tourItem.LocalTourTitle,
                TourDescrption = tourItem.TourDescrption,
                LocalTourDescription = tourItem.LocalTourDescription,
                TourTitle = tourItem.TourTitle,
                TourFromDateTime = tourItem.TourFromDateTime != null ? tourItem.TourFromDateTime.ToString("dd/MM/yyyy") : "",
                TourToDateTime = tourItem.TourToDateTime != null ? tourItem.TourToDateTime.ToString("dd/MM/yyyy") : "",
                CreatedBy = tourItem.CreatedBy,
                MemberId = tourItem.MemberId,
                //VTourLocation = tourItem.TourLocation,
                AssemblyId = tourItem.AssemblyId,
                Hits = tourItem.Hits,
                mode = tourItem.mode,
                Purpose = tourItem.Purpose,
                EditMode = Mode,
                Departure = tourItem.Departure,
                Arrival = tourItem.Arrival,
                DepartureTime = tourItem.DepartureTime,
                ArrivalTime = tourItem.ArrivalTime,
                IsPublished = tourItem.IsPublished,
                LastUpdatedDate = tourItem.LastUpdatedDate
            };
            return ViewModel;
        }

        #endregion

        #region AssmeblyFile

        public static tAssemblyFiles ToDomainModel(this AssemblyFileViewModel assemblyFile)
        {
            var DomainModel = new tAssemblyFiles()
            {
                AssemblyFileId = assemblyFile.VAssemblyFileId,
                UploadFile = assemblyFile.VUploadFile,
                SessionDateId = assemblyFile.VSessionDateId,
                SessionId = assemblyFile.VSessionId,
                Description = assemblyFile.VDescription,
                Status = assemblyFile.VStatus,
                TypeofDocumentId = assemblyFile.VTypeofDocumentId,
                CreatedBy = assemblyFile.VCreatedBy,
                AssemblyId = assemblyFile.VAssemblyId
            };
            return DomainModel;
        }


        public static List<AssemblyFileViewModel> ToViewModel1(this IEnumerable<tAssemblyFiles> tour)
        {
            var ViewModel = tour.Select(assemblyFile => new AssemblyFileViewModel()
            {
                VAssemblyFileId = assemblyFile.AssemblyFileId,
                VUploadFile = assemblyFile.UploadFile,
                VSessionDateId = assemblyFile.SessionDateId,
                VSessionId = assemblyFile.SessionId,
                VDescription = assemblyFile.Description,
                VStatus = assemblyFile.Status,
                VTypeofDocumentId = assemblyFile.TypeofDocumentId,
                VCreatedBy = assemblyFile.CreatedBy,
                VAssemblyId = assemblyFile.AssemblyId,
                VSessionName = assemblyFile.SessionName,
                VSessionDate = assemblyFile.SessionDate,
                VAssemblyName = assemblyFile.AssemblyName,
                VAssemblyDocTypeName = assemblyFile.AssemblyDocTypeName,
                FileAcessPath = assemblyFile.FileAcessPath
            });
            return ViewModel.ToList();
        }

        public static AssemblyFileViewModel ToViewModel1(this tAssemblyFiles assemblyFile, string Mode)
        {
            var ViewModel = new AssemblyFileViewModel()
            {
                VAssemblyFileId = assemblyFile.AssemblyFileId,
                VUploadFile = assemblyFile.UploadFile,
                VSessionDateId = assemblyFile.SessionDateId,
                VSessionId = assemblyFile.SessionId,
                VDescription = assemblyFile.Description,
                VStatus = assemblyFile.Status,
                VTypeofDocumentId = assemblyFile.TypeofDocumentId,
                VCreatedBy = assemblyFile.CreatedBy,
                VAssemblyId = assemblyFile.AssemblyId,
                VSessionName = assemblyFile.SessionName,
                VSessionDate = assemblyFile.SessionDate,
                VAssemblyName = assemblyFile.AssemblyName,
                VAssemblyDocTypeName = assemblyFile.AssemblyDocTypeName,
                Mode = Mode,
                FileAcessPath = assemblyFile.FileAcessPath
            };
            return ViewModel;
        }



        #endregion

        #region MemberDetails

        public static mMember ToDomainModel(this MemberDetailsModel memberDetails)
        {
            // IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);
            var DomainModel = new mMember()
            {
                MemberCode = memberDetails.MemberCode,
                MemberID = memberDetails.MemberId,
                Description = memberDetails.Details,
                Email = memberDetails.Email,
                FatherName = memberDetails.FatherName,
                District = memberDetails.DistrictName,
                Name = memberDetails.Name,
                Mobile = memberDetails.Mobile,
                PermanentAddress = memberDetails.PermanentAddress,
                TelResidence = memberDetails.TelePhoneResidenc,
                TelOffice = memberDetails.TelePhoneOffice,
                ShimlaAddress = memberDetails.ShimlaAddress,
                Prefix = memberDetails.PreFix,
                NameLocal = memberDetails.NameLocal
            };
            return DomainModel;
        }


        public static MemberDetailsModel ToViewModel1(this mMember memberDetails, string Mode)
        {
            var DomainModel = new MemberDetailsModel()
            {
                MemberCode = memberDetails.MemberCode,
                MemberId = memberDetails.MemberID,
                Details = memberDetails.Description,
                Email = memberDetails.Email,
                FatherName = memberDetails.FatherName,
                DistrictName = memberDetails.District,
                Name = memberDetails.Name,
                Mobile = memberDetails.Mobile,
                PermanentAddress = memberDetails.PermanentAddress,
                TelePhoneResidenc = memberDetails.TelResidence,
                TelePhoneOffice = memberDetails.TelOffice,
                ShimlaAddress = memberDetails.ShimlaAddress,
                PreFix = memberDetails.Prefix,
                NameLocal = memberDetails.NameLocal
            };
            return DomainModel;

        }


        public static IEnumerable<MemberDetailsModel> ToViewModel1(this IEnumerable<mMember> tour)
        {
            var ViewModel = tour.Select(memberDetails => new MemberDetailsModel()
            {
                MemberCode = memberDetails.MemberCode,
                MemberId = memberDetails.MemberID,
                Details = memberDetails.Description,
                Email = memberDetails.Email,
                FatherName = memberDetails.FatherName,
                DistrictName = memberDetails.District,
                Name = memberDetails.Name,
                Mobile = memberDetails.Mobile,
                PermanentAddress = memberDetails.PermanentAddress,
                TelePhoneResidenc = memberDetails.TelResidence,
                TelePhoneOffice = memberDetails.TelOffice,
                ShimlaAddress = memberDetails.ShimlaAddress,
                PreFix = memberDetails.Prefix,
                NameLocal = memberDetails.NameLocal

            });
            return ViewModel;
        }



        #endregion

        public static AlbumCategory AlbumCategoryDomainModel(this AlbumCategoryViewModel Category)
        {
            var DomainModel = new AlbumCategory()
            {
                AlbumCategoryID = Category.AlbumCategoryID,
                CategoryName = Category.CategoryName,
                CategoryDescription = Category.CategoryDescription,
                CreatedDate = Category.CreatedDate,
                ModifiedDate = Category.ModifiedDate,
                Mode = Category.Mode,
                IsActive = Category.IsActive,
                MemberCode=Convert.ToInt32(Category.UserId)

            };
            return DomainModel;
        }
        public static IEnumerable<AlbumCategoryViewModel> AlbumCategoryViewModel(this IEnumerable<AlbumCategory> galleryList)
        {
            var ViewModel = galleryList.Select(Category => new AlbumCategoryViewModel()
            {
                AlbumCategoryID = Category.AlbumCategoryID,
                CategoryName = Category.CategoryName,
                CategoryDescription = Category.CategoryDescription != null ? Category.CategoryDescription.Length > 100 ? Category.CategoryDescription.Substring(0, 100) + ".." : Category.CategoryDescription : null,
                IsActive = Category.IsActive,
                CreatedDate = Category.CreatedDate,
                ModifiedDate = Category.ModifiedDate,
                MemberCode=Category.MemberCode


                //  Status = Category.Status,

            });
            return ViewModel;

        }
        public static AlbumCategoryViewModel ToEditAlbumCategoryViewModel(this AlbumCategory Category, string Mode)
        {
            var ViewModel = new AlbumCategoryViewModel()
            {
                AlbumCategoryID=Category.AlbumCategoryID,
                CategoryName = Category.CategoryName,
                CategoryDescription = Category.CategoryDescription,
                
                CreatedDate = Category.CreatedDate,
                ModifiedDate = Category.ModifiedDate,
                Mode = Mode,
                MemberCode=Category.MemberCode
               
            };
            return ViewModel;
        }


        #region SessionDateSignature

        public static tSessionDateSignature ToDomainModel(this SessionDateSignatureViewModel SessionDateSignature)
        {
            var DomainModel = new tSessionDateSignature()
            {
                SessionDateSignatureDetailsId = SessionDateSignature.SessionDateSignatureDetailsId,
                AssemblyId = SessionDateSignature.AssemblyId,
                SessionDateId = SessionDateSignature.SessionDateId,
                SessionId = SessionDateSignature.SessionId,
                SignatureName = SessionDateSignature.SignatureName,
                SignatureNameLocal = SessionDateSignature.SignatureNameLocal,
                HeaderText = SessionDateSignature.HeaderText,
                HeaderTextLocal = SessionDateSignature.HeaderTextLocal,
                SignaturePlace = SessionDateSignature.SignaturePlace,
                SignaturePlaceLocal = SessionDateSignature.SignaturePlaceLocal,
                SignatureDate = SessionDateSignature.SignatureDate,
                SignatureDateLocal = SessionDateSignature.SignatureDateLocal,
                SignatureDesignations = SessionDateSignature.SignatureDesignations,
                SignatureDesignationsLocal = SessionDateSignature.SignatureDesignationsLocal,
                IsActive = SessionDateSignature.IsActive,
                CreatedBy = SessionDateSignature.CreatedBy,
                ModifiedBy = SessionDateSignature.ModifiedBy



            };
            return DomainModel;
        }


        public static IEnumerable<SessionDateSignatureViewModel> ToViewModel1(this IEnumerable<tSessionDateSignature> tour)
        {
            var ViewModel = tour.Select(model => new SessionDateSignatureViewModel()
            {
                SessionDateSignatureDetailsId = model.SessionDateSignatureDetailsId,

                SessionDateId = model.SessionDateId,
                SessionId = model.SessionId,
                AssemblyId = model.AssemblyId,
                SessionName = model.SessionName,
                SessionDate = model.SessionDate,
                AssemblyName = model.AssemblyName,
                SignatureName = model.SignatureName,
                SignatureNameLocal = model.SignatureNameLocal,
                HeaderText = model.HeaderText,
                HeaderTextLocal = model.HeaderTextLocal,
                SignaturePlace = model.SignaturePlace,
                SignaturePlaceLocal = model.SignaturePlaceLocal,
                SignatureDate = model.SignatureDate,
                SignatureDateLocal = model.SignatureDateLocal,
                SignatureDesignations = model.SignatureDesignations,
                SignatureDesignationsLocal = model.SignatureDesignationsLocal,
                IsActive = model.IsActive ?? false,
                CreatedBy = model.CreatedBy,
                ModifiedBy = model.ModifiedBy

            });
            return ViewModel;
        }

        public static SessionDateSignatureViewModel ToViewModel1(this tSessionDateSignature model, string Mode)
        {
            var ViewModel = new SessionDateSignatureViewModel()
            {
                SessionDateSignatureDetailsId = model.SessionDateSignatureDetailsId,

                SessionDateId = model.SessionDateId,
                SessionId = model.SessionId,
                AssemblyId = model.AssemblyId,
                SessionName = model.SessionName,
                SessionDate = model.SessionDate,
                AssemblyName = model.AssemblyName,
                SignatureName = model.SignatureName,
                SignatureNameLocal = model.SignatureNameLocal,
                HeaderText = model.HeaderText,
                HeaderTextLocal = model.HeaderTextLocal,
                SignaturePlace = model.SignaturePlace,
                SignaturePlaceLocal = model.SignaturePlaceLocal,
                SignatureDate = model.SignatureDate,
                SignatureDateLocal = model.SignatureDateLocal,
                SignatureDesignations = model.SignatureDesignations,
                SignatureDesignationsLocal = model.SignatureDesignationsLocal,
                IsActive = model.IsActive ?? false,

                Mode = Mode

            };
            return ViewModel;
        }



        #endregion



        public static List<FeedbackViewModel> ToViewModel(this IEnumerable<ContactUs> model)
        {

            var ViewModel = model.Select(part => new FeedbackViewModel()
            {
                VId = part.Id,
                VName = part.Name,
                VEmail = part.Email,
                VMobile = part.Mobile,
                VMessage = part.Message,
                VType=part.FeedBackType,
                VDate = part.Date != null ? part.Date.Value.ToString("dd/MM/yyyy") : "",
                

            });
            return ViewModel.ToList();
        }




        #region Forms

        public static tForms ToDomainModel(this FormsViewModel assemblyFile)
        {
            var DomainModel = new tForms()
            {
                FormId = assemblyFile.VFormId,
                UploadFile = assemblyFile.VUploadFile,
                //SessionDateId = assemblyFile.VSessionDateId,
                //SessionId = assemblyFile.VSessionId,
                Description = assemblyFile.VDescription,
                Status = assemblyFile.VStatus,
                TypeofFormId = assemblyFile.VTypeofFormId,
                CreatedBy = assemblyFile.VCreatedBy,
                AssemblyId = assemblyFile.VAssemblyId
            };
            return DomainModel;
        }


        public static IEnumerable<FormsViewModel> ToViewModel1(this IEnumerable<tForms> tour)
        {
            var ViewModel = tour.Select(assemblyFile => new FormsViewModel()
            {
                VFormId = assemblyFile.FormId,
                VUploadFile = assemblyFile.UploadFile,
                //VSessionDateId = assemblyFile.SessionDateId,
                //VSessionId = assemblyFile.SessionId,
                VDescription = assemblyFile.Description,
                VStatus = assemblyFile.Status,
                VTypeofFormId = assemblyFile.TypeofFormId,
                VCreatedBy = assemblyFile.CreatedBy,
                VAssemblyId = assemblyFile.AssemblyId,
                //VSessionName = assemblyFile.SessionName,
                //VSessionDate = assemblyFile.SessionDate,
                VAssemblyName = assemblyFile.AssemblyName,
                VFormDocTypeName = assemblyFile.FormDocTypeName,
                FileAcessPath = assemblyFile.FileAcessPath
            });
            return ViewModel;
        }

        public static FormsViewModel ToViewModel1(this tForms assemblyFile, string Mode)
        {
            var ViewModel = new FormsViewModel()
            {
                VFormId = assemblyFile.FormId,
                VUploadFile = assemblyFile.UploadFile,
                //VSessionDateId = assemblyFile.SessionDateId,
                //VSessionId = assemblyFile.SessionId,
                VDescription = assemblyFile.Description,
                VStatus = assemblyFile.Status,
                VTypeofFormId = assemblyFile.TypeofFormId,
                VCreatedBy = assemblyFile.CreatedBy,
                VAssemblyId = assemblyFile.AssemblyId,
                //VSessionName = assemblyFile.SessionName,
                //VSessionDate = assemblyFile.SessionDate,
                VAssemblyName = assemblyFile.AssemblyName,
                VFormDocTypeName = assemblyFile.FormDocTypeName,
                Mode = Mode,
                FileAcessPath = assemblyFile.FileAcessPath
            };
            return ViewModel;
        }



        #endregion


        #region FooterPublicData

        public static FooterPublicData ToDomainModel(this FooterPublicDataViewModel model)
        {
            return new FooterPublicData
            {

                ID = model.VID,
                TitleID = model.VTitleID,
                TitleName = model.VTitleName,
                TitleDescription = model.VTitleDescription


            };
        }



        public static IEnumerable<FooterPublicDataViewModel> ToViewModel(this IEnumerable<FooterPublicData> model)
        {

            var ViewModel = model.Select(part => new FooterPublicDataViewModel()
            {
                VID = part.ID,
                VTitleID = part.TitleID,
                VTitleName = part.TitleName,
                VTitleDescription = part.TitleDescription

            });
            return ViewModel.ToList();
        }

        public static FooterPublicDataViewModel ToViewModel1(this FooterPublicData model, string Mode)
        {
            return new FooterPublicDataViewModel
            {
                VID = model.ID,
                VTitleID = model.TitleID,
                VTitleName = model.TitleName,
                VTitleDescription = model.TitleDescription,

                Mode = Mode

            };
        }



        #endregion

        #region Upload Meta Data file
        public static List<UploadMetaViewModel> ToViewModel(this List<mUploadmetaDataFile> entityList)
        {
            var viewModel = entityList.Select(entity => new UploadMetaViewModel()
            {
                AssemblyId = entity.AssemblyId,
                SessionId = entity.SessionId,
                SessionDateId = entity.SessionDateId,
                DocumentTypeId = entity.DocumentTypeId,
                BillId = entity.BillId,
                PdfFile = entity.PdfFile,
                Image = entity.Image

            });
            return viewModel.ToList();
        }
        public static mUploadmetaDataFile ToUploadModel(this UploadMetaViewModel uploadFile)
        {
            var DomainModel = new mUploadmetaDataFile()
            {
                AssemblyId = uploadFile.AssemblyId,
                SessionId = uploadFile.SessionId,
                SessionDateId = uploadFile.SessionDateId,
                QuestionId = uploadFile.QuestionId,
                PdfFile = uploadFile.PdfFile,
                Image = uploadFile.Image,
                DocumentTypeId = uploadFile.DocumentTypeId,
                BillId = uploadFile.BillId,
                 CreatedDate = uploadFile.CreatedDate,
                 CommitteeId=uploadFile.CommitteeId,
                 CommitteeTypeId=uploadFile.CommitteeTypeId,
                 DepartmentId=uploadFile.DepartmentId



            };
            return DomainModel;
        }
        #endregion


        public static FormsViewModel ToFAQViewModel(this tFAQ faq, string Mode)
        {
            var ViewModel = new FormsViewModel()
            {

                Id = faq.Id,
                FAQ = faq.FAQ,
                FAA = faq.FAA,
                IsPublished = faq.IsPublished

            };
            return ViewModel;
        }


        public static FormsViewModel ToSiteMapViewModel(this tSiteMap faq, string Mode)
        {
            var ViewModel = new FormsViewModel()
            {
                SiteID = faq.Id,
                SiteSection = faq.Section,
                SiteTitle = faq.Title,
                SiteUrl = faq.URLPath

            };
            return ViewModel;
        }




        public static IEnumerable<FormsViewModel> ToViewModelFAQ(this IEnumerable<tFAQ> notice)
        {
            // tFAQ notice = new tFAQ();
            var ViewModel = notice.Select(n => new FormsViewModel()
            {
                FAQ = n.FAQ,
                FAA = n.FAA,
                Id = n.Id

            });
            return ViewModel;
        }
        public static IEnumerable<FormsViewModel> ToViewModelSiteMap(this IEnumerable<tSiteMap> notice)
        {
            // tFAQ notice = new tFAQ();
            var ViewModel = notice.Select(n => new FormsViewModel()
            {
                SiteID = n.Id,
                SiteSection = n.Section,
                SiteTitle = n.Title,
                SiteUrl=n.URLPath

            });
            return ViewModel;
        }

        public static IEnumerable<FormsViewModel> ToViewModelHD(this IEnumerable<tUserMannuals> notice)
        {
            // tFAQ notice = new tFAQ();
            var ViewModel = notice.Select(n => new FormsViewModel()
            {
                Title = n.Title,
                Description = n.Description,
                MannualID = n.MannualID,
                FilePath = n.FilePath

            });
            return ViewModel;
        }
        public static FormsViewModel ToHDViewModel(this tUserMannuals hd, string Mode)
        {
            var ViewModel = new FormsViewModel()
            {

                MannualID = hd.MannualID,
                Title = hd.Title,
                Description = hd.Description,
                FilePath = hd.FilePath,

            };
            return ViewModel;
        }

        public static tFAQ ToDomainModel1(this FormsViewModel assemblyFile1)
        {
            var DomainModel1 = new tFAQ()
            {
                Id = assemblyFile1.Id,
                FAQ = assemblyFile1.FAQ,
                FAA = assemblyFile1.FAA,
                IsPublished = assemblyFile1.IsPublished
            };
            return DomainModel1;
        }

        public static tSiteMap ToDomainModelSiteMap(this FormsViewModel assemblyFile1)
        {
            var DomainModel1 = new tSiteMap()
            {
                Id = assemblyFile1.SiteID,
                Section = assemblyFile1.SiteSection,
                Title = assemblyFile1.SiteTitle,
                URLPath = assemblyFile1.SiteUrl
            };
            return DomainModel1;
        }
        public static tUserMannuals ToDomainModel2(this FormsViewModel assemblyFile1)
        {
            var DomainModel1 = new tUserMannuals()
            {
                MannualID = assemblyFile1.MannualID,
                Title = assemblyFile1.Title,
                Description = assemblyFile1.Description,
                FilePath = assemblyFile1.FilePath
            };
            return DomainModel1;
        }

        public static tKnowldgeBankRef ToDomainKnowledgeBankListModel(this ReferenceSearchResult list)
        {
            var DomainModel5 = new tKnowldgeBankRef()
            {
                Id = list.Id,
                SectorId = list.SectorId,
                Description = list.Description,
                FileLocation = list.FileLocation,
                CollectionTypeId = list.CollectionTypeId,
                SubmittedDate = list.Date,
                RegionId = list.RegionId
            };

            return DomainModel5;
        }

        public static ReferenceSearchResult ToDomainKnowledgeBankListModelEdit(this tKnowldgeBankRef list)
        {
            var DomainModel5 = new ReferenceSearchResult()
            {
                Id = list.Id,
                SectorId = list.SectorId,
                Description = list.Description,
                FileLocation = list.FileLocation,
                CollectionTypeId = list.CollectionTypeId,
                Date = list.SubmittedDate,
                RegionId = list.RegionId,
            };

            return DomainModel5;
        }


        public static tSectors ToDomainSectorModel(this ReferenceSearchResult sector)
        {
            var DomainModel5 = new tSectors()
            {
                Id = sector.Id,
                Title = sector.SectorName,
                Active = sector.IsActive

            };

            return DomainModel5;
        }

        public static ReferenceSearchResult ToDomainSectorModelEdit(this tSectors sector)
        {
            var DomainModel5 = new ReferenceSearchResult()
            {
                Id = sector.Id,
                SectorName = sector.Title,
                IsActive = sector.Active,

            };

            return DomainModel5;
        }

        public static tCollectionType ToDomainCollectionTypeModel(this ReferenceSearchResult collection)
        {
            var DomainModel5 = new tCollectionType()
            {
                Id = collection.Id,
                Title = collection.CollectionName,
                Active = collection.IsActive

            };

            return DomainModel5;
        }

        public static ReferenceSearchResult ToDomainCollectionTypeModelEdit(this tCollectionType collection)
        {
            var DomainModel5 = new ReferenceSearchResult()
            {
                Id = collection.Id,
                CollectionName = collection.Title,
                IsActive = collection.Active,

            };

            return DomainModel5;
        }


    }}
