﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class FeedbackViewModel
    {

        public int VId { get; set; }
        public string VName { get; set; }
        public string VEmail { get; set; }
        public string VMobile { get; set; }
        public string VMessage { get; set; }
        public string VDate { get; set; }
        public string VType { get; set; }
        public IEnumerable<FeedbackViewModel> GetAllFeedback { get; set; }

    }
}