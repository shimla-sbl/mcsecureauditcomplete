﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class AlbumCategoryViewModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AlbumCategoryID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? IsActive { get; set; }
        public int? MemberCode { get; set; }
        [NotMapped]
        public string Mode { get; set; }
        public string UserId { get; set; }
    }
}