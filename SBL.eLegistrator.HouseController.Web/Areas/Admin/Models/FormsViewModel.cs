﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Forms;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    [Serializable]
    public class FormsViewModel
    {
        public int VFormId { get; set; }

        [Required(ErrorMessage = "Assembly is Required")]
        public int VAssemblyId { get; set; }


        [Required(ErrorMessage = "Form Type is Required")]
        public int? VTypeofFormId { get; set; }

        public string VDescription { get; set; }

        public string VUploadFile { get; set; }

        public string VCreatedBy { get; set; }

        public DateTime? VCreatedDate { get; set; }

        public bool VStatus { get; set; }

        public DateTime? VModifiedDate { get; set; }

        public string VModifiedBy { get; set; }

        public string VAssemblyName { get; set; }

        

        public string VFormDocTypeName { get; set; }

        public string FileAcessPath { get; set; }

        public string Mode { get; set; }

        public List<mAssembly> AssemblyList { get; set; }


        public List<tFormsTypeofDocuments> FormDocumentTypeList { get; set; }

        public SelectList FormDocumentTypeList1 { get; set; }

        public SelectList StatusTypeList { get; set; }

        public bool IsCreatNew { get; set; }

        public string FAQ { get; set; }
         [UIHint("tinymce_jquery_full"), AllowHtml]
        public string FAA { get; set; }

        public bool IsPublished { get; set; }

        public int Id { get; set; }


        public int MannualID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string FilePath { get; set; }

        public string ImageLocation { get; set; }
      
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string LocalFAA { get; set; }
        public string LocalFAQ { get; set; }
        public bool LocalIsPublished { get; set; }

        public int SiteID { get; set; }

        public string SiteSection { get; set; }

        public string SiteTitle { get; set; }

        public string SiteUrl { get; set; }
    }
}