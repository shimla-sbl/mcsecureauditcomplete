﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models.SystemModule
{
    public class SystemModuleModel
    {
        public int SystemModuleID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public string Name { get; set; }
        public string Description { get; set; }

        public string Mode { get; set; }
        public bool SaveCreateNew { get; set; }
    }

    public class SystemFunctionModel
    {
        public int SystemFunctionID { get; set; }
        public string Name { get; set; }

        [AllowHtml]
        public string Description { get; set; }
        public string AssociatedContactGroups { get; set; }
        public string SMSTemplate { get; set; }
        public string EmailTemplate { get; set; }
        public bool SendSMS { get; set; }
        public bool SendEmail { get; set; }
        public string Parameters { get; set; }

        public string Mode { get; set; }
        public bool SaveCreateNew { get; set; }
    }
}