﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class SerialNoViewModel
    {
        public int id { get; set; }
        public int year { get; set; }
        public long runno { get; set; }
        public string ptype { get; set; }
        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public string Mode { get; set; }

        public string strId { get; set; }
    }
}
