﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Connection;
using System.ComponentModel;
namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
 

    public class DepartmnetDetailsViewModel
    {

        public long departmentDetailsId { get; set; }

  
        [DisplayName("Title")]
        public string Title { get; set; }

        [DisplayName("शीर्षक")]
        public string TitleLocal { get; set; }

        [Required]
        [DisplayName("Department Name")]
        public string deptId { get; set; }
        [DisplayName("Category Name")]
        [Required]
        public Int64 catId { get; set; }

        [AllowHtml]
        public string deptDescription { get; set; }

        [AllowHtml]
        public string deptDescriptionLocal { get; set; }

        public bool? isDeleted { get; set; }

        public DateTime deleteDate { get; set; }

        public DateTime createdDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public bool? isActive { get; set; }

        public string mode { get; set; }

        public int ProgrammeOrder { get; set; }

        public string departmentName { get; set; }
        public string categoryName { get; set; }
        public string departmentNameLocal { get; set; }

        public static void DeleteDepartmentDetailsById(int Id)
        {
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@departmentDetailsId", Id) };
                SqlHelper.ExecuteNonQuery(connection, "[DeleteDepartmentDetailsById]", parameterValues);
                connection.Close();
            }
            catch (Exception)
            {
            }
        }


        public static void DeleteDocumentById(int Id)
        {
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@programmeDocsId", Id) };
                SqlHelper.ExecuteNonQuery(connection, "[DeleteDeptDocumentById]", parameterValues);
                connection.Close();
            }
            catch (Exception)
            {
            }
        }


        public static SelectList GetAllDepartmentDetails(Int64 CatId)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@CatId", CatId), new SqlParameter("@DeptId", "") };
                connection.Close();
               
                foreach (DataRow row in SqlHelper.ExecuteDataset(connection, "[GetAllDepartmentDetails]", parameterValues).Tables[0].Rows)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = Convert.ToString(row["deptname"].ToString());
                    item.Value = Convert.ToString(row["departmentDetailsId"].ToString());
                    items.Add(item);
                }
                return new SelectList(items, "Value", "Text");
            }
            catch (Exception)
            {
                return new SelectList(items, "Value", "Text");
            }
        }
        public static SelectList GetAllDepartmentDetailsFilter(Int64 CatId)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@CatId", CatId)};
                connection.Close();

                foreach (DataRow row in SqlHelper.ExecuteDataset(connection, "[GetAllDepartmentDetailsFilter]", parameterValues).Tables[0].Rows)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = Convert.ToString(row["deptname"].ToString());
                    item.Value = Convert.ToString(row["departmentDetailsId"].ToString());
                    items.Add(item);
                }
                return new SelectList(items, "Value", "Text");
            }
            catch (Exception)
            {
                return new SelectList(items, "Value", "Text");
            }
        }
        public static SelectList GetAllDepartmentDetails(Int64 CatId,string DeptId)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@CatId", CatId), new SqlParameter("@DeptId", DeptId) };
                connection.Close();

                foreach (DataRow row in SqlHelper.ExecuteDataset(connection, "[GetAllDepartmentDetails]", parameterValues).Tables[0].Rows)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = Convert.ToString(row["deptname"].ToString());
                    item.Value = Convert.ToString(row["departmentDetailsId"].ToString());
                    items.Add(item);
                }
                return new SelectList(items, "Value", "Text");
            }
            catch (Exception)
            {
                return new SelectList(items, "Value", "Text");
            }
        }
        public static SelectList GetAllCategoryDetails()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                connection.Close();
                foreach (DataRow row in SqlHelper.ExecuteDataset(connection, "[GetAllCategoryDetails]", new object[0]).Tables[0].Rows)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = Convert.ToString(row["CategoryName"].ToString());
                    item.Value = Convert.ToString(row["catId"].ToString());
                    items.Add(item);
                }
                return new SelectList(items, "Value", "Text");
            }
            catch (Exception)
            {
                return new SelectList(items, "Value", "Text");
            }
        }

        public static SelectList GetTenderCategory()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                connection.Close();
                foreach (DataRow row in SqlHelper.ExecuteDataset(connection, "[GetTenderCategory]", new object[0]).Tables[0].Rows)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = Convert.ToString(row["CategoryName"].ToString());
                    item.Value = Convert.ToString(row["catId"].ToString());
                    items.Add(item);
                }
                return new SelectList(items, "Value", "Text");
            }
            catch (Exception)
            {
                return new SelectList(items, "Value", "Text");
            }
        }
        

        public static SelectList getDepartment()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                connection.Close();
                foreach (DataRow row in SqlHelper.ExecuteDataset(connection, "[GetDepartment]", new object[0]).Tables[0].Rows)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = Convert.ToString(row["deptname"].ToString());
                    item.Value = Convert.ToString(row["deptid"].ToString());
                    items.Add(item);
                }
                return new SelectList(items, "Value", "Text");
            }
            catch (Exception)
            {
                return new SelectList(items, "Value", "Text");
            }
        }

        public static SelectList GetDepartmentById(string DeptId)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@deptid", DeptId) };
                connection.Close();
                foreach (DataRow row in SqlHelper.ExecuteDataset(connection, "[GetDepartmentById]", parameterValues).Tables[0].Rows)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = Convert.ToString(row["deptname"].ToString());
                    item.Value = Convert.ToString(row["deptid"].ToString());
                    items.Add(item);
                }
                return new SelectList(items, "Value", "Text");
            }
            catch (Exception)
            {
                return new SelectList(items, "Value", "Text");
            }
        }

        




        public static List<DepartmnetDetailsViewModel> GetDepartmentDetails()
        {
            List<DepartmnetDetailsViewModel> list = new List<DepartmnetDetailsViewModel>();
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                connection.Close();
                foreach (DataRow row in SqlHelper.ExecuteDataset(connection, "[getDepartmentDetailsList]", new object[0]).Tables[0].Rows)
                {
                    DepartmnetDetailsViewModel item = new DepartmnetDetailsViewModel
                    {
                        departmentDetailsId = Convert.ToInt32(row["departmentDetailsId"].ToString()),
                        isActive = new bool?(Convert.ToBoolean(row["isActive"])),
                        deptDescription = Convert.ToString(row["deptDescription"].ToString()),
                        deptDescriptionLocal = Convert.ToString(row["deptDescriptionLocal"].ToString()),
                        createdDate = Convert.ToDateTime(Convert.ToString(row["createdDate"])),
                        ModifiedDate = Convert.ToDateTime(Convert.ToString(row["ModifiedDate"])),
                        departmentName = Convert.ToString(Convert.ToString(row["departmentName"])),
                        categoryName = Convert.ToString(Convert.ToString(row["CategoryName"])),
                        departmentNameLocal = Convert.ToString(Convert.ToString(row["departmentNameLocal"])),
                        Title = Convert.ToString(Convert.ToString(row["Title"])),
                    TitleLocal = Convert.ToString(Convert.ToString(row["TitleLocal"])),
                };
                    list.Add(item);
                }
                return list;
            }
            catch (Exception)
            {
                return list;
            }
        }



        public static DepartmnetDetailsViewModel GetDepartmentDetailsById(long Id)
        {
            DepartmnetDetailsViewModel model = new DepartmnetDetailsViewModel();
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@departmentDetailsId", Id) };
                connection.Close();
                foreach (DataRow row in SqlHelper.ExecuteDataset(connection, "[GetDepartmentDetailsById]", parameterValues).Tables[0].Rows)
                {
                    model.departmentDetailsId = Convert.ToInt64(row["departmentDetailsId"].ToString());
                    model.isActive = new bool?(Convert.ToBoolean(row["IsActive"]));
                    model.deptDescription = Convert.ToString(row["deptDescription"].ToString());
                    model.deptDescriptionLocal = Convert.ToString(row["deptDescriptionLocal"].ToString());
                    model.deptId = Convert.ToString(row["deptId"].ToString());
                    model.createdDate = Convert.ToDateTime(Convert.ToString(row["createdDate"]));
                    model.ModifiedDate = Convert.ToDateTime(Convert.ToString(row["ModifiedDate"]));
                    model.catId = Convert.ToInt64(Convert.ToString(row["catId"]));
                    model.Title = Convert.ToString(Convert.ToString(row["Title"]));
                    model.TitleLocal = Convert.ToString(Convert.ToString(row["TitleLocal"]));
                    model.mode = "Edit";
                }
                return model;
            }
            catch (Exception)
            {
                return model;
            }
        }



        public static SelectList GetStatusTypes()
        {
            SelectListItem item = new SelectListItem();
            item.Text = "Published";
            item.Value = "true";
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(item);
            SelectListItem item2 = new SelectListItem();
            item2.Text = "Unpublished ";
            item2.Value = "false";
            items.Add(item2);
            return new SelectList(items, "Value", "Text");
        }

        public static SelectList GetCategoryTypes()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                connection.Close();
                foreach (DataRow row in SqlHelper.ExecuteDataset(connection, "[GetCategory]", new object[0]).Tables[0].Rows)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = Convert.ToString(row["CategoryName"].ToString());
                    item.Value = Convert.ToString(row["CategoryId"].ToString());
                    items.Add(item);
                }
                return new SelectList(items, "Value", "Text");
            }
            catch (Exception)
            {
                return new SelectList(items, "Value", "Text");
            }
        }

        public static int SaveDepartmentDetails(DepartmnetDetailsViewModel model)
        {
            try
            {
                SqlConnection connection = ClsConnection.GetConnection();
                if ((connection.State == ConnectionState.Closed) || (connection.State == ConnectionState.Broken))
                {
                    connection.Open();
                }
                SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@Active", model.isActive),
                    new SqlParameter("@IsDeleted", "false"), new SqlParameter("@deptId", model.deptId), new SqlParameter("@deptDescription", model.deptDescription), new SqlParameter("@deptDescriptionLocal", model.deptDescriptionLocal), new SqlParameter("@departmentDetailsId", model.departmentDetailsId),
                    new SqlParameter("@catId", model.catId),
                    new SqlParameter("@Title", model.Title),new SqlParameter("@TitleLocal", model.TitleLocal),
                

                };
                SqlHelper.ExecuteNonQuery(connection, "[sp_SaveUpdateDepartmentDetails]", parameterValues);
                connection.Close();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }



        
    }
}

