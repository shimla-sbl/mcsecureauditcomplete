﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Tour;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    [Serializable]
    public class TourViewModel
    {
        public TourViewModel(){

            this.TourListCol = new List<tMemberTour>();
        }

        public int TourId { get; set; }

        public int AssemblyId { get; set; }

        public int MemberId { get; set; }

        [AllowHtml]
        public string TourTitle { get; set; }

        public string LocalTourTitle { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string TourDescrption { get; set; }

        [AllowHtml]
        public string LocalTourDescription { get; set; }

        [Required(ErrorMessage = "Tour From Date Is Required")]
        public string TourFromDateTime { get; set; }

        [Required(ErrorMessage = "Tour To Date Is Required")]
        public string TourToDateTime { get; set; }

        public string Attachement { get; set; }

        public string AliasAttachment { get; set; }

        public Guid CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public Guid? ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int Hits { get; set; }

        public string mode { get; set; }

        public string EditMode { get; set; }

        public string Purpose { get; set; }

        public string MemberName { get; set; }

        public bool IsPublished { get; set; }

        public string NotifyDate { get; set; }

        public string NotifyTime { get; set; }

        public SelectList MemberList { get; set; }

        public string ImageLocation { get; set; }

        public string Departure { get; set; }

        public string Arrival { get; set; }

        public string DepartureTime { get; set; }

        public string ArrivalTime { get; set; }

        public bool isLocale { get; set; }

        public DateTime? LastUpdatedDate { get; set; }

        public ICollection<tMemberTour> TourListCol { get; set; }
    }
}