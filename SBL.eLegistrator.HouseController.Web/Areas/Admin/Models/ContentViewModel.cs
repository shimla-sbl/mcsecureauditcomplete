﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class ContentViewModel
    {
        public int VContentID { get; set; }

        [Required(ErrorMessage = "Content Title is Required")]
        public string VContentTitle { get; set; }

        public string VContentLocalTitle { get; set; }

        [AllowHtml]
        public string VContentDescription { get; set; }

        [AllowHtml]
        public string VContentLocalDescription { get; set; }

        public byte VStatus { get; set; }

        [Range(1, 300, ErrorMessage = "Please Select Content Categroy")]
        public int VCategoryID { get; set; }

        public Guid VCreatedBy { get; set; }

        public DateTime VCreatedDate { get; set; }

        public Guid VModifiedBy { get; set; }

        public DateTime VModifiedDate { get; set; }

        public DateTime VStartPublish { get; set; }

        public DateTime VFinishPublish { get; set; }

        public int VOrdering { get; set; }

        public int VHits { get; set; }

        public string VCategoryTypeName { get; set; }

        public SelectList StatusTypeList { get; set; }

        public SelectList CategoryList { get; set; }

        public string mode { get; set; }

        public bool IsCreatNew { get; set; }

    }
}