﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class NewsViewModel
    {
        public int LatestUpdateID { get; set; }

        public DateTime Date_Of_Event { get; set; }

        public DateTime Publish_date { get; set; }

        public string NewsEventDate { get; set; }



        [Required(ErrorMessage = "News Title is Required")]
        public string News_Title { get; set; }

        public string Local_News_Title { get; set; }

        //[AllowHtml]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string News_Description { get; set; }

        public bool ShowAlways { get; set; }

        public Guid ModifiedBy { get; set; }

        public DateTime ModifiedWhen { get; set; }

        public string mode { get; set; }

        public byte Status { get; set; }

        //[Range(1, 300, ErrorMessage = "Please Select News Category")]
        [Required(ErrorMessage = "Please Select News Category")]
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public bool IsActive { get; set; }

        public SelectList TypeList { get; set; }


        public SelectList CategoryList { get; set; }

        //  public string Mode { get; set; }

        public string FilePath { get; set; }

        public string FileName { get; set; }
        public bool IsCreatNew { get; set; }
        [AllowHtml]
        public string HindiDescription { get; set; }

        public string ImageLocation { get; set; }

        public int Hits { get; set; }

        public string ThumbName { get; set; }

        public string FullImage { get; set; }

        public string ThumbImage { get; set; }

        public string FileLocation { get; set; }

        public string Name { get; set; }

        public int Length { get; set; }

        public string Type { get; set; }

        public string FilePath1 { get; set; }

        public string FileName1 { get; set; }

        public string ImageLocation1 { get; set; }

        public string ThumbName1 { get; set; }

        public string FullImage1 { get; set; }

        public string ThumbImage1 { get; set; }

        public string FileLocation1 { get; set; }

        public string FileAcessPath { get; set; }

        public bool isFlashNews
        {

            get;
            set;
        }



         
    }


}