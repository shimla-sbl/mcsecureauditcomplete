﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class SpeechViewModel
    {

        public int VSpeechID { get; set; }
        [Required(ErrorMessage = "Speech Title is Required")]
        public string VSpeechTitle { get; set; }

        public string VLocalSpeechTitle { get; set; }

        [AllowHtml]
        public string VSpeechDescription { get; set; }

        [AllowHtml]
        public string VLocalSpeechDescription { get; set; }

        public byte VSpeechType { get; set; }

        public string VFileName { get; set; }

        public string VFilePath { get; set; }

        public byte VStatus { get; set; }

        public DateTime VCreatedDate { get; set; }

        public DateTime VModifiedDate { get; set; }

        public Guid VCreatedBy { get; set; }

        public Guid VModifiedBy { get; set; }

        public DateTime VStartPublish { get; set; }

        public DateTime VStopPublish { get; set; }

        public int VOrdering { get; set; }

        public int VHits { get; set; }

        //[Range(1, 300, ErrorMessage = "Please Select Speech Categroy")]
        [Required(ErrorMessage = "Please Select Speech Categroy")]
        public int VCategoryId { get; set; }

        public string Mode { get; set; }

        public SelectList StatusTypeList { get; set; }

        public SelectList CategoryList { get; set; }

        public SelectList SpeechTypeList { get; set; }

        public string CategoryName { get; set; }

        public bool IsCreatNew { get; set; }

        public string ImageLocation { get; set; }

        [Range(1, 300, ErrorMessage = "Please Select Assembly")]
        public int VAssemblyId { get; set; }

        [Range(1, 300, ErrorMessage = "Please Select SessionId")]
        public int VSessionId { get; set; }

        public int? VSessionDateId { get; set; }

        public DateTime? VDate { get; set; }

        public string VAssemblyName { get; set; }

        public string VSessionName { get; set; }

        public string VSessionDate { get; set; }

        public List<mAssembly> AssemblyList { get; set; }

        public List<mSession> SessionList { get; set; }

        public List<mSessionDate> SessionDateList { get; set; }

        public string VFileName1 { get; set; }

        public string VFilePath1 { get; set; }

        public string ImageLocation1 { get; set; }
    }
}