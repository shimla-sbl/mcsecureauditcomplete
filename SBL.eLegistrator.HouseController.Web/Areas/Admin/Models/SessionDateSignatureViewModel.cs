﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SessionDateSignature;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{

    [Serializable]
    public class SessionDateSignatureViewModel
    {
        public int SessionDateSignatureDetailsId { get; set; }

        [Required(ErrorMessage = "Assembly Required")]
        public int AssemblyId { get; set; }

        [Required(ErrorMessage = "Session Required")]
        public int SessionId { get; set; }

        [Required(ErrorMessage = "Session Date Required")]
        public int? SessionDateId { get; set; }

        [Required(ErrorMessage = "Signature Name Required")]
        [StringLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string SignatureName { get; set; }

        [StringLength(70, ErrorMessage = "Maximum 70 characters allowed")]
        public string SignatureNameLocal { get; set; }

        [Required(ErrorMessage = "Header Text Required")]
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string HeaderText { get; set; }

        [StringLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string HeaderTextLocal { get; set; }

        [Required(ErrorMessage = "Signature Place with Pincode Required")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string SignaturePlace { get; set; }

        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string SignaturePlaceLocal { get; set; }

        [Required(ErrorMessage = "Signature Date Required")]
        public string SignatureDate { get; set; }

        public string SignatureDateLocal { get; set; }

        [Required(ErrorMessage = "Signature Designation Required")]
        [StringLength(60, ErrorMessage = "Maximum 60 characters allowed")]
        public string SignatureDesignations { get; set; }

        [StringLength(80, ErrorMessage = "Maximum 80 characters allowed")]
        public string SignatureDesignationsLocal { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public bool IsActive { get; set; }

        public string Mode { get; set; }

        public List<mAssembly> AssemblyList { get; set; }

        public List<mSession> SessionList { get; set; }

        public List<mSessionDate> SessionDateList { get; set; }

        public string AssemblyName { get; set; }

        public string SessionName { get; set; }

        public string SessionDate { get; set; }


    }
}