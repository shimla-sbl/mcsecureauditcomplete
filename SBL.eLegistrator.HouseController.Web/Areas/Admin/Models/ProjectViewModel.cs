﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Connection;
namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class ProjectViewModel
    {
        public int ProgramId { get; set; }

        [Required(ErrorMessage = "Program Title is Required")]
        public string ProgrammeTitle { get; set; }

        [Required(ErrorMessage = "Program Name is Required")]
        public string ProgrammeName { get; set; }
        public string ProgrammeNameLocal { get; set; }
        public string ProgrammeTitleLocal { get; set; }

        [AllowHtml]
        public string ProgrammeDescription { get; set; }

        [AllowHtml]
        public string ProgrammeDescriptionLocal { get; set; }

        public bool IsDeleted { get; set; }


        public Guid CreatedBy { get; set; }
        public int IsActive { get; set; }
        public DateTime CreatedDate { get; set; }

        public Guid ModifiedBy { get; set; }

        public DateTime ? ModifiedDate { get; set; }


        public SelectList StatusTypeList { get; set; }
        public string mode { get; set; }
        public int ProgrammeOrder { get; set; }


        public static int SaveProject(ProjectViewModel model)
        {
            try
            {
                SqlConnection con = ClsConnection.GetConnection();
                if (con.State == ConnectionState.Closed || con.State == ConnectionState.Broken)
                    con.Open();
                SqlParameter[] param = new SqlParameter[10];
                param[0] = new SqlParameter("@CreatedBy", model.CreatedBy.ToString ());
                param[1] = new SqlParameter("@Active",  model.IsActive);
                param[2] = new SqlParameter("@IsDeleted", 0);
                param[3] = new SqlParameter("@ProgramId", model.ProgramId);
                param[4] = new SqlParameter("@ProgrammeDescription", model.ProgrammeDescription);
                param[5] = new SqlParameter("@ProgrammeDescriptionLocal", model.ProgrammeDescriptionLocal);
                param[6] = new SqlParameter("@ProgrammeName", model.ProgrammeName);
                param[7] = new SqlParameter("@ProgrammeNameLocal", model.ProgrammeNameLocal);
                param[8] = new SqlParameter("@ProgrammeTitle", model.ProgrammeTitle);
                param[9] = new SqlParameter("@ProgrammeTitleLocal", model.ProgrammeTitleLocal);
                SqlHelper.ExecuteNonQuery(con, "[sp_SaveUpdateProject]", param);
                con.Close();
                return 1;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return 0;

            }
        }




        public static List<ProjectViewModel> GetProjectList()
        {
            List<ProjectViewModel> lstproject = new List<ProjectViewModel>();
            try
            {
                SqlConnection con = ClsConnection.GetConnection();
                if (con.State == ConnectionState.Closed || con.State == ConnectionState.Broken)
                    con.Open();

                DataSet ds = SqlHelper.ExecuteDataset(con, "[getProgramList]");
                con.Close();
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    ProjectViewModel project = new ProjectViewModel();
                    project.ProgramId = Convert.ToInt32(row["programId"].ToString());
                    project.IsActive = Convert.ToInt32(row["isActive"]);
                    project.ProgrammeDescription = Convert.ToString(row["ProgrammeDescription"].ToString());
                    project.ProgrammeDescriptionLocal = Convert.ToString(row["ProgrammeDescriptionLocal"].ToString());
                    project.ProgrammeName = Convert.ToString(row["ProgrammeName"].ToString());
                    project.ProgrammeNameLocal = Convert.ToString(row["ProgrammeNameLocal"].ToString());
                    project.ProgrammeTitle = Convert.ToString(row["ProgrammeTitle"].ToString());
                    project.ProgrammeTitleLocal = Convert.ToString(row["ProgrammeTitleLocal"].ToString());
                    project.CreatedDate = Convert.ToDateTime(Convert.ToString(row["createdWhen"]));
                    project.ModifiedDate = string.IsNullOrWhiteSpace(Convert.ToString(row["modifiedwhen"]))
    ? (DateTime?)null
    : DateTime.Parse(Convert.ToString(row["modifiedwhen"]));
                    lstproject.Add(project);

                }
                return lstproject;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return lstproject;

            }
        }


        public static ProjectViewModel GetProjectListById(int Id)
        {
            ProjectViewModel project = new ProjectViewModel();
            try
            {
                SqlConnection con = ClsConnection.GetConnection();
                if (con.State == ConnectionState.Closed || con.State == ConnectionState.Broken)
                    con.Open();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@programId", Id);
                

                DataSet ds = SqlHelper.ExecuteDataset(con, "[getProgramListbyId]",param);
                con.Close();
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                   
                    project.ProgramId = Convert.ToInt32(row["programId"].ToString());
                    project.IsActive = Convert.ToInt32(row["isActive"]);
                    project.ProgrammeDescription = Convert.ToString(row["ProgrammeDescription"].ToString());
                    project.ProgrammeDescriptionLocal = Convert.ToString(row["ProgrammeDescriptionLocal"].ToString());
                    project.ProgrammeName = Convert.ToString(row["ProgrammeName"].ToString());
                    project.ProgrammeNameLocal = Convert.ToString(row["ProgrammeNameLocal"].ToString());
                    project.ProgrammeTitle = Convert.ToString(row["ProgrammeTitle"].ToString());
                    project.ProgrammeTitleLocal = Convert.ToString(row["ProgrammeTitleLocal"].ToString());
                    project.CreatedDate = Convert.ToDateTime(Convert.ToString(row["createdWhen"]));
                    project.ModifiedDate = string.IsNullOrWhiteSpace(Convert.ToString(row["modifiedwhen"]))
    ? (DateTime?)null
    : DateTime.Parse(Convert.ToString(row["modifiedwhen"]));

                    
                   

                }
                return project;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return project;

            }
        }

        public static void DeleteProjectById(int Id)
        {
         
            try
            {
                SqlConnection con = ClsConnection.GetConnection();
                if (con.State == ConnectionState.Closed || con.State == ConnectionState.Broken)
                    con.Open();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@programId", Id);


                SqlHelper.ExecuteNonQuery(con, "[DeleteProgrambyId]", param);
                con.Close();

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
           

            }
        }

        public static void DeleteDocumentById(int Id)
        {

            try
            {
                SqlConnection con = ClsConnection.GetConnection();
                if (con.State == ConnectionState.Closed || con.State == ConnectionState.Broken)
                    con.Open();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@programmeDocsId", Id);


                SqlHelper.ExecuteNonQuery(con, "[DeleteDocumentById]", param);
                con.Close();

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


            }
        }
    }
}