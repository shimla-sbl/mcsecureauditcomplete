﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    [Serializable]
    public class MemberDetailsModel
    {

        public int MemberId { get; set; }

        public int MemberCode { get; set; }

        public string ShimlaAddress { get; set; }

        public string PermanentAddress { get; set; }

        [AllowHtml]
        public string Details { get; set; }

        public string TelePhoneOffice { get; set; }

        public string TelePhoneResidenc { get; set; }

        public string Mobile { get; set; }

        public string PreFix { get; set; }

        public string Email { get; set; }

        public string FatherName { get; set; }

        public string  DistrictName { get; set; }

        public string Name { get; set; }

        public string NameLocal { get; set; }

        public int DistrictId { get; set; }

        public string ImageFile { get; set; }

    }
}