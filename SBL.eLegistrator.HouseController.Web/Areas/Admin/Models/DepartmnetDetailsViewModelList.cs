﻿namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Web.Mvc;

    public class DepartmnetDetailsViewModelList
    {
        public DepartmnetDetailsViewModel lstDepartmnetDetailsViewModel { get; set; }

        public SelectList StatusTypeList { get; set; }

        public SelectList deptList { get; set; }

        public SelectList CategoryList { get; set; }
    }
}

