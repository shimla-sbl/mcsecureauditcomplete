﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Models
{
    public class HouseEmpCommitteeViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string AadharId { get; set; }
        public Boolean IsActive { get; set; }
        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public string Mode { get; set; }

        public string strId { get; set; }
    }
}
