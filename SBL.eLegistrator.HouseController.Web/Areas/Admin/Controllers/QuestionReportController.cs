﻿using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class QuestionReportController : Controller
    {
        //
        // GET: /Admin/QuestionReport/

        public ActionResult Index()
        {
            var MemberList = (List<mMemberAccountsDetails>)Helper.ExecuteService("MemberAccountDetails", "GetCurrentMembersDetails", null);
            ViewBag.MemberList = new SelectList(MemberList, "MemberCode", "MemberName");

            return View();
        }


        public PartialViewResult GetQuestionReport(int MemberId, string QuestionType)
        {

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            int SessionCode = siteSettingMod.SessionCode;
            int AssemblyId = siteSettingMod.AssemblyCode;
            tQuestionModel obj = new tQuestionModel();
            obj.MemberId = MemberId;
            obj.AssemblyId = AssemblyId;
            obj.SessionId = SessionCode;

            List<tQuestionModel> Result = new List<tQuestionModel>();
            if (QuestionType == "Submitted")
                Result = Helper.ExecuteService("Questions", "GetQuestionSubmittedListByMemberId", obj) as List<tQuestionModel>;
            else if (QuestionType == "Pending")
                Result = Helper.ExecuteService("Questions", "GetQuestionPendingListByMemberId", obj) as List<tQuestionModel>;


            return PartialView("/Areas/Admin/Views/QuestionReport/_QuestionList.cshtml", Result);
        }



    }
}
