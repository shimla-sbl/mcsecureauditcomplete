﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.AssemblyFileSystem;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.ComplexModel;
using System.Data;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SMS.API;
using SMSServices;
using SBL.DomainModel.Models.References;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class KnowldgeBankController : Controller
    {
        [Audit]
        [NoCache]
        [SBLAuthorize(Allow = "Authenticated")]

        public ActionResult Index()
        {
            DateTime LastUpdationDate = (DateTime)Helper.ExecuteService("Member", "getLastUpdationDate", null);
            ViewBag.lastUpdate = LastUpdationDate.ToString("dd/MM/yyyy");

            DateTime startDate = System.DateTime.Now;
            DateTime endDate = startDate.AddDays(-30);
            DateTime[] param = new DateTime[2];
            param[0] = startDate;
            param[1] = endDate;
            var Count = (int)Helper.ExecuteService("Member", "getReferencesCount", param);
            ViewBag.ThirtyDays = Convert.ToInt16(Count);

            var SectorDrp = (List<SectorResultDrp>)Helper.ExecuteService("Member", "GetCollectionData", null);
            ReferenceSearchResult model = new ReferenceSearchResult();
            model.SectorDrp = SectorDrp;
            model.SectorDrp.Insert(0, new SectorResultDrp { SectorName = "-----Select Sector-----" });


            var CollectionDrp = (List<CollectionResultDrp>)Helper.ExecuteService("Member", "GetCollectionDataForCollectionType", null);
            model.CollectionDrp = CollectionDrp;
            model.CollectionDrp.Insert(0, new CollectionResultDrp { Title = "-----Select Collection Type-----" });


            var DataList = (List<ReferenceSearchResult>)Helper.ExecuteService("Member", "getReferencesList", null);
            model.RefDataList = DataList;

            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            return PartialView("_Index", model);
        }

        public ActionResult _UpdatedData(int SectorId, int CollectionID, int RegionCode)
        {
            string[] param = new string[5];
            //param[0] = from;
            //param[1] = to;
            param[0] = SectorId.ToString();
            param[1] = CollectionID.ToString();
            param[2] = RegionCode.ToString();
            ReferenceSearchResult model = new ReferenceSearchResult();
            var DataList = (List<ReferenceSearchResult>)Helper.ExecuteService("Member", "getReferencesListUpdated", param);
            model.RefDataList = DataList;
            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            return PartialView(model);
        }


        //    -------------- Madhur-----------------------------------------------------------

        public ActionResult GetSector()
        {
            var SectorDrp = (List<SectorResultDrp>)Helper.ExecuteService("Member", "GetCollectionData", null);
            ReferenceSearchResult model = new ReferenceSearchResult();

            model.SectorDrp = SectorDrp;

            var DataList = (List<ReferenceSearchResult>)Helper.ExecuteService("Member", "getReferencesList", null);
            model.RefDataList = DataList;

            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            // return View("GetSector", model);
            return View(model);


        }

        public ActionResult CreateSector()
        {
            // ReferenceSearchResult model = new ReferenceSearchResult();
            // model.mode = "Add";
            // return View("CreateSector",model);

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                // var categorytype = (List<Category>)Helper.ExecuteService("AContent", "GetAllCategoryType", null);
                var model = new ReferenceSearchResult()
                {
                    mode = "Add",
                    //StatusTypeList = ModelMapping.GetStatusTypes(),
                    // CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null),
                    //VStatus = 1

                };
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Create  Contents Details");
                ViewBag.ErrorMessage = "Their is a Error While Creating Content Details";
                return View("AdminErrorPage");
            }



        }

        public ActionResult SaveSector(ReferenceSearchResult model)
        {

            //if (string.IsNullOrEmpty(CurrentSession.UserID))
            //{
            //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
            //}

            if (model.mode == "Add")
            {
                //string FileName = file.FileName;
               // model.IsActive = true;
                var data = model.ToDomainSectorModel();
                //  data.CreatedBy = Guid.Parse(CurrentSession.UserID);
                //  data.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                Helper.ExecuteService("Member", "AddSector", data);
            }
            else
            {
               // model.IsActive = true;
                var data = model.ToDomainSectorModel();
                // var data = model.ToDomainSectorModel();
                //    data.CreatedBy = Guid.Parse(CurrentSession.UserID);
                //    data.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                Helper.ExecuteService("Member", "UpdateSector", data);
                ////}
                //if (model.IsCreatNew)
                //{
                //    return RedirectToAction("CreateContent");
                //}
                //else
                //{
                // return RedirectToAction("GetSector");
            }
            return RedirectToAction("GetSector");
        }

        public ActionResult EditSector(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var serData = (tSectors)Helper.ExecuteService("Member", "GetSectorById", Id);
                var serData = (tSectors)Helper.ExecuteService("Member", "GetSectorById", new tSectors { Id = Id });
                var model = serData.ToDomainSectorModelEdit();
                model.mode = "Edit";

                return View("CreateSector", model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Create  Contents Details");
                ViewBag.ErrorMessage = "Their is a Error While Creating Content Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult DeleteSector(int Id)
        {
            tSectors sessionToDelete = (tSectors)Helper.ExecuteService("Member", "GetSectorById", new tSectors { Id = Id });
            Helper.ExecuteService("Member", "DeleteSector", sessionToDelete);
            // List<tSectors> data = (List<tSectors>)Helper.ExecuteService("Member", "DeleteSector", Id); ;
            return RedirectToAction("GetSector");
        }
   

        private string UploadPhoto(HttpPostedFileBase File, string FileName)
        {

            try
            {
                if (File != null)
                {
                    var NoticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNoticeFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    //var disFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    //string FilePath = System.IO.Path.Combine("C:/inetpub/e_Vidhan/FileStructure/KnowldgeBankFiles");
                    string FilePath = System.IO.Path.Combine(FileSettings.SettingValue + "KnowldgeBankFiles");
                    DirectoryInfo MDir = new DirectoryInfo(FilePath);
                    if (!MDir.Exists)
                    {
                        MDir.Create();
                    }
                   //string extension = System.IO.Path.GetExtension(File.FileName);
                    string path = System.IO.Path.Combine("C:/inetpub/e_Vidhan/FileStructure/KnowldgeBankFiles", FileName);//Server.MapPath("~/Images/News/"), FileName);
                    File.SaveAs(path);
                    return (FileName);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void RemoveExistingPhoto(string OldPhoto)
        {
            // string path = System.IO.Path.Combine(Server.MapPath("~/Images/Notice/"), OldPhoto);

            try
            {
                var noticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNoticeFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + noticesSettings.SettingValue + "\\", OldPhoto);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public ActionResult GetKnowledgeBankList()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                ReferenceSearchResult model = new ReferenceSearchResult();
                DateTime startDate = System.DateTime.Now;
                DateTime endDate = startDate.AddDays(-30);
                DateTime[] param = new DateTime[2];
                param[0] = startDate;
                param[1] = endDate;
                var Count = (int)Helper.ExecuteService("Member", "getReferencesCount", param);
                ViewBag.ThirtyDays = Convert.ToInt16(Count);
                var DataList = (List<ReferenceSearchResult>)Helper.ExecuteService("Member", "getReferencesList", null);
                model.RefDataList = DataList;
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                //RecordError(ex, "Create  Contents Details");
                ViewBag.ErrorMessage = "Their is a Error While Creating Content Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult CreateKnowledgeListEntry()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var SectorDrp = (List<SectorResultDrp>)Helper.ExecuteService("Member", "GetCollectionData", null);
                SectorDrp.Insert(0, new SectorResultDrp { SectorName = "-----Select Sector-----" });

                var CollectionDrp = (List<CollectionResultDrp>)Helper.ExecuteService("Member", "GetCollectionDataForCollectionType", null);
                CollectionDrp.Insert(0, new CollectionResultDrp { Title = "-----Select Collection Type-----" });

                List<SelectListItem> ObjList = new List<SelectListItem>()
               {
                new SelectListItem { Text = "--Select Region--", Value = "0" },
                new SelectListItem { Text = "Himachal Pradesh", Value = "2" },
                new SelectListItem { Text = "India", Value = "100" },
                new SelectListItem { Text = "International", Value = "101" },
                new SelectListItem { Text = "Not Applicable", Value = "105" },

               };

                ViewBag.Locations = ObjList;
                var model = new ReferenceSearchResult()
                {
                    mode = "Add",
                    SectorDrp = SectorDrp,
                    CollectionDrp = CollectionDrp,


                };
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                //RecordError(ex, "Create  Contents Details");
                ViewBag.ErrorMessage = "Their is a Error While Creating Content Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult EditListEntry(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var SectorDrp = (List<SectorResultDrp>)Helper.ExecuteService("Member", "GetCollectionData", null);
                SectorDrp.Insert(0, new SectorResultDrp { SectorName = "-----Select Sector-----" });

                var CollectionDrp = (List<CollectionResultDrp>)Helper.ExecuteService("Member", "GetCollectionDataForCollectionType", null);
                CollectionDrp.Insert(0, new CollectionResultDrp { Title = "-----Select Collection Type-----" });

                List<SelectListItem> ObjList = new List<SelectListItem>()
               {
                new SelectListItem { Text = "--Select Region--", Value = "0" },
                new SelectListItem { Text = "Himachal Pradesh", Value = "2" },
                new SelectListItem { Text = "India", Value = "100" },
                new SelectListItem { Text = "International", Value = "101" },
                new SelectListItem { Text = "Not Applicable", Value = "105" },

               };
                //var serData = (tSectors)Helper.ExecuteService("Member", "GetSectorById", Id);
                var serData = (tKnowldgeBankRef)Helper.ExecuteService("Member", "GetListEntryById", new tKnowldgeBankRef { Id = Id });
              //  var doctypeList = (List<mAssemblyTypeofDocuments>)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyTypeofDocumentLst", null);
                var model = serData.ToDomainKnowledgeBankListModelEdit();
                model.mode = "Edit";
                model.SectorDrp = SectorDrp;
                model.CollectionDrp = CollectionDrp;
                ViewBag.Locations = ObjList;
                
                return View("CreateKnowledgeListEntry", model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Create  Contents Details");
                ViewBag.ErrorMessage = "Their is a Error While Creating Content Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult SaveKnowledgeListEntry(ReferenceSearchResult model, HttpPostedFileBase file)
        {
            model.Date = System.DateTime.Now;
            var data = model.ToDomainKnowledgeBankListModel();
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            if (model.mode == "Add")
            {
                if (file != null)
                {
                    string extension = Path.GetExtension(file.FileName);
                    Guid FileName1 = Guid.NewGuid();
                    var FileName2 = FileName1 + extension;
                    data.FileLocation = UploadPhoto(file, FileName2);
                    //notice.Attachments = UploadPhoto(file, file.FileName);

                }
                else
                {
                    data.FileLocation = "";
                }

                //string FileName = file.FileName;
                //model.Date = System.DateTime.Now;
                //  model.IsActive = true;

                //  data.CreatedBy = Guid.Parse(CurrentSession.UserID);
                //  data.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                Helper.ExecuteService("Member", "AddKnowledgeBankListEntry", data);
            }
            else
            {
                if (file != null)
                {
                    if (!string.IsNullOrEmpty(data.FileLocation))
                    {
                        RemoveExistingPhoto(data.FileLocation);
                    }
                    string extension = Path.GetExtension(file.FileName);
                    Guid FileName1 = Guid.NewGuid();
                    var FileName2 = FileName1 + extension;
                    data.FileLocation = UploadPhoto(file, FileName2);
                    //  model.IsActive = true;
                    //model.Date = DateTime.Today;
                    //var data = model.ToDomainKnowledgeBankListModel();

                    Helper.ExecuteService("Member", "UpdateKnowledgeBankList", data);
                    ////}
                    //if (model.IsCreatNew)
                    //{
                    //    return RedirectToAction("CreateContent");
                    //}
                    //else
                    //{
                    // return RedirectToAction("GetSector");
                }
                else
                {
                    Helper.ExecuteService("Member", "UpdateKnowledgeBankList", data);
                }
              //  return RedirectToAction("GetKnowledgeBankList");
            }

            return RedirectToAction("GetKnowledgeBankList");
        }

        public ActionResult DeleteKnowledgeListEntry(int Id)
        {
            tKnowldgeBankRef sessionToDelete = (tKnowldgeBankRef)Helper.ExecuteService("Member", "GetListEntryById", new tKnowldgeBankRef { Id = Id });
            Helper.ExecuteService("Member", "DeleteKnowledgeListEntry", sessionToDelete);

            return RedirectToAction("GetKnowledgeBankList");
        }

        public ActionResult GetCollectionType()
        {
            var CollectionDrp = (List<CollectionResultDrp>)Helper.ExecuteService("Member", "GetCollectionDataForCollectionType", null);
            ReferenceSearchResult model = new ReferenceSearchResult();

            model.CollectionDrp = CollectionDrp;

            var DataList = (List<ReferenceSearchResult>)Helper.ExecuteService("Member", "getReferencesList", null);
            model.RefDataList = DataList;

            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            // return View("GetSector", model);
            return View(model);


        }

        public ActionResult CreateCollectionType()
        {
            // ReferenceSearchResult model = new ReferenceSearchResult();
            // model.mode = "Add";
            // return View("CreateSector",model);

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                // var categorytype = (List<Category>)Helper.ExecuteService("AContent", "GetAllCategoryType", null);
                var model = new ReferenceSearchResult()
                {
                    mode = "Add",

                };
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Create  Contents Details");
                ViewBag.ErrorMessage = "Their is a Error While Creating Content Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult EditCollectionType(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var serData = (tSectors)Helper.ExecuteService("Member", "GetSectorById", Id);
                var serData = (tCollectionType)Helper.ExecuteService("Member", "GetCollectionTypeById", new tCollectionType { Id = Id });
                var model = serData.ToDomainCollectionTypeModelEdit();
                model.mode = "Edit";

                return View("CreateCollectionType", model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Create  Contents Details");
                ViewBag.ErrorMessage = "Their is a Error While Creating Content Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult DeleteCollectionType(int Id)
        {
            tCollectionType sessionToDelete = (tCollectionType)Helper.ExecuteService("Member", "GetCollectionTypeById", new tCollectionType { Id = Id });
            Helper.ExecuteService("Member", "DeleteCollectionType", sessionToDelete);
            // List<tSectors> data = (List<tSectors>)Helper.ExecuteService("Member", "DeleteSector", Id); ;
            return RedirectToAction("GetCollectionType");
        }

        public ActionResult SaveCollectionType(ReferenceSearchResult model)
        {

            //if (string.IsNullOrEmpty(CurrentSession.UserID))
            //{
            //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
            //}

            if (model.mode == "Add")
            {

                var data = model.ToDomainCollectionTypeModel();
                Helper.ExecuteService("Member", "AddCollectionType", data);
            }
            else
            {
                // model.IsActive = true;
                var data = model.ToDomainCollectionTypeModel();
                Helper.ExecuteService("Member", "UpdateCollectionType", data);


            }
            return RedirectToAction("GetCollectionType");
        }
    }
}


    

