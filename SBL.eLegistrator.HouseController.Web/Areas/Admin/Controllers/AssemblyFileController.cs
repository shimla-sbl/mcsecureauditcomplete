﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.AssemblyFileSystem;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.ComplexModel;
using System.Data;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SMS.API;
using SMSServices;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class AssemblyFileController : Controller  
    {
        //
        // GET: /Admin/AssemblyFile/

        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult GetAllAssemblyFiles()
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(CurrentSession.UserID))
        //        {
        //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //        }

        //        //FileAccessingUrlPath
        //        //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
        //        var tassemblyfile = (List<tAssemblyFiles>)Helper.ExecuteService("AssemblyFileSystem", "GetAllAssemblyFiles", null);

        //        var model = tassemblyfile.ToViewModel1();
        //        return View(model);
        //    }
        //    catch (Exception ex)
        //    {

        //        RecordError(ex, "Getting All Assembly File");
        //        ViewBag.ErrorMessage = "Their is a Error While Deleting the Gallery Details";
        //        return View("AdminErrorPage");
        //    }

        //}



        public ActionResult GetAllAssemblyFiles(int pageId = 1, int pageSize = 50)
        {

            //int pageId = 1;
            //int pageSize = 10;
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var tassemblyfile = (List<tAssemblyFiles>)Helper.ExecuteService("AssemblyFileSystem", "GetAllAssemblyFiles", null);
                var model = tassemblyfile.ToViewModel1();
                ViewBag.PageSize = pageSize;
                List<AssemblyFileViewModel> pagedRecord = new List<AssemblyFileViewModel>();
                if (pageId == 1)
                {
                    pagedRecord = model.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = model.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(tassemblyfile.Count, pageId, pageSize);
                return View(pagedRecord);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Assembly File Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }




        public PartialViewResult GetAssemblyByPaging(int pageId, int pageSize)
        {
            var tassemblyfile = (List<tAssemblyFiles>)Helper.ExecuteService("AssemblyFileSystem", "GetAllAssemblyFiles", null);
            ViewBag.PageSize = pageSize;
            List<tAssemblyFiles> pagedRecord = new List<tAssemblyFiles>();
            if (pageId == 1)
            {
                pagedRecord = tassemblyfile.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = tassemblyfile.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(tassemblyfile.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel1();
            return PartialView("/Areas/Admin/Views/AssemblyFile/_Index.cshtml", model);
        }


        public PartialViewResult GetSearchList(string Type, int pageId, int pageSize)
        {
            List<tAssemblyFiles> AssemblyFile = new List<tAssemblyFiles>();

            int TypeId = (int)Helper.ExecuteService("AssemblyFileSystem", "SearchGetAssemblyTypeofDocumentLst", Type);
            if (TypeId > 0)
            {

                AssemblyFile = (List<tAssemblyFiles>)Helper.ExecuteService("AssemblyFileSystem", "GetAllAssemblyFilesSearch", TypeId);

            }
            else
            {
                AssemblyFile = (List<tAssemblyFiles>)Helper.ExecuteService("AssemblyFileSystem", "GetAllAssemblyFiles", null);

            }

            ViewBag.PageSize = pageSize;
            List<tAssemblyFiles> pagedRecord = new List<tAssemblyFiles>();
            if (pageId == 1)
            {
                pagedRecord = AssemblyFile.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = AssemblyFile.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(AssemblyFile.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel1();
            return PartialView("/Areas/Admin/Views/AssemblyFile/_Indexx.cshtml", model);
        }



        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "AssemblyFileError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }

        public ActionResult CreateAssemblyFile()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                AssemblyFileViewModel model = new AssemblyFileViewModel();

                //Code for AssemblyList 
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                model.VAssemblyId = data.AssemblyID;
                model.VSessionId = sesdate.SessionId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                // Code for DoumentTypeList
                var doctypeList = (List<mAssemblyTypeofDocuments>)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyTypeofDocumentLst", null);
                model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;
                model.AssemblyDocumentTypeList = doctypeList;
                model.StatusTypeList = ModelMapping.GetStatusTypes();
                model.Mode = "Add";
                return View("CreateAssemblyFile", model);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Creat Assembly File");
                ViewBag.ErrorMessage = "Their is a Error While Create the AssemblyFile Details";
                return View("AdminErrorPage");
            }

        }
        #region SMS For Assembly
        public ActionResult CustomizeSMSEMAIL()
        {
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            //mMember member = new mMember();
            //member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            //RecipientGroup recipientGroup = new RecipientGroup();
            //recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            //model.RecipientGroupList = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllGroupDetails", recipientGroup);

            return PartialView("_CustomizeSMSEMAIL", model);
        }

        public ActionResult CustomizeSMSEMAILwithattachment(string attachment)
        {
            ViewBag.attachment = attachment;
            ViewBag.GenericOnly = "GENRICONLY";
            OnlineMemberQmodel model = new OnlineMemberQmodel();           
            return PartialView("_CustomizeSMSEMAIL", model);
        }
        public JsonResult SendSMSEmail(string EmailIds, string MobileNos, string MsgBody, string IsSms, string IsEmail, int SMSLeft, string chklocal)
        {
            string msg = string.Empty;
            int smscount = 0;
            int mailcount = 0;
            List<string> phoneNumbers = new List<string>();
            List<string> eids = new List<string>();
            string emailSubject = string.Empty;
            string MSGTemplate = string.Empty;

            if (CurrentSession.IsMember.ToUpper() == "TRUE")
            {
                mMember member = new mMember();
                member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
                member = (mMember)Helper.ExecuteService("Member", "GetMemberDetailsById", member);

                if (CurrentSession.MemberDesignation == "Member Legislative Assembly")
                {
                    emailSubject = "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + " " + CurrentSession.ConstituencyName + ", HP";
                }
                else
                {
                    emailSubject = "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", HPVS";
                }


                MSGTemplate += MsgBody;
                if (IsEmail == "true")
                {
                    MSGTemplate += "<br />";
                }
                else
                {
                    MSGTemplate += Environment.NewLine;
                }
                if (CurrentSession.IsMember.ToUpper() == "FALSE" && CurrentSession.SubUserTypeID == "22")// For User under working of Member
                {
                    MSGTemplate += "" + CurrentSession.Designation + " to Hon'ble " + CurrentSession.MemberDesignation + ", HP Vidhan Sabha";
                }
                else
                {
                    if (CurrentSession.MemberDesignation == "Speaker" || CurrentSession.MemberDesignation == "Deputy Speaker")
                    {
                        MSGTemplate += "" + CurrentSession.MemberDesignation + ", HP Vidhan Sabha";
                    }
                    else
                    {
                        MSGTemplate += "" + CurrentSession.MemberDesignation + ", HP";
                    }
                }
            }
            else
            {
                MSGTemplate = MsgBody;

            }
            string[] emails = EmailIds.Split(',');

            for (int i = 0; i < emails.Length; i++)
            {
                if (!string.IsNullOrEmpty(emails[i]))
                {
                    eids.Add(emails[i]);
                    mailcount++;
                }
            }

            string[] mobnos = MobileNos.Split(',');

            for (int i = 0; i < mobnos.Length; i++)
            {
                if (!string.IsNullOrEmpty(mobnos[i]))
                {
                    var ismob = ValidationAtServer.CheckMobileNumber(mobnos[i].Trim());
                    if (Convert.ToBoolean(ismob) == true)
                    {
                        phoneNumbers.Add(mobnos[i].Trim());
                        smscount++;
                    }
                }
            }

            if (smscount > SMSLeft)
            {
                msg = "You Have Only <span class='badge badge-warning'>" + SMSLeft + "</span> Left and You are Sending SMS to <span class='badge badge-purple'>" + smscount + "</span> Receipient. Please Minimize the SMS Receipient List";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }

            if (IsSms == "true")
            {
                if (phoneNumbers.Count > 0)
                {
                }
                else
                {
                    msg = "No phone numbers found";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
            if (IsEmail == "true")
            {
                if (eids.Count > 0)
                {
                }
                else
                {
                    msg = "No Email Id found";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }

            Email.API.CustomMailMessage emailmsg = new Email.API.CustomMailMessage();
            emailmsg._body = MSGTemplate;
            emailmsg._isBodyHtml = true;
            emailmsg._from = CurrentSession.MemberEmail;
            emailmsg._subject = emailSubject;
            foreach (var em in eids)
            {
                emailmsg._toList.Add(em.Trim());
            }

            SMSMessageList smsMessage = new SMSMessageList();
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = Convert.ToInt64(CurrentSession.AadharId);
            smsMessage.SMSText = MSGTemplate;
            smsMessage.isLocale = Convert.ToBoolean(chklocal);

            foreach (var item in phoneNumbers)
            {
                smsMessage.MobileNo.Add(item.Trim());
            }

            Notification.Send(Convert.ToBoolean(IsSms), Convert.ToBoolean(IsEmail), smsMessage, emailmsg);

            msg = "Total (" + smscount + ") SMS & (" + mailcount + ") Email Sent Successfully";

            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        private DateTime GetFirstDayOfMonth(int iMonth)
        {
            DateTime dtFrom = new DateTime(DateTime.Now.Year, iMonth, 1);

            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));

            return dtFrom;
        }

        private DateTime GetLastDayOfMonth(int iMonth)
        {
            DateTime dtTo = new DateTime(DateTime.Now.Year, iMonth, 1);

            dtTo = dtTo.AddMonths(1);

            dtTo = dtTo.AddDays(-(dtTo.Day));

            return dtTo;
        }

        [HttpGet]
        public JsonResult GetSpentSMSByMember()
        {
            int SentSmsCount = 0;
            int CurrentMonth = DateTime.Today.Month;

            string FirstDay = GetFirstDayOfMonth(CurrentMonth).ToShortDateString();
            // get day of week for first day

            string[] dateParts = FirstDay.Split('/');

            DateTime dtFirstTemp = new

                DateTime(Convert.ToInt32(dateParts[2]),

                Convert.ToInt32(dateParts[0]),

                Convert.ToInt32(dateParts[1]));

            string LastDay = GetLastDayOfMonth(CurrentMonth).ToShortDateString();

            // get day of week for last day

            string[] dateParts2 = LastDay.Split('/');

            DateTime dtLastTemp = new

                DateTime(Convert.ToInt32(dateParts2[2]),

                Convert.ToInt32(dateParts2[0]),

                Convert.ToInt32(dateParts2[1]));

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@from", dtFirstTemp.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@to", dtLastTemp.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", "1"));

            if (CurrentSession.IsMember.ToUpper() == "FALSE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login
            {
                methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.MemberUserID));
            }
            else
            {
                //methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.AadharId));
                methodParameter.Add(new KeyValuePair<string, string>("@UserID", (!string.IsNullOrEmpty(CurrentSession.AadharId) ? CurrentSession.AadharId : "999999999999")));
            }
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "getAllSMSByUserID", methodParameter);

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                SentSmsCount = dataSet.Tables[0].Rows.Count;
            }


            return Json(SentSmsCount, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetMaxLimitSMSByMember()
        {
            mMember model = new mMember();
            if (CurrentSession.IsMember.ToUpper() == "FALSE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login and admin
            {
                model.AadhaarNo = CurrentSession.AadharId;
                CurrentSession.IsMember = "true";
                model.isAlive = Convert.ToBoolean(CurrentSession.IsMember);
            }
            else
            {
                model.AadhaarNo = (!string.IsNullOrEmpty(CurrentSession.MemberUserID) ? CurrentSession.MemberUserID : "999999999999");

                model.isAlive = (!string.IsNullOrEmpty(CurrentSession.IsMember) ? Convert.ToBoolean(CurrentSession.IsMember) : false);
                //model.isAlive = Convert.ToBoolean(CurrentSession.IsMember);

            }


            model.DistrictID = DateTime.Today.Month;
            var MaxLimityCount = (int)Helper.ExecuteService("ContactGroups", "GetMAXLimitByMember", model);

            var ExtendMaxLimityCount = 0;
            if (CurrentSession.IsMember.ToUpper() == "TRUE")
            {
                model.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
                ExtendMaxLimityCount = (int)Helper.ExecuteService("ContactGroups", "GetExtendSMSLimitByMember", model);
            }
            else { ExtendMaxLimityCount = 0; }

            var total = MaxLimityCount + ExtendMaxLimityCount;

            if (CurrentSession.IsMember.ToUpper() == "TRUE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login
            {
                CurrentSession.IsMember = "false";
            }
            return Json(total, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetMembersForALLGroups()
        {
            RecipientGroupMember model = new RecipientGroupMember();

            List<RecipientGroupMember> modelList = new List<RecipientGroupMember>();
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            model.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            //modelList = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetAllMembersForAllGroup", model);
            ViewBag.GroupMembers = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetAllMembersForAllGroup", model);

            return PartialView("_GroupMemberListmdl", model);
        }
        #endregion
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveAssemblyFile(AssemblyFileViewModel model, HttpPostedFileBase file)
        {


            try
            {



                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                // var domainmodel = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    //string FileName = file.FileName;

                    var data = model.ToDomainModel();

                    data.CreatedBy = CurrentSession.UserID;
                    data.ModifiedBy = CurrentSession.UserID;
                    if (file != null)
                    {
                        data.UploadFile = UploadPhoto(file, model);
                    }
                    else
                    {
                        data.UploadFile = null;
                    }
                    Helper.ExecuteService("AssemblyFileSystem", "AddAssemblyFile", data);

                }
                else
                {
                    var data = model.ToDomainModel();
                    //data.AssemblyId = assmebly.AssemblyId;

                    data.ModifiedBy = CurrentSession.UserID;
                    if (file != null && !string.IsNullOrEmpty(data.UploadFile))
                    {
                        var oldfilepath = data.UploadFile.Replace('/', '\\');
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                        RemoveExistingFile(FileSettings.SettingValue + oldfilepath);
                    }
                    if (file != null)
                        data.UploadFile = UploadPhoto(file, model);
                    Helper.ExecuteService("AssemblyFileSystem", "UpdateAssemblyFile", data);
                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateAssemblyFile");
                }
                else
                {
                    return RedirectToAction("GetAllAssemblyFiles");
                }
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Assembly File");
                ViewBag.ErrorMessage = "Their is a Error While Saving the AssemblyFile Details";
                return View("AdminErrorPage");
            }


        }

        public ActionResult EditAssmeblyFile(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                tAssemblyFiles data = new tAssemblyFiles();
                //data.AssemblyFileId = Id;
                data.AssemblyFileId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString()));
                var serData = (tAssemblyFiles)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyFileById", data);
                var reldata = serData.ToViewModel1("Edit");
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession sesdata = new mSession();
                sesdata.AssemblyID = reldata.VAssemblyId;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", sesdata);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = reldata.VSessionId;
                sesdate.AssemblyId = reldata.VAssemblyId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                // Code for DoumentTypeList
                var doctypeList = (List<mAssemblyTypeofDocuments>)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyTypeofDocumentLst", null);
                reldata.SessionDateList = sessionDateList;
                reldata.AssemblyList = assmeblyList;
                reldata.SessionList = sessionList;
                reldata.AssemblyDocumentTypeList = doctypeList;
                reldata.StatusTypeList = ModelMapping.GetStatusTypes();

                return View("CreateAssemblyFile", reldata);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Edit Assembly File");
                ViewBag.ErrorMessage = "Their is a Error While Editing AssmeblyFile Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult DeleteAssemblyFile(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                tAssemblyFiles data = new tAssemblyFiles();
                data.AssemblyFileId = Id;
                // var serData = (tAssemblyFiles)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyFileById", data);
                // RemoveExistingFile(serData

                var isSucess = (bool)Helper.ExecuteService("AssemblyFileSystem", "DeleteAssemblyFileById", data);

                return RedirectToAction("GetAllAssemblyFiles");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Deleting Assembly File");
                ViewBag.ErrorMessage = "Their is a Error While Deleting the assembly Files Details";
                return View("AdminErrorPage");
            }



        }


        private string UploadPhoto(HttpPostedFileBase File, AssemblyFileViewModel model)
        {
            try
            {

                if (File != null)
                {
                    // var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    //var disFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);


                    // DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~/Images/News/"));//C:\DIS_FileStructure\Notices
                    //Code for Session Date Folder creation
                    DirectoryInfo AssemblyDir = new DirectoryInfo(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId);  //"C:\\DIS_FileStructure\\Notices");
                    //DirectoryInfo MDir = new DirectoryInfo(disFileSettings.SettingValue);
                    if (!AssemblyDir.Exists)
                    {
                        AssemblyDir.Create();
                    }

                    //Code for Session Date Folder creation
                    DirectoryInfo sessionDir = new DirectoryInfo(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId);

                    if (!sessionDir.Exists)
                    {
                        sessionDir.Create();
                    }


                    if (model.VTypeofDocumentId != 1 && model.VTypeofDocumentId != 2)
                    {
                        //Code for Session Date Folder creation
                        string[] sessionDateFile = new string[3];
                        var sessioondatestring = "";
                        if (model.VSessionDate.Contains("/"))
                        {
                            sessionDateFile = model.VSessionDate.Split('/');
                        }
                        else if (model.VSessionDate.Contains("-"))
                        {
                            sessionDateFile = model.VSessionDate.Split('-');
                        }
                        else if (model.VSessionDate.Contains(" "))
                        {
                            sessionDateFile = model.VSessionDate.Split(' ');
                        }

                        if (Convert.ToInt16(sessionDateFile[1]) < 10)
                        {
                            sessionDateFile[1] = "0" + sessionDateFile[1];
                        }
                        if (Convert.ToInt16(sessionDateFile[0]) < 10)
                        {
                            sessionDateFile[0] = "0" + sessionDateFile[0];
                        }

                        sessioondatestring = sessionDateFile[2] + sessionDateFile[1] + sessionDateFile[0];
                        DirectoryInfo sessiondateDir = new DirectoryInfo(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + sessioondatestring);

                        if (!sessiondateDir.Exists)
                        {
                            sessiondateDir.Create();
                        }


                        //Code for Session Date Folder File Existense Check

                        var filepath = System.IO.Path.Combine(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + sessioondatestring + "\\" + model.VTypeofDocumentId + ".pdf");

                        if (System.IO.File.Exists(filepath))
                        {
                            // Remove Existence File

                            RemoveExistingFile(filepath);
                            File.SaveAs(filepath);

                            var path = "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + sessioondatestring + "\\" + model.VTypeofDocumentId + ".pdf";
                            return path.Replace('\\', '/');
                        }
                        else
                        {
                            File.SaveAs(filepath);

                            var path = "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + sessioondatestring + "\\" + model.VTypeofDocumentId + ".pdf";
                            return path.Replace('\\', '/');

                        }

                    }
                    else
                    {

                        var filepath = System.IO.Path.Combine(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + model.VTypeofDocumentId + ".pdf");

                        if (System.IO.File.Exists(filepath))
                        {
                            // Remove Existence File

                            RemoveExistingFile(filepath);
                            File.SaveAs(filepath);
                            var path = "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + model.VTypeofDocumentId + ".pdf";
                            return path.Replace('\\', '/');
                        }
                        else
                        {
                            File.SaveAs(filepath);

                            var path = "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + model.VTypeofDocumentId + ".pdf";
                            return path.Replace('\\', '/');

                        }

                    }


                }


            }
            catch (Exception ex)
            {


                throw ex;
            }


            return null;


        }


        private void RemoveExistingFile(string OldFile)
        {

            try
            {





                // var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                //  var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                //var disFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                //  string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, OldFile);//Server.MapPath("~/Images/News/")

                if (System.IO.File.Exists(OldFile))
                {
                    System.IO.File.Delete(OldFile);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public ActionResult ChangeAssembly(int assemblyId)
        {
            try
            {


                mSession data = new mSession();
                data.AssemblyID = assemblyId;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                return Json(sessionList, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting All Assembly File");
                ViewBag.ErrorMessage = "Their is a Error While Deleting the Gallery Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult ChangeSession(int sessionId,int assemblyId)
        {

            try
            {

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionId;
                sesdate.AssemblyId = assemblyId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                return Json(sessionDateList, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Change Session of Assembly File");
                ViewBag.ErrorMessage = "Their is a Error While Change Session of Assembly Files Details";
                return View("AdminErrorPage");
            }
        }

    }
}
