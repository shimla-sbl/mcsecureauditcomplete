﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Category;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.Service.Common;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class CategoryController : Controller
    {
        //
        // GET: /Admin/Category/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        ServiceAdaptor serviceAdaptor = new ServiceAdaptor();

        //public ActionResult Index()
        //{
        //    return View();
        //}

        /// <summary>
        /// Notice Insertion GET 
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult Category()
        {
            Category category = new Category();
           // category.mode = "Insert";
            return View(category);
        }

        /// <summary>
        /// Notice insertion post
        /// </summary>
        /// <param name="notice">Notice</param>
        /// <returns>View</returns>
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Category(Category category, HttpPostedFileBase file)
        //{
            
        //    if (category.mode == "Insert")
        //    {
        //        category.ModifiedBy = new Guid(CurrentSession.UserID);
                
        //        Helper.ExecuteService("ACategory", "AddCategory", category);
        //        return RedirectToAction("CategoryList");
        //    }
        //    else if (category.mode == "Edit")
        //    {

        //        Helper.ExecuteService("ACategory", "UpdateCategory", category);
        //        return RedirectToAction("CategoryList");
        //    }
        //    else
        //        return null;
        //}


        /// <summary>
        /// News list
        /// </summary>
        /// <returns></returns>
        const int pageSize = 10;
        public ActionResult CategoryList(int PageNumber = 1, int LoopStart = 1, int LoopEnd = 5, string SearchText = "")
        {
            //ServiceParameter objserviceparameter = ServiceParameter.Create("ANews", "GetNews", null);
            List<Category> category = (List<Category>)(Helper.ExecuteService("ACategory", "GetCategory", null));
            var model = new PagedList<Category>()
            {
                List = category.Skip((PageNumber - 1) * pageSize).Take(pageSize).ToList(),
                CurrentPage = PageNumber,
                RowsPerPage = pageSize,
                TotalRecordCount = category.Count(),
                LoopEnd = LoopEnd,
                LoopStart = LoopStart,
                SearchText = SearchText
            };

            if (Request.IsAjaxRequest())
            {
                return PartialView("_Category", model);
            }
            return View(model);
        }

        /// <summary>
        /// Edit News
        /// </summary>
        /// <param name="NoticeId">Notice ID</param>
        /// <returns></returns>

        //[HttpGet]
        //public ActionResult EditCategory(int CategoryID)
        //{
        //    //ServiceParameter objserviceparameter = ServiceParameter.Create("ANews", "EditNews", NewsID);
        //    Category category = (Category)(Helper.ExecuteService("ACategory", "EditCategory", CategoryID));
        //    category.mode = "Edit";
        //    return View("Category", category);
        //    //return RedirectToAction("NewsList");
        //}

        /// <summary>
        /// Delete Notice
        /// </summary>
        /// <param name="Id"> ID</param>
        /// <returns></returns>
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteCategory(string Id)
        {
            //ServiceParameter objserviceparameter = ServiceParameter.Create("ANews", "DeleteNews", Id);
            List<Category> data = (List<Category>)Helper.ExecuteService("ACategory", "DeleteCategory", Id); ;
            return RedirectToAction("CategoryList");
        }
        


    }
}
