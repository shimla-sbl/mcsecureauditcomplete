﻿using SBL.DomainModel.ComplexModel.UserComplexModel;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.SubDivision;
using SBL.DomainModel.Models.User;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class UserCreationController : Controller
    {
        //
        // GET: /Admin/UserCreation/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NodalOfficerList()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Content = (List<UserCreation>)Helper.ExecuteService("User", "GetNodalOfficerList", null);

                //for Delete msg display logic
#pragma warning disable CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                if (TempData["deleted"] == "deleted")
#pragma warning restore CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                {
                    ViewBag.deleteMsg = "Deleted";
                }

                return View(Content);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Getting all Nodal Officer Details";
                return View("AdminErrorPage");
            }
 
        }

        public ActionResult SDMList()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Content = (List<UserCreation>)Helper.ExecuteService("User", "GetSDMList", null);

                //for Delete msg display logic
#pragma warning disable CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                if (TempData["deleted"] == "deleted")
#pragma warning restore CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                {
                    ViewBag.deleteMsg = "Deleted";
                }

                return View(Content);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Getting all Nodal Officer Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult CreateNodalOfficer()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                UserCreation model = new UserCreation();

                model.MemberList = (List<SBL.DomainModel.ComplexModel.QuestionModelCustom>)Helper.ExecuteService("Member", "GetMemberList_ByAssembly", null);

                model.mode = "Add";

                //for saved msg display logic
#pragma warning disable CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                if (TempData["saved"] == "saved") {
#pragma warning restore CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                    ViewBag.saveMsg = "Saved";
                }

                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Opening Creating Nodal Offier Page";
                return View("AdminErrorPage");
            }
        }

        public ActionResult CreateSDM()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                UserCreation model = new UserCreation();

                model.mSubDivisionList = (List<mSubDivision>)Helper.ExecuteService("User", "get_AllSubdivision", null);

                model.mode = "Add";

                //for saved msg display logic
#pragma warning disable CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                if (TempData["saved"] == "saved")
#pragma warning restore CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                {
                    ViewBag.saveMsg = "Saved";
                }

                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Opening Creating SDM User Page";
                return View("AdminErrorPage");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveNodalOfficer(UserCreation model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                if (ModelState.IsValid)
                {
                    string ePassword = ComputeHash(model.Password, "SHA1", null);// for encyp the password

                    List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
                    mParameter.Add(new KeyValuePair<string, string>("@UserName", model.UserName.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@Password", ePassword.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@AdhaarId", model.AadhaarId.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@Mobile", model.Mobile.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@Gender", model.Gender.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@DeptId", model.DepartmentId.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@OfficeId", model.OfficeId.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@MemberCode", model.MemberCode.ToString()));


                    DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GenerateNODALOfficerLogins", mParameter);

                    TempData["saved"] = "saved";                    
                    return RedirectToAction("CreateNodalOfficer", "UserCreation");
                }
                else
                {
                    ModelState.AddModelError("required", "Please select and enter all fields");

                    model.MemberList = (List<SBL.DomainModel.ComplexModel.QuestionModelCustom>)Helper.ExecuteService("Member", "GetMemberList_ByAssembly", null);
                    model.mode = "Add";
                    return View("CreateNodalOfficer", model);
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Nodal Offier User";
                return View("AdminErrorPage");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveSDM(UserCreation model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                if (ModelState.IsValid)
                {
                    string ePassword = ComputeHash(model.Password, "SHA1", null);// for encyp the password

                    List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
                    mParameter.Add(new KeyValuePair<string, string>("@UserName", model.UserName.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@Password", ePassword.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@AdhaarId", model.AadhaarId.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@Mobile", model.Mobile.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@Gender", model.Gender.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@DeptId", model.DepartmentId.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@OfficeId", model.OfficeId.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@SubDivisionID", model.SubdivisionId.ToString()));


                    DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GenerateSDMOfficerLogins", mParameter);

                    TempData["saved"] = "saved";
                    return RedirectToAction("CreateSDM", "UserCreation");
                }
                else
                {
                    ModelState.AddModelError("required", "Please select and enter all fields");

                    //model.MemberList = (List<SBL.DomainModel.ComplexModel.QuestionModelCustom>)Helper.ExecuteService("Member", "GetMemberList_ByAssembly", null);
                    model.mode = "Add";
                    return View("CreateSDM", model);
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Saving SDM User";
                return View("AdminErrorPage");
            }
        }

        public ActionResult OfficeDrpList(string deptid)
        {
            mUsers user = new mUsers();
            user.DeptId = deptid;
            var OfficeDrpList = (ICollection<mOffice>)Helper.ExecuteService("Office", "GetOfficeListByDepartmentID", user);
            //MaxJsonLength = Int32.MaxValue

            return new JsonResult()
            {
                Data = OfficeDrpList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            };

            // return Json(OfficeDrpList, JsonRequestBehavior.AllowGet);
        }

        #region Encryptio/Decrytption

        public static string ComputeHash(string plainText, string hashAlgorithm, byte[] saltBytes)
        {
            // If salt is not specified, generate it on the fly.
            if (saltBytes == null)
            {
                // Define min and max salt sizes.
                int minSaltSize = 4;
                int maxSaltSize = 8;

                // Generate a random number for the size of the salt.
                Random random = new Random();
                int saltSize = random.Next(minSaltSize, maxSaltSize);

                // Allocate a byte array, which will hold the salt.
                saltBytes = new byte[saltSize];

                // Initialize a random number generator.
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

                // Fill the salt with cryptographically strong byte values.
                rng.GetNonZeroBytes(saltBytes);
            }

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes =
                    new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];


            HashAlgorithm hash;
            // Make sure hashing algorithm name is specified.
            if (hashAlgorithm == null)
                hashAlgorithm = "";

            // Initialize appropriate hashing algorithm class.
            switch (hashAlgorithm.ToUpper())
            {
                case "SHA1":
                    hash = new SHA1Managed();
                    break;

                case "SHA256":
                    hash = new SHA256Managed();
                    break;

                case "SHA384":
                    hash = new SHA384Managed();
                    break;

                case "SHA512":
                    hash = new SHA512Managed();
                    break;

                default:
                    hash = new MD5CryptoServiceProvider();
                    break;
            }

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(hashWithSaltBytes);

            // Return the result.
            return hashValue;
        }
        #endregion

        public JsonResult Check_Aadharcode(string Aadharcode)
        {
            string checkedData = Helper.ExecuteService("User", "CheckAadhaarID", Aadharcode) as string;
            
            return Json(checkedData, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DeleteNodalUser(string AadharID)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();

                mParameter.Add(new KeyValuePair<string, string>("@AdhaarId", AadharID));

                DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteConstituencyLogins", mParameter);

                TempData["deleted"] = "deleted";
                return RedirectToAction("NodalOfficerList", "UserCreation");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While deleting Nodal";
                return View("AdminErrorPage");
            }
        }

        public ActionResult DeleteSDMUser(string AadharID)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();

                mParameter.Add(new KeyValuePair<string, string>("@AdhaarId", AadharID));

                DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteConstituencyLogins", mParameter);

                TempData["deleted"] = "deleted";
                return RedirectToAction("SDMList", "UserCreation");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While deleting sdm user";
                return View("AdminErrorPage");
            }
        }
    }
}
