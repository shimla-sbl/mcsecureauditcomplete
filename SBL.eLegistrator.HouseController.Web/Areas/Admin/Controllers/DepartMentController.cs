﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Category;
using SBL.DomainModel.Models.Content;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.ComplexModel;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class DepartMentController : Controller
    {
        //
        // GET: /Admin/DepartMent/

        public ActionResult Index()
        {
            return View();
        }
        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "ContentError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }
        public ActionResult CreateDepartMentDetails()
        {
            ActionResult result;
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    result = base.RedirectToAction("LoginUP", "Account", new { area = "" });
                }
                else
                {
                    DepartmnetDetailsViewModel model1 = new DepartmnetDetailsViewModel();
                    model1.mode = "Add";
                    model1.isActive = true;
                    DepartmnetDetailsViewModelList list1 = new DepartmnetDetailsViewModelList();
                    list1.lstDepartmnetDetailsViewModel = model1;
                    list1.StatusTypeList = DepartmnetDetailsViewModel.GetStatusTypes();
                    list1.deptList = DepartmnetDetailsViewModel.getDepartment();
                    list1.CategoryList = DepartmnetDetailsViewModel.GetCategoryTypes();
                    DepartmnetDetailsViewModelList model = list1;
                    result = base.View(model);
                }
            }
            catch (Exception ex)
            {
                RecordError(ex, "Create DepartMent Details");
                ViewBag.ErrorMessage = "Their is a Error While Creating DepartMent Details";
                return View("AdminErrorPage");




            }
            return result;
        }

        public ActionResult CreateDocument()
        {
            ActionResult result;
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    result = base.RedirectToAction("LoginUP", "Account", new { area = "" });
                }
                else
                {
                   
                    DocumentViewModel model1 = new DocumentViewModel();
                    model1.Mode = "Add";
                    model1.StatusTypeList = ModelMapping.GetStatusTypes();
                    model1.IsActive = 1;
                    model1.IsDeleted = false;
               
                    model1.CategoryList = DepartmnetDetailsViewModel.GetAllCategoryDetails();
                    model1.ProjectList = DepartmnetDetailsViewModel.GetAllDepartmentDetails(0);
                    ViewBag.FileAcessingPath = "/DepartmentDetails/Doc/;";
                    return View(model1);


                }
            }
            catch (Exception ex)
            {
                RecordError(ex, "Create DepartMent Document");
                ViewBag.ErrorMessage = "Their is a Error While Creating DepartMent Document";
                return View("AdminErrorPage");




            }
            return result;
        }

        public ActionResult CreateImage()
        {
            ActionResult result;
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    result = base.RedirectToAction("LoginUP", "Account", new { area = "" });
                }
                else
                {
                  
                    ImageViewModel model1 = new ImageViewModel();
                    model1.Mode = "Add";
                    model1.StatusTypeList = ModelMapping.GetStatusTypes();
                    model1.IsActive = 1;
                    model1.IsDeleted = false;
                model1.CategoryList= DepartmnetDetailsViewModel.GetAllCategoryDetails();
                    model1.ProjectList = DepartmnetDetailsViewModel.GetAllDepartmentDetails(0);
                    ImageViewModel model = model1;

                    result = base.View(model);
                }
            }
            catch (Exception ex)
            {
                RecordError(ex, "Create DepartMent Image");
                ViewBag.ErrorMessage = "Their is a Error While Creating DepartMent Image";
                return View("AdminErrorPage");



            }
            return result;
        }

        public ActionResult DeleteDepartmentDetailst(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                else
                {
                    DepartmnetDetailsViewModel.DeleteDepartmentDetailsById(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));
                 

                    return RedirectToAction("DepartmentDetails");

                }
            }
            catch (Exception ex)
            {
                RecordError(ex, "Deleting DepartMent Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting  DepartMent Deails";
                return View("AdminErrorPage");


            }



        }

        public ActionResult DeleteDocument(string Id)
        {
           
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { area = "" });
                }
                else
                {
                    DepartmnetDetailsViewModel.DeleteDocumentById(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));
                    return RedirectToAction("DocumentList");
                }
            }
            catch (Exception ex)
            {
                RecordError(ex, "Deleting DepartMent Document Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting DepartMent Document Deails";
                return View("AdminErrorPage");
            }

              
        }


        public ActionResult DeleteImage(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { area = "" });
                }
                else
                {

                    ImageViewModel.DeleteDeptImageById(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));

                    return RedirectToAction("ImageList");
                }
            }
            catch (Exception ex)
            {

                RecordError(ex, "Deleting DepartMent Image Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting DepartMent Image Deails";
                return View("AdminErrorPage");


            }

        }
        public ActionResult DepartmentDetails()
        {
            try
            {
                return (!string.IsNullOrEmpty(CurrentSession.UserID) ? ((ActionResult)base.View(DepartmnetDetailsViewModel.GetDepartmentDetails())) : ((ActionResult)base.RedirectToAction("LoginUP", "Account", new { area = "" })));
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting All DepartMent");
                ViewBag.ErrorMessage = "Their is a Error While Getting All DepartMent";
                return View("AdminErrorPage");                
            }
        }


        public ActionResult DocumentList()
        {
            try
            {
                return (!string.IsNullOrEmpty(CurrentSession.UserID) ? ((ActionResult)base.View(DocumentViewModel.GetDeptDocumentList())) : ((ActionResult)base.RedirectToAction("LoginUP", "Account", new { area = "" })));
            }
            catch (Exception ex)
            {
                RecordError(ex, "Getting All DepartMent Document List");
                ViewBag.ErrorMessage = "Their is a Error While Getting All DepartMent Document List";
                return View("AdminErrorPage");
                               
            }
        }




       


        public ActionResult EditDepartmentDetails(string Id)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return base.RedirectToAction("LoginUP", "Account", new { area = "" });
            }
            DepartmnetDetailsViewModelList list1 = new DepartmnetDetailsViewModelList();
            list1.lstDepartmnetDetailsViewModel = DepartmnetDetailsViewModel.GetDepartmentDetailsById((long)Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString(), "qwerty12345")));
            list1.StatusTypeList = DepartmnetDetailsViewModel.GetStatusTypes();
            list1.deptList = DepartmnetDetailsViewModel.getDepartment();
            list1.CategoryList = DepartmnetDetailsViewModel.GetCategoryTypes();
            DepartmnetDetailsViewModelList model = list1;
            return base.View("CreateDepartMentDetails", model);
        }




   


        public ActionResult EditDocument(string Id)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return base.RedirectToAction("LoginUP", "Account", new { area = "" });
            }
            DocumentViewModel deptDocumentListById = DocumentViewModel.GetDeptDocumentListById(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString(), "qwerty12345")));
            deptDocumentListById.Mode = "Edit";
            deptDocumentListById.StatusTypeList = ModelMapping.GetStatusTypes();
            deptDocumentListById.ProjectList = DepartmnetDetailsViewModel.GetAllDepartmentDetails(deptDocumentListById.catId);
            deptDocumentListById.CategoryList = DepartmnetDetailsViewModel.GetAllCategoryDetails();
            return base.View("CreateDocument", deptDocumentListById);
        }



        public ActionResult EditImage(string Id)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return base.RedirectToAction("LoginUP", "Account", new { area = "" });
            }
            ImageViewModel deptImageListById = ImageViewModel.GetDeptImageListById(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString(), "qwerty12345")));
            deptImageListById.Mode = "Edit";
            deptImageListById.StatusTypeList = ModelMapping.GetStatusTypes();
            deptImageListById.ProjectList = DepartmnetDetailsViewModel.GetAllDepartmentDetails(deptImageListById.catId);
            deptImageListById.CategoryList = DepartmnetDetailsViewModel.GetAllCategoryDetails();
            return base.View("CreateImage", deptImageListById);
        }




     



        public ActionResult ImageList()
        {
            try
            {
                return (!string.IsNullOrEmpty(CurrentSession.UserID) ? ((ActionResult)base.View(ImageViewModel.GetDeptImageList())) : ((ActionResult)base.RedirectToAction("LoginUP", "Account", new { area = "" })));
            }
            catch (Exception ex)
            {
                RecordError(ex, "Getting All DepartMent Image List");
                ViewBag.ErrorMessage = "Their is a Error While Getting All DepartMent Image List";
                return View("AdminErrorPage");
                               
            }
        }





        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveDepartMent(DepartmnetDetailsViewModelList model)
        {
      
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                   return RedirectToAction("LoginUP", "Account", new { area = "" });
                }
                else
                {
                    if (model.lstDepartmnetDetailsViewModel.mode == "Add")
                    {
                        DepartmnetDetailsViewModel.SaveDepartmentDetails(model.lstDepartmnetDetailsViewModel);
                    }
                    else
                    {
                        DepartmnetDetailsViewModel.SaveDepartmentDetails(model.lstDepartmnetDetailsViewModel);
                    }
                    return RedirectToAction("DepartmentDetails");
                }
            }
            catch (Exception ex)
            {
                RecordError(ex, "Saving DepartMent Details");
                ViewBag.ErrorMessage = "Their is a Error While Saving DepartMent Details";
                return View("AdminErrorPage");
            }                
        }



        [HttpPost, ValidateAntiForgeryToken]
   
        public ActionResult SaveDocument(DocumentViewModel model)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string path = fileAcessingSettings.SettingValue + "\\DepartmentDetails\\Doc";


            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                if (model.Mode == "Add")
                {
                    // model.IsActive = true;
                    model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);



                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }



                    byte[] bytes1 = null;
                    string type = "";

                    string[] filenamearr = model.DocumentFileName.Split('.');
                    string FileName = filenamearr[0] + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond) + "." + filenamearr[1];
                    ConvertBase64StringToBytes(model.DocumentLocation, ref bytes1, ref type);
                    System.IO.File.WriteAllBytes(path + "/" + FileName, bytes1);
                    model.DocumentLocation = "DepartmentDetails/Doc/" + FileName;
                    DocumentViewModel.SaveDocumentDepartment(model);
                }
                else
                {
                    if (!string.IsNullOrEmpty(model.DocumentFileName))
                    {
                        // model.CreatedBy = Guid.Parse(CurrentSession.UserID);




                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        byte[] bytes1 = null;
                        string type = "";

                        string[] filenamearr = model.DocumentFileName.Split('.');
                        string FileName = filenamearr[0] + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond) + "." + filenamearr[1];
                        ConvertBase64StringToBytes(model.DocumentLocation, ref bytes1, ref type);
                        System.IO.File.WriteAllBytes(path + "/" + FileName, bytes1);
                        model.DocumentLocation = "DepartmentDetails\\Doc\\" + FileName;
                    }
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);

                    DocumentViewModel.SaveDocumentDepartment(model);
                    //var result = model.ToDomainModel();
                    //result.GalleryId = model.GalleryImageId.HasValue ? model.GalleryImageId.Value : 0;
                    //Helper.ExecuteService("Gallery", "UpdateGalleryCategory", result);
                }

                //if (model.IsCreatNew)
                //{
                //    return RedirectToAction("CreateGallery");
                //}
                //else
                //{
                //    return RedirectToAction("GalleryCategoryIndex");
                //}
                return RedirectToAction("DocumentList");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Department Document");
                ViewBag.ErrorMessage = "Their is a Error While saving the department Document";
                return View("AdminErrorPage");
            }

            // return RedirectToAction("GalleryCategoryIndex");
        }

        private static void ConvertBase64StringToBytes(string base64String, ref byte[] bytes1, ref string Type)
        {


            string[] split = base64String.Split(',');


            bytes1 = System.Convert.FromBase64String(base64String.Substring(base64String.IndexOf(',') + 1));

            if (base64String != "")
            {
                split = base64String.Split(',');
                Type = split[0];

            }


            //new SqlParameter("@filescannedBankGurnthdn",bytes1),

            //   new SqlParameter("@Type",Type),
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveImage(ImageViewModel model)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);



            string path = fileAcessingSettings.SettingValue + "\\DepartmentDetails\\Image\\";

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                if (model.Mode == "Add")
                {
                    // model.IsActive = true;
                    model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }



                    byte[] bytes1 = null;
                    string type = "";

                    string[] filenamearr = model.ImageFileName.Split('.');
                    string FileName = filenamearr[0] + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond) + "." + filenamearr[1];
                    ConvertBase64StringToBytes(model.ImageLocation, ref bytes1, ref type);
                    System.IO.File.WriteAllBytes(path + "/" + FileName, bytes1);
                    model.ImageLocation = "DepartmentDetails/Image/" + FileName;
                    ImageViewModel.SaveDeptImage(model);
                }
                else
                {
                    // model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);

                    if (!string.IsNullOrEmpty(model.ImageFileName))
                    {

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }



                        byte[] bytes1 = null;
                        string type = "";

                        string[] filenamearr = model.ImageFileName.Split('.');
                        string FileName = filenamearr[0] + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond) + "." + filenamearr[1];
                        ConvertBase64StringToBytes(model.ImageLocation, ref bytes1, ref type);
                        System.IO.File.WriteAllBytes(path + "/" + FileName, bytes1);
                        model.ImageLocation = "DepartmentDetails/Image/" + FileName;
                    }
                    ImageViewModel.SaveDeptImage(model);
                    //var result = model.ToDomainModel();
                    //result.GalleryId = model.GalleryImageId.HasValue ? model.GalleryImageId.Value : 0;
                    //Helper.ExecuteService("Gallery", "UpdateGalleryCategory", result);
                }


                return RedirectToAction("ImageList");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Image");
                ViewBag.ErrorMessage = "Their is a Error While saving the Department Image";
                return View("AdminErrorPage");
            }

            // return RedirectToAction("GalleryCategoryIndex");
        }


        [HttpGet]
        public ActionResult GetDeptByCatFilter(string catId)
        {
            SelectList dept = DepartmnetDetailsViewModel.GetAllDepartmentDetailsFilter(Convert.ToInt64(catId));




            return Json(dept, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetDeptByCat(string catId)
        {
          SelectList dept=  DepartmnetDetailsViewModel.GetAllDepartmentDetails(Convert.ToInt64(catId));



            
            return Json(dept, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetDeptByCatByDept(string catId)
        {
            SelectList dept = DepartmnetDetailsViewModel.GetAllDepartmentDetails(Convert.ToInt64(catId),CurrentSession.DeptID);




            return Json(dept, JsonRequestBehavior.AllowGet);
        }

    }
}
