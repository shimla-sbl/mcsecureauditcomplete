﻿using EvoPdf;
using Ionic.Zip;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Loan;
using SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class ManageLoanController : Controller
    {
        //
        // GET: /Admin/ManageLoan/

        public ActionResult Index()
        {
            return View();
        }
        #region stateMent

        public ActionResult loanStatement()
        {
            return View();
        }
        public ActionResult loanStatementPartial()
        {
            return PartialView("_loanStatement");
        }
        public ActionResult getActiveLoanListforStatement(string id)
        {
            List<loanSearchResult> _loanSearchResult = (List<loanSearchResult>)Helper.ExecuteService("loan", "getActiveLoanList", null);
            return PartialView("_currentActiveLoanListforStatement", _loanSearchResult);
        }
        public ActionResult statementforLoan(string loanID)
        {
            LoanStatement list = (LoanStatement)Helper.ExecuteService("loan", "statementforLoan", loanID);
            return PartialView("_statement", list);
        }
        public ActionResult getLoanInstallmentDetails(string autoid)
        {
            installmentDetails list = (installmentDetails)Helper.ExecuteService("loan", "getLoanDetailsByInstAutoID", autoid);
            list.AutoID = Convert.ToInt64(autoid);
            return PartialView("_updateLoanInstallment", list);
        }
        public ActionResult deleteLoanInstallment(string autoid)
        {
            long loannumber = (long)Helper.ExecuteService("loan", "deleteLoanInst", autoid);
            LoanStatement list = (LoanStatement)Helper.ExecuteService("loan", "statementforLoan", loannumber);
            return PartialView("_statement", list);

        }
        public ActionResult updateLoanInst(installmentDetails inst)
        {
            try
            {
                Helper.ExecuteService("loan", "updateLoanInst", inst);

            }
            catch (Exception ex)
            {
                return Content(inst.AutoID + ' ' + ex.InnerException.Message);
            }
            //return View();
            LoanStatement list = (LoanStatement)Helper.ExecuteService("loan", "statementforLoan", inst.loanNumber);
            return PartialView("_statement", list);
        }
        public ActionResult generateLonaPdf(string LoaNNumber)
        {
            LoanStatement list = (LoanStatement)Helper.ExecuteService("loan", "statementforLoan", LoaNNumber);
            loanDetailsViewModal view = new loanDetailsViewModal();
            installmentDetails ilist = (installmentDetails)Helper.ExecuteService("loan", "getLoanDetailsByLoanID", LoaNNumber);
            List<disbursementDetails> dslit = (List<disbursementDetails>)Helper.ExecuteService("loan", "getDisburmentListByID", LoaNNumber);
            view._disbursmentslist = dslit;
            string msg = string.Empty;
            int count = 0, counta = 0;
            double prec = 0, irec = 0, ipaid = 0, outi = 0;
            MemoryStream output = new MemoryStream();

            string outXml = "<html><body  style=\"font-family:DVOT-Yogesh; font-size: 12px;margin-right:25px;margin-left: 25px;\"><div><div  style=\"width: 100%;\">";
            outXml += "<table  style=\"width: 100%;font-size: 12px;\">";
            outXml += "<tr><td  style=\"text-align: center;\" colspan=\"8\"><b>Establishment of HIMACHAL PRADESH VIDHAN SABHA SHIMLA-171004.</b></td>";
            outXml += "</tr>";
            outXml += "<tr>";
            outXml += "<td colspan=\"8\"  style=\"text-align: center;\">" + ilist.loanType + " Loan Recovery Statement</td>";
            outXml += "</tr>";
            outXml += "<tr>";
            outXml += "<td colspan=\"2\"></td>";
            outXml += "<td>Name : " + ilist.memberName + " </td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "</tr>";
            outXml += "<tr>";
            outXml += "<td colspan=\"2\"></td>";
            outXml += "<td>Address</td>";
            outXml += "<td>" + ilist.Address + " </td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "<td></td>";
            outXml += "</tr>";
            outXml += "<tr><td colspan=\"3\">Total Loan Amount - " + ilist.loanAmount + ".00</td><td>Installment Fixed - " + ilist.noOFEMI + "</td><td>Loan Account Number - " + ilist.loanAccountNumber + "</td><td></td></tr>";

            outXml += "<tr><td colspan=\"8\"><hr /></td></tr>";
            outXml += "<tr ><td colspan=\"8\" align=\"center\"><table  style=\" width:60%;font-size: 12px;\">";

            outXml += "<tr><td>Installment Number </td><td> Installment Amount</td><td>Sanctioning Date</td></tr>";

            foreach (var critem in view._disbursmentslist)
            {
                count++;

                outXml += "<tr><td>" + count + "</td><td>" + critem.disburmentAmount + ".00</td><td>" + critem.disburmentRecDate.ToString().Split(' ').GetValue(0).ToString() + "</td></tr>";
            }

            outXml += "</table></td></tr><tr><td colspan=\"8\"><hr /></td></tr><tr><td colspan=\"8\"  style=\"vertical-align: top;\"><table border=\"1\" cellpadding=\"5\"  style=\"border-collapse:collapse; width: 100%;font-size: 12px;\">";
            outXml += "<tr><td>SNo</td><td>Op. Bal.(₹)</td><td>Payment Date</td><td>Pri. Rec.(₹) </td><td>Int. Paid(₹)</td><td>Int. Rate(%)</td><td>Int. for Month(₹)</td><td>Out. Int.(₹)</td><td>Cl Bal.(₹)</td><td>Payment Details</td> <td> Voucher No-Date</td> <td>Try Name</td> <td>DDO</td> </tr>";
            foreach (var dritem in list.statement)
            {
                counta++;
                outXml += "<tr><td>" + counta + "</td><td>" + dritem.openingBalance + "</td><td>" + dritem.Date + "</td><td>" + dritem.pAmount + " </td><td>" + dritem.iAmount + " </td><td>" + dritem.intsRate + "</td><td>" + dritem.interest + "</td><td>" + dritem.outstandingint + "</td><td>" + dritem.closingBalance + "</td><td>" + dritem.PaymentMode + "</td><td>" + dritem.Voucher + "</td> <td>" + dritem.TreasuryName + "</td> <td>" + dritem.DDOCode + "</td></tr>";
                prec += dritem.pAmount;
                irec += dritem.iAmount;
                ipaid += dritem.interest;
                outi = dritem.outstandingint;
            }
            outXml += "<tr><td colspan=\"3\">Grand Total</td><td>" + prec + "</td><td>" + irec + " </td><td></td><td>" + ipaid + "</td><td>" + outi + "</td><td colspan=\"5\"></td></tr>";
            outXml += "</table></td></tr></table>";
            outXml += @"</div></div></body></html>";
            string htmlStringToConvert = outXml;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;

            pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;

            TextElement footerTextElement = new TextElement(0, 30, "Page: &p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;


            TextElement footerTextElement3 = new TextElement(-20, 30, "Disbursement Officer",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement3.TextAlign = HorizontalTextAlign.Right;


            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            //  pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            //   stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            string url = "/LoanStatement/12/" + list.memberName + "/" + list.LoaNNumber + "/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), list.LoaNNumber + "loanStatement.pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();

            DownloadPDF(url);
            return View();
        }
        public object DownloadPDF(string url)
        {
            try
            {
                //string url = "/QuestionList" + "/DiaryWiseStarred/" + "12/" + "7";

                string directory = Server.MapPath(url);

                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                    //zip.AddDirectoryByName("Files");

                    if (Directory.Exists(directory))
                    {
                        zip.AddDirectory(directory, "loan statement");
                    }
                    else
                    {
                    }

                    Response.Clear();
                    Response.BufferOutput = false;
                    string zipName = String.Format("{0}.zip", "loan statement");
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);

                    zip.Save(Response.OutputStream);
                    Response.End();

                    return Response;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return "No File Available!";
            }

#pragma warning disable CS0162 // Unreachable code detected
            return Response;
#pragma warning restore CS0162 // Unreachable code detected
        }

        #endregion stateMent

    }
}
