﻿using SBL.eLegistrator.HouseController.Web.Extensions;
using Microsoft.Security.Application;
using SBL.DomainModel.Models.Category;
using SBL.DomainModel.Models.Speech;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.LibraryVS;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class SpeechController : Controller
    {
        //
        // GET: /Admin/Speech/

        #region SpeechCategoryCode

        public ActionResult SpeechCategoryIndex()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var Speechs = (List<Category>)Helper.ExecuteService("ASpeech", "GetAllSpeechCategory", null);
                var model = Speechs.ToViewModel();
                return View("SpeechCategoryIndex", model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Get All Speech Category Details");
                ViewBag.ErrorMessage = "Their is a Error While Getting All Speech Category Deails";
                return View("AdminErrorPage");
            }



          
        }

        public ActionResult CreateSpeechCategory()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new CategoryViewModel()
                {
                    Mode = "Add",
                    TypeList = ModelMapping.GetStatusTypes(),
                    SpeechTypeList = ModelMapping.GetSpeechType(),
                    Status = 1

                };
                return View("CreateSpeechCategory", model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Create Speech Category Details");
                ViewBag.ErrorMessage = "Their is a Error While Creating Speech Category Details";
                return View("AdminErrorPage");
            }


         
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveSpeechCategory(CategoryViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                if (model.Mode == "Add")
                {
                    model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("ASpeech", "AddSpeechCategory", model.ToDomainModel());

                }
                else
                {
                    // model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("ASpeech", "UpdateSpeechCategory", model.ToDomainModel());
                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateSpeechCategory");
                }
                else
                {
                    return RedirectToAction("SpeechCategoryIndex");
                }

            }
            catch (Exception ex)
            {

                RecordError(ex, "Save Speech Category Details");
                ViewBag.ErrorMessage = "Their is a Error While Saving Speech Category Deails";
                return View("AdminErrorPage");
            }




        }

        public ActionResult GetSpeechCategoryById(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                //var News = (Category)Helper.ExecuteService("ASpeech", "GetSpeechCategoryById", new Category { CategoryID = Id });
                var News = (Category)Helper.ExecuteService("ASpeech", "GetSpeechCategoryById", new Category { CategoryID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                return View("CreateSpeechCategory", News.ToViewModel("Edit"));
            }
            catch (Exception ex)
            {

                RecordError(ex, "Edit Speech Details");
                ViewBag.ErrorMessage = "Their is a Error While Edit Speech Details";
                return View("AdminErrorPage");
            }




        }

        public ActionResult DeleteSpeechCategory(int Id)
        {


            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var News = Helper.ExecuteService("ASpeech", "DeleteSpeechCategory", new Category { CategoryID = Id });

                return RedirectToAction("SpeechCategoryIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Delete Speech Category Details");
                ViewBag.ErrorMessage = "Their is a Error While Deleting Speech Category  Details";
                return View("AdminErrorPage");
            }

        }

        #endregion

        #region SpeechCode

        public ActionResult CreateSpeech()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                //Code for AssemblyList 
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);



                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);



                var categorytype = (List<Category>)Helper.ExecuteService("ASpeech", "GetAllSpeechCategory", null);
                var model = new SpeechViewModel()
                {
                    Mode = "Add",

                    StatusTypeList = ModelMapping.GetStatusTypes(),

                    SpeechTypeList = ModelMapping.GetSpeechType(),

                    CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null),
                    VStatus = 1

                };
                model.VSpeechType = 1;
                model.VAssemblyId = data.AssemblyID;
                model.VSessionId = sesdate.SessionId;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;
                model.SessionDateList = sessionDateList;
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Create Speech Details");
                ViewBag.ErrorMessage = "Their is a Error While Create Speech Details";
                return View("AdminErrorPage");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSpeech(SpeechViewModel model, HttpPostedFileBase file, HttpPostedFileBase file1)
        {


            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    if (model.Mode == "Add")
                    {


                        if (file != null)
                        {
                            //string FileName = "English" + file.FileName;
                            string FileName = "1" + ".pdf";
                            model.VFilePath = UploadFile(file, FileName, model);
                            model.VFileName = FileName;
                        }
                        else
                        {
                            model.VFilePath = "";
                            model.VFileName = "";
                        }


                        if (file1 != null)
                        {
                            //string FileName1 = "Hindi" + file1.FileName;
                            string FileName1 = "2" + ".pdf";
                            model.VFilePath1 = UploadFile(file1, FileName1, model);
                            model.VFileName1 = FileName1;
                        }
                        else
                        {
                            model.VFilePath1 = "";
                            model.VFileName1 = "";
                        }


                        var data = model.ToDomainModel();
                        data.CreatedBy = Guid.Parse(CurrentSession.UserID);
                        data.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                        Helper.ExecuteService("ASpeech", "AddSpeech", data);
                    }
                    else
                    {

                        if (file != null)
                        {
                            if (model.VFileName != null)
                            {
                                RemoveExistingPhoto(model.VFileName);
                            }

                            //string Name = "English" + file.FileName;
                            string Name = "1" + ".pdf";
                            model.VFilePath = UploadFile(file, Name, model);
                            model.VFileName = Name;

                            //model.VFilePath = UploadFile(file, file.FileName, model);
                            //model.VFileName = file.FileName;
                        }
                        if (file1 != null)
                        {
                            if (model.VFileName1 != null)
                            {
                                RemoveExistingPhoto(model.VFileName1);
                            }

                            //string Name1 = "Hindi" + file1.FileName;
                            string Name1 = "2" + ".pdf";
                            model.VFilePath1 = UploadFile(file1, Name1, model);
                            model.VFileName1 = Name1;

                            //model.VFilePath1 = UploadFile(file1, file1.FileName, model);
                            //model.VFileName1 = file1.FileName;
                        }
                        var data = model.ToDomainModel();
                        data.CreatedBy = Guid.Parse(CurrentSession.UserID);
                        data.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                        Helper.ExecuteService("ASpeech", "UpdateSpeech", data);
                    }

                    if (model.IsCreatNew)
                    {
                        return RedirectToAction("CreateSpeech");
                    }
                    else
                    {
                        return RedirectToAction("GetSpeechList");
                    }
                }
                else
                {
                    var categorytype = (List<Category>)Helper.ExecuteService("ASpeech", "GetAllSpeechCategory", null);

                    model.StatusTypeList = ModelMapping.GetStatusTypes();

                    model.SpeechTypeList = ModelMapping.GetSpeechType();

                    model.CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null);
                    return View("CreateSpeech", model);
                }
            }
            catch (Exception ex)
            {

                RecordError(ex, "Save Speech Details");
                ViewBag.ErrorMessage = "Their is a Error While Saving Speech Details";
                return View("AdminErrorPage");
            }


        }

        public ActionResult GetSpeechById(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var categorytype = (List<Category>)Helper.ExecuteService("ASpeech", "GetAllSpeechCategory", null);
                var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                //var speech = (tSpeech)Helper.ExecuteService("ASpeech", "GetSpeechById", new tSpeech { SpeechID = Id });
                var speech = (tSpeech)Helper.ExecuteService("ASpeech", "GetSpeechById", new tSpeech { SpeechID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = speech.ToViewModel1("Edit");
                model.CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null);
                //var newPath = model.VFilePath + "\\" + model.VFileName;
                model.ImageLocation = fileAcessingSettings.SettingValue + model.VFilePath + "/" + model.VFileName;
                model.ImageLocation1 = fileAcessingSettings.SettingValue + model.VFilePath1 + "/" + model.VFileName1;

                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession sesdata = new mSession();
                sesdata.AssemblyID = model.VAssemblyId;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", sesdata);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = model.VSessionId;
                sesdate.AssemblyId = model.VAssemblyId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);


                model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;
                

                return View("CreateSpeech", model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Edit Speech Details");
                ViewBag.ErrorMessage = "Their is a Error While Editing Speech Details";
                return View("AdminErrorPage");
            }



        }


        public ActionResult DeleteSpeech(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var actualspeeches = (tSpeech)Helper.ExecuteService("ASpeech", "GetSpeechById", new tSpeech { SpeechID = Id });


                var speeches = Helper.ExecuteService("ASpeech", "DeleteSpeech", new tSpeech { SpeechID = Id });//

                if (!string.IsNullOrEmpty(actualspeeches.FileName))
                    RemoveExistingFile(actualspeeches.FilePath, actualspeeches.FileName);

                if (!string.IsNullOrEmpty(actualspeeches.FileName1))
                    RemoveExistingFile(actualspeeches.FilePath1, actualspeeches.FileName1);

                return RedirectToAction("GetSpeechList");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Delete Speech Details");
                ViewBag.ErrorMessage = "Their is a Error While Deleting Speech Details";
                return View("AdminErrorPage");
            }



        }

        public ActionResult GetSpeechList()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                var Speechs = (List<tSpeech>)Helper.ExecuteService("ASpeech", "GetAllSpeech", null);
                
                var model = Speechs.ToViewModel1();
                
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Get All Speech List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting All Speech List Details";
                return View("AdminErrorPage");
            }



        }

        public JsonResult CheckSpeechCategoryChildExist(int categoryId)
        {
            try
            {
                var isExist = (Boolean)Helper.ExecuteService("ASpeech", "IsSpeechCategoryChildExist", new Category { CategoryID = categoryId });

                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }



        }

        #endregion

        #region Private Methods

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "SpeechError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

               
            }
        }


        private string UploadFile(HttpPostedFileBase File, string FileName, SpeechViewModel model)
        {
            if (File != null)
            {
                

                var speechSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSpeechFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                
                //DirectoryInfo sessionDir = new DirectoryInfo(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId);

                //if (!sessionDir.Exists)
                //{
                //    sessionDir.Create();
                //}




                string[] sessionDateFile = new string[3];
                var sessioondatestring = "";
                if (model.VSessionDate.Contains("/"))
                {
                    sessionDateFile = model.VSessionDate.Split('/');
                }
                else if (model.VSessionDate.Contains("-"))
                {
                    sessionDateFile = model.VSessionDate.Split('-');
                }
                else if (model.VSessionDate.Contains(" "))
                {
                    sessionDateFile = model.VSessionDate.Split(' ');
                }

                //sessioondatestring = sessionDateFile[2] + sessionDateFile[1] + sessionDateFile[0];
                sessioondatestring = sessionDateFile[0] + sessionDateFile[1] + sessionDateFile[2];

                DirectoryInfo sessiondateDir = new DirectoryInfo(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + sessioondatestring);

                if (!sessiondateDir.Exists)
                {
                    sessiondateDir.Create();
                }






                string extension = System.IO.Path.GetExtension(File.FileName);
                //string path = System.IO.Path.Combine(FileSettings.SettingValue + speechSettings.SettingValue + "\\", FileName);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + sessioondatestring + "\\", FileName);

                string path1 = System.IO.Path.Combine("\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + sessioondatestring);

                
                File.SaveAs(path);

                return (path1);
                
                //return ("/Speechs");

            }
            return null;
        }

        private void RemoveExistingPhoto(string OldPhoto)
        {

            var noticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSpeechFileSetting", null);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string path = System.IO.Path.Combine(FileSettings.SettingValue + noticesSettings.SettingValue + "\\", OldPhoto);

            

            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
        }

        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult DownloadFile(string fileName)
        {
            fileName = Sanitizer.GetSafeHtmlFragment(fileName);
            var noticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSpeechFileSetting", null);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            var urlPath = FileSettings.SettingValue + noticesSettings.SettingValue + "\\" + fileName;
            Boolean isExist = System.IO.File.Exists(urlPath);
            if (isExist)
            {
                var fs = System.IO.File.OpenRead(urlPath);//(Server.MapPath(fileName));
                return File(fs, "appliaction/zip", fileName);//.Substring(fileName.LastIndexOf("/") + 1));
            }
            else
                return null;
        }



        public ActionResult ChangeAssembly(int assemblyId)
        {
            try
            {


                mSession data = new mSession();
                data.AssemblyID = assemblyId;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                return Json(sessionList, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting All Assembly File");
                ViewBag.ErrorMessage = "Their is a Error While Deleting the Gallery Details";
                return View("AdminErrorPage");
            }
        }


        public ActionResult ChangeSession(int sessionId, int assemblyId)
        {

            try
            {

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionId;
                sesdate.AssemblyId = assemblyId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                return Json(sessionDateList, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Change Session of Assembly File");
                ViewBag.ErrorMessage = "Their is a Error While Change Session of Assembly Files Details";
                return View("AdminErrorPage");
            }
        }

        private void RemoveExistingFile(string path, string file)
        {

            try
            {

                var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                var path1 = fileAcessingSettings.SettingValue + "\\" + path + "\\" + file;
                if (System.IO.File.Exists(path1))
                {
                    System.IO.File.Delete(path1);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public ActionResult SessionStartAndEndDate(int AssemblyId, int SessionId)
        {
            try
            {                
                SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
                data.AssemblyID = AssemblyId;
                data.SessionCode = SessionId;
                DateTime? strtdate = (DateTime?)Helper.ExecuteService("Session", "sessionstartdate", data);
                DateTime? enddate = (DateTime?)Helper.ExecuteService("Session", "sessionEnddate", data);

                VsLibraryBulletin model = new VsLibraryBulletin();
                model.StartDate = strtdate.Value.ToShortDateString();
                model.EndDate = enddate.Value.ToShortDateString();

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        #endregion
    }
}
