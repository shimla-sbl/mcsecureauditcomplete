﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.DomainModel.Models.testingEmp;
using SBL.eLegistrator.HouseController.Web.Helpers;


namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class testemployeeController : Controller
    {
        //
        // GET: /Admin/testemployee/
        [Audit]
        [NoCache]
        [SBLAuthorize(Allow = "Authenticated")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveData(string empname, string address)
        {
            var parameter = new Emptest
            {
                EmployeeName = empname,
                Address = address,
               
            };
            var model = (Emptest)Helper.ExecuteService("Employee", "dataemp", parameter);
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        

    }
}
