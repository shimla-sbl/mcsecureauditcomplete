﻿using SBL.eLegislator.HPMS.ServiceAdaptor;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.Constituency.Models;
using SBL.DomainModel.Models.Office;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.ComplexModel;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class ConstituencyReportController : Controller
    {
        //
        // GET: /Admin/ConstituencyReport/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ConstituencywiseReportPartial(int ConstituencyCode)
        {
            string CCode = Convert.ToString(ConstituencyCode);
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@ConstituencyCode", CCode));
            DataSet dataSetSession = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetAllWorksDataByConsId", methodParameter);

            ConstituencyWiseWorkList ConstituencyWiseWorkList = new ConstituencyWiseWorkList();
            List<ConstituencyWiseWorkList> ListData = new List<ConstituencyWiseWorkList>();
            if (ConstituencyCode == 0)
            {
                for (int i = 0; i < dataSetSession.Tables[0].Rows.Count; i++)
                {
                    {
                        ConstituencyWiseWorkList tConstituencyWiseWorkList = new ConstituencyWiseWorkList();
                        tConstituencyWiseWorkList.ConstituencyName = Convert.ToString(dataSetSession.Tables[0].Rows[i]["ConstituencyName"]);
                        tConstituencyWiseWorkList.TotalDiary = Convert.ToString(dataSetSession.Tables[0].Rows[i]["Total Diary"]);
                        tConstituencyWiseWorkList.TotalWorks = Convert.ToString(dataSetSession.Tables[0].Rows[i]["Total Works"]);

                        tConstituencyWiseWorkList.TotalSanctionedPost = Convert.ToString(dataSetSession.Tables[0].Rows[i]["Total Sanctioned Post"]);
                        if (tConstituencyWiseWorkList.TotalSanctionedPost == "")
                        {
                            tConstituencyWiseWorkList.TotalSanctionedPost = "0";
                        }
                        tConstituencyWiseWorkList.TotalFilledPost = Convert.ToString(dataSetSession.Tables[0].Rows[i]["Total Filled Post"]);
                        if (tConstituencyWiseWorkList.TotalFilledPost == "")
                        {
                            tConstituencyWiseWorkList.TotalFilledPost = "0";
                        }
                        string Mmembercode = Convert.ToString(dataSetSession.Tables[0].Rows[i]["MemberCode"]);
                        List<mOffice> list = (List<mOffice>)Helper.ExecuteService("ConstituencyVS", "GetAllOfficemappedByMemberCode", Mmembercode);
                        int countt = list.Count();
                        tConstituencyWiseWorkList.mCount = countt;

                        ListData.Add(tConstituencyWiseWorkList);
                    }
                }
                ConstituencyWiseWorkList.tConstituencyWiseWorkList = ListData;
            }
            else
            {
                for (int i = 0; i < dataSetSession.Tables[0].Rows.Count; i++)
                {

                    ConstituencyWiseWorkList mConstituencyWiseWorkList = new ConstituencyWiseWorkList();
                    mConstituencyWiseWorkList.ConstituencyName = Convert.ToString(dataSetSession.Tables[0].Rows[i]["ConstituencyName"]);
                    mConstituencyWiseWorkList.TotalDiary = Convert.ToString(dataSetSession.Tables[0].Rows[i]["Total Diary"]);
                    mConstituencyWiseWorkList.TotalWorks = Convert.ToString(dataSetSession.Tables[0].Rows[i]["Total Works"]);
                    mConstituencyWiseWorkList.TotalSanctionedPost = Convert.ToString(dataSetSession.Tables[0].Rows[i]["Total Sanctioned Post"]);
                    mConstituencyWiseWorkList.TotalFilledPost = Convert.ToString(dataSetSession.Tables[0].Rows[i]["Total Filled Post"]);
                    string Mmembercode = Convert.ToString(dataSetSession.Tables[0].Rows[i]["MemberCode"]);
                    List<mOffice> list = (List<mOffice>)Helper.ExecuteService("ConstituencyVS", "GetAllOfficemappedByMemberCode", Mmembercode);
                    int countt = list.Count();
                    mConstituencyWiseWorkList.mCount = countt;
                    ListData.Add(mConstituencyWiseWorkList);
                }

            }
            ConstituencyWiseWorkList.tConstituencyWiseWorkList = ListData;


            //string[] str = new string[2];
            //str[0] = CurrentSession.AssemblyId;
            //str[1] = CCode;
            //string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetMemberCodeByConstituencyId", str);



            //List<mOffice> list1 = (List<mOffice>)Helper.ExecuteService("ConstituencyVS", "GetAllSDMByconsCode", CCode);
            //List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
            //methodParameter1 = new List<KeyValuePair<string, string>>();
            //methodParameter1.Add(new KeyValuePair<string, string>("@ConstituencyCode", CCode));
            //DataSet DataSetSession = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetAllWorksDataByOfficeId", methodParameter);

            return PartialView("PConstituencyWisePublicWorksReport", ConstituencyWiseWorkList);
        }

        public JsonResult GetConstituencyByAssemblyId()
        {
            List<fillListGenricInt> list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetConstituencyByAssemblyId", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDistrictByAssemblyId()
        {
            List<fillListGenricInt> list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetAllDistricts", null);

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DiswiseReport()
        {
            return View();
        }
        public JsonResult GetConstituencyByDistrictId(int DistrictCode)
        {
            List<fillListGenricInt> list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetConstituencyByDistrictId", DistrictCode);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

    }
}
