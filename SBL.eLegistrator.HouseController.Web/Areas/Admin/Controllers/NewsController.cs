﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.News;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.Service.Common;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.DomainModel.Models.Category;
using System.IO;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;


using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using SBL.DomainModel.Models.PaperLaid;


namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class NewsController : Controller
    {
        #region NewsList



        public ActionResult NewsEntryFormIndex()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var News = (List<mNews>)Helper.ExecuteService("ANews", "GetNews", null);
                var model = News.ToViewModel();
                return View(model);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting News List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting News List Deails";
                return View("AdminErrorPage");
            }


        }

        public ActionResult viewImage(string filepath,string filename)
        { 
       filename= filename.Remove(filename.Length - 1);

            string [] splitImage =  filename.Split('?');
            for (int i=0;i< splitImage.Length;i++)
            {

                splitImage[i] = filepath + splitImage[i];

            }

            try
            {
                
                return PartialView("_ViewImage", splitImage);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Create News  Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating News  Deails";
                return View("AdminErrorPage");
            }




        }

   
        public ActionResult NewsEntryForm()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var categorytype = (List<Category>)Helper.ExecuteService("ANews", "GetAllCategoryType", null);
                var model = new NewsViewModel()
                {
                    mode = "Add",
                    TypeList = ModelMapping.GetStatusTypes(),
                    CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null),
                    Status = 1


                };
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Create News  Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating News  Deails";
                return View("AdminErrorPage");
            }




        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveNewsEntry(NewsViewModel model, HttpPostedFileBase file, HttpPostedFileBase file1, List<HttpPostedFileBase> postedFiles)
        {
           

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var data = model.ToDomainModel();
                if (model.mode == "Add")
                {
                    model.IsActive = true;
                    data.Images = "";
                    data.FileName = "";

                    data.Images1 = "";
                    data.FileName1 = "";


                    data.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    data.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    var newsId = (int)Helper.ExecuteService("ANews", "AddNews", data);
                    data.LatestNewsID = newsId;

                    if (file == null)
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/TempData"));
                        string SourceFile = savedFileName[0];
                       
                        foreach (string page in savedFileName)
                        {
                            string name = Path.GetFileName(page);
                            string nameKey = Path.GetFileNameWithoutExtension(page);
                            string directory = Path.GetDirectoryName(page);

                            Console.WriteLine("{0}, {1}, {2}, {3}",
                            page, name, nameKey, directory);

                            data.Images = UploadPhoto(file, name) ;
                            data.FileName += name + "?";
                            data.ThumbName += name + "?";
                        }

                        Helper.ExecuteService("ANews", "UpdateNews", data);

                    }



                    if (file1 != null)
                    {
                        // var filename = memberId + "_" + file.FileName;
                        var filename = newsId + ".pdf";
                        data.Images1 = UploadPhoto1(file1, filename);
                        data.FileName1 = filename;
                        data.ThumbName1 = filename;
                        Helper.ExecuteService("ANews", "UpdateNews", data);

                    }





                }
                else
                {
                    if (file == null)
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/TempData"));
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            string name = Path.GetFileName(page);
                            string nameKey = Path.GetFileNameWithoutExtension(page);
                            string directory = Path.GetDirectoryName(page);

                            Console.WriteLine("{0}, {1}, {2}, {3}",
                            page, name, nameKey, directory);

                            data.Images = UploadPhoto(file, name);
                            data.FileName += name + "?";
                            data.ThumbName += name + "?";
                        }

                        Helper.ExecuteService("ANews", "UpdateNews", data);

                    }
                    else
                    {
                        Helper.ExecuteService("ANews", "UpdateNews", data);
                    }




                    if (file1 != null)
                    {
                        if (data.FileName1 != null)
                        {
                            RemoveExistingPhoto1(data.FileName1);
                        }
                        //var upfileName = member.MemberID + "_" + file.FileName;
                        var upfileName = data.LatestNewsID + ".pdf";
                        data.Images1 = UploadPhoto1(file1, upfileName);

                        data.FileName1 = upfileName;
                        data.ThumbName1 = upfileName;

                        Helper.ExecuteService("ANews", "UpdateNews", data);
                    }
                    else
                    {
                        data.FileName1 = model.ThumbImage1;
                        data.ThumbName1 = model.ThumbImage1;
                        Helper.ExecuteService("ANews", "UpdateNews", data);
                    }




                }


                if (model.IsCreatNew)
                {
                    return RedirectToAction("NewsEntryForm");
                }
                else
                {
                    return RedirectToAction("NewsEntryFormIndex");
                }
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving News Deails");
                ViewBag.ErrorMessage = "There is a Error While Saving News Details";
                return View("AdminErrorPage");
            }




            // return RedirectToAction("NewsEntryFormIndex");
        }

        public ActionResult NewsEntryEdit(string Id)
        {


            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var categorytype = (List<Category>)Helper.ExecuteService("ANews", "GetAllCategoryType", null);
                var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                //var News = (mNews)Helper.ExecuteService("ANews", "GetNewsById", new mNews { LatestNewsID = Id });
                var News = (mNews)Helper.ExecuteService("ANews", "GetNewsById", new mNews { LatestNewsID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = News.ToViewModel("Edit");
                model.CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null);
                model.ImageLocation = fileAcessingSettings.SettingValue + model.FileLocation + "/" + model.FullImage;
                model.ImageLocation1 = fileAcessingSettings.SettingValue + model.FileLocation1 + "/" + model.FullImage1;
                model.FileAcessPath = fileAcessingSettings.SettingValue;
                return View("NewsEntryForm", model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Edit News  Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deting News  Deails";
                return View("AdminErrorPage");
            }





        }

        public ActionResult DeleteNewsEntry(int Id)
        {

            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var ActualNews = (mNews)Helper.ExecuteService("ANews", "GetNewsById", new mNews { LatestNewsID = Id });

                //Code for remove thumb image for category
                //var Newsthumbimage = (bool)Helper.ExecuteService("ANews", "DeleteNewsThumbForCategory", new mNews { LatestNewsID = Id });
                var News = Helper.ExecuteService("ANews", "DeleteNews", new mNews { LatestNewsID = Id });
                if (!string.IsNullOrEmpty(ActualNews.FileName))
                    RemoveExistingPhoto(ActualNews.FileName);

                if (!string.IsNullOrEmpty(ActualNews.FileName1))
                    RemoveExistingPhoto1(ActualNews.FileName1);

                return RedirectToAction("NewsEntryFormIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Deleting News Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting News Deails";
                return View("AdminErrorPage");
            }




        }

        [HttpPost]
        public ContentResult UploadFiles()
        {
            //var r = new List<NewsViewModel>();
           // HttpPostedFile file = (HttpPostedFile)(txtFile.PostedFile);
           
            string url = "~/TempData";
            string directory = Server.MapPath(url);
         

            var r = new List<NewsViewModel>();

            foreach (string file in Request.Files)
            {
                

                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                //if (Directory.Exists(directory))
                //{
                //    System.IO.Directory.Delete(directory, true);
                //}

                //int iFileSize = hpf.ContentLength;
                //if (iFileSize > 164000000)  // 3MB approx (actually less though)
                //{
                //    // File is too big so do something here
                //    return Content("{\"name\":\"File is too large so you can upload 150MB files\"}", "application/json");
                //}
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/TempData";
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/TempData"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new NewsViewModel()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }


        #endregion

        #region NewsCategory
        public ActionResult NewsIndex()
        {

            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var News = (List<Category>)Helper.ExecuteService("ANews", "GetNewsCategory", null);
                var model = News.ToViewModel();
                return View(model);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting News Category List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting News Category List Deails";
                return View("AdminErrorPage");
            }






        }
        public ActionResult CreateNews()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new CategoryViewModel()
                {
                    Mode = "Add",
                    TypeList = ModelMapping.GetStatusTypes(),
                    CategoryTypeList = ModelMapping.GetCategoryTypes(),
                    Status = 1
                    //CategoryList=ModelMapping.GetNewsCategory()
                };
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Create News Category  Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating News Category  Deails";
                return View("AdminErrorPage");
            }




        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveNews(CategoryViewModel model)
        {

            try
            {
                if (!string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    if (model.Mode == "Add")
                    {
                        model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                        model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                        Helper.ExecuteService("ANews", "AddNewsCategory", model.ToDomainModel());

                    }
                    else
                    {
                        // model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                        model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                        Helper.ExecuteService("ANews", "UpdateNewsCategory", model.ToDomainModel());
                    }
                    if (model.IsCreatNew)
                    {
                        return RedirectToAction("CreateNews");
                    }
                    else
                    {
                        return RedirectToAction("NewsIndex");
                    }
                }
                else
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                // return RedirectToAction("NewsIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Save News Category Deails");
                ViewBag.ErrorMessage = "Their is a Error While Save News Category Deails";
                return View("AdminErrorPage");
            }
            string url = "~/TempData";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

        }
        public ActionResult Edit(string Id)
        {


            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                //var News = (Category)Helper.ExecuteService("ANews", "GetNewsByIdCategory", new Category { CategoryID = Id });
                var News = (Category)Helper.ExecuteService("ANews", "GetNewsByIdCategory", new Category { CategoryID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                return View("CreateNews", News.ToViewModel("Edit"));
            }
            catch (Exception ex)
            {

                RecordError(ex, "Edit News Deails");
                ViewBag.ErrorMessage = "Their is a Error While Edit News Deails";
                return View("AdminErrorPage");
            }





        }

        public ActionResult DeleteNews(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var News = Helper.ExecuteService("ANews", "DeleteNewsCategory", new Category { CategoryID = Id });

                return RedirectToAction("NewsIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Delete News Deails");
                ViewBag.ErrorMessage = "Their is an Error While Deleting News Deails";
                return View("AdminErrorPage");
            }

        }


        public JsonResult CheckNewsCategoryChildExist(int categoryId)
        {


            var isExist = (Boolean)Helper.ExecuteService("ANews", "IsNewsCategoryChildExist", new Category { CategoryID = categoryId });

            return Json(isExist, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Private Methods

        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string UploadPhoto(HttpPostedFileBase File, string FileName)
        {

            try
            {
                string newpath = "/TempData/" + FileName;
                newpath = Server.MapPath(newpath);
                if (File == null)
                {                    
                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNewsFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    var newsThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNewsThumbFileSetting", null);
                    var newsDetailsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNewsDetailsFileSetting", null);


                    //string extension = System.IO.Path.GetExtension(File.FileName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, FileName);//Server.MapPath("~/Images/News/"), FileName);
                    string path1 = System.IO.Path.Combine(FileSettings.SettingValue + newsThumbSettings.SettingValue, FileName);//Server.MapPath("~/Images/News/"), FileName);
                    string path2 = System.IO.Path.Combine(FileSettings.SettingValue + newsDetailsSettings.SettingValue, FileName);//Server.MapPath("~/Images/News/"), FileName);
                    string basePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue + "\\Temp", FileName);//Server.MapPath("~/Images/News/"), FileName);


                    string dir = System.IO.Path.Combine(FileSettings.SettingValue + newsDetailsSettings.SettingValue);
                    string dir1 = System.IO.Path.Combine(FileSettings.SettingValue + newsThumbSettings.SettingValue);
                    string dir2 = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue + "\\Temp");
                     if (!System.IO.Directory.Exists(dir))
                    {
                        System.IO.Directory.CreateDirectory(dir);
                      
                    }
                    if (!System.IO.Directory.Exists(dir1))
                    {
                        System.IO.Directory.CreateDirectory(dir1);
                      
                    }
                    if (!System.IO.Directory.Exists(dir2))
                    {
                        System.IO.Directory.CreateDirectory(dir2);

                    }
                    
                    
                    Extensions.ImageResizerExtensions imgex = new Extensions.ImageResizerExtensions(600);
                    //File.SaveAs(basePath);
                    System.IO.File.Copy(newpath, basePath, true);
                    imgex.Resize(newpath, basePath);
                    System.IO.File.Delete(basePath);
                    Extensions.ImageResizerExtensions imgext = new Extensions.ImageResizerExtensions(450);
                    //File.SaveAs(basePath);
                    //System.IO.File.Copy(newpath, path, true);
                    System.IO.File.Copy(newpath, path, true);
                    imgext.Resize(newpath, path);
                    //System.IO.File.Delete(path);

                   Extensions.ImageResizerExtensions imgext1 = new Extensions.ImageResizerExtensions(105);
                    //File.SaveAs(basePath);
                    System.IO.File.Copy(newpath, path1, true);
                    imgext1.Resize(newpath, path1);
                    //System.IO.File.Delete(path1);
                    //System.IO.File.Delete(newpath);

                    Extensions.ImageResizerExtensions imgext2 = new Extensions.ImageResizerExtensions(200);
                    //File.SaveAs(basePath);
                    System.IO.File.Copy(newpath, path2, true);
                    imgext2.Resize(newpath, path2);
                    //System.IO.File.Delete(path1);
                    System.IO.File.Delete(newpath);
                    return ("/News");

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            if (File == null)
            {
                string extension = System.IO.Path.GetExtension(File.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Images/News/"), FileName);
                File.SaveAs(path);
                return ("/Images/News/");
            }


            return null;
        }

        private void RemoveExistingPhoto(string OldPhoto)
        {
            var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNewsFileSetting", null);
            var newsThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNewsThumbFileSetting", null);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            var newsDetailsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNewsDetailsFileSetting", null);

            //string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, OldPhoto);//Server.MapPath("~/Images/News/")
            string path1 = System.IO.Path.Combine(FileSettings.SettingValue + newsThumbSettings.SettingValue, OldPhoto);
            string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue + "\\", OldPhoto);
            string path2 = System.IO.Path.Combine(FileSettings.SettingValue + newsDetailsSettings.SettingValue, OldPhoto);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
                System.IO.File.Delete(path1);
                System.IO.File.Delete(path2);
            }
        }

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "NewsError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }



        private void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(image.Width * scaleFactor);
                var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }




        #endregion

        #region ServerSidePaginationRegion

        public ActionResult NewServerSidePagination(NewsJqueryModelList param)
        {

            IEnumerable<CategoryViewModel> NewsCategory = new List<CategoryViewModel>();

            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }



                var News = (List<Category>)Helper.ExecuteService("ANews", "GetNewsCategory", null);


                //var displayedCompanies = filteredCompanies
                //                    .Skip(param.iDisplayStart)
                //                    .Take(param.iDisplayLength);
                var model = News.ToViewModel();
                // NewsCategory = model;
                // return View(model);
                if (param.iDisplayLength == 0)
                {
                    param.iDisplayLength = 10;
                }
                NewsCategory = model.Skip(param.iDisplayStart).Take(param.iDisplayLength);

                var result = from a in NewsCategory
                             select new[] {Convert.ToString(a.CategoryID),a.CategoryName,a.CategoryDescription,
                    a.LocalCategoryName,a.LocalCategoryDescription, 
                    a.Status==1?"<span class='label label-sm label-success'>Published</span>":"<span class='label label-sm label-warning'>Unpublished</span>",
                    GetUserDeleteFunctions(a.CategoryID)
                };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = model.Count(),
                    iTotalDisplayRecords = NewsCategory.Count(),
                    aaData = result
                },
                        JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting News Category List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting News Category List Deails";
                return View("AdminErrorPage");
            }


#pragma warning disable CS0162 // Unreachable code detected
            return View();
#pragma warning restore CS0162 // Unreachable code detected
        }

        public ActionResult NewsServersideIndex()
        {
            return View("_NewServerSidePagination");
        }


        public string GetUserDeleteFunctions(int categoryId)
        {

            string htmldescription = "<div class='hidden-sm hidden-xs action-buttons'><a data-rel='tooltip' class='green tooltip-success' title='Edit'   href='@Url.Action('NewsEntryEdit', 'News', new { Id =" + categoryId + " })'>" +
                                                   "<i class='ace-icon fa fa-pencil bigger-130'></i></a><a  data-rel='tooltip' class='red tooltip-error DeleteNews' title='Delete'   href='@Url.Action('DeleteNewsEntry', 'News', new { Id =" + categoryId + " })'>" +
                                                    "<i class='ace-icon fa fa-trash-o bigger-130'></i></a></div><div class='hidden-md hidden-lg'><div class='inline position-relative'><button class='btn btn-minier btn-yellow dropdown-toggle' data-toggle='dropdown' data-position='auto'>" +
                                                        "<i class='ace-icon fa fa-caret-down icon-only bigger-120'></i></button><ul class='dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close'><li>" +
                                                        "<a  href='@Url.Action('NewsEntryEdit', 'News', new { Id =" + categoryId + " })' class='tooltip-success' data-rel='tooltip' title='Edit'><span class='green'><i class='ace-icon fa fa-pencil bigger-120'></i>" +
                                                                "</span></a></li><li><a  href='@Url.Action('DeleteNewsEntry', 'News', new { Id =" + categoryId + " })' class='tooltip-error DeleteNews' data-rel='tooltip' title='Delete'><span class='red'><i class='ace-icon fa fa-trash-o bigger-120'></i>" +
                                                                "</span></a></li></ul></div></div>";






            return htmldescription;
        }

        #endregion







       
        #region Private Methods

       

        private string UploadPhoto1(HttpPostedFileBase File, string FileName)
        {
            try
            {


                if (File != null)
                {




                    var memberSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNewsFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    var memberThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNewsThumbFileSetting", null);

                    //DirectoryInfo MemberDir = new DirectoryInfo(FileSettings.SettingValue + memberThumbSettings.SettingValue);
                    //if (!MemberDir.Exists)
                    //{
                    //    MemberDir.Create();
                    //}


                    //DirectoryInfo MemberTempDir = new DirectoryInfo(FileSettings.SettingValue + "\\News\\Temp");
                    //if (!MemberTempDir.Exists)
                    //{
                    //    MemberTempDir.Create();
                    //}



                    string extension = System.IO.Path.GetExtension(File.FileName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue, FileName);
                    string path1 = System.IO.Path.Combine(FileSettings.SettingValue + memberThumbSettings.SettingValue, FileName);
                    string basePath = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue + "\\Temp", FileName);

                    //SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions imgex = new SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions(750);
                    //File.SaveAs(basePath);
                    //imgex.Resize(basePath, path);
                    //System.IO.File.Delete(basePath);
                    //SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions imgext = new SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions(85);
                    File.SaveAs(path);
                    //imgext.Resize(basePath, path1);
                    //System.IO.File.Delete(path);

                    File.SaveAs(path1);
                    //System.IO.File.Delete(path1);
                    return ("/News");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            if (File != null)
            {
                string extension = System.IO.Path.GetExtension(File.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Images/News/"), FileName);
                File.SaveAs(path);
                return ("/Images/News/");
            }
            return null;
        }

        private void RemoveExistingPhoto1(string OldPhoto)
        {

            try
            {

                var memberSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNewsFileSetting", null);
                var memberThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNewsThumbFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path1 = System.IO.Path.Combine(FileSettings.SettingValue + memberThumbSettings.SettingValue, OldPhoto);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue + "\\", OldPhoto);

                //string path = System.IO.Path.Combine(Server.MapPath("~/Images/Gallery/"), OldPhoto);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    System.IO.File.Delete(path1);
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

       

        #endregion





    }
}
