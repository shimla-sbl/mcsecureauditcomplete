﻿using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class MemberGalleryController : Controller
    {
        //
        // GET: /Admin/MemberGallery/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CreateMemberCategory()
        {
            //if (string.IsNullOrEmpty(CurrentSession.UserID))
            //{
            //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
            //}
            var model = new AlbumCategoryViewModel()
            {
                Mode = "Add"

            };
            return View(model);
        }

    }
}
