﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.ComplexModel;
using SBL.eLegistrator.HouseController.Web.Helpers;
using Email.API;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using Email.WebService;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
	[Audit]
	[NoCache]
	[SBLAuthorize(Allow = "Authenticated")]
	public class EMailController : Controller
	{
		//
		// GET: /Admin/EMail/

		public ActionResult Index()
		{
			return View();
		}

		public ActionResult GetContactList()
		{
			EMailModel model = new EMailModel();
			model.MemberContactCol = (List<MemberContactViewModel>)Helper.ExecuteService("Contacts", "GetAllMembersEMailIds", null);
			PartialView("_GetMembersContactList", model);
			return PartialView("_GetContactList");
		}

		public ActionResult GetMinistersList()
		{
			EMailModel model = new EMailModel();
			model.MinisterContactCol = (List<MinisterContactViewModel>)Helper.ExecuteService("Contacts", "GetAllMinisterEMailIds", null);
			return PartialView("_GetMinistersContactList", model);
		}

		public ActionResult GetMembersList()
		{
			EMailModel model = new EMailModel();
			model.MemberContactCol = (List<MemberContactViewModel>)Helper.ExecuteService("Contacts", "GetAllMembersEMailIds", null);
			return PartialView("_GetMembersContactList", model);
		}

		public ActionResult GetEmployeesList()
		{
			EMailModel model = new EMailModel();
			model.UserContactCol = (List<mUsers>)Helper.ExecuteService("Contacts", "GetAllEmployeeEmailIds", null);
			return PartialView("_GetEmployeesContactList", model);
		}

		[HttpPost]
		public ActionResult SendMail(EMailModel model)
		{
			if (model.To != "" && model.To != null && model.Subject != ""
			&& model.Subject != null && model.Message != "" && model.Message != null)
			{
				try
				{
					Service1 EmailWebServiceClient = new Service1();

					string[] emailList = model.To.Split(new string[] { "," },
					StringSplitOptions.RemoveEmptyEntries);

					var emailGroup = (from email in emailList
									  select email).Distinct();

					if (emailList.Length > 0)
					{
						CustomMailMessage emailMessage = new CustomMailMessage();

						emailMessage._isBodyHtml = true;

						emailMessage._subject = model.Subject;

						emailMessage._body = model.Message;

						emailMessage.ModuleActionID = 1;

						emailMessage.UniqueIdentificationID = 1;

						foreach (string email in emailGroup)
						{
							emailMessage._toList.Add(email);
						}

						string url = "/EmailAttachment/" + CurrentSession.UserID + "/";

						string directory = Server.MapPath(url);

						DirectoryInfo d = new DirectoryInfo(directory);

						FileInfo[] Files = d.GetFiles();

						if (Files.Any())
						{
							emailMessage._attachments = new List<EAttachment>();

							foreach (FileInfo file in Files)
							{
								EAttachment ea = new EAttachment();
								ea.FileName = file.Name;
								ea.FileContent = System.IO.File.ReadAllBytes(file.FullName);
								emailMessage._attachments.Add(ea);
							}
						}

						model.SendSMS = false;

						model.SendEmail = true;

						Notification.Send(model.SendSMS, model.SendEmail, null, emailMessage);

						if (Files.Any())
						{
							foreach (FileInfo file in Files)
							{
								file.Delete();
							}
						}
					}

					model.Result = "success";
				}
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
				catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
				{
					model.Result = "EMail Sending Failed";
				}
			}

			return View("Index",model);
		}

		[HttpPost]
		public JsonResult Upload()
		{
			for (int i = 0; i < Request.Files.Count; i++)
			{
				var file = Request.Files[i];

				var fileName = Path.GetFileName(file.FileName);

				if (!String.IsNullOrEmpty(fileName))
				{
					string url = "/EmailAttachment/" + CurrentSession.UserID + "/";

					string directory = Server.MapPath(url);

					if (!Directory.Exists(directory))
					{
						Directory.CreateDirectory(directory);
					}
					
					var path = Path.Combine(directory, fileName);

					file.SaveAs(path);
				}
			}

			string msg = "attached";

			return Json(msg, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public ActionResult GetGroupMembersPartial(int? GroupID)
		{
			SMSComposeModel mdl = new SMSComposeModel();
			RecipientGroupMember model = new RecipientGroupMember();

			List<RecipientGroupMember> modelList = new List<RecipientGroupMember>();
			model.GroupID = GroupID ?? 0;

			ViewBag.GroupMembers = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", model);

			modelList = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", model);

			return PartialView("_GetMembersContactList", mdl);
		}
	}
}
