﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Gallery;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using SBL.DomainModel.Models.Category;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.IO;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.DomainModel.Models.AlbumCategory;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class GalleryController : Controller
    {

        //
        // GET: /Admin/Gallery/


        #region Gallery Category
        public ActionResult GalleryCategoryIndex()
        {

            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var Gallery = (List<Category>)Helper.ExecuteService("Gallery", "GetAllGalleryCategory", null);
                var filacessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                var model = Gallery.ToViewModel();
                ViewBag.FileAcessPath = filacessingSettings.SettingValue;
                return View(model);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting the Gallery Category List");
                ViewBag.ErrorMessage = "Their is a Error While Getting the Gallery Category List";
                return View("AdminErrorPage");
            }



        }
        //Gallery Category Creation... 
        public ActionResult CreateGallery()
        {

            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                AlbumCategoryViewModel model1 = new AlbumCategoryViewModel();
                var Res = (List<AlbumCategory>)Helper.ExecuteService("Gallery", "GetDropDownData", null);


                var filacessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                var model = new CategoryViewModel()
                {
                    Mode = "Add",
                    TypeList = ModelMapping.GetStatusTypes(),
                    ImagesList = ModelMapping.GetGalleryImages(),
                    Status = 1,


                    Dropdownlist = new SelectList(Res, "AlbumCategoryID", "CategoryName"),

                };

                ViewBag.FileAcessingPath = filacessingSettings.SettingValue;
                return View(model);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Creating Gallery Category");
                ViewBag.ErrorMessage = "Their is a Error While Creating the Gallery Category Details";
                return View("AdminErrorPage");
            }




        }


        //Save Gallery Category...
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveGallery(CategoryViewModel model)
        {


            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                if (model.Mode == "Add")
                {
                    // model.IsActive = true;
                    model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("Gallery", "AddGalleryCategory", model.ToDomainModel());

                }
                else
                {
                    // model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    var result = model.ToDomainModel();
                    result.GalleryId = model.GalleryImageId.HasValue ? model.GalleryImageId.Value : 0;
                    Helper.ExecuteService("Gallery", "UpdateGalleryCategory", result);
                }

                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateGallery");
                }
                else
                {
                    return RedirectToAction("GalleryCategoryIndex");
                }

            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Gallery Category");
                ViewBag.ErrorMessage = "Their is a Error While saving the Gallery Category Details";
                return View("AdminErrorPage");
            }

            // return RedirectToAction("GalleryCategoryIndex");
        }



        public ActionResult Edit(string Id)
        {


            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                //var Gallery = (Category)Helper.ExecuteService("Gallery", "GetGalleryByIdCategory", new Category { CategoryID = Id });
                var Gallery = (Category)Helper.ExecuteService("Gallery", "GetGalleryByIdCategory", new Category { CategoryID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var Res = (List<AlbumCategory>)Helper.ExecuteService("Gallery", "GetDropDownData", null);

                var gallerycategorymode = Gallery.ToViewModel("Edit");
                gallerycategorymode.Dropdownlist = new SelectList(Res, "AlbumCategoryID", "CategoryName");

                gallerycategorymode.GalleryImageId = Gallery.GalleryId;
                //var galImagesLst = (List<tGallery>)Helper.ExecuteService("Gallery", "GetGalleryImagesByCategoryId", new Category { CategoryID = Id });
                var galImagesLst = (List<tGallery>)Helper.ExecuteService("Gallery", "GetGalleryImagesByCategoryId", new Category { CategoryID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                gallerycategorymode.GalleryImageList = new SelectList(galImagesLst, "GalleryId", "FileName", null);
                var settingdata = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                ViewBag.FileAcessingPath = settingdata.SettingValue;
                return View("CreateGallery", gallerycategorymode);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Edit Gallery Category");
                ViewBag.ErrorMessage = "Their is a Error While editing the Gallery Category Details";
                return View("AdminErrorPage");
            }



        }

        public ActionResult DeleteGallery(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                var Gallery = Helper.ExecuteService("Gallery", "DeleteGalleryCategory", new Category { CategoryID = Id });

                return RedirectToAction("GalleryCategoryIndex");

            }
            catch (Exception ex)
            {

                RecordError(ex, "Delete Gallery Category");
                ViewBag.ErrorMessage = "Their is a Error While Deleting the Gallery Category";
                return View("AdminErrorPage");
            }




        }

        #endregion

        #region Gallery Images
        public ActionResult GalleryCategoryFormIndex()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var Gallery = (List<tGallery>)Helper.ExecuteService("Gallery", "GetAllGallery", null);
                var model = Gallery.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Create Gallery Category");
                ViewBag.ErrorMessage = "Their is a Error While Create the Gallery Categroy";
                return RedirectToAction("AdminErrorPage");
            }



        }

        //Create Gallery 
        public ActionResult CreateGalleryEntry()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var categorytype = (List<Category>)Helper.ExecuteService("Gallery", "GetAllCategoryType", null);
                var Res = (List<AlbumCategory>)Helper.ExecuteService("Gallery", "GetDropDownData", null);

                var model = new GalleryViewModel()
                {
                    Mode = "Add",
                    TypeList = ModelMapping.GetStatusTypes(),
                    ImagesList = ModelMapping.GetGalleryImages(),
                    CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null),
                    GalleryTypeList = ModelMapping.GetGalleryType(),
                    Status = 1
                };
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Creating Gallery");
                ViewBag.ErrorMessage = "Their is a Error While Creating the Gallery Details";
                return View("AdminErrorPage");
            }



        }

        public ActionResult GalleryEditEntry(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var categorytype = (List<Category>)Helper.ExecuteService("Gallery", "GetAllCategoryType", null);
                var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                //var Gallery = (tGallery)Helper.ExecuteService("Gallery", "GetGalleryById", new tGallery { GalleryId = Id });
                var Gallery = (tGallery)Helper.ExecuteService("Gallery", "GetGalleryById", new tGallery { GalleryId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = Gallery.ToViewModel("Edit");
                model.ImageLocation = fileAcessingSettings.SettingValue + model.FileLocation + "/" + model.FullImage;
                model.CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null);


                //var newPath = model.FileLocation + "\\" + model.FullImage;
                //byte[] image = ReadImage(newPath);
                // System.IO.File.Delete(path);
                // model.bytesImage = "data:image/png;base64," + Convert.ToBase64String(image);
                return View("CreateGalleryEntry", model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Editing");
                ViewBag.ErrorMessage = "Their is a Error While Editing the Gallery Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult DeleteGalleryEntry(int Id)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            try
            {
                //Code for remove image
                var Galleryimage = (tGallery)Helper.ExecuteService("Gallery", "GetGalleryById", new tGallery { GalleryId = Id });

                //Code for remove thumb image for category
                var Gallerythumbimage = (bool)Helper.ExecuteService("Gallery", "DeleteGalleryThumbForCategory", new tGallery { GalleryId = Id });

                var Gallery = Helper.ExecuteService("Gallery", "DeleteGallery", new tGallery { GalleryId = Id });//DeleteGalleryThumbForCategory
                if (!string.IsNullOrEmpty(Galleryimage.FileName))
                    RemoveExistingPhoto(Galleryimage.FileName);

                return RedirectToAction("GalleryCategoryFormIndex");
            }
            catch (Exception ex)
            {
                RecordError(ex, "Deleting Gallery");
                ViewBag.ErrorMessage = "Their is a Error While Deleting the Gallery Details";
                return View("AdminErrorPage");
            }
        }


        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveGalleryForm(GalleryViewModel model, HttpPostedFileBase file)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var gallery = model.ToDomainModel();
                if (model.Mode == "Add")
                {
                    // Guid FileName = Guid.NewGuid();
                    gallery.FilePath = "";//UploadPhoto(file, file.FileName);
                    gallery.FileName = "";//file.FileName;
                    gallery.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    gallery.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    model.IsActive = true;
                    var galleryId = (int)Helper.ExecuteService("Gallery", "AddGallery", gallery);

                    gallery.GalleryId = galleryId;
                    // Code for change the Gallery File name 

                    if (file != null)
                    {
                        var filename = galleryId + "_" + file.FileName;
                        gallery.FilePath = UploadPhoto(file, filename);
                        gallery.FileName = filename;
                        gallery.ThumbName = filename;
                        Helper.ExecuteService("Gallery", "UpdateGallery", gallery);

                    }


                }
                else
                {
                    if (file != null)
                    {
                        if (gallery.FileName != null)
                        {
                            RemoveExistingPhoto(gallery.FileName);
                        }
                        var upfileName = gallery.GalleryId + "_" + file.FileName;

                        gallery.FilePath = UploadPhoto(file, upfileName);

                        gallery.FileName = upfileName;
                        gallery.ThumbName = upfileName;
                        //  Guid FileName = Guid.NewGuid();

                        gallery.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                        Helper.ExecuteService("Gallery", "UpdateGallery", gallery);
                    }
                    else
                    {
                        Helper.ExecuteService("Gallery", "UpdateGallery", gallery);
                    }


                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateGalleryEntry");
                }
                else
                {
                    return RedirectToAction("GalleryCategoryFormIndex");
                }
            }
            catch (Exception ex)
            {
                RecordError(ex, "Saving Gallery");
                ViewBag.ErrorMessage = "Their is a Error While saving the Gallery Details";
                return View("AdminErrorPage");
            }
        }

        public JsonResult CheckGalleryCategoryChildExist(int categoryId)
        {
            var isExist = (Boolean)Helper.ExecuteService("Gallery", "IsGalleryCategoryChildExist", new Category { CategoryID = categoryId });

            return Json(isExist, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Private Methods

        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        private string UploadPhoto(HttpPostedFileBase File, string FileName)
        {
            try
            {


                if (File != null)
                {
                    var gallerySettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetGalleryFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    var galleryThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetGalleryThumbFileSetting", null);
                    string extension = System.IO.Path.GetExtension(File.FileName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + gallerySettings.SettingValue, FileName);//Server.MapPath("~/Images/News/"), FileName);
                    string path1 = System.IO.Path.Combine(FileSettings.SettingValue + galleryThumbSettings.SettingValue, FileName);//Server.MapPath("~/Images/News/"), FileName);
                    string basePath = System.IO.Path.Combine(FileSettings.SettingValue + gallerySettings.SettingValue + "\\Temp", FileName);//Server.MapPath("~/Images/News/"), FileName);
                    
                    Extensions.ImageResizerExtensions imgex = new Extensions.ImageResizerExtensions(750);
                    File.SaveAs(basePath);
                    imgex.Resize(basePath, path);
                    System.IO.File.Delete(basePath);
                    Extensions.ImageResizerExtensions imgext = new Extensions.ImageResizerExtensions(85);
                    File.SaveAs(basePath);
                    imgext.Resize(basePath, path1);
                   
                    System.IO.File.Delete(basePath);
                    return ("/Gallery");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            if (File != null)
            {
                string extension = System.IO.Path.GetExtension(File.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Images/Gallery/"), FileName);
                File.SaveAs(path);
                return ("/Images/Gallery/");
            }
            return null;
        }

        private void RemoveExistingPhoto(string OldPhoto)
        {

            try
            {

                var gallerySettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetGalleryFileSetting", null);
                var galleryThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetGalleryThumbFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path1 = System.IO.Path.Combine(FileSettings.SettingValue + galleryThumbSettings.SettingValue, OldPhoto);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + gallerySettings.SettingValue + "\\", OldPhoto);

                //string path = System.IO.Path.Combine(Server.MapPath("~/Images/Gallery/"), OldPhoto);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    System.IO.File.Delete(path1);
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "galleryError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }
        private void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(image.Width * scaleFactor);
                var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }

        #endregion

    }

    //#region imagesize

    //public class ImageResizer
    //{
       
    //    public int MaxX { get; set; }

    //    public int MaxY { get; set; }

    //    public bool TrimImage { get; set; }

      
    //    public ImageFormat SaveFormat { get; set; }

    //    public ImageResizer()
    //    {
    //        MaxX = MaxY = 800;
    //        TrimImage = false;
    //        SaveFormat = ImageFormat.Jpeg;
    //    }

      
    //    public bool Resize(string source, string target)
    //    {
    //        using (Image src = Image.FromFile(source, true))
    //        {
    //            // Check that we have an image
    //            if (src != null)
    //            {
    //                int origX, origY, newX, newY;
    //                int trimX = 0, trimY = 0;

    //                // Default to size of source image
    //                newX = origX = src.Width;
    //                newY = origY = src.Height;

    //                // Does image exceed maximum dimensions?
    //                if (origX > MaxX || origY > MaxY)
    //                {
    //                    // Need to resize image
    //                    if (TrimImage)
    //                    {
    //                        // Trim to exactly fit maximum dimensions
    //                        double factor = Math.Max((double)MaxX / (double)origX,
    //                            (double)MaxY / (double)origY);
    //                        newX = (int)Math.Ceiling((double)origX * factor);
    //                        newY = (int)Math.Ceiling((double)origY * factor);
    //                        trimX = newX - MaxX;
    //                        trimY = newY - MaxY;
    //                    }
    //                    else
    //                    {
    //                        // Resize (no trim) to keep within maximum dimensions
    //                        double factor = Math.Min((double)MaxX / (double)origX,
    //                            (double)MaxY / (double)origY);
    //                        newX = (int)Math.Ceiling((double)origX * factor);
    //                        newY = (int)Math.Ceiling((double)origY * factor);
    //                    }
    //                }

    //                // Create destination image
    //                using (Image dest = new Bitmap(newX - trimX, newY - trimY))
    //                {
    //                    Graphics graph = Graphics.FromImage(dest);
    //                    graph.InterpolationMode =
    //                        System.Drawing.Drawing2D.InterpolationMode.Default;
    //                    graph.DrawImage(src, -(trimX / 2), -(trimY / 2), newX, newY);
                       
    //                    dest.Save(target, SaveFormat);
    //                    // Indicate success
                      
    //                    return true;
    //                }
    //            }
    //        }
    //        // Indicate failure
    //        return false;
    //    }
    //}
    //#endregion
    //#region thumbimagesize

    //public class ThumbImageResizer
    //{

    //    public int MaxX { get; set; }

    //    public int MaxY { get; set; }

    //    public bool TrimImage { get; set; }


    //    public ImageFormat SaveFormat { get; set; }

    //    public ThumbImageResizer()
    //    {
    //        MaxX = MaxY = 85;
    //        TrimImage = false;
    //        SaveFormat = ImageFormat.Jpeg;
    //    }


    //    public bool Resize(string source, string target)
    //    {
    //        using (Image src = Image.FromFile(source, true))
    //        {
    //            // Check that we have an image
    //            if (src != null)
    //            {
    //                int origX, origY, newX, newY;
    //                int trimX = 0, trimY = 0;

    //                // Default to size of source image
    //                newX = origX = src.Width;
    //                newY = origY = src.Height;

    //                // Does image exceed maximum dimensions?
    //                if (origX > MaxX || origY > MaxY)
    //                {
    //                    // Need to resize image
    //                    if (TrimImage)
    //                    {
    //                        // Trim to exactly fit maximum dimensions
    //                        double factor = Math.Max((double)MaxX / (double)origX,
    //                            (double)MaxY / (double)origY);
    //                        newX = (int)Math.Ceiling((double)origX * factor);
    //                        newY = (int)Math.Ceiling((double)origY * factor);
    //                        trimX = newX - MaxX;
    //                        trimY = newY - MaxY;
    //                    }
    //                    else
    //                    {
    //                        // Resize (no trim) to keep within maximum dimensions
    //                        double factor = Math.Min((double)MaxX / (double)origX,
    //                            (double)MaxY / (double)origY);
    //                        newX = (int)Math.Ceiling((double)origX * factor);
    //                        newY = (int)Math.Ceiling((double)origY * factor);
    //                    }
    //                }

    //                // Create destination image
    //                using (Image dest = new Bitmap(newX - trimX, newY - trimY))
    //                {
    //                    Graphics graph = Graphics.FromImage(dest);
    //                    graph.InterpolationMode =
    //                        System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
    //                    graph.DrawImage(src, -(trimX / 2), -(trimY / 2), newX, newY);

    //                    dest.Save(target, SaveFormat);
    //                    // Indicate success

    //                    return true;
    //                }
    //            }
    //        }
    //        // Indicate failure
    //        return false;
    //    }
    //}
    //#endregion
}
