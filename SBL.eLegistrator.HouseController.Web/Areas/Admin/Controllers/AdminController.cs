﻿using CaptchaMvc.Attributes;
using Microsoft.Security.Application;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Models;
using SMS.API;
using SMSServices;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.DomainModel.Models.Forms;
using SBL.eLegistrator.HouseController.Web.Utility;
using EvoPdf;
using System.Drawing;
using SBL.DomainModel.ComplexModel;
using System.Net;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Question;
using System.Globalization;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Models;
using SBL.DomainModel.Models.PaperLaid;
using System.Net.Mail;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [NoCache]
    [Audit]
    //[SBLAuthorize(Allow = "Authenticated")]
    public class AdminController : Controller
    {
        [HttpPost]
        public ActionResult AddReceipent()
        {
            return View();
        }

        public ActionResult Action()
        {
            return View();
        }
        public ActionResult AdminLeftMenu()
        {
            return PartialView("_LeftMenu");
        }


        public ActionResult GetUserPassword()
        {
            ForgetPassword model = new ForgetPassword();
            return View(model);
        }



        public ActionResult GetForgetPassword(string adhar)
        {

            mUsers user = new mUsers();
            user.AadarId = adhar.Trim();
            // user.DOB = model.DOB;
            user = (mUsers)Helper.ExecuteService("User", "GetForgetPassword", user);

            if (user != null)
            {
                string mobileNo = "";
                if (user.IsMember == "true" || user.IsMember == "True")
                {
                    string _AadharNo = user.AadarId;
                    var _ObjmMember = (mMember)Helper.ExecuteService("mMember", "GetMemberByAadharID", _AadharNo);

                    if (_ObjmMember != null)
                    {
                        mobileNo = _ObjmMember.Mobile;
                    }
                    else
                    {
                        mobileNo = user.MobileNo;
                    }
                }
                else
                {
                    mobileNo = user.MobileNo;
                }

                TempUser tempuser = new TempUser();
                tempuser = (TempUser)Helper.ExecuteService("User", "CheckPasswordIntemp", user);

                if (tempuser != null)
                {
                    if (tempuser.password == null || tempuser.password == "")
                    {
                        user.CaptchaMessage = "Your plain password is not available in database: ";
                        user.PasswordString = "";
                    }
                    else
                    {
                        user.CaptchaMessage = "Your existing password is: ";
                        user.PasswordString = tempuser.password;
                    }
                }
                else
                {
                    user.CaptchaMessage = "Your plain password is not available in database: ";
                    user.PasswordString = "";
                }

                return Json(user, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(user, JsonRequestBehavior.AllowGet);
            }


        }




        public ActionResult SendMessage(string adhar, string mobile, string pass)
        {
            SMSService Services = new SMSService();
            SMSMessageList smsMessage = new SMSMessageList();
            smsMessage.SMSText = "Your e-Vidhan Login Id is your Aadhar No " + adhar + " and password is " + pass.Trim() + " Thanks ... Secretary HP Vidhan Sabha.";
            //"Your Password for eVidhan is " + pass.Trim() + " .";
            // Services.sendSingleSMS("hpgovt", "hpdit@1234", "hpgovt", mob.Trim(), "Your new Password for eVidhan is " + tempuser.password + "");
            smsMessage.ModuleActionID = 2;
            smsMessage.UniqueIdentificationID = 2;
            smsMessage.MobileNo.Add(mobile.Trim());
            Notification.Send(true, false, smsMessage, null);
            return Json("true", JsonRequestBehavior.AllowGet);

        }
        public static string GeneratePassword()
        {
            int maxSize = 8; // whatever length you want
            // char[] chars = new char[62];
            string a;
            a = "1qaz2wsx3edc4rfv5tgb6yhn7ujm8ik9ol";
            char[] chars = new char[a.Length];
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }
        public static string ComputeHash(string plainText, string hashAlgorithm, byte[] saltBytes)
        {
            // If salt is not specified, generate it on the fly.
            if (saltBytes == null)
            {
                // Define min and max salt sizes.
                int minSaltSize = 4;
                int maxSaltSize = 8;

                // Generate a random number for the size of the salt.
                Random random = new Random();
                int saltSize = random.Next(minSaltSize, maxSaltSize);

                // Allocate a byte array, which will hold the salt.
                saltBytes = new byte[saltSize];

                // Initialize a random number generator.
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

                // Fill the salt with cryptographically strong byte values.
                rng.GetNonZeroBytes(saltBytes);
            }

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes =
                    new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];


            HashAlgorithm hash;
            // Make sure hashing algorithm name is specified.
            if (hashAlgorithm == null)
                hashAlgorithm = "";

            // Initialize appropriate hashing algorithm class.
            switch (hashAlgorithm.ToUpper())
            {
                case "SHA1":
                    hash = new SHA1Managed();
                    break;

                case "SHA256":
                    hash = new SHA256Managed();
                    break;

                case "SHA384":
                    hash = new SHA384Managed();
                    break;

                case "SHA512":
                    hash = new SHA512Managed();
                    break;

                default:
                    hash = new MD5CryptoServiceProvider();
                    break;
            }

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(hashWithSaltBytes);

            // Return the result.
            return hashValue;
        }


        public ActionResult AdminSMSCompose()
        {

            SBL.DomainModel.ComplexModel.OnlineMemberQmodel model = new SBL.DomainModel.ComplexModel.OnlineMemberQmodel();
            ViewBag.GenericOnly = "GENRICONLY";

            if (CurrentSession.UserName == "admin")
            {
                ViewBag.NoSMSLimitRequired = "TRUE";
            }
            if (CurrentSession.UserName == "Sunder Singh Verma")
            {
                ViewBag.NoSMSLimitRequired = "TRUE";
            }
            if (CurrentSession.UserName == "Yash Paul Sharma")
            {
                ViewBag.NoSMSLimitRequired = "TRUE";
            }
            return View(model);


        }
        public ActionResult AdminComposeSMS()
        {
            return View();
        }

        public ActionResult AdminSMSReport()
        {

            return View();
        }

        //Region Frequently Asked Question

        public ActionResult FAQIndex()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var Notice = (List<tFAQ>)Helper.ExecuteService("Forms", "GetAllFAQ", null);

                foreach (var tag in Notice)
                {
                    if (tag.FAA != null)
                    {
                        tag.FAA = System.Text.RegularExpressions.Regex.Replace(tag.FAA, @"<[^>]+>|&nbsp;", "").Trim();
                    }

                }

                var model = Notice.ToViewModelFAQ();

                //return View(Notice);
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                // RecordError(ex, "Getting Notices List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting NOtices List Deails";
                return View("AdminErrorPage");
            }
        }
        public ActionResult CreateFAQEntry()
        {
            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                FormsViewModel model1 = new FormsViewModel();
                // tFAQQ model1 = new tFAQQ();
                model1.Mode = "Add";

                //var Assemblylist = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                //model1.AssemblyList = new SelectList(Assemblylist, "AssemblyId", "AssemblyName", null);
                //model1.Status = 1;


                //mSession data = new mSession();
                //data.AssemblyID = Assemblylist.FirstOrDefault().AssemblyCode;
                //var Sessionlist = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                //model1.SessionList = new SelectList(Sessionlist, "SessionId", "SessionName", null);


                //var categorytype = (List<Category>)Helper.ExecuteService("ANotice", "GetAllCategoryType", null);
                //model1.CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null);
                //model1.Mode = "Add";
                // model1.Status = 0;
                // model1.TypeList = ModelMapping.GetStatusTypes();
                ////Code for SessionList 
                ////  mSession data = new mSession();
                ////  data.AssemblyID = model1.AssemblyList.FirstOrDefault().AssemblyCode;
                ////  var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                ////  model1.SessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);



                ////var model = new NoticeViewModel()

                ////{
                ////    Mode = "Add",
                ////    TypeList = ModelMapping.GetStatusTypes(),
                ////    CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null),
                ////    Status = 1
                ////    //NoticeTypeList = ModelMapping.GetNoticeType()
                ////};
                // return View(model1);

                return View(model1);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                // RecordError(ex, "Creating Notices Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Deails";
                return View("AdminErrorPage");
            }
        }
        public ActionResult SaveFAQEntry(FormsViewModel model)
        {
            try
            {
                if (model.LocalFAA != null)
                {
                    model.FAA = model.LocalFAA;
                    model.FAQ = model.LocalFAQ;
                    model.IsPublished = model.LocalIsPublished;
                }


                var notice = model.ToDomainModel1();
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                if (model.Mode == "Add")
                {
                    model.FAQ = notice.FAQ;
                    model.FAA = notice.FAA;
                    model.IsPublished = notice.IsPublished;
                    Helper.ExecuteService("Forms", "AddFAQ", notice);

                }
                else
                {

                    Helper.ExecuteService("Forms", "UpdateFAQ", notice);
                }

                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateFAQEntry");
                }
                else
                {
                    return RedirectToAction("FAQIndex");
                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Saving Notice Deails";
                return View("AdminErrorPage");
            }
        }
        public ActionResult FAQEditEntry(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var notice = (tFAQ)Helper.ExecuteService("Forms", "GetFAQById", new tFAQ { Id = Id });
                var model = notice.ToFAQViewModel("Edit");

                return View("CreateFAQEntry", model);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Deails";
                return View("AdminErrorPage");
            }

        }
        public ActionResult DeleteFAQEntry(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                // var actualnotice = (tFAQ)Helper.ExecuteService("Forms", "GetFAQById", new tFAQ { Id = Id });

                var notice = Helper.ExecuteService("Forms", "DeleteFAQById", new tFAQ { Id = Id });

                return RedirectToAction("FAQIndex");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Deleting Notices Deails";
                return View("AdminErrorPage");
            }

        }


        //Region Helpdesk
        public ActionResult HDIndex()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var Notice = (List<tUserMannuals>)Helper.ExecuteService("Forms", "GetAllHD", null);

                var model = Notice.ToViewModelHD();


                //return View(model);
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                // RecordError(ex, "Getting Notices List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting NOtices List Deails";
                return View("AdminErrorPage");
            }
        }
        public ActionResult CreateHDEntry()
        {
            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                FormsViewModel model1 = new FormsViewModel();
                // tFAQQ model1 = new tFAQQ();
                model1.Mode = "Add";

                //var Assemblylist = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                //model1.AssemblyList = new SelectList(Assemblylist, "AssemblyId", "AssemblyName", null);
                //model1.Status = 1;


                //mSession data = new mSession();
                //data.AssemblyID = Assemblylist.FirstOrDefault().AssemblyCode;
                //var Sessionlist = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                //model1.SessionList = new SelectList(Sessionlist, "SessionId", "SessionName", null);


                //var categorytype = (List<Category>)Helper.ExecuteService("ANotice", "GetAllCategoryType", null);
                //model1.CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null);
                //model1.Mode = "Add";
                // model1.Status = 0;
                // model1.TypeList = ModelMapping.GetStatusTypes();
                ////Code for SessionList 
                ////  mSession data = new mSession();
                ////  data.AssemblyID = model1.AssemblyList.FirstOrDefault().AssemblyCode;
                ////  var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                ////  model1.SessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);



                ////var model = new NoticeViewModel()

                ////{
                ////    Mode = "Add",
                ////    TypeList = ModelMapping.GetStatusTypes(),
                ////    CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null),
                ////    Status = 1
                ////    //NoticeTypeList = ModelMapping.GetNoticeType()
                ////};
                // return View(model1);

                return View(model1);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                // RecordError(ex, "Creating Notices Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Deails";
                return View("AdminErrorPage");
            }
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveHDEntry(FormsViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var notice = model.ToDomainModel2();

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (model.Mode == "Add")
                {
                    if (file != null)
                    {

                        Guid FileName1 = Guid.NewGuid();
                        var FileName2 = FileName1 + ".pdf";
                        notice.FilePath = UploadPhoto(file, FileName2);
                        //notice.Attachments = UploadPhoto(file, file.FileName);

                    }
                    else
                    {
                        notice.FilePath = "";
                    }

                    Helper.ExecuteService("Forms", "AddHD", notice);

                }
                else
                {
                    if (file != null)
                    {
                        if (!string.IsNullOrEmpty(notice.FilePath))
                        {
                            RemoveExistingPhoto(notice.FilePath);
                        }


                        Guid FileName1 = Guid.NewGuid();
                        var FileName2 = FileName1 + ".pdf";
                        notice.FilePath = UploadPhoto(file, FileName2);
                        //  notice.AssemblyId = model.AssemblyId;
                        //  notice.SessionId = model.SessionId;

                        //notice.Attachments = UploadPhoto(file, file.FileName);

                        Helper.ExecuteService("Forms", "UpdateHD", notice);
                    }
                    else
                    {
                        Helper.ExecuteService("ANotice", "UpdateHD", notice);
                    }

                }

                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateHDEntry");
                }
                else
                {
                    return RedirectToAction("HDIndex");
                }
                //  return RedirectToAction("NoticeEntryIndex");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Saving Notice  Deails");
                ViewBag.ErrorMessage = "Their is a Error While Saving Notice Deails";
                return View("AdminErrorPage");
            }
        }
        public ActionResult HDEditEntry(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var notice = (tUserMannuals)Helper.ExecuteService("Forms", "GetHDById", new tUserMannuals { MannualID = Id });
                var model = notice.ToHDViewModel("Edit");
                var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                model.ImageLocation = fileAcessingSettings.SettingValue + "/Notices/" + model.FilePath;

                return View("CreateHDEntry", model);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Deails";
                return View("AdminErrorPage");
            }

        }
        public ActionResult DeleteHDEntry(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var actualnotice = (tUserMannuals)Helper.ExecuteService("Forms", "GetHDById", new tUserMannuals { MannualID = Id });

                var notice = Helper.ExecuteService("Forms", "DeleteHD", new tUserMannuals { MannualID = Id });
                if (!string.IsNullOrEmpty(actualnotice.FilePath))
                    RemoveExistingPhoto(actualnotice.FilePath);
                return RedirectToAction("HDIndex");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Deleting Notices Deails";
                return View("AdminErrorPage");
            }

        }


        //Region SiteMap
        public ActionResult SiteMapIndex()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var Notice = (List<tSiteMap>)Helper.ExecuteService("Forms", "GetAllSiteMap", null);
                var model = Notice.ToViewModelSiteMap();
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                // RecordError(ex, "Getting Notices List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting NOtices List Deails";
                return View("AdminErrorPage");
            }
        }
        public ActionResult CreateSiteMapEntry()
        {
            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                FormsViewModel model1 = new FormsViewModel();
                model1.Mode = "Add";
                return View(model1);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                // RecordError(ex, "Creating Notices Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Deails";
                return View("AdminErrorPage");
            }
        }
        public ActionResult SaveSiteMapEntry(FormsViewModel model)
        {
            try
            {

                var notice = model.ToDomainModelSiteMap();
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                if (model.Mode == "Add")
                {
                    model.SiteSection = notice.Section;
                    model.SiteTitle = notice.Title;
                    model.SiteUrl = notice.URLPath;
                    Helper.ExecuteService("Forms", "AddSiteMap", notice);

                }
                else
                {
                    Helper.ExecuteService("Forms", "UpdateSiteMap", notice);
                }
                return RedirectToAction("SiteMapIndex");


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Saving Notice Deails";
                return View("AdminErrorPage");
            }
        }
        public ActionResult SiteMapEditEntry(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var notice = (tSiteMap)Helper.ExecuteService("Forms", "GetSiteMapById", new tSiteMap { Id = Id });
                var model = notice.ToSiteMapViewModel("Edit");
                return View("CreateSiteMapEntry", model);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Deails";
                return View("AdminErrorPage");
            }

        }
        public ActionResult DeleteSiteMapEntry(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var notice = Helper.ExecuteService("Forms", "DeleteSiteMapById", new tSiteMap { Id = Id });
                return RedirectToAction("SiteMapIndex");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Deleting Notices Deails";
                return View("AdminErrorPage");
            }

        }

        private string UploadPhoto(HttpPostedFileBase File, string FileName)
        {

            try
            {
                if (File != null)
                {
                    var UserMannual = "UserMannuals";

                    var NoticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNoticeFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    //var disFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string FilePath = System.IO.Path.Combine(FileSettings.SettingValue + UserMannual);
                    DirectoryInfo MDir = new DirectoryInfo(FilePath);
                    if (!MDir.Exists)
                    {
                        MDir.Create();
                    }

                    string path = System.IO.Path.Combine(FileSettings.SettingValue + UserMannual, FileName);//Server.MapPath("~/Images/Notice/")

                    File.SaveAs(path);

                    return (FileName);
                }
                return null;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }
        private void RemoveExistingPhoto(string OldPhoto)
        {
            // string path = System.IO.Path.Combine(Server.MapPath("~/Images/Notice/"), OldPhoto);

            try
            {
                var UserMannual = "UserMannuals";

                var noticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNoticeFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + UserMannual + "\\", OldPhoto);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public ActionResult DownloadFile(string filePath)
        {

            try
            {
                var UserMannual = "UserMannuals";
                filePath = Sanitizer.GetSafeHtmlFragment(filePath);
                var noticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNoticeFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                var urlPath = FileSettings.SettingValue + UserMannual + "\\" + filePath;
                Boolean isExist = System.IO.File.Exists(urlPath);
                if (isExist)
                {
                    var fs = System.IO.File.OpenRead(urlPath);//(Server.MapPath(fileName));
                    return File(fs, "appliaction/zip", filePath);//.Substring(fileName.LastIndexOf("/") + 1));
                }
                else
                    return null;
            }
            catch (Exception ex)
            {

                throw ex;
            }



        }

        #region
        public ActionResult RefrenceIndex(int pageId = 1, int pageSize = 25)
        {
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;
            tReferenceMaterial model = new tReferenceMaterial();
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            model.ReferenceMaterialList = (List<tReferenceMaterial>)Helper.ExecuteService("tReference", "GetReference", model);
            // ViewData["PagedList"] = Helper.BindPager(model.SpeakerPadPdfList.Count, pageId, pageSize);
            return PartialView("RefrenceIndex", model);
        }
        //public PartialViewResult CreateReference()
        //{

        //    tReferenceMaterial model = new tReferenceMaterial();
        //    model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
        //    model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
        //   // var SessionDate = (List<SBL.DomainModel.Models.Speaker.mspeakerPadPdf>)Helper.ExecuteService("Session", "GetSessionDateForPdf", mdl);

        //    //ViewBag.sessiondates = new SelectList(SessionDate, "SessionDates", "SessionDates", null);
        //   // mdl.SessionDatesList = SessionDate;
        //    return PartialView("_CreateSpeakerPadPdf", mdl);
        //}

        public ActionResult CreateReference()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                tReferenceMaterial model1 = new tReferenceMaterial();
                model1.Mode = "Add";
                model1.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                model1.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                var Memberlist = (List<SBL.DomainModel.ComplexModel.QuestionModelCustom>)Helper.ExecuteService("Member", "GetMemberList_ByAssembly", null);
                model1.MaterialTypeList = (List<SBL.DomainModel.Models.Member.mReferenceMaterialType>)Helper.ExecuteService("tReference", "GetReferenceMaterialList", null);
                model1.memberList = Memberlist;
                return View(model1);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "Their is a Error While Creating Reference Deails";
                return View("AdminErrorPage");
            }
        }
        public ActionResult SaveRefrence(tReferenceMaterial model)
        {
            try
            {
                // tReferenceMaterial mdl = new tReferenceMaterial();
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string url = "/tReference";
                string directory = FileSettings.SettingValue + url;

                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }
                int fileID = GetFileRandomNo();

                string url1 = "~/ePaper/TempFile";
                string directory1 = Server.MapPath(url1);
                if (model.IsAttachment == "Y")
                {
                    model.ReferenceMaterialAttachment = model.ReferenceMaterialAttachment;
                }
                else
                {
                    if (Directory.Exists(directory))
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                        if (savedFileName.Length > 0)
                        {
                            string SourceFile = savedFileName[0];
                            foreach (string page in savedFileName)
                            {
                                Guid FileName = Guid.NewGuid();

                                string name = Path.GetFileName(page);

                                string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                                if (!string.IsNullOrEmpty(name))
                                {
                                    if (!string.IsNullOrEmpty(model.ReferenceMaterialAttachment))
                                        System.IO.File.Delete(System.IO.Path.Combine(directory, model.ReferenceMaterialAttachment));
                                        System.IO.File.Copy(SourceFile, path, true);
                                    model.ReferenceMaterialAttachment = "/tReference/" + FileName + "_" + name.Replace(" ", "");
                                }

                            }

                        }
                        else
                        {
                            model.ReferenceMaterialAttachment = null;
                            // return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                        }

                    }

                }
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.CreateUser = CurrentSession.UserID;
                // if (model.Mode == "Add")
                //{               

                int res = (int)Helper.ExecuteService("tReference", "AddReference", model);
                if (res == 0)
                {
                    //  SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("5");
                    return RedirectToAction("RefrenceIndex");
                }
                else
                {
                    //  SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("6");
                    return RedirectToAction("RefrenceIndex");
                }
               
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "Their is a Error While Saving Notice Deails";
                return View("AdminErrorPage");
            }
        }
        public ActionResult EditReference(int Id)
        {

            try
            {
                tReferenceMaterial model = new tReferenceMaterial();
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                model.ID = Id;
                model = (tReferenceMaterial)Helper.ExecuteService("tReference", "GetRefrenceById", model.ID);
                var Memberlist = (List<SBL.DomainModel.ComplexModel.QuestionModelCustom>)Helper.ExecuteService("Member", "GetMemberList_ByAssembly", null);
                model.MaterialTypeList = (List<SBL.DomainModel.Models.Member.mReferenceMaterialType>)Helper.ExecuteService("tReference", "GetReferenceMaterialList", null);
                model.memberList = Memberlist;
                model.Mode = "Edit";
                return View("CreateReference", model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Deails";
                return View("AdminErrorPage");
            }

        }
        public ActionResult DeleteReference(int ID)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                 var _data = Helper.ExecuteService("tReference", "DeleteRefrence", ID);
                return RedirectToAction("RefrenceIndex");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "Their is a Error While Deleting Reference Material";
                return View("AdminErrorPage");
            }

        }


        static int GetFileRandomNo()
        {
            Random ran = new Random();
            int rno = ran.Next(1, 99999);
            return rno;
        }

        #endregion


        //Code For sending e-Mail to member of questions
        public ActionResult GetFilters(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            LegislationFixationModel model = new LegislationFixationModel();
            tPaperLaidV model1 = new tPaperLaidV();
            //Get the Total count of All Type of question.
            model1 = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model1);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            else
            {
                model.AssemblyId = model1.AssemblyId;
                model.SessionId = model1.SessionId;
            }



            // model.AssemblyId = model1.AssemblyId;
            //model.SessionId = model1.SessionId;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;

            mSessionDate sessionDates = new mSessionDate();

            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();


            mSession.SessionCode = model.SessionId;
            mSession.AssemblyID = model.AssemblyId;
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {

                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
                LegislationFixationModel selectList = new LegislationFixationModel();
                selectList.Id = 0;
                selectList.SessionDate = "Select Session Date";
                model.CustomSessionDateList.Add(selectList);
            }

            if (PageNumber != null && PageNumber != "")
            {
                model.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                model.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                model.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                model.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                model.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                model.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                model.loopEnd = Convert.ToInt32("5");
            }

            return PartialView("GetFilters", model);
        }

        public ActionResult eMailIndex()
        {

            LegislationFixationModel model = new LegislationFixationModel();
            tPaperLaidV model1 = new tPaperLaidV();
            //Get the Total count of All Type of question.
            model1 = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model1);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            else
            {
                model.AssemblyId = model1.AssemblyId;
                model.SessionId = model1.SessionId;
            }



            // model.AssemblyId = model1.AssemblyId;
            //model.SessionId = model1.SessionId;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;

            mSessionDate sessionDates = new mSessionDate();

            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();


            mSession.SessionCode = model.SessionId;
            mSession.AssemblyID = model.AssemblyId;
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {

                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
                LegislationFixationModel selectList = new LegislationFixationModel();
                selectList.Id = 0;
                selectList.SessionDate = "Select Session Date";
                model.CustomSessionDateList.Add(selectList);
            }

            return View(model);
        }
        public ActionResult GetQuestionListToeMail(string Date, string Assemblyid, string SessionId, string Designation)
        {
            tQuestion Question = new tQuestion();
            Date = Sanitizer.GetSafeHtmlFragment(Date);
            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Dater = DateTime.ParseExact(Date, "dd/MM/yyyy", provider);
            DateTime sessDate = Dater;
            Question.IsFixedDate = sessDate;
            Question.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            Question.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            Question.Designation = Designation;
            List<QuestionModelCustom> ListQuestion = new List<QuestionModelCustom>();
            ListQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateForEmail", Question);
            if (Designation == "Member")
            {
                var groupedList = ListQuestion.GroupBy(u => u.MemberId).Select(grp => grp.ToList()).ToList();
                ViewBag.GList = groupedList.ToList();
            }
            else 
            {
                var groupedList = ListQuestion.GroupBy(u => u.MinisterID).Select(grp => grp.ToList()).ToList();
                ViewBag.GList = groupedList.ToList();
            }
          
            ViewBag.Designation = Designation;
            Question.QuestionListForeMail = ListQuestion;
            return PartialView("GetQuestionListToeMail", Question);
        }


        public string GeneratePdfnSendeMail(string AssemblyId, string SessionId, string SessionDate, string SessionDateId, string MemberId)
        {

            int MemberID = Convert.ToInt16(MemberId);
            int AssemblyID = Convert.ToInt16(AssemblyId);
            int SessionID = Convert.ToInt16(SessionId);
            int SessionDateID = Convert.ToInt16(SessionDateId);
            string SessionDate1 = Sanitizer.GetSafeHtmlFragment(SessionDate);
            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Dater = DateTime.ParseExact(SessionDate1, "dd/MM/yyyy", provider);
            DateTime SessDate = Convert.ToDateTime(Dater);

            tQuestion Question = new tQuestion();
            Question.AssemblyID = AssemblyID;
            Question.SessionID = SessionID;
            Question.IsFixedDate = SessDate;
            Question.MemberID = MemberID;
            List<QuestionModelCustom> ListQuestion = new List<QuestionModelCustom>();
            ListQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQDetailsBySessionDatenMemId", Question);
            string outXml = "";
#pragma warning disable CS0219 // The variable 'srrial' is assigned but its value is never used
            string srrial = "";
#pragma warning restore CS0219 // The variable 'srrial' is assigned but its value is never used

            tMemberNotice model = new tMemberNotice();
            model.AssemblyID = AssemblyID;
            model.SessionID = SessionID;
            model.SessionDateId = SessionDateID;
            model.NoticeDate = SessDate;

            model.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", model);

            mSessionDate SessiondID = new mSessionDate();
            SessiondID.Id = SessionDateID;
            var FileSettings = (SBL.DomainModel.Models.Session.mSessionDate)Helper.ExecuteService("Session", "GetSessionDatesById", SessiondID);
            outXml = @"<html>                           
                     <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:1px;margin-bottom:200px;'><table style='width: 100%;'>";

            outXml += @"<table style='width: 100%;'>                     		
                        <tr>
                        <td style='text-align:center;font-size:24px;' >
                        <b>हिमाचल प्रदेश तेरहवीं विधान सभा</b>
                        </td>
                        </tr>
                        </table>";

            outXml += @"<table style='width: 100%;'>                     		
                        <tr>
                        <td style='text-align:center;font-size:22px;' >
                        <b>षष्ठम सत्र</b>
                        </td>
                        </tr>
                        </table>";

            outXml += @"<table style='width: 100%;'>                     		
                        <tr>
                        <td style='text-align:center;font-size:20px;' >
                        <b>" + FileSettings.SessionDateLocal + "</td></tr></table>";

            outXml += @"<tr>" + model.MinisterName + "</tr>";

            foreach (var item in ListQuestion)
            {
                if (item.QuestionTypeId == 1)
                {
                    outXml += @"<table style='font-family:DVOT-Yogesh;margin-top:-65px;'>";
                    outXml += @"<tr><td style='color:Blue;'>Question No.:   " + item.QuestionNumber + "</td></tr>";
                    string filteredString = item.Subject.Replace("<p>", string.Empty).Replace("</p>", string.Empty);

                    outXml += @"<br/><tr><td style='color:Blue;'>Subject:   " + WebUtility.HtmlDecode(filteredString) + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Blue;'>Member Name:   " + item.MemberName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Blue;'>Minister Name:   " + item.MinisterName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Blue;'>Department Name:   " + item.DepartmentName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Blue;'>Question:   " + WebUtility.HtmlDecode(item.MainQuestion) + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Blue; text-align:center;'>*******</td></tr></table>";
                }
                else
                {
                    outXml += @"<table style='font-family:DVOT-Yogesh;margin-top:-65px;'>";
                    outXml += @"<tr><td style='color:Black;'>Question No.:   " + item.QuestionNumber + "</td></tr>";
                    string filteredString = item.Subject.Replace("<p>", string.Empty).Replace("</p>", string.Empty);

                    outXml += @"<br/><tr><td style='color:Black;'>Subject:   " + WebUtility.HtmlDecode(filteredString) + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Black;'>Member Name:   " + item.MemberName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Black;'>Minister Name:   " + item.MinisterName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Black;'>Department Name:   " + item.DepartmentName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Black;'>Question:   " + WebUtility.HtmlDecode(item.MainQuestion) + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Black; text-align:center;'>*******</td></tr></table>";
                }

            }
            outXml += "</table> </body> </html>";

            string asda = WebUtility.HtmlDecode(outXml);
            ConvertHTMLtoPDF(outXml, AssemblyID, SessionID, SessionDate, MemberID);
            //Get eMail address by Id
            mMember mMem = new mMember();
            mMem.MemberID = MemberID;
            string RecEmail = (string)Helper.ExecuteService("Questions", "GeteMailByMemberId", mMem);
#pragma warning disable CS0219 // The variable 'subject' is assigned but its value is never used
            string subject= "Questions";
#pragma warning restore CS0219 // The variable 'subject' is assigned but its value is never used
            string Message = outXml;
            //string a= SendEmail(RecEmail, subject, Message);

           
            UpdateTable(AssemblyId, SessionId, SessionDate, SessionDateId, MemberId, RecEmail);
            return ("Success");
        }



        public string UpdateTable(string AssemblyId, string SessionId, string SessionDate, string SessionDateId, string MemberId, string recemailid)
        {
            try
            {

                tMailStatus v = new tMailStatus();
                v.AssemblyID = Convert.ToInt16(AssemblyId);
                v.SessionID = Convert.ToInt16(SessionId);
                CultureInfo provider = CultureInfo.InvariantCulture;
                var Dater = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", provider);
                DateTime SessDate = Convert.ToDateTime(Dater);
                v.SessionDate = SessDate;
                v.SessionDateId = Convert.ToInt16(SessionDateId);
                v.MemberID = Convert.ToInt16(MemberId);
                v.RecEmailId = recemailid;
                v.Type = "Questions";
                Helper.ExecuteService("Questions", "Addemailstatus", v);
                return("Updated");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Saving Notice Deails";
                return ("Error");
            }
        }


        public string GeneratePdf(string AssemblyId, string SessionId, string SessionDate, string SessionDateId, string MemberId)
        {

            int MemberID = Convert.ToInt16(MemberId);
            int AssemblyID = Convert.ToInt16(AssemblyId);
            int SessionID = Convert.ToInt16(SessionId);
            int SessionDateID = Convert.ToInt16(SessionDateId);
            string SessionDate1 = Sanitizer.GetSafeHtmlFragment(SessionDate);
            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Dater = DateTime.ParseExact(SessionDate1, "dd/MM/yyyy", provider);
            DateTime SessDate = Convert.ToDateTime(Dater);

            tQuestion Question = new tQuestion();
            Question.AssemblyID = AssemblyID;
            Question.SessionID = SessionID;
            Question.IsFixedDate = SessDate;
            Question.MemberID = MemberID;
            List<QuestionModelCustom> ListQuestion = new List<QuestionModelCustom>();
            ListQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQDetailsBySessionDatenMemId", Question);
            string outXml = "";
#pragma warning disable CS0219 // The variable 'srrial' is assigned but its value is never used
            string srrial = "";
#pragma warning restore CS0219 // The variable 'srrial' is assigned but its value is never used

            tMemberNotice model = new tMemberNotice();
            model.AssemblyID = AssemblyID;
            model.SessionID = SessionID;
            model.SessionDateId = SessionDateID;
            model.NoticeDate = SessDate;

            model.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", model);

            mSessionDate SessiondID = new mSessionDate();
            SessiondID.Id = SessionDateID;
            var FileSettings = (SBL.DomainModel.Models.Session.mSessionDate)Helper.ExecuteService("Session", "GetSessionDatesById", SessiondID);
            outXml = @"<html>                           
                     <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:1px;margin-bottom:200px;'><table style='width: 100%;'>";

            outXml += @"<table style='width: 100%;'>                     		
                        <tr>
                        <td style='text-align:center;font-size:24px;' >
                        <b>हिमाचल प्रदेश तेरहवीं विधान सभा</b>
                        </td>
                        </tr>
                        </table>";

            outXml += @"<table style='width: 100%;'>                     		
                        <tr>
                        <td style='text-align:center;font-size:22px;' >
                        <b>षष्ठम सत्र</b>
                        </td>
                        </tr>
                        </table>";

            outXml += @"<table style='width: 100%;'>                     		
                        <tr>
                        <td style='text-align:center;font-size:20px;' >
                        <b>" + FileSettings.SessionDateLocal + "</td></tr></table>";

            outXml += @"<tr>" + model.MinisterName + "</tr>";

            foreach (var item in ListQuestion)
            {
                if (item.QuestionTypeId == 1)
                {
                    outXml += @"<table style='font-family:DVOT-Yogesh;margin-top:-65px;'>";
                    outXml += @"<tr><td style='color:Blue;'>Question No.:   " + item.QuestionNumber + "</td></tr>";
                    string filteredString = item.Subject.Replace("<p>", string.Empty).Replace("</p>", string.Empty);

                    outXml += @"<br/><tr><td style='color:Blue;'>Subject:   " + WebUtility.HtmlDecode(filteredString) + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Blue;'>Member Name:   " + item.MemberName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Blue;'>Minister Name:   " + item.MinisterName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Blue;'>Department Name:   " + item.DepartmentName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Blue;'>Question:   " + WebUtility.HtmlDecode(item.MainQuestion) + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Blue; text-align:center;'>*******</td></tr></table>";
                }
                else
                {
                    outXml += @"<table style='font-family:DVOT-Yogesh;margin-top:-65px;'>";
                    outXml += @"<tr><td style='color:Black;'>Question No.:   " + item.QuestionNumber + "</td></tr>";
                    string filteredString = item.Subject.Replace("<p>", string.Empty).Replace("</p>", string.Empty);

                    outXml += @"<br/><tr><td style='color:Black;'>Subject:   " + WebUtility.HtmlDecode(filteredString) + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Black;'>Member Name:   " + item.MemberName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Black;'>Minister Name:   " + item.MinisterName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Black;'>Department Name:   " + item.DepartmentName + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Black;'>Question:   " + WebUtility.HtmlDecode(item.MainQuestion) + "</td></tr>";
                    outXml += @"<br/><tr><td style='color:Black; text-align:center;'>*******</td></tr></table>";
                }

            }
            outXml += "</table> </body> </html>";

            string asda = WebUtility.HtmlDecode(outXml);
            //ConvertHTMLtoPDF(outXml, AssemblyID, SessionID, SessionDate, MemberID);

            MemoryStream output = new MemoryStream();

            PdfConverter pdfConverter = new PdfConverter();

            // set the license key
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";


            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;

            // set the header height in points
            pdfConverter.PdfHeaderOptions.HeaderHeight = 50;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;

            TextElement footerTextElement = new TextElement(0, 30, "&p;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }
            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_Mail.pdf");
            string CurrentAssembly = CurrentSession.AssemblyId;
            string Currentsession = CurrentSession.SessionId;
            //string sessiondate = "16/02/2019";
            string sessiondateis = SessionDate.Replace("/", "_");
            var FileSettings1 = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string directory = FileSettings1.SettingValue + "/eMail/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondateis + "/" + MemberID + "/";
            DirectoryInfo Dir = new DirectoryInfo(directory);
            if (!Dir.Exists)
            {
                Dir.Create();
            }
            string fileName = "Draft_Mail.pdf";
            var path = Path.Combine(directory, fileName);

            System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
            // close file stream
            _FileStream.Close();


            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            string getdirectory = fileAcessingSettings.SettingValue + "/eMail/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondateis + "/" + MemberID + "/";
            string fname = Path.Combine(getdirectory, fileName);
            return (fname);
        }


        public ActionResult ConvertHTMLtoPDF(string outXml, int AssemblyID, int SessionID, string SessDate, int MemberID)
        {
            MemoryStream output = new MemoryStream();

            PdfConverter pdfConverter = new PdfConverter();

            // set the license key
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";


            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;

            // set the header height in points
            pdfConverter.PdfHeaderOptions.HeaderHeight = 50;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;

            TextElement footerTextElement = new TextElement(0, 30, "&p;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }
            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_Mail.pdf");
            string CurrentAssembly = CurrentSession.AssemblyId;
            string Currentsession = CurrentSession.SessionId;
            //string sessiondate = "16/02/2019";
            string sessiondateis = SessDate.Replace("/", "_");
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string directory = FileSettings.SettingValue + "/eMail/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondateis + "/" + MemberID + "/";
            DirectoryInfo Dir = new DirectoryInfo(directory);
            if (!Dir.Exists)
            {
                Dir.Create();
            }
            string fileName = "Draft_Mail.pdf";
            var path = Path.Combine(directory, fileName);

            System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
            // close file stream
            _FileStream.Close();

            string fname = Path.Combine(directory, fileName);
            string ReportURL = fname;
            byte[] FileBytes = System.IO.File.ReadAllBytes(ReportURL);
            return File(FileBytes, "application/pdf");

            // return File(output, "application/pdf");
        }

      
        public string SendEmail(string receiver, string subject, string message)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var senderEmail = new MailAddress("joginder.pathania@gmail.com", "Joginder");
                    var receiverEmail = new MailAddress(receiver, "Receiver");
                    var password = "neeju1234";
                    var sub = subject;
                    var body = message;
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(senderEmail.Address, password)
                    };
                    using (var mess = new MailMessage(senderEmail, receiverEmail)
                    {
                        IsBodyHtml = true,
                        Subject = subject,
                        Body = body
                    })
                    {
                        smtp.Send(mess);
                    }
                   
                }
                return ("Sucess");
            }
            catch (Exception)
            {
                ViewBag.Error = "Some Error";
                return ("Some Error");
            }
           
        }  


        
    }
}
