﻿using System;
using SBL.DomainModel.Models.eFile;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.Data.Entity.Infrastructure.Annotations;

using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;


namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SerialNoController : Controller
    {
        //
        // GET: /Admin/SerialNo/

        public ActionResult Index()
        {
            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var SerialNo = (List<eFilePaperSno>)Helper.ExecuteService("SerialNo", "GetAllSerialNo", null);
                var model = SerialNo.ToViewModel();
                return View(model);

            }
            //return View();

            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Highest Qualification Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateSerialNo()
        {
            var model = new SerialNoViewModel()
            {
                Mode = "Add",
                //IsActive = true

            };

            return View("CreateSerialNo", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveProgramme(SerialNoViewModel model)
        {
            eFilePaperSno SerialNo = new eFilePaperSno();
            SerialNo.year = model.year;
            SerialNo.runno = model.runno;
            SerialNo.id = Convert.ToInt32(model.strId);
            if (ModelState.IsValid)
            {

                //string strID = model.strId;

                //model.id=Convert.ToInt32(EncryptionUtility.Decrypt(strID));

                //var SerialNo = model.ToDomainModel();


                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("SerialNo", "CreateSerialNo", SerialNo);
                }
                else
                {
                    //string strID = model.strId;

                    //model.id = Convert.ToInt32(EncryptionUtility.Decrypt(strID));

                    //var vaNo = model.ToDomainModel();
                    Helper.ExecuteService("SerialNo", "UpdateSerialNo", SerialNo);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditProgramme(string id)
        {

            //mMemberHighestQualification stateToEdit = (mMemberHighestQualification)Helper.ExecuteService("HighestQualification", "GetHighestQualificationById", new mMemberHighestQualification { MemberHighestQualificationId = Id });
            ////eFilePaperSno stateToEdit = (eFilePaperSno)Helper.ExecuteService("SerialNo", "GetSerialNoById", new eFilePaperSno { id = Convert.ToInt32(EncryptionUtility.Decrypt(id.ToString())) });
            //stateToEdit.strID = ID;
            eFilePaperSno stateToEdit = (eFilePaperSno)Helper.ExecuteService("SerialNo", "GetSerialNoById", new eFilePaperSno { id = Convert.ToInt32(id) });
            var model = stateToEdit.ToViewModel("Edit", id);
            return View("CreateSerialNo", model);
        }

        public ActionResult DeleteSerialNo(int id)
        {
            eFilePaperSno stateToDelete = (eFilePaperSno)Helper.ExecuteService("SerialNo", "GetSerialNoById", new eFilePaperSno { id = id });
            Helper.ExecuteService("SerialNo", "DeleteSerialNo", stateToDelete);
            return RedirectToAction("Index");

        }

    }
}