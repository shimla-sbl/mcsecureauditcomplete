﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Category;
using SBL.DomainModel.Models.Content;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class ContentController : Controller
    {
        #region NewsList


        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "ContentError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }

        public ActionResult ContentList()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Content = (List<Content>)Helper.ExecuteService("AContent", "GetContents", null);
                var model = Content.ToViewModel1();
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting All Contents");
                ViewBag.ErrorMessage = "Their is a Error While Getting all Content Details";
                return View("AdminErrorPage");
            }

        }
        public ActionResult CreateContent()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var categorytype = (List<Category>)Helper.ExecuteService("AContent", "GetAllCategoryType", null);
                var model = new ContentViewModel()
                {
                    mode = "Add",
                    StatusTypeList = ModelMapping.GetStatusTypes(),
                    CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null),
                    VStatus = 1

                };
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Create  Contents Details");
                ViewBag.ErrorMessage = "Their is a Error While Creating Content Details";
                return View("AdminErrorPage");
            }


        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveContent(ContentViewModel model)
        {

            try
            {


                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                if (model.mode == "Add")
                {
                    //string FileName = file.FileName;
                    var data = model.ToDomainModel();
                    data.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    data.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("AContent", "AddContent", data);
                }
                else
                {
                    var data = model.ToDomainModel();
                    data.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    data.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("AContent", "UpdateContent", data);
                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateContent");
                }
                else
                {
                    return RedirectToAction("ContentList");
                }


            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Contents Details");
                ViewBag.ErrorMessage = "Their is a Error While Saving Content Details";
                return View("AdminErrorPage");
            }
        }
        public ActionResult EditContent(string Id)
        {

            try
            {

            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting All Contents");
                ViewBag.ErrorMessage = "Their is a Error While Getting all Content Details";
                return View("AdminErrorPage");
            }

            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }


            var categorytype = (List<Category>)Helper.ExecuteService("AContent", "GetAllCategoryType", null);
            //var Content = (Content)Helper.ExecuteService("AContent", "EditContent", new Content { ContentID = Id });
            var Content = (Content)Helper.ExecuteService("AContent", "EditContent", new Content { ContentID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = Content.ToViewModel1("Edit");
            model.CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null);
            return View("CreateContent", model);

        }

        public ActionResult DeleteContent(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                var Content = Helper.ExecuteService("AContent", "DeleteContent", new Content { ContentID = Id });
                return RedirectToAction("ContentList");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Deleting  Contents Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting  Content Details";
                return View("AdminErrorPage");
            }



        }

        #endregion

        #region NewsCategory
        public ActionResult ContentCategoryList()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                var Content = (List<Category>)Helper.ExecuteService("AContent", "GetContentCategory", null);
                var model = Content.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting All Contents Categories ");
                ViewBag.ErrorMessage = "Their is a Error While Getting all Content Category List Details";
                return View("AdminErrorPage");
            }



        }
        public ActionResult CreateContentCategtory()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var model = new CategoryViewModel()
                {
                    Mode = "Add",
                    TypeList = ModelMapping.GetStatusTypes(),
                    CategoryTypeList = ModelMapping.GetCategoryTypes()
                    //CategoryList=ModelMapping.GetNewsCategory()
                };
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Creating Contents Category");
                ViewBag.ErrorMessage = "Their is a Error While Creating  Content Details";
                return View("AdminErrorPage");
            }



        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveContentCategtory(CategoryViewModel model)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                if (model.Mode == "Add")
                {
                    model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    var newmodel = model.ToDomainModel();
                    newmodel.Status = 1;
                    Helper.ExecuteService("AContent", "AddContentCategory", newmodel);

                }
                else
                {
                    // model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("AContent", "UpdateContentCategory", model.ToDomainModel());
                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateContentCategtory");
                }
                else
                {
                    return RedirectToAction("ContentCategoryList");
                }
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving  Contents");
                ViewBag.ErrorMessage = "Their is a Error While Saving Content Details";
                return View("AdminErrorPage");
            }


            // return RedirectToAction("NewsIndex");
        }
        public ActionResult EditContentCategtory(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var Content = (Category)Helper.ExecuteService("AContent", "GetContentCategoryById", new Category { CategoryID = Id });
                var Content = (Category)Helper.ExecuteService("AContent", "GetContentCategoryById", new Category { CategoryID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                return View("CreateContentCategtory", Content.ToViewModel("Edit"));
            }
            catch (Exception ex)
            {

                RecordError(ex, "Edit Contents");
                ViewBag.ErrorMessage = "Their is an Error While Editing Content Details";
                return View("AdminErrorPage");
            }





        }
        public ActionResult DeleteContentCategtory(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Content = Helper.ExecuteService("AContent", "DeleteContentCategory", new Category { CategoryID = Id });

                return RedirectToAction("ContentCategoryList");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Delete Content Details");
                ViewBag.ErrorMessage = "Their is a Error While Deleting Content Details";
                return View("AdminErrorPage");
            }

        }


        public JsonResult CheckContentCategoryChildExist(int categoryId)
        {
            try
            {
                var isExist = (Boolean)Helper.ExecuteService("AContent", "IsContentCategoryChildExist", new Category { CategoryID = categoryId });

                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }




        }
        #endregion


    }
}
