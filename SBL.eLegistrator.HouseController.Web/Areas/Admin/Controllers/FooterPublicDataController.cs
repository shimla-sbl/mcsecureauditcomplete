﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SBL.DomainModel.Models.FooterPublicData;
using SBL.eLegistrator.HouseController.Web.Helpers;
//using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class FooterPublicDataController : Controller
    {
        //
        // GET: /Admin/FooterPublicData/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var FooterPublicData = (List<FooterPublicData>)Helper.ExecuteService("FooterPublicDatas", "GetAllFooterPublicData", null);
                var model = FooterPublicData.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting FooterPublicData Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateFooterPublicData()
        {
            var model = new FooterPublicDataViewModel()
            {
                Mode = "Add"

            };

            return View("CreateFooterPublicData", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveFooterPublicData(FooterPublicDataViewModel model)
        {

            if (ModelState.IsValid)
            {
                var bill = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("FooterPublicDatas", "CreateFooterPublicData", bill);
                }
                else
                {
                    Helper.ExecuteService("FooterPublicDatas", "UpdateFooterPublicData", bill);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditFooterPublicData(string Id)
        {

            //FooterPublicData FooterPublicDataToEdit = (FooterPublicData)Helper.ExecuteService("FooterPublicDatas", "GetFooterPublicDataById", new FooterPublicData { ID = Id });
            FooterPublicData FooterPublicDataToEdit = (FooterPublicData)Helper.ExecuteService("FooterPublicDatas", "GetFooterPublicDataById", new FooterPublicData { ID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = FooterPublicDataToEdit.ToViewModel1("Edit");
            return View("CreateFooterPublicData", model);
        }

        public ActionResult DeleteFooterPublicData(int Id)
        {
            FooterPublicData FooterPublicDataToDelete = (FooterPublicData)Helper.ExecuteService("FooterPublicDatas", "GetFooterPublicDataById", new FooterPublicData { ID = Id });
            Helper.ExecuteService("FooterPublicDatas", "DeleteFooterPublicData", FooterPublicDataToDelete);
            return RedirectToAction("Index");

        }


        [HttpPost]
        public JsonResult CheckId(string TitleID)
        {

            FooterPublicData obj = new FooterPublicData();


            obj.TitleID = TitleID;
            string st = Helper.ExecuteService("FooterPublicDatas", "CheckId", obj) as string;

            return Json(st, JsonRequestBehavior.AllowGet);
        }

    }
}
