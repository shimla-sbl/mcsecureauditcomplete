﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.UploadMetaFile;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net;
//using System.Data.SqlClient;
using System.Text.RegularExpressions;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.House;
//using System.Data.EntityClient;
using System.Configuration;
using System.Web.Configuration;
using System.Data.SqlClient;
using SBL.DomainModel;
using SBL.DomainModel.Models.Passes;


namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class UploadMetaFileController : Controller 
    {
        //
        // GET: /Admin/UploadMetaFile/
     
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetAllMetaDataFile(int pageId = 1, int pageSize = 10)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                UploadMetaViewModel model = new UploadMetaViewModel();

                //var tassemblyfile = (List<mUploadmetaDataFile>)Helper.ExecuteService("UploadMetaFile", "GetAllMetaDataFile", null);
                //if (tassemblyfile != null)
                //{
                //    model.MetaDataList = tassemblyfile.ToViewModel();
                //}
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                model.AssemblyId =data.AssemblyID;
                model.SessionId = sesdate.SessionId;
                //ViewBag.AssemblyId = sesdate.SessionId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);
                var departmentlist = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
                var committeelist = (List<tCommittee>)Helper.ExecuteService("UploadMetaFile", "GetAllCommittee", null);
                // Code for DoumentTypeList
              model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
                  model.SessionList = sessionList;
                model.DepartmentList = departmentlist;
                model.CommiteeList = committeelist;
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "";
                return View("AdminErrorPage");
            }
        }
        public ActionResult GetSessionDate(string assembly, string session)
        {

            // Code for DoumentTypeList
            UploadMetaViewModel model = new UploadMetaViewModel();
            if (!string.IsNullOrEmpty(assembly as string) && !string.IsNullOrEmpty(session as string))
            {
                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = Convert.ToInt32(session);
                sesdate.AssemblyId = Convert.ToInt32(assembly);

                model.SessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);
            }
            return Json(model.SessionDateList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetSessionList(string assembly)
        {

            // Code for DoumentTypeList
            UploadMetaViewModel model = new UploadMetaViewModel();
            if (!string.IsNullOrEmpty(assembly as string))
            {
                mSession data = new mSession();
                data.AssemblyID = Convert.ToInt32(assembly);

                model.SessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
            }
            return Json(model.SessionList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetAssemblyList()
        {

            // Code for DoumentTypeList
            UploadMetaViewModel model = new UploadMetaViewModel();

            model.AssemblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

            return Json(model.AssemblyList, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult SaveMetaFile(UploadMetaViewModel model, List<HttpPostedFileBase> fileUpload)
        {
            try
            {
                foreach (var Postfile in fileUpload)
                {
                    if (Postfile != null)
                    {
                        string[] dateArr = model.SessionDateId.Split('-');
                        string sessiondate = dateArr[2].Trim() + "" + dateArr[1].PadLeft(2, '0') + "" + dateArr[0].PadLeft(2, '0');
                        string url = "";
                        if (model.DocumentTypeId == "Question")
                        {
                            url = "/AssemblyFiles/" + model.AssemblyId + "/" + model.SessionId + "/" + sessiondate + "/" + model.QuestionId;
                            model.BillId = "0";
                            model.SpeechId = "0";
                            model.SpeechType = "0";
                            model.CommitteeId = "0";
                            model.DepartmentId = "0";
                            model.CommitteeTypeId = "0";
                        }
                        else if (model.DocumentTypeId == "Bills")
                        {
                            url = "/AssemblyFiles/" + model.AssemblyId + "/" + model.DocumentTypeId + "/" + model.BillId;
                            model.QuestionId = "0";
                            model.SpeechId = "0";
                            model.SpeechType = "0";
                            model.CommitteeId = "0";
                            model.DepartmentId = "0";
                            model.CommitteeTypeId = "0";
                        }
                        else if (model.DocumentTypeId == "Debates")
                        {
                            string exten = Path.GetExtension(Postfile.FileName);
                            if (exten == ".png" || exten == ".jpg" || exten == ".gif" || exten == ".bmp")
                            {
                                url = "/AssemblyFiles/" + model.AssemblyId + "/" + model.SessionId + "/" + sessiondate + "/8";
                            }
                            else
                            {
                                url = "/AssemblyFiles/" + model.AssemblyId + "/" + model.SessionId + "/" + sessiondate;
                            }
                           
                            model.BillId = "0";
                            model.SpeechId = "0";
                            model.SpeechType = "0";
                            model.QuestionId = "0";
                            model.CommitteeId = "0";
                            model.DepartmentId = "0";
                            model.CommitteeTypeId = "0";
                        }
                        else if (model.DocumentTypeId == "Debates1")
                        {
                            string exten = Path.GetExtension(Postfile.FileName);
                            if (exten == ".png" || exten == ".jpg" || exten == ".gif" || exten == ".bmp")
                            {
                                url = "/AssemblyFiles/" + model.AssemblyId + "/" + model.SessionId + "/" + sessiondate + "/" +"Edited"   + "/8";
                            }
                            else
                            {
                                url = "/AssemblyFiles/" + model.AssemblyId + "/" + model.SessionId + "/" + sessiondate + "/" + "Edited";
                            }

                            model.BillId = "0";
                            model.SpeechId = "0";
                            model.SpeechType = "0";
                            model.QuestionId = "0";
                            model.CommitteeId = "0";
                            model.DepartmentId = "0";
                            model.CommitteeTypeId = "0";
                        }
                        else if (model.DocumentTypeId == "Speeches")
                        {
                            string exten = Path.GetExtension(Postfile.FileName);
                            if (exten == ".png" || exten == ".jpg" || exten == ".gif" || exten == ".bmp")
                            {
                               // url = "/AssemblyFiles/" + model.AssemblyId + "/" + model.SessionId + "/" + sessiondate + "/" + "Edited" + "/8";
                            }
                            else
                            {
                                url = "/Speeches/" + model.AssemblyId + "/" + model.SessionId + "/" + sessiondate + "/" + model.SpeechId + "/" + model.SpeechType;
                            }

                            model.BillId = "0";
                            model.QuestionId = "0";
                            model.CommitteeId = "0";
                            model.DepartmentId = "0";
                            model.CommitteeTypeId = "0";
                        }
                        else if (model.DocumentTypeId == "Passes")
                        {
                            string exten = Path.GetExtension(Postfile.FileName);
                            if (exten == ".png" || exten == ".jpg" || exten == ".gif" || exten == ".bmp")
                            {
                                url = "/Images/" + "/Pass/" + "/Photo/" + model.AssemblyId + "/" + model.SessionId;
                            }
                            else
                            {
                                //url = "/Images/" + "/Pass/" + "/Photo/" + model.AssemblyId + "/" + model.SessionId ;
                            }

                            model.BillId = "0";
                            model.QuestionId = "0";
                            model.CommitteeId = "0";
                            model.DepartmentId = "0";
                            model.CommitteeTypeId = "0";
                        }
                        else if (model.DocumentTypeId == "CommitteeReports")
                        {
                            url = "/AssemblyFiles/" + model.AssemblyId + "/" + model.SessionId + "/" + sessiondate.Trim() + "/" + model.CommitteeId.Trim() + "/" + model.DepartmentId.Trim() + "/" + model.CommitteeTypeId.Trim();
                            model.BillId = "0";
                            model.QuestionId = "0";
                            model.SpeechId = "0";
                            model.SpeechType = "0";
                        }

                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                        //string sourceDir = Server.MapPath(url);

                        if (Directory.Exists(directory))
                        {
                            Postfile.SaveAs(directory + "/" + Postfile.FileName);

                        }
                        else
                        {
                            Directory.CreateDirectory(directory);
                            Postfile.SaveAs(directory + "/" + Postfile.FileName);
                        }
                        model.PdfFile = Postfile.FileName;
                        model.CreatedDate = DateTime.Now;
                        var datamodel = model.ToUploadModel();
                        Helper.ExecuteService("UploadMetaFile", "AddUploadmetaData", datamodel);

                    }
                }

                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                model.AssemblyId =data.AssemblyID;
                model.SessionId = sesdate.SessionId;
                ViewBag.SessionId = sesdate.SessionId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);
             
                var departmentlist = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
                var committeelist = (List<tCommittee>)Helper.ExecuteService("UploadMetaFile", "GetAllCommittee", null);
                // Code for DoumentTypeList
                model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
               model.SessionList = sessionList;
                model.DepartmentList = departmentlist;
                model.CommiteeList = committeelist;
             
                return PartialView("_UploadMetaDataFile", model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "";
                return View("AdminErrorPage");
            }
        }

        public ActionResult UploadMetaDataFile()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                UploadMetaViewModel model = new UploadMetaViewModel();

                //Code for AssemblyList 
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                model.AssemblyId = data.AssemblyID;
                model.SessionId = sesdate.SessionId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);
                // Code for DoumentTypeList
                model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;


                return View("_UploadMetaDataFile", model);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "Their is a Error While Create the AssemblyFile Details";
                return View("AdminErrorPage");
            }

        }


        //public ActionResult SaveUploadMetaData(UploadMetaViewModel model, HttpPostedFileBase file)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(CurrentSession.UserID))
        //        {
        //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //        }

        //        var data = model.ToUploadModel();
        //        if (file != null)
        //        {

        //            data.PdfFile = UploadFiles(file, model);

        //        }
        //        else
        //        {
        //            model.PdfFile = "No Available";

        //        }
        //        Helper.ExecuteService("UploadMetaFile", "AddUploadmetaData", data);


        //        return RedirectToAction("GetAllMetaDataFile");
        //    }
        //    catch (Exception ex)
        //    {


        //        ViewBag.ErrorMessage = "Their is a Error While Saving the AssemblyFile Details";
        //        return View("AdminErrorPage");
        //    }


        //}

        public string SaveScript(HttpPostedFileBase ScriptFile)
        {
            try
            {
                var orfile = Server.MapPath("/AssemblyScript/");
                DirectoryInfo Dir = new DirectoryInfo(orfile);
                if (!Dir.Exists)
                {
                    Dir.Create();
                }
                string url = orfile + ScriptFile.FileName;
                ScriptFile.SaveAs(url);

                //EntityConnection entityConnection = context.Connection as EntityConnection;
                //string sqlConnectionString = WebConfigurationManager.ConnectionStrings["eVidhan"].ConnectionString;
                // string providerConnectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;
                // <db connectionstring="Data Source=10.25.129.161\SQLEXPRESS,1433;Initial Catalog=HPMS_IntranetApp;Persist Security Info=True;User ID=sa;Password=e_Vidhan_PDB"/>
               string sqlConnectionString = "Data Source=10.25.129.161\\SQLEXPRESS,1433;Initial Catalog=HPMS_IntranetApp;Persist Security Info=True;User ID=sa;Password=e_Vidhan_PDB";
                //string sqlConnectionString = "Data Source=SBL-THINK-02;Initial Catalog=HPMS_26June;Persist Security Info=True;User ID=sa;Password=Microsoftsql_1";
                SqlConnection con = new SqlConnection(sqlConnectionString);

                FileInfo file = new FileInfo(url);
                string script = file.OpenText().ReadToEnd();

                SqlConnection conn = new SqlConnection(sqlConnectionString);

                //IEnumerable<string> scriptarr = SplitSqlStatements(script);
                conn.Open();

                SqlCommand sqlCmd = new SqlCommand(script, conn);
                sqlCmd.ExecuteNonQuery();
                return "Script successfuly uploaded";
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "Their is a Error While Create the AssemblyFile Details";
                return "Error on file uploading";
            }
          
        }
        private static IEnumerable<string> SplitSqlStatements(string sqlScript)
        {
            // Split by "GO" statements
            var statements = Regex.Split(
                    sqlScript,
                    @"^\s*GO\s* ($ | \-\- .*$)",
                    RegexOptions.Multiline |
                    RegexOptions.IgnorePatternWhitespace |
                    RegexOptions.IgnoreCase);

            // Remove empties, trim, and return
            return statements
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.Trim(' ', '\r', '\n'));
        }
        //private string UploadFiles(HttpPostedFileBase file, UploadMetaViewModel model)
        //{
        //    var FileName = "";
        //    if (file != null)
        //    {
        //        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
        //        DirectoryInfo AssemblyDir = new DirectoryInfo(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.AssemblyId);
        //        var url = "";

        //        if (!AssemblyDir.Exists)
        //        {
        //            AssemblyDir.Create();
        //        }
        //        if (model.DocumentTypeId == "Bills")
        //        {
        //            var BillFile = AssemblyDir + "/" + model.BillId;
        //            DirectoryInfo Dir = new DirectoryInfo(BillFile);
        //            if (!Dir.Exists)
        //            {
        //                Dir.Create();
        //            }
        //            url = BillFile + file.FileName + ".pdf";
        //        }
        //        else if (model.DocumentTypeId == "Debates")
        //        {
        //            string[] sessionDateFile = new string[3];
        //            var sessioondatestring = "";
        //            if (model.SessionDateId.Contains("/"))
        //            {
        //                sessionDateFile = model.SessionDateId.Split('/');
        //            }
        //            else if (model.SessionDateId.Contains("-"))
        //            {
        //                sessionDateFile = model.SessionDateId.Split('-');
        //            }
        //            else if (model.SessionDateId.Contains(" "))
        //            {
        //                sessionDateFile = model.SessionDateId.Split(' ');
        //            }

        //            sessioondatestring = sessionDateFile[2] + sessionDateFile[1] + sessionDateFile[0];
        //            var debatesFile = AssemblyDir + "/" + model.SessionId + "/" + sessioondatestring;
        //            DirectoryInfo Dir = new DirectoryInfo(debatesFile);
        //            if (!Dir.Exists)
        //            {
        //                Dir.Create();
        //            }
        //            FileName = file.FileName;
        //            url = debatesFile + "/" + FileName;
        //        }
        //        file.SaveAs(url);


        //    }
        //    return FileName;

        //}
        public ActionResult DeleteRecords(string AllIds, string Doctype, string Assembly, string Session, string Date)
        {
            try
            {
              
                UploadMetaViewModel model = new UploadMetaViewModel();
                string ViewString = "";
                if (!string.IsNullOrEmpty(AllIds) && !string.IsNullOrEmpty(Doctype) && !string.IsNullOrEmpty(Assembly) && !string.IsNullOrEmpty(Session) && !string.IsNullOrEmpty(Date))
                {
                    
                    mUploadmetaDataFile Searchmodel = new mUploadmetaDataFile();
                    Searchmodel.DeletedIdsList = AllIds;
                    DateTime? datestr = new DateTime();
                    datestr = new DateTime(Convert.ToInt32(Date.Split('/')[2]), Convert.ToInt32(Date.Split('/')[1]), Convert.ToInt32(Date.Split('/')[0]));
                    Searchmodel.SessionDate = datestr;
                    Searchmodel.AssemblyId = Convert.ToInt32(Assembly);
                    Searchmodel.SessionId = Convert.ToInt32(Session);
                    if (Doctype == "SQuestion")
                    {
                        model.StarredQuestionsHistoryList = (List<tStarredQuestionsHistory>)Helper.ExecuteService("UploadMetaFile", "DeleteSQuestionList", Searchmodel);
                        ViewString = "_SQuestionList";
                    }
                    else if (Doctype == "USQuestion")
                    {
                        model.UnStarredQuestionsHistoryList = (List<tUnStarredQuestionsHistory>)Helper.ExecuteService("UploadMetaFile", "DeleteUSQuestionList", Searchmodel);
                        ViewString = "_USQuestionList";
                    }
                    else if (Doctype == "Debates")
                    {
                        model.ReporterDescriptionsLogList = (List<ReporterDescriptionsLog>)Helper.ExecuteService("UploadMetaFile", "DeleteDebateList", Searchmodel);
                        ViewString = "_DebateList";
                    }

                }


                return View(ViewString, model);
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }
        public ActionResult SearchMetaDataFile(string Assembly, string Session, string Date, string Doctype)
        {

            try
            {
                UploadMetaViewModel model = new UploadMetaViewModel();
                string ViewString = "";
                if (!string.IsNullOrEmpty(Assembly) && !string.IsNullOrEmpty(Session) && !string.IsNullOrEmpty(Date) && !string.IsNullOrEmpty(Doctype))
                {
                    mUploadmetaDataFile Searchmodel = new mUploadmetaDataFile();
                    DateTime? datestr = new DateTime();
                    datestr = new DateTime(Convert.ToInt32(Date.Split('-')[2]), Convert.ToInt32(Date.Split('-')[1]), Convert.ToInt32(Date.Split('-')[0]));
                    Searchmodel.SessionDate = datestr;
                    Searchmodel.AssemblyId = Convert.ToInt32(Assembly);
                    Searchmodel.SessionId = Convert.ToInt32(Session);

                    if (Doctype == "SQuestion")
                    {
                        model.StarredQuestionsHistoryList = (List<tStarredQuestionsHistory>)Helper.ExecuteService("UploadMetaFile", "GetFilteredSQuestionList", Searchmodel);
                        ViewString = "_SQuestionList";
                    }
                    else if (Doctype == "USQuestion")
                    {
                        model.UnStarredQuestionsHistoryList = (List<tUnStarredQuestionsHistory>)Helper.ExecuteService("UploadMetaFile", "GetFilteredUSQuestionList", Searchmodel);
                        ViewString = "_USQuestionList";
                    }
                    else if (Doctype == "Debates")
                    {
                        model.ReporterDescriptionsLogList = (List<ReporterDescriptionsLog>)Helper.ExecuteService("UploadMetaFile", "GetFilteredDebateList", Searchmodel);
                        ViewString = "_DebateList";
                    }
                  
                }


                return View(ViewString, model);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "Their is a Error While Create the AssemblyFile Details";
                return View("AdminErrorPage");
            }

        }
      
        
    }
}
