﻿using SBL.DomainModel.ComplexModel.MediaComplexModel;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class DashboardController : Controller
    {
        //
        // GET: /Admin/Dashboard/

        public ActionResult Index()
        {
            MediaModel Mpass = new MediaModel();


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId) && string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                var model1 = (MediaModel)Helper.ExecuteService("Media", "GetDashBoardValue", Mpass);
                CurrentSession.AssemblyId = model1.AssemblyId.ToString();
                CurrentSession.SessionId = model1.SessionId.ToString();

            }

            return View();
        }

    }
}
