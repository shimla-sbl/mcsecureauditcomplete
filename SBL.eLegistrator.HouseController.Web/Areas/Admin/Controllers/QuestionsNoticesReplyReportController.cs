﻿using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class QuestionsNoticesReplyReportController : Controller
    {
        //
        // GET: /Admin/QuestionsNoticesReplyReport/

        public ActionResult Index()
        {
            LegislationFixationModel model = new LegislationFixationModel();


            tPaperLaidV model1 = new tPaperLaidV();
            //Get the Total count of All Type of question.
            model1 = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model1);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            else
            {

                model.AssemblyId = model1.AssemblyId;//Convert.ToInt16(CurrentSession.AssemblyId);
                model.SessionId = model1.SessionId;// Convert.ToInt16(CurrentSession.SessionId);
            }



            // model.AssemblyId = model1.AssemblyId;
            //model.SessionId = model1.SessionId;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;

            mSessionDate sessionDates = new mSessionDate();

            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();


            mSession.SessionCode = model.SessionId;
            mSession.AssemblyID = model.AssemblyId;
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {

                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
                LegislationFixationModel selectList = new LegislationFixationModel();
                selectList.Id = 0;
                selectList.SessionDate = "Select Session Date";
                model.CustomSessionDateList.Add(selectList);


            }
            return View( model);
        }

    }
}
