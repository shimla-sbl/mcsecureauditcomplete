﻿using SBL.DomainModel.Models.AssemblyFileSystem;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using System.Data;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using iTextSharp.text;
using iTextSharp.text.pdf;
using EvoPdf;
using SBL.DomainModel.Models.Event;


namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class AttendenceController : Controller
    {
        //
        // GET: /Admin/Attendence/

        public ActionResult MemberAttendence()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                AssemblyFileViewModel model = new AssemblyFileViewModel();

                //Code for AssemblyList 
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                model.VAssemblyId = data.AssemblyID;
                model.VSessionId = sesdate.SessionId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                // Code for DoumentTypeList
                var doctypeList = (List<mAssemblyTypeofDocuments>)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyTypeofDocumentLst", null);
                model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;
                model.AssemblyDocumentTypeList = doctypeList;
                model.StatusTypeList = ModelMapping.GetStatusTypes();
                model.Mode = "Add";
                return View("MemberAttendence", model);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Creat Assembly File");
                ViewBag.ErrorMessage = "Their is a Error While Create the AssemblyFile Details";
                return View("AdminErrorPage");
            }



        }
        public JsonResult GetAttendenceData(int sessionDateId, int assemblyId, int sessionId)
        {

            List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter1);
            string filephyacesSetting = "";

           

                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "FileAccessingUrlPath")
                    {
                        filephyacesSetting = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }

                }



                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@AseemblyId", Convert.ToString(assemblyId)));
                methodParameter.Add(new KeyValuePair<string, string>("@SessionId", Convert.ToString(sessionId)));
                methodParameter.Add(new KeyValuePair<string, string>("@SessionDateId", Convert.ToString(sessionDateId)));
                string outXml = "";
                int j = 1;
                DataSet MembersData = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetAttendence", methodParameter);
                if (MembersData.Tables[0].Rows.Count != 0)
                {
                for (int i = 0; i < MembersData.Tables[0].Rows.Count; i++)
                {
                    string Name = Convert.ToString(MembersData.Tables[0].Rows[i]["Name"]);
                    string ConstituencyName = Convert.ToString(MembersData.Tables[0].Rows[i]["ConstituencyName"]);
                    DateTime TimefromServer = Convert.ToDateTime(MembersData.Tables[0].Rows[i]["AttendanceDt"]);
                    string Time1 = TimefromServer.ToString("dd/MM/yyyy");
                    string Time = TimefromServer.ToString("dd/MM/yyyy hh:mm tt");
                    if (i == 0)
                    {
                        var actinlink = Url.Action("PDFforHonbleMember", "Attendence", new { date = sessionDateId, session = sessionId, Assem = assemblyId });
                        outXml += "<div style='text-align:center;'><h2>Attendance of Hon'ble Members (" + Time1 + ")</h2></div>";
                        outXml += "<div style='height: 27px; position: relative; top: -10px;'><span><a href='" + actinlink + "', target = '_blank', class='PdfBackGround pdficon'></a></span></div>";
                        outXml += "<table style='width:100%' class='table table-striped table-bordered table-hover dataTable'>";
                        outXml += @"<tr><td style='width:5%;text-align:center;'><b>Sr.No.</b></td><td style='width:25%;text-align:center;'><b>Name</b></td><td style='width:25%;text-align:center;'><b>Constituency<b></td><td style='width:25%;text-align:center;'><b>Date/Time<b></td><td style='width:25%;text-align:center;'><b>Signature</td></tr>";
                    }

                    string SigNature = Convert.ToString(MembersData.Tables[0].Rows[i]["SignaturePath"]);
                    string sign = "";
                    if (SigNature == null)
                    {
                        sign = "<td style='width:25%'></td>";
                    }
                    else if (SigNature == "")
                    {
                        sign = "<td style='width:25%'></td>";

                    }
                    else
                    {
                        string URL = filephyacesSetting + SigNature;
                        sign = "<td style='width:25%'><img style='height:50px; width:100px;' src='" + URL + "' alt='Sign' /></td>";
                    }

                    outXml += @"<tr><td style='width:5%;text-align:center;'>" + j + "</td><td style='width:25%'>" + Name + "</td><td style='width:25%'>" + ConstituencyName + "</td><td style='width:25%;text-align:center;'>" + Time + "</td>" + sign + "</tr>";
                    // i++;
                    j++;
                }
                outXml += "</table>";
            }
            else
            {
                outXml += "No Data Found";
            }
            string SessionData = outXml;
            return Json(SessionData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDiscussionData(int sessionDateId, int assemblyId, int sessionId, int Event)
        {

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@AseemblyId", Convert.ToString(assemblyId)));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", Convert.ToString(sessionId)));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDateId", Convert.ToString(sessionDateId)));
            string outXml = "";
            int j = 1;
            DataSet MembersData = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Members_Discussion", methodParameter);
            for (int i = 0; i < MembersData.Tables[0].Rows.Count; i++)
            {
                string Name = Convert.ToString(MembersData.Tables[0].Rows[i]["Name"]);
                DateTime SessionDate = Convert.ToDateTime(MembersData.Tables[0].Rows[i]["SessionDate"]);
                string Date = SessionDate.ToString("dd/MM/yyyy");
                string EventName = Convert.ToString(MembersData.Tables[0].Rows[i]["EditedDescription"]);
                DateTime startTime = Convert.ToDateTime(MembersData.Tables[0].Rows[i]["startTime"]);
                DateTime endtime = Convert.ToDateTime(MembersData.Tables[0].Rows[i]["endtime"]);
                string ST = startTime.ToString("hh:mm tt");
                string ET = endtime.ToString("hh:mm tt");

                if (i == 0)
                {
                    var actinlink = Url.Action("PDFforHonbleMemberDiscussion", "Attendence", new { date = sessionDateId, session = sessionId, Assem = assemblyId });
                    outXml += "<div><span class='menu-text'><h1><b>Discussion Time by Hon'ble Member (" + Date + ")</b></span></div>";
                    outXml += "<div style='height: 27px; position: relative; top: -10px;'><span><a href='" + actinlink + "', target = '_blank', class='PdfBackGround pdficon'></a></span></div>";
                    outXml += "<table style='width:100%' class='table table-striped table-bordered table-hover dataTable'>";
                    outXml += @"<tr><td style='width:5%;text-align:center;'><b>Sr.No.</b></td><td style='width:25%;text-align:center;'><b>Name</b></td><td style='width:25%;text-align:center;'><b>Event Name<b></td><td style='width:25%;text-align:center;'><b>Start Time<b></td><td style='width:25%;text-align:center;'><b>End Time</td></tr>";
                }



                outXml += @"<tr><td style='width:5%;text-align:center;'>" + j + "</td><td style='width:25%'><h3>" + Name + "</h3></td><td style='width:25%'><h3>" + EventName + "</h3></td><td style='width:25%;text-align:center;'><h3>" + ST + "</h3></td><td style='width:25%'><h3>" + ET + "</h3></tr>";
                // i++;
                j++;
            }
            outXml += "</table>";
            string SessionData = outXml;
            return Json(SessionData, JsonRequestBehavior.AllowGet);
        }


        public void PDFforHonbleMember(int date, int session, int Assem)
        {
            List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter1);
            string filephyacesSetting = "";



            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "FileAccessingUrlPath")
                {
                    filephyacesSetting = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }

            }
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@AseemblyId", Convert.ToString(Assem)));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", Convert.ToString(session)));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDateId", Convert.ToString(date)));
            string outXml = "";
            int j = 1;
            DataSet MembersData = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetAttendence", methodParameter);
            for (int i = 0; i < MembersData.Tables[0].Rows.Count; i++)
            {
                string Name = Convert.ToString(MembersData.Tables[0].Rows[i]["Name"]);
                string ConstituencyName = Convert.ToString(MembersData.Tables[0].Rows[i]["ConstituencyName"]);
                DateTime TimefromServer = Convert.ToDateTime(MembersData.Tables[0].Rows[i]["AttendanceDt"]);
                string Time1 = TimefromServer.ToString("dd/MM/yyyy");
                string Time = TimefromServer.ToString("dd/MM/yyyy hh:mm tt");
                if (i == 0)
                {

                    outXml += "<div style='text-align:center;'><h2>Attendance of Hon'ble Members (" + Time1 + ")</h2></span></div>";
                    outXml += "<table style='width:100%;border: 1px solid black; border-collapse: collapse;'>";
                    outXml += @"<tr><td style='width:5%;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:50px'><b>Sr.No.</b></td><td style='width:25%;text-align:center;border: 1px solid black; border-collapse: collapse;height:50px'><b>Name</b></td><td style='width:25%;text-align:center;border: 1px solid black; border-collapse: collapse;height:50px'><b>Constituency<b></td><td style='width:25%;text-align:center;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:50px'><b>Date/Time<b></td><td style='width:25%;text-align:center;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:50px'><b>Signature</td></tr>";
                }

                string SigNature = Convert.ToString(MembersData.Tables[0].Rows[i]["SignaturePath"]);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string Sign = SigNature.Replace('/', '\\');
                string FinalSig = FileSettings.SettingValue + Sign;
               
              
                if (SigNature == null)
                {
                 outXml += @"<tr><td style='width:5%;text-align:center;text-align:center;border: 1px solid black; border-collapse: collapse;height:60px'>" + j + "</td><td style='width:25%;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:60px'>" + Name + "</td><td style='width:25%;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:50px'>" + ConstituencyName + "</td><td style='width:25%;text-align:center;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:50px'>" + Time + "</td><td style='width:5%;text-align:center;border: 1px solid black; border-collapse: collapse;height:60px'></td></tr>";
                }
                else if (SigNature == "")
                {
                  outXml += @"<tr><td style='width:5%;text-align:center;text-align:center;border: 1px solid black; border-collapse: collapse;height:60px'>" + j + "</td><td style='width:25%;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:60px'>" + Name + "</td><td style='width:25%;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:50px'>" + ConstituencyName + "</td><td style='width:25%;text-align:center;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:50px'>" + Time + "</td><td style='width:5%;text-align:center;border: 1px solid black; border-collapse: collapse;height:60px'></td></tr>";
                }
                else
                {
                    outXml += @"<tr><td style='width:5%;text-align:center;text-align:center;border: 1px solid black; border-collapse: collapse;height:60px'>" + j + "</td><td style='width:25%;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:60px'>" + Name + "</td><td style='width:25%;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:50px'>" + ConstituencyName + "</td><td style='width:25%;text-align:center;height:50px;text-align:center;border: 1px solid black; border-collapse: collapse;height:50px'>" + Time + "</td><td style='width:5%;text-align:center;border: 1px solid black; border-collapse: collapse;height:60px'><img style='height:50px; width:100px;' src='" + FinalSig + "' /></td></tr>";
                }

               
                // i++;
                j++;
            }
            outXml += "</table>";

            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
            byte[] outPdfBuffer = null;
            string htmlString = outXml;
            string baseUrl = "";
            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;

            // set the header height in points
            pdfConverter.PdfHeaderOptions.HeaderHeight = 40;
            pdfConverter.PdfFooterOptions.FooterHeight = 40;

            outPdfBuffer = pdfConverter.ConvertHtml(htmlString, baseUrl);
            Response.AddHeader("Content-Type", "application/pdf");

            // Instruct the browser to open the PDF file as an attachment or inline
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename=Getting_Started.pdf; size={1}", "inline", outPdfBuffer.Length.ToString()));

            // Write the PDF document buffer to HTTP response
            Response.BinaryWrite(outPdfBuffer);

            // End the HTTP response and stop the current page processing
            Response.End();
        }


        public ActionResult MemberDiscusion()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                AssemblyFileViewModel model = new AssemblyFileViewModel();

                //Code for AssemblyList 
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                model.VAssemblyId = data.AssemblyID;
                model.VSessionId = sesdate.SessionId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                // Code for DoumentTypeList
                var doctypeList = (List<mAssemblyTypeofDocuments>)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyTypeofDocumentLst", null);
             
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetEvent = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEventsLOB", methodParameter);
                List<mEvent> EventList = new List<mEvent>();
                //mEvent Event1 = new mEvent();
                //Event1.EventId = 0;
                //Event1.EventName = "Select";
                //EventList.Add(Event1);

                for (int i = 0; i < dataSetEvent.Tables[0].Rows.Count; i++)
                {
                    mEvent Event2 = new mEvent();
                    Event2.EventId =Convert.ToInt16(dataSetEvent.Tables[0].Rows[i]["EventID"]);
                    Event2.EventName = Convert.ToString(dataSetEvent.Tables[0].Rows[i]["EventName"]);
                    EventList.Add(Event2);
                }

                model.EventList = EventList;




                model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;
                model.AssemblyDocumentTypeList = doctypeList;
                model.StatusTypeList = ModelMapping.GetStatusTypes();
                model.Mode = "Add";
                return View("MemberDiscusion", model);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Creat Assembly File");
                ViewBag.ErrorMessage = "Their is a Error While Create the AssemblyFile Details";
                return View("AdminErrorPage");
            }



        }

        public void PDFforHonbleMemberDiscussion(int date, int session, int Assem, int Event)
        {
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@AseemblyId", Convert.ToString(Assem)));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", Convert.ToString(session)));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDateId", Convert.ToString(date)));
            string outXml = "";
            int j = 1;
            DataSet MembersData = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Members_Discussion", methodParameter);
            for (int i = 0; i < MembersData.Tables[0].Rows.Count; i++)
            {
                string Name = Convert.ToString(MembersData.Tables[0].Rows[i]["Name"]);
                DateTime SessionDate = Convert.ToDateTime(MembersData.Tables[0].Rows[i]["SessionDate"]);
                string Date = SessionDate.ToString("dd/MM/yyyy");
                string EventName = Convert.ToString(MembersData.Tables[0].Rows[i]["EditedDescription"]);
                DateTime startTime = Convert.ToDateTime(MembersData.Tables[0].Rows[i]["startTime"]);
                DateTime endtime = Convert.ToDateTime(MembersData.Tables[0].Rows[i]["endtime"]);
                string ST = startTime.ToString("hh:mm tt");
                string ET = endtime.ToString("hh:mm tt");

                if (i == 0)
                {
                    //var actinlink = Url.Action("PDFforHonbleMemberDiscussion", "Attendence", new { date = sessionDateId, session = sessionId, Assem = assemblyId });
                    outXml += "<div><span class='menu-text'><h1><b>Discussion Time by Hon'ble Member (" + Date + ")</b></span></div>";
                    //outXml += "<div style='height: 27px; position: relative; top: -10px;'><span><a href='" + actinlink + "', target = '_blank', class='PdfBackGround pdficon'></a></span></div>";
                    outXml += "<table style='width:100%;border: 1px solid black'>";
                    outXml += @"<tr><td style='width:5%;text-align:center;border: 1px solid black'>Sr.No.</td><td style='width:25%;text-align:center;border: 1px solid black'>Name</td><td style='width:25%;text-align:center;border: 1px solid black'>Event Name</td><td style='width:25%;text-align:center;border: 1px solid black'>Start Time</td><td style='width:25%;text-align:center;border: 1px solid black'>End Time</td></tr>";
                }



                outXml += @"<tr><td style='width:5%;text-align:center;border: 1px solid black'>" + j + "</td><td style='width:25%;border: 1px solid black'>" + Name + "</td><td style='width:25%;border: 1px solid black'>" + EventName + "</td><td style='width:25%;text-align:center;border: 1px solid black'>" + ST + "</td><td style='width:25%;border: 1px solid black'>" + ET + "</td></tr>";
                // i++;
                j++;
            }
            outXml += "</table>";

            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Landscape;
            byte[] outPdfBuffer = null;
            string htmlString = outXml;
            string baseUrl = "";
            outPdfBuffer = pdfConverter.ConvertHtml(htmlString, baseUrl);
            Response.AddHeader("Content-Type", "application/pdf");

            // Instruct the browser to open the PDF file as an attachment or inline
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename=Getting_Started.pdf; size={1}", "inline", outPdfBuffer.Length.ToString()));

            // Write the PDF document buffer to HTTP response
            Response.BinaryWrite(outPdfBuffer);

            // End the HTTP response and stop the current page processing
            Response.End();
        }



    }
}

