﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using SBL.DomainModel.Models.ContactUs;
using SBL.eLegistrator.HouseController.Web.Helpers;
//using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Models;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Diaries;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.DomainModel.Models.References;
using SBL.DomainModel.Models.Member;




namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class FeedbackController : Controller
    {
        //
        // GET: /SuperAdmin/Feedback/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Feedback = (List<ContactUs>)Helper.ExecuteService("Feedback", "GetAllFeedback", null);
                var model = Feedback.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Feedback Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult DeleteFeedback(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                ContactUs sessionToDelete = (ContactUs)Helper.ExecuteService("Feedback", "GetFeedbackById", new ContactUs { Id = Id });
                Helper.ExecuteService("Feedback", "DeleteFeedback", sessionToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Feedback Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult MeetingView()
        {
            return View();

        }




        //Code for Benificiary Details--Constituency




        public ActionResult NewMethodDraft()
        {
            return PartialView("NewMethodDraft");
        }

        public ActionResult HelpLineIndex()
        {
            return PartialView("HelpLineIndex");
        }

        //public ActionResult BenificiaryDetailsList(int pageId, Int32 pageSize)
        //{
        //    ////////////////////////////Server Side Paging/////////////////////////////////// int pageId = 1, int pageSize = 25
        //    ViewBag.PageSize = pageSize;

        //    string Constituency_Id = CurrentSession.ConstituencyID;
        //    int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "Get_ConstituencyCode_ById", CurrentSession.ConstituencyID);

        //   // string ConstituencyId = CurrentSession.ConstituencyID;
        //    //string ConstituencyId = "14";
        //    List<tBeneficearyFeedback> pagedRecords = new List<tBeneficearyFeedback>();
        //    var BenificiaryDetailsList = (List<tBeneficearyFeedback>)Helper.ExecuteService("Constituency", "GetBenificiaryDetailsList", Constituency_Code);

        //    if (BenificiaryDetailsList != null)
        //    {
        //        pagedRecords = BenificiaryDetailsList.ToList();
        //    }

        //    ViewBag.CurrentPage = pageId;
        //    //ViewData["PagedList"] = Helper.BindPager(BenificiaryDetailsList.Count, pageId, pageSize);
        //    ////////////////////////////Server Side Paging///////////////////////////////////
        //    return PartialView("_BenificiaryDetailsList", pagedRecords);
        //}
        [HttpGet]
        public ActionResult BenificiaryDetailsList(string page, string RowsPerPage, string loopStart, string loopEnd, string SearchText,
                                                   string SchemeId, string BlockCode, string PanchayatCode, string ActionTypeCode)
        {

            ////////////////////////////Server Side Paging/////////////////////////////////// int pageId = 1, int pageSize = 25 
            tBeneficearyFeedback tbf = new tBeneficearyFeedback();
            List<tBeneficearyFeedback> pagedRecords = new List<tBeneficearyFeedback>();

            mMemberAssembly mAssembly = new mMemberAssembly();
            mAssembly.MemberID = Convert.ToInt32(CurrentSession.MemberCode);
            mAssembly.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "_Get_ConstituencyCode", mAssembly);

            //string ConstituencyId = CurrentSession.ConstituencyID;
            //int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "Get_ConstituencyCode_ById", ConstituencyId);

            string pConstituencyCode = Convert.ToString(Constituency_Code);

            // string ConstituencyId = "14";
            ViewBag.CurrentPage = page;
            int pageIndex = 1;
            int pageSize = 10;
            pageIndex = Convert.ToInt32(page);
            string pageNumberIs = Convert.ToString(pageIndex);


            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@ConstituencyID", pConstituencyCode));
            methodParameter.Add(new KeyValuePair<string, string>("@BeneficearySchemeId", SchemeId));
            methodParameter.Add(new KeyValuePair<string, string>("@BlockCode", BlockCode));
            methodParameter.Add(new KeyValuePair<string, string>("@PanchayatCode", PanchayatCode));
            methodParameter.Add(new KeyValuePair<string, string>("@ActionType", ActionTypeCode));

            methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", pageNumberIs));
            methodParameter.Add(new KeyValuePair<string, string>("@Text", SearchText));
            System.Data.DataSet ds = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "pBeneficearyFeedbackCountList", methodParameter);


            //Creating the ViewModel's Object
            tempViewModel obj = new tempViewModel();
            List<tBeneficearyFeedback> lstPerson = new List<tBeneficearyFeedback>();
            if (ds != null && ds.Tables.Count == 3)
            {
                int pagenois = Convert.ToInt32(page);
                int countstartno = pagenois - 1;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    tBeneficearyFeedback objPerson = new tBeneficearyFeedback();

                    if (countstartno != 0)
                    {
                        int b = i;
                        b++;
                        if (b != 10)
                        {
                            objPerson.SrNo = countstartno + "" + Convert.ToString(b);
                        }
                        else
                        {
                            int a = countstartno;
                            a++;
                            objPerson.SrNo = a + "0";
                        }

                    }
                    else
                    {
                        int b = i;
                        b++;
                        objPerson.SrNo = Convert.ToString(b);
                    }

                    objPerson.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["Id"]);
                    objPerson.BeneficearySchemeTypeName = Convert.IsDBNull(ds.Tables[0].Rows[i]["SchemeName"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["SchemeName"]);
                    objPerson.BlockName = Convert.IsDBNull(ds.Tables[0].Rows[i]["BlockName"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["BlockName"]);
                    objPerson.PanchayatName = Convert.IsDBNull(ds.Tables[0].Rows[i]["PanchayatName"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["PanchayatName"]);
                    objPerson.BeneficearyName = Convert.IsDBNull(ds.Tables[0].Rows[i]["BeneficearyName"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["BeneficearyName"]);
                    objPerson.BeneficearyAdress = Convert.IsDBNull(ds.Tables[0].Rows[i]["BeneficearyAdress"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["BeneficearyAdress"]);
                    objPerson.BeneficearyMobile = Convert.IsDBNull(ds.Tables[0].Rows[i]["beneficearyMobile"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["beneficearyMobile"]);

                    objPerson.BenefitDateString = Convert.IsDBNull(ds.Tables[0].Rows[i]["BenefitDate"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["BenefitDate"]);
                    objPerson.BeneficearyComponent = Convert.IsDBNull(ds.Tables[0].Rows[i]["BeneficearyComponent"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["BeneficearyComponent"]);
                    objPerson.BeneficearyAmount = Convert.IsDBNull(ds.Tables[0].Rows[i]["BeneficearyAmount"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["BeneficearyAmount"]);
                    objPerson.FeedbackDateString = Convert.IsDBNull(ds.Tables[0].Rows[i]["FeedbackDate"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["FeedbackDate"]);
                    objPerson.FeedBackDetails = Convert.IsDBNull(ds.Tables[0].Rows[i]["FeedBackDetails"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["FeedBackDetails"]);
                    objPerson.ActionTypeName = Convert.IsDBNull(ds.Tables[0].Rows[i]["Action Type"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Action Type"]);


                    objPerson.ActionDateString = Convert.IsDBNull(ds.Tables[0].Rows[i]["ActionDate"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["ActionDate"]);
                    objPerson.ActionDetails = Convert.IsDBNull(ds.Tables[0].Rows[i]["actiondetails"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["actiondetails"]);


                    lstPerson.Add(objPerson);
                }

                string ita = Convert.IsDBNull(ds.Tables[2].Rows[0]["TotalRecordCount"]) ? "" : Convert.ToString(ds.Tables[2].Rows[0]["TotalRecordCount"]);
                ViewBag.TotalRecordCount = ita;

                ViewBag.TotalPagesCount = Convert.IsDBNull(ds.Tables[1].Rows[0]["TotalPages"]) ? "" : Convert.ToString(ds.Tables[1].Rows[0]["TotalPages"]);
                pagedRecords = lstPerson.ToList();

                tbf.tBeneficearyModalList = pagedRecords;
                tbf.ResultCount = Convert.ToInt32(ita);
                tbf.RowsPerPage = pageSize;
                tbf.selectedPage = pageIndex;

                if (page != null && page != "")
                {
                    tbf.PageNumber = Convert.ToInt32(page);
                }
                else
                {
                    tbf.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tbf.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tbf.RowsPerPage = Convert.ToInt32("10");
                }
                if (page != null && page != "")
                {
                    tbf.selectedPage = Convert.ToInt32(page);
                }
                else
                {
                    tbf.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tbf.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tbf.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tbf.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tbf.loopEnd = Convert.ToInt32("5");
                }
            }

            //////////////////////////////////////
            return PartialView("_BenificiaryDetailsList", tbf);
        }

        [HttpGet]
        public ActionResult SearchBeneficiaryDetailsList(string page, string RowsPerPage, string loopStart, string loopEnd, string SearchText,
          string SchemeId, string BlockCode, string PanchayatCode, string ActionTypeCode)
        {

            ////////////////////////////Server Side Paging/////////////////////////////////// int pageId = 1, int pageSize = 25 
            tBeneficearyFeedback tbf = new tBeneficearyFeedback();
            List<tBeneficearyFeedback> pagedRecords = new List<tBeneficearyFeedback>();

            string ConstituencyId = CurrentSession.ConstituencyID;
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "Get_ConstituencyCode_ById", ConstituencyId);
            string pConstituencyCode = Convert.ToString(Constituency_Code);

            // string ConstituencyId = "14";
            ViewBag.CurrentPage = page;
            int pageIndex = 1;
            int pageSize = 10;
            pageIndex = Convert.ToInt32(page);
            string pageNumberIs = Convert.ToString(pageIndex);


            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@ConstituencyID", pConstituencyCode));
            methodParameter.Add(new KeyValuePair<string, string>("@BeneficearySchemeId", SchemeId));
            methodParameter.Add(new KeyValuePair<string, string>("@BlockCode", BlockCode));
            methodParameter.Add(new KeyValuePair<string, string>("@PanchayatCode", PanchayatCode));
            methodParameter.Add(new KeyValuePair<string, string>("@ActionType", ActionTypeCode));

            methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", pageNumberIs));
            methodParameter.Add(new KeyValuePair<string, string>("@Text", SearchText));
            System.Data.DataSet ds = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "pBeneficearyFeedbackCountList", methodParameter);


            //Creating the ViewModel's Object
            tempViewModel obj = new tempViewModel();
            List<tBeneficearyFeedback> lstPerson = new List<tBeneficearyFeedback>();
            if (ds != null && ds.Tables.Count == 3)
            {
                int pagenois = Convert.ToInt32(page);
                int countstartno = pagenois - 1;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    tBeneficearyFeedback objPerson = new tBeneficearyFeedback();

                    if (countstartno != 0)
                    {
                        int b = i;
                        b++;
                        if (b != 10)
                        {
                            objPerson.SrNo = countstartno + "" + Convert.ToString(b);
                        }
                        else
                        {
                            int a = countstartno;
                            a++;
                            objPerson.SrNo = a + "0";
                        }

                    }
                    else
                    {
                        int b = i;
                        b++;
                        objPerson.SrNo = Convert.ToString(b);
                    }

                    objPerson.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["Id"]);
                    objPerson.BeneficearySchemeTypeName = Convert.IsDBNull(ds.Tables[0].Rows[i]["SchemeName"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["SchemeName"]);
                    objPerson.BlockName = Convert.IsDBNull(ds.Tables[0].Rows[i]["BlockName"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["BlockName"]);
                    objPerson.PanchayatName = Convert.IsDBNull(ds.Tables[0].Rows[i]["PanchayatName"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["PanchayatName"]);
                    objPerson.BeneficearyName = Convert.IsDBNull(ds.Tables[0].Rows[i]["Name and Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Name and Address"]);
                    objPerson.BeneficearyMobile = Convert.IsDBNull(ds.Tables[0].Rows[i]["beneficearyMobile"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["beneficearyMobile"]);

                    objPerson.BenefitDateString = Convert.IsDBNull(ds.Tables[0].Rows[i]["BenefitDate"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["BenefitDate"]);
                    objPerson.BeneficearyComponent = Convert.IsDBNull(ds.Tables[0].Rows[i]["BeneficearyComponent"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["BeneficearyComponent"]);
                    objPerson.BeneficearyAmount = Convert.IsDBNull(ds.Tables[0].Rows[i]["BeneficearyAmount"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["BeneficearyAmount"]);
                    objPerson.FeedbackDateString = Convert.IsDBNull(ds.Tables[0].Rows[i]["FeedbackDate"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["FeedbackDate"]);
                    objPerson.FeedBackDetails = Convert.IsDBNull(ds.Tables[0].Rows[i]["FeedBackDetails"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["FeedBackDetails"]);
                    objPerson.ActionTypeName = Convert.IsDBNull(ds.Tables[0].Rows[i]["Action Type"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Action Type"]);


                    objPerson.ActionDateString = Convert.IsDBNull(ds.Tables[0].Rows[i]["ActionDate"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["ActionDate"]);
                    objPerson.ActionDetails = Convert.IsDBNull(ds.Tables[0].Rows[i]["actiondetails"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["actiondetails"]);


                    lstPerson.Add(objPerson);
                }

                string ita = Convert.IsDBNull(ds.Tables[2].Rows[0]["TotalRecordCount"]) ? "" : Convert.ToString(ds.Tables[2].Rows[0]["TotalRecordCount"]);
                ViewBag.TotalRecordCount = ita;

                ViewBag.TotalPagesCount = Convert.IsDBNull(ds.Tables[1].Rows[0]["TotalPages"]) ? "" : Convert.ToString(ds.Tables[1].Rows[0]["TotalPages"]);
                pagedRecords = lstPerson.ToList();

                tbf.tBeneficearyModalList = pagedRecords;
                tbf.ResultCount = Convert.ToInt32(ita);
                tbf.RowsPerPage = pageSize;
                tbf.selectedPage = pageIndex;

                if (page != null && page != "")
                {
                    tbf.PageNumber = Convert.ToInt32(page);
                }
                else
                {
                    tbf.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tbf.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tbf.RowsPerPage = Convert.ToInt32("10");
                }
                if (page != null && page != "")
                {
                    tbf.selectedPage = Convert.ToInt32(page);
                }
                else
                {
                    tbf.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tbf.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tbf.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tbf.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tbf.loopEnd = Convert.ToInt32("5");
                }
            }

            //////////////////////////////////////
            return PartialView("_BenificiaryDetailsList", tbf);
        }


        public PartialViewResult ComposeNew()
        {
            mMemberAssembly mAssembly = new mMemberAssembly();
            mAssembly.MemberID = Convert.ToInt32(CurrentSession.MemberCode);
            mAssembly.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "_Get_ConstituencyCode", mAssembly);



            //string Constituency_Id = "14";
            // string Constituency_Id = CurrentSession.ConstituencyID;
            //  int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "Get_ConstituencyCode_ById", Constituency_Id);


            var Beneficiary_Schemes = (List<tBeneficiarySchemes>)Helper.ExecuteService("Constituency", "GetBeneficiarySchemesList", null);
            ViewBag.Beneficiary_Schemes = new SelectList(Beneficiary_Schemes, "Id", "SchemeName", null);

            var Constituency_Block = (List<mBlock>)Helper.ExecuteService("Constituency", "GetBlockListByConstId", Constituency_Code);
            ViewBag.Constituency_Block = new SelectList(Constituency_Block, "ID", "BlockName", null);

            var TypeOfAction = (List<mTypeOfAction>)Helper.ExecuteService("Constituency", "GetActionTypes", null);
            ViewBag.TypeOfAction = new SelectList(TypeOfAction, "ActionCode", "ActionName", null);


            return PartialView("_tComposeNewBenific");
        }
        [HttpPost]
        public JsonResult GetPanchayatByBlockId(string BlockId)
        {
            List<mPanchayat> Panchayats = new List<mPanchayat>();
            try
            {
                if (BlockId != "")
                {
                    Panchayats = (List<mPanchayat>)Helper.ExecuteService("Constituency", "GetPanchayatByBlockId", BlockId);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Json(Panchayats, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Save_Benificiary_Details(List<tBeneficearyFeedback> ListIs)
        {

            mMemberAssembly mAssembly = new mMemberAssembly();
            mAssembly.MemberID = Convert.ToInt32(CurrentSession.MemberCode);
            mAssembly.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "_Get_ConstituencyCode", mAssembly);

            //int Constituency_Code = 14;
            // int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "Get_ConstituencyCode_ById", CurrentSession.ConstituencyID);

            int count = 0;
            foreach (var item in ListIs)
            {
                tBeneficearyFeedback obj = new tBeneficearyFeedback();
                obj.ConstituencyCode = Constituency_Code;
                obj.BeneficearySchemeTypeId = item.BeneficearySchemeTypeId;
                obj.BlockCode = item.BlockCode;
                obj.PanchayatCode = item.PanchayatCode;
                obj.BeneficearyName = item.BeneficearyName;
                obj.BeneficearyAdress = item.BeneficearyAdress;
                obj.BeneficearyMobile = item.BeneficearyMobile;
                obj.Sex = item.Sex;
                obj.BenefitDate = item.BenefitDate;
                obj.BeneficearyComponent = item.BeneficearyComponent;
                obj.BeneficearyAmount = item.BeneficearyAmount;
                obj.FeedbackDate = item.FeedbackDate;
                obj.FeedBackDetails = item.FeedBackDetails;
                obj.ActionDate = item.ActionDate;
                obj.ActionTypeCode = item.ActionTypeCode;
                obj.ActionDetails = item.ActionDetails;
                obj.CreatedDate = System.DateTime.Now;
                int res = (int)Helper.ExecuteService("Constituency", "Save_Benificiary_Details", obj);
            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult EditBenificiaryDetails(string Id)
        {
            int ID = Convert.ToInt16(Id);

            mMemberAssembly mAssembly = new mMemberAssembly();
            mAssembly.MemberID = Convert.ToInt32(CurrentSession.MemberCode);
            mAssembly.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "_Get_ConstituencyCode", mAssembly);

            //int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "Get_ConstituencyCode_ById", CurrentSession.ConstituencyID);

            // int Constituency_Code = 14;

            var BenificiaryDetailsEditList = (List<tBeneficearyFeedback>)Helper.ExecuteService("Constituency", "GetBenificiaryDetailsForEdit", ID);
            List<tBeneficearyFeedback> pagedRecords = new List<tBeneficearyFeedback>();
            if (BenificiaryDetailsEditList != null)
            {
                pagedRecords = BenificiaryDetailsEditList.ToList();
            }
            var Beneficiary_Schemes = (List<tBeneficiarySchemes>)Helper.ExecuteService("Constituency", "GetBeneficiarySchemesList", null);
            var Constituency_Block = (List<mBlock>)Helper.ExecuteService("Constituency", "GetBlockListByConstId", Constituency_Code);
            var TypeOfAction = (List<mTypeOfAction>)Helper.ExecuteService("Constituency", "GetActionTypes", null);





            foreach (var item in pagedRecords)
            {
                var listItems = new List<SelectListItem>();
                if (item.Sex == "Male")
                {
                    listItems.Add(new SelectListItem { Text = "N/A", Value = "N/A" });
                    listItems.Add(new SelectListItem { Text = "Male", Value = "Male", Selected = true });
                    listItems.Add(new SelectListItem { Text = "Female", Value = "Female" });
                }
                else if (item.Sex == "Female")
                {

                    listItems.Add(new SelectListItem { Text = "N/A", Value = "N/A" });
                    listItems.Add(new SelectListItem { Text = "Male", Value = "Male" });
                    listItems.Add(new SelectListItem { Text = "Female", Value = "Female", Selected = true });
                }
                else
                {
                    listItems.Add(new SelectListItem { Text = "N/A", Value = "N/A", Selected = true });
                    listItems.Add(new SelectListItem { Text = "Male", Value = "Male" });
                    listItems.Add(new SelectListItem { Text = "Female", Value = "Female" });
                }

                var Gender_List = listItems.ToList();

                var Panchayats = (List<mPanchayat>)Helper.ExecuteService("Constituency", "GetPanchayatByBlockId", item.BlockCode);

                ViewBag.Beneficiary_Schemes = new SelectList(Beneficiary_Schemes, "Id", "SchemeName", item.BeneficearySchemeTypeId);
                ViewBag.Constituency_Block = new SelectList(Constituency_Block, "ID", "BlockName", item.BlockCode);
                ViewBag.TypeOfAction = new SelectList(TypeOfAction, "ActionCode", "ActionName", item.ActionTypeCode);
                ViewBag.PanchayatsList = new SelectList(Panchayats, "PanchayatCode", "PanchayatName", item.PanchayatCode);
                ViewBag.GenderList = new SelectList(Gender_List, "Value", "Text", item.Sex);
            }
            return PartialView("_EditBenificiaryDetails", pagedRecords);
        }

        [HttpPost]
        public JsonResult Update_Benificiary_Details(List<tBeneficearyFeedback> ListIs)
        {

            mMemberAssembly mAssembly = new mMemberAssembly();
            mAssembly.MemberID = Convert.ToInt32(CurrentSession.MemberCode);
            mAssembly.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "_Get_ConstituencyCode", mAssembly);

            //int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "Get_ConstituencyCode_ById", CurrentSession.ConstituencyID);
            //int Constituency_Code = 14;
            int count = 0;
            foreach (var item in ListIs)
            {
                tBeneficearyFeedback obj = new tBeneficearyFeedback();
                obj.Id = item.Id;
                obj.ConstituencyCode = Constituency_Code;
                obj.BeneficearySchemeTypeId = item.BeneficearySchemeTypeId;
                obj.BlockCode = item.BlockCode;
                obj.PanchayatCode = item.PanchayatCode;
                obj.BeneficearyName = item.BeneficearyName;
                obj.Sex = item.Sex;
                obj.BeneficearyAdress = item.BeneficearyAdress;
                obj.BeneficearyMobile = item.BeneficearyMobile;
                obj.BenefitDate = item.BenefitDate;
                obj.BeneficearyComponent = item.BeneficearyComponent;
                obj.BeneficearyAmount = item.BeneficearyAmount;
                obj.FeedbackDate = item.FeedbackDate;
                obj.FeedBackDetails = item.FeedBackDetails;
                obj.ActionDate = item.ActionDate;
                obj.ActionTypeCode = item.ActionTypeCode;
                obj.ActionDetails = item.ActionDetails;
                obj.ModifiedDate = System.DateTime.Now;
                int res = (int)Helper.ExecuteService("Constituency", "Update_Benificiary_Details", obj);
            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        //Search Drop Down
        public JsonResult GetBeneficiarySchemeList()
        {
            var Beneficiary_Schemes = (List<tBeneficiarySchemes>)Helper.ExecuteService("Constituency", "GetBeneficiarySchemesList", null);
            return Json(Beneficiary_Schemes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBlockList()
        {
            mMemberAssembly mAssembly = new mMemberAssembly();
            mAssembly.MemberID = Convert.ToInt32(CurrentSession.MemberCode);
            mAssembly.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "_Get_ConstituencyCode", mAssembly);


            //string Constituency_Id = CurrentSession.ConstituencyID;
            // int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "Get_ConstituencyCode_ById", Constituency_Id);
            var Constituency_Block = (List<mBlock>)Helper.ExecuteService("Constituency", "GetBlockListByConstId", Constituency_Code);

            return Json(Constituency_Block, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetActionTypeList()
        {
            var TypeOfAction = (List<mTypeOfAction>)Helper.ExecuteService("Constituency", "GetActionTypes", null);
            return Json(TypeOfAction, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult ComposeNewHelplineDetail()
        {

            mMemberAssembly mAssembly = new mMemberAssembly();
            mAssembly.MemberID = Convert.ToInt32(CurrentSession.MemberCode);
            mAssembly.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "_Get_ConstituencyCode", mAssembly);

            //string Constituency_Id = "14";
            // string Constituency_Id = CurrentSession.ConstituencyID;
            //int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "Get_ConstituencyCode_ById", Constituency_Id);


            var Helpline_SectorList = (List<tSectors>)Helper.ExecuteService("Constituency", "GetHelplineSectorList", null);
            ViewBag.Helpline_SectorList = new SelectList(Helpline_SectorList, "Id", "Title", null);

            var Constituency_Block = (List<mBlock>)Helper.ExecuteService("Constituency", "GetBlockListByConstId", Constituency_Code);
            ViewBag.Constituency_Block = new SelectList(Constituency_Block, "ID", "BlockName", null);

            var TypeOfAction = (List<mTypeOfAction>)Helper.ExecuteService("Constituency", "GetActionTypes", null);
            ViewBag.TypeOfAction = new SelectList(TypeOfAction, "ActionCode", "ActionName", null);


            return PartialView("_ComposeNewHelplineDetail");
        }

        [HttpPost]
        public JsonResult Save_Helpline_Details(List<tMemberHelpline> ListIs)
        {
            mMemberAssembly mAssembly = new mMemberAssembly();
            mAssembly.MemberID = Convert.ToInt32(CurrentSession.MemberCode);
            mAssembly.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "_Get_ConstituencyCode", mAssembly);


            //int Constituency_Code = 14;
            //int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "Get_ConstituencyCode_ById", CurrentSession.ConstituencyID);

            int count = 0;
            foreach (var item in ListIs)
            {
                tMemberHelpline obj = new tMemberHelpline();
                obj.ConstituencyCode = Constituency_Code;
                obj.SectorId = item.SectorId;
                obj.BlockCode = item.BlockCode;
                obj.PanchayatCode = item.PanchayatCode;
                obj.ApplicantName = item.ApplicantName;
                obj.ApplicantAddress = item.ApplicantAddress;
                obj.ApplicantMobile = item.ApplicantMobile;
                obj.Sex = item.Sex;

                obj.CallingDate = item.CallingDate;
                obj.CallDetails = item.CallDetails;
                obj.ActionDate = item.ActionDate;
                obj.ActionTypeCode = item.ActionTypeCode;
                obj.ActionDetails = item.ActionDetails;
                obj.CreatedDate = System.DateTime.Now;
                int res = (int)Helper.ExecuteService("Constituency", "Save_Helpline_Details", obj);
            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult HelpLineDetailList(string page, string RowsPerPage, string loopStart, string loopEnd, string SearchText,
                                                   string SectorId, string BlockCode, string PanchayatCode, string ActionTypeCode)
        {

            ////////////////////////////Server Side Paging/////////////////////////////////// int pageId = 1, int pageSize = 25 
            tMemberHelpline tbf = new tMemberHelpline();
            List<tMemberHelpline> pagedRecords = new List<tMemberHelpline>();


            mMemberAssembly mAssembly = new mMemberAssembly();
            mAssembly.MemberID = Convert.ToInt32(CurrentSession.MemberCode);
            mAssembly.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "_Get_ConstituencyCode", mAssembly);

            // string ConstituencyId = CurrentSession.ConstituencyID;
            //int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "Get_ConstituencyCode_ById", ConstituencyId);
            string pConstituencyCode = Convert.ToString(Constituency_Code);

            // string ConstituencyId = "14";
            ViewBag.CurrentPage = page;
            int pageIndex = 1;
            int pageSize = 10;
            pageIndex = Convert.ToInt32(page);
            string pageNumberIs = Convert.ToString(pageIndex);


            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@ConstituencyID", pConstituencyCode));
            methodParameter.Add(new KeyValuePair<string, string>("@SectorId", SectorId));
            methodParameter.Add(new KeyValuePair<string, string>("@BlockCode", BlockCode));
            methodParameter.Add(new KeyValuePair<string, string>("@PanchayatCode", PanchayatCode));
            methodParameter.Add(new KeyValuePair<string, string>("@ActionType", ActionTypeCode));

            methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", pageNumberIs));
            methodParameter.Add(new KeyValuePair<string, string>("@Text", SearchText));
            System.Data.DataSet ds = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "pHelpLineCountList", methodParameter);


            //Creating the ViewModel's Object
            tempViewModel obj = new tempViewModel();
            List<tMemberHelpline> lstPerson = new List<tMemberHelpline>();
            if (ds != null && ds.Tables.Count == 3)
            {
                int pagenois = Convert.ToInt32(page);
                int countstartno = pagenois - 1;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    tMemberHelpline objPerson = new tMemberHelpline();

                    if (countstartno != 0)
                    {
                        int b = i;
                        b++;
                        if (b != 10)
                        {
                            objPerson.SrNo = countstartno + "" + Convert.ToString(b);
                        }
                        else
                        {
                            int a = countstartno;
                            a++;
                            objPerson.SrNo = a + "0";
                        }

                    }
                    else
                    {
                        int b = i;
                        b++;
                        objPerson.SrNo = Convert.ToString(b);
                    }

                    objPerson.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["Id"]);
                    objPerson.SectorName = Convert.IsDBNull(ds.Tables[0].Rows[i]["Title"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Title"]);
                    objPerson.BlockName = Convert.IsDBNull(ds.Tables[0].Rows[i]["BlockName"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["BlockName"]);
                    objPerson.PanchayatName = Convert.IsDBNull(ds.Tables[0].Rows[i]["PanchayatName"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["PanchayatName"]);
                    objPerson.ApplicantName = Convert.IsDBNull(ds.Tables[0].Rows[i]["ApplicantName"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["ApplicantName"]);
                    objPerson.ApplicantAddress = Convert.IsDBNull(ds.Tables[0].Rows[i]["ApplicantAddress"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["ApplicantAddress"]);
                    objPerson.ApplicantMobile = Convert.IsDBNull(ds.Tables[0].Rows[i]["ApplicantMobile"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["ApplicantMobile"]);


                    objPerson.CallingDateString = Convert.IsDBNull(ds.Tables[0].Rows[i]["CallingDate"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["CallingDate"]);
                    objPerson.CallDetails = Convert.IsDBNull(ds.Tables[0].Rows[i]["CallDetails"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["CallDetails"]);
                    objPerson.ActionTypeName = Convert.IsDBNull(ds.Tables[0].Rows[i]["Action Type"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Action Type"]);


                    objPerson.ActionDateString = Convert.IsDBNull(ds.Tables[0].Rows[i]["ActionDate"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["ActionDate"]);
                    objPerson.ActionDetails = Convert.IsDBNull(ds.Tables[0].Rows[i]["actiondetails"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["actiondetails"]);
                    objPerson.Sex = Convert.IsDBNull(ds.Tables[0].Rows[i]["Sex"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Sex"]);

                    lstPerson.Add(objPerson);
                }

                string ita = Convert.IsDBNull(ds.Tables[2].Rows[0]["TotalRecordCount"]) ? "" : Convert.ToString(ds.Tables[2].Rows[0]["TotalRecordCount"]);
                ViewBag.TotalRecordCount = ita;

                ViewBag.TotalPagesCount = Convert.IsDBNull(ds.Tables[1].Rows[0]["TotalPages"]) ? "" : Convert.ToString(ds.Tables[1].Rows[0]["TotalPages"]);
                pagedRecords = lstPerson.ToList();

                tbf.tMemberHelplineModalList = pagedRecords;
                tbf.ResultCount = Convert.ToInt32(ita);
                tbf.RowsPerPage = pageSize;
                tbf.selectedPage = pageIndex;

                if (page != null && page != "")
                {
                    tbf.PageNumber = Convert.ToInt32(page);
                }
                else
                {
                    tbf.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tbf.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tbf.RowsPerPage = Convert.ToInt32("10");
                }
                if (page != null && page != "")
                {
                    tbf.selectedPage = Convert.ToInt32(page);
                }
                else
                {
                    tbf.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tbf.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tbf.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tbf.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tbf.loopEnd = Convert.ToInt32("5");
                }
            }

            //////////////////////////////////////
            return PartialView("_HelpLineDetailList", tbf);
        }


        public JsonResult GetHelpLineSectorList()
        {
            List<tSectors> lstSector = new List<tSectors>();
            var Helpline_SectorList = (List<tSectors>)Helper.ExecuteService("Constituency", "GetHelplineSectorList", null);
            lstSector = Helpline_SectorList.ToList();
            return Json(lstSector, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult EditHelpLineDetails(string Id)
        {
            int ID = Convert.ToInt16(Id);

            mMemberAssembly mAssembly = new mMemberAssembly();
            mAssembly.MemberID = Convert.ToInt32(CurrentSession.MemberCode);
            mAssembly.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "_Get_ConstituencyCode", mAssembly);

            var HelpLineDetailsEditList = (List<tMemberHelpline>)Helper.ExecuteService("Constituency", "GetHelpLineDetailsForEdit", ID);
            List<tMemberHelpline> pagedRecords = new List<tMemberHelpline>();
            if (HelpLineDetailsEditList != null)
            {
                pagedRecords = HelpLineDetailsEditList.ToList();
            }

            var Sector_List = (List<tSectors>)Helper.ExecuteService("Constituency", "GetHelplineSectorList", null);
            //var Beneficiary_Schemes = (List<tBeneficiarySchemes>)Helper.ExecuteService("Constituency", "GetBeneficiarySchemesList", null);
            var Constituency_Block = (List<mBlock>)Helper.ExecuteService("Constituency", "GetBlockListByConstId", Constituency_Code);
            var TypeOfAction = (List<mTypeOfAction>)Helper.ExecuteService("Constituency", "GetActionTypes", null);




            foreach (var item in pagedRecords)
            {
                var listItems = new List<SelectListItem>();
                if (item.Sex == "Male")
                {
                    listItems.Add(new SelectListItem { Text = "N/A", Value = "N/A" });
                    listItems.Add(new SelectListItem { Text = "Male", Value = "Male", Selected = true });
                    listItems.Add(new SelectListItem { Text = "Female", Value = "Female" });
                }
                else if (item.Sex == "Female")
                {

                    listItems.Add(new SelectListItem { Text = "N/A", Value = "N/A" });
                    listItems.Add(new SelectListItem { Text = "Male", Value = "Male" });
                    listItems.Add(new SelectListItem { Text = "Female", Value = "Female", Selected = true });
                }
                else
                {
                    listItems.Add(new SelectListItem { Text = "N/A", Value = "N/A", Selected = true });
                    listItems.Add(new SelectListItem { Text = "Male", Value = "Male" });
                    listItems.Add(new SelectListItem { Text = "Female", Value = "Female" });
                }

                var Gender_List = listItems.ToList();
                var Panchayats = (List<mPanchayat>)Helper.ExecuteService("Constituency", "GetPanchayatByBlockId", item.BlockCode);
                ViewBag.HelpLine_Sector = new SelectList(Sector_List, "Id", "Title", item.SectorId);
                ViewBag.Constituency_Block = new SelectList(Constituency_Block, "ID", "BlockName", item.BlockCode);
                ViewBag.TypeOfAction = new SelectList(TypeOfAction, "ActionCode", "ActionName", item.ActionTypeCode);
                ViewBag.PanchayatsList = new SelectList(Panchayats, "PanchayatCode", "PanchayatName", item.PanchayatCode);
                ViewBag.GenderList = new SelectList(Gender_List, "Value", "Text", item.Sex);
            }




            return PartialView("EditHelpLineDetails", pagedRecords);
        }


        [HttpPost]
        public JsonResult Update_Helpline_Details(List<tMemberHelpline> ListIs)
        {

            mMemberAssembly mAssembly = new mMemberAssembly();
            mAssembly.MemberID = Convert.ToInt32(CurrentSession.MemberCode);
            mAssembly.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            int Constituency_Code = (Int32)Helper.ExecuteService("Constituency", "_Get_ConstituencyCode", mAssembly);

            int count = 0;
            foreach (var item in ListIs)
            {
                tMemberHelpline obj = new tMemberHelpline();
                obj.Id = item.Id;
                obj.ConstituencyCode = Constituency_Code;
                obj.SectorId = item.SectorId;
                obj.BlockCode = item.BlockCode;
                obj.PanchayatCode = item.PanchayatCode;
                obj.ApplicantName = item.ApplicantName;
                obj.ApplicantAddress = item.ApplicantAddress;
                obj.ApplicantMobile = item.ApplicantMobile;
                obj.Sex = item.Sex;

                obj.CallingDate = item.CallingDate;
                obj.CallDetails = item.CallDetails;
                obj.ActionDate = item.ActionDate;
                obj.ActionTypeCode = item.ActionTypeCode;
                obj.ActionDetails = item.ActionDetails;
                //obj.ModifiedDate = System.DateTime.Now;
                int res = (int)Helper.ExecuteService("Constituency", "Update_Helpline_Details", obj);
            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }

    }
}
