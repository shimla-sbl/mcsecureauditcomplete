﻿using System;
using SBL.DomainModel.Models.Committee;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;


namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class HouseEmpCommitteeController : Controller
    {
        //
        // GET: /Admin/HouseEmpCommittee/

        public ActionResult Index()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var HouseEmpCommittee = (List<AuthEmployee>)Helper.ExecuteService("HouseEmpCommittee", "GetAllHouseEmpCommittee", null);
                var model = HouseEmpCommittee.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Highest Qualification Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }
        public ActionResult CreateHouseEmpCommittee()
        {
            var model = new HouseEmpCommitteeViewModel()
            {
                Mode = "Add",
                IsActive = true

            };

            return View("CreateHouseEmpCommittee", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveProgramme(HouseEmpCommitteeViewModel model)
        {
            AuthEmployee HouseEmpCommittee = new AuthEmployee();
            HouseEmpCommittee.Name = model.Name;
            HouseEmpCommittee.AadharId = model.AadharId;
            HouseEmpCommittee.ID = Convert.ToInt32(model.strId);


            if (ModelState.IsValid)
            {
                //string strID = model.strId;

                //model.ID = Convert.ToInt32(EncryptionUtility.Decrypt(strID));
                //var HouseEmpCommittee = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("HouseEmpCommittee", "CreateHouseEmpCommittee", HouseEmpCommittee);
                }
                else
                {
                    Helper.ExecuteService("HouseEmpCommittee", "UpdateHouseEmpCommittee", HouseEmpCommittee);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditProgramme(string Id)
        {

            //mMemberHighestQualification stateToEdit = (mMemberHighestQualification)Helper.ExecuteService("HighestQualification", "GetHighestQualificationById", new mMemberHighestQualification { MemberHighestQualificationId = Id });
            //AuthEmployee stateToEdit = (AuthEmployee)Helper.ExecuteService("HouseEmpCommittee", "GetHouseEmpCommitteeById", new AuthEmployee { ID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            AuthEmployee stateToEdit = (AuthEmployee)Helper.ExecuteService("HouseEmpCommittee", "GetHouseEmpCommitteeById", new AuthEmployee { ID = Convert.ToInt32(Id) });
            var model = stateToEdit.ToViewModel("Edit", Id);
            return View("CreateHouseEmpCommittee", model);
        }

        public ActionResult DeleteHouseEmpCommittee(int Id)
        {
            AuthEmployee stateToDelete = (AuthEmployee)Helper.ExecuteService("HouseEmpCommittee", "GetHouseEmpCommitteeById", new AuthEmployee { ID = Id });
            Helper.ExecuteService("HouseEmpCommittee", "DeleteHouseEmpCommittee", stateToDelete);
            return RedirectToAction("Index");

        }

    }
}