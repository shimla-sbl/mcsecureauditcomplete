﻿using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class UpdateDepartmentsController : Controller
    {
        //
        // GET: /Admin/UpdateDepartments/

        public ActionResult Index()
        {
            List<mUsers> UserList = (List<mUsers>)Helper.ExecuteService("User", "GetUserDetailsByUserTypeAndOfficeLevel", new mUsers { UserType = 2, OfficeLevel = "2" });
            return View(UserList);
        }

        public PartialViewResult PopulateDepartment(Guid UserId)
        {
            try
            {
                mUsers model = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", new mUsers { UserId = UserId });
                var Departments = (List<mDepartment>)Helper.ExecuteService("Employee", "GetAllDepartment", null);
                ViewBag.Departments = new MultiSelectList(Departments, "deptId", "deptname", model.DeptId.Split(','));
                ViewBag.UserId = UserId;
                ViewBag.SelectedDept = model.DeptId.Split(',');
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                
                throw;
            }
           
            return PartialView("/Areas/Admin/Views/UpdateDepartments/_PopulateDepartment.cshtml");
        }

        public PartialViewResult UpdateDepartmenta(Guid UserId, string Departments)
        {
            try
            {
                mUsers model = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", new mUsers { UserId = UserId });
                model.DeptId = Departments;

                var IsUpdate = (bool)Helper.ExecuteService("User", "UpdateUserNodalOffice", model);
                if (IsUpdate)
                {
                    ViewBag.Msg = "Sucessfully updated ";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
                else
                {
                    ViewBag.Msg = "Unable to update department";
                    ViewBag.Class = "alert alert-danger";
                    ViewBag.Notification = "Error";
                }
            }
            catch (Exception ex)
            {

                ViewBag.Msg = ex.InnerException;
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }
           
            List<mUsers> UserList = (List<mUsers>)Helper.ExecuteService("User", "GetUserDetailsByUserTypeAndOfficeLevel", new mUsers { UserType = 2, OfficeLevel = "2" });
            return PartialView("/Areas/Admin/Views/UpdateDepartments/_UserList.cshtml", UserList);
        }
    }
}
