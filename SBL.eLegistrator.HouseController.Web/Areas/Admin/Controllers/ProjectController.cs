﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Category;
using SBL.DomainModel.Models.Content;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class ProjectController : Controller
    {
        //
        // GET: /Admin/Project/

        public ActionResult Index()
        {
            return View();
        }

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "ContentError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }
        public ActionResult CreateProject()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
             
                var model = new ProjectViewModel()
                {
                    mode = "Add",
                    StatusTypeList = ModelMapping.GetStatusTypes(),
                    IsActive =1
                    

                };
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Create  Project Details");
                ViewBag.ErrorMessage = "Their is a Error While Creating Project Details";
                return View("AdminErrorPage");
            }


        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveProject(ProjectViewModel model)
        {

            try
            {


                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
              
                    if (model.mode == "Add")
                    {
                        //string FileName = file.FileName;

                        model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                     
                        ProjectViewModel.SaveProject(model);
                    }
                    else
                    {

                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);

                    ProjectViewModel.SaveProject(model);
                }

                return RedirectToAction("ProjectList");
      
                


            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Project Details");
                ViewBag.ErrorMessage = "Their is a Error While Saving Project Details";
                return View("AdminErrorPage");
            }
        }


        public ActionResult ProjectList()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                return View(ProjectViewModel.GetProjectList()) ;
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting All Contents");
                ViewBag.ErrorMessage = "Their is a Error While Getting all Content Details";
                return View("AdminErrorPage");
            }

        }


        public ActionResult EditProject(string Id)
        {


            try
            {

            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting All Project");
                ViewBag.ErrorMessage = "Their is a Error While Getting all Project Details";
                return View("AdminErrorPage");
            }

            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            var model= ProjectViewModel.GetProjectListById(Convert.ToInt32 (EncryptionUtility.Decrypt(Id.ToString())));

            model.mode = "Edit";
            model.StatusTypeList = ModelMapping.GetStatusTypes();
                    
            return View("CreateProject", model);

        }

        public ActionResult EditDocument(string Id)
        {

            
            try
            {

            }
            catch (Exception ex)
            {

                RecordError(ex, "Edit All Document");
                ViewBag.ErrorMessage = "Their is a Error While Edit Document";
                return View("AdminErrorPage");
            }

            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            var model = DocumentViewModel.GetDocumentListById(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));

            model.Mode = "Edit";
            model.StatusTypeList = ModelMapping.GetStatusTypes();
           model.ProjectList = new SelectList(ProjectViewModel.GetProjectList(), "ProgramId", "ProgrammeTitle", 0);
            return View("CreateDocument", model);

        }

        public ActionResult EditImage(string Id)
        {

            try
            {

            }
            catch (Exception ex)
            {

                RecordError(ex, "Edit All Image");
                ViewBag.ErrorMessage = "Their is a Error While Edit Image";
                return View("AdminErrorPage");
            }

            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            var model = ImageViewModel.GetImageListById(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));

            model.Mode = "Edit";
            model.StatusTypeList = ModelMapping.GetStatusTypes();
            model.ProjectList = new SelectList(ProjectViewModel.GetProjectList(), "ProgramId", "ProgrammeTitle", 0);
            return View("CreateImage", model);

        }

        public ActionResult DeleteDocument(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }




                ProjectViewModel.DeleteDocumentById(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));
                return RedirectToAction("DocumentList");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Deleting  Document Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting  Document Details";
                return View("AdminErrorPage");
            }



        }
        public ActionResult DeleteImage(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }




                ImageViewModel.DeleteImageById(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));
                return RedirectToAction("DocumentList");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Deleting  Document Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting  Document Details";
                return View("AdminErrorPage");
            }



        }
        public ActionResult DeleteProject(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }



          
            ProjectViewModel.DeleteProjectById (Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));
            return RedirectToAction("ProjectList");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Deleting  Project Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting  Project Details";
                return View("AdminErrorPage");
            }



        }
        public ActionResult CreateDocument()
        {

            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }



                
                SelectList objProject = new SelectList(ProjectViewModel.GetProjectList(), "ProgramId", "ProgrammeTitle", 0);
                var model = new DocumentViewModel()
                {
                    Mode = "Add",
                    StatusTypeList = ModelMapping.GetStatusTypes(),
                    IsActive = 1,
                    IsDeleted = false,
                    ProjectList= objProject

                };

             
                ViewBag.FileAcessingPath = "/Content/Project/;";
                return View(model);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Creating Document");
                ViewBag.ErrorMessage = "Their is a Error While Creating the Document  Details";
                return View("AdminErrorPage");
            }




        }
        public ActionResult CreateImage()
        {

            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }




                SelectList objProject = new SelectList(ProjectViewModel.GetProjectList(), "ProgramId", "ProgrammeTitle", 0);
                var model = new ImageViewModel()
                {
                    Mode = "Add",
                    StatusTypeList = ModelMapping.GetStatusTypes(),
                    IsActive = 1,
                    IsDeleted = false,
                    ProjectList = objProject

                };


            
                return View(model);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Creating Project Image");
                ViewBag.ErrorMessage = "Their is a Error While Creating the Project Image";
                return View("AdminErrorPage");
            }




        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveDocument(DocumentViewModel model)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string path = fileAcessingSettings.SettingValue + "\\Project\\Doc";


            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                if (model.Mode == "Add")
                {
                    // model.IsActive = true;
                    model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                   


                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                   

                  
                    byte[] bytes1 = null;
                    string type = "";
                
                    string[] filenamearr = model.DocumentFileName.Split('.');
                    string FileName = filenamearr[0]+ "_"+ DateTime.Now.Month + "_" + DateTime.Now.Day +"_"+ DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond) +"." + filenamearr[1];
                    ConvertBase64StringToBytes(model.DocumentLocation, ref bytes1, ref type);
                   System.IO.File.WriteAllBytes(path+ "/" + FileName, bytes1);
                    model.DocumentLocation ="Project/Doc/"  + FileName;
                    DocumentViewModel.SaveDocument(model);
                }
                else
                {
                    if (!string.IsNullOrEmpty(model.DocumentFileName))
                    {
                        // model.CreatedBy = Guid.Parse(CurrentSession.UserID);

                      

                        
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        byte[] bytes1 = null;
                        string type = "";

                        string[] filenamearr = model.DocumentFileName.Split('.');
                        string FileName = filenamearr[0] + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond) + "." + filenamearr[1];
                        ConvertBase64StringToBytes(model.DocumentLocation, ref bytes1, ref type);
                        System.IO.File.WriteAllBytes(path + "/" + FileName, bytes1);
                        model.DocumentLocation = "Project\\Doc\\"  + FileName;
                    }
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);

                    DocumentViewModel.SaveDocument(model);
                    //var result = model.ToDomainModel();
                    //result.GalleryId = model.GalleryImageId.HasValue ? model.GalleryImageId.Value : 0;
                    //Helper.ExecuteService("Gallery", "UpdateGalleryCategory", result);
                }

                //if (model.IsCreatNew)
                //{
                //    return RedirectToAction("CreateGallery");
                //}
                //else
                //{
                //    return RedirectToAction("GalleryCategoryIndex");
                //}
                return RedirectToAction("DocumentList");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Document");
                ViewBag.ErrorMessage = "Their is a Error While saving the Project Document";
                return View("AdminErrorPage");
            }

            // return RedirectToAction("GalleryCategoryIndex");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveImage(ImageViewModel model)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);



            string path = fileAcessingSettings.SettingValue + "\\Project\\Image\\";

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                if (model.Mode == "Add")
                {
                    // model.IsActive = true;
                    model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                   
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }



                    byte[] bytes1 = null;
                    string type = "";
                  
                    string[] filenamearr = model.ImageFileName.Split('.');
                    string FileName = filenamearr[0] + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond) + "." + filenamearr[1];
                    ConvertBase64StringToBytes(model.ImageLocation, ref bytes1, ref type);
                    System.IO.File.WriteAllBytes(path + "/" + FileName, bytes1);
                    model.ImageLocation = "Project/Image/" + FileName;
                    ImageViewModel.SaveImage(model);
                }
                else
                {
                    // model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);

                    if (!string.IsNullOrEmpty(model.ImageFileName))
                    {
                       
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }



                        byte[] bytes1 = null;
                        string type = "";

                        string[] filenamearr = model.ImageFileName.Split('.');
                        string FileName = filenamearr[0] + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond) + "." + filenamearr[1];
                        ConvertBase64StringToBytes(model.ImageLocation, ref bytes1, ref type);
                        System.IO.File.WriteAllBytes(path + "/" + FileName, bytes1);
                        model.ImageLocation = "Project/Image/"  + FileName;
                    }
                    ImageViewModel.SaveImage(model);
                    //var result = model.ToDomainModel();
                    //result.GalleryId = model.GalleryImageId.HasValue ? model.GalleryImageId.Value : 0;
                    //Helper.ExecuteService("Gallery", "UpdateGalleryCategory", result);
                }

               
                return RedirectToAction("ImageList");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Image");
                ViewBag.ErrorMessage = "Their is a Error While saving the Project Image";
                return View("AdminErrorPage");
            }

            // return RedirectToAction("GalleryCategoryIndex");
        }
        private static void ConvertBase64StringToBytes(string base64String, ref byte[] bytes1, ref string Type)
        {


            string[] split = base64String.Split(',');


            bytes1 = System.Convert.FromBase64String(base64String.Substring(base64String.IndexOf(',') + 1));

            if (base64String != "")
            {
                split = base64String.Split(',');
                Type = split[0];

            }


            //new SqlParameter("@filescannedBankGurnthdn",bytes1),

            //   new SqlParameter("@Type",Type),
        }


        public ActionResult DocumentList()
        {
            
            
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                return View(DocumentViewModel.GetDocumentList());
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting All Document List");
                ViewBag.ErrorMessage = "Their is a Error While Getting all Document Details";
                return View("AdminErrorPage");
            }

        }


        public ActionResult ImageList()
        {
           
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                return View(ImageViewModel.GetImageList());
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting All Document List");
                ViewBag.ErrorMessage = "Their is a Error While Getting all Document Details";
                return View("AdminErrorPage");
            }

        }

    }
}
