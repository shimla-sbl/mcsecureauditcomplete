﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class PaperVerificationReportController : Controller
    {

        //
        // GET: /Admin/PaperVerificationReport/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult fillSessionDate()
        {
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            List<fillSessionDate> lst = (List<fillSessionDate>)Helper.ExecuteService("Diary", "getSessionDateList", new string[] { siteSettingMod.AssemblyCode.ToString(), siteSettingMod.SessionCode.ToString() });
            foreach (var item in lst)
            {
                item.Text = item.value.ToString("dd/MM/yyyy");

            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getPaperLaidList(string SessionDate)
        {
            List<tAuditTrialViewModel> list = (List<tAuditTrialViewModel>)Helper.ExecuteService("Diary", "getPaperListBydate", SessionDate);
            foreach (var item in list)
            {
                if (!string.IsNullOrEmpty(item.filePath))
                {
                    string[] filepathName1 = item.filePath.Split('/');
                    item.filePathName = filepathName1[filepathName1.Length - 1];
                }

                if (!string.IsNullOrEmpty(item.filePath2))
                {
                    string[] filepathName2 = item.filePath2.Split('/');
                    item.filePath2Name = filepathName2[filepathName2.Length - 1];
                }


            }
            return PartialView("_paperLaidList", list);
        }
        public ActionResult VerifiedDocumentsReport()
        {
            return View();
        }
        public ActionResult getDocumentsList(string department, string doctype, string layingDate, string submitDate, string time, string isLatest)
        {

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            List<tAuditTrialViewModel> list = (List<tAuditTrialViewModel>)Helper.ExecuteService("Diary", "getFilterTempPaperList", new string[] { siteSettingMod.SessionCode.ToString(), siteSettingMod.AssemblyCode.ToString(), department, doctype, layingDate, submitDate, time, isLatest });
            foreach (var item in list)
            {
                string[] filepathName1 = item.filePath.Split('/');
                item.filePathName = filepathName1[filepathName1.Length - 1];
                if (!string.IsNullOrEmpty(item.filePath2))
                {
                    string[] filepathName2 = item.filePath2.Split('/');
                    item.filePath2Name = filepathName2[filepathName2.Length - 1];
                }


            }
            return PartialView("_documentsList", list);
        }
        public ActionResult fillDepartmentID()
        {
            List<fillListGenric> list = (List<fillListGenric>)Helper.ExecuteService("Diary", "fillDepartmentID", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public  string getAllFileVersion(string id)
        {
            List<tAuditTrialViewModel> list = (List<tAuditTrialViewModel>)Helper.ExecuteService("Diary", "getAllPaperByLaidId", new string[] { id });
          StringBuilder FileReport = new StringBuilder();

            if (list != null && list.Count() > 0)
            {
               
                FileReport.Append("<div class='panel panel-default' style='width:100%;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                FileReport.Append("<thead class='header' ><tr style='background-color: #6fb3e0 ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:80px;text-align:left;'>Sr. No.</th><th style='width:80px;text-align:left;'>File</th><th style='width:80px;text-align:left;'>Submitted Date</th><th style='width:80px;text-align:left;'>Verified On</th>");
              
                FileReport.Append("</tr></thead>");


                FileReport.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in list)
                {
                    count++;
                    string  path=obj.SecurePath+"/"+obj.filePath;
                    String VarifiedDate = Convert.ToString(obj.ReferencedDate);
                    if (VarifiedDate != null && VarifiedDate != "")
                    {
                        VarifiedDate = Convert.ToDateTime(VarifiedDate).ToString("dd/MM/yyyy HH:mm");
                    }
                    String SubmitDate = Convert.ToString(obj.SubmitDate);
                    if (SubmitDate != null && SubmitDate != "")
                    {
                        SubmitDate = Convert.ToDateTime(SubmitDate).ToString("dd/MM/yyyy HH:mm");
                    }

                    FileReport.Append("<tr>");
                    FileReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", count));
                    FileReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>","<i class=''><a href='"+path+"' target='_blank'>"+obj.filePathName+"</a>"));
                    FileReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", SubmitDate));
                    FileReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", VarifiedDate));
                    FileReport.Append("</tr>");
                }
                FileReport.Append("</tbody></table></div> ");
            }
            else
            {
                FileReport.Append("No records available");
            }
            return FileReport.ToString();

        }

        public ActionResult OnlineVsManualLaying()
        {
            CurrentSession.SetSessionAlive = null;
            return PartialView("_OnlineVsManualLaying");
        }
    }
}

