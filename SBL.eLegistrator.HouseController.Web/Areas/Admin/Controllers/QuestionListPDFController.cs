﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.IO;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.ComplexModel;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using EvoPdf;
using SBL.DomainModel.Models.Notice;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class QuestionListPDFController : Controller
    {
        //
        // GET: /Admin/QuestionListPDF/

        public ActionResult Index()
        {
            QuestionListPDFViewModel model = new QuestionListPDFViewModel();
            model.AssemblyCode = Convert.ToInt32(Helper.ExecuteService("QuestionListPDF", "GetCurrentAssembly", null));
            model.SessionID = Convert.ToInt32(Helper.ExecuteService("QuestionListPDF", "GetCurrentSession", null));
            model.AssemblyCol = (List<mAssembly>)Helper.ExecuteService("QuestionListPDF", "GetAllAssemblies", null);
            model.SessionCol = (List<mSession>)Helper.ExecuteService("QuestionListPDF", "GetAllSessions", null);
            model.QuestionTypeCol = (List<tQuestionTypeModel>)Helper.ExecuteService("QuestionListPDF", "GetQuestionTypes", null);
            model.MemberCol = (List<mMember>)Helper.ExecuteService("QuestionListPDF", "GetAllMembers", null);
            return View("Index", model);
        }

        [HttpGet]
        public ActionResult GetQuestionList(int? AssemblyCode, int? SessionID, int? MemberCode, string NoticeRecievedDate, int? QuestionType)
        {
            QuestionListPDFViewModel model = new QuestionListPDFViewModel();
            model.AssemblyCode = AssemblyCode ?? 0;
            model.SessionID = SessionID ?? 0;
            model.MemberCode = MemberCode ?? 0;
            model.NoticeRecievedDate = string.IsNullOrEmpty(NoticeRecievedDate) ? DateTime.Now : DateTime.ParseExact(NoticeRecievedDate, "dd/MM/yyyy", null);
            model.QuestionType = QuestionType ?? 1;
            model.QuestionListCol = (List<QuestionListPDFViewModel>)Helper.ExecuteService("QuestionListPDF", "GetAllQuestionList", model);

            string path = "";

            string fileName = "";

            string LOBName = "";

            string MemberName = "";

            string res = "";

            if (model.QuestionListCol.Count > 1)
            {
                //generate pdf

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                try
                {
                    foreach (QuestionListPDFViewModel question in model.QuestionListCol)
                    {
                        string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";

                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                        PdfPageOrientation.Landscape);

                        outXml += @"<table style='width:100%'>
                            <table style='width:100%'>
                                <tr>
                                    <td style='text-align:left;font-size:20px;' >
                                        DiaryNumber : " + question.DiaryNumber + @"
                                    </td>
                                </tr>
                            </table>
                            <table style='width:100%'>
                                <tr>
                                    <td style='text-align:left;font-size:20px;' >
                                        Notice Recieved Date:  " + question.NoticeRecievedDate.Value.ToString("dd-MM-yyyy") + @"
                                    </td>
                                </tr>
                            </table>
                            <table style='width:100%'>
                                <tr>
                                    <td style='text-align:left;font-size:20px;' >
                                        Notice Recieved Time:  " + question.NoticeRecievedTime + @"
                                    </td>
                                </tr>
                            </table>
                            <table style='width:100%'>
                                <tr>
                                    <td style='text-align:left;font-size:20px;' >
                                        QuestionNumber:  " + question.QuestionNumber + @"
                                     </td>
                                </tr>
                            </table>
                            <table style='width:100%'>
                                <tr>
                                    <td style='text-align:left;font-size:20px;' >
                                        Fixed Date:  " + question.IsFixedDate + @"
                                    </td>
                                </tr>
                            </table>
                            <table style='width:100%'>
                                <tr>
                                    <td style='text-align:left;font-size:20px;' >
                                        Subject:  " + question.Subject + @"
                                     </td>
                                </tr>
                            </table>
                            <table style='width:100%'>
                                <tr>
                                    <td style='text-align:left;font-size:20px;' >
                                        Main Question:  " + question.MainQuestion + @"
                                    </td>
                                </tr>
                            </table>
                            </table>";

                        outXml += "</body></html>";

                        string htmlStringToConvert = outXml;

                        HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                        AddElementResult addResult = page.AddElement(htmlToPdfElement);
                        
                        MemberName = question.Name;
                    }

                    byte[] pdfBytes = document1.Save();

                    output.Write(pdfBytes, 0, pdfBytes.Length);

                    output.Position = 0;

                    string url = "/QuestionList/" + "MemberPdf/";

                    string directory = Server.MapPath(url);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    LOBName = !string.IsNullOrEmpty(MemberName) ? MemberName : "";

                    if (model.QuestionType == 1)
                    {
                        fileName = LOBName.Replace(" ", "_") + "Starred" + ".pdf";
                    }
                    else if (model.QuestionType == 2)
                    {
                        fileName = LOBName.Replace(" ", "_") + "UnStarred" + ".pdf";
                    }

                    path = Path.Combine(Server.MapPath("~" + url), fileName);

                    FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                    System.IO.FileAccess.Write);

                    _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                    // close file stream
                    _FileStream.Close();

                    //HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);

                   res = "Question List PDF Generation Successfull";
                }
                catch
                {
                    res = "Question List PDF Generation Failed";
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
            }
            else
            {
                res = "No Data Available";
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Constituency()
        {
            tMemberNotice model = new tMemberNotice();
            model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            model = (tMemberNotice)Helper.ExecuteService("QuestionListPDF", "GetMembers", model);     
            return View("MemberDropdown", model);
        }

        public ActionResult ConstituencyOffice()
        {

            tMemberNotice model = new tMemberNotice();
            model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            model = (tMemberNotice)Helper.ExecuteService("QuestionListPDF", "GetMembers", model);        
            return View("MemberofficeDrop", model);
        }

    }
}

