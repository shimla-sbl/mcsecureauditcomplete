﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.ComplexModel;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SMS.API;
using SMS.WebService;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
	[Audit]
	[NoCache]
	[SBLAuthorize(Allow = "Authenticated")]
	public class SMSComposeController : Controller
	{
		//
		// GET: /Admin/SMSCompose/

		public ActionResult Index()
		{

			RecipientGroup data = new RecipientGroup();
			data.CreatedBy = CurrentSession.UserID;
			var list = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetOwnGroups", data);
			ViewBag.GroupList = new SelectList(list, "GroupID", "Name", null);

			return View();
		}

		public ActionResult GetContactList()
		{
			SMSComposeModel model = new SMSComposeModel();
			model.MemberContactCol = (List<MemberContactViewModel>)Helper.ExecuteService("Contacts", "GetAllMembersMobileNos", null);
			PartialView("_GetMembersContactList", model);
			return PartialView("_GetContactList");
		}

		public ActionResult GetMinistersList()
		{
			SMSComposeModel model = new SMSComposeModel();
			model.MinisterContactCol = (List<MinisterContactViewModel>)Helper.ExecuteService("Contacts", "GetAllMinisterMobileNos", null);
			return PartialView("_GetMinistersContactList", model);
		}

		public ActionResult GetMembersList()
		{
			SMSComposeModel model = new SMSComposeModel();
			model.MemberContactCol = (List<MemberContactViewModel>)Helper.ExecuteService("Contacts", "GetAllMembersMobileNos", null);
			return PartialView("_GetMembersContactList", model);
		}

		public ActionResult GetEmployeesList()
		{
			SMSComposeModel model = new SMSComposeModel();
			model.UserContactCol = (List<mUsers>)Helper.ExecuteService("Contacts", "GetAllEmployeePhoneNos", null);
			return PartialView("_GetEmployeesContactList", model);
		}

		[HttpPost]
		public JsonResult SendSMS(string to, string message)
		{
			string msg = "";

			if (!String.IsNullOrEmpty(to) && !(String.IsNullOrEmpty(message)))
			{
				try
				{

					string[] sMSList = to.Split(new string[] { "," },
					StringSplitOptions.RemoveEmptyEntries);

					var smsGroup = (from sms in sMSList
									select sms).Distinct();

					SMSMessageList smsMessage = new SMSMessageList();

					Service1 smsServiceClient = new Service1();

					foreach (string smsNo in smsGroup)
					{
						smsMessage.ModuleActionID = -1;
						smsMessage.UniqueIdentificationID = -1;
						smsMessage.SMSText = message;
						smsMessage.MobileNo.Add(smsNo);

					}

					SMSComposeModel model = new SMSComposeModel();

					model.SendSMS = true;

					model.SendEmail = false;

					Notification.Send(model.SendSMS, model.SendEmail, smsMessage, null);

					msg = "SMS Send Successfully";
				}
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
				catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
				{

					msg = "SMS Sending Failed";
				}
			}

			return Json(msg, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GenericGroupsSMS()
		{
			OnlineMemberQmodel partMd = new OnlineMemberQmodel();

			RecipientGroup data = new RecipientGroup();



			var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllContactGroupsByMember", data);

			partMd.RecipientGroupList = recipientGroupCol;

			return Json(partMd.RecipientGroupList, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public ActionResult GetGroupMembersPartial(int? GroupID)
		{
			SMSComposeModel mdl = new SMSComposeModel();
			RecipientGroupMember model = new RecipientGroupMember();

			List<RecipientGroupMember> modelList = new List<RecipientGroupMember>();
			model.GroupID = GroupID ?? 0;

			ViewBag.GroupMembers = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", model);

			modelList = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", model);

			return PartialView("_GetMembersContactList", mdl);
		}


		[HttpPost]
		public ActionResult SaveGroup(int? GroupId, string GroupName, string Mode)
		{
			RecipientGroupViewModel model = new RecipientGroupViewModel();

			try
			{
				RecipientGroup data = new RecipientGroup();

				data.GroupID = GroupId ?? 0;
				data.Description = GroupName;
				data.Name = GroupName;
				data.IsActive = false;
				data.CreatedBy = CurrentSession.UserID;
				data.CreatedDate = DateTime.Now;

				if (Mode == "Add")
				{
					Helper.ExecuteService("ContactGroups", "CreateNewContactGroup", data);
					model.Message = "Group Saved Successfully";
				}
				else
				{
					Helper.ExecuteService("ContactGroups", "UpdateContactGroup", data);
					model.Message = "Group Updated Successfully";
				}

				//model.RecipientGroupCount = ((DiaryModel)Helper.ExecuteService("Notice", "GetRecipientGroupCnt", data.MemberCode)).ResultCount;
			}
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
			catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
			{
				model.Message = "There was an Error While Saving Recipient Group Details";

			}

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		public PartialViewResult GetOwnGroupList()
		{
			List<RecipientGroup> dataList = new List<RecipientGroup>();
			RecipientGroup data = new RecipientGroup();
			data.CreatedBy = CurrentSession.UserID;
			dataList = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetOwnGroups", data);

			return PartialView("_OwnGroupList", dataList);
		}

		public JsonResult GetOwnGroupListDetails()
		{
			List<RecipientGroup> dataList = new List<RecipientGroup>();
			RecipientGroup data = new RecipientGroup();
			data.CreatedBy = CurrentSession.UserID;
			var list = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetOwnGroups", data);
			//ViewBag.GroupList = new SelectList(list, "GroupID", "Name", null);
			return Json(list, JsonRequestBehavior.AllowGet);
		}

		public PartialViewResult GetGroupMemberByGroupID(int GroupID)
		{
			List<RecipientGroupMember> mdlList= new List<RecipientGroupMember> ();
			RecipientGroupMember recipientGroupMember = new RecipientGroupMember();
			recipientGroupMember.GroupID = GroupID;

			mdlList = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", recipientGroupMember);

			return PartialView("_OwnGroupMemberList", mdlList);
		}

		[HttpPost]
		public ActionResult SaveGroupMember(int? GroupMemberId, int? GroupId,	string GroupMemberName, string Gender, 	string Mobile, string Email,string Mode)
		{
			RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();
			RecipientGroupMember data = new RecipientGroupMember();
			try
			{

				data.GroupMemberID = GroupMemberId ?? 0;
				data.GroupID = GroupId ?? 0;
				data.Name = GroupMemberName;
				data.Gender = Gender;
				data.MobileNo = Mobile;
				data.Email = Email;
				data.IsActive = true;
				data.CreatedBy = CurrentSession.UserID;
				data.CreatedDate = DateTime.Now;			 

				if (Mode == "Add")
				{
					Helper.ExecuteService("ContactGroups", "CreateNewContactGroupMember", data);
					model.Message = "Group Member Saved Successfully";
				}
				else
				{
					Helper.ExecuteService("ContactGroups", "UpdateContactGroupMember", data);
					model.Message = "Group Member Updated Successfully";
				}

			}
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
			catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
			{
				model.Message = "There was an Error While Saving Recipient Group Member Details";
			}

			return Json(model, JsonRequestBehavior.AllowGet);
		}


		public JsonResult GetOwnGroupListCustomizeGroup()
		{
			List<RecipientGroup> dataList = new List<RecipientGroup>();
			RecipientGroup data = new RecipientGroup();
			data.CreatedBy = CurrentSession.UserID;
			dataList = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetOwnGroups", data);

			return Json(dataList, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public ActionResult DeleteGroupMemberById(int GroupMemberID)
		{
			RecipientGroupMember data = new RecipientGroupMember();

			data.GroupMemberID = GroupMemberID;

			Helper.ExecuteService("ContactGroups", "DeleteContactGroupMember", data);

			ViewBag.DeleteMsg = "Deleted Successfully";

			return Json(ViewBag.DeleteMsg, JsonRequestBehavior.AllowGet);
			
		}
	}
}
