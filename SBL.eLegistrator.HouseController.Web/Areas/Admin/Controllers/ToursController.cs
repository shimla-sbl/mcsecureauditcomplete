﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Tour;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using Email.API;
using SMS.API;
using System.IO;
//using Microsoft.Win32;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class ToursController : Controller
    {
        //
        // GET: /Admin/Tours/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult tMemberTourList()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var tMemberTour = (List<tMemberTour>)Helper.ExecuteService("ATour", "GetAllTour", null);
                var model = tMemberTour.ToViewModel1();
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Get All Tour List Details");
                ViewBag.ErrorMessage = "There was an error while getting all the tour list details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult CreatetMemberTour()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var members = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembers", null);
                var model = new TourViewModel()
                {
                    EditMode = "Add",
                    MemberList = new SelectList(members, "MemberCode", "Name", null)
                };
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Create Tour Details");
                ViewBag.ErrorMessage = "There was an error while creating the tour details";
                return View("AdminErrorPage");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SavetMemberTour(TourViewModel model, HttpPostedFileBase file)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }

                    Email.WebService.Service1 EmailWebServiceClient = new Email.WebService.Service1();

                    SMSWebServiceLatest.Service1Client SMSWebServiceClient = new SMSWebServiceLatest.Service1Client();

                    tPaperLaidV sessiondata = new tPaperLaidV();

                    var assmebly = (SBL.DomainModel.Models.PaperLaid.tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", sessiondata);
					model.IsPublished = true;
                    if (model.EditMode == "Add")
                    {
						
                        var data = model.ToDomainModel();

                        data.AssemblyId = assmebly.AssemblyId;

                        data.CreatedBy = Guid.Parse(CurrentSession.UserID);

                        data.ModifiedBy = Guid.Parse(CurrentSession.UserID);

                        //data.mode = model.mode;

                        //data.Purpose = model.Purpose;

                        string realName = "";

                        string aliasName = "";

                        if (file != null)
                        {
                            UploadFile(file, file.FileName, out realName, out aliasName);

                            data.Attachement = realName;

                            data.AliasAttachment = aliasName;
                        }
                        else
                        {
                            data.Attachement = "";

                            data.AliasAttachment = "";
                        }

                        Helper.ExecuteService("ATour", "AddTour", data);

                        mMember member = new mMember();

                        member.MemberCode = data.MemberId;

                        member = (mMember)Helper.ExecuteService("Member", "GetMemberDetailsById", member);

                        if (!string.IsNullOrEmpty(member.Email))
                        {
                            //E-Mail Integration

                            CustomMailMessage emailMessage = new CustomMailMessage();

                            emailMessage._isBodyHtml = true;

                            emailMessage._subject = data.TourTitle;

                            emailMessage._body = data.TourDescrption;

                            emailMessage.ModuleActionID = -1;

                            emailMessage.UniqueIdentificationID = -1;

                            emailMessage._toList.Add(member.Email);

                            var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);

                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                            string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);

                            DirectoryInfo d = new DirectoryInfo(path);

                            FileInfo[] Files = d.GetFiles();

                            if (Files.Any())
                            {
                                if (Files.Any())
                                {
                                    foreach (FileInfo fileInfo in Files)
                                    {
                                        if (fileInfo.Name == data.AliasAttachment)
                                        {
                                            emailMessage._attachments = new List<EAttachment>();

                                            EAttachment ea = new EAttachment();

                                            string realPath = path + "\\" + data.Attachement;

                                            string aliasPath = path + "\\" + data.AliasAttachment;

                                            System.IO.File.Move(aliasPath, realPath);

                                            ea.FileContent = System.IO.File.ReadAllBytes(realPath);

                                            emailMessage._attachments.Add(ea);

                                            System.IO.File.Move(realPath, aliasPath);
                                        }
                                    }

                                    EmailWebServiceClient.UploadAttachments(emailMessage);
                                }
                            }

                            //EmailWebServiceClient.AddEmailToQueue(emailMessage);

                            //SMS Integration

                            SMSMessageList smsMessage = new SMSMessageList();

                            smsMessage.ModuleActionID = -1;

                            smsMessage.UniqueIdentificationID = -1;

                            smsMessage.SMSText = data.TourDescrption;

                            smsMessage.MobileNo.Add(member.Mobile);

                           // SMSWebServiceClient.AddSMSToQueue(smsMessage);
                        }
                    }
                    else
                    {
                        var data = model.ToDomainModel();

                        //data.AssemblyId = assmebly.AssemblyId;
                        data.CreatedBy = Guid.Parse(CurrentSession.UserID);

                        data.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                        // data.MemberId = Convert.ToInt32(CurrentSession.MemberID);

                        //data.mode = model.mode;

                        //data.Purpose = model.Purpose;

                        string realName = "";

                        string aliasName = "";

                        if (file != null && !string.IsNullOrEmpty(data.AliasAttachment))
                        {
                            RemoveExistingFile(data.AliasAttachment);
                        }
                        if (file != null)
                        {
                            UploadFile(file, file.FileName, out realName, out aliasName);

                            data.Attachement = realName;

                            data.AliasAttachment = aliasName;
                        }

                        Helper.ExecuteService("ATour", "UpdateTour", data);

                        mMember member = new mMember();

                        member.MemberCode = data.MemberId;

                        member = (mMember)Helper.ExecuteService("Member", "GetMemberDetailsById", member);

                        if (!string.IsNullOrEmpty(member.Email))
                        {
                            //E-Mail Integration

                            CustomMailMessage emailMessage = new CustomMailMessage();

                            emailMessage._isBodyHtml = true;

                            emailMessage._subject = data.TourTitle;

                            emailMessage._body = data.TourDescrption;

                            emailMessage.ModuleActionID = -1;

                            emailMessage.UniqueIdentificationID = -1;

                            emailMessage._toList.Add(member.Email);

                            var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);

                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                            string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);

                            DirectoryInfo d = new DirectoryInfo(path);

                            FileInfo[] Files = d.GetFiles();

                            if (Files.Any())
                            {
                                foreach (FileInfo fileInfo in Files)
                                {
                                    if (fileInfo.Name == data.AliasAttachment)
                                    {
                                        emailMessage._attachments = new List<EAttachment>();

                                        EAttachment ea = new EAttachment();

                                        string realPath = path + "\\" + data.Attachement;

                                        string aliasPath = path + "\\" + data.AliasAttachment;

                                        System.IO.File.Move(aliasPath, realPath);

                                        ea.FileContent = System.IO.File.ReadAllBytes(realPath);

                                        emailMessage._attachments.Add(ea);

                                        System.IO.File.Move(realPath, aliasPath);
                                    }
                                }

                                EmailWebServiceClient.UploadAttachments(emailMessage);
                            }

                            //EmailWebServiceClient.AddEmailToQueue(emailMessage);

                            //SMS Integration

                            SMSMessageList smsMessage = new SMSMessageList();

                            smsMessage.ModuleActionID = -1;

                            smsMessage.UniqueIdentificationID = -1;

                            smsMessage.SMSText = data.TourDescrption;

                            smsMessage.MobileNo.Add(member.Mobile);

                            //SMSWebServiceClient.AddSMSToQueue(smsMessage);
                        }
                    }
                }

                if (model.EditMode == "Add")
                {
                    return RedirectToAction("CreatetMemberTour");
                }
                else
                {
                    return RedirectToAction("tMemberTourList");
                }
            }
            catch (Exception ex)
            {

                RecordError(ex, "Save Tour Details");
                ViewBag.ErrorMessage = "There was an error while saving the tour details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult EdittMemberTour(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var memberList = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembers", null);
                var tMemberTour = (tMemberTour)Helper.ExecuteService("ATour", "GetTourById", new tMemberTour { TourId = Id });
                var model = tMemberTour.ToViewModel1("Edit");
                model.MemberList = new SelectList(memberList, "MemberCode", "Name", null);
                if (!string.IsNullOrEmpty(model.Attachement) && !(string.IsNullOrEmpty(model.AliasAttachment)))
                {
                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, model.AliasAttachment);
                    model.ImageLocation = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, model.AliasAttachment);
                    //var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                    //model.ImageLocation = fileAcessingSettings.SettingValue + "/Tours/" + model.VAttachement;
                }
                return View("CreatetMemberTour", model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Edit Tour Details");
                ViewBag.ErrorMessage = "There was an error while editing the tour details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult DeletetMemberTour(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var ActualTour = (tMemberTour)Helper.ExecuteService("ATour", "GetTourById", new tMemberTour { TourId = Id });
                var tMemberTour = Helper.ExecuteService("ATour", "DeleteTour", new tMemberTour { TourId = Id });
                if (!string.IsNullOrEmpty(ActualTour.AliasAttachment))
                    RemoveExistingFile(ActualTour.AliasAttachment);
                return RedirectToAction("tMemberTourList");
            }
            catch (Exception ex)
            {
                RecordError(ex, "Delete Tour Details");
                ViewBag.ErrorMessage = "There was an error while deleting the tour details";
                return View("AdminErrorPage");
            }
        }

        private void UploadFile(HttpPostedFileBase File, string FileName, out string realFile, out string aliasFile)
        {
            try
            {
                realFile = "";
                aliasFile = "";

                if (File != null)
                {
                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    realFile = FileName;
                    string fileAliasName = GetAliasNameForFile(FileName);
                    //string extension = System.IO.Path.GetExtension(fileAliasName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, fileAliasName);
                    File.SaveAs(path);
                    aliasFile = fileAliasName;
                    //return (File.FileName);
                }
                //return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RemoveExistingFile(string fileName)
        {
            try
            {
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, fileName);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //public static string GetContentType(string fileName)
        //{
        //    var extension = Path.GetExtension(fileName);

        //    if (String.IsNullOrWhiteSpace(extension))
        //    {
        //        return null;
        //    }

        //    var registryKey = Registry.Classes123.OpenSubKey(extension);

        //    if (registryKey == null)
        //    {
        //        return null;
        //    }

        //    var value = registryKey.GetValue("Content Type") as string;

        //    return String.IsNullOrWhiteSpace(value) ? null : value;
        //}

        public FileResult DownloadAttachment(string realAttachment, string aliasAttachment)
        {
            try
            {
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, aliasAttachment);
                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = realAttachment;
                }

                return pathRes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "TourError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }

        private string GetAliasNameForFile(string fileName)
        {
            string realName = Path.GetFileNameWithoutExtension(fileName);

            string appendName = GetTimestamp(DateTime.Now);

            string appendFileName = realName + "_" + appendName + Path.GetExtension(fileName);

            return appendFileName;
        }

        private string GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
    }
}
