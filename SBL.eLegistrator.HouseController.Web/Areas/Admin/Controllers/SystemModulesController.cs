﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.SystemModule;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System.ComponentModel.DataAnnotations;


namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class SystemModulesController : Controller
    {
        #region System Modules
        //
        // GET: /Admin/SystemModules/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetList()
        {
            SystemModuleModel model = new SystemModuleModel();

            model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetModuleList", model);

            return PartialView("ListModules", model);
        }


        [HttpGet]
        public ActionResult NewSystemModule()
        {
            return PartialView("PageSystemModule");
        }

        public JsonResult SaveEntry(value model)
        {
            if (ModelState.IsValid)
            {
                //SystemModuleModel model = new SystemModuleModel();
                SystemModule objModel = new SystemModule();
                objModel.ModuleName = model.Name;
                objModel.ModuleDescription = model.Description;
                objModel = Helper.ExecuteService("SystemModule", "SaveEntryModules", objModel) as SystemModule;
                //   Update.Message = "Update Sucessfully";
                return Json("Update.Message", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("modelerror", JsonRequestBehavior.AllowGet);
               // return EmptyResult();
            }
        }

        public JsonResult UpdateEntry(value model)
        {
            //SystemModuleModel model = new SystemModuleModel();
            SystemModule objModel = new SystemModule();
            objModel.ModuleIdID = model.ModuleId;
            objModel.ModuleName = model.Name;
            objModel.ModuleDescription = model.Description;
            objModel = Helper.ExecuteService("SystemModule", "UpdateEntryModules", objModel) as SystemModule;
            //   Update.Message = "Update Sucessfully";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataByID(int ModuleId)
        {
#pragma warning disable CS0219 // The variable 'Month' is assigned but its value is never used
            string Month = "";
#pragma warning restore CS0219 // The variable 'Month' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Day' is assigned but its value is never used
            string Day = "";
#pragma warning restore CS0219 // The variable 'Day' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Year' is assigned but its value is never used
            string Year = "";
#pragma warning restore CS0219 // The variable 'Year' is assigned but its value is never used
            SystemModuleModel model = new SystemModuleModel();
            int Id = Convert.ToInt32(ModuleId);
            model.SystemModuleID = Id;
            model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetModuleDataById", model);

            foreach (var item in model.objList)
            {
                model.SystemModuleID = item.SystemModuleID;
                model.Name = item.Name;
                model.Description = item.Description;

            }

            return PartialView("UpdateSystemModules", model);
        }
      
        public ActionResult DeleteSystemModule(Int32 SystemModuleID)
        {
            return RedirectToAction("Index");
        }

        public ActionResult EditSystemModule(Int32 SystemModuleID)
        {
            SystemModuleModel systemModuleModel = new SystemModuleModel();
            return View("NewSystemModule", systemModuleModel);
        }
        public JsonResult DeleteEntry(int ModuleId)
        {
            //SystemModuleModel model = new SystemModuleModel();
            SystemModule objModel = new SystemModule();
            objModel.ModuleIdID = ModuleId;
            objModel = Helper.ExecuteService("SystemModule", "DeleteModule", objModel) as SystemModule;
            //   Update.Message = "Update Sucessfully";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region System Functions.
        public ActionResult SystemFunctions()
        {
            return View();
        }

        public ActionResult GetSystemFunctList()
        {
            SystemModuleModel model = new SystemModuleModel();

            model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetModuleActionList", model);

            return PartialView("ListSystemFunction", model);
        }

        [HttpGet]
        public ActionResult NewSystemFunction()
        {
            SystemModuleModel model = new SystemModuleModel();
            model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetModules", model);
            model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetRecipientList", model);

            return PartialView("PageSystemFunction", model);
        }


        [HttpPost]
        public ActionResult NewSystemFunction(SystemModuleModel model)
        {
            if (ModelState.IsValid)
            {
                ModuleAction Value = new ModuleAction();
                Value.ModuleId = model.ModuleId;
                Value.ActionName = model.ActionName;
                Value.Parameters = model.Parameters;
                Value.Description = model.Description;
                Value.SendSMS = model.SendSMS;
                Value.SendEmail = model.SendEmail;
                foreach (var item in model.RecipNameIds)
                {

                    string ResultString = string.Join(",", model.RecipNameIds.ToArray());
                    Value.AssociatedContactGroups = ResultString;
                }
                Value = Helper.ExecuteService("SystemModule", "SaveModulesAction", Value) as ModuleAction;
                return RedirectToAction("SystemFunctions");
            }
            return Content("saved");
        }
        public ActionResult PopUp(string Id)
        {
            SystemModuleModel model = new SystemModuleModel();
            int GroupId = Convert.ToInt32(Id);
            model.GroupId = GroupId;
            model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetRecipList", model);
            return PartialView("ListGroupContact", model);
        }
        
        public ActionResult DeleteSystemFunction(Int32 SystemFunctionID)
        {
            return RedirectToAction("SystemFunctions");
        }

        public ActionResult EditSystemFunction(Int32 SystemFunctionID)
        {
            SystemModuleModel systemModuleModel = new SystemModuleModel();
            return View("NewSystemFunction", systemModuleModel);
        }
        public ActionResult GetDataByIDfunction(int ModuleId)
        {
#pragma warning disable CS0219 // The variable 'Month' is assigned but its value is never used
            string Month = "";
#pragma warning restore CS0219 // The variable 'Month' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Day' is assigned but its value is never used
            string Day = "";
#pragma warning restore CS0219 // The variable 'Day' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Year' is assigned but its value is never used
            string Year = "";
#pragma warning restore CS0219 // The variable 'Year' is assigned but its value is never used
            SystemModuleModel model = new SystemModuleModel();
            int Id = Convert.ToInt32(ModuleId);
            model.SystemModuleID = Id;
            model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetModules", model);
            model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetRecipientList", model);
            model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetActionModuleDataById", model);

            foreach (var item in model.UpdatevalueList)
            {
                model.UAId = item.UAId;
                model.SystemModuleID = item.ModuleId;
                model.ActionName = item.ActionName;
                model.Parameters = item.Parameters;
                model.Description = item.Description;
                model.SendSMS = item.SendSMS;
                model.SendEmail = item.SendEmail;
                //model.AssociatedContactGroups = item.AssociatedContactGroups;
                string s = item.AssociatedContactGroups;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');
                    model.RecipNameIds = new List<string>();
                    for (int i = 0; i < values.Length; i++)
                    {
                        model.RecipNameIds.Add(values[i]);
                    }

                }
            }

            return PartialView("_UpdateFunctions", model);
        }
        [HttpPost]
        public ActionResult NewSystemFunctionUpdate(SystemModuleModel model)
        {
            //if (model.SaveCreateNew)
            //{
            ModuleAction Value = new ModuleAction();
            Value.SystemFunctionID = model.UAId;
            Value.ModuleId = model.SystemModuleID;
            Value.ActionName = model.ActionName;
            Value.Parameters = model.Parameters;
            Value.Description = model.Description;
            Value.SendSMS = model.SendSMS;
            Value.SendEmail = model.SendEmail;
            if (model.RecipNameIds != null)
            {
                foreach (var item in model.RecipNameIds)
                {

                    string ResultString = string.Join(",", model.RecipNameIds.ToArray());
                    Value.AssociatedContactGroups = ResultString;
                }
            }
            Value = Helper.ExecuteService("SystemModule", "UpdateModulesAction", Value) as ModuleAction;

            //    return RedirectToAction("NewSystemFunction");
            //}
            //else
            //{
            return RedirectToAction("SystemFunctions");
        }
        public ActionResult TemplatePopUp(string Id)
        {
            SystemModuleModel model = new SystemModuleModel();
            int GroupId = Convert.ToInt32(Id);
            model.SystemModuleID = GroupId;
            model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetTemplate", model);
            model = (SystemModuleModel)Helper.ExecuteService("SystemModule", "GetValue", model);
            return PartialView("TemplatePopUp", model);
        }
        public JsonResult UpdateTemplate(value model)
        {
            SystemModuleModel Val = new SystemModuleModel();
            ModuleAction update = new ModuleAction();
            update.SystemFunctionID = model.SystFuntId;
            update.SMSTemplate = model.text;
            update.EmailTemplate = model.textFromEditor;
            update = Helper.ExecuteService("SystemModule", "UpdateModulesActionTemplates", update) as ModuleAction;
            Val.Message = "Update Sucessfully";
            return Json(Val.Message, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
    public class value
    {
        [AllowHtml]
        [Required(ErrorMessage = "Field is required")]
        public string Name { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public int ModuleId { get; set; }
        [AllowHtml]
        public string text { get; set; }
        public string textFromEditor { get; set; }
        public int SystFuntId { get; set; }

    }
}
