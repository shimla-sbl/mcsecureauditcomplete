﻿using Ionic.Zip;
using Microsoft.Security.Application;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.LOB;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class LOBFileController : Controller
    {
        //
        // GET: /Admin/LOBFile/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TransferFileForAssemblyHouse()
        {

            LegislationFixationModel model = new LegislationFixationModel();
            tPaperLaidV model1 = new tPaperLaidV();
            //Get the Total count of All Type of question.
            model1 = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model1);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            else
            {
                model.AssemblyId = model1.AssemblyId;
                model.SessionId = model1.SessionId;
            }



            // model.AssemblyId = model1.AssemblyId;
            //model.SessionId = model1.SessionId;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;

            mSessionDate sessionDates = new mSessionDate();

            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();


            mSession.SessionCode = model.SessionId;
            mSession.AssemblyID = model.AssemblyId;
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {

                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
                LegislationFixationModel selectList = new LegislationFixationModel();
                selectList.Id = 0;
                selectList.SessionDate = "Select Session Date";
                model.CustomSessionDateList.Add(selectList);
            }

            return View(model);
        }
        public ActionResult PaperLaidVHtml()
        {
            LegislationFixationModel model = new LegislationFixationModel();
            tPaperLaidV model1 = new tPaperLaidV();
            //Get the Total count of All Type of question.
            model1 = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model1);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            else
            {
                model.AssemblyId = model1.AssemblyId;
                model.SessionId = model1.SessionId;
            }



            // model.AssemblyId = model1.AssemblyId;
            //model.SessionId = model1.SessionId;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;

            mSessionDate sessionDates = new mSessionDate();

            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();


            mSession.SessionCode = model.SessionId;
            mSession.AssemblyID = model.AssemblyId;
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {

                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
                LegislationFixationModel selectList = new LegislationFixationModel();
                selectList.Id = 0;
                selectList.SessionDate = "Select Session Date";
                model.CustomSessionDateList.Add(selectList);


                //model.SessionId = siteSettingMod.SessionCode;
                // model.AssemblyId = siteSettingMod.AssemblyCode;


            }


            return View(model);
            //return PartialView("GetSessionList", model);
        }
        public ActionResult GetLatestFileVersion()
        {
            LegislationFixationModel model = new LegislationFixationModel();
            tPaperLaidV model1 = new tPaperLaidV();
            //Get the Total count of All Type of question.
            model1 = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model1);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            else
            {
                model.AssemblyId = model1.AssemblyId;
                model.SessionId = model1.SessionId;
            }



            // model.AssemblyId = model1.AssemblyId;
            //model.SessionId = model1.SessionId;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;

            mSessionDate sessionDates = new mSessionDate();

            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();


            mSession.SessionCode = model.SessionId;
            mSession.AssemblyID = model.AssemblyId;
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {

                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
                LegislationFixationModel selectList = new LegislationFixationModel();
                selectList.Id = 0;
                selectList.SessionDate = "Select Session Date";
                model.CustomSessionDateList.Add(selectList);



            }


            return View(model);
            //return PartialView("GetSessionList", model);
        }
        public ActionResult DraftAndApprovedPaperLaidVHtml()
        {
            LegislationFixationModel model = new LegislationFixationModel();
            tPaperLaidV model1 = new tPaperLaidV();
            //Get the Total count of All Type of question.
            model1 = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model1);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            else
            {
                model.AssemblyId = model1.AssemblyId;
                model.SessionId = model1.SessionId;
            }



            // model.AssemblyId = model1.AssemblyId;
            //model.SessionId = model1.SessionId;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;

            mSessionDate sessionDates = new mSessionDate();

            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();


            mSession.SessionCode = model.SessionId;
            mSession.AssemblyID = model.AssemblyId;
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {

                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
                LegislationFixationModel selectList = new LegislationFixationModel();
                selectList.Id = 0;
                selectList.SessionDate = "Select Session Date";
                model.CustomSessionDateList.Add(selectList);


                //model.SessionId = siteSettingMod.SessionCode;
                // model.AssemblyId = siteSettingMod.AssemblyCode;


            }


            return View(model);
            //return PartialView("GetSessionList", model);
        }
        public ActionResult LetetFileversionDownload()
        {
            LegislationFixationModel model = new LegislationFixationModel();
            tPaperLaidV model1 = new tPaperLaidV();
            //Get the Total count of All Type of question.
            model1 = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model1);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            else
            {
                model.AssemblyId = model1.AssemblyId;
                model.SessionId = model1.SessionId;
            }


            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;

            mSessionDate sessionDates = new mSessionDate();

            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();


            mSession.SessionCode = model.SessionId;
            mSession.AssemblyID = model.AssemblyId;
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {

                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
                LegislationFixationModel selectList = new LegislationFixationModel();
                selectList.Id = 0;
                selectList.SessionDate = "Select Session Date";
                model.CustomSessionDateList.Add(selectList);

            }

            return View(model);
        }
        public ActionResult GetSessionList(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyID = siteSettingMod.AssemblyCode;
            mSessionDate sessionDates = new mSessionDate();
            LegislationFixationModel model = new LegislationFixationModel();
            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();
            //if (siteSettingMod != null)
            //{

                mSession.SessionCode =int.Parse (CurrentSession.SessionId);
                  
                mSession.AssemblyID = int.Parse(CurrentSession.AssemblyId);
                model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
                if (model.SessionDateList.Count > 0)
                {

                    foreach (var session in model.SessionDateList)
                    {
                        LegislationFixationModel fixModel = new LegislationFixationModel();
                        fixModel.Id = session.Id;
                        fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                        model.CustomSessionDateList.Add(fixModel);
                    }
                    LegislationFixationModel selectList = new LegislationFixationModel();
                    selectList.Id = 0;
                    selectList.SessionDate = "Select Session Date";
                    model.CustomSessionDateList.Add(selectList);

                }
            //model.SessionId = siteSettingMod.SessionCode;
            //    model.AssemblyId = siteSettingMod.AssemblyCode;


            //}

            if (PageNumber != null && PageNumber != "")
            {
                model.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                model.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                model.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                model.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                model.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                model.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                model.loopEnd = Convert.ToInt32("5");
            }

            return PartialView("GetSessionList", model);
        }

        public JsonResult TransferringAllDocFile(string SessionDate, string AssemblyId, string SessionId)
        {
            DateTime Sessiondate = new DateTime();
            StringBuilder ErorLists = new StringBuilder();
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageCulture == "hi-IN")
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                Sessiondate = Convert.ToDateTime(Date);
            }
            else
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Sessiondate = Convert.ToDateTime(Date);
            }
            try
            {
                int Count = 0;
                DraftLOB DraftLob = new DraftLOB();
                List<QuestionModelCustom> ListOtherpaper = new List<QuestionModelCustom>();
                DraftLob.SessionDate = Sessiondate;
                ListOtherpaper = (List<QuestionModelCustom>)Helper.ExecuteService("LOB", "GetDraftLobLatestPapersLaid", DraftLob);
                if (ListOtherpaper.Count > 0)
                {
                    for (int i = 0; i < ListOtherpaper.Count; i++)
                    {
                        string[] dateArr = SessionDate.Split('/');
                        string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                        string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "_Doc" + "/Documents/";
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                        if (i == 0)
                        {
                            if (Directory.Exists(directory))
                            {

                            }
                            else
                            {
                                Directory.CreateDirectory(directory);
                            }

                        }

                        if (ListOtherpaper[i].DocFilePath != null && ListOtherpaper[i].DocFilePath != "")
                        {
                            string fileExt = Path.GetExtension(ListOtherpaper[i].DocFilePath);
                            string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                            if (ListOtherpaper[i].SrNo1 != null && ListOtherpaper[i].SrNo1.ToString().Trim() != "")
                            {
                                fileName = ListOtherpaper[i].SrNo1.ToString().Trim();
                            }
                            if (ListOtherpaper[i].SrNo2 != null && ListOtherpaper[i].SrNo2.ToString().Trim() != "")
                            {
                                fileName = fileName + "_" + ListOtherpaper[i].SrNo2.ToString().Trim();
                            }
                            if (ListOtherpaper[i].SrNo3 != null && ListOtherpaper[i].SrNo3.ToString().Trim() != "")
                            {
                                fileName = fileName + "_" + ListOtherpaper[i].SrNo3;
                            }
                            if (fileExt == ".doc")
                            {
                                fileName = fileName + ".doc";
                            }
                            else if (fileExt == ".docx")
                            {
                                fileName = fileName + ".docx";
                            }

                            var path = System.IO.Path.Combine(directory, fileName);
                            if (ListOtherpaper[i].DocFilePath.Contains("~"))
                            {
                                ListOtherpaper[i].DocFilePath = ListOtherpaper[i].DocFilePath.Substring(1, ListOtherpaper[i].DocFilePath.Length - 1);
                            }
                            string From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].DocFilePath);
                            string copyPath = From;
                            string pastePath = path;
                            if (FileExists(From))
                            {
                                System.IO.File.Copy(copyPath, pastePath, true);
                                Count = 1;
                            }

                        }

                    }
                }

                tQuestion Question = new tQuestion();
                Question.IsFixedDate = Sessiondate;
                Question.QuestionType = 1;
                List<tQuestion> ListQuestion = new List<tQuestion>();
                List<QuestionModelCustom> ListQuestionModelCustom = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateWithPaper", Question);

                for (int i = 0; i < ListQuestionModelCustom.Count; i++)
                {
                    string[] dateArr = SessionDate.Split('/');
                    string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                    string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "_Doc" + "/Starred/";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    if (i == 0)
                    {
                        if (Directory.Exists(directory))
                        {
                        }
                        else
                        {
                            Directory.CreateDirectory(directory);
                        }

                    }

                    if (ListQuestionModelCustom[i].DocFilePath != null && ListQuestionModelCustom[i].DocFilePath != "")
                    {

                        string fileExt = Path.GetExtension(ListQuestionModelCustom[i].DocFilePath);
                        string fileName = "";
                        if (fileExt == ".doc")
                        {
                            fileName = ListQuestionModelCustom[i].QuestionNumber + ".doc";
                        }
                        else if (fileExt == ".docx")
                        {
                            fileName = ListQuestionModelCustom[i].QuestionNumber + ".docx";
                        }
                        var path = Path.Combine(directory, fileName);
                        string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].DocFilePath);
                        string copyPath = From;
                        string pastePath = path;
                        if (FileExists(From))
                        {
                            System.IO.File.Copy(copyPath, pastePath, true);
                            Count = 1;
                        }


                    }
                }
                Question = new tQuestion();
                Question.IsFixedDate = Sessiondate;
                Question.QuestionType = 2;
                ListQuestion = new List<tQuestion>();
                ListQuestionModelCustom = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateWithPaper", Question);

                for (int i = 0; i < ListQuestionModelCustom.Count; i++)
                {
                    string[] dateArr = SessionDate.Split('/');
                    string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                    string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "_Doc" + "/Unstarred/";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    if (i == 0)
                    {
                        if (Directory.Exists(directory))
                        {
                        }
                        else
                        {
                            Directory.CreateDirectory(directory);
                        }

                    }

                    if (ListQuestionModelCustom[i].DocFilePath != null && ListQuestionModelCustom[i].DocFilePath != "")
                    {

                        string fileExt = Path.GetExtension(ListQuestionModelCustom[i].DocFilePath);
                        string fileName = "";

                        if (fileExt == ".doc")
                        {
                            fileName = ListQuestionModelCustom[i].QuestionNumber + ".doc";
                        }
                        else if (fileExt == ".docx")
                        {
                            fileName = ListQuestionModelCustom[i].QuestionNumber + ".docx";
                        }
                        var path = Path.Combine(directory, fileName);
                        string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].DocFilePath);

                        string copyPath = From;
                        string pastePath = path;
                        if (FileExists(From))
                        {
                            System.IO.File.Copy(copyPath, pastePath, true);
                            Count = 1;
                        }


                    }
                }

                if (Count == 0)
                {
                    ErorLists.Append("No File Available!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
                else if (!string.IsNullOrEmpty(ErorLists.ToString()))
                {
                    ErorLists.Append("!! Files are not Transferred !!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ErorLists.Append("All Files are Transferred!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("File Download Error!", JsonRequestBehavior.AllowGet);
            }

            // return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult TransferringFileForAssemblyHouse(string SessionDate, string AssemblyId, string SessionId)
        {
            DateTime Sessiondate = new DateTime();
            StringBuilder ErorLists = new StringBuilder();
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageCulture == "hi-IN")
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                Sessiondate = Convert.ToDateTime(Date);
            }
            else
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Sessiondate = Convert.ToDateTime(Date);
            }
            try
            {
                int Count = 0;
                DraftLOB DraftLob = new DraftLOB();
                List<QuestionModelCustom> ListOtherpaper = new List<QuestionModelCustom>();
                DraftLob.SessionDate = Sessiondate;
                ListOtherpaper = (List<QuestionModelCustom>)Helper.ExecuteService("LOB", "GetDraftLobLatestPapersLaid", DraftLob);
                if (ListOtherpaper.Count > 0)
                {
                    for (int i = 0; i < ListOtherpaper.Count; i++)
                    {
                        string[] dateArr = SessionDate.Split('/');
                        string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                        string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/OnlineDocuments/";
                        //string urlDoc = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/OnlineDocuments/DocFile";
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                        // string Docdirectory = System.IO.Path.Combine(FileSettings.SettingValue + urlDoc);
                        if (i == 0)
                        {
                            if (Directory.Exists(directory))
                            {

                            }
                            else
                            {
                                Directory.CreateDirectory(directory);
                            }
                            //if (Directory.Exists(Docdirectory))
                            //{

                            //}
                            //else
                            //{
                            //    Directory.CreateDirectory(Docdirectory);
                            //}
                        }

                        if (ListOtherpaper[i].FileName != null && ListOtherpaper[i].FileName != "")
                        {

                            string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                            if (ListOtherpaper[i].SrNo1 != null && ListOtherpaper[i].SrNo1.ToString().Trim() != "")
                            {
                                fileName = ListOtherpaper[i].SrNo1.ToString().Trim();
                            }
                            if (ListOtherpaper[i].SrNo2 != null && ListOtherpaper[i].SrNo2.ToString().Trim() != "")
                            {
                                fileName = fileName + "_" + ListOtherpaper[i].SrNo2.ToString().Trim();
                            }
                            if (ListOtherpaper[i].SrNo3 != null && ListOtherpaper[i].SrNo3.ToString().Trim() != "")
                            {
                                fileName = fileName + "_" + ListOtherpaper[i].SrNo3;
                            }
                            fileName = fileName + ".pdf";

                            var path = System.IO.Path.Combine(directory, fileName);
                            if (ListOtherpaper[i].FileName.Contains("~"))
                            {
                                ListOtherpaper[i].FileName = ListOtherpaper[i].FileName.Substring(1, ListOtherpaper[i].FileName.Length - 1);
                            }

                            string From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].FileName);

                            string copyPath = From;
                            string pastePath = path;
                            if (FileExists(From))
                            {

                                System.IO.File.Copy(copyPath, pastePath, true);
                            }
                            else
                            {
                                ErorLists.Append(From + ",  ");
                            }
                            Count = 1;
                        }
                        //if (ListOtherpaper[i].DocFilePath != null && ListOtherpaper[i].DocFilePath != "")
                        //{

                        //    string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                        //    if (ListOtherpaper[i].SrNo1 != null && ListOtherpaper[i].SrNo1.ToString().Trim() != "")
                        //    {
                        //        fileName = ListOtherpaper[i].SrNo1.ToString().Trim();
                        //    }
                        //    if (ListOtherpaper[i].SrNo2 != null && ListOtherpaper[i].SrNo2.ToString().Trim() != "")
                        //    {
                        //        fileName = fileName + "_" + ListOtherpaper[i].SrNo2.ToString().Trim();
                        //    }
                        //    if (ListOtherpaper[i].SrNo3 != null && ListOtherpaper[i].SrNo3.ToString().Trim() != "")
                        //    {
                        //        fileName = fileName + "_" + ListOtherpaper[i].SrNo3;
                        //    }
                        //    fileName = fileName + ".doc";


                        //    var path = System.IO.Path.Combine(Docdirectory, fileName);
                        //    if (ListOtherpaper[i].DocFilePath.Contains("~"))
                        //    {
                        //        ListOtherpaper[i].DocFilePath = ListOtherpaper[i].DocFilePath.Substring(1, ListOtherpaper[i].DocFilePath.Length - 1);
                        //    }
                        //    string From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].DocFilePath);
                        //    string copyPath = From;
                        //    string pastePath = path;
                        //    if (FileExists(From))
                        //    {
                        //        System.IO.File.Copy(copyPath, pastePath, true);
                        //    }

                        //    Count = 1;
                        //}

                    }
                }

                ListOtherpaper = (List<QuestionModelCustom>)Helper.ExecuteService("LOB", "GetDraftLobLatestPapersForTransfer", DraftLob);

                if (ListOtherpaper.Count > 0)
                {
                    for (int i = 0; i < ListOtherpaper.Count; i++)
                    {
                        string[] dateArr = SessionDate.Split('/');
                        string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                        // string url = "/AssemblyHouse/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Documents/";

                        string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Documents/";
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);

                        if (i == 0)
                        {
                            //   string directory = Server.MapPath(url);
                            if (Directory.Exists(directory))
                            {
                                Directory.Delete(directory, true);
                                Directory.CreateDirectory(directory);
                            }
                            else
                            {
                                Directory.CreateDirectory(directory);
                            }
                        }
                        string From = "";
                        string path = "";
                        if (ListOtherpaper[i].DocFilePath != null && ListOtherpaper[i].DocFilePath != "")
                        {
                            if (ListOtherpaper[i].PaperLaidId == null || ListOtherpaper[i].PaperLaidId.ToString() == "" || ListOtherpaper[i].PaperLaidId == 0)
                            {

                                int indexof = ListOtherpaper[i].DocFilePath.LastIndexOf('/');
                                string fileName = ListOtherpaper[i].DocFilePath.Substring(indexof + 1);

                                //var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                path = System.IO.Path.Combine(directory, fileName);
                                if (ListOtherpaper[i].DocFilePath.Contains("~"))
                                {
                                    ListOtherpaper[i].DocFilePath = ListOtherpaper[i].DocFilePath.Substring(1, ListOtherpaper[i].DocFilePath.Length - 1);
                                }

                                From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].DocFilePath);
                                string copyPath = From;
                                string pastePath = path;
                                if (FileExists(From))
                                {
                                    System.IO.File.Copy(copyPath, pastePath, true);
                                }
                                else
                                {
                                    ErorLists.Append(From + ",  ");
                                }
                            }
                            if (ListOtherpaper[i].PaperLaidId != null && ListOtherpaper[i].PaperLaidId.ToString() != "" && ListOtherpaper[i].PaperLaidId != 0)
                            {
                                string fileName = "";// addLines.LOBId + "_PDF_" + Location;

                                if (ListOtherpaper[i].SrNo1 != null && ListOtherpaper[i].SrNo1.ToString().Trim() != "")
                                {
                                    fileName = ListOtherpaper[i].SrNo1.ToString().Trim();
                                }
                                if (ListOtherpaper[i].SrNo2 != null && ListOtherpaper[i].SrNo2.ToString().Trim() != "")
                                {
                                    fileName = fileName + "_" + ListOtherpaper[i].SrNo2.ToString().Trim();
                                }
                                if (ListOtherpaper[i].SrNo3 != null && ListOtherpaper[i].SrNo3.ToString().Trim() != "")
                                {
                                    fileName = fileName + "_" + ListOtherpaper[i].SrNo3;
                                }
                                fileName = fileName + ".pdf";


                                path = System.IO.Path.Combine(directory, fileName);
                                if (ListOtherpaper[i].FileName.Contains("~"))
                                {
                                    ListOtherpaper[i].FileName = ListOtherpaper[i].FileName.Substring(1, ListOtherpaper[i].FileName.Length - 1);
                                }


                                From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].FileName);
                                string copyPath = From;
                                string pastePath = path;
                                if (FileExists(From))
                                {
                                    System.IO.File.Copy(copyPath, pastePath, true);
                                }
                                else
                                {
                                    ErorLists.Append(From + ",  ");
                                }
                            }
                            //string From = Server.MapPath(ListAdminLOB[i].PDFLocation);



                            Count = 1;
                        }

                    }
                }

                tQuestion Question = new tQuestion();
                Question.IsFixedDate = Sessiondate;
                Question.QuestionType = 1;
                List<tQuestion> ListQuestion = new List<tQuestion>();
                List<QuestionModelCustom> ListQuestionModelCustom = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateWithPaper", Question);

                for (int i = 0; i < ListQuestionModelCustom.Count; i++)
                {
                    string[] dateArr = SessionDate.Split('/');
                    string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                    string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Starred/";
                    //string urlDoc = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Starred/DocFile";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    //  string Docdirectory = System.IO.Path.Combine(FileSettings.SettingValue + urlDoc);
                    if (i == 0)
                    {
                        if (Directory.Exists(directory))
                        {
                        }
                        else
                        {
                            Directory.CreateDirectory(directory);
                        }
                        //if (Directory.Exists(Docdirectory))
                        //{
                        //}
                        //else
                        //{
                        //    Directory.CreateDirectory(Docdirectory);
                        //}
                    }
                    if (ListQuestionModelCustom[i].FileName != null && ListQuestionModelCustom[i].FileName != "")
                    {

                        string fileExt = Path.GetExtension(ListQuestionModelCustom[i].FileName);
                        string fileName = "";
                        if (fileExt == ".pdf")
                        {
                            fileName = ListQuestionModelCustom[i].QuestionNumber + ".pdf";
                        }

                        var path = Path.Combine(directory, fileName);
                        string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].FileName);

                        string copyPath = From;
                        string pastePath = path;
                        if (FileExists(From))
                        {
                            System.IO.File.Copy(copyPath, pastePath, true);
                        }
                        else
                        {
                            ErorLists.Append(From + ",  ");
                        }
                        Count = 1;
                    }
                    //if (ListQuestionModelCustom[i].DocFilePath != null && ListQuestionModelCustom[i].DocFilePath != "")
                    //{

                    //    string fileExt = Path.GetExtension(ListQuestionModelCustom[i].DocFilePath);
                    //    string fileName = "";
                    //    if (fileExt == ".doc")
                    //    {
                    //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".doc";
                    //    }
                    //    else if (fileExt == ".docx")
                    //    {
                    //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".docx";
                    //    }
                    //    var path = Path.Combine(Docdirectory, fileName);
                    //    string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].DocFilePath);
                    //    string copyPath = From;
                    //    string pastePath = path;
                    //    if (FileExists(From))
                    //    {
                    //        System.IO.File.Copy(copyPath, pastePath, true);
                    //    }

                    //    Count = 1;
                    //}
                }


                Question = new tQuestion();
                Question.IsFixedDate = Sessiondate;
                Question.QuestionType = 2;
                ListQuestion = new List<tQuestion>();
                ListQuestionModelCustom = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateWithPaper", Question);

                for (int i = 0; i < ListQuestionModelCustom.Count; i++)
                {
                    string[] dateArr = SessionDate.Split('/');
                    string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                    string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Unstarred/";
                    // string urlDoc = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Unstarred/DocFile";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    // string Docdirectory = System.IO.Path.Combine(FileSettings.SettingValue + urlDoc);
                    if (i == 0)
                    {
                        if (Directory.Exists(directory))
                        {
                        }
                        else
                        {
                            Directory.CreateDirectory(directory);
                        }
                        //if (Directory.Exists(Docdirectory))
                        //{
                        //}
                        //else
                        //{
                        //    Directory.CreateDirectory(Docdirectory);
                        //}
                    }
                    if (ListQuestionModelCustom[i].FileName != null && ListQuestionModelCustom[i].FileName != "")
                    {

                        string fileExt = Path.GetExtension(ListQuestionModelCustom[i].FileName);
                        string fileName = "";
                        if (fileExt == ".pdf")
                        {
                            fileName = ListQuestionModelCustom[i].QuestionNumber + ".pdf";
                        }

                        var path = Path.Combine(directory, fileName);
                        string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].FileName);
                        string copyPath = From;
                        string pastePath = path;
                        if (FileExists(From))
                        {
                            System.IO.File.Copy(copyPath, pastePath, true);
                        }
                        else
                        {
                            ErorLists.Append(From + ",  ");
                        }

                        Count = 1;
                    }
                    //if (ListQuestionModelCustom[i].DocFilePath != null && ListQuestionModelCustom[i].DocFilePath != "")
                    //{

                    //    string fileExt = Path.GetExtension(ListQuestionModelCustom[i].DocFilePath);
                    //    string fileName = "";

                    //    if (fileExt == ".doc")
                    //    {
                    //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".doc";
                    //    }
                    //    else if (fileExt == ".docx")
                    //    {
                    //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".docx";
                    //    }
                    //    var path = Path.Combine(Docdirectory, fileName);
                    //    string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].DocFilePath);

                    //    string copyPath = From;
                    //    string pastePath = path;
                    //    if (FileExists(From))
                    //    {
                    //        System.IO.File.Copy(copyPath, pastePath, true);
                    //    }

                    //    Count = 1;
                    //}
                }



                if (Count == 0)
                {
                    ErorLists.Append("No File Available!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
                else if (!string.IsNullOrEmpty(ErorLists.ToString()))
                {
                    ErorLists.Append("!! Files are not Transferred !!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ErorLists.Append("All Files are Transferred!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("File Download Error!", JsonRequestBehavior.AllowGet);
            }

            // return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult TransferringSelectedFileForAssemblyHouse(string SessionDate, string AssemblyId, string SessionId, string QuestionIds, string AssemblyIds)
        {

            string[] QuestionArray = null;
            if (!string.IsNullOrEmpty(QuestionIds))
                QuestionArray = QuestionIds.Split(',');

            string[] AssemblyArray = null;
            if (!string.IsNullOrEmpty(AssemblyIds))
                AssemblyArray = AssemblyIds.Split(',');

            DateTime Sessiondate = new DateTime();
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageCulture == "hi-IN")
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                Sessiondate = Convert.ToDateTime(Date);
            }
            else
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Sessiondate = Convert.ToDateTime(Date);
            }
            try
            {
                int Count = 0;
                StringBuilder ErorLists = new StringBuilder();
                DraftLOB DraftLob = new DraftLOB();
                List<QuestionModelCustom> ListOtherpaper = new List<QuestionModelCustom>();
                DraftLob.SessionDate = Sessiondate;
                ListOtherpaper = (List<QuestionModelCustom>)Helper.ExecuteService("LOB", "GetDraftLobLatestPapersLaid", DraftLob);
                if (ListOtherpaper.Count > 0)
                {
                    for (int i = 0; i < ListOtherpaper.Count; i++)
                    {
                        string[] dateArr = SessionDate.Split('/');
                        string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                        string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/OnlineDocuments/";
                        //string urlDoc = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/OnlineDocuments/DocFile";
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                        // string Docdirectory = System.IO.Path.Combine(FileSettings.SettingValue + urlDoc);
                        if (i == 0)
                        {
                            if (Directory.Exists(directory))
                            {

                            }
                            else
                            {
                                Directory.CreateDirectory(directory);
                            }
                            //if (Directory.Exists(Docdirectory))
                            //{

                            //}
                            //else
                            //{
                            //    Directory.CreateDirectory(Docdirectory);
                            //}
                        }
                        if (!string.IsNullOrEmpty(AssemblyIds))
                        {
                            if (AssemblyArray.Contains(ListOtherpaper[i].PaperLaidId.ToString()))
                            {
                                if (ListOtherpaper[i].FileName != null && ListOtherpaper[i].FileName != "")
                                {

                                    string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                                    if (ListOtherpaper[i].SrNo1 != null && ListOtherpaper[i].SrNo1.ToString().Trim() != "")
                                    {
                                        fileName = ListOtherpaper[i].SrNo1.ToString().Trim();
                                    }
                                    if (ListOtherpaper[i].SrNo2 != null && ListOtherpaper[i].SrNo2.ToString().Trim() != "")
                                    {
                                        fileName = fileName + "_" + ListOtherpaper[i].SrNo2.ToString().Trim();
                                    }
                                    if (ListOtherpaper[i].SrNo3 != null && ListOtherpaper[i].SrNo3.ToString().Trim() != "")
                                    {
                                        fileName = fileName + "_" + ListOtherpaper[i].SrNo3;
                                    }
                                    fileName = fileName + ".pdf";

                                    var path = System.IO.Path.Combine(directory, fileName);
                                    if (ListOtherpaper[i].FileName.Contains("~"))
                                    {
                                        ListOtherpaper[i].FileName = ListOtherpaper[i].FileName.Substring(1, ListOtherpaper[i].FileName.Length - 1);
                                    }

                                    string From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].FileName);

                                    string copyPath = From;
                                    string pastePath = path;
                                    if (FileExists(From))
                                    {

                                        System.IO.File.Copy(copyPath, pastePath, true);
                                    }
                                    else
                                    {
                                        ErorLists.Append(From + ",  ");
                                    }

                                    Count = 1;
                                }
                                //if (ListOtherpaper[i].DocFilePath != null && ListOtherpaper[i].DocFilePath != "")
                                //{

                                //    string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                                //    if (ListOtherpaper[i].SrNo1 != null && ListOtherpaper[i].SrNo1.ToString().Trim() != "")
                                //    {
                                //        fileName = ListOtherpaper[i].SrNo1.ToString().Trim();
                                //    }
                                //    if (ListOtherpaper[i].SrNo2 != null && ListOtherpaper[i].SrNo2.ToString().Trim() != "")
                                //    {
                                //        fileName = fileName + "_" + ListOtherpaper[i].SrNo2.ToString().Trim();
                                //    }
                                //    if (ListOtherpaper[i].SrNo3 != null && ListOtherpaper[i].SrNo3.ToString().Trim() != "")
                                //    {
                                //        fileName = fileName + "_" + ListOtherpaper[i].SrNo3;
                                //    }
                                //    fileName = fileName + ".doc";


                                //    var path = System.IO.Path.Combine(Docdirectory, fileName);
                                //    if (ListOtherpaper[i].DocFilePath.Contains("~"))
                                //    {
                                //        ListOtherpaper[i].DocFilePath = ListOtherpaper[i].DocFilePath.Substring(1, ListOtherpaper[i].DocFilePath.Length - 1);
                                //    }
                                //    string From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].DocFilePath);
                                //    string copyPath = From;
                                //    string pastePath = path;
                                //    if (FileExists(From))
                                //    {
                                //        System.IO.File.Copy(copyPath, pastePath, true);
                                //    }

                                //    Count = 1;
                                //}
                            }
                        }
                    }
                }

                ListOtherpaper = (List<QuestionModelCustom>)Helper.ExecuteService("LOB", "GetDraftLobLatestPapersForTransfer", DraftLob);

                if (ListOtherpaper.Count > 0)
                {
                    for (int i = 0; i < ListOtherpaper.Count; i++)
                    {
                        string[] dateArr = SessionDate.Split('/');
                        string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                        string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Documents/";

                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);

                        if (i == 0)
                        {
                            //string directory = Server.MapPath(url);
                            if (Directory.Exists(directory))
                            {
                                //Directory.Delete(directory, true);
                                //Directory.CreateDirectory(directory);
                            }
                            else
                            {
                                Directory.CreateDirectory(directory);
                            }
                        }
                        string From = "";
                        string path = "";
                        if (ListOtherpaper[i].DocFilePath != null && ListOtherpaper[i].DocFilePath != "")
                        {
                            if (ListOtherpaper[i].PaperLaidId == null || ListOtherpaper[i].PaperLaidId.ToString() == "" || ListOtherpaper[i].PaperLaidId == 0)
                            {

                                int indexof = ListOtherpaper[i].DocFilePath.LastIndexOf('/');
                                string fileName = ListOtherpaper[i].DocFilePath.Substring(indexof + 1);

                                //var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                path = System.IO.Path.Combine(directory, fileName);
                                if (ListOtherpaper[i].DocFilePath.Contains("~"))
                                {
                                    ListOtherpaper[i].DocFilePath = ListOtherpaper[i].DocFilePath.Substring(1, ListOtherpaper[i].DocFilePath.Length - 1);
                                }

                                From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].DocFilePath);
                                string copyPath = From;
                                string pastePath = path;
                                if (FileExists(From))
                                {
                                    System.IO.File.Copy(copyPath, pastePath, true);
                                }
                                else
                                {
                                    ErorLists.Append(From + ",  ");
                                }
                            }
                            if (ListOtherpaper[i].PaperLaidId != null && ListOtherpaper[i].PaperLaidId.ToString() != "" && ListOtherpaper[i].PaperLaidId != 0)
                            {
                                string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                                if (!string.IsNullOrEmpty(AssemblyIds))
                                {
                                    if (AssemblyArray.Contains(ListOtherpaper[i].PaperLaidId.ToString()))
                                    {
                                        if (ListOtherpaper[i].SrNo1 != null && ListOtherpaper[i].SrNo1.ToString().Trim() != "")
                                        {
                                            fileName = ListOtherpaper[i].SrNo1.ToString().Trim();
                                        }
                                        if (ListOtherpaper[i].SrNo2 != null && ListOtherpaper[i].SrNo2.ToString().Trim() != "")
                                        {
                                            fileName = fileName + "_" + ListOtherpaper[i].SrNo2.ToString().Trim();
                                        }
                                        if (ListOtherpaper[i].SrNo3 != null && ListOtherpaper[i].SrNo3.ToString().Trim() != "")
                                        {
                                            fileName = fileName + "_" + ListOtherpaper[i].SrNo3;
                                        }
                                        fileName = fileName + ".pdf";


                                        path = System.IO.Path.Combine(directory, fileName);
                                        if (ListOtherpaper[i].FileName.Contains("~"))
                                        {
                                            ListOtherpaper[i].FileName = ListOtherpaper[i].FileName.Substring(1, ListOtherpaper[i].FileName.Length - 1);
                                        }


                                        From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].FileName);
                                        string copyPath = From;
                                        string pastePath = path;
                                        if (FileExists(From))
                                        {
                                            System.IO.File.Copy(copyPath, pastePath, true);
                                        }
                                        else
                                        {
                                            ErorLists.Append(From + ",  ");
                                        }
                                    }
                                }
                            }

                            Count = 1;
                        }

                    }
                }

                tQuestion Question = new tQuestion();
                Question.IsFixedDate = Sessiondate;
                Question.QuestionType = 1;
                List<tQuestion> ListQuestion = new List<tQuestion>();
                List<QuestionModelCustom> ListQuestionModelCustom = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateWithPaper", Question);

                for (int i = 0; i < ListQuestionModelCustom.Count; i++)
                {
                    string[] dateArr = SessionDate.Split('/');
                    string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                    string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Starred/";
                    // string urlDoc = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Starred/DocFile";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    // string Docdirectory = System.IO.Path.Combine(FileSettings.SettingValue + urlDoc);
                    if (i == 0)
                    {
                        if (Directory.Exists(directory))
                        {
                        }
                        else
                        {
                            Directory.CreateDirectory(directory);
                        }
                        //if (Directory.Exists(Docdirectory))
                        //{
                        //}
                        //else
                        //{
                        //    Directory.CreateDirectory(Docdirectory);
                        //}
                    }
                    if (!string.IsNullOrEmpty(QuestionIds))
                    {
                        if (QuestionArray.Contains(ListQuestionModelCustom[i].QuestionNumber.ToString()))
                        {
                            if (ListQuestionModelCustom[i].FileName != null && ListQuestionModelCustom[i].FileName != "")
                            {

                                string fileExt = Path.GetExtension(ListQuestionModelCustom[i].FileName);
                                string fileName = "";
                                if (fileExt == ".pdf")
                                {
                                    fileName = ListQuestionModelCustom[i].QuestionNumber + ".pdf";
                                }

                                var path = Path.Combine(directory, fileName);
                                string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].FileName);

                                string copyPath = From;
                                string pastePath = path;
                                if (FileExists(From))
                                {
                                    System.IO.File.Copy(copyPath, pastePath, true);
                                }
                                else
                                {
                                    ErorLists.Append(From + ",  ");
                                }
                                Count = 1;
                            }
                            //if (ListQuestionModelCustom[i].DocFilePath != null && ListQuestionModelCustom[i].DocFilePath != "")
                            //{

                            //    string fileExt = Path.GetExtension(ListQuestionModelCustom[i].DocFilePath);
                            //    string fileName = "";
                            //    if (fileExt == ".doc")
                            //    {
                            //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".doc";
                            //    }
                            //    else if (fileExt == ".docx")
                            //    {
                            //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".docx";
                            //    }
                            //    var path = Path.Combine(Docdirectory, fileName);
                            //    string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].DocFilePath);
                            //    string copyPath = From;
                            //    string pastePath = path;
                            //    if (FileExists(From))
                            //    {
                            //        System.IO.File.Copy(copyPath, pastePath, true);
                            //    }

                            //    Count = 1;
                            //}
                        }
                    }
                }


                Question = new tQuestion();
                Question.IsFixedDate = Sessiondate;
                Question.QuestionType = 2;
                ListQuestion = new List<tQuestion>();
                ListQuestionModelCustom = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateWithPaper", Question);

                for (int i = 0; i < ListQuestionModelCustom.Count; i++)
                {
                    string[] dateArr = SessionDate.Split('/');
                    string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                    string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Unstarred/";
                    //string urlDoc = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Unstarred/DocFile";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    // string Docdirectory = System.IO.Path.Combine(FileSettings.SettingValue + urlDoc);
                    if (i == 0)
                    {
                        if (Directory.Exists(directory))
                        {
                        }
                        else
                        {
                            Directory.CreateDirectory(directory);
                        }
                        //if (Directory.Exists(Docdirectory))
                        //{
                        //}
                        //else
                        //{
                        //    Directory.CreateDirectory(Docdirectory);
                        //}
                    }

                    if (!string.IsNullOrEmpty(QuestionIds))
                    {
                        if (QuestionArray.Contains(ListQuestionModelCustom[i].QuestionNumber.ToString()))
                        {
                            if (ListQuestionModelCustom[i].FileName != null && ListQuestionModelCustom[i].FileName != "")
                            {

                                string fileExt = Path.GetExtension(ListQuestionModelCustom[i].FileName);
                                string fileName = "";
                                if (fileExt == ".pdf")
                                {
                                    fileName = ListQuestionModelCustom[i].QuestionNumber + ".pdf";
                                }

                                var path = Path.Combine(directory, fileName);
                                string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].FileName);
                                string copyPath = From;
                                string pastePath = path;
                                if (FileExists(From))
                                {
                                    System.IO.File.Copy(copyPath, pastePath, true);
                                }
                                else
                                {
                                    ErorLists.Append(From + ",  ");
                                }

                                Count = 1;
                            }
                            //if (ListQuestionModelCustom[i].DocFilePath != null && ListQuestionModelCustom[i].DocFilePath != "")
                            //{

                            //    string fileExt = Path.GetExtension(ListQuestionModelCustom[i].DocFilePath);
                            //    string fileName = "";

                            //    if (fileExt == ".doc")
                            //    {
                            //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".doc";
                            //    }
                            //    else if (fileExt == ".docx")
                            //    {
                            //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".docx";
                            //    }
                            //    var path = Path.Combine(Docdirectory, fileName);
                            //    string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].DocFilePath);

                            //    string copyPath = From;
                            //    string pastePath = path;
                            //    if (FileExists(From))
                            //    {
                            //        System.IO.File.Copy(copyPath, pastePath, true);
                            //    }

                            //    Count = 1;
                            //}
                        }
                    }
                }


                if (Count == 0)
                {
                    ErorLists.Append("No File Available!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
                else if (!string.IsNullOrEmpty(ErorLists.ToString()))
                {
                    ErorLists.Append("!! Files are not Transferred !!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ErorLists.Append("Selected Files are Transferred!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("File Download Error!", JsonRequestBehavior.AllowGet);
            }

            //return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadFileForAssemblyHouse()
        {
            return View();
        }
        public bool FileExists(string path)
        {
            string curFile = @"" + path; ;
            bool valu = System.IO.File.Exists(curFile) ? true : false;
            return valu;
        }

        public ActionResult GetSessionListbyAssembly(int AssemblyId, int SessionId)
        {
            LegislationFixationModel model = new LegislationFixationModel();
            mSession mSession = new mSession();
            //mSession.SessionCode = siteSettingMod.SessionCode;            
            mSession.AssemblyID = AssemblyId;
            mSession.SessionCode = SessionId;
            
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {
                
                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
            }
            model.SessionId = SessionId;
            model.AssemblyId = AssemblyId;

            model.PageNumber = Convert.ToInt32("1");
            model.RowsPerPage = Convert.ToInt32("10");
            model.selectedPage = Convert.ToInt32("1");
            model.loopStart = Convert.ToInt32("1");
            model.loopEnd = Convert.ToInt32("5");
            return Json(model, JsonRequestBehavior.AllowGet);
            //return PartialView(model);
        }



        public ActionResult GetSessionListForDownload(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            mSessionDate sessionDates = new mSessionDate();
            LegislationFixationModel model = new LegislationFixationModel();

            model.mAssemblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

            SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
            data.AssemblyID = model.mAssemblyList.FirstOrDefault().AssemblyCode;

            model.sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
            var a = model.sessionList.FirstOrDefault().SessionCode;

            mSession mSession = new mSession();
            if (siteSettingMod != null)
            {

                mSession.SessionCode = siteSettingMod.SessionCode;
                mSession.AssemblyID = siteSettingMod.AssemblyCode;
                model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
                if (model.SessionDateList.Count > 0)
                {
                    foreach (var session in model.SessionDateList)
                    {
                        LegislationFixationModel fixModel = new LegislationFixationModel();
                        fixModel.Id = session.Id;
                        fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                        model.CustomSessionDateList.Add(fixModel);
                    }
                }
                model.SessionId = siteSettingMod.SessionCode;
                model.AssemblyId = siteSettingMod.AssemblyCode;
            }

            if (PageNumber != null && PageNumber != "")
            {
                model.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                model.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                model.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                model.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                model.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                model.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                model.loopEnd = Convert.ToInt32("5");
            }

            return PartialView("GetSessionListForDownload", model);
        }

        public ActionResult GenerateXMLQuestions(string sessionDate, string QuestionType)
        {

            string test = CreateQuesXML(sessionDate, QuestionType);

            if (QuestionType == "1")
            {
                return File(test, "application/xml", "QuestingSFile.xml");
            }
            else
            {
                return File(test, "application/xml", "QuestingUSFile.xml");
            }
        }
        public string CreateQuesXML(string sessionDate, string QuestionType)
        {

            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", sessionDate));
            methodParameter.Add(new KeyValuePair<string, string>("@QuesType", QuestionType));

            DataSet dataset = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectQuestionBySessionDate]", methodParameter);

            if (dataset != null)
            {

                XElement LOB = new System.Xml.Linq.XElement("LOB");
                XElement pages = new System.Xml.Linq.XElement("pages");
                LOB.Add(pages);
                //XElement header = new XElement("header-page");
                ////header.Attribute("id").Value="header_page";
                //header.SetAttributeValue("id", "header_page");
                //pages.Add(header);
                //XElement content = new XElement("content");
                //header.Add(content);

                //                content.ReplaceNodes(new XCData(@"<h2 style='text-align: center;font-family:DVOT-Yogesh;'>" + Convert.ToString(dataset.Tables[0].Rows[0]["AssemblyNameLocal"]) + @"</h2>
                //                    <h4 style='text-align: center;font-family:DVOT-Yogesh;'>????? - ????</h4>
                //                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionNameLocal"]) + @"  </h4>
                //                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionDateLocal"]) + @"  " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionTimeLocal"]) + @" ??? ????</h4>"));




                //page
                int idCount = 2;
#pragma warning disable CS0219 // The variable 'SrNo1' is assigned but its value is never used
                string SrNo1 = "";
#pragma warning restore CS0219 // The variable 'SrNo1' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo2' is assigned but its value is never used
                string SrNo2 = "";
#pragma warning restore CS0219 // The variable 'SrNo2' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo3' is assigned but its value is never used
                string SrNo3 = "";
#pragma warning restore CS0219 // The variable 'SrNo3' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo1Count' is assigned but its value is never used
                int SrNo1Count = 1;
#pragma warning restore CS0219 // The variable 'SrNo1Count' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo2Count' is assigned but its value is never used
                int SrNo2Count = 1;
#pragma warning restore CS0219 // The variable 'SrNo2Count' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo3Count' is assigned but its value is never used
                int SrNo3Count = 1;
#pragma warning restore CS0219 // The variable 'SrNo3Count' is assigned but its value is never used


#pragma warning disable CS0219 // The variable 'outXml' is assigned but its value is never used
                string outXml = "";
#pragma warning restore CS0219 // The variable 'outXml' is assigned but its value is never used
                DateTime SessionDate = new DateTime();
                if (dataset.Tables[0].Rows.Count > 0)
                {
                    SessionDate = Convert.ToDateTime(dataset.Tables[0].Rows[0]["IsFixedDate"]);
                }
                string date = "";

                if (SessionDate.ToString("dd/MM/yyyy") == "01/01/0001")
                {
                    string SDate = Convert.ToString(sessionDate);
                    date = SDate;
                }
                else
                {
                    date = SessionDate.ToString("dd/MM/yyyy");
                }
                date = date.Replace("/", "-");
                XElement page1 = new XElement("page");
                int IndexCount = 0;
                XElement index = new XElement("index-page");
                index.SetAttributeValue("id", "index_page");
                index.SetAttributeValue("Date", date);

                pages.Add(index);
                XElement indexContent = new XElement("content");
                XElement indexOne = new XElement("index");

                StringBuilder sb = new StringBuilder();



                //if (dataset.Tables[0].Rows.Count > 0)
                //{
                //    string sessiondate = null;
                //}
                //else
                //{
                //    string sessiondate = sessionDate;  
                //}

                for (int count = 0; count < dataset.Tables[0].Rows.Count; count++)
                {


                    sessionDate = Convert.ToString(dataset.Tables[0].Rows[0]["IsFixedDate"]);

                    string IsHindi = "False";

                    if (Convert.ToString(dataset.Tables[0].Rows[count]["IsHindi"]) == "True")
                    {
                        IsHindi = "True";

                    }
                    else
                    {
                        IsHindi = "False";
                    }


                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@DeptId", Convert.ToString(dataset.Tables[0].Rows[count]["DepartmentID"])));
                    methodParameter.Add(new KeyValuePair<string, string>("@MinistryId", Convert.ToString(dataset.Tables[0].Rows[count]["MinistryId"])));
                    DataSet datasetDept = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDepartmentInfoById]", methodParameter);
                    string DepartmentName = "";
                    string DepartmentNameLocal = "";
                    string MetaDepartmentId = "";
                    string MetaMinistryId = "";
                    if (datasetDept != null)
                    {
                        if (datasetDept.Tables[0].Rows[0] != null)
                        {
                            DepartmentName = Convert.ToString(datasetDept.Tables[0].Rows[0]["deptname"]).Trim();
                            DepartmentNameLocal = Convert.ToString(datasetDept.Tables[0].Rows[0]["deptnameLocal"]).Trim();
                            MetaDepartmentId = Convert.ToString(datasetDept.Tables[0].Rows[0]["MinistryDepartmentsID"]).Trim();
                            MetaMinistryId = Convert.ToString(datasetDept.Tables[0].Rows[0]["MinistryID"]).Trim();
                        }
                    }



                    string QuesNo = Convert.ToString(dataset.Tables[0].Rows[count]["QuestionNumber"]);
                    string Subject = Convert.ToString(dataset.Tables[0].Rows[count]["Subject"]);

                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@QuestionType", Convert.ToString(dataset.Tables[0].Rows[0]["QuestionType"])));
                    DataSet datasetQuestion = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectQuestionTypeById]", methodParameter);
                    string QuesType = "";
                    if (datasetQuestion != null)
                    {
                        if (datasetQuestion.Tables[0].Rows[0] != null)
                        {
                            QuesType = Convert.ToString(datasetQuestion.Tables[0].Rows[0]["QuestionTypeName"]);
                        }
                    }

                    string memberName = "";
                    string memberNameLocal = "";
                    string memberCode = "";
                    if (Convert.ToString(dataset.Tables[0].Rows[count]["IsClubbed"]) != "")
                    {
                        if (Convert.ToBoolean(dataset.Tables[0].Rows[count]["IsClubbed"]))
                        {
                            memberCode = dataset.Tables[0].Rows[count]["ReferenceMemberCode"].ToString();
                            string[] members = Convert.ToString(dataset.Tables[0].Rows[count]["ReferenceMemberCode"]).Split(',');
                            members = members.Distinct().ToArray();
                            for (int j = 0; j < members.Count(); j++)
                            {
                                if (j == members.Count() - 1)
                                {
                                    methodParameter = new List<KeyValuePair<string, string>>();
                                    methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", members[j]));
                                    DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);

                                    if (datasetMember != null)
                                    {
                                        if (datasetMember.Tables[0].Rows[0] != null)
                                        {
                                            string Prefix = Convert.ToString(datasetMember.Tables[0].Rows[0]["Prefix"]).Trim();
                                            if (Prefix.Contains("Sh."))
                                            {
                                                Prefix = Prefix.Replace("Sh.", "Shri");
                                            }
                                            memberName = memberName + Prefix + " " + Convert.ToString(datasetMember.Tables[0].Rows[0]["Name"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ")";
                                            memberNameLocal = memberNameLocal + Convert.ToString(datasetMember.Tables[0].Rows[0]["NameLocal"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName_Local"]) + ")";
                                        }
                                    }
                                }
                                else
                                {
                                    methodParameter = new List<KeyValuePair<string, string>>();
                                    methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", members[j]));
                                    DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);

                                    if (datasetMember != null)
                                    {
                                        if (datasetMember.Tables[0].Rows[0] != null)
                                        {
                                            string Prefix = Convert.ToString(datasetMember.Tables[0].Rows[0]["Prefix"]).Trim();
                                            if (Prefix.Contains("Sh."))
                                            {
                                                Prefix = Prefix.Replace("Sh.", "Shri");
                                            }
                                            memberName = memberName + Prefix + " " + Convert.ToString(datasetMember.Tables[0].Rows[0]["Name"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ") ,";
                                            memberNameLocal = memberNameLocal + Convert.ToString(datasetMember.Tables[0].Rows[0]["NameLocal"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName_Local"]) + ") ,";
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            methodParameter = new List<KeyValuePair<string, string>>();
                            methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", Convert.ToString(dataset.Tables[0].Rows[count]["MemberID"])));
                            DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);

                            if (datasetMember != null)
                            {
                                if (datasetMember.Tables[0].Rows[0] != null)
                                {
                                    string Prefix = Convert.ToString(datasetMember.Tables[0].Rows[0]["Prefix"]).Trim();
                                    if (Prefix.Contains("Sh."))
                                    {
                                        Prefix = Prefix.Replace("Sh.", "Shri");
                                    }
                                    memberCode = datasetMember.Tables[0].Rows[0]["MemberCode"].ToString();
                                    memberName = Prefix + " " + Convert.ToString(datasetMember.Tables[0].Rows[0]["Name"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ")";
                                    memberNameLocal = Convert.ToString(datasetMember.Tables[0].Rows[0]["NameLocal"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName_Local"]) + ")";
                                }
                            }
                        }
                    }
                    else
                    {

                        methodParameter = new List<KeyValuePair<string, string>>();
                        methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", Convert.ToString(dataset.Tables[0].Rows[count]["MemberID"])));
                        DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);

                        if (datasetMember != null)
                        {
                            if (datasetMember.Tables[0].Rows[0] != null)
                            {
                                string Prefix = Convert.ToString(datasetMember.Tables[0].Rows[0]["Prefix"]).Trim();
                                if (Prefix.Contains("Sh."))
                                {
                                    Prefix = Prefix.Replace("Sh.", "Shri");
                                }
                                memberCode = datasetMember.Tables[0].Rows[0]["MemberCode"].ToString();
                                memberName = Prefix + " " + Convert.ToString(datasetMember.Tables[0].Rows[0]["Name"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ")";
                                memberNameLocal = Convert.ToString(datasetMember.Tables[0].Rows[0]["NameLocal"]) + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName_Local"]) + ")";
                            }
                        }

                    }

                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@MinistryID", Convert.ToString(dataset.Tables[0].Rows[count]["MinistryId"])));
                    DataSet datasetMember1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMinistryInfoByMinistryID]", methodParameter);
                    string MinisterName = "";
                    string MinisterNameLocal = "";
                    string MinistersName = "";
                    string MinistersCode = "";

                    mMinistry Model = new mMinistry();
                    Model.MinistryID = Convert.ToInt16(dataset.Tables[0].Rows[count]["MinistryId"]);
                    List<mMinsitryMinister> result = (List<mMinsitryMinister>)Helper.ExecuteService("MinistryMinister", "GetMinisterInfoByMinistryId", Model);
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (i == 0)
                        {
                            MinistersName = result[0].MinisterName;
                            MinistersCode = result[0].MemberCode.ToString();
                            methodParameter = new List<KeyValuePair<string, string>>();
                            methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", Convert.ToString(result[0].MemberCode)));
                            DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);
                            if (datasetMember != null)
                            {
                                if (datasetMember.Tables[0].Rows[0] != null)
                                {
                                    MinistersName = MinistersName + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ")";

                                }
                            }
                        }
                        else
                        {
                            MinistersName = MinistersName + ", " + result[i].MinisterName;
                            MinistersCode = MinistersCode + ", " + result[i].MemberCode.ToString();
                            methodParameter = new List<KeyValuePair<string, string>>();
                            methodParameter.Add(new KeyValuePair<string, string>("@MemberCode", Convert.ToString(result[0].MemberCode)));
                            DataSet datasetMember = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectMemberInfoByMemberCode]", methodParameter);
                            if (datasetMember != null)
                            {
                                if (datasetMember.Tables[0].Rows[0] != null)
                                {
                                    MinistersName = MinistersName + "(" + Convert.ToString(datasetMember.Tables[0].Rows[0]["ConstituencyName"]) + ")";
                                }
                            }
                        }
                    }
                    if (datasetMember1 != null)
                    {
                        if (datasetMember1.Tables[0].Rows[0] != null)
                        {
                            MinisterName = Convert.ToString(datasetMember1.Tables[0].Rows[0]["MinistryName"]);
                            MinisterNameLocal = Convert.ToString(datasetMember1.Tables[0].Rows[0]["MinistryNameLocal"]);
                        }
                    }


                    if (count == 0)
                    {
                        page1.SetAttributeValue("id", 2);
                        page1.SetAttributeValue("page-des", "first-page");
                        page1.SetAttributeValue("Date", date);

                    }
                    else
                    {
                        pages.Add(page1);
                        idCount++;
                        page1 = new XElement("page");
                        page1.SetAttributeValue("id", idCount);
                        page1.SetAttributeValue("Date", date);
                    }

                    IndexCount = IndexCount + 1;
                    if (IndexCount == 11)
                    {
                        index.Add(indexOne);
                        IndexCount = 0;
                        index = new XElement("index-page");
                        index.SetAttributeValue("id", "index_page");
                        index.SetAttributeValue("Date", date);

                        pages.Add(index);
                        indexContent = new XElement("content");
                        indexOne = new XElement("index");
                    }
                    ///For Indexing
                    ///
                    if (IsHindi == "True")
                    {
                        // Subject = Subject;
                    }
                    else
                    {
                        Subject = Subject.ToUpper();
                    }

                    /// remove unwanted tag from main Question
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, "&lt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&amp;&zwd;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, "p&gt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, "nbsp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, "lt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, "/p&gt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, "&nbsp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, "&", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);




                    memberNameLocal = memberNameLocal.Replace("\r\n", "");

                    string ind = QuesNo + "(" + Subject + ")";
                    sb.Append("<li>" + ind + "</li>");
                    XElement subIndex1 = new XElement("index");
                    subIndex1.SetAttributeValue("id", idCount);
                    ind = StripHTML(ind);
                    ind = System.Text.RegularExpressions.Regex.Replace(ind, "\r\r", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    subIndex1.SetAttributeValue("description", ind);
                    subIndex1.SetAttributeValue("MemberCodes", memberCode.Trim());
                    subIndex1.SetAttributeValue("Member", memberName.ToUpper());
                    subIndex1.SetAttributeValue("MemberLocal", memberNameLocal);
                    subIndex1.SetAttributeValue("MinisterCode", MinistersCode.ToUpper());
                    subIndex1.SetAttributeValue("MinisterConcerned", MinistersName.ToUpper());
                    subIndex1.SetAttributeValue("Meta_DepartmentId", MetaDepartmentId);
                    subIndex1.SetAttributeValue("Meta_MinistryId", MetaMinistryId);
                    subIndex1.SetAttributeValue("IsHindi", IsHindi);

                    subIndex1.SetAttributeValue("command", "test_command");
                    subIndex1.SetAttributeValue("enabled", "true");
                    subIndex1.ReplaceNodes(new XCData(@"<p style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'>" + ind + "</P>"));
                    indexOne.Add(subIndex1);


                    XElement contentOne = new XElement("content");
                    contentOne.SetAttributeValue("Department", DepartmentName);
                    contentOne.SetAttributeValue("Meta_DepartmentId", MetaDepartmentId);
                    contentOne.SetAttributeValue("Meta_MinistryId", MetaMinistryId);
                    contentOne.SetAttributeValue("DepartmentLocal", DepartmentNameLocal);
                    contentOne.SetAttributeValue("QNo", QuesNo);
                    contentOne.SetAttributeValue("Type", QuesType);
                    contentOne.SetAttributeValue("LayingDate", date);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"<p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"</p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);


                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, "&amp;", "$M", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&nbsp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"\r\n", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&amp;&zwd;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&amp;&zwj;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"nbsp;", "T", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"$MT", "&nbsp;", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"$M", "&amp;", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"T", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&T", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Subject = System.Text.RegularExpressions.Regex.Replace(Subject, @"&", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);


                    contentOne.SetAttributeValue("Subject", Subject);

                    contentOne.SetAttributeValue("MemberCodes", memberCode.Trim());
                    contentOne.SetAttributeValue("Member", memberName.ToUpper());
                    contentOne.SetAttributeValue("MemberLocal", memberNameLocal);
                    contentOne.SetAttributeValue("Minister", MinisterName.ToUpper());
                    contentOne.SetAttributeValue("MinisterLocal", MinisterNameLocal);
                    contentOne.SetAttributeValue("MinisterCode", MinistersCode.ToUpper());
                    contentOne.SetAttributeValue("MinisterConcerned", MinistersName.ToUpper());
                    contentOne.SetAttributeValue("Meta_DepartmentId", MetaDepartmentId);
                    contentOne.SetAttributeValue("Meta_MinistryId", MetaMinistryId);
                    contentOne.SetAttributeValue("IsHindi", IsHindi);
                    page1.Add(contentOne);


                    String Questions = Convert.ToString(dataset.Tables[0].Rows[count]["MainQuestion"]);


                    ////  remove unwanted tag from main Question
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<( )*tr([^>])*>", "<p>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<( )*p([^>])*>", "<p>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<( )*span([^>])*>", "<span>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<span>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"</span>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p>&lt;br/&gt;</p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p>&nbsp;</p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p><br/></p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p>", "<p style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'><span style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"</p>", "</span></p>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&nbsp;", " &nbsp; ", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&zwj;", string.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"zwj;", string.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&#xD;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&#xA;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&#xD;&#xA;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&lt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"p&gt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&amp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&amp;&zwd;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&amp;&zwj;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"nbsp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"lt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"/p&gt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&nbsp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    // Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"  nbsp", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"nbsp", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'><span style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'>nbsp</span></p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'><span style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'>  nbsp</span></p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"<p style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'><span style='font-family:DVOT-Yogesh;text-align:left;font-size:22px;'></span></p>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"#xD;&#xA;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"xD;&#xA;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&#", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    Questions = System.Text.RegularExpressions.Regex.Replace(Questions, @"&", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //    @"&#xD;", "<p>" &&;,
                    //   System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    //Questions = System.Text.RegularExpressions.Regex.Replace(Questions,
                    //         @"<( )*p([^>])*>", "<p>",
                    //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);


                    // if (Questions != "")
                    //{
                    //    Questions = Questions.Replace("<p>", "");
                    //    Questions = Questions.Replace("</p>", "");
                    //}

                    //string[] subqestion = Regex.Split(Questions, "^^");
                    string[] subqestion = Questions.Split(new string[] { "^^" }, StringSplitOptions.None);
                    for (int j = 0; j < subqestion.Count(); j++)
                    {
                        if (j == 0)
                        {
                            XElement Question = new XElement("Question");
                            var conten = subqestion[j];
                            if (subqestion.Count() > 1)
                            {
                                conten = conten + "</p>";
                            }
                            Question.ReplaceNodes(new XCData(conten));
                            page1.Add(Question);
                        }
                        else
                        {
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            page1.SetAttributeValue("id", idCount);
                            page1.SetAttributeValue("Date", date);
                            var conten = subqestion[j];
                            if (j == subqestion.Count() - 1)
                            {
                                conten = "<p>" + conten;
                            }
                            else
                            {
                                conten = "<p>" + conten + "</p>";
                            }

                            XElement Question = new XElement("Question");
                            Question.ReplaceNodes(new XCData(conten));
                            page1.Add(Question);

                        }
                    }



                    //methodParameter = new List<KeyValuePair<string, string>>();
                    //methodParameter.Add(new KeyValuePair<string, string>("@QuestionNo", Convert.ToString(dataset.Tables[0].Rows[count]["QuestionNumber"])));
                    //DataSet datasetSubQuestions = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSubQuestionByQuestionNo]", methodParameter);


                    //for (int i = 0; i < datasetSubQuestions.Tables[0].Rows.Count; i++)
                    //{
                    //    XElement Question = new XElement("Question");
                    //    Question.ReplaceNodes(new XCData(Convert.ToString(datasetSubQuestions.Tables[0].Rows[i]["SubQuestion"])));
                    //    page1.Add(Question);
                    //    XElement Answer = new XElement("Answer");
                    //    Answer.ReplaceNodes(new XCData(Convert.ToString(datasetSubQuestions.Tables[0].Rows[i]["SubAnswer"])));
                    //    page1.Add(Answer);
                    //    ////for page break
                    //    //if (Convert.ToString(dataset.Tables[0].Rows[count]["PageBreak"]) == "True")
                    //    //{
                    //    //    //page1.Add(business);
                    //    //    pages.Add(page1);
                    //    //    idCount++;
                    //    //    page1 = new XElement("page");
                    //    //    //business = new XElement("business");
                    //    //    page1.SetAttributeValue("id", idCount);
                    //    //}

                    //}

                    //if (dataset.Tables[0].Rows[count]["AnswerAttachLocation"] != null && Convert.ToString(dataset.Tables[0].Rows[count]["AnswerAttachLocation"]) != "")
                    //{
                    string name = Convert.ToString(dataset.Tables[0].Rows[count]["AnswerAttachLocation"]);
                    int index1 = name.LastIndexOf('/');
                    name = name.Substring(index1 + 1);
                    XElement subactions = new XElement("actions");
                    page1.Add(subactions);
                    XElement subactionOne = new XElement("action");
                    subactionOne.SetAttributeValue("id", "PDF");
                    if (QuestionType == "2")
                    {
                        subactionOne.SetAttributeValue("command", "OPEN_PDF_UQ");
                    }
                    else if (QuestionType == "1")
                    {
                        subactionOne.SetAttributeValue("command", "OPEN_PDF_QA");
                    }
                    subactionOne.SetAttributeValue("commandvalue", QuesNo + ".pdf");
                    subactionOne.SetAttributeValue("image", "QAndA.png");
                    subactionOne.SetAttributeValue("enabled", "true");
                    subactions.Add(subactionOne);
                    //}

                    //string  LOBText = "";
                    //  XElement contentOne = new XElement("content");
                    //  contentOne.SetAttributeValue("id", SrNo1);
                    //  contentOne.SetAttributeValue("level", "1");
                    //  contentOne.SetAttributeValue("Set", SrNo1);
                    //  contentOne.SetAttributeValue("value", LOBText);
                    //  page1.Add(contentOne);

                    //  XElement subactions = new XElement("actions");
                    //  page1.Add(subactions);
                    //  XElement subactionOne = new XElement("action");
                    //  subactionOne.SetAttributeValue("id", "PDF");
                    //  subactionOne.SetAttributeValue("command", "OPEN_PDF");
                    //  subactionOne.SetAttributeValue("commandvalue", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                    //  subactionOne.SetAttributeValue("image", "place.png");
                    //  subactionOne.SetAttributeValue("enabled", "true");
                    //subactions.Add(subactionOne);

                    //if (count == dataset.Tables[0].Rows.Count - 1)
                    //{
                    //    pages.Add(page1);
                    //}

                    if (count == dataset.Tables[0].Rows.Count - 1)
                    {


                        //pages.Add(page1);
                        ///Last page
                        ///
                        pages.Add(page1);
                        idCount++;
                        page1 = new XElement("page");
                        page1.SetAttributeValue("id", idCount);
                        page1.SetAttributeValue("Date", date);
                        XElement content = new XElement("content");
                        content.SetAttributeValue("Department", "");
                        content.SetAttributeValue("Meta_DepartmentId", "");
                        content.SetAttributeValue("Meta_MinistryId", "");
                        content.SetAttributeValue("DepartmentLocal", "");
                        content.SetAttributeValue("QNo", "");
                        content.SetAttributeValue("Type", "");
                        content.SetAttributeValue("LayingDate", "");

                        content.SetAttributeValue("Subject", "");


                        content.SetAttributeValue("Member", "");
                        content.SetAttributeValue("MemberLocal", "");
                        content.SetAttributeValue("Minister", "");
                        content.SetAttributeValue("MinisterLocal", "");
                        content.SetAttributeValue("MinisterCode", "");
                        content.SetAttributeValue("MinisterConcerned", "");
                        content.SetAttributeValue("IsHindi", "False");


                        XElement Question = new XElement("Question");

                        page1.Add(content);
                        page1.Add(Question);
                        pages.Add(page1);

                    }

                    //idCount++;
                    //page1 = new XElement("page");
                    //// business = new XElement("business");
                    //page1.SetAttributeValue("id", idCount);


                }
                indexContent.ReplaceNodes(new XCData("<h5><b>Today's list of Questions:</b></h5><ol>" + sb + "<ol>"));
                index.Add(indexContent);
                index.Add(indexOne);

                ///Save XML File
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(LOB.ToString());



                methodParameter = new List<KeyValuePair<string, string>>();

                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string CurrentSession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        CurrentSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }
                string fileName = "";

                //sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                date = date.Replace('-', ' ');


                if (QuestionType == "1")
                {
                    fileName = "QuestingSFile.xml";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + date + "/";
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    // string directory = Server.MapPath(url);
                    if (!Directory.Exists(directory))
                    {

                        Directory.CreateDirectory(directory);
                    }

                    string path = System.IO.Path.Combine(directory, fileName);
                    //  file.SaveAs(path);
                    xml.Save(path);
                    return path;
                }
                else
                {
                    fileName = "QuestingUSFile.xml";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + date + "/";
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);

                    // string directory = Server.MapPath(url);
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = System.IO.Path.Combine(directory, fileName);
                    //  file.SaveAs(path);
                    xml.Save(path);
                    return path;
                    //xml.Save("C:/eVidhan/LOB/LOBFile.xml");
                }


                //if (QuestionType == "1")
                //{
                //    xml.Save("C:/eVidhan/LOB/QuestingSFile.xml");
                //}
                //else if (QuestionType == "2")
                //{
                //    xml.Save("C:/eVidhan/LOB/QuestingUnFile.xml");
                //}
#pragma warning disable CS0162 // Unreachable code detected
                return "";
#pragma warning restore CS0162 // Unreachable code detected
            }
            return "";
        }

        public object DownloadingDocFileForAssemblyHouse(string SessionDate, string AssemblyId, string SessionId)
        {
            DateTime Sessiondate = new DateTime();
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageCulture == "hi-IN")
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                Sessiondate = Convert.ToDateTime(Date);
            }
            else
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Sessiondate = Convert.ToDateTime(Date);
            }

            try
            {

                string[] dateArr = SessionDate.Split('/');
                string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "_Doc";
                string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                // string directory = Server.MapPath(url);


                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    //zip.AddDirectoryByName("Files");


                    if (Directory.Exists(directory))
                    {
                        zip.AddDirectory(directory, "LOBDoc");
                    }
                    else
                    {

                    }



                    Response.Clear();
                    Response.BufferOutput = false;
                    string zipName = String.Format("{0}.zip", "LOBDoc_" + SessionDate);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    Response.End();

                    return Response;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return "No File Available!";
            }

#pragma warning disable CS0162 // Unreachable code detected
            return Response;
#pragma warning restore CS0162 // Unreachable code detected
        }
        public object DownloadingFileForAssemblyHouse(string SessionDate, string AssemblyId, string SessionId)
        {
            DateTime Sessiondate = new DateTime();
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageCulture == "hi-IN")
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                Sessiondate = Convert.ToDateTime(Date);
            }
            else
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Sessiondate = Convert.ToDateTime(Date);
            }

            try
            {

                string[] dateArr = SessionDate.Split('/');
                string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate;
                string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                // string directory = Server.MapPath(url);


                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    //zip.AddDirectoryByName("Files");


                    if (Directory.Exists(directory))
                    {
                        zip.AddDirectory(directory, "LOB");
                    }
                    else
                    {

                    }



                    Response.Clear();
                    Response.BufferOutput = false;
                    string zipName = String.Format("{0}.zip", "LOB_" + SessionDate);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    Response.End();

                    return Response;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return "No File Available!";
            }

#pragma warning disable CS0162 // Unreachable code detected
            return Response;
#pragma warning restore CS0162 // Unreachable code detected
        }
        private string StripHTML(string source)
        {
            try
            {
                string result;

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = source.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove all others. More can be added, see

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // for testing&#xD;
                //result = System.Text.RegularExpressions.Regex.Replace(result, @"&#xD;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result, @"&lt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result, @"p&gt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result, @"nbsp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                ////result = System.Text.RegularExpressions.Regex.Replace(result, @"lt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result, @"/p&gt;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result, @"&nbsp;", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"&", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //System.Text.RegularExpressions.Regex.Replace(result,
                //       this.txtRegex.Text,string.Empty,
                //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // make line breaking consistent
                result = result.Replace("\n", "\r");

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4.
                // Prepare first to remove any whitespaces in between
                // the escaped characters and remove redundant tabs in between line breaks
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove multiple tabs following a line break with just one tab
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Initial replacement target string for line breaks
                string breaks = "\r\r\r";
                // Initial replacement target string for tabs
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }

                // That's it.
                return result;
            }
            catch
            {

                return source;
            }
        }

        public string UpdatePaperLaid(string hdnLobIDList, string hdnQIDList)
        {
            tPaperLaidV Question = new tPaperLaidV();
            AdminLOB Adminlobs = new AdminLOB();

            if (hdnLobIDList != "" && hdnLobIDList != null)
            {
                hdnLobIDList = hdnLobIDList.TrimEnd(',');
                string[] idsarr = hdnLobIDList.Split(',');
                foreach (var ids in idsarr)
                {
                    Adminlobs.Id = Convert.ToInt32(ids);

                    Helper.ExecuteService("LOB", "UpdatePaperLaid", Adminlobs);
                }
            }
            if (hdnQIDList != "" && hdnQIDList != null)
            {
                hdnQIDList = hdnQIDList.TrimEnd(',');
                string[] idsarr = hdnQIDList.Split(',');
                foreach (var ids in idsarr)
                {
                    Question.PaperLaidId = Convert.ToInt32(ids);
                    Helper.ExecuteService("Questions", "UpdatePaperLaid", Question);
                }
            }

            return "";
        }
        public ActionResult PapersLaidInHouse(string SessionDate, int AssemblyId, int SessionId)
        {

            SessionDate = Sanitizer.GetSafeHtmlFragment(SessionDate);
            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", provider);
            DateTime sessDate = Date;
            var html = GetPaperLaidHtml(sessDate, SessionId, AssemblyId);
            //var model = new PapersLaidModel
            //{
            //    Heading = SBL.eVidhan.Public.Web.Global.AssemblyQuestions,
            //    outhtml = html
            //};
            AdminLOB lobs = new AdminLOB();
            lobs.TextLOB = html;
            ViewData["SessionDate"] = sessDate.ToString("D") + " Papers Laid in House";// String.Format("{dd,MMMM, yyyy}", sessDate);

            return PartialView("PaperLaidPrtial", lobs);
        }
        public string GetPaperLaidHtml(DateTime Date, int SesCode, int AssCode)
        {
            int ival = 1;
            int jval = 1;
#pragma warning disable CS0219 // The variable 'kval' is assigned but its value is never used
            int kval = 1;
#pragma warning restore CS0219 // The variable 'kval' is assigned but its value is never used
            int QuestionType = 1;
            tQuestion Question = new tQuestion();
            AdminLOB Adminlobs = new AdminLOB();
            DateTime sessDate = Convert.ToDateTime(Date);
            Question.IsFixedDate = sessDate;
            Question.QuestionType = QuestionType;
            List<QuestionModelCustom> ListStartedQuestion = new List<QuestionModelCustom>();
            List<QuestionModelCustom> ListUnStartedQuestion = new List<QuestionModelCustom>();
            List<AdminLOB> ListAdminLOB = new List<AdminLOB>();
            ListStartedQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetDraftAndApprovedQuestionBySessionDateAndLaidType", Question);
            Question.QuestionType = 2;
            ListUnStartedQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetDraftAndApprovedQuestionBySessionDateAndLaidType", Question);

            Adminlobs.SessionDate = sessDate;
            Adminlobs.SessionId = SesCode;
            Adminlobs.AssemblyId = AssCode;
            ListAdminLOB = (List<AdminLOB>)Helper.ExecuteService("LOB", "GetPapersLaid", Adminlobs);
            //string sessiondate = null;



            //List<tQuestion> ListQuestion = new List<tQuestion>();
            //List<QuestionModelCustom> ListQuestionModelCustom = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateWithPaper", Question);

            string outXml = "";
            string srrial = "";
            outXml = "<table>";
            outXml += @"<tr><td style='font-weight:bold;'>Serial No.</td><td style='width='112px;font-weight:bold;'>Type</td><td style='font-weight:bold;'>Subject</td><td style='width=112px;font-weight:bold;'>File</td><td><input  type='checkbox' onclick='SelectAllChb(this);'  class='checkedAll'/></td></tr>";
            for (int i = 0; i < ListAdminLOB.Count; i++)
            {
                string sr1 = ListAdminLOB[i].SrNo1.ToString();
                string sr2 = ListAdminLOB[i].SrNo2.ToString();
                string sr3 = ListAdminLOB[i].SrNo3.ToString();
                if (sr1.Length != 0 && sr2.Length != 0 && sr3.Length != 0)
                {
                    srrial = sr1 + "." + sr2 + "." + sr3;
                }
                if (sr1.Length != 0 && sr2.Length != 0 && sr3.Length == 0)
                {
                    srrial = sr1 + "." + sr2;
                }
                if (sr1.Length != 0 && sr2.Length == 0 && sr3.Length == 0)
                {
                    srrial = sr1;
                }

                if ((ListAdminLOB[i].ConcernedEventId == 3) && (ListAdminLOB[i].TextLOB.Trim().IndexOf("तारांकित") != -1 && ListAdminLOB[i].TextLOB.Trim().IndexOf("अतारांकित") == -1))
                {
                    ival = 1;
                    outXml += @"<tr><td>" + srrial + "</td><td></td><td>" + ListAdminLOB[i].TextLOB + "</td><td></td><td></td></tr>";
                    for (int j = 0; j < ListStartedQuestion.Count; j++)
                    {
                        //string ppath = ListStartedQuestion[j].FileName;
                        //var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                        //string FileAccessSettings = fileAcessingSettings.SettingValue;
                        // FileAccessSettings = FileAccessSettings + ppath;
                        // Boolean isExist = true;
                        //isExist = checkFileLocation(FileAccessSettings);

                        string sub = ListStartedQuestion[j].Subject.Replace("<p>", " ");
                        sub = sub.Replace("</p>", " ");
                        if (ListStartedQuestion[j].IsLaid == true && ListStartedQuestion[j].FileName != null)
                        {
                            // var actinlink = Url.Action("DisplayQuestionPDF", "LOBFile", new { id = ListStartedQuestion[j].QuestionID });
                            string actinlink = SquestionFromLaidPDF(ListStartedQuestion[j].QuestionNumber.ToString(), sessDate.ToShortDateString(), SesCode, AssCode);
                            outXml += @"<tr><td>" + srrial + "." + ival + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td width='76px'><a href='" + actinlink + "' target='_blank' >Paper Laid</a></td><td><input class='addclassQ'  type='checkbox' value='" + ListStartedQuestion[j].PaperLaidId + "' /></td></tr>";
                        }
                        else if ((ListStartedQuestion[j].IsLaid == false || ListStartedQuestion[j].IsLaid == null) && ListStartedQuestion[j].FileName != null)
                        {
                            outXml += @"<tr><td>" + srrial + "." + ival + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td>Not Laid</td><td><input type='checkbox' class='addclassQ' value='" + ListStartedQuestion[j].PaperLaidId + "' /></td></tr>";
                        }
                        else
                        {
                            outXml += @"<tr><td>" + srrial + "." + ival + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td>Not Laid</td><td><input type='checkbox' class='addclassQ' value='" + ListStartedQuestion[j].PaperLaidId + "' /></td></tr>";
                        }

                        ival = ival + 1;
                    }
                }
                else if ((ListAdminLOB[i].ConcernedEventId == 3) && (ListAdminLOB[i].TextLOB.Trim().IndexOf("अतारांकित") != -1))
                {
                    jval = 1;
                    outXml += @"<tr><td>" + srrial + "</td><td></td><td>" + ListAdminLOB[i].TextLOB + "</td><td></td><td></td></tr>";
                    for (int k = 0; k < ListUnStartedQuestion.Count; k++)
                    {

                        string sub = ListUnStartedQuestion[k].Subject.Replace("<p>", " ");
                        sub = sub.Replace("</p>", " ");
                        if (ListUnStartedQuestion[k].IsLaid == true && ListUnStartedQuestion[k].FileName != null)
                        {
                            // var actinlink = Url.Action("DisplayQuestionPDF", "LOBFile", new { id = ListUnStartedQuestion[k].QuestionID });
                            string actinlink = USquestionFromLaidPDF(ListUnStartedQuestion[k].QuestionNumber.ToString(), sessDate.ToShortDateString(), SesCode, AssCode);
                            outXml += @"<tr><td>" + srrial + "." + jval + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td  width='76px'><a href='" + actinlink + "' target='_blank' >Paper Laid</a></td><td><input class='addclassQ' type='checkbox' value='" + ListUnStartedQuestion[k].PaperLaidId + "' /></td></tr>";
                        }
                        else if ((ListUnStartedQuestion[k].IsLaid == false || ListUnStartedQuestion[k].IsLaid == null) && ListUnStartedQuestion[k].FileName != null)
                        {
                            outXml += @"<tr><td>" + srrial + "." + jval + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td>Not Laid</td><td><input class='addclassQ' type='checkbox' value='" + ListUnStartedQuestion[k].PaperLaidId + "' /></td></tr>";
                        }
                        else
                        {
                            outXml += @"<tr><td>" + srrial + "." + jval + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td></td><td></td></tr>";
                        }
                        jval = jval + 1;
                    }
                }
                else if ((ListAdminLOB[i].ConcernedEventId == 3) && (ListAdminLOB[i].TextLOB.Trim().IndexOf("स्थगित") != -1 || ListAdminLOB[i].TextLOB.Trim().IndexOf("दिन के लिए") != -1))
                {
                }
                else
                {
                    string ppath = ListAdminLOB[i].PDFLocation;
                    //ppath = ppath.Remove(1,1);
                    if (ppath != null)
                    {
                        ppath = ppath.Remove(0, 1);
                    }

                    if (ListAdminLOB[i].IsLaid == true && ListAdminLOB[i].PDFLocation != null)
                    {
                        //  var actinlink = Url.Action("DisplayLobPDF", "LOBFile", new { id = ListAdminLOB[i].Id });
                        string actinlink = LobPapersFromLaidPDF(srrial.ToString(), sessDate.ToShortDateString(), SesCode, AssCode);
                        outXml += @"<tr><td>" + srrial + "</td><td></td><td >" + ListAdminLOB[i].TextLOB + "</td><td width='76px'><a href='" + actinlink + "' target='_blank' >Paper Laid</a></td><td><input class='addclassL' type='checkbox' value='" + ListAdminLOB[i].Id + "' /></td></tr>";
                    }//kval = kval + 1;
                    else if ((ListAdminLOB[i].IsLaid == false || ListAdminLOB[i].IsLaid == null) && ListAdminLOB[i].PDFLocation != null)
                    {
                        var actinlink = Url.Action("DisplayLobPDF", "LOBFile", new { id = ListAdminLOB[i].Id });
                        outXml += @"<tr><td>" + srrial + "</td><td></td><td >" + ListAdminLOB[i].TextLOB + "</td><td width='76px'>Not Laid</td><td><input class='addclassL' type='checkbox' value='" + ListAdminLOB[i].Id + "' /></td></tr>";
                    }
                    else
                    {
                        if(ListAdminLOB[i].IsLaid == false || ListAdminLOB[i].IsLaid == null)
                        {
                            outXml += @"<tr><td>" + srrial + "</td><td></td><td>" + ListAdminLOB[i].TextLOB + "</td><td width='76px'>Not Laid</td><td><input class='addclassL' type='checkbox' value='" + ListAdminLOB[i].Id + "' /></td></tr>";

                        }
                        else
                        {
                            outXml += @"<tr><td>" + srrial + "</td><td></td><td>" + ListAdminLOB[i].TextLOB + "</td><td width='76px'>Paper Laid</td><td><input class='addclassL' type='checkbox' value='" + ListAdminLOB[i].Id + "' /></td></tr>";

                        }
                      
                        // kval = kval + 1;
                    }
                }

            }
            outXml += "</table>";

            if (ListAdminLOB.Count == 0 && ListUnStartedQuestion.Count == 0 && ListStartedQuestion.Count == 0)
            {
                outXml = "No Records Found";
            }
            return outXml;
        }

        public ActionResult LatestPapersLaidInHouse(string SessionDate, int AssemblyId, int SessionId, string datefrom, string time)
        {

            SessionDate = Sanitizer.GetSafeHtmlFragment(SessionDate);
            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", provider);
            DateTime sessDate = Date;

            datefrom = Sanitizer.GetSafeHtmlFragment(datefrom);
            CultureInfo provider1 = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var DateFrm = DateTime.ParseExact(datefrom, "dd/MM/yyyy", provider);
            DateTime FromDate = DateFrm;

            var html = GetLatestPaperLaidHtml(sessDate, SessionId, AssemblyId, FromDate, time);

            AdminLOB lobs = new AdminLOB();
            lobs.TextLOB = html;


            return PartialView("LatestFilePartial", lobs);
        }
        public string GetLatestPaperLaidHtml(DateTime Date, int SesCode, int AssCode, DateTime fromdate, string time)
        {
            string[] timearr = time.Split(':');


            DateTime CompDate = fromdate.AddHours(Convert.ToInt32(timearr[0]));
            CompDate = CompDate.AddMinutes(Convert.ToInt32(timearr[1]));
            CompDate = CompDate.AddSeconds(Convert.ToInt32(timearr[2]));
            //CompDate = CompDate.AddDays(Convert.ToInt32(-1));
            bool isline = false;
            int ival = 1;
#pragma warning disable CS0219 // The variable 'jval' is assigned but its value is never used
            int jval = 1;
#pragma warning restore CS0219 // The variable 'jval' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'kval' is assigned but its value is never used
            int kval = 1;
#pragma warning restore CS0219 // The variable 'kval' is assigned but its value is never used
            int QuestionType = 1;
            tQuestion Question = new tQuestion();
            DraftLOB DraftLob = new DraftLOB();
            DateTime sessDate = Convert.ToDateTime(Date);
            Question.IsFixedDate = sessDate;
            Question.QuestionType = QuestionType;
            Question.AssemblyID = AssCode;
            Question.SessionID = SesCode;
            List<QuestionModelCustom> ListStartedQuestion = new List<QuestionModelCustom>();
            List<QuestionModelCustom> ListUnStartedQuestion = new List<QuestionModelCustom>();
            List<QuestionModelCustom> ListOtherpaper = new List<QuestionModelCustom>();
            //List<AdminLOB> ListAdminLOB = new List<AdminLOB>();
            ListStartedQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateAndLatestFile", Question);
            Question.QuestionType = 2;
            ListUnStartedQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateAndLatestFile", Question);

            DraftLob.SessionDate = sessDate;
            DraftLob.SessionId = SesCode;
            DraftLob.AssemblyId = AssCode;

            ListOtherpaper = (List<QuestionModelCustom>)Helper.ExecuteService("LOB", "GetDraftLobLatestPapersLaid", DraftLob);


            string outXml = "";
            string srrial = "";
            outXml = "<table>";
            outXml += @"<tr><td style='font-weight:bold;'>Serial No.</td><td style='width='112px;font-weight:bold;'>Type</td><td style='font-weight:bold;'>Subject</td><td style='width=112px;font-weight:bold;'>Date</td><td style='width=112px;font-weight:bold;'>Department</td><td style='width=112px;font-weight:bold;'>File</td><td style='font-weight:bold;'>Latest Version</td><td style='font-weight:bold;'>Last Verified Version</td></tr>";
            for (int j = 0; j < ListStartedQuestion.Count; j++)
            {

                DateTime submiteddate = Convert.ToDateTime(ListStartedQuestion[j].DeptSubmittedDate);

                bool isGraterDate = false;
                int result = DateTime.Compare(submiteddate, CompDate);
                if (result > 0)
                {
                    isGraterDate = true;
                }
                string sub = ListStartedQuestion[j].Subject.Replace("<p>", " ");
                sub = sub.Replace("</p>", " ");
                var actinlink = Url.Action("DisplayQuestionPDF", "LOBFile", new { id = ListStartedQuestion[j].QuestionID });
                var actinlinkDoc = Url.Action("GetDocQfile", "LOBFile", new { id = ListStartedQuestion[j].QuestionID });

                if (isGraterDate == true && ListStartedQuestion[j].FileName != null)
                {
                    string DFile = "";
                    string sFile = ListStartedQuestion[j].FileName.Substring(ListStartedQuestion[j].FileName.LastIndexOf('/') + 1);
                    if (ListStartedQuestion[j].DocFilePath != null && ListStartedQuestion[j].DocFilePath != "")
                    {
                        DFile = ListStartedQuestion[j].DocFilePath.Substring(ListStartedQuestion[j].DocFilePath.LastIndexOf('/') + 1);
                    }

                    String VarifiedDate = Convert.ToString(ListStartedQuestion[j].LastVarifiedVersionDate);
                    if (VarifiedDate != null && VarifiedDate != "")
                    {
                        VarifiedDate = Convert.ToDateTime(VarifiedDate).ToString("dd/MM/yyyy HH:mm");
                    }

                    outXml += @"<tr><td>" + ival + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListStartedQuestion[j].DeptSubmittedDate) + "</td><td>" + ListStartedQuestion[j].DepartmentName + "</td><td width='76px'><a style='color:red;' href='" + actinlink + "' target='_blank' >" + sFile + "</a></br><a style='color:red;' href='" + actinlinkDoc + "' target='_blank' >" + DFile + "</a></td><td> " + ListStartedQuestion[j].Version + "</td><td> " + ListStartedQuestion[j].LastVarifiedVersion + "<br/> Date: " + VarifiedDate + "" + "</td></tr>";
                    ival = ival + 1;
                    isline = true;
                }
                //else if (isGraterDate == false && ListStartedQuestion[j].FileName != null)
                //{
                //    outXml += @"<tr><td>" + ival + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListStartedQuestion[j].DeptSubmittedDate) + "</td><td>" + ListStartedQuestion[j].DepartmentName + "</td><td><a href='" + actinlink + "' target='_blank' >Download File</a></td><td>" + ListStartedQuestion[j].Version + "</td></tr>";
                //}
                //else
                //{
                //    outXml += @"<tr><td>" + ival + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td></td><td></td><td></td><td></td></tr>";
                //}

                //ival = ival + 1;
            }

            for (int k = 0; k < ListUnStartedQuestion.Count; k++)
            {
                DateTime submiteddate = Convert.ToDateTime(ListUnStartedQuestion[k].DeptSubmittedDate);

                bool isGraterDate = false;
                int result = DateTime.Compare(submiteddate, CompDate);
                if (result > 0)
                {
                    isGraterDate = true;
                }
                var actinlink = Url.Action("DisplayQuestionPDF", "LOBFile", new { id = ListUnStartedQuestion[k].QuestionID });
                var actinlinkDoc = Url.Action("GetDocQfile", "LOBFile", new { id = ListUnStartedQuestion[k].QuestionID });
                string sub = ListUnStartedQuestion[k].Subject.Replace("<p>", " ");
                sub = sub.Replace("</p>", " ");
                if (isGraterDate == true && ListUnStartedQuestion[k].FileName != null)
                {
                    string DFile = "";
                    string sFile = ListUnStartedQuestion[k].FileName.Substring(ListUnStartedQuestion[k].FileName.LastIndexOf('/') + 1);
                    //string DFile = ListUnStartedQuestion[k].DocFilePath.Substring(ListUnStartedQuestion[k].DocFilePath.LastIndexOf('/') + 1);
                    if (ListUnStartedQuestion[k].DocFilePath != null && ListUnStartedQuestion[k].DocFilePath != "")
                    {
                        DFile = ListUnStartedQuestion[k].DocFilePath.Substring(ListUnStartedQuestion[k].DocFilePath.LastIndexOf('/') + 1);
                    }
                    outXml += @"<tr><td>" + ival + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListUnStartedQuestion[k].DeptSubmittedDate) + "</td><td>" + ListUnStartedQuestion[k].DepartmentName + "</td><td  width='76px'><a style='color:red;' href='" + actinlink + "' target='_blank' >" + sFile + "</a></br><a style='color:red;' href='" + actinlinkDoc + "' target='_blank' >" + DFile + "</a></td><td>" + ListUnStartedQuestion[k].Version + "</td></tr>";
                    ival = ival + 1;
                    isline = true;
                }
                //else if (isGraterDate == false && ListUnStartedQuestion[k].FileName != null)
                //{
                //    outXml += @"<tr><td>" + ival + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td>" +  string.Format("{0:dd/MM/yyyy hh:mm:ss tt}",  ListUnStartedQuestion[k].DeptSubmittedDate)  + "</td><td>" + ListUnStartedQuestion[k].DepartmentName + "</td><td><a href='" + actinlink + "' target='_blank' >Download File</a></td><td>" + ListUnStartedQuestion[k].Version + "</td></tr>";
                //}
                //else
                //{
                //    outXml += @"<tr><td>" + ival + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td></td><td></td><td></td><td></td></tr>";
                //}

            }

            //for (int i = 0; i < ListOtherpaper.Count; i++)
            //{
            //    DateTime submiteddate = Convert.ToDateTime(ListOtherpaper[i].DeptSubmittedDate);

            //    bool isGraterDate = false;
            //    int result = DateTime.Compare(submiteddate, CompDate);
            //    if (result > 0)
            //    {
            //        isGraterDate = true;
            //    }
            //    var actinlink = Url.Action("GetAssOtherfile", "LOBFile", new { id = ListOtherpaper[i].PaperLaidId });
            //    if (isGraterDate == true && ListOtherpaper[i].FileName != null)
            //    {

            //        outXml += @"<tr><td>" + ival + "</td><td>Other Paper</td><td >" + ListOtherpaper[i].Subject + "</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListOtherpaper[i].DeptSubmittedDate) + "</td><td>" + ListOtherpaper[i].DepartmentName + "</td><td width='76px'><a style='color:red;' href='" + actinlink + "' target='_blank' >Download File(new)</a></td><td>" + ListOtherpaper[i].Version + "</td></tr>";
            //    }//kval = kval + 1;

            //    else
            //    {
            //        outXml += @"<tr><td>" + ival + "</td><td>Other Paper</td><td>" + ListOtherpaper[i].Subject + "</td><td></td><td></td><td></td><td></td></tr>";
            //        // kval = kval + 1;
            //    }
            //    ival = ival + 1;
            //}
            if (ListOtherpaper != null && ListOtherpaper.Count > 0)
            {
                for (int i = 0; i < ListOtherpaper.Count; i++)
                {
                    string sr1 = ListOtherpaper[i].SrNo1.ToString();
                    string sr2 = ListOtherpaper[i].SrNo2.ToString();
                    string sr3 = ListOtherpaper[i].SrNo3.ToString();
                    if (sr1.Length != 0 && sr2.Length != 0 && sr3.Length != 0)
                    {
                        srrial = sr1 + "." + sr2 + "." + sr3;
                    }
                    if (sr1.Length != 0 && sr2.Length != 0 && sr3.Length == 0)
                    {
                        srrial = sr1 + "." + sr2;
                    }
                    if (sr1.Length != 0 && sr2.Length == 0 && sr3.Length == 0)
                    {
                        srrial = sr1;
                    }

                    if ((ListOtherpaper[i].ConcernedEventId == 3) && (ListOtherpaper[i].Subject.Trim().IndexOf("प्रश्नोत्तर") != -1 || ListOtherpaper[i].Subject.Trim().IndexOf("तारांकित") != -1 || ListOtherpaper[i].Subject.Trim().IndexOf("अतारांकित") != -1))
                    {
                    }
                    else if ((ListOtherpaper[i].ConcernedEventId == 3) && (ListOtherpaper[i].Subject.Trim().IndexOf("स्थगित") != -1 || ListOtherpaper[i].Subject.Trim().IndexOf("दिन के लिए") != -1))
                    {
                    }
                    else
                    {
                        DateTime submiteddate = Convert.ToDateTime(ListOtherpaper[i].DeptSubmittedDate);

                        bool isGraterDate = false;
                        int result = DateTime.Compare(submiteddate, CompDate);
                        if (result > 0)
                        {
                            isGraterDate = true;
                        }
                        var actinlink = Url.Action("GetAssOtherfile", "LOBFile", new { id = ListOtherpaper[i].PaperLaidId });
                        var actinlinkDoc = Url.Action("GetDocOtherfile", "LOBFile", new { id = ListOtherpaper[i].PaperLaidId });
                        if (isGraterDate == true && ListOtherpaper[i].FileName != null)
                        {
                            string DFile = "";
                            string sFile = ListOtherpaper[i].FileName.Substring(ListOtherpaper[i].FileName.LastIndexOf('/') + 1);
                            if (ListOtherpaper[i].DocFilePath != null && ListOtherpaper[i].DocFilePath != "")
                            {
                                DFile = ListOtherpaper[i].DocFilePath.Substring(ListOtherpaper[i].DocFilePath.LastIndexOf('/') + 1);
                            }
                            outXml += @"<tr><td>" + srrial + "</td><td></td><td >" + ListOtherpaper[i].Subject + "</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListOtherpaper[i].DeptSubmittedDate) + "</td><td>" + ListOtherpaper[i].DepartmentName + "</td><td width='76px'><a style='color:red;' href='" + actinlink + "' target='_blank' >" + sFile + "</a></br><a style='color:red;' href='" + actinlinkDoc + "' target='_blank' >" + DFile + "</a></td><td>" + ListOtherpaper[i].Version + "</td></tr>";
                            isline = true;
                        }

                        //else
                        //{
                        //    outXml += @"<tr><td>" + srrial + "</td><td></td><td>" + ListOtherpaper[i].Subject + "</td><td></td><td></td><td></td><td></td></tr>";

                        //}
                    }
                }
            }

            outXml += "</table>";

            if ((ListOtherpaper.Count == 0 && ListUnStartedQuestion.Count == 0 && ListStartedQuestion.Count == 0) || (isline == false))
            {
                outXml = "No Records Found";
            }
            return outXml;
        }
        public ActionResult DrafAndOtherPapersLaidInHouse(string SessionDate, int AssemblyId, int SessionId)
        {

            SessionDate = Sanitizer.GetSafeHtmlFragment(SessionDate);
            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", provider);
            DateTime sessDate = Date;
            var html = GetDraftAndApprovedPaperLaidHtml(sessDate, SessionId, AssemblyId);

            AdminLOB lobs = new AdminLOB();
            lobs.TextLOB = html;
            ViewData["SessionDate"] = sessDate.ToString("D") + " Papers Laid in House";// String.Format("{dd,MMMM, yyyy}", sessDate);

            return PartialView("DraftAndApprovedPaperLaidPrtial", lobs);
        }
        public string GetDraftAndApprovedPaperLaidHtml(DateTime Date, int SesCode, int AssCode)
        {
#pragma warning disable CS0219 // The variable 'ival' is assigned but its value is never used
            int ival = 1;
#pragma warning restore CS0219 // The variable 'ival' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'jval' is assigned but its value is never used
            int jval = 1;
#pragma warning restore CS0219 // The variable 'jval' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'kval' is assigned but its value is never used
            int kval = 1;
#pragma warning restore CS0219 // The variable 'kval' is assigned but its value is never used
            int QuestionType = 1;
            tQuestion Question = new tQuestion();
            DraftLOB DraftLobs = new DraftLOB();
            DateTime sessDate = Convert.ToDateTime(Date);
            Question.IsFixedDate = sessDate;
            Question.QuestionType = QuestionType;
            List<QuestionModelCustom> ListStartedQuestion = new List<QuestionModelCustom>();
            List<QuestionModelCustom> ListUnStartedQuestion = new List<QuestionModelCustom>();
            List<DraftLOB> ListDraftLOB = new List<DraftLOB>();
            ListStartedQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetDraftAndApprovedQuestionBySessionDateAndLaidType", Question);
            Question.QuestionType = 2;
            ListUnStartedQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetDraftAndApprovedQuestionBySessionDateAndLaidType", Question);

            DraftLobs.SessionDate = sessDate;
            DraftLobs.SessionId = SesCode;
            DraftLobs.AssemblyId = AssCode;
            ListDraftLOB = (List<DraftLOB>)Helper.ExecuteService("LOB", "GetDraftLobPapersLaid", DraftLobs);
            //string sessiondate = null;



            //List<tQuestion> ListQuestion = new List<tQuestion>();
            //List<QuestionModelCustom> ListQuestionModelCustom = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateWithPaper", Question);

            string outXml = "";
            string srrial = "";
            outXml = "<table>";

            int SQS1 = 0;
            int SQS2 = 0;
            int SQS3 = 0;
            int UQS1 = 0;
            int UQS2 = 0;
            int UQS3 = 0;
            if (ListStartedQuestion != null && ListStartedQuestion.Count > 0)
            {
                SQS1 = 1;
                SQS2 = 1;
                SQS3 = 1;
                outXml += @"<tr><td>" + SQS1 + "</td><td></td><td>प्रश्नोत्तर:</td><td>Department</td><td>Draft File</td><td width='83px'>Signed File</td><td>Sined Date</td></tr>";
                outXml += @"<tr><td>" + SQS1 + "." + SQS2 + "</td><td></td><td>तारांकित: (पृथक सूचियों में मुद्रित प्रश्न पूछे जाएंगे तथा उनके उत्तर दिए जाएंगे ।):</td><td></td><td></td><td></td><td></td></tr>";
                for (int j = 0; j < ListStartedQuestion.Count; j++)
                {

                    string sub = ListStartedQuestion[j].Subject.Replace("<p>", " ");
                    sub = sub.Replace("</p>", " ");

                    if (ListStartedQuestion[j].QuestionNumber != null)
                    {
                        var actinlinkDraft = Url.Action("DisplayDraftQuestionPDF", "LOBFile", new { id = ListStartedQuestion[j].QuestionID });
                        var actinlinkApproved = Url.Action("DisplayQuestionPDF", "LOBFile", new { id = ListStartedQuestion[j].QuestionID });
                        if (ListStartedQuestion[j].PaperLaidId != null)
                        {
                            if (ListStartedQuestion[j].FileName != null)
                            {
                                string dFile = "";
                                string sFile = ListStartedQuestion[j].FileName.Substring(ListStartedQuestion[j].FileName.LastIndexOf('/') + 1);

                                if (ListStartedQuestion[j].SfilePath != null && ListStartedQuestion[j].SfilePath != "")
                                {
                                    dFile = ListStartedQuestion[j].SfilePath.Substring(ListStartedQuestion[j].SfilePath.LastIndexOf('/') + 1);
                                }
                                outXml += @"<tr><td>" + SQS1 + "." + SQS2 + "." + SQS3 + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td>" + ListStartedQuestion[j].DepartmentName + "</td><td width='76px'><a href='" + actinlinkDraft + "' target='_blank' >" + dFile + "</a></td><td width='76px'><a href='" + actinlinkApproved + "' target='_blank' >" + sFile + "</a></td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListStartedQuestion[j].QSubmitedDate) + "</td></tr>";
                            }
                            else if (ListStartedQuestion[j].FileName == null && ListStartedQuestion[j].SfilePath != null)
                            {
                                string dFile = ListStartedQuestion[j].SfilePath.Substring(ListStartedQuestion[j].SfilePath.LastIndexOf('/') + 1);

                                outXml += @"<tr><td>" + SQS1 + "." + SQS2 + "." + SQS3 + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td>" + ListStartedQuestion[j].DepartmentName + "</td><td width='76px'><a href='" + actinlinkDraft + "' target='_blank' >" + dFile + "</a></td><td width='76px'></td><td></td></tr>";
                            }
                            else
                            {
                                outXml += @"<tr><td>" + SQS1 + "." + SQS2 + "." + SQS3 + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td>" + ListStartedQuestion[j].DepartmentName + "</td><td></td><td></td><td></td></tr>";
                            }
                        }
                        else
                        {
                            outXml += @"<tr><td>" + SQS1 + "." + SQS2 + "." + SQS3 + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td>" + ListStartedQuestion[j].DepartmentName + "</td><td></td><td></td><td></td></tr>";
                        }
                    }

                    else
                    {
                        outXml += @"<tr><td>" + SQS1 + "." + SQS2 + "." + SQS3 + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td>" + ListStartedQuestion[j].DepartmentName + "</td><td></td><td></td><td></td><td></td></tr>";
                    }

                    SQS3 = SQS3 + 1;
                }
            }
            if (ListUnStartedQuestion != null && ListUnStartedQuestion.Count > 0)
            {
                UQS1 = 1;
                UQS2 = 2;
                UQS3 = 1;
                outXml += @"<tr><td>" + UQS1 + "." + UQS2 + "</td><td></td><td>अतारांकित: (पृथक सूचियों में मुद्रित प्रश्नों के उत्तर सभा पटल पर रखे जाएंगे ।)</td><td></td><td></td><td></td><td></td></tr>";
                for (int k = 0; k < ListUnStartedQuestion.Count; k++)
                {

                    string sub = ListUnStartedQuestion[k].Subject.Replace("<p>", " ");
                    sub = sub.Replace("</p>", " ");
                    if (ListUnStartedQuestion[k].Subject != null)
                    {
                        var actinlinkDraft = Url.Action("DisplayDraftQuestionPDF", "LOBFile", new { id = ListUnStartedQuestion[k].QuestionID });
                        var actinlinkApproved = Url.Action("DisplayQuestionPDF", "LOBFile", new { id = ListUnStartedQuestion[k].QuestionID });
                        if (ListUnStartedQuestion[k].PaperLaidId != null)
                        {
                            if (ListUnStartedQuestion[k].FileName != null)
                            {
                                string sFile = ListUnStartedQuestion[k].FileName.Substring(ListUnStartedQuestion[k].FileName.LastIndexOf('/') + 1);
                                string dFile = ListUnStartedQuestion[k].SfilePath.Substring(ListUnStartedQuestion[k].SfilePath.LastIndexOf('/') + 1);
                                outXml += @"<tr><td>" + UQS1 + "." + UQS2 + "." + UQS3 + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td>" + ListUnStartedQuestion[k].DepartmentName + "</td><td  width='76px'><a href='" + actinlinkDraft + "' target='_blank' >" + dFile + "</a></td><td width='79px'><a href='" + actinlinkApproved + "' target='_blank' >" + sFile + "</a></td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListUnStartedQuestion[k].QSubmitedDate) + "</td></tr>";
                            }
                            else if (ListUnStartedQuestion[k].FileName == null && ListUnStartedQuestion[k].SfilePath != null)
                            {
                                string dFile = ListUnStartedQuestion[k].SfilePath.Substring(ListUnStartedQuestion[k].SfilePath.LastIndexOf('/') + 1);
                                outXml += @"<tr><td>" + UQS1 + "." + UQS2 + "." + UQS3 + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td>" + ListUnStartedQuestion[k].DepartmentName + "</td><td  width='76px'><a href='" + actinlinkDraft + "' target='_blank' >" + dFile + "</a></td><td width='79px'></td><td></td></tr>";
                            }
                            else
                            {
                                outXml += @"<tr><td>" + UQS1 + "." + UQS2 + "." + UQS3 + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td>" + ListUnStartedQuestion[k].DepartmentName + "</td><td></td><td></td><td></td></tr>";
                            }
                        }
                        else
                        {
                            outXml += @"<tr><td>" + UQS1 + "." + UQS2 + "." + UQS3 + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td>" + ListUnStartedQuestion[k].DepartmentName + "</td><td></td><td></td><td></td></tr>";
                        }

                    }

                    else
                    {
                        outXml += @"<tr><td>" + UQS1 + "." + UQS2 + "." + UQS3 + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td>" + ListUnStartedQuestion[k].DepartmentName + "</td><td></td><td></td><td></td></tr>";
                    }
                    UQS3 = UQS3 + 1;
                }
            }
            if (DraftLobs != null && ListDraftLOB.Count > 0)
            {
                for (int i = 0; i < ListDraftLOB.Count; i++)
                {
                    string sr1 = ListDraftLOB[i].SrNo1.ToString();
                    string sr2 = ListDraftLOB[i].SrNo2.ToString();
                    string sr3 = ListDraftLOB[i].SrNo3.ToString();
                    if (sr1.Length != 0 && sr2.Length != 0 && sr3.Length != 0)
                    {
                        srrial = sr1 + "." + sr2 + "." + sr3;
                    }
                    if (sr1.Length != 0 && sr2.Length != 0 && sr3.Length == 0)
                    {
                        srrial = sr1 + "." + sr2;
                    }
                    if (sr1.Length != 0 && sr2.Length == 0 && sr3.Length == 0)
                    {
                        srrial = sr1;
                    }

                    if ((ListDraftLOB[i].ConcernedEventId == 3) && (ListDraftLOB[i].TextLOB.Trim().IndexOf("प्रश्नोत्तर") != -1 || ListDraftLOB[i].TextLOB.Trim().IndexOf("तारांकित") != -1 || ListDraftLOB[i].TextLOB.Trim().IndexOf("अतारांकित") != -1))
                    {
                    }
                    else if ((ListDraftLOB[i].ConcernedEventId == 3) && (ListDraftLOB[i].TextLOB.Trim().IndexOf("स्थगित") != -1 || ListDraftLOB[i].TextLOB.Trim().IndexOf("दिन के लिए") != -1))
                    {
                    }
                    else
                    {
                        string ppath = ListDraftLOB[i].PDFLocation;
                        //ppath = ppath.Remove(1,1);
                        if (ppath != null)
                        {
                            ppath = ppath.Remove(0, 1);
                        }

                        if (ListDraftLOB[i].PDFLocation != null)
                        {
                            string sFile = ListDraftLOB[i].PDFLocation.Substring(ListDraftLOB[i].PDFLocation.LastIndexOf('/') + 1);
                            var actinlink = Url.Action("DisplayDraftLobPDF", "LOBFile", new { id = ListDraftLOB[i].Id });
                            outXml += @"<tr><td>" + srrial + "</td><td></td><td >" + ListDraftLOB[i].TextLOB + "</td><td></td><td></td><td><a href='" + actinlink + "' target='_blank' >" + sFile + "</a></td><td></td></tr>";
                        }//kval = kval + 1;

                        else
                        {
                            outXml += @"<tr><td>" + srrial + "</td><td></td><td>" + ListDraftLOB[i].TextLOB + "</td><td></td><td></td><td></td><td></td></tr>";
                            // kval = kval + 1;
                        }
                    }
                }
            }

            outXml += "</table>";

            if (ListDraftLOB.Count == 0 && ListUnStartedQuestion.Count == 0 && ListStartedQuestion.Count == 0)
            {
                outXml = "No Records Found";
            }
            return outXml;
        }
        public ActionResult SearchLetestFileToCopyForAssemblyHouse(string Date)
        {

            bool isline = false;
            int ival = 1;
#pragma warning disable CS0219 // The variable 'jval' is assigned but its value is never used
            int jval = 1;
#pragma warning restore CS0219 // The variable 'jval' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'kval' is assigned but its value is never used
            int kval = 1;
#pragma warning restore CS0219 // The variable 'kval' is assigned but its value is never used
            int QuestionType = 1;
            tQuestion Question = new tQuestion();
            DraftLOB DraftLob = new DraftLOB();
            Date = Sanitizer.GetSafeHtmlFragment(Date);
            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Dater = DateTime.ParseExact(Date, "dd/MM/yyyy", provider);
            DateTime sessDate = Dater;
            Question.IsFixedDate = sessDate;
            Question.QuestionType = QuestionType;
            //Question.AssemblyID = AssCode;
            //Question.SessionID = SesCode;
            List<QuestionModelCustom> ListStartedQuestion = new List<QuestionModelCustom>();
            List<QuestionModelCustom> ListUnStartedQuestion = new List<QuestionModelCustom>();
            List<QuestionModelCustom> ListOtherpaper = new List<QuestionModelCustom>();
            //List<AdminLOB> ListAdminLOB = new List<AdminLOB>();
            ListStartedQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateAndLatestFile", Question);
            Question.QuestionType = 2;
            ListUnStartedQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateAndLatestFile", Question);

            DraftLob.SessionDate = sessDate;
            //DraftLob.SessionId = SesCode;
            //DraftLob.AssemblyId = AssCode;

            ListOtherpaper = (List<QuestionModelCustom>)Helper.ExecuteService("LOB", "GetDraftLobLatestPapersLaid", DraftLob);


            string outXml = "";
            string srrial = "";
            outXml = "<table>";
            outXml += @"<tr><td style='font-weight:bold;'>Serial No.</td><td style='width='112px;font-weight:bold;'>Type</td><td style='font-weight:bold;'>Subject</td><td style='width=112px;font-weight:bold;'>Date</td><td style='width=112px;font-weight:bold;'>Department</td><td style='width=112px;font-weight:bold;'>File</td><td style='font-weight:bold;'>Version</td><td><input  type='checkbox' onclick='SelectAllChb(this);'  class='checkedAll'/></td></tr>";
            //for (int j = 0; j < ListStartedQuestion.Count; j++)
            //{

            //    //DateTime submiteddate = Convert.ToDateTime(ListStartedQuestion[j].DeptSubmittedDate);

            //    //bool isGraterDate = false;
            //    //int result = DateTime.Compare(submiteddate, CompDate);
            //    //if (result > 0)
            //    //{
            //    //    isGraterDate = true;
            //    //}
            //    string sub = ListStartedQuestion[j].Subject.Replace("<p>", " ");
            //    sub = sub.Replace("</p>", " ");
            //    var actinlink = Url.Action("DisplayQuestionPDF", "LOBFile", new { id = ListStartedQuestion[j].QuestionID });
            //    if (ListStartedQuestion[j].FileName != null)
            //    {
            //        string sFile = ListStartedQuestion[j].FileName.Substring(ListStartedQuestion[j].FileName.LastIndexOf('/') + 1);
            //        outXml += @"<tr><td>" + ival + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListStartedQuestion[j].DeptSubmittedDate) + "</td><td>" + ListStartedQuestion[j].DepartmentName + "</td><td width='76px'><a style='color:red;' href='" + actinlink + "' target='_blank' >" + sFile + "</a></td><td> " + ListStartedQuestion[j].Version + "</td><td><input class='addclassQ'  type='checkbox' value='" + ListStartedQuestion[j].QuestionNumber + "' /></td></tr>";
            //        ival = ival + 1;
            //        isline = true;
            //    }

            //    else
            //    {
            //        outXml += @"<tr><td>" + ival + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td></td><td></td><td></td><td></td><td></td></tr>";
            //    }

            //    //ival = ival + 1;
            //}

            //for (int k = 0; k < ListUnStartedQuestion.Count; k++)
            //{
            //    DateTime submiteddate = Convert.ToDateTime(ListUnStartedQuestion[k].DeptSubmittedDate);

            //    //bool isGraterDate = false;
            //    //int result = DateTime.Compare(submiteddate, CompDate);
            //    //if (result > 0)
            //    //{
            //    //    isGraterDate = true;
            //    //}
            //    var actinlink = Url.Action("DisplayQuestionPDF", "LOBFile", new { id = ListUnStartedQuestion[k].QuestionID });
            //    string sub = ListUnStartedQuestion[k].Subject.Replace("<p>", " ");
            //    sub = sub.Replace("</p>", " ");
            //    if (ListUnStartedQuestion[k].FileName != null)
            //    {
            //        string sFile = ListUnStartedQuestion[k].FileName.Substring(ListUnStartedQuestion[k].FileName.LastIndexOf('/') + 1);
            //        outXml += @"<tr><td>" + ival + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListUnStartedQuestion[k].DeptSubmittedDate) + "</td><td>" + ListUnStartedQuestion[k].DepartmentName + "</td><td  width='76px'><a style='color:red;' href='" + actinlink + "' target='_blank' >" + sFile + "</a></td><td>" + ListUnStartedQuestion[k].Version + "</td><td><input class='addclassQ'  type='checkbox' value='" + ListUnStartedQuestion[k].QuestionNumber + "' /></td></tr>";
            //        ival = ival + 1;
            //        isline = true;
            //    }

            //    else
            //    {
            //        outXml += @"<tr><td>" + ival + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td></td><td></td><td></td><td></td><td></td></tr>";
            //    }

            //}


            if (ListOtherpaper != null && ListOtherpaper.Count > 0)
            {
                for (int i = 0; i < ListOtherpaper.Count; i++)
                {
                    string sr1 = ListOtherpaper[i].SrNo1.ToString();
                    string sr2 = ListOtherpaper[i].SrNo2.ToString();
                    string sr3 = ListOtherpaper[i].SrNo3.ToString();
                    if (sr1.Length != 0 && sr2.Length != 0 && sr3.Length != 0)
                    {
                        srrial = sr1 + "." + sr2 + "." + sr3;
                    }
                    if (sr1.Length != 0 && sr2.Length != 0 && sr3.Length == 0)
                    {
                        srrial = sr1 + "." + sr2;
                    }
                    if (sr1.Length != 0 && sr2.Length == 0 && sr3.Length == 0)
                    {
                        srrial = sr1;
                    }



                    if ((ListOtherpaper[i].ConcernedEventId == 3) && (ListOtherpaper[i].Subject.Trim().IndexOf("प्रश्नोत्तर") != -1 || ListOtherpaper[i].Subject.Trim().IndexOf("तारांकित") != -1 || ListOtherpaper[i].Subject.Trim().IndexOf("अतारांकित") != -1))
                    {
                    }
                    else if ((ListOtherpaper[i].ConcernedEventId == 3) && (ListOtherpaper[i].Subject.Trim().IndexOf("स्थगित") != -1 || ListOtherpaper[i].Subject.Trim().IndexOf("दिन के लिए") != -1))
                    {
                    }
                    else
                    {
                        DateTime submiteddate = Convert.ToDateTime(ListOtherpaper[i].DeptSubmittedDate);

                        //bool isGraterDate = false;
                        //int result = DateTime.Compare(submiteddate, CompDate);
                        //if (result > 0)
                        //{
                        //    isGraterDate = true;
                        //}
                        var actinlink = Url.Action("GetAssOtherfile", "LOBFile", new { id = ListOtherpaper[i].PaperLaidId });
                        //string sFile = ListDraftLOB[i].PDFLocation.Substring(ListDraftLOB[i].PDFLocation.LastIndexOf('/') + 1);
                        var actinlinkLocal = Url.Action("DisplayDraftLobPDF", "LOBFile", new { id = ListOtherpaper[i].LobId });
                        if (ListOtherpaper[i].FileName != null)
                        {
                            string sFile = ListOtherpaper[i].FileName.Substring(ListOtherpaper[i].FileName.LastIndexOf('/') + 1);
                            outXml += @"<tr><td>" + srrial + "</td><td></td><td >" + ListOtherpaper[i].Subject + "</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListOtherpaper[i].DeptSubmittedDate) + "</td><td>" + ListOtherpaper[i].DepartmentName + "</td><td width='76px'><a style='color:red;' href='" + actinlink + "' target='_blank' >" + sFile + "</a></td><td>" + ListOtherpaper[i].Version + "</td><td><input class='addclassL'  type='checkbox' value='" + ListOtherpaper[i].PaperLaidId + "' /></td></tr>";
                            isline = true;
                        }
                        else if (ListOtherpaper[i].LocalLobPdf != null)
                        {
                            string sFile1 = ListOtherpaper[i].LocalLobPdf.Substring(ListOtherpaper[i].LocalLobPdf.LastIndexOf('/') + 1);
                            outXml += @"<tr><td>" + srrial + "</td><td></td><td >" + ListOtherpaper[i].Subject + "</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListOtherpaper[i].DeptSubmittedDate) + "</td><td>" + ListOtherpaper[i].DepartmentName + "</td><td width='76px'><a style='color:red;' href='" + actinlinkLocal + "' target='_blank' >" + sFile1 + "</a></td><td>" + ListOtherpaper[i].Version + "</td><td><input class='addclassL'  type='checkbox' value='" + ListOtherpaper[i].PaperLaidId + "' /></td></tr>";
                            isline = true;
                        }

                        else
                        {
                            outXml += @"<tr><td>" + srrial + "</td><td></td><td>" + ListOtherpaper[i].Subject + "</td><td></td><td></td><td></td><td></td><td><input class='addclassL'  type='checkbox' value='" + ListOtherpaper[i].PaperLaidId + "' /></td></tr>";
                            isline = true;
                        }
                    }
                }
            }

            outXml += "</table>";

            if ((ListOtherpaper.Count == 0 && ListUnStartedQuestion.Count == 0 && ListStartedQuestion.Count == 0) || (isline == false))
            {
                outXml = "No Records Found";
            }
            //return outXml;
            AdminLOB lobs = new AdminLOB();
            lobs.TextLOB = outXml;
            //ViewData["SessionDate"] = sessDate.ToString("D") + " Papers Laid in House";// String.Format("{dd,MMMM, yyyy}", sessDate);

            return PartialView("DraftAndApprovedPaperLaidPrtial", lobs);
        }
        public ActionResult SearchLetestFile(string Date)
        {
            // string[] timearr = time.Split(':');


            //DateTime CompDate = fromdate.AddHours(Convert.ToInt32(timearr[0]));
            //CompDate = CompDate.AddMinutes(Convert.ToInt32(timearr[1]));
            //CompDate = CompDate.AddSeconds(Convert.ToInt32(timearr[2]));
            //CompDate = CompDate.AddDays(Convert.ToInt32(-1));
            bool isline = false;
            int ival = 1;
#pragma warning disable CS0219 // The variable 'jval' is assigned but its value is never used
            int jval = 1;
#pragma warning restore CS0219 // The variable 'jval' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'kval' is assigned but its value is never used
            int kval = 1;
#pragma warning restore CS0219 // The variable 'kval' is assigned but its value is never used
            int QuestionType = 1;
            tQuestion Question = new tQuestion();
            DraftLOB DraftLob = new DraftLOB();
            Date = Sanitizer.GetSafeHtmlFragment(Date);
            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Dater = DateTime.ParseExact(Date, "dd/MM/yyyy", provider);
            DateTime sessDate = Dater;
            Question.IsFixedDate = sessDate;
            Question.QuestionType = QuestionType;
            //Question.AssemblyID = AssCode;
            //Question.SessionID = SesCode;
            List<QuestionModelCustom> ListStartedQuestion = new List<QuestionModelCustom>();
            List<QuestionModelCustom> ListUnStartedQuestion = new List<QuestionModelCustom>();
            List<QuestionModelCustom> ListOtherpaper = new List<QuestionModelCustom>();
            //List<AdminLOB> ListAdminLOB = new List<AdminLOB>();
            ListStartedQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateAndLatestFile", Question);
            Question.QuestionType = 2;
            ListUnStartedQuestion = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateAndLatestFile", Question);

            DraftLob.SessionDate = sessDate;
            //DraftLob.SessionId = SesCode;
            //DraftLob.AssemblyId = AssCode;

            ListOtherpaper = (List<QuestionModelCustom>)Helper.ExecuteService("LOB", "GetDraftLobLatestPapersLaid", DraftLob);


            string outXml = "";
            string srrial = "";
            outXml = "<table>";
            outXml += @"<tr><td style='font-weight:bold;'>Serial No.</td><td style='width='112px;font-weight:bold;'>Type</td><td style='font-weight:bold;'>Subject</td><td style='width=112px;font-weight:bold;'>Date</td><td style='width=112px;font-weight:bold;'>Department</td><td style='width=112px;font-weight:bold;'>File</td><td style='font-weight:bold;'>Version</td></tr>";
            for (int j = 0; j < ListStartedQuestion.Count; j++)
            {

                //DateTime submiteddate = Convert.ToDateTime(ListStartedQuestion[j].DeptSubmittedDate);

                //bool isGraterDate = false;
                //int result = DateTime.Compare(submiteddate, CompDate);
                //if (result > 0)
                //{
                //    isGraterDate = true;
                //}
                string sub = ListStartedQuestion[j].Subject.Replace("<p>", " ");
                sub = sub.Replace("</p>", " ");
                var actinlink = Url.Action("DisplayQuestionPDF", "LOBFile", new { id = ListStartedQuestion[j].QuestionID });
                if (ListStartedQuestion[j].FileName != null)
                {
                    string sFile = ListStartedQuestion[j].FileName.Substring(ListStartedQuestion[j].FileName.LastIndexOf('/') + 1);
                    outXml += @"<tr><td>" + ival + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListStartedQuestion[j].DeptSubmittedDate) + "</td><td>" + ListStartedQuestion[j].DepartmentName + "</td><td width='76px'><a style='color:red;' href='" + actinlink + "' target='_blank' >" + sFile + "</a></td><td> " + ListStartedQuestion[j].Version + "</td></tr>";
                    ival = ival + 1;
                    isline = true;
                }

                else
                {
                    outXml += @"<tr><td>" + ival + "</td><td>Starred Q.</td><td>" + ListStartedQuestion[j].QuestionNumber + "(" + sub + ")</td><td></td><td></td><td></td><td></td></tr>";
                }

                //ival = ival + 1;
            }

            for (int k = 0; k < ListUnStartedQuestion.Count; k++)
            {
                DateTime submiteddate = Convert.ToDateTime(ListUnStartedQuestion[k].DeptSubmittedDate);

                //bool isGraterDate = false;
                //int result = DateTime.Compare(submiteddate, CompDate);
                //if (result > 0)
                //{
                //    isGraterDate = true;
                //}
                var actinlink = Url.Action("DisplayQuestionPDF", "LOBFile", new { id = ListUnStartedQuestion[k].QuestionID });
                string sub = ListUnStartedQuestion[k].Subject.Replace("<p>", " ");
                sub = sub.Replace("</p>", " ");
                if (ListUnStartedQuestion[k].FileName != null)
                {
                    string sFile = ListUnStartedQuestion[k].FileName.Substring(ListUnStartedQuestion[k].FileName.LastIndexOf('/') + 1);
                    outXml += @"<tr><td>" + ival + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListUnStartedQuestion[k].DeptSubmittedDate) + "</td><td>" + ListUnStartedQuestion[k].DepartmentName + "</td><td  width='76px'><a style='color:red;' href='" + actinlink + "' target='_blank' >" + sFile + "</a></td><td>" + ListUnStartedQuestion[k].Version + "</td></tr>";
                    ival = ival + 1;
                    isline = true;
                }

                else
                {
                    outXml += @"<tr><td>" + ival + "</td><td>UnStarred Q.</td><td>" + ListUnStartedQuestion[k].QuestionNumber + "(" + sub + ")</td><td></td><td></td><td></td><td></td></tr>";
                }

            }


            if (ListOtherpaper != null && ListOtherpaper.Count > 0)
            {
                for (int i = 0; i < ListOtherpaper.Count; i++)
                {
                    string sr1 = ListOtherpaper[i].SrNo1.ToString();
                    string sr2 = ListOtherpaper[i].SrNo2.ToString();
                    string sr3 = ListOtherpaper[i].SrNo3.ToString();
                    if (sr1.Length != 0 && sr2.Length != 0 && sr3.Length != 0)
                    {
                        srrial = sr1 + "." + sr2 + "." + sr3;
                    }
                    if (sr1.Length != 0 && sr2.Length != 0 && sr3.Length == 0)
                    {
                        srrial = sr1 + "." + sr2;
                    }
                    if (sr1.Length != 0 && sr2.Length == 0 && sr3.Length == 0)
                    {
                        srrial = sr1;
                    }

                    if ((ListOtherpaper[i].ConcernedEventId == 3) && (ListOtherpaper[i].Subject.Trim().IndexOf("प्रश्नोत्तर") != -1 || ListOtherpaper[i].Subject.Trim().IndexOf("तारांकित") != -1 || ListOtherpaper[i].Subject.Trim().IndexOf("अतारांकित") != -1))
                    {
                    }
                    else if ((ListOtherpaper[i].ConcernedEventId == 3) && (ListOtherpaper[i].Subject.Trim().IndexOf("स्थगित") != -1 || ListOtherpaper[i].Subject.Trim().IndexOf("दिन के लिए") != -1))
                    {
                    }
                    else
                    {
                        DateTime submiteddate = Convert.ToDateTime(ListOtherpaper[i].DeptSubmittedDate);

                        //bool isGraterDate = false;
                        //int result = DateTime.Compare(submiteddate, CompDate);
                        //if (result > 0)
                        //{
                        //    isGraterDate = true;
                        //}
                        var actinlinkLocal = Url.Action("DisplayDraftLobPDF", "LOBFile", new { id = ListOtherpaper[i].LobId });
                        var actinlink = Url.Action("GetAssOtherfile", "LOBFile", new { id = ListOtherpaper[i].PaperLaidId });
                        if (ListOtherpaper[i].FileName != null)
                        {
                            string sFile = ListOtherpaper[i].FileName.Substring(ListOtherpaper[i].FileName.LastIndexOf('/') + 1);
                            outXml += @"<tr><td>" + srrial + "</td><td></td><td >" + ListOtherpaper[i].Subject + "</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListOtherpaper[i].DeptSubmittedDate) + "</td><td>" + ListOtherpaper[i].DepartmentName + "</td><td width='76px'><a style='color:red;' href='" + actinlink + "' target='_blank' >" + sFile + "</a></td><td>" + ListOtherpaper[i].Version + "</td></tr>";
                            isline = true;
                        }
                        else if (ListOtherpaper[i].LocalLobPdf != null)
                        {
                            string sFile1 = ListOtherpaper[i].LocalLobPdf.Substring(ListOtherpaper[i].LocalLobPdf.LastIndexOf('/') + 1);
                            outXml += @"<tr><td>" + srrial + "</td><td></td><td >" + ListOtherpaper[i].Subject + "</td><td>" + string.Format("{0:dd/MM/yyyy hh:mm:ss tt}", ListOtherpaper[i].DeptSubmittedDate) + "</td><td>" + ListOtherpaper[i].DepartmentName + "</td><td width='76px'><a style='color:red;' href='" + actinlinkLocal + "' target='_blank' >" + sFile1 + "</a></td><td>" + ListOtherpaper[i].Version + "</td></tr>";
                            isline = true;
                        }
                        else
                        {
                            outXml += @"<tr><td>" + srrial + "</td><td></td><td>" + ListOtherpaper[i].Subject + "</td><td></td><td></td><td></td><td></td></tr>";

                        }
                    }
                }
            }

            outXml += "</table>";

            if ((ListOtherpaper.Count == 0 && ListUnStartedQuestion.Count == 0 && ListStartedQuestion.Count == 0) || (isline == false))
            {
                outXml = "No Records Found";
            }
            //return outXml;
            AdminLOB lobs = new AdminLOB();
            lobs.TextLOB = outXml;
            //ViewData["SessionDate"] = sessDate.ToString("D") + " Papers Laid in House";// String.Format("{dd,MMMM, yyyy}", sessDate);

            return PartialView("DraftAndApprovedPaperLaidPrtial", lobs);
        }

        public bool checkFileLocation(string filepath)
        {
            try
            {

#pragma warning disable CS0219 // The variable 'stream' is assigned but its value is never used
                Stream stream = null;
#pragma warning restore CS0219 // The variable 'stream' is assigned but its value is never used

                int bytesToRead = 10000;
                byte[] buffer = new Byte[bytesToRead];
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(filepath);
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;
                return true;
                // stream = fileResp.GetResponseStream();
                // return File(stream, "application/pdf");
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return false;
            }
        }
        public ActionResult DisplayQuestionPDF(string id)
        {
            //var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            ///string path = fileAcessingSettings.SettingValue;
            tQuestion model = new tQuestion();
            model.QuestionID = Convert.ToInt32(id);
            string FileName = (string)Helper.ExecuteService("Questions", "GetAssQuestionFromID", model);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string fullpath = System.IO.Path.Combine(FileSettings.SettingValue + FileName);

            //string fullpath = Server.MapPath(FileName);
            try
            {
                //Stream stream = null;

                //int bytesToRead = 10000;
                //byte[] buffer = new Byte[bytesToRead];
                //HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(fullpath);
                //HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                //if (fileReq.ContentLength > 0)
                //    fileResp.ContentLength = fileReq.ContentLength;

                //stream = fileResp.GetResponseStream();
                //return File(stream, "application/pdf");
                byte[] bytes = System.IO.File.ReadAllBytes(fullpath);
                return File(bytes, "application/pdf");
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return View("FileNotFound");
            }
        }
        public ActionResult DisplayDraftQuestionPDF(string id)
        {
            //var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            ///string path = fileAcessingSettings.SettingValue;
            tQuestion model = new tQuestion();
            model.QuestionID = Convert.ToInt32(id);
            string FileName = (string)Helper.ExecuteService("Questions", "GetDraftPDFQuestionFromID", model);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string fullpath = System.IO.Path.Combine(FileSettings.SettingValue + FileName);

            //string fullpath = Server.MapPath(FileName);
            try
            {

                byte[] bytes = System.IO.File.ReadAllBytes(fullpath);
                return File(bytes, "application/pdf");
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return View("FileNotFound");
            }
        }

        public ActionResult DisplayLobPDF(string id)
        {

            // var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            // string path = fileAcessingSettings.SettingValue;
            AdminLOB model = new AdminLOB();
            model.Id = Convert.ToInt32(id);
            string FileName = (string)Helper.ExecuteService("LOB", "GetFilenamePapersLaid", model);
#pragma warning disable CS0219 // The variable 'Filename' is assigned but its value is never used
            string Filename = "";
#pragma warning restore CS0219 // The variable 'Filename' is assigned but its value is never used
            if (FileName.Contains("~"))
            {
                FileName = FileName.Replace("~", "");
            }
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string fullpath = System.IO.Path.Combine(FileSettings.SettingValue + FileName);
            //   string fullpath = Server.MapPath(FileName);
            //fullpath = fullpath.Replace("~", "");
            try
            {
#pragma warning disable CS0219 // The variable 'stream' is assigned but its value is never used
                Stream stream = null;
#pragma warning restore CS0219 // The variable 'stream' is assigned but its value is never used


                byte[] bytes = System.IO.File.ReadAllBytes(fullpath);
                return File(bytes, "application/pdf");
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return View("FileNotFound");
            }
        }
        public ActionResult DisplayDraftLobPDF(string id)
        {

            // var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            // string path = fileAcessingSettings.SettingValue;
            DraftLOB model = new DraftLOB();
            model.Id = Convert.ToInt32(id);
            string FileName = (string)Helper.ExecuteService("LOB", "GetDraftFilenamePapersLaid", model);
            //string Filename = "";
            if (FileName.Contains("~"))
            {
                FileName = FileName.Replace("~", "");
            }
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string fullpath = System.IO.Path.Combine(FileSettings.SettingValue + FileName);
            //   string fullpath = Server.MapPath(FileName);
            //fullpath = fullpath.Replace("~", "");
            try
            {
#pragma warning disable CS0219 // The variable 'stream' is assigned but its value is never used
                Stream stream = null;
#pragma warning restore CS0219 // The variable 'stream' is assigned but its value is never used


                byte[] bytes = System.IO.File.ReadAllBytes(fullpath);
                return File(bytes, "application/pdf");
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return View("FileNotFound");
            }
        }
        public ActionResult GetAssOtherfile(string id)
        {

            // var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            // string path = fileAcessingSettings.SettingValue;
            tPaperLaidTemp model = new tPaperLaidTemp();
            model.PaperLaidTempId = Convert.ToInt32(id);
            string FileName = (string)Helper.ExecuteService("Questions", "GetAssOtherfileFromID", model);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string fullpath = System.IO.Path.Combine(FileSettings.SettingValue + FileName);
            //string fullpath = Server.MapPath(FileName);
            //fullpath = fullpath.Replace("~", "");
            try
            {
                //Stream stream = null;


                byte[] bytes = System.IO.File.ReadAllBytes(fullpath);
                return File(bytes, "application/pdf");
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return View("FileNotFound");
            }
        }
        public ActionResult GetDocQfile(string id)
        {

            tQuestion model = new tQuestion();
            model.QuestionID = Convert.ToInt32(id);
            string FileName = (string)Helper.ExecuteService("Questions", "GetAssQuestionDocFromID", model);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string fullpath = System.IO.Path.Combine(FileSettings.SettingValue + FileName);


            try
            {

                Response.ContentType = "application/octet-stream";
                Response.AppendHeader("Content-Disposition", "attachment;filename=" + FileName);
                Response.TransmitFile(fullpath);
                Response.End();
                return new EmptyResult();


            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return View("FileNotFound");
            }
        }
        public ActionResult GetDocOtherfile(string id)
        {

            tPaperLaidTemp model = new tPaperLaidTemp();
            model.PaperLaidTempId = Convert.ToInt32(id);
            string FileName = (string)Helper.ExecuteService("Questions", "GetOtherFileSDocFromID", model);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string fullpath = System.IO.Path.Combine(FileSettings.SettingValue + FileName);


            try
            {

                Response.ContentType = "application/octet-stream";
                Response.AppendHeader("Content-Disposition", "attachment;filename=" + FileName);
                Response.TransmitFile(fullpath);
                Response.End();
                return new EmptyResult();


            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return View("FileNotFound");
            }
        }
        public string SquestionFromLaidPDF(string id, string SDate, Int32 session, Int32 assembly)
        {
            DateTime Sessiondate = new DateTime();
            Sessiondate = Convert.ToDateTime(SDate);
            int day = Sessiondate.Day;
            int month = Sessiondate.Month;
            int year = Sessiondate.Year;
            string mn = "";
            string dd = "";
            if (month < 10)
            {
                mn = "0" + month.ToString();
            }
            else
            {
                mn = month.ToString();
            }
            if (day < 10)
            {
                dd = "0" + day.ToString();
            }
            else
            {
                dd = day.ToString();
            }
            string sesDate = year + "" + "" + mn.Trim() + "" + dd.Trim();

            string fullPath = "";
            if (id != null && SDate != null)
            {

                string url = "/AssemblyFiles/" + assembly + "/" + session + "/" + sesDate + "/Starred/";
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                fullPath = directory + id + ".pdf";

            }
            return fullPath;

        }

        public string USquestionFromLaidPDF(string id, string SDate, Int32 session, Int32 assembly)
        {
            DateTime Sessiondate = new DateTime();
            Sessiondate = Convert.ToDateTime(SDate);
            int day = Sessiondate.Day;
            int month = Sessiondate.Month;
            int year = Sessiondate.Year;
            string mn = "";
            string dd = "";
            if (month < 10)
            {
                mn = "0" + month.ToString();
            }
            else
            {
                mn = month.ToString();
            }
            if (day < 10)
            {
                dd = "0" + day.ToString();
            }
            else
            {
                dd = day.ToString();
            }
            string sesDate = year + "" + "" + mn.Trim() + "" + dd.Trim();

            string fullPath = "";
            if (id != null && SDate != null)
            {

                string url = "/AssemblyFiles/" + assembly + "/" + session + "/" + sesDate + "/Unstarred/";
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                fullPath = directory + id + ".pdf";

            }
            return fullPath;

        }
        public string LobPapersFromLaidPDF(string id, string SDate, Int32 session, Int32 assembly)
        {
            DateTime Sessiondate = new DateTime();
            Sessiondate = Convert.ToDateTime(SDate);
            int day = Sessiondate.Day;
            int month = Sessiondate.Month;
            int year = Sessiondate.Year;
            string mn = "";
            string dd = "";
            if (month < 10)
            {
                mn = "0" + month.ToString();
            }
            else
            {
                mn = month.ToString();
            }
            if (day < 10)
            {
                dd = "0" + day.ToString();
            }
            else
            {
                dd = day.ToString();
            }
            string sesDate = year + "" + "" + mn.Trim() + "" + dd.Trim();

            string fullPath = "";
            if (id != null && SDate != null)
            {
                id = id.Replace('.', '_');
                string url = "/AssemblyFiles/" + assembly + "/" + session + "/" + sesDate + "/Documents/";
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                fullPath = directory + id + ".pdf";

            }
            return fullPath;

        }
        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }

}