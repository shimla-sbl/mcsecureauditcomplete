﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.SessionDateSignature;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using System.IO;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class SessionDateSignatureController : Controller
    {
        //
        // GET: /Admin/SessionDateSignature/

        public ActionResult Index()
        {
            return View();
        }




        public ActionResult GetAllSessionDateSignature()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                var tSessionDateSignature = (List<tSessionDateSignature>)Helper.ExecuteService("SessionDateSignature", "GetAllSessionDateSignature", null);

                var model = tSessionDateSignature.ToViewModel1();
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Deleting the SessionDateSignature Details";
                return View("AdminErrorPage");
            }

        }


        public ActionResult CreateSessionDateSignature()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                SessionDateSignatureViewModel model = new SessionDateSignatureViewModel();

                //Code for AssemblyList 
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                model.AssemblyId = data.AssemblyID;
                model.SessionId = sesdate.SessionId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);


                model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;

                model.Mode = "Add";
                model.IsActive = true;
                return View("CreateSessionDateSignature", model);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Create the SessionDateSignature Details";
                return View("AdminErrorPage");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSessionDateSignature(SessionDateSignatureViewModel model)
        {


            try
            {



                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }



                if (ModelState.IsValid)
                {

                    if (model.Mode == "Add")
                    {
                        var data = model.ToDomainModel();

                        data.CreatedBy = CurrentSession.UserID;
                        data.ModifiedBy = CurrentSession.UserID;

                        Helper.ExecuteService("SessionDateSignature", "AddSessionDateSignature", data);
                    }
                    else
                    {
                        var data = model.ToDomainModel();


                        data.ModifiedBy = CurrentSession.UserID;
                        Helper.ExecuteService("SessionDateSignature", "UpdateSessionDateSignature", data);
                    }
                    return RedirectToAction("GetAllSessionDateSignature");
                }
                else
                {
                    return RedirectToAction("GetAllSessionDateSignature");
                }



            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Saving the SessionDateSignature Details";
                return View("AdminErrorPage");
            }


        }

        public ActionResult EditSessionDateSignature(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                tSessionDateSignature data = new tSessionDateSignature();
                data.SessionDateSignatureDetailsId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString()));

                var serData = (tSessionDateSignature)Helper.ExecuteService("SessionDateSignature", "GetSessionDateSignatureById", data);
                var reldata = serData.ToViewModel1("Edit");
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession sesdata = new mSession();
                sesdata.AssemblyID = reldata.AssemblyId;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", sesdata);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = reldata.SessionId;

                sesdate.AssemblyId = reldata.AssemblyId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);


                reldata.SessionDateList = sessionDateList;
                reldata.AssemblyList = assmeblyList;
                reldata.SessionList = sessionList;

                return View("CreateSessionDateSignature", reldata);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Editing SessionDateSignature Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult DeleteSessionDateSignature(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                tSessionDateSignature data = new tSessionDateSignature();
                data.SessionDateSignatureDetailsId = Id;

                var isSucess = (bool)Helper.ExecuteService("SessionDateSignature", "DeleteSessionDateSignatureById", data);

                return RedirectToAction("GetAllSessionDateSignature");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Deleting the SessionDateSignature Details";
                return View("AdminErrorPage");
            }



        }



        public ActionResult ChangeAssembly(int assemblyId)
        {
            try
            {


                mSession data = new mSession();
                data.AssemblyID = assemblyId;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                return Json(sessionList, JsonRequestBehavior.AllowGet);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Deleting the SessionDateSignature Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult ChangeSession(int sessionId, int assemblyId)
        {

            try
            {

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionId;
                sesdate.AssemblyId = assemblyId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                return Json(sessionDateList, JsonRequestBehavior.AllowGet);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "Their is a Error While Change Session of SessionDateSignature Details";
                return View("AdminErrorPage");
            }
        }

    }
}

