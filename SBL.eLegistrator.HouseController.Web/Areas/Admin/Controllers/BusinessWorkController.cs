﻿using Microsoft.Security.Application;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.salaryhead;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Areas.BusinessWork.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Question;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.tool.xml;
using System.Globalization;
using System.Xml.Serialization;
using System.Xml;
using EvoPdf;
using System.Xml.Linq;
using System.Text;
using SBL.DomainModel.Models.LOB;
using SBL.eLegistrator.HouseController.Web.App_Code;
using SBL.DomainModel.Models.Bill;
//using SBL.DomainModel.Models.PaperLaid;
//using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Filters;
//using SBL.eLegistrator.HouseController.Web.Utility;
//using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Filters;
using System.Text.RegularExpressions;
//using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Member;
using System.Collections.Specialized;
using SBL.DomainModel.Models.Committee;
using Ionic.Zip;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.eFile;
using System.Drawing;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class BusinessWorkController : Controller
    {
        //
        // GET: /BusinessWork/BusinessWork/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CreateLOB()
        {
            //  List<salaryheads> headList = (List<salaryheads>)Helper.ExecuteService("salaryheads", "getSalaryHeadList", null);
            //return PartialView("_sHeadList", headList);
            return View();
        }
        public ActionResult GetAssebmlySessionDetails()
        {
            // List<salaryheads> headList = (List<salaryheads>)Helper.ExecuteService("salaryheads", "getSalaryHeadList", null);
            LoadData();
            // return PartialView("GetAssebmlySessionDetails", headList);
            return View();

        }
        public ActionResult LoadData()
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            //AddBusinessModel AddLOB = new AddBusinessModel();
            AddBusinessModel model = new AddBusinessModel();
            try
            {
                ////Getting the Current assembly AND session.
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

                //System.Data.DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = CurrentSession.AssemblyId;
                string Currentsession = CurrentSession.SessionId;

                //for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                //{
                //    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                //    {
                //        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                //    }
                //    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                //    {
                //        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                //    }
                //}
                model.SessionId = Currentsession;
                model.AssemblyId = CurrentAssembly;
                //addLines.mDepartmentList = DepartmentList;

                ///getting the assembly Information
                methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@AssemblyID", CurrentAssembly));
                DataSet dataSetAssembly = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectAssemblyInfoById", methodParameter);
                model.AssemblyName = Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyName"]);

                ///getting the session information
                methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@SessionId", Currentsession));
                methodParameter.Add(new KeyValuePair<string, string>("@AssemblyID", CurrentAssembly));
                DataSet dataSetSession = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionInfoById", methodParameter);

                List<mSession> ListSession = new List<mSession>();

                mSession Session1 = new mSession();
                Session1.SessionDate = "Select";
                ListSession.Add(Session1);

                for (int i = 0; i < dataSetSession.Tables[0].Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        model.SessionName = Convert.ToString(dataSetSession.Tables[0].Rows[i]["SessionName"]);
                        model.SessionTime = Convert.ToString(dataSetSession.Tables[0].Rows[i]["SessionTimeF"]);
                    }
                    mSession Session = new mSession();
                    Session.SessionDate = Convert.ToDateTime(Convert.ToString(dataSetSession.Tables[0].Rows[i]["SessionDate"])).ToString("dd/MM/yyyy");
                    ListSession.Add(Session);
                }
                model.mSessionList = ListSession;
                /// For Secretary
                /// 
                // SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models.PaperLaidSummaryViewModel model1 = new SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models.PaperLaidSummaryViewModel();

                //if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
                //{
                //    DiaryModel DMdl = new DiaryModel();


                //    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                //    {
                //        DMdl.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                //    }

                //    if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                //    {
                //        DMdl.SessionID = Convert.ToInt32(CurrentSession.SessionId);
                //    }
                //    model1.mAssemblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("LegislationFixation", "GetAssemblyList", null);
                //    model1.sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("LegislationFixation", "GetSessionLsitbyAssemblyId", DMdl);
                //    model1.PendingNoticesAssignCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeAssignPendingCount", DMdl);
                //    model1.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCount", DMdl);
                //    // model.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCountLegislatin", DMdl);
                //    model1.SentNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeSentCount", DMdl);
                //    model1.RecievedNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperRecievedCount", DMdl);
                //    model1.CurrentLobNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperCurrentLobCount", DMdl);
                //    model1.LaidInHomeNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperLaidInHomeCount", DMdl);
                //    ////pending to lay Notice
                //    model1.PendingToLayNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperPendingToLayListCount", DMdl);

                //    model1.RecievedBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperRecievedCount", DMdl);
                //    model1.CurrentLobBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperCurrentLobCount", DMdl);
                //    model1.LaidInHomeBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperLaidInHomeCount", DMdl);
                //    ////Pending to lay Bills
                //    model1.PendingToLayBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperPendingToLayListCount", DMdl);

                //    model1.RecievedCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperRecievedCount", DMdl);
                //    model1.CurrentLobCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperCurrentLobCount", DMdl);
                //    model1.LaidInHomeCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperLaidInHomeCount", DMdl);

                //    ////pending to lay committee
                //    model1.PendingToLayCommitteeCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperPendingToLayListCount", DMdl);

                //    model1.PendingStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPendingCount", DMdl);
                //    model1.SentStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionSentCount", DMdl);
                //    model1.RecievedStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperRecievedCount", DMdl);
                //    model1.CurrentLobStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperCurrentLobCount", DMdl);
                //    model1.LaidInHomeStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperLaidInHomeCount", DMdl);

                //    ////starred Question pending to lay
                //    model1.PendingToLayStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperPendingToLayListCount", DMdl);

                //    model1.PendingUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPendingCount", DMdl);
                //    model1.SentUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionSentCount", DMdl);
                //    model1.RecievedUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperRecievedCount", DMdl);
                //    model1.CurrentLobUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperCurrentLobCount", DMdl);
                //    model1.LaidInHomeUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperLaidInHomeCount", DMdl);
                //    ////pending to lay Unstarred
                //    model1.PendingToLayUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperPendingToLayListCount", DMdl);


                //    model1.RecievedPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayRecievedCount", DMdl);
                //    model1.CurrentLobPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayCurrentLobCount", DMdl);
                //    model1.LaidInHomePaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayLaidInHomeCount", DMdl);
                //    ////Pending to lay
                //    model1.PendingToLayOtherPaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetOtherPaperPendingToLayListCount", DMdl);

                //    //// Fixed and UnFixed Question Count For Starred And UnStarred Question type Created by Sunil on 02-July-2014  
                //    model1.Starredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredFixedQuesCount", DMdl);
                //    model1.StarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredUnFixedQuesCount", DMdl);
                //    model1.Unstarredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredFixedQuesCount", DMdl);
                //    model1.UnstarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredUnFixedQuesCount", DMdl);

                //    model1.StarredCountForWithdrawn = (int)Helper.ExecuteService("LegislationFixation", "GetStarredforWithdrawnCount", DMdl);
                //    model1.StarredWithdrawnCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredListWithdrawnCount", DMdl);
                //    model1.UnstarredCountForWithdrawn = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredforWithdrawnCount", DMdl);
                //    model1.UntarredWithdrawnCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredListWithdrawnCount", DMdl);

                //    tPaperLaidV mdl = new tPaperLaidV();
                //    SBL.DomainModel.Models.SiteSetting.SiteSettings siteSettingMod = new SBL.DomainModel.Models.SiteSetting.SiteSettings();
                //    SBL.DomainModel.Models.Session.mSession Mdl = new SBL.DomainModel.Models.Session.mSession();
                //    SBL.DomainModel.Models.Assembly.mAssembly assmblyMdl = new SBL.DomainModel.Models.Assembly.mAssembly();
                //    tMemberNotice model2 = new tMemberNotice();
                //    CutMotionModel Noticecoiunt = new CutMotionModel();
                //    if (DMdl.AssemblyID != 0 && DMdl.SessionID != 0)
                //    {
                //        mdl.AssemblyCode = DMdl.AssemblyID;
                //        mdl.SessionCode = DMdl.SessionID;

                //        //Add Value for Session object
                //        Mdl.SessionCode = DMdl.SessionID;
                //        Mdl.AssemblyID = DMdl.AssemblyID;
                //        //Add Value for Assembly Object 
                //        assmblyMdl.AssemblyID = DMdl.AssemblyID;
                //        // Add Value for tMemberNoticec Object
                //        model2.AssemblyCode = DMdl.AssemblyID;
                //        model2.SessionID = DMdl.SessionID;
                //        model1.AssemblyID = DMdl.AssemblyID;
                //        model1.SessionID = DMdl.SessionID;

                //    }
                //    else
                //    {
                //        siteSettingMod = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                //        mdl.SessionCode = siteSettingMod.SessionCode;
                //        mdl.AssemblyCode = siteSettingMod.AssemblyCode;
                //        //Add Value for Session object
                //        Mdl.SessionCode = siteSettingMod.SessionCode;
                //        Mdl.AssemblyID = siteSettingMod.AssemblyCode;
                //        //Add Value for Assembly Object 
                //        assmblyMdl.AssemblyID = siteSettingMod.AssemblyCode;
                //        // Add Value for tMemberNoticec Object
                //        model2.AssemblyCode = siteSettingMod.AssemblyCode;
                //        model2.SessionID = siteSettingMod.SessionCode;
                //        model1.AssemblyID = siteSettingMod.AssemblyCode;
                //        model1.SessionID = siteSettingMod.SessionCode;
                //        model1.AccessFilePath = siteSettingMod.AccessFilePath;
                //        CurrentSession.AssemblyId = siteSettingMod.AssemblyCode.ToString();
                //        CurrentSession.SessionId = siteSettingMod.SessionCode.ToString();
                //        CurrentSession.FileAccessPath = siteSettingMod.AccessFilePath;

                //    }

                //    model1.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                //    model1.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
                //    mdl = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "GetUpdatedOtherPaperLaidCounters", mdl);
                //    if (mdl != null)
                //    {
                //        model1.TotalUpdatedFileCounter = mdl.TotalOtherpaperLaidUpdated;

                //    }
                //    SBL.DomainModel.Models.Department.mDepartment deptInfo = new SBL.DomainModel.Models.Department.mDepartment();
                //    if (!string.IsNullOrEmpty(CurrentSession.DeptID))
                //    {
                //        deptInfo.deptId = CurrentSession.DeptID;
                //        deptInfo = (SBL.DomainModel.Models.Department.mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as SBL.DomainModel.Models.Department.mDepartment;
                //    }

                //    if (deptInfo != null)
                //    {
                //        model1.DepartmentName = deptInfo.deptname;
                //    }

                //    model1.CurrentUserName = CurrentSession.UserName;
                //    model1.UserDesignation = CurrentSession.MemberDesignation;
                //    model1.UserName = CurrentSession.Name;
                //    // added by Sameer
                //    // Get the Total count of All Type of question.
                //    model2.QuestionTypeId = (int)QuestionType.StartedQuestion;
                //    model2 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForProofReader", model2);
                //    model1.TotalStaredReceived = model1.TotalStaredReceived;
                //    model2 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForClubbed", model2);
                //    model1.TotalStaredReceivedCl = model1.TotalStaredReceivedCl;
                //    model1.TotalSBracktedQuestion = model1.TotalSBracktedQuestion;
                //    model1.TotalInActiveQuestion = model1.TotalInActiveQuestion;
                //    model1.TotalClubbedQuestion = model1.TotalClubbedQuestion;
                //    model2 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForUnStaredClubbed", model2);
                //    model1.TotalUnStaredReceivedCl = model1.TotalUnStaredReceivedCl;
                //    model1.TotalUBractedQuestion = model1.TotalUBractedQuestion;
                //    model1.UnstaredTotalClubbedQuestion = model1.UnstaredTotalClubbedQuestion;
                //    model2.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                //    model2 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForLegisUnstartQuestion", model2);
                //    model1.TotalUnStaredReceived = model1.TotalUnStaredReceived;
                //    int c = model.TotalStaredReceived + model.TotalUnStaredReceived;

                //    model1.TotalValue = c;
                //    int d = model.TotalStaredReceivedCl + model.TotalClubbedQuestion + model.TotalInActiveQuestion;
                //    model1.TotalValueCl = d;

                //    model.StarredCountExess = (int)Helper.ExecuteService("LegislationFixation", "GetCountQuestionsForExcess", DMdl);
                //    model.StarredCountExessed = (int)Helper.ExecuteService("LegislationFixation", "GetCountExcessedQuestions", DMdl);

                //    model1.UnStarredCountExess = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredCountQuestionsForExcess", DMdl);
                //    model1.UnStarredCountExessed = (int)Helper.ExecuteService("LegislationFixation", "GetCountUnStarredExcessedQuestions", DMdl);

                //    // Add Code for Rejected and StarredToUnstarred Count By type Created by Sunil on 28-July-2014 

                //    model1.StarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredRejectedCount", DMdl);
                //    model1.UnstarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredRejectedCount", DMdl);
                //    model1.StarredToUnstarredCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredToUnstarredCount", DMdl);
                //    model1.ChangedRuleNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetChangedRuleNoticeCount", DMdl);
                //    Noticecoiunt.AssemblyID = model.AssemblyID;
                //    Noticecoiunt.SessionID = model.SessionID;
                //    Noticecoiunt = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesForClubWithBracket", Noticecoiunt);
                //    model1.CountForClub = Noticecoiunt.ResultCount;
                //    Noticecoiunt = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesClubbed", Noticecoiunt);
                //    model1.CountForClubbed = Noticecoiunt.Noticeclubbed;

                //    model1.CustomMessage = (string)TempData["CallingMethod"];
                //    if ((string)TempData["CallingMethod"] == "StarredPending")
                //    {
                //        ViewBag.TagID = "liStarred";
                //    }
                //    else if ((string)TempData["CallingMethod"] == "UnStarredPending")
                //    {
                //        ViewBag.TagID = "liUnstarred";
                //    }
                //    else if ((string)TempData["CallingMethod"] == "NoticePending")
                //    {
                //        ViewBag.TagID = "liNotice";
                //    }

                //    //add code for house committee count by robin - 23-june2017
                //    #region For received list count
                //    string DepartmentId = CurrentSession.DeptID;
                //    string Officecode = CurrentSession.OfficeId;
                //    string AadharId = CurrentSession.AadharId;
                //    string currtBId = CurrentSession.BranchId;
                //    string Year = "5";
                //    string papers = "1";
                //    string[] strngBIDN = new string[2];
                //    strngBIDN[0] = CurrentSession.BranchId;
                //    var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
                //    string[] str = new string[6];
                //    str[0] = DepartmentId;
                //    str[1] = Officecode;
                //    str[2] = AadharId;
                //    str[3] = value123[0];
                //    str[4] = Year;
                //    str[5] = papers;
                //    var ReceivedList = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
                //    model1.ReceivedListCount = ReceivedList.Count;
                //    #endregion

                //    #region For Draft List Count
                //    string[] strD = new string[6];
                //    strD[0] = DepartmentId;
                //    strD[1] = Officecode;
                //    strD[2] = AadharId;
                //    strD[3] = currtBId;
                //    strD[4] = CurrentSession.Designation;
                //    strD[5] = "1";
                //    var DraftListCount = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
                //    model1.DraftListCount = DraftListCount.Count;
                //    #endregion

                //    #region For Send List Count
                //    string[] strS = new string[6];
                //    strS[0] = DepartmentId;
                //    strS[1] = Officecode;
                //    strS[2] = AadharId;
                //    strS[3] = currtBId;
                //    strS[4] = "5";
                //    strS[5] = "1";
                //    List<eFileAttachment> ListModelForSend = new List<eFileAttachment>();
                //    ListModelForSend = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendList", strS);
                //    model1.SentPaperListCount = ListModelForSend.Count;
                //    #endregion
                //}

                // return PartialView("GetAssebmlySessionDetails", Tuple.Create(model, model1));
                tPaperLaidV model1 = new tPaperLaidV();
                //Get the Total count of All Type of question.
                model1 = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model1);
                model.mAssemblyList = model1.mAssemblyList;
                model.sessionList = model1.sessionList;
                return PartialView("GetAssebmlySessionDetails", model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return PartialView("GetAssebmlySessionDetails", model);
            }
        }

        [ValidateInput(false)]
        public ActionResult PartialShowPapers(string TextLOB, string IndexId, string RowIndexId)
        {
            // ConcernedEventId = Sanitizer.GetSafeHtmlFragment(ConcernedEventId);
            tPaperLaidV PaperLaid = new tPaperLaidV();
            tPaperLaidV PaperLaidPartial = new tPaperLaidV();

            List<KeyValuePair<string, string>> mD = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", mD);
            string CurrentAssembly = "";
            string Currentsessions = "";
#pragma warning disable CS0219 // The variable 'OldSession' is assigned but its value is never used
            string OldSession = "";
#pragma warning restore CS0219 // The variable 'OldSession' is assigned but its value is never used
            string PreviousSession = "";
#pragma warning disable CS0219 // The variable 'PreviousAssembly' is assigned but its value is never used
            String PreviousAssembly = "";
#pragma warning restore CS0219 // The variable 'PreviousAssembly' is assigned but its value is never used
            int MarkedPapersCount = 0;
            int UnMarkedPapersCount = 0;
            string Datefrom = "";
            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                {
                    CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }

                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "PreviousSession")
                {
                    PreviousSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                {
                    Currentsessions = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }

            }


            // PaperLaid.mDepartmentList = DepartmentList;
            PaperLaid.AssemblyId = Convert.ToInt32(CurrentAssembly);
            PaperLaid.SessionId = Convert.ToInt32(Currentsessions);
            PaperLaid.EventId = 0;
            List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
            mParameter.Add(new KeyValuePair<string, string>("@Assembly", CurrentAssembly));
            mParameter.Add(new KeyValuePair<string, string>("@session", PreviousSession));
            DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "LastSessionDates", mParameter);
            string Date = Convert.ToString(dataDate.Tables[0].Rows[0]["SessionDate"]);
            DateTime endDate = Convert.ToDateTime(Date);
            if (endDate == DateTime.Now)
            {
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;
                string LastSessionDate = StartDay;
                Datefrom = LastSessionDate;

            }
            else
            {
                endDate = endDate.AddDays(1);
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;
                string LastSessionDate = StartDay;
                Datefrom = LastSessionDate;
            }
            SBL.DomainModel.Models.Session.mSessionDate mSessiondate = new SBL.DomainModel.Models.Session.mSessionDate();
            //string d = "23/08/2018";
            //DateTime Sessiondate = DateTime.ParseExact(d, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //// DateTime Sessiondate = Convert.ToDateTime(d);
            //mSessiondate.SessionDate = Sessiondate;
            //mSessiondate.AssemblyId = 13;
            //var modelReturned = Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate) as SBL.DomainModel.Models.Session.mSessionDate;
            ////  mSessiondate = (mSession)Helpers.Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate);
            //var a = modelReturned.SessionId;
            //int Sessiondateid = Convert.ToInt32(a);


            // string d = "2014-08-06 00:00:00.000";
            // DateTime Sessiondate = Convert.ToDateTime(d);
            // int Sessiondateid = 1692;

            //tPaperLaidV PaperLaid = new tPaperLaidV();
            string Eventid = "4";
            string PaperId = "Pending Papers";
            //string Priority = "ALL";
            //string Datefrom = "05/04/2018";
            //string Dateto = "10/12/2018";
            //string Datefrom = "01/09/2018";
           
            DateTime time = DateTime.Now;
            string Dateto = time.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); //= 09/06/2014
            PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
            PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);



            //string papertypeid = "11";
            PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
            PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);
            //PaperLaid.SessionDate = Sessiondate;
            //PaperLaid.SessionDateId = Sessiondateid;
            PaperLaid.DepartmentId = Eventid;
            if (PaperId == "Pending Papers")
            {
                PaperLaid.IsLaid = false;

            }
            else if (PaperId == "Paper Laid")
            {
                PaperLaid.IsLaid = true;

            }

            if (Eventid != null || Eventid != "")
            {
                PaperLaid.EventId = Convert.ToInt32(Eventid);
            }

            if (PaperLaid.EventId == 4)
            {

                //For Notices

                tMemberNotice PaperLaidNotice = new tMemberNotice();
                PaperLaidNotice.SessionID = Convert.ToInt32(CurrentSession.SessionId);
                PaperLaidNotice.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                if (PaperId == "Pending Papers")
                {
                    PaperLaidNotice.IsSubmitted = true;

                }
                else if (PaperId == "Paper Laid")
                {
                    PaperLaidNotice.IsSubmitted = false;

                }
                PaperLaidNotice = (tMemberNotice)Helper.ExecuteService("PaperLaid", "GetSentNotices", PaperLaidNotice);
                //PaperLaid.ListtPaperLaidVSub = model;
                if (PaperLaidNotice.memberNoticeList != null)
                {


                    for (int i = 0; i < PaperLaidNotice.memberNoticeList.Count; i++)
                    {
                        string noticeno = PaperLaidNotice.memberNoticeList[i].NoticeNumber;
                        if (PaperLaidNotice.memberNoticeList[i].NoticeNumber != null)
                        {
                            string[] tokens = noticeno.Split('/');
                            if (tokens[2].Length == 1)
                            {

                                noticeno = tokens[0] + "/" + tokens[1] + "/" + "0" + tokens[2];
                                PaperLaidNotice.memberNoticeList[i].NoticeNumber = noticeno;
                            }

                        }
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);

                        if (PaperLaidNotice.memberNoticeList[i].IsOnlineSubmitted == true)
                        {
                            OnlineNotices OnlineNotices = new OnlineNotices();
                            OnlineNotices.ManualNoticeId = PaperLaidNotice.memberNoticeList[i].NoticeId;
                            OnlineNotices = (OnlineNotices)Helper.ExecuteService("PaperLaid", "GetOnlineNoticesDetailsById", OnlineNotices);
                            if (OnlineNotices.OriMemFileName != null)
                            {
                                PaperLaidNotice.nFileNameSentByMember = OnlineNotices.OriMemFileName;
                                PaperLaidNotice.nFilePathSentByMember = OnlineNotices.OriMemFilePath;
                                FileSettings.SettingValue = FileSettings.SettingValue.Replace("SecureFileStructure", "");
                                String FullFilePathSentByMember = FileSettings.SettingValue + PaperLaidNotice.nFilePathSentByMember + PaperLaidNotice.nFileNameSentByMember;
                                PaperLaidNotice.memberNoticeList[i].FilePath = FullFilePathSentByMember;
                            }

                        }

                        else
                        {
                            if (PaperLaidNotice.memberNoticeList[i].OriDiaryFileName != null)
                            {
                                String FullFilePath = FileSettings.SettingValue + "/" + PaperLaidNotice.memberNoticeList[i].OriDiaryFilePath + PaperLaidNotice.memberNoticeList[i].OriDiaryFileName;
                                PaperLaidNotice.memberNoticeList[i].FilePath = FullFilePath;
                            }


                        }

                        PaperLaidNotice.memberNoticeList[i].NoticeDiaryDate = PaperLaidNotice.memberNoticeList[i].NoticeDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); //= 09/06/2014

                        if (PaperLaidNotice.memberNoticeList[i].DepartmentId != null)
                        {

                            SBL.DomainModel.Models.Department.mDepartment deptInfo = new SBL.DomainModel.Models.Department.mDepartment();
                            deptInfo.deptId = PaperLaidNotice.memberNoticeList[i].DepartmentId;
                            deptInfo = (SBL.DomainModel.Models.Department.mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as SBL.DomainModel.Models.Department.mDepartment;


                            if (deptInfo != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].DepartmentName = deptInfo.deptname;
                            }
                        }


                        if (PaperLaidNotice.memberNoticeList[i].MemberId != null)
                        {

                            mMember mM = new mMember();
                            mM.MemberID = Convert.ToInt32(PaperLaidNotice.memberNoticeList[i].MemberId);
                            mM = (mMember)Helper.ExecuteService("PaperLaid", "GetMemberByID", mM) as mMember;


                            if (mM != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].MemberName = mM.Name;
                            }
                        }
                        //For Notice Rule type
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                        if (PaperLaidNotice.memberNoticeList[i].NoticeTypeID != null)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                        {
                            SBL.DomainModel.Models.Event.mEvent mM = new SBL.DomainModel.Models.Event.mEvent();
                            mM.EventId = PaperLaidNotice.memberNoticeList[i].NoticeTypeID;
                            mM = (SBL.DomainModel.Models.Event.mEvent)Helpers.Helper.ExecuteService("PaperLaid", "GetNoticeRuleByNoticeTypeID", mM);

                            if (mM != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].RuleNo = mM.RuleNo;
                                PaperLaidNotice.memberNoticeList[i].EventName = mM.EventName;
                            }
                        }

                        ////end
                        //For Marked/Unmarked Count

                        if (PaperLaidNotice.memberNoticeList[i].IsSecChecked == true)
                        {
                            MarkedPapersCount = MarkedPapersCount + 1;
                        }
                        else
                        {
                            UnMarkedPapersCount = UnMarkedPapersCount + 1;
                        }
                        //end


                        //tPaperLaidTemp paperLaidpartial = new tPaperLaidTemp();
                        //// paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                        //if (PaperLaidNotice.memberNoticeList[i].PaperLaidId != null)
                        //{
                        //    paperLaidpartial.PaperLaidTempId = (long)PaperLaidNotice.memberNoticeList[i].PaperLaidId;
                        //    paperLaidpartial = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", paperLaidpartial);
                        //}

                        tPaperLaidV paperLaidpartial = new tPaperLaidV();
                        tPaperLaidTemp pl = new tPaperLaidTemp();
                        // paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                        if (PaperLaidNotice.memberNoticeList[i].PaperLaidId != null)
                        {
                            paperLaidpartial.PaperLaidId = (long)PaperLaidNotice.memberNoticeList[i].PaperLaidId;
                            paperLaidpartial = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidById", paperLaidpartial);

                            pl.PaperLaidTempId = (long)paperLaidpartial.ListtPaperLaidV[0].DeptActivePaperId;
                            pl = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", pl);
                        }
                        if (paperLaidpartial != null)
                        {
                            //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                            //String FullFilePath = FileSettings.SettingValue + "/" + PaperLaidNotice.memberNoticeList[i].OriDiaryFilePath + PaperLaidNotice.memberNoticeList[i].OriDiaryFileName;
                            //PaperLaidNotice.memberNoticeList[i].FilePath = FullFilePath;
                            if (PaperLaidNotice.memberNoticeList[i].SubmittedDate != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].SubmittedDateString = Convert.ToDateTime(PaperLaidNotice.memberNoticeList[i].SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");

                            }
                            if (PaperLaidNotice.memberNoticeList[i].PaperLaidId != null)
                            {
                                if (pl.SignedFilePath != null)
                                {
                                    PaperLaidNotice.memberNoticeList[i].NDate = Convert.ToDateTime(pl.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                                    //PaperLaidNotice.memberNoticeList[i].NDate = Convert.ToString(pl.DeptSubmittedDate);
                                    //PaperLaidNotice.memberNoticeList[i].NoticeDate = Convert.ToDateTime(pl.DeptSubmittedDate);
                                    //PaperLaidNotice.memberNoticeList[i].FilePath = paperLaidpartial.SignedFilePath;
                                    var nFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                                    String nFullFilePath = nFileSettings.SettingValue + "/" + pl.SignedFilePath;

                                    PaperLaidNotice.memberNoticeList[i].NoticeReplyPath = nFullFilePath;
                                }

                            }

                            // PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                            // PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                            // PaperLaidNotice.memberNoticeList[i].FilePath = paperLaidpartial.SignedFilePath;
                        }
                        //else 
                        //{
                        //    PaperLaid.ListtPaperLaidV.RemoveAt(i);
                        //    if (i != 0) {
                        //        i = i + 1;
                        //    }

                        //}

                    }
                }


                List<tMemberNotice> _listNoticepartial = new List<tMemberNotice>();
                if (PaperLaidNotice.memberNoticeList != null)
                {

                    foreach (var item in PaperLaidNotice.memberNoticeList)
                    {
                        string aNoticeDate = Convert.ToDateTime(item.NoticeDate).ToString("dd/MM/yyyy");
                        item.NoticeDiaryDate = aNoticeDate;
                        item.SelectedIndexId = IndexId;
                        item.iSelectedIndexId = Convert.ToInt32(IndexId);
                        _listNoticepartial.Add(item);
                        //_listNoticepartial.Sort();

                    }


                }
                var ass = _listNoticepartial.OrderBy(x => x.MemberName).ToList();
                PaperLaid.ListtPaperLaidVNoticeList = ass;
                PaperLaid.SelectedIndexId = Convert.ToInt32(IndexId);
                PaperLaid.RowIndexId = Convert.ToInt32(RowIndexId);
                PaperLaid.TextLOB = TextLOB;
            }


            tMemberNotice PaperLaidNoticeCount = new tMemberNotice();
            PaperLaidNoticeCount.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            PaperLaidNoticeCount.SessionID = Convert.ToInt32(CurrentSession.SessionId);
            PaperLaid.NoticeCount = (int)Helper.ExecuteService("PaperLaid", "GetNoticeCount", PaperLaidNoticeCount);

            tPaperLaidV PaperLaidOtherpaperCount = new tPaperLaidV();
            PaperLaidOtherpaperCount.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            PaperLaidOtherpaperCount.SessionId = Convert.ToInt32(CurrentSession.SessionId);
            PaperLaidOtherpaperCount.DateFromis = PaperLaid.DateFromis;
            PaperLaidOtherpaperCount.DateTois = PaperLaid.DateTois;
            PaperLaid.OtherPapersCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetOtherpaperCount", PaperLaidOtherpaperCount);

            PaperLaid.CommitteeRepCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetCRCount", PaperLaidOtherpaperCount);

            PaperLaid.BillsCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "BillspaperCount", PaperLaidOtherpaperCount);

            PaperLaid.MarkedPapersCount = MarkedPapersCount;
            PaperLaid.UnMarkedPapersCount = UnMarkedPapersCount;

            return PartialView(PaperLaid);
        }

        public ActionResult getPaperLaidListByEventId(string Eventid, string Priority, string PaperId, string ConcernedDeptId, string Datefrom, string Dateto)
        {
            string d = "2014-08-06 00:00:00.000";
            DateTime Sessiondate = Convert.ToDateTime(d);
            int Sessiondateid = 1692;

            tPaperLaidV PaperLaid = new tPaperLaidV();
            PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
            PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);
            PaperLaid.SessionDate = Sessiondate;
            PaperLaid.SessionDateId = Sessiondateid;
            PaperLaid.DepartmentId = ConcernedDeptId;
            if (PaperId == "Pending Papers")
            {
                PaperLaid.IsLaid = false;

            }
            else if (PaperId == "Paper Laid")
            {
                PaperLaid.IsLaid = true;

            }
            else if (PaperId == "All")
            {
                PaperLaid.IsLaid = null;

            }
            if (Eventid != null || Eventid != "")
            {
                PaperLaid.EventId = Convert.ToInt32(Eventid);
            }
            PaperLaid.SessionId = Convert.ToInt32(CurrentSession.SessionId);
            PaperLaid.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByDeptId2", PaperLaid);
            if (PaperLaid.ListtPaperLaidV != null)
            {

                for (int i = 0; i < PaperLaid.ListtPaperLaidV.Count; i++)
                {


                    tPaperLaidTemp paperLaid = new tPaperLaidTemp();


                    // paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);

                    paperLaid.PaperLaidTempId = (long)PaperLaid.ListtPaperLaidV[i].DeptActivePaperId;
                    paperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", paperLaid);
                    if (paperLaid != null)
                    {

                        PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                        PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");

                        // PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                        // PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                        PaperLaid.ListtPaperLaidV[i].actualFilePath = paperLaid.SignedFilePath;
                    }
                    //else 
                    //{
                    //    PaperLaid.ListtPaperLaidV.RemoveAt(i);
                    //    if (i != 0) {
                    //        i = i + 1;
                    //    }

                    //}

                }
            }
            List<tPaperLaidV> _list = new List<tPaperLaidV>();
            if (PaperLaid.ListtPaperLaidV != null)
            {

                foreach (var item in PaperLaid.ListtPaperLaidV)
                {

                    _list.Add(item);

                }


            }
            PaperLaid.ListtPaperLaidV = _list;

            return PartialView("getPaperLaidListByEventId", PaperLaid);
        }

        public ActionResult GetPaperLaidListBySelectedId(string Eventid, string Priority, string PaperId, string ConcernedDeptId, string Datefrom, string Dateto, string SessionDate, string IndexId, string RowIndexId)
        {

            tPaperLaidV PaperLaid = new tPaperLaidV();
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

            System.Data.DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
            string CurrentAssembly = "";
            string Currentsession = "";
            string PreviousSession = "";
            int MarkedPapersCount = 0;
            int UnMarkedPapersCount = 0;
            
            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                {
                    CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                {
                    Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "PreviousSession")
                {
                    PreviousSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
            }


            List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
            mParameter.Add(new KeyValuePair<string, string>("@Assembly", CurrentAssembly));
            mParameter.Add(new KeyValuePair<string, string>("@session", PreviousSession));
            DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "LastSessionDates", mParameter);
            string Date = Convert.ToString(dataDate.Tables[0].Rows[0]["SessionDate"]);
            DateTime endDate = Convert.ToDateTime(Date);
            if (endDate == DateTime.Now)
            {
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;
                string LastSessionDate = StartDay;
                Datefrom = LastSessionDate;

            }
            else
            {
                endDate = endDate.AddDays(1);
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;
                string LastSessionDate = StartDay;
                Datefrom = LastSessionDate;
            }
          
            DateTime time = DateTime.Now;
            Dateto = time.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); //= 09/06/2014
            PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
            PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);



            //string papertypeid = "11";
            PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
            PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);



           
            PaperLaid.AssemblyId = Convert.ToInt32(CurrentAssembly);
            PaperLaid.SessionId = Convert.ToInt32(Currentsession);
            if (Eventid != null || Eventid != "")
            {
                PaperLaid.EventId = Convert.ToInt32(Eventid);
            }

            if (PaperLaid.EventId == 4)
            {
                int AssemblyId = Convert.ToInt32(CurrentAssembly);
                // tPaperLaidV PaperLaid = new tPaperLaidV();
                tPaperLaidV PaperLaidPartial = new tPaperLaidV();

                SBL.DomainModel.Models.Session.mSessionDate mSessiondate = new SBL.DomainModel.Models.Session.mSessionDate();
                string d = SessionDate;
                if (d == "Select")
                {
                    d = "10/12/2018";

                }
                DateTime Sessiondate = DateTime.ParseExact(d, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                // DateTime Sessiondate = Convert.ToDateTime(d);
                mSessiondate.SessionDate = Sessiondate;
                mSessiondate.AssemblyId = AssemblyId;
                var modelReturned = Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate) as SBL.DomainModel.Models.Session.mSessionDate;
                //  mSessiondate = (mSession)Helpers.Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate);
                var a = modelReturned.SessionId;
                int Sessiondateid = Convert.ToInt32(a);


                PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);
                PaperLaid.SessionDate = Sessiondate;
                PaperLaid.SessionDateId = Sessiondateid;
                PaperLaid.DepartmentId = ConcernedDeptId;

                //else if (PaperId == "All")
                //{
                //    PaperLaid.IsLaid = null;

                //}


                //For Notices

                tMemberNotice PaperLaidNotice = new tMemberNotice();
                PaperLaidNotice.SessionID = Convert.ToInt32(CurrentSession.SessionId);
                PaperLaidNotice.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                if (PaperId == "Pending Papers")
                {
                    PaperLaidNotice.IsSubmitted = true;

                }
                else if (PaperId == "ForLaying")
                {
                    PaperLaidNotice.IsSubmitted = null;
                    PaperLaidNotice.IsLOBAttached = true;
                }
                else if (PaperId == "Paper Laid")
                {
                    PaperLaidNotice.IsSubmitted = false;

                }
                PaperLaidNotice = (tMemberNotice)Helper.ExecuteService("PaperLaid", "GetSentNotices", PaperLaidNotice);
                //PaperLaid.ListtPaperLaidVSub = model;
                if (PaperLaidNotice.memberNoticeList != null)
                {

                    for (int i = 0; i < PaperLaidNotice.memberNoticeList.Count; i++)
                    {

                        string noticeno = PaperLaidNotice.memberNoticeList[i].NoticeNumber;
                        if (PaperLaidNotice.memberNoticeList[i].NoticeNumber != null)
                        {
                            string[] tokens = noticeno.Split('/');
                            if (tokens[2].Length == 1)
                            {

                                noticeno = tokens[0] + "/" + tokens[1] + "/" + "0" + tokens[2];
                                PaperLaidNotice.memberNoticeList[i].NoticeNumber = noticeno;
                            }

                        }
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);

                        if (PaperLaidNotice.memberNoticeList[i].IsOnlineSubmitted == true)
                        {
                            OnlineNotices OnlineNotices = new OnlineNotices();
                            OnlineNotices.ManualNoticeId = PaperLaidNotice.memberNoticeList[i].NoticeId;
                            OnlineNotices = (OnlineNotices)Helper.ExecuteService("PaperLaid", "GetOnlineNoticesDetailsById", OnlineNotices);
                            if (OnlineNotices.OriMemFileName != null)
                            {
                                PaperLaidNotice.nFileNameSentByMember = OnlineNotices.OriMemFileName;
                                PaperLaidNotice.nFilePathSentByMember = OnlineNotices.OriMemFilePath;
                                FileSettings.SettingValue = FileSettings.SettingValue.Replace("SecureFileStructure", "");
                                String FullFilePathSentByMember = FileSettings.SettingValue + PaperLaidNotice.nFilePathSentByMember + PaperLaidNotice.nFileNameSentByMember;
                                PaperLaidNotice.memberNoticeList[i].FilePath = FullFilePathSentByMember;
                            }

                        }

                        else
                        {
                            if (PaperLaidNotice.memberNoticeList[i].OriDiaryFileName != null)
                            {
                                String FullFilePath = FileSettings.SettingValue + "/" + PaperLaidNotice.memberNoticeList[i].OriDiaryFilePath + PaperLaidNotice.memberNoticeList[i].OriDiaryFileName;
                                PaperLaidNotice.memberNoticeList[i].FilePath = FullFilePath;
                            }


                        }




                        PaperLaidNotice.memberNoticeList[i].NoticeDiaryDate = PaperLaidNotice.memberNoticeList[i].NoticeDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); //= 09/06/2014

                        if (PaperLaidNotice.memberNoticeList[i].DepartmentId != null)
                        {

                            SBL.DomainModel.Models.Department.mDepartment deptInfo = new SBL.DomainModel.Models.Department.mDepartment();
                            deptInfo.deptId = PaperLaidNotice.memberNoticeList[i].DepartmentId;
                            deptInfo = (SBL.DomainModel.Models.Department.mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as SBL.DomainModel.Models.Department.mDepartment;


                            if (deptInfo != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].DepartmentName = deptInfo.deptname;
                            }
                        }


                        if (PaperLaidNotice.memberNoticeList[i].MemberId != null)
                        {

                            mMember mM = new mMember();
                            mM.MemberID = Convert.ToInt32(PaperLaidNotice.memberNoticeList[i].MemberId);
                            mM = (mMember)Helper.ExecuteService("PaperLaid", "GetMemberByID", mM) as mMember;


                            if (mM != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].MemberName = mM.Name;
                            }
                        }
                        //For Notice Rule type
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                        if (PaperLaidNotice.memberNoticeList[i].NoticeTypeID != null)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                        {
                            SBL.DomainModel.Models.Event.mEvent mM = new SBL.DomainModel.Models.Event.mEvent();
                            mM.EventId = PaperLaidNotice.memberNoticeList[i].NoticeTypeID;
                            mM = (SBL.DomainModel.Models.Event.mEvent)Helpers.Helper.ExecuteService("PaperLaid", "GetNoticeRuleByNoticeTypeID", mM);

                            if (mM != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].RuleNo = mM.RuleNo;
                                PaperLaidNotice.memberNoticeList[i].EventName = mM.EventName;
                            }
                        }

                        ////end
                        //For Marked/Unmarked Count

                        if (PaperLaidNotice.memberNoticeList[i].IsSecChecked == true)
                        {
                            MarkedPapersCount = MarkedPapersCount + 1;
                        }
                        else
                        {
                            UnMarkedPapersCount = UnMarkedPapersCount + 1;
                        }
                        //end


                        //tPaperLaidTemp paperLaidpartial = new tPaperLaidTemp();
                        //// paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                        //if (PaperLaidNotice.memberNoticeList[i].PaperLaidId != null)
                        //{
                        //    paperLaidpartial.PaperLaidTempId = (long)PaperLaidNotice.memberNoticeList[i].PaperLaidId;
                        //    paperLaidpartial = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", paperLaidpartial);
                        //}

                        tPaperLaidV paperLaidpartial = new tPaperLaidV();
                        tPaperLaidTemp pl = new tPaperLaidTemp();
                        // paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                        if (PaperLaidNotice.memberNoticeList[i].PaperLaidId != null)
                        {
                            paperLaidpartial.PaperLaidId = (long)PaperLaidNotice.memberNoticeList[i].PaperLaidId;
                            paperLaidpartial = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidById", paperLaidpartial);

                            if (paperLaidpartial.DesireLayingDate != null)
                            {
                                DateTime DesireLayingDate = DateTime.ParseExact(paperLaidpartial.DesireLayingDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                PaperLaidNotice.memberNoticeList[i].DesireLayingDateString = DesireLayingDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }

                            if (paperLaidpartial.ListtPaperLaidV[0].LOBRecordId != null)
                            {
                                DraftLOB Dlob = new DraftLOB();
                                Dlob.Id = Convert.ToInt32(paperLaidpartial.ListtPaperLaidV[0].LOBRecordId);
                                Dlob = (DraftLOB)Helpers.Helper.ExecuteService("PaperLaid", "GetLaidDateById", Dlob);
                                if (Dlob.SessionDateLocal == "e")
                                {
                                    //DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                    //DateTime LaidDate = DateTime.ParseExact(Dlob.SessionDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                    //PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                                }
                                else
                                {
                                    DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                    PaperLaidNotice.memberNoticeList[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                                }



                            }
                            pl.PaperLaidTempId = (long)paperLaidpartial.ListtPaperLaidV[0].DeptActivePaperId;
                            pl = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", pl);
                        }
                        if (paperLaidpartial != null)
                        {
                            //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                            //String FullFilePath = FileSettings.SettingValue + "/" + PaperLaidNotice.memberNoticeList[i].OriDiaryFilePath + PaperLaidNotice.memberNoticeList[i].OriDiaryFileName;
                            //PaperLaidNotice.memberNoticeList[i].FilePath = FullFilePath;

                            if (PaperLaidNotice.memberNoticeList[i].SubmittedDate != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].SubmittedDateString = Convert.ToDateTime(PaperLaidNotice.memberNoticeList[i].SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");

                            }


                            if (PaperLaidNotice.memberNoticeList[i].PaperLaidId != null)
                            {
                                if (pl.SignedFilePath != null)
                                {
                                    PaperLaidNotice.memberNoticeList[i].NoticeDate = Convert.ToDateTime(pl.DeptSubmittedDate);
                                    //PaperLaidNotice.memberNoticeList[i].FilePath = paperLaidpartial.SignedFilePath;
                                    var nFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                                    String nFullFilePath = nFileSettings.SettingValue + "/" + pl.SignedFilePath;

                                    PaperLaidNotice.memberNoticeList[i].NoticeReplyPath = nFullFilePath;
                                }

                            }

                            // PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                            // PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                            // PaperLaidNotice.memberNoticeList[i].FilePath = paperLaidpartial.SignedFilePath;
                        }
                        //else 
                        //{
                        //    PaperLaid.ListtPaperLaidV.RemoveAt(i);
                        //    if (i != 0) {
                        //        i = i + 1;
                        //    }

                        //}

                    }
                }


                List<tMemberNotice> _listNoticepartial = new List<tMemberNotice>();
                if (PaperLaidNotice.memberNoticeList != null)
                {

                    foreach (var item in PaperLaidNotice.memberNoticeList)
                    {
                        string aNoticeDate = Convert.ToDateTime(item.NoticeDate).ToString("dd/MM/yyyy");
                        item.NoticeDiaryDate = aNoticeDate;
                        item.SelectedIndexId = IndexId;
                        item.iSelectedIndexId = Convert.ToInt32(IndexId);
                        _listNoticepartial.Add(item);
                        //_listNoticepartial.Sort();

                    }

                    PaperLaid.ListtPaperLaidV = null;
                }
                var SortedList = _listNoticepartial.OrderBy(x => x.MemberName).ToList();
                PaperLaid.ListtPaperLaidVNoticeList = SortedList;
                ViewBag.Message = PaperLaidNotice.IsSubmitted;

                PaperLaid.MarkedPapersCount = MarkedPapersCount;
                PaperLaid.UnMarkedPapersCount = UnMarkedPapersCount;

            }
            else if (PaperLaid.EventId == 0)
            {
                int AssemblyId = Convert.ToInt32(CurrentAssembly);
                // tPaperLaidV PaperLaid = new tPaperLaidV();
                tPaperLaidV PaperLaidPartial = new tPaperLaidV();

                SBL.DomainModel.Models.Session.mSessionDate mSessiondate = new SBL.DomainModel.Models.Session.mSessionDate();
                string d = SessionDate;
                if (d == "Select")
                {
                    //  d = "10/12/2018";
                    //DateTime Sessiondate = DateTime.ParseExact(d, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    // DateTime Sessiondate = Convert.ToDateTime(d);
                    //mSessiondate.SessionDate = Sessiondate;
                    // mSessiondate.AssemblyId = AssemblyId;
                    // var modelReturned = Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate) as SBL.DomainModel.Models.Session.mSessionDate;
                    //  mSessiondate = (mSession)Helpers.Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate);
                    // var a = modelReturned.SessionId;
                    // int Sessiondateid = Convert.ToInt32(a);


                    PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                    PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);
                    //PaperLaid.SessionDate = Sessiondate;
                    //  PaperLaid.SessionDateId = Sessiondateid;
                    PaperLaid.DepartmentId = ConcernedDeptId;
                }
                else
                {
                    DateTime Sessiondate = DateTime.ParseExact(d, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    // DateTime Sessiondate = Convert.ToDateTime(d);
                    mSessiondate.SessionDate = Sessiondate;
                    mSessiondate.AssemblyId = AssemblyId;
                    var modelReturned = Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate) as SBL.DomainModel.Models.Session.mSessionDate;
                    //  mSessiondate = (mSession)Helpers.Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate);
                    var a = modelReturned.SessionId;
                    int Sessiondateid = Convert.ToInt32(a);


                    PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                    PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);
                    PaperLaid.SessionDate = Sessiondate;
                    PaperLaid.SessionDateId = Sessiondateid;
                    PaperLaid.DepartmentId = ConcernedDeptId;
                }

                if (PaperId == "Pending Papers")
                {
                    PaperLaid.IsLaid = false;

                }
                else if (PaperId == "ForLaying")
                {
                    PaperLaid.IsLaid = false;
                    PaperLaid.IsLOBAttached = true;
                }
                else if (PaperId == "Paper Laid")
                {
                    PaperLaid.IsLaid = true;

                }
                PaperLaid.SessionId = Convert.ToInt32(CurrentSession.SessionId);
                PaperLaid.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
                PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByDeptId2", PaperLaid);
                if (PaperLaid.ListtPaperLaidV != null)
                {

                    for (int i = 0; i < PaperLaid.ListtPaperLaidV.Count; i++)
                    {


                        tPaperLaidTemp paperLaid = new tPaperLaidTemp();


                        // paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);

                        paperLaid.PaperLaidTempId = (long)PaperLaid.ListtPaperLaidV[i].DeptActivePaperId;
                        paperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", paperLaid);
                        if (paperLaid != null)
                        {

                            PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                            PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");

                            // PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                            // PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                            PaperLaid.ListtPaperLaidV[i].actualFilePath = paperLaid.SignedFilePath;

                            tPaperLaidV paperLaidpartial = new tPaperLaidV();
                            paperLaidpartial.PaperLaidId = (long)paperLaid.PaperLaidId;
                            paperLaidpartial = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidById", paperLaidpartial);
                            if (PaperLaid.ListtPaperLaidV[i].DesireLayingDate != null)
                            {
                                DateTime DesireLayingDate = DateTime.ParseExact(PaperLaid.ListtPaperLaidV[i].DesireLayingDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = DesireLayingDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        //For Marked/Unmarked
                        if (PaperLaid.ListtPaperLaidV[i].IsVSSecMarked == true)
                        {
                            MarkedPapersCount = MarkedPapersCount + 1;
                        }
                        else
                        {
                            UnMarkedPapersCount = UnMarkedPapersCount + 1;
                        }

                        if (PaperLaid.ListtPaperLaidV[i].LOBRecordId != null)
                        {
                            DraftLOB Dlob = new DraftLOB();
                            Dlob.Id = Convert.ToInt32(PaperLaid.ListtPaperLaidV[i].LOBRecordId);
                            Dlob = (DraftLOB)Helpers.Helper.ExecuteService("PaperLaid", "GetLaidDateById", Dlob);
                            if (Dlob.SessionDateLocal == "e")
                            {
                                //DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                //DateTime LaidDate = DateTime.ParseExact(Dlob.SessionDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                //PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                            }
                            else
                            {
                                //DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                DateTime LaidDate = DateTime.ParseExact(Dlob.SessionDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                            }

                        }
                        //else 
                        //{
                        //    PaperLaid.ListtPaperLaidV.RemoveAt(i);
                        //    if (i != 0) {
                        //        i = i + 1;
                        //    }

                        //}

                    }
                }

                List<tPaperLaidV> _list = new List<tPaperLaidV>();
                if (PaperLaid.ListtPaperLaidV != null)
                {

                    foreach (var item in PaperLaid.ListtPaperLaidV)
                    {

                        _list.Add(item);

                    }


                }
                //var nSortList = _list.OrderBy(x => x.MinisterSubmittedDate).ToList();
                //  var nSortList = _list.OrderBy(e => e.MinisterSubmittedDate).ThenBy(e => e.MinisterSubmittedDate).ToList();
                PaperLaid.ListtPaperLaidVNotice = _list;
                ViewBag.Message = PaperLaid.IsLaid;
                PaperLaid.MarkedPapersCount = MarkedPapersCount;
                PaperLaid.UnMarkedPapersCount = UnMarkedPapersCount;
            }


            else if (PaperLaid.EventId == 5)
            {
                int AssemblyId = Convert.ToInt32(CurrentAssembly);
                // tPaperLaidV PaperLaid = new tPaperLaidV();
                tPaperLaidV PaperLaidPartial = new tPaperLaidV();

                SBL.DomainModel.Models.Session.mSessionDate mSessiondate = new SBL.DomainModel.Models.Session.mSessionDate();
                string d = SessionDate;
                if (d == "Select")
                {
                    PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                    PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);
                    PaperLaid.DepartmentId = ConcernedDeptId;
                }
                else
                {

                    DateTime Sessiondate = DateTime.ParseExact(d, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    // DateTime Sessiondate = Convert.ToDateTime(d);
                    mSessiondate.SessionDate = Sessiondate;
                    mSessiondate.AssemblyId = AssemblyId;
                    var modelReturned = Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate) as SBL.DomainModel.Models.Session.mSessionDate;
                    //  mSessiondate = (mSession)Helpers.Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate);
                    var a = modelReturned.SessionId;
                    int Sessiondateid = Convert.ToInt32(a);
                    PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                    PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);
                    PaperLaid.SessionDate = Sessiondate;
                    PaperLaid.SessionDateId = Sessiondateid;
                    PaperLaid.DepartmentId = ConcernedDeptId;
                }

                if (PaperId == "Pending Papers")
                {
                    PaperLaid.IsLaid = false;

                }
                else if (PaperId == "Paper Laid")
                {
                    PaperLaid.IsLaid = true;

                }
                PaperLaid.SessionId = Convert.ToInt32(CurrentSession.SessionId);
                PaperLaid.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
                PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByDeptId2", PaperLaid);
                if (PaperLaid.ListtPaperLaidV != null)
                {

                    for (int i = 0; i < PaperLaid.ListtPaperLaidV.Count; i++)
                    {


                        tPaperLaidTemp paperLaid = new tPaperLaidTemp();


                        // paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                        if (PaperLaid.ListtPaperLaidV[i].DeptActivePaperId != null)
                        {
                            paperLaid.PaperLaidTempId = (long)PaperLaid.ListtPaperLaidV[i].DeptActivePaperId;
                            paperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", paperLaid);
                            if (paperLaid != null)
                            {

                                tPaperLaidV paperLaidpartial = new tPaperLaidV();
                                paperLaidpartial.PaperLaidId = (long)paperLaid.PaperLaidId;
                                paperLaidpartial = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidById", paperLaidpartial);
                                if (PaperLaid.ListtPaperLaidV[i].DesireLayingDate != null)
                                {
                                    DateTime DesireLayingDate = DateTime.ParseExact(PaperLaid.ListtPaperLaidV[i].DesireLayingDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                    PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = DesireLayingDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }


                                PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                                PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");

                                // PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                                // PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                                PaperLaid.ListtPaperLaidV[i].actualFilePath = paperLaid.SignedFilePath;
                            }
                        }
                        if (PaperLaid.ListtPaperLaidV[i].LOBRecordId != null)
                        {
                            DraftLOB Dlob = new DraftLOB();
                            Dlob.Id = Convert.ToInt32(PaperLaid.ListtPaperLaidV[i].LOBRecordId);
                            Dlob = (DraftLOB)Helpers.Helper.ExecuteService("PaperLaid", "GetLaidDateById", Dlob);
                            if (Dlob.SessionDateLocal == "e")
                            {
                                //DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                //DateTime LaidDate = DateTime.ParseExact(Dlob.SessionDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                //PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                            }
                            else
                            {
                                DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                //  DateTime DesireLayingDate = DateTime.ParseExact(PaperLaid.ListtPaperLaidV[i].DesireLayingDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                            }
                        }

                        //else 
                        //{
                        //    PaperLaid.ListtPaperLaidV.RemoveAt(i);
                        //    if (i != 0) {
                        //        i = i + 1;
                        //    }

                        //}

                    }
                }

                List<tPaperLaidV> _nBilllist = new List<tPaperLaidV>();
                if (PaperLaid.ListtPaperLaidV != null)
                {

                    foreach (var item in PaperLaid.ListtPaperLaidV)
                    {

                        _nBilllist.Add(item);

                    }
                    PaperLaid.ListtPaperLaidV = null;

                }
                var nSortList = _nBilllist.OrderBy(x => x.MemberName).ToList();
                PaperLaid.ListtPaperLaidVBills = nSortList;
                ViewBag.Message = PaperLaid.IsLaid;


            }

            else if (PaperLaid.EventId == 8)
            {
                int AssemblyId = Convert.ToInt32(CurrentAssembly);
                // tPaperLaidV PaperLaid = new tPaperLaidV();
                tPaperLaidV PaperLaidPartial = new tPaperLaidV();

                SBL.DomainModel.Models.Session.mSessionDate mSessiondate = new SBL.DomainModel.Models.Session.mSessionDate();
                string d = SessionDate;
                if (d == "Select")
                {
                    d = "10/12/2018";

                }
                DateTime Sessiondate = DateTime.ParseExact(d, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                // DateTime Sessiondate = Convert.ToDateTime(d);
                mSessiondate.SessionDate = Sessiondate;
                mSessiondate.AssemblyId = AssemblyId;
                var modelReturned = Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate) as SBL.DomainModel.Models.Session.mSessionDate;
                //  mSessiondate = (mSession)Helpers.Helper.ExecuteService("PaperLaid", "GetSessionDateIdByDate", mSessiondate);
                var a = modelReturned.SessionId;
                int Sessiondateid = Convert.ToInt32(a);


                PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);
                PaperLaid.SessionDate = Sessiondate;
                PaperLaid.SessionDateId = Sessiondateid;
                PaperLaid.DepartmentId = ConcernedDeptId;
                tCommitteeReport PaperLaidCR = new tCommitteeReport();
                if (PaperId == "Pending Papers")
                {
                    PaperLaidCR.IsFreeze = false;

                }
                else if (PaperId == "Paper Laid")
                {
                    PaperLaidCR.IsFreeze = true;

                }
                PaperLaidCR.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                PaperLaidCR.SessionID = Convert.ToInt32(CurrentSession.SessionId);
                PaperLaidCR = (tCommitteeReport)Helpers.Helper.ExecuteService("PaperLaid", "GetCommitteeReports", PaperLaidCR);
                if (PaperLaidCR.committeereportList != null)
                {

                    for (int i = 0; i < PaperLaidCR.committeereportList.Count; i++)
                    {
                        if (PaperLaidCR.committeereportList[i].DeptmentID != null)
                        {

                            SBL.DomainModel.Models.Department.mDepartment deptInfo = new SBL.DomainModel.Models.Department.mDepartment();
                            deptInfo.deptId = PaperLaidCR.committeereportList[i].DeptmentID;
                            deptInfo = (SBL.DomainModel.Models.Department.mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as SBL.DomainModel.Models.Department.mDepartment;


                            if (deptInfo != null)
                            {
                                PaperLaidCR.committeereportList[i].DeptmentID = deptInfo.deptname;
                            }
                        }

                        if (PaperLaidCR.committeereportList[i].CommitteeId != null)
                        {
                            string comid = Convert.ToString(PaperLaidCR.committeereportList[i].CommitteeId);
                            List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                            methodParameter1.Add(new KeyValuePair<string, string>("@CommitteeId", comid));
                            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommitteeInfoById", methodParameter1);
                            if (dataSet != null)
                            {
                                PaperLaidCR.committeereportList[i].CommitteeName = Convert.ToString(dataSet.Tables[0].Rows[0]["CommitteeName"]);
                            }
                        }


                        if (PaperLaidCR.committeereportList[i].DateOfLaying != null)
                        {
                            DateTime dt = DateTime.ParseExact(PaperLaidCR.committeereportList[i].DateOfLaying.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                            //string s = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            PaperLaidCR.committeereportList[i].LayingDateString = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                        }

                        tPaperLaidV paperLaidpartial = new tPaperLaidV();
                        tPaperLaidTemp pl = new tPaperLaidTemp();
                        // paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                        if (PaperLaidCR.committeereportList[i].PaperLaidId != null)
                        {
                            paperLaidpartial.PaperLaidId = (long)PaperLaidCR.committeereportList[i].PaperLaidId;
                            paperLaidpartial = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidById", paperLaidpartial);


                            pl.PaperLaidTempId = (long)paperLaidpartial.ListtPaperLaidV[0].DeptActivePaperId;
                            pl = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", pl);
                        }
                        if (paperLaidpartial != null)
                        {
                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                            string nPdfPath = PaperLaidCR.committeereportList[i].PDFPath.Replace("~", "");
                            String FullFilePath = FileSettings.SettingValue + "/" + nPdfPath;
                            PaperLaidCR.committeereportList[i].PDFPath = FullFilePath;
                            //PaperLaidCR.committeereportList[i].DateOfLaying = PaperLaidCR.committeereportList[i].DateOfLaying.ToString("dd/MM/yyyy");


                        }


                    }
                }

                List<tCommitteeReport> _nCRlist = new List<tCommitteeReport>();
                if (PaperLaidCR.committeereportList != null)
                {

                    foreach (var item in PaperLaidCR.committeereportList)
                    {

                        _nCRlist.Add(item);

                    }

                    PaperLaid.ListtPaperLaidV = null;
                }
                var nSortList = _nCRlist.OrderBy(x => x.CommitteeId).ToList();
                PaperLaid.ListtPaperLaidVCommRep = nSortList;
                ViewBag.Message = PaperLaidCR.IsFreeze;

            }
            // return PartialView("PartialShowPapers", PaperLaid);
            PaperLaid.SelectedIndexId = Convert.ToInt32(IndexId);
            PaperLaid.RowIndexId = Convert.ToInt32(RowIndexId);

            return PartialView("getPaperLaidListByEventId", PaperLaid);
        }

        public ActionResult ViewPaperLaid(string PaperLaidTempId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            PaperLaidTempId = Sanitizer.GetSafeHtmlFragment(PaperLaidTempId);
            if (PaperLaidTempId != null && PaperLaidTempId != "")
            {
                tPaperLaidTemp PaperLaid = new tPaperLaidTemp();
                PaperLaid.PaperLaidTempId = Convert.ToInt32(PaperLaidTempId);

                PaperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", PaperLaid);
                string Location = "";

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                Location = System.IO.Path.Combine(FileSettings.SettingValue + PaperLaid.SignedFilePath);
                //int indx = addLOBModel1.PDFLocation.IndexOf("~");
                //if (indx != -1)
                //{

                //    //string filstring = addLOBModel1.PDFLocation.TrimStart('~');
                //    Location = System.IO.Path.Combine(FileSettings.SettingValue + PaperLaid.SignedFilePath);

                //}
                //foreach (var item in PaperLaid.ListPaperLaidTemp)
                //{
                //    Location = item.FilePath;
                //}
                //Location = PaperLaid.FilePath+PaperLaid.FileName;
                //Location = PaperLaid.SignedFilePath;
                return File(Location, "application/pdf");
                // return RedirectToAction("LoadAction", "Module", new { area = "", ActionName = "LoadPDF", ControllerName = "PDFViewer", ModuleName = "PDFViewer", PDFPath = path });
            }
            else
            {
                return null;
            }
        }

        public ActionResult PartialShowIntimationPapers(string ConcernedEventId)
        {
            ConcernedEventId = Sanitizer.GetSafeHtmlFragment(ConcernedEventId);
            tPaperLaidV PaperLaid = new tPaperLaidV();

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            DataSet dataSetDept = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "SelectDepartment", methodParameter);
            List<SBL.DomainModel.Models.Department.mDepartment> DepartmentList = new List<SBL.DomainModel.Models.Department.mDepartment>();
            SBL.DomainModel.Models.Department.mDepartment Department2 = new SBL.DomainModel.Models.Department.mDepartment();
            Department2.deptId = "0";
            Department2.deptname = "Select";

            DepartmentList.Add(Department2);

            for (int i = 0; i < dataSetDept.Tables[0].Rows.Count; i++)
            {
                SBL.DomainModel.Models.Department.mDepartment Department = new SBL.DomainModel.Models.Department.mDepartment();
                Department.deptId = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptId"]);
                //Department.deptname = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptname"]);
                string Dept = Convert.ToString(dataSetDept.Tables[0].Rows[i]["deptname"]);

                if (Dept.Contains(">"))
                {

                    Department.deptname = Dept.Replace(">", "");
                }
                else
                {
                    Department.deptname = Dept;
                }
                DepartmentList.Add(Department);
            }

            List<KeyValuePair<string, string>> mD = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", mD);

            string PreviousSession = "";
            String PreviousAssembly = "";

            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {

                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "PreviousSession")
                {
                    PreviousSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "PreviousAssembly")
                {
                    PreviousAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
            }


            PaperLaid.mDepartmentList = DepartmentList;

            List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
            mParameter.Add(new KeyValuePair<string, string>("@Assembly", PreviousAssembly));
            mParameter.Add(new KeyValuePair<string, string>("@session", PreviousSession));
            DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "LastSessionDates", mParameter);
            string Date = Convert.ToString(dataDate.Tables[0].Rows[0]["SessionDate"]);
            DateTime endDate = Convert.ToDateTime(Date);
            if (endDate == DateTime.Now)
            {
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");

                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;

                string LastSessionDate = StartDay;

            }
            else
            {
                endDate = endDate.AddDays(1);
                string StartDay = (endDate).ToString("dd/MM/yyyy");

                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");

                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;

                string LastSessionDate = StartDay;

            }
            ConcernedEventId = "5";
            PaperLaid.EventId = Convert.ToInt32(ConcernedEventId);
            List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
            methodParameter1.Add(new KeyValuePair<string, string>("@EventId", ConcernedEventId));

            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEventInfoById", methodParameter1);

            if (dataSet != null)
            {
                PaperLaid.EventName = Convert.ToString(dataSet.Tables[0].Rows[0]["EventName"]);
                string extra = "\r\n";
                if (PaperLaid.EventName.Contains(extra))
                {
                    string remove = PaperLaid.EventName.Trim();
                    PaperLaid.EventName = remove;
                }
            }
            return PartialView("PartialShowIntimationPapers", PaperLaid);
        }
        public ActionResult PatialgetIntimationPaperLaidListByEventId(string ConcernedEventId, string PaperId, string ConcernedDeptId, string Datefrom, string Dateto)
        {
            string AssemblyCode = null;
            string SessionCode = null;
            DateTime? DateFrm = new DateTime();
            DateTime? DateTo = new DateTime();
            string EventId = "";
            string PaperType = "";
            string DepartmentId = "";
            tPaperLaidV model = new tPaperLaidV();
            if (string.IsNullOrEmpty(ConcernedEventId))
            {
                EventId = null;
            }
            else
            {
                EventId = ConcernedEventId;
            }
            if (string.IsNullOrEmpty(PaperId))
            {
                PaperType = null;
            }
            else
            {
                PaperType = PaperId;
            }
            if (string.IsNullOrEmpty(ConcernedDeptId) || ConcernedDeptId == "0")
            {
                DepartmentId = null;
            }
            else
            {
                DepartmentId = ConcernedDeptId;
            }
            ////Getting the Current assembly AND session.
            List<KeyValuePair<string, string>> methodParameters = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameters);
            //string CurrentAssembly = "";
            //string Currentsession = "";


            //for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            //{
            //    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
            //    {
            //        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
            //    }
            //    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
            //    {
            //        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
            //    }
            //}
            //AssemblyCode = CurrentAssembly;
            //SessionCode = Currentsession;
            if (!string.IsNullOrEmpty(Datefrom) && !string.IsNullOrEmpty(Dateto))
            {

                DateFrm = new DateTime(Convert.ToInt32(Datefrom.Split('/')[2]), Convert.ToInt32(Datefrom.Split('/')[1]), Convert.ToInt32(Datefrom.Split('/')[0]));
                DateTo = new DateTime(Convert.ToInt32(Dateto.Split('/')[2]), Convert.ToInt32(Dateto.Split('/')[1]), Convert.ToInt32(Dateto.Split('/')[0]));
                DataSet SummaryDataSet = new DataSet();
                List<tPaperLaidV> _list = new List<tPaperLaidV>();

                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@AssemblyCode", AssemblyCode));
                methodParameter.Add(new KeyValuePair<string, string>("@SessionCode", SessionCode));
                methodParameter.Add(new KeyValuePair<string, string>("@DateFrom", DateFrm.ToString()));
                methodParameter.Add(new KeyValuePair<string, string>("@DateTo", DateTo.ToString()));
                methodParameter.Add(new KeyValuePair<string, string>("@EventId", EventId));
                methodParameter.Add(new KeyValuePair<string, string>("@PaperType", PaperType));
                methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", DepartmentId));

                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[GetFilteredPaperLaidRecord]", methodParameter);
                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {

                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        tPaperLaidV mel = new tPaperLaidV();
                        mel.DeparmentName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["DeparmentName"]);
                        mel.Title = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["Title"]);
                        mel.PaperLaidId = Convert.ToInt32(SummaryDataSet.Tables[0].Rows[i]["PaperLaidId"]);
                        mel.actualFilePath = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SignedFilePath"]);
                        mel.PaperLaidTempId = Convert.ToInt32(SummaryDataSet.Tables[0].Rows[i]["PaperLaidTempId"]);
                        mel.FileVersion = Convert.ToInt32(SummaryDataSet.Tables[0].Rows[i]["Version"]);
                        mel.DeptActivePaperId = Convert.ToInt32(SummaryDataSet.Tables[0].Rows[i]["DeptActivePaperId"]);
                        string ddd = SummaryDataSet.Tables[0].Rows[i]["DeptSubmittedDate"].ToString();
                        if (!string.IsNullOrEmpty(ddd))
                        {
                            mel.DeptSubmittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["DeptSubmittedDate"]);
                        }
                        _list.Add(mel);
                    }
                }
                model.ListtPaperLaidV = _list;
                model.ListtPaperLaidV = model.ListtPaperLaidV.OrderBy(z => z.PaperLaidId).GroupBy(x => x.PaperLaidId).Select(y => y.First()).ToList();
                List<tPaperLaidV> testlist1 = new List<tPaperLaidV>();
                testlist1 = _list.OrderByDescending(s => s.PaperLaidTempId).ToList();
                foreach (var val in model.ListtPaperLaidV)
                {
                    List<tPaperLaidV> _Sublist = new List<tPaperLaidV>();
                    foreach (var val1 in testlist1)
                    {
                        if (val1.PaperLaidId == val.PaperLaidId)
                        {
                            tPaperLaidV mel = new tPaperLaidV();
                            mel.PaperLaidId = val1.PaperLaidId;
                            mel.actualFilePath = val1.actualFilePath;
                            mel.PaperLaidTempId = val1.PaperLaidTempId;
                            mel.FileVersion = val1.FileVersion;
                            mel.DeptActivePaperId = val1.DeptActivePaperId;
                            mel.DeptSubmittedDate = val1.DeptSubmittedDate;
                            _Sublist.Add(mel);
                        }
                    }
                    val.ListtPaperLaidVSub = _Sublist;

                }

            }

            // return PartialView("_getListEventId", PaperLaid);
            return PartialView("PatialgetIntimationPaperLaidListByEventId", model);
        }

        [HttpPost]
        public JsonResult InsertCustomers(List<tPaperLaidV> yearList)
        {
            tPaperLaidV PaperLaid = new tPaperLaidV();
            PaperLaid.TextLOB = yearList[0].TextLOB;
            return Json(yearList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InsertCustomers(IEnumerable<string> array)
        {
            using (BusinessWorkController entities = new BusinessWorkController())
            {
                //Truncate Table to delete all old records.
                // entities.Database.ExecuteSqlCommand("TRUNCATE TABLE [Customers]");

                //Check for NULL.
                //if (customers == null)
                //{
                //    customers = new List<AddBusinessModel>();
                //}

                ////Loop and insert records.
                //foreach (AddBusinessModel customer in customers)
                //{
                //    entities.Customers.Add(customer);
                //}
                int insertedRecords = 1;
                return Json(insertedRecords);
            }
        }

        public void InsertLOB(AddLOBModel model)
        {


            string Location = "";
            if (model.NewPdfPath != "File Name" && model.NewPdfPath !="")
            {
                Location = model.NewPdfPath;

                string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

                foreach (char c in invalid)
                {
                    Location = Location.Replace(c.ToString(), "");
                }

            }
            int AssemblyId = int.Parse(CurrentSession.AssemblyId);
            int SessionId = int.Parse(CurrentSession.SessionId);


            AddLOBModel addLines = new AddLOBModel();
            addLines.SrNo1 = model.SrNo1;
            addLines.LOBId = model.LOBId;
            addLines.SrNo2 = model.SrNo2;
            addLines.SrNo3 = model.SrNo3;
            addLines.TextLOB = model.TextLOB;
            addLines.RecordId = "";
            addLines.AssemblyId = Convert.ToString(AssemblyId);
            addLines.SessionId = Convert.ToString(SessionId);
            addLines.Sessiondate = model.Sessiondate;
            addLines.PageBreak = model.PageBreak;
            addLines.ConcernedEventId = model.ConcernedEventId;
            if (model.PaperLaidId != null)
            {
                addLines.PaperLaidId = model.PaperLaidId;
            }
            try
            {
                if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }

                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

                string Sdate = addLines.Sessiondate;
                Sdate = Sdate.Replace('/', ' ');
                //TempData["addLines"] = Sdate;

                //if (addLines.RecordId == null || addLines.RecordId == "")
                //{
                //    if (addLines.LOBId == null || addLines.LOBId == "")
                //    {
                //        methodParameter = new List<KeyValuePair<string, string>>();
                //        DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectMaxLOBId", methodParameter);
                //        if (dataSetLOB != null)
                //        {
                //            if (Convert.ToString(dataSetLOB.Tables[0].Rows[0][0]) != "")
                //            {
                //                addLines.LOBId = Convert.ToString(Convert.ToInt32(dataSetLOB.Tables[0].Rows[0][0]) + 1);
                //            }
                //            else
                //            {
                //                addLines.LOBId = "1";
                //            }
                //        }
                //    }
                //}

                methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBId", addLines.LOBId));
                string extra = "<b>";
                string removediv = "<div>";

                if (addLines.TextLOB != null)
                {
                    if (addLines.TextLOB.Contains(removediv))
                    {
                        string a = addLines.TextLOB.Replace("<div>", " ");
                        string b = a.Replace("</div>", " ");
                        // addLines.TextLOB = a;
                        addLines.TextLOB = b;
                    }
                    if (addLines.TextLOB.Contains(extra))
                    {
                        string a = addLines.TextLOB.Replace("<b>", "<strong>");
                        string b = a.Replace("</b>", "</strong>&nbsp;");
                        // addLines.TextLOB = a;
                        addLines.TextLOB = b;
                    }
                }

                if (addLines.AssemblyId != null && addLines.AssemblyId != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@AssemblyId", addLines.AssemblyId));
                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyID", addLines.AssemblyId));
                    DataSet dataSetAssembly = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectAssemblyInfoById", methodParameter1);

                    if (dataSetAssembly != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@AssemblyName", Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyName"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@AssemblyNameLocal", Convert.ToString(dataSetAssembly.Tables[0].Rows[0]["AssemblyNameLocal"])));
                    }
                }

                if (addLines.SessionId != null && addLines.SessionId != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionId", addLines.SessionId));
                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionId", addLines.SessionId));
                    methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyID", addLines.AssemblyId));
                    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionInfoById", methodParameter1);

                    if (dataSet != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionName", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionName"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionNameLocal"])));
                    }
                }
                //   string Sessiondate="";
                if (addLines.Sessiondate != null && addLines.Sessiondate != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", addLines.Sessiondate));
                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionDate", addLines.Sessiondate));
                    methodParameter1.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
                    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionDateInfo", methodParameter1);

                    if (dataSet != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionDateLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionDateLocal"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionTime", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionTime"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@SessionTimeLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["SessionTimeLocal"])));
                    }
                }

                if (addLines.SrNo1 != null && addLines.SrNo1 != "Select" && addLines.SrNo1 != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo1", addLines.SrNo1));
                }
                if (addLines.SrNo2 != null && addLines.SrNo2 != "Select" && addLines.SrNo2 != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo2", addLines.SrNo2));
                }
                if (addLines.SrNo3 != null && addLines.SrNo3 != "Select" && addLines.SrNo3 != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@SrNo3", addLines.SrNo3));
                }

                if (addLines.TextLOB != null && addLines.TextLOB != "")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@TextLOB", addLines.TextLOB));
                }

                #region Commented code for Concerned Data


                #endregion
                if (addLines.ConcernedCommitteeId != null && addLines.ConcernedCommitteeId != "0")
                {

                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeId", addLines.ConcernedCommitteeId));
                    methodParameter.Add(new KeyValuePair<string, string>("@CommitteeReportTitle", addLines.CommitteeReportTitle));

                }

                if (addLines.ConcernedCommitteeRepTypId != null && addLines.ConcernedCommitteeRepTypId != "0")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedCommitteeRepTypId", addLines.ConcernedCommitteeRepTypId));

                }

                if (addLines.ConcernedDeptId != null && addLines.ConcernedDeptId != "0")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedDeptId", addLines.ConcernedDeptId));
                }





                if (addLines.ConcernedEventId != null && addLines.ConcernedEventId != "0")
                {
                    //// getting member Info
                    methodParameter.Add(new KeyValuePair<string, string>("@ConcernedEventId", addLines.ConcernedEventId));

                    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                    methodParameter1.Add(new KeyValuePair<string, string>("@EventId", addLines.ConcernedEventId));
                    // methodParameter1.Add(new KeyValuePair<string, string>("@AssemblyId", addLines.AssemblyId));
                    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEventInfoById", methodParameter1);

                    if (dataSet != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedEventName", Convert.ToString(dataSet.Tables[0].Rows[0]["EventName"])));
                        methodParameter.Add(new KeyValuePair<string, string>("@ConcernedEventNameLocal", Convert.ToString(dataSet.Tables[0].Rows[0]["EventNameLocal"])));
                    }
                }

                methodParameter.Add(new KeyValuePair<string, string>("@EVoting", Convert.ToString(addLines.IsEVoting)));
                methodParameter.Add(new KeyValuePair<string, string>("@PageBreak", Convert.ToString(addLines.PageBreak)));

                DataSet dataSetLine = null;
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                if ((addLines.PaperLaidIdTemp != null && addLines.PaperLaidIdTemp != "" && addLines.PaperLaidId != null && addLines.PaperLaidId != "") && (addLines.BillTextNumber != null && addLines.BillTextNumber != "" && addLines.BillNumberYear != null && addLines.BillNumberYear != 0))
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                {
                    if (addLines.BillTextNumber != null)
                    {
                        addLines.BillTextNumber = addLines.BillTextNumber.Trim() + " of " + addLines.BillNumberYear;
                        methodParameter.Add(new KeyValuePair<string, string>("@BillNo", Convert.ToString(addLines.BillTextNumber)));
                    }
                }
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                if ((addLines.PaperLaidIdTemp == null || addLines.PaperLaidIdTemp == "" && addLines.PaperLaidId == null || addLines.PaperLaidId == "") && (addLines.BillTextNumber != null && addLines.BillTextNumber != "" && addLines.BillNumberYear != null && addLines.BillNumberYear != 0))
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                {
                    if (addLines.BillTextNumber != null)
                    {
                        addLines.BillTextNumber = addLines.BillTextNumber.Trim() + " of " + addLines.BillNumberYear;
                        methodParameter.Add(new KeyValuePair<string, string>("@BillNo", Convert.ToString(addLines.BillTextNumber)));
                    }
                }
                if (addLines.RecordId == null || addLines.RecordId == "")
                {
                    if (addLines.TextLOB != "" && addLines.TextLOB != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", Utility.CurrentSession.UserID));
                        methodParameter.Add(new KeyValuePair<string, string>("@CreatedDate", Convert.ToString(System.DateTime.Now)));
                        // methodParameter.Add(new KeyValuePair<string, string>("@PaperLaidId", Convert.ToString(addLines.PaperLaidId)));
                        // dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLOBLineRecord", methodParameter);

                        ErrorLog.WriteToLog("methodParameter :" + methodParameter);
                        dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertLOBLineRecordAdmin", methodParameter);
                        //  UpdatetPaperLaidVs(addLines.Sessiondate);
                        // addLines.RecordId = Convert.ToString(dataSetLine.Tables[0].Rows[0][0]);
                        if (dataSetLine.Tables[0].Rows.Count > 0)
                        {
                            String LOBRecordId = Convert.ToString(dataSetLine.Tables[0].Rows[0][0]);
                            if (addLines.PaperLaidId != null)
                            {
                                List<KeyValuePair<string, string>> param = new List<KeyValuePair<string, string>>();
                                param.Add(new KeyValuePair<string, string>("@LOBRecordId", LOBRecordId));
                                param.Add(new KeyValuePair<string, string>("@PaperLaidId", addLines.PaperLaidId));
                                DataSet getdataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Update_PaperLaidVS_OnSave", param);
                            }

                        }

                    }
                }
                else
                {
                    if (addLines.TextLOB != "" && addLines.TextLOB != null)
                    {
                        methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));
                        methodParameter.Add(new KeyValuePair<string, string>("@Id", addLines.RecordId));
                        dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBLineRecord", methodParameter);
                    }
                }




                if (Location != "")
                {

                    //    methodParameter = new List<KeyValuePair<string, string>>();

                    //    DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                    //    string CurrentAssembly = "";
                    //    string Currentsession = "";

                    //    for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                    //    {
                    //        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    //        {
                    //            CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    //        }
                    //        if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    //        {
                    //            Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    //        }
                    //    }

                    string Sessiondate = addLines.Sessiondate;
                    Sessiondate = Sessiondate.Replace('/', ' ');



                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string directory = FileSettings.SettingValue + "/LOB/" + AssemblyId + "/" + SessionId + "/" + Sessiondate + "/Documents/";

                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }



                    string fileName = "";
                    if (addLines.SrNo1 != null && addLines.SrNo1 != "Select" && addLines.SrNo1 != "")
                    {
                        fileName = addLines.SrNo1;
                    }
                    if (addLines.SrNo2 != null && addLines.SrNo2 != "Select" && addLines.SrNo2 != "")
                    {
                        fileName = fileName + "_" + addLines.SrNo2;
                    }
                    if (addLines.SrNo3 != null && addLines.SrNo3 != "Select" && addLines.SrNo3 != "")
                    {
                        fileName = fileName + "_" + addLines.SrNo3;
                    }
                    fileName = fileName + ".pdf";
                    string sourcedirectory = Server.MapPath("~/LOBTemp/");
                    string sourceFile = System.IO.Path.Combine(sourcedirectory, Location);
                    string destFile = System.IO.Path.Combine(directory, fileName);

                    System.IO.File.Copy(sourceFile, destFile, true);

                    string recordId = addLines.RecordId;

                    if (recordId == null || recordId == "")
                    {
                        recordId = Convert.ToString(dataSetLine.Tables[0].Rows[0][0]);
                    }
                    string savepath = "/LOB/" + AssemblyId + "/" + SessionId + "/" + Sessiondate + "/Documents/";
                    string File = System.IO.Path.Combine(savepath, fileName);

                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@PDFLocation", File));
                    methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));
                    methodParameter.Add(new KeyValuePair<string, string>("@LOBId", addLines.LOBId));
                    methodParameter.Add(new KeyValuePair<string, string>("@Id", recordId));

                    dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentPDF", methodParameter);

                }
                else
                {
                    //string File = "";
                    //string recordId = addLines.RecordId;
                    //methodParameter = new List<KeyValuePair<string, string>>();
                    //methodParameter.Add(new KeyValuePair<string, string>("@PDFLocation", File));
                    //methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", Utility.CurrentSession.UserID));
                    //methodParameter.Add(new KeyValuePair<string, string>("@LOBId", addLines.LOBId));
                    //methodParameter.Add(new KeyValuePair<string, string>("@Id", recordId));

                    //dataSetLine = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateLOBDocumentPDF", methodParameter);
                }


            }
            catch (Exception ex)
            {
                ErrorLog.WriteToLog(ex, "InsertLOB");
            }
            // string jsonData = "Save Successfully";
            // return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult DisplayResult(List<AddLOBModel> stulist)
        {
            String LOBId = "";
            String SessionDate = "";
            int count = 0;
            try
            {



             
                foreach (var cust in stulist)
                {
                    if (count == 0)
                    {
                        var check = CheckSessionDateLOB(cust.Sessiondate);
                        if (check == true)
                        {
                            SessionDate = cust.Sessiondate;
                            List<KeyValuePair<string, string>> tParam = new List<KeyValuePair<string, string>>();
                            tParam.Add(new KeyValuePair<string, string>("@SessionDate", cust.Sessiondate));
                            tParam.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
                            DataSet getdataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLOBFromSessionDate", tParam);

                            List<AddLOBModel> List = new List<AddLOBModel>();
                            // string LOBId = "";
                            if (getdataSetLOB.Tables[0].Rows.Count > 0)
                            {
                                //List<DataRow> list = dataSetLOB.Tables[0].AsEnumerable().ToList();
                                // LOBId = Convert.ToString(Convert.ToInt32(dataSetLOB.Tables[0].Rows[0][1]));
                                for (int i = 0; i < getdataSetLOB.Tables[0].Rows.Count; i++)
                                {
                                    String LOBRecordId = Convert.ToString(getdataSetLOB.Tables[0].Rows[i]["Id"]);
                                    List<KeyValuePair<string, string>> param1 = new List<KeyValuePair<string, string>>();
                                    param1.Add(new KeyValuePair<string, string>("@LOBRecordId", LOBRecordId));
                                    DataSet getdataSet1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetPaperIdbyRecordId", param1);
                                    if (getdataSet1.Tables[0].Rows.Count > 0)
                                    {

                                        string PaperLaidId = Convert.ToString(getdataSetLOB.Tables[0].Rows[0]["PaperLaidId"]);
                                        List<KeyValuePair<string, string>> param = new List<KeyValuePair<string, string>>();
                                        param.Add(new KeyValuePair<string, string>("@LOBRecordId", null));
                                        param.Add(new KeyValuePair<string, string>("@PaperLaidId", PaperLaidId));
                                        DataSet getdataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Update_PaperLaidVS_OnSave", param);


                                    }

                                    //String PaperLaidId = Convert.ToString(getdataSetLOB.Tables[0].Rows[i]["PaperLaidId"]);
                                    //if (PaperLaidId.Length > 0)
                                    //{
                                    //    //String LOBRecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                                    //    //String PaperLaidId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PaperLaidId"]);
                                    //    List<KeyValuePair<string, string>> param = new List<KeyValuePair<string, string>>();
                                    //    param.Add(new KeyValuePair<string, string>("@LOBRecordId", null));
                                    //    param.Add(new KeyValuePair<string, string>("@PaperLaidId", PaperLaidId));
                                    //    DataSet getdataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Update_PaperLaidVS_OnFreeze", param);
                                    //}


                                }

                            }
                            List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                            methodParameter1.Add(new KeyValuePair<string, string>("@SessionDate", cust.Sessiondate));
                            methodParameter1.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
                            DataSet LOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_DeleteLOBData", methodParameter1);
                            LOBId = Convert.ToString(Convert.ToInt32(LOB.Tables[0].Rows[0][0]));
                        }
                        else
                        {
                            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                            methodParameter = new List<KeyValuePair<string, string>>();
                            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectMaxLOBId", methodParameter);
                            if (dataSetLOB != null)
                            {
                                if (Convert.ToString(dataSetLOB.Tables[0].Rows[0][0]) != "")
                                {
                                    LOBId = Convert.ToString(Convert.ToInt32(dataSetLOB.Tables[0].Rows[0][0]) + 1);
                                }
                                else
                                {
                                    //LOBId = "1";
                                }
                            }
                        }
                    }
                    AddLOBModel obj = new AddLOBModel();
                    obj.LOBId = LOBId;
                    ErrorLog.WriteToLog("obj.LOBId :" + obj.LOBId);
                    obj.NewPdfPath = Convert.ToString(cust.NewPdfPath).Trim();
                    obj.SrNo1 = cust.SrNo1;
                    ErrorLog.WriteToLog("obj.SrNo1 :" + obj.SrNo1);
                    obj.SrNo2 = cust.SrNo2;
                    ErrorLog.WriteToLog("obj.SrNo2 :" + obj.SrNo2);
                    obj.SrNo3 = cust.SrNo3;
                    ErrorLog.WriteToLog("obj.SrNo3 :" + obj.SrNo3);
                    if (obj.SrNo3 != null)
                    {
                        //(i)
                        string yourString = obj.SrNo3.Replace("(", string.Empty).Replace(")", string.Empty);
                        //string Sr3 = obj.SrNo3.Remove('(');
                        //string SR3 = Sr3.Remove(')');
                        int SRNO3 = RomanToArabic(yourString);
                        obj.SrNo3 = Convert.ToString(SRNO3);
                    }

                    obj.TextLOB = cust.TextLOB;
                    ErrorLog.WriteToLog("obj.TextLOB :" + obj.TextLOB);
                    obj.Sessiondate = cust.Sessiondate;
                    ErrorLog.WriteToLog("obj.Sessiondate :" + obj.Sessiondate);
                    obj.ConcernedEventId = cust.ConcernedEventId;
                    ErrorLog.WriteToLog("obj.Sessiondate :" + obj.ConcernedEventId);
                    obj.PageBreak = cust.PageBreak;
                    ErrorLog.WriteToLog("obj.PageBreak :" + obj.PageBreak);
                    if (cust.PaperLaidId != null)
                    {
                        obj.PaperLaidId = cust.PaperLaidId.Replace('"', ' ');
                        ErrorLog.WriteToLog("obj.PaperLaidId :" + obj.PaperLaidId);
                        obj.PaperLaidId = obj.PaperLaidId.Trim();
                        ErrorLog.WriteToLog("obj.PaperLaidId :" + obj.PaperLaidId);
                    }



                    InsertLOB(obj);
                    
                    count = count + 1;
                }
                ApproveLOB(LOBId);
                TransferringFileForAssemblyHouse(stulist[0].Sessiondate, CurrentSession.AssemblyId, CurrentSession.SessionId);
                ErrorLog.WriteToLog("SessionDate :" + SessionDate);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteToLog(ex, "DisplayResult error");

            }

            //return Json(stulist, JsonRequestBehavior.AllowGet);
            return Json(SessionDate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            String pfileName = "";
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    NameValueCollection PFileName = Request.Form;
                    foreach (var key in PFileName.AllKeys)
                    {
                        pfileName = PFileName[key];

                        // etc.
                    }


                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    //for (int i = 0; i < files.Count; i++)
                    //{
                    //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                    //string filename = Path.GetFileName(Request.Files[i].FileName);  

                    //foreach (var key in files.AllKeys)
                    //{
                    //    var value = files[key];
                    //    // etc.
                    //}
                    //foreach (var key in files.Keys)
                    //{
                    //    var value = files[key.ToString()];
                    //    // etc.
                    //}
                    HttpPostedFileBase file = files[0];
                    string fname;


                    // Checking for Internet Explorer  

                    fname = file.FileName;
                    //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    //DirectoryInfo AssemblyDir = new DirectoryInfo(FileSettings.SettingValue + "\\nListOfBusiness\\" + sessionDate + "\\Documents\\");  
                    ////DirectoryInfo MDir = new DirectoryInfo(disFileSettings.SettingValue);
                    //if (!AssemblyDir.Exists)
                    //{
                    //    AssemblyDir.Create();
                    //}



                    //Code for Assembly Folder File Existense Check
                    pfileName = Convert.ToString(DateTime.Now.ToFileTime()) + ".pdf";
                    var filepath = System.IO.Path.Combine("\\LOBTemp\\" + pfileName);
                    // Get the complete folder path and store the file inside it.  
                    fname = Path.Combine(Server.MapPath(filepath));
                    file.SaveAs(fname);
                    // }
                    // Returns message that successfully uploaded  
                    return Json(pfileName, "File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        [HttpGet]
        public FileResult GetReport(string parameter)
        {


            //string a = Request.QueryString["parameter"];



            var filepath = System.IO.Path.Combine("\\LOBTemp\\" + parameter.Trim());
            // Get the complete folder path and store the file inside it.  
            string fname = Path.Combine(Server.MapPath(filepath));
            string ReportURL = fname;
            byte[] FileBytes = System.IO.File.ReadAllBytes(ReportURL);
            return File(FileBytes, "application/pdf");
        }



        public JsonResult TransferringFileForAssemblyHouse(string SessionDate, string AssemblyId, string SessionId)
        {
            DateTime Sessiondate = new DateTime();
            StringBuilder ErorLists = new StringBuilder();
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageCulture == "hi-IN")
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                Sessiondate = Convert.ToDateTime(Date);
            }
            else
            {
                var Date = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Sessiondate = Convert.ToDateTime(Date);
            }
            try
            {
                int Count = 0;
                DraftLOB DraftLob = new DraftLOB();
                List<QuestionModelCustom> ListOtherpaper = new List<QuestionModelCustom>();
                DraftLob.SessionDate = Sessiondate;
                ListOtherpaper = (List<QuestionModelCustom>)Helper.ExecuteService("LOB", "GetDraftLobLatestPapersLaid", DraftLob);
                if (ListOtherpaper.Count > 0)
                {
                    for (int i = 0; i < ListOtherpaper.Count; i++)
                    {
                        string[] dateArr = SessionDate.Split('/');
                        string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                        string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/OnlineDocuments/";
                        //string urlDoc = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/OnlineDocuments/DocFile";
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                        // string Docdirectory = System.IO.Path.Combine(FileSettings.SettingValue + urlDoc);
                        if (i == 0)
                        {
                            if (Directory.Exists(directory))
                            {

                            }
                            else
                            {
                                Directory.CreateDirectory(directory);
                            }
                            //if (Directory.Exists(Docdirectory))
                            //{

                            //}
                            //else
                            //{
                            //    Directory.CreateDirectory(Docdirectory);
                            //}
                        }

                        if (ListOtherpaper[i].FileName != null && ListOtherpaper[i].FileName != "")
                        {

                            string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                            if (ListOtherpaper[i].SrNo1 != null && ListOtherpaper[i].SrNo1.ToString().Trim() != "")
                            {
                                fileName = ListOtherpaper[i].SrNo1.ToString().Trim();
                            }
                            if (ListOtherpaper[i].SrNo2 != null && ListOtherpaper[i].SrNo2.ToString().Trim() != "")
                            {
                                fileName = fileName + "_" + ListOtherpaper[i].SrNo2.ToString().Trim();
                            }
                            if (ListOtherpaper[i].SrNo3 != null && ListOtherpaper[i].SrNo3.ToString().Trim() != "")
                            {
                                fileName = fileName + "_" + ListOtherpaper[i].SrNo3;
                            }
                            fileName = fileName + ".pdf";

                            var path = System.IO.Path.Combine(directory, fileName);
                            if (ListOtherpaper[i].FileName.Contains("~"))
                            {
                                ListOtherpaper[i].FileName = ListOtherpaper[i].FileName.Substring(1, ListOtherpaper[i].FileName.Length - 1);
                            }

                            string From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].FileName);

                            string copyPath = From;
                            string pastePath = path;
                            if (FileExists(From))
                            {

                                System.IO.File.Copy(copyPath, pastePath, true);
                            }
                            else
                            {
                                ErorLists.Append(From + ",  ");
                            }
                            Count = 1;
                        }
                        //if (ListOtherpaper[i].DocFilePath != null && ListOtherpaper[i].DocFilePath != "")
                        //{

                        //    string fileName = "";// addLines.LOBId + "_PDF_" + Location;
                        //    if (ListOtherpaper[i].SrNo1 != null && ListOtherpaper[i].SrNo1.ToString().Trim() != "")
                        //    {
                        //        fileName = ListOtherpaper[i].SrNo1.ToString().Trim();
                        //    }
                        //    if (ListOtherpaper[i].SrNo2 != null && ListOtherpaper[i].SrNo2.ToString().Trim() != "")
                        //    {
                        //        fileName = fileName + "_" + ListOtherpaper[i].SrNo2.ToString().Trim();
                        //    }
                        //    if (ListOtherpaper[i].SrNo3 != null && ListOtherpaper[i].SrNo3.ToString().Trim() != "")
                        //    {
                        //        fileName = fileName + "_" + ListOtherpaper[i].SrNo3;
                        //    }
                        //    fileName = fileName + ".doc";


                        //    var path = System.IO.Path.Combine(Docdirectory, fileName);
                        //    if (ListOtherpaper[i].DocFilePath.Contains("~"))
                        //    {
                        //        ListOtherpaper[i].DocFilePath = ListOtherpaper[i].DocFilePath.Substring(1, ListOtherpaper[i].DocFilePath.Length - 1);
                        //    }
                        //    string From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].DocFilePath);
                        //    string copyPath = From;
                        //    string pastePath = path;
                        //    if (FileExists(From))
                        //    {
                        //        System.IO.File.Copy(copyPath, pastePath, true);
                        //    }

                        //    Count = 1;
                        //}

                    }
                }

                ListOtherpaper = (List<QuestionModelCustom>)Helper.ExecuteService("LOB", "GetDraftLobLatestPapersForTransfer", DraftLob);

                if (ListOtherpaper.Count > 0)
                {
                    for (int i = 0; i < ListOtherpaper.Count; i++)
                    {
                        string[] dateArr = SessionDate.Split('/');
                        string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                        // string url = "/AssemblyHouse/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Documents/";

                        string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Documents/";
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);

                        if (i == 0)
                        {
                            //   string directory = Server.MapPath(url);
                            if (Directory.Exists(directory))
                            {
                                Directory.Delete(directory, true);
                                Directory.CreateDirectory(directory);
                            }
                            else
                            {
                                Directory.CreateDirectory(directory);
                            }
                        }
                        string From = "";
                        string path = "";
                        if (ListOtherpaper[i].DocFilePath != null && ListOtherpaper[i].DocFilePath != "")
                        {
                            if (ListOtherpaper[i].PaperLaidId == null || ListOtherpaper[i].PaperLaidId.ToString() == "" || ListOtherpaper[i].PaperLaidId == 0)
                            {

                                int indexof = ListOtherpaper[i].DocFilePath.LastIndexOf('/');
                                string fileName = ListOtherpaper[i].DocFilePath.Substring(indexof + 1);

                                //var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                path = System.IO.Path.Combine(directory, fileName);
                                if (ListOtherpaper[i].DocFilePath.Contains("~"))
                                {
                                    ListOtherpaper[i].DocFilePath = ListOtherpaper[i].DocFilePath.Substring(1, ListOtherpaper[i].DocFilePath.Length - 1);
                                }

                                From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].DocFilePath);
                                string copyPath = From;
                                string pastePath = path;
                                if (FileExists(From))
                                {
                                    System.IO.File.Copy(copyPath, pastePath, true);
                                }
                                else
                                {
                                    ErorLists.Append(From + ",  ");
                                }
                            }
                            if (ListOtherpaper[i].PaperLaidId != null && ListOtherpaper[i].PaperLaidId.ToString() != "" && ListOtherpaper[i].PaperLaidId != 0)
                            {
                                string fileName = "";// addLines.LOBId + "_PDF_" + Location;

                                if (ListOtherpaper[i].SrNo1 != null && ListOtherpaper[i].SrNo1.ToString().Trim() != "")
                                {
                                    fileName = ListOtherpaper[i].SrNo1.ToString().Trim();
                                }
                                if (ListOtherpaper[i].SrNo2 != null && ListOtherpaper[i].SrNo2.ToString().Trim() != "")
                                {
                                    fileName = fileName + "_" + ListOtherpaper[i].SrNo2.ToString().Trim();
                                }
                                if (ListOtherpaper[i].SrNo3 != null && ListOtherpaper[i].SrNo3.ToString().Trim() != "")
                                {
                                    fileName = fileName + "_" + ListOtherpaper[i].SrNo3;
                                }
                                fileName = fileName + ".pdf";


                                path = System.IO.Path.Combine(directory, fileName);
                                if (ListOtherpaper[i].FileName.Contains("~"))
                                {
                                    ListOtherpaper[i].FileName = ListOtherpaper[i].FileName.Substring(1, ListOtherpaper[i].FileName.Length - 1);
                                }


                                From = System.IO.Path.Combine(FileSettings.SettingValue + ListOtherpaper[i].FileName);
                                string copyPath = From;
                                string pastePath = path;
                                if (FileExists(From))
                                {
                                    System.IO.File.Copy(copyPath, pastePath, true);
                                }
                                else
                                {
                                    ErorLists.Append(From + ",  ");
                                }
                            }
                            //string From = Server.MapPath(ListAdminLOB[i].PDFLocation);



                            Count = 1;
                        }

                    }
                }

                tQuestion Question = new tQuestion();
                Question.IsFixedDate = Sessiondate;
                Question.QuestionType = 1;
                List<tQuestion> ListQuestion = new List<tQuestion>();
                List<QuestionModelCustom> ListQuestionModelCustom = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateWithPaper", Question);

                for (int i = 0; i < ListQuestionModelCustom.Count; i++)
                {
                    string[] dateArr = SessionDate.Split('/');
                    string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                    string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Starred/";
                    //string urlDoc = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Starred/DocFile";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    //  string Docdirectory = System.IO.Path.Combine(FileSettings.SettingValue + urlDoc);
                    if (i == 0)
                    {
                        if (Directory.Exists(directory))
                        {
                        }
                        else
                        {
                            Directory.CreateDirectory(directory);
                        }
                        //if (Directory.Exists(Docdirectory))
                        //{
                        //}
                        //else
                        //{
                        //    Directory.CreateDirectory(Docdirectory);
                        //}
                    }
                    if (ListQuestionModelCustom[i].FileName != null && ListQuestionModelCustom[i].FileName != "")
                    {

                        string fileExt = Path.GetExtension(ListQuestionModelCustom[i].FileName);
                        string fileName = "";
                        if (fileExt == ".pdf")
                        {
                            fileName = ListQuestionModelCustom[i].QuestionNumber + ".pdf";
                        }

                        var path = Path.Combine(directory, fileName);
                        string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].FileName);

                        string copyPath = From;
                        string pastePath = path;
                        if (FileExists(From))
                        {
                            System.IO.File.Copy(copyPath, pastePath, true);
                        }
                        else
                        {
                            ErorLists.Append(From + ",  ");
                        }
                        Count = 1;
                    }
                    //if (ListQuestionModelCustom[i].DocFilePath != null && ListQuestionModelCustom[i].DocFilePath != "")
                    //{

                    //    string fileExt = Path.GetExtension(ListQuestionModelCustom[i].DocFilePath);
                    //    string fileName = "";
                    //    if (fileExt == ".doc")
                    //    {
                    //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".doc";
                    //    }
                    //    else if (fileExt == ".docx")
                    //    {
                    //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".docx";
                    //    }
                    //    var path = Path.Combine(Docdirectory, fileName);
                    //    string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].DocFilePath);
                    //    string copyPath = From;
                    //    string pastePath = path;
                    //    if (FileExists(From))
                    //    {
                    //        System.IO.File.Copy(copyPath, pastePath, true);
                    //    }

                    //    Count = 1;
                    //}
                }


                Question = new tQuestion();
                Question.IsFixedDate = Sessiondate;
                Question.QuestionType = 2;
                ListQuestion = new List<tQuestion>();
                ListQuestionModelCustom = (List<QuestionModelCustom>)Helper.ExecuteService("Questions", "GetQuestionBySessionDateWithPaper", Question);

                for (int i = 0; i < ListQuestionModelCustom.Count; i++)
                {
                    string[] dateArr = SessionDate.Split('/');
                    string sessiondate = dateArr[2].Trim() + "" + dateArr[1].Trim() + "" + dateArr[0].Trim();
                    string url = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Unstarred/";
                    // string urlDoc = "/AssemblyFiles/" + AssemblyId + "/" + SessionId + "/" + sessiondate + "/Unstarred/DocFile";
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    // string Docdirectory = System.IO.Path.Combine(FileSettings.SettingValue + urlDoc);
                    if (i == 0)
                    {
                        if (Directory.Exists(directory))
                        {
                        }
                        else
                        {
                            Directory.CreateDirectory(directory);
                        }
                        //if (Directory.Exists(Docdirectory))
                        //{
                        //}
                        //else
                        //{
                        //    Directory.CreateDirectory(Docdirectory);
                        //}
                    }
                    if (ListQuestionModelCustom[i].FileName != null && ListQuestionModelCustom[i].FileName != "")
                    {

                        string fileExt = Path.GetExtension(ListQuestionModelCustom[i].FileName);
                        string fileName = "";
                        if (fileExt == ".pdf")
                        {
                            fileName = ListQuestionModelCustom[i].QuestionNumber + ".pdf";
                        }

                        var path = Path.Combine(directory, fileName);
                        string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].FileName);
                        string copyPath = From;
                        string pastePath = path;
                        if (FileExists(From))
                        {
                            System.IO.File.Copy(copyPath, pastePath, true);
                        }
                        else
                        {
                            ErorLists.Append(From + ",  ");
                        }

                        Count = 1;
                    }
                    //if (ListQuestionModelCustom[i].DocFilePath != null && ListQuestionModelCustom[i].DocFilePath != "")
                    //{

                    //    string fileExt = Path.GetExtension(ListQuestionModelCustom[i].DocFilePath);
                    //    string fileName = "";

                    //    if (fileExt == ".doc")
                    //    {
                    //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".doc";
                    //    }
                    //    else if (fileExt == ".docx")
                    //    {
                    //        fileName = ListQuestionModelCustom[i].QuestionNumber + ".docx";
                    //    }
                    //    var path = Path.Combine(Docdirectory, fileName);
                    //    string From = System.IO.Path.Combine(FileSettings.SettingValue + ListQuestionModelCustom[i].DocFilePath);

                    //    string copyPath = From;
                    //    string pastePath = path;
                    //    if (FileExists(From))
                    //    {
                    //        System.IO.File.Copy(copyPath, pastePath, true);
                    //    }

                    //    Count = 1;
                    //}
                }



                if (Count == 0)
                {
                    ErorLists.Append("No File Available!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
                else if (!string.IsNullOrEmpty(ErorLists.ToString()))
                {
                    ErorLists.Append("!! Files are not Transferred !!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ErorLists.Append("All Files are Transferred!");
                    return Json(ErorLists.ToString(), JsonRequestBehavior.AllowGet);
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("File Download Error!", JsonRequestBehavior.AllowGet);
            }

            // return Json("", JsonRequestBehavior.AllowGet);
        }

        public bool FileExists(string path)
        {
            string curFile = @"" + path; ;
            bool valu = System.IO.File.Exists(curFile) ? true : false;
            return valu;
        }

        private Dictionary<char, int> CharValues = null;
        // Convert Roman numerals to an integer.
        private int RomanToArabic(string roman)
        {
            // Initialize the letter map.
            if (CharValues == null)
            {
                CharValues = new Dictionary<char, int>();
                CharValues.Add('I', 1);
                CharValues.Add('V', 5);
                CharValues.Add('X', 10);
                CharValues.Add('L', 50);
                CharValues.Add('C', 100);
                CharValues.Add('D', 500);
                CharValues.Add('M', 1000);
            }

            if (roman.Length == 0) return 0;
            roman = roman.ToUpper();

            // See if the number begins with (.
            if (roman[0] == '(')
            {
                // Find the closing parenthesis.
                int pos = roman.LastIndexOf(')');

                // Get the value inside the parentheses.
                string part1 = roman.Substring(1, pos - 1);
                string part2 = roman.Substring(pos + 1);
                return 1000 * RomanToArabic(part1) + RomanToArabic(part2);
            }

            // The number doesn't begin with (.
            // Convert the letters' values.
            int total = 0;
            int last_value = 0;
            for (int i = roman.Length - 1; i >= 0; i--)
            {
                int new_value = CharValues[roman[i]];

                // See if we should add or subtract.
                if (new_value < last_value)
                    total -= new_value;
                else
                {
                    total += new_value;
                    last_value = new_value;
                }
            }

            // Return the result.
            return total;
        }

        #region "GenerateXMLlob"
        //[HttpGet]
        public ActionResult GenerateXMLlob(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            //  string test1 = CreateQuesXML("2", "20-12-2013");
            // string test2 = CreateQuesXML("1", "20-12-2013");
            string test = CreateXML(LOBId, "Draft");
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
            methodParameter.Add(new KeyValuePair<string, string>("@DraftLOBxmlLocation", test));

            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UpdateDraftLOBxmlPathLOB", methodParameter);
            return File(test, "application/xml", "DraftLOBxml.xml");
        }

        public ActionResult DownloadlobPDF(string SessionDate)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            //  string test1 = CreateQuesXML("2", "20-12-2013");
            // string test2 = CreateQuesXML("1", "20-12-2013");
            SessionDate = SessionDate.Replace("/", " ");
           
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string fileName = "Draft_LOB.pdf";
            string url = "/LOB/" + 7 + "/" + CurrentSession.SessionId + "/" + SessionDate + "/";
            string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
            string path = System.IO.Path.Combine(directory, fileName);
            return File(path, "application/pdf");
        }
        public ActionResult DownloadApprovedlobPDF(string SessionDate)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            //  string test1 = CreateQuesXML("2", "20-12-2013");
            // string test2 = CreateQuesXML("1", "20-12-2013");
            SessionDate = SessionDate.Replace("/", " ");

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string fileName = "Approved_LOB.pdf";
            string url = "/LOB/" + 7 + "/" + CurrentSession.SessionId + "/" + SessionDate + "/";
            string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
            string path = System.IO.Path.Combine(directory, fileName);
            return File(path, "application/pdf");
        }
        public string CreateXML(string LOBId, string Form)
        {
            var methodParameter = new List<KeyValuePair<string, string>>();
            DataSet dataset = null;
            methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));
            if (Form == "Draft")
            {

                dataset = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBForXML]", methodParameter);
            }
            else
            {
                dataset = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
            }



            if (dataset != null)
            {

                XElement LOB = new System.Xml.Linq.XElement("LOB");
                XElement pages = new System.Xml.Linq.XElement("pages");
                LOB.Add(pages);
                XElement header = new XElement("headerpage");
                //header.Attribute("id").Value="header_page";
                header.SetAttributeValue("id", "headerpage");
                pages.Add(header);
                XElement content = new XElement("content");
                header.Add(content);



                DateTime SessionDate = Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]);
                string sessiondate = Convert.ToString(dataset.Tables[0].Rows[0]["SessionDate"]);
                string date = ConvertSQLDate(SessionDate);
                date = date.Replace("/", "");
                date = date.Replace("-", "");
                string LOBName = date + "_" + "LOB" + "_" + Convert.ToString(dataset.Tables[0].Rows[0]["LOBId"]);

                content.ReplaceNodes(new XCData(@"<h2 style='text-align: center;font-family:DVOT-Yogesh;'>" + Convert.ToString(dataset.Tables[0].Rows[0]["AssemblyNameLocal"]) + @"</h2>
                    <h4 style='text-align: center;font-family:DVOT-Yogesh;'>कार्य - सूची</h4>
                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionNameLocal"]) + @"  </h4>
                        <h4 style='text-align: center; font-family:DVOT-Yogesh;'> " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionDateLocal"]) + @"  " + Convert.ToString(dataset.Tables[0].Rows[0]["SessionTimeLocal"]) + @" बजे शुरू</h4>"));


                LOB.SetAttributeValue("AssemblyId", Convert.ToString(dataset.Tables[0].Rows[0]["AssemblyId"]));
                LOB.SetAttributeValue("SessionId", Convert.ToString(dataset.Tables[0].Rows[0]["SessionId"]));
                LOB.SetAttributeValue("SessionDate", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd/MM/yyyy"));

                //page
                int idCount = 2;
                string SrNo1 = "";
                string SrNo2 = "";
                string SrNo3 = "";
                int SrNo1Count = 1;
                int SrNo2Count = 1;
                int SrNo3Count = 1;
                int MainEventId = 2;
                string ConcernedEventId = "";
                string outXml = "";

                //XElement page = new XElement("page");
                //XElement cont = new XElement("content");

                XElement page1 = new XElement("page");
                // XElement cont1 = new XElement("content");
                XElement business = new XElement("business");
                //  page1.Add(business);
                //Index
                XElement index = new XElement("index-page");
                index.SetAttributeValue("id", "index_page");
                pages.Add(index);
                XElement indexContent = new XElement("content");
                StringBuilder sb = new StringBuilder();

                XElement indexOne = new XElement("index");
                int businessNo = 1;

                for (int count = 0; count < dataset.Tables[0].Rows.Count; count++)
                {
                    ConcernedEventId = Convert.ToString(dataset.Tables[0].Rows[count]["ConcernedEventId"]);
                    business = new XElement("business");

                    if (count == 0)
                    {
                        page1.SetAttributeValue("id", 2);
                        page1.SetAttributeValue("page-des", "first-page");
                        page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));
                        businessNo = 1;


                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    XElement subactions = new XElement("actions");
                        //    page1.Add(subactions);
                        //    XElement subactionOne = new XElement("action");
                        //    subactionOne.SetAttributeValue("id", "PDF");
                        //    subactionOne.SetAttributeValue("command", "OPEN_PDF");
                        //    subactionOne.SetAttributeValue("commandvalue", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //    subactionOne.SetAttributeValue("image", "place.png");
                        //    subactionOne.SetAttributeValue("enabled", "true");
                        //    subactions.Add(subactionOne);
                        //}


                    }
                    else
                    {
                        if (Convert.ToInt32(dataset.Tables[0].Rows[count - 1]["SrNo1"]) != Convert.ToInt32(dataset.Tables[0].Rows[count]["SrNo1"]))
                        {
                            // cont1.ReplaceNodes(new XCData(outXml));
                            //    page1.Add(business);
                            //   pages.Add(page1);

                            //     outXml = "";
                        }

                    }




                    if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == true && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == true)
                    {

                        if (count != 0)
                        {
                            MainEventId++;
                            businessNo = 1;
                            //  page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            /// business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);
                            page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));
                        }


                        ///For Index
                        string LOBText1 = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string FinalText = LOBText1.Replace("<i>", "<span>&nbsp;");
                        string FinalTt = FinalText.Replace("</i>", "</span>");
                        if (LOBText1 != "")
                        {
                            FinalTt = LOBText1.Replace("<p>", "");
                            FinalTt = LOBText1.Replace("</p>", "");
                            FinalTt = @"<span style='font-family:DVOT-Yogesh;'>" + LOBText1 + "</span>";

                        }
                        sb.Append("<li>" + LOBText1 + "</li>");
                        XElement subIndex1 = new XElement("index");
                        subIndex1.SetAttributeValue("id", idCount);

                        LOBText1 = StripHTML(LOBText1);
                        string Lobtext1 = LOBText1.Replace("\r\r", "");
                        subIndex1.SetAttributeValue("description", Lobtext1);
                        subIndex1.SetAttributeValue("command", "test_command");
                        subIndex1.SetAttributeValue("commandvalue", MainEventId);
                        subIndex1.SetAttributeValue("enabled", "true");
                        indexOne.Add(subIndex1);




                        // }


                        /////For first page Index
                        //string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        //if (LOBText != "")
                        //{
                        //    LOBText = LOBText.Replace("<p>", "");
                        //    LOBText = LOBText.Replace("</p>", "");
                        //}

                        //sb.Append("<li>" + LOBText + "</li>");
                        //XElement subIndex = new XElement("index");
                        //subIndex.SetAttributeValue("id", 1);

                        //LOBText = StripHTML(LOBText);
                        //subIndex.SetAttributeValue("description", LOBText);
                        //subIndex.SetAttributeValue("command", "test_command");
                        //subIndex.SetAttributeValue("enabled", "true");
                        //indexOne.Add(subIndex);


                        /// For contant
                        outXml = "";
                        SrNo1 = Convert.ToString(SrNo1Count) + ". ";
                        outXml += @" <p style='font-size:24px;text-align:justify;'><span style='font-family:DVOT-Yogesh;'>
                                    
                                    " + SrNo1 + @"";
                        string LOBTexts1 = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string FinalTexts1 = LOBTexts1.Replace("<i>", "<span>&nbsp;");
                        string LOBText = FinalTexts1.Replace("</i>", "</span>");
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        outXml += LOBText + "</span></p>";
                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) == "")
                        //{
                        //    outXml += "<br/>";
                        //}


                        XElement contentOne = new XElement("content");
                        //contentOne.SetAttributeValue("id", SrNo1);
                        //contentOne.SetAttributeValue("level", "1");
                        //contentOne.SetAttributeValue("Set", SrNo1);
                        //  contentOne.SetAttributeValue("value", LOBText);




                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    contentOne.SetAttributeValue("action", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //}
                        contentOne.ReplaceNodes(new XCData(outXml));

                        business.SetAttributeValue("id", MainEventId + "." + businessNo);
                        business.SetAttributeValue("MainEventId", MainEventId);

                        business.SetAttributeValue("ConcernedEventId", ConcernedEventId);
                        var methodParameter3 = new List<KeyValuePair<string, string>>();
                        methodParameter3.Add(new KeyValuePair<string, string>("@XmlEventId", MainEventId + "." + businessNo.ToString()));
                        methodParameter3.Add(new KeyValuePair<string, string>("@Id", dataset.Tables[0].Rows[count]["Id"].ToString()));
                        methodParameter3.Add(new KeyValuePair<string, string>("@LobType", Form));
                        ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_UpdateXmlEventId]", methodParameter3);



                        string EventId = Convert.ToString(dataset.Tables[0].Rows[count]["ConcernedEventId"]);

                        DataSet dataset2 = null;
                        var methodParameter4 = new List<KeyValuePair<string, string>>();
                        methodParameter4.Add(new KeyValuePair<string, string>("@Id", EventId));
                        dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetPaperTypeByEventId]", methodParameter4);
                        if (dataset2 != null)
                        {
                            if (!DBNull.Value.Equals(dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"]))
                            {
                                string papertypeid = dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"].ToString();
                                business.SetAttributeValue("PaperCatTypeId", papertypeid);
                            }
                        }
                        string SNo = dataset.Tables[0].Rows[count]["SrNo1"].ToString();
                        business.SetAttributeValue("SerialNumber", SNo);
                        string pdflocation = dataset.Tables[0].Rows[count]["PDFLocation"].ToString();
                        if (pdflocation != "" && pdflocation != null)
                        {
                            if (Form == "Draft")
                            {

                                var methodParameter5 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["Id"].ToString();
                                methodParameter5.Add(new KeyValuePair<string, string>("@Id", draftid));

                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter5);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                            else
                            {

                                var methodParameter5 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["DraftRecordId"].ToString();
                                methodParameter5.Add(new KeyValuePair<string, string>("@Id", draftid));
                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter5);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                        }

                        if (EventId == "3" || EventId == "4")
                        {
                            business.SetAttributeValue("Question", "True");
                        }
                        else
                        {
                            business.SetAttributeValue("Question", "False");
                        }

                        business.Add(contentOne);
                        businessNo++;
                        ///For Start and Stop Event Actions
                        XElement subactions = new XElement("actions");
                        business.Add(subactions);
                        XElement subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STARTEVENT");
                        subactionOne.SetAttributeValue("command", "START_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "start.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STOPEVENT");
                        subactionOne.SetAttributeValue("command", "STOP_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "stop.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        ///For show Atteched PDF action
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        {
                            string name = Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]);
                            int index1 = name.LastIndexOf('/');
                            name = name.Substring(index1 + 1);

                            subactionOne = new XElement("action");
                            subactionOne.SetAttributeValue("id", "ATTACHED_PDF");
                            subactionOne.SetAttributeValue("command", "OPEN_PDF");
                            subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                            subactionOne.SetAttributeValue("commandvalue", name);
                            subactionOne.SetAttributeValue("image", "place.png");
                            subactionOne.SetAttributeValue("concernedmember", "");
                            subactionOne.SetAttributeValue("enabled", "true");
                            subactions.Add(subactionOne);
                        }

                        SrNo1Count = SrNo1Count + 1;
                        SrNo2Count = 1;

                        //for page break
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PageBreak"]) == "True")
                        {
                            page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);

                            page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));

                        }
                        else
                        {
                            page1.Add(business);

                        }

                    }
                    else if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == true)
                    {
                        outXml = "";
                        SrNo2 = " (" + Convert.ToString(SrNo2Count) + ") ";
                        outXml += @" <p style='line-height:4; font-family:DVOT-Yogesh;text-align:justify;font-size:22px;padding-left: 25px'><span>
                                    " + SrNo2 + @" ";
                        //string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string LOBTexts2 = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string FinalTexts2 = LOBTexts2.Replace("<i>", "<span>&nbsp;");
                        string LOBText = FinalTexts2.Replace("</i>", "</span>");

                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        outXml += LOBText + "</span></p>";
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) == "")
                        {
                            outXml += "<br/>";
                        }
                        XElement contentOne = new XElement("content");
                        //contentOne.SetAttributeValue("id", Convert.ToString(SrNo2Count));
                        //contentOne.SetAttributeValue("level", "2");
                        //contentOne.SetAttributeValue("Set", SrNo1 + "." + SrNo2);
                        //  contentOne.SetAttributeValue("value", LOBText);
                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    contentOne.SetAttributeValue("action", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //}
                        contentOne.ReplaceNodes(new XCData(outXml));

                        business.SetAttributeValue("id", MainEventId + "." + businessNo);

                        business.SetAttributeValue("MainEventId", MainEventId);
                        business.SetAttributeValue("ConcernedEventId", ConcernedEventId);
                        var methodParameter4 = new List<KeyValuePair<string, string>>();
                        methodParameter4.Add(new KeyValuePair<string, string>("@XmlEventId", MainEventId + "." + businessNo.ToString()));
                        methodParameter4.Add(new KeyValuePair<string, string>("@Id", dataset.Tables[0].Rows[count]["Id"].ToString()));
                        methodParameter4.Add(new KeyValuePair<string, string>("@LobType", Form));
                        ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_UpdateXmlEventId]", methodParameter4);

                        string EventId = Convert.ToString(dataset.Tables[0].Rows[count]["ConcernedEventId"]);

                        DataSet dataset2 = null;
                        var methodParameter5 = new List<KeyValuePair<string, string>>();
                        methodParameter5.Add(new KeyValuePair<string, string>("@Id", EventId));
                        dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetPaperTypeByEventId]", methodParameter5);
                        if (dataset2 != null)
                        {
                            if (!DBNull.Value.Equals(dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"]))
                            {
                                string papertypeid = dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"].ToString();
                                business.SetAttributeValue("PaperCatTypeId", papertypeid);
                            }
                        }
                        string SNo1 = dataset.Tables[0].Rows[count]["SrNo1"].ToString();
                        string SNo2 = dataset.Tables[0].Rows[count]["SrNo2"].ToString();
                        business.SetAttributeValue("SerialNumber", SNo1 + "." + SNo2);
                        string pdflocation = dataset.Tables[0].Rows[count]["PDFLocation"].ToString();
                        if (pdflocation != "" && pdflocation != null)
                        {
                            if (Form == "Draft")
                            {
                                var methodParameter6 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["Id"].ToString();
                                methodParameter6.Add(new KeyValuePair<string, string>("@Id", draftid));

                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter6);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                            else
                            {

                                var methodParameter6 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["DraftRecordId"].ToString();
                                methodParameter6.Add(new KeyValuePair<string, string>("@Id", draftid));
                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter6);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                        }
                        if (EventId == "3" || EventId == "4")
                        {
                            business.SetAttributeValue("Question", "True");
                        }
                        else
                        {
                            business.SetAttributeValue("Question", "False");
                        }

                        business.Add(contentOne);
                        businessNo++;


                        ///For Start and Stop Event Actions
                        XElement subactions = new XElement("actions");
                        business.Add(subactions);
                        XElement subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STARTEVENT");
                        subactionOne.SetAttributeValue("command", "START_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "start.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STOPEVENT");
                        subactionOne.SetAttributeValue("command", "STOP_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "stop.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        ///for the action tab for pdf link
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        {
                            string name = Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]);
                            int index1 = name.LastIndexOf('/');
                            name = name.Substring(index1 + 1);

                            subactionOne = new XElement("action");
                            subactionOne.SetAttributeValue("id", "ATTACHED_PDF");
                            subactionOne.SetAttributeValue("command", "OPEN_PDF");
                            subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                            subactionOne.SetAttributeValue("commandvalue", name);
                            subactionOne.SetAttributeValue("image", "place.png");
                            subactionOne.SetAttributeValue("concernedmember", "");
                            subactionOne.SetAttributeValue("enabled", "true");
                            subactions.Add(subactionOne);
                        }

                        SrNo2Count = SrNo2Count + 1;
                        SrNo3Count = 1;
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PageBreak"]) == "True")
                        {
                            page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);

                            page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));
                            // businessNo = 1;
                        }
                        else
                        {
                            page1.Add(business);

                        }

                    }
                    else if (System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo1"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo2"]) == false && System.DBNull.Value.Equals(dataset.Tables[0].Rows[count]["SrNo3"]) == false)
                    {
                        outXml = "";
                        SrNo3 = "(" + ToRoman(Convert.ToInt32(Convert.ToString(SrNo3Count))) + ") ";
                        outXml += @"<p style='text-align:justify;padding-left: 50px;font-family:DVOT-Yogesh;font-size:22px'><span >  
                                    " + SrNo3 + @"";
                        //string LOBText = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string LOBTexts3 = Convert.ToString(dataset.Tables[0].Rows[count]["TextLOB"]);
                        string FinalTexts3 = LOBTexts3.Replace("<i>", "<span>&nbsp;");
                        string LOBText = FinalTexts3.Replace("</i>", "</span>");
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        outXml += LOBText + "</span></p>";
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) == "")
                        {
                            outXml += "<br/>";
                        }

                        XElement contentOne = new XElement("content");
                        //contentOne.SetAttributeValue("id", Convert.ToString(SrNo3Count));
                        //contentOne.SetAttributeValue("level", "3");
                        //contentOne.SetAttributeValue("Set", SrNo1 + "." + SrNo2 + "." + SrNo3);
                        //   contentOne.SetAttributeValue("value", LOBText);
                        //if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        //{
                        //    contentOne.SetAttributeValue("action", Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]));
                        //}
                        contentOne.ReplaceNodes(new XCData(outXml));

                        business.SetAttributeValue("id", MainEventId + "." + businessNo);


                        business.SetAttributeValue("MainEventId", MainEventId);
                        business.SetAttributeValue("ConcernedEventId", ConcernedEventId);
                        var methodParameter5 = new List<KeyValuePair<string, string>>();
                        methodParameter5.Add(new KeyValuePair<string, string>("@XmlEventId", MainEventId + "." + businessNo.ToString()));
                        methodParameter5.Add(new KeyValuePair<string, string>("@Id", dataset.Tables[0].Rows[count]["Id"].ToString()));
                        methodParameter5.Add(new KeyValuePair<string, string>("@LobType", Form));
                        ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_UpdateXmlEventId]", methodParameter5);


                        string EventId = Convert.ToString(dataset.Tables[0].Rows[count]["ConcernedEventId"]);
                        DataSet dataset2 = null;
                        var methodParameter4 = new List<KeyValuePair<string, string>>();
                        methodParameter4.Add(new KeyValuePair<string, string>("@Id", EventId));
                        dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetPaperTypeByEventId]", methodParameter4);
                        if (dataset2 != null)
                        {
                            if (!DBNull.Value.Equals(dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"]))
                            {
                                string papertypeid = dataset2.Tables[0].Rows[0]["PaperCategoryTypeId"].ToString();
                                business.SetAttributeValue("PaperCatTypeId", papertypeid);
                            }
                        }
                        string SNo1 = dataset.Tables[0].Rows[count]["SrNo1"].ToString();
                        string SNo2 = dataset.Tables[0].Rows[count]["SrNo2"].ToString();
                        string SNo3 = dataset.Tables[0].Rows[count]["SrNo3"].ToString();
                        business.SetAttributeValue("SerialNumber", SNo1 + "." + SNo2 + "." + SNo3);
                        string pdflocation = dataset.Tables[0].Rows[count]["PDFLocation"].ToString();
                        if (pdflocation != "" && pdflocation != null)
                        {
                            if (Form == "Draft")
                            {
                                var methodParameter6 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["Id"].ToString();
                                methodParameter6.Add(new KeyValuePair<string, string>("@Id", draftid));

                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter6);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                            else
                            {

                                var methodParameter6 = new List<KeyValuePair<string, string>>();
                                string draftid = dataset.Tables[0].Rows[count]["DraftRecordId"].ToString();
                                methodParameter6.Add(new KeyValuePair<string, string>("@Id", draftid));
                                dataset2 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_GetConcernMinisterMemberCode]", methodParameter6);
                                if (dataset2 != null)
                                {
                                    if (dataset2.Tables[0].Rows.Count > 0)
                                    {
                                        if (dataset2.Tables[0].Rows[0]["MemberCode"] != DBNull.Value)
                                        {
                                            string ConcernMinisterMemberCode = dataset2.Tables[0].Rows[0]["MemberCode"].ToString();
                                            business.SetAttributeValue("ConcernMinisterMemberCode", ConcernMinisterMemberCode);
                                        }
                                    }
                                }

                            }
                        }
                        if (EventId == "3" || EventId == "4")
                        {
                            business.SetAttributeValue("Question", "True");
                        }
                        else
                        {
                            business.SetAttributeValue("Question", "False");
                        }

                        business.Add(contentOne);
                        businessNo++;


                        ///For Start and Stop Event Actions
                        XElement subactions = new XElement("actions");
                        business.Add(subactions);
                        XElement subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STARTEVENT");
                        subactionOne.SetAttributeValue("command", "START_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "start.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        subactionOne = new XElement("action");
                        subactionOne.SetAttributeValue("id", "STOPEVENT");
                        subactionOne.SetAttributeValue("command", "STOP_EVENT");
                        subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                        subactionOne.SetAttributeValue("commandvalue", MainEventId);
                        subactionOne.SetAttributeValue("image", "stop.png");
                        subactionOne.SetAttributeValue("concernedmember", "");
                        subactionOne.SetAttributeValue("enabled", "true");
                        subactions.Add(subactionOne);


                        ///for the action tab for pdf link
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]) != "")
                        {
                            string name = Convert.ToString(dataset.Tables[0].Rows[count]["PDFLocation"]);
                            int index1 = name.LastIndexOf('/');
                            name = name.Substring(index1 + 1);

                            subactionOne = new XElement("action");
                            subactionOne.SetAttributeValue("id", "ATTACHED_PDF");
                            subactionOne.SetAttributeValue("command", "OPEN_PDF");
                            subactionOne.SetAttributeValue("EventID", MainEventId + "." + (businessNo - 1));
                            subactionOne.SetAttributeValue("commandvalue", name);
                            subactionOne.SetAttributeValue("image", "place.png");
                            subactionOne.SetAttributeValue("concernedmember", "");
                            subactionOne.SetAttributeValue("enabled", "true");
                            subactions.Add(subactionOne);
                        }

                        SrNo3Count = SrNo3Count + 1;
                        if (Convert.ToString(dataset.Tables[0].Rows[count]["PageBreak"]) == "True")
                        {
                            page1.Add(business);
                            pages.Add(page1);
                            idCount++;
                            page1 = new XElement("page");
                            business = new XElement("business");
                            page1.SetAttributeValue("id", idCount);
                            page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));
                            //    businessNo = 1;
                        }
                        else
                        {
                            page1.Add(business);

                        }
                    }




                    if (count == dataset.Tables[0].Rows.Count - 1)
                    {

                        //businessNo = 1;
                        //business = new XElement("business");
                        //page1.Add(business);
                        //pages.Add(page1);

                        //page1.SetAttributeValue("id", idCount);
                        //page1.SetAttributeValue("Date", Convert.ToDateTime(dataset.Tables[0].Rows[0]["SessionDate"]).ToString("dd-MM-yyyy"));
                        //// cont1.ReplaceNodes(new XCData(outXml));
                        //// page1.Add(business);
                        //pages.Add(page1);
                        //business = new XElement("business");

                        pages.Add(page1);
                        ///Last page
                        page1 = new XElement("page");
                        //content = new XElement("content");
                        business = new XElement("business");
                        page1.SetAttributeValue("id", idCount + 1);
                        page1.SetAttributeValue("page-des", "last-page");
                        //content.ReplaceNodes(new XCData("<p style='font-size:22px;'>The End</P>"));
                        //business.Add(content);
                        page1.Add(business);
                        pages.Add(page1);

                    }
                }
                //indexContent.SetValue(sb);
                indexContent.ReplaceNodes(new XCData("<h5><b>Today's list of business:</b></h5><ol>" + sb + "<ol>"));
                index.Add(indexContent);

                index.Add(indexOne);

                ///Save XML File
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(LOB.ToString());


                methodParameter = new List<KeyValuePair<string, string>>();

                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string CurrentSession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        CurrentSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";


                sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                sessiondate = sessiondate.Replace('/', ' ');
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                if (Form == "Draft")
                {
                    fileName = "DraftLOBFile.xml";
                    string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + sessiondate + "/";
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = System.IO.Path.Combine(directory, fileName);
                    //  file.SaveAs(path);
                    xml.Save(path);
                    return path;
                }
                else
                {
                    fileName = "LOBFile.xml";
                    string url = "/LOB/" + CurrentAssembly + "/" + CurrentSession + "/" + sessiondate + "/";
                    string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = System.IO.Path.Combine(directory, fileName);
                    //  file.SaveAs(path);
                    xml.Save(path);
                    return path;
                    //xml.Save("C:/eVidhan/LOB/LOBFile.xml");
                }

#pragma warning disable CS0162 // Unreachable code detected
                return "";
#pragma warning restore CS0162 // Unreachable code detected
            }
            return "";
        }
        private string StripHTML(string source)
        {
            try
            {
                string result;

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = source.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove all others. More can be added, see

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // for testing
                //System.Text.RegularExpressions.Regex.Replace(result,
                //       this.txtRegex.Text,string.Empty,
                //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // make line breaking consistent
                result = result.Replace("\n", "\r");

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4.
                // Prepare first to remove any whitespaces in between
                // the escaped characters and remove redundant tabs in between line breaks
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove multiple tabs following a line break with just one tab
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Initial replacement target string for line breaks
                string breaks = "\r\r\r";
                // Initial replacement target string for tabs
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }

                // That's it.
                return result;
            }
            catch
            {

                return source;
            }
        }
        public string ConvertSQLDate(DateTime? dt)
        {
            DateTime ConvtDate = Convert.ToDateTime(dt);
            string month = ConvtDate.Month.ToString();
            if (month.Length < 2)
            {
                month = "0" + month;
            }
            string day = ConvtDate.Day.ToString();
            string year = ConvtDate.Year.ToString();
            string Date = day + "/" + month + "/" + year;
            return Date;
        }
        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "m" + ToRoman(number - 1000);
            if (number >= 900) return "cm" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "d" + ToRoman(number - 500);
            if (number >= 400) return "cd" + ToRoman(number - 400);
            if (number >= 100) return "c" + ToRoman(number - 100);
            if (number >= 90) return "xc" + ToRoman(number - 90);
            if (number >= 50) return "l" + ToRoman(number - 50);
            if (number >= 40) return "xl" + ToRoman(number - 40);
            if (number >= 10) return "x" + ToRoman(number - 10);
            if (number >= 9) return "ix" + ToRoman(number - 9);
            if (number >= 5) return "v" + ToRoman(number - 5);
            if (number >= 4) return "iv" + ToRoman(number - 4);
            if (number >= 1) return "i" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        #endregion
        [HttpGet]

        public FileStreamResult GenerateLOBPdf(string Sessiondate, string LOBType)
        {

            string LOBId = "";
            int AssemblyId = (int)Helper.ExecuteService("PaperLaid", "AssemblyCode", null);
            int SessionId = (int)Helper.ExecuteService("PaperLaid", "SessionCode", null);
            List<KeyValuePair<string, string>> nmethodParameter1 = new List<KeyValuePair<string, string>>();
            nmethodParameter1.Add(new KeyValuePair<string, string>("@AssemblyId", Convert.ToString(AssemblyId)));
            nmethodParameter1.Add(new KeyValuePair<string, string>("@SessionId", Convert.ToString(SessionId)));
            nmethodParameter1.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));

            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLOBFromAssemblySessionDate", nmethodParameter1);
            List<DataRow> list = dataSetLOB.Tables[0].AsEnumerable().ToList();

            List<AddLOBModel> List = new List<AddLOBModel>();
            var dt = dataSetLOB.Tables[0];
            if (dt.Rows.Count > 0)
            {
                LOBId = dt.Rows[0]["LOBId"].ToString();
            }
            else
            {
                LOBId = "132";
            }



            bool download = true;
            //string LOBType = "";
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string LOBName = "20Dec2013_1_LOB";
            string CurrentSecretery = "";
            string CurrentSpeaker = "";
            string CurrenttSessionPlace = "";
            DateTime SessionDate = new DateTime();
            string savedPDF = string.Empty;


            MemoryStream output = new MemoryStream();
            if (LOBId != null && LOBId != "")
            {
                LOBModel objLOBModel = new LOBModel();
                DataSet SummaryDataSet = new DataSet();
                string sessiondate = "";
                string outXml = "";
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));

                if (LOBType == "Draft")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }
                else if (LOBType == "Approved")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
                }
                else
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }


                //Getting Current Secretary
                DataSet SiteSettingDataSet = new DataSet();
                var methodParameter1 = new List<KeyValuePair<string, string>>();
                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
                {
                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
                    if (dtSiteSetting != null)
                    {
                        IEnumerable<DataRow> siteSettingQuery = from siteSetting in dtSiteSetting.AsEnumerable() select siteSetting;


                        IEnumerable<DataRow> curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
                        }

                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSpeaker");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSpeaker = obj.Field<string>("SettingValueLocal");
                        }

                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "SessionPlace");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrenttSessionPlace = obj.Field<string>("SettingValueLocal");
                        }
                    }
                }
#pragma warning disable CS0219 // The variable 'sessionplace' is assigned but its value is never used
                string sessionplace = "";
#pragma warning restore CS0219 // The variable 'sessionplace' is assigned but its value is never used
                string SubittedDate = "";
                string SubittedTime = "";
                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {

                    outXml = @"<html>                           
                                 <body style='font-family:DVOT-Surekh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    string SrNo1 = "";
                    string SrNo2 = "";
                    string SrNo3 = "";
                    int SrNo1Count = 1;
                    int SrNo2Count = 1;
                    int SrNo3Count = 1;
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {

                        if (i == 0)
                        {
                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            string date = ConvertSQLDate(SessionDate);
                            date = date.Replace("/", "");
                            date = date.Replace("-", "");
                            //SessionDate = DateTime.ParseExact(sessiondate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                            sessiondate = sessiondate.Replace('/', ' ');
                            // SessionDate = SessionDate.Replace('/', ' ');                     
                            //TempData["addLines"] = sessiondate;

                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


                            outXml += @"<tr>
                                                    <td style='text-align: center; font-size: 60px; font-weight: bold;'>               
                                                         हिमाचल प्रदेश&nbsp;" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align: center; font-size: 34px; font-weight: bold;'>               
                                                          " + "कार्यसूची" + @"    
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 36px; font-weight: bold'>
                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style='text-align: center; font-size: 38px; font-weight: bold;'>               
                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 32px; font-weight: bold;text-decoration: underline;'>               
                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
                                                    </td>
                                                </tr>";

                            if (LOBType == "Draft")
                            {
                                if (SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"] != null && Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]) != "")
                                {
                                    SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]).ToString("D", new CultureInfo("hi")); ;
                                }
                                else
                                {
                                    SubittedDate = System.DateTime.Now.ToString("dd/MM/yyyy");
                                }

                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]) != "")
                                {
                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]));
                                    DateTime time = DateTime.Today.Add(interval);
                                    string displayTime = time.ToString("hh:mm tt");

                                    SubittedTime = displayTime;
                                }
                                else
                                {
                                    SubittedTime = "";
                                }
                            }
                            else
                            {

                                SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]).ToString("D", new CultureInfo("hi"));
                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]) != "")
                                {
                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]));
                                    DateTime time = DateTime.Today.Add(interval);
                                    string displayTime = time.ToString("hh:mm tt");

                                    SubittedTime = displayTime;
                                }
                                else
                                {
                                    SubittedTime = "";
                                }
                            }

                        }

                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
                            outXml += @"<tr><td style='text-align: left; font-size: 34px;'>
                                     <div style='padding-left:10px;text-align:justify;'>
                                  <b>  " + SrNo1 + @" </b> &nbsp;&nbsp;&nbsp; <div style='padding-left:20px; margin: -50px 0 -20 2%;text-align:justify;'>";
                            SrNo1Count = SrNo1Count + 1;
                            SrNo2Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {
                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 32px;'><div style='padding-left:30px;text-align:justify;'>
                                    <b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px; margin: -51px 0 -20 7.5%;text-align:justify;'>";
                            SrNo2Count = SrNo2Count + 1;
                            SrNo3Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
                        {

                           SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";

                           outXml += @"<tr>
                                        <td style='text-align: left; font-size: 32px;'>
                                        <div style='padding-left:60px;text-align:justify;'>
                                        <b>" + SrNo3 + @"</b>&nbsp;&nbsp;&nbsp;
                                        <div style='padding-left:15px;margin: -50px 0 -20 10%;text-align:justify;'>";
                            SrNo3Count = SrNo3Count + 1;
                          


                        }

                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }


                        outXml += @"" + LOBText + "</div></div> ";


                        outXml += @"<br/> </td></tr>";


                    }

                    if (LOBType == "Draft")
                    {
                        //Bottom Section
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 30px;'>
                                               <b> (" + CurrentSpeaker + @"),</b>
                                                </td>
                                                <td style='width:25%;float:left;font-size: 30px;'>
                                                <b> (" + CurrentSecretery + @"),  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 32px;padding-left:10%;'>
                                               <b> महापौर ।</b>
                                                </td>
                                                <td style='width:25%;float:right;text-align:right;padding-right:80px;font-size: 32px;'>
                                               <b> आयुक्त ।</b>
                                                </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }
                    else if (LOBType == "Approved")
                    {
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 30px;'>
                                     
                                                    <b> " + CurrenttSessionPlace + @",</b>
                                                </td>
                                                <td style='width:25%;float:left;font-size: 30px;'>
                                                <b> " + CurrentSecretery + @",  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 32px;'>
                                               <b> दिनांक: " + SubittedDate + @"</b>
                                              
                                                </td>
                                                <td style='width:25%;float:right;text-align:right;padding-right:85px;font-size: 32px;'>
                                               <b>  आयुक्त ।</b>
                                                </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }
                }

                // convert HTML to PDF
                EvoPdf.Document document1 = new EvoPdf.Document();
                AddElementResult addResult;
                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(50, 55, 40, 30);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(20, 55, 75, 65), PdfPageOrientation.Portrait);
               


                //*****************
                // The position on X anf Y axes where to add the next element
                // Add section title
               // float yLocation = 75;
               // float xLocation = 75;
               // float titlesYLocation = yLocation;
               //// EvoPdf.PdfFont font = new EvoPdf.PdfFont;
               // // string f = font.FontFamily;
               // EvoPdf.PdfFont titleFont = document1.AddFont(new EvoPdf.StandardCJKFont());
               // TextElement titleTextElement = new TextElement(xLocation, yLocation, "", titleFont);
               // titleTextElement.ForeColor = Color.Black;
               // addResult = page.AddElement(titleTextElement);
               // yLocation = addResult.EndPageBounds.Bottom + 10;
               // page = addResult.EndPdfPage;
               // //EvoPdf.PdfFont subtitleFont = document1.AddFont("Times New Roman");
               // EvoPdf.PdfFont subtitleFont = document1.AddFont(new EvoPdf.StandardCJKFont());
               // TextElement subtitleTextElement = new TextElement(xLocation, titlesYLocation, "", subtitleFont);
               // //subtitleTextElement.ForeColor = Color.Navy;
               // addResult = page.AddElement(subtitleTextElement);

               // page = addResult.EndPdfPage;
               // float imagesYLocation = addResult.EndPageBounds.Bottom + 3;

               // string imagePath = Server.MapPath("~/Areas/BusinessWork/Views/Shared/Curly.jpg");
               // ImageElement unscaledImageElement = new ImageElement(xLocation, imagesYLocation, imagePath);
               // addResult = page.AddElement(unscaledImageElement);

               // RectangleF scaledDownImageRectangle = new RectangleF(addResult.EndPageBounds.Right + 3, addResult.EndPageBounds.Y,
               //             addResult.EndPageBounds.Width, addResult.EndPageBounds.Height);

                //





                //*****************
                //EvoPdf.PdfFont ft = document1.AddFont("DVOT-Surekh");
                //var strBody = new StringBuilder();
               
                //strBody.Append("<html " + "xmlns:o='urn:schemas-microsoft-com:office:office' " + "xmlns:w='urn:schemas-microsoft-com:office:word'" +
                //  "xmlns='http://www.w3.org/TR/REC-html40'>" + "<head><title>Time</title>");

                ////The setting specifies document's view after it is downloaded as Print
                ////instead of the default Web Layout
                //strBody.Append("<!--[if gte mso 9]>" +
                // "<xml>" +
                // "<w:WordDocument>" +
                // "<w:View>Print</w:View>" +
                // "<w:Zoom>90</w:Zoom>" +
                // "<w:DoNotOptimizeForBrowser/>" +
                // "</w:WordDocument>" +
                // "</xml>" +
                // "<![endif]-->");

                //strBody.Append("<style>" +
                // "<!-- /* Style Definitions */" +
                // "@page Section1" +
                // "   {size:8.5in 11.0in; " +
                // "   margin:1.0in 1.25in 1.0in 1.25in ; " +
                // "   mso-header-margin:.5in; " +
                // "   mso-footer-margin:.5in; mso-paper-source:0;}" +
                // " div.Section1" +
                // "   {page:Section1;}" +
                // "-->" +
                // "</style></head>");

                //strBody.Append("<body lang=EN-US style='tab-interval:.5in'>" +
                // "<div class=Section1>");
                //strBody.Append(outXml);
                //strBody.Append("</div></body></html>");

                ////Force this content to be downloaded 
                ////as a Word document with the name of your choice
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=myword.doc");

                //Response.Write(strBody.ToString());

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(50, 20, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }



                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msInput, null);
                //document.Close();

                //byte[] file = ms.ToArray();
                //output.Write(file, 0, file.Length);
                //output.Position = 0;


                ///To get File Path

                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";


                //sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                //sessiondate = sessiondate.Replace('/', ' ');



                if (download)
                {
                    if (LOBType == "Draft")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_LOB.pdf");
                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        // string directory = Server.MapPath(url);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(directory);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Draft_LOB.pdf";
                        //  var path = Path.Combine(Server.MapPath("~" + url), fileName);//sanjay for FileStructure
                        var path = Path.Combine(directory, fileName);
                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);


                        // close file stream
                        _FileStream.Close();


                        var filepath = Path.Combine(Server.MapPath(dbpath));
                        // Get the complete folder path and store the file inside it.  
                        DirectoryInfo Dir1 = new DirectoryInfo(filepath);
                        if (!Dir1.Exists)
                        {
                            Dir1.Create();
                        }
                        string fname = Path.Combine(filepath, fileName);
                        System.IO.FileStream _FileStream1 = new System.IO.FileStream(fname, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream1.Close();
                        // DownloadPDF(path);

                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        //XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        //System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        //sers.Serialize(wr, pdfBytes);
                        //XmlDocument doc = new XmlDocument();
                        //doc.LoadXml(sb.ToString());

                        //ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");

                        #endregion

                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", dbpath + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedLOBPath]", methodParameter);
                        return null;

                    }
                    else if (LOBType == "Approved")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Approved_LOB.pdf");
                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        // string directory = Server.MapPath(url);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(directory);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Approved_LOB.pdf";
                        var path = Path.Combine(directory, fileName);
                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion

                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", dbpath + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedApprovedLOBPath]", methodParameter);

                        //string pathfileforsign = url + fileName;
                        //string imageFileLoc = Server.MapPath("/SignaturePath/246639196407.gif");
                        //fileName = "Approved_LOB_Sign.pdf";
                        //string newFileLocation = url + fileName;

                        //PDFExtensions.InsertImageToPdf(Server.MapPath(pathfileforsign), imageFileLoc, Server.MapPath(newFileLocation), 4, 4);


                        return null;
                    }
                    else if (LOBType == "Published")
                    {

                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Published_" + LOBId + ".pdf");
                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(directory);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Published_" + LOBId + ".pdf";
                        var path = Path.Combine(directory, fileName);
                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion

                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@PublishLOBPath", dbpath + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_PublishedLOBPath]", methodParameter);
                        return null;
                    }
                }
            }

            return File(output, "application/pdf");
        }
        [HttpGet]

        public FileStreamResult GenerateLOBPdfForLegis(string Sessiondate, string LOBType)
        {

            string LOBId = "";
            int AssemblyId = (int)Helper.ExecuteService("PaperLaid", "AssemblyCode", null);
            int SessionId = (int)Helper.ExecuteService("PaperLaid", "SessionCode", null);
            List<KeyValuePair<string, string>> nmethodParameter1 = new List<KeyValuePair<string, string>>();
            nmethodParameter1.Add(new KeyValuePair<string, string>("@AssemblyId", Convert.ToString(AssemblyId)));
            nmethodParameter1.Add(new KeyValuePair<string, string>("@SessionId", Convert.ToString(SessionId)));
            nmethodParameter1.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));

            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLOBFromAssemblySessionDate", nmethodParameter1);
            List<DataRow> list = dataSetLOB.Tables[0].AsEnumerable().ToList();

            List<AddLOBModel> List = new List<AddLOBModel>();
            var dt = dataSetLOB.Tables[0];
            if (dt.Rows.Count > 0)
            {
                LOBId = dt.Rows[0]["LOBId"].ToString();
            }
            else
            {
                LOBId = "132";
            }



            bool download = true;
            //string LOBType = "";
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string LOBName = "20Dec2013_1_LOB";
            string CurrentSecretery = "";
            string CurrentSpeaker = "";
            string CurrenttSessionPlace = "";
            DateTime SessionDate = new DateTime();
            string savedPDF = string.Empty;


            MemoryStream output = new MemoryStream();
            if (LOBId != null && LOBId != "")
            {
                LOBModel objLOBModel = new LOBModel();
                DataSet SummaryDataSet = new DataSet();
                string sessiondate = "";
                string outXml = "";
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));

                if (LOBType == "Draft")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }
                else if (LOBType == "Approved")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
                }
                else
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }


                //Getting Current Secretary
                DataSet SiteSettingDataSet = new DataSet();
                var methodParameter1 = new List<KeyValuePair<string, string>>();
                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
                {
                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
                    if (dtSiteSetting != null)
                    {
                        IEnumerable<DataRow> siteSettingQuery = from siteSetting in dtSiteSetting.AsEnumerable() select siteSetting;


                        IEnumerable<DataRow> curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
                        }

                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSpeaker");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSpeaker = obj.Field<string>("SettingValueLocal");
                        }

                        curtSecretery = siteSettingQuery.Where(p => p.Field<string>("SettingName") == "SessionPlace");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrenttSessionPlace = obj.Field<string>("SettingValueLocal");
                        }
                    }
                }
#pragma warning disable CS0219 // The variable 'sessionplace' is assigned but its value is never used
                string sessionplace = "";
#pragma warning restore CS0219 // The variable 'sessionplace' is assigned but its value is never used
                string SubittedDate = "";
                string SubittedTime = "";
                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {

                    outXml = @"<html>                           
                                 <body style='font-family:DVOT-Surekh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    string SrNo1 = "";
                    string SrNo2 = "";
#pragma warning disable CS0219 // The variable 'SrNo3' is assigned but its value is never used
                    string SrNo3 = "";
#pragma warning restore CS0219 // The variable 'SrNo3' is assigned but its value is never used
                    int SrNo1Count = 1;
                    int SrNo2Count = 1;
                    int SrNo3Count = 1;
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {

                        if (i == 0)
                        {
                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            string date = ConvertSQLDate(SessionDate);
                            date = date.Replace("/", "");
                            date = date.Replace("-", "");
                            //SessionDate = DateTime.ParseExact(sessiondate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                            sessiondate = sessiondate.Replace('/', ' ');
                            // SessionDate = SessionDate.Replace('/', ' ');                     
                            //TempData["addLines"] = sessiondate;

                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


                            outXml += @"<tr>
                                                    <td style='text-align: center; font-size: 60px; font-weight: bold;'>               
                                                         हिमाचल प्रदेश&nbsp;" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align: center; font-size: 34px; font-weight: bold;'>               
                                                          " + "कार्यसूची" + @"    
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 36px; font-weight: bold'>
                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style='text-align: center; font-size: 38px; font-weight: bold;'>               
                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 32px; font-weight: bold;text-decoration: underline;'>               
                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
                                                    </td>
                                                </tr>";

                            if (LOBType == "Draft")
                            {
                                if (SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"] != null && Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]) != "")
                                {
                                    SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]).ToString("D", new CultureInfo("hi")); ;
                                }
                                else
                                {
                                    SubittedDate = System.DateTime.Now.ToString("dd/MM/yyyy");
                                }

                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]) != "")
                                {
                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]));
                                    DateTime time = DateTime.Today.Add(interval);
                                    string displayTime = time.ToString("hh:mm tt");

                                    SubittedTime = displayTime;
                                }
                                else
                                {
                                    SubittedTime = "";
                                }
                            }
                            else
                            {

                                SubittedDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]).ToString("D", new CultureInfo("hi"));
                                if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]) != "")
                                {
                                    TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedTime"]));
                                    DateTime time = DateTime.Today.Add(interval);
                                    string displayTime = time.ToString("hh:mm tt");

                                    SubittedTime = displayTime;
                                }
                                else
                                {
                                    SubittedTime = "";
                                }
                            }

                        }
                        //                        string Srn1 =Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]);
                        //                        var Srn2 = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]);
                        //                        var Srn3 = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]);
                        //                        if (Srn1 != "")
                        //                        {
                        //                            if (Srn2 != "")
                        //                            {
                        //                                if (Srn3 != "")
                        //                                {
                        //                                    SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";

                        //                                    outXml += @"<tr><td style='text-align: left; font-size: 32px;'>
                        //                                              <div style='padding-left:60px;text-align:justify;'>
                        //                                              <b>" + SrNo3 + @"</b>&nbsp;&nbsp;&nbsp;
                        //                                              <div style='padding-left:15px;margin: -50px 0 -20 10%;text-align:justify;'>";
                        //                                    SrNo3Count = SrNo3Count + 1;
                        //                                }
                        //                                else
                        //                                {
                        //                                    SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                        //                                    outXml += @"<tr><td style='text-align: left; font-size: 32px;'><div style='padding-left:30px;text-align:justify;'>
                        //                                              <b></b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px;margin: -51px 0 -20 7.5%;text-align:justify;'>";
                        //                                    SrNo2Count = SrNo2Count + 1;
                        //                                    SrNo3Count = 1;
                        //                                }
                        //                            }
                        //                            else
                        //                            {
                        //                                SrNo1 = Convert.ToString(SrNo1Count) + ".";
                        //                                outXml += @"<tr><td style='text-align: left; font-size: 34px;'>
                        //                                         <div style='padding-left:10px;text-align:justify;'>
                        //                                         <b>  " + SrNo1 + @" </b> &nbsp;&nbsp;&nbsp; <div style='padding-left:20px; margin: -50px 0 -20 2%;text-align:justify;'>";
                        //                                SrNo1Count = SrNo1Count + 1;
                        //                                SrNo2Count = 1;
                        //                            }
                        //                        }
                        string b = "";
                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
                            outXml += @"<tr><td style='text-align: left; font-size: 34px;'>
                                     <div style='padding-left:10px;text-align:justify;'>
                                  <b>  " + SrNo1 + @" </b> &nbsp;&nbsp;&nbsp; <div style='padding-left:20px; margin: -50px 0 -20 2%;text-align:justify;'>";
                            SrNo1Count = SrNo1Count + 1;
                            SrNo2Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {
//                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
//                            outXml += @"<tr><td style='text-align: left; font-size: 32px;'><div style='padding-left:30px;text-align:justify;'>
//                                    <b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px; margin: -51px 0 -20 7.5%;text-align:justify;'>";
//                            SrNo2Count = SrNo2Count + 1;
//                            SrNo3Count = 1;

                            int a = SrNo2Count;
                            SrNo2Count = SrNo2Count + 1;

                            DraftLOB lob = new DraftLOB();
                            b = SrNo1.Replace(".", "");
                            lob.SrNo1 = Convert.ToInt32(b);
                            lob.SrNo2 = Convert.ToInt32(SrNo2Count);
                           // lob.SrNo3 = Convert.ToInt32(SrNo2Count);
                            lob.LOBId = Convert.ToInt32(SummaryDataSet.Tables[0].Rows[i]["LOBId"]); ;
                            lob = (DraftLOB)Helpers.Helper.ExecuteService("PaperLaid", "CheckNextSrNo", lob);
                            if (lob.PPTLocation != "null")
                            {
                                SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(a) + ")";
                                outXml += @"<tr><td style='text-align: left; font-size: 32px;'><div style='padding-left:30px;text-align:justify;'>
                                                                <b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px;margin: -51px 0 -20 7.5%;text-align:justify;'>";
                            }
                            else
                            {
                                int ne1 = a - 1;
                                int old2 = Convert.ToInt32(b);
                                DraftLOB lob1 = new DraftLOB();
                                lob1.SrNo1 = Convert.ToInt32(old2);
                                lob1.SrNo2 = Convert.ToInt32(ne1);
                                lob1.LOBId = Convert.ToInt32(SummaryDataSet.Tables[0].Rows[i]["LOBId"]); ;
                                lob1 = (DraftLOB)Helpers.Helper.ExecuteService("PaperLaid", "CheckPreviousSrNo", lob1);
                                if (lob1.PPTLocation != "null")
                                {
                                    SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(a) + ")";
                                    outXml += @"<tr><td style='text-align: left; font-size: 32px;'><div style='padding-left:30px;text-align:justify;'>
                                                                <b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px;margin: -51px 0 -20 7.5%;text-align:justify;'>";
                                }
                                else
                                {
                                    //SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(a) + ")";
                                    outXml += @"<tr><td style='text-align: left; font-size: 32px;'><div style='padding-left:30px;text-align:justify;'>
                                                                <b></b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px;margin: -51px 0 -20 7.5%;text-align:justify;'>";
                                }
                            }

                            SrNo3Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
                        {

//                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";

//                            outXml += @"<tr>
//                                        <td style='text-align: left; font-size: 32px;'>
//                                        <div style='padding-left:60px;text-align:justify;'>
//                                        <b>" + SrNo3 + @"</b>&nbsp;&nbsp;&nbsp;
//                                        <div style='padding-left:15px;margin: -50px 0 -20 10%;text-align:justify;'>";
//                            SrNo3Count = SrNo3Count + 1;



                            int a = SrNo3Count;
                            string nSrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                            SrNo3Count = SrNo3Count + 1;
                          
                            DraftLOB lob = new DraftLOB();
                            b = SrNo1.Replace(".", "");
                            lob.SrNo1 = Convert.ToInt32(b);
                            lob.SrNo2 = Convert.ToInt32(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]);
                            lob.SrNo3 = Convert.ToInt32(SrNo3Count);
                            lob.LOBId = Convert.ToInt32(SummaryDataSet.Tables[0].Rows[i]["LOBId"]); ;
                            lob = (DraftLOB)Helpers.Helper.ExecuteService("PaperLaid", "CheckNextSubSrNo", lob);
                            if (lob.PPTLocation != "null")
                            {
                                //SrNo3 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(nSrNo3) + ")";

                                string Text = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                                //SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(a) + ")";
                                if (Text.Contains("<img src"))
                                {
                                    outXml += @"<tr>
                                        <td style='text-align: left; font-size: 32px;'>
                                        <div style='padding-left:60px;text-align:justify;'>
                                       <b>" + nSrNo3 + @"</b>&nbsp;&nbsp;&nbsp;
                                        <div style='padding-left:15px;margin: -185 0 -20 10%;text-align:justify;'>";
                                }
                                else 
                                {
                                    
                                        outXml += @"<tr>
                                        <td style='text-align: left; font-size: 32px;'>
                                        <div style='padding-left:60px;text-align:justify;'>
                                       <b>" + nSrNo3 + @"</b>&nbsp;&nbsp;&nbsp;
                                        <div style='padding-left:15px;margin: -50px 0 -20 10%;text-align:justify;'>";
                                    
                                    
                                }


                              
                            }
                            else
                            {
                                int ne1 = a - 1;
                                int old2 = Convert.ToInt32(b);
                                DraftLOB lob1 = new DraftLOB();
                                lob1.SrNo1 = Convert.ToInt32(old2);
                                lob1.SrNo2 = Convert.ToInt32(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]);
                                lob1.SrNo3 = Convert.ToInt32(ne1);
                                lob1.LOBId = Convert.ToInt32(SummaryDataSet.Tables[0].Rows[i]["LOBId"]); ;
                                lob1 = (DraftLOB)Helpers.Helper.ExecuteService("PaperLaid", "CheckPreviousSubSrNo", lob1);
                                string Text = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                                if (lob1.PPTLocation != "null")
                                {
                                    //if (Text.Contains("दिन के लिए"))
                                    if (Text.Contains("imggg"))
                                    {
                                        outXml += @"<tr>
                                       <td style='text-align: left; font-size: 32px;'>
                                        <div style='padding-left:60px;text-align:justify;margin:-90px;'>
                                        <b>" + nSrNo3 + @"</b>&nbsp;&nbsp;&nbsp;
                                        <div style='padding-left:15px;margin: 0 0 -20 10%;text-align:justify;'>";
                                    }
                                    else 
                                    {

                                        outXml += @"<tr>
                                       <td style='text-align: left; font-size: 32px;'>
                                        <div style='padding-left:60px;text-align:justify;'>
                                        <b>" + nSrNo3 + @"</b>&nbsp;&nbsp;&nbsp;
                                        <div style='padding-left:15px;margin: -50px 0 -20 10%;text-align:justify;'>";
                                    }

                                    //SrNo3 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(nSrNo3) + ")";
                                   
                                }
                                else
                                {
                                   
                                    //SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(a) + ")";
                                    if (Text.Contains("<img src"))
                                    {
                                        outXml += @"<tr>
                                        <td style='text-align: left; font-size: 32px;'>
                                        <div>                                       
                                      <div style='padding-left:15px;margin: -50px 0 -20 10%;text-align:justify;'>";
                                    }
                                    else 
                                    {
                                        outXml += @"<tr>
                                        <td style='text-align: left; font-size: 32px;'>
                                        <div style='padding-left:60px;text-align:justify;'>
                                        <b></b>&nbsp;&nbsp;&nbsp;                                       
                                      <div style='padding-left:15px;margin: -50px 0 -20 10%;text-align:justify;'>";
                                    }
                                    
                                }
                            }

                        }

                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");                           
                            LOBText = LOBText.Replace("</p>", "");
                      //      LOBText = LOBText.Replace("}", "</div></div></td><td style='text-align: left;width:50%; font-size: 29px;'>}</td><td  style='font-size: 32px;'><div style='text-align:justify;'><div style=';text-align:justify;'>");
                        }


                        outXml += @"" + LOBText + "</div></div> ";
                        //                        if (SrNo3 == "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(ii)")
                        //                        {

                        //                              outXml += @"<div><span><img src='~/Areas/BusinessWork/Views/Shared/Curly.jpg'/></span></div></div>";
                        ////                            outXml += @"<div style='background-image:url('~/Areas/BusinessWork/Views/Shared/Curly.jpg') height: 200px; width: 400px; border: 1px solid black;'>
                        ////                                      <img src='~/Areas/BusinessWork/Views/Shared/Curly.jpg'></div>";
                        //                        }

                        outXml += @"<br/> </td></tr>";


                    }

                    if (LOBType == "Draft")
                    {
                        //Bottom Section
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 30px;'>
                                               <b> (" + CurrentSpeaker + @"),</b>
                                                </td>
                                                <td style='width:25%;float:left;font-size: 30px;'>
                                                <b> (" + CurrentSecretery + @"),  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 32px;padding-left:10%;'>
                                               <b> महापौर ।</b>
                                                </td>
                                                <td style='width:25%;float:right;text-align:right;padding-right:80px;font-size: 32px;'>
                                               <b> आयुक्त ।</b>
                                                </td>

                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }
                    else if (LOBType == "Approved")
                    {
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 30px;'>
                                     
                                                    <b> " + CurrenttSessionPlace + @",</b>
                                                </td>
                                                <td style='width:25%;float:left;font-size: 30px;'>
                                                <b> " + CurrentSecretery + @",  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:75%;float:left;font-size: 32px;'>
                                               <b> दिनांक: " + SubittedDate + @"</b>
                                              
                                                </td>
                                                <td style='width:25%;float:right;text-align:right;padding-right:85px;font-size: 32px;'>
                                               <b>  आयुक्त ।</b>
                                                </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 18px;' colspan='2'>               
                                                          " + "******************" + @"    
                                                    </td>
                                                </tr>
<tr>
                                                    <td style='text-align: center; font-size: 22px;' colspan='2'>               
                                                          " + "(अनुपूरक कार्यसूची, यदि कोई हो,की भी जांच कर लें)" + @"    
                                                    </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }
                }

                // convert HTML to PDF
                EvoPdf.Document document1 = new EvoPdf.Document();
                AddElementResult addResult;
                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(50, 55, 40, 30);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(20, 55, 75, 65), PdfPageOrientation.Portrait);



                //*****************
                // The position on X anf Y axes where to add the next element
                // Add section title
                // float yLocation = 75;
                // float xLocation = 75;
                // float titlesYLocation = yLocation;
                //// EvoPdf.PdfFont font = new EvoPdf.PdfFont;
                // // string f = font.FontFamily;
                // EvoPdf.PdfFont titleFont = document1.AddFont(new EvoPdf.StandardCJKFont());
                // TextElement titleTextElement = new TextElement(xLocation, yLocation, "", titleFont);
                // titleTextElement.ForeColor = Color.Black;
                // addResult = page.AddElement(titleTextElement);
                // yLocation = addResult.EndPageBounds.Bottom + 10;
                // page = addResult.EndPdfPage;
                // //EvoPdf.PdfFont subtitleFont = document1.AddFont("Times New Roman");
                // EvoPdf.PdfFont subtitleFont = document1.AddFont(new EvoPdf.StandardCJKFont());
                // TextElement subtitleTextElement = new TextElement(xLocation, titlesYLocation, "", subtitleFont);
                // //subtitleTextElement.ForeColor = Color.Navy;
                // addResult = page.AddElement(subtitleTextElement);

                // page = addResult.EndPdfPage;
                // float imagesYLocation = addResult.EndPageBounds.Bottom + 3;

                // string imagePath = Server.MapPath("~/Areas/BusinessWork/Views/Shared/Curly.jpg");
                // ImageElement unscaledImageElement = new ImageElement(xLocation, imagesYLocation, imagePath);
                // addResult = page.AddElement(unscaledImageElement);

                // RectangleF scaledDownImageRectangle = new RectangleF(addResult.EndPageBounds.Right + 3, addResult.EndPageBounds.Y,
                //             addResult.EndPageBounds.Width, addResult.EndPageBounds.Height);

                //





                //*****************
                //EvoPdf.PdfFont ft = document1.AddFont("DVOT-Surekh");
                //var strBody = new StringBuilder();

                //strBody.Append("<html " + "xmlns:o='urn:schemas-microsoft-com:office:office' " + "xmlns:w='urn:schemas-microsoft-com:office:word'" +
                //  "xmlns='http://www.w3.org/TR/REC-html40'>" + "<head><title>Time</title>");

                ////The setting specifies document's view after it is downloaded as Print
                ////instead of the default Web Layout
                //strBody.Append("<!--[if gte mso 9]>" +
                // "<xml>" +
                // "<w:WordDocument>" +
                // "<w:View>Print</w:View>" +
                // "<w:Zoom>90</w:Zoom>" +
                // "<w:DoNotOptimizeForBrowser/>" +
                // "</w:WordDocument>" +
                // "</xml>" +
                // "<![endif]-->");

                //strBody.Append("<style>" +
                // "<!-- /* Style Definitions */" +
                // "@page Section1" +
                // "   {size:8.5in 11.0in; " +
                // "   margin:1.0in 1.25in 1.0in 1.25in ; " +
                // "   mso-header-margin:.5in; " +
                // "   mso-footer-margin:.5in; mso-paper-source:0;}" +
                // " div.Section1" +
                // "   {page:Section1;}" +
                // "-->" +
                // "</style></head>");

                //strBody.Append("<body lang=EN-US style='tab-interval:.5in'>" +
                // "<div class=Section1>");
                //strBody.Append(outXml);
                //strBody.Append("</div></body></html>");

                ////Force this content to be downloaded 
                ////as a Word document with the name of your choice
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=myword.doc");

                //Response.Write(strBody.ToString());

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(50, 20, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }



                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msInput, null);
                //document.Close();

                //byte[] file = ms.ToArray();
                //output.Write(file, 0, file.Length);
                //output.Position = 0;


                ///To get File Path

                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";


                //sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                //sessiondate = sessiondate.Replace('/', ' ');



                if (download)
                {
                    if (LOBType == "Draft")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_LOB.pdf");
                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        // string directory = Server.MapPath(url);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(directory);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Draft_LOB.pdf";
                        //  var path = Path.Combine(Server.MapPath("~" + url), fileName);//sanjay for FileStructure
                        var path = Path.Combine(directory, fileName);
                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);


                        // close file stream
                        _FileStream.Close();


                        var filepath = Path.Combine(Server.MapPath(dbpath));
                        // Get the complete folder path and store the file inside it.  
                        DirectoryInfo Dir1 = new DirectoryInfo(filepath);
                        if (!Dir1.Exists)
                        {
                            Dir1.Create();
                        }
                        string fname = Path.Combine(filepath, fileName);
                        System.IO.FileStream _FileStream1 = new System.IO.FileStream(fname, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream1.Close();
                        // DownloadPDF(path);

                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        //XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        //System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        //sers.Serialize(wr, pdfBytes);
                        //XmlDocument doc = new XmlDocument();
                        //doc.LoadXml(sb.ToString());

                        //ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");

                        #endregion

                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", dbpath + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedLOBPath]", methodParameter);
                        return null;

                    }
                    else if (LOBType == "Approved")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Approved_LOB.pdf");
                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        // string directory = Server.MapPath(url);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(directory);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Approved_LOB.pdf";
                        var path = Path.Combine(directory, fileName);
                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion

                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", dbpath + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedApprovedLOBPath]", methodParameter);

                        //string pathfileforsign = url + fileName;
                        //string imageFileLoc = Server.MapPath("/SignaturePath/246639196407.gif");
                        //fileName = "Approved_LOB_Sign.pdf";
                        //string newFileLocation = url + fileName;

                        //PDFExtensions.InsertImageToPdf(Server.MapPath(pathfileforsign), imageFileLoc, Server.MapPath(newFileLocation), 4, 4);


                        return null;
                    }
                    else if (LOBType == "Published")
                    {

                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Published_" + LOBId + ".pdf");
                        // string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        DirectoryInfo Dir = new DirectoryInfo(directory);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        fileName = "Published_" + LOBId + ".pdf";
                        var path = Path.Combine(directory, fileName);
                        string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion

                        methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                        methodParameter.Add(new KeyValuePair<string, string>("@PublishLOBPath", dbpath + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_PublishedLOBPath]", methodParameter);
                        return null;
                    }
                }
            }

            return File(output, "application/pdf");
        }

        public bool CheckSessionDateLOB(string Sessiondate)
        {
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLOBFromSessionDate", methodParameter);
            bool result = false;
            if (dataSetLOB.Tables[0].Rows.Count > 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        public JsonResult EditSessionDateLOB(string Sessiondate)
        {
            if (Sessiondate == null)
            {
                Sessiondate = "23/08/2018";
            }
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLOBFromSessionDate", methodParameter);

            List<AddLOBModel> List = new List<AddLOBModel>();

            if (dataSetLOB.Tables[0].Rows.Count > 0)
            {
                // IEnumerable<DataRow> sequence = dataSetLOB.AsEnumerable();
                List<DataRow> list = dataSetLOB.Tables[0].AsEnumerable().ToList();
                //List = list < AddLOBModel> list;
                return Json(list);
            }
            else
            {
                return Json(List);
                // return List;
            }
        }

        [HttpGet]
        public JsonResult GetLOBDetails(string Sessiondate)
        {
            int AssemblyId =int.Parse ( CurrentSession.AssemblyId);
               // (int)Helper.ExecuteService("PaperLaid", "AssemblyCode", null);
            int SessionId = int.Parse(CurrentSession.SessionId);
            //(int)Helper.ExecuteService("PaperLaid", "SessionCode", null);
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

            ErrorLog.WriteToLog("GetLOBDetails.Sessiondate:" + Sessiondate);
            ErrorLog.WriteToLog("GetLOBDetails.AssemblyId:" + AssemblyId);
            ErrorLog.WriteToLog("GetLOBDetails.SessionId:" + SessionId);
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));
            methodParameter.Add(new KeyValuePair<string, string>("@AssemblyId", Convert.ToString(AssemblyId)));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", Convert.ToString(SessionId)));

            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLOBFromAssemblySessionDate", methodParameter);
            List<DataRow> list = dataSetLOB.Tables[0].AsEnumerable().ToList();

            List<AddLOBModel> List = new List<AddLOBModel>();

            var dt = dataSetLOB.Tables[0];
            string destDirectory = Server.MapPath("~/LOBTemp/");

            //delete files:
            DirectoryInfo directory = new DirectoryInfo(destDirectory);
            directory.GetFiles().ToList().ForEach(f => f.Delete());


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AddLOBModel student = new AddLOBModel();
             
                student.SrNo1 = dt.Rows[i]["SrNo1"].ToString();
                ErrorLog.WriteToLog("GetLOBDetails.SrNo1:" + student.SrNo1);
                student.SrNo2 = dt.Rows[i]["SrNo2"].ToString();
                ErrorLog.WriteToLog("GetLOBDetails.SrNo2:" + student.SrNo2);
                student.SrNo3 = dt.Rows[i]["SrNo3"].ToString();

                ErrorLog.WriteToLog("GetLOBDetails.SrNo3:" + student.SrNo3);

                if (student.SrNo3 != "")
                {
                    student.SrNo3 = "(" + ToRoman(Convert.ToInt32(student.SrNo3)) + ")";
                }
                student.TextLOB = dt.Rows[i]["TextLOB"].ToString();
                ErrorLog.WriteToLog("GetLOBDetails.TextLOB:" + student.TextLOB);
                student.ConcernedEventId = dt.Rows[i]["ConcernedEventId"].ToString();
                ErrorLog.WriteToLog("GetLOBDetails.ConcernedEventId:" + student.ConcernedEventId);
                student.ConcernedEventName = dt.Rows[i]["ConcernedEventName"].ToString();
                ErrorLog.WriteToLog("GetLOBDetails.ConcernedEventName:" + student.ConcernedEventName);
                student.PageBreak = Convert.ToBoolean(dt.Rows[i]["PageBreak"]);
                student.stringPageBreak = dt.Rows[i]["PageBreak"].ToString();
                student.IsSubmitted ="False" ;
                  //  dt.Rows[i]["IsSubmitted"].ToString();
                student.PaperLaidId = dt.Rows[i]["PaperLaidId"].ToString();

                String LOBRecordId = dt.Rows[i]["Id"].ToString();
                List<KeyValuePair<string, string>> param1 = new List<KeyValuePair<string, string>>();
                param1.Add(new KeyValuePair<string, string>("@LOBRecordId", LOBRecordId));
                DataSet getdataSet1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetPaperIdbyRecordId", param1);
                if (getdataSet1.Tables[0].Rows.Count > 0)
                {

                    string PaperLaidId = Convert.ToString(getdataSet1.Tables[0].Rows[0]["PaperLaidId"]);
                    ErrorLog.WriteToLog("GetLOBDetails.PaperLaidId:" + PaperLaidId);
                    student.PaperLaidId = PaperLaidId;

                }






                if (dt.Rows[i]["PDFLocation"].ToString() != "")
                {
                    string fileName = "";
                    student.PDFLocation = dt.Rows[i]["PDFLocation"].ToString();
                    string filstring = student.PDFLocation.TrimStart('~');
                    student.PDFLocation = filstring;
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string sourceFile = FileSettings.SettingValue + student.PDFLocation;
                    Uri uri = new Uri(sourceFile);
                    if (uri.IsFile)
                    {
                        fileName = System.IO.Path.GetFileName(uri.LocalPath);

                    }
                    student.NewPdfPath = fileName;
                    string destFile = System.IO.Path.Combine(destDirectory, fileName);
                    System.IO.File.Copy(sourceFile, destFile, true);
                }
                else
                {

                    student.NewPdfPath = "";

                }

                List.Add(student);

            }
            ErrorLog.WriteToLog("List:" + List);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public virtual ActionResult Download(string file)
        {
            string[] tokens = file.Split(',');
            file = tokens[0];
            string path = tokens[1];
            path = path.Replace("/", " ");

            path = "~/LOB/13/4/" + path;
            //string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
            file = "Draft_LOB.pdf";
            string fullPath = Path.Combine(Server.MapPath(path), file);
            return File(fullPath, "application/pdf", file);
        }

        public JsonResult CopyFileToTemp(int PaperLaidId, string FileName)
        {
            string Result = "";
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
#pragma warning disable CS0219 // The variable 'Description' is assigned but its value is never used
            string Description = "";
#pragma warning restore CS0219 // The variable 'Description' is assigned but its value is never used
            string fileName = FileName;
            long PaperLaidIdTemp = 0;
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
            if (PaperLaidId != null && PaperLaidId != 0)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
            {
                tPaperLaidTemp PaperLaid = new tPaperLaidTemp();
                PaperLaid.PaperLaidId = Convert.ToInt32(PaperLaidId);

                PaperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempId", PaperLaid);

                foreach (var item in PaperLaid.ListPaperLaidTemp)
                {
                    //Description = item.Description;
                    PaperLaidIdTemp = item.PaperLaidTempId;
                }


            }


#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'long' is never equal to 'null' of type 'long?'
            if (PaperLaidIdTemp != null)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'long' is never equal to 'null' of type 'long?'
            {

                tPaperLaidTemp PaperLaid = new tPaperLaidTemp();
                PaperLaid.PaperLaidTempId = Convert.ToInt32(PaperLaidIdTemp);
                PaperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", PaperLaid);
                string Location = "";
                methodParameter = new List<KeyValuePair<string, string>>();


                string sourcePath = PaperLaid.SignedFilePath;
                Location = PaperLaid.SignedFilePath;
                int index = Location.LastIndexOf(@"/");
                Location = Location.Substring(index + 1);
                sourcePath = sourcePath.Substring(0, index);
                string fname;








                var filepath = System.IO.Path.Combine("\\LOBTemp\\");
                // Get the complete folder path and store the file inside it.  
                fname = Path.Combine(Server.MapPath(filepath));


                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                // string directory = FileSettings.SettingValue + "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";
                DirectoryInfo Dir = new DirectoryInfo(fname);
                //if (!Dir.Exists)
                //{
                //    Dir.Create();
                //}
                if (!System.IO.Directory.Exists(fname))
                {
                    System.IO.Directory.CreateDirectory(fname);
                }
                string sourcedirectory = FileSettings.SettingValue + sourcePath;
                //string directory = FolderCreate();


                // addLines.LOBId + "_PDF_" + Location;

                //  fileName = fileName + ".pdf";
                string sourceFile = System.IO.Path.Combine(sourcedirectory, Location);
                string destFile = System.IO.Path.Combine(fname, fileName);
                //  string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";
                // To copy a folder's contents to a new location: 
                // Create a new target folder, if necessary. 
                if (!System.IO.Directory.Exists(fname))
                {
                    System.IO.Directory.CreateDirectory(fname);
                }

                // To copy a file to another location and  
                // overwrite the destination file if it already exists.
                System.IO.File.Copy(sourceFile, destFile, true);
                Result = Location + "," + sourcePath;
            }

            // string Result = Location + "," + "Updated";
            //  string Result ="";
            return Json(Result, JsonRequestBehavior.AllowGet);

        }
        public JsonResult CopyCRFileToTemp(int PaperLaidId, string FileName, string FPath)
        {
            string Result = "";
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
#pragma warning disable CS0219 // The variable 'Description' is assigned but its value is never used
            string Description = "";
#pragma warning restore CS0219 // The variable 'Description' is assigned but its value is never used
            string fileName = FileName;
#pragma warning disable CS0219 // The variable 'PaperLaidIdTemp' is assigned but its value is never used
            long PaperLaidIdTemp = 0;
#pragma warning restore CS0219 // The variable 'PaperLaidIdTemp' is assigned but its value is never used
            //if (PaperLaidId != null && PaperLaidId != 0)
            //{
            //    tPaperLaidTemp PaperLaid = new tPaperLaidTemp();
            //    PaperLaid.PaperLaidId = Convert.ToInt32(PaperLaidId);

            //    PaperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempId", PaperLaid);

            //    foreach (var item in PaperLaid.ListPaperLaidTemp)
            //    {
            //        //Description = item.Description;
            //        PaperLaidIdTemp = item.PaperLaidTempId;
            //    }


            //}




            //tPaperLaidTemp PaperLaid = new tPaperLaidTemp();
            //PaperLaid.PaperLaidTempId = Convert.ToInt32(PaperLaidIdTemp);
            //PaperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", PaperLaid);
            //string Location = "";
            //methodParameter = new List<KeyValuePair<string, string>>();


            //string sourcePath = PaperLaid.SignedFilePath;
            //Location = PaperLaid.SignedFilePath;
            //int index = Location.LastIndexOf(@"/");
            //Location = Location.Substring(index + 1);
            //sourcePath = sourcePath.Substring(0, index);
            string fname;
            string sourcePath = "";
            string Location = "";
            var filepath = System.IO.Path.Combine("\\LOBTemp\\");
            // Get the complete folder path and store the file inside it.  
            fname = Path.Combine(Server.MapPath(filepath));


            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);


            DirectoryInfo Dir = new DirectoryInfo(fname);

            if (!System.IO.Directory.Exists(fname))
            {
                System.IO.Directory.CreateDirectory(fname);
            }

            string str = FPath.Replace("http://secure.shimlamc.org/SecureFileStructure//", "");
            string sourcedirectory = FileSettings.SettingValue + str;

            //    string sourcedirectory = FileSettings.SettingValue + sourcePath;
            // http://secure.shimlamc.org/SecureFileStructure//LOB/13/3/27%2008%202018/Documents/4_1_1.pdf
            // LOB/13/3/27%2008%202018/Documents/4_1_1.pdf   
            // string sourcedirectory = FPath;
            //string directory = FolderCreate();


            // addLines.LOBId + "_PDF_" + Location;

            //  fileName = fileName + ".pdf";

            string sourceFile = System.IO.Path.Combine(sourcedirectory);
            string destFile = System.IO.Path.Combine(fname, fileName);
            //  string dbpath = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + Sessiondate + "/Documents/";
            // To copy a folder's contents to a new location: 
            // Create a new target folder, if necessary. 
            if (!System.IO.Directory.Exists(fname))
            {
                System.IO.Directory.CreateDirectory(fname);
            }

            // To copy a file to another location and  
            // overwrite the destination file if it already exists.
            System.IO.File.Copy(sourceFile, destFile, true);


            // PaperLaid/13/4/13_4_C_7354_V2.pdf
            if (str != null)
            {
                string[] tokens = str.Split('/');
                sourcePath = tokens[0] + "/" + tokens[1] + "/" + tokens[2];
                Location = tokens[3];
            }

            Result = Location + "," + sourcePath;


            // string Result = Location + "," + "Updated";
            //  string Result ="";
            return Json(Result, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public JsonResult GetAllEvents(string Id)
        {
            AddLOBModel addLines = new AddLOBModel();
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            List<AddLOBModel> List = new List<AddLOBModel>();
            methodParameter = new List<KeyValuePair<string, string>>();
            DataSet dataSetEvent = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEventsLOB", methodParameter);
            List<mEvent> EventList = new List<mEvent>();
            mEvent Event1 = new mEvent();
            Event1.EventId = "0";
            Event1.EventName = "Select";
            EventList.Add(Event1);

            for (int i = 0; i < dataSetEvent.Tables[0].Rows.Count; i++)
            {
                mEvent Event2 = new mEvent();
                Event2.EventId = Convert.ToString(dataSetEvent.Tables[0].Rows[i]["EventID"]);
                Event2.EventName = Convert.ToString(dataSetEvent.Tables[0].Rows[i]["EventName"]);
                EventList.Add(Event2);
            }

            addLines.mEventList = EventList;

            return Json(EventList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FreezeLOB(string Sessiondate)
        {
            if (Sessiondate == null)
            {
                Sessiondate = "23/08/2018";
            }
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLOBFromSessionDate", methodParameter);

            List<AddLOBModel> List = new List<AddLOBModel>();
            string LOBId = "";
            //string SrNo = "";
            if (dataSetLOB.Tables[0].Rows.Count > 0)
            {
                //List<DataRow> list = dataSetLOB.Tables[0].AsEnumerable().ToList();
                LOBId = Convert.ToString(Convert.ToInt32(dataSetLOB.Tables[0].Rows[0][1]));
                //for (int i = 0; i < dataSetLOB.Tables[0].Rows.Count; i++)
                //{
                //    String LOBRecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                //    String PaperLaidId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PaperLaidId"]);
                //    if (PaperLaidId.Length > 0)
                //    {
                //        //String LOBRecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                //        //String PaperLaidId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PaperLaidId"]);
                //        List<KeyValuePair<string, string>> param = new List<KeyValuePair<string, string>>();
                //        param.Add(new KeyValuePair<string, string>("@LOBRecordId", LOBRecordId));
                //        param.Add(new KeyValuePair<string, string>("@PaperLaidId", PaperLaidId));
                //        DataSet getdataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Update_PaperLaidVS_OnFreeze", param);
                //    }

                //}

                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                methodParameter1.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SubmitLOBById", methodParameter1);
                GenerateLOBPdf(Sessiondate,"Draft");
                LOBId = "Success";
                return Json(LOBId, JsonRequestBehavior.AllowGet);


                //for (int i = 0; i < dataSetLOB.Tables[0].Rows.Count; i++)
                //{
                //    string a =Convert.ToString(dataSetLOB.Tables[0].Rows[i][20]);
                //    if (a == "")
                //    {
                //         SrNo = Convert.ToString(dataSetLOB.Tables[0].Rows[i][12]) + "_" + Convert.ToString(dataSetLOB.Tables[0].Rows[i][13]) + "_" + Convert.ToString(dataSetLOB.Tables[0].Rows[i][14]);
                //         int s = i;
                //         s++;
                //         LOBId = Convert.ToString(s);
                //         return Json(LOBId, JsonRequestBehavior.AllowGet);     
                //    }

                //}
                //if (SrNo == "")
                //{
                //    List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                //    methodParameter1.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                //    DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SubmitLOBById", methodParameter1);
                //    GenerateLOBPdf(Sessiondate);
                //    LOBId = "Success";
                //    return Json(LOBId, JsonRequestBehavior.AllowGet);
                //}
                //else
                //{
                //    LOBId = SrNo;
                //    return Json(LOBId, JsonRequestBehavior.AllowGet);                    
                //}

            }
            else
            {
                LOBId = "NotSaved";
                return Json(LOBId, JsonRequestBehavior.AllowGet);
                // return List;
            }
        }

        public ActionResult GetSubmittedLOB()
        {
            tPaperLaidV paperLaidModel = new tPaperLaidV();

            string PageNumber = "1";
            string RowsPerPage = "20";
            string loopStart = "1";
            string loopEnd = "5";
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            DataSet SummaryDataSet = new DataSet();
            //  LOBModel objPendingLOB = new LOBModel();
            LOBModel objPendingLOB = new LOBModel();
            AddBusinessModel model = new AddBusinessModel();
           // SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models.PaperLaidSummaryViewModel model1 = new SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models.PaperLaidSummaryViewModel();

            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@PageNumber", PageNumber));
                methodParameter.Add(new KeyValuePair<string, string>("@RowsPerPage", RowsPerPage));
                SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSubmittedLOB]", methodParameter);

                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {
                    List<LOBModel> listLOBForDay = new List<LOBModel>();
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {
                        LOBModel objLOB = new LOBModel();
                        objLOB.LOBId = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);
                        objLOB.AssemblyName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyName"]);
                        objLOB.AssemblyNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]);
                        objLOB.SessionName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionName"]);
                        objLOB.SessionNameLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]);
                        objLOB.SessionDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        objLOB.SessionDateLocal = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]);
                        objLOB.SubmittedDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedDate"]);



                        objLOB.PDFLocation = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedLOBPath"]);
                        //if (objLOB.PDFLocation == null)
                        //{

                        //    GenerateLOBPdf(objLOB.SessionDate);
                        //}



                        if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["IsApproved"]) != "")
                        {
                            objLOB.IsApproved = Convert.ToBoolean(SummaryDataSet.Tables[0].Rows[i]["IsApproved"]);
                            if (objLOB.IsApproved == true)
                            {
                                //DataSet ApprovedDataSet = new DataSet();
                                //var SelectedmethodParameter = new List<KeyValuePair<string, string>>();

                                //SelectedmethodParameter.Add(new KeyValuePair<string, string>("@LOBID", objLOB.LOBId));
                                //ApprovedDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSubmittedApprovedLOBPath]", SelectedmethodParameter);
                                //if (ApprovedDataSet != null)
                                //{
                                //    //if (ApprovedDataSet.Tables[0].Rows.Count >= 1)
                                //    //{
                                //    //    objLOB.PDFLocation = ApprovedDataSet.Tables[0].Rows[0]["LOBPath"].ToString();
                                //    //    if (objLOB.PDFLocation == null || objLOB.PDFLocation == "")
                                //    //    {
                                //    //        objLOB.PDFLocation = ApprovedDataSet.Tables[0].Rows[1]["LOBPath"].ToString();
                                //    //    }
                                //    //}
                                //}
                            }
                        }
                        else
                        {
                            objLOB.IsApproved = false;
                        }
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                        int indx = objLOB.PDFLocation.IndexOf("~");
                        if (indx != -1)
                        {

                            string filstring = objLOB.PDFLocation.TrimStart('~');

                            objLOB.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + filstring);

                        }
                        else if (objLOB.PDFLocation != null && objLOB.PDFLocation != "")
                        {
                            objLOB.PDFLocation = System.IO.Path.Combine(FileSettings.SettingValue + objLOB.PDFLocation);
                        }


                        if (Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]) != "")
                        {
                            TimeSpan interval = TimeSpan.Parse(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SubmittedTime"]));
                            DateTime time = DateTime.Today.Add(interval);
                            string displayTime = time.ToString("hh:mm tt");

                            objLOB.SubmittedTime = displayTime;
                        }
                        else
                        {
                            objLOB.SubmittedTime = "";
                        }

                        int indexof = objLOB.PDFLocation.LastIndexOf('/');
                        objLOB.PPTLocation = objLOB.PDFLocation.Substring(indexof + 1);
                        if (i == 0)
                        {
                            objPendingLOB.ResultCount = Convert.ToInt32(Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TotalRecords"]));
                        }

                
                        listLOBForDay.Add(objLOB);
                    }

                    var newList = listLOBForDay.OrderByDescending(x => x.SessionDate).ThenByDescending(x => x.SubmittedDate).ThenByDescending(x => x.SubmittedTime).ToList();
                    objPendingLOB.ListLOBBYId = newList;
                }

                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    objPendingLOB.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    objPendingLOB.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    objPendingLOB.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    objPendingLOB.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    objPendingLOB.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    objPendingLOB.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    objPendingLOB.loopEnd = Convert.ToInt32("5");
                }




            }



#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
           // return PartialView("GetSubmittedLOB", Tuple.Create(objPendingLOB, model1));
           return PartialView("GetSubmittedLOB", objPendingLOB);

        }
        public JsonResult GetSessionByAssemblyId(int AssemblyId)
        {
            SBL.DomainModel.Models.Session.mSession mdl = new SBL.DomainModel.Models.Session.mSession();
            List<SBL.DomainModel.Models.Session.mSession> SessLst = new List<SBL.DomainModel.Models.Session.mSession>();
            if (AssemblyId != 0)
            {
                mdl.AssemblyID = AssemblyId;
                SessLst = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", mdl);
            }
            return Json(SessLst, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UnFreezeLOB(string Sessiondate)
        {
            if (Sessiondate == null)
            {
                Sessiondate = "23/08/2018";
            }
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLOBFromSessionDate", methodParameter);

            List<AddLOBModel> List = new List<AddLOBModel>();
            string LOBId = "";
            if (dataSetLOB.Tables[0].Rows.Count > 0)
            {
                //List<DataRow> list = dataSetLOB.Tables[0].AsEnumerable().ToList();
                LOBId = Convert.ToString(Convert.ToInt32(dataSetLOB.Tables[0].Rows[0][1]));
                //for (int i = 0; i < dataSetLOB.Tables[0].Rows.Count; i++)
                //{
                //    String LOBRecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                //    String PaperLaidId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PaperLaidId"]);
                //    if (PaperLaidId.Length > 0)
                //    {
                //        //String LOBRecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                //        //String PaperLaidId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PaperLaidId"]);
                //        List<KeyValuePair<string, string>> param = new List<KeyValuePair<string, string>>();
                //        param.Add(new KeyValuePair<string, string>("@LOBRecordId", null));
                //        param.Add(new KeyValuePair<string, string>("@PaperLaidId", PaperLaidId));
                //        DataSet getdataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Update_PaperLaidVS_OnFreeze", param);
                //    }


                //}
                List<DataRow> list = dataSetLOB.Tables[0].AsEnumerable().ToList();
                LOBId = Convert.ToString(Convert.ToInt32(dataSetLOB.Tables[0].Rows[0][1]));
                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                methodParameter1.Add(new KeyValuePair<string, string>("@LOBId", LOBId));
                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_UnFreezeLOBById", methodParameter1);

                return Json(LOBId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(List);
                // return List;
            }
        }
        public JsonResult ApproveLOB(string LOBId)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string result = "";
            try
            {
                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBId", LOBId));

                DataSet dataSet1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionDateByLobId", methodParameter);
                if (dataSet1 != null)
                {
                    String sessdate = Convert.ToString(dataSet1.Tables[0].Rows[0]["SessionDate"]);
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", sessdate));

                }

                methodParameter.Add(new KeyValuePair<string, string>("@ApprovedBy", Utility.CurrentSession.UserID));
                methodParameter.Add(new KeyValuePair<string, string>("@ApprovedDate", Convert.ToString(System.DateTime.Now)));
                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_InsertAdminLOB", methodParameter);
                // GenerateLOBPdf(LOBId, true, "Approved");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }
            var res2 = Json(result, JsonRequestBehavior.AllowGet);
            return res2;
        }
        public ActionResult GetNoticeDetailsById(string NoticeId)
        {

            tPaperLaidV PaperLaid = new tPaperLaidV();
            tPaperLaidV PaperLaidPartial = new tPaperLaidV();




            tMemberNotice PaperLaidNotice = new tMemberNotice();
            PaperLaidNotice.NoticeId = Convert.ToInt32(NoticeId);
            PaperLaidNotice = (tMemberNotice)Helper.ExecuteService("PaperLaid", "GetNoticeDetailsById", PaperLaidNotice);

            SBL.DomainModel.Models.Department.mDepartment deptInfo = new SBL.DomainModel.Models.Department.mDepartment();
            deptInfo.deptId = PaperLaidNotice.DepartmentId;
            deptInfo = (SBL.DomainModel.Models.Department.mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as SBL.DomainModel.Models.Department.mDepartment;
            if (deptInfo != null)
            {
                PaperLaidNotice.DepartmentName = deptInfo.deptname;
            }


            mMember mM = new mMember();
            mM.MemberID = Convert.ToInt32(PaperLaidNotice.MemberId);
            mM = (mMember)Helper.ExecuteService("PaperLaid", "GetMemberByID", mM) as mMember;
            if (mM != null)
            {
                PaperLaidNotice.MemberName = mM.Name;
            }


            if (PaperLaidNotice.SubmittedDate != null)
            {
                PaperLaidNotice.SubmittedDateString = Convert.ToDateTime(PaperLaidNotice.SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");

            }
            if (PaperLaidNotice.IsAcknowledgmentDate != null)
            {
                PaperLaidNotice.AcknowledgmentDateString = Convert.ToDateTime(PaperLaidNotice.IsAcknowledgmentDate).ToString("dd/MM/yyyy hh:mm:ss:tt");

            }


            PaperLaidNotice.NoticeDiaryDate = PaperLaidNotice.NoticeDiaryDate;

            string aNoticeDate = Convert.ToDateTime(PaperLaidNotice.NoticeDate).ToString("dd/MM/yyyy");
            PaperLaidNotice.NoticeDiaryDate = aNoticeDate;

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            String FullFilePath = FileSettings.SettingValue + "/" + PaperLaidNotice.OriDiaryFilePath + PaperLaidNotice.OriDiaryFileName;
            PaperLaidNotice.FilePath = FullFilePath;



            //tMemberNotice NoticeTypeId = new tMemberNotice();
            //NoticeTypeId.NoticeTypeID = Convert.ToInt32(PaperLaidNotice.NoticeTypeID);
            string ntid = Convert.ToString(PaperLaidNotice.NoticeTypeID);
            ntid = (String)Helper.ExecuteService("PaperLaid", "GetNoticeTypeNamebyId", ntid);
            PaperLaidNotice.NoticeTypeName = Regex.Replace(ntid, @"<[^>]+>|&nbsp;", "").Trim();

            if (PaperLaidNotice.Subject != null)
            {
                PaperLaidNotice.Subject = Regex.Replace(PaperLaidNotice.Subject, @"<[^>]+>|&nbsp;", "").Trim();
            }
            if (PaperLaidNotice.Notice != null)
            {
                PaperLaidNotice.Notice = Regex.Replace(PaperLaidNotice.Notice, @"<[^>]+>|&nbsp;", "").Trim();

            }

            if (PaperLaidNotice.IsOnlineSubmitted == true)
            {
                OnlineNotices OnlineNotices = new OnlineNotices();
                OnlineNotices.ManualNoticeId = Convert.ToInt32(NoticeId);
                OnlineNotices = (OnlineNotices)Helper.ExecuteService("PaperLaid", "GetOnlineNoticesDetailsById", OnlineNotices);
                if (OnlineNotices.OriMemFileName != null)
                {
                    PaperLaidNotice.nFileNameSentByMember = OnlineNotices.OriMemFileName;
                    PaperLaidNotice.nFilePathSentByMember = OnlineNotices.OriMemFilePath;
                    FileSettings.SettingValue = FileSettings.SettingValue.Replace("SecureFileStructure", "");
                    String FullFilePathSentByMember = FileSettings.SettingValue + PaperLaidNotice.nFilePathSentByMember + PaperLaidNotice.nFileNameSentByMember;
                    PaperLaidNotice.OnlineNoticeByMemberPdf = FullFilePathSentByMember;
                }
                if (OnlineNotices.AttachedFileName != null)
                {
                    PaperLaidNotice.nFileAttNameSentByMember = OnlineNotices.AttachedFileName;
                    PaperLaidNotice.nFileAttPathSentByMember = OnlineNotices.AttachedFilePath;
                    String FullAttachPathSentByMember = FileSettings.SettingValue + "/" + PaperLaidNotice.nFileAttPathSentByMember + PaperLaidNotice.nFileAttNameSentByMember;
                    PaperLaidNotice.OnlineAttachByMemberPdf = FullAttachPathSentByMember;
                }
            }


            return PartialView("ViewNoticeDetailsById", PaperLaidNotice);
        }
        public JsonResult GetNoticeDiaryPdf(string NoticeId)
        {
            string result = "";
            tPaperLaidV PaperLaid = new tPaperLaidV();
            tPaperLaidV PaperLaidPartial = new tPaperLaidV();

            tMemberNotice PaperLaidNotice = new tMemberNotice();
            PaperLaidNotice.NoticeId = Convert.ToInt32(NoticeId);
            PaperLaidNotice = (tMemberNotice)Helper.ExecuteService("PaperLaid", "GetNoticeDetailsById", PaperLaidNotice);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            if (PaperLaidNotice.Notice == null)
            {
                PaperLaidNotice.Notice = PaperLaidNotice.Subject;
            }

            if (PaperLaidNotice.IsOnlineSubmitted == true)
            {
                OnlineNotices OnlineNotices = new OnlineNotices();
                OnlineNotices.ManualNoticeId = Convert.ToInt32(NoticeId);
                OnlineNotices = (OnlineNotices)Helper.ExecuteService("PaperLaid", "GetOnlineNoticesDetailsById", OnlineNotices);
                if (OnlineNotices.OriMemFileName != null)
                {
                    PaperLaidNotice.nFileNameSentByMember = OnlineNotices.OriMemFileName;
                    PaperLaidNotice.nFilePathSentByMember = OnlineNotices.OriMemFilePath;
                    //FileSettings.SettingValue = FileSettings.SettingValue.Replace("SecureFileStructure", "");   
                    result = FileSettings.SettingValue + PaperLaidNotice.nFilePathSentByMember + PaperLaidNotice.nFileNameSentByMember + "e+" + PaperLaidNotice.Notice;
                }
            }
            else
            {
                result = FileSettings.SettingValue + "/" + PaperLaidNotice.OriDiaryFilePath + PaperLaidNotice.OriDiaryFileName + "e+" + PaperLaidNotice.Notice;
            }

            var res2 = Json(result, JsonRequestBehavior.AllowGet);
            return res2;
        }
        public JsonResult GetSessionTime(string Sessiondate)
        {
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
            DataSet dataSetSession = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSessionDateInfo", methodParameter);
            string result = "";
            List<mSession> ListSession = new List<mSession>();
            result = Convert.ToString(dataSetSession.Tables[0].Rows[0]["SessionTimeF"]);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLOBIdBySessionDate(string Sessiondate)
        {
            if (Sessiondate == null)
            {
                Sessiondate = "23/08/2018";
            }
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLOBFromSessionDate", methodParameter);

            List<AddLOBModel> List = new List<AddLOBModel>();

            if (dataSetLOB.Tables[0].Rows.Count > 0)
            {
                List<DataRow> list = dataSetLOB.Tables[0].AsEnumerable().ToList();
                string LOBId = Convert.ToString(Convert.ToInt32(dataSetLOB.Tables[0].Rows[0][1]));
                return Json(LOBId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string LOBId = "0";
                return Json(LOBId, JsonRequestBehavior.AllowGet);
                // return List;
            }
        }
        public ActionResult ShowSummary()
        {

            tPaperLaidV PaperLaid = new tPaperLaidV();
            List<KeyValuePair<string, string>> mD = new List<KeyValuePair<string, string>>();
            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", mD);
            string CurrentAssembly = "";
            string PreviousSession = "";
            string Datefrom = "";
            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                {
                    CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }

                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "PreviousSession")
                {
                    PreviousSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }

            }

            List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
            mParameter.Add(new KeyValuePair<string, string>("@Assembly", CurrentAssembly));
            mParameter.Add(new KeyValuePair<string, string>("@session", PreviousSession));
            DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "LastSessionDates", mParameter);
            string Date = Convert.ToString(dataDate.Tables[0].Rows[0]["SessionDate"]);
            DateTime endDate = Convert.ToDateTime(Date);
            if (endDate == DateTime.Now)
            {
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;
                string LastSessionDate = StartDay;
                Datefrom = LastSessionDate;
            }
            else
            {
                endDate = endDate.AddDays(1);
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;
                string LastSessionDate = StartDay;
                Datefrom = LastSessionDate;
            }
            DateTime time = DateTime.Now;
            string Dateto = time.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); //= 09/06/2014
            PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
            PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);

            //For Total Notices Count
            tMemberNotice PaperLaidNoticeCount = new tMemberNotice();
            PaperLaidNoticeCount.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            PaperLaidNoticeCount.SessionID = Convert.ToInt32(CurrentSession.SessionId);
            PaperLaid.NoticeCount = (int)Helper.ExecuteService("PaperLaid", "GetNoticeCount", PaperLaidNoticeCount);
            PaperLaid.NoticeLaidCount = (int)Helper.ExecuteService("PaperLaid", "GetLaidNoticeCount", PaperLaidNoticeCount);
            PaperLaid.NoticePendingCount = (int)Helper.ExecuteService("PaperLaid", "GetPendingtoLayNoticeCount", PaperLaidNoticeCount);
            //For OtherPaper Count
            tPaperLaidV PaperLaidOtherpaperCount = new tPaperLaidV();
            PaperLaidOtherpaperCount.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            PaperLaidOtherpaperCount.SessionId = Convert.ToInt32(CurrentSession.SessionId);
            PaperLaidOtherpaperCount.DateFromis = PaperLaid.DateFromis;
            PaperLaidOtherpaperCount.DateTois = PaperLaid.DateTois;
            PaperLaid.OtherPapersCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetOtherpaperCount", PaperLaidOtherpaperCount);
            PaperLaid.OtherPapersLaidCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetLaidOtherPaperCount", PaperLaidOtherpaperCount);
            PaperLaid.OtherPapersPendingToLayCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetPendingtoLayOtherPaperCount", PaperLaidOtherpaperCount);

            //For Committeereports count
            PaperLaid.CommitteeRepCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetCRCount", PaperLaidOtherpaperCount);
            PaperLaid.CommitteeReportsLaidCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetLaidCRCount", PaperLaidOtherpaperCount);
            PaperLaid.CommitteeReportsPendingToLayCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetPendingToLayCRCount", PaperLaidOtherpaperCount);

            //For Bills Count
            PaperLaid.BillsCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "BillspaperCount", PaperLaidOtherpaperCount);
            PaperLaid.BillsLaidCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "BillsLaidCount", PaperLaidOtherpaperCount);
            PaperLaid.BillsPendingToLayCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "BillsPendingToLayCount", PaperLaidOtherpaperCount);

            return PartialView("ShowSummary", PaperLaid);
        }
        public JsonResult UpdatetPaperLaidVs(string Sessiondate)
        {
            if (Sessiondate == null)
            {
                Sessiondate = "23/08/2018";
            }
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", Sessiondate));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
            DataSet dataSetLOB = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectLOBFromSessionDate", methodParameter);

            List<AddLOBModel> List = new List<AddLOBModel>();
            string LOBId = "";
            //string SrNo = "";
            if (dataSetLOB.Tables[0].Rows.Count > 0)
            {
                //List<DataRow> list = dataSetLOB.Tables[0].AsEnumerable().ToList();
                LOBId = Convert.ToString(Convert.ToInt32(dataSetLOB.Tables[0].Rows[0][1]));
                for (int i = 0; i < dataSetLOB.Tables[0].Rows.Count; i++)
                {
                    String LOBRecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                    String PaperLaidId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PaperLaidId"]);
                    if (PaperLaidId.Length > 0)
                    {
                        //String LOBRecordId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["Id"]);
                        //String PaperLaidId = Convert.ToString(dataSetLOB.Tables[0].Rows[i]["PaperLaidId"]);
                        List<KeyValuePair<string, string>> param = new List<KeyValuePair<string, string>>();
                        param.Add(new KeyValuePair<string, string>("@LOBRecordId", LOBRecordId));
                        param.Add(new KeyValuePair<string, string>("@PaperLaidId", PaperLaidId));
                        DataSet getdataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Update_PaperLaidVS_OnFreeze", param);
                    }

                }

                return Json(LOBId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                LOBId = "NotSaved";
                return Json(LOBId, JsonRequestBehavior.AllowGet);
                // return List;
            }
        }
                
        [ValidateInput(false)]
        public ActionResult PartialViewPapers(string TextLOB)
        {
            tPaperLaidV PaperLaid = new tPaperLaidV();
            tPaperLaidV PaperLaidPartial = new tPaperLaidV();
            int MarkedPapersCount = 0;
            int UnMarkedPapersCount = 0;
            List<KeyValuePair<string, string>> mD = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", mD);
            string CurrentAssembly = "";
            string PreviousSession = "";
            string CurrentSessions = "";
            string Datefrom = "";
            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                {
                    CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }

                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "PreviousSession")
                {
                    PreviousSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                {
                    CurrentSessions = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }

            }

            PaperLaid.AssemblyId = Convert.ToInt32(CurrentAssembly);
            PaperLaid.SessionId = Convert.ToInt32(CurrentSessions);
            List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
            mParameter.Add(new KeyValuePair<string, string>("@Assembly", CurrentAssembly));
            mParameter.Add(new KeyValuePair<string, string>("@session", PreviousSession));
            DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "LastSessionDates", mParameter);
            string Date = Convert.ToString(dataDate.Tables[0].Rows[0]["SessionDate"]);
            DateTime endDate = Convert.ToDateTime(Date);
            if (endDate == DateTime.Now)
            {
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;
                string LastSessionDate = StartDay;
                Datefrom = LastSessionDate;
            }
            else
            {
                endDate = endDate.AddDays(1);
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;
                string LastSessionDate = StartDay;
                Datefrom = LastSessionDate;
            }

            PaperLaid.EventId = 4;      
            DateTime time = DateTime.Now;
            string Dateto = time.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); //= 09/06/2014
            PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
            PaperLaid.DateTois = DateTime.ParseExact(Dateto, "dd/MM/yyyy", null);

            
            // Get Count
            tMemberNotice PaperLaidNoticeCount = new tMemberNotice();
            PaperLaidNoticeCount.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            PaperLaidNoticeCount.SessionID = Convert.ToInt32(CurrentSession.SessionId);
            PaperLaid.NoticeCount = (int)Helper.ExecuteService("PaperLaid", "GetNoticeCount", PaperLaidNoticeCount);

            tPaperLaidV PaperLaidOtherpaperCount = new tPaperLaidV();
            PaperLaidOtherpaperCount.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            PaperLaidOtherpaperCount.SessionId = Convert.ToInt32(CurrentSession.SessionId);
            PaperLaidOtherpaperCount.DateFromis = PaperLaid.DateFromis;
            PaperLaidOtherpaperCount.DateTois = PaperLaid.DateTois;
            PaperLaid.OtherPapersCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetOtherpaperCount", PaperLaidOtherpaperCount);


            tPaperLaidV PaperLaidCRCount = new tPaperLaidV();
            PaperLaid.CommitteeRepCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetCRCount", PaperLaidOtherpaperCount);

            tPaperLaidV BillspaperCount = new tPaperLaidV();
            BillspaperCount.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            BillspaperCount.SessionId = Convert.ToInt32(CurrentSession.SessionId);
            PaperLaid.BillsCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "BillspaperCount", PaperLaidOtherpaperCount);

            PaperLaid.MarkedPapersCount = MarkedPapersCount;
            PaperLaid.UnMarkedPapersCount = UnMarkedPapersCount;

            //For Total Notices Count

            tMemberNotice PaperLaidNoticeCount1 = new tMemberNotice();
            PaperLaidNoticeCount1.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            PaperLaidNoticeCount1.SessionID = Convert.ToInt32(CurrentSession.SessionId);
            PaperLaid.NoticeCount = (int)Helper.ExecuteService("PaperLaid", "GetNoticeCount", PaperLaidNoticeCount1);
            PaperLaid.NoticeLaidCount = (int)Helper.ExecuteService("PaperLaid", "GetLaidNoticeCount", PaperLaidNoticeCount1);
            PaperLaid.NoticePendingCount = (int)Helper.ExecuteService("PaperLaid", "GetPendingtoLayNoticeCount", PaperLaidNoticeCount1);
            PaperLaid.NoticeDraftCount = (int)Helper.ExecuteService("PaperLaid", "GetForLayingNoticeCount", PaperLaidNoticeCount1);




            //For OtherPaper Count
            tPaperLaidV PaperLaidOtherpaperCount1 = new tPaperLaidV();
            PaperLaidOtherpaperCount1.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            PaperLaidOtherpaperCount1.SessionId = Convert.ToInt32(CurrentSession.SessionId);
            PaperLaidOtherpaperCount1.DateFromis = PaperLaid.DateFromis;
            PaperLaidOtherpaperCount1.DateTois = PaperLaid.DateTois;
            PaperLaid.OtherPapersCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetOtherpaperCount", PaperLaidOtherpaperCount1);
            PaperLaid.OtherPapersLaidCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetLaidOtherPaperCount", PaperLaidOtherpaperCount1);
            PaperLaid.OtherPapersPendingToLayCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetPendingtoLayOtherPaperCount", PaperLaidOtherpaperCount1);
            PaperLaid.OtherPapersForLayingCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetPapersForLayingCount", PaperLaidOtherpaperCount1);

            //For Committeereports count
            PaperLaid.CommitteeRepCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetCRCount", PaperLaidOtherpaperCount);
            PaperLaid.CommitteeReportsLaidCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetLaidCRCount", PaperLaidOtherpaperCount);
            PaperLaid.CommitteeReportsPendingToLayCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetPendingToLayCRCount", PaperLaidOtherpaperCount);
            PaperLaid.CommitteeReportsForLayingCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "GetForLayingCRCount", PaperLaidOtherpaperCount);

            //For Bills Count
            PaperLaid.BillsCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "BillspaperCount", PaperLaidOtherpaperCount);
            PaperLaid.BillsLaidCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "BillsLaidCount", PaperLaidOtherpaperCount);
            PaperLaid.BillsPendingToLayCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "BillsPendingToLayCount", PaperLaidOtherpaperCount);
            PaperLaid.BillsForLayingCount = (int)Helpers.Helper.ExecuteService("PaperLaid", "BillsForLayingCount", PaperLaidOtherpaperCount);

            return PartialView(PaperLaid);
        }

        public ActionResult GetPaperViewListBySelectedId(string Eventid, string Priority, string PaperId, string ConcernedDeptId, string Datefrom, string Dateto, string SessionDate)
        {
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

            System.Data.DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
            string CurrentAssembly = "";
            string Currentsession = "";
            string PreviousSession = "";
            int MarkedPapersCount = 0;
            int UnMarkedPapersCount = 0;
            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                {
                    CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "PreviousSession")
                {
                    PreviousSession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                {
                    Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
            }

            tPaperLaidV PaperLaid = new tPaperLaidV();
            PaperLaid.AssemblyId = Convert.ToInt32(CurrentAssembly);
            PaperLaid.SessionId = Convert.ToInt32(Currentsession);
            //For Last Session Date of Previous Session
            List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
            mParameter.Add(new KeyValuePair<string, string>("@Assembly", CurrentAssembly));
            mParameter.Add(new KeyValuePair<string, string>("@session", PreviousSession));
            DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "LastSessionDates", mParameter);
            string Date = Convert.ToString(dataDate.Tables[0].Rows[0]["SessionDate"]);
            DateTime endDate = Convert.ToDateTime(Date);
            if (endDate == DateTime.Now)
            {
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;
                string LastSessionDate = StartDay;
                Datefrom = LastSessionDate;
            }
            else
            {
                endDate = endDate.AddDays(1);
                string StartDay = (endDate).ToString("dd/MM/yyyy");
                string CurrentDay = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.Date = StartDay;
                ViewBag.dateto = CurrentDay;
                string LastSessionDate = StartDay;
                Datefrom = LastSessionDate;
            }
            if (Eventid != null || Eventid != "")
            {
                PaperLaid.EventId = Convert.ToInt32(Eventid);
            }

            PaperLaid.DateFromis = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
            DateTime time = DateTime.Now;
            string Datetill = time.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); //= 09/06/2014
            PaperLaid.DateTois = DateTime.ParseExact(Datetill, "dd/MM/yyyy", null);

            PaperLaid.SessionId = Convert.ToInt32(CurrentSession.SessionId);
            PaperLaid.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            int AssemblyId = Convert.ToInt32(CurrentAssembly);

            //For Notices
            if (PaperLaid.EventId == 4)
            {
                tPaperLaidV PaperLaidPartial = new tPaperLaidV();
                tMemberNotice PaperLaidNotice = new tMemberNotice();
                PaperLaidNotice.SessionID = Convert.ToInt32(CurrentSession.SessionId);
                PaperLaidNotice.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                if (PaperId == "Pending Papers")
                {
                    PaperLaidNotice.IsSubmitted = true;
                }
                else if (PaperId == "ForLaying")
                {
                    PaperLaidNotice.IsSubmitted = null;
                    PaperLaidNotice.IsLOBAttached = true;
                }
                else if (PaperId == "Paper Laid")
                {
                    PaperLaidNotice.IsSubmitted = false;
                }
               
                PaperLaidNotice = (tMemberNotice)Helper.ExecuteService("PaperLaid", "GetSentNotices", PaperLaidNotice);
                if (PaperLaidNotice.memberNoticeList != null)
                {
                    for (int i = 0; i < PaperLaidNotice.memberNoticeList.Count; i++)
                    {
                        string noticeno = PaperLaidNotice.memberNoticeList[i].NoticeNumber;
                        if (PaperLaidNotice.memberNoticeList[i].NoticeNumber != null)
                        {
                            string[] tokens = noticeno.Split('/');
                            if (tokens[2].Length == 1)
                            {
                                noticeno = tokens[0] + "/" + tokens[1] + "/" + "0" + tokens[2];
                                PaperLaidNotice.memberNoticeList[i].NoticeNumber = noticeno;
                            }
                        }
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);

                        if (PaperLaidNotice.memberNoticeList[i].IsOnlineSubmitted == true)
                        {
                            OnlineNotices OnlineNotices = new OnlineNotices();
                            OnlineNotices.ManualNoticeId = PaperLaidNotice.memberNoticeList[i].NoticeId;
                            OnlineNotices = (OnlineNotices)Helper.ExecuteService("PaperLaid", "GetOnlineNoticesDetailsById", OnlineNotices);
                            if (OnlineNotices.OriMemFileName != null)
                            {
                                PaperLaidNotice.nFileNameSentByMember = OnlineNotices.OriMemFileName;
                                PaperLaidNotice.nFilePathSentByMember = OnlineNotices.OriMemFilePath;
                                FileSettings.SettingValue = FileSettings.SettingValue.Replace("SecureFileStructure", "");
                                String FullFilePathSentByMember = FileSettings.SettingValue + PaperLaidNotice.nFilePathSentByMember + PaperLaidNotice.nFileNameSentByMember;
                                PaperLaidNotice.memberNoticeList[i].FilePath = FullFilePathSentByMember;
                            }
                        }
                        else
                        {
                            if (PaperLaidNotice.memberNoticeList[i].OriDiaryFileName != null)
                            {
                                String FullFilePath = FileSettings.SettingValue + "/" + PaperLaidNotice.memberNoticeList[i].OriDiaryFilePath + PaperLaidNotice.memberNoticeList[i].OriDiaryFileName;
                                PaperLaidNotice.memberNoticeList[i].FilePath = FullFilePath;
                            }
                        }

                        PaperLaidNotice.memberNoticeList[i].NoticeDiaryDate = PaperLaidNotice.memberNoticeList[i].NoticeDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); //= 09/06/2014

                        if (PaperLaidNotice.memberNoticeList[i].DepartmentId != null)
                        {
                            SBL.DomainModel.Models.Department.mDepartment deptInfo = new SBL.DomainModel.Models.Department.mDepartment();
                            deptInfo.deptId = PaperLaidNotice.memberNoticeList[i].DepartmentId;
                            deptInfo = (SBL.DomainModel.Models.Department.mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as SBL.DomainModel.Models.Department.mDepartment;
                            if (deptInfo != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].DepartmentName = deptInfo.deptname;
                            }
                        }

                        if (PaperLaidNotice.memberNoticeList[i].MemberId != null)
                        {
                            mMember mM = new mMember();
                            mM.MemberID = Convert.ToInt32(PaperLaidNotice.memberNoticeList[i].MemberId);
                            mM = (mMember)Helper.ExecuteService("PaperLaid", "GetMemberByID", mM) as mMember;
                            if (mM != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].MemberName = mM.Name;
                            }
                        }
                        //For Notice Rule type
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                        if (PaperLaidNotice.memberNoticeList[i].NoticeTypeID != null)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                        {
                            SBL.DomainModel.Models.Event.mEvent mM = new SBL.DomainModel.Models.Event.mEvent();
                            mM.EventId = PaperLaidNotice.memberNoticeList[i].NoticeTypeID;
                            mM = (SBL.DomainModel.Models.Event.mEvent)Helpers.Helper.ExecuteService("PaperLaid", "GetNoticeRuleByNoticeTypeID", mM);

                            if (mM != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].RuleNo = mM.RuleNo;
                                PaperLaidNotice.memberNoticeList[i].EventName = mM.EventName;
                            }
                        }

                        ////end
                        //For Marked/Unmarked Count

                        if (PaperLaidNotice.memberNoticeList[i].IsSecChecked == true)
                        {
                            MarkedPapersCount = MarkedPapersCount + 1;
                        }
                        else
                        {
                            UnMarkedPapersCount = UnMarkedPapersCount + 1;
                        }
                        //end


                        //tPaperLaidTemp paperLaidpartial = new tPaperLaidTemp();
                        //// paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                        //if (PaperLaidNotice.memberNoticeList[i].PaperLaidId != null)
                        //{
                        //    paperLaidpartial.PaperLaidTempId = (long)PaperLaidNotice.memberNoticeList[i].PaperLaidId;
                        //    paperLaidpartial = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", paperLaidpartial);
                        //}

                        tPaperLaidV paperLaidpartial = new tPaperLaidV();
                        tPaperLaidTemp pl = new tPaperLaidTemp();
                        if (PaperLaidNotice.memberNoticeList[i].PaperLaidId != null)
                        {
                            paperLaidpartial.PaperLaidId = (long)PaperLaidNotice.memberNoticeList[i].PaperLaidId;

                            paperLaidpartial = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidById", paperLaidpartial);
                            //PaperLaidNotice.memberNoticeList[i].IsLaid = paperLaidpartial.IsLaid;


                            tPaperLaidV elob = new tPaperLaidV();
                            elob.PaperLaidId = paperLaidpartial.PaperLaidId;
                            elob = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "CheckIsLOBAttached", elob);
                            if (elob.LOBRecordId > 0)
                            {
                                PaperLaidNotice.memberNoticeList[i].IsLOBAttached = true;
                            }


                            if (paperLaidpartial.DesireLayingDate != null)
                            {
                                DateTime DesireLayingDate = DateTime.ParseExact(paperLaidpartial.DesireLayingDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                PaperLaidNotice.memberNoticeList[i].DesireLayingDateString = DesireLayingDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            if (paperLaidpartial.ListtPaperLaidV[0].LOBRecordId != null)
                            {
                                DraftLOB Dlob = new DraftLOB();
                                Dlob.Id = Convert.ToInt32(paperLaidpartial.ListtPaperLaidV[0].LOBRecordId);
                                Dlob = (DraftLOB)Helpers.Helper.ExecuteService("PaperLaid", "GetLaidDateById", Dlob);
                                if (Dlob.SessionDateLocal == "e")
                                {
                                    //DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                    //DateTime LaidDate = DateTime.ParseExact(Dlob.SessionDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                    //PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                                }
                                else
                                {
                                    DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                    PaperLaidNotice.memberNoticeList[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                                }



                            }
                            pl.PaperLaidTempId = (long)paperLaidpartial.ListtPaperLaidV[0].DeptActivePaperId;
                            pl = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", pl);
                        }
                        if (paperLaidpartial != null)
                        {
                            if (PaperLaidNotice.memberNoticeList[i].SubmittedDate != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].SubmittedDateString = Convert.ToDateTime(PaperLaidNotice.memberNoticeList[i].SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                            }

                            if (PaperLaidNotice.memberNoticeList[i].PaperLaidId != null)
                            {
                                PaperLaidNotice.memberNoticeList[i].NDate = Convert.ToDateTime(pl.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                                //PaperLaidNotice.memberNoticeList[i].FilePath = paperLaidpartial.SignedFilePath;
                                var nFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                                String nFullFilePath = nFileSettings.SettingValue + "/" + pl.SignedFilePath;
                                PaperLaidNotice.memberNoticeList[i].NoticeReplyPath = nFullFilePath;
                            }
                        }
                    }
                }


                List<tMemberNotice> _listNoticepartial = new List<tMemberNotice>();
                if (PaperLaidNotice.memberNoticeList != null)
                {
                    foreach (var item in PaperLaidNotice.memberNoticeList)
                    {
                        _listNoticepartial.Add(item);
                        //_listNoticepartial.Sort();
                    }
                }
                var SortingList = _listNoticepartial.OrderBy(x => x.MemberName).ToList();
                PaperLaid.ListtPaperLaidVNoticeList = SortingList;
                ViewBag.Message = PaperLaidNotice.IsSubmitted;

                PaperLaid.MarkedPapersCount = MarkedPapersCount;
                PaperLaid.UnMarkedPapersCount = UnMarkedPapersCount;

            }
            //For Other Papers List
            else if (PaperLaid.EventId == 0)
            {
                tPaperLaidV PaperLaidPartial = new tPaperLaidV();

                if (PaperId == "Pending Papers")
                {
                    PaperLaid.IsLaid = false;
                    
                }
                else if (PaperId == "ForLaying")
                {
                    PaperLaid.IsLaid = false;
                    PaperLaid.IsLOBAttached = true;
                }
                else if (PaperId == "Paper Laid")
                {
                    PaperLaid.IsLaid = true;
                }

                PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByDeptId2", PaperLaid);
                if (PaperLaid.ListtPaperLaidV != null)
                {
                    for (int i = 0; i < PaperLaid.ListtPaperLaidV.Count; i++)
                    {
                        tPaperLaidTemp paperLaid = new tPaperLaidTemp();
                        // paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);                        
                        paperLaid.PaperLaidTempId = (long)PaperLaid.ListtPaperLaidV[i].DeptActivePaperId;
                        paperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", paperLaid);
                        if (paperLaid != null)
                        {
                            PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                            PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");

                            tPaperLaidV paperLaidpartial = new tPaperLaidV();
                            paperLaidpartial.PaperLaidId = (long)paperLaid.PaperLaidId;
                            paperLaidpartial = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidById", paperLaidpartial);
                            if (PaperLaid.ListtPaperLaidV[i].DesireLayingDate != null)
                            {
                                DateTime DesireLayingDate = DateTime.ParseExact(PaperLaid.ListtPaperLaidV[i].DesireLayingDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = DesireLayingDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            PaperLaid.ListtPaperLaidV[i].actualFilePath = paperLaid.SignedFilePath;
                            //For Marked/Unmarked Count

                            if (PaperLaid.ListtPaperLaidV[i].IsVSSecMarked == true)
                            {
                                MarkedPapersCount = MarkedPapersCount + 1;
                            }
                            else
                            {
                                UnMarkedPapersCount = UnMarkedPapersCount + 1;
                            }
                            //end

                        }

                        if (PaperLaid.ListtPaperLaidV[i].LOBRecordId != null)
                        {
                            DraftLOB Dlob = new DraftLOB();
                            Dlob.Id = Convert.ToInt32(PaperLaid.ListtPaperLaidV[i].LOBRecordId);
                            Dlob = (DraftLOB)Helpers.Helper.ExecuteService("PaperLaid", "GetLaidDateById", Dlob);
                            if (Dlob.SessionDateLocal == "e")
                            {
                                //DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                //DateTime LaidDate = DateTime.ParseExact(Dlob.SessionDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                //PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                            }
                            else
                            {
                                //DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                DateTime LaidDate = DateTime.ParseExact(Dlob.SessionDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                            }

                        }
                    }
                }

                List<tPaperLaidV> _list = new List<tPaperLaidV>();
                if (PaperLaid.ListtPaperLaidV != null)
                {
                    foreach (var item in PaperLaid.ListtPaperLaidV)
                    {
                        _list.Add(item);
                    }
                }
                PaperLaid.ListtPaperLaidVNotice = _list;
                if (PaperLaid.IsLOBAttached == true)
                {
                    ViewBag.Message = null;
                }
                else
                {
                    ViewBag.Message = PaperLaid.IsLaid;
                }
            

                PaperLaid.MarkedPapersCount = MarkedPapersCount;
                PaperLaid.UnMarkedPapersCount = UnMarkedPapersCount;
            }
            //For Bills list
            else if (PaperLaid.EventId == 5)
            {
                tPaperLaidV PaperLaidPartial = new tPaperLaidV();

                if (PaperId == "Pending Papers")
                {
                    PaperLaid.IsLaid = false;
                }
                else if (PaperId == "ForLaying")
                {
                    PaperLaid.IsLaid = false;
                    PaperLaid.IsLOBAttached = true;
                }
                else if (PaperId == "Paper Laid")
                {
                    PaperLaid.IsLaid = true;
                }

                PaperLaid = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidByDeptId2", PaperLaid);
                if (PaperLaid.ListtPaperLaidV != null)
                {
                    for (int i = 0; i < PaperLaid.ListtPaperLaidV.Count; i++)
                    {
                        tPaperLaidTemp paperLaid = new tPaperLaidTemp();
                        // paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                        if (PaperLaid.ListtPaperLaidV[i].DeptActivePaperId != null)
                        {
                            paperLaid.PaperLaidTempId = (long)PaperLaid.ListtPaperLaidV[i].DeptActivePaperId;
                            paperLaid = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", paperLaid);
                            if (paperLaid != null)
                            {
                                tPaperLaidV paperLaidpartial = new tPaperLaidV();
                                paperLaidpartial.PaperLaidId = (long)paperLaid.PaperLaidId;
                                paperLaidpartial = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidById", paperLaidpartial);

                                if (PaperLaid.ListtPaperLaidV[i].LOBRecordId != null)
                                {
                                    DraftLOB Dlob = new DraftLOB();
                                    Dlob.Id = Convert.ToInt32(PaperLaid.ListtPaperLaidV[i].LOBRecordId);
                                    Dlob = (DraftLOB)Helpers.Helper.ExecuteService("PaperLaid", "GetLaidDateById", Dlob);
                                    if (Dlob.SessionDateLocal == "e")
                                    {
                                        //DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                        //DateTime LaidDate = DateTime.ParseExact(Dlob.SessionDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                        //PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    }
                                    else
                                    {
                                        DateTime LaidDate = Convert.ToDateTime(Dlob.SessionDate);
                                        //  DateTime DesireLayingDate = DateTime.ParseExact(PaperLaid.ListtPaperLaidV[i].DesireLayingDate.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                        PaperLaid.ListtPaperLaidV[i].DesireLayingDateString = LaidDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    }
                                }

                                PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                                PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");

                                // PaperLaid.ListtPaperLaidV[i].MinisterSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss:tt");
                                // PaperLaid.ListtPaperLaidV[i].DeptSubmittedDate = Convert.ToDateTime(paperLaid.DeptSubmittedDate).Date;
                                PaperLaid.ListtPaperLaidV[i].actualFilePath = paperLaid.SignedFilePath;
                            }
                        }
                    }
                }

                List<tPaperLaidV> _nBilllist = new List<tPaperLaidV>();
                if (PaperLaid.ListtPaperLaidV != null)
                {
                    foreach (var item in PaperLaid.ListtPaperLaidV)
                    {
                        _nBilllist.Add(item);
                    }
                }
                var nSortList = _nBilllist.OrderBy(x => x.MemberName).ToList();
                PaperLaid.ListtPaperLaidVBills = nSortList;
                if (PaperLaid.IsLOBAttached == true)
                {
                    ViewBag.Message = null;
                }
                else
                {
                    ViewBag.Message = PaperLaid.IsLaid;
                }
                
            }
            //For Committee Reports List
            else if (PaperLaid.EventId == 8)
            {
                tPaperLaidV PaperLaidPartial = new tPaperLaidV();
                tCommitteeReport PaperLaidCR = new tCommitteeReport();
                if (PaperId == "Pending Papers")
                {
                    PaperLaidCR.IsFreeze = false;

                }
                else if (PaperId == "ForLaying")
                {
                    PaperLaidCR.IsFreeze = false;
                    PaperLaidCR.IsLOBAttached = true;
                }
                else if (PaperId == "Paper Laid")
                {
                    PaperLaidCR.IsFreeze = true;

                }
                PaperLaidCR.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                PaperLaidCR.SessionID = Convert.ToInt32(CurrentSession.SessionId);
                PaperLaidCR = (tCommitteeReport)Helpers.Helper.ExecuteService("PaperLaid", "GetCommitteeReports", PaperLaidCR);


                //DraftLOB GetCRDraftLOB = new DraftLOB();
                //GetCRDraftLOB.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
                //GetCRDraftLOB.SessionId = Convert.ToInt32(CurrentSession.SessionId);
                //GetCRDraftLOB = (DraftLOB)Helpers.Helper.ExecuteService("PaperLaid", "GetCRDraftLOB", PaperLaidCR);

                //for (int i = 0; i < GetCRDraftLOB.DraftLOBCRList.Count; i++)
                //{
                //    String LOBRecordId = Convert.ToString(GetCRDraftLOB.DraftLOBCRList[i].Id);
                //    List<KeyValuePair<string, string>> param1 = new List<KeyValuePair<string, string>>();
                //    param1.Add(new KeyValuePair<string, string>("@LOBRecordId", LOBRecordId));
                //    DataSet getdataSet1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetPaperIdbyRecordId", param1);
                //    if (getdataSet1.Tables[0].Rows.Count > 0)
                //    {

                //        string PaperLaidId = Convert.ToString(getdataSet1.Tables[0].Rows[0]["PaperLaidId"]);
                //        List<KeyValuePair<string, string>> param = new List<KeyValuePair<string, string>>();
                //        param.Add(new KeyValuePair<string, string>("@LOBRecordId", null));
                //        param.Add(new KeyValuePair<string, string>("@PaperLaidId", PaperLaidId));
                //        DataSet getdataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Update_PaperLaidVS_OnSave", param);


                //    }
                //}







                if (PaperLaidCR.committeereportList != null)
                {
                    for (int i = 0; i < PaperLaidCR.committeereportList.Count; i++)
                    {
                        if (PaperLaidCR.committeereportList[i].DeptmentID != null)
                        {
                            SBL.DomainModel.Models.Department.mDepartment deptInfo = new SBL.DomainModel.Models.Department.mDepartment();
                            deptInfo.deptId = PaperLaidCR.committeereportList[i].DeptmentID;
                            deptInfo = (SBL.DomainModel.Models.Department.mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as SBL.DomainModel.Models.Department.mDepartment;
                            if (deptInfo != null)
                            {
                                PaperLaidCR.committeereportList[i].DeptmentID = deptInfo.deptname;
                            }
                        }
                        if (PaperLaidCR.committeereportList[i].CommitteeId != null)
                        {
                            string comid = Convert.ToString(PaperLaidCR.committeereportList[i].CommitteeId);
                            List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
                            methodParameter1.Add(new KeyValuePair<string, string>("@CommitteeId", comid));
                            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectCommitteeInfoById", methodParameter1);
                            if (dataSet != null)
                            {
                                PaperLaidCR.committeereportList[i].CommitteeName = Convert.ToString(dataSet.Tables[0].Rows[0]["CommitteeName"]);
                            }
                        }

                        DateTime dt = DateTime.ParseExact(PaperLaidCR.committeereportList[i].DateOfLaying.ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                        //string s = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        PaperLaidCR.committeereportList[i].LayingDateString = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                        tPaperLaidV paperLaidpartial = new tPaperLaidV();
                        tPaperLaidTemp pl = new tPaperLaidTemp();
                        // paperLaid.DeptSubmittedDate = DateTime.ParseExact(Datefrom, "dd/MM/yyyy", null);
                        if (PaperLaidCR.committeereportList[i].PaperLaidId != null)
                        {
                            paperLaidpartial.PaperLaidId = (long)PaperLaidCR.committeereportList[i].PaperLaidId;
                            paperLaidpartial = (tPaperLaidV)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidById", paperLaidpartial);

                            pl.PaperLaidTempId = (long)paperLaidpartial.ListtPaperLaidV[0].DeptActivePaperId;
                            pl = (tPaperLaidTemp)Helpers.Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", pl);
                        }
                        if (paperLaidpartial != null)
                        {
                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                            string nPdfPath = PaperLaidCR.committeereportList[i].PDFPath.Replace("~", "");
                            String FullFilePath = FileSettings.SettingValue + "/" + nPdfPath;
                            PaperLaidCR.committeereportList[i].PDFPath = FullFilePath;
                        }
                    }
                }

                List<tCommitteeReport> _nCRlist = new List<tCommitteeReport>();
                if (PaperLaidCR.committeereportList != null)
                {
                    foreach (var item in PaperLaidCR.committeereportList)
                    {
                        _nCRlist.Add(item);
                    }
                }
                var nSortList = _nCRlist.OrderBy(x => x.CommitteeId).ToList();
                PaperLaid.ListtPaperLaidVCommRep = nSortList;
                if (PaperLaidCR.IsLOBAttached == true)
                {
                    ViewBag.Message = null;
                }
                else
                {
                    ViewBag.Message = PaperLaidCR.IsFreeze;
                }
                

            }
            // return PartialView("PartialShowPapers", PaperLaid);
            return PartialView("GetPaperViewListBySelectedId", PaperLaid);
        }
        [HttpPost]
        public JsonResult SaveResult(List<tMemberNotice> stulist)
        {

            String SessionDate = "";

            foreach (var cust in stulist)
            {

                tMemberNotice model = new tMemberNotice();
                model.NoticeNumber = cust.NoticeNumber;
                if (cust.DataStatus == "True")
                {
                    model.IsSecChecked = true;
                }
                else
                {
                    model.IsSecChecked = false;
                }

                model = (tMemberNotice)Helper.ExecuteService("PaperLaid", "UpdateNoticeIsSecCheck", model);

            }

            //return Json(stulist, JsonRequestBehavior.AllowGet);
            return Json(SessionDate, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveOtherPaperIsSecMark(List<tPaperLaidV> stulist)
        {

            String SessionDate = "";

            foreach (var cust in stulist)
            {

                tPaperLaidV model = new tPaperLaidV();
                model.PaperLaidId = cust.PaperLaidId;
                if (cust.IsVSSecMarked == true)
                {
                    model.IsVSSecMarked = true;
                }
                else
                {
                    model.IsVSSecMarked = false;
                }

                model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateOtherPaperIsVsSecMark", model);

            }

            //return Json(stulist, JsonRequestBehavior.AllowGet);
            return Json(SessionDate, JsonRequestBehavior.AllowGet);
        }

    }
}

