﻿using SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models;
using System;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Extensions;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.RecipientGroups;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.IO;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Passes;
using System.Linq;
using SBL.DomainModel.Models.SystemModule;
using System.Configuration;
using RazorEngine;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.Enums;
using SMS.API;
using System.Web.Script.Serialization;
using Email.API;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class RecipientGroupsController : Controller
    {
        #region Recipient Groups
        //
        // GET: /RecipientGroups/RecipientGroups/
        //public ActionResult Index(bool IsGenericGroup = false, bool IsPanchayatGroup = false, bool IsConstituencyGroup = false)
        //{
        //    RecipientGroupDashboardModel model = new RecipientGroupDashboardModel();
        //    RecipientGroup RGModel = new RecipientGroup();
        //    RGModel.CreatedBy = CurrentSession.UserName;
        //    var allContactGroups = Helper.ExecuteService("ContactGroups", "GetAllContactGroups", RGModel) as List<RecipientGroup>;

        //    if (IsGenericGroup)
        //    {
        //        var listObj = allContactGroups.Where(m => m.PancayatCode == null).Where(m => m.ConstituencyCode == null).Where(m => m.IsPublic == true).ToList();
        //        model.RecipientGroupModelList.AddRange(listObj.ToModelList());
        //        model.IsGenericGroup = true;
        //    }
        //    else if (IsPanchayatGroup)
        //    {
        //        var listObj = allContactGroups.Where(m => m.PancayatCode != null).ToList();
        //        model.RecipientGroupModelList.AddRange(listObj.ToModelList());
        //        model.IsPanchayatGroup = true;
        //    }
        //    else if (IsConstituencyGroup)
        //    {
        //        var listObj = allContactGroups.Where(m => m.ConstituencyCode != null).ToList();
        //        model.RecipientGroupModelList.AddRange(listObj.ToModelList());
        //        model.IsConstituencyGroup = true;
        //    }
        //    else
        //    {                
        //        model.RecipientGroupModelList.AddRange(allContactGroups.ToModelList());
        //    }
        //    return View(model);
        //}

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ContactGroupList(bool IsGenericGroup = false, bool IsPanchayatGroup = false, bool IsConstituencyGroup = false)
        {
            RecipientGroupDashboardModel model = new RecipientGroupDashboardModel();
            RecipientGroup RGModel = new RecipientGroup();
            RGModel.CreatedBy = CurrentSession.UserName;
            var allContactGroups = Helper.ExecuteService("ContactGroups", "GetAllContactGroups", RGModel) as List<RecipientGroup>;

            if (IsGenericGroup)
            {
                var listObj = allContactGroups.Where(m => m.PancayatCode == null).Where(m => m.ConstituencyCode == null).Where(m => m.IsPublic == true).ToList();
                model.RecipientGroupModelList.AddRange(listObj.ToModelList());
                model.IsGenericGroup = true;
            }
            else if (IsPanchayatGroup)
            {
                var listObj = allContactGroups.Where(m => m.PancayatCode != null).ToList();
                model.RecipientGroupModelList.AddRange(listObj.ToModelList());
                model.IsPanchayatGroup = true;
            }
            else if (IsConstituencyGroup)
            {
                var listObj = allContactGroups.Where(m => m.ConstituencyCode != null).ToList();
                model.RecipientGroupModelList.AddRange(listObj.ToModelList());
                model.IsConstituencyGroup = true;
            }
            else
            {
                model.RecipientGroupModelList.AddRange(allContactGroups.ToModelList());
            }
            return View("_ContactGroupList", model);
        }

        [HttpGet]
        public ActionResult NewRecipientGroup(bool IsGenericGroup = false, bool IsPanchayatGroup = false, bool IsConstituencyGroup = false)
        {

            #region Get the associated Departments for the User.
            RecipientGroup RGModel = new RecipientGroup();
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                RGModel.UserID = new Guid(CurrentSession.UserID);
            }

            //List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            //AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("ContactGroups", "GetAuthorizedEmployees", RGModel);

            //string DepartmentIDs = string.Empty;

            //if (AuthorisedEmp != null && AuthorisedEmp.Count != 0)
            //{
            //    foreach (var item in AuthorisedEmp)
            //    {
            //        DepartmentIDs = item.AssociatedDepts;
            //    }
            //}
            //else
            //{ 
            //    if (DepartmentIDs == string.Empty)
            //    {
            //        DepartmentIDs = CurrentSession.DeptID;
            //    }
            //}

            mDepartmentPasses department = new mDepartmentPasses();
            //department.DepartmentID = DepartmentIDs;

            var mDepartments = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", department);

            #endregion

            var panchayatList = Helper.ExecuteService("Constituency", "GetAllPanchayats", null) as List<mPanchayat>;
            var constituencyList = Helper.ExecuteService("Constituency", "GetAllConstituency", null) as List<mConstituency>;

            RecipientGroupModel model = new RecipientGroupModel()
            {
                Mode = "Add",
                IsActive = true,
                IsPublic = false,
                mDepartmentList = mDepartments,
                mPanchayatList = panchayatList,
                mConstituencyList = constituencyList,
                IsGenericGroup = IsGenericGroup,
                IsPanchayatGroup = IsPanchayatGroup,
                IsConstituencyGroup = IsConstituencyGroup
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult NewRecipientGroup(RecipientGroupModel model)
        {
            //Save the Contact Group TO Database.
            if (ModelState.IsValid)
            {
                if (model.GroupID == 0)
                {
                    model.CreatedBy = CurrentSession.UserName;
                    model.CreatedDate = DateTime.Now;
                    var modelToPass = model.ToEntity();

                    if (model.IsGenericGroup) { modelToPass.GroupTypeID = (int)ContactGroupTypeEnum.Generic; }
                    else if (model.IsPanchayatGroup) { modelToPass.GroupTypeID = (int)ContactGroupTypeEnum.panchayat; }
                    else if (model.IsConstituencyGroup) { modelToPass.GroupTypeID = (int)ContactGroupTypeEnum.Constituency; }

                    var savedRecord = Helper.ExecuteService("ContactGroups", "CreateNewContactGroup", modelToPass);
                }
                else
                {
                    var modelToPass = model.ToEntity();
                    var savedRecord = Helper.ExecuteService("ContactGroups", "UpdateContactGroup", modelToPass);
                }
            }

            if (model.SaveCreateNew)
            {
                return RedirectToAction("NewRecipientGroup");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult DeleteContactGroup(Int32 GroupID)
        {
            Helper.ExecuteService("ContactGroups", "DeleteContactGroup", new RecipientGroup { GroupID = GroupID });
            return Json(true, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public ActionResult EditContactGroup(Int32 GroupID)
        {
            var contactGroup = Helper.ExecuteService("ContactGroups", "GetContactGroupByID", new RecipientGroup { GroupID = GroupID }) as RecipientGroup;
            var contactGroupModel = contactGroup.ToModel();

            #region Get the associated Departments for the User.
            RecipientGroup RGModel = new RecipientGroup();
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                RGModel.UserID = new Guid(CurrentSession.UserID);
            }

            //List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            //AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("ContactGroups", "GetAuthorizedEmployees", RGModel);

            //string DepartmentIDs = string.Empty;

            //if (AuthorisedEmp != null && AuthorisedEmp.Count != 0)
            //{
            //    foreach (var item in AuthorisedEmp)
            //    {
            //        DepartmentIDs = item.AssociatedDepts;
            //    }
            //}
            //else
            //{
            //    if (DepartmentIDs == string.Empty)
            //    {
            //        DepartmentIDs = CurrentSession.DeptID;
            //    }
            //}

            mDepartmentPasses department = new mDepartmentPasses();
            department.DepartmentID = contactGroupModel.DepartmentCode;
            var mDepartments = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", department);

            #endregion

            contactGroupModel.mDepartmentList = mDepartments;

            if (contactGroup.GroupTypeID == (int)ContactGroupTypeEnum.Constituency)
            {
                contactGroupModel.IsConstituencyGroup = true;
                var constituencyList = Helper.ExecuteService("Constituency", "GetAllConstituency", null) as List<mConstituency>;
                contactGroupModel.mConstituencyList = constituencyList;
            }
            else if (contactGroup.GroupTypeID == (int)ContactGroupTypeEnum.Generic)
            {
                contactGroupModel.IsGenericGroup = true;
            }
            else if (contactGroup.GroupTypeID == (int)ContactGroupTypeEnum.panchayat)
            {
                contactGroupModel.IsPanchayatGroup = true;
                var panchayatList = Helper.ExecuteService("Constituency", "GetAllPanchayats", null) as List<mPanchayat>;
                contactGroupModel.mPanchayatList = panchayatList;
            }

            contactGroupModel.Mode = "Update";
            return View("NewRecipientGroup", contactGroupModel);
        }
        #endregion

        #region Recipient Group Members

        [HttpGet]
        public ActionResult RecipientGroupMember(bool IsGenericGroup = false, bool IsPanchayatGroup = false, bool IsConstituencyGroup = false)
        {
            RecipientGroup RGModel = new RecipientGroup();
            RGModel.CreatedBy = CurrentSession.UserName;

            RecipientGroupMemDashboardModel model = new RecipientGroupMemDashboardModel();
            var contactGroupsAll = Helper.ExecuteService("ContactGroups", "GetContactGroupsByCretedBy", RGModel) as List<RecipientGroup>;

            if (IsGenericGroup || IsPanchayatGroup || IsConstituencyGroup)
            {
                if (IsGenericGroup) { RGModel.GroupTypeID = (int)ContactGroupTypeEnum.Generic; }
                else if (IsPanchayatGroup) { RGModel.GroupTypeID = (int)ContactGroupTypeEnum.panchayat; }
                else if (IsConstituencyGroup) { RGModel.GroupTypeID = (int)ContactGroupTypeEnum.Constituency; }

                model.GroupList = contactGroupsAll.Where(m => m.GroupTypeID == RGModel.GroupTypeID).ToList();
                //Helper.ExecuteService("ContactGroups", "GetContactGroupsByCretedByType", RGModel) as List<RecipientGroup>
            }
            else
            {
                model.GroupList = contactGroupsAll;
                //Helper.ExecuteService("ContactGroups", "GetContactGroupsByCretedBy", RGModel) as List<RecipientGroup>;
            }
            model.GroupListAll = contactGroupsAll;

            return View(model);
        }

        [HttpGet]
        public ActionResult NewRecipientGroupMember(Int32 groupID)
        {
            RecipientGroup groupDetails = new RecipientGroup();
            if (groupID != 0)
            {
                groupDetails = Helper.ExecuteService("ContactGroups", "GetContactGroupByID", new RecipientGroup { GroupID = groupID }) as RecipientGroup;
            }
            RecipientGroupMemberModel model = new RecipientGroupMemberModel();

            #region Get the associated Departments for the User.
            RecipientGroup RGModel = new RecipientGroup();
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                RGModel.UserID = new Guid(CurrentSession.UserID);
            }
            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("ContactGroups", "GetAuthorizedEmployees", RGModel);

            string DepartmentIDs = string.Empty;

            if (AuthorisedEmp != null && AuthorisedEmp.Count != 0)
            {
                foreach (var item in AuthorisedEmp) { DepartmentIDs = item.AssociatedDepts; }
            }
            else
            {
                if (DepartmentIDs == string.Empty) { DepartmentIDs = CurrentSession.DeptID; }
            }

            mDepartmentPasses department = new mDepartmentPasses();
            department.DepartmentID = DepartmentIDs;
            var mDepartments = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Passes", "GetDepartmentByids", department);

            #endregion

            if (groupID != 0 && groupDetails != null)
            {
                model = new RecipientGroupMemberModel()
                {
                    Mode = "Add",
                    GenderList = ExtensionMethods.GetGenderList(),
                    IsActive = true,
                    GroupID = groupID,
                    GroupName = groupDetails.Name,
                    DepartmentCode = CurrentSession.DeptID,
                    mDepartmentList = mDepartments
                };
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult NewRecipientGroupMember(RecipientGroupMemberModel model)
        {
            //Save the Contact Group Member To Database.
            if (ModelState.IsValid)
            {
                if (model.GroupMemberID == 0)
                {
                    model.CreatedDate = DateTime.Now;
                    model.CreatedBy = CurrentSession.UserName;
                    var modelToPass = model.ToEntity();
                    var savedRecord = Helper.ExecuteService("ContactGroups", "CreateNewContactGroupMember", modelToPass);
                }
                else
                {
                    var modelToPass = model.ToEntity();
                    var savedRecord = Helper.ExecuteService("ContactGroups", "UpdateContactGroupMember", modelToPass);
                }
            }

            if (model.SaveCreateNew)
            {
                //return RedirectToAction("NewRecipientGroupMember");
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public ActionResult EditContactGroupMember(Int32 GroupMemberID)
        {
            var contactGroupMember = Helper.ExecuteService("ContactGroups", "GetContactGroupMemberByID", new RecipientGroupMember { GroupMemberID = GroupMemberID }) as RecipientGroupMember;
            var contactGroupMemberModel = contactGroupMember.ToModel();
            contactGroupMemberModel.Mode = "Update";

            RecipientGroup groupDetails = new RecipientGroup();
            if (contactGroupMemberModel.GroupID != 0)
            {
                groupDetails = Helper.ExecuteService("ContactGroups", "GetContactGroupByID", new RecipientGroup { GroupID = contactGroupMemberModel.GroupID }) as RecipientGroup;
            }
            contactGroupMemberModel.GroupName = groupDetails.Name;

            return View("NewRecipientGroupMember", contactGroupMemberModel);
        }

        [HttpGet]
        public ActionResult DeleteContactGroupMember(Int32 GroupMemberID)
        {
            Helper.ExecuteService("ContactGroups", "DeleteContactGroupMember", new RecipientGroupMember { GroupMemberID = GroupMemberID });
            return RedirectToAction("RecipientGroupMember");
        }

        [HttpGet]
        public ActionResult ImportExcel(Int32 GroupID)
        {
            RecipientGroup groupDetails = new RecipientGroup();
            if (GroupID != 0)
            {
                groupDetails = Helper.ExecuteService("ContactGroups", "GetContactGroupByID", new RecipientGroup { GroupID = GroupID }) as RecipientGroup;
            }

            if (GroupID != 0 && groupDetails != null)
            {
                RecipientGroupDashboardModel model = new RecipientGroupDashboardModel()
                {
                    GroupID = GroupID,
                    GroupName = groupDetails.Name
                };

                return View("_ExportFromExcelPartial", model);
            }
            return RedirectToAction("RecipientGroupMember");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ImportExcel(RecipientGroupDashboardModel model, HttpPostedFileBase file)
        {
            if (file.ContentLength > 0)
            {
                string extension = System.IO.Path.GetExtension(file.FileName);

                //Check the directory for the excel files and Create if not already present. 
                string excelFileURL = "/Content/Upload/ExcelFiles";
                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(excelFileURL));
                if (!Dir.Exists)
                {
                    Dir.Create();
                }

                string path1 = string.Format("{0}/{1}", Server.MapPath(excelFileURL), file.FileName);
                if (System.IO.File.Exists(path1))
                    System.IO.File.Delete(path1);


                file.SaveAs(path1);

                //Create connection string to Excel work book
                string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=Excel 12.0;Persist Security Info=False";

                DataSet contactDS = new DataSet();
                using (OleDbConnection conn = new OleDbConnection(excelConnectionString))
                {
                    conn.Open();
                    //Get All Sheets Name
                    DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" });

                    //Get the First Sheet Name
                    string firstSheetName = sheetsName.Rows[0][2].ToString();

                    //Query String 
                    string sql = string.Format("SELECT * FROM [{0}]", firstSheetName);
                    OleDbDataAdapter adaptor = new OleDbDataAdapter(sql, excelConnectionString);
                    adaptor.Fill(contactDS);
                }

                List<RecipientGroupMemberModel> GroupMemberList = new List<RecipientGroupMemberModel>();

                GroupMemberList = contactDS.Tables[0].DataTableToList<RecipientGroupMemberModel>();

                Helper.ExecuteService("ContactGroups", "CreateMultipleContactGroupMember", GroupMemberList.ToEntityList(model.GroupID));
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult GetContactsByGroupID(Int32 GroupID, bool readOnly)
        {
            var model = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", new RecipientGroupMember { GroupID = GroupID }) as List<RecipientGroupMember>;

            if (readOnly)
            {
                return View("_GroupMemberList", model);
            }
            else
            {
                return View("_AddtoReceipentGroup", model);
            }
        }

        [HttpGet]
        public ActionResult GetExistingContactsToAdd(Int32 GroupID, bool readOnly)
        {
            var model = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", new RecipientGroupMember { GroupID = GroupID }) as List<RecipientGroupMember>;
            return View("_GetExistingContactsToAdd", model);
        }
        #endregion

        #region Send Notification Example
        [HttpGet]
        public ActionResult GetNotificationsForGridMain()
        {
            return View("_GetNotificationsForGridMain");
        }

        public ActionResult GetNotificationForGridDetail(Int32 tourID)
        {
            return View("_NotificationForGridDetail");
        }

        public ActionResult NotificationStatusDetails()
        {
            //Here we can use the List of the [SMSMessageList] class.
            var notificationDetails = Notification.GetNotificationStatus(1, 1);
            return View("_NotificationStatusDetails", notificationDetails);
        }

        [HttpGet]
        public ActionResult SendNotifications()
        {
            string moduleName = ConfigurationManager.AppSettings["moduleName"];
            string moduleActionName = ConfigurationManager.AppSettings["moduleActionName"];

            SendNotificationsModel model = new SendNotificationsModel();

            List<RecipientGroup> contactGroups = new List<RecipientGroup>();
            //Get the Record from the SystemModules on the basis of the moduleName.
            SystemModuleModel systemMoule = new SystemModuleModel();
            systemMoule.Name = moduleName;
            var moduleObj = Helper.ExecuteService("SystemModule", "GetSystemModuleByName", systemMoule) as SystemModuleModel;

            SystemModuleModel actionObj = new SystemModuleModel();

            if (moduleObj != null)
            {
                //Get the record from the SystemFunctions on the basis of moduleName and actionName.
                SystemModuleModel moduleAction = new SystemModuleModel();
                moduleAction.ActionName = moduleActionName;
                actionObj = Helper.ExecuteService("SystemModule", "GetModuleActionByName", moduleAction) as SystemModuleModel;
            }

            if (actionObj != null)
            {
                #region Get All The Contacts Associated to the action.
                if (!string.IsNullOrEmpty(actionObj.AssociatedContactGroups))
                {
                    //pass the Comma seperated Associated Contact Groups to a Function and get all the group details at a time.
                    contactGroups = Helper.ExecuteService("ContactGroups", "GetContactGroupsByGroupIDs", new RecipientGroupMember { AssociatedContactGroups = actionObj.AssociatedContactGroups }) as List<RecipientGroup>;
                }
                #endregion
            }

            var modelSMS = new { MemberName = "Sujeet Kumar", TourLocation = "Solan" };

            string SMSTemplateText = actionObj.SMSTemplate;
            string emailTemplateText = actionObj.EmailTemplate;

            try
            {
                model.SMSTemplate = Razor.Parse(SMSTemplateText, modelSMS);
                model.availableRecipientGroups = contactGroups != null ? contactGroups : new List<RecipientGroup>();
                model.selectedRecipientGroups = contactGroups != null ? contactGroups : new List<RecipientGroup>();
                model.SendSMS = actionObj.SendSMS;
                model.SendEmail = actionObj.SendEmail;
            }
            catch (Exception)
            {

                throw;
            }

            model.RemainderTypesList = ExtensionMethods.GetRemainderTypes();

            return View("_SendNotifications", model);
        }

        [HttpPost]
        public ActionResult SendNotifications(SendNotificationsModel model)
        {
            List<string> emailAddresses = new List<string>();
            List<string> phoneNumbers = new List<string>();

            if (!string.IsNullOrEmpty(model.EmailAddresses))
                emailAddresses = model.EmailAddresses.Split(',').Select(s => s).ToList();

            if (!string.IsNullOrEmpty(model.PhoneNumbers))
                phoneNumbers = model.PhoneNumbers.Split(',').Select(s => s).ToList();
            //List<int> groupIds = new List<int>();

            //string[] values = multipleIDs.AssociatedContactGroups.Split(',');
            //for (int i = 0; i < values.Length; i++)
            //{
            //    groupIds.Add(int.Parse(values[i]));
            //}

            SMSMessageList smsMessage = new SMSMessageList();
            CustomMailMessage emailMessage = new CustomMailMessage();

            smsMessage.SMSText = model.SMSTemplate;
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = 1;

            foreach (var item in phoneNumbers)
            {
                smsMessage.MobileNo.Add(item);
            }

            Notification.Send(model.SendSMS, model.SendEmail, smsMessage, emailMessage);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult GetAllAddedContacts()
        {
            return View("_EmployeTable");
        }

        [HttpGet]
        public ActionResult GetAllContactsForUser()
        {
            //Get all the Comma seperated Contact Group IDs and Pass to this method.
            RecipientGroup RGModel = new RecipientGroup();
            RGModel.CreatedBy = CurrentSession.UserName;
            var allContactGroups = Helper.ExecuteService("ContactGroups", "GetAllContactGroups", RGModel) as List<RecipientGroup>;

            List<int> myListOfInt = new List<int>();
            foreach (var item in allContactGroups)
            {
                myListOfInt.Add(item.GroupID);
            }

            string contactGroupIDs = string.Empty;
            contactGroupIDs = string.Join<int>(", ", myListOfInt);

            var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactMembersByGroupID", new RecipientGroupMember { AssociatedContactGroups = contactGroupIDs }) as List<RecipientGroupMember>;
            //return View("_MemberTable");
            return View("_GetExistingContactsToAdd", contactGroupMembers);

        }

        [HttpGet]
        public ActionResult GetAllContactsByGroupID(Int32 groupID)
        {
            var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", new RecipientGroupMember { GroupID = groupID }) as List<RecipientGroupMember>;
            return Json(contactGroupMembers, JsonRequestBehavior.AllowGet);

        }
        #endregion
    }
}
