﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.CommitteeReport;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.eFile;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class CommitteeItemMasterController : Controller
    {
        //
        // GET: /Admin/CommitteeItemMaster/

        public ActionResult Index()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserName))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            ViewBag.loginType = "VidhanSabha";//for admin to show edit delete logic
            ViewBag.loginTypeadmin = "admin";
            tCommitteReplyPendency model = new tCommitteReplyPendency();

            model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            model.tCommitteeList = (List<tCommittee>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteList", null);
            model.mCommitteeReplyItemTypeList = (List<mCommitteeReplyItemType>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyItemType", null);
            model.mCommitteeReplyMasterList = (List<tCommitteReplyByMaster>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyByMasterRecord", null);

            //new requirement to show selected committee only..
            string[] str = new string[3];
            str[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);
            model.CommitteeId = Convert.ToInt16(value[0]);
            ViewBag.CommitteeId = value[0];
            ViewBag.CommitteeName = value[1];

            model.eFilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);
            model.eFilePaperNatureList = (List<eFilePaperNature>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperNature", null);
            model.mCommitteeReplyStatusList = (List<mCommitteeReplyStatus>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteReplyStatus", null);
            // return PartialView("_MainDashBoard", model);
            return View(model);
        }

        public ActionResult _MainDashBoard()
        {
            ViewBag.loginType = "VidhanSabha";//for admin to show edit delete logic
            ViewBag.loginTypeadmin = "admin";
            tCommitteReplyPendency model = new tCommitteReplyPendency();


            model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            model.tCommitteeList = (List<tCommittee>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteList", null);
            model.mCommitteeReplyItemTypeList = (List<mCommitteeReplyItemType>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyItemType", null);
            model.mCommitteeReplyMasterList = (List<tCommitteReplyByMaster>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyByMasterRecord", null);

            //new requirement to show selected committee only..
            string[] str = new string[3];
            str[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);
            model.CommitteeId = Convert.ToInt16(value[0]);
            ViewBag.CommitteeId = value[0];
            ViewBag.CommitteeName = value[1];

            model.eFilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);
            model.eFilePaperNatureList = (List<eFilePaperNature>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperNature", null);
            model.mCommitteeReplyStatusList = (List<mCommitteeReplyStatus>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteReplyStatus", null);
            return PartialView("_MainDashBoard", model);
        }

        [HttpPost]
        public ActionResult GetFilteredPendencyReplyList(tCommitteReplyPendency model)
        {
            try
            {
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                ViewBag.FileLocation = FileSettings.SettingValue;
                if (model.ItemTypeId == 1)
                {
                    ViewBag.itemtype = "Audit Parah(Finance)";
                }
                else if (model.ItemTypeId == 2)
                {
                    ViewBag.itemtype = "Audit Parah(Revenue)";
                }
                else if (model.ItemTypeId == 3)
                {
                    ViewBag.itemtype = "Audit Parah(Civil)";
                }
                else if (model.ItemTypeId == 4)
                {
                    ViewBag.itemtype = "Report(Original)";
                }
                else if (model.ItemTypeId == 5)
                {
                    ViewBag.itemtype = "Report(Action Taken)";
                }
                else if (model.ItemTypeId == 6)
                {
                    ViewBag.itemtype = "Report(Further Action Taken)";
                }
                else if (model.ItemTypeId == 7)
                {
                    ViewBag.itemtype = "Assurances";
                }
                else if (model.ItemTypeId == 8)
                {
                    ViewBag.itemtype = "Title";
                }
                else if (model.ItemTypeId == 9)
                {
                    ViewBag.itemtype = "Other";
                }

                if (model.ItemTypeId == 4 || model.ItemTypeId == 5 || model.ItemTypeId == 6)
                {
                    ViewBag.pendencyby = "Report";
                    var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemType", model);
                    return PartialView("_PendencyReplyList", modelList);
                }
                else
                {
                    var modelList = (List<tCommitteReplyPendency>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyPendencyForItemType", model);

                    return PartialView("_PendencyReplyList", modelList);
                }

            }
            catch (Exception)
            {
                return RedirectToAction("SessionTimedOut", "Error", new { @area = "" });
            }
        }

        public ActionResult DeleteNewCommiteeReplyPendencyDetail(int id)
        {
            try
            {
                ViewBag.pendencyid = id;
                Helper.ExecuteService("CommitteReplyPendency", "DeletePendencyNewEntryByPendencyId", id);
                return PartialView("_PendencyReplyList");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult NewCommiteeReplyEntryForm()
        {
            tCommitteReplyPendency model = new tCommitteReplyPendency();
            model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            //mDepartment mdep = new mDepartment();
            //mdep.deptId = CurrentSession.DeptID;
            //model.mDepartmentList = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);

            model.tCommitteeList = (List<tCommittee>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteList", null);
            model.mCommitteeReplyItemTypeList = (List<mCommitteeReplyItemType>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyItemType", null);
            model.mCommitteeReplyMasterList = (List<tCommitteReplyByMaster>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyByMasterRecord", null);

            //new requirement to show selected committee only..
            string[] str = new string[3];
            str[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);
            model.CommitteeId = Convert.ToInt16(value[0]);
            ViewBag.CommitteeId = value[0];
            ViewBag.CommitteeName = value[1];

            model.eFilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);
            model.eFilePaperNatureList = (List<eFilePaperNature>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperNature", null);
            model.mCommitteeReplyStatusList = (List<mCommitteeReplyStatus>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteReplyStatus", null);

            var BranchId = CurrentSession.BranchId;
            ViewBag.efileno = (List<fillListGenricInt>)Helper.ExecuteService("CommitteReplyPendency", "eFileWithdepartment", BranchId);

            var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            //assmeblyList.Add(new{te})
            ViewBag.assmeblyList = assmeblyList;
            //Code for SessionList 
            SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
            data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
            var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
            ViewBag.sessionList = sessionList;

            mSession mSession = new mSession();
            mSession.SessionCode = sessionList.FirstOrDefault().SessionCode;
            mSession.AssemblyID = data.AssemblyID;
            //ViewBag.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            // model.mode = "add"; 
            return PartialView("NewCommiteeReplyEntryForm", model);
        }

        public ActionResult EditCommiteeReplyEntryForm(int id)
        {
            tCommitteReplyPendency model = new tCommitteReplyPendency();
            model = (tCommitteReplyPendency)Helper.ExecuteService("CommitteReplyPendency", "CommitteReplyPendencyForEdit", id);
            model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            //mDepartment mdep = new mDepartment();
            //mdep.deptId = CurrentSession.DeptID;
            //model.mDepartmentList = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);

            model.tCommitteeList = (List<tCommittee>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteList", null);
            model.mCommitteeReplyItemTypeList = (List<mCommitteeReplyItemType>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyItemType", null);
            model.mCommitteeReplyMasterList = (List<tCommitteReplyByMaster>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyByMasterRecord", null);

            //new requirement to show selected committee only..
            string[] str = new string[3];
            str[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);
            model.CommitteeId = Convert.ToInt16(value[0]);
            ViewBag.CommitteeId = value[0];
            ViewBag.CommitteeName = value[1];

            model.CheckValue = model.TypeNo;//to check duplicate record
            model.CheckItemTypeId = model.ItemTypeId;//to check duplicate record
            model.CheckValueDept = model.DepartmentId;//to check duplicate record
            model.CheckValueSessionDate = model.SessionDate;//to check duplicate record
            model.CheckValueAssemblyId = Convert.ToString(model.AssemblyID);//to check duplicate record
            if (model.SessionID != null)
            {
                model.CheckValueSessionId = Convert.ToString(model.SessionID);//to check duplicate record
            }
            model.CheckFinancialYear = model.FinancialYear;
            model.eFilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);
            model.eFilePaperNatureList = (List<eFilePaperNature>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperNature", null);
            model.mCommitteeReplyStatusList = (List<mCommitteeReplyStatus>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteReplyStatus", null);

            var BranchId = CurrentSession.BranchId;
            ViewBag.efileno = (List<fillListGenricInt>)Helper.ExecuteService("CommitteReplyPendency", "eFileWithdepartment", BranchId);

            var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            //assmeblyList.Add(new{te})
            ViewBag.assmeblyList = assmeblyList;
            //Code for SessionList 
            SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
            data.AssemblyID = model.AssemblyID;
            var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
            ViewBag.sessionList = sessionList;

            //mSession mSession = new mSession();
            //mSession.SessionCode = sessionList.FirstOrDefault().SessionCode;
            //mSession.AssemblyID = data.AssemblyID;
            ViewBag.EditForm = "EditForm";
            //model.SessionID = mSession.SessionCode;                
            return PartialView("NewCommiteeReplyEntryForm", model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveCommiteeReplyEntryForm(tCommitteReplyPendency model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    string url = "/ReplyPendencyCoverPath/";
                    string directory = FileSettings.SettingValue + url;

                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }

                    string url1 = "~/Committee/TempFile";
                    string directory1 = Server.MapPath(url1);

                    if (Directory.Exists(directory1))
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                        if (savedFileName.Length > 0)
                        {
                            string SourceFile = savedFileName[0];
                            foreach (string page in savedFileName)
                            {
                                Guid FileName = Guid.NewGuid();

                                string name = Path.GetFileName(page);

                                string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));

                                if (!string.IsNullOrEmpty(name))
                                {
                                    if (!string.IsNullOrEmpty(model.CoverLetterPdfPath))
                                        System.IO.File.Delete(System.IO.Path.Combine(directory, model.CoverLetterPdfPath));
                                    System.IO.File.Copy(SourceFile, path, true);
                                    model.CoverLetterPdfPath = "/ReplyPendencyCoverPath/" + FileName + "_" + name.Replace(" ", "");

                                    //logic to delete temp file
                                    if (Directory.Exists(directory1))
                                    {
                                        string[] filePaths = Directory.GetFiles(directory1);
                                        foreach (string filePath in filePaths)
                                        {
                                            System.IO.File.Delete(filePath);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (model.PendencyId != 0)//in case of edit
                    {

                        // model.FinancialYear = Convert.ToString(model.SessionDate.Year);
                        if (model.CheckValue != model.TypeNo || model.CheckValueDept != model.DepartmentId || model.CheckValueAssemblyId != Convert.ToString(model.AssemblyID) || model.CheckValueSessionId != Convert.ToString(model.SessionID) || model.CheckFinancialYear != model.FinancialYear || model.CheckItemTypeId != model.ItemTypeId || model.CheckValueSessionDate != model.SessionDate)
                        {
                            var ItemNoCheck1 = Helper.ExecuteService("CommitteReplyPendency", "ItemNoCheck", model);
                            if (ItemNoCheck1 == null)
                            {
                                if (model.ItemTypeId == 4 || model.ItemTypeId == 5 || model.ItemTypeId == 6) { model.LaidInTheHouse = model.SessionDate; }

                                Helper.ExecuteService("CommitteReplyPendency", "SaveCommiteeReplyEntryForm", model);
                                return Content("edit");
                            }
                            else
                            {
                                return Content("TypeNoError");//type no already exist                       
                            }
                        }
                        else
                        {
                            if (model.ItemTypeId == 4 || model.ItemTypeId == 5 || model.ItemTypeId == 6) { model.LaidInTheHouse = model.SessionDate; }

                            Helper.ExecuteService("CommitteReplyPendency", "SaveCommiteeReplyEntryForm", model);
                            return Content("edit");
                        }
                    }
                    else
                    {// in case of new value

                        var ItemNoCheck1 = Helper.ExecuteService("CommitteReplyPendency", "ItemNoCheck", model);
                        if (ItemNoCheck1 == null)
                        {
                            model.CreatedDate = Convert.ToString(DateTime.Now);
                            model.CreatedBy = CurrentSession.UserName;
                            // model.FinancialYear = Convert.ToString(model.SessionDate.Year);
                            if (model.ItemTypeId == 4 || model.ItemTypeId == 5 || model.ItemTypeId == 6) { model.LaidInTheHouse = model.SessionDate; }

                            Helper.ExecuteService("CommitteReplyPendency", "SaveCommiteeReplyEntryForm", model);
                            return Content("Saved");
                        }
                        else
                        {
                            return Content("TypeNoError");//type no already exist
                        }
                    }
                }
                else
                {
                    //ModelState.AddModelError("Error", ex.Message);
                    return Content("modelError");
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
