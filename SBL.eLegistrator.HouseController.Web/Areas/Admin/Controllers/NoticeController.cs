﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Notice;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.DomainModel.Models.Category;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.IO;
#pragma warning disable CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using Microsoft.Security.Application;
#pragma warning restore CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models;
using SBL.eLegistrator.HouseController.Web.SMSWebServiceLatest;
using SBL.eLegistrator.HouseController.Web.EmailWebService;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Department;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class NoticeController : Controller
    {
        //
        // GET: /Admin/Notice/

        public ActionResult Index()
        {
            return View();
        }
        #region NoticeCategory



        public ActionResult NoticeIndex()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var Notice = (List<Category>)Helper.ExecuteService("ANotice", "GetAllNoticeCategory", null);
                var model = Notice.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting Notice Category List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting Notice Category List Deails";
                return View("AdminErrorPage");
            }



        }

        public ActionResult CreateNotice()
        {

            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var model = new CategoryViewModel()
                {
                    Mode = "Add",
                    TypeList = ModelMapping.GetStatusTypes(),
                    CategoryTypeList = ModelMapping.GetCategoryTypes(),
                    Status = 1
                };
                return View(model);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Creating Notice Category Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating Notice Category Deails";
                return View("AdminErrorPage");
            }



        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveNotice(CategoryViewModel model)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                if (model.Mode == "Add")
                {
                    model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("ANotice", "AddNoticeCategory", model.ToDomainModel());

                }
                else
                {

                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("ANotice", "UpdateNoticeCategory", model.ToDomainModel());
                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateNotice");
                }
                else
                {
                    return RedirectToAction("NoticeIndex");
                }
                // return RedirectToAction("NoticeIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Notices Category Deails");
                ViewBag.ErrorMessage = "Their is a Error While Saving Notices Category Deails";
                return View("AdminErrorPage");
            }




        }

        public ActionResult Edit(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                //var notice = (Category)Helper.ExecuteService("ANotice", "GetNoticeByIdCategory", new Category { CategoryID = Id });
                var notice = (Category)Helper.ExecuteService("ANotice", "GetNoticeByIdCategory", new Category { CategoryID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                //var model = notice.ToViewModel1("Edit");
                // model.TypeList = ModelMapping.GetNewsTypes();
                return View("CreateNotice", notice.ToViewModel("Edit"));
            }
            catch (Exception ex)
            {

                RecordError(ex, "Creating Notices Category Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Category Deails";
                return View("AdminErrorPage");
            }




        }

        public ActionResult DeleteNotice(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var notice = Helper.ExecuteService("ANotice", "DeleteNoticeCategory", new Category { CategoryID = Id });

                return RedirectToAction("NoticeIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Deleting Notices Category Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting Notices Category Deails";
                return View("AdminErrorPage");
            }



        }
        #endregion

        #region NoticeList
        public ActionResult NoticeEntryIndex()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var Notice = (List<mNotice>)Helper.ExecuteService("ANotice", "GetAllNoticeLastWeek", null);

                var model = Notice.ToViewModel1();

                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting Notices List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting NOtices List Deails";
                return View("AdminErrorPage");
            }
        }
      
        public ActionResult _NoticeOnFilter(string ID)
        {
            try
            {
                var Filter = "";

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                if (ID == "2")
                {
                    Filter = "GetAllNotice";
                }
                else
                {
                    Filter = "GetAllNoticeLastWeek";
                }


                var Notice = (List<mNotice>)Helper.ExecuteService("ANotice", Filter, null);

                var model = Notice.ToViewModel1();

                return PartialView(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting Notices List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting NOtices List Deails";
                return View("AdminErrorPage");
            }
        }

        public ActionResult CreateNoticeEntry()
        {
            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                NoticeViewModel model1 = new NoticeViewModel();

                var Assemblylist = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                model1.AssemblyList = new SelectList(Assemblylist, "AssemblyId", "AssemblyName", null);
                model1.Status = 1;
                var deptList = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
               
                model1.DeptList = new SelectList(deptList, "deptId", "deptname", null);
                mSession data = new mSession();
                data.AssemblyID = Assemblylist.FirstOrDefault().AssemblyCode;
                var Sessionlist = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                model1.SessionList = new SelectList(Sessionlist, "SessionId", "SessionName", null);


                var categorytype = (List<Category>)Helper.ExecuteService("ANotice", "GetAllCategoryType", null);
                model1.CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null);
                model1.Mode = "Add";
                model1.Status = 1;
                model1.TypeList = ModelMapping.GetStatusTypes();
                //Code for SessionList 
                //  mSession data = new mSession();
                //  data.AssemblyID = model1.AssemblyList.FirstOrDefault().AssemblyCode;
                //  var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                //  model1.SessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);



                //var model = new NoticeViewModel()

                //{
                //    Mode = "Add",
                //    TypeList = ModelMapping.GetStatusTypes(),
                //    CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null),
                //    Status = 1
                //    //NoticeTypeList = ModelMapping.GetNoticeType()
                //};
                return View(model1);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Creating Notices Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Deails";
                return View("AdminErrorPage");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveNoticeEntry(NoticeViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var notice = model.ToDomainModel();
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (model.Mode == "Add")
                {

                    // Guid FileName = Guid.NewGuid();
                    if (file != null)
                    {

                        Guid FileName1 = Guid.NewGuid();
                        var FileName2 = FileName1 + ".pdf";
                        notice.Attachments = UploadPhoto(file, FileName2);
                        //notice.Attachments = UploadPhoto(file, file.FileName);

                    }
                    else
                    {
                        notice.Attachments = "";
                    }
                    notice.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    notice.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                  
                    // notice.AssemblyId = model.AssemblyId;
                    //  notice.SessionId = model.SessionId;
                    //notice.ShowAlways = true;
                    //notice.NoticeType = "Image";//model.NoticeType;
                    // model.IsActive = true;
                    Helper.ExecuteService("ANotice", "AddNotice", notice);

                }
                else
                {
                    if (file != null)
                    {
                        if (!string.IsNullOrEmpty(notice.Attachments))
                        {
                            RemoveExistingPhoto(notice.Attachments);
                        }


                        Guid FileName1 = Guid.NewGuid();
                        var FileName2 = FileName1 + ".pdf";
                        notice.Attachments = UploadPhoto(file, FileName2);
                        //  notice.AssemblyId = model.AssemblyId;
                        //  notice.SessionId = model.SessionId;

                        //notice.Attachments = UploadPhoto(file, file.FileName);
                        notice.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                        Helper.ExecuteService("ANotice", "UpdateNotice", notice);
                    }
                    else
                    {
                        Helper.ExecuteService("ANotice", "UpdateNotice", notice);
                    }


                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateNoticeEntry");
                }
                else
                {
                    return RedirectToAction("NoticeEntryIndex");
                }
                //  return RedirectToAction("NoticeEntryIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Notice  Deails");
                ViewBag.ErrorMessage = "Their is a Error While Saving Notice Deails";
                return View("AdminErrorPage");
            }
        }

        public ActionResult NoticeEditEntry(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var categorytype = (List<Category>)Helper.ExecuteService("ANotice", "GetAllCategoryType", null);
                var Assemblylist = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                mSession data = new mSession();
                data.AssemblyID = Assemblylist.FirstOrDefault().AssemblyCode;
                var Sessionlist = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                //var notice = (mNotice)Helper.ExecuteService("ANotice", "GetNoticeById", new mNotice { NoticeID = Id });
                var notice = (mNotice)Helper.ExecuteService("ANotice", "GetNoticeById", new mNotice { NoticeID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = notice.ToViewModel1("Edit");
                model.CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null);
                model.SessionList = new SelectList(Sessionlist, "SessionId", "SessionName", null);
                model.AssemblyList = new SelectList(Assemblylist, "AssemblyId", "AssemblyName", null);
                model.ImageLocation = fileAcessingSettings.SettingValue + "/Notices/" + model.FileLocation;
                var deptList = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);

                deptList.Add(new mDepartment
                {
                    deptId = "-1",
                    deptname = "All Department"
                }); 
                model.DeptList = new SelectList(deptList, "deptId", "deptname", null);
             
                //var newPath = model.FileLocation;
                //byte[] image = ReadImage(newPath);
                //// System.IO.File.Delete(path);
                //model.bytesImage = "data:image/png;base64," + Convert.ToBase64String(image);
                return View("CreateNoticeEntry", model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Create Notices Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Deails";
                return View("AdminErrorPage");
            }

        }


        public ActionResult DeleteNoticeEntry(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var actualnotice = (mNotice)Helper.ExecuteService("ANotice", "GetNoticeById", new mNotice { NoticeID = Id });

                var notice = Helper.ExecuteService("ANotice", "DeleteNotice", new mNotice { NoticeID = Id });
                if (!string.IsNullOrEmpty(actualnotice.Attachments))
                    RemoveExistingPhoto(actualnotice.Attachments);
                return RedirectToAction("NoticeEntryIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Delete Notices Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting Notices Deails";
                return View("AdminErrorPage");
            }

        }


        public JsonResult CheckNoticeCategoryChildExist(int categoryId)
        {
            try
            {

                var isExist = (Boolean)Helper.ExecuteService("ANotice", "IsNoticeCategoryChildExist", new Category { CategoryID = categoryId });

                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #endregion

        #region Private Methods

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "NoticeError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }

        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string UploadPhoto(HttpPostedFileBase File, string FileName)
        {

            try
            {
                if (File != null)
                {
                    var NoticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNoticeFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    //var disFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string FilePath = System.IO.Path.Combine(FileSettings.SettingValue + NoticesSettings.SettingValue);
                    DirectoryInfo MDir = new DirectoryInfo(FilePath);
                    if (!MDir.Exists)
                    {
                        MDir.Create();
                    }
                    //DirectoryInfo MDir = new DirectoryInfo(disFileSettings.SettingValue);
                    //if (!MDir.Exists)
                    //{
                    //    MDir.Create();

                    //}

                    //DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~/Images/Notice/"));
                    //if (!Dir.Exists)
                    //{
                    //    Dir.Create();
                    //}

                    //string extension = System.IO.Path.GetExtension(File.FileName);
                    //string path = System.IO.Path.Combine(NoticesSettings.SettingValue + "\\", FileName);//Server.MapPath("~/Images/News/"), FileName);
                    //File.SaveAs(path);
                    //return (File.FileName);

                    //  string extension = System.IO.Path.GetExtension(File.FileName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + NoticesSettings.SettingValue, FileName);//Server.MapPath("~/Images/Notice/")
                    //if (!System.IO.File.Exists(path))
                    // {
                    File.SaveAs(path);
                    //  }
                    return (FileName);
                }
                return null;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        private void RemoveExistingPhoto(string OldPhoto)
        {
            // string path = System.IO.Path.Combine(Server.MapPath("~/Images/Notice/"), OldPhoto);

            try
            {
                var noticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNoticeFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + noticesSettings.SettingValue + "\\", OldPhoto);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public ActionResult DownloadFile(string fileName)
        {

            try
            {
                fileName = Sanitizer.GetSafeHtmlFragment(fileName);
                var noticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNoticeFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                var urlPath = FileSettings.SettingValue + noticesSettings.SettingValue + "\\" + fileName;
                Boolean isExist = System.IO.File.Exists(urlPath);
                if (isExist)
                {
                    var fs = System.IO.File.OpenRead(urlPath);//(Server.MapPath(fileName));
                    return File(fs, "appliaction/zip", fileName);//.Substring(fileName.LastIndexOf("/") + 1));
                }
                else
                    return null;
            }
            catch (Exception ex)
            {

                throw ex;
            }



        }

        #endregion

        #region PartialNoticeCategory
        public ActionResult PNoticeIndex()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var Notice = (List<Category>)Helper.ExecuteService("ANotice", "GetAllNoticeCategory", null);
                var model = Notice.ToViewModel();
                return PartialView("_NoticeIndexPartial", model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting Notice Category List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting Notice Category List Deails";
                return View("AdminErrorPage");
            }



        }
        public ActionResult PCreateNotice()
        {

            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var model = new CategoryViewModel()
                {
                    Mode = "Add",
                    TypeList = ModelMapping.GetStatusTypes(),
                    CategoryTypeList = ModelMapping.GetCategoryTypes(),
                    Status = 1
                };
                return PartialView("_CreateNoticePartial", model);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Creating Notice Category Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating Notice Category Deails";
                return View("AdminErrorPage");
            }



        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PSaveNotice(CategoryViewModel model)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                if (model.Mode == "Add")
                {
                    model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("ANotice", "AddNoticeCategory", model.ToDomainModel());

                }
                else
                {

                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("ANotice", "UpdateNoticeCategory", model.ToDomainModel());
                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("PCreateNotice");
                }
                else
                {
                    return RedirectToAction("PNoticeIndex");
                }
                // return RedirectToAction("NoticeIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Notices Category Deails");
                ViewBag.ErrorMessage = "Their is a Error While Saving Notices Category Deails";
                return View("AdminErrorPage");
            }




        }

        public ActionResult PEdit(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                var notice = (Category)Helper.ExecuteService("ANotice", "GetNoticeByIdCategory", new Category { CategoryID = Id });
                //var model = notice.ToViewModel1("Edit");
                // model.TypeList = ModelMapping.GetNewsTypes();
                return View("_CreateNoticePartial", notice.ToViewModel("Edit"));
            }
            catch (Exception ex)
            {

                RecordError(ex, "Creating Notices Category Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Category Deails";
                return View("AdminErrorPage");
            }




        }

        public ActionResult PDeleteNotice(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var notice = Helper.ExecuteService("ANotice", "DeleteNoticeCategory", new Category { CategoryID = Id });

                return RedirectToAction("PNoticeIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Deleting Notices Category Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting Notices Category Deails";
                return View("AdminErrorPage");
            }



        }
        #endregion

        #region partialNoticeList
        public ActionResult PNoticeEntryIndex()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var Notice = (List<mNotice>)Helper.ExecuteService("ANotice", "GetAllNotice", null);

                var model = Notice.ToViewModel1();

                return PartialView("_NoticeEntryIndexPartial", model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting Notices List Deails");
                ViewBag.ErrorMessage = "Their is a Error While Getting NOtices List Deails";
                return View("AdminErrorPage");
            }
        }

        public ActionResult PCreateNoticeEntry()
        {
            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var categorytype = (List<Category>)Helper.ExecuteService("ANotice", "GetAllCategoryType", null);
                var model = new NoticeViewModel()
                {
                    Mode = "Add",
                    TypeList = ModelMapping.GetStatusTypes(),
                    CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null),
                    Status = 1
                    //NoticeTypeList = ModelMapping.GetNoticeType()
                };
                return PartialView("_CreateNoticeEntryPartial", model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Creating Notices Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Deails";
                return View("AdminErrorPage");
            }





        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PSaveNoticeEntry(NoticeViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var notice = model.ToDomainModel();
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (model.Mode == "Add")
                {

                    // Guid FileName = Guid.NewGuid();
                    if (file != null)
                    {

                        Guid FileName1 = Guid.NewGuid();
                        var FileName2 = FileName1 + ".pdf";
                        notice.Attachments = UploadPhoto(file, FileName2);
                        //notice.Attachments = UploadPhoto(file, file.FileName);

                    }
                    else
                    {
                        notice.Attachments = "";
                    }
                    notice.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    notice.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    //notice.ShowAlways = true;
                    //notice.NoticeType = "Image";//model.NoticeType;
                    // model.IsActive = true;
                    Helper.ExecuteService("ANotice", "AddNotice", notice);

                }
                else
                {
                    if (file != null)
                    {
                        if (!string.IsNullOrEmpty(notice.Attachments))
                        {
                            RemoveExistingPhoto(notice.Attachments);
                        }


                        Guid FileName1 = Guid.NewGuid();
                        var FileName2 = FileName1 + ".pdf";
                        notice.Attachments = UploadPhoto(file, FileName2);


                        //notice.Attachments = UploadPhoto(file, file.FileName);
                        notice.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                        Helper.ExecuteService("ANotice", "UpdateNotice", notice);
                    }
                    else
                    {
                        Helper.ExecuteService("ANotice", "UpdateNotice", notice);
                    }


                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("PCreateNoticeEntry");
                }
                else
                {
                    return RedirectToAction("PNoticeEntryIndex");
                }
                //  return RedirectToAction("NoticeEntryIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Notice  Deails");
                ViewBag.ErrorMessage = "Their is a Error While Saving Notice Deails";
                return View("AdminErrorPage");
            }




        }
        public ActionResult PNoticeEditEntry(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var categorytype = (List<Category>)Helper.ExecuteService("ANotice", "GetAllCategoryType", null);
                var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                var notice = (mNotice)Helper.ExecuteService("ANotice", "GetNoticeById", new mNotice { NoticeID = Id });
                var model = notice.ToViewModel1("Edit");
                model.CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null);
                model.ImageLocation = fileAcessingSettings.SettingValue + "/Notices/" + model.FileLocation;
                //var newPath = model.FileLocation;
                //byte[] image = ReadImage(newPath);
                //// System.IO.File.Delete(path);
                //model.bytesImage = "data:image/png;base64," + Convert.ToBase64String(image);
                return View("_CreateNoticeEntryPartial", model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Create Notices Deails");
                ViewBag.ErrorMessage = "Their is a Error While Creating Notices Deails";
                return View("AdminErrorPage");
            }





        }
        public ActionResult PDeleteNoticeEntry(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var actualnotice = (mNotice)Helper.ExecuteService("ANotice", "GetNoticeById", new mNotice { NoticeID = Id });

                var notice = Helper.ExecuteService("ANotice", "DeleteNotice", new mNotice { NoticeID = Id });
                if (!string.IsNullOrEmpty(actualnotice.Attachments))
                    RemoveExistingPhoto(actualnotice.Attachments);
                return RedirectToAction("PNoticeEntryIndex");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Delete Notices Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting Notices Deails";
                return View("AdminErrorPage");
            }

        }
        public object SendSMSandEmail(string NoticeIDs)
        {
            string[] NoticeId = NoticeIDs.Split(',');
            for (int i = 0; i < NoticeId.Length; i++)
            {
                Int32 NoticeIdInt = Convert.ToInt32(NoticeId[i]);
                if (NoticeIdInt != 0)
                {
                    var notice = (mNotice)Helper.ExecuteService("ANotice", "GetNoticeById", new mNotice { NoticeID = NoticeIdInt });


                    //#region SMS And Email

                    //List<string> emailAddresses = new List<string>();
                    //List<string> phoneNumbers = new List<string>();
                    //SendNotificationsModel model = new SendNotificationsModel();
                    //SMSMessageList smsMessage = new SMSMessageList();
                    //CustomMailMessage emailMessage = new CustomMailMessage();

                    ////# SMS Settings
                    //if (notice != null)
                    //{

                    //    emailAddresses.Add(notice.Attachments);
                    //    phoneNumbers.Add(notice.CategoryName);

                    //}
                    //if (emailAddresses != null && phoneNumbers != null)
                    //{
                    //    foreach (var item in phoneNumbers)
                    //    {
                    //        smsMessage.MobileNo.Add(item);
                    //    }
                    //    foreach (var item in emailAddresses)
                    //    {
                    //        emailMessage._toList.Add(item);
                    //    }
                    //}


                    //    model.SMSTemplate = "Your entry pass request with request ID :- " + EntityModel.RequestdID + " has been approved by admin, please collect it from Vidhan Sabha";
                    //    model.EmailTemplate = "Your entry pass request with request ID :- " + EntityModel.RequestdID + " has been approved by admin, please collect it from Vidhan Sabha";



                    //string message = model.SMSTemplate;
                    //smsMessage.SMSText = message;
                    //smsMessage.ModuleActionID = 1;
                    //smsMessage.UniqueIdentificationID = 1;
                    //bool Mobile = false;
                    //bool Email = false;
                    //if (EntityModel.MobileNo != null)
                    //{
                    //    Mobile = EntityModel.MobileNo.Equals(EntityModel.MobileNo) ? true : false;
                    //}
                    //if (EntityModel.Email != null)
                    //{
                    //    Email = EntityModel.Email.Equals(EntityModel.Email) ? true : false;
                    //}
                    //emailMessage._isBodyHtml = true;
                    //emailMessage._subject = EntityModel.AssemblyCode + "th Legislative Assembly Pass Request";

                    //// model.EmailTemplate = "Your pass request with request ID :- " + EntityModel.RequestdID + " has been generated, please keep this for future reference";

                    //emailMessage._body = model.EmailTemplate;

                    //try
                    //{
                    //    if (EntityModel.MobileNo != null || EntityModel.Email != null)
                    //    {
                    //        Notification.Send(Mobile, Email, smsMessage, emailMessage);
                    //    }

                    //}
                    //catch (Exception exp)
                    //{
                    //    string errorMsg = exp.Message;
                    //    throw;
                    //}

                    //#endregion

                }
            }
            return Json("URL", JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
