﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
////using SBL.DomainModel.Models.AdminEvent;
//using SBL.eLegislator.HPMS.ServiceAdaptor;
//using SBL.Service.Common;
//using SBL.eLegistrator.HouseController.Web.Helpers;

//namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
//{
//    public class EventController : Controller
//    {
//        //
//        // GET: /Admin/Event/

//        //public ActionResult Index()
//        //{
//        //    return View();
//        //}

//        ServiceAdaptor serviceAdaptor = new ServiceAdaptor();
//        [HttpGet]
//        public ActionResult AddEvent()
//        {
//            mAEvent events = new mAEvent();
//            events.mode = "Insert";
//            return View(events);
//        }
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult AddEvent(mAEvent events)
//        {
//            if (events.mode == "Insert")
//            {
//                //ServiceParameter serviceparameter = ServiceParameter.Create("Events", "AddEvent", events);
//                //int result = Convert.ToInt32(serviceAdaptor.CallService(serviceparameter));
//                Helper.ExecuteService("AEvent", "AddEvent", events);
//                return RedirectToAction("GetEventList");
//            }
//            else if (events.mode == "Edit")
//            {
//                //ServiceParameter serviceparameter = ServiceParameter.Create("Events", "UpdateEvent", events);
//                //int result = Convert.ToInt32(serviceAdaptor.CallService(serviceparameter));
//                Helper.ExecuteService("AEvent", "UpdateEvent", events);
//                return RedirectToAction("GetEventList");
//            }
//            return null;
//        }
//        const int pageSize = 10;
//        //[HttpGet]
//        public ActionResult GetEventList(int PageNumber = 1, int LoopStart = 1, int LoopEnd = 5, string SearchText = "")
//        {
//            //ServiceParameter serviceParameter = ServiceParameter.Create("Events", "EventList", null);
//            List<mAEvent> events = (List<mAEvent>)(Helper.ExecuteService("AEvent", "EventList", null));

//            var model = new PagedList<mAEvent>()
//            {
//                //List = distrctList.ToList(),
//                List = events.Skip((PageNumber - 1) * pageSize).Take(pageSize).ToList(),
//                CurrentPage = PageNumber,
//                RowsPerPage = pageSize,
//                TotalRecordCount = events.Count(),
//                LoopEnd = LoopEnd,
//                LoopStart = LoopStart,
//                SearchText = SearchText
//            };
//            if (Request.IsAjaxRequest())
//            {
//                return PartialView("_Event", model);
//            }
//            return View(model);
//        }

//        public ActionResult EditEvent(int EventId)
//        {
//            //ServiceParameter serviceparameter = ServiceParameter.Create("Events", "EditEvent", EventId);
//            mAEvent events = (mAEvent)(Helper.ExecuteService("AEvent", "EditEvent", EventId));
//            events.mode = "Edit";
//            return View("AddEvent", events);
//        }
//        //[ValidateAntiForgeryToken]
//        public ActionResult DeleteEvent(int EventId)
//        {
//            //ServiceParameter serviceparameter = ServiceParameter.Create("Events", "DeleteEvent", EventId);
//            List<mAEvent> data = (List<mAEvent>)Helper.ExecuteService("AEvent", "DeleteEvent", EventId);
//            //return PartialView("EventList", data);
//            return RedirectToAction("GetEventList");
//        }

//    }
//}
