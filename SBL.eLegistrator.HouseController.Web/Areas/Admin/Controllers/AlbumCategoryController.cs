﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.AlbumCategory;
namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class AlbumCategoryController : Controller
    {
        //
        // GET: /Admin/AlbumCategory/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetAllAlbumCategory()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            var Gallery = (List<AlbumCategory>)Helper.ExecuteService("AlbumCategory", "GetAlbumCategory", null);
            var model = Gallery.AlbumCategoryViewModel();
            return View(model);
         
        }
        public ActionResult CreateAlbumCategory()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
          var model=new AlbumCategoryViewModel()
          {
              Mode="Add"
            
        };
          return View(model);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveAlbumCategory(AlbumCategoryViewModel model)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            if (model.Mode == "Add")
            {
                Helper.ExecuteService("AlbumCategory", "AddAlbumCategory", model.AlbumCategoryDomainModel());
            }
            else
            {
                Helper.ExecuteService("AlbumCategory", "UpdateAlbumCategory", model.AlbumCategoryDomainModel());
            }


            return RedirectToAction("GetAllAlbumCategory");

        }
        public ActionResult EditAlbumCategory(string Id)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            //AlbumCategory Gallery = (AlbumCategory)Helper.ExecuteService("AlbumCategory", "EditAlbumCategory", Id);
            AlbumCategory Gallery = (AlbumCategory)Helper.ExecuteService("AlbumCategory", "EditAlbumCategory", Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));
            var model = Gallery.ToEditAlbumCategoryViewModel("Edit");

            return View("CreateAlbumCategory", model);
        }
        public ActionResult DeleteAlbumCategory(int Id)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            AlbumCategory Gallery = (AlbumCategory)Helper.ExecuteService("AlbumCategory", "DeleteAlbumCategory", Id);

            return RedirectToAction("GetAllAlbumCategory");

        }


        public JsonResult CheckAlbumCategoryChildExist(int Id)
        {
            var isExist = (Boolean)Helper.ExecuteService("AlbumCategory", "IsAlbumCategoryIdChildExist", new AlbumCategory { AlbumCategoryID = Id });

            return Json(isExist, JsonRequestBehavior.AllowGet);
        }


    }
}
