﻿using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net;
//using System.Data.SqlClient;
using System.Text.RegularExpressions;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.House;
//using System.Data.EntityClient;
using System.Configuration;
using System.Web.Configuration;
using System.Data.SqlClient;
using SBL.DomainModel;
using SBL.DomainModel.Models.Passes;
using System.Data;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.UploadMetaFile;
using EvoPdf;
using SBL.DomainModel.Models.SiteSetting;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class PassCountListController : Controller
    {
        //
        // GET: /Admin/PassCountList/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                UploadMetaViewModel model = new UploadMetaViewModel();
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                mSession data = new mSession();
                data.AssemblyID = int.Parse(CurrentSession.AssemblyId);// 12;// assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = int.Parse(CurrentSession.SessionId);// 6;// sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                model.AssemblyId = data.AssemblyID;
                model.SessionId = sesdate.SessionId;
                //ViewBag.AssemblyId = sesdate.SessionId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);
                model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "";
                return View("AdminErrorPage");
            }
        }
        public ActionResult SearchPassCountList(string Assembly, string Session, string Date)
        {

            try
            {
                UploadMetaViewModel model = new UploadMetaViewModel();

                if (!string.IsNullOrEmpty(Assembly) && !string.IsNullOrEmpty(Session) && !string.IsNullOrEmpty(Date))
                {

                    //mUploadmetaDataFile Searchmodel = new mUploadmetaDataFile();
                    DateTime? datestr = new DateTime();
                    datestr = new DateTime(Convert.ToInt32(Date.Split('-')[2]), Convert.ToInt32(Date.Split('-')[1]), Convert.ToInt32(Date.Split('-')[0]));
                    //Searchmodel.SessionDate = datestr;
                    // Searchmodel.AssemblyId = Convert.ToInt32(Assembly);
                    //Searchmodel.SessionId = Convert.ToInt32(Session);
                    ViewBag.SearchAssembly = Assembly;
                    ViewBag.SearchSession = Session;
                    ViewBag.SessionDate = Date;
                    DataSet SummaryDataSet = new DataSet();
                    List<VisitorData> listToupdate = new List<VisitorData>();
                    var methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@AssemblyCode", Assembly));
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionCode", Session));
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", datestr.ToString()));
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[GetPassReport]", methodParameter);
                    if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                    {

                        for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                        {
                            VisitorData mel = new VisitorData();
                            mel.PassCategoryID = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["PassCategoryID"]);
                            mel.PassCategory = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["Name"]);
                            mel.AllowPassCount = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ALLOWED"]);
                            mel.InvalidPassCount = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["INVALID"]);
                            mel.BlockedPassCount = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["BLOCKED"]);
                            mel.ApprovedPassCount = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedPass"]);
                            mel.ApproveRequestedCount = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedRequested"]);
                            //     mel.InvalidPassCount = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["invalid"]);
                            listToupdate.Add(mel);
                        }
                    }
                    model.VisitorDataList = listToupdate;
                }


                return View("_PassCountList", model);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "Their is a Error While Create the AssemblyFile Details";
                return View("AdminErrorPage");
            }

        }
        public ActionResult GetPassList(string Assembly, string Session, string id, string type, string date)
        {

            try
            {
                UploadMetaViewModel model = new UploadMetaViewModel();
                if (!string.IsNullOrEmpty(Assembly) && !string.IsNullOrEmpty(Session) && !string.IsNullOrEmpty(Session) && !string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(type) && !string.IsNullOrEmpty(date))
                {
                    DateTime? datestr = new DateTime();
                    datestr = new DateTime(Convert.ToInt32(date.Split('-')[2]), Convert.ToInt32(date.Split('-')[1]), Convert.ToInt32(date.Split('-')[0]));
                    DataSet SummaryDataSet = new DataSet();
                    List<VisitorData> listToupdate = new List<VisitorData>();
                    var methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@AssemblyCode", Assembly));
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionCode", Session));
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", datestr.ToString()));
                    methodParameter.Add(new KeyValuePair<string, string>("@PassCategoryID", id));
                    methodParameter.Add(new KeyValuePair<string, string>("@ResultType", type));
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[GetPassListByCategory]", methodParameter);
                    if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                    {

                        for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                        {
                            VisitorData mel = new VisitorData();
                            mel.PassCategoryID = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["PassCategoryID"]);
                            mel.PassCategory = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["CategoryName"]);
                            mel.Name = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["Name"]);
                            mel.MobileNo = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["MobileNo"]);
                            mel.AadharID = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AadharID"]);
                            mel.OrganizationName = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["OrganizationName"]);
                            mel.Address = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["Address"]);
                            mel.SessionDateFrom = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateFrom"]);
                            mel.SessionDateTo = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateTo"]);
                            mel.ApprovedDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ApprovedDate"]);
                            mel.EntryTimeSl = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["EntryTime"]);
                            mel.EntryDate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            mel.GateNumber = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["GateNumber"]);

                            listToupdate.Add(mel);
                        }
                    }
                    model.VisitorDataList = listToupdate;
                }


                return View("_PassListPopup", model);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "Their is a Error While Create the AssemblyFile Details";
                return View("AdminErrorPage");
            }

        }

        [HttpPost]
        public ActionResult DownloadExcel(string Assembly, string Session, string Date)
        {

            string Result = this.GetPassContList(Assembly, Session, Date);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=PassCountList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }


        public ActionResult DownloadPDF(string Assembly, string Session, string Date)
        {
            string url = "/PassCountList/";



            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            string path = "";
            string Result = this.GetPassContList(Assembly, Session, Date);
            string FileName = CreatePassCountListPdf(Result, url);
            try
            {

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                path = Path.Combine(Server.MapPath("~" + url), FileName);

#pragma warning disable CS0219 // The variable 'contentType' is assigned but its value is never used
                string contentType = "application/octet-stream";
#pragma warning restore CS0219 // The variable 'contentType' is assigned but its value is never used
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

            byte[] bytes = System.IO.File.ReadAllBytes(path);
            return File(bytes, "application/pdf");
        }

        public string CreatePassCountListPdf(string Report, string Url)
        {
            try
            {
                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 20, 20), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = Report;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }



                Guid FId = Guid.NewGuid();
                string fileName = FId + "_PassCountList.pdf";


                string directory = Server.MapPath(Url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(Server.MapPath("~" + Url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();



                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {


            }
        }

        public string GetPassContList(string Assembly, string Session, string Date)
        {

            StringBuilder ReplyList = new StringBuilder();
            DateTime? datestr = new DateTime();
            datestr = new DateTime(Convert.ToInt32(Date.Split('-')[2]), Convert.ToInt32(Date.Split('-')[1]), Convert.ToInt32(Date.Split('-')[0]));
            DataSet SummaryDataSet = new DataSet();
            List<VisitorData> listToupdate = new List<VisitorData>();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@AssemblyCode", Assembly));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionCode", Session));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDate", datestr.ToString()));
            SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[GetPassReport]", methodParameter);

            if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
            {
                ReplyList.Append(string.Format("<div style='text-align:center;'><h2>Pass Count List</h2></div>"));

                ReplyList.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                ReplyList.Append("<thead class='header' ><tr style='background-color: #428bca ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:30px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px;'>SR. NO.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Pass Category</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Allowed Pass</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Invalid Pass</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Blocked Pass</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Approved Pass</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Requested Pass</th>");

                ReplyList.Append("</tr></thead>");


                ReplyList.Append("<tbody  class='body'>");
                int count = 1;
                for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                {
                    ReplyList.Append("<tr>");
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", count));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", SummaryDataSet.Tables[0].Rows[i]["Name"].ToString()));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", SummaryDataSet.Tables[0].Rows[i]["ALLOWED"].ToString()));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", SummaryDataSet.Tables[0].Rows[i]["INVALID"].ToString()));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", SummaryDataSet.Tables[0].Rows[i]["BLOCKED"].ToString()));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", SummaryDataSet.Tables[0].Rows[i]["ApprovedPass"].ToString()));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", SummaryDataSet.Tables[0].Rows[i]["ApprovedRequested"].ToString()));
                    ReplyList.Append("</tr>");
                    count++;
                }


                ReplyList.Append("</tbody></table></div> ");
            }
            else
            {
                ReplyList.Append("No Record Found");
            }
            return ReplyList.ToString();
        }

    }
}
