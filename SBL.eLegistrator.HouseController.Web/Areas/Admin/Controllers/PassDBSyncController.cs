﻿using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net;
//using System.Data.SqlClient;
using System.Text.RegularExpressions;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.House;
//using System.Data.EntityClient;
using System.Configuration;
using System.Web.Configuration;
using System.Data.SqlClient;
using SBL.DomainModel;
using SBL.DomainModel.Models.Passes;
using System.Data;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.UploadMetaFile;
using EvoPdf;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    public class PassDBSyncController : Controller
    {
        //
        // GET: /Admin/PassDBSync/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                UploadMetaViewModel model = new UploadMetaViewModel();
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                mSession data = new mSession();
                data.AssemblyID = 12;// assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                var PassCategoriesList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = 6;// sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                model.AssemblyId = data.AssemblyID;
                model.SessionId = sesdate.SessionId;
                //ViewBag.AssemblyId = sesdate.SessionId;
                model.PassCategoryList = PassCategoriesList;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "";
                return View("AdminErrorPage");
            }
        }

        public JsonResult UploadPassData(int AssemblyId, int SessionId, int PassCategoryId, string TableName)
        {
            try
            {


                var mainScript = new StringBuilder();
                if (TableName == "PublicPasses")
                {
                    var publicPassRequest = (List<PublicPass>)Helper.ExecuteService("AdministrationBranch", "GetPublicPassesByAssemblyAndSession", new PublicPass { PassCategoryID = PassCategoryId, AssemblyCode = AssemblyId, SessionCode = SessionId });
                    if (publicPassRequest != null && publicPassRequest.Count() > 0)
                    {
                        mainScript.Append("DELETE FROM " + TableName + " WHERE PassCategoryID='" + PassCategoryId + "' and AssemblyCode=" + AssemblyId + " and SessionCode=" +
                                      SessionId + " and IsLocal=1 ;" + "\n");
                        foreach (var item in publicPassRequest)
                        {
                            mainScript.Append("INSERT INTO [dbo].[PublicPasses] ([PassCode] ,[PassCategoryID]  ,[AssemblyCode] ,[SessionCode] ,[Prefix]");
                            mainScript.Append(" ,[AadharID] ,[Name] ,[Gender] ,[Age] ,[FatherName] ,[NoOfPersions] ,[RecommendationType] ,[RecommendationBy] ,[RecommendationDescription]");
                            mainScript.Append(" ,[MobileNo] ,[Email] ,[Address] ,[OrganizationName] ,[Designation] ,[DayTime] ,[Time] ,[DepartmentID] ,[SessionDateFrom]");
                            mainScript.Append(" ,[SessionDateTo] ,[ApprovedDate] ,[ApprovedBy] ,[VehicleNumber] ,[GateNumber]  ,[IsRequested]  ,[RequestedDate]");
                            mainScript.Append(" ,[Status] ,[RequestedBy] ,[IsActive] ,[IsApproved] ,[Photo] ,[RejectionDate] ,[RejectionReason]  ,[DeptApprovalNeeded],[IsLocal])");
                            mainScript.Append(string.Format(" VALUES ({0},{1},{2},{3},'{4}','{5}','{6}','{7}',{8},'{9}',{10},'{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}'); \n",
                                (item.PassCode.HasValue ? item.PassCode.Value : 0), item.PassCategoryID, item.AssemblyCode, item.SessionCode, (!string.IsNullOrEmpty(item.Prefix) ? item.Prefix : ""),

                               (!string.IsNullOrEmpty(item.AadharID) ? item.AadharID : ""), (!string.IsNullOrEmpty(item.Name) ? item.Name : ""),

                               (!string.IsNullOrEmpty(item.Gender) ? item.Gender : ""), (item.Age.HasValue ? item.Age.Value : 0), (!string.IsNullOrEmpty(item.FatherName) ? item.FatherName : ""),

                              (item.NoOfPersions.HasValue ? item.NoOfPersions.Value : 0), (!string.IsNullOrEmpty(item.RecommendationType) ? item.RecommendationType : ""),
                               (!string.IsNullOrEmpty(item.RecommendationBy) ? item.RecommendationBy : ""),
                                   (!string.IsNullOrEmpty(item.RecommendationDescription) ? item.RecommendationDescription : ""),

                            (!string.IsNullOrEmpty(item.MobileNo) ? item.MobileNo : ""), (!string.IsNullOrEmpty(item.Email) ? item.Email : ""),
                         (!string.IsNullOrEmpty(item.Address) ? item.Address.Replace("'", "''") : ""), (!string.IsNullOrEmpty(item.OrganizationName) ? item.OrganizationName : ""),
                         (!string.IsNullOrEmpty(item.Designation) ? item.Designation.Replace("'", "''") : ""), (!string.IsNullOrEmpty(item.DayTime) ? item.DayTime : " "),
                          item.Time, (!string.IsNullOrEmpty(item.DepartmentID) ? item.DepartmentID : ""), (!string.IsNullOrEmpty(item.SessionDateFrom) ? item.SessionDateFrom : ""),
                          (!string.IsNullOrEmpty(item.SessionDateTo) ? item.SessionDateTo : ""),
                        item.ApprovedDate, (!string.IsNullOrEmpty(item.ApprovedBy) ? item.ApprovedBy : ""), (!string.IsNullOrEmpty(item.VehicleNumber) ? item.VehicleNumber : ""),
                           (!string.IsNullOrEmpty(item.GateNumber) ? item.GateNumber : ""),
                               item.IsRequested, item.RequestedDate, item.Status,
                               (!string.IsNullOrEmpty(item.RequestedBy) ? item.RequestedBy : ""),
                               item.IsActive, item.IsApproved,
                              (!string.IsNullOrEmpty(item.Photo) ? item.Photo : ""), item.RejectionDate,
                               (!string.IsNullOrEmpty(item.RejectionReason) ? item.RejectionReason : ""),
                              item.DeptApprovalNeeded, true));



                        }
                    }

                }
                else if (TableName == "mPasses")
                {
                    var PassRequest = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetAllMPasses",
                        new mPasses { ApprovedPassCategoryID = PassCategoryId, AssemblyCode = AssemblyId, SessionCode = SessionId });
                    if (PassRequest != null && PassRequest.Count() > 0)
                    {
                        mainScript.Append("DELETE FROM " + TableName + " WHERE ApprovedPassCategoryID='" + PassCategoryId + "' and AssemblyCode=" + AssemblyId + " and SessionCode=" +
                                      SessionId + " and IsLocal=1 ;" + "\n");
                        foreach (var item in PassRequest)
                        {
                            mainScript.Append("INSERT INTO [dbo].[mPasses] ([PassCode] ,[AssemblyCode] ,[SessionCode]  ,[Prefix] ,[AadharID]");
                            mainScript.Append(" ,[Name]  ,[Gender] ,[Age] ,[FatherName] ,[NoOfPersions]  ,[RecommendationType] ,[RecommendationBy]");
                            mainScript.Append(",[RecommendationDescription] ,[MobileNo] ,[Email] ,[Address] ,[OrganizationName] ,[Designation]");
                            mainScript.Append(" ,[DayTime] ,[Time] ,[DepartmentID] ,[SessionDateFrom] ,[SessionDateTo] ,[ApprovedDate] ,[ApprovedBy]");
                            mainScript.Append(",[VehicleNumber] ,[GateNumber] ,[IsRequested] ,[RequestedDate] ,[Status] ,[RequestedBy] ,[IsActive]");
                            mainScript.Append(",[IsApproved]  ,[RejectionDate] ,[RejectionReason] ,[Photo] ,[RequestedPassCategoryID] ,[ApprovedPassCategoryID]");
                            mainScript.Append(" ,[RequestID],[IsLocal])");
                            mainScript.Append(string.Format(" VALUES ({0},{1},{2},'{3}','{4}','{5}','{6}',{7},'{8}',{9},'{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}',{36},{37},'{38}','{39}'); \n",
                              (item.PassCode.HasValue ? item.PassCode.Value : 0), item.AssemblyCode, item.SessionCode,
                              (!string.IsNullOrEmpty(item.Prefix) ? item.Prefix : ""), (!string.IsNullOrEmpty(item.AadharID) ? item.AadharID : ""),
                              (!string.IsNullOrEmpty(item.Name) ? item.Name : ""), (!string.IsNullOrEmpty(item.Gender) ? item.Gender : ""),
                              (item.Age.HasValue ? item.Age.Value : 0), (!string.IsNullOrEmpty(item.FatherName) ? item.FatherName : ""),
                            (item.NoOfPersions.HasValue ? item.NoOfPersions.Value : 0), (!string.IsNullOrEmpty(item.RecommendationType) ? item.RecommendationType : ""),
                            (!string.IsNullOrEmpty(item.RecommendationBy) ? item.RecommendationBy : ""), (!string.IsNullOrEmpty(item.RecommendationDescription) ? item.RecommendationDescription : ""),
                              (!string.IsNullOrEmpty(item.MobileNo) ? item.MobileNo : ""), (!string.IsNullOrEmpty(item.Email) ? item.Email : ""),
                              (!string.IsNullOrEmpty(item.Address) ? item.Address.Replace("'", "''") : ""), (!string.IsNullOrEmpty(item.OrganizationName) ? item.OrganizationName : ""),
                              (!string.IsNullOrEmpty(item.Designation) ? item.Designation.Replace("'", "''") : ""), (!string.IsNullOrEmpty(item.DayTime) ? item.DayTime : ""),
                              item.Time,
                               (!string.IsNullOrEmpty(item.DepartmentID) ? item.DepartmentID : ""), (!string.IsNullOrEmpty(item.SessionDateFrom) ? item.SessionDateFrom : ""),
                               (!string.IsNullOrEmpty(item.SessionDateTo) ? item.SessionDateTo : ""), item.ApprovedDate,
                                (!string.IsNullOrEmpty(item.ApprovedBy) ? item.ApprovedBy : ""), (!string.IsNullOrEmpty(item.VehicleNumber) ? item.VehicleNumber : ""),
                                 (!string.IsNullOrEmpty(item.GateNumber) ? item.GateNumber : ""), item.IsRequested, item.RequestedDate, item.Status,
                                  (!string.IsNullOrEmpty(item.RequestedBy) ? item.RequestedBy : ""), item.IsActive, item.IsApproved,
                                item.RejectionDate, (!string.IsNullOrEmpty(item.RejectionReason) ? item.RejectionReason : ""),
                                (!string.IsNullOrEmpty(item.Photo) ? item.Photo : ""), item.RequestedPassCategoryID,
                                item.ApprovedPassCategoryID, (!string.IsNullOrEmpty(item.RequestID) ? item.RequestID : ""), true));


                        }
                    }
                }
                else if (TableName == "VisitorData")
                {

                    var VisitorData = (List<VisitorData>)Helper.ExecuteService("AdministrationBranch", "GetVisitorDataList",
                            new VisitorData { AssemblyCode = AssemblyId, SessionCode = SessionId });
                    if (VisitorData != null && VisitorData.Count() > 0)
                    {
                        mainScript.Append("DELETE FROM " + TableName + " WHERE AssemblyCode=" + AssemblyId + " and SessionCode=" +
                                      SessionId + " and IsLocal=1 ;" + "\n");
                        foreach (var item in VisitorData)
                        {
                            mainScript.Append("INSERT INTO [dbo].[VisitorData]  ([AssemblyCode] ,[SessionCode] ,[SessionDate] ,[PassCode]");
                            mainScript.Append(",[EntryTime] ,[GateNumber] ,[PoliceID] ,[VerificationResult] ,[BlockingReason],[IsLocal])");

                            mainScript.Append(string.Format(" VALUES ({0},{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}'); \n",
                                item.AssemblyCode, item.SessionCode, item.SessionDate, item.PassCode, item.EntryTime, (!string.IsNullOrEmpty(item.GateNumber) ? item.GateNumber : ""),
                                item.PoliceID, (!string.IsNullOrEmpty(item.VerificationResult) ? item.VerificationResult : ""),
                            (!string.IsNullOrEmpty(item.BlockingReason) ? item.BlockingReason : ""), true));
                        }
                    }
                }


                string DBScript = mainScript.ToString();
                if (!string.IsNullOrEmpty(DBScript))
                {
                    string sqlConnectionString = "Data Source=10.25.129.161\\SQLEXPRESS,1433;Initial Catalog=HPMS_IntranetApp;Persist Security Info=True;User ID=sa;Password=e_Vidhan_PDB";
                 //   string sqlConnectionString = "Data Source=SBL-PC10;Initial Catalog=HPMS_19NOV;Persist Security Info=True;User ID=sa;Password=sa";
                    SqlConnection conn = new SqlConnection(sqlConnectionString);

                    conn.Open();

                    SqlCommand sqlCmd = new SqlCommand(DBScript, conn);
                    sqlCmd.ExecuteNonQuery();
                    conn.Close();




                    return Json("Script successfuly uploaded", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data is not availble.", JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                return Json(ex.InnerException.ToString(), JsonRequestBehavior.AllowGet);
                throw;
            }
        }
        public static string GET(string url, string jsonContent)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            long length = 0;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    length = response.ContentLength;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (WebException ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                // Log exception and throw as for GET example above
            }
            return "";
        }



    }
}
