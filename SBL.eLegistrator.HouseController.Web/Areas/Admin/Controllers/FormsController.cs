﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SBL.DomainModel.Models.Forms;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using System.IO;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class FormsController : Controller
    {
        //
        // GET: /Admin/Forms/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetAllForms()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var tForms = (List<tForms>)Helper.ExecuteService("Forms", "GetAllForms", null);

                var model = tForms.ToViewModel1();
                return View(model);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Getting All Forms");
                ViewBag.ErrorMessage = "Their is a Error While Deleting the Forms Details";
                return View("AdminErrorPage");
            }

        }

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "AssemblyFileError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

              
            }
        }

        public ActionResult CreateForms()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                FormsViewModel model = new FormsViewModel();

                //Code for AssemblyList 
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                
                var doctypeList = (List<tFormsTypeofDocuments>)Helper.ExecuteService("Forms", "GetFormsTypeofDocumentLst", null);
                var docList = new SelectList(doctypeList, "TypeofFormId", "TypeofFormName", null);
                model.AssemblyList = assmeblyList;
                
                model.FormDocumentTypeList = doctypeList;
                model.FormDocumentTypeList1 = docList;
                model.StatusTypeList = ModelMapping.GetStatusTypes();
                model.Mode = "Add";
                return View("CreateForms", model);

            }
            catch (Exception ex)
            {

                RecordError(ex, "Creat Forms");
                ViewBag.ErrorMessage = "Their is a Error While Creating the Forms Details";
                return View("AdminErrorPage");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveForms(FormsViewModel model, HttpPostedFileBase file)
        {


            try
            {



                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

               

                if (model.Mode == "Add")
                {
                    

                    var data = model.ToDomainModel();

                    data.CreatedBy = CurrentSession.UserID;
                    data.ModifiedBy = CurrentSession.UserID;
                    if (file != null)
                    {
                        data.UploadFile = UploadPhoto(file, model);
                    }
                    else
                    {
                        data.UploadFile = null;
                    }
                    Helper.ExecuteService("Forms", "AddForms", data);

                }
                else
                {
                    var data = model.ToDomainModel();
                   

                    data.ModifiedBy = CurrentSession.UserID;
                    if (file != null && !string.IsNullOrEmpty(data.UploadFile))
                    {
                        var oldfilepath = data.UploadFile.Replace('/', '\\');
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                        RemoveExistingFile(FileSettings.SettingValue + oldfilepath);
                    }
                    if (file != null)
                        data.UploadFile = UploadPhoto(file, model);
                    Helper.ExecuteService("Forms", "UpdateForms", data);
                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateForms");
                }
                else
                {
                    return RedirectToAction("GetAllForms");
                }
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Forms");
                ViewBag.ErrorMessage = "Their is a Error While Saving the Forms Details";
                return View("AdminErrorPage");
            }


        }

        public ActionResult EditForms(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                tForms data = new tForms();
                //data.FormId = Id;
                data.FormId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString()));
                var serData = (tForms)Helper.ExecuteService("Forms", "GetFormsById", data);
                var reldata = serData.ToViewModel1("Edit");
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                
                var doctypeList = (List<tFormsTypeofDocuments>)Helper.ExecuteService("Forms", "GetFormsTypeofDocumentLst", null);
                var docList = new SelectList(doctypeList, "TypeofFormId", "TypeofFormName", null);
                reldata.AssemblyList = assmeblyList;
               
                reldata.FormDocumentTypeList = doctypeList;
                reldata.FormDocumentTypeList1 = docList;
                reldata.StatusTypeList = ModelMapping.GetStatusTypes();

                return View("CreateForms", reldata);
            }
            catch (Exception ex)
            {

                RecordError(ex, "Edit Forms");
                ViewBag.ErrorMessage = "Their is a Error While Editing Forms Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult DeleteForms(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                tForms data = new tForms();
                data.FormId = Id;
                

                var isSucess = (bool)Helper.ExecuteService("Forms", "DeleteFormsById", data);

                return RedirectToAction("GetAllForms");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Deleting Forms");
                ViewBag.ErrorMessage = "Their is a Error While Deleting the Forms Details";
                return View("AdminErrorPage");
            }



        }


        private string UploadPhoto(HttpPostedFileBase File, FormsViewModel model)
        {
            try
            {

                if (File != null)
                {
                    // var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    //var disFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);


                    // DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~/Images/News/"));//C:\DIS_FileStructure\Notices
                    //Code for Assembly Folder creation
                    DirectoryInfo AssemblyDir = new DirectoryInfo(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId);  //"C:\\DIS_FileStructure\\Notices");
                    //DirectoryInfo MDir = new DirectoryInfo(disFileSettings.SettingValue);
                    if (!AssemblyDir.Exists)
                    {
                        AssemblyDir.Create();
                    }

                   

                    //Code for Assembly Folder File Existense Check

                    var filepath = System.IO.Path.Combine(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VTypeofFormId + ".pdf");

                    if (System.IO.File.Exists(filepath))
                    {
                        // Remove Existence File

                        RemoveExistingFile(filepath);
                        File.SaveAs(filepath);

                        var path = "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VTypeofFormId + ".pdf";
                        return path.Replace('\\', '/');
                    }
                    else
                    {
                        File.SaveAs(filepath);

                        var path = "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VTypeofFormId + ".pdf";
                        return path.Replace('\\', '/');

                    }

                }
                


            }
            catch (Exception ex)
            {


                throw ex;
            }


            return null;


        }


        private void RemoveExistingFile(string OldFile)
        {

            try
            {

                               
                if (System.IO.File.Exists(OldFile))
                {
                    System.IO.File.Delete(OldFile);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        [HttpPost]
        public JsonResult CheckId(int FormTypeId)
        {

            tForms obj = new tForms();


            obj.TypeofFormId = FormTypeId;
            string st = Helper.ExecuteService("Forms", "CheckId", obj) as string;

            return Json(st, JsonRequestBehavior.AllowGet);
        }


    }
}
