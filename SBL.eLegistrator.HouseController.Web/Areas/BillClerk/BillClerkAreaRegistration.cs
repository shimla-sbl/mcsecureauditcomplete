﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.BillClerk
{
    public class BillClerkAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BillClerk";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "BillClerk_default",
                "BillClerk/{controller}/{action}/{id}",
                new { action = "Index", Controller = "DashBoardBillClerk", id = UrlParameter.Optional }
            );
        }
    }
}
