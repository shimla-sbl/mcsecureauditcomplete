﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.BillClerk.Controllers
{
    public class DashBoardBillClerkController : Controller
    {
        //
        // GET: /BillClerk/DashBoardBillClerk/

        public ActionResult Index()
        {
            CurrentSession.SetSessionAlive = null;
            return View();
        }

        public ActionResult TravelBills()
        {
            return View();
        }

        public ActionResult MedicalBills()
        {
            return View();
        }

        public ActionResult OtherBills()
        {
            return View();
        }

        public ActionResult RoadPermits()
        {
            return View();
        }

        public ActionResult ReportsOnRoadPermits()
        {
            return View();
        }

    }
}
