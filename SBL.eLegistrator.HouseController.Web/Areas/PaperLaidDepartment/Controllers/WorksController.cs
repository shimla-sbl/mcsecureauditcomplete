﻿using Newtonsoft.Json;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.ComplexModel.Constituency;
using SBL.DomainModel.Models.ConstituencyVS;
using SBL.DomainModel.Models.Grievance;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Constituency.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.eFile;
using EvoPdf;
using iTextSharp.text;
using System.Linq;
using SBL.DomainModel.Models.Assembly;
using System.Collections.Generic;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.ComplexModel.Conzstituency;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Controllers
{
    public class WorksController : Controller
    {
        //
        // GET: /PaperLaidDepartment/Works/

        public string myConsituency = CurrentSession.ConstituencyID;

        public string MemberCode = CurrentSession.MemberCode;
        public string currentUser = CurrentSession.UserID;
        public string aadharID = CurrentSession.AadharId;
        public string currentAssemby = CurrentSession.AssemblyId;

        //
        // GET: /Admin/Works/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UpdateWorks()
        {


            return View();

        }



        public ActionResult ViewWorks()
        {
            string PassesDeptmement = ""; string PassedOffice = ""; string DistrictCode = "";

            var methodParameter = new List<KeyValuePair<string, string>>();
            schemeMapping _scheme = new schemeMapping();
            var santioned1 = _scheme.mySchemeList;
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "7")
            {
                if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
                {
                    ViewBag.DeptID = CurrentSession.DeptID;
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    MlaDiary model1 = new MlaDiary();
                    model1.UserofcId = CurrentSession.OfficeId;
                    var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);

                    var Agency = CurrentSession.DeptID;
                    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
                    santioned1 = _scheme.mySchemeList.Where(x => Officelist.Any(y => y.OfficeId == Convert.ToInt16(x.ExecutiveOfficeId))).ToList();
                    methodParameter.Add(new KeyValuePair<string, string>("@Department", Agency));
                    methodParameter.Add(new KeyValuePair<string, string>("@OfficeId", model1.UserofcId));
                    PassesDeptmement = CurrentSession.DeptID;
                    PassedOffice = CurrentSession.OfficeId;
                    DistrictCode = CurrentSession.DistrictCode;
                }
            }


            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "2")
            {
                if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
                {
                    ViewBag.DeptID = CurrentSession.DeptID;
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    MlaDiary model1 = new MlaDiary();
                    var Agency = CurrentSession.DeptID;
                    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
                    santioned1 = _scheme.mySchemeList.ToList();
                    methodParameter.Add(new KeyValuePair<string, string>("@Department", Agency));
                    PassesDeptmement = CurrentSession.DeptID;
                    PassedOffice = CurrentSession.OfficeId;
                    DistrictCode = CurrentSession.DistrictCode;
                }
            }

            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "0" || SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "")
            {
                if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
                {
                    ViewBag.DeptID = CurrentSession.DeptID;
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    MlaDiary model1 = new MlaDiary();
                    //CurrentSession.DeptID = "0";
                    //CurrentSession.OfficeId = "0";
                    //CurrentSession.DistrictCode = "0";
                    var Agency = CurrentSession.DeptID;
                    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
                    methodParameter.Add(new KeyValuePair<string, string>("@Department", Agency));
                    santioned1 = _scheme.mySchemeList.ToList();
                    PassesDeptmement = CurrentSession.DeptID;
                    PassedOffice = "0";
                    DistrictCode = "0";
                }
            }
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "-1")
            {
                if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
                {
                    ViewBag.DeptID = CurrentSession.DeptID;
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    MlaDiary model1 = new MlaDiary();
                    //CurrentSession.DeptID = "0";
                    //CurrentSession.OfficeId = "0";
                    //CurrentSession.DistrictCode = "0";
                    var Agency = "0";
                    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
                    methodParameter.Add(new KeyValuePair<string, string>("@Department", null));
                    santioned1 = _scheme.mySchemeList.ToList();
                    PassesDeptmement = "0";
                    PassedOffice = "0";
                    DistrictCode = "0";
                }
            }


            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "1")
            {
                if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
                {
                    //ViewBag.DeptID = CurrentSession.DeptID;
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    MlaDiary model1 = new MlaDiary();
                    model1.UserofcId = CurrentSession.OfficeId;
                    var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);

                    var distt = CurrentSession.DistrictCode;

                    DiaryModel dobj = new DiaryModel();
                    dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                    dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                    dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
                    var Consttituency = Convert.ToString(dobj.ConstituencyCode);
                    CurrentSession.ConstituencyID = Consttituency;
                    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType2Count", new string[] { Consttituency });
                    santioned1 = _scheme.mySchemeList.ToList();
                    methodParameter.Add(new KeyValuePair<string, string>("@Consttituency", Consttituency));
                    PassesDeptmement = CurrentSession.DeptID;
                    PassedOffice = CurrentSession.OfficeId;
                    DistrictCode = CurrentSession.DistrictCode;

                }
            }



            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "8")
            {
                if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
                {
                    ViewBag.DeptID = CurrentSession.DeptID;
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    var distt = CurrentSession.DistrictCode;
                    MlaDiary model1 = new MlaDiary();
                    PassesDeptmement = "0";
                    PassedOffice = "0";
                    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByType1Count", new string[] { distt });
                    methodParameter.Add(new KeyValuePair<string, string>("@distt", distt));
                    santioned1 = _scheme.mySchemeList.ToList();
                    DistrictCode = CurrentSession.DistrictCode;
                }
            }


            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "9")
            {
                if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
                {
                    ViewBag.DeptID = CurrentSession.DeptID;
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    var SubDivisionId = CurrentSession.SubDivisionId;
                    MlaDiary model1 = new MlaDiary();
                    _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeSDMCount", new string[] { SubDivisionId });
                    methodParameter.Add(new KeyValuePair<string, string>("@SubDivisionId", SubDivisionId));
                    santioned1 = _scheme.mySchemeList.ToList();
                    PassesDeptmement = CurrentSession.DeptID;
                    PassedOffice = CurrentSession.OfficeId;
                    DistrictCode = CurrentSession.DistrictCode;
                }
            }


            DataSet dataSetUser = new DataSet();
            dataSetUser = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql",

            //"getAllLatestProgressImages", methodParameter);
            "getAllLatestProgressImagesMC", methodParameter);




            DataSet dataSetUser1 = new DataSet();
            dataSetUser1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "getAllLatestwork", methodParameter);
            List<ConstituencyWorkProgressImages> empList = new List<ConstituencyWorkProgressImages>();
            if (dataSetUser.Tables[0].Rows.Count > 0)
            {
                empList = dataSetUser.Tables[0].AsEnumerable().Select(dataRow => new ConstituencyWorkProgressImages
                {
                    filePath = dataRow.Field<string>("filePath"),
                    fileName = dataRow.Field<string>("fileName"),
                    tFileCaption = dataRow.Field<string>("workName"),
                    UploadedDate = dataRow.Field<DateTime>("UploadedDate"),
                    constituencyCode = dataRow.Field<Int32>("Number"),
                    schemeCode = dataRow.Field<long>("schemeCode")
                }).ToList();
            }
            var getAllLatestworkList = new List<myShemeList>();
            if (dataSetUser.Tables[0].Rows.Count > 0)
            {
                getAllLatestworkList = dataSetUser1.Tables[0].AsEnumerable().Select(dataRow => new myShemeList()
                {
                    workName = dataRow.Field<string>("workName"),
                    ExecutiveDepartment = dataRow.Field<string>("DepartmentName"),
                    ExecutiveOffice = dataRow.Field<string>("ExecutingAgency"),
                    workStatus = dataRow.IsNull("WorkStatus") ? "" : dataRow.Field<string>("WorkStatus"),
                    schemeTypeId = dataRow.IsNull("Physical Progress till Date") ? 0 : dataRow.Field<int>("Physical Progress till Date"),
                    physicalProgress = dataRow.IsNull("financial Progress") ? 0 : dataRow.Field<double>("financial Progress"),
                    sanctionedDate = dataRow.IsNull("LastUpdatedDate") ? "" : Convert.ToString(dataRow.Field<DateTime>("LastUpdatedDate")),
                }).ToList();
            }
            ViewBag.Images = empList;
            ViewBag.LatestUpdate = getAllLatestworkList;
            var santioned = santioned1.Where(x => x.workStatusCode == 1).Count();
            var Inprogress = santioned1.Where(x => x.workStatusCode == 2).Count();
            var Completed = santioned1.Where(x => x.workStatusCode == 3).Count();
            var PendingCourt = santioned1.Where(x => x.workStatusCode == 5).Count();
            var Canceled = santioned1.Where(x => x.workStatusCode == 4).Count();
            var PendingClear = santioned1.Where(x => x.workStatusCode == 6).Count();
            var PendingClearDPR = santioned1.Where(x => x.workStatusCode == 7).Count();

            return View(new string[] { PassedOffice, PassesDeptmement, CurrentSession.UserTypeConstituencyId, DistrictCode, Convert.ToString(santioned), Convert.ToString(Inprogress), Convert.ToString(Completed), Convert.ToString(PendingCourt), Convert.ToString(Canceled), Convert.ToString(PendingClear), Convert.ToString(PendingClearDPR) });
        }




        public ActionResult UpdateImagesbyOfficer(ConstituencyWorkProgressImages images, string FileCaption)
        {
            string pathString = "";
            string TemppathString = "";

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            // string directory = FileSettings.SettingValue + SavedPdfPath;
            string url = "\\constituencyImage\\" + currentAssemby + "\\" + images.schemeCode + "\\";

            string path = FileSettings.SettingValue + url;
            var originalDirectory = new DirectoryInfo(string.Format("{0}", path));
            pathString = System.IO.Path.Combine(originalDirectory.ToString(), "Full");
            bool isExists = System.IO.Directory.Exists(pathString);

            if (!isExists)
                System.IO.Directory.CreateDirectory(pathString);

            var tempDir = new DirectoryInfo(string.Format("{0}tempimg\\", Server.MapPath(@"\")));
            TemppathString = System.IO.Path.Combine(tempDir.ToString(), "conimg");
            ResizeImage(600, TemppathString + "/" + images.tempPath, pathString + "\\" + images.tempPath);

            pathString = System.IO.Path.Combine(originalDirectory.ToString(), "thumb");
            isExists = System.IO.Directory.Exists(pathString);
            if (!isExists)
                System.IO.Directory.CreateDirectory(pathString);
            ResizeImage(150, TemppathString + "/" + images.tempPath, pathString + "\\" + images.tempPath);
            isExists = System.IO.File.Exists(TemppathString + "/" + images.tempPath);
            if (!isExists)
                System.IO.File.Delete(pathString);
            images.fileName = images.tempPath;
            images.thumbFilePath = url + "thumb";
            images.filePath = url + "Full"; ;
            images.createdBY = currentUser;
            images.createdDate = DateTime.Now;
            images.isActive = true;
            images.isDeleted = false;
            // images.
            WorkDetails _scheme = (WorkDetails)Helper.ExecuteService("ConstituencyVS", "getWorkDetailsByID", images.schemeCode);
            images.workCode = _scheme.workCode;
            images.constituencyCode = _scheme.eConstituencyID.Value;
            images.UploadedDate = DateTime.Now;
            Helper.ExecuteService("ConstituencyVS", "UpdateImagesbyOfficer", images);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateProgressbyOfficer(ConstituencyWorkProgress progress)
        {
            WorkDetails _scheme = (WorkDetails)Helper.ExecuteService("ConstituencyVS", "getWorkDetailsByID", progress.schemeCode);
            progress.workCode = _scheme.workCode;
            progress.constituencyCode = _scheme.eConstituencyID.Value;
            progress.submitDate = DateTime.Now;
            progress.createdBY = currentUser;
            progress.createdDate = DateTime.Now;
            progress.isActive = true;
            progress.isDeleted = false;

            Helper.ExecuteService("ConstituencyVS", "UpdateProgressbyOfficer", progress);
            return Json(progress.schemeCode, JsonRequestBehavior.AllowGet);
        }

        public ActionResult updateWorksbyMembers(WorkDetails work, HttpPostedFileBase photo, HttpPostedFileBase photo1)
        {
            string checkedData = "";
            string Statecode = Helper.ExecuteService("SiteSetting", "Get_StateCode", null) as string;
            work.StateCode = Convert.ToInt32(Statecode);
            work.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
            }


            //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            //string url = "/constituencyDPR";
            //string directory = FileSettings.SettingValue + url;

            //if (!System.IO.Directory.Exists(directory))
            //{
            //    System.IO.Directory.CreateDirectory(directory);
            //}
            //int fileID = GetFileRandomNo();

            //string url1 = "~/constituencyDPR";
            //string directory1 = Server.MapPath(url1);

            //if (Directory.Exists(directory))
            //{
            //    string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
            //    if (savedFileName.Length > 0)
            //    {
            //        string SourceFile = savedFileName[0];
            //        foreach (string page in savedFileName)
            //        {
            //            Guid FileName = Guid.NewGuid();

            //            string name = Path.GetFileName(page);

            //            string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


            //            if (!string.IsNullOrEmpty(name))
            //            {
            //                if (!string.IsNullOrEmpty(work.dprFile))
            //                    System.IO.File.Delete(System.IO.Path.Combine(directory, work.dprFile));
            //                System.IO.File.Copy(SourceFile, path, true);
            //                work.dprFile = "/constituencyDPR/" + FileName + "_" + name.Replace(" ", "");
            //            }

            //        }

            //    }
            //    else
            //    {
            //        return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
            //    }

            //}
            //foreach (var item in photo)
            //{
            //    var s = item.FileName;
            //}



#pragma warning disable CS0219 // The variable 'isSavedSuccessfully' is assigned but its value is never used
            bool isSavedSuccessfully = true;
#pragma warning restore CS0219 // The variable 'isSavedSuccessfully' is assigned but its value is never used
            string fName = "";
            string pathString = "";
            string NFileName = "";

            string fName1 = "";
            string pathString1 = "";
            string NFileName1 = "";

            if (photo != null)
            {
                //Save file content goes here
                fName = photo.FileName;
                if (photo != null && photo.ContentLength > 0)
                {


                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    // string directory = FileSettings.SettingValue + SavedPdfPath;
                    string url = "\\constituencyDPR\\" + currentAssemby + "\\" + work.workCode + "\\";
                    pathString = FileSettings.SettingValue + url;

                    var fileName1 = Path.GetFileName(photo.FileName);
                    string ext = Path.GetExtension(photo.FileName);
                    NFileName = Guid.NewGuid().ToString() + ext;

                    bool isExists = System.IO.Directory.Exists(pathString);

                    if (!isExists)
                        System.IO.Directory.CreateDirectory(pathString);

                    var path = string.Format("{0}\\{1}", pathString, NFileName);
                    photo.SaveAs(path);
                    work.dprFile = url + NFileName;
                }
            }
            if (photo1 != null)
            {
                //Save file content goes here
                fName1 = photo1.FileName;
                if (photo1 != null && photo1.ContentLength > 0)
                {


                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    // string directory = FileSettings.SettingValue + SavedPdfPath;
                    string url = "\\salientfeatures\\" + currentAssemby + "\\" + work.workCode + "\\";
                    pathString1 = FileSettings.SettingValue + url;

                    var fileName1 = Path.GetFileName(photo1.FileName);
                    string ext = Path.GetExtension(photo1.FileName);
                    NFileName1 = Guid.NewGuid().ToString() + ext;

                    bool isExists = System.IO.Directory.Exists(pathString1);

                    if (!isExists)
                        System.IO.Directory.CreateDirectory(pathString1);

                    var path = string.Format("{0}\\{1}", pathString1, NFileName1);
                    photo1.SaveAs(path);
                    work.featuresfile = url + NFileName1;
                }
            }
            work.userid = currentUser;
            //if (work.eConstituencyID == null)
            //{
            //    work.eConstituencyID = Convert.ToInt64(work.ConId_ForSDM_);
            //}
            //  work.eConstituencyID = Convert.ToInt64(work.ConId_ForSDM_);
            work.memberCode = string.IsNullOrEmpty(MemberCode) ? work.memberCode : Convert.ToInt32(MemberCode);
            string membercode = "";
            if (work.memberCode == null)
            {
                work.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
                membercode = Helper.ExecuteService("ConstituencyVS", "getMemberCodeByConstituency", work) as string;
                work.memberCode = Convert.ToInt32(membercode);
            }

            work.isMember = string.IsNullOrEmpty(CurrentSession.IsMember) ? false : Convert.ToBoolean(CurrentSession.IsMember);
            //  work.WorkTypeID = "work";
            if (work.WorkTypeID.Contains("work") || (work.programName.Contains("program")))
            {
                return Json("Please Wait To Fill All Input field", JsonRequestBehavior.AllowGet);
            };
            if (work.Mode == "New")
            {
                checkedData = Helper.ExecuteService("ConstituencyVS", "CheckWorkCode_ByCID", work) as string;
                if (checkedData != "N")
                {
                    checkedData = "Y";
                }
                else
                {
                    Helper.ExecuteService("ConstituencyVS", "CreateNewWorkbyMembers", work);


                }
                // Helper.ExecuteService("ConstituencyVS", "CreateNewWorkbyMembers", work);
            }
            else
            {
                Helper.ExecuteService("ConstituencyVS", "updateWorksbyMembers", work);
            }

            return Json(checkedData, JsonRequestBehavior.AllowGet); ;
        }
        public static void ResizeImage(int size, string filePath, string saveFilePath)
        {
            //variables for image dimension/scale
            double newHeight = 0;
            double newWidth = 0;
            double scale = 0;

            //create new image object
            Bitmap curImage = new Bitmap(filePath);

            //Determine image scaling
            if (curImage.Height > curImage.Width)
            {
                scale = Convert.ToSingle(size) / curImage.Height;
            }
            else
            {
                scale = Convert.ToSingle(size) / curImage.Width;
            }
            if (scale < 0 || scale > 1) { scale = 1; }

            //New image dimension
            newHeight = Math.Floor(Convert.ToSingle(curImage.Height) * scale);
            newWidth = Math.Floor(Convert.ToSingle(curImage.Width) * scale);

            //Create new object image
            Bitmap newImage = new Bitmap(curImage, Convert.ToInt32(newWidth), Convert.ToInt32(newHeight));
            Graphics imgDest = Graphics.FromImage(newImage);
            imgDest.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            imgDest.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            imgDest.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            imgDest.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            ImageCodecInfo[] info = ImageCodecInfo.GetImageEncoders();
            EncoderParameters param = new EncoderParameters(1);
            param.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);

            //Draw the object image
            imgDest.DrawImage(curImage, 0, 0, newImage.Width, newImage.Height);

            //Save image file
            newImage.Save(saveFilePath, info[1], param);

            //Dispose the image objects
            curImage.Dispose();
            newImage.Dispose();
            imgDest.Dispose();
        }
    }
}