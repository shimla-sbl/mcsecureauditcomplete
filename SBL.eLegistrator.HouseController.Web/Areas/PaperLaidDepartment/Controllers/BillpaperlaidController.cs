﻿using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.PaperLaid;
using SBL.eLegistrator.HouseController.Web.Filters;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Bill;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.SiteSetting;
using System.Globalization;
using Lib.Web.Mvc.JQuery.JqGrid;
using System.Linq;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Filters;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.LOB;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class BillpaperlaidController : Controller
    {
        //
        // GET: /PaperLaidDepartment/BillPaperLaid/
        public ActionResult Index()
        {
            return View();
        }

        #region SrinivasaCode

        /// <summary>
        /// Author: Srinivasa
        /// 
        /// GetDraftBills method is mainly used for getting DraftBills.
        /// Database tables involved in getting this DraftBills are
        /// 1.paperLaidVS 
        /// 
        /// Inputs:
        /// 1) AssemblyId
        /// 2) SessionId
        /// 
        /// Conditions when we are getting data are
        /// 1) Check for eventId to be as Bill.
        /// 2) Check for DeptActiveId as Null.
        /// 3) Check for AssemblyId and sessionId of the existing year.
        /// 
        /// OutPut:
        /// This method returns a list of paperLaidVS objects.
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="assemblyId"></param>
        /// <returns></returns>
        //Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList();
        public ActionResult GetBillSentRecordById(int paperLaidId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV mdl = new tPaperLaidV();
                mdl.PaperLaidId = Convert.ToInt64(paperLaidId);
                mdl.DepartmentId = CurrentSession.DeptID;
                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "ShowSentBillById", mdl);
                return PartialView("_ShowBillSentDetailsByID", mdl);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PaperLaidSubmitt(HttpPostedFileBase file,HttpPostedFileBase DocFile, tPaperLaidV obj, string submitButton)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV mdl = new tPaperLaidV();
                tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    mdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    mdl.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                //mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", mdl);


                //if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                //{
                //    mdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                //}

                //if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                //{
                //    mdl.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                //}
                submitButton = "Save";
                if (submitButton == "Save")
                {
                    switch (submitButton)
                    {
                        case "Send":
                            {
                                tPaperLaidV billDetails = new tPaperLaidV();
                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                                billDetails = obj;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                                mdl.EventId = obj.EventId;
                                // int paperLaidId = (int)mdl.PaperLaidId;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetFileSaveDetails", mdl);

                                string categoryType = mdl.RespectivePaper;
                                if (file != null)
                                {
                                    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                    //string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + mdl.RespectivePaper + "/";
                                    string Url = "/PaperLaid/" + mdl.AssemblyId + "/" + mdl.SessionId + "/";
                                    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                    if (!Dir.Exists)
                                    {
                                        Dir.Create();
                                    }

                                    var ver = obj.FileVersion.GetValueOrDefault() + 1;//uday updated
                                    string ext = Path.GetExtension(file.FileName);
                                    string fileName = file.FileName.Replace(ext, "");
                                    string actualFileName = mdl.AssemblyId + "_" + mdl.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + ver + ext;
                                    file.SaveAs(Server.MapPath(Url + actualFileName));
                                    mdlTemp.FileName = actualFileName;
                                    mdlTemp.FilePath = Url;
                                    mdlTemp.Version = obj.FileVersion + 1;
                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    mdlTemp.DeptSubmittedDate = DateTime.Now;
                                    mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                    mdlTemp.AssemblyId = CurrentSession.AssemblyId;
                                    mdlTemp.SessionId = CurrentSession.SessionId;
                                }
                                else
                                {
                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    mdlTemp.DeptSubmittedDate = DateTime.Now;
                                    mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                    mdlTemp.AssemblyId = CurrentSession.AssemblyId;
                                    mdlTemp.SessionId = CurrentSession.SessionId;
                                }

                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                                //if (categoryType.IndexOf("bill", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                                //  {
                                billDetails.PaperLaidId = mdl.PaperLaidId;
                                billDetails.AssemblyId = mdl.AssemblyId;
                                billDetails.SessionId = mdl.SessionId;
                                billDetails.BillId = obj.BillId;
                                if (obj.BillTypeId == 1)
                                {

                                }
                                tBillRegister bills = new tBillRegister();
                                if (obj.BillTypeId == 1)
                                {
                                    billDetails.IsAmendment = false;
                                }
                                else
                                {
                                    billDetails.IsAmendment = true;
                                }
                                bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "InsertBillDetails", billDetails);
                                //  }

                                mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                                mdl.ModifiedBy = CurrentSession.UserName;
                                mdl.ModifiedWhen = DateTime.Now;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntryForBilReplace", mdl);
                                //SendBillPaperByDept(mdl);
                                break;
                            }
                        case "Update":
                            {
                                obj.tpaperLaidTempId = 0;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);
                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", mdl);
                                if (file != null)
                                {
                                    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                    //string Url = "~/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + mdl.RespectivePaper + "/";
                                    string Url = mdlTemp.FilePath;
                                    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                    if (!Dir.Exists)
                                    {
                                        Dir.Create();
                                    }
                                    var ver = obj.FileVersion.GetValueOrDefault();
                                    string ext = Path.GetExtension(file.FileName);
                                    string fileName = file.FileName.Replace(ext, "");
                                    string actualFileName = mdl.AssemblyId + "_" + mdl.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + ver + ext;
                                    file.SaveAs(Server.MapPath(Url + actualFileName));
                                    mdlTemp.FileName = actualFileName;
                                }

                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaid", mdlTemp);
                                tBillRegister billDetails = new tBillRegister();
                                billDetails.PaperLaidId = obj.PaperLaidId;
                                billDetails.BillTypeId = obj.BillTypeId;
                                billDetails.BillNumber = obj.BOTStatus.ToString() + " of " + obj.BillNumberYear.ToString();
                                billDetails.ShortTitle = obj.Description;
                                billDetails.MinisterId = obj.MinistryId;
                                billDetails.BillId = obj.BillId;
                                CultureInfo provider = CultureInfo.InvariantCulture;
                                //provider = new CultureInfo("en-US");
                                //DateTime dart = new DateTime();
                                string[] formats = { "dd/MM/yyyy","dd-MM-yyyy", "dd/M/yyyy","dd-M-yyyy", "d/M/yyyy","d-M-yyyy", "d/MM/yyyy","d-MM-yyyy","MM/dd/yyyy","MM-dd-yyyy","MM/dd/yy","MM-dd-yy",
                    "dd/MM/yy","dd-MM-yy", "dd/M/yy","dd-M-yy","d/M/yy","d-M-yy","d/MM/yy","d-MM-yy"};
                                //bool fsdf = DateTime.TryParseExact(obj.BillDate,"d",provider,DateTimeStyles.AllowWhiteSpaces,out dart);
                                var datetstring = obj.BillDate.Split(' ')[0];
                                billDetails.BillDate = string.IsNullOrEmpty(obj.BillDate) ? (DateTime?)null : DateTime.ParseExact(datetstring, formats, CultureInfo.InvariantCulture, DateTimeStyles.None); //DateTime.ParseExact(obj.BillDate, "d", provider);

                                tBillRegister bills = new tBillRegister();
                                bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "UpdateBillDetails", billDetails);

                                mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                                mdl.ModifiedBy = CurrentSession.UserName;
                                mdl.ModifiedWhen = DateTime.Now;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntryForBilReplace", mdl);

                                break;
                            }
                        case "Save":
                            {

                                tPaperLaidV billDetails = new tPaperLaidV();

                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                                billDetails = obj;

                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                                mdl.EventId = obj.EventId;
                                int paperLaidId = (int)mdl.PaperLaidId;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetFileSaveDetails", mdl);

                                //tPaperLaidV versioncount = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetFileVersion", obj);
                                string categoryType = mdl.RespectivePaper;


                                if (file == null)
                                {
                                    string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.FileName = name;
                                        }
                                        string ext = Path.GetExtension(mdlTemp.FileName);
                                        string fileNam1 = mdlTemp.FileName.Replace(ext, "");


                                        var vers = obj.FileVersion.GetValueOrDefault() + 1;


                                        string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + vers + ext;


                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);
                                        string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.FileName = actualFileName;
                                        mdlTemp.Version = vers;

                                    }
                                    else
                                    {
                                        mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                        mdlTemp.Version = mdl.Count;
                                        mdlTemp.FileName = mdl.FileName;
                                        mdlTemp.DocFileName = mdl.DocFileName;
                                        mdlTemp.FilePath = mdl.FilePath;
                                    }
                                   

                                }

                                else
                                {
                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    mdlTemp.DeptSubmittedDate = DateTime.Now;
                                    mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                }


                                if (DocFile == null)
                                {

                                    string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.DocFileName = name;
                                        }

                                        string ext = Path.GetExtension(mdlTemp.DocFileName);

                                        var DocVer = obj.DocFileVersion.GetValueOrDefault() + 1;


                                        string fileName = mdlTemp.DocFileName.Replace(ext, "");


                                        string actualDocFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + DocVer + ext;



                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualDocFileName);
                                        //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.DocFileName = actualDocFileName;
                                        mdlTemp.DocFileVersion = DocVer;
                                    }

                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    mdlTemp.DocFileVersion = obj.MainDocCount;

                                }

                                mdlTemp.AssemblyId = CurrentSession.AssemblyId;
                                mdlTemp.SessionId = CurrentSession.SessionId;
                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                                //if (categoryType.IndexOf("bill", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                                //{
                                billDetails.PaperLaidId = mdl.PaperLaidId;
                                billDetails.AssemblyCode = mdl.AssemblyId;
                                billDetails.SessionCode = mdl.SessionId;
                                billDetails.BillId = obj.BillId;



                                tBillRegister bills = new tBillRegister();
                                bills.BillName = mdl.Title;
                                bills.PaperLaidId = obj.PaperLaidId;
                                bills.AssemblyId = mdl.AssemblyId;
                                bills.SessionId = mdl.SessionId;
                                bills.BillId = obj.BillId;
                                bills.MinisterId = obj.MinistryId;

                                bills.ShortTitle = obj.Title;

                                bills.BillNo = obj.BillNUmberValue;
                                bills.BillYear = obj.BillNumberYear;

                                //bills.BillDate = obj.BillDate;
                                bills.BillTypeId = obj.BillTypeId;




                                if (obj.BillTypeId == 1)
                                {
                                    billDetails.IsAmendment = false;
                                }
                                else
                                {
                                    bills.IsAmendment = true;
                                }
                                if (obj.BillTypeId == 1)
                                {
                                    bills.IsAmendment = false;
                                }
                                else
                                {
                                    billDetails.IsAmendment = true;
                                }
                                if (bills.PaperLaidId != 0 && bills.BillId != 0)
                                {
                                    var data = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "UpdateBillDetails", bills);

                                    // bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "UpdateBillDetails", billDetails);
                                }
                                else
                                {
                                    bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "InsertBillDetails", billDetails);
                                }


                                //}


                                mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;

                                mdl.ModifiedBy = CurrentSession.UserName;
                                mdl.ModifiedWhen = DateTime.Now;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntryForBilReplace", mdl);


                                string url1 = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                                string MainPDFdirectory1 = Server.MapPath(url1);
                                if (Directory.Exists(MainPDFdirectory1))
                                {
                                    System.IO.Directory.Delete(MainPDFdirectory1, true);
                                }
                                string urlMainDoc = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                                string MainDocdirectory = Server.MapPath(urlMainDoc);
                                if (Directory.Exists(MainDocdirectory))
                                {
                                    System.IO.Directory.Delete(MainDocdirectory, true);
                                }


                                break;
                            }
                        case "Update Send":
                            {
                                // obj.tpaperLaidTempId = 0;

                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", obj);
                                obj.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);

                                if (file != null)
                                {
                                    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                    //  string Url = "~/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + mdl.RespectivePaper + "/";
                                    string Url = mdlTemp.FilePath;
                                    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                    if (!Dir.Exists)
                                    {
                                        Dir.Create();
                                    }
                                    var ver = obj.FileVersion.GetValueOrDefault();
                                    string ext = Path.GetExtension(file.FileName);
                                    string fileName = file.FileName.Replace(ext, "");
                                    string actualFileName = mdl.AssemblyId + "_" + mdl.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + ver + ext;
                                    file.SaveAs(Server.MapPath(Url + actualFileName));
                                    mdlTemp.FileName = actualFileName;

                                }

                                mdlTemp.DeptSubmittedDate = DateTime.Now;
                                mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaid", mdlTemp);

                                tBillRegister billDetails = new tBillRegister();
                                billDetails.PaperLaidId = obj.PaperLaidId;
                                billDetails.BillTypeId = obj.BillTypeId;
                                billDetails.PreviousBillId = obj.BillId;
                                billDetails.BillNumber = obj.BillNUmberValue.ToString() + " of " + obj.BillNumberYear.ToString();
                                string[] formats = { "dd/MM/yyyy","dd-MM-yyyy", "dd/M/yyyy","dd-M-yyyy", "d/M/yyyy","d-M-yyyy", "d/MM/yyyy","d-MM-yyyy","MM/dd/yyyy","MM-dd-yyyy","MM/dd/yy","MM-dd-yy",
                    "dd/MM/yy","dd-MM-yy", "dd/M/yy","dd-M-yy","d/M/yy","d-M-yy","d/MM/yy","d-MM-yy"};
                                //bool fsdf = DateTime.TryParseExact(obj.BillDate,"d",provider,DateTimeStyles.AllowWhiteSpaces,out dart);
                                var datetstring = obj.BillDate.Split(' ')[0];
                                billDetails.BillDate = string.IsNullOrEmpty(obj.BillDate) ? (DateTime?)null : DateTime.ParseExact(datetstring, formats, CultureInfo.InvariantCulture, DateTimeStyles.None); //DateTime.ParseExact(obj.BillDate, "d", provider);
                                billDetails.ShortTitle = obj.Title;
                                billDetails.MinisterId = obj.MinistryId;
                                billDetails.SessionId = mdl.SessionId;
                                billDetails.AssemblyId = mdl.AssemblyId;
                                billDetails.BillId = obj.BillId;
                                tBillRegister bills = new tBillRegister();
                                if (obj.BillTypeId == 1)
                                {
                                    billDetails.IsAmendment = false;
                                }
                                else
                                {
                                    billDetails.IsAmendment = true;
                                }
                                bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "UpdateBillDetails", billDetails);
                                break;
                            }


                    }
                }

                if (Request.IsAjaxRequest())
                {
                    tPaperLaidV mod = new tPaperLaidV();

                    return Json(mod, JsonRequestBehavior.AllowGet);
                }
                else
                {


                }
                return null;

                //TempData["lSM"] = "BLL";
                //TempData["Msg"] = "Save";
                //return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment"); //RedirectToAction("DepartmentDashboard");
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        //public ActionResult ShowBillSentDetailsByID(int paperLaidId)
        //{

        //    tPaperLaidV mdl = new tPaperLaidV();
        //    mdl.PaperLaidId = Convert.ToInt64(paperLaidId);
        //    mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "ShowDraftBillById", mdl);
        //    return View();
        //}

        public ActionResult ShowDraftBillById(int paperLaidId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV mdl = new tPaperLaidV();
                mdl.PaperLaidId = Convert.ToInt64(paperLaidId);
                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "ShowDraftBillById", mdl);
                mEvent mEventModel = new mEvent();

                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    mdl.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", mdl);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        mdl.DepartmentId = item.AssociatedDepts;
                        mdl.IsPermissions = item.IsPermissions;
                    }
                }
                else
                {
                    mdl.DepartmentId = CurrentSession.DeptID;
                    mdl.IsPermissions = true;
                }

                mdl.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryByDepartmentIds", mdl);

                mDepartment mdep = new mDepartment();
                mdep.deptId = mdl.DepartmentId;
                mdl.mDepartment = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);

                if (paperLaidId != 0)
                {
                    mdl.PaperLaidId = paperLaidId;
                    string dptId = (string)Helper.ExecuteService("PaperLaid", "getDepartmentIdByPaperId", mdl);
                    if (dptId != "")
                    {
                        mdl.DepartmentId = dptId;
                    }
                    else
                    {
                        mdl.DepartmentId = CurrentSession.DeptID;
                    }
                }

                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                mdl.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", mdl);

                mdl.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("BillPaperLaid", "GetDocumentType", mdl);
                mdl.tBillAmendment = (ICollection<tBillRegister>)Helper.ExecuteService("BillPaperLaid", "GetAmendmentBills", mdl);

                List<mEvent> events = (List<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
                mdl.mEvents = events;
                //.FindAll(a => a.EventId == 5 || a.EventId == 6);

                mdl.mBillTypes = (List<mBillType>)Helper.ExecuteService("BillPaperLaid", "GetBillTypes", "Testdata");

                foreach (var item in mdl.mBillTypes)
                {
                    mdl.BillTypeId = item.BillTypeId;
                }

                List<SelectListItem> years = new List<SelectListItem>();
                int currentYear = DateTime.Now.Year;
                for (int i = currentYear - 5; i < currentYear; i++)
                {
                    SelectListItem year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };
                    years.Add(year);
                }

                for (int i = currentYear; i < currentYear + 5; i++)
                {
                    SelectListItem year = new SelectListItem();
                    if (i == DateTime.Now.Year)
                    {
                        year = new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = true };
                    }
                    else
                    {
                        year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };
                    }
                    years.Add(year);
                }

                //ICollection<int> yearList = new List<int>();

                //for (int i = currentYear - 5; i < currentYear; i++)
                //{
                //    yearList.Add(i);
                //}
                //for (int i = currentYear; i < currentYear + 5; i++)
                //{
                //    yearList.Add(i);
                //}
                mdl.yearList = years;
                //mdl.BillNumberYear = mdl;

                return PartialView("_CreateNewBillDraft", mdl);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        public ActionResult CreateNewBillDraft(int sessionId, int assemblyId)
        {
            tPaperLaidV parameter = new tPaperLaidV();
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return base.RedirectToAction("LoginUP", "Account");
            }
            mEvent event2 = new mEvent();
            if ((CurrentSession.UserID != null) && (CurrentSession.UserID != ""))
            {
                parameter.UserID = new Guid(CurrentSession.UserID);
            }
            List<AuthorisedEmployee> source = new List<AuthorisedEmployee>();
            source = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", parameter);
            if ((source == null) || (source.Count<AuthorisedEmployee>() == 0))
            {
                parameter.DepartmentId = CurrentSession.DeptID;
                parameter.IsPermissions = true;
            }
            else
            {
                foreach (AuthorisedEmployee employee in source)
                {
                    parameter.DepartmentId = employee.AssociatedDepts;
                    parameter.IsPermissions = employee.IsPermissions;
                }
            }
            mDepartment department = new mDepartment
            {
                deptId = parameter.DepartmentId
            };
            parameter.mDepartment = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", department);
            tPaperLaidV dv2 = new tPaperLaidV
            {
                DepartmentId = parameter.DepartmentId
            };
            parameter.SessionId = int.Parse(CurrentSession.SessionId);
            parameter.AssemblyCode = int.Parse(CurrentSession.AssemblyId);
            parameter.AssemblyId = int.Parse(CurrentSession.AssemblyId);
            parameter.SessionCode = int.Parse(CurrentSession.SessionId);
            mMinisteryMinisterModel model1 = new mMinisteryMinisterModel();
            parameter.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", parameter);
            parameter.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("BillPaperLaid", "GetDocumentType", parameter);
            parameter.tBillAmendment = (ICollection<tBillRegister>)Helper.ExecuteService("BillPaperLaid", "GetAmendmentBills", parameter);
            parameter.mEvents = (List<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", event2);
            parameter.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryByDepartmentIds", dv2);
            parameter.mBillTypes = (List<mBillType>)Helper.ExecuteService("BillPaperLaid", "GetBillTypes", "Testdata");
            foreach (mBillType type in parameter.mBillTypes)
            {
                parameter.BillTypeId = type.BillTypeId;
            }
            List<SelectListItem> list3 = new List<SelectListItem>();
            int year = DateTime.Now.Year;
            for (int i = year - 5; i < year; i++)
            {
                SelectListItem item1 = new SelectListItem();
                item1.Text = i.ToString();
                item1.Value = i.ToString();
                SelectListItem item = item1;
                list3.Add(item);
            }
            for (int j = year; j < (year + 5); j++)
            {
                SelectListItem item = new SelectListItem();
                DateTime now = DateTime.Now;
                if (j != now.Year)
                {
                    SelectListItem item4 = new SelectListItem();
                    item4.Text = j.ToString();
                    item4.Value = j.ToString();
                    item = item4;
                }
                else
                {
                    SelectListItem item3 = new SelectListItem();
                    item3.Text = j.ToString();
                    item3.Value = j.ToString();
                    item3.Selected = true;
                    item = item3;
                }
                list3.Add(item);
            }
            parameter.yearList = list3;
            parameter.BillNumberYear = DateTime.Now.Year;
            return this.PartialView("_CreateNewBillDraft", parameter);
        }



        public ActionResult CheckBillNumber(string number, string year)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tBillRegister mdl = new tBillRegister();
                string billNumber = number + " of " + year;
                mdl = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "CheckBillNUmber", billNumber);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        public ActionResult GetBillSentByID(string paperLaidId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV movementModel = new tPaperLaidV();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetSubmittedPaperLaidByID", movementModel);
                return PartialView("_GetBillSentByID", movementModel);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateBillPaperLaidFile(HttpPostedFileBase file, tPaperLaidV tPaper)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidTemp tPaperTemp = new tPaperLaidTemp();
                tPaperLaidV obj = new tPaperLaidV();
                tPaper = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", tPaper);

                if (file == null)
                {

                    string url = "~/DepartmentPdf/MainReplyPdf"+ "/" + CurrentSession.UserID;
                    string directory = Server.MapPath(url);
                    if (Directory.Exists(directory))
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            string name = Path.GetFileName(page);
                            string nameKey = Path.GetFileNameWithoutExtension(page);
                            string directory1 = Path.GetDirectoryName(page);
                            //
                            // Display the Path strings we extracted.
                            //
                            Console.WriteLine("{0}, {1}, {2}, {3}",
                            page, name, nameKey, directory1);
                            tPaperTemp.FileName = name;
                        }
                        string ext = Path.GetExtension(tPaperTemp.FileName);
                        string fileNam1 = tPaperTemp.FileName.Replace(ext, "");



                        tPaper.Count = tPaper.Count;
                        //var vers = tPaper.FileVersion.GetValueOrDefault() + 1;


                        string actualFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + "B" + "_" + tPaper.PaperLaidId + "_V" + tPaper.Count + ext;



                        // string actualFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + QType + "_" + QuestionNo + "_V" + tPaper.Count + ext;



                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                        string Url = FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";
                        DirectoryInfo Dir = new DirectoryInfo(Url);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }

                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/", actualFileName);
                        string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                        System.IO.File.Copy(SourceFile, path, true);
                        tPaperTemp.FilePath = "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";

                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                        //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                        string FileStructurePath = Acess.SettingValue;
                        tPaperTemp.FileStructurePath = FileStructurePath;
                        tPaperTemp.FileName = actualFileName;

                        // tPaperTemp.Version = tPaper.Count;
                        tPaperTemp.PaperLaidId = tPaper.PaperLaidId;

                        tPaperTemp.Version = tPaper.Count;


                    }

                    tPaperTemp.Version = tPaper.Count;

                }

                tPaperTemp.AssemblyId = CurrentSession.AssemblyId;
                tPaperTemp.SessionId = CurrentSession.SessionId;

                tPaperLaidTemp mdlTemp1 = new tPaperLaidTemp();
                mdlTemp1 = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", tPaper);
                tPaperTemp.DocFileName = mdlTemp1.DocFileName;

                tPaperTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateBillPaperLaidFileByID", tPaperTemp);
                tPaper.DeptActivePaperId = tPaperTemp.PaperLaidTempId;

                tPaper.ModifiedBy = CurrentSession.UserName;
                tPaper.ModifiedWhen = DateTime.Now;
                tPaper = (tPaperLaidV)Helper.ExecuteService("Notice", "UpdatePaperLaidEntryForNoticeReplace", tPaper);



                if (Request.IsAjaxRequest())
                {
                    tPaperLaidV mod = new tPaperLaidV();

                    return Json(mod, JsonRequestBehavior.AllowGet);
                }
                else
                {


                }
                return null;

                //TempData["lSM"] = "BLL";
                //TempData["Msg"] = "File";
                //return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment");
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        //public tPaperLaidTemp SaveFileToDirectory(tQuestion tQuestions, HttpPostedFileBase file, tPaperLaidTemp mdlTemp, tPaperLaidV mdl, tPaperLaidV obj)
        //{
        //    string QType = string.Empty;

        //    if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
        //    {
        //        QType = "S";
        //    }
        //    else if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
        //    {
        //        QType = "US";
        //    }
        //    string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
        //    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
        //    if (!Dir.Exists)
        //    {
        //        Dir.Create();
        //    }
        //    string ext = Path.GetExtension(file.FileName);
        //    string fileName = file.FileName.Replace(ext, "");
        //    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + tQuestions.QuestionNumber + "_V" + mdl.Count + ext;
        //    //+ mdl.Count 
        //    file.SaveAs(Server.MapPath(Url + actualFileName));
        //    mdlTemp.FileName = actualFileName;
        //    mdlTemp.FilePath = Url;
        //    mdlTemp.Version = mdl.Count;
        //    mdlTemp.PaperLaidId = mdl.PaperLaidId;

        //    return mdlTemp;
        //}
        #endregion

        //By Ram
        [HttpPost]
        public ActionResult GetAllCurrentBills(int sessionId, int assemblyId, int PageNumber, int RowsPerPage, int loopStart, int loopEnd)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                //obj.AssemblyId = siteSettingMod.AssemblyCode;
                //obj.SessionId = siteSettingMod.SessionCode;
                //obj.loopEnd = loopEnd;
                //obj.loopStart = loopStart;
                //obj.RowsPerPage = RowsPerPage;
                //obj.PageNumber = PageNumber;
                //obj.PageIndex = PageNumber;
                //obj.PAGE_SIZE = RowsPerPage;
                //obj.selectedPage = PageNumber;
                //obj.DepartmentId = CurrentSession.DeptID;
                //obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetAllCurrentBillsList", obj);
                return PartialView("_GetAllCurrentBills", obj);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetAllCurrentBillsJqGrid(JqGridRequest request)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                //obj = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", obj);


                //if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
                //{
                //    CurrentSession.AssemblyId = obj.AssemblyId.ToString();
                //}

                //if (string.IsNullOrEmpty(CurrentSession.SessionId))
                //{
                //    CurrentSession.SessionId = obj.SessionId.ToString();
                //}
                obj.PAGE_SIZE = request.RecordsCount;
                obj.PageIndex = request.PageIndex + 1;

                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    obj.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        obj.DepartmentId = item.AssociatedDepts;
                        obj.IsPermissions = item.IsPermissions; ;
                    }
                }
                else
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
                /*End*/

                var result = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetAllCurrentBillsList", obj);
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.ListtPaperLaidV.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from billList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(billList.PaperLaidId), new tPaperLaidV(billList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        //added code venkat for dynamic 
        
        public ActionResult GetDraftBills(int PageNumber, int RowsPerPage, int loopStart, int loopEnd, string Sub, int? MemId, string BNumber)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                SiteSettings siteSettingMod = new SiteSettings();
                tPaperLaidV obj = new tPaperLaidV();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                //obj = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", obj);


                //if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
                //{
                //    CurrentSession.AssemblyId = obj.AssemblyId.ToString();
                //}

                //if (string.IsNullOrEmpty(CurrentSession.SessionId))
                //{
                //    CurrentSession.SessionId = obj.SessionId.ToString();
                //}
                obj.DSCApplicable = siteSettingMod.DSCApplicable;
                //added code venkat for dynamic 
                obj.Title = Sub;
                obj.MemberId = MemId ?? 0;
                obj.BillNumber = BNumber;
                //end-----------------------------------
                return PartialView("_GetDraftBills", obj);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }


        [HttpGet]
        public ActionResult GetPendingsBills(int PageNumber, int RowsPerPage, int loopStart, int loopEnd)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                SiteSettings siteSettingMod = new SiteSettings();
                tPaperLaidV obj = new tPaperLaidV();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

             
                obj.DSCApplicable = siteSettingMod.DSCApplicable;
                return PartialView("_GetPendingsBills", obj);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }


        public ActionResult GetPendingsBillsJqGrid(JqGridRequest request)
        {
            
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                SiteSettings siteSettingMod = new SiteSettings();
                tPaperLaidV obj = new tPaperLaidV();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

               

                obj.PAGE_SIZE = request.RecordsCount;
                obj.PageIndex = request.PageIndex + 1;
                obj.DSCApplicable = siteSettingMod.DSCApplicable;
                //New changes according  employee authorizations

                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    obj.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        obj.DepartmentId = item.AssociatedDepts;
                        obj.IsPermissions = item.IsPermissions; ;
                    }
                }
                else
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
                /*End*/

                var result = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetpendingsBill", obj);
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.ListtPaperLaidV.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from billList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(billList.PaperLaidId), new tPaperLaidV(billList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        //added code venkat for dynamic 
        public ActionResult GetDraftBillsJqGrid(JqGridRequest request, string Billnumber, string title, int? MemberId)
        {
            ;
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                SiteSettings siteSettingMod = new SiteSettings();
                tPaperLaidV obj = new tPaperLaidV();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                //  obj = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", obj);


                //if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
                //{
                //    CurrentSession.AssemblyId = obj.AssemblyId.ToString();
                //}

                //if (string.IsNullOrEmpty(CurrentSession.SessionId))
                //{
                //    CurrentSession.SessionId = obj.SessionId.ToString();
                //}


                obj.PAGE_SIZE = request.RecordsCount;
                obj.PageIndex = request.PageIndex + 1;
                obj.DSCApplicable = siteSettingMod.DSCApplicable;
                //added code venkat for dynamic 
                obj.BillNumber = Billnumber;
                obj.Title = title;
                obj.MemberId = MemberId ?? 0;
                //end------------------------------
                //New changes according  employee authorizations

                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    obj.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        obj.DepartmentId = item.AssociatedDepts;
                        obj.IsPermissions = item.IsPermissions; ;
                    }
                }
                else
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
                /*End*/

                var result = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDraftsBill", obj);
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.BillsDraftCustomModel.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<BillsDraftCustomModel>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from billList in resultForGrid
                                          select new JqGridRecord<BillsDraftCustomModel>(Convert.ToString(billList.PaperLaidId), new BillsDraftCustomModel(billList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        //added code venkat for dynamic 
     
        public ActionResult GetSentBills(int PageNumber, int RowsPerPage, int loopStart, int loopEnd, string title, int? MemId, string BNumber)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                //added code venkat for dynamic 
                obj.Title = title;
                //obj.MinistryId = MemId;
                obj.BillNumber = BNumber;
                //end--------------------------
                return PartialView("_GetSentBills", obj);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        //added code venkat for dynamic 
        public ActionResult GetSentBillsJqGrid(JqGridRequest request, string BNumber, string title, int? MemberId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                SiteSettings siteSettingMod = new SiteSettings();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                // obj = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", obj);


                //if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
                //{
                //    CurrentSession.AssemblyId = obj.AssemblyId.ToString();
                //}

                //if (string.IsNullOrEmpty(CurrentSession.SessionId))
                //{
                //    CurrentSession.SessionId = obj.SessionId.ToString();
                //}

                obj.PAGE_SIZE = request.RecordsCount;
                obj.PageIndex = request.PageIndex + 1;
                //added code venkat for dynamic 
                obj.BillNumber = BNumber;
                obj.Title = title;
                obj.MemberId = MemberId;
                //end------------------------------
                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    obj.UserID = new Guid(CurrentSession.UserID);
                }
                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        obj.DepartmentId = item.AssociatedDepts;
                        obj.IsPermissions = item.IsPermissions; ;
                    }
                }
                else
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
                /*End*/

                var result = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetsentBill", obj);

                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.ListtPaperLaidV.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from billList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(billList.PaperLaidId), new tPaperLaidV(billList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        //added code venkat for dynamic 
        [HttpPost]
        public ActionResult GetBillUPLOBList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd, string Sub, int? MemId, string BNumber)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                //added code venkat for dynamic 
                obj.Title = Sub;
                obj.MemberId = MemId;
                obj.BillNumber = BNumber;
               // end-----------------------
                return PartialView("_GetBillUPLOBList", obj);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        //added code venkat for dynamic 
        public ActionResult GetBillUPLOBListJqGrid(JqGridRequest request, string BNumber, string title, int? MemberId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {

                tPaperLaidV obj = new tPaperLaidV();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                obj = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", obj);


                if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    CurrentSession.AssemblyId = obj.AssemblyId.ToString();
                }

                if (string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    CurrentSession.SessionId = obj.SessionId.ToString();
                }
                obj.PAGE_SIZE = request.RecordsCount;
                obj.PageIndex = request.PageIndex + 1;
                //added code venkat for dynamic 
                obj.BillNumber = BNumber;
                obj.Title = title;
                obj.MemberId = MemberId;
                //end-------------------------
                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    obj.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        obj.DepartmentId = item.AssociatedDepts;
                        obj.IsPermissions = item.IsPermissions; ;
                    }
                }
                else
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
                /*End*/

                var result = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillULOBList", obj);

                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.ListtPaperLaidV.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from billList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(billList.PaperLaidId), new tPaperLaidV(billList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        //added code venkat for dynamic 
        [HttpPost]
        public ActionResult GetBillLIHList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd, string Sub, int? MemId, string BNumber)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                //added code venkat for dynamic 
                obj.Title = Sub;
                obj.MemberId = MemId;
                obj.BillNumber = BNumber;
                //end-------------------------
                return PartialView("_GetBillLIHList", obj);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        //added code venkat for dynamic 
        public ActionResult GetBillLIHListJqGrid(JqGridRequest request, string BNumber, string title, int? MemberId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {

                tPaperLaidV obj = new tPaperLaidV();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                obj = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", obj);


                if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    CurrentSession.AssemblyId = obj.AssemblyId.ToString();
                }

                if (string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    CurrentSession.SessionId = obj.SessionId.ToString();
                }
                obj.PAGE_SIZE = request.RecordsCount;
                obj.PageIndex = request.PageIndex + 1;
                //added code venkat for dynamic 
                obj.BillNumber = BNumber;
                obj.Title = title;
                obj.MemberId = MemberId;
                //end-----------------------------
                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    obj.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        obj.DepartmentId = item.AssociatedDepts;
                        obj.IsPermissions = item.IsPermissions; ;
                    }
                }
                else
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
                /*End*/

                var result = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillLIHList", obj);

                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.ListtPaperLaidV.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from billList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(billList.PaperLaidId), new tPaperLaidV(billList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        //added code venkat for dynamic 
        [HttpPost]
        public ActionResult GetBillPLIHList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd, string Sub, int? MemId, string BNumber)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                //added code venkat for dynamic 
                obj.Title = Sub;
                obj.MemberId = MemId;
                obj.BillNumber = BNumber;
                //end--------------------
                return PartialView("_GetBillPLIHList", obj);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        //added code venkat for dynamic 
        public ActionResult GetBillPLIHListJqGrid(JqGridRequest request, string BNumber, string title, int? MemberId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {

                tPaperLaidV obj = new tPaperLaidV();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                obj = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", obj);


                if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    CurrentSession.AssemblyId = obj.AssemblyId.ToString();
                }

                if (string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    CurrentSession.SessionId = obj.SessionId.ToString();
                }
                obj.PAGE_SIZE = request.RecordsCount;
                obj.PageIndex = request.PageIndex + 1;
                //added code venkat for dynamic 
                obj.BillNumber = BNumber;
                obj.Title = title;
                obj.MemberId = MemberId;
                //end-----------------------------------------
                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    obj.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        obj.DepartmentId = item.AssociatedDepts;
                        obj.IsPermissions = item.IsPermissions; ;
                    }
                }
                else
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
                /*End*/

                var result = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillPLIHList", obj);
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.ListtPaperLaidV.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from billList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(billList.PaperLaidId), new tPaperLaidV(billList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }


    

    }
}
