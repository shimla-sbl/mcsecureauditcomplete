﻿using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class MiscellaneousController : Controller
    {
        //
        // GET: /PaperLaidDepartment/Miscellaneous/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetEmployeeAuthorisation()
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                var userId = CurrentSession.UserID;
                var deptList = (List<mDepartment>)Helper.ExecuteService("SecrtEmpDSCAuthorization", "GetSecretaryDepartmentList", userId);
                EmployeeDSCAuthorisedModel model = new EmployeeDSCAuthorisedModel();
                SelectListItem firstelement = new SelectListItem { Value = "0000", Text = "Select Department", Selected = true };
                List<SelectListItem> deptSelcLst = deptList.Select(a => new SelectListItem { Value = a.deptId, Text = a.deptname }).ToList();
                deptSelcLst.Add(firstelement);
                model.DepartmentList = deptSelcLst;
                return PartialView("_GetEmployeeAuthorisation", model);
            }
            else
            {

                return RedirectToAction("LoginUP", "Account");
            }
        }

        public ActionResult GetDepartmentEmpList(string deptId)
        {
            var deptList = (List<mUsers>)Helper.ExecuteService("SecrtEmpDSCAuthorization", "GetDepartmentEmployeeList", deptId);
            DepartmentEmployeeList model = new DepartmentEmployeeList();
            model.UnAuthorisedEmployeList.ResultCount = deptList != null ? deptList.Where(a => a.IsDSCAuthorized == false).Count() : 0;
            model.AuthorisedEmployeList.ResultCount = deptList != null ? deptList.Where(a => a.IsDSCAuthorized).Count() : 0;

            model.AuthorisedEmployeList.loopEnd = 5;
            model.AuthorisedEmployeList.loopStart = 1;
            model.AuthorisedEmployeList.RowsPerPage = 10;
            model.AuthorisedEmployeList.PageNumber = 1;
            model.AuthorisedEmployeList.PageIndex = 1;
            model.AuthorisedEmployeList.PAGE_SIZE = 10;
            model.AuthorisedEmployeList.selectedPage = 1;
            var authEmpLst = deptList != null ? deptList.Where(a => a.IsDSCAuthorized).Skip((model.AuthorisedEmployeList.PageIndex - 1) * model.AuthorisedEmployeList.PAGE_SIZE).Take(model.AuthorisedEmployeList.PAGE_SIZE).ToList() : null;
            //model.AuthorisedEmployeList.EmployeeList = authEmpLst;
            // model.AuthorisedEmployeList.ResultCount = authEmpLst.Count;

            model.UnAuthorisedEmployeList.loopEnd = 5;
            model.UnAuthorisedEmployeList.loopStart = 1;
            model.UnAuthorisedEmployeList.RowsPerPage = 10;
            model.UnAuthorisedEmployeList.PageNumber = 1;
            model.UnAuthorisedEmployeList.PageIndex = 1;
            model.UnAuthorisedEmployeList.PAGE_SIZE = 10;
            model.UnAuthorisedEmployeList.selectedPage = 1;
            var unAuthEmpLast = deptList != null ? deptList.Where(a => a.IsDSCAuthorized == false).Skip((model.UnAuthorisedEmployeList.PageIndex - 1) * model.UnAuthorisedEmployeList.PAGE_SIZE).Take(model.UnAuthorisedEmployeList.PAGE_SIZE).ToList() : null;
            //model.UnAuthorisedEmployeList.EmployeeList = unAuthEmpLast;
            // model.UnAuthorisedEmployeList.ResultCount = unAuthEmpLast.Count;

            return PartialView("_SecDeptEmployeeList", model);
        }

        public ActionResult AuthorizeEmployees(string userIds, string deptId)
        {
            string[] userIDS = userIds.Split(',');
            var issucess = (bool)Helper.ExecuteService("SecrtEmpDSCAuthorization", "AuthoriseSecDeptEmployes", userIDS);
            var deptList = (List<mUsers>)Helper.ExecuteService("SecrtEmpDSCAuthorization", "GetDepartmentEmployeeList", deptId);
            DepartmentEmployeeList model = new DepartmentEmployeeList();

            model.UnAuthorisedEmployeList.ResultCount = deptList != null ? deptList.Where(a => a.IsDSCAuthorized == false).Count() : 0;
            model.AuthorisedEmployeList.ResultCount = deptList != null ? deptList.Where(a => a.IsDSCAuthorized).Count() : 0;

            model.AuthorisedEmployeList.loopEnd = 5;
            model.AuthorisedEmployeList.loopStart = 1;
            model.AuthorisedEmployeList.RowsPerPage = 10;
            model.AuthorisedEmployeList.PageNumber = 1;
            model.AuthorisedEmployeList.PageIndex = 1;
            model.AuthorisedEmployeList.PAGE_SIZE = 10;
            model.AuthorisedEmployeList.selectedPage = 1;
            var authEmpLst = deptList != null ? deptList.Where(a => a.IsDSCAuthorized).Skip((model.AuthorisedEmployeList.PageIndex - 1) * model.AuthorisedEmployeList.PAGE_SIZE).Take(model.AuthorisedEmployeList.PAGE_SIZE).ToList() : null;
            //model.AuthorisedEmployeList.EmployeeList = authEmpLst;
            // model.AuthorisedEmployeList.ResultCount = authEmpLst.Count;

            model.UnAuthorisedEmployeList.loopEnd = 5;
            model.UnAuthorisedEmployeList.loopStart = 1;
            model.UnAuthorisedEmployeList.RowsPerPage = 10;
            model.UnAuthorisedEmployeList.PageNumber = 1;
            model.UnAuthorisedEmployeList.PageIndex = 1;
            model.UnAuthorisedEmployeList.PAGE_SIZE = 10;
            model.UnAuthorisedEmployeList.selectedPage = 1;
            var unAuthEmpLast = deptList != null ? deptList.Where(a => a.IsDSCAuthorized == false).Skip((model.UnAuthorisedEmployeList.PageIndex - 1) * model.UnAuthorisedEmployeList.PAGE_SIZE).Take(model.UnAuthorisedEmployeList.PAGE_SIZE).ToList() : null;
            //model.UnAuthorisedEmployeList.EmployeeList = unAuthEmpLast;
            //model.UnAuthorisedEmployeList.ResultCount = unAuthEmpLast.Count;

            return PartialView("_SecDeptEmployeeList", model);
            // return View();
        }

        public ActionResult UnAuthorizeEmployees(string userIds, string deptId)
        {
            string[] userIDS = userIds.Split(',');
            var issucess = (bool)Helper.ExecuteService("SecrtEmpDSCAuthorization", "UnAuthoriseSecDeptEmployes", userIDS);
            var deptList = (List<mUsers>)Helper.ExecuteService("SecrtEmpDSCAuthorization", "GetDepartmentEmployeeList", deptId);
            DepartmentEmployeeList model = new DepartmentEmployeeList();

            model.UnAuthorisedEmployeList.ResultCount = deptList != null ? deptList.Where(a => a.IsDSCAuthorized == false).Count() : 0;
            model.AuthorisedEmployeList.ResultCount = deptList != null ? deptList.Where(a => a.IsDSCAuthorized).Count() : 0;

            model.AuthorisedEmployeList.loopEnd = 5;
            model.AuthorisedEmployeList.loopStart = 1;
            model.AuthorisedEmployeList.RowsPerPage = 10;
            model.AuthorisedEmployeList.PageNumber = 1;
            model.AuthorisedEmployeList.PageIndex = 1;
            model.AuthorisedEmployeList.PAGE_SIZE = 10;
            model.AuthorisedEmployeList.selectedPage = 1;
            var authEmpLst = deptList != null ? deptList.Where(a => a.IsDSCAuthorized).Skip((model.AuthorisedEmployeList.PageIndex - 1) * model.AuthorisedEmployeList.PAGE_SIZE).Take(model.AuthorisedEmployeList.PAGE_SIZE).ToList() : null;
            //model.AuthorisedEmployeList.EmployeeList = authEmpLst;

            model.UnAuthorisedEmployeList.loopEnd = 5;
            model.UnAuthorisedEmployeList.loopStart = 1;
            model.UnAuthorisedEmployeList.RowsPerPage = 10;
            model.UnAuthorisedEmployeList.PageNumber = 1;
            model.UnAuthorisedEmployeList.PageIndex = 1;
            model.UnAuthorisedEmployeList.PAGE_SIZE = 10;
            model.UnAuthorisedEmployeList.selectedPage = 1;
            var unAuthEmpLast = deptList != null ? deptList.Where(a => a.IsDSCAuthorized == false).Skip((model.UnAuthorisedEmployeList.PageIndex - 1) * model.UnAuthorisedEmployeList.PAGE_SIZE).Take(model.UnAuthorisedEmployeList.PAGE_SIZE).ToList() : null;
           // model.UnAuthorisedEmployeList.EmployeeList = unAuthEmpLast;

            return PartialView("_SecDeptEmployeeList", model);
        }

        //For paging
        public ActionResult GetAuthorisedList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd, string deptId)
        {
            var deptList = (List<mUsers>)Helper.ExecuteService("SecrtEmpDSCAuthorization", "GetDepartmentEmployeeList", deptId);
            EmployeeListModel model = new EmployeeListModel();

            model.loopEnd = loopEnd;
            model.loopStart = loopStart;
            model.RowsPerPage = RowsPerPage;
            model.PageNumber = PageNumber;
            model.PageIndex = PageNumber;
            model.PAGE_SIZE = RowsPerPage;
            model.selectedPage = PageNumber;
            var authorisedlst = deptList != null ? deptList.Where(a => a.IsDSCAuthorized).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList() : null;
            model.ResultCount = deptList != null ? deptList.Where(a => a.IsDSCAuthorized).Count() : 0;
           // model.EmployeeList = authorisedlst;
            return PartialView("_GetAuthorisedList", model);
        }

        //ForPaging
        public ActionResult GetUnAuthorisedList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd, string deptId)
        {
            var deptList = (List<mUsers>)Helper.ExecuteService("SecrtEmpDSCAuthorization", "GetDepartmentEmployeeList", deptId);
            EmployeeListModel model = new EmployeeListModel();

            model.loopEnd = loopEnd;
            model.loopStart = loopStart;
            model.RowsPerPage = RowsPerPage;
            model.PageNumber = PageNumber;
            model.PageIndex = PageNumber;
            model.PAGE_SIZE = RowsPerPage;
            model.selectedPage = PageNumber;
            var authorisedlst = deptList != null ? deptList.Where(a => a.IsDSCAuthorized == false).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList() : null;
           // model.EmployeeList = authorisedlst;
            model.ResultCount = deptList != null ? deptList.Where(a => a.IsDSCAuthorized == false).Count() : 0;
            return PartialView("_GetUnAuthorisedList", model);
        }
    }
}
