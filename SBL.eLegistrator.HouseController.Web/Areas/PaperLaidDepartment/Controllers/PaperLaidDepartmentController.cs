﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.PaperLaid;
using SBL.eLegistrator.HouseController.Web.Filters;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Notice;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.ComplexModel;
using Lib.Web.Mvc.JQuery.JqGrid;
using System.Linq;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister.Models;
using SBL.DomainModel.Models.LOB;
using SBL.eLegistrator.HouseController.Filters;
using SBL.DomainModel.Models.Departmentpdfpath;
using SMSServices;
using SMS.API;
using System.Security.Cryptography;
using System.Text;
using SBL.eLegistrator.HouseController.Web.Filters.Security;
using SBL.DomainModel.Models.UserModule;
using SBL.DomainModel.Models.Member;
using System.Net;
using SBL.DomainModel.Models.AssemblyFileSystem;
using System.Text.RegularExpressions;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.SmsGateway;
using SBL.DomainModel.Models.eFile;
using SBL.DomainModel.Models.Committee;
using SBL.eLegislator.HPMS.ServiceAdaptor;


namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Controllers
{
    [SBLAuthorize(Allow = "Authenticated")]
    [Audit]
    [NoCache]
    public class PaperLaidDepartmentController : Controller
    {
        // GET: /PaperLaidDepartment/PaperLaidDepartment/      
        public ActionResult Index()
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV paperLaidModel = new tPaperLaidV();

            mSession mSessionModel = new mSession();
            mAssembly mAssemblymodel = new mAssembly();
            paperLaidModel = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", paperLaidModel);
            paperLaidModel.mSession = (ICollection<mSession>)Helper.ExecuteService("Session", "GetAllSessionData", mSessionModel);
            paperLaidModel.mAssembly = (ICollection<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssembly", mAssemblymodel);
            return View(paperLaidModel);
        }

        public ActionResult PaperHide()
        {
            return View();
        }

        public ActionResult GetPaperEntry(string sId, string aId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                tPaperLaidV paperLaidModel = new tPaperLaidV();
                mDepartment departmentModel = new mDepartment();
                mEvent mEventModel = new mEvent();
                paperLaidModel.DepartmentId = CurrentSession.DeptID;
                tPaperLaidV ministerDetails = new tPaperLaidV();
                ministerDetails.DepartmentId = paperLaidModel.DepartmentId;
                paperLaidModel.SessionId = Convert.ToInt32(sId);
                paperLaidModel.AssemblyCode = Convert.ToInt32(aId);

                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                paperLaidModel.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("PaperLaid", "GetSessionDates", paperLaidModel);

                paperLaidModel.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("PaperLaid", "GetDocumentType", paperLaidModel);
                paperLaidModel.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
                paperLaidModel.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);

                paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);
                //   paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("PaperLaid", "GetMinisterMinistryName", paperLaidModel.DepartmentId);
                //   paperLaidModel.MinistryId = mMinisteryMinisterModel.MinistryID;
                return PartialView("_GetPaperEntry", paperLaidModel);
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/Index");
        }

        public ActionResult GetPaperEntryForDashBord(string questionNumber, string QuestionID, string QuestionTypeID, string MobileNumber, string ModuleId)
        {
            try
            {
                if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }
                string OTP = CurrentSession.UserID;
                string sub = OTP.Substring(0, 1);
                if (sub == "T")
                {
                    if (CurrentSession.UserID != null)
                    {
                        tPaperLaidV paperLaidModel = new tPaperLaidV();
                        mDepartment departmentModel = new mDepartment();
                        mEvent mEventModel = new mEvent();
                        tPaperLaidV ministerDetails = new tPaperLaidV();
                        // paperLaidModel.DepartmentId = CurrentSession.DeptID;
                        if (QuestionID != "")
                        {
                            ministerDetails.QuestionID = int.Parse(QuestionID);
                            string dptId = (string)Helper.ExecuteService("PaperLaid", "getDepartmentIdByQuestionId", ministerDetails);
                            if (dptId != "")
                            {
                                paperLaidModel.DepartmentId = dptId;
                            }
                            else
                            {
                                paperLaidModel.DepartmentId = CurrentSession.DeptID;
                            }
                        }

                        ministerDetails.DepartmentId = paperLaidModel.DepartmentId;

                        if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                        {
                            paperLaidModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                        }

                        if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                        {
                            paperLaidModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                        }

                        //mSession mSessionModel = new mSession();
                        //mAssembly mAssemblymodel = new mAssembly();
                        //paperLaidModel = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", paperLaidModel);
                        //paperLaidModel.SessionId = Convert.ToInt32(paperLaidModel.SessionCode);
                        //paperLaidModel.AssemblyId = Convert.ToInt32(paperLaidModel.AssemblyCode);

                        mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                        paperLaidModel.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("PaperLaid", "GetSessionDates", paperLaidModel);
                        //paperLaidModel.mSessionDate = (List<mSessionDate>)Helper.ExecuteService("PaperLaid", "GetSessionDates", paperLaidModel);

                        paperLaidModel.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("PaperLaid", "GetDocumentType", paperLaidModel);
                        paperLaidModel.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
                        paperLaidModel.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);

                        paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);

                        tQuestion mdl = new tQuestion();
                        tQuestion questionModel = new tQuestion();
                        questionModel.QuestionType = Convert.ToInt32(QuestionTypeID);
                        if (questionNumber != "")
                        {

                            questionModel.QuestionNumber = Convert.ToInt32(questionNumber);
                        }
                        if (Convert.ToInt32(questionNumber) == 0)
                        {

                            questionModel.QuestionNumber = null;
                        }
                        questionModel.QuestionID = Convert.ToInt32(QuestionID);

                        mdl = (tQuestion)Helper.ExecuteService("PaperLaid", "GetQuestionDetails", questionModel);
                        mEvent mEvents = new mEvent();
                        mEvents = (mEvent)Helper.ExecuteService("PaperLaid", "GetQuetionEventID", mEvents);

                        paperLaidModel.Title = mdl.Subject;
                        paperLaidModel.DiaryNumber = mdl.DiaryNumber;
                        paperLaidModel.Description = mdl.MainQuestion;
                        paperLaidModel.EventId = mEvents.EventId;
                        paperLaidModel.QuestionID = mdl.QuestionID;
                        paperLaidModel.DepartmentId = mdl.DepartmentID;
                        paperLaidModel.MergeDiaryNo = mdl.MergeDiaryNo;
                        paperLaidModel.IsClubbed = mdl.IsClubbed;
                        paperLaidModel.MemberName = mdl.RMemberName;
                        paperLaidModel.MobileNo = MobileNumber;
                        paperLaidModel.ModuleId = Convert.ToInt32(ModuleId);
                        paperLaidModel.QuestionTypeId = Convert.ToInt32(QuestionTypeID);
                        if (questionNumber != "")
                        {
                            paperLaidModel.QuestionNumber = Convert.ToInt32(questionNumber);
                        }
                        if (Convert.ToInt32(questionNumber) == 0)
                        {

                            paperLaidModel.QuestionNumber = null;
                        }
                        //New
                        paperLaidModel.DesireLayingDate = mdl.QuestionLayingDate;
                        paperLaidModel.QuestionTypeId = mdl.QuestionType;
                        if (mdl.SessionDateId != null)
                        {
                            paperLaidModel.DesireLayingDateId = Convert.ToInt32(mdl.SessionDateId);
                            paperLaidModel.SessionDateId = Convert.ToInt32(mdl.SessionDateId);
                        }
                        paperLaidModel.ModuleId = Convert.ToInt32(ModuleId);
                        return PartialView("_GetPaperEnrtyOTP", paperLaidModel);
                    }
                }
                else
                {
                    if (CurrentSession.UserID != null)
                    {
                        tPaperLaidV paperLaidModel = new tPaperLaidV();
                        mDepartment departmentModel = new mDepartment();
                        mEvent mEventModel = new mEvent();
                        tPaperLaidV ministerDetails = new tPaperLaidV();
                        // paperLaidModel.DepartmentId = CurrentSession.DeptID;
                        if (QuestionID != "")
                        {
                            ministerDetails.QuestionID = int.Parse(QuestionID);
                            string dptId = (string)Helper.ExecuteService("PaperLaid", "getDepartmentIdByQuestionId", ministerDetails);
                            if (dptId != "")
                            {
                                paperLaidModel.DepartmentId = dptId;
                            }
                            else
                            {
                                paperLaidModel.DepartmentId = CurrentSession.DeptID;
                            }
                        }

                        ministerDetails.DepartmentId = paperLaidModel.DepartmentId;

                        if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                        {
                            paperLaidModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                        }

                        if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                        {
                            paperLaidModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                        }

                        //mSession mSessionModel = new mSession();
                        //mAssembly mAssemblymodel = new mAssembly();
                        //paperLaidModel = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", paperLaidModel);
                        //paperLaidModel.SessionId = Convert.ToInt32(paperLaidModel.SessionCode);
                        //paperLaidModel.AssemblyId = Convert.ToInt32(paperLaidModel.AssemblyCode);

                        mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                        paperLaidModel.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("PaperLaid", "GetSessionDates", paperLaidModel);

                        paperLaidModel.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("PaperLaid", "GetDocumentType", paperLaidModel);
                        paperLaidModel.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
                        paperLaidModel.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);

                        paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);

                        tQuestion mdl = new tQuestion();
                        tQuestion questionModel = new tQuestion();
                        questionModel.QuestionType = Convert.ToInt32(QuestionTypeID);
                        if (questionNumber != "")
                        {
                            questionModel.QuestionNumber = Convert.ToInt32(questionNumber);
                        }
                        questionModel.QuestionID = Convert.ToInt32(QuestionID);

                        mdl = (tQuestion)Helper.ExecuteService("PaperLaid", "GetQuestionDetails", questionModel);
                        mEvent mEvents = new mEvent();
                        mEvents = (mEvent)Helper.ExecuteService("PaperLaid", "GetQuetionEventID", mEvents);

                        paperLaidModel.Title = mdl.Subject;
                        paperLaidModel.DiaryNumber = mdl.DiaryNumber;
                        paperLaidModel.Description = mdl.MainQuestion;
                        paperLaidModel.EventId = mEvents.EventId;
                        paperLaidModel.QuestionID = mdl.QuestionID;
                        paperLaidModel.DepartmentId = mdl.DepartmentID;
                        paperLaidModel.MinistryId = (int)mdl.MinistryId;
                        paperLaidModel.MergeDiaryNo = mdl.MergeDiaryNo;
                        paperLaidModel.IsClubbed = mdl.IsClubbed;
                        paperLaidModel.IsBracket = mdl.IsBracket;
                        paperLaidModel.DocFileName = mdl.OriDiaryFileName;
                        paperLaidModel.MemberName = mdl.RMemberName;
                        paperLaidModel.MobileNo = MobileNumber;
                        paperLaidModel.ModuleId = Convert.ToInt32(ModuleId);
                        paperLaidModel.QuestionTypeId = Convert.ToInt32(QuestionTypeID);
                        if (questionNumber != "")
                            paperLaidModel.QuestionNumber = Convert.ToInt32(questionNumber);

                        //New
                        paperLaidModel.DesireLayingDate = mdl.QuestionLayingDate;
                        paperLaidModel.QuestionTypeId = mdl.QuestionType;
                        if (mdl.SessionDateId != null)
                        {
                            paperLaidModel.DesireLayingDateId = Convert.ToInt32(mdl.SessionDateId);
                            paperLaidModel.SessionDateId = Convert.ToInt32(mdl.SessionDateId);
                        }
                        paperLaidModel.ModuleId = Convert.ToInt32(CurrentSession.MenuId);

                        return PartialView("_GetPaperEntryForDashBoard", paperLaidModel);
                    }
                }

                return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public JsonResult PaperLaidDepartmentChange(tPaperLaidV obj, string Department, int Ministry, string OldDepartment, int OldMinistryId, int QuestionID, int QtypeId, string MergeDiaryNo, string DiaryNumber)
        {
            tPaperLaidV mdl = new tPaperLaidV();
            tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
            tQuestion tQuestions = new tQuestion();
            AdhaarDetails aadhar = new AdhaarDetails();




            obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            tQuestions.DiaryNumber = obj.DiaryNumber;
            tQuestions.DepartmentID = Department;
            tQuestions.QuestionID = QuestionID;
            tQuestions.MergeDiaryNo = MergeDiaryNo;
            tQuestions.OldDepartmentId = OldDepartment;
            tQuestions.MinistryId = Ministry;
            tQuestions.OldMinistryId = OldMinistryId;
            tQuestions = (tQuestion)Helper.ExecuteService("PaperLaid", "UpdateQuestionDepartmentMinistryId", tQuestions);

            aadhar.AdhaarID = CurrentSession.AadharId;
            // aadhar = (AdhaarDetails)Helper.ExecuteService("PaperLaid", "GetUserNameByAadharId", aadhar);

            string Name = (string)Helper.ExecuteService("PaperLaid", "GetUserNameByAadharId", aadhar);

            mchangeDepartmentAuditTrail ChangedDepDetails = new mchangeDepartmentAuditTrail();
            ChangedDepDetails.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            ChangedDepDetails.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            ChangedDepDetails.DepartmentID = Department;
            ChangedDepDetails.AadharId = CurrentSession.AadharId;
            ChangedDepDetails.UserName = Name;
            ChangedDepDetails.DiaryNumber = DiaryNumber;
            ChangedDepDetails.ChangedDepartmentID = OldDepartment;
            mdl.QuestionTypeId = QtypeId;
            ChangedDepDetails.QuestionNumber = tQuestions.QuestionNumber;
            ChangedDepDetails.QuestionType = tQuestions.QuestionType;
            ChangedDepDetails.ChangedBy = !string.IsNullOrEmpty(CurrentSession.UserName) ? CurrentSession.UserName : null;
            ChangedDepDetails.ChangedDate = DateTime.Now;
            mchangeDepartmentAuditTrail ChangeDept = new mchangeDepartmentAuditTrail();
            ChangeDept = (mchangeDepartmentAuditTrail)Helper.ExecuteService("PaperLaid", "SubmittChangeDeptEntry", ChangedDepDetails);

            return Json(mdl, JsonRequestBehavior.AllowGet);

        }


        public ActionResult GetChangePaper(string questionNumber, string QuestionID, string QuestionTypeID, string DiaryNumberId)
        {
            tPaperLaidV paperLaidModel1 = new tPaperLaidV();

            ChangeDeptModule model = new ChangeDeptModule();
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                tPaperLaidV paperLaidModel = new tPaperLaidV();
                mDepartment departmentModel = new mDepartment();
                mEvent mEventModel = new mEvent();

                tPaperLaidV ministerDetails = new tPaperLaidV();
                // paperLaidModel.DepartmentId = CurrentSession.DeptID;
                if (QuestionID != "")
                {
                    ministerDetails.QuestionID = int.Parse(QuestionID);
                    string dptId = (string)Helper.ExecuteService("PaperLaid", "getDepartmentIdByQuestionId", ministerDetails);
                    int MinistryId = (int)Helper.ExecuteService("PaperLaid", "getMinistryIdByQuestionId", ministerDetails);
                    paperLaidModel.MinistryId = MinistryId;

                    if (dptId != "")
                    {
                        paperLaidModel.DepartmentId = dptId;
                    }
                    else
                    {
                        paperLaidModel.DepartmentId = CurrentSession.DeptID;
                    }
                }

                ministerDetails.DepartmentId = paperLaidModel.DepartmentId;
                mSession mSessionModel = new mSession();
                mAssembly mAssemblymodel = new mAssembly();
                paperLaidModel = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", paperLaidModel);
                paperLaidModel.SessionId = Convert.ToInt32(paperLaidModel.SessionCode);
                paperLaidModel.AssemblyId = Convert.ToInt32(paperLaidModel.AssemblyCode);

                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                paperLaidModel.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("PaperLaid", "GetSessionDates", paperLaidModel);

                paperLaidModel.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("PaperLaid", "GetDocumentType", paperLaidModel);
                paperLaidModel.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetChangeDepartment", departmentModel);
                paperLaidModel.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);

                //paperLaidModel.mDepartment = (List<mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", paperLaidModel);


                List<tMemberNotice> ListtMemberNotice = new List<tMemberNotice>();
                tMemberNotice noticeModel = new tMemberNotice();
                noticeModel.DepartmentId = "0";
                noticeModel.DepartmentName = "Select Department";
                ListtMemberNotice.Add(noticeModel);
                paperLaidModel.DepartmentLt = ListtMemberNotice;


                mMinisteryMinisterModel mmModel = new mMinisteryMinisterModel();
                mmModel.MinistryID = 0;
                mmModel.MinisterMinistryName = "Select Minister";
                paperLaidModel.mMinisteryMinister = Helper.ExecuteService("MinistryMinister", "GetMinisterMinistry", null) as List<mMinisteryMinisterModel>;
                paperLaidModel.mMinisteryMinister.Add(mmModel);


                //paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetails", ministerDetails);
                paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);

                tQuestion mdl = new tQuestion();
                tQuestion questionModel = new tQuestion();

                questionModel.QuestionType = Convert.ToInt32(QuestionTypeID);

                if (questionNumber != "")
                {
                    questionModel.QuestionNumber = Convert.ToInt32(questionNumber);
                }
                questionModel.QuestionID = Convert.ToInt32(QuestionID);

                mdl = (tQuestion)Helper.ExecuteService("PaperLaid", "GetQuestionDetails", questionModel);
                mEvent mEvents = new mEvent();
                mEvents = (mEvent)Helper.ExecuteService("PaperLaid", "GetQuetionEventID", mEvents);

                paperLaidModel1.CustomMessage = "Data Save Successfully !!";

                paperLaidModel.MergeDiaryNo = DiaryNumberId;

                paperLaidModel.Title = mdl.Subject;
                paperLaidModel.DiaryNumber = mdl.DiaryNumber;
                paperLaidModel.Description = mdl.MainQuestion;
                paperLaidModel.EventId = mEvents.EventId;
                paperLaidModel.QuestionID = mdl.QuestionID;
                paperLaidModel.DepartmentId = mdl.DepartmentID;

                if (questionNumber != "")
                    paperLaidModel.QuestionNumber = Convert.ToInt32(questionNumber);

                //New
                paperLaidModel.DesireLayingDate = mdl.QuestionLayingDate;
                paperLaidModel.QuestionTypeId = mdl.QuestionType;
                if (mdl.SessionDateId != null)
                {
                    paperLaidModel.DesireLayingDateId = Convert.ToInt32(mdl.SessionDateId);
                    paperLaidModel.SessionDateId = Convert.ToInt32(mdl.SessionDateId);
                }

                return PartialView("_GetChangeDepartment", paperLaidModel);
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/Index");
        }

        public ActionResult CategoryView(string categoryId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                int id = Convert.ToInt32(categoryId);
                mEvent mEventModel = new mEvent();
                mEventModel.EventId = id;
                int[] Ids = new int[2];
                int papertypeId = (int)Helper.ExecuteService("Events", "GetPaperCategoryID", mEventModel);
                tQuestionTypeModel questionModel = new tQuestionTypeModel();
                tPaperLaidV paperModel = new tPaperLaidV();
                Ids[0] = papertypeId;
                Ids[1] = Convert.ToInt32(categoryId);
                paperModel = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPaperLaidCategoryDetails", Ids);
                if (paperModel.categoryType.Equals("bill"))
                {
                    ICollection<int> yearList = new List<int>();
                    int currentYear = DateTime.Now.Year;
                    for (int i = currentYear - 5; i < currentYear; i++)
                    {
                        yearList.Add(i);
                    }
                    for (int i = currentYear; i < currentYear + 5; i++)
                    {
                        yearList.Add(i);
                    }
                    //paperModel.yearList = yearList;
                }
                return PartialView("_CategoryView", paperModel);
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/Index");
        }

        public JsonResult callQuestionNumber(int Id)
        {
            tPaperLaidV mdel = new tPaperLaidV();
            mdel.QuestionTypeId = Id;
            mdel.DepartmentId = CurrentSession.DeptID;
            if (CurrentSession.UserID != null)
            {
                tPaperLaidV mdl = new tPaperLaidV();
                mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetQuestionNumber", mdel);
                return Json(mdl.tQuestion, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public JsonResult CheckBillNumber(string number, string year)
        {
            if (CurrentSession.UserID != null)
            {
                tBillRegister mdl = new tBillRegister();
                string billNumber = number + " of " + year;
                mdl = (tBillRegister)Helper.ExecuteService("PaperLaid", "CheckBillNUmber", billNumber);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public JsonResult GetQuestionDetails(int Id)
        {
            if (CurrentSession.UserID != null)
            {
                tQuestion mdl = new tQuestion();
                mdl = (tQuestion)Helper.ExecuteService("PaperLaid", "GetQuestionDetails", Id);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        string noticeNumber = string.Empty;
        public JsonResult GetNoticeDetails(string Id)
        {
            if (CurrentSession.UserID != null)
            {
                noticeNumber = Id;
                tMemberNotice noticeModel = new tMemberNotice();
                noticeModel = (tMemberNotice)Helper.ExecuteService("PaperLaid", "GetNoticeDetails", Id);
                return Json(noticeModel, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public JsonResult callNoticeNumber(int Id)
        {
            if (CurrentSession.UserID != null)
            {
                tPaperLaidV mdl = new tPaperLaidV();
                mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetNotices", Id);
                return Json(mdl.tMemberNotice, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        [HttpPost]
        public ContentResult UploadSupFiles()
        {

            string url = "~/App_SupData" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPaperLaidV>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/App_SupData" + "/" + CurrentSession.UserID;
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/App_SupData" + "/" + CurrentSession.UserID), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPaperLaidV()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        [HttpPost]
        public ContentResult UploadFiles()
        {

            string url = "~/TempData" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPaperLaidV>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/TempData" + "/" + CurrentSession.UserID;
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/TempData" + "/" + CurrentSession.UserID), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPaperLaidV()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PaperLaidSubmitt(HttpPostedFileBase file, HttpPostedFileBase DocFile, HttpPostedFileBase Supfile, HttpPostedFileBase SupDocFile, tPaperLaidV obj, string submitButton)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }
            string fileName = "";
            string OTP = CurrentSession.UserID;
            string sub = OTP.Substring(0, 1);
            if (sub == "T")
            {
                if (CurrentSession.UserID != null)
                {
                    obj.ModifiedBy = !string.IsNullOrEmpty(CurrentSession.UserName) ? CurrentSession.UserName : null;
                    obj.CreatedBy = !string.IsNullOrEmpty(CurrentSession.UserName) ? CurrentSession.UserName : null;
                    obj.ModifiedWhen = DateTime.Now;

                    submitButton = submitButton.Replace(" ", "");
                    tPaperLaidV mdl = new tPaperLaidV();
                    tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
                    mdlTemp.RandomVal = SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Extensions.ExtensionMethods.GenerateRandomValue();
                    var RVal = mdlTemp.RandomVal;
                    switch (submitButton)
                    {

                        //Modified By sameer
                        case "Submit":
                            {
                                obj.AssemblyId = obj.AssemblyId;
                                obj.SessionId = obj.SessionId;
                                int questionId = obj.QuestionID;
                                int noticeId = obj.NoticeID;
                                tPaperLaidV billDetails = new tPaperLaidV();

                                obj = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                                billDetails = obj;
                                mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "SubmittPaperLaidEntry", obj);
                                mdl.EventId = obj.EventId;
                                int paperLaidId = (int)mdl.PaperLaidId;

                                mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetFileSaveDetails", mdl);
                                string categoryType = mdl.RespectivePaper;

                                tQuestion tQuestions = new tQuestion();
                                tQuestions.PaperLaidId = paperLaidId;
                                tQuestions.QuestionID = questionId;
                                tQuestions = (tQuestion)Helper.ExecuteService("PaperLaid", "UpdateQuestionPaperLaidId", tQuestions);

                                if (tQuestions.QuestionType == 1)
                                {
                                    TempData["lSM"] = "SQ";
                                }
                                else
                                {
                                    TempData["lSM"] = "USQ";
                                }
                                // TempData["Msg"] = "Save";

                                if (file != null)
                                {
                                    mdlTemp = SaveFileToDirectory(tQuestions, file, mdlTemp, mdl, obj);
                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                                    mdl.PaperLaidId = paperLaidId;

                                    if (mdlTemp.PaperLaidTempId == 0)
                                    {
                                        mdlTemp.PaperLaidTempId = (long)Helper.ExecuteService("PaperLaid", "GetActiveDeptActiveId", mdl);
                                    }
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    billDetails = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", mdlTemp);
                                    string url = "/OTPDeptUpload/" + "PDF/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    string directory = Server.MapPath(url);
                                    if (!Directory.Exists(directory))
                                    {
                                        Directory.CreateDirectory(directory);
                                    }
                                    fileName = file.FileName;
                                    var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                    System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                                    obj.TempPdfFile = url + fileName;
                                }



                                if (DocFile == null)
                                {


                                    string[] savedFileName = Directory.GetFiles(Server.MapPath("~/TempData" + "/" + CurrentSession.UserID));
                                    string SourceFile = savedFileName[0];
                                    foreach (string page in savedFileName)
                                    {
                                        string name = Path.GetFileName(page);
                                        string nameKey = Path.GetFileNameWithoutExtension(page);
                                        string directory = Path.GetDirectoryName(page);
                                        //
                                        // Display the Path strings we extracted.
                                        //
                                        Console.WriteLine("{0}, {1}, {2}, {3}",
                                        page, name, nameKey, directory);
                                        mdlTemp.DocFileName = name;
                                    }
                                    string QType = string.Empty;
                                    int? QuestionNo = null;

                                    if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
                                    {
                                        QType = "S";
                                    }
                                    else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
                                    {
                                        QType = "US";

                                    }

                                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                    string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    DirectoryInfo Dir = new DirectoryInfo(Url);
                                    if (!Dir.Exists)
                                    {
                                        Dir.Create();
                                    }



                                    string UrlSave = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    //DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                    //if (!Dir.Exists)
                                    //{
                                    //    Dir.Create();
                                    //}
                                    string ext = Path.GetExtension(mdlTemp.DocFileName);
                                    string fileName1 = mdlTemp.DocFileName.Replace(ext, "");

                                    if (tQuestions.QuestionNumber != null)
                                    {
                                        QuestionNo = tQuestions.QuestionID;
                                    }
                                    else
                                    {
                                        QuestionNo = tQuestions.QuestionID;
                                    }


                                    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + RVal + "_" + QuestionNo + ext;
                                    string path1 = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);
                                    //+ mdl.Count 
                                    //DocFile.SaveAs(Server.MapPath(Url + actualFileName));
                                    //var path1 = Path.Combine(Server.MapPath("~" + Url), actualFileName);
                                    //System.IO.FileStream _FileStream = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                                    System.IO.File.Copy(SourceFile, path1, true);


                                    mdlTemp.DocFileName = actualFileName;
                                    mdlTemp.FilePath = UrlSave;
                                    // mdlTemp.Version = mdl.Count;
                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    //  mdlTemp = SaveDocFileToDirectory(tQuestions, DocFile, mdlTemp, mdl, obj);
                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SaveDetailsInTempPaperLaidOTP", mdlTemp);
                                    mdl.PaperLaidId = paperLaidId;

                                    if (mdlTemp.PaperLaidTempId == 0)
                                    {
                                        mdlTemp.PaperLaidTempId = (long)Helper.ExecuteService("PaperLaid", "GetActiveDeptActiveId", mdl);
                                    }
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    billDetails = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", mdlTemp);
                                    string url = "/OTPDeptUpload/" + "DOC/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    string direc = Server.MapPath(url);
                                    if (!Directory.Exists(direc))
                                    {
                                        Directory.CreateDirectory(direc);
                                    }

                                    fileName = mdlTemp.DocFileName;
                                    var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                    System.IO.File.Copy(SourceFile, path, true);
                                    //System.IO.FileStream _FileStream1 = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                                    obj.TempDocFile = url + fileName;
                                }


                                if (obj.SuplDocFileStatus != null)
                                {
                                    string[] savedFileName = Directory.GetFiles(Server.MapPath("~/App_SupData" + "/" + CurrentSession.UserID));
                                    string SourceFile = savedFileName[0];
                                    foreach (string page in savedFileName)
                                    {
                                        string name = Path.GetFileName(page);
                                        string nameKey = Path.GetFileNameWithoutExtension(page);
                                        string directory = Path.GetDirectoryName(page);
                                        //
                                        // Display the Path strings we extracted.
                                        //
                                        Console.WriteLine("{0}, {1}, {2}, {3}",
                                        page, name, nameKey, directory);
                                        mdlTemp.SupDocFileName = name;
                                    }
                                    string QType = string.Empty;
                                    int? QuestionNo = null;

                                    if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
                                    {
                                        QType = "S";
                                    }
                                    else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
                                    {
                                        QType = "US";
                                    }
                                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                    string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    DirectoryInfo Dir = new DirectoryInfo(Url);
                                    if (!Dir.Exists)
                                    {
                                        Dir.Create();
                                    }
                                    string UrlSave = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    //DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                    //if (!Dir.Exists)
                                    //{
                                    //    Dir.Create();
                                    //}
                                    string ext = Path.GetExtension(mdlTemp.DocFileName);
                                    string fileName1 = mdlTemp.DocFileName.Replace(ext, "");

                                    if (tQuestions.QuestionNumber != null)
                                    {
                                        QuestionNo = tQuestions.QuestionID;
                                    }
                                    else
                                    {
                                        QuestionNo = tQuestions.QuestionID;
                                    }

                                    //+ mdl.Count 
                                    //+ mdl.Count 
                                    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + RVal + "_" + QuestionNo + "Sup" + ext;
                                    string path2 = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);
                                    //string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + RVal + "_" + QuestionNo + "Sup" + ext;
                                    //+ mdl.Count 
                                    //DocFile.SaveAs(Server.MapPath(Url + actualFileName));
                                    // var path2 = Path.Combine(Server.MapPath("~" + Url), actualFileName);
                                    System.IO.File.Copy(SourceFile, path2, true);
                                    //System.IO.FileStream _FileStream1 = new System.IO.FileStream(path2, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                                    mdlTemp.SupDocFileName = actualFileName;
                                    mdlTemp.FilePath = UrlSave;
                                    // mdlTemp.Version = mdl.Count;
                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    //mdlTemp = SupSaveDocFileToDirectory(tQuestions, SupDocFile, mdlTemp, mdl, obj);
                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SaveDetailsInTempPaperLaidOTP", mdlTemp);
                                    mdl.PaperLaidId = paperLaidId;

                                    if (mdlTemp.PaperLaidTempId == 0)
                                    {
                                        mdlTemp.PaperLaidTempId = (long)Helper.ExecuteService("PaperLaid", "GetActiveDeptActiveId", mdl);
                                    }
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    billDetails = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", mdlTemp);
                                    string url = "/OTPDeptUpload/" + "DOC/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    string direc = Server.MapPath(url);
                                    if (!Directory.Exists(direc))
                                    {
                                        Directory.CreateDirectory(direc);
                                    }
                                    fileName = mdlTemp.SupDocFileName;
                                    var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                    System.IO.File.Copy(SourceFile, path, true);
                                    //System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                                    obj.TempSuplDocFile = url + fileName;
                                }
                                // string MBno = obj.MobileNo;
                                var msg = (string)Helper.ExecuteService("OneTimeUserRegistration", "SaveAuditTrialOTPRegistration", obj);
                                var msg1 = (string)Helper.ExecuteService("OneTimeUserRegistration", "UpdateDateUserRegistration", obj);

                                break;
                            }
                    }
                    if (Request.IsAjaxRequest())
                    {
                        string url1 = "~/App_SupData" + "/" + CurrentSession.UserID;
                        string directory1 = Server.MapPath(url1);
                        if (Directory.Exists(directory1))
                        {
                            System.IO.Directory.Delete(directory1, true);
                        }
                        string url = "~/App_Data" + "/" + CurrentSession.UserID;
                        string directory = Server.MapPath(url);
                        if (Directory.Exists(directory))
                        {
                            System.IO.Directory.Delete(directory, true);
                        }
                        string url3 = "~/TempData" + "/" + CurrentSession.UserID;
                        string directory3 = Server.MapPath(url3);
                        if (Directory.Exists(directory3))
                        {
                            System.IO.Directory.Delete(directory3, true);
                        }
                        TempData["Id"] = obj.QuestionTypeId;
                        TempData["AssemblyId"] = obj.AssemblyId;
                        TempData["SessionId"] = obj.SessionId;
                        TempData["MobileNo"] = obj.MobileNo;
                        var data = submitButton;
                        var result = Json(data, JsonRequestBehavior.AllowGet);
                        return result;
                    }
                    else
                    {
                        string url1 = "~/App_SupData" + "/" + CurrentSession.UserID;
                        string directory1 = Server.MapPath(url1);
                        if (Directory.Exists(directory1))
                        {
                            System.IO.Directory.Delete(directory1, true);
                        }
                        string url3 = "~/TempData" + "/" + CurrentSession.UserID;
                        string directory3 = Server.MapPath(url3);
                        if (Directory.Exists(directory3))
                        {
                            System.IO.Directory.Delete(directory3, true);
                        }
                        TempData["Id"] = obj.QuestionTypeId;
                        TempData["AssemblyId"] = obj.AssemblyId;
                        TempData["SessionId"] = obj.SessionId;
                        TempData["MobileNo"] = obj.MobileNo;

                        return RedirectToAction("Index", "OneTimeUserRegistration", new { area = "OneTimeUserRegistration" });

                    }
                }


                return RedirectToAction("Index", "OneTimeUserRegistration", new { area = "OneTimeUserRegistration" });


            }
            else
            {
                if (CurrentSession.UserID != null)
                {
                    obj.ModifiedBy = !string.IsNullOrEmpty(CurrentSession.UserName) ? CurrentSession.UserName : null;
                    obj.ModifiedWhen = DateTime.Now;

                    //submitButton = submitButton.Replace(" ", "");
                    submitButton = "Save";
                    tPaperLaidV mdl = new tPaperLaidV();
                    tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
                    switch (submitButton)
                    {
                        case "Save":
                            {
                                obj.AssemblyId = obj.AssemblyId;
                                obj.SessionId = obj.SessionId;
                                int questionId = obj.QuestionID;
                                int noticeId = obj.NoticeID;
                                tPaperLaidV billDetails = new tPaperLaidV();

                                obj = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                                billDetails = obj;
                                mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "SubmittPaperLaidEntry", obj);
                                mdl.EventId = obj.EventId;
                                int paperLaidId = (int)mdl.PaperLaidId;

                                mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetFileSaveDetails", mdl);
                                string categoryType = mdl.RespectivePaper;

                                tQuestion tQuestions = new tQuestion();
                                tQuestions.PaperLaidId = paperLaidId;
                                tQuestions.QuestionID = questionId;
                                // tQuestions.IsAcknowledgmentDate = DateTime.Now;
                                tQuestions = (tQuestion)Helper.ExecuteService("PaperLaid", "UpdateQuestionPaperLaidId", tQuestions);
                                obj.QuestionTypeId = tQuestions.QuestionType;
                                if (tQuestions.QuestionType == 1)
                                {
                                    TempData["lSM"] = "SQ";
                                }
                                else
                                {
                                    TempData["lSM"] = "USQ";
                                }
                                //TempData["Msg"] = "Save";

                                //Assigened old File by sujeet
                                mdlTemp.DocFileName = obj.DocFileName;
                                mdlTemp.SupDocFileName = obj.SupDocFileName;
                                mdlTemp.FileName = obj.FileName;
                                mdlTemp.SupFileName = obj.SupFileName;
                                //String Usid = CurrentSession.UserID
                                if (file == null)
                                {
                                    string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.FileName = name;
                                        }
                                        string ext = Path.GetExtension(mdlTemp.FileName);
                                        string fileNam1 = mdlTemp.FileName.Replace(ext, "");

                                        string QType = string.Empty;
                                        int? QuestionNo = null;

                                        if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
                                        {
                                            QType = "S";
                                        }
                                        else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
                                        {
                                            QType = "US";
                                        }

                                        if (tQuestions.QuestionNumber != null)
                                        {
                                            QuestionNo = tQuestions.QuestionID;
                                        }
                                        else
                                        {
                                            QuestionNo = tQuestions.QuestionID;
                                        }

                                        string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + QuestionNo + "_V" + mdl.Count + ext;


                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);
                                        string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.FileName = actualFileName;
                                    }
                                    mdl.PaperLaidId = paperLaidId;
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    mdlTemp.Version = mdl.Count;
                                    mdlTemp.AssemblyId = CurrentSession.AssemblyId;
                                    mdlTemp.SessionId = CurrentSession.SessionId;
                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);


                                    if (mdlTemp.PaperLaidTempId == 0)
                                    {
                                        mdlTemp.PaperLaidTempId = (long)Helper.ExecuteService("PaperLaid", "GetActiveDeptActiveId", mdl);
                                    }
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    billDetails = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", mdlTemp);

                                }
                                if (DocFile == null)
                                {

                                    string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.DocFileName = name;
                                        }
                                        string ext = Path.GetExtension(mdlTemp.DocFileName);
                                        string fileNam1 = mdlTemp.DocFileName.Replace(ext, "");

                                        string QType = string.Empty;
                                        int? QuestionNo = null;

                                        if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
                                        {
                                            QType = "S";
                                        }
                                        else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
                                        {
                                            QType = "US";
                                        }

                                        if (tQuestions.QuestionNumber != null)
                                        {
                                            QuestionNo = tQuestions.QuestionID;
                                        }
                                        else
                                        {
                                            QuestionNo = tQuestions.QuestionID;
                                        }
                                        string actualDocFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + QuestionNo + "Main" + ext;


                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualDocFileName);
                                        //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.DocFileName = actualDocFileName;

                                    }
                                    mdl.PaperLaidId = paperLaidId;
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    mdlTemp.DocFileVersion = mdl.MainDocCount;

                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);


                                    if (mdlTemp.PaperLaidTempId == 0)
                                    {
                                        mdlTemp.PaperLaidTempId = (long)Helper.ExecuteService("PaperLaid", "GetActiveDeptActiveId", mdl);
                                    }
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    billDetails = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", mdlTemp);

                                }

                                if (Supfile == null)
                                {
                                    string url = "~/DepartmentPdf/SupPdf" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/SupPdf" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.SupFileName = name;
                                        }
                                        string ext = Path.GetExtension(mdlTemp.SupFileName);
                                        string fileNam1 = mdlTemp.SupFileName.Replace(ext, "");

                                        string QType = string.Empty;
                                        int? QuestionNo = null;

                                        if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
                                        {
                                            QType = "S";
                                        }
                                        else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
                                        {
                                            QType = "US";
                                        }
                                        if (tQuestions.QuestionNumber != null)
                                        {
                                            QuestionNo = tQuestions.QuestionID;
                                        }
                                        else
                                        {
                                            QuestionNo = tQuestions.QuestionID;
                                        }

                                        string actualSupFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + QuestionNo + "SUP" + ext;


                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualSupFileName);
                                        //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.SupFileName = actualSupFileName;
                                        mdl.PaperLaidId = paperLaidId;
                                        mdlTemp.PaperLaidId = paperLaidId;

                                        mdlTemp.SupVersion = mdl.SupPDFCount;

                                    }


                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);


                                    if (mdlTemp.PaperLaidTempId == 0)
                                    {
                                        mdlTemp.PaperLaidTempId = (long)Helper.ExecuteService("PaperLaid", "GetActiveDeptActiveId", mdl);
                                    }
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    billDetails = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", mdlTemp);
                                }


                                if (SupDocFile == null)
                                {
                                    string url = "~/DepartmentPdf/SuppReplyDoc" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/SuppReplyDoc" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.SupDocFileName = name;
                                        }
                                        string ext = Path.GetExtension(mdlTemp.SupDocFileName);
                                        string fileNam1 = mdlTemp.SupDocFileName.Replace(ext, "");

                                        string QType = string.Empty;
                                        int? QuestionNo = null;

                                        if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
                                        {
                                            QType = "S";
                                        }
                                        else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
                                        {
                                            QType = "US";
                                        }
                                        if (tQuestions.QuestionNumber != null)
                                        {
                                            QuestionNo = tQuestions.QuestionID;
                                        }
                                        else
                                        {
                                            QuestionNo = tQuestions.QuestionID;
                                        }

                                        string actualSupDocFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + QuestionNo + "SUP" + ext;


                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualSupDocFileName);
                                        //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.SupDocFileName = actualSupDocFileName;
                                        mdl.PaperLaidId = paperLaidId;
                                        mdlTemp.PaperLaidId = paperLaidId;
                                        //mdlTemp.DocFileVersion = mdl.MainDocCount;

                                    }


                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);


                                    if (mdlTemp.PaperLaidTempId == 0)
                                    {
                                        mdlTemp.PaperLaidTempId = (long)Helper.ExecuteService("PaperLaid", "GetActiveDeptActiveId", mdl);
                                    }
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    billDetails = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", mdlTemp);

                                    string url1 = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                                    string MainPDFdirectory1 = Server.MapPath(url1);
                                    if (Directory.Exists(MainPDFdirectory1))
                                    {
                                        System.IO.Directory.Delete(MainPDFdirectory1, true);
                                    }
                                    string urlMainDoc = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                                    string MainDocdirectory = Server.MapPath(urlMainDoc);
                                    if (Directory.Exists(MainDocdirectory))
                                    {
                                        System.IO.Directory.Delete(MainDocdirectory, true);
                                    }

                                    string SupPDFurl1 = "~/DepartmentPdf/SupPdf" + "/" + CurrentSession.UserID;
                                    string SupPDFdirectory1 = Server.MapPath(SupPDFurl1);
                                    if (Directory.Exists(SupPDFdirectory1))
                                    {
                                        System.IO.Directory.Delete(SupPDFdirectory1, true);
                                    }
                                    string SupDocurl = "~/DepartmentPdf/SuppReplyDoc" + "/" + CurrentSession.UserID;
                                    string SupDocdirectory = Server.MapPath(SupDocurl);
                                    if (Directory.Exists(SupDocdirectory))
                                    {
                                        System.IO.Directory.Delete(SupDocdirectory, true);
                                    }
                                }



                                break;
                            }
                        case "Save&Send":
                            {
                                obj.AssemblyId = obj.AssemblyCode;
                                obj.SessionId = obj.SessionId;
                                int questionId = obj.QuestionID;
                                int noticeId = obj.NoticeID;
                                tPaperLaidV billDetails = new tPaperLaidV();

                                obj = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                                billDetails = obj;
                                mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "SubmittPaperLaidEntry", obj);
                                mdl.EventId = obj.EventId;
                                int paperLaidId = (int)mdl.PaperLaidId;
                                mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetFileSaveDetails", mdl);
                                string categoryType = mdl.RespectivePaper;

                                tQuestion tQuestions = new tQuestion();
                                tQuestions.PaperLaidId = paperLaidId;
                                tQuestions.QuestionID = questionId;
                                tQuestions = (tQuestion)Helper.ExecuteService("PaperLaid", "UpdateQuestionPaperLaidId", tQuestions);

                                mdlTemp.DocFileName = obj.DocFileName;
                                mdlTemp.SupDocFileName = obj.SupDocFileName;

                                if (file != null)
                                {
                                    mdlTemp = SaveFileToDirectory(tQuestions, file, mdlTemp, mdl, obj);

                                    //File is uploaded save the details.
                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                                }

                                if (DocFile != null)
                                {
                                    mdlTemp = SaveDocFileToDirectory(tQuestions, DocFile, mdlTemp, mdl, obj);
                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                                    mdl.PaperLaidId = paperLaidId;

                                    if (mdlTemp.PaperLaidTempId == 0)
                                    {
                                        mdlTemp.PaperLaidTempId = (long)Helper.ExecuteService("PaperLaid", "GetActiveDeptActiveId", mdl);
                                    }
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    billDetails = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", mdlTemp);
                                }

                                if (Supfile != null)
                                {
                                    mdlTemp = SupSaveFileToDirectory(tQuestions, Supfile, mdlTemp, mdl, obj);
                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                                    mdl.PaperLaidId = paperLaidId;

                                    if (mdlTemp.PaperLaidTempId == 0)
                                    {
                                        mdlTemp.PaperLaidTempId = (long)Helper.ExecuteService("PaperLaid", "GetActiveDeptActiveId", mdl);
                                    }
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    billDetails = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", mdlTemp);
                                }
                                if (SupDocFile != null)
                                {
                                    mdlTemp = SupSaveDocFileToDirectory(tQuestions, SupDocFile, mdlTemp, mdl, obj);
                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                                    mdl.PaperLaidId = paperLaidId;

                                    if (mdlTemp.PaperLaidTempId == 0)
                                    {
                                        mdlTemp.PaperLaidTempId = (long)Helper.ExecuteService("PaperLaid", "GetActiveDeptActiveId", mdl);
                                    }
                                    mdlTemp.PaperLaidId = paperLaidId;
                                    billDetails = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", mdlTemp);
                                }


                                tPaperLaidTemp model = new tPaperLaidTemp();
                                tPaperLaidV mod = new tPaperLaidV();
                                model.DeptSubmittedBy = CurrentSession.UserName;
                                model.DeptSubmittedDate = DateTime.Now;

                                model.PaperLaidId = Convert.ToInt32(paperLaidId);
                                model.DeptSubmittedBy = CurrentSession.UserName;
                                model.DeptSubmittedDate = DateTime.Now;
                                model = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SubmitPendingDepartmentPaperLaid", model);

                                mod = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", model);
                                break;
                            }
                        case "Send":
                            {
                                tPaperLaidTemp model = new tPaperLaidTemp();
                                tPaperLaidV mod = new tPaperLaidV();
                                model.PaperLaidId = Convert.ToInt32(obj.PaperLaidId);
                                model.DeptSubmittedBy = CurrentSession.UserName;
                                model.DeptSubmittedDate = DateTime.Now;
                                model = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SubmitPendingDepartmentPaperLaid", model);
                                mod = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", model);

                                break;
                            }
                    }
                    if (Request.IsAjaxRequest())
                    {
                        tPaperLaidV mod = new tPaperLaidV();
                        List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                        AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", mod);



                        mod.QuestionTypeId = obj.QuestionTypeId;

                        if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                        {
                            foreach (var item in AuthorisedEmp)
                            {
                                mod.DepartmentId = item.AssociatedDepts;
                                mod.IsPermissions = item.IsPermissions; ;
                            }

                            if (mod.DepartmentId == null)
                            {
                                mod.DepartmentId = CurrentSession.DeptID;
                            }
                        }
                        else
                        {
                            mod.DepartmentId = CurrentSession.DeptID;
                        }
                        mod.ModuleId = 6;
                        var data = submitButton;


                        return Json(mod, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                    }
                }

                return null;

            }

        }

        public ActionResult testPDF()
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            mUsers objmUsers = new mUsers();
            objmUsers.UserId = Guid.Parse(CurrentSession.UserID);
            var Result = Helper.ExecuteService("User", "GetUserDeptDetailsByUserID", objmUsers) as mUsers;

            if (Result.IsSecChange == true && Result.IsSecretoryId == false && Result.UserType == 2)
            {
                return Content("<script>alert('Your Secretary has been changed,Please Setup your profile')</script>");
                //return Content("<script>alert('Your Secretary has been changed,Please Setup your profile');window.location.assign('/UserManagement/UserRequest');</script>");

            }


            tPaperLaidV model = new tPaperLaidV();
            //Get the Total count of All Type of question.
            //added code venkat for dynamic
            model.UserID = new Guid(CurrentSession.UserID);
            //end--------------------------------------------------
            if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
            {
                model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            }
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model);

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);



            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);

            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }
            else
            {
                model.DepartmentId = CurrentSession.DeptID;
            }

            /*End*/


            // model.DepartmentId = CurrentSession.DeptID;



            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetStarredQuestionCounter", model);

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetUnstarredQuestionCounter", model);

            model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetCountForOtherPaperLaidTypes", model);
            model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidCounters", model);

            mDepartment deptInfo1 = new mDepartment();
            deptInfo1.deptId = model.DepartmentId;

            List<mDepartment> deptInfo = new List<mDepartment>();

            model = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetCountForBillsandNotice", model);

            model.CurrentUserName = CurrentSession.UserName;

            model.UserDesignation = CurrentSession.MemberDesignation;

            //model.SessionId = Convert.ToInt32(siteSettingMod.SessionCode);
            //model.AssemblyCode = Convert.ToInt32(siteSettingMod.AssemblyCode);

            mSession Mdl = new mSession();
            Mdl.SessionCode = model.SessionId;
            Mdl.AssemblyID = model.AssemblyCode;
            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = model.AssemblyCode;

            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
            {
                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

                deptInfo = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartmentByIDs", model);
                // deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;


                foreach (var item in deptInfo)
                {
                    if (item != null)
                    {
                        model.DeparmentNameByids += item.deptname + ", ";
                    }


                }

                if (model.DeparmentNameByids != null && model.DeparmentNameByids.LastIndexOf(",") > 0)
                {
                    model.DeparmentNameByids = model.DeparmentNameByids.Substring(0, model.DeparmentNameByids.Length - 2);
                }


            }
            else
            {
                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameLocalBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameLocalByAssemblyCode", assmblyMdl);


                deptInfo = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartmentByIDsLocal", model);

                foreach (var item in deptInfo)
                {
                    model.DeparmentNameByids += item.deptnameLocal + ", ";

                }

                if (model.DeparmentNameByids.LastIndexOf(",") > 0)
                {
                    model.DeparmentNameByids = model.DeparmentNameByids.Substring(0, model.DeparmentNameByids.Length - 2);
                }
            }



            if (Convert.ToString(TempData["lSM"]) != "")
            {
                model.lSM = Convert.ToString(TempData["lSM"]);

            }
            else
            {
                model.lSM = "";
            }
            if (Convert.ToString(TempData["Msg"]) != "")
            {
                if (Convert.ToString(TempData["Msg"]) == "Save")
                {
                    model.CustomMessage = "Data Save Successfully !!";
                    model.RequestMessage = "Your Request ID : " + Convert.ToString(TempData["ReqMesg"]);
                }
                if (Convert.ToString(TempData["Msg"]) == "Update")
                {
                    model.CustomMessage = "Data Update Successfully !!";
                }
                if (Convert.ToString(TempData["Msg"]) == "File")
                {
                    model.CustomMessage = "Paper Replacement Successfully !!";
                }
                if (Convert.ToString(TempData["Msg"]) == "Sign")
                {
                    model.CustomMessage = "Paper Signed Successfully !!";
                }

            }
            else
            {
                model.CustomMessage = "";
            }
            return View(model);
        }



        public tPaperLaidTemp SaveFileToDirectory(tQuestion tQuestions, HttpPostedFileBase file, tPaperLaidTemp mdlTemp, tPaperLaidV mdl, tPaperLaidV obj)
        {
            string QType = string.Empty;
            int? QuestionNo = null;

            if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
            {
                QType = "S";
            }
            else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
            {
                QType = "US";
            }
            string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
            if (!Dir.Exists)
            {
                Dir.Create();
            }
            string ext = Path.GetExtension(file.FileName);
            string fileName = file.FileName.Replace(ext, "");

            if (tQuestions.QuestionNumber != null)
            {
                QuestionNo = tQuestions.QuestionID;
            }
            else
            {
                QuestionNo = tQuestions.QuestionID;
            }


            string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + QuestionNo + "_V" + mdl.Count + ext;
            //+ mdl.Count 
            file.SaveAs(Server.MapPath(Url + actualFileName));
            mdlTemp.FileName = actualFileName;
            mdlTemp.FilePath = Url;
            mdlTemp.Version = mdl.Count;
            mdlTemp.PaperLaidId = mdl.PaperLaidId;

            return mdlTemp;
        }

        public tPaperLaidTemp SaveDocFileToDirectory(tQuestion tQuestions, HttpPostedFileBase DocFile, tPaperLaidTemp mdlTemp, tPaperLaidV mdl, tPaperLaidV obj)
        {
            string QType = string.Empty;
            int? QuestionNo = null;

            if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
            {
                QType = "S";
            }
            else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
            {
                QType = "US";
            }
            string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
            if (!Dir.Exists)
            {
                Dir.Create();
            }
            string ext = Path.GetExtension(DocFile.FileName);
            string fileName = DocFile.FileName.Replace(ext, "");

            if (tQuestions.QuestionNumber != null)
            {
                QuestionNo = tQuestions.QuestionID;
            }
            else
            {
                QuestionNo = tQuestions.QuestionID;
            }


            string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + QuestionNo + "_V" + mdl.MainDocCount + ext;
            //+ mdl.Count 
            DocFile.SaveAs(Server.MapPath(Url + actualFileName));
            mdlTemp.DocFileName = actualFileName;
            mdlTemp.FilePath = Url;
            mdlTemp.DocFileVersion = mdl.MainDocCount;
            mdlTemp.PaperLaidId = mdl.PaperLaidId;

            return mdlTemp;
        }


        public tPaperLaidTemp SupSaveFileToDirectory(tQuestion tQuestions, HttpPostedFileBase Supfile, tPaperLaidTemp mdlTemp, tPaperLaidV mdl, tPaperLaidV obj)
        {
            string QType = string.Empty;
            int? QuestionNo = null;

            if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
            {
                QType = "S";
            }
            else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
            {
                QType = "US";
            }
            string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
            if (!Dir.Exists)
            {
                Dir.Create();
            }
            string ext = Path.GetExtension(Supfile.FileName);
            string fileName = Supfile.FileName.Replace(ext, "");

            if (tQuestions.QuestionNumber != null)
            {
                QuestionNo = tQuestions.QuestionID;
            }
            else
            {
                QuestionNo = tQuestions.QuestionID;
            }


            string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + QuestionNo + "_V" + mdl.SupPDFCount + "SUP" + ext;
            //+ mdl.Count 
            Supfile.SaveAs(Server.MapPath(Url + actualFileName));
            mdlTemp.SupFileName = actualFileName;
            mdlTemp.FilePath = Url;
            mdlTemp.SupVersion = mdl.SupPDFCount;
            mdlTemp.PaperLaidId = mdl.PaperLaidId;
            mdlTemp.Version = mdl.Count;
            return mdlTemp;
        }

        public tPaperLaidTemp SupSaveDocFileToDirectory(tQuestion tQuestions, HttpPostedFileBase SupDocFile, tPaperLaidTemp mdlTemp, tPaperLaidV mdl, tPaperLaidV obj)
        {
            string QType = string.Empty;
            int? QuestionNo = null;

            if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
            {
                QType = "S";
            }
            else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
            {
                QType = "US";
            }
            string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
            if (!Dir.Exists)
            {
                Dir.Create();
            }
            string ext = Path.GetExtension(SupDocFile.FileName);
            string fileName = SupDocFile.FileName.Replace(ext, "");

            if (tQuestions.QuestionNumber != null)
            {
                QuestionNo = tQuestions.QuestionID;
            }
            else
            {
                QuestionNo = tQuestions.QuestionID;
            }


            string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + QuestionNo + "Sup" + ext;
            //+ mdl.Count 
            SupDocFile.SaveAs(Server.MapPath(Url + actualFileName));
            mdlTemp.SupDocFileName = actualFileName;

            mdlTemp.FilePath = Url;
            mdlTemp.Version = mdl.Count;
            mdlTemp.SupVersion = mdl.Count;
            mdlTemp.PaperLaidId = mdl.PaperLaidId;

            return mdlTemp;
        }

        public ActionResult FilterPendingForSubmissionList(int AssemblyId, int SessionId, int PaperCategoryTypeId, int CategoryTypeId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {

                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mPaperCategoryType> PaperCategoryTypeList = Helper.ExecuteService("PaperLaid", "GetAllPaperCategoryType", PaperCategoryTypeListMod) as List<mPaperCategoryType>;
                tPaperLaidDepartmentModel.mPaperCategoryTypeList = PaperCategoryTypeList;

                #endregion

                tPaperLaidV objMovement = new tPaperLaidV();
                objMovement.AssemblyId = AssemblyId;
                objMovement.SessionId = SessionId;
                objMovement.PaperCategoryTypeId = PaperCategoryTypeId;
                objMovement.EventId = CategoryTypeId;
                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                objPaperModel.ResultCount = int.Parse(ResultCount);
                objPaperModel.PaperCategoryTypeId = PaperCategoryTypeId;
                tPaperLaidDepartmentModel.DepartmentPendingList = (ICollection<PaperMovementModel>)Helper.ExecuteService("PaperLaid", "GetPendingDepartmentPaperLaidList", objPaperModel);
                if (tPaperLaidDepartmentModel.DepartmentPendingList.Count > 0)
                {
                    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentPendingList.Count;
                }
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetPendingForSubmission", tPaperLaidDepartmentModel);
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/Index");
        }

        public ActionResult GetPendingForSubmission(int AssemblyId, int SessionId, int PaperCategoryTypeId, int CategoryTypeId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mPaperCategoryType> PaperCategoryTypeList = Helper.ExecuteService("PaperLaid", "GetAllPaperCategoryType", PaperCategoryTypeListMod) as List<mPaperCategoryType>;
                tPaperLaidDepartmentModel.mPaperCategoryTypeList = PaperCategoryTypeList;

                #endregion

                tPaperLaidV objMovement = new tPaperLaidV();
                objMovement.AssemblyId = AssemblyId;
                objMovement.SessionId = SessionId;
                objMovement.PaperCategoryTypeId = PaperCategoryTypeId;
                objMovement.EventId = CategoryTypeId;
                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                objPaperModel.ResultCount = int.Parse(ResultCount);
                tPaperLaidDepartmentModel.DepartmentPendingList = (ICollection<PaperMovementModel>)Helper.ExecuteService("PaperLaid", "GetPendingDepartmentPaperLaidList", objPaperModel);
                if (tPaperLaidDepartmentModel.DepartmentPendingList.Count > 0)
                {
                    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentPendingList.Count;
                }
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetPendingForSubmission", tPaperLaidDepartmentModel);
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/Index");

        }

        [HttpPost]
        public JsonResult GetCategoryByPaperCategoryTypeId(int PaperCategoryTypeId)
        {
            if (CurrentSession.UserID != null)
            {
                tPaperLaidV model = new tPaperLaidV();
                mEvent objEventModel = new mEvent();

                objEventModel.PaperCategoryTypeId = PaperCategoryTypeId;
                model.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetEventsByPaperCategoryID", objEventModel);

                return Json(model.mEvents, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public ActionResult FilterSubmittedForSubmissionList(int AssemblyId, int SessionId, int PaperCategoryTypeId, int CategoryTypeId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mPaperCategoryType> PaperCategoryTypeList = Helper.ExecuteService("PaperLaid", "GetAllPaperCategoryType", PaperCategoryTypeListMod) as List<mPaperCategoryType>;
                tPaperLaidDepartmentModel.mPaperCategoryTypeList = PaperCategoryTypeList;

                #endregion

                tPaperLaidV objMovement = new tPaperLaidV();
                objMovement.AssemblyId = AssemblyId;
                objMovement.SessionId = SessionId;
                objMovement.PaperCategoryTypeId = PaperCategoryTypeId;
                objMovement.EventId = CategoryTypeId;
                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                objPaperModel.ResultCount = int.Parse(ResultCount);
                objPaperModel.PaperCategoryTypeId = PaperCategoryTypeId;
                tPaperLaidDepartmentModel.DepartmentSubmitList = (ICollection<PaperMovementModel>)Helper.ExecuteService("PaperLaid", "GetSubmittedPaperLaidList", objPaperModel);
                if (tPaperLaidDepartmentModel.DepartmentSubmitList.Count > 0)
                {
                    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentSubmitList.Count;
                }
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetPaperLaidSubmittList", tPaperLaidDepartmentModel);
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/Index");
        }

        public ActionResult GetPaperLaidSubmittList(int AssemblyId, int SessionId, int PaperCategoryTypeId, int CategoryTypeId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mPaperCategoryType> PaperCategoryTypeList = Helper.ExecuteService("PaperLaid", "GetAllPaperCategoryType", PaperCategoryTypeListMod) as List<mPaperCategoryType>;
                tPaperLaidDepartmentModel.mPaperCategoryTypeList = PaperCategoryTypeList;

                #endregion

                tPaperLaidV objMovement = new tPaperLaidV();
                objMovement.AssemblyId = AssemblyId;
                objMovement.SessionId = SessionId;
                objMovement.PaperCategoryTypeId = PaperCategoryTypeId;
                objMovement.EventId = CategoryTypeId;
                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                objPaperModel.ResultCount = int.Parse(ResultCount);
                tPaperLaidDepartmentModel.DepartmentSubmitList = (ICollection<PaperMovementModel>)Helper.ExecuteService("PaperLaid", "GetSubmittedPaperLaidList", objPaperModel);
                if (tPaperLaidDepartmentModel.DepartmentSubmitList.Count > 0)
                {
                    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentSubmitList.Count;
                }
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetPaperLaidSubmittList", tPaperLaidDepartmentModel);
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/Index");

        }

        public ActionResult SubmitSelectedPendingList(string DetailIds)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != "")
            {
                tPaperLaidTemp model = new tPaperLaidTemp();
                string[] ids = DetailIds.Split(',');
                foreach (var id in ids)
                {
                    string[] pdId = id.Split('-');
                    model.PaperLaidId = Convert.ToInt32(pdId[0]);
                    model.PaperLaidTempId = Convert.ToInt32(pdId[1]);
                    model.DeptSubmittedBy = CurrentSession.UserName;
                    model.DeptSubmittedDate = DateTime.Now;
                    model = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SubmitPendingDepartmentPaperLaid", model);
                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public ActionResult SubmittPendingRecord(string paperLaidId, string depId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != "")
            {
                tPaperLaidTemp model = new tPaperLaidTemp();
                tPaperLaidV mod = new tPaperLaidV();
                model.PaperLaidId = Convert.ToInt32(paperLaidId);
                model.DeptSubmittedBy = CurrentSession.UserName;
                model.DeptSubmittedDate = DateTime.Now;
                model = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SubmitPendingDepartmentPaperLaid", model);
                mod = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", model);

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public ActionResult DeletePendingRecord(string paperLaidId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != "")
            {
                tPaperLaidV model = new tPaperLaidV();
                model.PaperLaidId = Convert.ToInt32(paperLaidId);
                model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "DeletePaperLaidByID", model);

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdatePaperLaidFile(HttpPostedFileBase file, HttpPostedFileBase Supfile, HttpPostedFileBase Docfile, tPaperLaidV tPaper)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != "")
            {
                tPaperLaidTemp tPaperTemp = new tPaperLaidTemp();

                // tPaper.SupDocFileName = tPaperTemp.SupDocFileName;
                PaperMovementModel obj = new PaperMovementModel();
                obj.PaperLaidId = tPaper.PaperLaidId;
                obj.FilePath = tPaper.FilePath;
                tPaperLaidV model = new tPaperLaidV();
                model.DocFileName = tPaper.DocFileName;
                //model.SupDocFileName = tPaper.SupDocFileName;
                model.SupFileName = tPaper.SupFileName;
                model.SupPDFCount = tPaper.SupPDFCount;
                model.MainDocCount = tPaper.MainDocCount;
                model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetFileVersion", obj);
                if (model != null)
                {
                    //Get Question details by Paper Laid ID.
                    tQuestion tQuestions = new tQuestion();
                    tQuestions.PaperLaidId = Convert.ToInt32(tPaper.PaperLaidId);
                    tQuestions = (tQuestion)Helper.ExecuteService("PaperLaid", "GetQuestionByPaperLaidID", tQuestions) as tQuestion;

                    if (tQuestions.QuestionType == 1)
                    {
                        //TempData["lSM"] = "SQ";
                    }
                    else
                    {
                        //TempData["lSM"] = "USQ";
                    }

                    //TempData["Msg"] = "File";
                    model.PaperLaidId = tPaper.PaperLaidId;
                    var paperLaid = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPaperLaidByPaperLaidId", model) as tPaperLaidV;

                    tPaperTemp.DocFileName = tPaper.DocFileName;

                    tPaperTemp.FileName = tPaper.FileName;
                    tPaperTemp.SupFileName = tPaper.SupFileName;
                    tPaperTemp.SupVersion = tPaper.SupFileVersion;
                    tPaperTemp.DocFileVersion = tPaper.DocFileVersion;
                    tPaper.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    tPaper.SessionId = Convert.ToInt16(CurrentSession.SessionId);

                    if (file == null)
                    {
                        string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                        string directory = Server.MapPath(url);
                        if (Directory.Exists(directory))
                        {
                            string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                            string SourceFile = savedFileName[0];
                            foreach (string page in savedFileName)
                            {
                                string name = Path.GetFileName(page);
                                string nameKey = Path.GetFileNameWithoutExtension(page);
                                string directory1 = Path.GetDirectoryName(page);
                                //
                                // Display the Path strings we extracted.
                                //
                                Console.WriteLine("{0}, {1}, {2}, {3}",
                                page, name, nameKey, directory1);
                                tPaperTemp.FileName = name;
                            }
                            string ext = Path.GetExtension(tPaperTemp.FileName);
                            string fileNam1 = tPaperTemp.FileName.Replace(ext, "");

                            string QType = string.Empty;
                            int? QuestionNo = null;

                            if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
                            {
                                QType = "S";
                            }
                            else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
                            {
                                QType = "US";
                            }

                            if (tQuestions.QuestionNumber != null)
                            {
                                QuestionNo = tQuestions.QuestionID;
                            }
                            else
                            {
                                QuestionNo = tQuestions.QuestionID;
                            }

                            tPaper.Count = model.Count;
                            string actualFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + QType + "_" + QuestionNo + "_V" + tPaper.Count + ext;



                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                            string Url = FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";
                            DirectoryInfo Dir = new DirectoryInfo(Url);
                            if (!Dir.Exists)
                            {
                                Dir.Create();
                            }

                            string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/", actualFileName);
                            string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                            System.IO.File.Copy(SourceFile, path, true);
                            tPaperTemp.FilePath = "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";

                            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                            //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                            string FileStructurePath = Acess.SettingValue;
                            tPaperTemp.FileStructurePath = FileStructurePath;
                            tPaperTemp.FileName = actualFileName;

                            tPaperTemp.Version = tPaper.Count;
                            tPaperTemp.PaperLaidId = tPaper.PaperLaidId;
                        }

                        tPaperTemp.Version = tPaper.Count;




                        if (Supfile == null)
                        {
                            string urlSupFile = "~/DepartmentPdf/SupPdf" + "/" + CurrentSession.UserID;
                            string directorySupFile = Server.MapPath(urlSupFile);
                            if (Directory.Exists(directorySupFile))
                            {
                                string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/SupPdf" + "/" + CurrentSession.UserID));
                                string SourceFile = savedFileName[0];
                                foreach (string page in savedFileName)
                                {
                                    string name = Path.GetFileName(page);
                                    string nameKey = Path.GetFileNameWithoutExtension(page);
                                    string directory1 = Path.GetDirectoryName(page);
                                    //
                                    // Display the Path strings we extracted.
                                    //
                                    Console.WriteLine("{0}, {1}, {2}, {3}",
                                    page, name, nameKey, directory1);
                                    tPaperTemp.SupFileName = name;
                                }
                                string ext = Path.GetExtension(tPaperTemp.SupFileName);
                                string fileNam1 = tPaperTemp.SupFileName.Replace(ext, "");

                                string QType = string.Empty;
                                int? QuestionNo = null;

                                if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
                                {
                                    QType = "S";
                                }
                                else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
                                {
                                    QType = "US";
                                }
                                if (tQuestions.QuestionNumber != null)
                                {
                                    QuestionNo = tQuestions.QuestionID;
                                }
                                else
                                {
                                    QuestionNo = tQuestions.QuestionID;
                                }

                                string actualSupFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + QType + "_" + QuestionNo + "_V" + model.SupPDFCount + "SUP" + ext;


                                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                string Url = FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";
                                DirectoryInfo Dir = new DirectoryInfo(Url);
                                if (!Dir.Exists)
                                {
                                    Dir.Create();
                                }

                                string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/", actualSupFileName);
                                //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                System.IO.File.Copy(SourceFile, path, true);
                                //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                tPaperTemp.FilePath = "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";

                                var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                string FileStructurePath = Acess.SettingValue;
                                tPaperTemp.FileStructurePath = FileStructurePath;
                                tPaperTemp.SupFileName = actualSupFileName;
                                tPaperTemp.SupFileName = tPaperTemp.SupFileName;
                                tPaperTemp.SupVersion = model.SupPDFCount + 1;

                            }
                            else
                            {
                                tPaperTemp.SupVersion = model.SupPDFCount;
                            }
                        }


                        if (Docfile == null)
                        {
                            string urlDocFile = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                            string directoryDocFile = Server.MapPath(urlDocFile);
                            if (Directory.Exists(directoryDocFile))
                            {
                                string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID));
                                string SourceFile = savedFileName[0];
                                foreach (string page in savedFileName)
                                {
                                    string name = Path.GetFileName(page);
                                    string nameKey = Path.GetFileNameWithoutExtension(page);
                                    string directory1 = Path.GetDirectoryName(page);
                                    //
                                    // Display the Path strings we extracted.
                                    //
                                    Console.WriteLine("{0}, {1}, {2}, {3}",
                                    page, name, nameKey, directory1);
                                    tPaperTemp.DocFileName = name;
                                }
                                string ext = Path.GetExtension(tPaperTemp.DocFileName);
                                string fileNam1 = tPaperTemp.DocFileName.Replace(ext, "");

                                string QType = string.Empty;
                                int? QuestionNo = null;

                                if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
                                {
                                    QType = "S";
                                }
                                else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
                                {
                                    QType = "US";
                                }

                                if (tQuestions.QuestionNumber != null)
                                {
                                    QuestionNo = tQuestions.QuestionID;
                                }
                                else
                                {
                                    QuestionNo = tQuestions.QuestionID;
                                }
                                //string actualDocFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + QType + "_" + QuestionNo + "_V" + model.MainDocCount + ext;
                                string actualDocFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + QType + "_" + QuestionNo + "Main" + ext;

                                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                string Url = FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";
                                DirectoryInfo Dir = new DirectoryInfo(Url);
                                if (!Dir.Exists)
                                {
                                    Dir.Create();
                                }

                                string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/", actualDocFileName);
                                //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                System.IO.File.Copy(SourceFile, path, true);
                                //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                tPaperTemp.FilePath = "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";

                                var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                string FileStructurePath = Acess.SettingValue;
                                tPaperTemp.FileStructurePath = FileStructurePath;
                                tPaperTemp.DocFileName = actualDocFileName;



                                tPaperTemp.PaperLaidId = tPaper.PaperLaidId;
                                tPaperTemp.DocFileVersion = model.MainDocCount + 1;
                                tPaperTemp.DocFileName = tPaperTemp.DocFileName;

                            }
                            else
                            {


                                tPaperTemp.DocFileVersion = model.MainDocCount;
                            }
                        }




                        tPaperTemp.SupDocFileName = tPaper.SupDocFileName;
                        var RefNumber = tPaper.AssemblyId + "/" + tPaper.SessionId + "/" + tPaperTemp.PaperLaidId + "/" + "v" + tPaperTemp.Version;
                        tPaperTemp.evidhanReferenceNumber = RefNumber;
                        tPaperTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "UpdatePaperLaidFileByID", tPaperTemp);

                    }



                    //if (file == null)
                    //{

                    //    tPaperTemp = SaveFileToDirectory(tQuestions, file, tPaperTemp, model, paperLaid);

                    //    tPaperTemp.SupVersion = model.SupPDFCount;
                    //    tPaperTemp.DocFileVersion = model.MainDocCount;

                    //    if (Supfile != null)
                    //    {
                    //        tPaperTemp = SupSaveFileToDirectory(tQuestions, Supfile, tPaperTemp, model, paperLaid);
                    //        tPaperTemp.SupFileName = tPaperTemp.SupFileName;
                    //        tPaperTemp.SupVersion = tPaperTemp.SupVersion;
                    //        // tPaperTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "UpdatePaperLaidOtherFileByID", tPaperTemp);
                    //    }
                    //    if (Docfile != null)
                    //    {
                    //        tPaperTemp = SaveDocFileToDirectory(tQuestions, Docfile, tPaperTemp, model, paperLaid);
                    //        tPaperTemp.DocFileName = tPaperTemp.DocFileName;
                    //        tPaperTemp.DocFileVersion = tPaperTemp.DocFileVersion;
                    //    }
                    //    tPaperTemp.SupDocFileName = tPaper.SupDocFileName;
                    //    tPaperTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "UpdatePaperLaidFileByID", tPaperTemp);
                    //}



                    tPaperLaidV mod = new tPaperLaidV();
                    mod = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", tPaperTemp);


                    string url1 = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                    string MainPDFdirectory1 = Server.MapPath(url1);
                    if (Directory.Exists(MainPDFdirectory1))
                    {
                        System.IO.Directory.Delete(MainPDFdirectory1, true);
                    }
                    string urlMainDoc = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                    string MainDocdirectory = Server.MapPath(urlMainDoc);
                    if (Directory.Exists(MainDocdirectory))
                    {
                        System.IO.Directory.Delete(MainDocdirectory, true);
                    }

                    string SupPDFurl1 = "~/DepartmentPdf/SupPdf" + "/" + CurrentSession.UserID;
                    string SupPDFdirectory1 = Server.MapPath(SupPDFurl1);
                    if (Directory.Exists(SupPDFdirectory1))
                    {
                        System.IO.Directory.Delete(SupPDFdirectory1, true);
                    }
                    string SupDocurl = "~/DepartmentPdf/SuppReplyDoc" + "/" + CurrentSession.UserID;
                    string SupDocdirectory = Server.MapPath(SupDocurl);
                    if (Directory.Exists(SupDocdirectory))
                    {
                        System.IO.Directory.Delete(SupDocdirectory, true);
                    }

                    // TempData["valID"] = 1;
                }
            }

            if (Request.IsAjaxRequest())
            {
                tPaperLaidV mod = new tPaperLaidV();
                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", mod);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        mod.DepartmentId = item.AssociatedDepts;
                        mod.IsPermissions = item.IsPermissions; ;
                    }

                    if (mod.DepartmentId == null)
                    {
                        mod.DepartmentId = CurrentSession.DeptID;
                    }
                }
                else
                {
                    mod.DepartmentId = CurrentSession.DeptID;
                }
                mod.ModuleId = 6;


                return Json(mod, JsonRequestBehavior.AllowGet);
            }
            else
            {
            }
            return null;
            //return RedirectToAction("DepartmentDashboard");
        }

        public ActionResult EditPendingRecord(string paperLaidId, string SessionId, string MobileNumber, string ModuleId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            string OTP = CurrentSession.UserID;
            string sub = OTP.Substring(0, 1);
            if (sub == "T")
            {
                if (CurrentSession.UserID != "")
                {
                    tPaperLaidV model = new tPaperLaidV();



                    model.PaperLaidId = Convert.ToInt32(paperLaidId);
                    model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "EditPendingPaperLaidDetials", model);



                    mDepartment departmentModel = new mDepartment();
                    mEvent mEventModel = new mEvent();

                    model.SessionId = Convert.ToInt32(SessionId);
                    model.MobileNo = MobileNumber;
                    //	model.QuestionTypeId = Convert.ToInt32(QuestionTypeID);
                    mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                    model.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("PaperLaid", "GetSessionDates", model);
                    model.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("PaperLaid", "GetDocumentType", model);
                    model.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
                    model.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
                    //model.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistry", mMinisteryMinisterModel);

                    tPaperLaidV paperLaidModel = new tPaperLaidV();

                    // paperLaidModel.DepartmentId = CurrentSession.DeptID;

                    if (paperLaidId != "")
                    {
                        paperLaidModel.PaperLaidId = int.Parse(paperLaidId);
                        string dptId = (string)Helper.ExecuteService("PaperLaid", "getDepartmentIdByPaperId", paperLaidModel);
                        if (dptId != "")
                        {
                            paperLaidModel.DepartmentId = dptId;
                        }
                        else
                        {
                            paperLaidModel.DepartmentId = CurrentSession.DeptID;
                        }
                    }


                    model.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", paperLaidModel);

                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    model.AssemblyId = Convert.ToInt32(siteSettingMod.AssemblyCode);
                    model.mEvents.Add(mEventModel);

                    //Get Question details by Paper Laid ID.
                    tQuestion questionDet = new tQuestion();
                    questionDet.PaperLaidId = Convert.ToInt32(paperLaidId);
                    questionDet.DepartmentID = paperLaidModel.DepartmentId;
                    questionDet = (tQuestion)Helper.ExecuteService("PaperLaid", "GetQuestionByPaperLaidID", questionDet) as tQuestion;
                    if (questionDet != null)
                    {
                        if (questionDet.IsFinalApproved == true)
                            model.QuestionNumber = questionDet.QuestionNumber;
                        else
                            model.QuestionNumber = (int?)null;

                        model.QuestionID = questionDet.QuestionID;
                        model.QuestionTypeId = questionDet.QuestionType;
                        model.DiaryNumber = questionDet.DiaryNumber;
                        model.SessionDateId = questionDet.SessionDateId;
                        model.MergeDiaryNo = questionDet.MergeDiaryNo;
                        // model.QtypeId = questionDet.QtypeId;
                        if (questionDet.IsFinalApproved == true)
                        {
                            if (questionDet.SessionDateId != null)
                            {
                                model.DesireLayingDateId = Convert.ToInt32(questionDet.SessionDateId);
                            }
                        }
                        else
                        {
                            model.DesireLayingDateId = 0;
                        }
                    }
                    model.ModuleId = Convert.ToInt32(ModuleId);
                    return PartialView("_GetPaperEnrtyOTP", model);
                }
            }
            else
            {
                if (CurrentSession.UserID != "")
                {
                    tPaperLaidV model = new tPaperLaidV();

                    model.PaperLaidId = Convert.ToInt32(paperLaidId);
                    model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "EditPendingPaperLaidDetials", model);


                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);

                    var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);


                    string FileStructurePath = Acess.SettingValue;
                    model.FileStructurePath = FileStructurePath;

                    mDepartment departmentModel = new mDepartment();
                    mEvent mEventModel = new mEvent();

                    model.SessionId = Convert.ToInt32(SessionId);
                    model.MobileNo = MobileNumber;
                    //	model.QuestionTypeId = Convert.ToInt32(QuestionTypeID);
                    mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                    model.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("PaperLaid", "GetSessionDates", model);
                    model.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("PaperLaid", "GetDocumentType", model);
                    model.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
                    model.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
                    //model.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistry", mMinisteryMinisterModel);

                    tPaperLaidV paperLaidModel = new tPaperLaidV();

                    // paperLaidModel.DepartmentId = CurrentSession.DeptID;

                    if (paperLaidId != "")
                    {
                        paperLaidModel.PaperLaidId = int.Parse(paperLaidId);
                        string dptId = (string)Helper.ExecuteService("PaperLaid", "getDepartmentIdByPaperId", paperLaidModel);
                        if (dptId != "")
                        {
                            paperLaidModel.DepartmentId = dptId;
                        }
                        else
                        {
                            paperLaidModel.DepartmentId = CurrentSession.DeptID;
                        }
                    }


                    model.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", paperLaidModel);

                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    model.AssemblyId = Convert.ToInt32(siteSettingMod.AssemblyCode);
                    model.mEvents.Add(mEventModel);

                    //Get Question details by Paper Laid ID.
                    tQuestion questionDet = new tQuestion();
                    questionDet.PaperLaidId = Convert.ToInt32(paperLaidId);
                    questionDet.DepartmentID = paperLaidModel.DepartmentId;
                    questionDet = (tQuestion)Helper.ExecuteService("PaperLaid", "GetQuestionByPaperLaidID", questionDet) as tQuestion;
                    if (questionDet != null)
                    {
                        if (questionDet.IsFinalApproved == true)
                            model.QuestionNumber = questionDet.QuestionNumber;
                        else
                            model.QuestionNumber = (int?)null;

                        model.QuestionID = questionDet.QuestionID;
                        model.QuestionTypeId = questionDet.QuestionType;
                        model.DiaryNumber = questionDet.DiaryNumber;
                        model.SessionDateId = questionDet.SessionDateId;
                        model.MergeDiaryNo = questionDet.MergeDiaryNo;
                        model.MemberName = questionDet.RMemberName;

                        if (questionDet.IsFinalApproved == true)
                        {
                            if (questionDet.SessionDateId != null)
                            {
                                model.DesireLayingDateId = Convert.ToInt32(questionDet.SessionDateId);
                            }
                        }
                        else
                        {
                            model.DesireLayingDateId = 0;
                        }
                    }
                    model.ModuleId = Convert.ToInt32(ModuleId);
                    return PartialView("_GetPaperEntryForDashBoard", model);
                }
            }


            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/Index");
        }

        public ActionResult ShowPaperLaidDetailByID(string paperLaidId)
        {
            tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);

                var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);


                string FileStructurePath = Acess.SettingValue;
                movementModel.FileStructurePath = FileStructurePath;

                tPaperLaidDepartmentModel.DepartmentPendingList = (ICollection<PaperMovementModel>)Helper.ExecuteService("PaperLaid", "GetMemberNameByIDs", movementModel);

                foreach (var item in tPaperLaidDepartmentModel.DepartmentPendingList)
                {
                    movementModel.DepartmentId = item.DepartmentId;
                    movementModel.MemberName = item.MemberName;
                    movementModel.AssemblyId = item.AssemblyId;
                    movementModel.SessionId = item.AssemblyId;
                    movementModel.DiaryNumber = item.DiaryNumber;
                    movementModel.Number = item.Number;
                    movementModel.Title = item.Title;
                    movementModel.DeparmentName = item.DeparmentName;
                    movementModel.MinisterName = item.MinisterName;
                    movementModel.PaperCategoryTypeName = item.PaperCategoryTypeName;
                    movementModel.QuestionNumber = item.QuestionNumber;
                    movementModel.Description = item.Description;
                    movementModel.DesireLayingDate = item.DesireLayingDate;
                    movementModel.ProvisionUnderWhich = item.ProvisionUnderWhich;
                    movementModel.Remark = item.Remark;
                    movementModel.ReplyPathFileLocation = item.ReplyPathFileLocation;
                    movementModel.Status = item.Status;
                    movementModel.PaperLaidTempId = item.PaperLaidTempId;
                    movementModel.version = item.version;
                    movementModel.IsClubbed = item.IsClubbed;
                    movementModel.MergeDiaryNo = item.MergeDiaryNo;
                    movementModel.DocFileName = item.DocFileName;
                    movementModel.SupFileName = item.SupFileName;
                    movementModel.SupDocFileName = item.SupDocFileName;
                    movementModel.SupFilePath = item.SupFilePath;
                    movementModel.SupFileVersion = item.SupFileVersion;
                    movementModel.AuditTrialList = item.AuditTrialList;
                }

                // movementModel = (PaperMovementModel)Helper.ExecuteService("PaperLaid", "ShowPaperLaidDetailByID", movementModel);

                MovementDetailsCustom objMovement = new MovementDetailsCustom();
                objMovement.DepartmentId = movementModel.DepartmentId;
                objMovement.AssemblyId = movementModel.AssemblyId;
                objMovement.SessionId = movementModel.SessionId;
                objMovement.Number = movementModel.Number;
                objMovement.PaperCategoryTypeId = movementModel.PaperCategoryTypeId;

                movementModel.ListMovementDetails = (List<MovementDetailsCustom>)Helper.ExecuteService("MovementDetails", "GetMovementDetailsByIds", objMovement);
                return PartialView("_GetFullRecord", movementModel);
            }
            return null;
        }

        public ActionResult GetQuestionDetailsByQuestionID(string questionID)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                tQuestionModel tQuestionModel = new tQuestionModel();
                mchangeDepartmentAuditTrail changeDep = new mchangeDepartmentAuditTrail();
                tQuestionModel.QuestionID = Convert.ToInt32(questionID);
                tQuestionModel = (tQuestionModel)Helper.ExecuteService("Questions", "GetQuestionDetailsDepartmentByID", tQuestionModel);
                // tQuestionModel.mQuesList = (ICollection<tQuestionModel>)Helper.ExecuteService("Questions", "GetQuestionDetailsDepartmentByID", tQuestionModel);



                foreach (var item in tQuestionModel.objQuestList1)
                {
                    tQuestionModel.DepartmentId = item.DepartmentId;
                    tQuestionModel.MemberN = item.MemberN;
                    tQuestionModel.AssemblyId = item.AssemblyId;
                    tQuestionModel.SessionId = item.SessionId;
                    tQuestionModel.DiaryNumber = item.DiaryNumber;

                    tQuestionModel.SubmittedDate = item.SubmittedDate;
                    tQuestionModel.ConstituencyName = item.ConstituencyName;
                    tQuestionModel.DepartmentN = item.DepartmentN;
                    tQuestionModel.MinisterN = item.MinisterN;
                    tQuestionModel.Subject = item.Subject;
                    changeDep.DiaryNumber = item.DiaryNumber;
                    if (item.IsFinalApproved == true)
                    {
                        tQuestionModel.QuestionNumber = item.QuestionNumber;
                    }

                    tQuestionModel.MainQuestion = item.MainQuestion;
                    tQuestionModel.RMemberName = item.RMemberName;
                    tQuestionModel.IsClubbed = item.IsClubbed;
                    tQuestionModel.QtypeId = item.QtypeId;
                    tQuestionModel.StatusId = item.StatusId;
                    tQuestionModel.MergeDiaryNo = item.MergeDiaryNo;
                    tQuestionModel.IsBracket = item.IsBracket;
                    tQuestionModel.IsAcknowledgmentDate = item.IsAcknowledgmentDate;

                    changeDep = (mchangeDepartmentAuditTrail)Helper.ExecuteService("PaperLaid", "GetLastChangeDepartment", changeDep);
                    tQuestionModel.UserName = changeDep.UserName;
                    tQuestionModel.ChangedDateString = changeDep.ChangedDateString;
                }
                return PartialView("_GetFullRecordForDashboard", tQuestionModel);
            }
            return null;
        }

        public ActionResult GetSubmittedPaperLaidByID(string paperLaidId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                tPaperLaidV PaperLaidModel = new tPaperLaidV();
                PaperLaidModel.PaperLaidId = Convert.ToInt64(paperLaidId);


                var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                string FileStructurePath = Acess.SettingValue;
                PaperLaidModel.FileStructurePath = FileStructurePath;
                PaperLaidModel = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetSubmittedPaperLaidByID", PaperLaidModel);

                //New.
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("PaperLaid", "ShowPaperLaidDetailByID", movementModel);
                PaperLaidModel.paperMovementModel = movementModel;
                PaperLaidModel.QuestionTypeId = movementModel.QuestionTypeId;
                PaperLaidModel.DocFileName = movementModel.DocFileName;
                PaperLaidModel.SupFileName = movementModel.SupFileName;

                PaperLaidModel.SupDocFileName = movementModel.SupDocFileName;
                PaperLaidModel.DocFilePath = movementModel.DocFilePath;
                PaperLaidModel.PaperLaidTempId = movementModel.PaperLaidTempId;
                PaperLaidModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                PaperLaidModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                // PaperLaidModel.DocFileVersion = movementModel.DocFileVersion;
                return PartialView("_GetSubmittedListById", PaperLaidModel);
            }
            return null;
        }
        //added code venkat for dynamic 
        public ActionResult DepartmentDashboard()
        {
            //CurrentSession.UserID = "8B69A0BD-C553-4070-A760-66C279B7419B";
            if (CurrentSession.UserID == null || CurrentSession.UserID =="" ) { return RedirectToAction("Login", "Account"); }
            int number;
            if (int.TryParse(CurrentSession.MemberCode,out number)==false)
            {
                CurrentSession.MemberCode = "0";
            }
            
            mUsers objmUsers = new mUsers();
            objmUsers.UserId = Guid.Parse(CurrentSession.UserID);
            var Result = Helper.ExecuteService("User", "GetUserDeptDetailsByUserID", objmUsers) as mUsers;

            if (Result.IsSecChange == true && Result.IsSecretoryId == false && Result.UserType == 3)
            {
                return Content("<script>alert('Your Secretary has been changed,Please Setup your profile');window.location.assign('/UserManagement/UserRequest');</script>");
            }

            tPaperLaidV model = new tPaperLaidV();

            var Heading = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetHeadingSetting", null);

            model.Heading = Heading.SettingValue;
            model.HeadingLocal = Heading.SettingValueLocal;

            //Get the Total count of All Type of question.
            //added code venkat for dynamic
            model.UserID = new Guid(CurrentSession.UserID);
            //end--------------------------------------------------
            if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
            {
                model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            }
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
            {
                model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model);
            }
            else
            {
                model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValueLocal", model);
            }
            Session["MenuList"] = model;
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);

            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            //AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }
            else
            {
                model.DepartmentId = CurrentSession.DeptID;
            }

            /*End*/

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;

            //model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetCountForOtherPaperLaidTypes", model);
            //model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidCounters", model);

            mDepartment deptInfo1 = new mDepartment();
            deptInfo1.deptId = model.DepartmentId;

            List<mDepartment> deptInfo = new List<mDepartment>();

            //model = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetCountForBillsandNotice", model);

            model.CurrentUserName = CurrentSession.UserName;

            model.UserDesignation = CurrentSession.MemberDesignation;


            mSession Mdl = new mSession();
            Mdl.SessionCode = model.SessionId;
            Mdl.AssemblyID = model.AssemblyCode;
            Mdl.AssemblyID = model.AssemblyId;
            Mdl.SessionID = model.SessionId;

            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = model.AssemblyCode;
            assmblyMdl.AssemblyID = model.AssemblyId;
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
            {
                //model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                //model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
                //model.mSessionDate = (List<mSessionDate>)Helper.ExecuteService("PaperLaid", "GetSessionDate", model);
                deptInfo = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartmentByIDs", model);
                // deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;


                foreach (var item in deptInfo)
                {
                    if (item != null)
                    {
                        model.DeparmentNameByids += item.deptname + ", ";
                    }


                }

                if (model.DeparmentNameByids != null && model.DeparmentNameByids.LastIndexOf(",") > 0)
                {
                    model.DeparmentNameByids = model.DeparmentNameByids.Substring(0, model.DeparmentNameByids.Length - 2);
                }


            }
            else
            {
                //model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameLocalBySessionCode", Mdl);
                //model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameLocalByAssemblyCode", assmblyMdl);


                deptInfo = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartmentByIDsLocal", model);

                foreach (var item in deptInfo)
                {
                    model.DeparmentNameByids += item.deptnameLocal + ", ";

                }

                if (model.DeparmentNameByids.LastIndexOf(",") > 0)
                {
                    model.DeparmentNameByids = model.DeparmentNameByids.Substring(0, model.DeparmentNameByids.Length - 2);
                }


            }

            if (Request.IsAjaxRequest())
            {
                return Json(new int[] { model.TotalStaredPendingForSubmissionNew, model.TotalUnStaredPendingForSubmissionNew, model.NoticeReplayDraft, model.TotalBillDraftCount, model.OtherPaperCount, model.TotalStaredSubmitted, model.TotalUnstaredSubmitted, model.TotalUnstaredQuestions, model.TotalStaredReceived, model.TotalStaredQuestions, model.TotalNoticesCount, model.TotalBillSentCount, model.Count, model.TotalBillSentCount }, JsonRequestBehavior.AllowGet);
            }


            else
            {
                return View(model);
            }
        }

        public JsonResult GetSessionByAssemblyId(int AssemblyId)
        {
            mSession mdl = new mSession();
            List<mSession> SessLst = new List<mSession>();
            if (AssemblyId != 0)
            {
                mdl.AssemblyID = AssemblyId;
                SessLst = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", mdl);
            }
            return Json(SessLst, JsonRequestBehavior.AllowGet);
        }

        #region Stared Questions
        [HttpPost]
        public ActionResult GetAllStarredQuestions(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_AllQuestionsStarred", model);
        }

        public ActionResult PendingStaredQuestion(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_PendingStaredQuestion", model);
        }

        public ActionResult PendingForsubmissionStared(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_PendingForsubmissionStared", model);
        }

        public ActionResult SubmittedStaredQuestion(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_SubmittedStaredQuestion", model);
        }
        [HttpPost]
        public ActionResult StarredUpcomingLOB(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_StarredUpcomingLOB", model);
        }
        [HttpPost]
        public ActionResult LaidInHouseStared(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_LaidInHouseStared", model);
        }
        [HttpPost]
        public ActionResult PendingToLayStared(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_PendingToLayStared", model);
        }

        #endregion

        #region Un-Stared Questions.
        [HttpPost]
        public ActionResult GetAllUnstarredQuestions(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_AllQuestionsUnstarred", model);
        }
        [HttpPost]
        public ActionResult PendingUnstaredQuestion(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_PendingUnstaredQuestion", model);
        }
        [HttpPost]
        public ActionResult PendingForsubmissionUnstared(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_PendingForsubmissionUnstared", model);
        }
        [HttpPost]
        public ActionResult SubmittedUnstaredQuestion(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_SubmittedUnstaredQuestion", model);
        }

        public ActionResult UnstarredUpcomingLOB(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_UnstarredUpcomingLOB", model);
        }
        [HttpPost]
        public ActionResult LaidInHouseUnstared(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_LaidInHouseUnstared", model);
        }
        [HttpPost]
        public ActionResult PendingToLayUnstared(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_PendingToLayUnstared", model);
        }

        #endregion

        #region Grid Binder Starred Question
        public ActionResult GetAllStarredQuestionsGrid(JqGridRequest request, tPaperLaidV customModel)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;

            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.StartedQuestion;


            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }

            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetAllQuestions", model);



            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<QuestionModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<QuestionModelCustom>(Convert.ToString(questionList.QuestionID), new QuestionModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        //added code venkat for dynamic 
        [HttpPost]
        public ActionResult PendingStaredQuestionGrid(JqGridRequest request, string Diarynumber, string title, int? number, int? MemberId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PageIndex = request.PageIndex + 1;
            //added code venkat for dynamic 
            model.DiaryNumber = Diarynumber;
            model.Title = title;
            model.QuestionNumber = number ?? 0;
            //int  MemberId = Convert.ToInt32(membername);
            model.MemberId = MemberId;
            //End---------------------------
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;

            //Get Current Session and Assembly Information.
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyId = siteSettingMod.AssemblyCode;
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }

            }

            /*End*/

            //var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
            //var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

            ////string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
            //string FileStructurePath = Acess.SettingValue;
            //model.FileStructurePath = FileStructurePath + NewsSettings.SettingValue;



            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPendingQuestionsByType", model);

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPendingQuestionsByType", model);

            List<QuestionModelCustom> customQuestions = new List<QuestionModelCustom>();
            customQuestions.AddRange(model.tQuestionModel);

            foreach (var item in model.tQuestionModel)
            {

                result.MemberName = item.MemberName;

            }


            tPaperLaidV PendingForSub = new tPaperLaidV();
            PendingForSub = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPendingForSubQuestionsByType", model);

            customQuestions.AddRange(PendingForSub.tQuestionModel);
            var totalCount = model.ResultCount + PendingForSub.ResultCount;

            model.tQuestionModel = customQuestions;

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)totalCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = totalCount
            };

            var resultToSort = customQuestions.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<QuestionModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<QuestionModelCustom>(Convert.ToString(questionList.QuestionID), new QuestionModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        [HttpPost]
        public ActionResult PendingForsubmissionStaredGrid(JqGridRequest request)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;

            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPendingForSubQuestionsByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<QuestionModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<QuestionModelCustom>(Convert.ToString(questionList.QuestionID), new QuestionModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        //added code venkat for dynamic 
        public ActionResult SubmittedStaredQuestionGrid(JqGridRequest request, string Diarynumber, string title, int? number, int? MemberId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            //added code venkat for dynamic 
            model.DiaryNumber = Diarynumber;
            model.Title = title;
            model.QuestionNumber = number ?? 0;
            model.MemberId = MemberId;
            //end---------------------------------
            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/
            //var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);

            //var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);


            //string FileStructurePath = Acess.SettingValue;
            //model.FileStructurePath = FileStructurePath + NewsSettings.SettingValue;


            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetSubmittedQuestionsByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionSendModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult StarredUpcomingLOBGrid(JqGridRequest request)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;

            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }
            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }
            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpcomingLOBByQuestionType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionSendModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult LaidInHouseStaredGrid(JqGridRequest request)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;

            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetLaidInHouseQuestionsByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionSendModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult PendingToLayStaredGrid(JqGridRequest request)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;

            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;


            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPendingToLayQuestionsByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionSendModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        #endregion

        #region Grid Binder Unstarred Question

        public ActionResult GetAllUnstarredQuestionsGrid(JqGridRequest request, tPaperLaidV customModel)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;

            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;


            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetAllQuestions", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<QuestionModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<QuestionModelCustom>(Convert.ToString(questionList.QuestionID), new QuestionModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        //added code venkat for dynamic 
        public ActionResult PendingUnstaredQuestionGrid(JqGridRequest request, string Diarynumber, string title, int? number, int? MemberId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            //added code venkat for dynamic 
            model.DiaryNumber = Diarynumber;
            model.Title = title;
            model.QuestionNumber = number ?? 0;
            //int  MemberId = Convert.ToInt32(membername);
            model.MemberId = MemberId;
            //end------------------------------
            //Get Current Session and Assembly Information.
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyId = siteSettingMod.AssemblyCode;


            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPendingQuestionsByType", model);

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPendingQuestionsByType", model);

            List<QuestionModelCustom> customQuestions = new List<QuestionModelCustom>();
            customQuestions.AddRange(model.tQuestionModel);

            tPaperLaidV PendingForSub = new tPaperLaidV();
            PendingForSub = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPendingForSubQuestionsByType", model);

            customQuestions.AddRange(PendingForSub.tQuestionModel);
            var totalCount = model.ResultCount + PendingForSub.ResultCount;

            model.tQuestionModel = customQuestions;

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)totalCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = totalCount
            };

            var resultToSort = customQuestions.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<QuestionModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<QuestionModelCustom>(Convert.ToString(questionList.QuestionID), new QuestionModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult PendingForsubmissionUnstaredGrid(JqGridRequest request)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;

            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;


            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPendingForSubQuestionsByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<QuestionModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<QuestionModelCustom>(Convert.ToString(questionList.QuestionID), new QuestionModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        //added code venkat for dynamic 
        public ActionResult SubmittedUnstaredQuestionGrid(JqGridRequest request, string Diarynumber, string title, int? number, int? MemberId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            //added code venkat for dynamic 
            model.DiaryNumber = Diarynumber;
            model.Title = title;
            model.QuestionNumber = number ?? 0;
            model.MemberId = MemberId;
            //end------------------------------------
            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }


            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            //var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);

            //var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);


            //string FileStructurePath = Acess.SettingValue;
            //model.FileStructurePath = FileStructurePath + NewsSettings.SettingValue;

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetSubmittedQuestionsByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionSendModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        [HttpPost]
        public ActionResult UnstarredUpcomingLOBGrid(JqGridRequest request)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;

            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpcomingLOBByQuestionType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionSendModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult LaidInHouseUnstaredGrid(JqGridRequest request)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;

            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }


            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetLaidInHouseQuestionsByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<QuestionModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<QuestionModelCustom>(Convert.ToString(questionList.QuestionID), new QuestionModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult PendingToLayUnstaredGrid(JqGridRequest request)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;

            //Get Current Session and Assembly Information
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;


            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPendingToLayQuestionsByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionSendModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        #endregion

        public ActionResult GetFullQuestionDetails(string paperLaidId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("PaperLaid", "ShowPaperLaidDetailByID", movementModel);
                return PartialView("_GetFullQuestionDetails", movementModel);
            }
            return null;
        }

        /* Sending starred Paper By Secretary*/
        [HttpPost]
        public ActionResult PendingForsendingStared(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();

            SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyCode = siteSettingMod.AssemblyCode;

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.DSCApplicable = siteSettingMod.DSCApplicable;

            return PartialView("_PendingForSendingStarred", model);
        }
        //added code venkat for dynamic 
        public ActionResult PendingForsendingStaredJqGrid(JqGridRequest request, string Diarynumber, string title, int? number, int? MemberId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            //added code venkat for dynamic
            model.DiaryNumber = Diarynumber;
            model.Title = title;
            model.QuestionNumber = number ?? 0;
            model.MemberId = MemberId;
            //End-------------------------------
            //Get Current Session and Assembly Information
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.StartedQuestion;


            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "SendingPendingForSubQuestionsByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionDraftModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperDraftModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<PaperDraftModelCustom>(Convert.ToString(questionList.QuestionID), new PaperDraftModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };


        }


        /* Sending UnStarred Paper By Secretary*/
        [HttpPost]
        public ActionResult PendingForsendingUnStared(string AssemblyId, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            model.DSCApplicable = siteSettingMod.DSCApplicable;

            return PartialView("_PendingForSendingUnStarred", model);
        }
        //added code venkat for dynamic 
        public ActionResult PendingForsendingUnStaredJqGrid(JqGridRequest request, string Diarynumber, string title, int? number, int? MemberId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            //Added code venkat for dynamic
            model.DiaryNumber = Diarynumber;
            model.DiaryNumber = Diarynumber;
            model.Title = title;
            model.QuestionNumber = number ?? 0;
            model.MemberId = MemberId;
            //End------------------------------
            //Get Current Session and Assembly Information
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "SendingPendingForSubQuestionsByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionDraftModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperDraftModelCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<PaperDraftModelCustom>(Convert.ToString(questionList.QuestionID), new PaperDraftModelCustom(questionList)));

            return new JqGridJsonResult() { Data = response };


        }

        public ActionResult ShowSendPaperLaidDetailByID(string paperLaidId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("PaperLaid", "ShowPapertoSendLaidDetailByID", movementModel);
                return PartialView("_GetFullSendRecord", movementModel);
            }
            return null;
        }

        #region "DSC Sign"

        public ActionResult SubmitSelectedPaper(string Ids, string lSMCode)
        {

            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

            if (lSMCode != null && lSMCode != "")
            {
                if (lSMCode == "SQ")
                {
                    TempData["lSM"] = "SQ";
                }
                else if (lSMCode == "USQ")
                {
                    TempData["lSM"] = "USQ";
                }
                else if (lSMCode == "NTC")
                {
                    TempData["lSM"] = "NTC";
                }
                else if (lSMCode == "BLL")
                {
                    TempData["lSM"] = "BLL";
                }
                else if (lSMCode == "OPT")
                {
                    TempData["lSM"] = "OPT";
                }


            }

            if (Ids != null && Ids != "")
            {

                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetDepartmentPaperAttachmentByIds", Ids);

                string path = "";
                string Files = "";
                string Prifix = "";
                foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
                {
                    path = item.FilePath + item.FileName;
                    path = path.Replace(" ", "%20");
                    var test = Request.Url.AbsoluteUri;
                    string[] abc = test.Split('/');
                    Prifix = abc[0] + "//" + abc[2];
                    if (Files == "")
                    {
                        Files = Prifix + path;
                    }
                    else
                    {
                        Files = Files + "," + Prifix + path;
                    }
                }

                mMinisteryMinisterModel.FilePath = Files;
                mMinisteryMinisterModel.HashKey = CurrentSession.UserHashKey;
                mMinisteryMinisterModel.ForSave = Prifix + "/PaperLaidDepartment/PaperLaidDepartment/SubmitedSignIntention?PaperLaidId=" + Ids;

                mMinisteryMinisterModel.Message = Prifix + "/PaperLaidDepartment/PaperLaidDepartment/UpdateInfoSignPDF?PaperLaidId=" + Ids + "&Uid=" + CurrentSession.UserName;

                string session = Convert.ToString(Session["Ids"]);
            }
            return PartialView("_SignSecretaryFile", mMinisteryMinisterModel);
        }

        [SBLAuthorize(Allow = "ALL")]
        public ActionResult SubmitedSignIntention(HttpPostedFileBase file, string PaperLaidId)
        {
            PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);
            tPaperLaidV model = new tPaperLaidV();
            if (file != null)
            {


                //string filename = System.IO.Path.GetFileName(file.FileName);
                //bool folderExists = Directory.Exists(Server.MapPath(file.FileName));

                //string AssemblyId = "";
                //string SessionId = "";

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                // model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);




                //if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                //{
                //    model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                //}

                //if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                //{
                //    model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                //}



                string Url = "/PaperLaid/" + model.AssemblyId + "/" + model.SessionId + "/" + "/Signed/";
                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                if (!Dir.Exists)
                {
                    Dir.Create();
                }
                string ext = Path.GetExtension(file.FileName);
                string fileName = file.FileName.Replace(ext, "");


                file.SaveAs(Server.MapPath(Url + fileName + ext));


                //string path = System.IO.Path.Combine(Server.MapPath("~/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/"), filename);

                //int indexof = path.LastIndexOf("\\");

                //string directory = path.Substring(0, indexof);
                ////  string directory = Server.MapPath(path);
                //if (!System.IO.Directory.Exists(directory))
                //{
                //    System.IO.Directory.CreateDirectory(directory);
                //}

                //file.SaveAs(path);


                return null;
            }

            return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment", message = "Sign completed" });
        }

        [SBLAuthorize(Allow = "ALL")]
        public ActionResult UpdateInfoSignPDF(string PaperLaidId, string Uid)
        {
            PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);

            Uid = Sanitizer.GetSafeHtmlFragment(Uid);
            tPaperLaidV model = new tPaperLaidV();
            tPaperLaidTemp pT = new tPaperLaidTemp();
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }


            pT.FilePath = PaperLaidId;
            pT.MinisterSubmittedDate = DateTime.Now;
            pT.DeptSubmittedDate = DateTime.Now;
            pT.DeptSubmittedBy = Convert.ToString(Uid);

            string AssemblyId = "";
            string SessionId = "";

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            pT.AssemblyId = AssemblyId;
            pT.SessionId = SessionId;

            var modelReturned = Helper.ExecuteService("PaperLaid", "InsertSignPathAttachment", pT) as tPaperLaidTemp;

            var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateDepartmentMinisterActivePaperId", PaperLaidId) as tPaperLaidTemp;

            TempData["Msg"] = "Sign";

            return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment", message = "Sign completed" });
        }

        private string alert(string p)
        {
            throw new NotImplementedException();
        }

        #endregion
        public Boolean ShowPaperLaidHtml(string SessionDate, int SesCode, int AssCode)
        {

            DateTime sessDate = Convert.ToDateTime(SessionDate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            List<AdminLOB> ListAdminLOB = new List<AdminLOB>();
            //ListAdminLOB = _LOBRepository.GetPapersLaid(sessDate, SesCode, AssCode);
            ListAdminLOB = (List<AdminLOB>)Helper.ExecuteService("PaperLaid", "GetPapersLaid", new AdminLOB { SessionDate = Convert.ToDateTime(SessionDate), SessionId = SesCode, AssemblyId = AssCode });
            //ListAdminLOB = (List<AdminLOB>)Helper.ExecuteService("", "", SessionDate, SesCode, AssCode));
            if (ListAdminLOB.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public ActionResult MainDepartmentDashboard()
        {
            tPaperLaidV model = new tPaperLaidV();
            mSessionDate sessDate = new mSessionDate();
            //Get the Total count of All Type of question.
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                sessDate.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                sessDate.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            //model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }

            /*End*/
            List<mSessionDate> mSessionDate = new List<mSessionDate>();

            //  model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDepartmentDashboardItemsCounter", model);
            List<tPaperLaidV> ListtPaperLaidV = new List<tPaperLaidV>();

            mSessionDate = (List<mSessionDate>)Helper.ExecuteService("PaperLaid", "GetSessionDate", null);
            for (int i = 0; i < mSessionDate.Count; i++)
            {
                tPaperLaidV tpaperlaid = new tPaperLaidV();
                tpaperlaid.SessionDate = mSessionDate[i].SessionDate;
                tpaperlaid.ShowLOB = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "GetShowLob", mSessionDate[i].Id);
                tpaperlaid.ShowSQuestion = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showSQuestion", mSessionDate[i].Id);
                tpaperlaid.ShowUnQuestion = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showUnQuestion", mSessionDate[i].Id);
                tpaperlaid.ShowBillPassedorIntroduced = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showBillPassedorIntroduced", mSessionDate[i].Id);
                tpaperlaid.ShowBreifOfProceeding = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showBreifOfProceeding", mSessionDate[i].Id);
                tpaperlaid.ShowHouseProceeding = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showHouseProceeding", mSessionDate[i].Id);
                tpaperlaid.PostUponQuestion = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showPostUpon", mSessionDate[i].Id);
                //int DateCount = mSessionDate.Count;

                //int LocalCount = i + 1;
                //tpaperlaid.IsShowPaid = ShowPaperLaidHtml((mSessionDate[i].SessionDate).ToString("dd/MM/yyyy"),model.SessionId,model.AssemblyId);

                model.ListtPaperLaidV.Add(tpaperlaid);
            }

            var assemblyid = (List<SiteSettings>)Helper.ExecuteService("PaperLaid", "GetAssemblyId", null);

            var sessionid = (List<SiteSettings>)Helper.ExecuteService("PaperLaid", "GetSessionId", null);
            StringBuilder sb = new StringBuilder();
            sb.Append(sessionid.FirstOrDefault().SettingName + ",");
            sb.Append(assemblyid.FirstOrDefault().SettingName + ",");

            model.ShowProvisonalCalender = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showProvisonalCalender", sessDate);
            model.ShowRotationalMinister = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showRotationalMinister", sessDate);//sb.ToString());


            mUsers user = new mUsers();
            user.UserId = new Guid(CurrentSession.UserID);
            user.IsMember = CurrentSession.IsMember;

            user = (mUsers)Helper.ExecuteService("User", "GetIsMemberDetails", user);
            if (user != null)
            {

                model.CurrentUserName = user.Name;

            }
            return PartialView("_MainDashboard", model);

        }


        public ActionResult GetAssemblyFilePdf(string filepathName)
        {
            try
            {

                //   var filephyacesSetting = _siteRepository.GetFileAcessPhySettings().SettingValue;
                //  var filephyfileacessingpath = filepathName.Replace('/', '\\');
                Stream stream = null;

                //int bytesToRead = 10000;
                //byte[] buffer = new Byte[bytesToRead];
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(filepathName);
                fileReq.Method = "GET";
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                //if (fileReq.ContentLength > 0)
                //  fileResp.ContentLength = fileReq.ContentLength;

                stream = fileResp.GetResponseStream();
                return File(stream, "application/pdf");
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return View("FileNotFound");
            }
        }
        public ActionResult StarredQuesHandWrittenSignDocument(string SIds)
        {
            HandwrittenSignatureModel model = new HandwrittenSignatureModel();
            model.PagesToApplySignatureList = StaticControlBinder.GetPagesToApplySignature();
            model.PositionToApplySignatureList = StaticControlBinder.GetPositionToApplySignature();
            return View("_StarredQuesHandWrittenSignDocument", model);
        }

        public ActionResult SubmitHandWrittenSignatureSelectedPaperWithoutSign(string Ids, string Uid, string lSMCode, HttpPostedFileBase file, int SignatureType, int PagesToApplySignatureID, int PositionToApplySignatureID)
        {
            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            tPaperLaidTemp pT = new tPaperLaidTemp();
            Uid = Sanitizer.GetSafeHtmlFragment(Uid);
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

            tQuestion ques = new tQuestion();

            if (lSMCode != null && lSMCode != "")
            {
                if (lSMCode == "SQ")
                {
                    TempData["lSM"] = "SQ";
                }
                else if (lSMCode == "USQ")
                {
                    TempData["lSM"] = "USQ";
                }
                else if (lSMCode == "NTC")
                {
                    TempData["lSM"] = "NTC";
                }
                else if (lSMCode == "BLL")
                {
                    TempData["lSM"] = "BLL";
                }
                else if (lSMCode == "OPT")
                {
                    TempData["lSM"] = "OPT";
                }
            }

            if (Ids != null && Ids != "")
            {
                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

                string path = "";
                string Files = "";
                string Prifix = "";
                string OldFileName = "";
                foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
                {
                    path = item.FilePath + item.FileName;
                    OldFileName = item.FileName;

                    path = path.Replace(" ", "%20");
                    var test = Request.Url.AbsoluteUri;
                    string[] abc = test.Split('/');
                    Prifix = abc[0] + "//" + abc[2];
                    if (Files == "")
                    {
                        Files = Prifix + path;
                    }
                    else
                    {
                        Files = Files + "," + Prifix + path;
                    }

                    string AssemblyId = CurrentSession.AssemblyId;
                    string SessionId = CurrentSession.SessionId;

                    // PaperLaidAttachmentVS = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", PaperLaidAttachmentVS);


                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                    {
                        PaperLaidAttachmentVS.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                        pT.AssemblyId = CurrentSession.AssemblyId;

                    }

                    if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                    {
                        PaperLaidAttachmentVS.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                        pT.SessionId = CurrentSession.SessionId;

                    }

                    pT.FilePath = Ids;
                    pT.MinisterSubmittedDate = DateTime.Now;
                    pT.DeptSubmittedDate = DateTime.Now;
                    pT.DeptSubmittedBy = CurrentSession.UserName;
                    pT.SessionId = SessionId;
                    pT.AssemblyId = AssemblyId;

                    string imageFileLoc = "";

                    if (CurrentSession.SignaturePath == "")
                    {
                        imageFileLoc = "";
                        string[] Afile = OldFileName.Split('.');
                        OldFileName = Afile[0];

                        string newFileLocation = "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/";
                        DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(newFileLocation));
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        string PasteLoc = Server.MapPath(newFileLocation + OldFileName + "_Signed.pdf");
                        string CopyLoc = Server.MapPath(path);

                        System.IO.File.Copy(CopyLoc, PasteLoc, true);
                        ques.AnswerAttachLocation = PasteLoc;
                        ques.FilePath = Ids;
                    }
                    else
                    {
                        imageFileLoc = Server.MapPath(CurrentSession.SignaturePath);
                        string[] Afile = OldFileName.Split('.');
                        OldFileName = Afile[0];
                        string newFileLocation = "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/" + OldFileName + "_signed.pdf";
                        ques.AnswerAttachLocation = newFileLocation;
                        ques.FilePath = Ids;
                        string textLine1 = "";
                        string textLine2 = "";

                        if (CurrentSession.UserName != "")
                        {
                            if (CurrentSession.IsMember != null)
                            {
                                if (CurrentSession.IsMember.ToUpper() == "FALSE")
                                {
                                    mEmployee empMod = new mEmployee();
                                    if (CurrentSession.DeptID != "")
                                    {
                                        empMod.deptid = CurrentSession.DeptID;
                                    }
                                    empMod.empcd = CurrentSession.UserName;
                                    empMod = (mEmployee)Helper.ExecuteService("Employee", "GetEmployeeDetialsByEmpID", empMod);
                                    if (empMod != null)
                                    {
                                        textLine1 = empMod.empfname + "" + empMod.emplname;
                                    }
                                    else
                                    {
                                        textLine1 = "";
                                    }
                                }
                                else
                                {
                                    PaperLaidAttachmentVS = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", PaperLaidAttachmentVS);
                                }
                            }
                        }
                        else
                        {
                            textLine1 = "";
                        }
                        if (CurrentSession.Designation != "")
                        {
                            textLine2 = CurrentSession.Designation;
                        }
                        else
                        {
                            textLine2 = "";
                        }

                        PDFExtensions.InsertImageToPdf(Server.MapPath(path), imageFileLoc, Server.MapPath(newFileLocation), PositionToApplySignatureID, PagesToApplySignatureID, textLine1, textLine2);
                    }

                }


                mMinisteryMinisterModel.FilePath = Files;

                var modelReturned = Helper.ExecuteService("PaperLaid", "InsertSignPathAttachment", pT) as tPaperLaidTemp;
                var modelReturned2 = Helper.ExecuteService("PaperLaid", "InsertSignPathAttachmenttQuestion", ques) as tQuestion;
                var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateDepartmentMinisterActivePaperId", Ids) as tPaperLaidTemp;

                ///For LOB File Replace
                string[] obja = Ids.Split(',');

                foreach (var item in obja)
                {
                    tPaperLaidV paperLaid = new tPaperLaidV();
                    paperLaid.PaperLaidId = Convert.ToInt32(item);
                    paperLaid = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPaperLaidByPaperLaidId", paperLaid);

                    if (paperLaid != null)
                    {
                        if (paperLaid.LOBRecordId != 0)
                        {
                            tPaperLaidTemp ptT = new tPaperLaidTemp();
                            ptT.PaperLaidTempId = Convert.ToInt32(paperLaid.MinisterActivePaperId);
                            ptT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", ptT);
                            if (ptT != null)
                            {
                                DraftLOB LobModel = new DraftLOB();
                                LobModel.Id = Convert.ToInt32(paperLaid.LOBRecordId);

                                //(from Draftlob in obj1.DraftLOB where Draftlob.Id == LOBRecordId select Draftlob).FirstOrDefault();
                                LobModel = (DraftLOB)Helper.ExecuteService("LOB", "GetDraftLOBById", LobModel);
                                if (LobModel != null)
                                {
                                    string copyPath = Server.MapPath(ptT.SignedFilePath);
                                    string pastePath = Server.MapPath(LobModel.PDFLocation);
                                    System.IO.File.Copy(copyPath, pastePath, true);
                                }
                            }
                        }
                    }
                }

            }

            tPaperLaidV mdl = new tPaperLaidV();

            TempData["Msg"] = "Sign";
            return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment", message = "Sign completed" });
        }
        public JsonResult SubmitHandWrittenSignatureSelectedPaperWithoutSignImage(string Ids, string lSMCode, int qType)
        {
            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            tPaperLaidTemp pT = new tPaperLaidTemp();

            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
            PaperLaidAttachmentVS.QuestionTypeId = qType;
            tQuestion ques = new tQuestion();

            if (lSMCode != null && lSMCode != "")
            {
                if (lSMCode == "SQ")
                {
                    TempData["lSM"] = "SQ";
                }
                else if (lSMCode == "USQ")
                {
                    TempData["lSM"] = "USQ";
                }
                else if (lSMCode == "NTC")
                {
                    TempData["lSM"] = "NTC";
                }
                else if (lSMCode == "BLL")
                {
                    TempData["lSM"] = "BLL";
                }
                else if (lSMCode == "OPT")
                {
                    TempData["lSM"] = "OPT";
                }
            }

            if (Ids != null && Ids != "")
            {
                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

                string path = "";

                string Files = "";

                string Prifix = "";
                string OldFileName = "";


                foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
                {
                    path = item.FilePath + item.FileName;
                    OldFileName = item.FileName;

                    path = path.Replace(" ", "%20");
                    var test = Request.Url.AbsoluteUri;
                    string[] abc = test.Split('/');
                    Prifix = abc[0] + "//" + abc[2];
                    if (Files == "")
                    {
                        Files = Prifix + path;
                    }
                    else
                    {
                        Files = Files + "," + Prifix + path;
                    }

                    string AssemblyId = "";
                    string SessionId = "";

                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                    {

                        AssemblyId = CurrentSession.AssemblyId;

                    }

                    if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                    {

                        SessionId = CurrentSession.SessionId;

                    }


                    pT.FilePath = Ids;
                    pT.MinisterSubmittedDate = DateTime.Now;
                    pT.DeptSubmittedDate = DateTime.Now;
                    pT.DeptSubmittedBy = CurrentSession.UserName;
                    pT.SessionId = CurrentSession.SessionId;
                    pT.AssemblyId = CurrentSession.AssemblyId;


#pragma warning disable CS0219 // The variable 'imageFileLoc' is assigned but its value is never used
                    string imageFileLoc = "";
#pragma warning restore CS0219 // The variable 'imageFileLoc' is assigned but its value is never used
                    if (CurrentSession.SignaturePath == "")
                    {
                        imageFileLoc = "";
                        string[] Afile = OldFileName.Split('.');
                        OldFileName = Afile[0];
                        var _OldFileName = Afile[0] + ".pdf";

                        // var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);



                        string NewFileLocation = FileSettings.SettingValue + "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/";
                        string OldFileLocation = FileSettings.SettingValue + "PaperLaid/" + AssemblyId + "/" + SessionId + "/";
                        DirectoryInfo Dir = new DirectoryInfo(NewFileLocation);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }

                        string PasteLoc = NewFileLocation + OldFileName + "_Signed.pdf";

                        string CopyLoc = OldFileLocation + _OldFileName;



                        //string CopyLoc = Server.MapPath(path);

                        System.IO.File.Copy(CopyLoc, PasteLoc, true);
                        ques.AnswerAttachLocation = PasteLoc;
                        ques.FilePath = Ids;


                    }
                }

                mMinisteryMinisterModel.FilePath = Files;

                var modelReturned2 = Helper.ExecuteService("PaperLaid", "InsertSignPathAttachmenttQuestion", ques) as tQuestion;
                var modelReturned = Helper.ExecuteService("PaperLaid", "InsertSignPathAttachment", pT) as tPaperLaidTemp;
                var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateDepartmentMinisterActivePaperId", Ids) as tPaperLaidTemp;




                string[] objaa = Ids.Split(',');
                pT.PaperLaidId = Convert.ToInt32(objaa[0]);
                pT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GeteRefNum", pT);



                mUsers user = new mUsers();
                user.AadarId = CurrentSession.AadharId;

                user = (mUsers)Helper.ExecuteService("User", "GetForgetPassword", user);
                var MobileNumber = user.MobileNo;
                PaperLaidAttachmentVS.MobileNo = MobileNumber;

                if (MobileNumber != null)
                {
                    Boolean isValid = ValidationAtServer.CheckMobileNumber(MobileNumber);
                    if (isValid == false)
                    {
                        PaperLaidAttachmentVS.IsValid = isValid;
                        return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);
                    }
                    var res = MobileNumber.Split('\n', '\r');
                    user.MobileNo = res[0];
                    var Mob = user.MobileNo;

                    SBL.DomainModel.Models.SmsGateway.tSmsGateway SMS = new SBL.DomainModel.Models.SmsGateway.tSmsGateway();

                    SMS.SmsGatewayList = (List<tSmsGateway>)Helper.ExecuteService("PaperLaid", "GetSMSGatewayList", null);



                    try
                    {
                        SMSService Services = new SMSService();
                        SMSMessageList smsMessage = new SMSMessageList();

                        smsMessage.SMSText = "Document sent to H.P. Vidhan Sabha and eVidhan Reference number is: " + pT.evidhanReferenceNumber + " Dated: " + pT.DeptSubmittedDate + "";
                        //smsMessage.SMSText = "Acknowledgement : Document  received  by eVidhan Reference  number:" + pT.evidhanReferenceNumber + " Dated: " + pT.DeptSubmittedDate + ", Secretary H.P vidhan sabha";
                        smsMessage.ModuleActionID = 5;
                        smsMessage.UniqueIdentificationID = 5;
                        smsMessage.MobileNo.Add(Mob);

                        //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSmsGatewayValue", null);

                        //string Value1 = FileSettings.SettingValue;

                        //if (Value1 == "1" || Value1 =="")
                        //{
                        //    foreach (var item in SMS.SmsGatewayList)
                        //    {
                        //        string textReplaceNew = "";
                        //        textReplaceNew = smsMessage.SMSText.Replace(item.SMSText, item.SMSTextReplaceValue);
                        //        smsMessage.SMSText = textReplaceNew;
                        //    }

                        //}
                        //else if (Value1 == "2" || Value1 == "")
                        //{
                        //    foreach (var item in SMS.SmsGatewayList)
                        //    {
                        //        string textReplaceNew = "";
                        //        textReplaceNew = smsMessage.SMSText.Replace(item.SMSText, item.SMSTextReplaceValue);
                        //        smsMessage.SMSText = textReplaceNew;
                        //    }
                        //}
                        //else
                        //{

                        //}

                        Notification.Send(true, false, smsMessage, null);



                    }
                    catch
                    {

                    }
                }
                else
                {
                    PaperLaidAttachmentVS.MobileNo = MobileNumber;
                    return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);
                }



                ///For LOB File Replace
                string[] obja = Ids.Split(',');

                foreach (var item in obja)
                {
                    tPaperLaidV paperLaid = new tPaperLaidV();
                    paperLaid.PaperLaidId = Convert.ToInt32(item);
                    paperLaid = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPaperLaidByPaperLaidId", paperLaid);

                    if (paperLaid != null)
                    {
                        if (paperLaid.LOBRecordId != 0)
                        {
                            tPaperLaidTemp ptT = new tPaperLaidTemp();
                            ptT.PaperLaidTempId = Convert.ToInt32(paperLaid.MinisterActivePaperId);
                            ptT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", ptT);
                            if (ptT != null)
                            {
                                DraftLOB LobModel = new DraftLOB();
                                LobModel.Id = Convert.ToInt32(paperLaid.LOBRecordId);

                                //(from Draftlob in obj1.DraftLOB where Draftlob.Id == LOBRecordId select Draftlob).FirstOrDefault();
                                LobModel = (DraftLOB)Helper.ExecuteService("LOB", "GetDraftLOBById", LobModel);
                                if (LobModel != null)
                                {
                                    string copyPath = Server.MapPath(ptT.SignedFilePath);
                                    string pastePath = Server.MapPath(LobModel.PDFLocation);
                                    System.IO.File.Copy(copyPath, pastePath, true);
                                }
                            }
                        }
                    }
                }
            }
            return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);


        }


        public JsonResult NoticeSubmitHandWrittenSignatureSelectedPaperWithoutSignImage(string Ids, string lSMCode)
        {
            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            tPaperLaidTemp pT = new tPaperLaidTemp();

            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

            tQuestion ques = new tQuestion();

            if (lSMCode != null && lSMCode != "")
            {
                if (lSMCode == "SQ")
                {
                    TempData["lSM"] = "SQ";
                }
                else if (lSMCode == "USQ")
                {
                    TempData["lSM"] = "USQ";
                }
                else if (lSMCode == "NTC")
                {
                    TempData["lSM"] = "NTC";
                }
                else if (lSMCode == "BLL")
                {
                    TempData["lSM"] = "BLL";
                }
                else if (lSMCode == "OPT")
                {
                    TempData["lSM"] = "OPT";
                }
            }

            if (Ids != null && Ids != "")
            {
                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

                string path = "";

                string Files = "";

                string Prifix = "";
                string OldFileName = "";


                foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
                {
                    path = item.FilePath + item.FileName;
                    OldFileName = item.FileName;

                    path = path.Replace(" ", "%20");
                    var test = Request.Url.AbsoluteUri;
                    string[] abc = test.Split('/');
                    Prifix = abc[0] + "//" + abc[2];
                    if (Files == "")
                    {
                        Files = Prifix + path;
                    }
                    else
                    {
                        Files = Files + "," + Prifix + path;
                    }

                    string AssemblyId = "";
                    string SessionId = "";

                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                    {

                        AssemblyId = CurrentSession.AssemblyId;

                    }

                    if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                    {

                        SessionId = CurrentSession.SessionId;

                    }


                    pT.FilePath = Ids;
                    pT.MinisterSubmittedDate = DateTime.Now;
                    pT.DeptSubmittedDate = DateTime.Now;
                    pT.DeptSubmittedBy = CurrentSession.UserName;
                    pT.SessionId = CurrentSession.SessionId;
                    pT.AssemblyId = CurrentSession.AssemblyId;

#pragma warning disable CS0219 // The variable 'imageFileLoc' is assigned but its value is never used
                    string imageFileLoc = "";
#pragma warning restore CS0219 // The variable 'imageFileLoc' is assigned but its value is never used
                    if (CurrentSession.SignaturePath == "")
                    {
                        imageFileLoc = "";
                        string[] Afile = OldFileName.Split('.');
                        OldFileName = Afile[0];
                        var _OldFileName = Afile[0] + ".pdf";

                        // var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);



                        string NewFileLocation = FileSettings.SettingValue + "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/";
                        string OldFileLocation = FileSettings.SettingValue + "PaperLaid/" + AssemblyId + "/" + SessionId + "/";
                        DirectoryInfo Dir = new DirectoryInfo(NewFileLocation);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }

                        string PasteLoc = NewFileLocation + OldFileName + "_Signed.pdf";

                        string CopyLoc = OldFileLocation + _OldFileName;



                        //string CopyLoc = Server.MapPath(path);

                        System.IO.File.Copy(CopyLoc, PasteLoc, true);
                        ques.AnswerAttachLocation = PasteLoc;
                        ques.FilePath = Ids;


                    }
                }

                mMinisteryMinisterModel.FilePath = Files;

                var modelReturned2 = Helper.ExecuteService("PaperLaid", "InsertSignPathAttachmenttQuestion", ques) as tQuestion;
                var modelReturned = Helper.ExecuteService("PaperLaid", "InsertSignPathAttachment", pT) as tPaperLaidTemp;
                var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateDepartmentMinisterActivePaperId", Ids) as tPaperLaidTemp;



                string[] objaa = Ids.Split(',');
                pT.PaperLaidId = Convert.ToInt32(objaa[0]);
                pT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GeteRefNum", pT);



                mUsers user = new mUsers();
                user.AadarId = CurrentSession.AadharId;

                user = (mUsers)Helper.ExecuteService("User", "GetForgetPassword", user);
                var MobileNumber = user.MobileNo;
                PaperLaidAttachmentVS.MobileNo = MobileNumber;

                if (MobileNumber != null)
                {
                    Boolean isValid = ValidationAtServer.CheckMobileNumber(MobileNumber);
                    if (isValid == false)
                    {
                        PaperLaidAttachmentVS.IsValid = isValid;
                        return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);
                    }
                    var res = MobileNumber.Split('\n', '\r');
                    user.MobileNo = res[0];
                    var Mob = user.MobileNo;
                    try
                    {
                        SMSService Services = new SMSService();
                        SMSMessageList smsMessage = new SMSMessageList();
                        //smsMessage.SMSText = "Acknowledgement : Document received  by eVidhan Reference number:" + pT.evidhanReferenceNumber + " Date: " + pT.DeptSubmittedDate + ", Secretary H.P vidhan sabha";
                        smsMessage.SMSText = "Document sent to H.P. Vidhan Sabha and eVidhan Reference number is: " + pT.evidhanReferenceNumber + " Dated: " + pT.DeptSubmittedDate + "";
                        smsMessage.ModuleActionID = 5;
                        smsMessage.UniqueIdentificationID = 5;
                        smsMessage.MobileNo.Add(Mob);
                        Notification.Send(true, false, smsMessage, null);

                    }
                    catch
                    {

                    }
                }
                else
                {
                    PaperLaidAttachmentVS.MobileNo = MobileNumber;
                    return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);
                }





                //string[] objaa = Ids.Split(',');
                //pT.PaperLaidId = Convert.ToInt16(objaa[0]);
                //pT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GeteRefNum", pT);


                //mUsers user = new mUsers();
                //user.AadarId = CurrentSession.AadharId;

                //user = (mUsers)Helper.ExecuteService("User", "GetForgetPassword", user);


                //var MobileNumber = user.MobileNo;
                //PaperLaidAttachmentVS.MobileNo = MobileNumber;

                //if (MobileNumber != null)
                //{
                //    Boolean isValid = ValidationAtServer.CheckMobileNumber(MobileNumber);
                //    var res = MobileNumber.Split('\n', '\r');
                //    user.MobileNo = res[0];
                //    var Mob = user.MobileNo;
                //    try
                //    {
                //        SMSService Services = new SMSService();
                //        SMSMessageList smsMessage = new SMSMessageList();
                //        smsMessage.SMSText = " This Is Your Reference number:" + pT.evidhanReferenceNumber;
                //        smsMessage.ModuleActionID = 5;
                //        smsMessage.UniqueIdentificationID = 5;
                //        smsMessage.MobileNo.Add(Mob);
                //        Notification.Send(true, false, smsMessage, null);

                //    }
                //    catch
                //    {

                //    }
                //}
                //else
                //{
                //    return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);
                //}


                ///For LOB File Replace
                string[] obja = Ids.Split(',');

                foreach (var item in obja)
                {
                    tPaperLaidV paperLaid = new tPaperLaidV();
                    paperLaid.PaperLaidId = Convert.ToInt32(item);
                    paperLaid = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPaperLaidByPaperLaidId", paperLaid);

                    if (paperLaid != null)
                    {
                        if (paperLaid.LOBRecordId != 0)
                        {
                            tPaperLaidTemp ptT = new tPaperLaidTemp();
                            ptT.PaperLaidTempId = Convert.ToInt32(paperLaid.MinisterActivePaperId);
                            ptT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", ptT);
                            if (ptT != null)
                            {
                                DraftLOB LobModel = new DraftLOB();
                                LobModel.Id = Convert.ToInt32(paperLaid.LOBRecordId);

                                //(from Draftlob in obj1.DraftLOB where Draftlob.Id == LOBRecordId select Draftlob).FirstOrDefault();
                                LobModel = (DraftLOB)Helper.ExecuteService("LOB", "GetDraftLOBById", LobModel);
                                if (LobModel != null)
                                {
                                    string copyPath = Server.MapPath(ptT.SignedFilePath);
                                    string pastePath = Server.MapPath(LobModel.PDFLocation);
                                    System.IO.File.Copy(copyPath, pastePath, true);
                                }
                            }
                        }
                    }
                }
            }
            return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);


        }




        public JsonResult BillsSubmitHandWrittenSignatureSelectedPaperWithoutSignImage(string Ids, string lSMCode)
        {
            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();

            tPaperLaidTemp pT = new tPaperLaidTemp();
            tBillRegister bills = new tBillRegister();
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

            tQuestion ques = new tQuestion();

            if (lSMCode != null && lSMCode != "")
            {
                if (lSMCode == "SQ")
                {
                    TempData["lSM"] = "SQ";
                }
                else if (lSMCode == "USQ")
                {
                    TempData["lSM"] = "USQ";
                }
                else if (lSMCode == "NTC")
                {
                    TempData["lSM"] = "NTC";
                }
                else if (lSMCode == "BLL")
                {
                    TempData["lSM"] = "BLL";
                }
                else if (lSMCode == "OPT")
                {
                    TempData["lSM"] = "OPT";
                }
            }

            if (Ids != null && Ids != "")
            {
                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

                string path = "";
                string Files = "";
                string Prifix = "";
                string OldFileName = "";
                foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
                {
                    path = item.FilePath + item.FileName;


                    OldFileName = item.FileName;
                    PaperLaidAttachmentVS.FileName = OldFileName;
                    if (OldFileName != null)
                    {
                        pT.PaperLaidId = item.PaperLaidId;
                        path = path.Replace(" ", "%20");
                        var test = Request.Url.AbsoluteUri;
                        string[] abc = test.Split('/');
                        Prifix = abc[0] + "//" + abc[2];
                        if (Files == "")
                        {
                            Files = Prifix + path;
                        }
                        else
                        {
                            Files = Files + "," + Prifix + path;
                        }

                        string AssemblyId = "";
                        string SessionId = "";

                        if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                        {
                            // pT.AssemblyId = CurrentSession.AssemblyId;
                            AssemblyId = CurrentSession.AssemblyId;

                        }

                        if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                        {
                            // pT.SessionId = CurrentSession.SessionId;
                            SessionId = CurrentSession.SessionId;

                        }

                        //PaperLaidAttachmentVS = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", PaperLaidAttachmentVS);


                        //if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
                        //{
                        //    CurrentSession.AssemblyId = PaperLaidAttachmentVS.AssemblyId.ToString();
                        //}

                        //if (string.IsNullOrEmpty(CurrentSession.SessionId))
                        //{
                        //    CurrentSession.SessionId = PaperLaidAttachmentVS.SessionId.ToString();
                        //}

                        pT.FilePath = Ids;
                        //pT.MinisterSubmittedDate = DateTime.Now;
                        //pT.MinisterSubmittedBy = Convert.ToInt32(CurrentSession.UserName);
                        pT.DeptSubmittedDate = DateTime.Now;
                        pT.DeptSubmittedBy = CurrentSession.UserName;
                        pT.SessionId = CurrentSession.SessionId;
                        pT.AssemblyId = CurrentSession.AssemblyId;
                        bills.FilePath = Ids;
#pragma warning disable CS0219 // The variable 'imageFileLoc' is assigned but its value is never used
                        string imageFileLoc = "";
#pragma warning restore CS0219 // The variable 'imageFileLoc' is assigned but its value is never used
                        if (CurrentSession.SignaturePath == "")
                        {
                            imageFileLoc = "";
                            string[] Afile = OldFileName.Split('.');
                            OldFileName = Afile[0];

                            var _OldFileName = Afile[0] + ".pdf";


                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);


                            string NewFileLocation = FileSettings.SettingValue + "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/";
                            string OldFileLocation = FileSettings.SettingValue + "PaperLaid/" + AssemblyId + "/" + SessionId + "/";
                            DirectoryInfo Dir = new DirectoryInfo(NewFileLocation);
                            if (!Dir.Exists)
                            {
                                Dir.Create();
                            }

                            string PasteLoc = NewFileLocation + OldFileName + "_Signed.pdf";

                            string CopyLoc = OldFileLocation + _OldFileName;


                            //string newFileLocation = "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/";
                            //DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(newFileLocation));
                            //if (!Dir.Exists)
                            //{
                            //    Dir.Create();
                            //}
                            //string PasteLoc = Server.MapPath(newFileLocation + OldFileName + "_Signed.pdf");
                            //string CopyLoc = Server.MapPath(path);

                            System.IO.File.Copy(CopyLoc, PasteLoc, true);
                            ques.AnswerAttachLocation = PasteLoc;
                            ques.FilePath = Ids;
                            bills.FilePath = Ids;
                        }
                    }
                    else
                    {
                        return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);
                    }

                }

                mMinisteryMinisterModel.FilePath = Files;
                // ques.AnswerAttachLocation = Files;
                //var modelReturned2 = Helper.ExecuteService("PaperLaid", "InsertSignPathAttachmenttQuestion", ques) as tQuestion;

                var modelReturned = Helper.ExecuteService("PaperLaid", "InsertSignPathAttachment", pT) as tPaperLaidTemp;

                var BillRegister = Helper.ExecuteService("PaperLaid", "UpdateBillsRegister", bills) as tBillRegister;
                var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateBillsDepartmentMinisterActivePaperId", Ids) as tPaperLaidTemp;



                string[] objaa = Ids.Split(',');
                pT.PaperLaidId = Convert.ToInt32(objaa[0]);
                pT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GeteRefNum", pT);



                mUsers user = new mUsers();
                user.AadarId = CurrentSession.AadharId;

                user = (mUsers)Helper.ExecuteService("User", "GetForgetPassword", user);
                var MobileNumber = user.MobileNo;
                PaperLaidAttachmentVS.MobileNo = MobileNumber;

                if (MobileNumber != null)
                {
                    Boolean isValid = ValidationAtServer.CheckMobileNumber(MobileNumber);
                    if (isValid == false)
                    {
                        PaperLaidAttachmentVS.IsValid = isValid;
                        return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);
                    }
                    var res = MobileNumber.Split('\n', '\r');
                    user.MobileNo = res[0];
                    var Mob = user.MobileNo;
                    try
                    {
                        SMSService Services = new SMSService();
                        SMSMessageList smsMessage = new SMSMessageList();
                        //smsMessage.SMSText = "Acknowledgement : Document received  by eVidhan Reference number:" + pT.evidhanReferenceNumber + " Date: " + pT.DeptSubmittedDate + ", Secretary H.P vidhan sabha";
                        smsMessage.SMSText = "Document sent to H.P. Vidhan Sabha and eVidhan Reference number is: " + pT.evidhanReferenceNumber + " Dated: " + pT.DeptSubmittedDate + "";
                        smsMessage.ModuleActionID = 5;
                        smsMessage.UniqueIdentificationID = 5;
                        smsMessage.MobileNo.Add(Mob);
                        Notification.Send(true, false, smsMessage, null);

                    }
                    catch
                    {

                    }
                }
                else
                {
                    PaperLaidAttachmentVS.MobileNo = MobileNumber;
                    return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);
                }


                //string[] objaa = Ids.Split(',');
                //pT.PaperLaidId = Convert.ToInt16(objaa[0]);
                //pT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GeteRefNum", pT);


                //mUsers user = new mUsers();
                //user.AadarId = CurrentSession.AadharId;

                //user = (mUsers)Helper.ExecuteService("User", "GetForgetPassword", user);


                //var MobileNumber = user.MobileNo;
                //PaperLaidAttachmentVS.MobileNo = MobileNumber;

                //if (MobileNumber == null)
                //{
                //    return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);
                //}
                //var res = MobileNumber.Split('\n','\r');


                //user.MobileNo = res[0];

                //var Mob = user.MobileNo;

                //try
                //{
                //    SMSService Services = new SMSService();
                //    SMSMessageList smsMessage = new SMSMessageList();
                //    smsMessage.SMSText = " This Is Your Reference number:" + pT.evidhanReferenceNumber;
                //    smsMessage.ModuleActionID = 5;
                //    smsMessage.UniqueIdentificationID = 5;
                //    smsMessage.MobileNo.Add(Mob);
                //    Notification.Send(true, false, smsMessage, null);

                //}
                //catch
                //{

                //}


                //var modelReturned = Helper.ExecuteService("PaperLaidMinister", "InsertSignPathAttachment", pT) as tPaperLaidTemp;
                //var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateMinisterActivePaperId", Ids) as tPaperLaidTemp;

                ///For LOB File Replace
                string[] obja = Ids.Split(',');

                foreach (var item in obja)
                {
                    tPaperLaidV paperLaid = new tPaperLaidV();
                    paperLaid.PaperLaidId = Convert.ToInt32(item);
                    paperLaid = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPaperLaidByPaperLaidId", paperLaid);

                    if (paperLaid != null)
                    {
                        if (paperLaid.LOBRecordId != 0)
                        {
                            tPaperLaidTemp ptT = new tPaperLaidTemp();
                            ptT.PaperLaidTempId = Convert.ToInt32(paperLaid.MinisterActivePaperId);
                            ptT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", ptT);
                            if (ptT != null)
                            {
                                DraftLOB LobModel = new DraftLOB();
                                LobModel.Id = Convert.ToInt32(paperLaid.LOBRecordId);

                                //(from Draftlob in obj1.DraftLOB where Draftlob.Id == LOBRecordId select Draftlob).FirstOrDefault();
                                LobModel = (DraftLOB)Helper.ExecuteService("LOB", "GetDraftLOBById", LobModel);
                                if (LobModel != null)
                                {
                                    //pT.SessionId = CurrentSession.SessionId;
                                    //pT.AssemblyId = CurrentSession.AssemblyId;
                                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                                    string OldFileLocation = FileSettings.SettingValue + "/PaperLaid/" + pT.AssemblyId + "/" + pT.SessionId + "/Signed/";
                                    string NewFileLocation = FileSettings.SettingValue + "/LOB/" + pT.AssemblyId + "/" + pT.SessionId + "/";

                                    DirectoryInfo Dir = new DirectoryInfo(NewFileLocation);
                                    if (!Dir.Exists)
                                    {
                                        Dir.Create();
                                    }

                                    string copyPath = FileSettings.SettingValue + ptT.SignedFilePath;
                                    string pastePath = FileSettings.SettingValue + LobModel.PDFLocation;
                                    System.IO.File.Copy(copyPath, pastePath, true);
                                }
                            }
                        }
                    }
                }



            }


            return Json(PaperLaidAttachmentVS, JsonRequestBehavior.AllowGet);
        }



        public ActionResult StarredBeforeFixation()
        {
            tPaperLaidV obj = new tPaperLaidV();

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                obj.UserID = new Guid(CurrentSession.UserID);
            }

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);


            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);


            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    obj.DepartmentId = item.AssociatedDepts;
                    obj.IsPermissions = item.IsPermissions; ;
                }
            }
            else
            {
                if (obj.DepartmentId == null)
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
            }
            /*End*/

            var model = (List<mDepartmentPdfPath>)Helper.ExecuteService("PaperLaid", "BeforeFixation", obj);

            return PartialView("_StarredBeforeFixation", model);
        }

        public ActionResult UnStarredBeforeFixation()
        {
            tPaperLaidV obj = new tPaperLaidV();

            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                obj.UserID = new Guid(CurrentSession.UserID);
            }

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);


            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);


            }


            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                obj.UserID = new Guid(CurrentSession.UserID);
            }
            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    obj.DepartmentId = item.AssociatedDepts;
                    obj.IsPermissions = item.IsPermissions; ;
                }
            }
            else
            {
                if (obj.DepartmentId == null)
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
            }
            /*End*/

            var model = (List<mDepartmentPdfPath>)Helper.ExecuteService("PaperLaid", "BeforeFixation", obj);

            return PartialView("_UnStarredBeforeFixation", model);
        }

        public ActionResult StarredAfterFixation()
        {
            tPaperLaidV obj = new tPaperLaidV();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            var model = (List<mSessionDate>)Helper.ExecuteService("PaperLaid", "AfterFixation", obj);

            return PartialView("_StarredAfterFixation", model);
        }

        public ActionResult UnStarredAfterFixation()
        {
            tPaperLaidV obj = new tPaperLaidV();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            var model = (List<mSessionDate>)Helper.ExecuteService("PaperLaid", "AfterFixation", obj);

            return PartialView("_UnStarredAfterFixation", model);
        }

        [HttpGet]
        public ActionResult OTPRegistarionReply(string DiaryNumber, string QType, string Subject)
        {
            mSession Mdl = new mSession();
            mAssembly assmblyMdl = new mAssembly();
            if (CurrentSession.AssemblyId != null && CurrentSession.AssemblyId != "")
            {
                Mdl.AssemblyID = int.Parse(CurrentSession.AssemblyId);
                assmblyMdl.AssemblyID = int.Parse(CurrentSession.AssemblyId);
            }
            if (CurrentSession.SessionId != null && CurrentSession.SessionId != "")
            {
                Mdl.SessionCode = int.Parse(CurrentSession.SessionId);
            }
            tUserRegistrationDetails obj = new tUserRegistrationDetails();
            obj.DiaryNumber = DiaryNumber;
            obj.Subject = Subject;
            obj.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
            obj.AssemblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
            obj = (tUserRegistrationDetails)Helper.ExecuteService("PaperLaid", "OTPRegistarionDetailsList", obj);
            obj.PaperTypeId = int.Parse(QType);

            foreach (var item in obj.UserRegistrationDetailsModel)
            {
                obj.IsLocked = item.IsLocked;
            }

            return PartialView("_OTPRegistarionReply", obj);
        }

        public ActionResult OTPDiaryList(string DiaryNumber)
        {

            OTPRegistrationAuditTrialModel obj = new OTPRegistrationAuditTrialModel();
            obj.DiaryNumber = DiaryNumber;
            obj = (OTPRegistrationAuditTrialModel)Helper.ExecuteService("PaperLaid", "OTPDiaryList", obj);
            return PartialView("_OTPDiaryList", obj);

        }

        public ActionResult OTPViewListList(string DiaryNumber)
        {

            OTPRegistrationAuditTrialModel obj = new OTPRegistrationAuditTrialModel();
            obj.DiaryNumber = DiaryNumber;
            obj.AadharId = CurrentSession.AadharId;
            obj = (OTPRegistrationAuditTrialModel)Helper.ExecuteService("PaperLaid", "OTPViewListList", obj);
            return PartialView("_OTPAadharList", obj);

        }

        public ActionResult OTPMobileList(string MobileNo)
        {

            OTPRegistrationAuditTrialModel obj = new OTPRegistrationAuditTrialModel();
            obj.MobileNumber = MobileNo;
            obj = (OTPRegistrationAuditTrialModel)Helper.ExecuteService("PaperLaid", "OTPMobileList", obj);

            return PartialView("_OTPMobileList", obj);

        }

        public JsonResult UpdateLocked(string DiaryNumber)
        {
            tUserRegistrationDetails obj = new tUserRegistrationDetails();
            tOneTimeUserRegistration obj1 = new tOneTimeUserRegistration();
            obj1.DiaryNumber = DiaryNumber;

            obj.DiaryNumber = DiaryNumber;
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);

            }
            obj = (tUserRegistrationDetails)Helper.ExecuteService("PaperLaid", "UpdateLocked", obj);

            //obj1 = (tOneTimeUserRegistration)Helper.ExecuteService("PaperLaid", "UpdateOneTimeUSerLocked", obj1);

            return Json("True", JsonRequestBehavior.AllowGet);
        }




        public JsonResult SubmitRequestOTPRegistarionReplyNotices(string DiaryNumber, string MobileNo, string UserName, string AddressLocations, int DocumentTypeId, int RuleId, string Email)
        {

            tOneTimeUserRegistration obj = new tOneTimeUserRegistration();
            tUserRegistrationDetails obj1 = new tUserRegistrationDetails();
            obj1.AadharId = CurrentSession.AadharId;
            tTempMobileUser tmbl = new tTempMobileUser();
            obj.MobileNumber = MobileNo;
            obj1.RuleId = RuleId;
            obj1.DocumentTypeId = 3;
            obj.UserName = UserName;
            obj.AddressLocations = AddressLocations;
            //obj1.DocumentTypeId = DocumentTypeId;
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj1.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                obj1.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            tOneTimeUserRegistration mdl = new tOneTimeUserRegistration();
            tUserRegistrationDetails mdl1 = new tUserRegistrationDetails();
            tTempMobileUser mbl = new tTempMobileUser();
            bool mob = CheckMobileNo(MobileNo);

            if (mob == false)
            {
                obj.UserId = SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Extensions.ExtensionMethods.GenerateOTPUSerIdValue(obj.AssemblyId, obj.SessionId, MobileNo);


                #region Encrypt Password
                var chars = "0123456789";
                var random = new Random();
                var result = new string(
                Enumerable.Repeat(chars, 5)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
                String PlanePassword = result;

                string ePassword = ComputeHash(PlanePassword, "SHA1", null);

                #endregion

                obj.IsActive = true;
                obj.OTPPassword = ePassword;
                obj.Email = Email;
                mdl = (tOneTimeUserRegistration)Helper.ExecuteService("PaperLaid", "SubmittOTPEntry", obj);
                obj.CustomMessage = "Data Save Successfully !!";
                tmbl.UserId = obj.UserId;
                tmbl.userGuid = new Guid(CurrentSession.UserID); ;
                tmbl.password = PlanePassword;
                mbl = (tTempMobileUser)Helper.ExecuteService("PaperLaid", "SubmittMobileUser", tmbl);
                try
                {
                    SMSService Services = new SMSService();
                    SMSMessageList smsMessage = new SMSMessageList();
                    smsMessage.SMSText = "Your temporary user on eVidhan.\n\n Your User Id is " + obj.UserId + " and password is " + PlanePassword;
                    smsMessage.ModuleActionID = 4;
                    smsMessage.UniqueIdentificationID = 4;
                    smsMessage.MobileNo.Add(obj.MobileNumber);
                    Notification.Send(true, false, smsMessage, null);

                }
                catch
                {

                }
            }

            //int DocumentTypeId = 1;
            bool IsExists = CheckMobileDiaryNo(MobileNo, DiaryNumber, DocumentTypeId);
            obj1.MobileNumber = MobileNo;
            //obj1.DocumentTypeId = 1;
            obj1.IsLocked = false;
            obj1.IsAssignDate = DateTime.Now;
            obj1.DiaryNumber = DiaryNumber;
            if (IsExists == false)
            {
                try
                {
                    SMSService Services = new SMSService();
                    SMSMessageList smsMessage = new SMSMessageList();
                    smsMessage.SMSText = "Diary Number : " + obj1.DiaryNumber + " has been allotted for creating reply, please use your temporary user Id and password for login";
                    smsMessage.ModuleActionID = 5;
                    smsMessage.UniqueIdentificationID = 5;
                    smsMessage.MobileNo.Add(obj1.MobileNumber);
                    Notification.Send(true, false, smsMessage, null);

                }
                catch
                {

                }
                obj1.AadharId = CurrentSession.AadharId;

                mdl1 = (tUserRegistrationDetails)Helper.ExecuteService("PaperLaid", "UserRegistrationDetails", obj1);
            }

            return Json(obj1, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SubmitRequestOTPRegistarionReply(string DiaryNumber, string MobileNo, string UserName, string AddressLocations, int DocumentTypeId, string Email)
        {

            tOneTimeUserRegistration obj = new tOneTimeUserRegistration();
            tUserRegistrationDetails obj1 = new tUserRegistrationDetails();
            tTempMobileUser tmbl = new tTempMobileUser();
            obj.MobileNumber = MobileNo;
            obj.UserName = UserName;
            obj1.AadharId = CurrentSession.AadharId;
            obj.AddressLocations = AddressLocations;
            obj1.DocumentTypeId = DocumentTypeId;
            obj1.Email = Email;

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj1.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                obj1.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            tOneTimeUserRegistration mdl = new tOneTimeUserRegistration();
            tUserRegistrationDetails mdl1 = new tUserRegistrationDetails();
            tTempMobileUser mbl = new tTempMobileUser();
            bool mob = CheckMobileNo(MobileNo);

            if (mob == false)
            {
                obj.UserId = SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Extensions.ExtensionMethods.GenerateOTPUSerIdValue(obj.AssemblyId, obj.SessionId, MobileNo);


                #region Encrypt Password
                var chars = "0123456789";
                var random = new Random();
                var result = new string(
                Enumerable.Repeat(chars, 5)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
                String PlanePassword = result;

                string ePassword = ComputeHash(PlanePassword, "SHA1", null);

                #endregion

                obj.IsActive = true;
                obj.OTPPassword = ePassword;
                obj.Email = Email;
                mdl = (tOneTimeUserRegistration)Helper.ExecuteService("PaperLaid", "SubmittOTPEntry", obj);
                obj.CustomMessage = "Data Save Successfully !!";
                tmbl.UserId = obj.UserId;
                tmbl.userGuid = new Guid(CurrentSession.UserID); ;
                tmbl.password = PlanePassword;
                mbl = (tTempMobileUser)Helper.ExecuteService("PaperLaid", "SubmittMobileUser", tmbl);
                try
                {
                    SMSService Services = new SMSService();
                    SMSMessageList smsMessage = new SMSMessageList();
                    smsMessage.SMSText = "Your temporary user on eVidhan.\n\n Your User Id is " + obj.UserId + " and password is " + PlanePassword;
                    smsMessage.ModuleActionID = 4;
                    smsMessage.UniqueIdentificationID = 4;
                    smsMessage.MobileNo.Add(obj.MobileNumber);
                    Notification.Send(true, false, smsMessage, null);

                }
                catch
                {

                }
            }

            //int DocumentTypeId = 1;
            bool IsExists = CheckMobileDiaryNo(MobileNo, DiaryNumber, DocumentTypeId);
            string T = "T";
            obj1.MobileNumber = MobileNo;
            obj1.UserId = T + MobileNo;
            //obj1.DocumentTypeId = 1;
            obj1.IsLocked = false;
            obj1.IsAssignDate = DateTime.Now;
            obj1.DiaryNumber = DiaryNumber;
            if (IsExists == false)
            {
                try
                {
                    // mbl = (tTempMobileUser)Helper.ExecuteService("PaperLaid", "GetPasswordMobileUser", obj1);
                    var val = (tTempMobileUser)Helper.ExecuteService("PaperLaid", "GetPasswordMobileUser", obj1);
                    SMSService Services = new SMSService();
                    SMSMessageList smsMessage = new SMSMessageList();
                    smsMessage.SMSText = "Diary Number : " + obj1.DiaryNumber + " has been allotted for creating reply, please use your temporary user Id = " + val.UserId + "  and password =" + val.password + " for login";
                    smsMessage.ModuleActionID = 5;
                    smsMessage.UniqueIdentificationID = 5;
                    smsMessage.MobileNo.Add(obj1.MobileNumber);
                    Notification.Send(true, false, smsMessage, null);

                }
                catch
                {

                }
                mdl1 = (tUserRegistrationDetails)Helper.ExecuteService("PaperLaid", "UserRegistrationDetails", obj1);
            }

            return Json(obj1, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckMobileDiaryNum(string MobileNo, string DiaryNumber, int DocumentTypeId)
        {
            tUserRegistrationDetails obj = new tUserRegistrationDetails();

            obj.MobileNumber = MobileNo;
            obj.DiaryNumber = DiaryNumber;
            obj.DocumentTypeId = DocumentTypeId;

            string msg = Helper.ExecuteService("PaperLaid", "CheckExistinigData", obj) as string;
            return Json(msg, JsonRequestBehavior.AllowGet);

        }

        public bool CheckMobileDiaryNo(string MobileNo, string DiaryNumber, int DocumentTypeId)
        {
            tUserRegistrationDetails obj = new tUserRegistrationDetails();

            obj.MobileNumber = MobileNo;
            obj.DiaryNumber = DiaryNumber;
            obj.DocumentTypeId = DocumentTypeId;
            try
            {
                var mdl = (bool)Helper.ExecuteService("PaperLaid", "CheckMobileDiaryExist", obj);
                // var mdl = (bool)Helper.ExecuteService("PaperLaid", "CheckMobileNoExist", MobileNo);
                return mdl;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }





        #region Encryptio/Decrytption

        public static string ComputeHash(string plainText, string hashAlgorithm, byte[] saltBytes)
        {
            // If salt is not specified, generate it on the fly.
            if (saltBytes == null)
            {
                // Define min and max salt sizes.
                int minSaltSize = 4;
                int maxSaltSize = 8;

                // Generate a random number for the size of the salt.
                Random random = new Random();
                int saltSize = random.Next(minSaltSize, maxSaltSize);

                // Allocate a byte array, which will hold the salt.
                saltBytes = new byte[saltSize];

                // Initialize a random number generator.
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

                // Fill the salt with cryptographically strong byte values.
                rng.GetNonZeroBytes(saltBytes);
            }

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes =
                    new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];


            HashAlgorithm hash;
            // Make sure hashing algorithm name is specified.
            if (hashAlgorithm == null)
                hashAlgorithm = "";

            // Initialize appropriate hashing algorithm class.
            switch (hashAlgorithm.ToUpper())
            {
                case "SHA1":
                    hash = new SHA1Managed();
                    break;

                case "SHA256":
                    hash = new SHA256Managed();
                    break;

                case "SHA384":
                    hash = new SHA384Managed();
                    break;

                case "SHA512":
                    hash = new SHA512Managed();
                    break;

                default:
                    hash = new MD5CryptoServiceProvider();
                    break;
            }

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(hashWithSaltBytes);

            // Return the result.
            return hashValue;
        }
        #endregion

        public bool CheckMobileNo(string MobileNo)
        {
            try
            {
                var mdl = (bool)Helper.ExecuteService("PaperLaid", "CheckMobileNoExist", MobileNo);
                return mdl;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }





        [HttpPost]
        public JsonResult CheckMobileNo(string MobileNo, string AddressLocations, string UserName)
        {

            OTPRegistrationAuditTrialModel obj = new OTPRegistrationAuditTrialModel();
            tUserRegistrationDetails obj1 = new tUserRegistrationDetails();
            obj.MobileNumber = MobileNo;
            obj.AddressLocations = AddressLocations;
            obj.UserName = UserName;
            obj = (OTPRegistrationAuditTrialModel)Helper.ExecuteService("PaperLaid", "CheckMobileNo", obj);
            foreach (var item in obj.OTPRegistrationModel)
            {
                obj.AddressLocations = item.AddressLocations;
                obj1.MobileNumber = item.MobileNumber;
                obj1.AddressLocations = item.AddressLocations;
                obj.UserName = item.UserName;
                obj1.UserName = item.UserName;
            }
            return Json(obj1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ContentResult UploadMainPdfFiles()
        {

            string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPaperLaidV>();
            //if (ext == ".pdf")
            //{
            //}
            //else
            //{

            //}
            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                // string Fname=  hpf.FileName;
                string ext = Path.GetExtension(hpf.FileName);
                if (ext == ".pdf")
                {
                    if (hpf.ContentLength == 0)
                        continue;
                    string url1 = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                    string directory1 = Server.MapPath(url1);
                    if (!Directory.Exists(directory1))
                    {
                        Directory.CreateDirectory(directory1);
                    }
                    string savedFileName = Path.Combine(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID), Path.GetFileName(hpf.FileName));
                    hpf.SaveAs(savedFileName);

                    r.Add(new tPaperLaidV()
                    {
                        Name = hpf.FileName,
                        Length = hpf.ContentLength,
                        Type = hpf.ContentType


                    });
                    return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
                }
                else
                {
                    r.Add(new tPaperLaidV()
                    {
                        // Name = "only .pdf Allowed",
                        Length = hpf.ContentLength,
                        Type = hpf.ContentType


                    });
                    // string Name = "only .pdf Allowed";
                    return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
                }
            }


            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }
        public JsonResult RemoveMainFiles()
        {
            string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ContentResult UploadMainDocFiles()
        {

            string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPaperLaidV>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPaperLaidV()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }
        public JsonResult RemoveMainFilesDoc()
        {
            string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ContentResult UploadSupFiles1()
        {

            string url = "~/DepartmentPdf/SupPdf" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPaperLaidV>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/DepartmentPdf/SupPdf" + "/" + CurrentSession.UserID;
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/DepartmentPdf/SupPdf" + "/" + CurrentSession.UserID), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPaperLaidV()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }
        public JsonResult RemoveSupFiles()
        {
            string url = "~/DepartmentPdf/SupPdf" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ContentResult UploadSupDocFiles()
        {

            string url = "~/DepartmentPdf/SuppReplyDoc" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPaperLaidV>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/DepartmentPdf/SuppReplyDoc" + "/" + CurrentSession.UserID;
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/DepartmentPdf/SuppReplyDoc" + "/" + CurrentSession.UserID), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPaperLaidV()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }
        public JsonResult RemoveSupDocFiles()
        {
            string url = "~/DepartmentPdf/SuppReplyDoc" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }


        #region Added Code venkat for Dynamic Menu
        public ActionResult GetSubMenu(int ModuleId, string ActIds)
        {
            mUserSubModules SM = new mUserSubModules();
            SM.ModuleId = ModuleId;
            SM.UserTypeID = Convert.ToInt32(CurrentSession.OfficeLevel);
            SM.SubUserTypeID = Convert.ToInt32(CurrentSession.SubUserTypeID);

            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
            {
                SM = (mUserSubModules)Helper.ExecuteService("Notice", "GetSubMenuByMenuId", SM);
                //SM.MemberId = MemberId;
                SM.ActionIds = ActIds;
            }
            else
            {
                SM = (mUserSubModules)Helper.ExecuteService("Notice", "GetSubMenuByMenuIdHindi", SM);
                //SM.MemberId = MemberId;
                SM.ActionIds = ActIds;
            }

            return PartialView("_SubMenu", SM);
        }


        public ActionResult GetActionView(string PaperType, int Count)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PaperEntryType = PaperType;
            model.SessionId = Convert.ToInt32(CurrentSession.SessionId);
            model.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            model.ResultCount = Count;
            if (CurrentSession.MenuId != null)
            {
                model.ModuleId = Convert.ToInt16(CurrentSession.MenuId);
            }
            // model.ModuleId = ModId;
            //model.RIds = ActionId;
            CurrentSession.SPSubject = "";
            CurrentSession.SPMinId = "";
            CurrentSession.SPFDate = "";
            CurrentSession.SPTDate = "";
            CurrentSession.SPDNum = "";
            return PartialView("_ActionViewForDept", model);


        }
        public ActionResult GetActionVieweFile(string PaperType, int Count, int SubModuleID)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PaperEntryType = PaperType;
            model.SessionId = Convert.ToInt32(CurrentSession.SessionId);
            model.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            model.ResultCount = Count;
            if (CurrentSession.MenuId != null)
            {
                model.ModuleId = Convert.ToInt16(CurrentSession.MenuId);
            }
            // model.ModuleId = ModId;
            //model.RIds = ActionId;
            CurrentSession.SPSubject = "";
            CurrentSession.SPMinId = "";
            CurrentSession.SPFDate = "";
            CurrentSession.SPTDate = "";
            CurrentSession.SPDNum = "";

            if (model.ModuleId == 64)
            {

                if (SubModuleID == 243)  //efile
                {
                    return PartialView("/Areas/eFile/Views/eFile/_MethodeFile.cshtml");

                }
                if (SubModuleID == 244) //receive
                {

                    return PartialView("/Areas/eFile/Views/eFile/_MethodReceive.cshtml");

                }
                if (SubModuleID == 245) //send
                {

                    return PartialView("/Areas/eFile/Views/eFile/_MethodSend.cshtml");

                }
                if (SubModuleID == 277) //Pendency Report
                {

                    return PartialView("/Areas/eFile/Views/eFile/_MethodPendencyReport.cshtml");

                }



            }
            else if (model.ModuleId == 78)
            {

                if (SubModuleID == 1288) //Received Paper
                {

                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodReceive.cshtml");

                }
                if (SubModuleID == 1289) //Send Paper
                {

                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodSend.cshtml");

                }

                if (SubModuleID == 1291)  //File
                {
                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodeFile.cshtml");

                }
                if (SubModuleID == 1292)  //Papers Mettings
                {
                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodPapersMettings.cshtml");

                }
                if (SubModuleID == 1295)  //Committe Reply Pendency
                {
                    return PartialView("/Areas/HouseCommittee/Views/CommitteReplyPendency/Index.cshtml");
                }
                if (SubModuleID == 1294)  //Committe Report to be Laid
                {
                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodsCommitteePaperLaid.cshtml");
                }
                if (SubModuleID == 1290)  //Draft Paper
                {
                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/_MethodDraft.cshtml");

                }

                if (SubModuleID == 1309)  // Letters/Correspondence
                {
                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/NewMethodDraftDept.cshtml");
                }

            }

            return PartialView("_ActionViewForDept", model);


        }

        public ActionResult GetListByPaperType(string PaperType, int AssemId, int SessId)
        {
            tPaperLaidV model = new tPaperLaidV();
            tMemberNotice tmmodel = new tMemberNotice();
            model.AssemblyId = AssemId;
            model.SessionId = SessId;
            // model.ModuleId = ModId;
            model.ModuleId = Convert.ToInt16(CurrentSession.MenuId);

            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }
            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                    model.IsPermissions = true ;
                }

            }
            if (model.ModuleId == 6)
            {
                if (PaperType == "Pending For Reply" || PaperType == "उत्तर के लिए लंबित")
                {
                    return PartialView("_PendingStaredQuestion", model);
                }
                else if (PaperType == "Draft Replies" || PaperType == "मसौदा का जवाब")
                {

                    return PartialView("_PendingForSendingStarred", model);
                }

                else if (PaperType == "Reply Sent" || PaperType == "जवाब भेजा")
                {

                    return PartialView("_SubmittedStaredQuestion", model);
                }
            }
            else if (model.ModuleId == 7)
            {
                if (PaperType == "Pending For Reply" || PaperType == "उत्तर के लिए लंबित")
                {

                    return PartialView("_PendingUnstaredQuestion", model);
                }
                else if (PaperType == "Draft Replies" || PaperType == "मसौदा का जवाब")
                {

                    return PartialView("_PendingForSendingUnStarred", model);
                }

                else if (PaperType == "Reply Sent" || PaperType == "जवाब भेजा")
                {

                    return PartialView("_SubmittedUnstaredQuestion", model);
                }
            }

            else if (model.ModuleId == 8)
            {
                if (PaperType == "Pending For Reply" || PaperType == "उत्तर के लिए लंबित")
                {

                    return PartialView("_GetNoticeInBoxList", tmmodel);
                }
                else if (PaperType == "Draft Replies" || PaperType == "मसौदा का जवाब")
                {

                    return PartialView("_GetNoticeReplyDraftSendList", model);
                }
                else if (PaperType == "Reply Sent" || PaperType == "जवाब भेजा")
                {

                    return PartialView("_GetNoticeReplySentList", model);

                }


            }


            else if (model.ModuleId == 23)
            {
                if (PaperType == "Draft Memorandum" || PaperType == "विधेयक का प्रारूप")
                {

                    return PartialView("_GetBillDraftes", model);
                }
                else if (PaperType == "Draft Sent" || PaperType == "भेजे गए विधेयकों")
                {

                    return PartialView("_GetBillSent", model);
                }
                else if (PaperType == "Upcoming LOB" || PaperType == "कार्यवाई के आगामी सूची")
                {

                    return PartialView("_GetBillUPLOBList", model);

                }
                else if (PaperType == "Laid In The House" || PaperType == "सदन में रखी गई")
                {

                    return PartialView("_GetBillLIHList", model);

                }
                else if (PaperType == "Pending To Lay" || PaperType == "अपूर्ण आधार शिला रखना")
                {

                    return PartialView("_GetBillPLIHList", model);
                }

            }

            else if (model.ModuleId == 10)
            {
                if (PaperType == "Draft Other Papers" || PaperType == "अन्य कागजात का प्रारूप")
                {
                    return PartialView("_GetOtherPaperLaidPendings", model);
                }
                else if (PaperType == "Other Papers Sent" || PaperType == "भेजे गए कागज")
                {

                    return PartialView("_GetOtherPaperLaidSubmit", model);
                }
                else if (PaperType == "Upcoming LOB" || PaperType == "कार्यवाई के आगामी सूची")
                {

                    return PartialView("_GetUpComingOtherPaperLaid", model);

                }
                else if (PaperType == "Laid In The House" || PaperType == "सदन में रखी गई")
                {

                    return PartialView("_OtherPaperLaidInHouse", model);

                }
                else if (PaperType == "Pending To Lay" || PaperType == "अपूर्ण आधार शिला रखना")
                {

                    return PartialView("_OtherPaperPendingToLay", model);
                }

            }
            else if (model.ModuleId == 12)
            {
                if (PaperType == "Pending Request" || PaperType == "लंबित अनुरोध")
                {
                    return PartialView("_GetPendingListUserAccess", model);
                }
                else if (PaperType == "Accepted Request" || PaperType == "स्वीकृत अनुरोध")
                {

                    return PartialView("_GetAcceptedListUserAccess", model);
                }
                else if (PaperType == "Rejected Request" || PaperType == "अस्वीकृत अनुरोध")
                {

                    return PartialView("_GetRejectedListUserAccess", model);

                }


            }
            else if (model.ModuleId == 51)
            {
                if (PaperType == "Update Grievances")
                {
                    return PartialView("_GetGrievanceList", model);
                }
            }

            else if (model.ModuleId == 78)  // Letters/Correspondence
                if (PaperType == "Item Pendency")
                {
                    return PartialView("/Areas/HouseCommittee/Views/CommitteReplyPendency/NewItemPendencyIndex.cshtml");
                }
                else
                {
                    return PartialView("/Areas/HouseCommittee/Views/HouseCommittee/NewMethodDraftDept.cshtml");
                }
            //Add By sanjay
            #region PublishDocuments
            else if (model.ModuleId == 57)
            {
                return PartialView("/Areas/PublishDocument/Views/PublishDocument/_DocumentList.cshtml");
            }
            else if (model.ModuleId == 52)
            {
                //List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                //System.Data.DataSet ds = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "sp_GetWorksSubModule", methodParameter);
                //if (ds.Tables[0].Rows[0]["SubModuleId"].ToString() == "Update Works")
                return PartialView("_UpdateWorks");
                //else
                //    return PartialView("ViewWorks");
                //return PartialView("_UpdateWorks");
                //return PartialView("/Areas/Grievances/Views/Grievances/_ViewWorks.cshtml");
                //return PartialView("/Areas/Grievances/Views/Grievances/_Works.cshtml");
            }
            #endregion




            return null;
        }

        public ActionResult SearchByType(string PaperType, int AssemId, int SessId)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            model.ModuleId = Convert.ToInt16(CurrentSession.MenuId);
            model.Title = CurrentSession.SPSubject;
            model.MemberName = CurrentSession.SPFDate;
            model.DiaryNumber = CurrentSession.SPDNum;
            model.PaperEntryType = PaperType;
            model.memMinList = (ICollection<mMember>)Helper.ExecuteService("PaperLaid", "GetMemberList", model);
            model.EventList = (List<mEvent>)Helper.ExecuteService("PaperLaid", "GetEventNamesList", null);
            return PartialView("_SearchByTypeForDept", model);
        }

        public ActionResult SearchDataByType(string PaperType, string Sub, int? MemId, string DNumber, int? QNumber, string BNumber, string user, string access)
        {
            tPaperLaidV Dmdl = new tPaperLaidV();
            tQuestion model = new tQuestion();

            tMemberNotice tmn = new tMemberNotice();
            Dmdl.ModuleId = Convert.ToInt16(CurrentSession.MenuId);
            Dmdl.PaperEntryType = PaperType;
            Dmdl.Title = Sub;
            Dmdl.QuestionNumber = QNumber ?? 0;
            Dmdl.PaperTypeID = QNumber ?? 0;
            Dmdl.MemberId = MemId ?? 0;
            Dmdl.MinistryId = MemId ?? 0;
            Dmdl.EventId = MemId ?? 0;
            Dmdl.DiaryNumber = DNumber;
            Dmdl.BillNumber = DNumber;
            Dmdl.NoticeNumber = DNumber;
            tmn.NoticeNumber = DNumber;
            tmn.MemberId = MemId ?? 0;
            tmn.Title = Sub;
            Dmdl.UserName = user;
            Dmdl.ModuleName = access;
            // Dmdl.PaperTypeID = DNumber;
            CurrentSession.SPSubject = Sub;
            CurrentSession.SPMinId = MemId.ToString();
            CurrentSession.SPDNum = DNumber;
            if (Dmdl.ModuleId == 6)
            {
                if (PaperType == "Pending For Reply")
                {
                    return PartialView("_PendingStaredQuestion", Dmdl);
                }
                else if (PaperType == "Draft Replies")
                {

                    return PartialView("_PendingForSendingStarred", Dmdl);
                }
                else if (PaperType == "Reply Sent")
                {

                    return PartialView("_SubmittedStaredQuestion", Dmdl);
                }
            }

            else if (Dmdl.ModuleId == 7)
            {
                if (PaperType == "Pending For Reply")
                {

                    return PartialView("_PendingUnstaredQuestion", Dmdl);
                }
                else if (PaperType == "Draft Replies")
                {

                    return PartialView("_PendingForSendingUnStarred", Dmdl);
                }

                else if (PaperType == "Reply Sent")
                {

                    return PartialView("_SubmittedUnstaredQuestion", Dmdl);
                }
            }
            else if (Dmdl.ModuleId == 8)
            {
                if (PaperType == "Pending For Reply")
                {

                    return PartialView("_GetNoticeInBoxList", tmn);
                }
                else if (PaperType == "Draft Replies")
                {

                    return PartialView("_GetNoticeReplyDraftSendList", Dmdl);
                }
                else if (PaperType == "Reply Sent")
                {

                    return PartialView("_GetNoticeReplySentList", tmn);

                }
                else if (PaperType == "Upcoming LOB")
                {

                    return PartialView("_GetNoticeULOBList", tmn);

                }
                else if (PaperType == "Laid In The House")
                {

                    return PartialView("_GetNoticeLIHList", tmn);
                }

                else if (PaperType == "Pending To Lay")
                {

                    return PartialView("_GetNoticePLIHList", tmn);
                }
            }
            else if (Dmdl.ModuleId == 9)
            {
                if (PaperType == "Draft Memorandum")
                {

                    return PartialView("_GetBillDraftes", Dmdl);
                }
                else if (PaperType == "Draft Sent")
                {

                    return PartialView("_GetBillSent", Dmdl);
                }
                else if (PaperType == "Upcoming LOB")
                {

                    return PartialView("_GetBillUPLOBList", Dmdl);

                }
                else if (PaperType == "Laid In The House")
                {

                    return PartialView("_GetBillLIHList", Dmdl);

                }
                else if (PaperType == "Pending To Lay")
                {

                    return PartialView("_GetBillPLIHList", Dmdl);
                }

            }
            else if (Dmdl.ModuleId == 10)
            {
                if (PaperType == "Draft Other Papers")
                {
                    return PartialView("_GetOtherPaperLaidPendings", Dmdl);
                }
                else if (PaperType == "Other Papers Sent")
                {

                    return PartialView("_GetOtherPaperLaidSubmit", Dmdl);
                }
                else if (PaperType == "Upcoming LOB")
                {

                    return PartialView("_GetUpComingOtherPaperLaid", Dmdl);

                }
                else if (PaperType == "Laid In The House")
                {

                    return PartialView("_OtherPaperLaidInHouse", Dmdl);

                }
                else if (PaperType == "Pending To Lay")
                {

                    return PartialView("_OtherPaperPendingToLay", Dmdl);
                }

            }
            else if (Dmdl.ModuleId == 12)
            {
                if (PaperType == "Pending Request")
                {
                    return PartialView("_GetPendingListUserAccess", Dmdl);
                }
                else if (PaperType == "Accepted Request")
                {

                    return PartialView("_GetAcceptedListUserAccess", Dmdl);
                }
                else if (PaperType == "Rejected Request")
                {

                    return PartialView("_GetRejectedListUserAccess", Dmdl);

                }


            }
            return null;
        }


        public ActionResult GetNoticeAttachment(string filepathName)
        {
            try
            {

                Stream stream = null;

                int bytesToRead = 10000;
                byte[] buffer = new Byte[bytesToRead];
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(filepathName);
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                stream = fileResp.GetResponseStream();
                return File(stream, "application/pdf");
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return View("FileNotFound");
            }
        }



        public ActionResult DisplayQuestionPDF(int id)
        {
            tPaperLaidTemp temp = new tPaperLaidTemp();
            temp.PaperLaidId = id;


            var FileName = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "PaperLaidFilePath", temp);


            string FilePath = FileName.FilePath + FileName.FileName;

            string filephyacesSetting = FilePath;
            temp.FilePath = filephyacesSetting;
            try
            {
                byte[] bytes = System.IO.File.ReadAllBytes(filephyacesSetting);
                return File(bytes, "application/pdf");
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {

                return View("FileNotFound");
            }

        }

        public ActionResult DisplayQuestionDoc(int id)
        {
            tPaperLaidTemp temp = new tPaperLaidTemp();
            temp.PaperLaidId = id;
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

            string Url = FileSettings.SettingValue;

            var FileName = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "PaperLaidFilePath", temp);

            string FilePath = FileName.FilePath + FileName.DocFileName;


            //string filephyacesSetting = _siteRepository.FileAccessingUrlPathLocal().SettingValue;
            //string pp = _LOBRepository.GetSQuestionFromID(Convert.ToInt16(id));
            string filephyacesSetting = Url + FilePath;
            temp.FilePath = filephyacesSetting;
            //try
            //{
            //    Stream stream = null;

            //    int bytesToRead = 10000;
            //    byte[] buffer = new Byte[bytesToRead];
            //    HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(filephyacesSetting);
            //    HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

            //    if (fileReq.ContentLength > 0)
            //        fileResp.ContentLength = fileReq.ContentLength;

            //    stream = fileResp.GetResponseStream();
            //    return File(stream, "application/msword");
            //}
            //catch (Exception e)
            //{
            //    return View("FileNotFound");
            //}
            return View("testPDF", temp);
        }

        public ActionResult DisplayQuestionSupPDF(int id)
        {
            tPaperLaidTemp temp = new tPaperLaidTemp();
            temp.PaperLaidId = id;
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

            string Url = FileSettings.SettingValue;

            var FileName = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "PaperLaidFilePath", temp);

            string FilePath = FileName.FilePath + FileName.SupFileName;



            string filephyacesSetting = Url + FilePath;
            temp.FilePath = filephyacesSetting;

            return View("testPDF", temp);
        }


        public ActionResult DisplayQuestionSupDoc(int id)
        {
            tPaperLaidTemp temp = new tPaperLaidTemp();
            temp.PaperLaidId = id;
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

            string Url = FileSettings.SettingValue;

            var FileName = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "PaperLaidFilePath", temp);

            string FilePath = FileName.FilePath + FileName.DocFileName;


            //string filephyacesSetting = _siteRepository.FileAccessingUrlPathLocal().SettingValue;
            //string pp = _LOBRepository.GetSQuestionFromID(Convert.ToInt16(id));
            string filephyacesSetting = Url + FilePath;
            temp.FilePath = filephyacesSetting;

            return View("testPDF", temp);
        }


        public ActionResult DisplayQuestionSignedPDF(int id)
        {
            tPaperLaidTemp temp = new tPaperLaidTemp();
            temp.PaperLaidId = id;
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

            string Url = FileSettings.SettingValue;

            var FileName = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "PaperLaidFilePath", temp);

            string FilePath = FileName.SignedFilePath;


            //string filephyacesSetting = _siteRepository.FileAccessingUrlPathLocal().SettingValue;
            //string pp = _LOBRepository.GetSQuestionFromID(Convert.ToInt16(id));
            string filephyacesSetting = Url + FilePath;
            temp.FilePath = filephyacesSetting;


            return View("testPDF", temp);



        }

        public ActionResult DownloadPdf(string Wid)
        {


            string[] Val = Wid.Split(',', ',');
            string fistId = Val[0].ToString();
            string SecondType = Val[1].ToString();

            int Id = Convert.ToInt32(fistId);

            string path = string.Empty;
            string FileName = string.Empty;

            try
            {
                tPaperLaidTemp temp = new tPaperLaidTemp();
                tMemberNotice PaperLaid = new tMemberNotice();

                temp.PaperLaidId = Id;
                PaperLaid.NoticeId = Id;

                var NewModel = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GetFilesDetials", temp);
                var NewModelPaperLaid = (tMemberNotice)Helper.ExecuteService("PaperLaid", "GetFilesDetialsPaperLaid", PaperLaid);
                //var NewModel = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GetFilesDetials", new tPaperLaidTemp { PaperLaidId = Id });
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                var Filelocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string FileStructurePath = Acess.SettingValue;
                if (SecondType == "M")
                {
                    string CompletePath = NewModel.objList.FirstOrDefault().FilePath + NewModel.objList.FirstOrDefault().DocFileName;
                    string url = FileStructurePath + NewsSettings.SettingValue + CompletePath;
                    path = Filelocation.SettingValue + "\\" + CompletePath;
                    Response.ContentType = "application/octet-stream";
                    Response.AppendHeader("Content-Disposition", "attachment;filename=" + NewModel.objList.FirstOrDefault().DocFileName);
                    Response.TransmitFile(path);
                    Response.End();
                    return new EmptyResult();
                }
                if (SecondType == "MD")
                {



                    string CompletePath = NewModel.objList.FirstOrDefault().FilePath + NewModel.objList.FirstOrDefault().DocFileName;
                    string url = FileStructurePath + CompletePath;
                    path = Filelocation.SettingValue + "\\" + CompletePath;
                    Response.ContentType = "application/octet-stream";
                    Response.AppendHeader("Content-Disposition", "attachment;filename=" + NewModel.objList.FirstOrDefault().DocFileName);
                    Response.TransmitFile(path);
                    Response.End();
                    return new EmptyResult();
                }
                else if (SecondType == "SD")
                {
                    string CompletePath = NewModel.objList.FirstOrDefault().FilePath + NewModel.objList.FirstOrDefault().SupDocFileName;
                    string url = FileStructurePath + CompletePath;
                    path = Filelocation.SettingValue + "\\" + CompletePath;
                    Response.ContentType = "application/octet-stream";
                    Response.AppendHeader("Content-Disposition", "attachment;filename=" + NewModel.objList.FirstOrDefault().SupDocFileName);
                    Response.TransmitFile(path);
                    Response.End();
                    return new EmptyResult();
                }
                else if (SecondType == "EN")
                {
                    string CompletePath = NewModelPaperLaid.memberNoticeList1.FirstOrDefault().OriDiaryFilePath + NewModelPaperLaid.memberNoticeList1.LastOrDefault().OriDiaryFileName;
                    string url = FileStructurePath + CompletePath;
                    path = Filelocation.SettingValue + "\\" + CompletePath;

                }
                else if (SecondType == "S")
                {
                    string CompletePath = NewModel.objList.FirstOrDefault().FilePath + NewModel.objList.FirstOrDefault().SupDocFileName;
                    string url = FileStructurePath + NewsSettings.SettingValue + CompletePath;
                    path = Filelocation.SettingValue + "\\" + CompletePath;
                    Response.ContentType = "application/octet-stream";
                    Response.AppendHeader("Content-Disposition", "attachment;filename=" + NewModel.objList.FirstOrDefault().SupDocFileName);
                    Response.TransmitFile(path);
                    Response.End();
                    return new EmptyResult();
                }
                else if (SecondType == "MP")
                {
                    string CompletePath = NewModel.objList.FirstOrDefault().FilePath + NewModel.objList.LastOrDefault().FileName;
                    string url = FileStructurePath + CompletePath;
                    path = Filelocation.SettingValue + "\\" + CompletePath;

                }
                else if (SecondType == "SP")
                {
                    string CompletePath = NewModel.objList.FirstOrDefault().FilePath + NewModel.objList.LastOrDefault().SupFileName;
                    string url = FileStructurePath + CompletePath;
                    path = Filelocation.SettingValue + "\\" + CompletePath;

                }
                else if (SecondType == "SignedP")
                {
                    string CompletePath = NewModel.objList.LastOrDefault().SignedFilePath;
                    string url = FileStructurePath + CompletePath;
                    path = Filelocation.SettingValue + "\\" + CompletePath;

                }

                byte[] bytes = System.IO.File.ReadAllBytes(path);

                return File(bytes, "application/pdf");
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {

                return RedirectToAction("FileNotFound", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });

            }
            finally
            {

            }

        }

        public ActionResult FileNotFound()
        {

            return View();
        }



        public JsonResult GetCount(int AssemblyId, int SessionId, int QtypeId)
        {

            tPaperLaidV mod = new tPaperLaidV();
            mod.AssemblyId = AssemblyId;
            mod.SessionId = SessionId;
            mod.QuestionTypeId = QtypeId;


            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                mod.UserID = new Guid(CurrentSession.UserID);

            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", mod);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    mod.DepartmentId = item.AssociatedDepts;
                    mod.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (mod.DepartmentId == null)
                {
                    mod.DepartmentId = CurrentSession.DeptID;
                }

            }
            mod = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "DraftRepliescount", mod);


            return Json(mod, JsonRequestBehavior.AllowGet);
        }



        public ActionResult GetDiaryDetails(string Diarynumber)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                tQuestionModel tQuestionModel = new tQuestionModel();
                tQuestionModel.DiaryNumber = Diarynumber;

                tQuestionModel = (tQuestionModel)Helper.ExecuteService("Questions", "GetQuestionDetailsDiaryNumber", tQuestionModel);
                // tQuestionModel.mQuesList = (ICollection<tQuestionModel>)Helper.ExecuteService("Questions", "GetQuestionDetailsDepartmentByID", tQuestionModel);

                foreach (var item in tQuestionModel.objQuestList1)
                {
                    tQuestionModel.DepartmentId = item.DepartmentId;
                    tQuestionModel.MemberN = item.MemberN;
                    tQuestionModel.AssemblyId = item.AssemblyId;
                    tQuestionModel.SessionId = item.SessionId;
                    tQuestionModel.DiaryNumber = item.DiaryNumber;
                    tQuestionModel.QuestionID = item.QuestionID;
                    tQuestionModel.ConstituencyName = item.ConstituencyName;
                    tQuestionModel.DepartmentN = item.DepartmentN;
                    tQuestionModel.MinisterN = item.MinisterN;
                    tQuestionModel.Subject = item.Subject;

                    if (item.IsFinalApproved == true)
                    {
                        tQuestionModel.QuestionNumber = item.QuestionNumber;
                    }

                    tQuestionModel.MainQuestion = item.MainQuestion;
                    tQuestionModel.RMemberName = item.RMemberName;
                    tQuestionModel.IsClubbed = item.IsClubbed;
                    tQuestionModel.QtypeId = item.QtypeId;
                    tQuestionModel.StatusId = item.StatusId;
                    tQuestionModel.MergeDiaryNo = item.MergeDiaryNo;
                    tQuestionModel.IsBracket = item.IsBracket;
                    tQuestionModel.BracketedWithDNo = item.BracketedWithDNo;

                }
                return PartialView("_GetDiaryDetails", tQuestionModel);
            }
            return null;
        }
        #endregion


        public ActionResult GetTransferHistoryDetails(string Diarynumber)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                tQuestionModel tQuestionModel = new tQuestionModel();
                tQuestionModel.DiaryNumber = Diarynumber;

                tQuestionModel = (tQuestionModel)Helper.ExecuteService("Questions", "GetHistoryDetailsDiaryNumber", tQuestionModel);


                foreach (var item in tQuestionModel.objQuestList1)
                {
                    tQuestionModel.DepartmentId = item.DepartmentId;

                    tQuestionModel.DiaryNumber = item.DiaryNumber;

                    tQuestionModel.ChangedDepartmentID = item.ChangedDepartmentID;



                }
                return PartialView("_GetTransferHistoryDetails", tQuestionModel);
            }
            return null;
        }



        public ActionResult UpdateAcknowledgementQuestion(tPaperLaidV obj, string DiaryNumber, string QId)
        {
            tPaperLaidV mdl = new tPaperLaidV();
            tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
            tQuestion tQuestions = new tQuestion();
            AdhaarDetails aadhar = new AdhaarDetails();

            obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            tQuestions.QuestionID = Convert.ToInt32(QId);
            tQuestions.IsAcknowledgmentDate = DateTime.Now;
            tQuestions.UserName = CurrentSession.UserName;
            tQuestions.AadharId = CurrentSession.AadharId;
            tQuestions = (tQuestion)Helper.ExecuteService("PaperLaid", "UpdateIsAcknowledgmentDateByDiaryNumber", tQuestions);



            tQuestionModel tQuestionModel = new tQuestionModel();

            tQuestionModel.DiaryNumber = DiaryNumber;
            tQuestionModel = (tQuestionModel)Helper.ExecuteService("Questions", "GetQuestionDetailsByDiaryNumber", tQuestionModel);
            // tQuestionModel.mQuesList = (ICollection<tQuestionModel>)Helper.ExecuteService("Questions", "GetQuestionDetailsDepartmentByID", tQuestionModel);



            foreach (var item in tQuestionModel.objQuestList1)
            {
                tQuestionModel.DepartmentId = item.DepartmentId;
                tQuestionModel.MemberN = item.MemberN;
                tQuestionModel.AssemblyId = item.AssemblyId;
                tQuestionModel.SessionId = item.SessionId;
                tQuestionModel.DiaryNumber = item.DiaryNumber;
                tQuestionModel.IsAcknowledgmentDate = item.IsAcknowledgmentDate;
                tQuestionModel.SubmittedDate = item.SubmittedDate;
                tQuestionModel.ConstituencyName = item.ConstituencyName;
                tQuestionModel.DepartmentN = item.DepartmentN;
                tQuestionModel.MinisterN = item.MinisterN;
                tQuestionModel.Subject = item.Subject;

                if (item.IsFinalApproved == true)
                {
                    tQuestionModel.QuestionNumber = item.QuestionNumber;
                }

                tQuestionModel.MainQuestion = item.MainQuestion;
                tQuestionModel.RMemberName = item.RMemberName;
                tQuestionModel.IsClubbed = item.IsClubbed;
                tQuestionModel.QtypeId = item.QtypeId;
                tQuestionModel.StatusId = item.StatusId;
                tQuestionModel.MergeDiaryNo = item.MergeDiaryNo;
                tQuestionModel.IsBracket = item.IsBracket;
                tQuestionModel.QuestionID = item.QuestionID;
                tQuestionModel.UserName = CurrentSession.UserName;
            }

            return PartialView("_GetFullRecordForDashboard", tQuestionModel);
            // return Json(tQuestionModel, JsonRequestBehavior.AllowGet);

        }

        public JsonResult CheckQStatus(int Qid)
        {

            tQuestionModel obj = new tQuestionModel();
            obj.QuestionID = Qid;
            string st = Helper.ExecuteService("Notice", "CheckQStatus", obj) as string;
            obj.DiaryNumber = st;
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Showppop()
        {
            tPaperLaidV paperLaidModel = new tPaperLaidV();
            return PartialView("_PopShow", paperLaidModel);

        }

    }
}
