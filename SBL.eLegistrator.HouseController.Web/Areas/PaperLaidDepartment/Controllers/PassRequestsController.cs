﻿using SBL.DomainModel.Models.Passes;
using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Controllers
{
    public class PassRequestsController : Controller
    {
        //
        // GET: /PaperLaidDepartment/PassRequests/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PassRequestIndex(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
        {
            return PartialView("_PassRequestIndex");
        }




        public ActionResult CreatePassRequest()
        {

            var departments = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Assembly", "GetAllDepartments", null);
            var Passtype = (List<PassCategory>)Helper.ExecuteService("Pass", "GetAllPassCategories", null);
           
            var model = new PassRequestViewModel()
            {

                DeptList = new SelectList(departments, "deptname", "deptname", null),
                PasstypeList = new SelectList(Passtype, "Name", "Name", null)

            };


            return PartialView("_CreatePassRequest", model);
        }



    }
}
