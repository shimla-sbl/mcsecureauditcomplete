﻿using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.PaperLaid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Notice;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.SiteSetting;
using Lib.Web.Mvc.JQuery.JqGrid;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.DomainModel.Models.User;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class NoticePaperLaidController : Controller
    {
        //
        // GET: /PaperLaidDepartment/NoticePaperLaid/
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ContentResult UploadFiles()
        {

            string url = "~/App_NoticeData" +"/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPaperLaidV>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/App_NoticeData" + "/" + CurrentSession.UserID;
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/App_NoticeData" + "/" + CurrentSession.UserID), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPaperLaidV()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        public ActionResult CreateNewNoticeReply(int noticeId, int sessionId, int assemblyId, int ModuleId, string MobileNumber)
        {

            string OTP = CurrentSession.UserID;
            string sub = OTP.Substring(0, 1);
            if (sub == "T")
            {
                if (!String.IsNullOrEmpty(CurrentSession.UserID))
                {
                    tMemberNotice data = new tMemberNotice();
                    data.NoticeId = noticeId;
                    data = (tMemberNotice)Helper.ExecuteService("Notice", "GetNoticeByNoticeID", data);

                    tPaperLaidV mdl = new tPaperLaidV();
                    mdl.Title = data.Subject;
                    mdl.Description = data.Subject;
                    mdl.SessionId = sessionId;
                    mdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    mdl.NoticeID = data.NoticeId;
                    mdl.ModuleId = ModuleId;
                    mdl.PaperLaidId = Convert.ToInt64(data.PaperLaidId); ;
                    mdl.MobileNo = MobileNumber;
                    mdl.RuleId = noticeId;
                    mdl.DocTypeId = 3;
                    mdl.DiaryNumber = data.NoticeNumber;
                    //mdl.PaperLaidId = Convert.ToInt64(noticeId);
                    //mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "ShowDraftBillById", mdl);

#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'long' is never equal to 'null' of type 'long?'
                    if (mdl.PaperLaidId != null)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'long' is never equal to 'null' of type 'long?'
                    {
                        if (mdl.PaperLaidId != 0)
                        {
                            mdl = (tPaperLaidV)Helper.ExecuteService("Notice", "EditPendingNoticeDetials", mdl);
                        }
                       
                    }
                    //mDepartment departmentModel = new mDepartment();
                    mEvent mEventModel = new mEvent();
                    mdl.DepartmentId = data.DepartmentId;
                    tPaperLaidV ministerDetails = new tPaperLaidV();
                    ministerDetails.DepartmentId = mdl.DepartmentId;


                    mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                    mdl.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", mdl);

                    mdl.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("BillPaperLaid", "GetDocumentType", mdl);
                    //mdl.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
                    List<mEvent> events = (List<mEvent>)Helper.ExecuteService("Events", "GetAllNoticeEventsDetails", mEventModel);
                    mdl.mEvents = events;
                    mdl.EventId = data.NoticeTypeID;
                    mdl.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);
                    //mdl.mBiatllTypes = (List<mBillType>)Helper.ExecuteService("BillPaperLaid", "GetBillTypes", "Testdata");

                    ICollection<int> yearList = new List<int>();
                    int currentYear = DateTime.Now.Year;
                    for (int i = currentYear - 5; i < currentYear; i++)
                    {
                        yearList.Add(i);
                    }
                    for (int i = currentYear; i < currentYear + 5; i++)
                    {
                        yearList.Add(i);
                    }
                    // mdl.yearList = yearList;

                    return PartialView("_CreateNewNoticeReplyOTP", mdl);
                }
                else
                {
                    return RedirectToAction("LoginUP", "Account");
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(CurrentSession.UserID))
                {
                    tMemberNotice data = new tMemberNotice();
                    data.NoticeId = noticeId;
                    data = (tMemberNotice)Helper.ExecuteService("Notice", "GetNoticeByNoticeID", data);

                    tPaperLaidV mdl = new tPaperLaidV();
                    mdl.Title = data.Subject;
                    mdl.Description = data.Subject;
                    mdl.SessionId = sessionId;
                    mdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    mdl.NoticeID = data.NoticeId;
                    mdl.ModuleId = ModuleId;
                    mdl.MinistryId = data.MinistryId.Value;
                    // mdl.MobileNo = MobileNumber;
                    //mdl.PaperLaidId = Convert.ToInt64(noticeId);
                    //mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "ShowDraftBillById", mdl);

                    //mDepartment departmentModel = new mDepartment();
                    mEvent mEventModel = new mEvent();
                    mdl.DepartmentId = data.DepartmentId;
                    tPaperLaidV ministerDetails = new tPaperLaidV();
                    ministerDetails.DepartmentId = mdl.DepartmentId;

                    mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                    mdl.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", mdl);

                    mdl.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("BillPaperLaid", "GetDocumentType", mdl);
                    //mdl.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
                    List<mEvent> events = (List<mEvent>)Helper.ExecuteService("Events", "GetAllNoticeEventsDetails", mEventModel);
                    mdl.mEvents = events;
                    mdl.EventId = data.NoticeTypeID;
                    mdl.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);
                    //mdl.mBiatllTypes = (List<mBillType>)Helper.ExecuteService("BillPaperLaid", "GetBillTypes", "Testdata");

                    ICollection<int> yearList = new List<int>();
                    int currentYear = DateTime.Now.Year;
                    for (int i = currentYear - 5; i < currentYear; i++)
                    {
                        yearList.Add(i);
                    }
                    for (int i = currentYear; i < currentYear + 5; i++)
                    {
                        yearList.Add(i);
                    }
                    // mdl.yearList = yearList;

                    return PartialView("_CreateNewNoticeReply", mdl);
                }
                else
                {
                    return RedirectToAction("LoginUP", "Account");
                }
            }
           
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PaperLaidSubmitt(HttpPostedFileBase file, HttpPostedFileBase DocFile, tPaperLaidV obj, string submitButton, string UpdatesubmitButton)
        {


            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {

                string OTP = CurrentSession.UserID;
                string sub = OTP.Substring(0, 1);
                if (sub == "T")
                {
                    tPaperLaidV mdl = new tPaperLaidV();
                    tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                    {
                        mdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                        obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    }

                    if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                    {
                        mdl.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                        obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                    }

                    string DocName = "";

                    switch (submitButton)
                    {

                        //case "Send":
                        //    {
                        //        obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                        //        obj.ModifiedBy = CurrentSession.UserName;
                        //        obj.ModifiedWhen = DateTime.Now;
                        //        mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                        //        mdl.EventId = obj.EventId;

                        //        if (file != null)
                        //        {
                        //            obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                        //            string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                        //            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                        //            if (!Dir.Exists)
                        //            {
                        //                Dir.Create();
                        //            }
                        //            string ext = Path.GetExtension(file.FileName);
                        //            var vers = obj.FileVersion.GetValueOrDefault() + 1;
                        //            string fileName = file.FileName.Replace(ext, "");
                        //            string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                        //            file.SaveAs(Server.MapPath(Url + actualFileName));
                        //            mdlTemp.FileName = actualFileName;
                        //            mdlTemp.FilePath = Url;
                        //            mdlTemp.Version = vers;
                        //            mdlTemp.PaperLaidId = mdl.PaperLaidId;
                        //            mdlTemp.DeptSubmittedDate = DateTime.Now;
                        //            mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                        //        }

                        //        mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);

                        //        tMemberNotice needupdatedata = new tMemberNotice();
                        //        needupdatedata.NoticeId = mdl.NoticeID;
                        //        needupdatedata.PaperLaidId = mdl.PaperLaidId;

                        //        var data = (tMemberNotice)Helper.ExecuteService("Notice", "UpdateNoticeById", needupdatedata);

                        //        mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                        //        mdl.ModifiedBy = CurrentSession.UserName;
                        //        mdl.ModifiedWhen = DateTime.Now;
                        //        mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", mdl);

                        //        break;
                        //    }
                        case "Update":
                            {
                                obj.ModifiedBy = CurrentSession.UserName;
                                obj.ModifiedWhen = DateTime.Now;
                                obj.tpaperLaidTempId = 0;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);
                                //mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", mdl);
                                //if (file != null)
                                //{
                                //    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                                //    string Url = mdlTemp.FilePath;
                                //    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                //    if (!Dir.Exists)
                                //    {
                                //        Dir.Create();
                                //    }
                                //    string ext = Path.GetExtension(file.FileName);
                                //    var vers = obj.FileVersion.GetValueOrDefault();
                                //    string fileName = file.FileName.Replace(ext, "");
                                //    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                                //    file.SaveAs(Server.MapPath(Url + actualFileName));
                                //    mdlTemp.FileName = actualFileName;
                                //    string url = "/OTPDeptUpload/" + "PDFNotice/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                //    string directory = Server.MapPath(url);
                                //    if (!Directory.Exists(directory))
                                //    {
                                //        Directory.CreateDirectory(directory);
                                //    }
                                //    fileName = file.FileName;
                                //    var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                //    System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                                //    //obj.TempPdfFile = url + fileName;
                                //}
                                //if (DocFile == null)
                                //{
                                //    string[] savedFileName = Directory.GetFiles(Server.MapPath("~/App_NoticeData"));
                                //    string SourceFile = savedFileName[0];
                                //    foreach (string page in savedFileName)
                                //    {
                                //        string name = Path.GetFileName(page);
                                //        string nameKey = Path.GetFileNameWithoutExtension(page);
                                //        string directory = Path.GetDirectoryName(page);
                                //        //
                                //        // Display the Path strings we extracted.
                                //        //
                                //        Console.WriteLine("{0}, {1}, {2}, {3}",
                                //        page, name, nameKey, directory);
                                //       // mdlTemp.DocFileName = name;

                                //    }


                                //    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                                //    string Url = mdlTemp.FilePath;
                                //    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                //    if (!Dir.Exists)
                                //    {
                                //        Dir.Create();
                                //    }
                                //    string ext = Path.GetExtension(file.FileName);

                                //    string fileName = file.FileName.Replace(ext, "");
                                //    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId  + ext;
                                //    file.SaveAs(Server.MapPath(Url + actualFileName));
                                //    mdlTemp.DocFileName = actualFileName;
                                //    string url = "/OTPDeptUpload/" + "DOCNotice/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                //    string direct = Server.MapPath(url);
                                //    if (!Directory.Exists(direct))
                                //    {
                                //        Directory.CreateDirectory(direct);
                                //    }
                                //    fileName = DocFile.FileName;
                                //    var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                //    System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                                //   // obj.TempDocFile = url + fileName;
                                //}
                                if (DocFile == null)
                                {
                                    string[] savedFileName = Directory.GetFiles(Server.MapPath("~/App_NoticeData"));
                                    string SourceFile = savedFileName[0];
                                    foreach (string page in savedFileName)
                                    {
                                        string name = Path.GetFileName(page);
                                        string nameKey = Path.GetFileNameWithoutExtension(page);
                                        string directory = Path.GetDirectoryName(page);
                                        //
                                        // Display the Path strings we extracted.
                                        //
                                        Console.WriteLine("{0}, {1}, {2}, {3}",
                                        page, name, nameKey, directory);
                                        DocName = name;


                                    }
                                    mdlTemp.DocFileName = DocName;
                                    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                    string ext = Path.GetExtension(mdlTemp.DocFileName);

                                    var DocVer = obj.DocFileVersion.GetValueOrDefault() + 1;


                                    string fileName = mdlTemp.DocFileName.Replace(ext, "");

                                    string actualDocFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + DocVer + ext;



                                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                    string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    DirectoryInfo Dir = new DirectoryInfo(Url);
                                    if (!Dir.Exists)
                                    {
                                        Dir.Create();
                                    }

                                    string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualDocFileName);
                                    //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                    System.IO.File.Copy(SourceFile, path, true);
                                    //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                    mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                    var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                    string FileStructurePath = Acess.SettingValue;
                                    mdlTemp.FileStructurePath = FileStructurePath;
                                    mdlTemp.DocFileName = actualDocFileName;
                                    mdlTemp.DocFileVersion = DocVer;
                                    //string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    //// string Url = mdlTemp.FilePath;
                                    //DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                    //if (!Dir.Exists)
                                    //{
                                    //    Dir.Create();
                                    //}
                                    //string ext = Path.GetExtension(mdlTemp.DocFileName);
                                    //// string ext = Path.GetExtension(file.FileName);
                                    //string fileName = mdlTemp.DocFileName.Replace(ext, "");
                                    ////string fileName = file.FileName.Replace(ext, "");
                                    //string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + ext;
                                    ////file.SaveAs(Server.MapPath(Url + actualFileName));
                                    //var path1 = Path.Combine(Server.MapPath("~" + Url), actualFileName);
                                    //System.IO.File.Copy(SourceFile, path1, true);
                                   // mdlTemp.DocFileName = actualFileName;
                                    string url1 = "/OTPDeptUpload/" + "PDFNotice/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    string directory1 = Server.MapPath(url1);
                                    if (!Directory.Exists(directory1))
                                    {
                                        Directory.CreateDirectory(directory1);
                                    }
                                    fileName = mdlTemp.DocFileName;
                                    var path1 = Path.Combine(Server.MapPath("~" + url1), fileName);
                                    System.IO.File.Copy(SourceFile, path1, true);
                                    obj.TempDocFile = url1 + fileName;
                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;

                                }
                                //mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                //mdlTemp.DeptSubmittedDate = DateTime.Now;
                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaidNotices", mdlTemp);

                                mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                                //mdl.ModifiedBy = CurrentSession.UserName;
                                //mdl.ModifiedWhen = DateTime.Now;
                               mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", mdl);

                                break;
                            }
                        case "Save":
                            {

                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);

                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                                mdl.EventId = obj.EventId;

                                //if (file != null)
                                //{
                                //    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                //    string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                //    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                //    if (!Dir.Exists)
                                //    {
                                //        Dir.Create();
                                //    }
                                //    string ext = Path.GetExtension(file.FileName);
                                //    var vers = obj.FileVersion.GetValueOrDefault() + 1;
                                //    string fileName = file.FileName.Replace(ext, "");
                                //    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                                //    file.SaveAs(Server.MapPath(Url + actualFileName));
                                //    mdlTemp.FileName = actualFileName;
                                //    mdlTemp.FilePath = Url;
                                //    mdlTemp.Version = vers;
                                //    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                //}
                                if (DocFile == null)
                                {
                                    string[] savedFileName = Directory.GetFiles(Server.MapPath("~/App_NoticeData" + "/" + CurrentSession.UserID));
                                    string SourceFile = savedFileName[0];
                                    foreach (string page in savedFileName)
                                    {
                                        string name = Path.GetFileName(page);
                                        string nameKey = Path.GetFileNameWithoutExtension(page);
                                        string directory = Path.GetDirectoryName(page);
                                        //
                                        // Display the Path strings we extracted.
                                        //
                                        Console.WriteLine("{0}, {1}, {2}, {3}",
                                        page, name, nameKey, directory);

                                        mdlTemp.DocFileName = name;

                                    }

                                    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                    string ext = Path.GetExtension(mdlTemp.DocFileName);

                                    var DocVer = obj.DocFileVersion.GetValueOrDefault() + 1;


                                    string fileName = mdlTemp.DocFileName.Replace(ext, "");

                                    string actualDocFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + DocVer + ext;



                                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                    string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    DirectoryInfo Dir = new DirectoryInfo(Url);
                                    if (!Dir.Exists)
                                    {
                                        Dir.Create();
                                    }

                                    string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualDocFileName);
                                    //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                    System.IO.File.Copy(SourceFile, path, true);
                                    //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                    mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                    var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                    string FileStructurePath = Acess.SettingValue;
                                    mdlTemp.FileStructurePath = FileStructurePath;
                                    mdlTemp.DocFileName = actualDocFileName;
                                    mdlTemp.DocFileVersion = DocVer;
                                    //string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    //// string Url = mdlTemp.FilePath;
                                    //DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                    //if (!Dir.Exists)
                                    //{
                                    //    Dir.Create();
                                    //}
                                    //string ext = Path.GetExtension(mdlTemp.DocFileName);
                                    //// string ext = Path.GetExtension(file.FileName);
                                    //string fileName = mdlTemp.DocFileName.Replace(ext, "");
                                    ////string fileName = file.FileName.Replace(ext, "");
                                    //string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + ext;
                                    ////file.SaveAs(Server.MapPath(Url + actualFileName));
                                    //var path1 = Path.Combine(Server.MapPath("~" + Url), actualFileName);
                                    //System.IO.File.Copy(SourceFile, path1, true);
                                    //mdlTemp.DocFileName = actualFileName;
                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    //mdlTemp.FilePath = Url;
                                    string url1 = "/OTPDeptUpload/" + "PDFNotice/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    string directory1 = Server.MapPath(url1);
                                    if (!Directory.Exists(directory1))
                                    {
                                        Directory.CreateDirectory(directory1);
                                    }
                                    fileName = mdlTemp.DocFileName;
                                    var path1 = Path.Combine(Server.MapPath("~" + url1), fileName);
                                    System.IO.File.Copy(SourceFile, path1, true);
                                    obj.TempDocFile = url1 + fileName;

                                }
                                //mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);

                                tMemberNotice needupdatedata = new tMemberNotice();
                                needupdatedata.NoticeId = mdl.NoticeID;
                                needupdatedata.PaperLaidId = mdl.PaperLaidId;

                                var data = (tMemberNotice)Helper.ExecuteService("Notice", "UpdateNoticeById", needupdatedata);

                                mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;

                                //mdl.ModifiedBy = CurrentSession.UserName;
                                //mdl.ModifiedWhen = DateTime.Now;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", mdl);

                                break;
                            }
                        //case "Update Send":
                        //    {
                        //        mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", obj);
                        //        obj.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                        //        mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);

                        //        if (file != null)
                        //        {
                        //            obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                        //            string Url = mdlTemp.FilePath;
                        //            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                        //            if (!Dir.Exists)
                        //            {
                        //                Dir.Create();
                        //            }
                        //            var vers = obj.FileVersion.GetValueOrDefault();
                        //            string ext = Path.GetExtension(file.FileName);
                        //            string fileName = file.FileName.Replace(ext, "");
                        //            string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                        //            file.SaveAs(Server.MapPath(Url + actualFileName));
                        //            mdlTemp.FileName = actualFileName;
                        //        }
                        //        if (DocFile != null)
                        //        {
                        //            obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                        //            string Url = mdlTemp.FilePath;
                        //            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                        //            if (!Dir.Exists)
                        //            {
                        //                Dir.Create();
                        //            }
                        //            string ext = Path.GetExtension(DocFile.FileName);

                        //            string fileName = DocFile.FileName.Replace(ext, "");
                        //            string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId  + ext;
                        //            file.SaveAs(Server.MapPath(Url + actualFileName));
                        //            mdlTemp.DocFileName = actualFileName;

                        //        }
                        //        mdlTemp.DeptSubmittedDate = DateTime.Now;
                        //        mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                        //        mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaid", mdlTemp);
                        //        break;
                        //    }
                    }
                    var msg = (string)Helper.ExecuteService("OneTimeUserRegistration", "SaveAuditTrialOTPRegistrationNotices", obj);
                    var msg1 = (string)Helper.ExecuteService("OneTimeUserRegistration", "UpdateDateUserRegistration", obj);
                    TempData["lSM"] = "NTC";
                    TempData["Msg"] = "Save";
                    return RedirectToAction("Index", "OneTimeUserRegistration", new { area = "OneTimeUserRegistration" });

                }
                else
                {

                    tPaperLaidV mdl = new tPaperLaidV();
                    tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                    {
                        mdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                        obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    }

                    if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                    {
                        mdl.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                        obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                    }

                    // obj.DepartmentId = CurrentSession.DeptID;
                    submitButton = "Send";

                    if (submitButton == "Send")
                    {
                        switch (submitButton)
                        {

                            case "Send":
                                {
                                    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                                    obj.ModifiedBy = CurrentSession.UserName;
                                    obj.ModifiedWhen = DateTime.Now;
                                    mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                                    mdl.EventId = obj.EventId;

                                    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                                    if (file == null)
                                    {
                                        string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                                        string directory = Server.MapPath(url);
                                        if (Directory.Exists(directory))
                                        {
                                            string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                                            string SourceFile = savedFileName[0];
                                            foreach (string page in savedFileName)
                                            {
                                                string name = Path.GetFileName(page);
                                                string nameKey = Path.GetFileNameWithoutExtension(page);
                                                string directory1 = Path.GetDirectoryName(page);
                                                //
                                                // Display the Path strings we extracted.
                                                //
                                                Console.WriteLine("{0}, {1}, {2}, {3}",
                                                page, name, nameKey, directory1);
                                                mdlTemp.FileName = name;
                                            }
                                            string ext = Path.GetExtension(mdlTemp.FileName);
                                            string fileNam1 = mdlTemp.FileName.Replace(ext, "");


                                            var vers = obj.FileVersion.GetValueOrDefault() + 1;


                                            string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;


                                            var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                            string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                            DirectoryInfo Dir = new DirectoryInfo(Url);
                                            if (!Dir.Exists)
                                            {
                                                Dir.Create();
                                            }

                                            string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);
                                            string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                            System.IO.File.Copy(SourceFile, path, true);
                                            mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                            //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                                            string FileStructurePath = Acess.SettingValue;
                                            mdlTemp.FileStructurePath = FileStructurePath;
                                            mdlTemp.FileName = actualFileName;
                                            mdlTemp.Version = vers;

                                        }
                                        mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                        mdlTemp.Version = obj.Count;


                                    }


                                    //if (file != null)
                                    //{
                                    //    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                                    //    string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                    //    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                    //    if (!Dir.Exists)
                                    //    {
                                    //        Dir.Create();
                                    //    }
                                    //    string ext = Path.GetExtension(file.FileName);
                                    //    var vers = obj.FileVersion.GetValueOrDefault() + 1;
                                    //    string fileName = file.FileName.Replace(ext, "");
                                    //    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                                    //    file.SaveAs(Server.MapPath(Url + actualFileName));
                                    //    mdlTemp.FileName = actualFileName;
                                    //    mdlTemp.FilePath = Url;
                                    //    mdlTemp.Version = vers;
                                    //    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    //    //mdlTemp.DeptSubmittedDate = DateTime.Now;
                                    //   // mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                    //}


                                    if (DocFile == null)
                                    {

                                        string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                                        string directory = Server.MapPath(url);
                                        if (Directory.Exists(directory))
                                        {
                                            string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID));
                                            string SourceFile = savedFileName[0];
                                            foreach (string page in savedFileName)
                                            {
                                                string name = Path.GetFileName(page);
                                                string nameKey = Path.GetFileNameWithoutExtension(page);
                                                string directory1 = Path.GetDirectoryName(page);
                                                //
                                                // Display the Path strings we extracted.
                                                //
                                                Console.WriteLine("{0}, {1}, {2}, {3}",
                                                page, name, nameKey, directory1);
                                                mdlTemp.DocFileName = name;
                                            }

                                            string ext = Path.GetExtension(mdlTemp.DocFileName);

                                            var DocVer = obj.DocFileVersion.GetValueOrDefault() + 1;


                                            string fileName = mdlTemp.DocFileName.Replace(ext, "");

                                            string actualDocFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + DocVer + ext;



                                            var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                            string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                            DirectoryInfo Dir = new DirectoryInfo(Url);
                                            if (!Dir.Exists)
                                            {
                                                Dir.Create();
                                            }

                                            string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualDocFileName);
                                            //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                            System.IO.File.Copy(SourceFile, path, true);
                                            //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                            mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                            string FileStructurePath = Acess.SettingValue;
                                            mdlTemp.FileStructurePath = FileStructurePath;
                                            mdlTemp.DocFileName = actualDocFileName;
                                            mdlTemp.DocFileVersion = DocVer;
                                        }

                                        mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                        mdlTemp.DocFileVersion = obj.MainDocCount;

                                    }



                                    //if (DocFile != null)
                                    //{
                                    //    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                                    //    string Url = mdlTemp.FilePath;
                                    //    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                    //    if (!Dir.Exists)
                                    //    {
                                    //        Dir.Create();
                                    //    }
                                    //    string ext = Path.GetExtension(file.FileName);
                                    //     var DocVer = obj.DocFileVersion.GetValueOrDefault() + 1;
                                    //    string fileName = file.FileName.Replace(ext, "");
                                    //    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + DocVer + ext;
                                    //    file.SaveAs(Server.MapPath(Url + actualFileName));
                                    //    mdlTemp.DocFileName = actualFileName;
                                    //    mdlTemp.DocFileVersion = DocVer; 

                                    //}
                                    if (mdl.DeptActivePaperId != null)
                                    {
                                        mdlTemp.PaperLaidTempId = (long)mdl.DeptActivePaperId;
                                    }
                                    mdlTemp.AssemblyId = CurrentSession.AssemblyId;
                                    mdlTemp.SessionId = CurrentSession.SessionId;
                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);

                                    tMemberNotice needupdatedata = new tMemberNotice();
                                    needupdatedata.NoticeId = mdl.NoticeID;
                                    needupdatedata.PaperLaidId = mdl.PaperLaidId;
                                   // needupdatedata.IsAcknowledgmentDate = DateTime.Now;
                                    var data = (tMemberNotice)Helper.ExecuteService("Notice", "UpdateNoticeById", needupdatedata);

                                    mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                                    mdl.ModifiedBy = CurrentSession.UserName;
                                    mdl.ModifiedWhen = DateTime.Now;

                                    mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", mdl);

                                    break;

#pragma warning disable CS0162 // Unreachable code detected
                                    string url1 = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
#pragma warning restore CS0162 // Unreachable code detected
                                    string MainPDFdirectory1 = Server.MapPath(url1);
                                    if (Directory.Exists(MainPDFdirectory1))
                                    {
                                        System.IO.Directory.Delete(MainPDFdirectory1, true);
                                    }
                                    string urlMainDoc = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                                    string MainDocdirectory = Server.MapPath(urlMainDoc);
                                    if (Directory.Exists(MainDocdirectory))
                                    {
                                        System.IO.Directory.Delete(MainDocdirectory, true);
                                    }
                                }
                            case "Update":
                                {
                                    //obj.tpaperLaidTempId = 0;
                                    mdlTemp.DocFileName = obj.DocFileName;
                                    mdlTemp.SupDocFileName = obj.SupDocFileName;
                                    mdlTemp.FileName = obj.FileName;
                                    mdlTemp.SupFileName = obj.SupFileName;
                                    mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);
                                    if (mdlTemp.Version != null)
                                    {
                                        mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", mdl);
                                    }
                                    if (file != null)
                                    {
                                        obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                                        string Url = mdlTemp.FilePath;
                                        DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }
                                        string ext = Path.GetExtension(file.FileName);
                                        var vers = obj.FileVersion.GetValueOrDefault();
                                        string fileName = file.FileName.Replace(ext, "");
                                        string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                                        file.SaveAs(Server.MapPath(Url + actualFileName));
                                        mdlTemp.FileName = actualFileName;

                                        mdlTemp.FilePath = Url;
                                        mdlTemp.Version = vers;
                                        mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                        mdlTemp.DeptSubmittedDate = DateTime.Now;
                                        mdlTemp.DeptSubmittedBy = CurrentSession.UserName;

                                    }
                                    if (DocFile != null)
                                    {
                                        obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                                        string Url = mdlTemp.FilePath;
                                        DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }
                                        string ext = Path.GetExtension(file.FileName);
                                        var DocVer = obj.DocFileVersion.GetValueOrDefault() + 1;
                                        string fileName = file.FileName.Replace(ext, "");
                                        string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + DocVer + ext;
                                        file.SaveAs(Server.MapPath(Url + actualFileName));
                                        mdlTemp.DocFileName = actualFileName;
                                        mdlTemp.DocFileVersion = DocVer;

                                    }
                                    mdlTemp.PaperLaidTempId = obj.tpaperLaidTempId;
                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaid", mdlTemp);

                                    mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                                    mdl.ModifiedBy = CurrentSession.UserName;
                                    mdl.ModifiedWhen = DateTime.Now;
                                    mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", mdl);

                                    break;
                                }
                            case "Save":
                                {

                                    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);

                                    mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                                    mdl.EventId = obj.EventId;

                                    if (file != null)
                                    {
                                        obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                        string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }
                                        string ext = Path.GetExtension(file.FileName);
                                        var vers = obj.FileVersion.GetValueOrDefault() + 1;
                                        string fileName = file.FileName.Replace(ext, "");
                                        string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                                        file.SaveAs(Server.MapPath(Url + actualFileName));
                                        mdlTemp.FileName = actualFileName;
                                        mdlTemp.FilePath = Url;
                                        mdlTemp.Version = vers;
                                        mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    }

                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);

                                    tMemberNotice needupdatedata = new tMemberNotice();
                                    needupdatedata.NoticeId = mdl.NoticeID;
                                    needupdatedata.PaperLaidId = mdl.PaperLaidId;

                                    var data = (tMemberNotice)Helper.ExecuteService("Notice", "UpdateNoticeById", needupdatedata);

                                    mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                                    mdl.ModifiedBy = CurrentSession.UserName;
                                    mdl.ModifiedWhen = DateTime.Now;
                                    mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", mdl);

                                    break;
                                }
                            case "Update Send":
                                {
                                    mdlTemp.DocFileName = obj.DocFileName;
                                    mdlTemp.SupDocFileName = obj.SupDocFileName;
                                    mdlTemp.FileName = obj.FileName;
                                    mdlTemp.SupFileName = obj.SupFileName;
                                    if (mdlTemp.Version != null)
                                    {
                                        mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", obj);
                                        obj.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                                    }
                                    else
                                    {
                                        mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetPaperLaidTemp", obj);
                                        obj.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                                    }

                                    mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);

                                    if (file != null)
                                    {
                                        obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                                        string Url = mdlTemp.FilePath;
                                        DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        var vers = obj.FileVersion.GetValueOrDefault();





                                        string ext = Path.GetExtension(file.FileName);
                                        string fileName = file.FileName.Replace(ext, "");
                                        string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                                        file.SaveAs(Server.MapPath(Url + actualFileName));
                                        mdlTemp.FileName = actualFileName;
                                        mdlTemp.Version = vers;
                                    }

                                    if (DocFile != null)
                                    {
                                        obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);

                                        string Url = mdlTemp.FilePath;
                                        DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }
                                        string ext = Path.GetExtension(file.FileName);

                                        string fileName = file.FileName.Replace(ext, "");
                                        string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "SUP" + ext;
                                        file.SaveAs(Server.MapPath(Url + actualFileName));
                                        mdlTemp.DocFileName = actualFileName;

                                    }
                                    mdlTemp.DeptSubmittedDate = DateTime.Now;
                                    mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                    mdlTemp.PaperLaidTempId = obj.PaperLaidId;

                                    mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaid", mdlTemp);
                                    break;
                                }
                        }
                    }
                    if (Request.IsAjaxRequest())
                    {
                        tPaperLaidV mod = new tPaperLaidV();
                        List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                        AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", mod);

                        if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                        {
                            foreach (var item in AuthorisedEmp)
                            {
                                mod.DepartmentId = item.AssociatedDepts;
                                mod.IsPermissions = item.IsPermissions; ;
                            }

                            if (mod.DepartmentId == null)
                            {
                                mod.DepartmentId = CurrentSession.DeptID;
                            }
                        }
                        else
                        {
                            mod.DepartmentId = CurrentSession.DeptID;
                        }
                        mod.ModuleId = 8;
                        var data = submitButton;

                        return Json(mod, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {


                    }
                    return null;
                    //TempData["lSM"] = "NTC";
                    //TempData["Msg"] = "Save";
                    //return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment");
                }
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        
        #region "Changes Regarding New Template Implementations Start here"
        [HttpPost]
        public ActionResult GetAllCurrentNotices(int PageNumber, int RowsPerPage, int loopStart, int loopEnd)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV viewModel = new tPaperLaidV();
                return PartialView("_GetAllCurrentNotices", viewModel);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllCurrentNotices(JqGridRequest request)
        {
            tPaperLaidV model = new tPaperLaidV();
            tMemberNotice inputmodel = new tMemberNotice();          
            inputmodel.PAGE_SIZE = request.RecordsCount;

            inputmodel.PageIndex = request.PageIndex +1;

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                inputmodel.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                inputmodel.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    inputmodel.DepartmentId = item.AssociatedDepts;
                    inputmodel.IsPermissions = item.IsPermissions; ;
                }
            }
            else
            {
                inputmodel.DepartmentId = CurrentSession.DeptID;
            }
            /*End*/            

            var result = (tPaperLaidV)Helper.ExecuteService("Notice", "GetAllCurrentNoticesList", inputmodel);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.ListtPaperLaidV.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tPaperLaidV>(Convert.ToString(questionList.NoticeID), new tPaperLaidV(questionList)));
            return new JqGridJsonResult() { Data = response };
        }

        #endregion
        //added code venkat for dynamic 
        public ActionResult GetNoticeInBoxList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd, string Noticenumber, string title, int? MemberId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tMemberNotice model = new tMemberNotice();
                //added code venkat for dynamic 
                model.NoticeNumber = Noticenumber;
                model.Title = title;
                model.MemberId = MemberId ?? 0;
                //end--------------------------------
                //model.DepartmentId = CurrentSession.DeptID;
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                //model.SessionID = siteSettingMod.SessionCode;
                //model.AssemblyID = siteSettingMod.AssemblyCode;
                //model.loopEnd = loopEnd;
                //model.loopStart = loopStart;
                //model.RowsPerPage = RowsPerPage;
                //model.PageNumber = PageNumber;
                //model.PageIndex = PageNumber;
                //model.PAGE_SIZE = RowsPerPage;
                //model.selectedPage = PageNumber;
                //model = (tMemberNotice)Helper.ExecuteService("Notice", "GetNoticeInBoxList", model);
                
                return PartialView("_GetNoticeInBoxList", model);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }
        //added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNoticeInBoxGridList(JqGridRequest request, string Noticenumber, string title, int? MemberId)
        {

            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tMemberNotice inputmodel = new tMemberNotice();
                tPaperLaidV model = new tPaperLaidV();
                inputmodel.PAGE_SIZE = request.RecordsCount;
                inputmodel.PageIndex = request.PageIndex +1;
                //added code venkat for dynamic 
                inputmodel.NoticeNumber = Noticenumber;
                inputmodel.Title = title;
                inputmodel.MemberId = MemberId;
                //end--------------------------------
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    inputmodel.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    inputmodel.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }



                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    model.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        inputmodel.DepartmentId = item.AssociatedDepts;
                        inputmodel.IsPermissions = item.IsPermissions; ;
                    }

                }
                else
                {
                    inputmodel.DepartmentId = CurrentSession.DeptID;
                }

                /*End*/

               

                var result = (tPaperLaidV)Helper.ExecuteService("Notice", "GetCurrentInboxDraftNoticesList", inputmodel);

                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.ListtPaperLaidV.AsQueryable();

                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());

                response.Records.AddRange(from noticeList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(noticeList.NoticeID), new tPaperLaidV(noticeList)));

                return new JqGridJsonResult() { Data = response };

            }

            else
            { return RedirectToAction("LoginUP", "Account"); }
          
        }
        
        public ActionResult GetNoticeReplyDraftList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();                
                return PartialView("_GetNoticeReplyDraftList", obj);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNoticeReplyDraftGridList(JqGridRequest request)
        {

            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV inputmodel = new tPaperLaidV();

                inputmodel.PAGE_SIZE = request.RecordsCount;
                inputmodel.PageIndex = request.PageIndex +1;
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    inputmodel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    inputmodel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    inputmodel.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", inputmodel);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        inputmodel.DepartmentId = item.AssociatedDepts;
                        inputmodel.IsPermissions = item.IsPermissions; ;
                    }

                }
                else
                {
                    inputmodel.DepartmentId = CurrentSession.DeptID;
                }

                /*End*/

               

                var result = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeReplyDraftList", inputmodel);

                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.ListtPaperLaidV.AsQueryable();

                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());

                response.Records.AddRange(from noticeList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(noticeList.PaperLaidId), new tPaperLaidV(noticeList)));

                return new JqGridJsonResult() { Data = response };

            }

            else
            { return RedirectToAction("LoginUP", "Account"); }


        }
        //added code venkat for dynamic 
        public ActionResult GetNoticeReplySentList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd, string Noticenumber, string title, int? MemberId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                //added code venkat for dynamic 
                obj.NoticeNumber = Noticenumber;
                obj.Title = title;
                obj.MemberId = MemberId;
                //end----------------------------------------------
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                //obj.SessionId = siteSettingMod.SessionCode;
                //obj.AssemblyId = siteSettingMod.AssemblyCode;
                //obj.loopEnd = loopEnd;
                //obj.loopStart = loopStart;
                //obj.RowsPerPage = RowsPerPage;
                //obj.PageNumber = PageNumber;
                //obj.PageIndex = PageNumber;
                //obj.PAGE_SIZE = RowsPerPage;
                //obj.selectedPage = PageNumber;
                //obj.DepartmentId = CurrentSession.DeptID;
                //obj = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeReplySentList", obj);
                //obj.ResultCount = obj.ListtPaperLaidV.Count;
                return PartialView("_GetNoticeReplySentList", obj);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }
        //added code venkat for dynamic 
        public ActionResult GetNoticeReplySentGridList(JqGridRequest request, string Noticenumber, string title, int? MemberId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                //added code venkat for dynamic 
                obj.NoticeNumber = Noticenumber;
                obj.Title = title;
                obj.MemberId = MemberId;
                //end--------------------------------
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }              
               
                obj.PAGE_SIZE = request.RecordsCount;
                obj.PageIndex = request.PageIndex+1;


                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    obj.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        obj.DepartmentId = item.AssociatedDepts;
                        obj.IsPermissions = item.IsPermissions; ;
                    }

                }
                else
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }

                /*End*/

                var result = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeReplySentList", obj);              

                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.ListtPaperLaidV.AsQueryable();

                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());

                response.Records.AddRange(from noticeList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(noticeList.PaperLaidId), new tPaperLaidV(noticeList)));

                return new JqGridJsonResult() { Data = response };


            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        public ActionResult GetNoticeULOBList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                //obj.SessionId = siteSettingMod.SessionCode;
                //obj.AssemblyId = siteSettingMod.AssemblyCode;
                //obj.loopEnd = loopEnd;
                //obj.loopStart = loopStart;
                //obj.RowsPerPage = RowsPerPage;
                //obj.PageNumber = PageNumber;
                //obj.PageIndex = PageNumber;
                //obj.PAGE_SIZE = RowsPerPage;
                //obj.selectedPage = PageNumber;
                //obj.DepartmentId = CurrentSession.DeptID;
                //obj = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeULOBList", obj);
                //obj.ResultCount = obj.ListtPaperLaidV.Count;
                return PartialView("_GetNoticeULOBList", obj);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        public ActionResult GetNoticeULOBGridList(JqGridRequest request)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }     
                obj.PAGE_SIZE = request.RecordsCount;
                obj.PageIndex = request.PageIndex + 1;

                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    obj.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        obj.DepartmentId = item.AssociatedDepts;
                        obj.IsPermissions = item.IsPermissions; ;
                    }
                }
                else
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
                /*End*/

                obj = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeULOBList", obj);

                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)obj.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = obj.ListtPaperLaidV.Count
                };

                var resultToSort = obj.ListtPaperLaidV.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from noticeList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(noticeList.PaperLaidId), new tPaperLaidV(noticeList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        public ActionResult GetNoticeLIHList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                //obj.SessionId = siteSettingMod.SessionCode;
                //obj.AssemblyId = siteSettingMod.AssemblyCode;
                //obj.loopEnd = loopEnd;
                //obj.loopStart = loopStart;
                //obj.RowsPerPage = RowsPerPage;
                //obj.PageNumber = PageNumber;
                //obj.PageIndex = PageNumber;
                //obj.PAGE_SIZE = RowsPerPage;
                //obj.selectedPage = PageNumber;
                //obj.DepartmentId = CurrentSession.DeptID;
                //obj = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeLIHList", obj);
                //obj.ResultCount = obj.ListtPaperLaidV.Count;
                return PartialView("_GetNoticeLIHList", obj);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        public ActionResult GetNoticeLIHGridList(JqGridRequest request)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }     
                obj.PAGE_SIZE = request.RecordsCount;
                obj.PageIndex = request.PageIndex;

                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    obj.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        obj.DepartmentId = item.AssociatedDepts;
                        obj.IsPermissions = item.IsPermissions; ;
                    }
                }
                else
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
                /*End*/

                obj = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeLIHList", obj);
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)obj.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = obj.ResultCount
                };

                var resultToSort = obj.ListtPaperLaidV.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from noticeList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(noticeList.PaperLaidId), new tPaperLaidV(noticeList)));
                return new JqGridJsonResult() { Data = response };               
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        public ActionResult GetNoticePLIHList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                //obj.SessionId = siteSettingMod.SessionCode;
                //obj.AssemblyId = siteSettingMod.AssemblyCode;
                //obj.loopEnd = loopEnd;
                //obj.loopStart = loopStart;
                //obj.RowsPerPage = RowsPerPage;
                //obj.PageNumber = PageNumber;
                //obj.PageIndex = PageNumber;
                //obj.PAGE_SIZE = RowsPerPage;
                //obj.selectedPage = PageNumber;
                //obj.DepartmentId = CurrentSession.DeptID;
                //obj = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticePLIHList", obj);
                //obj.ResultCount = obj.ListtPaperLaidV.Count;
                return PartialView("_GetNoticePLIHList", obj);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        public ActionResult GetNoticePLIHGridList(JqGridRequest request)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }     
                obj.PAGE_SIZE = request.RecordsCount;
                obj.PageIndex = request.PageIndex+1;

                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    obj.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", obj);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        obj.DepartmentId = item.AssociatedDepts;
                        obj.IsPermissions = item.IsPermissions; ;
                    }
                }
                else
                {
                    obj.DepartmentId = CurrentSession.DeptID;
                }
                /*End*/

                obj = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticePLIHList", obj);

                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)obj.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = obj.ResultCount
                };

                var resultToSort = obj.ListtPaperLaidV.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<tPaperLaidV>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from noticeList in resultForGrid
                                          select new JqGridRecord<tPaperLaidV>(Convert.ToString(noticeList.PaperLaidId), new tPaperLaidV(noticeList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        public ActionResult GetNoticeSentForReplaceByID(string paperLaidId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV movementModel = new tPaperLaidV();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel.DepartmentId = CurrentSession.DeptID;
                movementModel = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeSubmittedPaperLaidByID", movementModel);
                return PartialView("_GetNoticeSentForReplaceByID", movementModel);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateNoticePaperLaidFile(HttpPostedFileBase file, HttpPostedFileBase Docfile, tPaperLaidV tPaper)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidTemp tPaperTemp = new tPaperLaidTemp();
                tPaper = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", tPaper);

                tPaperTemp.DocFileName = tPaper.DocFileName;
                tPaperTemp.FileName = tPaper.FileName;

                tPaperTemp.DocFileVersion = tPaper.DocFileVersion;

                if (file == null)
                {
                    string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                    string directory = Server.MapPath(url);
                    if (Directory.Exists(directory))
                    {
                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            string name = Path.GetFileName(page);
                            string nameKey = Path.GetFileNameWithoutExtension(page);
                            string directory1 = Path.GetDirectoryName(page);
                            //
                            // Display the Path strings we extracted.
                            //
                            Console.WriteLine("{0}, {1}, {2}, {3}",
                            page, name, nameKey, directory1);
                            tPaperTemp.FileName = name;
                        }
                        string ext = Path.GetExtension(tPaperTemp.FileName);
                        string fileNam1 = tPaperTemp.FileName.Replace(ext, "");


                        tPaper.Count = tPaper.Count;
                        //var vers = tPaper.FileVersion.GetValueOrDefault() + 1;

                        string actualFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + "N" + "_" + tPaper.PaperLaidId + "_V" + tPaper.Count + ext;




                        // string actualFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + QType + "_" + QuestionNo + "_V" + tPaper.Count + ext;



                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                        string Url = FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";
                        DirectoryInfo Dir = new DirectoryInfo(Url);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }

                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/", actualFileName);
                        string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                        System.IO.File.Copy(SourceFile, path, true);
                        tPaperTemp.FilePath = "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";

                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                        //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                        string FileStructurePath = Acess.SettingValue;
                        tPaperTemp.FileStructurePath = FileStructurePath;
                        tPaperTemp.FileName = actualFileName;

                        // tPaperTemp.Version = tPaper.Count;
                        tPaperTemp.PaperLaidId = tPaper.PaperLaidId;

                        tPaperTemp.Version = tPaper.Count;


                    }

                    tPaperTemp.Version = tPaper.Count;


                    //SiteSettings siteSettingMod = new SiteSettings();
                    //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    //if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                    //{
                    //    tPaper.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    //}

                    //if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                    //{
                    //    tPaper.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                    //}
                    //string Url = "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";
                    //DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                    //if (!Dir.Exists)
                    //{
                    //    Dir.Create();
                    //}
                    //var vers = tPaper.FileVersion.GetValueOrDefault() + 1;
                    //string ext = Path.GetExtension(file.FileName);
                    //string fileName = file.FileName.Replace(ext, "");
                    //string actualFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + "N" + "_" + tPaper.PaperLaidId + "_V" + vers + ext;
                    //file.SaveAs(Server.MapPath(Url + actualFileName));
                    //tPaperTemp.FileName = actualFileName;
                    //tPaperTemp.FilePath = Url;
                    //tPaperTemp.Version = tPaper.FileVersion + 1;
                    //tPaperTemp.PaperLaidId = tPaper.PaperLaidId;
                    ////tPaperTemp.DeptSubmittedDate = DateTime.Now;
                    ////tPaperTemp.DeptSubmittedBy = CurrentSession.UserName;

                    if (Docfile == null)
                    {

                        string urlDocFile = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                        string directoryDocFile = Server.MapPath(urlDocFile);
                        if (Directory.Exists(directoryDocFile))
                        {
                            string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID));
                            string SourceFile = savedFileName[0];
                            foreach (string page in savedFileName)
                            {
                                string name = Path.GetFileName(page);
                                string nameKey = Path.GetFileNameWithoutExtension(page);
                                string directory1 = Path.GetDirectoryName(page);
                                //
                                // Display the Path strings we extracted.
                                //
                                Console.WriteLine("{0}, {1}, {2}, {3}",
                                page, name, nameKey, directory1);
                                tPaperTemp.DocFileName = name;
                            }
                            string Docext = Path.GetExtension(tPaperTemp.DocFileName);
                            string fileNam1 = tPaperTemp.DocFileName.Replace(Docext, "");

                            //var Docvers = tPaper.DocFileVersion.GetValueOrDefault() + 1;

                            string DocFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + "N" + "_" + tPaper.PaperLaidId + "_V" + tPaper.MainDocCount + Docext;
                            //string actualDocFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + QType + "_" + QuestionNo + "_V" + model.MainDocCount + ext;


                            var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                            string Url = FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";
                            DirectoryInfo Dir = new DirectoryInfo(Url);
                            if (!Dir.Exists)
                            {
                                Dir.Create();
                            }

                            string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/", DocFileName);
                            //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                            System.IO.File.Copy(SourceFile, path, true);
                            //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                            tPaperTemp.FilePath = "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";

                            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                            string FileStructurePath = Acess.SettingValue;
                            tPaperTemp.FileStructurePath = FileStructurePath;
                            tPaperTemp.DocFileName = DocFileName;



                            tPaperTemp.PaperLaidId = tPaper.PaperLaidId;
                            tPaperTemp.DocFileVersion = tPaper.MainDocCount;
                            tPaperTemp.DocFileName = tPaperTemp.DocFileName;

                        }
                        else
                        {


                            tPaperTemp.DocFileVersion = tPaper.MainDocCount;
                        }

                        //var Docvers = tPaper.DocFileVersion.GetValueOrDefault() + 1;
                        //string Docext = Path.GetExtension(file.FileName);
                        //string DocfileName = file.FileName.Replace(ext, "");
                        //string DocFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + "N" + "_" + tPaper.PaperLaidId + "_V" + Docvers + Docext;
                        //file.SaveAs(Server.MapPath(Url + DocFileName));
                        //tPaperTemp.DocFileName = DocFileName;
                        //tPaperTemp.FilePath = Url;
                        //tPaperTemp.DocFileVersion = tPaper.DocFileVersion + 1;
                        //tPaperTemp.PaperLaidId = tPaper.PaperLaidId;
                        //tPaperTemp.DocFileName = tPaperTemp.DocFileName;
                        //tPaperTemp.DocFileVersion = tPaperTemp.DocFileVersion;
                    }
                }

                tPaperTemp.AssemblyId = CurrentSession.AssemblyId;
                tPaperTemp.SessionId = CurrentSession.SessionId;
                tPaperTemp = (tPaperLaidTemp)Helper.ExecuteService("Notice", "UpdateNoticePaperLaidFileByID", tPaperTemp);

                tPaper.DeptActivePaperId = tPaperTemp.PaperLaidTempId;
                tPaper.ModifiedBy = CurrentSession.UserName;
                tPaper.ModifiedWhen = DateTime.Now;
                tPaper = (tPaperLaidV)Helper.ExecuteService("Notice", "UpdatePaperLaidEntryForNoticeReplace", tPaper);

                if (Request.IsAjaxRequest())
                {
                    tPaperLaidV mod = new tPaperLaidV();

                    return Json(mod, JsonRequestBehavior.AllowGet);
                }
                else
                {


                }
                return null;
                //TempData["lSM"] = "NTC";
                //TempData["Msg"] = "File";
                //return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment");
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        public ActionResult ShowNoticeInboxById(int noticeId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tMemberNotice data = new tMemberNotice();
                data.NoticeId = noticeId;
                data.DepartmentId = CurrentSession.DeptID;
               
                data = (tMemberNotice)Helper.ExecuteService("Notice", "GetNoticeByNoticeID", data);
                data.SubmittedDateString = Convert.ToDateTime(data.SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                return PartialView("_ShowNoticeInboxById", data);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        public ActionResult ShowNoticeDraftReplyById(int papperId, int NoticeID)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV mdl = new tPaperLaidV();
                mdl.PaperLaidId = papperId;
                mdl.NoticeID = NoticeID;
                mdl = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeDraftReplyById", mdl);

                mEvent mEventModel = new mEvent();
                mdl.DepartmentId = mdl.DepartmentId;
                tPaperLaidV ministerDetails = new tPaperLaidV();
                ministerDetails.DepartmentId = mdl.DepartmentId;
                
                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                mdl.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", mdl);

                mdl.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("BillPaperLaid", "GetDocumentType", mdl);
                List<mEvent> events = (List<mEvent>)Helper.ExecuteService("Events", "GetAllNoticeEventsDetails", mEventModel);
                mdl.mEvents = events;
                mdl.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);

                ICollection<int> yearList = new List<int>();
                int currentYear = DateTime.Now.Year;
                for (int i = currentYear - 5; i < currentYear; i++)
                {
                    yearList.Add(i);
                }
                for (int i = currentYear; i < currentYear + 5; i++)
                {
                    yearList.Add(i);
                }
                //mdl.yearList = yearList;


                var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                string FileStructurePath = Acess.SettingValue;
                mdl.FileStructurePath = FileStructurePath;




                return PartialView("_CreateNewNoticeReply", mdl);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        public ActionResult ShowNoticeSentRecordById(int paperLaidId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV mdl = new tPaperLaidV();
                mdl.PaperLaidId = Convert.ToInt64(paperLaidId);
                mdl.DepartmentId = CurrentSession.DeptID;
                mdl = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeReplySentRecordById", mdl);
                return PartialView("_ShowNoticeSentRecordById", mdl);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }

        //New Changes related to notice paper discussed on 13th June 2014
        //added code venkat for dynamic 
        public ActionResult GetNoticeReplyDraftSendList(int PageNumber, int RowsPerPage, int loopStart, int loopEnd, string Noticenumber, string title, int? MemberId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV obj = new tPaperLaidV();
                //added code venkat for dynamic 
                obj.NoticeNumber = Noticenumber;
                obj.Title = title;
                obj.MemberId = MemberId ?? 0;
                //end------------------------------
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                //obj.SessionCode = siteSettingMod.SessionCode;
                //obj.AssemblyCode = siteSettingMod.AssemblyCode;
                obj.DSCApplicable = siteSettingMod.DSCApplicable;
                return PartialView("_GetNoticeReplyDraftSendList", obj);
            }
            else
            { return RedirectToAction("LoginUP", "Account"); }
        }
        //added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNoticeReplyDraftSendGridList(JqGridRequest request, string Noticenumber, string title, int? MemberId)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;
            model.PageIndex = request.PageIndex + 1;
            //added code venkat for dynamic 
            model.NoticeNumber = Noticenumber;
            model.Title = title;
            model.MemberId = MemberId;
            //end------------------------------------
            //Get Current Session and Assembly Information
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
           

            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }
            }
            else
            {
                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }               
            }
            /*End*/

           // model.DepartmentId = CurrentSession.DeptID;
            //GetNoticeReplyDraftSignList

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "SendingPendingForNoticeByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tNoticeSendModel.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<NoticePaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<NoticePaperSendModelCustom>(Convert.ToString(questionList.PaperLaidId), new NoticePaperSendModelCustom(questionList)));
            return new JqGridJsonResult() { Data = response };
        }



        public JsonResult PaperLaidDepartmentChange(tPaperLaidV obj, string Department, int Ministry, string OldDepartment, int OldMinistryId, int NoticeID, string NoticeNumber, int noticeTypeId)
        {
            tPaperLaidV mdl = new tPaperLaidV();
            tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
            tMemberNotice tNotice = new tMemberNotice();

            obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            tNotice.DepartmentId = Department;
            tNotice.NoticeId = NoticeID;
            tNotice.NoticeNumber = NoticeNumber;
            tNotice.OldDepartmentId = OldDepartment;
            tNotice.MinistryId = Ministry;
            tNotice.OldMinistryId = OldMinistryId;
            tNotice = (tMemberNotice)Helper.ExecuteService("PaperLaid", "UpdateNoticeDepartmentMinistryId", tNotice);

            mchangeDepartmentAuditTrail ChangedDepDetails = new mchangeDepartmentAuditTrail();
            ChangedDepDetails.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            ChangedDepDetails.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            ChangedDepDetails.DepartmentID = Department;
            ChangedDepDetails.ChangedDepartmentID = OldDepartment;
            ChangedDepDetails.NoticeNumber = NoticeNumber;
            ChangedDepDetails.NoticeTypeId = noticeTypeId ;
            ChangedDepDetails.ChangedBy = !string.IsNullOrEmpty(CurrentSession.UserName) ? CurrentSession.UserName : null;
            ChangedDepDetails.ChangedDate = DateTime.Now;
            mchangeDepartmentAuditTrail ChangeDept = new mchangeDepartmentAuditTrail();
            ChangeDept = (mchangeDepartmentAuditTrail)Helper.ExecuteService("PaperLaid", "SubmittChangeDeptEntry", ChangedDepDetails);

            return Json(mdl, JsonRequestBehavior.AllowGet);

        }


        //[HttpPost, ValidateAntiForgeryToken]
        //public ActionResult PaperLaidDepartmentChange(HttpPostedFileBase file, tPaperLaidV obj, string submitButton)
        //{
        //    if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

        //    if (CurrentSession.UserID != null)
        //    {
        //        obj.ModifiedBy = !string.IsNullOrEmpty(CurrentSession.UserName) ? CurrentSession.UserName : null;
        //        obj.ModifiedWhen = DateTime.Now;
            
        //        submitButton = submitButton.Replace(" ", "");
        //        tPaperLaidV mdl = new tPaperLaidV();
        //        tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
        //        switch (submitButton)
        //        {
        //            case "Save":
        //                {
        //                    obj.AssemblyId = obj.AssemblyId;
        //                    obj.SessionId = obj.SessionId;
        //                    int questionId = obj.QuestionID;
        //                    int noticeId = obj.NoticeID;
                          
        //                    string DepartmentId = obj.DepartmentId;
        //                    int MinistryId = obj.MinistryId;

        //                    int? ChangedMinistryId =  obj.ChangedMinisteryId;
                         
        //                    tPaperLaidV billDetails = new tPaperLaidV();
        //                    string NoticeNumber = obj.NoticeNumber;
        //                    int NoticeTypeId = obj.noticeTypeId;

        //                    billDetails = obj;

        //                    mdl.EventId = obj.EventId;
        //                    string DepartId = obj.DepartmentId;
        //                    string ChangedDeptID = obj.ChangedDepartmentId;                            

        //                    //mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetFileSaveDetails", mdl);
        //                    string categoryType = mdl.RespectivePaper;

        //                    tMemberNotice tNotice = new tMemberNotice();
        //                    tNotice.DepartmentId = ChangedDeptID;
        //                    tNotice.OldDepartmentId = DepartId;
        //                    tNotice.NoticeId = noticeId;
        //                    tNotice.MinistryId = ChangedMinistryId;
        //                    tNotice.OldMinistryId = MinistryId;

        //                    tNotice = (tMemberNotice)Helper.ExecuteService("PaperLaid", "UpdateNoticeDepartmentMinistryId", tNotice);
        //                    //mDepartments = (tQuestion)Helper.ExecuteService("PaperLaid", "UpdateDepartmentId", obj);
        //                    mchangeDepartmentAuditTrail ChangedDepDetails = new mchangeDepartmentAuditTrail();

        //                    ChangedDepDetails.AssemblyID = tNotice.AssemblyID;
        //                    ChangedDepDetails.SessionID = tNotice.SessionID;

        //                    ChangedDepDetails.DepartmentID = obj.DepartmentId;
        //                    ChangedDepDetails.ChangedDepartmentID = tNotice.DepartmentId;

        //                    ChangedDepDetails.NoticeNumber = tNotice.NoticeNumber;
        //                    ChangedDepDetails.NoticeTypeId = tNotice.NoticeTypeID;

        //                    ChangedDepDetails.ChangedBy = !string.IsNullOrEmpty(CurrentSession.UserName) ? CurrentSession.UserName : null;
        //                    ChangedDepDetails.ChangedDate = DateTime.Now;
        //                    mchangeDepartmentAuditTrail ChangeDept = new mchangeDepartmentAuditTrail();
        //                    ChangeDept = (mchangeDepartmentAuditTrail)Helper.ExecuteService("PaperLaid", "SubmittChangeDeptEntry", ChangedDepDetails);

        //                    TempData["Msg"] = "Save";
        //                    //Modified according to new requirement of sending paper, 13 June 2014 Changes
        //                    break;
        //                }
       
        //            case "Send":
        //                {
        //                    tPaperLaidTemp model = new tPaperLaidTemp();
        //                    tPaperLaidV mod = new tPaperLaidV();
        //                    model.PaperLaidId = Convert.ToInt32(obj.PaperLaidId);
        //                    model.DeptSubmittedBy = CurrentSession.UserName;
        //                    model.DeptSubmittedDate = DateTime.Now;
        //                    model = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "SubmitPendingDepartmentPaperLaid", model);
        //                    mod = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "UpdateDeptActiveId", model);

        //                    break;
        //                }
        //        }
        //        if (Request.IsAjaxRequest())
        //        {
        //            var data = submitButton;
        //            var result = Json(data, JsonRequestBehavior.AllowGet);
        //            return result;
        //        }
        //        else
        //        {
        //            TempData["Msg"] = "Save";
        //            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        //           // return RedirectToAction("DepartmentDashboard");
        //        }
        //    }

        //    TempData["Msg"] = "Save";
        //    return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        //}

        public ActionResult GetChangePaper(string NoticeNumber, string NoticeId, string NoticeTypeID)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (CurrentSession.UserID != null)
            {
                tPaperLaidV paperLaidModel = new tPaperLaidV();
                mDepartment departmentModel = new mDepartment();
                mEvent mEventModel = new mEvent();

                tPaperLaidV ministerDetails = new tPaperLaidV();

                tMemberNotice memnotice = new tMemberNotice();

                var NoticeID = int.Parse(NoticeId);
                var NoticeTypeId = int.Parse(NoticeTypeID);
                // paperLaidModel.DepartmentId = CurrentSession.DeptID;

                if (NoticeId != "")
                {
                    ministerDetails.NoticeID = int.Parse(NoticeId);
                    string dptId = (string)Helper.ExecuteService("PaperLaid", "getDepartmentIdByNoticeId", ministerDetails);

                    int MinistryId = (int)Helper.ExecuteService("PaperLaid", "getMinistryIdByNoticeId", ministerDetails);

                    paperLaidModel.MinistryId = MinistryId;

                    if (dptId != "")
                    {
                        paperLaidModel.DepartmentId = dptId;
                        paperLaidModel.NoticeID = NoticeID;
                    }
                    else
                    {
                        paperLaidModel.DepartmentId = CurrentSession.DeptID;
                    }
                }

                ministerDetails.DepartmentId = paperLaidModel.DepartmentId;
                ministerDetails.MinistryId = paperLaidModel.MinistryId;
                mSession mSessionModel = new mSession();
                mAssembly mAssemblymodel = new mAssembly();
                paperLaidModel = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", paperLaidModel);

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    paperLaidModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    paperLaidModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                //paperLaidModel.SessionId = Convert.ToInt32(paperLaidModel.SessionCode);
                //paperLaidModel.AssemblyId = Convert.ToInt32(paperLaidModel.AssemblyCode);
                paperLaidModel.NoticeNumber = NoticeNumber;
                paperLaidModel.noticeTypeId = NoticeTypeId;

                //mMinisteryMinisterModel mmModel = new mMinisteryMinisterModel();
                //mmModel.MinistryID = 0;
                //mmModel.MinisterMinistryName = "Select Minister";
                //paperLaidModel.mMinisteryMinister = Helper.ExecuteService("MinistryMinister", "GetMinisterMinistry", null) as List<mMinisteryMinisterModel>;
                //paperLaidModel.mMinisteryMinister.Add(mmModel);

                ////paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);
                List<tMemberNotice> ListtMemberNotice = new List<tMemberNotice>();
                tMemberNotice noticeModel = new tMemberNotice();
                noticeModel.DepartmentId = "0";
                noticeModel.DepartmentName = "Select Department";
                ListtMemberNotice.Add(noticeModel);
                paperLaidModel.DepartmentLt = ListtMemberNotice;


                mMinisteryMinisterModel mmModel = new mMinisteryMinisterModel();
                mmModel.MinistryID = 0;
                mmModel.MinisterMinistryName = "Select Minister";
                paperLaidModel.mMinisteryMinister = Helper.ExecuteService("MinistryMinister", "GetMinisterMinistry", null) as List<mMinisteryMinisterModel>;
                paperLaidModel.mMinisteryMinister.Add(mmModel);


                //paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetails", ministerDetails);
                paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);
                paperLaidModel.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetChangeDepartment", departmentModel);
                return PartialView("_GetChangeDepartment", paperLaidModel);
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/Index");
        }

        //[HttpGet]
        //public JsonResult GetMinisterByDepartment(int MinistryId)
        //{
        //    tMemberNotice noticeModel = new tMemberNotice();

        //    mMinistry model = new mMinistry();
        //    model.MinistryID = MinistryId;
        //    List<tMemberNotice> deptartmentList = new List<tMemberNotice>();

        //    tMemberNotice model1 = new tMemberNotice();
        //    model1.DepartmentId = "";
        //    model1.DepartmentName = "Select Department";
        //    deptartmentList.Add(model1);

        //    var objDeptList = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model) as List<tMemberNotice>;
        //    if (objDeptList != null)
        //    {
        //        deptartmentList.AddRange(objDeptList);
        //    }

        //    return Json(deptartmentList, JsonRequestBehavior.AllowGet);
        //}

        //[HttpGet]
        //public JsonResult GetMinisterByDepartment(string deptId)
        //{
        //    tMemberNotice noticeModel = new tMemberNotice();
        //    mDepartment model = new mDepartment();

        //    model.deptId = deptId;

        //    noticeModel.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetMinisterByDepartment", model) as List<tMemberNotice>;


        //    foreach (var item in noticeModel.DepartmentLt)
        //    {
        //        noticeModel.MinisterId = item.MinisterId;
        //        noticeModel.MinisterName = item.MinisterName;
        //    }


        //    //noticeModel.MinisterId = Convert.ToInt32(0);
        //    //noticeModel.MinisterName = "Select Minister";
        //    // DepartmentLt.Add(noticeModel);
        //    return Json(noticeModel.DepartmentLt, JsonRequestBehavior.AllowGet);
        //}

        [HttpGet]
        public JsonResult GetMinisterByDepartment(string deptId)
        {
            tMemberNotice noticeModel = new tMemberNotice();

            mDepartment model = new mDepartment();
            model.deptId = deptId;
            List<tMemberNotice> deptartmentList = new List<tMemberNotice>();

            tMemberNotice model1 = new tMemberNotice();
            //model1.MinisterId = 0;
            //model1.MinisterName = "Select Minister";
           // deptartmentList.Add(model1);

            var objDeptList = Helper.ExecuteService("MinistryMinister", "GetMinisterByDepartment", model) as List<tMemberNotice>;
            if (objDeptList != null)
            {
                deptartmentList.AddRange(objDeptList);
            }
            SelectList objSession = new SelectList(deptartmentList, "MinisterId", "MinisterName", 0);
            return Json(deptartmentList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StarredQuesHandWrittenSignDocument(string SIds)
        {
            HandwrittenSignatureModel model = new HandwrittenSignatureModel();
            model.PagesToApplySignatureList = StaticControlBinder.GetPagesToApplySignature();
            model.PositionToApplySignatureList = StaticControlBinder.GetPositionToApplySignature();
            return View("_NoticeHandWrittenSignDocument", model);
        }

        public ActionResult SubmitHandWrittenSignatureSelectedPaperWithoutSign(string Ids, string lSMCode, int SignatureType, int PagesToApplySignatureID, int PositionToApplySignatureID)
        {
            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            tPaperLaidTemp pT = new tPaperLaidTemp();
           
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

            if (lSMCode != null && lSMCode != "")
            {
                if (lSMCode == "SQ")
                {
                    TempData["lSM"] = "SQ";
                }
                else if (lSMCode == "USQ")
                {
                    TempData["lSM"] = "USQ";
                }
                else if (lSMCode == "NTC")
                {
                    TempData["lSM"] = "NTC";
                }
                else if (lSMCode == "BLL")
                {
                    TempData["lSM"] = "BLL";
                }
                else if (lSMCode == "OPT")
                {
                    TempData["lSM"] = "OPT";
                }
            }

            if (Ids != null && Ids != "")
            {

                PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

                string path = "";
                string Files = "";
                string Prifix = "";
                string OldFileName = "";
                foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
                {
                    path = item.FilePath + item.FileName;
                    OldFileName = item.FileName;
                    path = path.Replace(" ", "%20");
                    var test = Request.Url.AbsoluteUri;
                    string[] abc = test.Split('/');
                    Prifix = abc[0] + "//" + abc[2];
                    if (Files == "")
                    {
                        Files = Prifix + path;
                    }
                    else
                    {
                        Files = Files + "," + Prifix + path;
                    }

                    string AssemblyId = "";
                    string SessionId = "";

                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                    SessionId = CurrentSession.AssemblyId; //Convert.ToInt32(siteSettingMod.SessionCode);
                    AssemblyId = CurrentSession.SessionId ;// Convert.ToInt32(siteSettingMod.AssemblyCode);

                    pT.FilePath = Ids;
                    pT.MinisterSubmittedDate = DateTime.Now;
                    pT.DeptSubmittedDate = DateTime.Now;
                    pT.DeptSubmittedBy = CurrentSession.UserName;
                    pT.SessionId = SessionId;
                    pT.AssemblyId = AssemblyId;
                    //pT.FilePath = Ids;
                    //pT.MinisterSubmittedDate = DateTime.Now;
                    //pT.MinisterSubmittedBy = Convert.ToInt32(CurrentSession.UserName);
                    string imageFileLoc = Server.MapPath(CurrentSession.SignaturePath);

                    string[] Afile = OldFileName.Split('.');
                    OldFileName = Afile[0];
                    string newFileLocation = "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/" + OldFileName + "_signed.pdf";
                    // string newFileLocation = item.FilePath + "HWSigned/" + Path.GetFileNameWithoutExtension(path) + "_HWSigned" + Path.GetExtension(path);

                    string textLine1 = "P.Mitra";
                    string textLine2 = "Principle Secretory";

                    PDFExtensions.InsertImageToPdf(Server.MapPath(path), imageFileLoc, Server.MapPath(newFileLocation), PositionToApplySignatureID, PagesToApplySignatureID, textLine1, textLine2);
                }

                //string Newpath = System.IO.Path.Combine(Server.MapPath("~/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/"), OldFileName);

                //int indexof = path.LastIndexOf("\\");

                //string directory = path.Substring(0, indexof);
                ////  string directory = Server.MapPath(path);
                //if (!System.IO.Directory.Exists(directory))
                //{
                //    System.IO.Directory.CreateDirectory(directory);
                //}


                //using (Stream FilesStream = new FileStream(Server.MapPath(path), FileMode.Open))
                //using (Stream NewpathStream = new FileStream(Server.MapPath(Newpath), FileMode.Create, FileAccess.ReadWrite))


                //System.IO.File.Copy(Server.MapPath(path), Server.MapPath(Newpath));

                mMinisteryMinisterModel.FilePath = Files;

                var modelReturned = Helper.ExecuteService("PaperLaid", "InsertSignPathAttachment", pT) as tPaperLaidTemp;
                var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateDepartmentMinisterActivePaperId", Ids) as tPaperLaidTemp;

                //var modelReturned = Helper.ExecuteService("PaperLaidMinister", "InsertSignPathAttachment", pT) as tPaperLaidTemp;
                //var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateMinisterActivePaperId", Ids) as tPaperLaidTemp;

            }
            tPaperLaidV mdl = new tPaperLaidV();
            if (Convert.ToString(TempData["Msg"]) == "Sign")
            {
                mdl.CustomMessage = "Paper Signed Successfully !!";
            }
           // TempData["Msg"] = "Sign";            
            return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment", message = "Sign completed" });
        }



        public ActionResult OTPRegistarionReplyNotice(string NoticeNumber, int NoticeId, string NoticeTypeID, string Subject)
        {
            mSession Mdl = new mSession();
            mAssembly assmblyMdl = new mAssembly();
            if (CurrentSession.AssemblyId != null && CurrentSession.AssemblyId != "")
            {
                Mdl.AssemblyID = int.Parse(CurrentSession.AssemblyId);
                assmblyMdl.AssemblyID = int.Parse(CurrentSession.AssemblyId);
            }
            if (CurrentSession.SessionId != null && CurrentSession.SessionId != "")
            {
                Mdl.SessionCode = int.Parse(CurrentSession.SessionId);
            }
            tUserRegistrationDetails obj = new tUserRegistrationDetails();
            obj.DiaryNumber = NoticeNumber;
            obj.Subject = Subject;
            obj.RuleId =  NoticeId;
            obj.DocumentTypeId = 3;
            obj.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
            obj.AssemblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
            obj = (tUserRegistrationDetails)Helper.ExecuteService("PaperLaid", "OTPRegistarionDetailsList", obj);
          
            foreach (var item in obj.UserRegistrationDetailsModel)
            {
                obj.IsLocked = item.IsLocked;
            }

            return PartialView("_OTPRegistarionReplyNotice", obj);
        }



        [HttpPost]
        public ContentResult UploadNoticeFiles()
        {

            string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPaperLaidV>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPaperLaidV()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult RemoveMainFiles()
        {
            string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ContentResult UploadNoticeDocFiles()
        {

            string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPaperLaidV>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPaperLaidV()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult RemoveMainFilesDoc()
        {
            string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetNoticeCount(int AssemblyId, int SessionId)
        {

            tPaperLaidV mod = new tPaperLaidV();
            mod.AssemblyId = AssemblyId;
            mod.SessionId = SessionId;

            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                mod.UserID = new Guid(CurrentSession.UserID);

            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", mod);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    mod.DepartmentId = item.AssociatedDepts;
                    mod.IsPermissions = item.IsPermissions; ;
                }

            }
            else
            {
                if (mod.DepartmentId == null)
                {
                    mod.DepartmentId = CurrentSession.DeptID;
                }

            }

            // var result = (int)Helper.ExecuteService("PaperLaid", "DraftRepliescount", mod);
            mod = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "NoticeDraftRepliescount", mod);
            //mod.TotalStaredPendingForSubmissionNew = result;

            //   Newmodel = (PrintingPressModel)Helper.ExecuteService("PrintingPress", "GetCountForPrinting", Newmodel);

            return Json(mod, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public JsonResult GetDepartmentByMinistery(int MinistryId)
        {
            tMemberNotice noticeModel = new tMemberNotice();
            mMinistry model = new mMinistry();
            model.MinistryID = MinistryId;
            noticeModel.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model) as List<tMemberNotice>;
            //    List<tMemberNotice> DepartmentList = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model) as List<tMemberNotice>;
            noticeModel.DepartmentId = Convert.ToString(0);
            noticeModel.DepartmentName = "Select Department";
            // DepartmentLt.Add(noticeModel);
            return Json(noticeModel.DepartmentLt, JsonRequestBehavior.AllowGet);
        }



        public ActionResult UpdateAcknowledgementNotice(tPaperLaidV obj, int NoticeId)
        {
            tPaperLaidV mdl = new tPaperLaidV();
            tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
            tMemberNotice tQuestions = new tMemberNotice();


            obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            tQuestions.NoticeId = NoticeId;
            tQuestions.IsAcknowledgmentDate = DateTime.Now;
            tQuestions.AadharId = CurrentSession.AadharId;
            tQuestions = (tMemberNotice)Helper.ExecuteService("PaperLaid", "UpdateIsAcknowledgmentDateByNoticeId", tQuestions);


            tQuestions = (tMemberNotice)Helper.ExecuteService("Notice", "GetNoticeByNoticeID", tQuestions);
            return PartialView("_ShowNoticeInboxById", tQuestions);




        }
              
        public ActionResult HistoryPop(string NoticeNumber,string BussinessType,string OldNoticeTypeID,string TypeChangeDate)
        {
            tPaperLaidV paperLaidModel = new tPaperLaidV();
            paperLaidModel.NoticeNumber = NoticeNumber;
            paperLaidModel.BussinessType = BussinessType;
            paperLaidModel.MergeDiaryNo = OldNoticeTypeID;
            paperLaidModel.SubmittedDateString = TypeChangeDate;
            return PartialView("_HistoryPopUp", paperLaidModel);

        }

    }
}
