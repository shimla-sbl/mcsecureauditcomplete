﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.PaperLaid;
using SBL.eLegistrator.HouseController.Web.Filters;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Event;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.SiteSetting;
using Lib.Web.Mvc.JQuery.JqGrid;
using System.Linq;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Filters;
using SBL.DomainModel.ComplexModel;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Controllers
{
    [SBLAuthorize(Allow = "Authenticated")]
    [Audit]
    [NoCache]
    public class OtherPaperLaidController : Controller
    {
        //
        // GET: /PaperLaidDepartment/OtherPaperLaid/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DepartmentDashboard()
        {
            tPaperLaidV model = new tPaperLaidV();
            model.DepartmentId = CurrentSession.DeptID;
            model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetCountForOtherPaperLaidTypes", model);
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            return View(model);
        }

        #region Other PaperLaid
        [HttpPost]
        public ActionResult GetOtherPaperLaidAllList(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            if (CurrentSession.UserID != null)
            {
                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

                #region Paper Category Type Information


                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    tPaperLaidDepartmentModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    tPaperLaidDepartmentModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }


                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mEvent> PaperCategoryTypeList = Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidPaperCategoryType", PaperCategoryTypeListMod) as List<mEvent>;
                tPaperLaidDepartmentModel.mEvents = PaperCategoryTypeList;

                #endregion

                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);

                //var AllOtherPaperList = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidAllList", objPaperModel);
                //tPaperLaidDepartmentModel.DepartmentPendingList = AllOtherPaperList.DepartmentSubmitList;
                //if (tPaperLaidDepartmentModel.DepartmentPendingList.Count > 0)
                //{
                //    objPaperModel.ResultCount = AllOtherPaperList.ResultCount;
                //}
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("20");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetAllOtherPaperLaidList", tPaperLaidDepartmentModel);
            }
            TempData["lSM"] = "OPT";
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }
        //added code venkat for dynamic
        [HttpPost]
        public ActionResult GetOtherPaperLaidPendings(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, int? QNumber, int? Bname)//string ResultCount,
        {
            if (CurrentSession.UserID != null)
            {
                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();



                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    tPaperLaidDepartmentModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    // PaperCategoryTypeListMod.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    tPaperLaidDepartmentModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                    //PaperCategoryTypeListMod.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }
                List<mEvent> PaperCategoryTypeList = Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidPaperCategoryType", PaperCategoryTypeListMod) as List<mEvent>;
                tPaperLaidDepartmentModel.mEvents = PaperCategoryTypeList;

                #endregion
                //added code venkat for dynamic 
                tPaperLaidDepartmentModel.PaperTypeID = QNumber ?? 0;
                tPaperLaidDepartmentModel.EventId = Bname ?? 0;
                //end-------------------------------
                return PartialView("_GetPendingListOtherPaperLaid", tPaperLaidDepartmentModel);
            }
            TempData["lSM"] = "OPT";
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }

        public ActionResult FilterByBusinessType(int PaperCategoryTypeId, int CategoryTypeId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount, string view)
        {
            if (CurrentSession.UserID != null)
            {

                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mEvent> PaperCategoryTypeList = Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidPaperCategoryType", PaperCategoryTypeListMod) as List<mEvent>;
                tPaperLaidDepartmentModel.mEvents = PaperCategoryTypeList;

                #endregion

                tPaperLaidV objMovement = new tPaperLaidV();
                objMovement.PaperCategoryTypeId = PaperCategoryTypeId;
                objMovement.EventId = PaperCategoryTypeId;
                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                objPaperModel.ResultCount = int.Parse(ResultCount);
                objPaperModel.PaperCategoryTypeId = PaperCategoryTypeId;
                if (view != "Submit")
                {
                    tPaperLaidDepartmentModel.DepartmentPendingList = (ICollection<PaperMovementModel>)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidPendingsDetails", objPaperModel);
                }
                else
                {
                    tPaperLaidDepartmentModel.DepartmentPendingList = (ICollection<PaperMovementModel>)Helper.ExecuteService("OtherPaperLaid", "GetSubmittedOtherPaperLaidList", objPaperModel);

                }
                tPaperLaidDepartmentModel.EventId = objMovement.EventId;
                if (tPaperLaidDepartmentModel.DepartmentPendingList.Count > 0)
                {
                    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentPendingList.Count;
                }
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("20");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                if (view != "Submit")
                {
                    return PartialView("_GetPendingListOtherPaperLaid", tPaperLaidDepartmentModel);
                }
                else
                {
                    return PartialView("_GetPaperLaidSubmittList", tPaperLaidDepartmentModel);
                }
            }
            TempData["lSM"] = "OPT";
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }

        public ActionResult EditPendingRecordDashboard(string paperLaidId)
        {
            if (CurrentSession.UserID != "")
            {
                tPaperLaidV model = new tPaperLaidV();

                model.PaperLaidId = Convert.ToInt32(paperLaidId);
                model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "EditPendingOtherPaperLaidDetials", model);

                mDepartment departmentModel = new mDepartment();
                mEvent mEventModel = new mEvent();
                tPaperLaidV siteSetting = new tPaperLaidV();
                siteSetting = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", siteSetting);
                model.SessionId = siteSetting.SessionCode;
                model.AssemblyCode = siteSetting.AssemblyCode;
                model.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("OtherPaperLaid", "GetSessionDates", model);
                model.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("OtherPaperLaid", "GetDocumentType", model);

                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    model.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        model.DepartmentId = item.AssociatedDepts;
                        model.IsPermissions = item.IsPermissions;
                    }

                }
                else
                {
                    model.DepartmentId = CurrentSession.DeptID;
                    model.IsPermissions = true;

                }

                /*End*/

                mDepartment mdep = new mDepartment();
                mdep.deptId = model.DepartmentId;

                // model.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
                model.mDepartment = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);

                model.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
                //model.DepartmentId = CurrentSession.DeptID;
                //    model.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistry", mMinisteryMinisterModel);
                tPaperLaidV ministryDetails = new tPaperLaidV();
                ministryDetails.DepartmentId = model.DepartmentId;
                model.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryByDepartmentIds", ministryDetails);

                if (paperLaidId != "")
                {
                    model.PaperLaidId = int.Parse(paperLaidId);
                    string dptId = (string)Helper.ExecuteService("PaperLaid", "getDepartmentIdByPaperId", model);
                    if (dptId != "")
                    {
                        model.DepartmentId = dptId;
                    }
                    else
                    {
                        if (model.DepartmentId == null)
                        {
                            model.DepartmentId = CurrentSession.DeptID;
                        }

                    }
                }

                model.mEvents.Add(mEventModel);
                return PartialView("_GetPaperEntryForDashBoard", model);
            }
            TempData["lSM"] = "OPT";
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PaperLaidSubmittFromDashBoard(HttpPostedFileBase file,HttpPostedFileBase DocFile, tPaperLaidV obj, string submitButton)
        {
            if (CurrentSession.UserID != null)
            {
                // submitButton = submitButton.Replace(" ", "");
                tPaperLaidV mdl = new tPaperLaidV();
                tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
                submitButton = "Save";

                switch (submitButton)
                {
                    case "Save":
                        {
                            if (obj.PaperLaidId != 0)
                            {

                                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                                {
                                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                                    mdlTemp.AssemblyId = CurrentSession.AssemblyId;
                                }

                                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                                {
                                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                                    mdlTemp.SessionId = CurrentSession.SessionId;
                                }
                                mdl = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "UpdateOtherPaperLaidEntry", obj);
                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("OtherPaperLaid", "GetExisitngFileDetailsForOtherPaperLaid", mdl);



                                if (file == null)
                                {
                                    string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.FileName = name;
                                        }
                                        string ext = Path.GetExtension(mdlTemp.FileName);
                                        string fileNam1 = mdlTemp.FileName.Replace(ext, "");


                                        var vers = obj.FileVersion.GetValueOrDefault() + 1;


                                        string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "O" + "_" + mdl.PaperLaidId + "_V" + vers + ext;


                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);
                                        string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.FileName = actualFileName;
                                        mdlTemp.Version = vers;

                                    }
                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    mdlTemp.Version = mdl.Count;


                                }


                                if (DocFile == null)
                                {

                                    string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.DocFileName = name;
                                        }

                                        string ext = Path.GetExtension(mdlTemp.DocFileName);

                                        var DocVer = obj.DocFileVersion.GetValueOrDefault() + 1;


                                        string fileName = mdlTemp.DocFileName.Replace(ext, "");


                                        string actualDocFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "O" + "_" + mdl.PaperLaidId + "_V" + DocVer + ext;



                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualDocFileName);
                                        //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.DocFileName = actualDocFileName;
                                        mdlTemp.DocFileVersion = DocVer;
                                    }


                                    else
                                    {
                                        mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                        mdlTemp.DocFileVersion = obj.MainDocCount;
                                        mdlTemp.DocFileName = mdl.DocFileName;
                                    }

                                   

                                }

                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("OtherPaperLaid", "UpdateDetailsInTempOtherPaperLaid", mdlTemp);
                            }
                            else
                            {
                                SaveOtherPaperLaid(file,DocFile, obj);
                            }

                            break;
                        }
                }


            }

            if (Request.IsAjaxRequest())
            {
                tPaperLaidV mod = new tPaperLaidV();

                return Json(mod, JsonRequestBehavior.AllowGet);
            }
            else
            {


            }
            //return null;
            return RedirectToAction("GetOtherPaperLaidPendings", new { sessionId = 8, assemblyId = 12 });

        }

       
        public ActionResult GetOtherPaperEntry()
        {
            if (CurrentSession.UserID != null)
            {
                tPaperLaidV paperLaidModel = new tPaperLaidV();
                mDepartment departmentModel = new mDepartment();
                mEvent mEventModel = new mEvent();

                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    paperLaidModel.UserID = new Guid(CurrentSession.UserID);
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", paperLaidModel);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        paperLaidModel.DepartmentId = item.AssociatedDepts;
                        paperLaidModel.IsPermissions = item.IsPermissions; ;
                    }
                }
                else
                {
                    paperLaidModel.DepartmentId = CurrentSession.DeptID;
                    paperLaidModel.IsPermissions = true;
                }
                /*End*/

                mDepartment mdep = new mDepartment();
                mdep.deptId = paperLaidModel.DepartmentId;

                tPaperLaidV ministerDetails = new tPaperLaidV();
                ministerDetails.DepartmentId = paperLaidModel.DepartmentId;

                paperLaidModel.SessionId = int.Parse(CurrentSession.SessionId);
                paperLaidModel.AssemblyCode = int.Parse(CurrentSession.AssemblyId);
                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                paperLaidModel.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("OtherPaperLaid", "GetSessionDates", paperLaidModel);
                paperLaidModel.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("OtherPaperLaid", "GetDocumentType", paperLaidModel);
                paperLaidModel.mDepartment = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);
                // paperLaidModel.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
                paperLaidModel.mEvents = (ICollection<mEvent>)Helper.ExecuteService("OtherPaperLaid", "GetAllEventsByOtherPaperLaid", mEventModel);
                paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryByDepartmentIds", ministerDetails);
                //   paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("OtherPaperLaid", "GetMinisterMinistryName", paperLaidModel.DepartmentId);
                //   paperLaidModel.MinistryId = mMinisteryMinisterModel.MinistryID;
                return PartialView("_GetPaperEntryForDashBoard", paperLaidModel);
            }
            TempData["lSM"] = "OPT";
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }
        //added code venkat for dynamic 
        public ActionResult GetUpComingOtherPaperLaid(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, int? QNumber, int? Bname)//string ResultCount,
        {
            if (CurrentSession.UserID != null)
            {
                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                //ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();
                //added code venkat for dynamic 
                tPaperLaidDepartmentModel.QuestionNumber = QNumber ?? 0;
                tPaperLaidDepartmentModel.EventId = Bname ?? 0;
                //end-----------------------------------------
                //#region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mEvent> PaperCategoryTypeList = Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidPaperCategoryType", PaperCategoryTypeListMod) as List<mEvent>;
                tPaperLaidDepartmentModel.mEvents = PaperCategoryTypeList;

                //#endregion

                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                //objPaperModel.ResultCount = int.Parse(ResultCount);
                tPaperLaidV upComingLob = new tPaperLaidV();
                upComingLob.PageIndex = int.Parse(PageNumber);
                upComingLob.PAGE_SIZE = int.Parse(RowsPerPage);
                //upComingLob = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "UpcomingLOBByOtherpaperLaidId", upComingLob);
                //if (upComingLob != null)
                //{
                //    if (upComingLob.DepartmentSubmitList.Count > 0)
                //    {
                //        tPaperLaidDepartmentModel.DepartmentSubmitList = upComingLob.DepartmentSubmitList;
                //    }
                //}
                if (tPaperLaidDepartmentModel.DepartmentSubmitList.Count > 0)
                {
                    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentSubmitList.Count;
                }
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("20");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetUpComingPaperLaidList", tPaperLaidDepartmentModel);
            }
            TempData["lSM"] = "OPT";
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }
        //added code venkat for dynamic 
        public ActionResult OtherPaperLaidInHouse(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, int? QNumber, int? Bname)// string ResultCount,
        {
            if (CurrentSession.UserID != null)
            {
                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                //ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();
                //added code venkat for dynamic 
                tPaperLaidDepartmentModel.QuestionNumber = QNumber ?? 0;
                tPaperLaidDepartmentModel.EventId = Bname ?? 0;
                //end------------------------------------------
                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mEvent> PaperCategoryTypeList = Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidPaperCategoryType", PaperCategoryTypeListMod) as List<mEvent>;
                tPaperLaidDepartmentModel.mEvents = PaperCategoryTypeList;

                #endregion

                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                //objPaperModel.ResultCount = int.Parse(ResultCount);
                tPaperLaidV LaidInHouse = new tPaperLaidV();
                LaidInHouse.PageIndex = int.Parse(PageNumber);
                LaidInHouse.PAGE_SIZE = int.Parse(RowsPerPage);
                //LaidInHouse = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidInHouseByType", LaidInHouse);
                //if (LaidInHouse != null)
                //{
                //    if (LaidInHouse.DepartmentSubmitList.Count > 0)
                //    {
                //        tPaperLaidDepartmentModel.DepartmentSubmitList = LaidInHouse.DepartmentSubmitList;
                //    }
                //}
                if (tPaperLaidDepartmentModel.DepartmentSubmitList.Count > 0)
                {
                    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentSubmitList.Count;
                }
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);

                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("20");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetPaperLaidInHouseList", tPaperLaidDepartmentModel);
            }
            TempData["lSM"] = "OPT";
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }
        //added code venkat for dynamic 
        [HttpPost]
        public ActionResult OtherPaperPendingToLay(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, int? QNumber, int? Bname)// string ResultCount,
        {
            if (CurrentSession.UserID != null)
            {
                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                //ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();
                //added code venkat for dynamic 
                tPaperLaidDepartmentModel.QuestionNumber = QNumber ?? 0;
                tPaperLaidDepartmentModel.EventId = Bname ?? 0;
                //end----------------------------
                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mEvent> PaperCategoryTypeList = Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidPaperCategoryType", PaperCategoryTypeListMod) as List<mEvent>;
                tPaperLaidDepartmentModel.mEvents = PaperCategoryTypeList;

                #endregion

                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                //objPaperModel.ResultCount = int.Parse(ResultCount);
                tPaperLaidV PendinToLay = new tPaperLaidV();
                PendinToLay.PageIndex = int.Parse(PageNumber);
                PendinToLay.PAGE_SIZE = int.Parse(RowsPerPage);
                //PendinToLay = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetPendingToLayOtherPaperLaidByType", PendinToLay);
                //if (PendinToLay != null)
                //{
                //    if (PendinToLay.DepartmentSubmitList.Count > 0)
                //    {
                //        tPaperLaidDepartmentModel.DepartmentSubmitList = PendinToLay.DepartmentSubmitList;
                //    }
                //}
                if (tPaperLaidDepartmentModel.DepartmentSubmitList.Count > 0)
                {
                    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentSubmitList.Count;
                }
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("20");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetPaperPendinToLayList", tPaperLaidDepartmentModel);
            }
            TempData["lSM"] = "OPT";
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }
        //added code venkat for dynamic 
        [HttpPost]
        public ActionResult GetOtherPaperLaidSubmitList(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, int? QNumber, int? Bname)// string ResultCount,
        {
            if (CurrentSession.UserID != null)
            {
                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                // ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mEvent> PaperCategoryTypeList = Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidPaperCategoryType", PaperCategoryTypeListMod) as List<mEvent>;
                tPaperLaidDepartmentModel.mEvents = PaperCategoryTypeList;

                #endregion

                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                // objPaperModel.ResultCount = int.Parse(ResultCount);
                //tPaperLaidDepartmentModel.DepartmentPendingList = (ICollection<PaperMovementModel>)Helper.ExecuteService("OtherPaperLaid", "GetSubmittedOtherPaperLaidList", objPaperModel);
                //if (tPaperLaidDepartmentModel.DepartmentPendingList.Count > 0)
                //{
                //    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentPendingList.Count;
                //}
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);
                //added code venkat for dynamic 
                tPaperLaidDepartmentModel.QuestionNumber = QNumber ?? 0;
                tPaperLaidDepartmentModel.EventId = Bname ?? 0;
                //end-------------------------------------------------

                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("20");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetPaperLaidSubmittList", tPaperLaidDepartmentModel);
            }
            TempData["lSM"] = "OPT";
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }

        public long SaveOtherPaperLaid(HttpPostedFileBase file,HttpPostedFileBase DocFile, tPaperLaidV obj)
        {
            tPaperLaidV siteSetting = new tPaperLaidV();
            tPaperLaidV mdl = new tPaperLaidV();
            tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
            siteSetting = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", siteSetting);
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                mdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                mdl.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            obj = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
            mdl = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "SubmittOtherPaperLaidEntry", obj);
            mdl.EventId = obj.EventId;
            int paperLaidId = (int)mdl.PaperLaidId;
            mdl = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetFileSaveDetailsForOtherPaperLaid", mdl);


            if (file == null)
            {
                string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        string name = Path.GetFileName(page);
                        string nameKey = Path.GetFileNameWithoutExtension(page);
                        string directory1 = Path.GetDirectoryName(page);
                        //
                        // Display the Path strings we extracted.
                        //
                        Console.WriteLine("{0}, {1}, {2}, {3}",
                        page, name, nameKey, directory1);
                        mdlTemp.FileName = name;
                    }
                    string ext = Path.GetExtension(mdlTemp.FileName);
                    string fileNam1 = mdlTemp.FileName.Replace(ext, "");


                    var vers = obj.FileVersion.GetValueOrDefault() + 1;


                    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "O" + "_" + mdl.PaperLaidId + "_V" + vers + ext;


                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                    DirectoryInfo Dir = new DirectoryInfo(Url);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }

                    string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);
                    string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                    System.IO.File.Copy(SourceFile, path, true);
                    mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                    var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                    //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                    string FileStructurePath = Acess.SettingValue;
                    mdlTemp.FileStructurePath = FileStructurePath;
                    mdlTemp.FileName = actualFileName;
                    mdlTemp.Version = vers;

                }
                mdlTemp.PaperLaidId = mdl.PaperLaidId;
                mdlTemp.Version = mdl.Count;


            }


            if (DocFile == null)
            {

                string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID));
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        string name = Path.GetFileName(page);
                        string nameKey = Path.GetFileNameWithoutExtension(page);
                        string directory1 = Path.GetDirectoryName(page);
                        //
                        // Display the Path strings we extracted.
                        //
                        Console.WriteLine("{0}, {1}, {2}, {3}",
                        page, name, nameKey, directory1);
                        mdlTemp.DocFileName = name;
                    }

                    string ext = Path.GetExtension(mdlTemp.DocFileName);

                    var DocVer = obj.DocFileVersion.GetValueOrDefault() + 1;


                    string fileName = mdlTemp.DocFileName.Replace(ext, "");


                    string actualDocFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "O" + "_" + mdl.PaperLaidId + "_V" + DocVer + ext;



                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                    DirectoryInfo Dir = new DirectoryInfo(Url);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }

                    string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualDocFileName);
                    //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                    System.IO.File.Copy(SourceFile, path, true);
                    //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                    mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                    var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                    string FileStructurePath = Acess.SettingValue;
                    mdlTemp.FileStructurePath = FileStructurePath;
                    mdlTemp.DocFileName = actualDocFileName;
                    mdlTemp.DocFileVersion = DocVer;
                }

                mdlTemp.PaperLaidId = mdl.PaperLaidId;
                mdlTemp.DocFileVersion = obj.MainDocCount;

            }

            //if (file != null)
            //{
            //    string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
            //    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
            //    if (!Dir.Exists)
            //    {
            //        Dir.Create();
            //    }
            //    string ext = Path.GetExtension(file.FileName);
            //    string fileName = file.FileName.Replace(ext, "");

            //    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "O_" + paperLaidId + "_v" + mdl.Count + ext;

            //    file.SaveAs(Server.MapPath(Url + actualFileName));
            //    mdlTemp.FileName = actualFileName;
            //    mdlTemp.FilePath = Url;
            //    mdlTemp.Version = mdl.Count;
            //    mdlTemp.PaperLaidId = mdl.PaperLaidId;
            //    mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
            //}

            mdlTemp.AssemblyId = CurrentSession.AssemblyId;
            mdlTemp.SessionId = CurrentSession.SessionId;
            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("OtherPaperLaid", "SaveDetailsInTempOtherPaperLaid", mdlTemp);
            mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
            mdl = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "UpdateOthePaperLaidDeptActiveId", mdlTemp);
            return mdl.PaperLaidId;
        }

        public ActionResult ShowOtherPaperLaidDetailByID(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByID", movementModel);
                return PartialView("_GetOtherPaperLaidSubmittedListById", movementModel);
            }
            return null;
        }

        public ActionResult FullViewOtherPaperLaidDetailByID(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByID", movementModel);
                return PartialView("_ShowFullDetailByOtherPaperLaidId", movementModel);
            }
            return null;
        }

        public ActionResult GetSubmittedOtherPaperLaidByID(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByID", movementModel);
                return PartialView("_GetSubmittedOtherPaperLaidByID", movementModel);
            }
            return null;
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateOtherPaperLaidFile(HttpPostedFileBase file, tPaperLaidV tPaper)
        {
            if (CurrentSession.UserID != "")
            {
                tPaperLaidTemp tPaperTemp = new tPaperLaidTemp();
                PaperMovementModel obj = new PaperMovementModel();
                obj.PaperLaidId = tPaper.PaperLaidId;
                obj.actualFilePath = tPaper.FilePath;
                tPaperLaidV model = new tPaperLaidV();
                tPaperLaidV siteSetting = new tPaperLaidV();
                siteSetting = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", siteSetting);
                //obj.SessionId = siteSetting.SessionCode;
                //obj.AssemblyId = siteSetting.AssemblyCode;

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);

                }

                model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidFileVersion", tPaper);
                if (model != null)
                {

                    if (file == null)
                    {

                        string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                        string directory = Server.MapPath(url);
                        if (Directory.Exists(directory))
                        {
                            string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                            string SourceFile = savedFileName[0];
                            foreach (string page in savedFileName)
                            {
                                string name = Path.GetFileName(page);
                                string nameKey = Path.GetFileNameWithoutExtension(page);
                                string directory1 = Path.GetDirectoryName(page);
                                //
                                // Display the Path strings we extracted.
                                //
                                Console.WriteLine("{0}, {1}, {2}, {3}",
                                page, name, nameKey, directory1);
                                tPaperTemp.FileName = name;
                            }
                            string ext = Path.GetExtension(tPaperTemp.FileName);
                            string fileNam1 = tPaperTemp.FileName.Replace(ext, "");



                            model.Count = model.Count;

                            string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "O" + "_" + tPaper.PaperLaidId + "_V" + model.Count + ext;

                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                            string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                            DirectoryInfo Dir = new DirectoryInfo(Url);
                            if (!Dir.Exists)
                            {
                                Dir.Create();
                            }

                            string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);

                            System.IO.File.Copy(SourceFile, path, true);
                            tPaperTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                            string FileStructurePath = Acess.SettingValue;
                            tPaperTemp.FileStructurePath = FileStructurePath;
                            tPaperTemp.FileName = actualFileName;

                            tPaperTemp.PaperLaidId = tPaper.PaperLaidId;

                            tPaperTemp.Version = model.Count;

                        }

                        tPaperTemp.Version = model.Count;

                    }

                    //if (file != null)
                    //{
                    //    string Url = obj.actualFilePath;
                    //    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                    //    if (!Dir.Exists)
                    //    {
                    //        Dir.Create();
                    //    }
                    //    string ext = Path.GetExtension(file.FileName);
                    //    string fileName = file.FileName.Replace(ext, "");
                    //    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "O_" + obj.PaperLaidId + "_v" + model.Count + ext;
                    //    file.SaveAs(Server.MapPath(Url + actualFileName));
                    //    tPaperTemp.FileName = actualFileName;
                    //    tPaperTemp.FilePath = Url;
                    //    tPaperTemp.PaperLaidId = obj.PaperLaidId;
                    //    tPaperTemp.Version = model.Count;
                    //    tPaperTemp.DeptSubmittedBy = CurrentSession.UserName;
                    //    tPaperTemp.DeptSubmittedDate = DateTime.Now;
                    //    tPaperTemp.MinisterSubmittedBy = null;
                    //    tPaperTemp.MinisterSubmittedDate = null;
                    //}


                    tPaperLaidTemp tPaperTemp1 = new tPaperLaidTemp();


                    tPaperTemp1 = (tPaperLaidTemp)Helper.ExecuteService("OtherPaperLaid", "GetExisitngFileDetailsForOtherPaperLaid", tPaper);

                    tPaperTemp.DocFileName = tPaperTemp1.DocFileName;

                    tPaperTemp.AssemblyId = CurrentSession.AssemblyId;
                    tPaperTemp.SessionId = CurrentSession.SessionId;


                    tPaperTemp = (tPaperLaidTemp)Helper.ExecuteService("OtherPaperLaid", "UpdateOtherPaperLaidFileByID", tPaperTemp);
                    // TempData["valID"] = 1;
                }
            }
            //  return RedirectToAction("/PaperLaidDepartment/DepartmentDashboard");

            if (Request.IsAjaxRequest())
            {
                tPaperLaidV mod = new tPaperLaidV();

                return Json(mod, JsonRequestBehavior.AllowGet);
            }
            else
            {


            }
            return null;
        }
        #endregion

        #region Other Paper Laid Grid Binder

        public ActionResult GetOtherPaperLaidAllListGrid(JqGridRequest request)
        {
            tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();
            PaperMovementModel objPaperModel = new PaperMovementModel();

            objPaperModel.PAGE_SIZE = request.RecordsCount;
            objPaperModel.PageIndex = request.PageIndex + 1;


            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                tPaperLaidDepartmentModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                objPaperModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                tPaperLaidDepartmentModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                objPaperModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                tPaperLaidDepartmentModel.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", tPaperLaidDepartmentModel);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    objPaperModel.DepartmentId = item.AssociatedDepts;
                }

            }
            else
            {
                objPaperModel.DepartmentId = CurrentSession.DeptID;
            }

            /*End*/

            var result = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidAllList", objPaperModel);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.DepartmentSubmitList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<PaperMovementModel>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from otherPaperList in resultForGrid
                                      select new JqGridRecord<PaperMovementModel>(Convert.ToString(otherPaperList.PaperLaidId), new PaperMovementModel(otherPaperList)));

            return new JqGridJsonResult() { Data = response };
        }
        //added code venkat for dynamic 
        public ActionResult GetOtherPaperLaidPendingsGrid(JqGridRequest request, int? QNumber, int? Bname)
        {
            tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

            OtherPaperDraftCustom objPaperModel = new OtherPaperDraftCustom();
            objPaperModel.PAGE_SIZE = request.RecordsCount;
            objPaperModel.PageIndex = request.PageIndex + 1;
            //added code venkat for dynamic 
            objPaperModel.PaperTypeID = QNumber ?? 0;
            objPaperModel.EventId = Bname ?? 0;
            //end---------------------------------


            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                tPaperLaidDepartmentModel.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", tPaperLaidDepartmentModel);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    objPaperModel.DepartmentId = item.AssociatedDepts;
                }

            }
            else
            {
                objPaperModel.DepartmentId = CurrentSession.DeptID;
            }

            /*End*/

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                tPaperLaidDepartmentModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                objPaperModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                tPaperLaidDepartmentModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                objPaperModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }


            var result = (ICollection<OtherPaperDraftCustom>)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidPendingsDetails", objPaperModel);
            tPaperLaidDepartmentModel.DepartmentDraftList = result;

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.Count / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.Count
            };

            var resultToSort = tPaperLaidDepartmentModel.DepartmentDraftList.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<OtherPaperDraftCustom>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from otherPaperList in resultForGrid
                                      select new JqGridRecord<OtherPaperDraftCustom>(Convert.ToString(otherPaperList.PaperLaidId), new OtherPaperDraftCustom(otherPaperList)));

            return new JqGridJsonResult() { Data = response };

        }
        //added code venkat for dynamic 
        [HttpPost]
        public ActionResult GetUpComingOtherPaperLaidGrid(JqGridRequest request, int? QNumber, int? Bname)
        {
            tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

            PaperMovementModel objPaperModel = new PaperMovementModel();
            tPaperLaidDepartmentModel.PAGE_SIZE = request.RecordsCount;
            tPaperLaidDepartmentModel.PageIndex = request.PageIndex + 1;
            //added code venkat for dynamic 
            objPaperModel.QuestionNumber = QNumber ?? 0;
            objPaperModel.EventId = Bname ?? 0;
            //end---------------------------------
            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                tPaperLaidDepartmentModel.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", tPaperLaidDepartmentModel);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    tPaperLaidDepartmentModel.DepartmentId = item.AssociatedDepts;
                }

            }
            else
            {
                tPaperLaidDepartmentModel.DepartmentId = CurrentSession.DeptID;
            }

            /*End*/

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                tPaperLaidDepartmentModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                tPaperLaidDepartmentModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            var result = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "UpcomingLOBByOtherpaperLaidId", tPaperLaidDepartmentModel);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.DepartmentSubmitList.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperMovementModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from otherPaperList in resultForGrid
                                      select new JqGridRecord<PaperMovementModel>(Convert.ToString(otherPaperList.PaperLaidId), new PaperMovementModel(otherPaperList)));

            return new JqGridJsonResult() { Data = response };
        }
        [HttpPost]
        public ActionResult OtherPaperLaidInHouseGrid(JqGridRequest request)
        {
            tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

            PaperMovementModel objPaperModel = new PaperMovementModel();
            objPaperModel.PAGE_SIZE = request.RecordsCount;
            objPaperModel.PageIndex = request.PageIndex + 1;


            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                tPaperLaidDepartmentModel.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", tPaperLaidDepartmentModel);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    objPaperModel.DepartmentId = item.AssociatedDepts;
                }

            }
            else
            {
                objPaperModel.DepartmentId = CurrentSession.DeptID;
            }

            /*End*/

            var result = (ICollection<PaperMovementModel>)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidInHouseByType", objPaperModel);
            tPaperLaidDepartmentModel.DepartmentPendingList = result;

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.Count / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.Count
            };

            var resultToSort = tPaperLaidDepartmentModel.DepartmentPendingList.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperMovementModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from otherPaperList in resultForGrid
                                      select new JqGridRecord<PaperMovementModel>(Convert.ToString(otherPaperList.PaperLaidId), new PaperMovementModel(otherPaperList)));

            return new JqGridJsonResult() { Data = response };
        }
        //added code venkat for dynamic 
        public ActionResult OtherPaperPendingToLayGrid(JqGridRequest request, int? QNumber, int? Bname)
        {
            tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

            PaperMovementModel objPaperModel = new PaperMovementModel();
            tPaperLaidDepartmentModel.PAGE_SIZE = request.RecordsCount;
            tPaperLaidDepartmentModel.PageIndex = request.PageIndex + 1;
            //added code venkat for dynamic 
            objPaperModel.QuestionNumber = QNumber ?? 0;
            objPaperModel.EventId = Bname ?? 0;
            //end------------------------------------
            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                tPaperLaidDepartmentModel.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", tPaperLaidDepartmentModel);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    tPaperLaidDepartmentModel.DepartmentId = item.AssociatedDepts;
                }

            }
            else
            {
                tPaperLaidDepartmentModel.DepartmentId = CurrentSession.DeptID;
            }

            /*End*/

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                tPaperLaidDepartmentModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                tPaperLaidDepartmentModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            var result = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetPendingToLayOtherPaperLaidByType", tPaperLaidDepartmentModel);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.DepartmentSubmitList.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperMovementModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from otherPaperList in resultForGrid
                                      select new JqGridRecord<PaperMovementModel>(Convert.ToString(otherPaperList.PaperLaidId), new PaperMovementModel(otherPaperList)));

            return new JqGridJsonResult() { Data = response };
        }
        //added code venkat for dynamic 
        public ActionResult GetOtherPaperLaidSubmitListGrid(JqGridRequest request, int? QNumber, int? Bname)
        {
            tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

            PaperMovementModel objPaperModel = new PaperMovementModel();
            objPaperModel.PAGE_SIZE = request.RecordsCount;
            objPaperModel.PageIndex = request.PageIndex + 1;
            //added code venkat for dynamic 
            objPaperModel.QuestionNumber = QNumber ?? 0;
            objPaperModel.EventId = Bname ?? 0;
            //end------------------------------------
            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                tPaperLaidDepartmentModel.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", tPaperLaidDepartmentModel);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    objPaperModel.DepartmentId = item.AssociatedDepts;
                }

            }
            else
            {
                objPaperModel.DepartmentId = CurrentSession.DeptID;
            }

            /*End*/

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                tPaperLaidDepartmentModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                objPaperModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                tPaperLaidDepartmentModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                objPaperModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            var result = (ICollection<PaperMovementModel>)Helper.ExecuteService("OtherPaperLaid", "GetSubmittedOtherPaperLaidList", objPaperModel);
            tPaperLaidDepartmentModel.DepartmentPendingList = result;

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.Count / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.Count
            };

            var resultToSort = tPaperLaidDepartmentModel.DepartmentPendingList.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<PaperMovementModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from otherPaperList in resultForGrid
                                      select new JqGridRecord<PaperMovementModel>(Convert.ToString(otherPaperList.PaperLaidId), new PaperMovementModel(otherPaperList)));

            return new JqGridJsonResult() { Data = response };

        }


    }

        #endregion
}