﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.DeptRegisterModel;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.PaperLaid;
using SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Controllers
{
    public class DepartmentRegisterController : Controller
    {
        //
        // GET: /PaperLaidDepartment/DepartmentRegister/

        public ActionResult Index()
        {
            tPaperLaidV model = new tPaperLaidV();
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);

            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }
            else
            {
                model.DepartmentId = CurrentSession.DeptID;
            }
            DeptRegisterModel deptInfo1 = new DeptRegisterModel();
            deptInfo1.DepartmentId = model.DepartmentId;
            //List<mDepartment> deptInfo = new List<mDepartment>();
          //  deptInfo1 = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartmentByIDs", model);
          //  deptInfo1 = (List<DeptRegisterModel>)Helper.ExecuteService("Department", "GetDepartmentByIDs", model);
            //deptInfo1.deptId = model.DepartmentId;
            //deptInfo1 = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartmentByIDs", model);
            deptInfo1 = (DeptRegisterModel)Helper.ExecuteService("Department", "GetReportDepartmentByIDs", deptInfo1);
            return View(deptInfo1);
        }

        public ActionResult getListtransferQ(string DeptId)
        {
            DeptRegisterModel deptInfo1 = new DeptRegisterModel();

            deptInfo1.AadharId = CurrentSession.AadharId;
            deptInfo1.DepartmentId = DeptId;
            deptInfo1 = (DeptRegisterModel)Helper.ExecuteService("Notice", "GetListtTransfered", deptInfo1);
         
          
            //List<tAuditTrialViewModel> list = (List<tAuditTrialViewModel>)Helper.ExecuteService("Diary", "getPaperLaidList", ReferenceNumber);
            //foreach (var item in list)
            //{
            //    string[] filepathName1 = item.filePath.Split('/');
            //    item.filePathName = filepathName1[filepathName1.Length - 1];
            //}
            return PartialView("_ListTransferedQuestions", deptInfo1);
        }

    }
}
