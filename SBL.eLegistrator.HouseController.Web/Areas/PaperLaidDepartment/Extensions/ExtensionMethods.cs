﻿namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Extensions
{
    #region Namespace Reffrences

    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Diaries;
    using SBL.DomainModel.Models.eFile;
    using SBL.DomainModel.Models.Employee;
    using SBL.DomainModel.Models.Enums;
    using SBL.DomainModel.Models.Notice;
    using SBL.DomainModel.Models.PaperLaid;
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.DomainModel.Models.User;
    using SBL.DomainModel.Models.UserModule;
    using SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Models;
    using SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models;
    using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Models;
    using SBL.eLegistrator.HouseController.Web.Helpers;
    using SBL.eLegistrator.HouseController.Web.Utility;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Xml.Linq;

    #endregion Namespace Reffrences

    public static class ExtensionMethods
    {
        #region Static List Methods

        public static SelectList GetNamePrefixes()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Sri.", Value = "Sri." });
            result.Add(new SelectListItem() { Text = "Smt.", Value = "Smt." });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetIDCardTypes()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Aadhar Card", Value = "1" });
            result.Add(new SelectListItem() { Text = "PAN Card", Value = "2" });
            result.Add(new SelectListItem() { Text = "Voter ID Card", Value = "3" });
            result.Add(new SelectListItem() { Text = "Driving Licence Card", Value = "4" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetPassTypeList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Aadhar Card", Value = "1" });
            result.Add(new SelectListItem() { Text = "PAN Card", Value = "2" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetGateNumberList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Gate Number 1", Value = "1" });
            result.Add(new SelectListItem() { Text = "Gate Number 2", Value = "2" });
            result.Add(new SelectListItem() { Text = "Gate Number 3", Value = "3" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetGender()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Male", Value = "Male" });
            result.Add(new SelectListItem() { Text = "Female", Value = "Female" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetTimeTypeList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Full Day", Value = "Full Day" });
            result.Add(new SelectListItem() { Text = "AN", Value = "AN" });
            result.Add(new SelectListItem() { Text = "FN", Value = "FN" });
            result.Add(new SelectListItem() { Text = "After Session", Value = "After Session" });
            result.Add(new SelectListItem() { Text = "Before Session", Value = "Before Session" });

            return new SelectList(result, "Value", "Text");
        }

        #endregion Static List Methods

        #region Entity/Model extension methods.
        static List<PassCategory> ListPassCategoryList = new List<PassCategory>();
        public static mDepartmentPasses ToEntity(this PassesViewModel Model)
        {
            mDepartmentPasses entity = new mDepartmentPasses()
            {
                PassID = Model.PassID,
                PassCode = Model.PassCode,
                PassCategoryID = Model.PassCategoryID,
                AssemblyCode = Model.AssemblyCode,
                SessionCode = Model.SessionCode,
                AadharID = Model.AadharID,
                Prefix = Model.Prefix,
                Name = Model.Name,
                Gender = Model.Gender,
                Age = Model.Age,
                FatherName = Model.FatherName,
                NoOfPersions = Model.NoOfPersions,
                RecommendationType = Model.RecommendationType,
                RecommendationBy = Model.RecommendationBy,
                RecommendationDescription = Model.RecommendationDescription,
                MobileNo = Model.MobileNo,
                Email = Model.Email,
                Address = Model.Address,
                Photo = Model.Photo,
                OrganizationName = Model.OrganizationName,
                Designation = Model.Designation,
                DayTime = Model.DayTime,
                Time = Model.Time,
                DepartmentID = Model.DepartmentID,
                SessionDateFrom = Model.SessionDateFrom,
                SessionDateTo = Model.SessionDateTo,
                ApprovedDate = Model.ApprovedDate,
                ApprovedBy = Model.ApprovedBy,
                VehicleNumber = Model.VehicleNumber,
                GateNumber = Model.GateNumber,
                IsRequested = Model.IsRequested,
                RequestedDate = Model.RequestedDate,
                Status = Model.Status,
                RequestedBy = Model.RequestedBy,
                IsActive = Model.IsActive,
                IsRequestUserID = Model.IsRequestUserID,
                RequestdID = GenerateValue(Model.RecommendationType.Substring(0, 1), Model.AssemblyCode, Model.SessionCode)
            };
            return entity;
        }

        public static PassesViewModel ToModel(this mDepartmentPasses entity)
        {
            var Model = new PassesViewModel()
            {
                PassID = entity.PassID,
                PassCode = entity.PassCode,
                PassCategoryID = entity.PassCategoryID,
                AssemblyCode = entity.AssemblyCode,
                SessionCode = entity.SessionCode,
                AadharID = entity.AadharID,
                Prefix = entity.Prefix,
                Name = entity.Name,
                Gender = entity.Gender,
                Age = entity.Age,
                FatherName = entity.FatherName,
                NoOfPersions = entity.NoOfPersions,
                RecommendationType = entity.RecommendationType,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                MobileNo = entity.MobileNo,
                Email = entity.Email,
                Address = entity.Address,
                PhotoName=entity.Photo,
                Photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                OrganizationName = entity.OrganizationName,
                Designation = entity.Designation,
                DayTime = entity.DayTime,
                Time = entity.Time,
                DepartmentID = entity.DepartmentID,
                SessionDateFrom = entity.SessionDateFrom,
                SessionDateTo = entity.SessionDateTo,
                ApprovedDate = entity.ApprovedDate,
                ApprovedBy = entity.ApprovedBy,
                VehicleNumber = entity.VehicleNumber,
                GateNumber = entity.GateNumber,
                IsRequested = entity.IsRequested,
                RequestedDate = entity.RequestedDate,
                Status = entity.Status,
                RequestedBy = entity.RequestedBy,
                IsActive = entity.IsActive,
                PassCategoryName = GetPassCategoryNameByID(entity.PassCategoryID),
                IsApproved = entity.IsApproved,
                IsRequestUserID = entity.IsRequestUserID,
                RecommendationByName = GetMemberNameByCode(entity.RecommendationBy)
            };
            return Model;
        }

        public static List<PassesViewModel> ToModelList(this List<mDepartmentPasses> entityList)
        {
            var ModelList = entityList.Select(entity => new PassesViewModel()
            {
                PassID = entity.PassID,
                PassCode = entity.PassCode,
                PassCategoryID = entity.PassCategoryID,
                AssemblyCode = entity.AssemblyCode,
                SessionCode = entity.SessionCode,
                AadharID = entity.AadharID,
                Prefix = entity.Prefix,
                Name = entity.Name,
                Gender = entity.Gender,
                Age = entity.Age,
                FatherName = entity.FatherName,
                NoOfPersions = entity.NoOfPersions,
                RecommendationType = entity.RecommendationType,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                MobileNo = entity.MobileNo,
                Email = entity.Email,
                Address = entity.Address,
                Photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                OrganizationName = entity.OrganizationName,
                Designation = entity.Designation,
                DayTime = entity.DayTime,
                Time = entity.Time,
                DepartmentID = entity.DepartmentID,
                SessionDateFrom = entity.SessionDateFrom,
                SessionDateTo = entity.SessionDateTo,
                ApprovedDate = entity.ApprovedDate,
                ApprovedBy = entity.ApprovedBy,
                VehicleNumber = entity.VehicleNumber,
                GateNumber = entity.GateNumber,
                IsRequested = entity.IsRequested,
                RequestedDate = entity.RequestedDate,
                Status = entity.Status,
                RequestedBy = entity.RequestedBy,
                IsActive = entity.IsActive,
                IsApproved = entity.IsApproved,
                IsSessionTransferID = entity.IsSessionTransferID,
                RequestdID = entity.RequestdID
            });
            return ModelList.ToList();
        }


        public static string GetPassCategoryNameByID(int passCategoryID)
        {
            var passCategoryModel = (PassCategory)Helper.ExecuteService("AdministrationBranch", "GetPassCategoryById", new PassCategory { PassCategoryID = passCategoryID });

            if (passCategoryModel != null)
            {
                return passCategoryModel.Name;
            }
            return "";
        }

        #endregion Entity/Model extension methods.

        public static string GetMemberNameByCode(string memberCode)
        {
            return (string)Helper.ExecuteService("PublicPass", "GetMemberNameByCode", new PublicPass { RecommendationBy = memberCode });
        }

        public static string GenerateValue(string Prefix, int AssemblyCode, int SessionCode)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 6)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            var ResultVal = string.Format("{0}{1}{2}{3}", Prefix, AssemblyCode, SessionCode, result);
            return ResultVal;
        }

        public static string GenerateOTPUSerIdValue(int AssemblyId, int SessionId, string MobileNo)
        {
            var chars = "0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 5)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            //var ResultVal = string.Format("T/" + AssemblyId + "/" + SessionId + "/" + result);
            var ResultVal = string.Format("T" + MobileNo);
            return ResultVal;
        }

        public static string GenerateRandomValue()
        {
            var chars = "0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 5)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            //var ResultVal = string.Format("T/" + AssemblyId + "/" + SessionId + "/" + result);
            //var ResultVal = string.Format();
            return result;
        }

        public static string RandomValue()
        {
            var chars = "0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 6)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            //var ResultVal = string.Format("T/" + AssemblyId + "/" + SessionId + "/" + result);
            //var ResultVal = string.Format();
            return result;
        }

        #region Added venkat code for Department Dynamic Counting

        //Added venkat code for Department Dynamic Counting
        public static int GetTotalCountBySubMenuForDept(this string SubMenu, int ModuleId)
        {
            int Cnt = 0;
            tPaperLaidV model = new tPaperLaidV();
            tUserAccessRequest requstModel = new tUserAccessRequest();
            requstModel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);
            //requstModel.UserID = new Guid(CurrentSession.UserID);
            mUsers usermdl = new mUsers();
            usermdl.UserId = new Guid(CurrentSession.UserID);
            usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
            if (usermdl.IsSecretoryId == true)
            {
                requstModel.SecreataryId = Convert.ToInt16(usermdl.SecretoryId);
            }
            else if (usermdl.IsMember == "True")
            {
                requstModel.MemberId = Convert.ToInt16(usermdl.UserName);
            }
            else if (usermdl.IsHOD == true)
            {
                requstModel.HODId = Convert.ToInt16(usermdl.SecretoryId);
            }
            // PaperMovementModel model1 = new PaperMovementModel();

            model.ModuleId = ModuleId;
            model.UserID = new Guid(CurrentSession.UserID);
            if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
            {
                model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            }
            //Get the Total count of All Type of question.

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            //model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model);

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            //model.SessionCode = model.SessionId = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
           // AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    model.DepartmentId = item.AssociatedDepts;
                    model.IsPermissions = item.IsPermissions; ;
                }

                if (model.DepartmentId == null)
                {
                    model.DepartmentId = CurrentSession.DeptID;
                }
            }
            else
            {
                model.DepartmentId = CurrentSession.DeptID;
            }

            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
            {

                if (SubMenu != null && SubMenu != "")
                {
                    switch (SubMenu)
                    {
                        case "Pending For Reply":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "PendingForReplyQuestionsList1", model);
                                return result.TotalStaredPendingForSubmission1 + result.TotalStaredReceived1;
                            }

                        case "Draft Replies":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "DraftRepliesQuestionsList1", model);
                                if (model.ModuleId == 6)
                                {
                                    return result.TotalStaredPendingForSubmissionNew;
                                    //return result.ResultCount;
                                }
                                else if (model.ModuleId == 7)
                                {
                                    return result.TotalUnStaredPendingForSubmissionNew;
                                }
                                else
                                {
                                    return result.NoticeDraftCount;
                                }
                            }
                        case "Reply Sent":
                            {
                                //  var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "RepliesSentQuestionsList1", model);
                                var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "DraftRepliescount", model);
                                //return result.TotalStaredSubmitted;
                                if (model.ModuleId == 6)
                                {
                                    return result.TotalStaredReplySent;
                                    //return result.ResultCount;
                                }
                                else if (model.ModuleId == 7)
                                {
                                    return result.TotalUnStaredReplySent;
                                }
                                else
                                {
                                    return result.NoticeSentCount;
                                }
                            }
                        case "Replies Sent":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetNoticeReplySentCount", model);
                                return result;
                            }

                        case "Upcoming LOB":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetNoticeULOBList", model);
                                return result;
                            }
                        case "Laid In The House":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetNoticeLIHList", model);
                                return result;
                            }
                        case "Pending To Lay":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetNoticePLIHList", model);
                                return result;
                            }
                        case "Draft Memorandum":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetDraftBillsCount", model);
                                return result;
                            }
                        case "Draft Sent":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetBillsSentCount", model);
                                return result;
                            }
                        case "Draft Other Papers":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDraftOthePapersCountForOtherpaperlaid", model);
                                return result.OtherPaperCount;
                            }
                        case "Other Papers Sent":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetSubmittedOtherPaperLaidList", model);
                                return result.Count;
                            }
                        case "Pending Request":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetPendingUserRequestCount", requstModel);
                                return result.Count;
                            }
                        case "Accepted Request":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetAcceptedUserRequestCount", requstModel);
                                return result.Count;
                            }
                        case "Rejected Request":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetRejectedUserRequestCount", requstModel);
                                return result.Count;
                            }
                        case "Receipt":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PrintingPress", "GetRecieptCountFordept", model);

                                return result.ReceiptCount;
                            }
                        case "Dispatch":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PrintingPress", "GetDispatchCountFordept", model);

                                return result.DispatchCount;
                            }
                    }
                }
            }
            else
            {
                if (SubMenu != null && SubMenu != "")
                {
                    switch (SubMenu)
                    {
                        case "उत्तर के लिए लंबित":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "PendingForReplyQuestionsList1", model);
                                return result.TotalStaredPendingForSubmission1 + result.TotalStaredReceived1;
                            }

                        case "मसौदा का जवाब":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "DraftRepliesQuestionsList1", model);
                                if (model.ModuleId == 6)
                                {
                                    return result.TotalStaredPendingForSubmissionNew;
                                    //return result.ResultCount;
                                }
                                else if (model.ModuleId == 7)
                                {
                                    return result.TotalUnStaredPendingForSubmissionNew;
                                }
                                else
                                {
                                    return result.NoticeDraftCount;
                                }
                            }
                        case "जवाब भेजा":
                            {
                                //  var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "RepliesSentQuestionsList1", model);
                                var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "DraftRepliescount", model);
                                //return result.TotalStaredSubmitted;
                                if (model.ModuleId == 6)
                                {
                                    return result.TotalStaredReplySent;
                                    //return result.ResultCount;
                                }
                                else if (model.ModuleId == 7)
                                {
                                    return result.TotalUnStaredReplySent;
                                }
                                else
                                {
                                    return result.NoticeSentCount;
                                }
                            }
                        case "Replies Sent":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetNoticeReplySentCount", model);
                                return result;
                            }

                        case "कार्यवाई के आगामी सूची":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetNoticeULOBList", model);
                                return result;
                            }
                        case "सदन में रखी गई":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetNoticeLIHList", model);
                                return result;
                            }
                        case "अपूर्ण आधार शिला रखना":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetNoticePLIHList", model);
                                return result;
                            }
                        case "विधेयक का प्रारूप":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetDraftBillsCount", model);
                                return result;
                            }
                        case "भेजे गए विधेयकों":
                            {
                                var result = (int)Helper.ExecuteService("PaperLaid", "GetBillsSentCount", model);
                                return result;
                            }
                        case "अन्य कागजात का प्रारूप":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDraftOthePapersCountForOtherpaperlaid", model);
                                return result.OtherPaperCount;
                            }
                        case "भेजे गए कागज":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetSubmittedOtherPaperLaidList", model);
                                return result.Count;
                            }
                        case "लंबित अनुरोध":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetPendingUserRequestCount", requstModel);
                                return result.Count;
                            }
                        case "स्वीकृत अनुरोध":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetAcceptedUserRequestCount", requstModel);
                                return result.Count;
                            }
                        case "अस्वीकृत अनुरोध":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetRejectedUserRequestCount", requstModel);
                                return result.Count;
                            }
                        case "Receipt":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PrintingPress", "GetRecieptCountFordept", model);

                                return result.ReceiptCount;
                            }
                        case "Dispatch":
                            {
                                var result = (tPaperLaidV)Helper.ExecuteService("PrintingPress", "GetDispatchCountFordept", model);

                                return result.DispatchCount;
                            }
                    }
                }
            }
            return Cnt;
        }

        #endregion Added venkat code for Department Dynamic Counting

        #region Added Ashwani code for Vidhan Sabha Dynamic Counting

        public static int GetTotalCountBySubMenuForVidhanSabhaDept(this string SubMenu, int ModuleId, int CustomSubMenuID)
        {
            int Cnt = 0;
            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                DiaryModel DMdl = new DiaryModel();
                PaperLaidSummaryViewModel model = new PaperLaidSummaryViewModel();

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }

                //model.mAssemblyList = (List<mAssembly>)Helper.ExecuteService("LegislationFixation", "GetAssemblyList", null);
                //model.sessionList = (List<mSession>)Helper.ExecuteService("LegislationFixation", "GetSessionLsitbyAssemblyId", DMdl);
                //model.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCount", DMdl);
                //model.SentNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeSentCount", DMdl);
                //model.RecievedNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperRecievedCount", DMdl);
                //model.CurrentLobNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperCurrentLobCount", DMdl);
                //model.LaidInHomeNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperLaidInHomeCount", DMdl);
                //////pending to lay Notice
                //model.PendingToLayNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperPendingToLayListCount", DMdl);

                //model.RecievedBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperRecievedCount", DMdl);
                //model.CurrentLobBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperCurrentLobCount", DMdl);
                //model.LaidInHomeBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperLaidInHomeCount", DMdl);
                //////Pending to lay Bills
                //model.PendingToLayBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperPendingToLayListCount", DMdl);

                //model.RecievedCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperRecievedCount", DMdl);
                //model.CurrentLobCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperCurrentLobCount", DMdl);
                //model.LaidInHomeCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperLaidInHomeCount", DMdl);

                //////pending to lay committee
                //model.PendingToLayCommitteeCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperPendingToLayListCount", DMdl);

                //model.PendingStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPendingCount", DMdl);
                //model.SentStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionSentCount", DMdl);
                //model.RecievedStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperRecievedCount", DMdl);
                //model.CurrentLobStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperCurrentLobCount", DMdl);
                //model.LaidInHomeStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperLaidInHomeCount", DMdl);

                //////starred Question pending to lay
                //model.PendingToLayStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperPendingToLayListCount", DMdl);

                //model.PendingUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPendingCount", DMdl);
                //model.SentUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionSentCount", DMdl);
                //model.RecievedUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperRecievedCount", DMdl);
                //model.CurrentLobUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperCurrentLobCount", DMdl);
                //model.LaidInHomeUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperLaidInHomeCount", DMdl);
                //////pending to lay Unstarred
                //model.PendingToLayUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperPendingToLayListCount", DMdl);

                //model.RecievedPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayRecievedCount", DMdl);
                //model.CurrentLobPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayCurrentLobCount", DMdl);
                //model.LaidInHomePaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayLaidInHomeCount", DMdl);
                //////Pending to lay
                //model.PendingToLayOtherPaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetOtherPaperPendingToLayListCount", DMdl);

                ////// Fixed and UnFixed Question Count For Starred And UnStarred Question type Created by Sunil on 02-July-2014
                //model.Starredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredFixedQuesCount", DMdl);
                //model.StarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredUnFixedQuesCount", DMdl);
                //model.Unstarredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredFixedQuesCount", DMdl);
                //model.UnstarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredUnFixedQuesCount", DMdl);

                tPaperLaidV mdl = new tPaperLaidV();
                SiteSettings siteSettingMod = new SiteSettings();
                mSession Mdl = new mSession();
                mAssembly assmblyMdl = new mAssembly();
                tMemberNotice model1 = new tMemberNotice();

                if (DMdl.AssemblyID != 0 && DMdl.SessionID != 0)
                {
                    mdl.AssemblyCode = DMdl.AssemblyID;
                    mdl.SessionCode = DMdl.SessionID;

                    //Add Value for Session object
                    Mdl.SessionCode = DMdl.SessionID;
                    Mdl.AssemblyID = DMdl.AssemblyID;
                    //Add Value for Assembly Object
                    assmblyMdl.AssemblyID = DMdl.AssemblyID;
                    // Add Value for tMemberNoticec Object
                    model1.AssemblyCode = DMdl.AssemblyID;
                    model1.SessionID = DMdl.SessionID;
                    model.AssemblyID = DMdl.AssemblyID;
                    model.SessionID = DMdl.SessionID;
                }
                else
                {
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    mdl.SessionCode = siteSettingMod.SessionCode;
                    mdl.AssemblyCode = siteSettingMod.AssemblyCode;
                    //Add Value for Session object
                    Mdl.SessionCode = siteSettingMod.SessionCode;
                    Mdl.AssemblyID = siteSettingMod.AssemblyCode;
                    //Add Value for Assembly Object
                    assmblyMdl.AssemblyID = siteSettingMod.AssemblyCode;
                    // Add Value for tMemberNoticec Object
                    model1.AssemblyCode = siteSettingMod.AssemblyCode;
                    model1.SessionID = siteSettingMod.SessionCode;
                    model.AssemblyID = siteSettingMod.AssemblyCode;
                    model.SessionID = siteSettingMod.SessionCode;
                    model.AccessFilePath = siteSettingMod.AccessFilePath;
                    CurrentSession.AssemblyId = siteSettingMod.AssemblyCode.ToString();
                    CurrentSession.SessionId = siteSettingMod.SessionCode.ToString();
                    CurrentSession.FileAccessPath = siteSettingMod.AccessFilePath;
                }

                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
                mdl = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "GetUpdatedOtherPaperLaidCounters", mdl);
                if (mdl != null)
                {
                    model.TotalUpdatedFileCounter = mdl.TotalOtherpaperLaidUpdated;
                }
                mDepartment deptInfo = new mDepartment();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID))
                {
                    deptInfo.deptId = CurrentSession.DeptID;
                    deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
                }

                if (deptInfo != null)
                {
                    model.DepartmentName = deptInfo.deptname;
                }

                model.CurrentUserName = CurrentSession.UserName;
                model.UserDesignation = CurrentSession.MemberDesignation;
                model.UserName = CurrentSession.Name;
                // added by Sameer
                // Get the Total count of All Type of question.
                //model1.QuestionTypeId = (int)QuestionType.StartedQuestion;
                //model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForProofReader", model1);
                //model.TotalStaredReceived = model1.TotalStaredReceived;
                //model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForClubbed", model1);
                //model.TotalStaredReceivedCl = model1.TotalStaredReceivedCl;
                //model.TotalSBracktedQuestion = model1.TotalSBracktedQuestion;
                //model.TotalInActiveQuestion = model1.TotalInActiveQuestion;
                //model.TotalClubbedQuestion = model1.TotalClubbedQuestion;
                //model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForUnStaredClubbed", model1);
                //model.TotalUnStaredReceivedCl = model1.TotalUnStaredReceivedCl;
                //model.TotalUBractedQuestion = model1.TotalUBractedQuestion;
                //model.UnstaredTotalClubbedQuestion = model1.UnstaredTotalClubbedQuestion;
                //model1.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                //model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForLegisUnstartQuestion", model1);
                //model.TotalUnStaredReceived = model1.TotalUnStaredReceived;
                //int c = model.TotalStaredReceived + model.TotalUnStaredReceived;

                //model.TotalValue = c;
                //int d = model.TotalStaredReceivedCl + model.TotalClubbedQuestion + model.TotalInActiveQuestion;
                //model.TotalValueCl = d;

                //// Add Code for Rejected and StarredToUnstarred Count By type Created by Sunil on 28-July-2014

                //model.StarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredRejectedCount", DMdl);
                //model.UnstarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredRejectedCount", DMdl);
                //model.StarredToUnstarredCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredToUnstarredCount", DMdl);
                //model.ChangedRuleNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetChangedRuleNoticeCount", DMdl);

                //model.CustomMessage = (string)TempData["CallingMethod"];
                //if ((string)TempData["CallingMethod"] == "StarredPending")
                //{
                //    ViewBag.TagID = "liStarred";
                //}
                //else if ((string)TempData["CallingMethod"] == "UnStarredPending")
                //{
                //    ViewBag.TagID = "liUnstarred";
                //}
                //else if ((string)TempData["CallingMethod"] == "NoticePending")
                //{
                //    ViewBag.TagID = "liNotice";
                //}
                //return View(model);


                ///////////////////CodeForDynamic///////////////////////////////////
                tUserAccessRequest requstModel = new tUserAccessRequest();
                requstModel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);
                //requstModel.UserID = new Guid(CurrentSession.UserID);
                mUsers usermdl = new mUsers();
                usermdl.UserId = new Guid(CurrentSession.UserID);
                usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
                if (usermdl.IsSecretoryId == true)
                {
                    requstModel.SecreataryId = Convert.ToInt16(usermdl.SecretoryId);
                }
                else if (usermdl.IsMember == "True")
                {
                    requstModel.MemberId = Convert.ToInt16(usermdl.UserName);
                }
                else if (usermdl.IsHOD == true)
                {
                    requstModel.HODId = Convert.ToInt16(usermdl.SecretoryId);
                }


                SubMenu = SubMenu.Trim();
                //Menu = Menu.Trim();
                //int Cnt = 0;
                if (SubMenu != null && SubMenu != "" && ModuleId > 0)
                {
                    if (ModuleId == 39)//Starred Questions
                    {
                        switch (SubMenu)
                        {
                            case "Assign For Typing":
                                {
                                    model1.QuestionTypeId = (int)QuestionType.StartedQuestion;
                                    model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForProofReader", model1);
                                    model.TotalStaredReceived = model1.TotalStaredReceived;
                                    return model.TotalStaredReceived;
                                }
                            case "Type Change":
                                {
                                    model.StarredToUnstarredCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredToUnstarredCount", DMdl);
                                    return model.StarredToUnstarredCount;//custom
                                }
                            case "Rejected Questions":
                                {
                                    model.StarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredRejectedCount", DMdl);
                                    return model.StarredRejectedCount;
                                }
                            case "Questions Pending":
                                {
                                    model.PendingStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPendingCount", DMdl);
                                    return model.PendingStarredQuestionCount;
                                }
                            case "Questions Sent":
                                {
                                    if (CustomSubMenuID == 1)
                                    {
                                        model.StarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredUnFixedQuesCount", DMdl);
                                        return model.StarredUnfixed;//custom
                                    }
                                    else if (CustomSubMenuID == 2)
                                    {
                                        model.Starredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredFixedQuesCount", DMdl);
                                        return model.Starredfixed;
                                    }
                                    else if (CustomSubMenuID == 3)
                                    {
                                        model.SentStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionSentCount", DMdl);
                                        return model.SentStarredQuestionCount;
                                    }
                                    else
                                    {
                                        return 0;
                                    }
                                }
                            case "Replies Received":
                                {
                                    model.RecievedStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperRecievedCount", DMdl);
                                    return model.RecievedStarredQuestionCount;
                                }
                            case "Upcoming LOB":
                                {
                                    model.CurrentLobStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperCurrentLobCount", DMdl);
                                    return model.CurrentLobStarredQuestionCount;
                                }
                            case "Laid In The House":
                                {
                                    model.LaidInHomeStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperLaidInHomeCount", DMdl);
                                    return model.LaidInHomeStarredQuestionCount;
                                }
                            case "Pending to Lay":
                                {
                                    model.PendingToLayStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperPendingToLayListCount", DMdl);
                                    return model.PendingToLayStarredQuestionCount;
                                }
                            case "Postpone Questions":
                                {
                                    return 0;//N/A
                                }
                            case "Bracketed / Clubbed":
                                {
                                    model1.QuestionTypeId = (int)QuestionType.StartedQuestion;
                                    model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForClubbed", model1);
                                    model.TotalStaredReceivedCl = model1.TotalStaredReceivedCl;
                                    model.TotalSBracktedQuestion = model1.TotalSBracktedQuestion;
                                    model.TotalInActiveQuestion = model1.TotalInActiveQuestion;
                                    model.TotalClubbedQuestion = model1.TotalClubbedQuestion;

                                    if (CustomSubMenuID == 4)
                                    {
                                        return model.TotalSBracktedQuestion;
                                    }
                                    else if (CustomSubMenuID == 5)
                                    {
                                        return model.TotalStaredReceivedCl;
                                    }

                                    else if (CustomSubMenuID == 6)
                                    {
                                        return model.TotalClubbedQuestion;
                                    }
                                    else
                                    {
                                        return 0;
                                    }
                                    //custom need to review this methods
                                }
                            case "Fixed Question's PDF":
                                {
                                    return 0;//N/A
                                }
                            case "Reports":
                                {
                                    return 0;//N/A
                                }
                        }
                    }
                    else if (ModuleId == 40)//Unstarred Questions
                    {
                        switch (SubMenu)
                        {
                            case "Assign For Typing":
                                {
                                    model1.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                                    model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForLegisUnstartQuestion", model1);
                                    model.TotalUnStaredReceived = model1.TotalUnStaredReceived;
                                    return model.TotalUnStaredReceived;
                                }
                            case "Rejected Questions":
                                {
                                    model.UnstarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredRejectedCount", DMdl);
                                    return model.UnstarredRejectedCount;
                                }
                            case "Questions Pending":
                                {
                                    model.PendingUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPendingCount", DMdl);
                                    return model.PendingUnStarredQuestionCount;
                                }
                            case "Questions Sent":
                                {
                                    if (CustomSubMenuID == 7)
                                    {
                                        model.SentUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionSentCount", DMdl);
                                        return model.SentUnStarredQuestionCount;//custom
                                    }
                                    else if (CustomSubMenuID == 8)
                                    {
                                        model.UnstarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredUnFixedQuesCount", DMdl);
                                        return model.UnstarredUnfixed;
                                    }
                                    else if (CustomSubMenuID == 9)
                                    {
                                        model.Unstarredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredFixedQuesCount", DMdl);
                                        return model.Unstarredfixed;
                                    }
                                    else
                                    {
                                        return 0;
                                    }
                                }

                            case "Replies Received":
                                {
                                    model.RecievedUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperRecievedCount", DMdl);
                                    return model.RecievedUnStarredQuestionCount;
                                }
                            case "Upcoming LOB":
                                {
                                    model.CurrentLobUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperCurrentLobCount", DMdl);
                                    return model.CurrentLobUnStarredQuestionCount;
                                }
                            case "Laid In The House":
                                {
                                    model.LaidInHomeUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperLaidInHomeCount", DMdl);
                                    return model.LaidInHomeUnStarredQuestionCount;
                                }
                            case "Pending to Lay":
                                {
                                    model.PendingToLayUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperPendingToLayListCount", DMdl);
                                    return model.PendingToLayUnStarredQuestionCount;
                                }
                            case "Postpone Questions":
                                {
                                    return 0;//N/A
                                }
                            case "Bracketed / Clubbed":
                                {
                                    model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForUnStaredClubbed", model1);
                                    model.TotalUnStaredReceivedCl = model1.TotalUnStaredReceivedCl;
                                    model.TotalUBractedQuestion = model1.TotalUBractedQuestion;
                                    model.UnstaredTotalClubbedQuestion = model1.UnstaredTotalClubbedQuestion;

                                    if (CustomSubMenuID == 10)
                                    {
                                        return model.TotalUBractedQuestion;
                                    }
                                    else if (CustomSubMenuID == 11)
                                    {
                                        return model.TotalUnStaredReceivedCl;
                                    }

                                    else if (CustomSubMenuID == 12)
                                    {
                                        return model.UnstaredTotalClubbedQuestion;
                                    }
                                    else
                                    {
                                        return 0;
                                    }
                                    //custom need to review this methods
                                }
                            case "Fixed Question's PDF":
                                {
                                    return 0;//N/A
                                }
                            case "Reports":
                                {
                                    return 0;//N/A
                                }


                        }

                    }
                    else if (ModuleId == 41)//Notices
                    {
                        switch (SubMenu)
                        {
                            case "Assign For Typing":
                                {
                                    model.PendingNoticesAssignCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeAssignPendingCount", DMdl);
                                    return model.PendingNoticesAssignCount;
                                }
                            case "Type Change":
                                {
                                    model.ChangedRuleNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetChangedRuleNoticeCount", DMdl);
                                    return model.ChangedRuleNoticeCount;//custom
                                }
                            case "Notices Pending":
                                {
                                    model.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCount", DMdl);
                                    return model.PendingNoticesCount;
                                }
                            case "Notices Sent":
                                {
                                    model.SentNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeSentCount", DMdl);
                                    return model.SentNoticesCount;
                                }
                            case "Replies Received":
                                {
                                    model.RecievedNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperRecievedCount", DMdl);
                                    return model.RecievedNoticesCount;
                                }
                            case "Upcoming LOB":
                                {
                                    model.CurrentLobNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperCurrentLobCount", DMdl);
                                    return model.CurrentLobNoticesCount;
                                }
                            case "Laid In The House":
                                {
                                    model.LaidInHomeNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperLaidInHomeCount", DMdl);
                                    return model.LaidInHomeNoticesCount;
                                }
                            case "Pending to Lay":
                                {
                                    model.PendingToLayNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperPendingToLayListCount", DMdl);
                                    return model.PendingToLayNoticeCount;
                                }
                            case "Notices List":
                                {
                                    return 0;//N/A
                                }
                        }
                    }
                    else if (ModuleId == 42)//Bills
                    {
                        switch (SubMenu)
                        {
                            case "Bills Received":
                                {
                                    model.RecievedBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperRecievedCount", DMdl);
                                    return model.RecievedBillsCount;
                                }
                            case "Upcoming LOB":
                                {
                                    model.CurrentLobBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperCurrentLobCount", DMdl);
                                    return model.CurrentLobBillsCount;
                                }

                            case "Laid In The House":
                                {
                                    model.LaidInHomeBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperLaidInHomeCount", DMdl);
                                    return model.LaidInHomeBillsCount;
                                }

                            case "Pending to Lay":
                                {
                                    model.PendingToLayBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperPendingToLayListCount", DMdl);
                                    return model.PendingToLayBillsCount;
                                }
                        }
                    }
                    else if (ModuleId == 43)//Committees
                    {
                        switch (SubMenu)
                        {
                            case "Reports Received":
                                {
                                    model.RecievedCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperRecievedCount", DMdl);
                                    return model.RecievedCommitteePaperCount;
                                }
                            case "Upcoming LOB":
                                {
                                    model.CurrentLobCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperCurrentLobCount", DMdl);
                                    return model.CurrentLobCommitteePaperCount;
                                }

                            case "Laid In The House":
                                {
                                    model.LaidInHomeCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperLaidInHomeCount", DMdl);
                                    return model.LaidInHomeCommitteePaperCount;
                                }

                            case "Pending to Lay":
                                {
                                    model.PendingToLayCommitteeCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperPendingToLayListCount", DMdl);
                                    return model.PendingToLayCommitteeCount;
                                }
                        }
                    }
                    else if (ModuleId == 44)//Other Papers 
                    {
                        switch (SubMenu)
                        {
                            case "Papers Received":
                                {
                                    model.RecievedPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayRecievedCount", DMdl);
                                    return model.RecievedPaperToLayCount;
                                }
                            case "Upcoming LOB":
                                {
                                    model.CurrentLobPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayCurrentLobCount", DMdl);
                                    return model.CurrentLobPaperToLayCount;
                                }
                            case "Laid In The House":
                                {
                                    model.LaidInHomePaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayLaidInHomeCount", DMdl);
                                    return model.LaidInHomePaperToLayCount;
                                }
                            case "Pending to Lay":
                                {
                                    model.PendingToLayOtherPaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetOtherPaperPendingToLayListCount", DMdl);
                                    return model.PendingToLayOtherPaperCount;
                                }
                        }
                    }
                    else if (ModuleId == 45)//Miscellaneous
                    {
                        switch (SubMenu)
                        {
                            case "Updated LOB Papers":
                                {
                                    model1.QuestionTypeId = (int)QuestionType.StartedQuestion;
                                    model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForClubbed", model1);
                                    model.TotalStaredReceivedCl = model1.TotalStaredReceivedCl;
                                    return model.TotalStaredReceivedCl;
                                }
                        }
                    }

                    else if (ModuleId == 12)//Access Control
                    {
                        switch (SubMenu)
                        {
                            case "Pending Request":
                                {
                                    var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetPendingUserRequestCount", requstModel);
                                    return result.Count;
                                }
                            case "Accepted Request":
                                {
                                    var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetAcceptedUserRequestCount", requstModel);
                                    return result.Count;
                                }
                            case "Rejected Request":
                                {
                                    var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetRejectedUserRequestCount", requstModel);
                                    return result.Count;
                                }
                        }
                    }

                }
                ///////////////////EndCodeForDynamic///////////////////////////////////
            }
            else
            {
                return Cnt;
            }

            return Cnt;
        }

        public static int GetTotalCountBySubMenuForVidhanSabhaDept_OtherUser(this string SubMenu, int ModuleId, int CustomSubMenuID)
        {
            if (ModuleId==39||ModuleId==40)
            {
               return GetTotalCountBySubMenuForVidhanSabhaDept(SubMenu, ModuleId, CustomSubMenuID);
            }
            int Cnt = 0;
            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                SubMenu = SubMenu.Trim();
                //Menu = Menu.Trim();
                //int Cnt = 0;
                if (SubMenu != null && SubMenu != "" && ModuleId > 0)
                {
                    if(ModuleId == 78) //Added By robin for house committee count
                    {
                        switch (SubMenu)
                        {
                            case "Chief Reporter":
                                {
                                    string UserID = CurrentSession.UserID;
                                    string[] strngBID = new string[2];
                                    strngBID[0] = CurrentSession.BranchId;
                                    var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
                                    string ComitteeId = value[0];
                                    var cprocedding = (List<COmmitteeProceeding>)Helper.ExecuteService("eFile", "GetCommitteeProoceeding_ByCommittee", ComitteeId);
                                    return cprocedding.Count;
                                }
                            case "Received Papers":
                                {
                                    string DepartmentId = CurrentSession.DeptID;
                                    string Officecode = CurrentSession.OfficeId;
                                    string AadharId = CurrentSession.AadharId;
                                    string currtBId = CurrentSession.BranchId;
                                    string Year = "5";
                                    string papers = "1";
                                    string[] strngBIDN = new string[2];
                                    strngBIDN[0] = CurrentSession.BranchId;
                                    var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
                                    string[] str = new string[6];
                                    str[0] = DepartmentId;
                                    str[1] = Officecode;
                                    str[2] = AadharId;
                                    str[3] = value123[0];
                                    str[4] = Year;
                                    str[5] = papers;
                                    var ReceivedList = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
                                    return ReceivedList.Count;
                                }
                            case "Draft Papers":
                                {
                                    string DepartmentId = CurrentSession.DeptID;
                                    string Officecode = CurrentSession.OfficeId;
                                    string AadharId = CurrentSession.AadharId;
                                    string currtBId = CurrentSession.BranchId;
                                    string[] strD = new string[6];
                                    strD[0] = DepartmentId;
                                    strD[1] = Officecode;
                                    strD[2] = AadharId;
                                    strD[3] = currtBId;
                                    strD[4] = CurrentSession.Designation;
                                    strD[5] = "1";
                                    var DraftListCount = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
                                    return DraftListCount.Count;
                                }
                            case "Sent Papers":
                                {
                                    string DepartmentId = CurrentSession.DeptID;
                                    string Officecode = CurrentSession.OfficeId;
                                    string AadharId = CurrentSession.AadharId;
                                    string currtBId = CurrentSession.BranchId;
                                    string[] strS = new string[6];
                                    strS[0] = DepartmentId;
                                    strS[1] = Officecode;
                                    strS[2] = AadharId;
                                    strS[3] = currtBId;
                                    strS[4] = "5";
                                    strS[5] = "1";
                                    List<eFileAttachment> ListModelForSend = new List<eFileAttachment>();
                                    ListModelForSend = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendList", strS);
                                    return ListModelForSend.Count;
                                }
                        }
                    }
                    if (ModuleId == 50)//Diary
                    {
                        switch (SubMenu)
                        {
                            case "Starred Questions":
                                {
                                    DiaryModel objDiaryModel = new DiaryModel();
                                    DiaryModel objDiaryModel2 = new DiaryModel();
                                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
                                    {
                                        objDiaryModel.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                                        objDiaryModel.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                                    }
                                    objDiaryModel2 = (DiaryModel)Helper.ExecuteService("Diary", "GetInitialCount", objDiaryModel);
                                    if (objDiaryModel2 != null)
                                    {
                                        if (CustomSubMenuID == 1)
                                        {
                                            objDiaryModel = objDiaryModel2;
                                            return objDiaryModel.SCount;
                                        }
                                        else if (CustomSubMenuID == 2)
                                        {
                                            objDiaryModel = objDiaryModel2;
                                            return objDiaryModel.SPPCnt;
                                        }
                                        else
                                        {
                                            return 0;
                                        }

                                    }
                                    else
                                    {
                                        return 0;
                                    }
                                }
                            case "Unstarred Questions":
                                {
                                    DiaryModel objDiaryModel = new DiaryModel();
                                    DiaryModel objDiaryModel2 = new DiaryModel();
                                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
                                    {
                                        objDiaryModel.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                                        objDiaryModel.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                                    }
                                    objDiaryModel2 = (DiaryModel)Helper.ExecuteService("Diary", "GetInitialCount", objDiaryModel);
                                    if (objDiaryModel2 != null)
                                    {
                                        if (objDiaryModel2 != null)
                                        {
                                            if (CustomSubMenuID == 1)
                                            {
                                                objDiaryModel = objDiaryModel2;
                                                return objDiaryModel.UCount;
                                            }
                                            else if (CustomSubMenuID == 2)
                                            {
                                                objDiaryModel = objDiaryModel2;
                                                return objDiaryModel.UPPCnt;
                                            }
                                            else
                                            {
                                                return 0;
                                            }

                                        }
                                        else
                                        {
                                            return 0;
                                        }
                                    }
                                    else
                                    {
                                        return 0;
                                    }
                                }
                            case "Notices":
                                {
                                    DiaryModel objDiaryModel = new DiaryModel();
                                    DiaryModel objDiaryModel2 = new DiaryModel();
                                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
                                    {
                                        objDiaryModel.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                                        objDiaryModel.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                                    }
                                    objDiaryModel2 = (DiaryModel)Helper.ExecuteService("Diary", "GetInitialCount", objDiaryModel);
                                    if (objDiaryModel2 != null)
                                    {
                                        objDiaryModel = objDiaryModel2;
                                        return objDiaryModel.NCount;
                                    }
                                    else
                                    {
                                        return 0;
                                    }

                                }
                        }
                    }


                    if (ModuleId == 53)//typist
                    {
                        switch (SubMenu)
                        {
                            case "Starred Questions":
                                {
                                    tMemberNotice model = new tMemberNotice();
                                    model.UId = CurrentSession.UserID;
                                    model.RoleName = CurrentSession.RoleName;
                                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
                                    {
                                        model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                                        model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                                    }
                                    model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForQuestionTypes", model);
                                    return model.TotalStaredReceived;

                                }
                            case "Unstarred Questions":
                                {
                                    tMemberNotice model = new tMemberNotice();
                                    model.UId = CurrentSession.UserID;
                                    model.RoleName = CurrentSession.RoleName;
                                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
                                    {
                                        model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                                        model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                                    }
                                    model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForQuestionTypes", model);
                                    return model.TotalUnStaredReceived;
                                }
                            case "Notices":
                                {
                                    tMemberNotice model = new tMemberNotice();
                                    model.UId = CurrentSession.UserID;
                                    model.RoleName = CurrentSession.RoleName;
                                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
                                    {
                                        model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                                        model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                                    }
                                    model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForNotrices", model);
                                    return model.TotalNoticesReceived;
                                }
                        }

                    }

                    if (ModuleId == 54)//ProofReader
                    {
                        switch (SubMenu)
                        {
                            case "Starred Questions":
                                {
                                    tMemberNotice model = new tMemberNotice();
                                    model.UId = CurrentSession.UserID;
                                    model.RoleName = CurrentSession.RoleName;
                                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
                                    {
                                        model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                                        model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                                    }
                                    model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForQuestionTypes", model);
                                    return model.TotalStaredReceived;

                                }
                            case "Unstarred Questions":
                                {
                                    tMemberNotice model = new tMemberNotice();
                                    model.UId = CurrentSession.UserID;
                                    model.RoleName = CurrentSession.RoleName;
                                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
                                    {
                                        model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                                        model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                                    }
                                    model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForQuestionTypes", model);
                                    return model.TotalUnStaredReceived;
                                }

                        }

                    }

                    if (ModuleId == 56)//Translator
                    {
                        switch (SubMenu)
                        {
                            case "Starred Questions":
                                {
                                    tMemberNotice model = new tMemberNotice();
                                    model.RoleName = CurrentSession.RoleName;
                                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                                    {
                                        model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                                    }
                                    if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                                    {
                                        model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                                    }
                                    model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStarredAndUnstarredCnt", model);
                                    return model.TotalStaredReceived;
                                }
                            case "Unstarred Questions":
                                {
                                    tMemberNotice model = new tMemberNotice();
                                    model.RoleName = CurrentSession.RoleName;
                                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                                    {
                                        model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                                    }
                                    if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                                    {
                                        model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                                    }
                                    model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStarredAndUnstarredCnt", model);
                                    return model.TotalUnStaredReceived;
                                }
                        }
                    }


                }

            }
            else
            {
                return Cnt;
            }

            return Cnt;
        }


        #endregion Added Ashwani code for Vidhan Sabha Dynamic Counting

        #region Print Option For Department
        public static List<mPassModel> ToModelListWithXML(this List<mPasses> entityList)
        {
            var ModelList = entityList.Select(entity => new mPassModel()
            {
                AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                Email = entity.Email,
                FatherName = entity.FatherName,
                GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,
                MobileNo = entity.MobileNo,
                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                RequestedPassCategoryID = entity.RequestedPassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                Photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                Status = entity.Status,
                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                ApprovedPassCategoryID = entity.ApprovedPassCategoryID,
                PassCategoryName = GetPassCategoryNameByID(entity.ApprovedPassCategoryID),
                PassCategoryTemplate = GetPassCategoryTemplateByID(entity.ApprovedPassCategoryID),
                xmlDocument = XMLForQRCode(entity)
            });
            return ModelList.ToList();
        }
        public static string XMLForQRCode(mPasses model)
        {
            //XElement passParentNode = new System.Xml.Linq.XElement("PassDetails");

            XElement passParentNode = new System.Xml.Linq.XElement("PassCode");

            XElement passChildChildNode1 = new System.Xml.Linq.XElement("Name");
            passParentNode.Add(passChildChildNode1);

            passParentNode.SetAttributeValue("ID", model.PassCode);

            string information = model.Name + (model.Gender + "," + Convert.ToString(model.Age) + " Years");

            passChildChildNode1.ReplaceNodes(model.Name != null ? information : "");

            return passParentNode.ToString();
        }
        public static string GetPassCategoryTemplateByID(int passCategoryID)
        {
            if (ListPassCategoryList.Count == 0)
            {
                ListPassCategoryList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
            }

            var passCategoryModel = (PassCategory)ListPassCategoryList.Where(m => m.PassCategoryID == passCategoryID).FirstOrDefault();

            if (passCategoryModel != null)
            {
                return passCategoryModel.Template;
            }
            return "";
        }
        public static PassesViewModel ToModelmPass(this mPasses entity)
        {
            var Model = new PassesViewModel()
            {
                PassID = entity.PassID,
                PassCode = entity.PassCode,
                AssemblyCode = entity.AssemblyCode,
                SessionCode = entity.SessionCode,
                AadharID = entity.AadharID,
                Prefix = entity.Prefix,
                Name = entity.Name,
                Gender = entity.Gender,
                Age = entity.Age,
                FatherName = entity.FatherName,
                NoOfPersions = entity.NoOfPersions,
                RecommendationType = entity.RecommendationType,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                MobileNo = entity.MobileNo,
                Email = entity.Email,
                Address = entity.Address,
                Photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                OrganizationName = entity.OrganizationName,
                Designation = entity.Designation,
                DayTime = entity.DayTime,
                Time = entity.Time,
                DepartmentID = entity.DepartmentID,
                SessionDateFrom = entity.SessionDateFrom,
                SessionDateTo = entity.SessionDateTo,
                ApprovedDate = entity.ApprovedDate,
                ApprovedBy = entity.ApprovedBy,
                VehicleNumber = entity.VehicleNumber,
                GateNumber = entity.GateNumber,
                IsRequested = entity.IsRequested,
                RequestedDate = entity.RequestedDate,
                Status = entity.Status,
                RequestedBy = entity.RequestedBy,
                IsActive = entity.IsActive,
                PassCategoryName = GetPassCategoryNameByID(entity.ApprovedPassCategoryID),
                RecommendationByName = GetMemberNameByCode(entity.RecommendationBy)
            };
            return Model;
        }
        public static List<PassesViewModel> TomPassModelList(this List<mPasses> entityList)
        {
            var ModelList = entityList.Select(entity => new PassesViewModel()
            {
                PassID = entity.PassID,
                PassCode = entity.PassCode,
                PassCategoryID = entity.ApprovedPassCategoryID,
                AssemblyCode = entity.AssemblyCode,
                SessionCode = entity.SessionCode,
                AadharID = entity.AadharID,
                Prefix = entity.Prefix,
                Name = entity.Name,
                Gender = entity.Gender,
                Age = entity.Age,
                FatherName = entity.FatherName,
                NoOfPersions = entity.NoOfPersions,
                RecommendationType = entity.RecommendationType,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                MobileNo = entity.MobileNo,
                Email = entity.Email,
                Address = entity.Address,
                Photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                OrganizationName = entity.OrganizationName,
                Designation = entity.Designation,
                DayTime = entity.DayTime,
                Time = entity.Time,
                DepartmentID = entity.DepartmentID,
                SessionDateFrom = entity.SessionDateFrom,
                SessionDateTo = entity.SessionDateTo,
                ApprovedDate = entity.ApprovedDate,
                ApprovedBy = entity.ApprovedBy,
                VehicleNumber = entity.VehicleNumber,
                GateNumber = entity.GateNumber,
                IsRequested = entity.IsRequested,
                RequestedDate = entity.RequestedDate,
                Status = entity.Status,
                RequestedBy = entity.RequestedBy,
                IsActive = entity.IsActive,
                IsApproved = entity.IsApproved,
                RequestdID = entity.RequestID,
                PassCategoryName = GetPassCategoryNameByID(entity.ApprovedPassCategoryID)
            });
            return ModelList.ToList();
        }
        #endregion
    }
}