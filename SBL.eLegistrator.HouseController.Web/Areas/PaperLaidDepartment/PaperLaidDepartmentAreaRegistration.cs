﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment
{
    public class PaperLaidDepartmentAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PaperLaidDepartment";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PaperLaidDepartment_default",
                "PaperLaidDepartment/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
