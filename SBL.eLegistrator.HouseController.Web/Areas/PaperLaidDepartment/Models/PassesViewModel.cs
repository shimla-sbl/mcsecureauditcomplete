﻿using SBL.DomainModel.Models.Passes;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models;
using SBL.eLegistrator.HouseController.Web.Areas.Reporters.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Models
{
    public class PassesViewModel : PhotoCaptureGenericModel
    {
        public PassesViewModel()
        {
            this.ListSessionDate = new List<mSessionDate>();
            this.ListPasses = new List<PassesViewModel>();
            this.mSessionDateList = new List<mSessionDateModel>();
            this.ListPassCategory = new List<PassCategory>();
        }

        public int PassID { get; set; }

        public Int32? PassCode { get; set; }

        public int PassCategoryID { get; set; }

        public int AssemblyCode { get; set; }

        public int SessionCode { get; set; }

        [Required(ErrorMessage = "Aadhar ID is Required")]
        public string AadharID { get; set; }

        public string Prefix { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Gender is Required")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Age is Required")]
        public int? Age { get; set; }

        [Required(ErrorMessage = "Father Name is Required")]
        public string FatherName { get; set; }

        public int? NoOfPersions { get; set; }

        public string RecommendationType { get; set; }

        public string RecommendationBy { get; set; }

        public string RecommendationDescription { get; set; }

        [Required(ErrorMessage = "Mobile Number is Required")]
        [RegularExpression(@"^[0-9\.\+\-\/]+$", ErrorMessage = "Invalid phone number")]
        [StringLength(10, ErrorMessage = "Maximum 10 characters allowed")]
        public string MobileNo { get; set; }

        //[Required(ErrorMessage = "Email is Required")]
        [EmailAddress(ErrorMessage = "Invalid Email")]
        // [StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Address is Required")]
        public string Address { get; set; }

        //[Required(ErrorMessage = "Photo is Required")]
        //public string Photo { get; set; }

        //public string PhotoName { get; set; }


        public string OrganizationName { get; set; }

        [Required(ErrorMessage = "Designation & Address is Required")]
        public string Designation { get; set; }

        [Required(ErrorMessage = "This Feild is Required")]
        public string DayTime { get; set; }

        public TimeSpan? Time { get; set; }
        [Required(ErrorMessage = "Department is required")]
        public string DepartmentID { get; set; }

        [Required(ErrorMessage = "Session Date From is Required")]
        public String SessionDateFrom { get; set; }

        [Required(ErrorMessage = "Session Date To is Required")]
        public String SessionDateTo { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public string ApprovedBy { get; set; }

        public string VehicleNumber { get; set; }

        public string GateNumber { get; set; }

        public bool IsRequested { get; set; }

        public DateTime? RequestedDate { get; set; }

        public int? Status { get; set; }

        public string RequestedBy { get; set; }

        public string Mode { get; set; }

        public bool IsActive { get; set; }

        public string PassTypeName { get; set; }

        public SelectList GenderList { get; set; }


        public List<mSessionDate> ListSessionDate { get; set; }

        public SelectList TimeTypeList { get; set; }

        public List<mSessionDateModel> mSessionDateList { get; set; }

        [NotMapped]
        public int ValidateSessionID { get; set; }

        [NotMapped]
        public bool IsSessionTransferID { get; set; }

        public string RequestdID { get; set; }

        public string IsRequestUserID { get; set; }

        public string ErrorMessage { get; set; }

        public List<PassesViewModel> ListPasses { get; set; }

        public List<PassCategory> ListPassCategory { get; set; }

        public string PassCategoryName { get; set; }

        public List<SBL.DomainModel.Models.Department.mDepartment> mDepartment { get; set; }

        public List<SBL.DomainModel.Models.Department.mDepartment> mDepartmentList { get; set; }

        public bool IsApproved { get; set; }

        public string RecommendationByName { get; set; }

        public string IsDepartmentPrint { get; set; }

    }
}