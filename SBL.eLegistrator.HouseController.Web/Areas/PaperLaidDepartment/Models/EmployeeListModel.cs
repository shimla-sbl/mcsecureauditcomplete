﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.ComplexModel.UserComplexModel;
namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Models
{
    [Serializable]
    public class EmployeeListModel
    {
        public List<UserComplexModel> EmployeeList { get; set; }

        public List<UnAuthorizedEmpComplexModel> UnAuthEmploylist { get; set; }


        public int PAGE_SIZE { get; set; }

        public int PageIndex { get; set; }

        public int ResultCount { get; set; }

        public int loopStart { get; set; }

        public int loopEnd { get; set; }

        public int selectedPage { get; set; }

        public int PageNumber { get; set; }

        public int RowsPerPage { get; set; }

        public string DeptId { get; set; }

        public string UserId { get; set; }
    }
}