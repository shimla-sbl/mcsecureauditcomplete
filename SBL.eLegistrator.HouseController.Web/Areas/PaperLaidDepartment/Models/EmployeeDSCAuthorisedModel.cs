﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.User;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Models
{
    [Serializable]
    public class EmployeeDSCAuthorisedModel
    {
        public int SecretariatId { get; set; }

        public string DeptId { get; set; }

        public List<SelectListItem> DepartmentList { get; set; }

       // public EmployeeListModel EmployeeList { get; set; }

       
       

    }
}