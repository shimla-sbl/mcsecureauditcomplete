﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Models
{
    [Serializable]
    public class DepartmentEmployeeList
    {
        public DepartmentEmployeeList()
        {
            AuthorisedEmployeList = new EmployeeListModel();
            UnAuthorisedEmployeList = new EmployeeListModel();
        }

        

        public EmployeeListModel AuthorisedEmployeList { get; set; }

        public EmployeeListModel UnAuthorisedEmployeList { get; set; }

    }
}