﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Minister
{
    public class CommitteeChairpersonAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CommitteeChairperson";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CommitteeChairperson_default",
                "CommitteeChairperson/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
