﻿using SBL.DomainModel.Models.Passes;
using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Models;
using System;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.PassVerification.Extensions;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.PassVerification.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class PassVerificationController : Controller
    {
        //
        // GET: /PassVerification/PassVerification/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetPassCodeDetails(Int32 PassCode)
        {
            PassesViewModel viewModel = new PassesViewModel();
          //  mPasses pass = new mPasses();
            //pass.PassCode = PassCode;
            SBL.DomainModel.Models.SiteSetting.SiteSettings setting = new SBL.DomainModel.Models.SiteSetting.SiteSettings();
            setting = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helpers.Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", setting);
            viewModel.PassCode = PassCode;
            viewModel.SessionCode = setting.SessionCode;
            viewModel.AssemblyCode = setting.AssemblyCode;
            var mPasses = (mPasses)Helpers.Helper.ExecuteService("Pass", "GetDetailsByPassCode", viewModel.ToEntity());
            if (mPasses != null)
            {
                var newPasses = mPasses.ToModel();
                return PartialView("_PassCodeDetails", newPasses);
            }
            else {
                viewModel = new PassesViewModel();
                viewModel.SessionDateFrom = null;
                return PartialView("_PassCodeDetails", viewModel);
            }
            
        }
    }
}
