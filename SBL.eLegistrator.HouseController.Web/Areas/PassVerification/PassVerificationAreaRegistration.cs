﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PassVerification
{
    public class PassVerificationAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PassVerification";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PassVerification_default",
                "PassVerification/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
