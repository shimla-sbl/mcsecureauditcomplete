﻿using SBL.DomainModel.Models.Passes;
using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.PassVerification.Extensions
{
    public static class Extensions
    {
        public static mPasses ToEntity(this PassesViewModel Model)
        {
            mPasses entity = new mPasses()
            {

                //Address = Model.Address,
                //Age = Model.Age,
                AssemblyCode = Model.AssemblyCode,
                //DayTime = Model.DayTime,
                //Email = Model.Email,
                //FatherName = Model.FatherName,

                //Gender = Model.Gender,
                //IsActive = Model.IsActive,
             

                //Name = Model.Name,
                //NoOfPersions = Model.NoOfPersions,
                //OrganizationName = Model.OrganizationName,
                //PassCategoryID = Model.PassCategoryID,
                PassCode = Model.PassCode,
                //PassID = Model.PassID,
               // Photo = Model.Photo,
               // Prefix = Model.Prefix,

                SessionCode = Model.SessionCode,
               // SessionDateTo = Model.SessionDateTo,
              //  SessionDateFrom = Model.SessionDateFrom,
              //  Time = (TimeSpan)Model.Time,




            };
            return entity;
        }

        public static PassesViewModel ToModel(this mPasses entity)
        {
            var Model = new PassesViewModel()
            {

                Address = entity.Address,
                Age = entity.Age,

                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,

                Email = entity.Email,
                FatherName = entity.FatherName,

                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,

                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                PassCategoryID = entity.RequestedPassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                Photo = entity.Photo,
                Prefix = entity.Prefix,

                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,

                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,


                PassTypeName = GetPassTypeName(entity.RequestedPassCategoryID)
            };
            return Model;
        }
        public static string GetPassTypeName(int PassCategoryId)
        {
            return (string)Helper.ExecuteService("Pass", "GetPassCategaryName", new mPasses { RequestedPassCategoryID = PassCategoryId });
            //return null;
        }

    }
}