﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Reporters.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System.Data;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.BillWorks.Controllers
{
    [SBLAuthorize(Allow = "Authenticated")]
    [Audit]
    [NoCache]
    public class BillWorksController : Controller
    {
        //
        // GET: /BillWorks/BillWorks/

        public ActionResult Index()
        {
            CurrentSession.SetSessionAlive = null;
            mBills model = new mBills();

            model = (mBills)Helper.ExecuteService("BillWorks", "GetBills", model);

            return View(model);
        }
        public ActionResult ListBillls()
        {
            return PartialView("ListBillls");
        }
        public ActionResult GetDataID(int ID)
        {
            string CurrAssembly = "";

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId as string))
            {
                CurrAssembly = CurrentSession.AssemblyId as string;
            }
            else
            {
                List<KeyValuePair<string, string>> methodParameterAss = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameterAss);

                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }

                }
            }
            string Month = "";
            string Day = "";
            string Year = "";
            mBills model = new mBills();

            int Id = Convert.ToInt32(ID);
            model.ID = ID;
            model = (mBills)Helper.ExecuteService("BillWorks", "GetBillsDetails", model);
            foreach (var item in model.BillWorkModel)
            {
                model.IsFreeze = item.IsFreeze;
                model.ID = item.ID;
                model.BillTitle = item.BillTitle;
                if (item.BillNo != "")
                {
                    model.BillNo = item.BillNo;
                    string[] parts = model.BillNo.Split('o', 'f');
                    string fist = parts[0].ToString();
                    string Second = parts[2].ToString();
                    model.BillYearS = Convert.ToInt32(Second);
                    model.BillNoS = Convert.ToInt32(fist);
                }
                //model.IntroductionDate = item.IntroductionDate;
                model.PassingDate = item.PassingDate;
                model.AssesntDate = item.AssesntDate;

                if (!String.IsNullOrEmpty(item.ActNo))//item.ActNo != null)
                {
                    model.ActNo = item.ActNo;
                    string[] Acts = model.ActNo.Split('o', 'f');
                    string fistAct = Acts[0].ToString();
                    string SecondYear = Acts[2].ToString();
                    model.ActYearS = Convert.ToInt32(SecondYear);
                    model.ActNoS = Convert.ToInt32(fistAct);
                }
                model.IntroductionFilePath = item.IntroductionFilePath;
                model.PassingFilePath = item.PassingFilePath;
                model.AccentedFilePath = item.AccentedFilePath;
                model.ActFilePath = item.ActFilePath;
                model.AssemblyId = item.AssemblyId;
                model.SessionId = item.SessionId;
                model.AssentedBy = item.AssentedBy;
                model.PublishedDate = item.PublishedDate;
                if (item.IntroductionDate != null)
                {
                    string SDate = Convert.ToString(item.IntroductionDate.Value);

                    SDate = Convert.ToString(item.IntroductionDate.Value);
                    if (SDate != "")
                    {
                        if (SDate.IndexOf("/") > 0)
                        {
                            Month = SDate.Substring(0, SDate.IndexOf("/"));
                            if (Month.Length < 2)
                            {
                                Month = "0" + Month;
                            }
                            Day = SDate.Substring(SDate.IndexOf("/") + 1, (SDate.LastIndexOf("/") - SDate.IndexOf("/") - 1));
                            Year = SDate.Substring(SDate.LastIndexOf("/") + 1, 4);
                            SDate = Day + "/" + Month + "/" + Year;
                            model.IDate = SDate;
                        }
                        if (SDate.IndexOf("-") > 0)
                        {
                            Month = SDate.Substring(0, SDate.IndexOf("-"));
                            Day = SDate.Substring(SDate.IndexOf("-") + 1, (SDate.LastIndexOf("-") - SDate.IndexOf("-") - 1));
                            Year = SDate.Substring(SDate.LastIndexOf("-") + 1, 4);
                            SDate = Day + "/" + Month + "/" + Year;
                            model.IDate = SDate;
                        }
                    }
                }
                if (item.PassingDate != null)
                {
                    string PDate = Convert.ToString(item.PassingDate.Value);

                    PDate = Convert.ToString(item.PassingDate.Value);
                    if (PDate != "")
                    {
                        if (PDate.IndexOf("/") > 0)
                        {
                            Month = PDate.Substring(0, PDate.IndexOf("/"));
                            if (Month.Length < 2)
                            {
                                Month = "0" + Month;
                            }
                            Day = PDate.Substring(PDate.IndexOf("/") + 1, (PDate.LastIndexOf("/") - PDate.IndexOf("/") - 1));
                            Year = PDate.Substring(PDate.LastIndexOf("/") + 1, 4);
                            PDate = Day + "/" + Month + "/" + Year;
                            model.PDate = PDate;
                        }
                        if (PDate.IndexOf("-") > 0)
                        {
                            Month = PDate.Substring(0, PDate.IndexOf("-"));
                            Day = PDate.Substring(PDate.IndexOf("-") + 1, (PDate.LastIndexOf("-") - PDate.IndexOf("-") - 1));
                            Year = PDate.Substring(PDate.LastIndexOf("-") + 1, 4);
                            PDate = Day + "/" + Month + "/" + Year;
                            model.PDate = PDate;
                        }
                    }
                }
                if (item.AssesntDate != null)
                {
                    string ADate = Convert.ToString(item.AssesntDate.Value);

                    ADate = Convert.ToString(item.AssesntDate.Value);
                    if (ADate != "")
                    {
                        if (ADate.IndexOf("/") > 0)
                        {
                            Month = ADate.Substring(0, ADate.IndexOf("/"));
                            if (Month.Length < 2)
                            {
                                Month = "0" + Month;
                            }
                            Day = ADate.Substring(ADate.IndexOf("/") + 1, (ADate.LastIndexOf("/") - ADate.IndexOf("/") - 1));
                            Year = ADate.Substring(ADate.LastIndexOf("/") + 1, 4);
                            ADate = Day + "/" + Month + "/" + Year;
                            model.ADate = ADate;
                        }
                        if (ADate.IndexOf("-") > 0)
                        {
                            Month = ADate.Substring(0, ADate.IndexOf("-"));
                            Day = ADate.Substring(ADate.IndexOf("-") + 1, (ADate.LastIndexOf("-") - ADate.IndexOf("-") - 1));
                            Year = ADate.Substring(ADate.LastIndexOf("-") + 1, 4);
                            ADate = Day + "/" + Month + "/" + Year;
                            model.ADate = ADate;
                        }
                    }
                }


                if (item.PublishedDate != null)
                {
                    string PubDate = Convert.ToString(item.PublishedDate.Value);

                    PubDate = Convert.ToString(item.PublishedDate.Value);
                    if (PubDate != "")
                    {
                        if (PubDate.IndexOf("/") > 0)
                        {
                            Month = PubDate.Substring(0, PubDate.IndexOf("/"));
                            if (Month.Length < 2)
                            {
                                Month = "0" + Month;
                            }
                            Day = PubDate.Substring(PubDate.IndexOf("/") + 1, (PubDate.LastIndexOf("/") - PubDate.IndexOf("/") - 1));
                            Year = PubDate.Substring(PubDate.LastIndexOf("/") + 1, 4);
                            PubDate = Day + "/" + Month + "/" + Year;
                            model.PubDate = PubDate;
                        }
                        if (PubDate.IndexOf("-") > 0)
                        {
                            Month = PubDate.Substring(0, PubDate.IndexOf("-"));
                            Day = PubDate.Substring(PubDate.IndexOf("-") + 1, (PubDate.LastIndexOf("-") - PubDate.IndexOf("-") - 1));
                            Year = PubDate.Substring(PubDate.LastIndexOf("-") + 1, 4);
                            PubDate = Day + "/" + Month + "/" + Year;
                            model.PubDate = PubDate;
                        }
                    }
                }

            }
            model.AssemblyList = Helper.ExecuteService("BillWorks", "GetAssemblySessionList", null) as List<BillWorkModel>;
            model.SessionLt = Helper.ExecuteService("BillWorks", "GetSessionsByAssemblyID", model) as List<BillWorkModel>;

            var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
            //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

            //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
            string FileStructurePath = Acess.SettingValue;
            model.FileStructurePath = FileStructurePath + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue;
            List<SelectListItem> years = new List<SelectListItem>();
            int currentYear = DateTime.Now.Year;

            for (int i = currentYear - 70; i < currentYear; i++)
            {
                SelectListItem year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };

                years.Add(year);
            }

            for (int i = currentYear; i < currentYear + 5; i++)
            {
                SelectListItem year = new SelectListItem();
                if (i == DateTime.Now.Year)
                {
                    year = new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = true };
                }
                else
                {
                    year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };
                }
                years.Add(year);
            }
            //for (int i = currentYear ; i < currentYear + 1; i++)
            //{
            //    SelectListItem year = new SelectListItem { Text = i.ToString("Select"), Value = i.ToString("0") };

            //    years.Add(year);
            //}
            model.yearList = years;
            model.BillNumberYear = model.BillYearS;
            model.ActYearS = model.ActYearS;            
            return PartialView("EditFormBills", model);
        }


        //[HttpGet]
        //public JsonResult GetDepartmentByMinistery(int MinistryId)
        //{
        //    tMemberNotice noticeModel = new tMemberNotice();
        //    mMinistry model = new mMinistry();
        //    model.MinistryID = MinistryId;

        //    noticeModel.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model) as List<tMemberNotice>;
        //    noticeModel.DepartmentId = Convert.ToString(0);
        //    noticeModel.DepartmentName = "Select Department";

        //    return Json(noticeModel.DepartmentLt, JsonRequestBehavior.AllowGet);
        //}
        [HttpGet]
        public JsonResult GetSessionByAssemblyId(int AssemblyId)
        {
            BillWorkModel mdl = new BillWorkModel();
            mBills model = new mBills();
            model.AssemblyId = AssemblyId;
            mdl.SessionLt = Helper.ExecuteService("BillWorks", "GetSessionsByAssemblyID", model) as List<BillWorkModel>;
            //List<mSession> SessLst = new List<mSession>();
            //if (AssemblyId != 0)
            //{
            //    mdl.AssemblyID = AssemblyId;
            //    SessLst = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", mdl);
            //}
            return Json(mdl.SessionLt, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ContentResult UploadFiles()
        {

            string url = "~/BillWorksFiles/IntroPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<mBills>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/BillWorksFiles/IntroPdf";
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/BillWorksFiles/IntroPdf"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new mBills()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        [HttpPost]
        public ContentResult PassedUploadFiles()
        {

            string url = "~/BillWorksFiles/PassedPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<mBills>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/BillWorksFiles/PassedPdf";
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/BillWorksFiles/PassedPdf"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new mBills()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        [HttpPost]
        public ContentResult AssentedUploadFiles()
        {

            string url = "~/BillWorksFiles/AssentedPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<mBills>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/BillWorksFiles/AssentedPdf";
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/BillWorksFiles/AssentedPdf"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new mBills()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        [HttpPost]
        public ContentResult ActUploadFiles()
        {

            string url = "~/BillWorksFiles/ActPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<mBills>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/BillWorksFiles/ActPdf";
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/BillWorksFiles/ActPdf"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new mBills()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult RemoveIntroFiles()
        {
            string url = "~/BillWorksFiles/IntroPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemovePassedFiles()
        {
            string url = "~/BillWorksFiles/PassedPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveAssentedPdfFiles()
        {
            string url = "~/BillWorksFiles/AssentedPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveActPdfFiles()
        {
            string url = "~/BillWorksFiles/ActPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateBillsEntry(Updatevalue model)
        {
            string CurrAssembly = "";

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId as string))
            {
                CurrAssembly = CurrentSession.AssemblyId as string;
            }
            else
            {
                List<KeyValuePair<string, string>> methodParameterAss = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameterAss);

                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }

                }
            }
            //BillWorkModel mdl = new BillWorkModel();
            mBills Updatemodel = new mBills();
            Updatemodel.ID = model.ID;
            Updatemodel.AssemblyId = model.AssemblyId;
            Updatemodel.SessionId = model.SessionId;
            Updatemodel.BillTitle = model.Title;
            Updatemodel.ID = model.ID;
            Updatemodel.BillNo = Convert.ToInt32(model.BillNoSplit) + " of " + Convert.ToInt32(model.BillYearSplit);
            Updatemodel.IntroductionFilePath = model.IntroductionFilePath;
            Updatemodel.PassingFilePath = model.PassingFilePath;
            Updatemodel.AccentedFilePath = model.AccentedFilePath;
            Updatemodel.ActFilePath = model.ActFilePath;
            Updatemodel.AssentedBy = model.AssentedBy;
            Updatemodel.IsFreeze = model.IsFreeze;
            if (model.ActNosplit == 0)
            {
                Updatemodel.ActNo = null;
            }
            else
            {
                Updatemodel.ActNo = Convert.ToInt32(model.ActNosplit) + " of " + Convert.ToInt32(model.ActYearSplit); ;
            }
            if (model.IntroDate != null)
            {
                string IntroDate = model.IntroDate;
                Updatemodel.IntroductionDate = DateTime.ParseExact(IntroDate, "dd/MM/yyyy", null);
            }

            if (model.PassedDate != null)
            {
                string PassedDate = model.PassedDate;
                Updatemodel.PassingDate = DateTime.ParseExact(PassedDate, "dd/MM/yyyy", null);
            }
            if (model.AssentDate != null)
            {
                string AssesntDate = model.AssentDate;
                Updatemodel.AssesntDate = DateTime.ParseExact(AssesntDate, "dd/MM/yyyy", null);
            }
            if (model.PubDate != null)
            {
                string PubDate = model.PubDate;
                Updatemodel.PublishedDate = DateTime.ParseExact(PubDate, "dd/MM/yyyy", null);
            }

            string url = "~/BillWorksFiles/IntroPdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath("~/BillWorksFiles/IntroPdf"));
                string SourceFile = savedFileName[0];
                foreach (string page in savedFileName)
                {
                    string name = Path.GetFileName(page);
                    string nameKey = Path.GetFileNameWithoutExtension(page);
                    string directory1 = Path.GetDirectoryName(page);
                    //
                    // Display the Path strings we extracted.
                    //
                    Console.WriteLine("{0}, {1}, {2}, {3}",
                    page, name, nameKey, directory1);
                    Updatemodel.IntroFileName = name;
                }
                string ext = Path.GetExtension(Updatemodel.IntroFileName);
                string fileName = Updatemodel.IntroFileName.Replace(ext, "");
                string ChangeIntroFileName = model.BillNoSplit + "of" + model.BillYearSplit + "_" + "Introduction" + ext;
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/IntroductionPdf/", ChangeIntroFileName);
                string Murl = "AssemblyFiles" + "/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/" + "/IntroductionPdf/" + "/";
                if (!Directory.Exists(FileSettings.SettingValue + Murl))
                {
                    Directory.CreateDirectory(FileSettings.SettingValue + Murl);
                }
                string SaveIntroPath = "/IntroductionPdf/" + ChangeIntroFileName;
                System.IO.File.Copy(SourceFile, path, true);
                Updatemodel.IntroductionFilePath = SaveIntroPath;
            }
            string urlPassed = "~/BillWorksFiles/PassedPdf";
            string directoryPassed = Server.MapPath(urlPassed);
            if (Directory.Exists(directoryPassed))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath("~/BillWorksFiles/PassedPdf"));
                string SourceFile = savedFileName[0];
                foreach (string page in savedFileName)
                {
                    string name = Path.GetFileName(page);
                    string nameKey = Path.GetFileNameWithoutExtension(page);
                    string directory1 = Path.GetDirectoryName(page);
                    //
                    // Display the Path strings we extracted.
                    //
                    Console.WriteLine("{0}, {1}, {2}, {3}",
                    page, name, nameKey, directory1);
                    Updatemodel.PassFileName = name;
                }
                string ext = Path.GetExtension(Updatemodel.PassFileName);
                string fileName = Updatemodel.PassFileName.Replace(ext, "");
                string ChangePassedFileName = model.BillNoSplit + "of" + model.BillYearSplit + "_" + "Passed" + ext;
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/PassedPdf/", ChangePassedFileName);
                string Murl = "AssemblyFiles" + "/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/" + "/PassedPdf/" + "/";
                if (!Directory.Exists(FileSettings.SettingValue + Murl))
                {
                    Directory.CreateDirectory(FileSettings.SettingValue + Murl);
                }
                string SavePassedPath = "/PassedPdf/" + ChangePassedFileName;
                System.IO.File.Copy(SourceFile, path, true);
                Updatemodel.PassingFilePath = SavePassedPath;
            }
            string urlAssented = "~/BillWorksFiles/AssentedPdf";
            string directoryAssented = Server.MapPath(urlAssented);
            if (Directory.Exists(directoryAssented))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath("~/BillWorksFiles/AssentedPdf"));
                string SourceFile = savedFileName[0];
                foreach (string page in savedFileName)
                {
                    string name = Path.GetFileName(page);
                    string nameKey = Path.GetFileNameWithoutExtension(page);
                    string directory1 = Path.GetDirectoryName(page);
                    //
                    // Display the Path strings we extracted.
                    //
                    Console.WriteLine("{0}, {1}, {2}, {3}",
                    page, name, nameKey, directory1);
                    Updatemodel.AssentFileName = name;
                }
                string ext = Path.GetExtension(Updatemodel.AssentFileName);
                string fileName = Updatemodel.AssentFileName.Replace(ext, "");
                string ChangeAssentedFileName = model.BillNoSplit + "of" + model.BillYearSplit + "_" + "Assented" + ext;
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/AssentedPdf/", ChangeAssentedFileName);
                string Murl = "AssemblyFiles" + "/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/" + "/AssentedPdf/" + "/";
                if (!Directory.Exists(FileSettings.SettingValue + Murl))
                {
                    Directory.CreateDirectory(FileSettings.SettingValue + Murl);
                }
                string SaveAssentedPath = "/AssentedPdf/" + ChangeAssentedFileName;
                System.IO.File.Copy(SourceFile, path, true);
                Updatemodel.AccentedFilePath = SaveAssentedPath;
            }
            string urlActPdf = "~/BillWorksFiles/ActPdf";
            string directoryActPdf = Server.MapPath(urlActPdf);
            if (Directory.Exists(directoryActPdf))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath("~/BillWorksFiles/ActPdf"));
                string SourceFile = savedFileName[0];
                foreach (string page in savedFileName)
                {
                    string name = Path.GetFileName(page);
                    string nameKey = Path.GetFileNameWithoutExtension(page);
                    string directory1 = Path.GetDirectoryName(page);
                    //
                    // Display the Path strings we extracted.
                    //
                    Console.WriteLine("{0}, {1}, {2}, {3}",
                    page, name, nameKey, directory1);
                    Updatemodel.ActFileName = name;
                }
                string ext = Path.GetExtension(Updatemodel.ActFileName);
                string fileName = Updatemodel.ActFileName.Replace(ext, "");
                string ChangeActFileName = model.BillNoSplit + "of" + model.BillYearSplit + "_" + "Act" + ext;
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/ActPdf/", ChangeActFileName);
                string Murl = "AssemblyFiles" + "/" + CurrAssembly + "/" + NewsSettings.SettingValue + "/" + "/ActPdf/" + "/";
                if (!Directory.Exists(FileSettings.SettingValue + Murl))
                {
                    Directory.CreateDirectory(FileSettings.SettingValue + Murl);
                }
                string SaveActPdfPath = "/ActPdf/" + ChangeActFileName;
                System.IO.File.Copy(SourceFile, path, true);
                Updatemodel.ActFilePath = SaveActPdfPath;
            }
            Updatemodel = Helper.ExecuteService("BillWorks", "UpdateMBills", Updatemodel) as mBills;
            //    Update.Message = "Update Sucessfully";
            string url5 = "~/BillWorksFiles/IntroPdf";
            string directory5 = Server.MapPath(url5);
            if (Directory.Exists(directory5))
            {
                System.IO.Directory.Delete(directory5, true);
            }
            string url6 = "~/BillWorksFiles/PassedPdf";
            string directory6 = Server.MapPath(url6);
            if (Directory.Exists(directory6))
            {
                System.IO.Directory.Delete(directory6, true);
            }
            string url7 = "~/BillWorksFiles/AssentedPdf";
            string directory7 = Server.MapPath(url7);
            if (Directory.Exists(directory7))
            {
                System.IO.Directory.Delete(directory7, true);
            }
            string url8 = "~/BillWorksFiles/ActPdf";
            string directory8 = Server.MapPath(url8);
            if (Directory.Exists(directory8))
            {
                System.IO.Directory.Delete(directory8, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadPdf(string Wid)
        {
            string CurrAssembly = "";

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId as string))
            {
                CurrAssembly = CurrentSession.AssemblyId as string;
            }
            else
            {
                List<KeyValuePair<string, string>> methodParameterAss = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameterAss);

                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }

                }
            }

            string[] Val = Wid.Split(',', ',');
            string fistId = Val[0].ToString();
            string SecondType = Val[1].ToString();
            int Id = Convert.ToInt32(fistId);
            string path = string.Empty;
            string FileName = string.Empty;
            try
            {
                
                var model = (mBills)Helper.ExecuteService("BillWorks", "GetBillsDetails", new mBills { ID = Id });
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
                var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                var Filelocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string FileStructurePath = Acess.SettingValue;
                if (SecondType == "I")
                {
                    string url = FileStructurePath + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + model.BillWorkModel.FirstOrDefault().IntroductionFilePath;
                    path = Filelocation.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + model.BillWorkModel.FirstOrDefault().IntroductionFilePath;
                }
                else if (SecondType == "P")
                {
                    string url = FileStructurePath + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + model.BillWorkModel.FirstOrDefault().PassingFilePath;
                    path = Filelocation.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + model.BillWorkModel.FirstOrDefault().PassingFilePath;
                }
                else if (SecondType == "A")
                {
                    string url = FileStructurePath + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + model.BillWorkModel.FirstOrDefault().AccentedFilePath;
                    path = Filelocation.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + model.BillWorkModel.FirstOrDefault().AccentedFilePath;
                }
                else if (SecondType == "At")
                {
                    string url = FileStructurePath + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + model.BillWorkModel.FirstOrDefault().ActFilePath;
                    path = Filelocation.SettingValue + "/AssemblyFiles/" + CurrAssembly + "/" + NewsSettings.SettingValue + model.BillWorkModel.FirstOrDefault().ActFilePath;
                }
               
            }
            catch (Exception)
            {
                throw; 
            }
            finally
            {

            }
            byte[] bytes = System.IO.File.ReadAllBytes(path);
            return File(bytes, "application/pdf");
            //  return new EmptyResult();
        }


    }

    public class Updatevalue
    {
        public int ID { get; set; }
        public int AssemblyId { get; set; }
        public int SessionId { get; set; }
        [AllowHtml]
        public string Title { get; set; }
        public int BillNoSplit { get; set; }
        public int BillYearSplit { get; set; }
        public int ActNosplit { get; set; }
        public int ActYearSplit { get; set; }
        public string IntroDate { get; set; }
        public string PassedDate { get; set; }
        public string AssentDate { get; set; }
        public string IntroductionFilePath { get; set; }
        public string AccentedFilePath { get; set; }
        public string ActFilePath { get; set; }
        public string PassingFilePath { get; set; }
        public string PubDate { get; set; }
        public string AssentedBy { get; set; }
        public bool IsFreeze { get; set; }
    }
}
