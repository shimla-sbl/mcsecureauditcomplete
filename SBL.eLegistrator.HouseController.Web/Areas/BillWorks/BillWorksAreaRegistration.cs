﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.BillWorks
{
    public class BillWorksAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BillWorks";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "BillWorks_default",
                "BillWorks/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
