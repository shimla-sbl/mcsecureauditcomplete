﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.BusinessWork
{
    public class BusinessWorkAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BusinessWork";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "BusinessWork_default",
                "BusinessWork/{controller}/{action}/{id}",
                new { action = "index", Controller = "BusinessWork", id = UrlParameter.Optional }
            );
        }
    }
}
