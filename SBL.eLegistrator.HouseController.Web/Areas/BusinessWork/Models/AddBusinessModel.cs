﻿using SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;

namespace SBL.eLegistrator.HouseController.Web.Areas.BusinessWork.Models
{
    public class AddBusinessModel
    {
       
            public string RecordId { get; set; }
            public string LOBId { get; set; }

            public string AssemblyId { get; set; }
            public string AssemblyName { get; set; }
            public string SessionId { get; set; }
            public string SessionName { get; set; }

            [Required(ErrorMessage = "Please select Session Date")]
            public string Sessiondate { get; set; }
            public string SessionTime { get; set; }

            public string SrNo1 { get; set; }
            public string SrNo2 { get; set; }
            public string SrNo3 { get; set; }
            public string SrNo11 { get; set; }
            public string SrNo22 { get; set; }
            public string SrNo33 { get; set; }

            [Required(ErrorMessage = "Please enter Business")]
            [UIHint("tinymce_jquery_full"), AllowHtml]
            public string TextLOB { get; set; }

            public bool PageBreak { get; set; }

            public string TextMinister { get; set; }

            [Required(ErrorMessage = "Please enter Business")]
            [UIHint("tinymce_jquery_full"), AllowHtml]
            public string TextSpeaker { get; set; }
            public bool IsSpeakerPageBreak { get; set; }


            public string TextBrief { get; set; }
            //    public string ConcernedMemberId { get; set; }
            //    public string ConcernedMemberName { get; set; }
            //    public string ConcernedMemberDesignationId { get; set; }
            //    public string ConcernedMemberDesignationName { get; set; }
            //    public string ConcernedDeptId { get; set; }
            ////    public string ConcernedDeptName { get; set; }
            //     public string ConcernedCommitteeId { get; set; }
            //   public string ConcernedCommitteeName { get; set; }
            //   public string ConcernedRuleId { get; set; }
            //     public string ConcernedRuleName { get; set; }
            //    public string ConcernedRuleNameLocal { get; set; }


            public string ConcernedCommitteeRepTypId { get; set; }
            public string ConcernedCommitteeRepTypName { get; set; }

            public string ConcernedDeptId { get; set; }
            public string ConcernedDeptName { get; set; }

            public string ConcernedCommitteeId { get; set; }
            public string ConcernedCommitteeName { get; set; }

            public string CommitteeReportTitle { get; set; }

            public string ConcernedEventId { get; set; }
            public string ConcernedEventName { get; set; }
            public string ConcernedEventNameLocal { get; set; }

            public string PPTLocation { get; set; }
            public string PDFLocation { get; set; }
            public string VideoLocation { get; set; }
            public string ActionDocumentType { get; set; }
            public string NatureOfDocument { get; set; }

            public bool IsEVoting { get; set; }
            public string PaperLaidIdTemp { get; set; }
            public string PaperLaidId { get; set; }
            public string NewPdfPath { get; set; }
            public string ApproverdBy { get; set; }
            public string ApproverdDate { get; set; }
            public string CreatedBy { get; set; }
            public string CreateDate { get; set; }
            public string PublishedBy { get; set; }
            public string PublishedDate { get; set; }

            public int? CorrigendumId { get; set; }
            /*Paging Properties Start*/
            public int ResultCount { get; set; }
            public int RowsPerPage { get; set; }
            public int selectedPage { get; set; }
            public int loopStart { get; set; }
            public int loopEnd { get; set; }
            public int PageNumber { get; set; }

            /*Paging Properties End*/

            public bool? IsDeleted { get; set; }

            public bool? IsCorrigendum { get; set; }

            public HttpPostedFileBase Document { get; set; }

            //public virtual ICollection<mMinister> mMinisterList { get; set; }
            //public virtual ICollection<mDepartment> mDepartmentList { get; set; }
            //public virtual ICollection<mEvent> mEventList { get; set; }
            //public virtual ICollection<mCommittee> mCommitteeList { get; set; }
            //public virtual ICollection<mRule> mRuleList { get; set; }

            //public virtual ICollection<mCommitteeRepType> mCommitteeRepTypeList { get; set; }

            //public virtual ICollection<mResolution> mResolution { get; set; }

            public virtual ICollection<SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models.mSession> mSessionList { get; set; }

            public int ResolutionId { get; set; }
          //  public List<AddLOBModel> LOBList { get; set; }

            [NotMapped]
            [HiddenInput(DisplayValue = false)]
            public List<SelectListItem> yearList { get; set; }
            [NotMapped]
            public string BillTextNumber { get; set; }
            [NotMapped]
            public int BillNumberYear { get; set; }
            [NotMapped]
            public string BillTextTitle { get; set; }

       /// For Secretary Left menu
            public int PendingStarredQuestionCount { get; set; }
            public int SentStarredQuestionCount { get; set; }
            public int RecievedStarredQuestionCount { get; set; }
            public int CurrentLobStarredQuestionCount { get; set; }
            public int LaidInHomeStarredQuestionCount { get; set; }

            public int PendingUnStarredQuestionCount { get; set; }
            public int SentUnStarredQuestionCount { get; set; }
            public int RecievedUnStarredQuestionCount { get; set; }
            public int CurrentLobUnStarredQuestionCount { get; set; }
            public int LaidInHomeUnStarredQuestionCount { get; set; }

            public int PendingNoticesCount { get; set; }
            public int SentNoticesCount { get; set; }
            public int RecievedNoticesCount { get; set; }
            public int CurrentLobNoticesCount { get; set; }
            public int LaidInHomeNoticesCount { get; set; }


            public int RecievedCommitteePaperCount { get; set; }
            public int SentCommitteePaperCount { get; set; }
            public int CurrentLobCommitteePaperCount { get; set; }
            public int LaidInHomeCommitteePaperCount { get; set; }


            public int RecievedBillsCount { get; set; }
            public int SentBillsCount { get; set; }
            public int CurrentLobBillsCount { get; set; }
            public int LaidInHomeBillsCount { get; set; }

            public int SentPaperToLayCount { get; set; }
            public int RecievedPaperToLayCount { get; set; }
            public int CurrentLobPaperToLayCount { get; set; }
            public int LaidInHomePaperToLayCount { get; set; }

            //public string SessionName { get; set; }
            public string AssesmblyName { get; set; }


            public string DepartmentName { get; set; }
            public string UserDesignation { get; set; }
            public string CurrentUserName { get; set; }
            public string UserName { get; set; }
            public string CustomMessage { get; set; }
            //pendin to lay
            public int PendingToLayStarredQuestionCount { get; set; }
            //unstarred  pending to lay
            public int PendingToLayUnStarredQuestionCount { get; set; }
            // notice Pending to lay 
            public int PendingToLayNoticeCount { get; set; }
            //bills pending to lay
            public int PendingToLayBillsCount { get; set; }
            //Committee pending to lay
            public int PendingToLayCommitteeCount { get; set; }
            //OtherPaper pending to lay
            public int PendingToLayOtherPaperCount { get; set; }
            //Updated By Nitin
            public int TotalUpdatedFileCounter { get; set; }

            public int StarredUnfixed { get; set; }
            public int Starredfixed { get; set; }
            public int UnstarredUnfixed { get; set; }
            public int Unstarredfixed { get; set; }
            public int PostPoneCount { get; set; }
            public int ReFixPostPoneCount { get; set; }

            //updated By Sameer
            public int TotalValue { get; set; }
            public int TotalStaredReceived { get; set; }
            public int TotalUnStaredReceived { get; set; }
            public int NoticeResultCount { get; set; }
            public int TotalClubbedQuestion { get; set; }
            public int TotalInActiveQuestion { get; set; }
            public int TotalValueCl { get; set; }
            public int TotalStaredReceivedCl { get; set; }
            public int QuestionTypeId { get; set; }
            public string UId { get; set; }
            public string RoleName { get; set; }
            public int UnstaredTotalClubbedQuestion { get; set; }
            public int TotalUnStaredReceivedCl { get; set; }
            public int TotalSBracktedQuestion { get; set; }
            public int TotalUBractedQuestion { get; set; }
            public int StarredToUnstarredCount { get; set; }
            public int StarredRejectedCount { get; set; }
            public int UnstarredRejectedCount { get; set; }
            public int ChangedRuleNoticeCount { get; set; }

            public int AssemblyID { get; set; }
            public int SessionID { get; set; }
            public List<SBL.DomainModel.Models.Assembly.mAssembly> mAssemblyList { get; set; }
            public IList<SBL.DomainModel.Models.Session.mSession> sessionList { get; set; }
            public string AccessFilePath { get; set; }
            public int PendingNoticesAssignCount { get; set; }
            public int CompleteSDashCount { get; set; }
            public int CompleteUNDashCount { get; set; }
            public int CompleteNDashCount { get; set; }
            public int StarredCountExess { get; set; }
            public int StarredCountExessed { get; set; }
            public int UnStarredCountExess { get; set; }
            public int UnStarredCountExessed { get; set; }

            public int StarredCountForWithdrawn { get; set; }
            public int StarredWithdrawnCount { get; set; }
            public int UnstarredCountForWithdrawn { get; set; }
            public int UntarredWithdrawnCount { get; set; }
            public int CountForClub { get; set; }
            public int CountForClubbed { get; set; }

            //house committee count - by robin
            public int ReceivedListCount { get; set; }
            public int DraftListCount { get; set; }
            public int SentPaperListCount { get; set; }


    }
    [Serializable]
    public class mSessions
    {
        public string AssemblyId { get; set; }
        public string SessionId { get; set; }
        public string SessionDate { get; set; }
        public string Name { get; set; }

    }
   

}
