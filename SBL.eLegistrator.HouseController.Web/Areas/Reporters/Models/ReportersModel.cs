﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.LOB;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Reporters.Models
{
    public class ReportersModel 
    {

        public ReportersModel()
        {
            this.ListmSession = new List<mSession>();
            this.ListmAssembly = new List<mAssembly>();
            this.ListDates = new List<string>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AssemblyID { get; set; }

        public int AssemblyCode { get; set; }

        [Required]
        [StringLength(200)]
        public string AssemblyName { get; set; }

        [StringLength(200)]
        public string AssemblyNameLocal { get; set; }

        [Column(TypeName = "date")]
        public DateTime? AssemblyStartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? AssemblyEndDate { get; set; }

        [StringLength(12)]
        public string Period { get; set; }

        [Column(TypeName = "text")]
        public string Rem1 { get; set; }

        [Column(TypeName = "text")]
        public string Rem2 { get; set; }

        [Column(TypeName = "text")]
        public string Rem3 { get; set; }

        [Column(TypeName = "text")]
        public string Rem4 { get; set; }

        [Column(TypeName = "text")]
        public string Rem5 { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public int SessionCode { get; set; }

        [NotMapped]
        public virtual List<mSession> ListmSession { get; set; }

        [NotMapped]
        public virtual List<mAssembly> ListmAssembly { get; set; }

        [NotMapped]
        public virtual List<string> ListDates { get; set; }

        public string SessionDate { get; set; }

        public string SessionName { get; set; }

    }


    public class LOBModel
    {
        public LOBModel()
        {
            this.ListAdminLOB = new List<AdminLOB>();

        }

        public string outXml { get; set; }

        public DateTime? sessDate { get; set; }
        public List<AdminLOB> ListAdminLOB { get; set; }
    }


    public class SearchRecordsModel
    {
        public SearchRecordsModel()
        {
            this.AssemblyList = new List<mAssembly>();
            this.SessionList = new List<mSession>();
            this.mSessionDateList = new List<mSessionDateModel>();
            this.MemberList = new List<mMember>();
            this.EventList = new List<mEvent>();
        }
        public List<mAssembly> AssemblyList { get; set; }
        public int AssemblyId { get; set; }

        public List<mSession> SessionList { get; set; }
        public int SessionID { get; set; }

        public string Sessiondate { get; set; }
        public List<mSessionDateModel> mSessionDateList { get; set; }

        public string MemberId { get; set; }
        public List<mMember> MemberList { get; set; }

        public string EventId { get; set; }
        public List<mEvent> EventList { get; set; }
    }

    public class mSessionDateModel
    {
        public string AssemblyId { get; set; }
        public string SessionId { get; set; }
        public string SessionDate { get; set; }
        public string Name { get; set; }

    }
}