﻿using EvoPdf;
using Lib.Web.Mvc.JQuery.JqGrid;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.LOB;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Areas.Reporters.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using SBL.eLegistrator.HouseController.Web.Extensions;
using Ionic.Zip;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Reporters.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class ReportersController : Controller
    {
        //
        // GET: /Reporters/Reporters/

        #region "Index Page"

        /// <summary>
        /// Render the main dashbord for the Reporters
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ReportersModel reportersModel = new ReportersModel();
            mAssembly assembly = new mAssembly();
            reportersModel.ListmAssembly = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyWithSessions", assembly);

            return View(reportersModel);           
        }

        #endregion

        #region "Session Calender"

        /// <summary>
        /// Get the session calender info by session code and Assemblyid
        /// </summary>
        /// <param name="SessionCode"></param>
        /// <param name="AssemblyId"></param>
        /// <returns></returns>
        public ActionResult GetSessionCalendar(string SessionCode, string AssemblyId)
        {
            ReportersModel reportersModel = new ReportersModel();
            reportersModel.SessionCode = Convert.ToInt16(SessionCode);
            reportersModel.AssemblyID = Convert.ToInt16(AssemblyId);
            return PartialView("GetSessionCalendar", reportersModel);
        }
       
        /// <summary>
        /// get the session date info for the session
        /// </summary>
        /// <param name="SessionCode"></param>
        /// <param name="AssemblyId"></param>
        /// <returns></returns>
        public JsonResult GetSessionDateInfo(string SessionCode, string AssemblyId)
        {
            mSession session = new mSession();
            session.AssemblyID = Convert.ToInt16(AssemblyId);
            session.SessionCode = Convert.ToInt16(SessionCode);
            List<mSessionDate> ListSessionDates = new List<mSessionDate>();
            ListSessionDates = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", session);

            ReportersModel reportersModel = new ReportersModel();
            List<string> Listdate = new List<string>();
            foreach (var item in ListSessionDates)
            {
                reportersModel.SessionDate = ListSessionDates[0].SessionDate.ToString("dd/MM/yyyy");
                Listdate.Add(item.SessionDate.ToString("dd/MM/yyyy"));
            }
            reportersModel.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", session);

            mAssembly assembly = new mAssembly();
            assembly.AssemblyID = Convert.ToInt16(AssemblyId);
            reportersModel.AssemblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assembly);
            

            reportersModel.ListDates = Listdate;
            return Json(reportersModel, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "LOB by Date" 
        /// <summary>
        /// Get the Lob information for the session date
        /// </summary>
        /// <param name="SessionDate"></param>
        /// <returns>HTML for the LOB</returns>
        public ActionResult GetLOBBySessionDate(string SessionDate)
        {
            AdminLOB adminLOB = new AdminLOB();

            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Date = DateTime.ParseExact(SessionDate, "d", provider);

            DateTime sessDate = Convert.ToDateTime(Date);
            adminLOB.SessionDate = sessDate;
            // LOBID = 0;
            LOBModel lob = new LOBModel();
            lob.ListAdminLOB = (List<AdminLOB>)Helper.ExecuteService("LOB", "GetLOBBySessionDate", adminLOB);
            string outXml = "";
          string LOBName = "";
          string SrNo1 = "";
          string SrNo2 = "";
          string SrNo3 = "";
          int SrNo1Count = 1;
          int SrNo2Count = 1;
          int SrNo3Count = 1;
          DateTime SessionDate1 = new DateTime();

          SiteSettings siteSettingMod = new SiteSettings();
          siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

     //     SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
       //   AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);
          string CurrentSecretery = siteSettingMod.CurrentSecretery;
          if (lob.ListAdminLOB != null && lob.ListAdminLOB.Count() > 0)
          {
              outXml = outXml + @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
              for (int i = 0; i < lob.ListAdminLOB.Count; i++)
              {
                  if (i == 0)
                  {

                      // LOBID = lob.ListAdminLOB[i].LOBId;
                      // sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                      SessionDate1 = Convert.ToDateTime(lob.ListAdminLOB[i].SessionDate);
                      string date = ConvertSQLDate(SessionDate1);
                      date = date.Replace("/", "");
                      date = date.Replace("-", "");
                      LOBName = date + "_" + "LOB" + "_" + Convert.ToString(lob.ListAdminLOB[i].LOBId);


                      outXml += @"<tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
                                                          " + Convert.ToString(lob.ListAdminLOB[i].AssemblyNameLocal) + @"    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>
                                                                           
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold'>
                                                            " + Convert.ToString(lob.ListAdminLOB[i].SessionNameLocal) + @"               
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold; height: 30px;'>                
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
                                                           " + Convert.ToString(lob.ListAdminLOB[i].SessionDateLocal) + @"              
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>               
                                                             " + Convert.ToString(lob.ListAdminLOB[i].SessionTimeLocal) + @"              
                                                    </td>
                                                </tr>";
                  }

                  if (lob.ListAdminLOB[i].SrNo1 != null && lob.ListAdminLOB[i].SrNo2 == null && lob.ListAdminLOB[i].SrNo3 == null)
                  {

                      SrNo1 = Convert.ToString(SrNo1Count) + ".";
                      outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                      <br/>  
                                    " + SrNo1 + @"";
                      SrNo1Count = SrNo1Count + 1;
                      SrNo2Count = 1;


                  }
                  else if (lob.ListAdminLOB[i].SrNo1 != null && lob.ListAdminLOB[i].SrNo2 != null && lob.ListAdminLOB[i].SrNo3 == null)
                  {

                      SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                      outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                    " + SrNo2 + @"";
                      SrNo2Count = SrNo2Count + 1;
                      SrNo3Count = 1;


                  }
                  else if (lob.ListAdminLOB[i].SrNo1 != null && lob.ListAdminLOB[i].SrNo2 != null && lob.ListAdminLOB[i].SrNo3 != null)
                  {

                      SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                      outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                    " + SrNo3 + @"";
                      SrNo3Count = SrNo3Count + 1;

                  }

                  string LOBText = Convert.ToString(lob.ListAdminLOB[i].TextLOB);
                  if (LOBText != "")
                  {
                      LOBText = LOBText.Replace("<p>", "");
                      LOBText = LOBText.Replace("</p>", "");
                  }


                  outXml += @"" + LOBText + "<br/>";

                  if (lob.ListAdminLOB[i].PDFLocation != null && lob.ListAdminLOB[i].PDFLocation != "")
                  {
                      string pdfpath = lob.ListAdminLOB[i].PDFLocation;
                      pdfpath = pdfpath.Substring(1);
                      pdfpath = pdfpath.Replace(" ", "%20");
                      outXml += @" </td> </tr> <tr><td style='text-align: right;font-size:16px;font-weight:bold'>Paper :  <a href=" + pdfpath + " target='_blank' > <img style='width: 25px; height: 25px' title='PDF' src='/Images/Common/PDF.png'  />  </a>";
                      outXml += @"</td></tr>";
                  }
                  else
                  {
                      outXml += @"<br/> </td></tr>";
                  }



                  if (i == lob.ListAdminLOB.Count - 1)
                  {
                      outXml += @" <tr><td colspan='2'><br /><br /><br /><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:70%;float:left;font-size: 18px;'>
                                                Shimla-171004
                                                </td>
                                                <td style='width:30%;float:left;font-size: 18px;'>
                                                 " + CurrentSecretery + @"  
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:70%;float:left;font-size: 18px;'>
                                                " + SessionDate1.ToString("D", new CultureInfo("hi")) + @"
                                                </td>
                                                <td style='width:30%;float:left;'>
                                                
                                                </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                      outXml += "</table></div></div> </body> </html>";
                  }

              }

          }
           lob.sessDate = sessDate;

           lob.outXml = outXml;

           return PartialView("PartialLOB", lob);
        }
        #endregion

        #region "S/Un-s Questions"
        /// <summary>
        /// Action method for getting HTML for the Starred Question
        /// </summary>
        /// <param name="SessionDate"></param>
        /// <returns></returns>
        public ActionResult GetSQuestionsBySessionDate(string SessionDate)
        {
            LOBModel lob = new LOBModel();

            lob.outXml = GetQuestionByDate(SessionDate, 1);

            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Date = DateTime.ParseExact(SessionDate, "d", provider);

            DateTime sessDate = Convert.ToDateTime(Date);
            lob.sessDate = sessDate;

            return PartialView("PartialSQuestions", lob);
        }

        /// <summary>
        /// Action method for getting HTML for the Un-Starred Question
        /// </summary>
        /// <param name="SessionDate"></param>
        /// <returns></returns>
        public ActionResult GetUnSQuestionsBySessionDate(string SessionDate)
        {
            LOBModel lob = new LOBModel();

            lob.outXml = GetQuestionByDate(SessionDate, 2);
            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Date = DateTime.ParseExact(SessionDate, "d", provider);

            DateTime sessDate = Convert.ToDateTime(Date);
            lob.sessDate = sessDate;
            return PartialView("PartialUnSQuestions", lob);
            
        }

        /// <summary>
        /// Generate HTML from DB for the Starred/Un-Starred Question for the session date
        /// </summary>
        /// <param name="SessionDate"></param>
        /// <param name="QuestionType"></param>
        /// <returns></returns>
        public string GetQuestionByDate(string SessionDate, int QuestionType)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            tQuestion Question = new tQuestion();
            var Date = DateTime.ParseExact(SessionDate, "d", provider);

            DateTime sessDate = Convert.ToDateTime(Date);
            Question.IsFixedDate = sessDate;
            Question.QuestionType = QuestionType;
            List<tQuestion> ListQuestion = new List<tQuestion>();
            ListQuestion = (List<tQuestion>)Helper.ExecuteService("Questions", "GetQuestionBySessionDate", Question);

#pragma warning disable CS0219 // The variable 'idCount' is assigned but its value is never used
            int idCount = 2;
#pragma warning restore CS0219 // The variable 'idCount' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo1' is assigned but its value is never used
            string SrNo1 = "";
#pragma warning restore CS0219 // The variable 'SrNo1' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo2' is assigned but its value is never used
            string SrNo2 = "";
#pragma warning restore CS0219 // The variable 'SrNo2' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo3' is assigned but its value is never used
            string SrNo3 = "";
#pragma warning restore CS0219 // The variable 'SrNo3' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo1Count' is assigned but its value is never used
            int SrNo1Count = 1;
#pragma warning restore CS0219 // The variable 'SrNo1Count' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo2Count' is assigned but its value is never used
            int SrNo2Count = 1;
#pragma warning restore CS0219 // The variable 'SrNo2Count' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SrNo3Count' is assigned but its value is never used
            int SrNo3Count = 1;
#pragma warning restore CS0219 // The variable 'SrNo3Count' is assigned but its value is never used
            string sessiondate = null;
            string outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

            for (int i = 0; i < ListQuestion.Count; i++)
            {

                //Session date info
                DateTime SessionDate1 = Convert.ToDateTime(ListQuestion[0].IsFixedDate);
                sessiondate = Convert.ToString(ListQuestion[0].IsFixedDate);
                string date = SessionDate1.ToString("dd/MM/yyyy");

                ///Department Info
                mDepartment Department = new mDepartment();
                Department.deptId = ListQuestion[i].DepartmentID;
                Department = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", Department);


                string DepartmentName = "";
                string DepartmentNameLocal = "";
                if (Department != null)
                {
                    DepartmentName = Department.deptname.Trim();
                    DepartmentNameLocal = Department.deptnameLocal;
                }


                ///QuestionInfo
                string QuesNo = Convert.ToString(ListQuestion[i].QuestionNumber);
                string Subject = ListQuestion[i].Subject;


                tQuestionType tquestion = new tQuestionType();
                tquestion.QuestionTypeId = ListQuestion[i].QuestionType;
                tquestion = (tQuestionType)Helper.ExecuteService("Questions", "GetQuestionTypeByTypeId", tquestion);

                string QuesType = "";
                if (tquestion != null)
                {
                    QuesType = Convert.ToString(tquestion.QuestionTypeName);
                }

                string memberName = "";
                string memberNameLocal = "";

                if (Convert.ToBoolean(ListQuestion[i].IsClubbed))
                {

     
                    string[] members = ListQuestion[i].ReferenceMemberCode.Split(',');
                    for (int j = 0; j < members.Count(); j++)
                    {
                        ///Question member Info
                        if (j == members.Count() - 1)
                        {                            
                            mMember Member = new mMember();
                            Member.MemberCode =Convert.ToInt16(members[j]);
                            QuestionModelCustom CustomModel = new QuestionModelCustom();
                            CustomModel = (QuestionModelCustom)Helper.ExecuteService("Member", "GetMemberInfoByMemberCode", Member);

                            if (CustomModel != null)
                            {

                                memberName = memberName + CustomModel.MemberName + " (" + CustomModel.ConstituencyName + ")";
                                memberNameLocal = memberNameLocal + CustomModel.MemberNameLocal + " (" + CustomModel.ConstituencyName_Local + ")";

                            }
                        }
                        else
                        {
                            mMember Member = new mMember();
                            Member.MemberCode = Convert.ToInt16(members[j]);
                            QuestionModelCustom CustomModel = new QuestionModelCustom();
                            CustomModel = (QuestionModelCustom)Helper.ExecuteService("Member", "GetMemberInfoByMemberCode", Member);

                            if (CustomModel != null)
                            {

                                memberName = memberName + CustomModel.MemberName + " (" + CustomModel.ConstituencyName + ") ,";
                                memberNameLocal = memberName + CustomModel.MemberNameLocal + " (" + CustomModel.ConstituencyName_Local + "),";

                            }
                        }
                    }
                }
                else
                {
                    mMember Member = new mMember();
                    Member.MemberCode = ListQuestion[i].MemberID;
                    QuestionModelCustom CustomModel = new QuestionModelCustom();
                    CustomModel = (QuestionModelCustom)Helper.ExecuteService("Member", "GetMemberInfoByMemberCode", Member);

                    
                    if (CustomModel != null)
                    {

                        memberName = CustomModel.MemberName + " (" + CustomModel.ConstituencyName + ")";
                        memberNameLocal = CustomModel.MemberNameLocal + " (" + CustomModel.ConstituencyName_Local + ")";

                    }
                }
               

                ///Question Minister Info
                mMinistry Ministry = new mMinistry();
                Ministry.MinistryID =Convert.ToInt16(ListQuestion[i].MinistryId);
                Ministry = (mMinistry)Helper.ExecuteService("MinistryMinister", "GetMinisteryInfoById", Ministry);


                string MinisterName = "";
                string MinisterNameLocal = "";
                if (Ministry != null)
                {
                    MinisterName = Ministry.MinistryName;
                    MinisterNameLocal = Ministry.MinistryNameLocal;
                }

                if (i == 0)
                {

                    //                    outXml += @"<tr>
                    //                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
                    //                                                          " + QuesType + @" 
                    //                                                    </td>
                    //                                                </tr>";

                }

                outXml += @"<tr><td style='padding-left:5%'> <table style='width:100%;'>";

                outXml += @"<tr><td style='width:35%;font-size: 18px;'> <b> Question Number : </b> </td> <td style='width:65%;font-size: 18px;font-family:DVOT-Yogesh;'> " + QuesNo + "</td></tr>";
                outXml += @"<tr><td style='width:35%;font-size: 18px;'> <b> Department : </b> </td> <td style='width:65%;font-size: 18px;font-family:DVOT-Yogesh;'> " + DepartmentName + "</td></tr>";
                outXml += @"<tr><td style='width:35%;font-size: 18px;'> <b> Question Type : </b> </td> <td style='width:65%;font-size: 18px;font-family:DVOT-Yogesh;'> " + QuesType + "</td></tr>";
                
                outXml += @"<tr><td style='width:35%;font-size: 18px;'> <b> Date : </b> </td> <td style='width:65%;font-size: 18px;font-family:DVOT-Yogesh;'> " + date + "</td></tr>";
                outXml += @"<tr><td style='width:35%;font-size: 18px;'> <b> Subject : </b> </td> <td style='width:65%;font-size: 18px;font-family:DVOT-Yogesh;'> " + Subject + "</td></tr>";
                outXml += @"<tr><td style='width:35%;font-size: 18px;'> <b> Memeber : </b> </td> <td style='width:65%;font-size: 18px;font-family:DVOT-Yogesh;'> " + memberName + "</td></tr>";
                outXml += @"<tr><td style='width:35%;font-size: 18px;'> <b> Ministry : </b> </td> <td style='width:65%;font-size: 18px;font-family:DVOT-Yogesh;'> " + MinisterName + "<br/> </td></tr>";
                outXml += @"</table> <br/> </td> </tr>";

                string MainQuestion = ListQuestion[i].MainQuestion;
                MainQuestion = MainQuestion.Replace("^^", " ");
                outXml += "<tr><td style='font-size: 18px;font-family:DVOT-Yogesh;padding-left:5%;padding-right:5%'> " + ListQuestion[i].MainQuestion + "<td> </tr>";



                if (ListQuestion[i].AnswerAttachLocation != null && ListQuestion[i].AnswerAttachLocation != "")
                {
                    string pdfpath = ListQuestion[i].AnswerAttachLocation;
                    // pdfpath = pdfpath.Substring(1);
                    pdfpath = pdfpath.Replace(" ", "%20");
                    outXml += @" <tr><td style='text-align: right;padding-right:5%;font-size:16px;font-weight:bold'>Reply :  <a href=" + pdfpath + " target='_blank' > <img style='width: 25px; height: 25px' title='PDF' src='/Images/Common/PDF.png'  />  </a> </td></tr>";

                }
                else
                {

                }
                outXml += "<tr><td style='text-align: center; font-size: 25px; font-weight: bold;'> *************** <br/><td> </tr>";


                if (i == (ListQuestion.Count - 1))
                {
                    outXml += "</table></div></div> </body> </html>";
                }

            }

            return outXml;
        }

        #endregion

        #region "PartialSearchRecords"

        /// <summary>
        /// Show/Render the Search Secreen for the Repoters 
        /// </summary>
        /// <returns></returns>
        public ActionResult PartialSearchRecords()
        {
            SearchRecordsModel SearchRecords = new SearchRecordsModel();

            mSession modelSession = new mSession();
            mAssembly modelAssembly = new mAssembly();

            //get Current Assembly and Session Codes
            SiteSettings siteSettingModel = new SiteSettings();
            siteSettingModel = Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingModel) as SiteSettings;

            SearchRecords.AssemblyId = siteSettingModel.AssemblyCode;
            SearchRecords.SessionID = siteSettingModel.SessionCode;

            //get Assembly List
            SearchRecords.AssemblyList = Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", modelAssembly) as List<mAssembly>;

            modelSession.AssemblyID = siteSettingModel.AssemblyCode;
            //get session List by Assembly Id
            SearchRecords.SessionList = Helper.ExecuteService("Session", "GetSessionsByAssemblyIDReverse", modelSession) as List<mSession>;

            
            //Get Session Date By Id
            mSession session = new mSession();
            session.AssemblyID = Convert.ToInt16(siteSettingModel.AssemblyCode);
            session.SessionCode = Convert.ToInt16(siteSettingModel.SessionCode);
            List<mSessionDate> ListSessionDates = new List<mSessionDate>();
            ListSessionDates = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", session);

            List<mSessionDateModel> ListSession = new List<mSessionDateModel>();
            mSessionDateModel Session1 = new mSessionDateModel();
            Session1.SessionDate = "Select";
            ListSession.Add(Session1);

            for (int i = 0; i < ListSessionDates.Count; i++)
            {
                mSessionDateModel Session = new mSessionDateModel();
                Session.SessionDate = ListSessionDates[i].SessionDate.ToString("dd/MM/yyyy");
                ListSession.Add(Session);
            }
            SearchRecords.mSessionDateList = ListSession;


            //Get Member List
            List<mMember> memberList = new List<mMember>();
            mMember mem = new mMember();
            mem.Name = "Select";
            mem.MemberCode = 0;
            memberList.Add(mem);

            List<mMember> memberList1 = new List<mMember>();
            memberList1 = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembers", session);


            for (int i = 0; i < memberList1.Count; i++)
            {
                mMember member = new mMember();
                member.Name = memberList1[i].Name;
                member.MemberCode = memberList1[i].MemberCode;
                memberList.Add(member);
            }
            SearchRecords.MemberList = memberList;


            //Get Event List
            List<mEvent> EventList = new List<mEvent>();
            mEvent event1 = new mEvent();
            event1.EventName = "Select";
            event1.EventId = 0;
            EventList.Add(event1);

            List<mEvent> EventList1 = new List<mEvent>();
            EventList1 = (List<mEvent>)Helper.ExecuteService("Events", "GetEventsList", null);
            for (int i = 0; i < EventList1.Count; i++)
            {
                mEvent Event = new mEvent();
                Event.EventName = EventList1[i].EventName;
                Event.EventId = EventList1[i].EventId;
                EventList.Add(Event);
            }
            SearchRecords.EventList = EventList;


            return PartialView("PartialSearchRecords", SearchRecords);
        }

        #endregion
         
        #region "AssemblyChange"
        /// <summary>
        /// Get the Session Information by selected Assembly
        /// </summary>
        /// <param name="AssemblyId"></param>
        /// <returns></returns>
        public JsonResult AssemblyChange(string AssemblyId)
        {
            mSession modelSession = new mSession();
            modelSession.AssemblyID = Convert.ToInt16(AssemblyId);
            List<mSession> SessionList = Helper.ExecuteService("Session", "GetSessionsByAssemblyID", modelSession) as List<mSession>;
            SelectList objSession = new SelectList(SessionList, "Id", "Name", 0);
            return Json(SessionList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "SessionChange"

        /// <summary>
        /// get session date options for the selected Session
        /// </summary>
        /// <param name="AssemblyId"></param>
        /// <param name="SessionId"></param>
        /// <returns></returns>
        public JsonResult SessionChange(string AssemblyId, string SessionId)
        {
            mSession session = new mSession();
            session.AssemblyID = Convert.ToInt16(AssemblyId);
            session.SessionCode = Convert.ToInt16(SessionId);
            List<mSessionDate> ListSessionDates = new List<mSessionDate>();
            ListSessionDates = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", session);

            List<mSessionDateModel> ListSession = new List<mSessionDateModel>();
            mSessionDateModel Session1 = new mSessionDateModel();
            Session1.SessionDate = "Select";
            ListSession.Add(Session1);

            for (int i = 0; i < ListSessionDates.Count; i++)
            {
                mSessionDateModel Session = new mSessionDateModel();
                Session.SessionDate = ListSessionDates[i].SessionDate.ToString("dd/MM/yyyy");
                ListSession.Add(Session);
            }
            SelectList objSession = new SelectList(ListSession, "Id", "Name", 0);
            return Json(ListSession, JsonRequestBehavior.AllowGet);

            //     SearchRecords.mSessionDateList = ListSession;
        }

        #endregion

        #region "PartialSearchReports"

        /// <summary>
        /// Render the result Grid for the serch 
        /// </summary>
        /// <param name="AssemblyId"></param>
        /// <param name="SessionId"></param>
        /// <param name="SessionDate"></param>
        /// <param name="MemberId"></param>
        /// <param name="EventId"></param>
        /// <returns></returns>
        public ActionResult PartialSearchReports(string AssemblyId, string SessionId, string SessionDate, string MemberId, string EventId)
        {
            AdminLOB adminLOB = new AdminLOB();
            if (AssemblyId != null && AssemblyId != "")
            {
                adminLOB.AssemblyId = Convert.ToInt16(AssemblyId);
            }
            else
            {
                adminLOB.AssemblyId = 0;
            }
            if (SessionId != null && SessionId != "0")
            {
                adminLOB.SessionId = Convert.ToInt16(SessionId);
            }
            else
            {
                adminLOB.SessionId = 0;
            }
            if (SessionDate != null && SessionDate != "Select" && SessionDate != "0")
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                provider = new CultureInfo("fr-FR");
                var Date = DateTime.ParseExact(SessionDate, "d", provider);
                DateTime sessDate = Convert.ToDateTime(Date);
                adminLOB.SessionDate = sessDate;
            }
            else
            {
                adminLOB.SessionDate = null;
            }
            if (MemberId != null && MemberId != "0")
            {
                adminLOB.MemberId = Convert.ToInt16(MemberId);
            }
            else
            {
                adminLOB.MemberId = 0;
            }
            if (EventId != null && EventId != "0")
            {
                adminLOB.ConcernedEventId = Convert.ToInt16(EventId);
            }
            else
            {
                adminLOB.ConcernedEventId = 0;
            }
          // Helper.ExecuteService("LOB", "GetSearchResultReporters", adminLOB);

            return PartialView("PartialSearchReports",adminLOB);
        }

        /// <summary>
        /// get the search result on search perameters
        /// </summary>
        /// <param name="Mod"></param>
        /// <param name="request"></param>
        /// <param name="AssemblyId"></param>
        /// <param name="SessionId"></param>
        /// <param name="MemberId"></param>
        /// <param name="ConcernedEventId"></param>
        /// <param name="SessionDate"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetAllSearchedRecords(AdminLOB Mod, JqGridRequest request, string AssemblyId, string SessionId, string MemberId, string ConcernedEventId, string SessionDate)
        {
            AdminLOB adminLOB = new AdminLOB();

            adminLOB.PAGE_SIZE = request.RecordsCount;

            adminLOB.PageIndex = request.PageIndex+1;


            if (AssemblyId != null && AssemblyId != "")
            {
                adminLOB.AssemblyId = Convert.ToInt16(AssemblyId);
            }
            else
            {
                adminLOB.AssemblyId = null;
            }

            if (SessionId != null && SessionId != "0")
            {
                adminLOB.SessionId = Convert.ToInt16(SessionId);
            }
            else
            {
                adminLOB.SessionId = null;
            }


            if (SessionDate != null && SessionDate != "Select" && SessionDate != "0")
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                provider = new CultureInfo("fr-FR");
                var Date = DateTime.ParseExact(SessionDate, "d", provider);
                DateTime sessDate = Convert.ToDateTime(Date);
                adminLOB.SessionDate = sessDate;
            }
            else
            {
                adminLOB.SessionDate = null;
            }


            if (MemberId != null && MemberId != "0")
            {
                adminLOB.MemberId = Convert.ToInt16(MemberId);
            }
            else
            {
                adminLOB.MemberId = null;
            }

            if (ConcernedEventId != null && ConcernedEventId != "0")
            {
                adminLOB.ConcernedEventId = Convert.ToInt16(ConcernedEventId);
            }
            else
            {
                adminLOB.ConcernedEventId = null;
            }



            var result=(AdminLOB)Helper.ExecuteService("LOB", "GetSearchResultReporters", adminLOB);

            if (result != null)
            {
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.ListDraftLOB.AsQueryable();

                var resultForGrid = resultToSort.OrderBy<DraftLOB>(request.SortingName, request.SortingOrder.ToString());

                response.Records.AddRange(from questionList in resultForGrid
                                          select new JqGridRecord<DraftLOB>(Convert.ToString(questionList.LOBId), new DraftLOB(questionList)));
                return new JqGridJsonResult() { Data = response };
            }

            return new JqGridJsonResult() { Data = null };
       
        }

        #endregion

        #region "Download All LOB Files"

        /// <summary>
        /// Downlaod All LOB file
        /// </summary>
        /// <param name="SessionDate"></param>
        /// <returns></returns>
        public HttpResponseBase DownloadZip(string SessionDate)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Date = DateTime.ParseExact(SessionDate, "d", provider);

            DateTime sessDate = Convert.ToDateTime(Date);

            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                zip.AddDirectoryByName("Files");

                AdminLOB adminLOB = new AdminLOB();
                adminLOB.SessionDate = sessDate;
                 List<AdminLOB> ListAdminLOB = (List<AdminLOB>)Helper.ExecuteService("LOB", "GetLOBBySessionDate", adminLOB);

                 for (int i = 0; i < ListAdminLOB.Count; i++)
                 {
                     if (i == 0)
                     {
                         string LOB = ListAdminLOB[i].LOBPath;
                         int indexOf = LOB.LastIndexOf('/');
                         string path = Server.MapPath(LOB.Substring(0, indexOf));
                         string filname = LOB.Substring(indexOf);
                         string filePath = path + filname;
                         if (System.IO.File.Exists(filePath))
                         {
                             zip.AddFile(filePath, "Files");
                         }
                         else
                         {

                         }
                       
                     }
                     if (ListAdminLOB[i].PDFLocation != null && ListAdminLOB[i].PDFLocation != "")
                     {
                         string LOB1 = ListAdminLOB[i].PDFLocation;
                         int indexOf1 = LOB1.LastIndexOf('/');
                         string path1 = Server.MapPath(LOB1.Substring(0, indexOf1));
                         string filname1 = LOB1.Substring(indexOf1);
                         string filePath1 = path1 + filname1;
                         if (System.IO.File.Exists(filePath1))
                         {
                             zip.AddFile(filePath1, "Files");
                         }
                         else
                         {

                         }
                     }

                 }
                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("{0}.zip", "LOB_" + SessionDate);
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);
                Response.End();
           
             return Response;
            }
        }

        #endregion

        #region "Download All Starred Question Files"

        /// <summary>
        /// Download All Starred Question
        /// </summary>
        /// <param name="SessionDate"></param>
        /// <returns></returns>
        public HttpResponseBase DownloadZipStarredQuestions(string SessionDate)
        {

            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                zip.AddDirectoryByName("Files");

                string OutHTML = GetQuestionByDate(SessionDate, 1);

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                //document1.LicenseKey = "B4mYiJubiJiInIaYiJuZhpmahpGRkZE=";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(10, 10, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(10, 10, 0, 0), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = OutHTML;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();
                MemoryStream output = new MemoryStream();
                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }

                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";



                string sessiondate = SessionDate.Replace('/', ' ');



                //HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Starred_Questions.pdf");
                string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                fileName = "Starred_Questions.pdf";
                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                zip.AddFile(path, "Files");


                CultureInfo provider = CultureInfo.InvariantCulture;
                provider = new CultureInfo("fr-FR");
                tQuestion Question = new tQuestion();
                var Date = DateTime.ParseExact(SessionDate, "d", provider);

                DateTime sessDate = Convert.ToDateTime(Date);
                Question.IsFixedDate = sessDate;
                Question.QuestionType = 1;
                List<tQuestion> ListQuestion = new List<tQuestion>();
                ListQuestion = (List<tQuestion>)Helper.ExecuteService("Questions", "GetQuestionBySessionDate", Question);

                for (int i = 0; i < ListQuestion.Count; i++)
                {

                    if (ListQuestion[i].AnswerAttachLocation != null && ListQuestion[i].AnswerAttachLocation != "")
                    {
                        string LOB1 = ListQuestion[i].AnswerAttachLocation;
                        int indexOf1 = LOB1.LastIndexOf('/');
                        string path1 = Server.MapPath(LOB1.Substring(0, indexOf1));
                        string filname1 = LOB1.Substring(indexOf1);
                        string filePath1 = path1 + filname1;
                        if (System.IO.File.Exists(filePath1))
                        {
                            zip.AddFile(filePath1, "Files");
                        }
                        else
                        {

                        }
                   
                    }

                }
                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("{0}.zip", "StarredQuestions_" + SessionDate);
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);
                Response.End();

                return Response;
            }
        }

        #endregion

        #region "Download All Un-Starred Question Files"

        /// <summary>
        /// Download All Un-Starred Question
        /// </summary>
        /// <param name="SessionDate"></param>
        /// <returns></returns>
        public HttpResponseBase DownloadZipUnStarredQuestions(string SessionDate)
        {

            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                zip.AddDirectoryByName("Files");

                string OutHTML = GetQuestionByDate(SessionDate, 2);

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                //document1.LicenseKey = "B4mYiJubiJiInIaYiJuZhpmahpGRkZE=";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(10, 10, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(10, 10, 0, 0), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = OutHTML;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();
                MemoryStream output = new MemoryStream();
                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }

                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";



                string sessiondate = SessionDate.Replace('/', ' ');



              //  HttpContext.Response.AddHeader("content-disposition", "attachment; filename=UnStarred_Questions.pdf");
                string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                fileName = "UnStarred_Questions.pdf";
                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                zip.AddFile(path, "Files");


                CultureInfo provider = CultureInfo.InvariantCulture;
                provider = new CultureInfo("fr-FR");
                tQuestion Question = new tQuestion();
                var Date = DateTime.ParseExact(SessionDate, "d", provider);

                DateTime sessDate = Convert.ToDateTime(Date);
                Question.IsFixedDate = sessDate;
                Question.QuestionType = 2;
                List<tQuestion> ListQuestion = new List<tQuestion>();
                ListQuestion = (List<tQuestion>)Helper.ExecuteService("Questions", "GetQuestionBySessionDate", Question);

                for (int i = 0; i < ListQuestion.Count; i++)
                {

                    if (ListQuestion[i].AnswerAttachLocation != null && ListQuestion[i].AnswerAttachLocation != "")
                    {
                        string LOB1 = ListQuestion[i].AnswerAttachLocation;
                        int indexOf1 = LOB1.LastIndexOf('/');
                        string path1 = Server.MapPath(LOB1.Substring(0, indexOf1));
                        string filname1 = LOB1.Substring(indexOf1);
                        string filePath1 = path1 + filname1;
                        if (System.IO.File.Exists(filePath1))
                        {
                            zip.AddFile(filePath1, "Files");
                        }
                        else
                        {

                        }
                    }

                }
                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("{0}.zip", "UnStarredQuestions_" + SessionDate);
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);
                Response.End();

                return Response;
            }
        }

        #endregion

        #region "Common"

        /// <summary>
        /// Convert SQL date to dd/MM/yyyy format
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string ConvertSQLDate(DateTime? dt)
        {
            DateTime ConvtDate = Convert.ToDateTime(dt);
            string month = ConvtDate.Month.ToString();
            if (month.Length < 2)
            {
                month = "0" + month;
            }
            string day = ConvtDate.Day.ToString();
            string year = ConvtDate.Year.ToString();
            string Date = day + "/" + month + "/" + year;
            return Date;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="LOBId"></param>
        /// <param name="download"></param>
        /// <param name="LOBType"></param>
        /// <returns></returns>
        public FileStreamResult GenerateLOBPdf(string LOBId, bool download, string LOBType)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string LOBName = "20Dec2013_1_LOB";
            string CurrentSecretery = "";
            DateTime SessionDate = new DateTime();
            string savedPDF = string.Empty;

            MemoryStream output = new MemoryStream();
            if (LOBId != null && LOBId != "")
            {
                SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models.LOBModel objLOBModel = new ListOfBusiness.Models.LOBModel();
                DataSet SummaryDataSet = new DataSet();
                string sessiondate = "";
                string outXml = "";
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@LOBID", LOBId));

                if (LOBType == "Draft")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }
                else if (LOBType == "Approved")
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectAdminLOBReportByLOBId]", methodParameter);
                }
                else
                {
                    SummaryDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectDraftLOBReportByLOBId]", methodParameter);
                }

                //Getting Current Secretary
                DataSet SiteSettingDataSet = new DataSet();
                var methodParameter1 = new List<KeyValuePair<string, string>>();
                SiteSettingDataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[HPMS_SelectSiteSettings]", methodParameter1);
                if (SiteSettingDataSet != null && SiteSettingDataSet.Tables.Count > 0)
                {
                    DataTable dtSiteSetting = SiteSettingDataSet.Tables[0];
                    if (dtSiteSetting != null)
                    {
                        IEnumerable<DataRow> siteSettingQuery =
                                from siteSetting in dtSiteSetting.AsEnumerable()
                                select siteSetting;
                        IEnumerable<DataRow> curtSecretery =
                                             siteSettingQuery.Where(p => p.Field<string>("SettingName") == "CurrentSecretery");
                        foreach (DataRow obj in curtSecretery)
                        {
                            CurrentSecretery = obj.Field<string>("SettingValueLocal");
                        }
                    }
                }


                if (SummaryDataSet != null && SummaryDataSet.Tables.Count > 0)
                {

                    outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";

                    List<SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models.LOBModel> listLOBForDay = new List<SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models.LOBModel>();
                    string SrNo1 = "";
                    string SrNo2 = "";
                    string SrNo3 = "";
                    int SrNo1Count = 1;
                    int SrNo2Count = 1;
                    int SrNo3Count = 1;
                    for (int i = 0; i < SummaryDataSet.Tables[0].Rows.Count; i++)
                    {

                        if (i == 0)
                        {
                            sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            SessionDate = Convert.ToDateTime(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                            string date = ConvertSQLDate(SessionDate);
                            date = date.Replace("/", "");
                            date = date.Replace("-", "");
                            LOBName = date + "_" + "LOB" + "_" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["LOBId"]);


                            outXml += @"<tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
                                                          " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["AssemblyNameLocal"]) + @"    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>
                                                            ?????????               
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold'>
                                                            " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionNameLocal"]) + @"               
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold; height: 30px;'>                
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;'>               
                                                           " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDateLocal"]) + @"              
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 25px; font-weight: bold;text-decoration: underline;'>               
                                                             " + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionTimeLocal"]) + @"              
                                                    </td>
                                                </tr>";
                        }


                        if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == true && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo1 = Convert.ToString(SrNo1Count) + ".";
                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                      <br/>  
                                    " + SrNo1 + @"";
                            SrNo1Count = SrNo1Count + 1;
                            SrNo2Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == true)
                        {

                            SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                    " + SrNo2 + @"";
                            SrNo2Count = SrNo2Count + 1;
                            SrNo3Count = 1;


                        }
                        else if (System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo1"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo2"]) == false && System.DBNull.Value.Equals(SummaryDataSet.Tables[0].Rows[i]["SrNo3"]) == false)
                        {

                            SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                            outXml += @"<tr><td style='text-align: left; font-size: 18px;'>
                                    " + SrNo3 + @"";
                            SrNo3Count = SrNo3Count + 1;

                        }

                        string LOBText = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["TextLOB"]);
                        if (LOBText != "")
                        {
                            LOBText = LOBText.Replace("<p>", "");
                            LOBText = LOBText.Replace("</p>", "");
                        }

                        //string Member = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMemberDesignationLocal"]) + ")";
                        //string Minister = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterNameLocal"]) + " (" + Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedMinisterDesignationLocal"]) + ")";
                        //string Department = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedDeptNameLocal"]);
                        //string Committee = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedCommitteeNameLocal"]);
                        //string Rule = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["ConcernedRuleNameLocal"]);
                        outXml += @"" + LOBText + "<br/>";
                        //if (Member != " ()")
                        //{
                        //    outXml += @"<b>  सदस्य: </b>" + Member;
                        //}
                        //if (Department != "")
                        //{
                        //    outXml += @"<b>  विभाग: </b> " + Department;
                        //    if (Minister != "")
                        //    {
                        //        outXml += @"<b>  मंत्री: </b> " + Minister;
                        //    }
                        //}
                        //if (Committee != "")
                        //{
                        //    outXml += @"<b>  समिति: </b> " + Committee;
                        //}
                        //if (Rule != "")
                        //{
                        //    outXml += @"<b>  नियम: </b> " + Rule;
                        //}

                        outXml += @"<br/> </td></tr>";


                    }


                    //Bottom Section
                    outXml += @" <tr><td colspan='2'><br /><br /><br /><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:70%;float:left;'>
                                                ?????-171004
                                                </td>
                                                <td style='width:30%;float:left;'>
                                                 " + CurrentSecretery + @"  
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                <td style='width:70%;float:left;'>
                                                " + SessionDate.ToString("D", new CultureInfo("hi")) + @"
                                                </td>
                                                <td style='width:30%;float:left;'>
                                                ????
                                                </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                    outXml += "</table></div></div> </body> </html>";

                }

                // convert HTML to PDF
                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                //document1.LicenseKey = "B4mYiJubiJiInIaYiJuZhpmahpGRkZE=";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(10, 10, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(10, 10, 0, 0), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }



                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msInput, null);
                //document.Close();

                //byte[] file = ms.ToArray();
                //output.Write(file, 0, file.Length);
                //output.Position = 0;


                ///To get File Path

                methodParameter = new List<KeyValuePair<string, string>>();
                DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
                string CurrentAssembly = "";
                string Currentsession = "";


                for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                    {
                        CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                    if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                    {
                        Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                    }
                }

                string fileName = "";


                sessiondate = Convert.ToDateTime(sessiondate).ToString("dd/MM/yyyy");
                sessiondate = sessiondate.Replace('/', ' ');



                if (download)
                {
                    if (LOBType == "Draft")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Draft_" + LOBId + ".pdf");
                        string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        string directory = Server.MapPath(url);
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);
                        }
                        fileName = "Draft_" + LOBId + ".pdf";
                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");

                        #endregion


                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + "/" + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedLOBPath]", methodParameter);
                        return null;
                    }
                    else if (LOBType == "Approved")
                    {
                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Approved_" + LOBId + ".pdf");
                        string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        string directory = Server.MapPath(url);
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);
                        }
                        fileName = "Approved_" + LOBId + ".pdf";
                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion


                        methodParameter.Add(new KeyValuePair<string, string>("@filePath", url + "/" + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_SubmittedApprovedLOBPath]", methodParameter);
                        return null;
                    }
                    else if (LOBType == "Published")
                    {

                        HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Published_" + LOBId + ".pdf");
                        string url = "/LOB/" + CurrentAssembly + "/" + Currentsession + "/" + sessiondate + "/";
                        string directory = Server.MapPath(url);
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);
                        }
                        fileName = "Published_" + LOBId + ".pdf";
                        var path = Path.Combine(Server.MapPath("~" + url), fileName);
                        System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                        // close file stream
                        _FileStream.Close();


                        #region FileTransfer save

                        //int fileLength = _FileStream.ContentLength;

                        //byte[] myData = new byte[fileLength];
                        //file.InputStream.Read(myData, 0, fileLength);
                        // Gor in Binary Data
                        XmlSerializer sers = new XmlSerializer(pdfBytes.GetType());
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        System.IO.StringWriter wr = new System.IO.StringWriter(sb);
                        sers.Serialize(wr, pdfBytes);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());

                        ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, pdfBytes.Length, LOBId, "LOB");
                        // ServiceAdaptor.CallUploadFile(sb.ToString(), fileName, file.Length, "LOB", "Gallery");

                        #endregion


                        methodParameter.Add(new KeyValuePair<string, string>("@PublishLOBPath", url + "/" + fileName));
                        ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[Update_PublishedLOBPath]", methodParameter);
                        return null;
                    }
                }
            }

            return File(output, "application/pdf");
        }

        /// <summary>
        /// convert numbers in to roman format
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "m" + ToRoman(number - 1000);
            if (number >= 900) return "cm" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "d" + ToRoman(number - 500);
            if (number >= 400) return "cd" + ToRoman(number - 400);
            if (number >= 100) return "c" + ToRoman(number - 100);
            if (number >= 90) return "xc" + ToRoman(number - 90);
            if (number >= 50) return "l" + ToRoman(number - 50);
            if (number >= 40) return "xl" + ToRoman(number - 40);
            if (number >= 10) return "x" + ToRoman(number - 10);
            if (number >= 9) return "ix" + ToRoman(number - 9);
            if (number >= 5) return "v" + ToRoman(number - 5);
            if (number >= 4) return "iv" + ToRoman(number - 4);
            if (number >= 1) return "i" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        /// <summary>
        /// Test page action Method
        /// </summary>
        /// <returns></returns>
        public ActionResult Test()
        {
            return View();
        }

        #endregion

    }
}
