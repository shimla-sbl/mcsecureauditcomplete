﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Reporters
{
    public class ReportersAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Reporters";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Reporters_default",
                "Reporters/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
