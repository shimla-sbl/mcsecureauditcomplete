﻿namespace SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions
{
    #region Namespace Reffrences
    using SBL.DomainModel.Models.Adhaar;
    using SBL.DomainModel.Models.Passes;
    using SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Models;
    using SBL.eLegistrator.HouseController.Web.Helpers;
    using SBL.eLegistrator.HouseController.Web.Utility;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Xml.Linq;
    #endregion

    public static class ExtensionMethods
    {
        static List<PassCategory> ListPassCategoryList = new List<PassCategory>();

        #region Static List Methods

        public static SelectList GetPassTypeList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Public", Value = "17" });
            result.Add(new SelectListItem() { Text = "Casual Pass - Press Gallery", Value = "11" });
            result.Add(new SelectListItem() { Text = "Visitor", Value = "18" });
            result.Add(new SelectListItem() { Text = "VIP Gallery", Value = "2" });
            result.Add(new SelectListItem() { Text = "Speaker's Gallery", Value = "14" });
            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetGenderList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Male", Value = "Male" });
            result.Add(new SelectListItem() { Text = "Female", Value = "Female" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetRecommendationTypeList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Member", Value = "Member" });
            result.Add(new SelectListItem() { Text = "Minister", Value = "Minister" });
            result.Add(new SelectListItem() { Text = "Officer", Value = "Officer" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetTimeTypeList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Full Day", Value = "Full Day" });
            result.Add(new SelectListItem() { Text = "AN", Value = "AN" });
            result.Add(new SelectListItem() { Text = "FN", Value = "FN" });
            result.Add(new SelectListItem() { Text = "After Session", Value = "After Session" });
            result.Add(new SelectListItem() { Text = "Before Session", Value = "Before Session" });

            return new SelectList(result, "Value", "Text");
        }

        public static List<PassCategory> GetListPassCategoryList()
        {
            return (List<PassCategory>)Helper.ExecuteService("PublicPass", "GetPassCategoriesForReception", null);
        }

        #endregion

        #region Entity/Model extension methods.
        public static PublicPass ToEntity(this PublicPassModel Model)
        {
            PublicPass entity = new PublicPass()
            {
                AadharID = Model.AadharID,
                Address = Model.Address,
                Age = Model.Age,
                ApprovedBy = Model.ApprovedBy,
                ApprovedDate = Model.ApprovedDate,
                AssemblyCode = Model.AssemblyCode,
                DayTime = Model.DayTime,
                DepartmentID = Model.DepartmentID,
                Designation = Model.Designation,
                Email = Model.Email,
                FatherName = Model.FatherName,
                GateNumber = Model.GateNumber,
                Gender = Model.Gender,
                IsActive = Model.IsActive,
                IsRequested = Model.IsRequested,
                MobileNo = Model.MobileNo,
                Name = Model.Name,
                NoOfPersions = Model.NoOfPersions,
                OrganizationName = Model.OrganizationName,
                PassCategoryID = Model.PassCategoryID,
                PassCode = Model.PassCode,
                PassID = Model.PassID,
                Photo = Model.Photo,
                Prefix = Model.Prefix,
                RecommendationBy = Model.RecommendationBy,
                RecommendationDescription = Model.RecommendationDescription,
                RecommendationType = Model.RecommendationType,
                RequestedBy = Model.RequestedBy,
                RequestedDate = DateTime.Now,
                SessionCode = Model.SessionCode,
                SessionDateTo = Model.SessionDateTo,
                SessionDateFrom = Model.SessionDateFrom,
                Status = Model.Status,
                Time = Model.Time,
                VehicleNumber = Model.VehicleNumber,
                RejectionReason = Model.RejectionReason,
                RejectionDate = Model.RejectionDate,
                DeptApprovalNeeded = Model.DeptApprovalNeeded
            };
            return entity;
        }

        public static PublicPassModel ToModel(this PublicPass entity)
        {
            var Model = new PublicPassModel()
            {
                AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                Email = entity.Email,
                FatherName = entity.FatherName,
                GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,
                MobileNo = entity.MobileNo,
                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                PassCategoryID = entity.PassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                Photo = GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                Status = entity.Status,
                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                RecommendationByName = GetMemberNameByCode(entity.RecommendationBy),
                PassCategoryName = GetPassCategoryNameByID(entity.PassCategoryID),
                DeptApprovalNeeded = entity.DeptApprovalNeeded
            };
            return Model;
        }

        public static mPasses TomPassEntity(this PublicPass entity, int status)
        {
            var mPassEntity = new mPasses()
            {
                Name = entity.Name,
                Email = entity.Email,
                IsActive = entity.IsActive,
                MobileNo = entity.MobileNo,
                AadharID = entity.AadharID,
                OrganizationName = entity.OrganizationName,
                Photo = entity.Photo,
                Gender = entity.Gender,
                Age = entity.Age,
                Designation = entity.Designation,
                FatherName = entity.FatherName,
                Address = entity.Address,
                Status = status,
                ApprovedBy = CurrentSession.UserName,
                ApprovedDate = DateTime.Now,
                AssemblyCode = entity.AssemblyCode,
                GateNumber = entity.GateNumber,
                SessionCode = entity.SessionCode,
                SessionDateFrom = entity.SessionDateFrom,
                SessionDateTo = entity.SessionDateTo,
                IsRequested = entity.IsRequested,
                NoOfPersions = entity.NoOfPersions,
                RequestedPassCategoryID = entity.PassCategoryID,
                PassCode = GeneratePassCode(status, entity.AssemblyCode, entity.SessionCode),
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                IsApproved = true
            };
            return mPassEntity;
        }
        public static Int32 GeneratePassCode(int status, int AssemblyCode, int SessionCode)
        {
            var maxPassCode = (Int32)Helper.ExecuteService("AdministrationBranch", "GetMaxPublicPassCodeForSession", new mPasses { Status = status, AssemblyCode = AssemblyCode, SessionCode = SessionCode });

            if (maxPassCode != 0)
            {

                return maxPassCode;
            }
            else
                return 1;

        }
        public static List<PublicPassModel> ToModelList(this List<PublicPass> entityList)
        {
            var ModelList = entityList.Select(entity => new PublicPassModel()
            {
                AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                Email = entity.Email,
                FatherName = entity.FatherName,
                GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,
                MobileNo = entity.MobileNo,
                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                PassCategoryID = entity.PassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                Photo = entity.Photo,
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                Status = entity.Status,
                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                DeptApprovalNeeded = entity.DeptApprovalNeeded,
                PassCategoryName = GetPassCategoryNameByID(entity.PassCategoryID),
                PassCategoryTemplate = GetPassCategoryTemplateByID(entity.PassCategoryID),
                RecommendationByName = GetMemberNameByCode(entity.RecommendationBy)
            });
            return ModelList.ToList();
        }

        public static List<PublicPassModel> ToModelListForGrid(this List<PublicPass> entityList)
        {
            var ModelList = entityList.Select(entity => new PublicPassModel()
            {
                AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                Email = entity.Email,
                FatherName = entity.FatherName,
                GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,
                MobileNo = entity.MobileNo,
                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                PassCategoryID = entity.PassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                Photo = GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                Status = entity.Status,
                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                DeptApprovalNeeded = entity.DeptApprovalNeeded
            });
            return ModelList.ToList();
        }

        public static List<PublicPassModel> ToModelListWithXML(this List<PublicPass> entityList)
        {
            var ModelList = entityList.Select(entity => new PublicPassModel()
            {
                AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                Email = entity.Email,
                FatherName = entity.FatherName,
                GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,
                MobileNo = entity.MobileNo,
                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                PassCategoryID = entity.PassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                Photo = GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                Status = entity.Status,
                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                DeptApprovalNeeded = entity.DeptApprovalNeeded,
                PassCategoryName = GetPassCategoryNameByID(entity.PassCategoryID),
                PassCategoryTemplate = GetPassCategoryTemplateByID(entity.PassCategoryID),
                RecommendationByName = GetMemberNameByCode(entity.RecommendationBy),
                SignaturePath = entity.SignaturePath,
                xmlDocument = XMLForQRCode(entity)
            });
            return ModelList.ToList();
        }
        #endregion

        #region Speaker Galary Passes
        public static mPasses TomPassEntity(this SpeakerGalaryModel entity, int status)
        {
            var mPassEntity = new mPasses()
            {
                Name = entity.Name,
                Email = entity.Email,
                IsActive = entity.IsActive,
                MobileNo = entity.MobileNo,
                AadharID = entity.AadharID,
                OrganizationName = entity.OrganizationName,
                Photo = entity.Photo,
                Gender = entity.Gender,
                Age = entity.Age,
                Designation = entity.Designation,
                FatherName = entity.FatherName,
                Address = entity.Address,
                Status = status,
                ApprovedBy = CurrentSession.UserName,
                ApprovedDate = DateTime.Now,
                AssemblyCode = entity.AssemblyCode,
                GateNumber = entity.GateNumber,
                SessionCode = entity.SessionCode,
                SessionDateFrom = entity.SessionDateFrom,
                SessionDateTo = entity.SessionDateTo,
                IsRequested = entity.IsRequested,
                NoOfPersions = entity.NoOfPersions,
                RequestedPassCategoryID = entity.PassCategoryID,
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                IsApproved = true
            };
            return mPassEntity;
        }

        public static SpeakerGalaryModel TomPassEntity(this mPasses entity, int status)
        {
            var mPassEntity = new SpeakerGalaryModel()
            {
                Name = entity.Name,
                Email = entity.Email,
                IsActive = entity.IsActive,
                MobileNo = entity.MobileNo,
                AadharID = entity.AadharID,
                OrganizationName = entity.OrganizationName,
                Photo = entity.Photo,
                Gender = entity.Gender,
                Age = entity.Age,
                Designation = entity.Designation,
                FatherName = entity.FatherName,
                Address = entity.Address,
                Status = status,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                GateNumber = entity.GateNumber,
                SessionCode = entity.SessionCode,
                SessionDateFrom = entity.SessionDateFrom,
                SessionDateTo = entity.SessionDateTo,
                IsRequested = entity.IsRequested,
                NoOfPersions = entity.NoOfPersions,
                RequestedPassCategoryID = entity.RequestedPassCategoryID,
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                IsApproved = entity.IsApproved
            };
            return mPassEntity;
        }

        public static List<SpeakerGalaryModel> ToModelList(this List<mPasses> entityList)
        {
            var ModelList = entityList.Select(entity => new SpeakerGalaryModel()
            {
                AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                Email = entity.Email,
                FatherName = entity.FatherName,
                GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,
                MobileNo = entity.MobileNo,
                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                RequestedPassCategoryID = entity.RequestedPassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                Photo = entity.Photo,
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                Status = entity.Status,
                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                ApprovedPassCategoryID = entity.ApprovedPassCategoryID,
                PassCategoryName = GetPassCategoryNameByID(entity.ApprovedPassCategoryID),
                PassCategoryTemplate = GetPassCategoryTemplateByID(entity.ApprovedPassCategoryID)
            });
            return ModelList.ToList();
        }

        public static SpeakerGalaryModel ToModel(this mPasses entity)
        {
            var Model = new SpeakerGalaryModel()
            {
                AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                Email = entity.Email,
                FatherName = entity.FatherName,
                GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,
                MobileNo = entity.MobileNo,
                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                RequestedPassCategoryID = entity.RequestedPassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                Photo = entity.Photo,
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                Status = entity.Status,
                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                RequestedPassCategoryName = GetPassCategoryNameByID(entity.RequestedPassCategoryID),
                AllowedPassCategoryName = GetPassCategoryNameByID(entity.ApprovedPassCategoryID),
                ListPassCategory = GetListPassCategoryList(),
                RecommendationByName = GetMemberNameByCode(entity.RecommendationBy)
            };
            return Model;
        }
        #endregion


        #region Helper Methods

        public static string XMLForQRCode(PublicPass model)
        {
            XElement passParentNode = new System.Xml.Linq.XElement("PassCode");

            XElement passChildChildNode1 = new System.Xml.Linq.XElement("Name");
            passParentNode.Add(passChildChildNode1);

            passParentNode.SetAttributeValue("ID", model.PassCode);

            string information = model.Name + (model.Gender + "," + Convert.ToString(model.Age) + " Years");

            passChildChildNode1.ReplaceNodes(model.Name != null ? information : "");

            return passParentNode.ToString();
        }

        public static DateTime GetSessionDateByID(int sessionDateID)
        {
            //return (DateTime)Helper.ExecuteService("PublicPass", "GetSessionDateByID", new PublicPass { SessionDateID = sessionDateID });
            return DateTime.Now;
        }

        public static string GetMemberNameByCode(string memberCode)
        {
            return (string)Helper.ExecuteService("PublicPass", "GetMemberNameByCode", new PublicPass { RecommendationBy = memberCode });
            //return null;
        }

        public static string GetPassCategoryNameByID(int passCategoryID)
        {
            if (ListPassCategoryList.Count == 0)
            {
                ListPassCategoryList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
            }

            var passCategoryModel = (PassCategory)ListPassCategoryList.Where(m => m.PassCategoryID == passCategoryID).FirstOrDefault();

            if (passCategoryModel != null)
            {
                return passCategoryModel.Name;
            }
            return "";
        }

        public static string GetPassCategoryTemplateByID(int passCategoryID)
        {
            if (ListPassCategoryList.Count == 0)
            {
                ListPassCategoryList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
            }

            var passCategoryModel = (PassCategory)ListPassCategoryList.Where(m => m.PassCategoryID == passCategoryID).FirstOrDefault();

            if (passCategoryModel != null)
            {
                return passCategoryModel.Template;
            }
            return "";
        }
        public static string GetImagePath(int AssemblyCode, int SesionCode, string StrPhoto)
        {
            var PassFileLocalServerPath = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "PassFileLocalPath", null);
            var PassFileLocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "PassFileLocation", null);
            var GetSecureFileSettingLocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            string Photo = GetSecureFileSettingLocation.SettingValue + PassFileLocation.SettingValue + AssemblyCode + "/" + SesionCode + "/" + StrPhoto;
            return Photo;
        }
        public static string GetImageLocation(int AssemblyCode, int SesionCode)
        {
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            var PassFileLocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "PassFileLocation", null);
            string Photo = FileSettings.SettingValue + PassFileLocation.SettingValue + AssemblyCode + "/" + SesionCode + "/";
            return Photo;
        }
        //public static mPasses PPassTomPassEntity(this PublicPass Model)
        //{
        //    mPasses entity = new mPasses()
        //    {
        //        AadharID = Model.AadharID,
        //        Address = Model.Address,
        //        Age = Model.Age,
        //        ApprovedBy = Model.ApprovedBy,
        //        ApprovedDate = Model.ApprovedDate,
        //        AssemblyCode = Model.AssemblyCode,
        //        DayTime = Model.DayTime,
        //        DepartmentID = Model.DepartmentID,
        //        Designation = Model.Designation,
        //        Email = Model.Email,
        //        FatherName = Model.FatherName,
        //        GateNumber = Model.GateNumber,
        //        Gender = Model.Gender,
        //        IsActive = Model.IsActive,
        //        IsRequested = Model.IsRequested,
        //        MobileNo = Model.MobileNo,
        //        Name = Model.Name,
        //        NoOfPersions = Model.NoOfPersions,
        //        OrganizationName = Model.OrganizationName,
        //        PassCategoryID = Model.PassCategoryID,
        //        PassCode = Model.PassCode,
        //        PassID = Model.PassID,
        //        Photo = Model.Photo,
        //        Prefix = Model.Prefix,
        //        RecommendationBy = Model.RecommendationBy,
        //        RecommendationDescription = Model.RecommendationDescription,
        //        RecommendationType = Model.RecommendationType,
        //        RequestedBy = Model.RequestedBy,
        //        RequestedDate = Model.RequestedDate,
        //        SessionCode = Model.SessionCode,
        //        SessionDateTo = Model.SessionDateTo,
        //        SessionDateFrom = Model.SessionDateFrom,
        //        Status = Model.Status,
        //        Time = Model.Time,
        //        VehicleNumber = Model.VehicleNumber
        //    };
        //    return entity;
        //}

        #endregion
    }
}