﻿namespace SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Models
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using System.Web.Mvc;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Session;
    using System.ComponentModel.DataAnnotations.Schema;
    #endregion

    public class PublicPassDashboardModel
    {
        //Pass List of the Model to the 
        public List<PublicPassModel> PublicPassList { get; set; }

        //Heading Properties
        public string Heading { get; set; }
        public string BreadCrumbsHeading { get; set; }

        //Accordian Properties.
        public int ActiveTab { get; set; }

        public int Status { get; set; }

        public string AssemblyName { get; set; }

        public string SessionName { get; set; }

        //For List Of Assembly and Session-- sanjay
        [HiddenInput(DisplayValue = false)]
        public int AssemblyId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SessionId { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mAssembly> mAssemblyList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mSession> sessionList { get; set; }
    }
}