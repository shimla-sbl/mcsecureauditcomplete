﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Models
{
    public class PartialModel
    {
        public PublicPassDashboardModel PublicPassDashboardModel { get; set; }
        public PublicPassModel PublicPassModel { get; set; }
    }
}