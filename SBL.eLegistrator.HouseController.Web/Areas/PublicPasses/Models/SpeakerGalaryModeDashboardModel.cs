﻿namespace SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Models
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    #endregion

    public class SpeakerGalaryDashboardModel
    {
        //Pass List of the Model to the 
        public List<SpeakerGalaryModel> SpeakerGalaryPassList { get; set; }

        //Heading Properties
        public string Heading { get; set; }
        public string BreadCrumbsHeading { get; set; }

        //Accordian Properties.
        public int ActiveTab { get; set; }

        public int Status { get; set; }
    }
}