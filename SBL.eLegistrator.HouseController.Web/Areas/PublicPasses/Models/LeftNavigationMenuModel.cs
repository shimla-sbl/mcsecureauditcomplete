﻿namespace SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Models
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    #endregion

    public class LeftNavigationMenuModel
    {
        public int TotalPublicPassCount { get; set; }

        public int PendingPublicPassCount { get; set; }

        public int ApprovedPublicPassCount { get; set; }

        public int RejectedPublicPassCount { get; set; }

        public int PendingID { get; set; }

        public int ApprovedID { get; set; }

        public int RejectedID { get; set; }
    }
}