﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PublicPasses
{
    public class PublicPassesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PublicPasses";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PublicPasses_default",
                "PublicPasses/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
