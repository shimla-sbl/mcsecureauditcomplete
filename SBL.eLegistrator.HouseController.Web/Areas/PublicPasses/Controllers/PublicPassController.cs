﻿namespace SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Controllers
{
    #region Namespace Reffrences
    using SBL.DomainModel.Models.Adhaar;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.Officers;
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.Ministery;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.eLegistrator.HouseController.Filters;
    using SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions;
    using SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Models;
    using SBL.eLegistrator.HouseController.Web.Filters;
    using SBL.eLegistrator.HouseController.Web.Helpers;
    using SBL.eLegistrator.HouseController.Web.Utility;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using SBL.eLegislator.HPMS.ServiceAdaptor;
    #endregion

    [SBLAuthorize(Allow = "Authenticated")]
    [Audit]
    [NoCache]
    public class PublicPassController : Controller
    {
        //
        // GET: /PublicPasses/PublicPass/
        static string CapturedImage = "";
        static int SessionCode;
        static int AssemblyCode;
        string RoleID = "0FC60E41-C78E-46B0-B8AC-F6EB708BA119";
        public ActionResult Index(int Status = 1)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            // SessionCode = siteSettingMod.SessionCode;
            // AssemblyCode = siteSettingMod.AssemblyCode;
            PublicPass Ppass = new PublicPass();
            PublicPassDashboardModel model = new PublicPassDashboardModel();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                Ppass.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Ppass.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
            }

            var model1 = (PublicPass)Helper.ExecuteService("PublicPass", "GetAdminAssemblyList", Ppass);

            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model1.AssemblyCode.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model1.SessionCode.ToString();
            }
            SessionCode = model1.SessionCode;
            AssemblyCode = model1.AssemblyCode;
            mSession Mdl = new mSession();
            Mdl.SessionCode = SessionCode;
            Mdl.AssemblyID = AssemblyCode;
            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = AssemblyCode;

            var SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
            var AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

            var PublicPass = new List<PublicPassModel>();
            List<PublicPass> passesByStatus = new List<PublicPass>();
            var passesByStatusList = (List<PublicPass>)Helper.ExecuteService("PublicPass", "GetPassesByStatus", new PublicPass { Status = Status, AssemblyCode = AssemblyCode, SessionCode = SessionCode });

            if (Status == 1)
            {
                if (CurrentSession.RoleID == RoleID.ToLower())
                {
                    passesByStatus = passesByStatusList.Where(m => m.PassCategoryID == 9 || m.PassCategoryID == 20 || m.PassCategoryID == 5 || m.PassCategoryID == 10 || m.PassCategoryID == 8 || m.PassCategoryID == 11).ToList();
                }
                else
                {
                    passesByStatus = passesByStatusList.Where(m => m.PassCategoryID == 17 || m.PassCategoryID == 7).ToList();
                }
            }
            else
            {
                if (CurrentSession.RoleID == RoleID.ToLower())
                {
                    passesByStatus = passesByStatusList.Where(m => m.PassCategoryID == 9 || m.PassCategoryID == 20 || m.PassCategoryID == 5 || m.PassCategoryID == 10 || m.PassCategoryID == 8 || m.PassCategoryID == 11).ToList();
                }
                else
                {
                    ////////////////////Approved Reception.. Public Pass List Current Date Wise-----Added by sanjay
                    List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
                    mParameter.Add(new KeyValuePair<string, string>("@Assembly", AssemblyCode.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@Session", SessionCode.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@Status", "2"));
                    DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "usp_ApprovedPublicPass", mParameter);
                    List<PublicPass> passesByA = new List<PublicPass>();
                    if (dataDate != null)
                    {
                        PublicPass PublicPas = new PublicPass();
                        foreach (DataRow dr in dataDate.Tables[0].Rows)
                        {
                            var empList = dataDate.Tables[0].AsEnumerable().Select(dataRow => new PublicPass
                            {
                                PassID = dataRow.Field<Int32>("PassID"),
                                PassCode = dataRow.Field<Int32 ?>("PassCode"),
                                PassCategoryID = dataRow.Field<Int32>("PassCategoryID"),
                                Name = dataRow.Field<string>("Name"),
                                Age = dataRow.Field<Int32 ?>("Age"),
                                Gender = dataRow.Field<string>("Gender"),
                                FatherName = dataRow.Field<string>("FatherName"),
                                MobileNo = dataRow.Field<string>("MobileNo"),
                                AadharID = dataRow.Field<string>("AadharID"),
                                Address = dataRow.Field<string>("Address"),
                                ApprovedDate = dataRow.Field<DateTime>("ApprovedDate"),
                                Photo = dataRow.Field<string>("Photo"),
                                AssemblyCode = dataRow.Field<Int32>("AssemblyCode"),
                                SessionCode = dataRow.Field<Int32>("SessionCode"),
                                SessionDateFrom = dataRow.Field<string>("SessionDateFrom"),
                                SessionDateTo = dataRow.Field<string>("SessionDateTo"),
                                ApprovedBy = dataRow.Field<string>("ApprovedBy")

                            });
                            passesByStatus = empList.ToList();
                        }

                    }

                }
            }
            var passListForView = passesByStatus.ToModelListForGrid();
            switch (Status)
            {
                //case 0: { PublicPass = GetResultsAsList(1); break; }
                case 1: { PublicPass = passListForView.OrderByDescending(m => m.PassID).ToList(); break; }
                case 2: { PublicPass = passListForView.OrderByDescending(m => m.PassCode).ToList(); break; }
                case 3: { PublicPass = passListForView; break; }
            }


            model.PublicPassList = PublicPass;
            model.Heading = (Status == 1) ? "Pending" : (Status == 2) ? "Approved" : (Status == 3) ? "Rejected" : "All Public Passes";
            model.BreadCrumbsHeading = (Status == 1) ? "Pending" : (Status == 2) ? "Approved" : (Status == 3) ? "Rejected" : "All Public Passes";
            model.ActiveTab = 0;
            model.Status = Status;
            model.SessionName = model1.SessionName;
            model.AssemblyName = model1.AssesmblyName;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;
            model.AssemblyId = model1.AssemblyCode;
            model.SessionId = model1.SessionCode;


            if (!Request.IsAjaxRequest())
            {
                return View(model);
            }
            else
            {
                return View(model);
            }
        }

        public PartialViewResult LeftNavigationMenu()
        {

            List<PublicPass> PublicList = new List<PublicPass>();
            var PendingCount = 0;
            var ApprovedCount = 0;
            var RejectedCount = 0;
            var PendingList = (List<PublicPass>)Helper.ExecuteService("PublicPass", "MenuItemCountByType", new PublicPass { Status = 1, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
            var ApprovedList = (List<PublicPass>)Helper.ExecuteService("PublicPass", "MenuItemCountByType", new PublicPass { Status = 2, AssemblyCode = AssemblyCode, SessionCode = SessionCode });

            var RejectedList = (List<PublicPass>)Helper.ExecuteService("PublicPass", "MenuItemCountByType", new PublicPass { Status = 3, AssemblyCode = AssemblyCode, SessionCode = SessionCode });

            if (CurrentSession.RoleID == RoleID.ToLower())
            {
                PendingList = PendingList.Where(m => m.PassCategoryID == 9 || m.PassCategoryID == 20 || m.PassCategoryID == 5 || m.PassCategoryID == 10 || m.PassCategoryID == 8 || m.PassCategoryID == 11).ToList();
                ApprovedList = ApprovedList.Where(m => m.PassCategoryID == 9 || m.PassCategoryID == 20 || m.PassCategoryID == 5 || m.PassCategoryID == 10 || m.PassCategoryID == 8 || m.PassCategoryID == 11).ToList();
                RejectedList = RejectedList.Where(m => m.PassCategoryID == 9 || m.PassCategoryID == 20 || m.PassCategoryID == 5 || m.PassCategoryID == 10 || m.PassCategoryID == 8 || m.PassCategoryID == 11).ToList();
                PendingCount = PendingList.Count;
                ApprovedCount = ApprovedList.Count;
                RejectedCount = RejectedList.Count;
            }
            else
            {
                PendingList = PendingList.Where(m => m.PassCategoryID == 17 || m.PassCategoryID == 7).ToList();
                ApprovedList = ApprovedList.Where(m => m.PassCategoryID == 17 || m.PassCategoryID == 7).ToList();
                RejectedList = RejectedList.Where(m => m.PassCategoryID == 17 || m.PassCategoryID == 7).ToList();
                PendingCount = PendingList.Count;
                ApprovedCount = ApprovedList.Count;
                RejectedCount = RejectedList.Count;
            }
            // var PendingCount = (int)Helper.ExecuteService("PublicPass", "MenuItemCountByType", new PublicPass { Status = 1, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
            //var ApprovedCount = (int)Helper.ExecuteService("PublicPass", "MenuItemCountByType", new PublicPass { Status = 2, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
            // var RejectedCount = (int)Helper.ExecuteService("PublicPass", "MenuItemCountByType", new PublicPass { Status = 3, AssemblyCode = AssemblyCode, SessionCode = SessionCode });

            var model = new LeftNavigationMenuModel()
            {
                TotalPublicPassCount = PendingCount + ApprovedCount + RejectedCount,
                PendingPublicPassCount = PendingCount,
                ApprovedPublicPassCount = ApprovedCount,
                RejectedPublicPassCount = RejectedCount
            };
            return PartialView("_LeftNavigationMenu", model);
        }

        public ActionResult CreateNewPublicPass()
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            //Get Current Session and Assembly Information
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            //PublicPass pubPassModel = new PublicPass();
            //pubPassModel.AssemblyCode = siteSettingMod.AssemblyCode;

            AssemblyCode = siteSettingMod.AssemblyCode;
            SessionCode = siteSettingMod.SessionCode;

            //var memList = Helper.ExecuteService("PublicPass", "GetRecomendedByList", pubPassModel) as List<mMember>;

            var model = new PublicPassModel
            {
                AssemblyCode = AssemblyCode,
                SessionCode = SessionCode,
                Status = 1,
                Mode = "Add",
                NoOfPersions = 1
            };
            model = this.ControlsBinderCreateEditPublicPass(model);

            PublicPass Ppass = new PublicPass();
            var model1 = (PublicPass)Helper.ExecuteService("PublicPass", "GetAdminAssemblyList", Ppass);
            PublicPassDashboardModel model2 = new PublicPassDashboardModel();

            model2.mAssemblyList = model1.mAssemblyList;
            model2.sessionList = model1.sessionList;
            model2.SessionId = model1.SessionCode;
            model2.AssemblyId = model1.AssemblyCode;
            model2.AssemblyName = model1.AssesmblyName;
            model2.SessionName = model1.SessionName;

            PartialModel ptm = new PartialModel();
            ptm.PublicPassModel = model;
            ptm.PublicPassDashboardModel = model2;
            return View("_CreateNewPublicPass", ptm);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult CreateNewPublicPass(PartialModel models, string value)
        {
            PublicPassModel model = models.PublicPassModel;

            
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            if (model != null)
            {
                var modelToPass = model.ToEntity();

                string disallowedList = ConfigurationManager.AppSettings["disallowReceptionist"];
                string[] disallowedIDString = disallowedList.Split(',');

                int[] disallowedIDInts = Array.ConvertAll(disallowedIDString, s => int.Parse(s.Trim()));

                if (disallowedIDInts.Contains(modelToPass.PassCategoryID))
                {
                    modelToPass.DeptApprovalNeeded = true;
                }
                else
                {
                    modelToPass.DeptApprovalNeeded = false;
                }

                if (model.Mode == "Add")
                {

                    DataSet dataSetPubLicPass = null;
                    List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
                    mParameter.Add(new KeyValuePair<string, string>("@Assembly", AssemblyCode.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@Session", SessionCode.ToString()));
                    mParameter.Add(new KeyValuePair<string, string>("@PassCategoryID", Convert.ToString(modelToPass.PassCategoryID)));
                    mParameter.Add(new KeyValuePair<string, string>("@Prefix", Convert.ToString(modelToPass.Prefix)));
                    mParameter.Add(new KeyValuePair<string, string>("@AadharID", Convert.ToString(modelToPass.AadharID)));
                    mParameter.Add(new KeyValuePair<string, string>("@Name", Convert.ToString(modelToPass.Name)));
                    mParameter.Add(new KeyValuePair<string, string>("@Gender", Convert.ToString(modelToPass.Gender)));
                    mParameter.Add(new KeyValuePair<string, string>("@Age", Convert.ToString(modelToPass.Age)));
                    mParameter.Add(new KeyValuePair<string, string>("@FatherName", Convert.ToString(modelToPass.FatherName)));
                    mParameter.Add(new KeyValuePair<string, string>("@NoOfPersions", Convert.ToString(modelToPass.NoOfPersions)));
                    mParameter.Add(new KeyValuePair<string, string>("@RecommendationType", Convert.ToString(modelToPass.RecommendationType)));
                    mParameter.Add(new KeyValuePair<string, string>("@RecommendationBy", Convert.ToString(modelToPass.RecommendationBy)));
                    mParameter.Add(new KeyValuePair<string, string>("@RecommendationDescription", Convert.ToString(modelToPass.RecommendationDescription)));
                    mParameter.Add(new KeyValuePair<string, string>("@MobileNo", Convert.ToString(modelToPass.MobileNo)));
                    mParameter.Add(new KeyValuePair<string, string>("@Email", Convert.ToString(modelToPass.Email)));
                    mParameter.Add(new KeyValuePair<string, string>("@Address", Convert.ToString(modelToPass.Address)));
                    mParameter.Add(new KeyValuePair<string, string>("@DayTime", Convert.ToString(modelToPass.DayTime)));
                    mParameter.Add(new KeyValuePair<string, string>("@Time", Convert.ToString(modelToPass.Time)));
                    mParameter.Add(new KeyValuePair<string, string>("@SessionDateFrom", Convert.ToString(modelToPass.SessionDateFrom)));
                    mParameter.Add(new KeyValuePair<string, string>("@SessionDateTo", Convert.ToString(modelToPass.SessionDateTo)));
                    mParameter.Add(new KeyValuePair<string, string>("@GateNumber", Convert.ToString(modelToPass.GateNumber)));
                    mParameter.Add(new KeyValuePair<string, string>("@IsRequested", Convert.ToString(1)));
                    mParameter.Add(new KeyValuePair<string, string>("@RequestedDate", Convert.ToString(System.DateTime.Now)));
                    mParameter.Add(new KeyValuePair<string, string>("@RequestedBy", Convert.ToString(modelToPass.RequestedBy)));
                    mParameter.Add(new KeyValuePair<string, string>("@IsActive", Convert.ToString(1)));
                    mParameter.Add(new KeyValuePair<string, string>("@Photo", Convert.ToString(modelToPass.Photo)));
                    mParameter.Add(new KeyValuePair<string, string>("@DeptApprovalNeeded", Convert.ToString(modelToPass.DeptApprovalNeeded)));
                    if (CurrentSession.RoleID == RoleID.ToLower())
                    {
                        modelToPass.Status = 1;
                        mParameter.Add(new KeyValuePair<string, string>("@Status", Convert.ToString(modelToPass.Status)));
                        dataSetPubLicPass = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "sp_CreateNewPublicPass", mParameter);
                        if (dataSetPubLicPass != null)
                        {
#pragma warning disable CS0219 // The variable 'Msg' is assigned but its value is never used
                            var Msg = "Data Save Successfully!!";
#pragma warning restore CS0219 // The variable 'Msg' is assigned but its value is never used
                        }

                    }
                    else
                    {
                        string Action = Request.Form["Action"];                     


                        if (Action == "ApproveAndPrint")
                        {
                            #region Save&Approved Public Pass List
                            //Save and approved Public Pass List 
                            modelToPass.Status = 2;
                            modelToPass.IsApproved = true;
                            modelToPass.ApprovedBy = CurrentSession.UserName;
                            modelToPass.ApprovedDate = DateTime.Now.Date;

                            mParameter.Add(new KeyValuePair<string, string>("@Status", Convert.ToString(modelToPass.Status)));
                            mParameter.Add(new KeyValuePair<string, string>("@ApprovedDate", Convert.ToString(modelToPass.ApprovedDate)));
                            mParameter.Add(new KeyValuePair<string, string>("@ApprovedBy", Convert.ToString(modelToPass.ApprovedBy)));                       
                            dataSetPubLicPass = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "sp_CreateAndApproveNewPublicPass", mParameter);
                            
                            //var savedRecord = Helper.ExecuteService("PublicPass", "CreateNewPublicPass", modelToPass);
                           
                            if(dataSetPubLicPass!=null)
                            {
                                #region Print Public Pass
                                List<PublicPass> PrintPreviewModel = new List<PublicPass>();
                                //int IDForPass = PublicPassID;
                                // var PrintpublicPass = (PublicPass)Helper.ExecuteService("PublicPass", "GetPublicPassById", new PublicPass { PassID = PublicPassID });
                                modelToPass.SignaturePath = CurrentSession.SignaturePath;
                                modelToPass.PassCode =  Convert.ToInt32(Convert.ToString(dataSetPubLicPass.Tables[0].Rows[0]["PassCode"]));
                                PrintPreviewModel.Add(modelToPass);
                                //PrintPreviewModel.Add(publicPass);
                                return PartialView("_PrintPasses", PrintPreviewModel.ToModelListWithXML());
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            modelToPass.Status = 1;
                            mParameter.Add(new KeyValuePair<string, string>("@Status", Convert.ToString(modelToPass.Status)));                         
                            dataSetPubLicPass = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "sp_CreateNewPublicPass", mParameter);
                            if (dataSetPubLicPass != null)
                            {
#pragma warning disable CS0219 // The variable 'Msg' is assigned but its value is never used
                                var Msg = "Data Save Successfully!!";
#pragma warning restore CS0219 // The variable 'Msg' is assigned but its value is never used
                            }                            
                        }
                    }
                }
                else
                {
                    modelToPass.Photo = model.ImagePath;
                    var updatedRecord = Helper.ExecuteService("PublicPass", "UpdatePublicPass", modelToPass);
                    TempData["DutyUserAdd"] = "Update";
                }
                #region Page Return According to Opperation
                if (model.Mode == "Add")
                {
                    string Action = Request.Form["Action"];
                    if (Action == "Save")
                    {
                        return Json("Save", JsonRequestBehavior.AllowGet);
                    }
                    else if (Action == "create&new")
                    {
                        return Json("create&new", JsonRequestBehavior.AllowGet);
                    }

                }
#pragma warning disable CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                else if (TempData["DutyUserAdd"] == "Update")
#pragma warning restore CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                {

                    return Json("Update", JsonRequestBehavior.AllowGet);

                }
                #endregion


            }

            #region If Save will fail
            model = ControlsBinderCreateEditPublicPass(model);
            #endregion

            return View("_CreateNewPublicPass", model);
        }
        [HttpGet]
        public ActionResult EditPublicPass(int Id)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            var publicPass = (PublicPass)Helper.ExecuteService("PublicPass", "GetPublicPassById", new PublicPass { PassID = Id });
            var PhoName = publicPass.Photo;
            var publicPassModel = publicPass.ToModel();
            publicPassModel.ImagePath = PhoName;
            publicPassModel = this.ControlsBinderCreateEditPublicPass(publicPassModel);
            PublicPass Ppass = new PublicPass();
            var model1 = (PublicPass)Helper.ExecuteService("PublicPass", "GetAdminAssemblyList", Ppass);
            PublicPassDashboardModel model2 = new PublicPassDashboardModel();
            //Only for Edit Time SessionDateFrom and SessionDateTo Show Exist Database date
            publicPassModel.SessionDateFrom = publicPass.SessionDateFrom;
            publicPassModel.SessionDateTo = publicPass.SessionDateTo;
            ///// End 
            model2.mAssemblyList = model1.mAssemblyList;
            model2.sessionList = model1.sessionList;
            model2.SessionId = model1.SessionCode;
            model2.AssemblyId = model1.AssemblyCode;
            model2.AssemblyName = model1.AssesmblyName;
            model2.SessionName = model1.SessionName;
            PartialModel ptm = new PartialModel();
            ptm.PublicPassModel = publicPassModel;
            ptm.PublicPassDashboardModel = model2;
            return View("_CreateNewPublicPass", ptm);
        }

        public ActionResult FullPassDetails(int Id)
        {
            var publicPass = (PublicPass)Helper.ExecuteService("PublicPass", "GetPublicPassById", new PublicPass { PassID = Id });
            var publicPassModel = publicPass.ToModel();
            return View("_FullPassDetails", publicPassModel);
        }

        #region Approve Public Pass
        [HttpGet]
        public ActionResult ApprovePublicPass(int passID)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            int status = 1;
            try
            {
                if (passID != 0)
                {
                    var publicPass = (PublicPass)Helper.ExecuteService("PublicPass", "GetPublicPassById", new PublicPass { PassID = passID });
                    status = publicPass.Status;

                    var publicPassModel = publicPass;
                    publicPassModel.IsApproved = true;
                    publicPassModel.ApprovedBy = CurrentSession.UserName;
                    publicPassModel.ApprovedDate = DateTime.Now;
                    // publicPassModel.PassCode = publicPass.PassID;
                    publicPassModel.Status = 2;


                    //Save this approved Entity in the mPasses Table.
                    var mPassFromDeptPass = publicPass.TomPassEntity(publicPassModel.Status);

                    mPassFromDeptPass.ApprovedPassCategoryID = Convert.ToInt32(mPassFromDeptPass.RequestedPassCategoryID);

                    string PassCategory = mPassFromDeptPass.RecommendationType;


                    int mPassCode = ExtensionMethods.GeneratePassCode(2, AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId), SessionCode = Convert.ToInt32(CurrentSession.SessionId));
                    string mergecode = mPassCode > 1 ? mPassCode.ToString() : string.Format("{0}{1}{2}", CurrentSession.SessionId, String.Format("{0:00}", DateTime.Now.Month), mPassCode);
                    mPassFromDeptPass.PassCode = Convert.ToInt32(mergecode);
                    publicPassModel.PassCode = Convert.ToInt32(mergecode);



                    ////string PassValue = string.Format("{0}{1}", DateTime.Now.Year, String.Format("{0:00}", DateTime.Now.Month));
                    //int? PassCode = Convert.ToInt32(mPassFromDeptPass.PassCode);
                    //PassCode=PassCode.HasValue?PassCode.Value:1;
                    //string PassNew = PassCode.HasValue ? PassCode.ToString() : null;
                    //string mergecode = PassCode>1?PassCode.Value.ToString(): string.Format("{0}{1}{2}", CurrentSession.SessionId, String.Format("{0:00}", DateTime.Now.Month), PassCode);
                    //mPassFromDeptPass.PassCode = Convert.ToInt32(mergecode);
                    //publicPassModel.PassCode = Convert.ToInt32(mergecode);


                    var updatedRecord = (PublicPass)Helper.ExecuteService("PublicPass", "UpdatePublicPass", publicPassModel);
                    var masterPass = Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPassForPublic", mPassFromDeptPass) as mPasses;

                    masterPass.PassCode = publicPassModel.PassCode;
                    Helper.ExecuteService("AdministrationBranch", "UpdateMasterPass", masterPass);

                    string URL = string.Empty;
                    if (CurrentSession.UserID == null)
                    {
                        URL = "/Account/Login";
                        //URL = "/Account/Login?returnUrl";
                    }
                    else
                    {
                        URL = "/PublicPasses/PublicPass/Index?Status=" + status;
                    }

                    return Json(URL, JsonRequestBehavior.AllowGet);
                }
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var errMsg = ex.Message;
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        public object ApproveSelectedPublicPass(string PassIDs)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            string[] PassIds = PassIDs.Split(',');
            int status = 1;
            foreach (var passID in PassIds)
            {
                int IDForPass = Convert.ToInt32(passID);
                var publicPass = (PublicPass)Helper.ExecuteService("PublicPass", "GetPublicPassById", new PublicPass { PassID = IDForPass });
                status = publicPass.Status;
                var publicPassModel = publicPass;
                publicPassModel.IsApproved = true;
                publicPassModel.ApprovedBy = CurrentSession.UserName;
                publicPassModel.ApprovedDate = DateTime.Now;
                //publicPassModel.PassCode = publicPass.PassID;
                publicPassModel.Status = 2;
                //var updatedRecord = (PublicPass)Helper.ExecuteService("PublicPass", "UpdatePublicPass", publicPassModel);

                //Save this approved Entity in the mPasses Table.
                var mPassFromDeptPass = publicPass.TomPassEntity(publicPassModel.Status);



                mPassFromDeptPass.ApprovedPassCategoryID = Convert.ToInt32(mPassFromDeptPass.RequestedPassCategoryID);
                string PassCategory = mPassFromDeptPass.RecommendationType;


                ////string PassValue = string.Format("{0}{1}", DateTime.Now.Year, String.Format("{0:00}", DateTime.Now.Month));
                int? PassCode = Convert.ToInt32(mPassFromDeptPass.PassCode);

                //string PassNew = PassCode.HasValue ? PassCode.ToString() : null;
                //string mergecode = string.Format("{0}{1}{2}", CurrentSession.SessionId, String.Format("{0:00}", DateTime.Now.Month), PassCode);


                int mPassCode = ExtensionMethods.GeneratePassCode(2, AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId), SessionCode = Convert.ToInt32(CurrentSession.SessionId));
                string mergecode = mPassCode > 1 ? mPassCode.ToString() : string.Format("{0}{1}{2}", CurrentSession.SessionId, String.Format("{0:00}", DateTime.Now.Month), mPassCode);
                mPassFromDeptPass.PassCode = Convert.ToInt32(mergecode);
                publicPassModel.PassCode = Convert.ToInt32(mergecode);


                var updatedRecord = (PublicPass)Helper.ExecuteService("PublicPass", "UpdatePublicPass", publicPassModel);
                var masterPass = Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPassForPublic", mPassFromDeptPass) as mPasses;

                masterPass.PassCode = publicPassModel.PassCode;
                Helper.ExecuteService("AdministrationBranch", "UpdateMasterPass", masterPass);
            }
            string URL = "/PublicPasses/PublicPass/Index?Status=" + status;
            return Json(URL, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintSelectedPublicPass(string PassIDs)
        {
            List<PublicPassModel> PrintPreview = new List<PublicPassModel>();
            try
            {
                string[] PassIds = PassIDs.Split(',');
                List<PublicPass> PrintPreviewModel = new List<PublicPass>();
                int AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
                int SessioCode = Convert.ToInt32(CurrentSession.SessionId);
                foreach (var passID in PassIds)
                {
                    int IDForPass = Convert.ToInt32(passID);
                    var publicPass = (PublicPass)Helper.ExecuteService("PublicPass", "GetPublicPassById", new PublicPass { PassID = IDForPass, AssemblyCode = AssemblyCode, SessionCode = SessioCode });
                    PrintPreviewModel.Add(publicPass);
                }
                //PrintPreview.AddRange(PrintPreviewModel.ToModelList());

                return PartialView("_PrintPasses", PrintPreviewModel.ToModelListWithXML());
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw;
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ApprovePublicPass(PublicPassModel model)
        {
            try
            {
                //var publicPass = (PublicPass)Helper.ExecuteService("PublicPass", "GetPublicPassById", new PublicPass { PublicPassID = model.PublicPassID });
                //var publicPassModel = publicPass;
                //publicPassModel.IsApproved = true;
                //publicPassModel.ApprovedBy = CurrentSession.UserID;
                //publicPassModel.ApprovedDate = DateTime.Now;
                //publicPassModel.AdmissionCardNo = publicPassModel.AssemblyCode + "/" + publicPassModel.SessionCode + "/" + publicPassModel.PublicPassID;
                //publicPassModel.Status = 2;
                //publicPass.AllowedSessionDateID = model.AllowedSessionDateID;

                //var updatedRecord = Helper.ExecuteService("PublicPass", "UpdatePublicPass", publicPassModel);

                return RedirectToAction("Index", new { Status = model.Status });
            }
            catch (Exception ex)
            {
                var errMsg = ex.Message;
                return RedirectToAction("Index");
            }
        }


        #endregion

        #region Reject Public Pass
        [HttpGet]
        public ActionResult RejectPublicPass(int publicPassID)
        {
            PublicPassModel model = new PublicPassModel();
            try
            {
                if (publicPassID != 0)
                {
                    var publicPass = (PublicPass)Helper.ExecuteService("PublicPass", "GetPublicPassById", new PublicPass { PassID = publicPassID });

                    model = publicPass.ToModel();
                }
                return PartialView("_RejectPublicPass", model);
            }
            catch (Exception ex)
            {
                var errMsg = ex.Message;
                return RedirectToAction("Index");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult RejectPublicPass(PublicPassModel model)
        {
            var publicPass = (PublicPass)Helper.ExecuteService("PublicPass", "GetPublicPassById", new PublicPass { PassID = model.PassID });
            publicPass.RejectionReason = model.RejectionReason;
            publicPass.RejectionDate = DateTime.Now;
            publicPass.Status = 3;
            publicPass.IsApproved = false;

            var updatedRecord = Helper.ExecuteService("PublicPass", "UpdatePublicPass", publicPass);
            return RedirectToAction("Index", new { Status = model.Status });
        }
        #endregion

        public ActionResult GetRecomendedByOfficerList()
        {
            PublicPassModel model = new PublicPassModel();
            var officerList = Helper.ExecuteService("PublicPass", "GetRecomendedByOfficerList", model.ToEntity()) as List<mOfficerDetails>;
            model.RecomendedByListOfficer = officerList;
            return Json(model.RecomendedByListOfficer, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRecomendedByMemList()
        {
            PublicPass pubPassModel = new PublicPass();
            pubPassModel.AssemblyCode = AssemblyCode;

            var memList = Helper.ExecuteService("PublicPass", "GetRecomendedByList", pubPassModel) as List<mMember>;

            return Json(memList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRecomendedByMinList()
        {
            PublicPass pubPassModel = new PublicPass();
            pubPassModel.AssemblyCode = AssemblyCode;

            var minList = Helper.ExecuteService("PublicPass", "GetRecomendedByMinList", pubPassModel) as List<mMinsitryMinister>;

            return Json(minList, JsonRequestBehavior.AllowGet);
        }

        public PublicPassModel ControlsBinderCreateEditPublicPass(PublicPassModel model)
        {
            var passCatForReception = Helper.ExecuteService("PublicPass", "GetPassCategoriesForReception", null) as List<PassCategory>;
            // model.ListPassCategory = passCatForReception;

            string disallowedList = ConfigurationManager.AppSettings["disallowReceptionist"];
            string[] disallowedIDString = disallowedList.Split(',');

            int[] disallowedIDInts = Array.ConvertAll(disallowedIDString, s => int.Parse(s.Trim()));
            foreach (var item in passCatForReception)
            {

                // string RoleID = "0FC60E41-C78E-46B0-B8AC-F6EB708BA119";
                if (CurrentSession.RoleID == RoleID.ToLower())
                {
                    if (disallowedIDInts.Contains(item.PassCategoryID))
                    {
                        PassCategory PassCmodel = new PassCategory();
                        PassCmodel.PassCategoryID = item.PassCategoryID;
                        PassCmodel.Name = item.Name;
                        model.ListPassCategory.Add(PassCmodel);
                    }
                    else
                    {
                    }
                }
                else
                {
                    if (disallowedIDInts.Contains(item.PassCategoryID))
                    {

                    }
                    else
                    {
                        PassCategory PassCmodel = new PassCategory();
                        PassCmodel.PassCategoryID = item.PassCategoryID;
                        PassCmodel.Name = item.Name;
                        model.ListPassCategory.Add(PassCmodel);
                        // model.ListPassCategory=item.
                    }
                }

            }



            model.PassTypeList = ExtensionMethods.GetPassTypeList();
            model.GenderList = ExtensionMethods.GetGenderList();
            model.RecommendationTypeList = ExtensionMethods.GetRecommendationTypeList();
            model.TimeTypeList = ExtensionMethods.GetTimeTypeList();

            PublicPass pubPassModel = new PublicPass();
            pubPassModel.AssemblyCode = model.AssemblyCode;

            var memList = Helper.ExecuteService("PublicPass", "GetRecomendedByList", pubPassModel) as List<mMember>;
            model.RecomendedByListMem = memList;

            var sessionDates = Helper.ExecuteService("PublicPass", "GetSessionDatesList", new PublicPass { SessionCode = model.SessionCode, AssemblyCode = model.AssemblyCode }) as List<mSessionDate>;
            if (CurrentSession.RoleID == RoleID.ToLower())
            {
                model.mSessionDateList = sessionDates;
                model.IsdateRole = "0FC60E41-C78E-46B0-B8AC-F6EB708BA119";
            }
            else
            {
                DateTime SessionFrom = DateTime.Now;
                string SessionDFrom = SessionFrom.ToString("dd/MM/yyyy");
                foreach (var item in sessionDates)
                {
                    string date = item.SessionDate.ToString("dd/MM/yyyy");
                    if (SessionDFrom == date)
                    {
                        model.SessionDateFrom = SessionDFrom;
                        model.SessionDateTo = SessionDFrom;
                    }

                }

            }

            if (CurrentSession.RoleID == RoleID.ToLower())
            {
                if (string.IsNullOrEmpty(model.SessionDateTo))
                {
                    string currentDateVal = DateTime.Now.ToString("dd/MM/yyyy");
                    var sessionDate = sessionDates.OrderByDescending(m => m.DisplayDate).Take(1).FirstOrDefault() as mSessionDate;
                    model.SessionDateTo = sessionDate.DisplayDate.ToString();
                }
            }



            mSession Mdl = new mSession();
            Mdl.SessionCode = model.SessionCode;
            Mdl.AssemblyID = model.AssemblyCode;
            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = AssemblyCode;

            var SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
            var AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
            model.SessionName = SessionName;
            model.AssemblyName = AssesmblyName;

            return model;
        }

        #region Web Cam Helper Methods
        public void Capture()
        {
            var stream = Request.InputStream;
            var picLocation = SavePicToDirectory(stream, "");
            CapturedImage = picLocation;

        }

        public string returnPicLocation()
        {
            string picLoc = CapturedImage;
            return picLoc;
        }

        private byte[] String_To_Bytes(string strInput)
        {
            int numBytes = (strInput.Length) / 2;
            byte[] bytes = new byte[numBytes];

            for (int x = 0; x < numBytes; ++x)
            {
                bytes[x] = Convert.ToByte(strInput.Substring(x * 2, 2), 16);
            }

            return bytes;
        }

        #endregion

        public object GetAdhaarDetails(string AdhaarID, string picLocation)
        {
            AdhaarDetails details = new AdhaarDetails();
            AdhaarServices.ServiceSoapClient obj = new AdhaarServices.ServiceSoapClient();
            EncryptionDecryption.EncryptionDecryption objencr = new EncryptionDecryption.EncryptionDecryption();
            string inputValue = objencr.Encryption(AdhaarID.Trim());
            try
            {
                DataTable dt = obj.getHPKYCInDataTable(inputValue);
                if (dt != null && dt.Rows.Count > 0)
                {
                    details.AdhaarID = AdhaarID;
                    details.Name = objencr.Decryption(Convert.ToString(dt.Rows[0]["Name"]));
                    details.FatherName = objencr.Decryption(Convert.ToString(dt.Rows[0]["FatherName"]));
                    details.Gender = objencr.Decryption(Convert.ToString(dt.Rows[0]["Gender"]));
                    details.Address = objencr.Decryption(Convert.ToString(dt.Rows[0]["Address"]).ToString());
                    details.DOB = objencr.Decryption(Convert.ToString(dt.Rows[0]["DOB"]));
                    details.MobileNo = objencr.Decryption(Convert.ToString(dt.Rows[0]["MobileNumber"]));
                    details.Email = objencr.Decryption(Convert.ToString(dt.Rows[0]["EmailID"]));
                    details.District = objencr.Decryption(Convert.ToString(dt.Rows[0]["DistrictName"]).ToString());
                    details.PinCode = objencr.Decryption(Convert.ToString(dt.Rows[0]["PinCOde"]).ToString());
                    //Calculate the age.
                    DateTime dateOfBirth;

                    if (!string.IsNullOrEmpty(details.DOB) && DateTime.TryParse(details.DOB, out dateOfBirth))
                    {
                        dateOfBirth = Convert.ToDateTime(details.DOB);
                        DateTime today = DateTime.Today;
                        int age = today.Year - dateOfBirth.Year;
                        if (dateOfBirth > today.AddYears(-age))
                            age--;

                        details.DOB = Convert.ToString(age);
                    }
                    else
                    {
                        details.DOB = "";
                    }
                    Byte[] bytes = (Byte[])Convert.FromBase64String(objencr.Decryption(dt.Rows[0]["photo"].ToString()));
                    details.Photo = "data:image/png;base64," + Convert.ToBase64String(bytes);

                    string picName = GetPicNameFromPath(picLocation);
                    if (bytes.Length != 0)
                    {
                        Guid PicName;
                        PicName = Guid.NewGuid();
                        SiteSettings siteSettingMod = new SiteSettings();
                        siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                        var Url = ExtensionMethods.GetImageLocation(Convert.ToInt16(siteSettingMod.AssemblyCode), Convert.ToInt16(siteSettingMod.SessionCode));
                        DirectoryInfo Dir = new DirectoryInfo(Url);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }

                        var path = Url + PicName + ".jpg";
                        System.IO.File.WriteAllBytes(path, bytes);
                        string showUrl = ExtensionMethods.GetImagePath(Convert.ToInt16(siteSettingMod.AssemblyCode), Convert.ToInt16(siteSettingMod.SessionCode), PicName + ".jpg");
                        //details.Photo = SaveAadharPicToDirectory(bytes, picName);
                        details.Photo = showUrl + "," + PicName + ".jpg";
                        //details.Photo = SaveAadharPicToDirectory(bytes, picName);
                    }
                    else
                    {
                        details.Photo = null;
                    }
                    return Json(details, JsonRequestBehavior.AllowGet);
                }
                return null;
            }
            catch
            {
                return Json(details, JsonRequestBehavior.AllowGet);
                //  return Json("nodata", JsonRequestBehavior.AllowGet);
                //return false;
            }
        }



        public string SavePicToDirectory(Stream stream, string pictureName)
        {
            string dump;
            using (var reader = new StreamReader(stream))
                dump = reader.ReadToEnd();

            Guid PicName;
            if (string.IsNullOrEmpty(pictureName))
            {
                PicName = Guid.NewGuid();
            }
            else
            {
                PicName = new Guid(pictureName);
            }

            string Url = "/Images/Pass/Photo/" + AssemblyCode + "/" + SessionCode + "/";

            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
            if (!Dir.Exists)
            {
                Dir.Create();
            }

            var path = Server.MapPath(Url) + PicName + ".jpg";
            System.IO.File.WriteAllBytes(path, String_To_Bytes(dump));

            return Url + PicName + ".jpg";
        }

        public string SaveAadharPicToDirectory(Byte[] picBytes, string pictureName)
        {
            Guid PicName;

            if (string.IsNullOrEmpty(pictureName))
            {
                PicName = Guid.NewGuid();
            }
            else
            {
                PicName = new Guid(pictureName);
            }

            string Url = "/Images/Pass/Photo/" + AssemblyCode + "/" + SessionCode + "/";

            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
            if (!Dir.Exists)
            {
                Dir.Create();
            }

            var path = Server.MapPath(Url) + PicName + ".jpg";
            System.IO.File.WriteAllBytes(path, picBytes);

            return Url + PicName + ".jpg";
        }

        public string GetPicNameFromPath(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                string result = string.Empty;
                result = Path.GetFileNameWithoutExtension(Server.MapPath(filePath));
                return result;
            }
            else
            {
                return "";
            }
        }

        public Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public JsonResult GetSessionByAssemblyId(int AssemblyId)
        {
            mSession mdl = new mSession();
            List<mSession> SessLst = new List<mSession>();
            if (AssemblyId != 0)
            {
                mdl.AssemblyID = AssemblyId;
                SessLst = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", mdl);
            }
            return Json(SessLst, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckSessionDate(string SessionDate)
        {
            try
            {
                string inputValue = SessionDate.Trim();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                int AssemblyCode = siteSettingMod.AssemblyCode;
                int SessionCode = siteSettingMod.SessionCode;
                List<KeyValuePair<string, string>> mParameter = new List<KeyValuePair<string, string>>();
                mParameter.Add(new KeyValuePair<string, string>("@Assembly", AssemblyCode.ToString()));
                mParameter.Add(new KeyValuePair<string, string>("@Session", SessionCode.ToString()));
                mParameter.Add(new KeyValuePair<string, string>("@SessionDate", inputValue));
                DataSet dataDate = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "usp_SessionDateExist", mParameter);
                string message = "";
                if (dataDate.Tables[0].Rows.Count == 0)
                {
                    message = "True";
                    return Json(message, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    message = "False";
                    return Json(message, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ee)
            {
                string message = ee.Message;
                return Json(message, JsonRequestBehavior.AllowGet);
            }

        }

    }
}
