﻿using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Passes;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class SpeakerGalaryController : Controller
    {
        static int SessionCode;
        static int AssemblyCode;

        public ActionResult Index(int Status = 1)
        {
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            SessionCode = siteSettingMod.SessionCode;
            AssemblyCode = siteSettingMod.AssemblyCode;

            var speakerGalaryPasses = new List<SpeakerGalaryModel>();
            var passesByStatus = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetSpeakerGalaryPassesByStatus", new mPasses { Status = Status, RequestedPassCategoryID = (Int32)PassAllowsCreation.SpeakerGalaryPassID, AssemblyCode = AssemblyCode, SessionCode = SessionCode });

            var passListForView = passesByStatus.ToModelList();

            switch (Status)
            {
                case 1: { speakerGalaryPasses = passListForView; break; }
                case 2: { speakerGalaryPasses = passListForView; break; }
                case 3: { speakerGalaryPasses = passListForView; break; }
            }

            var model = new SpeakerGalaryDashboardModel
            {
                SpeakerGalaryPassList = speakerGalaryPasses,
                ActiveTab = 0,
                Status = Status
            };

            if (!Request.IsAjaxRequest())
            {
                return View(model);
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult CreateSpeakerGalaryPass()
        {
            //Get Current Session and Assembly Information
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            AssemblyCode = siteSettingMod.AssemblyCode;
            SessionCode = siteSettingMod.SessionCode;

            var model = new SpeakerGalaryModel
            {
                AssemblyCode = AssemblyCode,
                SessionCode = SessionCode,
                Status = 1,
                Mode = "Add",
                NoOfPersions = 1
            };
            model = this.ControlsBinderCreateEditSpeakerPass(model);

            return View("CreateSpeakerGalaryPass", model);
        }

        [HttpPost]
        public ActionResult CreateSpeakerGalaryPass(SpeakerGalaryModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                var modelToPass = model.TomPassEntity(1);

                if (model.Mode == "Add")
                {
                    modelToPass.Status = 1;
                    var savedRecord = Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPass", modelToPass);
                }
                else
                {
                    var updatedRecord = Helper.ExecuteService("AdministrationBranch", "UpdateMasterPass", modelToPass);
                }
                return RedirectToAction("Index");
            }

            #region If Save will fail
            model = ControlsBinderCreateEditSpeakerPass(model);
            #endregion

            return View("_CreateNewPublicPass", model);
        }

        public ActionResult EditSpeakerGalaryPass(int Id)
        {
            var speakerGalaryPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = Id });
            var speakerGalaryPassModel = speakerGalaryPass.ToModel();
            speakerGalaryPassModel = this.ControlsBinderCreateEditSpeakerPass(speakerGalaryPassModel);
            return View("CreateSpeakerGalaryPass", speakerGalaryPassModel);
        }

        public SpeakerGalaryModel ControlsBinderCreateEditSpeakerPass(SpeakerGalaryModel model)
        {
            var passCatForReception = Helper.ExecuteService("PublicPass", "GetPassCategoriesForReception", null) as List<PassCategory>;
            model.ListPassCategory = passCatForReception;

            model.PassTypeList = ExtensionMethods.GetPassTypeList();
            model.GenderList = ExtensionMethods.GetGenderList();
            model.RecommendationTypeList = ExtensionMethods.GetRecommendationTypeList();
            model.TimeTypeList = ExtensionMethods.GetTimeTypeList();

            PublicPass pubPassModel = new PublicPass();
            pubPassModel.AssemblyCode = model.AssemblyCode;

            var memList = Helper.ExecuteService("PublicPass", "GetRecomendedByList", pubPassModel) as List<mMember>;
            model.RecomendedByListMem = memList;

            var sessionDates = Helper.ExecuteService("PublicPass", "GetSessionDatesList", new PublicPass { SessionCode = model.SessionCode, AssemblyCode = model.AssemblyCode }) as List<mSessionDate>;
            model.mSessionDateList = sessionDates;
            if (string.IsNullOrEmpty(model.SessionDateTo))
            {
                var sessionDate = sessionDates.OrderByDescending(m => m.DisplayDate).Take(1).FirstOrDefault() as mSessionDate;
                model.SessionDateTo = sessionDate.DisplayDate.ToString();
            }

            return model;
        }
    }
}
