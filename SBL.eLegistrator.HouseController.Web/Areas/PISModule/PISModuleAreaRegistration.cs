﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PISModule
{
    public class PISModuleAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PISModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PISModule_default",
                "PISModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
