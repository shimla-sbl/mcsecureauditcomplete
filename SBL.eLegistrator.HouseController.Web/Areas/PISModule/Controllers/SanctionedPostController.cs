﻿using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.PISModules;
using SBL.DomainModel.Models.PISMolues;
using SBL.eLegistrator.HouseController.Web.Areas.PISModule.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Office;

namespace SBL.eLegistrator.HouseController.Web.Areas.PISModule.Controllers
{
    public class SanctionedPostController : Controller
    {
        //
        // GET: /PISModule/SanctionedPost/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SanctionPostList(string Mode)
        {
            SanctionViewModel model = new SanctionViewModel();
            try
            {
                tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    smodel.Deptid = CurrentSession.DeptID as string;
                }

                if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                {
                    smodel.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                }
                List<tPISEmpSanctionpost> ListSanctionPost = new List<tPISEmpSanctionpost>();
                ListSanctionPost = (List<tPISEmpSanctionpost>)Helper.ExecuteService("PISModule", "GetAllSenctionPost", smodel);
                model.SectionList = ListSanctionPost.ToModelList().OrderBy(p => p.OfficeName).ThenBy(p => p.Priority).ThenBy(p => p.Designation).ToList();
                if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                    {
                        offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    }
                    model.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);

                }
                if (Mode == "Update")
                {
                    return PartialView("/Areas/PISModule/Views/SanctionedPost/_UpdateAllSanctionList.cshtml", model);

                }
                else
                {
                    return PartialView("/Areas/PISModule/Views/SanctionedPost/PopulateSanctionList.cshtml", model);

                }
            }
#pragma warning disable CS0168 // The variable 'ee' is declared but never used
            catch (Exception ee)
#pragma warning restore CS0168 // The variable 'ee' is declared but never used
            {
            }
            return null;
        }
        public ActionResult PopulateSectionPost(int Id)
        {
            SanctionViewModel model = new SanctionViewModel();

            try
            {
                var SanctionModel = (tPISEmpSanctionpost)Helper.ExecuteService("PISModule", "GetSanctionPostListBiID", new tPISEmpSanctionpost { Id = Id });
                if (SanctionModel != null)
                {

                    model = SanctionModel.ToViewModel();
                }
                if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                    {
                        offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    }
                    model.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);

                }
            }
#pragma warning disable CS0168 // The variable 'ee' is declared but never used
            catch (Exception ee)
#pragma warning restore CS0168 // The variable 'ee' is declared but never used
            {
            }

            return PartialView("/Areas/PISModule/Views/SanctionedPost/PopulateSanctionForm.cshtml", model);
        }
        public PartialViewResult GetEmpList(string id, int pageId = 1, int pageSize = 20)
        {

            tPISEmployeePersonal PisEmpdetails = new tPISEmployeePersonal();
            PisEmpdetails.ID = Convert.ToInt32(id);
            try
            {

                PisEmpdetails.EmployeeList = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelAllEmpListById", PisEmpdetails);
                return PartialView("/Areas/PISModule/Views/SanctionedPost/_PopupEmpList.cshtml", PisEmpdetails);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public PartialViewResult GetEmpDetails(string AdhaarID)
        {
            //string AdharID = AdhaarID;
            AdhaarDetails details = new AdhaarDetails();
            tPISEmployeePersonal PisEmpdetails = new tPISEmployeePersonal();
            try
            {

                PisEmpdetails.AadharID = AdhaarID;
                PisEmpdetails = (tPISEmployeePersonal)Helper.ExecuteService("PISModule", "CheckAdharIsExist", PisEmpdetails);

                PisEmpdetails.DistrictList = (List<DistrictModel>)Helper.ExecuteService("PISModule", "GetAllDistrict", null);
                return PartialView("/Areas/PISModule/Views/SanctionedPost/_PopupEmpInfo.cshtml", PisEmpdetails);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult UpdateSanctionPost(SanctionViewModel model)
        {
            try
            {
                var EntityModel = model.ToUpdateModel();
                Helper.ExecuteService("PISModule", "UpdateSanctionPost", EntityModel);
                //Return List  Start
                List<tPISEmpSanctionpost> ListSanctionPost = new List<tPISEmpSanctionpost>();
                tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    smodel.Deptid = CurrentSession.DeptID as string;
                }
                if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                {
                    smodel.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                }
                ListSanctionPost = (List<tPISEmpSanctionpost>)Helper.ExecuteService("PISModule", "GetAllSenctionPost", smodel);
                model.SectionList = ListSanctionPost.ToModelList().OrderBy(p => p.OfficeName).ThenBy(p => p.Priority).ThenBy(p => p.Designation).ToList();
                if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                    {
                        offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    }
                    model.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);

                }
                ViewBag.Msg = "Sucessfully Updated";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
                //End List Code
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return PartialView("/Areas/PISModule/Views/SanctionedPost/PopulateSanctionList.cshtml", model);
        }

        public ActionResult UpdateAllSanctionPost(FormCollection coll)
        {
            SanctionViewModel model = new SanctionViewModel();
            try
            {
                string[] SanPIds = coll["RowCount"].Split(',');
                foreach (var item in SanPIds)
                {
                    string Checked = coll["ChbAssignBox-" + item];
                    if (Checked == "on")
                    {
                        if (!string.IsNullOrEmpty(coll["sanctpost-" + item]))
                        {
                            Int32 SanIDint = Convert.ToInt32(coll["Id-" + item]);
                            var SanctionModel = (tPISEmpSanctionpost)Helper.ExecuteService("PISModule", "GetSanctionPostListBiID", new tPISEmpSanctionpost { Id = SanIDint });
                            if (SanctionModel != null)
                            {
                                var Fp = "0";
                                if ((string.IsNullOrEmpty(coll["Filledpost-" + item])))
                                {
                                    Fp = "0";
                                }
                                else
                                {
                                    Fp = coll["Filledpost-" + item];
                                }
                                SanctionModel.sanctpost = Convert.ToInt64(coll["sanctpost-" + item]);
                                SanctionModel.Filledpost = Convert.ToInt64(Fp);
                                Helper.ExecuteService("PISModule", "UpdateSanctionPost", SanctionModel);
                            }
                        }
                    }
                }
                //Return List  Start
                List<tPISEmpSanctionpost> ListSanctionPost = new List<tPISEmpSanctionpost>();
                tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    smodel.Deptid = CurrentSession.DeptID as string;

                }
                if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                {
                    smodel.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                }
                ListSanctionPost = (List<tPISEmpSanctionpost>)Helper.ExecuteService("PISModule", "GetAllSenctionPost", smodel);
                model.SectionList = ListSanctionPost.ToModelList().OrderBy(p => p.OfficeName).ThenBy(p => p.Priority).ThenBy(p => p.Designation).ToList();
                if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                    {
                        offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    }  // enter in 9 may by parveen
                    model.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);

                }
                //End List Code
                ViewBag.Msg = "Sucessfully Updated";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";

            }
#pragma warning disable CS0168 // The variable 'ee' is declared but never used
            catch (Exception ee)
#pragma warning restore CS0168 // The variable 'ee' is declared but never used
            {
            }
            return PartialView("/Areas/PISModule/Views/SanctionedPost/PopulateSanctionList.cshtml", model);
        }
        public ActionResult CheckFilledPost(FormCollection coll)
        {
            try
            {
                tPISEmpSanctionpost model = new tPISEmpSanctionpost();
                bool checkpost = false;
                if (!string.IsNullOrEmpty(coll["sanctpost"]) && !string.IsNullOrEmpty(coll["Id"]))
                {
                    model.Id = Convert.ToInt32(coll["Id"]);
                    model.sanctpost = Convert.ToInt64(coll["sanctpost"]);

                    checkpost = (bool)Helper.ExecuteService("PISModule", "CheckSanctionPost", model);

                }
                return Json(checkpost, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }

        public ActionResult CheckAllFilledPost(FormCollection coll)
        {
            SanctionViewModel model = new SanctionViewModel();
            try
            {
                string[] SanPIds = coll["RowCount"].Split(',');
                bool checkpost = false;
                foreach (var item in SanPIds)
                {
                    string Checked = coll["ChbAssignBox-" + item];
                    if (Checked == "on")
                    {
                        if (!string.IsNullOrEmpty(coll["sanctpost-" + item]))
                        {
                            Int32 SanIDint = Convert.ToInt32(coll["Id-" + item]);
                            tPISEmpSanctionpost models = new tPISEmpSanctionpost();
                            models.Id = Convert.ToInt32(SanIDint);
                            models.sanctpost = Convert.ToInt64(coll["sanctpost-" + item]);

                            checkpost = (bool)Helper.ExecuteService("PISModule", "CheckSanctionPost", models);
                            if (checkpost == false)
                            {
                                break;
                            }
                        }
                    }
                }
                return Json(checkpost, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }

        public PartialViewResult SearchOfficeWise(string officeid, string type)
        {
            SanctionViewModel model = new SanctionViewModel();
            try
            {
                tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    smodel.Deptid = CurrentSession.DeptID as string;
                    smodel.POfficeId = Convert.ToInt32(officeid);
                    model.POfficeId = Convert.ToInt32(officeid);
                }
                List<tPISEmpSanctionpost> ListSanctionPost = new List<tPISEmpSanctionpost>();
                ListSanctionPost = (List<tPISEmpSanctionpost>)Helper.ExecuteService("PISModule", "GetPostByOfficeId", smodel);
                model.SectionList = ListSanctionPost.ToModelList().OrderBy(p => p.OfficeName).ThenBy(p => p.Priority).ThenBy(p => p.Designation).ToList();
                if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    offMdl.POfficeId = Convert.ToInt32(officeid);
                    model.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);

                }

                if (type == "Update")
                {
                    return PartialView("/Areas/PISModule/Views/SanctionedPost/_UpdateAllSanctionList.cshtml", model);

                }
                else
                {

                    return PartialView("/Areas/PISModule/Views/SanctionedPost/PopulateSanctionList.cshtml", model);
                }
            }
#pragma warning disable CS0168 // The variable 'ee' is declared but never used
            catch (Exception ee)
#pragma warning restore CS0168 // The variable 'ee' is declared but never used
            {
            }
            return null;
        }
    }
}
