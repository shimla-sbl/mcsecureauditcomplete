﻿using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Grievance;
using SBL.DomainModel.Models.PISMolues;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.PISModule.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.PISModules;
using SBL.DomainModel.Models.Adhaar;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Models;
using SBL.DomainModel.Models.District;

namespace SBL.eLegistrator.HouseController.Web.Areas.PISModule.Controllers
{
    public class PISEmployeeController : Controller
    {
        //
        // GET: /PISModule/PISEmployee/
        static string CapturedImage = "";
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult EmployeeIndex(int pageId = 1, int pageSize = 20)
        {
            try
            {

                //string MemberCode = CurrentSession.MemberCode;
                tPISEmpSanctionpost SancModel = new tPISEmpSanctionpost();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    SancModel.Deptid = CurrentSession.DeptID as string;
                }
                if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                {
                    ViewBag.Currentofficeid = CurrentSession.OfficeId as string;
                    SancModel.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                }
                //if (CurrentSession.OfficeLevel == "4" && CurrentSession.SubUserTypeID == "37")
                //{
                //    string StOfficeName = (string)Helper.ExecuteService("Office", "GetOfficeNameByOfficeIdNew", CurrentSession.OfficeId);
                //    ViewBag.OfficeName = StOfficeName;
                //}
                List<KeyValuePair<string, string>> methodParameter1 = new List<KeyValuePair<string, string>>();
            
                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateRelievedEmpRecord", methodParameter1);
                var EmpList = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelAllEmpList", SancModel);
                var EmpListRelieved = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelAllRelievedEmpList", SancModel);
                ViewBag.PageSize = pageSize;
                ViewBag.PageSizeRelieved = pageSize;
                List<tPISEmployeePersonal> pagedRecord = new List<tPISEmployeePersonal>();
                List<tPISEmployeePersonal> pagedRecordRelieved = new List<tPISEmployeePersonal>();
                if (pageId == 1)
                {
                    pagedRecord = EmpList.Take(pageSize).ToList();
                    pagedRecordRelieved = EmpListRelieved.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = EmpList.Skip(r).Take(pageSize).ToList();
                    pagedRecordRelieved = EmpListRelieved.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewBag.CurrentPageRelieved = pageId;
                ViewData["PagedList"] = Helper.BindPager(EmpList.Count, pageId, pageSize);
                ViewData["PagedListRelieved"] = Helper.BindPager(EmpListRelieved.Count, pageId, pageSize);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();
                if (TempData["Class"] != null)
                    ViewBag.Class = TempData["Class"].ToString();
                if (TempData["Notification"] != null)
                    ViewBag.Notification = TempData["Notification"].ToString();

                //ViewBag.DesignationCount = EmpList.Count;
                tPISEmployeePersonal newModel = new tPISEmployeePersonal();
                newModel.EmployeeList = pagedRecord;
                newModel.RelievedEmployeeList = pagedRecordRelieved;
                if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                   newModel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficesList", offMdl);
                   //newModel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);
                   SancModel.Deptid = offMdl.deptid;
                   newModel.DesignationList = (List<mPISDesignation>)Helper.ExecuteService("PISModule", "GetAllDesignationByOfficeId", SancModel);

                    
                }

                if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                {
                    ViewBag.Currentofficeid = CurrentSession.OfficeId as string;
                    SancModel.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                }
                
                if (CurrentSession.OfficeLevel == "4" && CurrentSession.SubUserTypeID == "37")
                {
                    string StOfficeName = (string)Helper.ExecuteService("Office", "GetOfficeNameByOfficeIdNew", CurrentSession.OfficeId);
                    ViewBag.OfficeName = StOfficeName;
                }
                return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeeIndex.cshtml", newModel);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Designation List";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        public JsonResult CheckAdhaarID(string AdhaarID)
        {
            mUsers user = new mUsers();
            user.AadarId = AdhaarID;
            user = (mUsers)Helper.ExecuteService("User", "CheckAdhaarID", user);
            if (user != null)
            {
            }
            else
            {
                user = new mUsers();
                user = null;
            }
            return Json(user, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetAdhaarDetails(string AdhaarID, string IsModel)
        {
            //string AdharID = AdhaarID;
            AdhaarDetails details = new AdhaarDetails();
            tPISEmployeePersonal PisEmpdetails = new tPISEmployeePersonal();
            try
            {
              
                PisEmpdetails.AadharID = AdhaarID;
                PisEmpdetails = (tPISEmployeePersonal)Helper.ExecuteService("PISModule", "CheckAdharIsExist", PisEmpdetails);

                if (PisEmpdetails == null)
                {
                    AdhaarServices.ServiceSoapClient obj = new AdhaarServices.ServiceSoapClient();
                    EncryptionDecryption.EncryptionDecryption objencr = new EncryptionDecryption.EncryptionDecryption();
                    string inputValue = objencr.Encryption(AdhaarID.Trim());

                    DataTable dt = obj.getHPKYCInDataTable(inputValue);
                    if (dt != null && dt.Rows.Count > 0)
                    {

                        PisEmpdetails = new tPISEmployeePersonal();
                        PisEmpdetails.AadharID = AdhaarID;
                        PisEmpdetails.empfname = objencr.Decryption(Convert.ToString(dt.Rows[0]["Name"]));
                        PisEmpdetails.empfmh = objencr.Decryption(Convert.ToString(dt.Rows[0]["FatherName"]));
                        PisEmpdetails.empgender = objencr.Decryption(Convert.ToString(dt.Rows[0]["Gender"]));
                        PisEmpdetails.Address = objencr.Decryption(Convert.ToString(dt.Rows[0]["Address"]).ToString());
                       
                        string dobs = objencr.Decryption(Convert.ToString(dt.Rows[0]["DOB"]));
                        dobs = String.Format("{0:dd/MM/yyyy}", dobs);
                        PisEmpdetails.empdob = Convert.ToDateTime(dobs);
                        PisEmpdetails.DistrictList = (List<DistrictModel>)Helper.ExecuteService("PISModule", "GetAllDistrict", null);
                        string mob = objencr.Decryption(Convert.ToString(dt.Rows[0]["MobileNumber"]));
                        if (mob != "" && mob != null && mob!=" ")
                        {
                            PisEmpdetails.Mobile = Convert.ToInt64(mob);
                        }
                       string pin = objencr.Decryption(Convert.ToString(dt.Rows[0]["PINCOde"]).ToString());
                       if (pin != "" && pin != null && pin!=" ")
                       {
                           PisEmpdetails.PinCode = Convert.ToInt64(pin);
                       }
                      
                        PisEmpdetails.Email = objencr.Decryption(Convert.ToString(dt.Rows[0]["EmailID"]));
                        PisEmpdetails.emphmdist = objencr.Decryption(Convert.ToString(dt.Rows[0]["DistrictName"]).ToString());

                        string stPhoto =Convert.ToString(dt.Rows[0]["photo"]);
                        if (stPhoto != null)
                        {

                        }

                            Byte[] bytes = (Byte[])Convert.FromBase64String(objencr.Decryption(dt.Rows[0]["photo"].ToString()));
                            if (bytes.Length > 0)
                            {
                                MemoryStream ms = new MemoryStream(bytes);
                                System.Drawing.Image image = System.Drawing.Image.FromStream(ms);

                                string DirPath = Server.MapPath("~/photos/");
                                if (!Directory.Exists(DirPath))
                                {
                                    Directory.CreateDirectory(DirPath);
                                }
                                ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                                imgCompress.GetImage = new System.Drawing.Bitmap(image);
                                imgCompress.Height = 160;
                                imgCompress.Width = 120;
                                imgCompress.Save("compressingAdhaarImage.jpg", DirPath);

                                string newPath = Path.Combine(DirPath, "compressingAdhaarImage.jpg");
                                byte[] newImage = ReadImage(newPath);

                                PisEmpdetails.empphoto = "data:image/png;base64," + Convert.ToBase64String(newImage);
                            }
                            else
                            {
                                PisEmpdetails.empphoto = "data:image/png;base64,";
                            }
                        
                        
                      

                        if (IsModel == "True")
                        {
                            //return details;
                        }
                        else
                        {
                            return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeePopUpForm.cshtml", PisEmpdetails);
                        }
                    }
                    else
                    {

                        PisEmpdetails = new tPISEmployeePersonal();
                        PisEmpdetails.AadharID = AdhaarID;
                        PisEmpdetails = (tPISEmployeePersonal)Helper.ExecuteService("PISModule", "CheckAdharIsExist", PisEmpdetails);
                                              if (PisEmpdetails == null)
                        {
                            PisEmpdetails = new tPISEmployeePersonal();
                            PisEmpdetails.AadharID = AdhaarID;
                        }
                        else
                        {
                            PisEmpdetails.DistrictList = (List<DistrictModel>)Helper.ExecuteService("PISModule", "GetAllDistrict", null);
                            PisEmpdetails.EmpKeyID = PisEmpdetails.ID;
                        }
                       // return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeePopUpForm.cshtml", PisEmpdetails);
                        return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeePopUpForm.cshtml", PisEmpdetails);
                    }
                }
                else
                {
                    PisEmpdetails.EmpKeyID = PisEmpdetails.ID;
                }
                PisEmpdetails.DistrictList = (List<DistrictModel>)Helper.ExecuteService("PISModule", "GetAllDistrict", null);
                return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeePopUpForm.cshtml", PisEmpdetails);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch(Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                //PisEmpdetails = new tPISEmployeePersonal();
                //PisEmpdetails.AadharID = AdhaarID;
                //PisEmpdetails.DistrictList = (List<DistrictModel>)Helper.ExecuteService("PISModule", "GetAllDistrict", null);
               // return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeePopUpForm.cshtml", PisEmpdetails);
                return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeePopUpForm.cshtml", PisEmpdetails);
              //  return null;
            }

        }
        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult OfficeDrpList(string deptid)
        {
            tPISEmployeePersonal newModel = new tPISEmployeePersonal();
            if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
                newModel.AadharID = CurrentSession.AadharId;
                newModel.deptid = CurrentSession.DeptID;
               // newModel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", newModel);
                newModel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficesList", newModel);
            }
            return Json(newModel.OfficeList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult DesignationDrpList(string officeid)
        {
            tPISEmpSanctionpost newModel = new tPISEmpSanctionpost();
            tPISEmployeePersonal emp = new tPISEmployeePersonal();
            if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
               // newModel.ad = CurrentSession.AadharId;
                newModel.Deptid = CurrentSession.DeptID;
                newModel.POfficeId = Convert.ToInt32(officeid);

                emp.DesignationList = (List<mPISDesignation>)Helper.ExecuteService("PISModule", "GetAllDesignationByOfficeId", newModel);
            }
            return Json(emp.DesignationList, JsonRequestBehavior.AllowGet);

        }
        //public ActionResult DistDrpList()
        //{
        //    tPISEmployeePersonal emp = new tPISEmployeePersonal();
        //    emp.DistrictList = (List<DistrictModel>)Helper.ExecuteService("PISModule", "GetAllDistrict", null);
        //    return Json(emp.DesignationList, JsonRequestBehavior.AllowGet);

        //}
        public ActionResult DeleteEmployee(string id)
        {
            try
            {
                tPISEmployeePersonal model = new tPISEmployeePersonal();
                model.ID = Convert.ToInt32(id);
                Helper.ExecuteService("PISModule", "DeleteEmployee", model);
                TempData["Msg"] = "Sucessfully Deleted";
                TempData["Class"] = "alert alert-info";
                TempData["Notification"] = "Success";
                return RedirectToAction("EmployeeIndex");
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }
        public PartialViewResult EditEmpDetials(string id, int pageId = 1, int pageSize = 20)
        {
            try
            {
                tPISEmpSanctionpost SancModel = new tPISEmpSanctionpost();
                tPISEmployeePersonal newModel = new tPISEmployeePersonal();
                newModel.ID = Convert.ToInt32(id);

                if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    SancModel.Deptid = CurrentSession.DeptID as string;
                }
                newModel = (tPISEmployeePersonal)Helper.ExecuteService("PISModule", "GetEmpInfoById", newModel);
                var EmpList = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelAllEmpList", SancModel);
                var EmpListRelieved = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelAllRelievedEmpList", SancModel);
                ViewBag.PageSize = pageSize;
                ViewBag.PageSizeRelieved = pageSize;
                List<tPISEmployeePersonal> pagedRecord = new List<tPISEmployeePersonal>();
                List<tPISEmployeePersonal> pagedRecordRelieved = new List<tPISEmployeePersonal>();
                if (pageId == 1)
                {
                    pagedRecord = EmpList.Take(pageSize).ToList();
                    pagedRecordRelieved = EmpListRelieved.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = EmpList.Skip(r).Take(pageSize).ToList();
                    pagedRecordRelieved = EmpListRelieved.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewBag.CurrentPageRelieved = pageId;
                ViewData["PagedList"] = Helper.BindPager(EmpList.Count, pageId, pageSize);
                ViewData["PagedListRelieved"] = Helper.BindPager(EmpListRelieved.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();

                ViewBag.DesignationCount = EmpList.Count;
                ViewBag.departmentid = newModel.deptid;
                ViewBag.OfficeID = newModel.POfficeId;

                newModel.EmployeeList = EmpList;
                newModel.EmpKeyID = newModel.ID;
                newModel.EmployeeList = pagedRecord;
                newModel.RelievedEmployeeList = pagedRecordRelieved;
                //newModel.DepartmentList = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
                if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    //newModel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);
                    newModel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficesList", offMdl);
                  
                    SancModel.Deptid = offMdl.deptid;
                    SancModel.POfficeId = Convert.ToInt32(newModel.POfficeId);
                    if (SancModel.POfficeId == 0)
                    {
                        SancModel.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    }
                    newModel.DesignationList = (List<mPISDesignation>)Helper.ExecuteService("PISModule", "GetAllDesignationByOfficeId", SancModel);
                }
                if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                {
                    ViewBag.Currentofficeid = CurrentSession.OfficeId as string;
                    SancModel.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
 
                }
                if (CurrentSession.OfficeLevel == "4" && CurrentSession.SubUserTypeID == "37")
                {
                    string StOfficeName = (string)Helper.ExecuteService("Office", "GetOfficeNameByOfficeIdNew", CurrentSession.OfficeId);
                    ViewBag.OfficeName = StOfficeName;
                }
                //newModel.DesignationList = (List<mPISDesignation>)Helper.ExecuteService("PISModule", "GetAllDesignation", null);
                return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeeIndex.cshtml", newModel);
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }
        [HttpPost, ValidateAntiForgeryToken]
        public PartialViewResult SaveAdharDetails(tPISEmployeePersonal model, HttpPostedFileBase uploadPhoto, int pageId = 1, int pageSize = 20)
        {

            try
            {
                tPISEmployeePersonal newModel = new tPISEmployeePersonal();
                if (uploadPhoto != null)
                {
                    string DirPath = Server.MapPath("~/photos/Emp/");
                    if (!Directory.Exists(DirPath))
                    {
                        //Directory.Delete(DirPath);
                        Directory.CreateDirectory(DirPath);
                    }
                   
                    string fileName =  GetFormNumber();

                    var path = Path.Combine(DirPath, fileName);
                    uploadPhoto.SaveAs(path);
                    ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                    imgCompress.GetImage = new System.Drawing.Bitmap(path);
                    imgCompress.Height = 160;
                    imgCompress.Width = 120;
                    imgCompress.Save("compressingImage.jpg", DirPath);

                    string newPath = Path.Combine(DirPath, "compressingImage.jpg");
                    byte[] image = ReadImage(newPath);

                    // System.IO.File.Delete(path);
                    model.empphoto = "data:image/png;base64," + Convert.ToBase64String(image);
                }
                Int64 count = (Int64)Helper.ExecuteService("PISModule", "GetEmpCount", null);
#pragma warning disable CS0472 // The result of the expression is always 'false' since a value of type 'long' is never equal to 'null' of type 'long?'
                if (count == null || count == 0)
#pragma warning restore CS0472 // The result of the expression is always 'false' since a value of type 'long' is never equal to 'null' of type 'long?'
                {
                    model.empcd = "EMPCD" + 1;
                    model.transid = 1;
                }
                else
                {
                    model.empcd = "EMPCD" + count + 1;
                    model.transid = count + 1;
                }
                if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    model.deptid = CurrentSession.DeptID;
                }
                else
                {
                    model.deptid = "NULL";
                }
                string Dob = Request.Form["hdnDob"];
                DateTime? empDob = new DateTime();
                if (!string.IsNullOrEmpty(Dob))
                    empDob = new DateTime(Convert.ToInt32(Dob.Split('/')[2]), Convert.ToInt32(Dob.Split('/')[1]), Convert.ToInt32(Dob.Split('/')[0]));
                else
                    empDob = null;
                model.empdob = empDob;
                newModel = (tPISEmployeePersonal)Helper.ExecuteService("PISModule", "CreateEmployee", model);
                ViewBag.Msg = "Sucessfully Added";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
                tPISEmpSanctionpost SancModel = new tPISEmpSanctionpost();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    SancModel.Deptid = CurrentSession.DeptID as string;
                }
                var EmpList = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelAllEmpList", SancModel);

                var EmpListRelieved = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelAllRelievedEmpList", SancModel);
                ViewBag.PageSize = pageSize;
                ViewBag.PageSizeRelieved = pageSize;
                List<tPISEmployeePersonal> pagedRecord = new List<tPISEmployeePersonal>();
                List<tPISEmployeePersonal> pagedRecordRelieved = new List<tPISEmployeePersonal>();
                if (pageId == 1)
                {
                    pagedRecord = EmpList.Take(pageSize).ToList();
                    pagedRecordRelieved = EmpListRelieved.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = EmpList.Skip(r).Take(pageSize).ToList();
                    pagedRecordRelieved = EmpListRelieved.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewBag.CurrentPageRelieved = pageId;
                ViewData["PagedList"] = Helper.BindPager(EmpList.Count, pageId, pageSize);
                ViewData["PagedListRelieved"] = Helper.BindPager(EmpListRelieved.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();

                ViewBag.DesignationCount = EmpList.Count;
                ViewBag.departmentid = newModel.deptid;
                ViewBag.OfficeID = newModel.officeid;
                newModel.EmployeeList = pagedRecord;
                newModel.RelievedEmployeeList = pagedRecordRelieved;
                newModel.EmpKeyID = newModel.ID;
                //newModel.DepartmentList = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
                if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    newModel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);
                    string Departmentid = offMdl.deptid;
                    newModel.DesignationList = (List<mPISDesignation>)Helper.ExecuteService("PISModule", "GetAllDesignationByDeptId", Departmentid);
                }
                // newModel.DesignationList = (List<mPISDesignation>)Helper.ExecuteService("PISModule", "GetAllDesignation", null);
                return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeeIndex.cshtml", newModel);
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }

        [HttpPost]
        public ActionResult UpdateEmpDetails(tPISEmployeePersonal model, int pageId = 1, int pageSize = 20)
        {

            try
            {
                string Dob = Request.Form["empdob"];
                string Doj = Request.Form["dofjoin"];
                string Dor = Request.Form["dofretirecurr"];
                DateTime? empDob = new DateTime();
                if (!string.IsNullOrEmpty(Dob))
                    empDob = new DateTime(Convert.ToInt32(Dob.Split('/')[2]), Convert.ToInt32(Dob.Split('/')[1]), Convert.ToInt32(Dob.Split('/')[0]));
                else
                   empDob = null;

                DateTime? dofjoin = new DateTime();
                if (!string.IsNullOrEmpty(Doj))
                    dofjoin = new DateTime(Convert.ToInt32(Doj.Split('/')[2]), Convert.ToInt32(Doj.Split('/')[1]), Convert.ToInt32(Doj.Split('/')[0]));
                else
                    dofjoin = null;

                DateTime? dofretirecurr = new DateTime();
                if (!string.IsNullOrEmpty(Dor))
                    dofretirecurr = new DateTime(Convert.ToInt32(Dor.Split('/')[2]), Convert.ToInt32(Dor.Split('/')[1]), Convert.ToInt32(Dor.Split('/')[0]));
                else
                    dofretirecurr = null;

                if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    model.deptid = CurrentSession.DeptID;
                }
                else
                {
                    model.deptid = "NULL";
                }
                model.empdob = empDob;
                model.dofjoin = dofjoin;
                model.dofretirecurr = dofretirecurr;
                mPISDesignation desigModel = new mPISDesignation();
                desigModel.Id = Convert.ToInt32(model.PDesignationId);
                string DesgCode = (string)Helper.ExecuteService("PISModule", "GetDesigCodeById", desigModel);
                model.currdesig = DesgCode.Trim();

                model.POfficeId = Convert.ToInt32(model.POfficeId);
                Session["OfficeSelected"] = model.POfficeId;
                
                Helper.ExecuteService("PISModule", "UpdateEmployee", model);
           
                TempData["Msg"] = "Sucessfully Updated";
                TempData["Class"] = "alert alert-info";
                TempData["Notification"] = "Success";
                return RedirectToAction("EmployeeIndex");
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }
        public ActionResult CheckFilledPost(tPISEmployeePersonal model)
        {
            try
            {
                string strResult = "";
                List<tPISEmpSanctionpost> result = (List<tPISEmpSanctionpost>)Helper.ExecuteService("PISModule", "CheckFilledPost", model);
                if (result == null)
                {                   
                    return Json(strResult, JsonRequestBehavior.AllowGet);
                }
                else
                {                    
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
               
              
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }
        [HttpPost]
        public ActionResult UpdateEmpDetailsByPopup(tPISEmployeePersonal model, HttpPostedFileBase uploadPhoto, int pageId = 1, int pageSize = 20)
        {

            try
            {
                if (uploadPhoto != null)
                {
                 
                    string DirPath = Server.MapPath("~/photos/Emp/");
                    string TempDirPath = Server.MapPath("~/photos/Emp/Temp/");

                    if (!Directory.Exists(TempDirPath))
                    {
                        Directory.CreateDirectory(TempDirPath);
                    }
                    if (!Directory.Exists(DirPath))
                    {
                        Directory.CreateDirectory(DirPath);
                    }
                    var fileName = Path.GetFileName(uploadPhoto.FileName);

                    var tempfile = Path.Combine(TempDirPath, fileName);
                    var Orgfile = Path.Combine(DirPath, fileName);
                    string UniqName = GetFormNumber();
                    var Orgfile1 = Path.Combine(DirPath, UniqName);
                    
                    uploadPhoto.SaveAs(tempfile);
                    //System.IO.File.Delete(Orgfile1);
                    System.IO.File.Copy(tempfile, Orgfile1 + ".jpg", true);
                    System.IO.File.Delete(tempfile);

                    ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                    imgCompress.GetImage = new System.Drawing.Bitmap(Orgfile1 + ".jpg");
                    imgCompress.Height = 160;
                    imgCompress.Width = 120;


                    imgCompress.Save("compressingImage.jpg", DirPath);

                    string newPath = Path.Combine(DirPath, "compressingImage.jpg");
                    byte[] image = ReadImage(newPath);

                    // System.IO.File.Delete(path);
                    model.empphoto = "data:image/png;base64," + Convert.ToBase64String(image);
                    uploadPhoto.InputStream.Close();
                    //System.IO.File.Delete(path);
                }
           
                model.ID = model.EmpKeyID;
                Helper.ExecuteService("PISModule", "UpdateEmployeeByPopup", model);
                TempData["Msg"] = "Sucessfully Updated";
                TempData["Class"] = "alert alert-info";
                TempData["Notification"] = "Success";
                return RedirectToAction("EmployeeIndex");
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }
        public JsonResult CheckAdharIsExist(string Adhar)
        {

            try
            {
                tPISEmployeePersonal model = new tPISEmployeePersonal();
                model.AadharID = Adhar;
                model = (tPISEmployeePersonal)Helper.ExecuteService("PISModule", "CheckAdharIsExist", model);
                if (model != null)
                {
                }
                else
                {
                    model = new tPISEmployeePersonal();
                    model = null;
                }
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }
        public ActionResult GetEmpbyPaging(int pageId = 1, int pageSize = 20)
        {
            tPISEmpSanctionpost SancModel = new tPISEmpSanctionpost();
            if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
                SancModel.Deptid = CurrentSession.DeptID as string;
            }
            var EmpList = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelAllEmpList", SancModel);
            //var EmpListRelieved = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelAllRelievedEmpList", null);
            ViewBag.PageSize = pageSize;
            List<tPISEmployeePersonal> pagedRecord = new List<tPISEmployeePersonal>();
            if (pageId == 1)
            {
                pagedRecord = EmpList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = EmpList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(EmpList.Count, pageId, pageSize);


            if (TempData["Msg"] != null)
                ViewBag.Msg = TempData["Msg"].ToString();

            ViewBag.DesignationCount = EmpList.Count;
            tPISEmployeePersonal newModel = new tPISEmployeePersonal();
            newModel.EmployeeList = pagedRecord;
      
            return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeeIndexList.cshtml", newModel);
        }

        public ActionResult SearchOfficeWise(string officeid,int pageId = 1, int pageSize = 20)
        {
            tPISEmpSanctionpost SancModel = new tPISEmpSanctionpost();
            if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
                SancModel.Deptid = CurrentSession.DeptID as string;
                SancModel.POfficeId = Convert.ToInt32(officeid);
            }
            var EmpList = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelEmpListByOfficeId", SancModel);
            //var EmpListRelieved = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelAllRelievedEmpList", null);
            ViewBag.PageSize = pageSize;
            List<tPISEmployeePersonal> pagedRecord = new List<tPISEmployeePersonal>();
            if (pageId == 1)
            {
                pagedRecord = EmpList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = EmpList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(EmpList.Count, pageId, pageSize);


            if (TempData["Msg"] != null)
                ViewBag.Msg = TempData["Msg"].ToString();

            ViewBag.DesignationCount = EmpList.Count;
            tPISEmployeePersonal newModel = new tPISEmployeePersonal();
            newModel.EmployeeList = pagedRecord;

            return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeeIndexList.cshtml", newModel);
        }
        public ActionResult GetRelievedEmpbyPaging(int pageId = 1, int pageSize = 20)
        {
            tPISEmpSanctionpost SancModel = new tPISEmpSanctionpost();
            if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
                SancModel.Deptid = CurrentSession.DeptID as string;
            }
            var EmpListRelieved = (List<tPISEmployeePersonal>)Helper.ExecuteService("PISModule", "GelAllRelievedEmpList", SancModel);
            ViewBag.PageSizeRelieved = pageSize;
            List<tPISEmployeePersonal> pagedRecordRelieved = new List<tPISEmployeePersonal>();
            if (pageId == 1)
            {
                pagedRecordRelieved = EmpListRelieved.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecordRelieved = EmpListRelieved.Skip(r).Take(pageSize).ToList();
            }
            ViewBag.CurrentPageRelieved = pageId;
            ViewData["PagedListRelieved"] = Helper.BindPager(EmpListRelieved.Count, pageId, pageSize);

            if (TempData["Msg"] != null)
                ViewBag.Msg = TempData["Msg"].ToString();
            tPISEmployeePersonal newModel = new tPISEmployeePersonal();
            newModel.RelievedEmployeeList = pagedRecordRelieved;
            return PartialView("/Areas/PISModule/Views/PISEmployee/_EmployeeRelievedList.cshtml", newModel);
        }
        
        public static string GetFormNumber()
        {
            byte[] buffer = Guid.NewGuid().ToByteArray();
            var FormNumber = BitConverter.ToUInt32(buffer, 0) ^ BitConverter.ToUInt32(buffer, 4) ^ BitConverter.ToUInt32(buffer, 8) ^ BitConverter.ToUInt32(buffer, 12);
            return FormNumber.ToString("X");

        }
        #region Web Cam Helper Methods
        public void Capture()
        {
            var stream = Request.InputStream;
            var picLocation = SavePicToDirectory(stream, "");
            CapturedImage = picLocation;

        }

        public string returnPicLocation()
        {
            string picLoc = CapturedImage;
            return picLoc;
        }

        private byte[] String_To_Bytes(string strInput)
        {
            int numBytes = (strInput.Length) / 2;
            byte[] bytes = new byte[numBytes];

            for (int x = 0; x < numBytes; ++x)
            {
                bytes[x] = Convert.ToByte(strInput.Substring(x * 2, 2), 16);
            }

            return bytes;
        }
        public string SavePicToDirectory(Stream stream, string pictureName)
        {
            string dump;
            using (var reader = new StreamReader(stream))
                dump = reader.ReadToEnd();

            Guid PicName;
            if (string.IsNullOrEmpty(pictureName))
            {
                PicName = Guid.NewGuid();
            }
            else
            {
                PicName = new Guid(pictureName);
            }

            string Url = "~/photos/Emp/Temp/";

            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
            if (!Dir.Exists)
            {
                Dir.Create();
            }

            var path = Server.MapPath(Url) + PicName + ".jpg";
            System.IO.File.WriteAllBytes(path, String_To_Bytes(dump));

          
            byte[] image = ReadImage(path);
           string filedata = "data:image/png;base64," + Convert.ToBase64String(image);


           return filedata;
        }
        #endregion
      
    }
}
