﻿using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Grievance;
using SBL.DomainModel.Models.PISMolues;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.PISModule.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.PISModules;
using System.IO;

namespace SBL.eLegistrator.HouseController.Web.Areas.PISModule.Controllers
{
    public class PISDesignationController : Controller
    {
        //
        // GET: /PISModule/PISDesignation/

        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult DesignationIndex(int pageId = 1, int pageSize = 20)
        {
            try
            {
               tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    smodel.Deptid=CurrentSession.DeptID as string;
                    if(CurrentSession.OfficeId !=""  || CurrentSession.OfficeId !=null)
                    {
                    smodel.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    }
                }
                if (CurrentSession.SubDivisionId != null)
                {
                  smodel.SubId = CurrentSession.SubDivisionId;
                }
                var DesignationList = (List<tPISEmpSanctionpost>)Helper.ExecuteService("PISModule", "GetAllSenctionPost", smodel);

                ViewBag.PageSize = pageSize;
                List<tPISEmpSanctionpost> pagedRecord = new List<tPISEmpSanctionpost>();
                if (pageId == 1)
                {
                    pagedRecord = DesignationList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = DesignationList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(DesignationList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();
                if (TempData["Class"] != null)
                    ViewBag.Class = TempData["Class"].ToString();
                if (TempData["Notification"] != null)
                    ViewBag.Notification = TempData["Notification"].ToString();

                ViewBag.DesignationCount = DesignationList.Count;
                smodel.PostList = pagedRecord;
                if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    smodel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);
                  
                }
                return PartialView("/Areas/PISModule/Views/PISDesignation/_DesignationIndex.cshtml", smodel);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Designation List";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        public ActionResult CheckUserExistToPost(string id)
        {
            try
            {
                tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
                smodel.Id = Convert.ToInt32(id);
                bool chk = (bool)Helper.ExecuteService("PISModule", "CheckEmpExist", smodel);

                return Json(chk, JsonRequestBehavior.AllowGet);
              
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }
        public PartialViewResult DeleteSantion(string id, int pageId = 1, int pageSize = 20)
        {
            try
            {
                
                tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
                smodel.Id = Convert.ToInt32(id);
                List<tPISEmpSanctionpost> pagedRecord = new List<tPISEmpSanctionpost>();
           
                  Helper.ExecuteService("PISModule", "DeleteSantion", smodel);
                  if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                  {
                      smodel.Deptid = CurrentSession.DeptID as string;
                  }
                  var DesignationList = (List<tPISEmpSanctionpost>)Helper.ExecuteService("PISModule", "GetAllSenctionPost", smodel);

                  ViewBag.PageSize = pageSize;
                 
                  if (pageId == 1)
                  {
                      pagedRecord = DesignationList.Take(pageSize).ToList();
                  }
                  else
                  {
                      int r = (pageId - 1) * pageSize;
                      pagedRecord = DesignationList.Skip(r).Take(pageSize).ToList();
                  }

                  ViewBag.CurrentPage = pageId;
                  ViewData["PagedList"] = Helper.BindPager(DesignationList.Count, pageId, pageSize);

                  ViewBag.Msg = "Sucessfully deleted";
                  ViewBag.Class = "alert alert-info";
                  ViewBag.Notification = "Success";


                  ViewBag.DesignationCount = DesignationList.Count;
                  smodel.PostList = pagedRecord;
                  if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                  {
                      tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                      offMdl.AadharID = CurrentSession.AadharId;
                      offMdl.deptid = CurrentSession.DeptID;
                      if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                      {
                          offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                      }
                      smodel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);

                  }

                  return PartialView("/Areas/PISModule/Views/PISDesignation/_DesignationList.cshtml", smodel);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Designation List";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        public PartialViewResult AddSantionpostForm(string did, string type)
        {
            try
            {
                PISCustom model = new PISCustom();
                if (type == "update")
                {
                    tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
                    smodel.Id = Convert.ToInt32(did);
                    smodel = (tPISEmpSanctionpost)Helper.ExecuteService("PISModule", "GetSantionpostByid", smodel);
                    model.DeptId = smodel.Deptid;
                    model.OfficeId = smodel.POfficeId;
                    model.Id = smodel.Id;
                    model.type = type;
                    ViewBag.departmentid = smodel.Deptid;
                    ViewBag.OfficeID = smodel.POfficeId;
                    ViewBag.type = type;
                    model.PDesignationId = smodel.PDesignationId;
                    model.AllDesignationIds = smodel.PDesignationId.ToString().Split(',').ToList();
                    model.DepartmentName = smodel.DepartmentName;
                    model.Priority = smodel.Priority;
                    //model.AllDesignationIds = smodel.desigcd.ToString();
                }
                if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    model.DeptId = CurrentSession.DeptID;
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                    {
                        offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    }
                    model.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);
                    string Departmentid = offMdl.deptid;
                    model.DesignationList = (List<mPISDesignation>)Helper.ExecuteService("PISModule", "GetAllDesignationByDeptId", Departmentid);

                }
                return PartialView("/Areas/PISModule/Views/PISDesignation/_AddSantionpostForm.cshtml", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Designation List";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        public ActionResult OfficeDrpList()
        {

            tPISEmployeePersonal newModel = new tPISEmployeePersonal();
            if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
                newModel.AadharID = CurrentSession.AadharId;
                newModel.deptid = CurrentSession.DeptID;
                if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                {
                    newModel.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                }
                newModel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", newModel);
            }
            return Json(newModel.OfficeList, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult SaveSantionpost(tPISEmpSanctionpost model, int pageId = 1, int pageSize = 20)
        {
            try
            {
                if (model.type == "update")
                {                  
                    mPISDesignation desigModel = new mPISDesignation();
                    desigModel.Id = Convert.ToInt32(model.PDesignationId);
                    string DesgCode = (string)Helper.ExecuteService("PISModule", "GetDesigCodeById", desigModel);
                    model.desigcd = DesgCode;
                    model.officeid = model.POfficeId.ToString();
                    Helper.ExecuteService("PISModule", "UpdateToSanctionPost", model);
                    TempData["Msg"] = "Sucessfully Updated";
                    TempData["Class"] = "alert alert-info";
                    TempData["Notification"] = "Success";

                }
                else
                {
                    if (model.AllDesignationIds.Count > 0 && model.AllDesignationIds != null)
                    {
                        foreach (var item in model.AllDesignationIds)
                        {
                            tPISEmpSanctionpost tmodel = new tPISEmpSanctionpost();

                            mPISDesignation desigModel = new mPISDesignation();
                            desigModel.Id = Convert.ToInt32(item);
                            string DesgCode = (string)Helper.ExecuteService("PISModule", "GetDesigCodeById", desigModel);
                            tmodel.PDesignationId = Convert.ToInt32(item.Trim());
                            tmodel.desigcd = DesgCode;
                            tmodel.Deptid = model.Deptid;
                            tmodel.officeid = model.POfficeId.ToString();
                            tmodel.POfficeId = model.POfficeId;
                            Helper.ExecuteService("PISModule", "CreateSantionPost", tmodel);
                        }
                        TempData["Msg"] = "Sucessfully Added";
                        TempData["Class"] = "alert alert-info";
                        TempData["Notification"] = "Success";
                    }
                }
                   return RedirectToAction("DesignationIndex");
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }
        public ActionResult CheckDuplicate(tPISEmpSanctionpost model)
        {
            try
            {
                if (model.type == "update")
                {
                    List<tPISEmpSanctionpost> result = (List<tPISEmpSanctionpost>)Helper.ExecuteService("PISModule", "CheckDuplicate", model);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    List<tPISEmpSanctionpost> result = (List<tPISEmpSanctionpost>)Helper.ExecuteService("PISModule", "CheckDuplicate", model);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }
        public PartialViewResult GetSantionbyPaging(int pageId = 1, int pageSize = 20)
        {
            string MemberCode = CurrentSession.MemberCode;
            tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
            if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
                smodel.Deptid = CurrentSession.DeptID as string;
            }
            var DesignationList = (List<tPISEmpSanctionpost>)Helper.ExecuteService("PISModule", "GetAllSenctionPost", smodel);

            ViewBag.PageSize = pageSize;
            List<tPISEmpSanctionpost> pagedRecord = new List<tPISEmpSanctionpost>();
            if (pageId == 1)
            {
                pagedRecord = DesignationList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = DesignationList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(DesignationList.Count, pageId, pageSize);
            ViewBag.GrievanceCount = DesignationList.Count;
            smodel.PostList = pagedRecord;
            if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
                tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                offMdl.AadharID = CurrentSession.AadharId;
                offMdl.deptid = CurrentSession.DeptID;
                if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                {
                    offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                }
                smodel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);

            }
            //return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
            return PartialView("/Areas/PISModule/Views/PISDesignation/_DesignationList.cshtml", smodel);
        }
        public PartialViewResult SearchOfficeWise(string officeid, int pageId = 1, int pageSize = 20)
        {
            string MemberCode = CurrentSession.MemberCode;
            tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
            if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
                smodel.Deptid = CurrentSession.DeptID as string;
                smodel.POfficeId = Convert.ToInt32(officeid);
            }
            var DesignationList = (List<tPISEmpSanctionpost>)Helper.ExecuteService("PISModule", "GetPostByOfficeId", smodel);

            ViewBag.PageSize = pageSize;
            List<tPISEmpSanctionpost> pagedRecord = new List<tPISEmpSanctionpost>();
            if (pageId == 1)
            {
                pagedRecord = DesignationList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = DesignationList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(DesignationList.Count, pageId, pageSize);
            ViewBag.GrievanceCount = DesignationList.Count;
            smodel.PostList = pagedRecord;
            if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
                tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                offMdl.AadharID = CurrentSession.AadharId;
                offMdl.deptid = CurrentSession.DeptID;
                if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                {
                    offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                }
                smodel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);

            }
            //return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
            return PartialView("/Areas/PISModule/Views/PISDesignation/_DesignationList.cshtml", smodel);
        }



        //Code For San & Fill Post Pdf

        public PartialViewResult SancFillPostIndex(int pageId = 1, int pageSize = 20)
        {
            try
            {
                tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    smodel.Deptid = CurrentSession.DeptID as string;
                    if (CurrentSession.OfficeId != "" || CurrentSession.OfficeId != null)
                    {
                        smodel.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    }
                }
                if (CurrentSession.SubDivisionId != null)
                {
                    smodel.SubId = CurrentSession.SubDivisionId;
                }
                var DesignationList = (List<SanFillPostPdf>)Helper.ExecuteService("PISModule", "GetAllSanFillPostsByOfficeId", smodel);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                smodel.FileAccessPath = FileSettings.SettingValue;
                ViewBag.PageSize = pageSize;
                List<SanFillPostPdf> pagedRecord = new List<SanFillPostPdf>();
                if (pageId == 1)
                {
                    pagedRecord = DesignationList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = DesignationList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(DesignationList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();
                if (TempData["Class"] != null)
                    ViewBag.Class = TempData["Class"].ToString();
                if (TempData["Notification"] != null)
                    ViewBag.Notification = TempData["Notification"].ToString();

                ViewBag.DesignationCount = DesignationList.Count;
                smodel.SancFillPostList = pagedRecord;
                if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
                {
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    smodel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);

                }
                return PartialView("/Areas/PISModule/Views/PISDesignation/_SancFillPostIndex.cshtml", smodel);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Designation List";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        public PartialViewResult AddSanFillPostForm(string did, string type)
        {
            try
            {
                tPISEmpSanctionpost model = new tPISEmpSanctionpost();
                if (type == "Update")
                {
                    SanFillPostPdf smodel = new SanFillPostPdf();
                    smodel.Id = Convert.ToInt32(did);
                    //  smodel = (tPISEmpSanctionpost)Helper.ExecuteService("PISModule", "GetSantionpostByid", smodel);
                    smodel = (SanFillPostPdf)Helper.ExecuteService("PISModule", "GetSanFillPostsByid", smodel);
                    model.Description = smodel.Description;
                    model.Id = smodel.Id;
                    model.FileLocation = smodel.PdfLocation;

                    model.ModifiedDate = Convert.ToString(smodel.ModifiedDate);
                    model.Mode = "Update";
                    ViewBag.OfficeID = smodel.officeid;
                    //model.AllDesignationIds = smodel.desigcd.ToString();
                    model.Deptid = CurrentSession.DeptID;
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                    {
                        offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    }
                    model.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);
                }
                else
                {
                    model.Deptid = CurrentSession.DeptID;
                    tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                    offMdl.AadharID = CurrentSession.AadharId;
                    offMdl.deptid = CurrentSession.DeptID;
                    if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                    {
                        offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                    }
                    model.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);
                    model.Mode = "Create";

                }
                return PartialView("/Areas/PISModule/Views/PISDesignation/AddSanFillPostForm.cshtml", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Designation List";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSanFillPost(tPISEmpSanctionpost model, HttpPostedFileBase file, int pageId = 1, int pageSize = 20)
        {
            try
            {
                //if (model.type == "update")
                if (model.Mode == "Update")
                {
                    SanFillPostPdf smodel = new SanFillPostPdf();
                    smodel.Id = Convert.ToInt32(model.Id);

                    if (file != null)
                    {
                        smodel.PdfLocation = UploadPhoto(file, model);
                        model.FileLocation = smodel.PdfLocation;
                    }
                    else
                    {
                        smodel.PdfLocation = null;
                    }

                    smodel.officeid = Convert.ToString(model.POfficeId);
                    smodel.Description = model.Description;

                    //  smodel = (tPISEmpSanctionpost)Helper.ExecuteService("PISModule", "GetSantionpostByid", smodel);
                    //smodel = (SanFillPostPdf)Helper.ExecuteService("PISModule", "GetSanFillPostsByid", smodel);

                    Helper.ExecuteService("PISModule", "UpdateSanFillPost", model);
                    TempData["Msg"] = "Sucessfully Updated";
                    TempData["Class"] = "alert alert-info";
                    TempData["Notification"] = "Success";

                }
                else
                {


                    SanFillPostPdf smodel = new SanFillPostPdf();
                    // smodel.Id = Convert.ToInt32(model.Id);

                    if (file != null)
                    {
                        smodel.PdfLocation = UploadPhoto(file, model);
                        model.FileLocation = smodel.PdfLocation;
                    }
                    else
                    {
                        smodel.PdfLocation = null;
                    }

                    smodel.officeid = Convert.ToString(model.POfficeId);
                    smodel.Description = model.Description;
                    smodel.CreatedDate = System.DateTime.Now;
                    smodel.ModifiedDate = System.DateTime.Now;

                    Helper.ExecuteService("PISModule", "CreateSanFillPost", smodel);

                    TempData["Msg"] = "Sucessfully Added";
                    TempData["Class"] = "alert alert-info";
                    TempData["Notification"] = "Success";

                }
                //   return PartialView("/Areas/PISModule/Views/PISDesignation/SancFillPostPage.cshtml");
                //return PartialView("/Areas/PISModule/Views/PISDesignation/_SancFillPostIndex.cshtml");
                //    return RedirectToAction("Index", "Grievances");
                //return RedirectToAction("Index", new RouteValueDictionary(new { controller = "Grievances", action = "Index" }));
                //return RedirectToAction("Index", "Grievances");
                //return RedirectToAction("SancFillPostIndex");
                return RedirectToAction("Index", "Grievances", new { area = "Grievances" });


            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }

        private string UploadPhoto(HttpPostedFileBase File, tPISEmpSanctionpost model)
        {
            try
            {

                if (File != null)
                {
                    // var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    //var disFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);


                    // DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~/Images/News/"));//C:\DIS_FileStructure\Notices
                    //Code for Assembly Folder creation
                    DirectoryInfo AssemblyDir = new DirectoryInfo(FileSettings.SettingValue + "\\SanctionedFilledPostsPDF\\" + model.POfficeId);  //"C:\\DIS_FileStructure\\Notices");
                    //DirectoryInfo MDir = new DirectoryInfo(disFileSettings.SettingValue);
                    if (!AssemblyDir.Exists)
                    {
                        AssemblyDir.Create();
                    }



                    //Code for Assembly Folder File Existense Check

                    var filepath = System.IO.Path.Combine(FileSettings.SettingValue + "\\SanctionedFilledPostsPDF\\" + model.POfficeId + "\\" + model.POfficeId + ".pdf");

                    if (System.IO.File.Exists(filepath))
                    {
                        // Remove Existence File

                        RemoveExistingFile(filepath);
                        File.SaveAs(filepath);

                        var path = "\\SanctionedFilledPostsPDF\\" + model.POfficeId + "\\" + model.POfficeId + ".pdf";
                        return path.Replace('\\', '/');
                    }
                    else
                    {
                        File.SaveAs(filepath);

                        var path = "\\SanctionedFilledPostsPDF\\" + model.POfficeId + "\\" + model.POfficeId + ".pdf";
                        return path.Replace('\\', '/');

                    }

                }



            }
            catch (Exception ex)
            {


                throw ex;
            }


            return null;


        }

        private void RemoveExistingFile(string OldFile)
        {

            try
            {


                if (System.IO.File.Exists(OldFile))
                {
                    System.IO.File.Delete(OldFile);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public PartialViewResult GetSanFillPostbyPaging(int pageId = 1, int pageSize = 20)
        {
            string MemberCode = CurrentSession.MemberCode;
            tPISEmpSanctionpost smodel = new tPISEmpSanctionpost();
            if (!string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
                smodel.Deptid = CurrentSession.DeptID as string;
            }
            var DesignationList = (List<SanFillPostPdf>)Helper.ExecuteService("PISModule", "GetAllSanFillPosts", smodel);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            smodel.FileAccessPath = FileSettings.SettingValue;

            ViewBag.PageSize = pageSize;
            List<SanFillPostPdf> pagedRecord = new List<SanFillPostPdf>();
            if (pageId == 1)
            {
                pagedRecord = DesignationList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = DesignationList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(DesignationList.Count, pageId, pageSize);
            ViewBag.GrievanceCount = DesignationList.Count;
            smodel.SancFillPostList = pagedRecord;
            if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {
                tPISEmployeePersonal offMdl = new tPISEmployeePersonal();
                offMdl.AadharID = CurrentSession.AadharId;
                offMdl.deptid = CurrentSession.DeptID;
                if (!string.IsNullOrEmpty(CurrentSession.OfficeId as string))
                {
                    offMdl.POfficeId = Convert.ToInt32(CurrentSession.OfficeId);
                }
                smodel.OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);

            }
            //return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
            return PartialView("/Areas/PISModule/Views/PISDesignation/SanFillPostList.cshtml", smodel);
        }

    }
}
