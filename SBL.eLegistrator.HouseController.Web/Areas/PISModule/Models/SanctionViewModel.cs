﻿using SBL.DomainModel.Models.Office;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.PISModule.Models
{
    public class SanctionViewModel
    {
        public SanctionViewModel()
        {
            this.OfficeList = new List<mOffice>();
        }
        public int Id { get; set; }

        public string Deptid { get; set; }

        public string officeid { get; set; }

        public Int32 POfficeId { get; set; }

        public string desigcd { get; set; }

        public Int64? sanctpost { get; set; }

        public Int64? Filledpost { get; set; }

        public string DepartmentName { get; set; }

        public string OfficeName { get; set; }

        public string Designation { get; set; }
        public Int32? Priority{get;set;} 
        public string Mode { get; set; }
        public Int32? PDesignationId { get; set; }
        public List<SanctionViewModel> SectionList { get; set; }
        [NotMapped]
        public List<mOffice> OfficeList { get; set; }
    }
}