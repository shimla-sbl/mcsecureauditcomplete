﻿using SBL.DomainModel.Models.Media;
using SBL.DomainModel.Models.Passes;
using SBL.eLegistrator.HouseController.Web.Areas.Passes.Models;
using System;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.ComplexModel.MediaComplexModel;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.PISMolues;

namespace SBL.eLegistrator.HouseController.Web.Areas.PISModule.Models
{
    [Serializable]
    public class PISCustom
    {

        public PISCustom()
        {
            this.OfficeList = new List<mOffice>();
            this.DepartmentList = new List<mDepartment>();
            this.DesignationList = new List<mPISDesignation>();
        }
       

        public List<mDepartment> DepartmentList { get; set; }

        public List<mOffice> OfficeList { get; set; }

        public List<mPISDesignation> DesignationList { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public Int32 Id { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public Int32 DesignationCode { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DesignationName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DeptId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DesignationLocal { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DesignationAbbr { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public Int32 OfficeId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public Int64? SanctionPost { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public Int64? FilledPost { get; set; }
        [NotMapped]
        public List<string> AllDesignationIds { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string type { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DepartmentName { get; set; }
          [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AadharID { get; set; }
          [NotMapped]
          [HiddenInput(DisplayValue = false)]
          public Int32 POfficeId { get; set; }

          [NotMapped]
          [HiddenInput(DisplayValue = false)]
          public Int32? PDesignationId { get; set; }
          [NotMapped]
          [HiddenInput(DisplayValue = false)]
          public Int32? Priority { get; set; }
    }
}