﻿using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.PISMolues;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.PISModule.Models
{
    public static class ModelMapping
    {
        public static List<SanctionViewModel> ToModelList(this List<tPISEmpSanctionpost> entityList)
        {
            var ModelList = entityList.Select(entity => new SanctionViewModel()
            {
                Id = entity.Id,
                Deptid = entity.Deptid,
                DepartmentName = entity.DepartmentName,
                OfficeName = entity.OfficeName,
                desigcd = entity.desigcd,
                sanctpost = entity.sanctpost,
                PDesignationId = entity.PDesignationId,
                Filledpost = entity.Filledpost,
                Priority = entity.Priority,
                Designation = entity.DesignationName


            });
            return ModelList.ToList();
        }
        public static SanctionViewModel ToViewModel(this tPISEmpSanctionpost model)
        {
            SanctionViewModel entity = new SanctionViewModel()
            {
                DepartmentName = model.DepartmentName,
                OfficeName = model.OfficeName,
                Id = model.Id,
                Deptid = model.Deptid,
                desigcd = model.desigcd,
                officeid = model.officeid,
                POfficeId = model.POfficeId,
                PDesignationId = model.PDesignationId,
                sanctpost = model.sanctpost,
                Filledpost = model.Filledpost,
                Priority = model.Priority,
                Designation = model.DesignationName
            };


            return entity;
        }

        public static tPISEmpSanctionpost ToUpdateModel(this SanctionViewModel model)
        {
            tPISEmpSanctionpost entity = new tPISEmpSanctionpost()
            {
                Deptid = model.Deptid,
                desigcd = model.desigcd,
                officeid = model.officeid,
                POfficeId = model.POfficeId,
                PDesignationId = model.PDesignationId,
                sanctpost = model.sanctpost,
                Filledpost = model.Filledpost,
                Priority = model.Priority,
                Id = model.Id

            };
            return entity;
        }
        public static string GetOfficeName(string OfficeID)
        {
            mOffice model = new mOffice();
            model.OfficeId = Convert.ToInt32(OfficeID);
            var officeName = (mOffice)Helper.ExecuteService("Office", "GetOfficeNameByOfficeID", model);
            if (officeName != null)
            {
                return officeName.officename;
            }
            return "";
        }
        public static string GetDepartmentByOfficeId(string DeptId)
        {

            mDepartment Dmodel = new mDepartment();
            Dmodel.deptId = DeptId;
            var Departname = (mDepartment)Helper.ExecuteService("Office", "GetDepartmentNameByDepartmentId", Dmodel);
            if (Departname != null)
            {
                return Departname.deptname;
            }
            return "";
        }
    }
}