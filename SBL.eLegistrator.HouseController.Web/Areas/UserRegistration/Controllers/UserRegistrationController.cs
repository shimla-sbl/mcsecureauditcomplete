﻿
using SMS.API;

namespace SBL.eLegistrator.HouseController.Web.Areas.UserRegistration.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using SBL.DomainModel.ComplexModel;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Employee;
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.OTP;
    using SBL.DomainModel.Models.Office;
    using SBL.DomainModel.Models.Secretory;
    using SBL.DomainModel.Models.HOD;
    using SBL.DomainModel.Models.User;
    using SBL.DomainModel.Models.RecipientGroups;
    using SBL.eLegistrator.HouseController.Web.Filters;
    using SBL.eLegistrator.HouseController.Web.Helpers;
    using SBL.eLegistrator.HouseController.Web.Areas.UserRegistration.Models;
    using System.Security.Cryptography;
    using System.Text;
    using SBL.eLegislator.HPMS.ServiceAdaptor;
    using CaptchaMvc.Attributes;
    using SBL.eLegistrator.HouseController.Web.Utility;
    using System.Net.Mail;
    using System.Net;
    using SBL.DomainModel.Models.Media;
    using System.Data;
    using SBL.DomainModel.Models.Adhaar;
    using System.IO;
    using SBL.DomainModel.ComplexModel.UserComplexModel;
    using SMSServices;
    using SBL.eLegistrator.HouseController.Web.Models;
    using SBL.eLegistrator.HouseController.Filters;
    using System.Web.Security;
    using SBL.DomainModel.Models.UserModule;
    using SBL.DomainModel.Models.UserAction;
    using SBL.DomainModel.Models.District;
    using SBL.DomainModel.Models.Constituency;
    using SBL.DomainModel.Models.SubDivision;


    //[SBLAuthorize(Allow = "Authenticated")]
    [SBLAuthorize(Allow = "All")]
    [Audit]
    [NoCache]
    public class UserRegistrationController : Controller
    {
        //
        // GET: /UserRegistration/UserRegistration/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserRegistraitonForm()
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "OtpRequiredInRegistration" });
            ViewBag.OtpRequiredInRegistration = "0";//SiteSettingsSessn.SettingValue;
            mUserModelClass user = new mUserModelClass();

            var radioButtonList = new List<KeyValuePair<int, string>>() { 
           new KeyValuePair<int, string>(1,Resources.UserRegistration.Honable),
           new KeyValuePair<int, string>(2,Resources.UserRegistration.Department),
           // new KeyValuePair<int, string>(3,Resources.UserRegistration.Media)
            };
            user.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartmentSorted", user);
            mDepartment dep = new mDepartment();

            user.radioList = radioButtonList;
            mSecretory secretory = new mSecretory();
            user.mSecretory = (ICollection<mSecretory>)Helper.ExecuteService("Secretory", "GetAllSecratoryDetails", secretory);
            mSecretory sec = new mSecretory();
            sec.SecretoryID = 0;
            sec.SecretoryName = "Please Select Secretary";
            user.mSecretory.Add(sec);
            List<SecretoryDepartmentModel> mSecretoryDepartment = new List<SecretoryDepartmentModel>();

            user.SecretoryDepartmentModel = mSecretoryDepartment;
            return View(user);
        }

        //public JsonResult ConvertImageToByte(string Image)
        //    {
        //    return Json(user, JsonRequestBehavior.AllowGet);
        //    var fileName = Path.GetFileName(Image);
        //    var path = Path.Combine(Server.MapPath("~/photos/"), fileName);
        //    uploadPhoto.SaveAs(path);
        //    byte[] image = ReadImage(path);
        //    System.IO.File.Delete(path);
        //    mdl.Photo = "data:image/png;base64," + Convert.ToBase64String(image);
        //    }


        //public List<KeyValuePair<int, string>> getactionlist(string ids)
        //{
        //    var radioButtonList = new List<KeyValuePair<int, string>>();
        //    IEnumerable<string> idss = ids.Split(',').Select(str => str);
        //    mUserActions user = new mUserActions();

        //    foreach (var itm2 in idss)
        //    {
        //        user.ActionId = Convert.ToInt32(itm2);
        //        user = (mUserActions)Helper.ExecuteService("Module", "GetActionDataById", user);
        //        radioButtonList.Insert(0, new KeyValuePair<Int32, string>(user.ActionId, user.ActionName));
        //    }
        //    return radioButtonList;
        //}
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateMemberEmployeeDetailsReporters(mUsers mUsers, HttpPostedFileBase uploadPhoto, HttpPostedFileBase signatureUpload)
        {
            mUsers mdl = mUsers as mUsers;
            
            if (signatureUpload != null)
            {
                string DirPath = Server.MapPath("~/Images/SignaturePath/");
                if (!Directory.Exists(DirPath))
                {
                    Directory.CreateDirectory(DirPath);
                }
                var fileName = Path.GetFileName(signatureUpload.FileName);
                var path = Path.Combine(Server.MapPath("~/Images/SignaturePath/"), fileName);
                signatureUpload.SaveAs(path);

                ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                imgCompress.GetImage = new System.Drawing.Bitmap(path);
                imgCompress.Height = 240;
                imgCompress.Width = 180;
                imgCompress.Save(signatureUpload.FileName, DirPath);

                mdl.SignaturePath = "/Images/SignaturePath/" + signatureUpload.FileName;
            }

            if (uploadPhoto != null)
            {
                string DirPath = Server.MapPath("~/photos/");
                if (!Directory.Exists(DirPath))
                {
                    Directory.CreateDirectory(DirPath);
                }
                var fileName = Path.GetFileName(uploadPhoto.FileName);

                var path = Path.Combine(Server.MapPath("~/photos/"), fileName);
                uploadPhoto.SaveAs(path);

                //  var path = Path.Combine(DirPath, fileName);
                //  photoUpload.SaveAs(path);
                ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                imgCompress.GetImage = new System.Drawing.Bitmap(path);
                imgCompress.Height = 160;
                imgCompress.Width = 120;
                imgCompress.Save("compressingImage.jpg", DirPath);

                string newPath = Path.Combine(DirPath, "compressingImage.jpg");
                byte[] image = ReadImage(newPath);

                // System.IO.File.Delete(path);
                mdl.Photo = "data:image/png;base64," + Convert.ToBase64String(image);
            }
            if (mUsers.AllDepartmentIds.Count > 0)
            {
                mdl.DepartmentIDs = "";
                foreach (var dept in mUsers.AllDepartmentIds)
                {
                    mUsers.DepartmentIDs += dept + ",";
                }
                bool flag = mUsers.DepartmentIDs.EndsWith(",");
                if (flag)
                {
                    mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
                }
            }
            mdl.EmailId = mdl.MailID;
            mdl.MobileNo = mdl.Mobile;
            mdl.Designation = mdl.Designation;
            mdl = (mUsers)Helper.ExecuteService("User", "UpdatedMemberDetailsReporters", mdl);

            TempData["Updated"] = "updated";
            return RedirectToAction("Index", "Home", new { area = "" });
        }

        //[HttpPost, ValidateAntiForgeryToken]
        //public ActionResult UpdateMemberEmployeeDetails(mUsers mUsers, HttpPostedFileBase uploadPhoto, HttpPostedFileBase signatureUpload, string IsSecretoryId, string IsHOD)
        //{
        //    mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "OtpRequiredInRegistration" });
        //    ViewBag.OtpRequiredInRegistration = "0";//SiteSettingsSessn.SettingValue;
        //    mUsers mdl = mUsers as mUsers;
        //    if (mdl.OfficeId != null)
        //    {
        //        CurrentSession.OfficeId = Convert.ToString(mdl.OfficeId);
        //    }
            
        //    if (signatureUpload != null)
        //    {
        //        string DirPath = Server.MapPath("~/Images/SignaturePath/");
        //        if (!Directory.Exists(DirPath))
        //        {
        //            Directory.CreateDirectory(DirPath);
        //        }
        //        var fileName = Path.GetFileName(signatureUpload.FileName);
        //        var path = Path.Combine(Server.MapPath("~/Images/SignaturePath/"), fileName);
        //        signatureUpload.SaveAs(path);

        //        ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
        //        imgCompress.GetImage = new System.Drawing.Bitmap(path);
        //        imgCompress.Height = 240;
        //        imgCompress.Width = 180;
        //        imgCompress.Save(signatureUpload.FileName, DirPath);

        //        mdl.SignaturePath = "/Images/SignaturePath/" + signatureUpload.FileName;
        //    }

        //    if (uploadPhoto != null)
        //    {
        //        string DirPath = Server.MapPath("~/photos/");
        //        if (!Directory.Exists(DirPath))
        //        {
        //            Directory.CreateDirectory(DirPath);
        //        }
        //        var fileName = Path.GetFileName(uploadPhoto.FileName);

        //        var path = Path.Combine(Server.MapPath("~/photos/"), fileName);
        //        uploadPhoto.SaveAs(path);

        //        //  var path = Path.Combine(DirPath, fileName);
        //        //  photoUpload.SaveAs(path);
        //        ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
        //        imgCompress.GetImage = new System.Drawing.Bitmap(path);
        //        imgCompress.Height = 160;
        //        imgCompress.Width = 120;
        //        imgCompress.Save("compressingImage.jpg", DirPath);

        //        string newPath = Path.Combine(DirPath, "compressingImage.jpg");
        //        byte[] image = ReadImage(newPath);

        //        // System.IO.File.Delete(path);
        //        mdl.Photo = "data:image/png;base64," + Convert.ToBase64String(image);
        //    }

        //    if (uploadPhoto == null)
        //    {
        //        mdl.Photo = mUsers.Photo;
        //    }
        //    if (TempData["Updated"] != "ReUpdated")
        //    {
        //        if (mUsers.UserType == 17 || mUsers.UserType == 18 || mUsers.UserType == 19 || mUsers.UserType == 20 || mUsers.UserType == 21 || mUsers.UserType == 36)
        //        {
        //            mdl.IsMember = "True";
        //            mdl.IsMedia = "False";
        //            mdl.IsSecretoryId = false;
        //            mdl.IsOther = false;
        //            mdl.OfficeId = 0;
        //            mdl.DeptId = null;
        //            mdl.SecretoryId = null;
        //            mdl.DepartmentIDs = null;
        //            mdl.IsHOD = false;
        //            mdl.UserType = mUsers.UserType;
        //            //mdl.UserName = mUsers.Name;
        //            mdl.OfficeLevel = mUsers.OfficeLevel;
        //            if (mUsers.EmpOrMemberCode == null || mUsers.EmpOrMemberCode == "")
        //            {
        //                int num;

        //                bool isNum = Int32.TryParse(mUsers.UserName, out num);

        //                if (isNum)
        //                {
        //                    mdl.UserName = mUsers.UserName;
        //                }
        //                else
        //                {
        //                    mdl.UserName = "0";
        //                }

        //            }
        //            else
        //            {
        //                mdl.UserName = mUsers.EmpOrMemberCode;
        //            }

        //        }
        //        if (mUsers.UserType == 22)
        //        {
        //            mdl.IsMember = "False";
        //            mdl.IsMedia = "False";
        //            mdl.IsSecretoryId = false;
        //            mdl.IsOther = false;
        //            mdl.OfficeId = 0;
        //            mdl.DeptId = null;
        //            //mdl.SecretoryId = null;
        //            mdl.DepartmentIDs = null;
        //            mdl.IsHOD = false;
        //            mdl.UserType = mUsers.UserType;
        //            mdl.UserName = mUsers.Name;
        //            mdl.OfficeLevel = mUsers.OfficeLevel;
        //            mdl.SecretoryId = null;
        //            if (mUsers.EmpOrMemberCode == null || mUsers.EmpOrMemberCode == "")
        //            {
        //                mdl.UserName = mUsers.UserName;
        //            }
        //            else
        //            {
        //                mdl.UserName = mUsers.EmpOrMemberCode;
        //            }

        //        }
        //        else if (mUsers.UserType == 2 || mUsers.UserType == 4 || mUsers.UserType == 15)
        //        {
        //            mdl.IsMember = "False";
        //            mdl.IsMedia = "False";
        //            mdl.IsSecretoryId = false;
        //            mdl.IsOther = false;

        //            //mdl.DeptId = mUsers.DeptId;
        //            mdl.UserName = mUsers.Name;
        //            mdl.OfficeLevel = mUsers.OfficeLevel;
        //            mdl.UserType = mUsers.UserType;
        //            mdl.SecretoryId = mUsers.SecretoryId;
        //            if (mUsers.AllDepartmentIds.Count > 0)
        //            {
        //                mdl.DepartmentIDs = "";
        //                mUsers.DeptId = "";
        //                foreach (var dept in mUsers.AllDepartmentIds)
        //                {
        //                    mUsers.DepartmentIDs += dept + ",";
        //                    mUsers.DeptId += dept + ",";
        //                }
        //                bool flag = mUsers.DepartmentIDs.EndsWith(",");
        //                if (flag)
        //                {
        //                    mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
        //                   // mdl.DeptId 
        //                }
        //            }
        //            else if (mUsers.DepartmentIDs != null && mUsers.DepartmentIDs != "")
        //            {
        //                mdl.DepartmentIDs = mUsers.DepartmentIDs;
        //                mdl.DeptId = mUsers.DepartmentIDs;
        //            }

        //            if (mUsers.UserType == 4)
        //            {
        //                if (mUsers.DepartmentIDs == null || mUsers.DepartmentIDs == "")
        //                {
        //                    mdl.DepartmentIDs = "HPD0001";
        //                    mdl.DeptId = "HPD0001";
        //                }
        //            }
        //            if (IsSecretoryId == "True")
        //            {
        //                mdl.IsSecretoryId = true;
        //                mdl.IsHOD = false;
        //                mdl.OfficeId = 0;
        //                mSecretory secretory = new mSecretory();
        //                secretory.AadhaarId = mdl.AadarId;
        //                bool result = (bool)Helper.ExecuteService("Secretory", "CheckSecretaryExist", secretory);
        //                if (result == false)
        //                {
        //                    secretory.DeptID = mdl.DepartmentIDs;
        //                    secretory.SecretoryName = mdl.Name;
        //                    secretory.Email = mdl.MailID;
        //                    secretory.Mobile = mdl.Mobile;
        //                    secretory.IsActive = true;
        //                    secretory.DesignationDescription = mdl.Designation;
        //                    //secretory.ModifiedWhen = DateTime.Now;
        //                    secretory.AadhaarId = mUsers.AadarId;
        //                    //secretory.CreationDate = DateTime.Now;
        //                    Helper.ExecuteService("Secretory", "CreateSecretory", secretory);
        //                    mSecretoryDepartment secdept = new mSecretoryDepartment();
        //                    secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretaryByAdharid", secretory);
        //                    secdept.SecretoryID = secretory.SecretoryID;
        //                    foreach (var tval in mUsers.AllDepartmentIds)
        //                    {
        //                        secdept.DepartmentID = tval.ToString();
        //                        secdept.IsActive = true;
        //                        Helper.ExecuteService("SecretoryDepartment", "CreateSecretoryDepartment", secdept);
        //                    }

        //                }
        //                else
        //                {
        //                    secretory.DeptID = mdl.DepartmentIDs;
        //                    secretory.SecretoryName = mdl.Name;
        //                    secretory.Email = mdl.MailID;
        //                    secretory.Mobile = mdl.Mobile;
        //                    secretory.IsActive = true;
        //                    secretory.DesignationDescription = mdl.Designation;
        //                    //secretory.ModifiedWhen = DateTime.Now;
        //                    secretory.AadhaarId = mUsers.AadarId;
        //                    //secretory.CreationDate = DateTime.Now;
        //                    Helper.ExecuteService("Secretory", "UpdateSecretoryByAdharId", secretory);
        //                    //mSecretoryDepartment secdept = new mSecretoryDepartment();
        //                    //secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretaryByAdharid", secretory);
        //                    //secdept.SecretoryID = secretory.SecretoryID;
        //                    //foreach (var tval in mUsers.AllDepartmentIds)
        //                    //{
        //                    //    secdept.DepartmentID = tval.ToString();
        //                    //    secdept.IsActive = true;
        //                    //    Helper.ExecuteService("SecretoryDepartment", "CreateSecretoryDepartment", secdept);
        //                    //}
        //                }


        //                secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretaryByAdharid", secretory);

        //                mdl.SecretoryName = secretory.SecretoryName;
        //                mdl.SecretoryId = secretory.SecretoryID;
        //            }
        //            if (IsHOD == "True")
        //            {
        //                mdl.IsHOD = true;
        //                mdl.IsSecretoryId = false;
        //                mdl.OfficeId = 0;
        //                mHOD hods = new mHOD();
        //                hods.AadhaarId = mdl.AadarId;
        //                bool result = (bool)Helper.ExecuteService("HOD", "CheckHodExist", hods);
        //                if (result == false)
        //                {
        //                    hods.DeptID = mdl.DepartmentIDs;
        //                    hods.HODName = mdl.Name;
        //                    hods.Email = mdl.MailID;
        //                    hods.Mobile = mdl.Mobile;
        //                    hods.IsActive = true;
        //                    hods.DesignationDescription = mdl.Designation;
        //                    //secretory.ModifiedWhen = DateTime.Now;
        //                    hods.AadhaarId = mUsers.AadarId;
        //                    //secretory.CreationDate = DateTime.Now;
        //                    Helper.ExecuteService("HOD", "CreateHOD", hods);


        //                }
        //                else
        //                {
        //                    hods.DeptID = mdl.DepartmentIDs;
        //                    hods.HODName = mdl.Name;
        //                    hods.Email = mdl.MailID;
        //                    hods.Mobile = mdl.Mobile;
        //                    hods.IsActive = true;
        //                    hods.DesignationDescription = mdl.Designation;
        //                    //secretory.ModifiedWhen = DateTime.Now;
        //                    hods.AadhaarId = mUsers.AadarId;
        //                    //secretory.CreationDate = DateTime.Now;
        //                    Helper.ExecuteService("HOD", "UpdateHODByAdharId", hods);
        //                    CurrentSession.DeptID = mdl.DepartmentIDs;
        //                }
        //                hods = (mHOD)Helper.ExecuteService("HOD", "GetHodByAdharid", hods);

        //                mdl.SecretoryName = hods.HODName;
        //                mdl.SecretoryId = hods.HODID;
        //            }
        //            bool obj = (bool)Helper.ExecuteService("Module", "CheckAccessRequestApproved", mdl);
        //            if (obj == true)
        //            {
        //                if (IsHOD == "True" || IsSecretoryId == "True")
        //                {
        //                    RecipientGroupMember recipient = new RecipientGroupMember();
        //                    recipient.AadharId = mdl.AadarId;


        //                    bool result1 = (bool)Helper.ExecuteService("ContactGroups", "CheckGroupMemberExist", recipient);
        //                    if (result1 == false)
        //                    {
        //                        if (IsHOD == "True")
        //                        {
        //                            recipient.GroupID = 118;
        //                            recipient.Address = mdl.Address;
        //                            if (mdl.DepartmentIDs != "NULL" && mdl.DepartmentIDs != "" && mdl.DepartmentIDs != null)
        //                            {
        //                                recipient.DepartmentCode = mdl.DepartmentIDs;
        //                            }
        //                            else
        //                            {
        //                                recipient.DepartmentCode = mdl.DeptId;
        //                            }

        //                            List<RecipientGroupMember> rlist = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", recipient);
        //                            recipient.RankingOrder = rlist.Count + 1;

        //                        }
        //                        else if (IsSecretoryId == "True")
        //                        {
        //                            recipient.GroupID = 117;
        //                            recipient.Address = "HP Secretariat, Shimla-2";
        //                            recipient.DepartmentCode = "HPD0012";
        //                            List<RecipientGroupMember> rlist = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", recipient);
        //                            recipient.RankingOrder = rlist.Count + 1;
        //                        }

        //                        if (mdl.DepartmentIDs != "NULL" && mdl.DepartmentIDs != "" && mdl.DepartmentIDs != null)
        //                        {

        //                            mdl.AllDepartmentIds = mdl.DepartmentIDs.Split(',').ToList();
        //                            string deptName = (string)Helper.ExecuteService("Department", "GetDeptNameById", mdl);
        //                            recipient.Name = deptName;

        //                        }
        //                        else
        //                        {
        //                            //recipient.Name = mdl.DeptId;
        //                            mdl.AllDepartmentIds = mdl.DeptId.Split(',').ToList();
        //                            string deptName = (string)Helper.ExecuteService("Department", "GetDeptNameById", mdl);
        //                            recipient.Name = deptName;
        //                        }
        //                        //recipient.Gender = mdl.Gender;
        //                        if (!string.IsNullOrEmpty(mdl.Gender))
        //                        {
        //                            if (mdl.Gender.ToUpper() == "M")
        //                            {
        //                                recipient.Gender = "Male";
        //                            }
        //                            else if (mdl.Gender.ToUpper() == "F")
        //                            {
        //                                recipient.Gender = "Female";
        //                            }
        //                            else
        //                            {
        //                                recipient.Gender = "Male";
        //                            }
        //                        }
        //                        recipient.Designation = mdl.Name + " (" + mdl.Designation + ")";
        //                        recipient.Email = mdl.MailID;
        //                        recipient.MobileNo = mdl.Mobile;

        //                        recipient.IsActive = true;
        //                        recipient.CreatedDate = DateTime.Now;


        //                        Helper.ExecuteService("ContactGroups", "CreateNewContactGroupMember", recipient);

        //                    }
        //                    else
        //                    {
        //                        //recipient.GroupID = 117;
        //                        if (IsHOD == "True")
        //                        {
        //                            recipient.GroupID = 118;
        //                            recipient.Address = mdl.Address;
        //                            if (mdl.DepartmentIDs != "NULL" && mdl.DepartmentIDs != "" && mdl.DepartmentIDs != null)
        //                            {
        //                                recipient.DepartmentCode = mdl.DepartmentIDs;
        //                            }
        //                            else
        //                            {
        //                                recipient.DepartmentCode = mdl.DeptId;
        //                            }
        //                            List<RecipientGroupMember> rlist = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", recipient);
        //                            recipient.RankingOrder = rlist.Count + 1;
        //                        }
        //                        else if (IsSecretoryId == "True")
        //                        {
        //                            recipient.GroupID = 117;
        //                            recipient.Address = "HP Secretariat, Shimla-2";
        //                            recipient.DepartmentCode = "HPD0012";
        //                            List<RecipientGroupMember> rlist = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", recipient);
        //                            recipient.RankingOrder = rlist.Count + 1;
        //                        }

        //                        if (mdl.DepartmentIDs != "NULL" && mdl.DepartmentIDs != "" && mdl.DepartmentIDs != null)
        //                        {
        //                            mdl.AllDepartmentIds = mdl.DepartmentIDs.Split(',').ToList();
        //                            string deptName = (string)Helper.ExecuteService("Department", "GetDeptNameById", mdl);
        //                            recipient.Name = deptName;
        //                        }
        //                        else
        //                        {
        //                            mdl.AllDepartmentIds = mdl.DeptId.Split(',').ToList();
        //                            string deptName = (string)Helper.ExecuteService("Department", "GetDeptNameById", mdl);
        //                            recipient.Name = deptName;
        //                        }
        //                        //recipient.Gender = mdl.Gender;
        //                        if (!string.IsNullOrEmpty(mdl.Gender))
        //                        {
        //                            if (mdl.Gender.ToUpper() == "M")
        //                            {
        //                                recipient.Gender = "Male";
        //                            }
        //                            else if (mdl.Gender.ToUpper() == "F")
        //                            {
        //                                recipient.Gender = "Female";
        //                            }
        //                            else
        //                            {
        //                                recipient.Gender = "Male";
        //                            }
        //                        }
        //                        recipient.Designation = mdl.Name + " (" + mdl.Designation + ")";
        //                        recipient.Email = mdl.MailID;
        //                        recipient.MobileNo = mdl.Mobile;

        //                        recipient.IsActive = true;
        //                        recipient.CreatedDate = DateTime.Now;
        //                        Helper.ExecuteService("ContactGroups", "UpdateContactGroupMemberFromUserReg", recipient);

        //                    }
        //                }
        //            }
        //        }
        //        else if (mUsers.UserType == 3)
        //        {
        //            mdl.IsMember = "False";
        //            mdl.IsMedia = "False";
        //            mdl.IsSecretoryId = false;
        //            mdl.IsHOD = false;
        //            mdl.IsOther = false;
        //            mdl.DeptId = mUsers.DeptId;
        //            mdl.UserName = mUsers.Name;
        //            mdl.OfficeLevel = mUsers.OfficeLevel;
        //            mdl.UserType = mUsers.UserType;
        //            mdl.SecretoryId = mUsers.SecretoryId;
        //            mdl.IsNodalOfficer = mUsers.IsNodalOfficer;

        //            //Update Nodal Officer
        //            RecipientGroupMember obj = new RecipientGroupMember();
        //            obj.GroupID = 163;
        //            obj.AadharId = mdl.AadarId;
        //            obj.MobileNo = mdl.Mobile;
        //            var dataAvailable = (RecipientGroupMember)Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByAadharIdAndMobileNumber", obj);

        //            if (dataAvailable !=null)
        //            {
        //                if(!string.IsNullOrEmpty(dataAvailable.AadharId))
        //                {
        //                obj.GroupMemberID = dataAvailable.GroupMemberID;                      
        //                var DepartmentNames = (string)Helper.ExecuteService("ContactGroups", "GetAllDepartmentByDepartmentCode", mdl.DeptId);
        //                obj.Name = DepartmentNames;
        //                obj.Gender = mdl.Gender;
        //                obj.Designation = mdl.Designation;
        //                if (mdl.Designation != null)
        //                {
        //                    obj.Designation = mdl.Name + " ," + mdl.Designation;
        //                }
        //                else
        //                {
        //                    obj.Designation = mdl.Name;
        //                }
        //                obj.Email = mdl.MailID;
        //                obj.Address = mdl.Address;
        //                obj.PinCode = "";
        //                obj.IsActive = true;
        //                obj.DepartmentCode = mdl.DeptId;
        //                obj.CreatedDate = DateTime.Now; 
        //                Helper.ExecuteService("ContactGroups", "UpdateContactGroupMember", obj);
        //                }
        //            }
                 
        //            if (mUsers.AllDepartmentIds.Count > 0)
        //            {
        //                mdl.DepartmentIDs = "";

        //                foreach (var dept in mUsers.AllDepartmentIds)
        //                {
        //                    mUsers.DepartmentIDs += dept + ",";
        //                }
        //                bool flag = mUsers.DepartmentIDs.EndsWith(",");
        //                if (flag)
        //                {
        //                    mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
        //                }
        //            }


        //        }
        //        else if (mUsers.UserType == 16)
        //        {
        //            mdl.IsMember = "False";
        //            mdl.IsMedia = "False";
        //            mdl.IsSecretoryId = false;
        //            mdl.IsHOD = false;
        //            mdl.IsOther = false;
        //            mdl.DeptId = mUsers.DeptId;
        //            mdl.UserName = mUsers.Name;
        //            mdl.OfficeLevel = mUsers.OfficeLevel;
        //            mdl.UserType = mUsers.UserType;

        //            if (mUsers.HODID == 0 || mUsers.HODID == null)
        //            {
        //                mdl.SecretoryId = mUsers.SecretoryId;
        //            }
        //            else
        //            {
        //                mdl.SecretoryId = mUsers.HODID;
        //            }
        //            if (mUsers.AllDepartmentIds.Count > 0)
        //            {
        //                mdl.DepartmentIDs = "";
        //                mUsers.DeptId = "";
        //                foreach (var dept in mUsers.AllDepartmentIds)
        //                {   
        //                    mUsers.DeptId  += dept + ",";
        //                    mUsers.DepartmentIDs += dept + ",";
        //                }
        //                bool flag = mUsers.DepartmentIDs.EndsWith(",");
        //                if (flag)
        //                {
        //                    mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
        //                }
        //                CurrentSession.DeptID = mUsers.DeptId;
        //            }

                   

        //        }
        //        else if (mUsers.UserType == 37)
        //        {
        //            mdl.IsMember = "False";
        //            mdl.IsMedia = "False";
        //            mdl.IsSecretoryId = false;
        //            mdl.IsHOD = false;
        //            mdl.IsOther = false;
        //            mdl.DeptId = mUsers.DeptId;
        //            mdl.UserName = mUsers.Name;
        //            mdl.OfficeLevel = mUsers.OfficeLevel;
        //            mdl.UserType = mUsers.UserType;
        //            mdl.SecretoryId = null;
        //            mdl.OfficeId = mUsers.OfficeId;
        //            if (mUsers.AllDepartmentIds.Count > 0)
        //            {
        //                mdl.DepartmentIDs = "";

        //                foreach (var dept in mUsers.AllDepartmentIds)
        //                {
        //                    mUsers.DepartmentIDs += dept + ",";
        //                }
        //                bool flag = mUsers.DepartmentIDs.EndsWith(",");
        //                if (flag)
        //                {
        //                    mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
        //                    mdl.DeptId = mdl.DepartmentIDs;
        //                }
        //            }


        //        }
        //        else if (mUsers.UserType == 5)
        //        {
        //            mdl.IsMember = "False";
        //            mdl.IsMedia = "False";
        //            mdl.IsSecretoryId = false;
        //            mdl.IsOther = false;
        //            mdl.IsHOD = false;
        //            mdl.DeptId = mUsers.DeptId;
        //            mdl.UserName = mUsers.Name;
        //            mdl.OfficeLevel = mUsers.OfficeLevel;
        //            mdl.UserType = mUsers.UserType;
        //            mdl.SecretoryId = mUsers.SecretoryId;
        //            if (mUsers.AllDepartmentIds.Count > 0)
        //            {
        //                mdl.DepartmentIDs = "";
        //                foreach (var dept in mUsers.AllDepartmentIds)
        //                {
        //                    mUsers.DepartmentIDs += dept + ",";
        //                }
        //                bool flag = mUsers.DepartmentIDs.EndsWith(",");
        //                if (flag)
        //                {
        //                    mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
        //                }
        //            }
        //            else if (mUsers.DepartmentIDs != null && mUsers.DepartmentIDs != "")
        //            {
        //                mdl.DepartmentIDs = mUsers.DepartmentIDs;
        //                mdl.DeptId = mUsers.DepartmentIDs;
        //            }
        //            else if (mUsers.DepartmentIDs == null || mUsers.DepartmentIDs == "")
        //            {
        //                mdl.DepartmentIDs = "HPD0001";
        //                mdl.DeptId = "HPD0001";
        //            }
        //        }
        //        else if (mUsers.UserType == 23 || mUsers.UserType == 25 || mUsers.UserType == 26)
        //        {
        //            mdl.IsMember = "False";
        //            mdl.IsMedia = "True";
        //            mdl.IsSecretoryId = false;
        //            mdl.IsOther = false;
        //            mdl.DeptId = null;
        //            mdl.UserName = mUsers.Name;
        //            mdl.OfficeId = 0;
        //            mdl.OfficeLevel = mUsers.OfficeLevel;
        //            mdl.IsSecretoryId = false;
        //            mdl.DepartmentIDs = null;
        //            mdl.IsHOD = false;
        //            //mdl.UserType = null;
        //            mdl.SecretoryId = null;
        //            mdl.UserType = mUsers.UserType;
        //        }
        //        else if (mUsers.UserType == 35)
        //        {
        //            mdl.IsOther = true;
        //            mdl.IsHOD = false;
        //            mdl.IsMember = "False";
        //            mdl.IsMedia = "False";
        //            mdl.IsSecretoryId = false;
        //            //mdl.IsOther = false;
        //            mdl.SecretoryId = null;
        //            mdl.DeptId = null;
        //            mdl.UserName = mUsers.Name;
        //            mdl.OfficeId = 0;
        //            mdl.OfficeLevel = mUsers.OfficeLevel;
        //            //mdl.IsSecretoryId = false;
        //            mdl.DepartmentIDs = null;
        //            //mdl.UserType = null;
        //            mdl.UserType = mUsers.UserType;
        //        }
        //        else if (mUsers.UserType == 27 || mUsers.UserType == 29 || mUsers.UserType == 30 || mUsers.UserType == 31 || mUsers.UserType == 32 || mUsers.UserType == 33 || mUsers.UserType == 34 || mUsers.UserType == 35)
        //        {
        //            mdl.IsOther = true;
        //            mdl.IsHOD = false;
        //            mdl.IsMember = "False";
        //            mdl.IsMedia = "False";
        //            mdl.IsSecretoryId = false;
        //            //mdl.IsOther = false;
        //            mdl.SecretoryId = null;
        //            mdl.DeptId = null;
        //            mdl.UserName = mUsers.Name;
        //            mdl.OfficeId = 0;
        //            mdl.OfficeLevel = mUsers.OfficeLevel;
        //            //mdl.IsSecretoryId = false;
        //            mdl.DepartmentIDs = null;
        //            //mdl.UserType = null;
        //            mdl.UserType = mUsers.UserType;
        //        }
        //        else if (mUsers.UserType == 38)
        //        {
        //            mdl.IsOther = false;
        //            mdl.IsHOD = false;
        //            mdl.IsMember = "False";
        //            mdl.IsMedia = "False";
        //            mdl.IsSecretoryId = false;
        //            mdl.SecretoryId = null;
        //            mdl.DeptId = null;
        //            mdl.UserName = mUsers.Name;
        //            mdl.OfficeId = 0;
        //            mdl.OfficeLevel = mUsers.OfficeLevel;
        //            mdl.DepartmentIDs = null;
        //            mdl.UserType = mUsers.UserType;
        //            if (mdl.AllMemberIds != null)
        //            {
        //                mdl.UserName = "";
        //                foreach (var ids in mdl.AllMemberIds)
        //                {
        //                    mdl.UserName += ids + ",";
        //                }
        //                bool flag = mdl.UserName.EndsWith(",");
        //                if (flag)
        //                {
        //                    mdl.UserName = mdl.UserName.Substring(0, mdl.UserName.Length - 1);
        //                }
        //            }
        //        }
        //        mdl.EmailId = mdl.MailID;
        //        mdl.MobileNo = mdl.Mobile;
        //        mdl.Designation = mdl.Designation;
        //        mdl.UserType = mUsers.UserType;
        //        mdl.AdhaarID = mUsers.AadarId;
        //        //mdl.UserName = mUsers.Name;
        //        //mdl.SecretoryId = mUsers.SecretoryId;
        //    }
        //    else
        //    {
        //        mdl.EmailId = mdl.MailID;
        //        mdl.MobileNo = mdl.Mobile;
        //        mdl.Designation = mdl.Designation;
        //    }
        //    if (mdl.UserName == null)
        //    {
        //        mdl.UserName = "";
        //    }


        //    var CheckUserDetails = (mUsers)Helper.ExecuteService("User", "GetUserDeptDetailsByUserID", mdl);



        //    bool checkbool = (bool)Helper.ExecuteService("User", "ExistUserSubTypeId", mdl);

        //    tUserAccessRequest mdlaccess = new tUserAccessRequest();
        //    mdlaccess.UserID = new Guid(CurrentSession.UserID);
        //    bool vtr = (bool)Helper.ExecuteService("Module", "ExistUserAccessRequestById", mdlaccess);
        //    if (vtr == true && checkbool == true)
        //    {
        //        if (CheckUserDetails.OfficeLevel != mUsers.OfficeLevel || CheckUserDetails.UserType != mUsers.UserType)
        //        {
        //            Helper.ExecuteService("Module", "DeleteUserAccessRequestByID", mdlaccess);

        //        }
        //    }

        //    var mdls = (mUsers)Helper.ExecuteService("User", "UpdatedMemberDetails", mdl);
        //    Helper.ExecuteService("User", "UpdateIdsInRequest", mdl);
        //    bool vtr1 = (bool)Helper.ExecuteService("Module", "ExistUserAccessRequestById", mdlaccess);
        //    if (mdl.UserType == 37 && vtr != true)
        //    {
        //        tUserAccessActions obj = new tUserAccessActions();
        //        tUserAccessRequest ustmdl = new tUserAccessRequest();
        //        obj.SubUserTypeID = Convert.ToInt32(mdl.UserType);
        //        mdl.UserAsscessActionlist = (List<tUserAccessActions>)Helper.ExecuteService("Module", "GetUserAccessActionByUserType", obj);
        //        foreach (var tr in mdl.UserAsscessActionlist)
        //        {
        //            ustmdl.UserID = new Guid(CurrentSession.UserID);
        //            ustmdl.AccessID = tr.UserAccessActionsId;
        //            ustmdl.ActionControlId = tr.MergeActionId;
        //            ustmdl.DomainAdminstratorId = 0;
        //            ustmdl.TypedDomainId = 37;
        //            ustmdl.AssociateDepartmentId = mdl.DepartmentIDs;
        //            ustmdl.DesignationId = 0;
        //            ustmdl.MemberId = 0;
        //            ustmdl.SecreataryId = 0;
        //            ustmdl.HODId = 0;
        //            ustmdl.IsAcceptedDate = DateTime.Now;
        //            ustmdl.Modifiedwhen = DateTime.Now;
        //            ustmdl.MergeActionId = tr.MergeActionId;
        //            ustmdl.SubUserTypeID = 37;
        //            ustmdl.SelectedAssociateDepartmentId = mdl.DepartmentIDs;
        //            ustmdl.SubAccessByUser = tr.MergeSubModuleId;
        //            ustmdl.OfficeID = Convert.ToInt32(mdl.OfficeId);
        //            Helper.ExecuteService("Module", "SaveUserAccessRequest", ustmdl);
        //        }
        //    }
        //    if (mdl.UserType == 37 && vtr == true)
        //    {
        //        tUserAccessRequest ustmdl = new tUserAccessRequest();
        //        ustmdl.UserID = new Guid(CurrentSession.UserID);
        //        Helper.ExecuteService("Module", "UpdateOfficerAccessRequest", ustmdl);
            
        //                    }
        //    // Helper.ExecuteService("Module", "SaveUserAccessRequest", mdl);


        //    TempData["Updated"] = "updated";
        //    return RedirectToAction("Index", "Home", new { area = "" });
        //}

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateMemberEmployeeDetails(mUsers mUsers, HttpPostedFileBase uploadPhoto, HttpPostedFileBase signatureUpload, string IsSecretoryId, string IsHOD)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "OtpRequiredInRegistration" });
            ViewBag.OtpRequiredInRegistration = "0";//SiteSettingsSessn.SettingValue;
            mUsers mdl = mUsers as mUsers;
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'long' is never equal to 'null' of type 'long?'
            if (mdl.OfficeId != null)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'long' is never equal to 'null' of type 'long?'
            {
                CurrentSession.OfficeId = Convert.ToString(mdl.OfficeId);
            }

            if (signatureUpload != null)
            {
                string DirPath = Server.MapPath("~/Images/SignaturePath/");
                if (!Directory.Exists(DirPath))
                {
                    Directory.CreateDirectory(DirPath);
                }
                var fileName = Path.GetFileName(signatureUpload.FileName);
                var path = Path.Combine(Server.MapPath("~/Images/SignaturePath/"), fileName);
                signatureUpload.SaveAs(path);

                ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                imgCompress.GetImage = new System.Drawing.Bitmap(path);
                imgCompress.Height = 240;
                imgCompress.Width = 180;
                imgCompress.Save(signatureUpload.FileName, DirPath);

                mdl.SignaturePath = "/Images/SignaturePath/" + signatureUpload.FileName;
            }

            if (uploadPhoto != null)
            {
                string DirPath = Server.MapPath("~/photos/");
                if (!Directory.Exists(DirPath))
                {
                    Directory.CreateDirectory(DirPath);
                }
                var fileName = Path.GetFileName(uploadPhoto.FileName);

                var path = Path.Combine(Server.MapPath("~/photos/"), fileName);
                uploadPhoto.SaveAs(path);

                //  var path = Path.Combine(DirPath, fileName);
                //  photoUpload.SaveAs(path);
                ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                imgCompress.GetImage = new System.Drawing.Bitmap(path);
                imgCompress.Height = 160;
                imgCompress.Width = 120;
                imgCompress.Save("compressingImage.jpg", DirPath);

                string newPath = Path.Combine(DirPath, "compressingImage.jpg");
                byte[] image = ReadImage(newPath);

                // System.IO.File.Delete(path);
                mdl.Photo = "data:image/png;base64," + Convert.ToBase64String(image);
            }

            if (uploadPhoto == null)
            {
                mdl.Photo = mUsers.Photo;
            }
#pragma warning disable CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
            if (TempData["Updated"] != "ReUpdated")
#pragma warning restore CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
            {
                if (mUsers.UserType == 17 || mUsers.UserType == 18 || mUsers.UserType == 19 || mUsers.UserType == 20 || mUsers.UserType == 21 || mUsers.UserType == 36)
                {
                    mdl.IsMember = "True";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    mdl.IsOther = false;
                    mdl.OfficeId = 0;
                    mdl.DeptId = null;
                    mdl.SecretoryId = null;
                    mdl.DepartmentIDs = null;
                    mdl.IsHOD = false;
                    mdl.UserType = mUsers.UserType;
                    //mdl.UserName = mUsers.Name;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    if (mUsers.EmpOrMemberCode == null || mUsers.EmpOrMemberCode == "")
                    {
                        int num;

                        bool isNum = Int32.TryParse(mUsers.UserName, out num);

                        if (isNum)
                        {
                            mdl.UserName = mUsers.UserName;
                        }
                        else
                        {
                            if (mUsers.AadarId.Contains("MLA") || mUsers.AadarId.Contains("HPD"))
                            {
                                mdl.UserName = mUsers.UserName_eCcon;
                            }
                            else
                            {
                                mdl.UserName = "0";
                            }
                        }

                    }
                    else
                    {
                        mdl.UserName = mUsers.EmpOrMemberCode;
                    }

                }
                if (mUsers.UserType == 22)
                {
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    mdl.IsOther = false;
                    mdl.OfficeId = 0;
                    mdl.DeptId = null;
                    //mdl.SecretoryId = null;
                    mdl.DepartmentIDs = null;
                    mdl.IsHOD = false;
                    mdl.UserType = mUsers.UserType;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    mdl.SecretoryId = null;
                    if (mUsers.EmpOrMemberCode == null || mUsers.EmpOrMemberCode == "")
                    {
                        mdl.UserName = mUsers.UserName;
                    }
                    else
                    {
                        mdl.UserName = mUsers.EmpOrMemberCode;
                    }

                }
                else if (mUsers.UserType == 2 || mUsers.UserType == 4 || mUsers.UserType == 15)
                {
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    mdl.IsOther = false;

                    //mdl.DeptId = mUsers.DeptId;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    mdl.UserType = mUsers.UserType;
                    mdl.SecretoryId = mUsers.SecretoryId;
                    if (mUsers.AllDepartmentIds.Count > 0)
                    {
                        mdl.DepartmentIDs = "";
                        mUsers.DeptId = "";
                        foreach (var dept in mUsers.AllDepartmentIds)
                        {
                            mUsers.DepartmentIDs += dept + ",";
                            mUsers.DeptId += dept + ",";
                        }
                        bool flag = mUsers.DepartmentIDs.EndsWith(",");
                        if (flag)
                        {
                            mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
                            // mdl.DeptId 
                        }
                    }
                    else if (mUsers.DepartmentIDs != null && mUsers.DepartmentIDs != "")
                    {
                        mdl.DepartmentIDs = mUsers.DepartmentIDs;
                        mdl.DeptId = mUsers.DepartmentIDs;
                    }

                    if (mUsers.UserType == 4)
                    {
                        if (mUsers.DepartmentIDs == null || mUsers.DepartmentIDs == "")
                        {
                            mdl.DepartmentIDs = "HPD0001";
                            mdl.DeptId = "HPD0001";
                        }
                    }
                    if (IsSecretoryId == "True")
                    {
                        mdl.IsSecretoryId = true;
                        mdl.IsHOD = false;
                        mdl.OfficeId = 0;
                        mSecretory secretory = new mSecretory();
                        secretory.AadhaarId = mdl.AadarId;
                        bool result = (bool)Helper.ExecuteService("Secretory", "CheckSecretaryExist", secretory);
                        if (result == false)
                        {
                            secretory.DeptID = mdl.DepartmentIDs;
                            secretory.SecretoryName = mdl.Name;
                            secretory.Email = mdl.MailID;
                            secretory.Mobile = mdl.Mobile;
                            secretory.IsActive = true;
                            secretory.DesignationDescription = mdl.Designation;
                            //secretory.ModifiedWhen = DateTime.Now;
                            secretory.AadhaarId = mUsers.AadarId;
                            //secretory.CreationDate = DateTime.Now;
                            Helper.ExecuteService("Secretory", "CreateSecretory", secretory);
                            mSecretoryDepartment secdept = new mSecretoryDepartment();
                            secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretaryByAdharid", secretory);
                            secdept.SecretoryID = secretory.SecretoryID;
                            foreach (var tval in mUsers.AllDepartmentIds)
                            {
                                secdept.DepartmentID = tval.ToString();
                                secdept.IsActive = true;
                                Helper.ExecuteService("SecretoryDepartment", "CreateSecretoryDepartment", secdept);
                            }

                        }
                        else
                        {
                            secretory.DeptID = mdl.DepartmentIDs;
                            secretory.SecretoryName = mdl.Name;
                            secretory.Email = mdl.MailID;
                            secretory.Mobile = mdl.Mobile;
                            secretory.IsActive = true;
                            secretory.DesignationDescription = mdl.Designation;
                            //secretory.ModifiedWhen = DateTime.Now;
                            secretory.AadhaarId = mUsers.AadarId;
                            //secretory.CreationDate = DateTime.Now;
                            Helper.ExecuteService("Secretory", "UpdateSecretoryByAdharId", secretory);
                            //mSecretoryDepartment secdept = new mSecretoryDepartment();
                            //secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretaryByAdharid", secretory);
                            //secdept.SecretoryID = secretory.SecretoryID;
                            //foreach (var tval in mUsers.AllDepartmentIds)
                            //{
                            //    secdept.DepartmentID = tval.ToString();
                            //    secdept.IsActive = true;
                            //    Helper.ExecuteService("SecretoryDepartment", "CreateSecretoryDepartment", secdept);
                            //}
                        }


                        secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretaryByAdharid", secretory);

                        mdl.SecretoryName = secretory.SecretoryName;
                        mdl.SecretoryId = secretory.SecretoryID;
                    }
                    if (IsHOD == "True")
                    {
                        mdl.IsHOD = true;
                        mdl.IsSecretoryId = false;
                        mdl.OfficeId = 0;
                        mHOD hods = new mHOD();
                        hods.AadhaarId = mdl.AadarId;
                        bool result = (bool)Helper.ExecuteService("HOD", "CheckHodExist", hods);
                        if (result == false)
                        {
                            hods.DeptID = mdl.DepartmentIDs;
                            hods.HODName = mdl.Name;
                            hods.Email = mdl.MailID;
                            hods.Mobile = mdl.Mobile;
                            hods.IsActive = true;
                            hods.DesignationDescription = mdl.Designation;
                            //secretory.ModifiedWhen = DateTime.Now;
                            hods.AadhaarId = mUsers.AadarId;
                            //secretory.CreationDate = DateTime.Now;
                            Helper.ExecuteService("HOD", "CreateHOD", hods);


                        }
                        else
                        {
                            hods.DeptID = mdl.DepartmentIDs;
                            hods.HODName = mdl.Name;
                            hods.Email = mdl.MailID;
                            hods.Mobile = mdl.Mobile;
                            hods.IsActive = true;
                            hods.DesignationDescription = mdl.Designation;
                            //secretory.ModifiedWhen = DateTime.Now;
                            hods.AadhaarId = mUsers.AadarId;
                            //secretory.CreationDate = DateTime.Now;
                            Helper.ExecuteService("HOD", "UpdateHODByAdharId", hods);
                            CurrentSession.DeptID = mdl.DepartmentIDs;
                        }
                        hods = (mHOD)Helper.ExecuteService("HOD", "GetHodByAdharid", hods);

                        mdl.SecretoryName = hods.HODName;
                        mdl.SecretoryId = hods.HODID;
                    }
                    bool obj = (bool)Helper.ExecuteService("Module", "CheckAccessRequestApproved", mdl);
                    if (obj == true)
                    {
                        if (IsHOD == "True" || IsSecretoryId == "True")
                        {
                            RecipientGroupMember recipient = new RecipientGroupMember();
                            recipient.AadharId = mdl.AadarId;


                            bool result1 = (bool)Helper.ExecuteService("ContactGroups", "CheckGroupMemberExist", recipient);
                            if (result1 == false)
                            {
                                if (IsHOD == "True")
                                {
                                    recipient.GroupID = 118;
                                    recipient.Address = mdl.Address;
                                    if (mdl.DepartmentIDs != "NULL" && mdl.DepartmentIDs != "" && mdl.DepartmentIDs != null)
                                    {
                                        recipient.DepartmentCode = mdl.DepartmentIDs;
                                    }
                                    else
                                    {
                                        recipient.DepartmentCode = mdl.DeptId;
                                    }

                                    List<RecipientGroupMember> rlist = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", recipient);
                                    recipient.RankingOrder = rlist.Count + 1;

                                }
                                else if (IsSecretoryId == "True")
                                {
                                    recipient.GroupID = 117;
                                    recipient.Address = "HP Secretariat, Shimla-2";
                                    recipient.DepartmentCode = "HPD0012";
                                    List<RecipientGroupMember> rlist = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", recipient);
                                    recipient.RankingOrder = rlist.Count + 1;
                                }

                                if (mdl.DepartmentIDs != "NULL" && mdl.DepartmentIDs != "" && mdl.DepartmentIDs != null)
                                {

                                    mdl.AllDepartmentIds = mdl.DepartmentIDs.Split(',').ToList();
                                    string deptName = (string)Helper.ExecuteService("Department", "GetDeptNameById", mdl);
                                    recipient.Name = deptName;

                                }
                                else
                                {
                                    //recipient.Name = mdl.DeptId;
                                    mdl.AllDepartmentIds = mdl.DeptId.Split(',').ToList();
                                    string deptName = (string)Helper.ExecuteService("Department", "GetDeptNameById", mdl);
                                    recipient.Name = deptName;
                                }
                                //recipient.Gender = mdl.Gender;
                                if (!string.IsNullOrEmpty(mdl.Gender))
                                {
                                    if (mdl.Gender.ToUpper() == "M")
                                    {
                                        recipient.Gender = "Male";
                                    }
                                    else if (mdl.Gender.ToUpper() == "F")
                                    {
                                        recipient.Gender = "Female";
                                    }
                                    else
                                    {
                                        recipient.Gender = "Male";
                                    }
                                }
                                recipient.Designation = mdl.Name + " (" + mdl.Designation + ")";
                                recipient.Email = mdl.MailID;
                                recipient.MobileNo = mdl.Mobile;

                                recipient.IsActive = true;
                                recipient.CreatedDate = DateTime.Now;


                                Helper.ExecuteService("ContactGroups", "CreateNewContactGroupMember", recipient);

                            }
                            else
                            {
                                //recipient.GroupID = 117;
                                if (IsHOD == "True")
                                {
                                    recipient.GroupID = 118;
                                    recipient.Address = mdl.Address;
                                    if (mdl.DepartmentIDs != "NULL" && mdl.DepartmentIDs != "" && mdl.DepartmentIDs != null)
                                    {
                                        recipient.DepartmentCode = mdl.DepartmentIDs;
                                    }
                                    else
                                    {
                                        recipient.DepartmentCode = mdl.DeptId;
                                    }
                                    List<RecipientGroupMember> rlist = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", recipient);
                                    recipient.RankingOrder = rlist.Count + 1;
                                }
                                else if (IsSecretoryId == "True")
                                {
                                    recipient.GroupID = 117;
                                    recipient.Address = "HP Secretariat, Shimla-2";
                                    recipient.DepartmentCode = "HPD0012";
                                    List<RecipientGroupMember> rlist = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", recipient);
                                    recipient.RankingOrder = rlist.Count + 1;
                                }

                                if (mdl.DepartmentIDs != "NULL" && mdl.DepartmentIDs != "" && mdl.DepartmentIDs != null)
                                {
                                    mdl.AllDepartmentIds = mdl.DepartmentIDs.Split(',').ToList();
                                    string deptName = (string)Helper.ExecuteService("Department", "GetDeptNameById", mdl);
                                    recipient.Name = deptName;
                                }
                                else
                                {
                                    mdl.AllDepartmentIds = mdl.DeptId.Split(',').ToList();
                                    string deptName = (string)Helper.ExecuteService("Department", "GetDeptNameById", mdl);
                                    recipient.Name = deptName;
                                }
                                //recipient.Gender = mdl.Gender;
                                if (!string.IsNullOrEmpty(mdl.Gender))
                                {
                                    if (mdl.Gender.ToUpper() == "M")
                                    {
                                        recipient.Gender = "Male";
                                    }
                                    else if (mdl.Gender.ToUpper() == "F")
                                    {
                                        recipient.Gender = "Female";
                                    }
                                    else
                                    {
                                        recipient.Gender = "Male";
                                    }
                                }
                                recipient.Designation = mdl.Name + " (" + mdl.Designation + ")";
                                recipient.Email = mdl.MailID;
                                recipient.MobileNo = mdl.Mobile;

                                recipient.IsActive = true;
                                recipient.CreatedDate = DateTime.Now;
                                Helper.ExecuteService("ContactGroups", "UpdateContactGroupMemberFromUserReg", recipient);

                            }
                        }
                    }
                }
                else if (mUsers.UserType == 3)
                {
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    mdl.IsHOD = false;
                    mdl.IsOther = false;
                    mdl.DeptId = mUsers.DeptId;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    mdl.UserType = mUsers.UserType;
                    mdl.SecretoryId = mUsers.SecretoryId;
                    mdl.IsNodalOfficer = mUsers.IsNodalOfficer;

                    //Update Nodal Officer
                    RecipientGroupMember obj = new RecipientGroupMember();
                    obj.GroupID = 163;
                    obj.AadharId = mdl.AadarId;
                    obj.MobileNo = mdl.Mobile;
                    var dataAvailable = (RecipientGroupMember)Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByAadharIdAndMobileNumber", obj);

                    if (dataAvailable != null)
                    {
                        if (!string.IsNullOrEmpty(dataAvailable.AadharId))
                        {
                            obj.GroupMemberID = dataAvailable.GroupMemberID;
                            var DepartmentNames = (string)Helper.ExecuteService("ContactGroups", "GetAllDepartmentByDepartmentCode", mdl.DeptId);
                            obj.Name = DepartmentNames;
                            obj.Gender = mdl.Gender;
                            obj.Designation = mdl.Designation;
                            obj.Email = mdl.MailID;
                            obj.Address = mdl.Address;
                            obj.PinCode = "";
                            obj.IsActive = true;
                            obj.DepartmentCode = mdl.DeptId;
                            obj.CreatedDate = DateTime.Now;
                            Helper.ExecuteService("ContactGroups", "UpdateContactGroupMember", obj);
                        }
                    }

                    if (mUsers.AllDepartmentIds.Count > 0)
                    {
                        mdl.DepartmentIDs = "";

                        foreach (var dept in mUsers.AllDepartmentIds)
                        {
                            mUsers.DepartmentIDs += dept + ",";
                        }
                        bool flag = mUsers.DepartmentIDs.EndsWith(",");
                        if (flag)
                        {
                            mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
                        }
                    }
                    mdl.DistrictCode = null;
                    mdl.ConstituencyId = null;
                    mdl.SubdivisionCode = null;

                }
                else if (mUsers.UserType == 16)
                {
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    mdl.IsHOD = false;
                    mdl.IsOther = false;
                    mdl.DeptId = mUsers.DeptId;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    mdl.UserType = mUsers.UserType;

#pragma warning disable CS0472 // The result of the expression is always 'false' since a value of type 'int' is never equal to 'null' of type 'int?'
                    if (mUsers.HODID == 0 || mUsers.HODID == null)
#pragma warning restore CS0472 // The result of the expression is always 'false' since a value of type 'int' is never equal to 'null' of type 'int?'
                    {
                        mdl.SecretoryId = mUsers.SecretoryId;
                    }
                    else
                    {
                        mdl.SecretoryId = mUsers.HODID;
                    }
                    if (mUsers.AllDepartmentIds.Count > 0)
                    {
                        mdl.DepartmentIDs = "";
                        mUsers.DeptId = "";
                        foreach (var dept in mUsers.AllDepartmentIds)
                        {
                            mUsers.DeptId += dept + ",";
                            mUsers.DepartmentIDs += dept + ",";
                        }
                        bool flag = mUsers.DepartmentIDs.EndsWith(",");
                        if (flag)
                        {
                            mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
                        }
                        CurrentSession.DeptID = mUsers.DeptId;
                    }
                    mdl.DistrictCode = null;
                    mdl.ConstituencyId = null;
                    mdl.SubdivisionCode = null;



                }
                else if (mUsers.UserType == 37)
                {
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    mdl.IsHOD = false;
                    mdl.IsOther = false;
                    mdl.DeptId = mUsers.DeptId;
                   // mdl.UserName = mUsers.Name;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    mdl.UserType = mUsers.UserType;
                    mdl.SecretoryId = null;
                    mdl.OfficeId = mUsers.OfficeId;
                    //mdl.FatherName = mUsers.FatherName;
                    //mdl.Gender = mUsers.Gender;
                    //mdl.DOB = mUsers.DOB;
                    //mdl.UserName = mUsers.Name;
                   // mdl.UserName = mUsers.UserName;
                    if (mUsers.AllDepartmentIds.Count > 0)
                    {
                        mdl.DepartmentIDs = "";

                        foreach (var dept in mUsers.AllDepartmentIds)
                        {
                            mUsers.DepartmentIDs += dept + ",";
                        }
                        bool flag = mUsers.DepartmentIDs.EndsWith(",");
                        if (flag)
                        {
                            mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
                            mdl.DeptId = mdl.DepartmentIDs;
                        }
                    }
                    mdl.DistrictCode = null;
                    mdl.ConstituencyId = null;
                    mdl.SubdivisionCode = null;

                }
                else if (mUsers.UserType == 5)
                {
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    mdl.IsOther = false;
                    mdl.IsHOD = false;
                    mdl.DeptId = mUsers.DeptId;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    mdl.UserType = mUsers.UserType;
                    mdl.SecretoryId = mUsers.SecretoryId;
                    if (mUsers.AllDepartmentIds.Count > 0)
                    {
                        mdl.DepartmentIDs = "";
                        foreach (var dept in mUsers.AllDepartmentIds)
                        {
                            mUsers.DepartmentIDs += dept + ",";
                        }
                        bool flag = mUsers.DepartmentIDs.EndsWith(",");
                        if (flag)
                        {
                            mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
                        }
                    }
                    else if (mUsers.DepartmentIDs != null && mUsers.DepartmentIDs != "")
                    {
                        mdl.DepartmentIDs = mUsers.DepartmentIDs;
                        mdl.DeptId = mUsers.DepartmentIDs;
                    }
                    else if (mUsers.DepartmentIDs == null || mUsers.DepartmentIDs == "")
                    {
                        mdl.DepartmentIDs = "HPD0001";
                        mdl.DeptId = "HPD0001";
                    }
                    mdl.DistrictCode = null;
                    mdl.ConstituencyId = null;
                    mdl.SubdivisionCode = null;
                }
                else if (mUsers.UserType == 23 || mUsers.UserType == 25 || mUsers.UserType == 26)
                {
                    mdl.IsMember = "False";
                    mdl.IsMedia = "True";
                    mdl.IsSecretoryId = false;
                    mdl.IsOther = false;
                    mdl.DeptId = null;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeId = 0;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    mdl.IsSecretoryId = false;
                    mdl.DepartmentIDs = null;
                    mdl.IsHOD = false;
                    //mdl.UserType = null;
                    mdl.SecretoryId = null;
                    mdl.UserType = mUsers.UserType;
                    mdl.DistrictCode = null;
                    mdl.ConstituencyId = null;
                    mdl.SubdivisionCode = null;
                }
                else if (mUsers.UserType == 35)
                {
                    mdl.IsOther = true;
                    mdl.IsHOD = false;
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    //mdl.IsOther = false;
                    mdl.SecretoryId = null;
                    mdl.DeptId = null;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeId = 0;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    //mdl.IsSecretoryId = false;
                    mdl.DepartmentIDs = null;
                    //mdl.UserType = null;
                    mdl.UserType = mUsers.UserType;
                    mdl.DistrictCode = null;
                    mdl.ConstituencyId = null;
                    mdl.SubdivisionCode = null;
                }
                else if (mUsers.UserType == 27 || mUsers.UserType == 29 || mUsers.UserType == 30 || mUsers.UserType == 31 || mUsers.UserType == 32 || mUsers.UserType == 33 || mUsers.UserType == 34 || mUsers.UserType == 35)
                {
                    mdl.IsOther = true;
                    mdl.IsHOD = false;
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    //mdl.IsOther = false;
                    mdl.SecretoryId = null;
                    mdl.DeptId = null;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeId = 0;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    //mdl.IsSecretoryId = false;
                    mdl.DepartmentIDs = null;
                    //mdl.UserType = null;
                    mdl.UserType = mUsers.UserType;
                    mdl.DistrictCode = null;
                    mdl.ConstituencyId = null;
                    mdl.SubdivisionCode = null;
                }
                else if (mUsers.UserType == 38)
                {
                    mdl.IsOther = false;
                    mdl.IsHOD = false;
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    mdl.SecretoryId = null;
                    mdl.DeptId = null;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeId = 0;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    mdl.DepartmentIDs = null;
                    mdl.UserType = mUsers.UserType;
                    if (mdl.AllMemberIds != null)
                    {
                        mdl.UserName = "";
                        foreach (var ids in mdl.AllMemberIds)
                        {
                            mdl.UserName += ids + ",";
                        }
                        bool flag = mdl.UserName.EndsWith(",");
                        if (flag)
                        {
                            mdl.UserName = mdl.UserName.Substring(0, mdl.UserName.Length - 1);
                        }
                    }
                    mdl.DistrictCode = null;
                    mdl.ConstituencyId = null;
                    mdl.SubdivisionCode = null;
                }
                //   Priyanka Work////
                else if (mUsers.UserType == 39 && mUsers.OfficeLevel == "4")
                {
                    mdl.IsOther = false;
                    mdl.IsHOD = false;
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    mdl.SecretoryId = null;
                    mdl.DeptId = null;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeId = 0;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    mdl.DepartmentIDs = null;
                    mdl.UserType = mUsers.UserType;
                    mdl.DistrictCode = mUsers.DistrictCode;
                    mdl.ConstituencyId = null;
                    mdl.SubdivisionCode = null;

                }
                else if (mUsers.UserType == 40 && mUsers.OfficeLevel == "4")
                {
                    mdl.IsOther = false;
                    mdl.IsHOD = false;
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    mdl.SecretoryId = null;
                 //   mdl.DeptId = null;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeId = mUsers.OfficeId;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    mdl.DepartmentIDs = null;
                    mdl.UserType = mUsers.UserType;
                    mdl.DistrictCode = mUsers.DistrictCode;
                    if (mUsers.AllDepartmentIds.Count > 0)
                    {
                        mdl.DepartmentIDs = "";
                        foreach (var dept in mUsers.AllDepartmentIds)
                        {
                            mUsers.DepartmentIDs += dept + ",";
                        }
                        bool flag = mUsers.DepartmentIDs.EndsWith(",");
                        if (flag)
                        {
                            mdl.DepartmentIDs = mUsers.DepartmentIDs.Substring(0, mUsers.DepartmentIDs.Length - 1);
                        }
                    }
                    else if (mUsers.DepartmentIDs != null && mUsers.DepartmentIDs != "")
                    {
                        mdl.DepartmentIDs = mUsers.DepartmentIDs;
                        mdl.DeptId = mUsers.DepartmentIDs;
                    }
                    else if (mUsers.DepartmentIDs == null || mUsers.DepartmentIDs == "")
                    {
                        mdl.DepartmentIDs = "HPD0001";
                        mdl.DeptId = "HPD0001";
                    }
                    //if (mUsers.ConIds.Count > 0)
                    //{
                    //    mdl.ConstituencyId = "";
                    //    foreach (var c in mUsers.ConIds)
                    //    {
                    //        mUsers.ConstituencyId += c + ",";
                    //    }
                    //    bool flag = mUsers.ConstituencyId.EndsWith(",");
                    //    if (flag)
                    //    {
                    //        mdl.ConstituencyId = mUsers.ConstituencyId.Substring(0, mUsers.ConstituencyId.Length - 1);
                    //    }
                    //}
                    //else if (mUsers.ConstituencyId != null && mUsers.ConstituencyId != "")
                    //{
                    //    mdl.ConstituencyId = mUsers.ConstituencyId;
                    // //   mdl.DeptId = mUsers.ConstituencyId;
                    //}
                    //else if (mUsers.ConstituencyId == null || mUsers.ConstituencyId == "")
                    //{
                    //    mdl.ConstituencyId = mUsers.ConstituencyId;
                    //}
                    //  mUsers.ConstituencyId = mUsers.Selected_ConId;
                    // mUsers.SubdivisionCode = mUsers.SubdivisionCode;

                    if ((mUsers.ConstituencyId == "" || mUsers.ConstituencyId == null) && (mUsers.Selected_ConId != ""))
                    {
                        mdl.ConstituencyId = mUsers.Selected_ConId;
                    }

                    if (mUsers.ConstituencyId == "" || mUsers.ConstituencyId == null)
                    {

                        mdl.ConstituencyId = mUsers.ConstituencyId;
                    }
                    else
                    {
                        mdl.ConstituencyId = mUsers.ConstituencyId.TrimEnd(',');
                    }
                    if ((mUsers.SubdivisionCode == "" || mUsers.SubdivisionCode == null) && (mUsers.Selected_SubCode != ""))
                    {
                        mdl.SubdivisionCode = mUsers.Selected_SubCode;
                    }
                    if (mUsers.SubdivisionCode == "" || mUsers.SubdivisionCode == null)
                    {
                        mdl.SubdivisionCode = mUsers.SubdivisionCode;
                    }
                    else
                    {
                        mdl.SubdivisionCode = mUsers.SubdivisionCode.TrimEnd(',');
                    }
                    //    if (mUsers.SubdivisionIds.Count > 0)
                    //    {
                    //        mdl.SubdivisionCode = "";
                    //        foreach (var c in mUsers.SubdivisionIds)
                    //        {
                    //            mUsers.SubdivisionCode = c + ",";
                    //        }
                    //        bool flag = mUsers.SubdivisionCode.EndsWith(",");
                    //        if (flag)
                    //        {
                    //            mdl.SubdivisionCode = mUsers.SubdivisionCode.Substring(0, mUsers.SubdivisionCode.Length - 1);
                    //        }
                    //    }
                    //    else if (mUsers.SubdivisionCode != null && mUsers.SubdivisionCode != "")
                    //    {
                    //        mdl.SubdivisionCode = mUsers.SubdivisionCode;
                    //        //   mdl.DeptId = mUsers.ConstituencyId;
                    //    }
                    //    else if (mUsers.SubdivisionCode == null || mUsers.SubdivisionCode == "")
                    //    {
                    //        mdl.SubdivisionCode = mUsers.SubdivisionCode;
                    //    }
                }
                else if (mUsers.UserType == 41 && mUsers.OfficeLevel == "4")
                {
                    mdl.IsOther = false;
                    mdl.IsHOD = false;
                    mdl.IsMember = "False";
                    mdl.IsMedia = "False";
                    mdl.IsSecretoryId = false;
                    mdl.SecretoryId = null;
                    mdl.DeptId = null;
                    mdl.UserName = mUsers.Name;
                    mdl.OfficeId = 0;
                    mdl.OfficeLevel = mUsers.OfficeLevel;
                    mdl.DepartmentIDs = null;
                    mdl.UserType = mUsers.UserType;
                    mdl.DistrictCode = null;
                    mdl.ConstituencyId = null;
                    mdl.SubdivisionCode = null;
                    mdl.PanchyatCode = mdl.PanchyatCode;
                }


                // Priyanka Work end///


                mdl.EmailId = mdl.MailID;
                mdl.MobileNo = mdl.Mobile;
                mdl.Designation = mdl.Designation;
                mdl.UserType = mUsers.UserType;
                mdl.AdhaarID = mUsers.AadarId;
                //mdl.UserName = mUsers.Name;
                //mdl.SecretoryId = mUsers.SecretoryId;
            }
            else
            {
                mdl.EmailId = mdl.MailID;
                mdl.MobileNo = mdl.Mobile;
                mdl.Designation = mdl.Designation;
            }
            mdl.FatherName = mUsers.FatherName;
            mdl.Gender = mUsers.Gender;
            mdl.DOB = mUsers.DOB;
            if (mdl.UserType != 22)
            {
                if (mdl.IsMember == "False")
                {
                    mdl.UserName = mUsers.Name;
                }
            }
            if (mdl.UserName == null)
            {
                mdl.UserName = "";
            }
           


            var CheckUserDetails = (mUsers)Helper.ExecuteService("User", "GetUserDeptDetailsByUserID", mdl);



            bool checkbool = (bool)Helper.ExecuteService("User", "ExistUserSubTypeId", mdl);

            tUserAccessRequest mdlaccess = new tUserAccessRequest();
            mdlaccess.UserID = new Guid(CurrentSession.UserID);
            bool vtr = (bool)Helper.ExecuteService("Module", "ExistUserAccessRequestById", mdlaccess);
            if (vtr == true && checkbool == true)
            {
                if (CheckUserDetails.OfficeLevel != mUsers.OfficeLevel || CheckUserDetails.UserType != mUsers.UserType)
                {
                    Helper.ExecuteService("Module", "DeleteUserAccessRequestByID", mdlaccess);

                }
            }

            var mdls = (mUsers)Helper.ExecuteService("User", "UpdatedMemberDetails", mdl);
            Helper.ExecuteService("User", "UpdateIdsInRequest", mdl);
            bool vtr1 = (bool)Helper.ExecuteService("Module", "ExistUserAccessRequestById", mdlaccess);
            if (mdl.UserType == 37 && vtr != true)
            {
                tUserAccessActions obj = new tUserAccessActions();
                tUserAccessRequest ustmdl = new tUserAccessRequest();
                obj.SubUserTypeID = Convert.ToInt32(mdl.UserType);
                mdl.UserAsscessActionlist = (List<tUserAccessActions>)Helper.ExecuteService("Module", "GetUserAccessActionByUserType", obj);
                foreach (var tr in mdl.UserAsscessActionlist)
                {
                    ustmdl.UserID = new Guid(CurrentSession.UserID);
                    ustmdl.AccessID = tr.UserAccessActionsId;
                    ustmdl.ActionControlId = tr.MergeActionId;
                    ustmdl.DomainAdminstratorId = 0;
                    ustmdl.TypedDomainId = 37;
                    ustmdl.AssociateDepartmentId = mdl.DepartmentIDs;
                    ustmdl.DesignationId = 0;
                    ustmdl.MemberId = 0;
                    ustmdl.SecreataryId = 0;
                    ustmdl.HODId = 0;
                    ustmdl.IsAcceptedDate = DateTime.Now;
                    ustmdl.Modifiedwhen = DateTime.Now;
                    ustmdl.MergeActionId = tr.MergeActionId;
                    ustmdl.SubUserTypeID = 37;
                    ustmdl.SelectedAssociateDepartmentId = mdl.DepartmentIDs;
                    ustmdl.SubAccessByUser = tr.MergeSubModuleId;
                    ustmdl.OfficeID = Convert.ToInt32(mdl.OfficeId);
                    Helper.ExecuteService("Module", "SaveUserAccessRequest", ustmdl);
                }
            }
            if (mdl.UserType == 37 && vtr == true)
            {
                tUserAccessRequest ustmdl = new tUserAccessRequest();
                ustmdl.UserID = new Guid(CurrentSession.UserID);
                Helper.ExecuteService("Module", "UpdateOfficerAccessRequest", ustmdl);

            }
            // Helper.ExecuteService("Module", "SaveUserAccessRequest", mdl);


            TempData["Updated"] = "updated";
            return RedirectToAction("Index", "Home", new { area = "" });
        }


        public ActionResult CheckDSCHasExist(string Dschashcode)
        {
            var mdl = (bool)Helper.ExecuteService("SecrtEmpDSCAuthorization", "CheckDSCHasExist", Dschashcode);

            return Json(mdl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateUserDetails()
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "OtpRequiredInRegistration" });
            ViewBag.OtpRequiredInRegistration = "0";// SiteSettingsSessn.SettingValue;
            if (!string.IsNullOrEmpty(CurrentSession.UserID))
            {
                mUsers user = new mUsers();
                user.DeptId = CurrentSession.DeptID;
                user.UserId = Guid.Parse(CurrentSession.UserID);
                user.SecretoryId = CurrentSession.SecretaryId != "" ? Int32.Parse(CurrentSession.SecretaryId) : 0;
                user.UserName = CurrentSession.UserName;

                user = (mUsers)Helper.ExecuteService("User", "GetUserDetails", user);
                if (user != null)
                {
                    user.Mobile = user.MobileNo;
                    user.MailID = user.EmailId;
                }
                //user.AllDepartmentIds = user.DepartmentIDs.Split(',').ToList();
                return PartialView("_UpdateUserDetails", user);
            }
            else
            {
                return RedirectToAction("Index", "Home", new { @area = "" });
            }

        }

        public ActionResult CheckOldPassword(string oldPassword)
        {
            mUsers userdetails = new mUsers();

            ServiceAdaptor login = new ServiceAdaptor();
            bool isoldpswdmatch = login.MatchPassword(CurrentSession.UserID, oldPassword);

            return Json(isoldpswdmatch, JsonRequestBehavior.AllowGet);
        }
      
        public ActionResult ChangePassword()
        {
            ChangePasswordModel model = new ChangePasswordModel();
            if (!string.IsNullOrEmpty(CurrentSession.UserID))
            {
                model.UserId = Guid.Parse(CurrentSession.UserID);
                model.UserName = CurrentSession.UserName;
                model.UserType = CurrentSession.SubUserTypeID;
                //ServiceAdaptor adptr = new ServiceAdaptor();
                //var data = adptr.DecryptData("M5hLBVZTqYzjVnnp2C2ITNFNq9fxCF9o2Y8=");
                return View("_ChangePassword", model);
            }
            return RedirectToAction("Index", "Home", new { @area = "" });
        }

        [HttpPost, ValidateAntiForgeryToken]
        [CaptchaVerify("Captcha Result is not valid")]
        public ActionResult SavePassword(ChangePasswordModel model)
        {
            //if (ModelState.IsValid)
            //{
                //string oldpassword = ComputeHash(model.OldPassword, "SHA1", null);
                //mUsers userdet = new mUsers();
                //userdet.UserId = model.UserId;
                //userdet.Password = oldpassword;
                //var isoldpswdmatch = (bool)Helper.ExecuteService("User", "CheckOldPassword", userdet);
                //if (isoldpswdmatch)
                //{
              
                if (Session["CaptchaImageText"] != null)
                {
                    if (this.Session["CaptchaImageText"].ToString() != model.CaptchaText)
                    {
                        model.CaptchaMessage = "Captcha Validation Failed!";
                        return View("_ChangePassword", model);
                    }
                }
                string ePassword = ComputeHash(model.NewPassword, "SHA1", null);
                mUsers userpswd = new mUsers();
                userpswd.Password = ePassword;
                userpswd.PasswordString = model.NewPassword;
                //userpswd.DeptId = CurrentSession.DeptID;
                userpswd.UserName = CurrentSession.UserName;
                userpswd.UserId = model.UserId;
                var details = (bool)Helper.ExecuteService("User", "ChangePassword", userpswd);
                CurrentSession.ClearSessions();
                return RedirectToAction("Index", "Home", new { @area = "" });
                //}
                //else
                //{
                //    ModelState.AddModelError("OldPassword", "Old Password Doesn't Match");
                //}

            //}
            //else
            //{
            //    var allErrors = ModelState.Values.SelectMany(x => x.Errors);
            //    model.CaptchaMessage = "Captcha result is not valid...";
            //    return View("_ChangePassword", model);
            //}
            //return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveUpdatedUserDetails(mUsers data, HttpPostedFileBase uploadPhoto)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "OtpRequiredInRegistration" });
            ViewBag.OtpRequiredInRegistration = "0";//SiteSettingsSessn.SettingValue;

            if (!string.IsNullOrEmpty(CurrentSession.UserID))
            {
                data.Photo = "";
                if (uploadPhoto != null)
                {
                    var fileName = Path.GetFileName(uploadPhoto.FileName);
                    var path = Path.Combine(Server.MapPath("~/photos/"), fileName);
                    uploadPhoto.SaveAs(path);
                    byte[] image = ReadImage(path);
                    System.IO.File.Delete(path);
                    data.Photo = "data:image/png;base64," + Convert.ToBase64String(image);
                }

                data.MobileNo = data.Mobile;
                data.EmailId = data.MailID;

                if (data.DeptId != "HPD0005" & data.AllDepartmentIds.Count != 0)
                {
                    string deptsid = "";
                    foreach (var item in data.AllDepartmentIds)
                    {
                        if (deptsid == "")
                        {
                            deptsid = item;
                        }
                        else
                        {
                            deptsid = deptsid + "," + item;
                        }
                    }
                    data.DepartmentIDs = deptsid;
                    CurrentSession.SecretaryId = data.SecretoryId.ToString();
                }
                Helper.ExecuteService("User", "UpdateUserDetails", data);
                return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { @area = "PaperLaidDepartment" });
            }
            else
            {
                return RedirectToAction("Index", "Home", new { @area = "" });
            }
        }

        public ActionResult DeactivateUserDsc(int DscId)
        {
            if (!string.IsNullOrEmpty(CurrentSession.UserID))
            {
                mUserDSHDetails details = new mUserDSHDetails();
                details.IdKey = DscId;
                details.UserId = Guid.Parse(CurrentSession.UserID);
                var userDscLst = (List<mUserDSHDetails>)Helper.ExecuteService("User", "DeactivateUserDsc", details);
                return PartialView("_UserDscList", userDscLst);
            }
            else
            {
                return RedirectToAction("Index", "Home", new { @area = "" });
            }

        }

        public ActionResult ActivateUserDsc(int DscId)
        {
            if (!string.IsNullOrEmpty(CurrentSession.UserID))
            {
                mUserDSHDetails details = new mUserDSHDetails();
                details.IdKey = DscId;
                details.UserId = Guid.Parse(CurrentSession.UserID);
                var userDscLst = (List<mUserDSHDetails>)Helper.ExecuteService("User", "ActivateUserDsc", details);
                return PartialView("_UserDscList", userDscLst);
            }
            else
            {
                return RedirectToAction("Index", "Home", new { @area = "" });
            }

        }

        public JsonResult CheckAdhaarID(string AdhaarID)
        {
            mUsers user = new mUsers();
            user.AadarId = AdhaarID;
            user = (mUsers)Helper.ExecuteService("User", "CheckAdhaarID", user);
            if (user != null)
            {
            }
            else
            {
                user = new mUsers();
                user = null;
            }
            return Json(user, JsonRequestBehavior.AllowGet);
        }

        public object GetAdhaarDetails(string AdhaarID, string IsModel)
        {
            AdhaarDetails details = new AdhaarDetails();
            AdhaarServices.ServiceSoapClient obj = new AdhaarServices.ServiceSoapClient();
            EncryptionDecryption.EncryptionDecryption objencr = new EncryptionDecryption.EncryptionDecryption();
            string inputValue = objencr.Encryption(AdhaarID.Trim());
            try
            {
                DataTable dt = obj.getHPKYCInDataTable(inputValue);
                if (dt != null && dt.Rows.Count > 0)
                {
                    details.AdhaarID = AdhaarID;
                    details.Name = objencr.Decryption(Convert.ToString(dt.Rows[0]["Name"]));
                    details.FatherName = objencr.Decryption(Convert.ToString(dt.Rows[0]["FatherName"]));
                    details.Gender = objencr.Decryption(Convert.ToString(dt.Rows[0]["Gender"]));
                    details.Address = objencr.Decryption(Convert.ToString(dt.Rows[0]["Address"]).ToString());

                    details.DOB = objencr.Decryption(Convert.ToString(dt.Rows[0]["DOB"]));
                    string dateTime = details.DOB;
                    DateTime dob = Convert.ToDateTime(dateTime);
                    details.DOB = String.Format("{0:dd/MM/yyyy}", dob);
                    details.MobileNo = objencr.Decryption(Convert.ToString(dt.Rows[0]["MobileNumber"]));
                    details.Email = objencr.Decryption(Convert.ToString(dt.Rows[0]["EmailID"]));
                    details.District = objencr.Decryption(Convert.ToString(dt.Rows[0]["DistrictName"]).ToString());
                    details.PinCode = objencr.Decryption(Convert.ToString(dt.Rows[0]["PinCOde"]).ToString());

                    Byte[] bytes = (Byte[])Convert.FromBase64String(objencr.Decryption(dt.Rows[0]["photo"].ToString()));
                    if (bytes.Length > 0)
                    {
                        MemoryStream ms = new MemoryStream(bytes);
                        System.Drawing.Image image = System.Drawing.Image.FromStream(ms);



                        string DirPath = Server.MapPath("~/photos/");
                        if (!Directory.Exists(DirPath))
                        {
                            Directory.CreateDirectory(DirPath);
                        }
                        ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                        imgCompress.GetImage = new System.Drawing.Bitmap(image);
                        imgCompress.Height = 160;
                        imgCompress.Width = 120;
                        imgCompress.Save("compressingAdhaarImage.jpg", DirPath);

                        string newPath = Path.Combine(DirPath, "compressingAdhaarImage.jpg");
                        byte[] newImage = ReadImage(newPath);

                        details.Photo = "data:image/png;base64," + Convert.ToBase64String(newImage);
                    }
                    else
                    {
                        details.Photo = "data:image/png;base64,";
                    }


                    //                    details.Photo = "data:image/png;base64," + Convert.ToBase64String(bytes);
                    if (IsModel == "True")
                    {
                        return details;
                    }
                    else
                    {
                        return PartialView("_AdhaarIDDetails", details);
                    }
                }
                else
                {
                    if (IsModel == "True")
                    {
                        return details;
                    }
                }
                return PartialView("_AdhaarIDDetails", details);
            }
            catch
            {
                //return false;
                //details.AdhaarID = "error";
                return PartialView("_AdhaarIDDetails", details);
            }

        }

        public JsonResult GetDetailsByEmpID(string Id, string SelectType, string deptId)
        {
            mUsers user = new mUsers();
            mEmployee employee = new mEmployee();


            mMember member = new mMember();
            if (SelectType == "1")
            {
                member.MemberCode = Convert.ToInt32(Id);
                member = (mMember)Helper.ExecuteService("Member", "GetMemberDetailsByMemberCode", member);
                if (member != null)
                {
                    user.Name = member.Prefix + " " + member.Name;
                    user.Address = member.PermanentAddress;
                    user.Mobile = member.Mobile;
                    user.MailID = member.Email;

                    if (member.Sex != null)
                    {
                        user.Gender = member.Sex;
                    }
                    else
                    {
                        user.Gender = "Not Available";
                    }
                    if (member.FatherName != null)
                    {
                        user.FatherName = member.FatherName;
                    }
                    else
                    {
                        user.FatherName = "Not Available";
                    }
                }
            }
            else if (SelectType == "2")
            {
                employee.empcd = Id;
                employee.deptid = deptId;

                //GenpmisMasterService PmisService = new GenpmisMasterService();
                //DataTable dt = PmisService.PortData_WS_EmployeeLeaveDetails("gen##$$pmis", "#123A#_?.", deptId, Id);
                //if (dt != null && dt.Rows.Count > 0)
                //{ 
                //    employee.empfname = Convert.ToString(dt.Rows[0]["Name"]);
                //    employee.empgender=Convert.ToString(dt.Rows[0]["Gender"]);
                //    employee.emphmtown=Convert.ToString(dt.Rows[0]["Address"]);

                //}

                employee = (mEmployee)Helper.ExecuteService("Employee", "GetEmployeeDetialsByEmpID", employee);

                if (employee != null)
                {
                    if (employee.empgender == "F")
                    {
                        user.Name = "Smt. " + employee.empfname + " " + employee.empmname + " " + employee.emplname;
                    }
                    else if (employee.empgender == "M")
                    {
                        user.Name = "Shri. " + employee.empfname + " " + employee.empmname + " " + employee.emplname;
                    }
                    user.Address = employee.emphmtown;
                    user.OfficeId = Convert.ToInt64(employee.officeid);
                    user.FatherName = employee.empfmhname;
                    user.Gender = employee.empgender;
                    if (user.Gender == "M")
                    { user.Gender = "Male"; }
                    else if (user.Gender == "F")
                    { user.Gender = "Female"; }
                    else
                    { user.Gender = "Not Available"; }
                    //     user.MailID=employee.em
                }
            }
            else if (SelectType == "3")
            {
                Journalist jour = new Journalist();
                jour.JournalistCode = Convert.ToInt32(Id);
                jour = (Journalist)Helper.ExecuteService("Media", "GetJournalistByJournalistCode", jour);
                if (jour != null)
                {
                    user.Name = jour.Prefix + " " + jour.Name;
                    user.Address = jour.Address;
                    user.MailID = jour.Email;
                    user.Mobile = jour.Phone;
                    if (jour.Gender != null)
                    {
                        user.Gender = jour.Gender;
                    }
                    else
                    {
                        user.Gender = "Not Available";

                    } if (jour.FatherName != null)
                    {
                        user.FatherName = jour.FatherName;
                    }
                    else
                    {
                        user.FatherName = "Not Available";
                    }
                }
            }

            return Json(user, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSelectedDeptBySecID(string secratoryId)
        {
            mSecretoryDepartment SecDept = new mSecretoryDepartment();
            SecDept.SecretoryID = Convert.ToInt32(secratoryId);
            mUsers model = new mUsers();
            model.SecretoryDepartmentModel = (ICollection<SecretoryDepartmentModel>)Helper.ExecuteService("SecretoryDepartment", "GetSecretoryDepartmentBySecID", SecDept);
            SecretoryDepartmentModel mSecretoryDepartment = new SecretoryDepartmentModel();
            return PartialView("_SecretoryDepartment", model);
        }
        public ActionResult GetAllDeptList()
        {
            mDepartment dept = new mDepartment();
            // SecDept.SecretoryID = Convert.ToInt32(secratoryId);
            mUsers model = new mUsers();
            model.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", dept);
            //SecretoryDepartmentModel mSecretoryDepartment = new SecretoryDepartmentModel();
            return PartialView("_SecretoryDepartment", model);
        }
        public JsonResult CheckDSCHaskKey(string key)
        {
            string HashKey = key;
            mUserDSHDetails DSCDetails = new mUserDSHDetails();
            DSCDetails.HashKey = HashKey;
            mUsers user = new mUsers();
            DSCDetails = (mUserDSHDetails)Helper.ExecuteService("User", "GetDSCDetailsByHashKey", DSCDetails);
            if (DSCDetails == null)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            return Json(DSCDetails, JsonRequestBehavior.AllowGet);
        }

        #region OTP Code
        public ActionResult CheckOTPByIDForUserUpdate(string empId, string Mobile, string OTPValue)
        {
            tOTPDetails model = new tOTPDetails();
            model.UserName = empId;
            model.MobileNo = Mobile;
            model.OTP = OTPValue;
            model = (tOTPDetails)Helper.ExecuteService("OTPDetails", "GetOTPByID", model);
            var isexist = model != null ? true : false;
            return Json(isexist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckOTPByID(string empId, string Mobile, string OTPValue)
        {
            tOTPDetails model = new tOTPDetails();
            model.UserName = empId;
            model.MobileNo = Mobile;
            model.OTP = OTPValue;
            model = (tOTPDetails)Helper.ExecuteService("OTPDetails", "GetOTPByID", model);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRegisteredDetails(string Id, string deptId, string selectType)
        {
            mUsers model = new mUsers();
            model.UserName = Id;
            model.DeptId = deptId;
            model.OfficeLevel = selectType;
            model = (mUsers)Helper.ExecuteService("User", "GetRegisteredDetails", model);


            if (model == null)
            {
                model = new mUsers();
                model.UserName = "Exists";
            }
            //else
            //{

            //    model.UserName = "Not Exist";
            //}
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOTP(string empId, string mobile)
        {
            mobile = mobile.Trim();
            if (mobile.Length > 10)
            {
                mobile = mobile.Substring(0, mobile.Length - 1);
            }

            //if (Utility.CurrentSession.MemberID == "")
            //    return RedirectToAction("Index", "Home", new { area = "" });

            ////tOTPDetailsVS obj = new tOTPDetailsVS();
            //obj.ModuleId = "1";
            //if (Utility.CurrentSession.UserName != null)
            //{
            //    obj.UserName = Utility.CurrentSession.UserName;
            //}
            //ServiceParameter serParam1 = ServiceParameter.Create("Bills", "GetOTP", obj);
            //ServiceAdaptor serv1 = new ServiceAdaptor();
            //obj = (tOTPDetailsVS)serv1.CallService(serParam1);
            //if (obj == null)
            //{


            ///generate OTP
            string OTP = GenerateOTPNmuber();
            tOTPDetails otp = new tOTPDetails();
            otp.MobileNo = mobile;
            otp.UserName = empId.ToString();
            otp.OTP = OTP;
            otp = (tOTPDetails)Helper.ExecuteService("OTPDetails", "AddOTP", otp);

            ///Get User Infoin

            //tBillRegisterVs BillRegisterVs = new tBillRegisterVs();
            //if (Utility.CurrentSession.UserName != null)
            //{
            //    mMemberVS MemberVS = new mMemberVS();
            //    MemberVS.MemberID = Convert.ToInt16(Utility.CurrentSession.UserName);
            //    ServiceParameter serviceParameter = ServiceParameter.Create("Member", "GetMemberbyid", MemberVS);
            //    ServiceAdaptor serv = new ServiceAdaptor();
            //    BillRegisterVs = (tBillRegisterVs)serv.CallService(serviceParameter);
            //}

            // string mobile = "";

            //foreach (var item in BillRegisterVs.mMemberVS)
            //{
            //    mobile = item.Mobile;
            //}
            //string OTPid = "";
            /////Save OTP Details in DB
            //if (mobile != "" && mobile != null)
            //{
            //    tOTPDetailsVS obj = new tOTPDetailsVS();
            //    obj.OTP = OTP;
            //    obj.ModuleId = "1";
            //    obj.IsMatched = "Not Used";
            //    obj.MobileNo = mobile;
            //    if (Utility.CurrentSession.UserName != null)
            //    {
            //        obj.UserName = Utility.CurrentSession.UserName;
            //    }
            //    ServiceParameter serParam = ServiceParameter.Create("Bills", "AddOTP", obj);
            //    ServiceAdaptor serv1 = new ServiceAdaptor();
            //    OTPid = Convert.ToString((int)serv1.CallService(serParam));

            //    ///Send OTP SMS
            //    //XmlDocument createdXML = CreateSMSXmlDoc(mobile, null, "8894887081", "Your OTP for Bills is" + OTP + " ,For OTP Id :" + OTPid + " .");
            //    //byte[] byteToPass = ConverXMLToByteArray(createdXML);
            //    //SBL.eAssembly.ServiceAdaptor.SMSServiceReference.SMSServiceClient serviceClient = new SBL.eAssembly.ServiceAdaptor.SMSServiceReference.SMSServiceClient();
            //    ////  serviceClient.Execute(byteToPass);
            //}



            //  }
            //BillRegisterVs = new tBillRegisterVs();
            //BillRegisterVs.BillId = BillId;
            //BillRegisterVs.message = Type;
            //if (OTPid != "")
            //{
            //    BillRegisterVs.OTPid = OTPid;
            //}
            try
            {
                SMSService Services = new SMSService();
                SMSMessageList smsMessage = new SMSMessageList();
                smsMessage.SMSText = "Your eVidhan OTP is " + otp.OTP + " .";
                smsMessage.ModuleActionID = 1;
                smsMessage.UniqueIdentificationID = 1;
                smsMessage.MobileNo.Add(mobile);
                Notification.Send(true, false, smsMessage, null);

                //Services.sendSingleSMS("hpgovt", "hpdit@1234", "hpgovt", mobile, "Your eVidhan OTP is " + otp.OTP + " .");

            }
            catch
            {

            }
            return PartialView("_OTP", otp);
        }

        public string GenerateOTPNmuber()
        {
            string OTP = GetUniqueKey();

            //      XmlDocument createdXML = CreateSMSXmlDoc("8894887081", null, "8894887081", "Your OTP for Bills is" + OTP+" .");
            //      byte[] byteToPass = ConverXMLToByteArray(createdXML);
            //      SBL.eAssembly.ServiceAdaptor.SMSServiceReference.SMSServiceClient serviceClient = new SBL.eAssembly.ServiceAdaptor.SMSServiceReference.SMSServiceClient();
            //  serviceClient.Execute(byteToPass);



            return OTP;


        }

        public static string GetUniqueKey()
        {
            int maxSize = 6; // whatever length you want
            // char[] chars = new char[62];
            string a;
            a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            char[] chars = new char[a.Length];
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }
        #endregion

        public static string GeneratePassword()
        {
            int maxSize = 8; // whatever length you want
            // char[] chars = new char[62];
            string a;
            a = "1qaz2wsx3edc4rfv5tgb6yhn7ujm8ik9ol";
            char[] chars = new char[a.Length];
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }

        public static string GenerateTempAdhaarID()
        {
            int maxSize = 11; // whatever length you want
            // char[] chars = new char[62];
            string a;
            a = "1234567890";
            char[] chars = new char[a.Length];
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return "X" + result.ToString();
        }




        public ActionResult GetMemberDetails()
        {

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "OtpRequiredInRegistration" });
            ViewBag.OtpRequiredInRegistration = "0";//SiteSettingsSessn.SettingValue;

            if (CurrentSession.UserID != null)
            {
                try
                {


                    mUsers user = new mUsers();
                    user.UserId = new Guid(CurrentSession.UserID);
                    user.IsMember = CurrentSession.IsMember;

                    user = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", user);
                    if (user != null)
                    {

                        mSecretory secretory = new mSecretory();
                        mOffice office = new mOffice();

                        user.mSecretory = (ICollection<mSecretory>)Helper.ExecuteService("Secretory", "GetAllSecratoryDetails", secretory);
                        if (user.DepartmentIDs != null)
                        {
                            user.AllDepartmentIds = user.DepartmentIDs.Split(',').ToList();
                            string DeptName = "";
                            DeptName = (string)Helper.ExecuteService("Department", "GetDeptNameById", user);
                            if (DeptName != null && DeptName != "")
                            {
                                DeptName = DeptName.TrimEnd(',');
                                user.DepartmentName = DeptName;
                            }

                        }
                        else if (user.DeptId != null)
                        {
                            user.AllDepartmentIds = user.DeptId.Split(',').ToList();
                            string DeptName = "";
                            DeptName = (string)Helper.ExecuteService("Department", "GetDeptNameById", user);
                            if (DeptName != null && DeptName != "")
                            {
                                DeptName = DeptName.TrimEnd(',');
                                user.DepartmentName = DeptName;
                            }

                        }
                        mDepartment depart = new mDepartment();
                        depart.deptId = user.DeptId;
                        if (user.ConstituencyId != null && user.ConstituencyId != "")
                        {
                            user.Selected_ConId = user.ConstituencyId;
                            //user.ConstituencyIDs = user.ConstituencyId;
                            user.ConIds = user.ConstituencyId.Split(',').ToList();
                            string ConName = "";
                            ConName = (string)Helper.ExecuteService("Constituency", "getConstituencyNameByid", user);
                            if (ConName != null && ConName != "")
                            {
                                ConName = ConName.TrimEnd(',');
                                user.ConstituencyName = ConName;
                            }
                        }
                        if (user.SubdivisionCode != null && user.SubdivisionCode != "")
                        {
                            user.Selected_SubCode = user.SubdivisionCode;
                            user.SubdivisionIds = user.SubdivisionCode.Split(',').ToList();
                            string DivisionName = "";
                            DivisionName = (string)Helper.ExecuteService("Constituency", "getDivisionNameByid", user);
                            if (DivisionName != null && DivisionName != "")
                            {
                                DivisionName = DivisionName.TrimEnd(',');
                                user.SubdivisionName = DivisionName;
                            }
                        }
                        user.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", user);
                        user.MembersList = (ICollection<mMember>)Helper.ExecuteService("Member", "GetAllMembersList", null);
                        user.HodsList = (ICollection<mHOD>)Helper.ExecuteService("HOD", "GetAllHODList", null);

                        user.UserTypeList = (ICollection<mUserType>)Helper.ExecuteService("Module", "GetUserTypeDropDown", null);
                        user.DistrictList = (ICollection<DistrictModel>)Helper.ExecuteService("Constituency", "GetAllDistrict_By_Assembly", null);
                        user.ConstituencyList = (ICollection<mConstituency>)Helper.ExecuteService("Constituency", "getConstituency_ByDistrict", user.DistrictCode);
                        // List<fillListGenricInt> _list1 = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getDistrictConstituency", user.DistrictCode);
                        user.SubdivisionList = (ICollection<mSubDivision>)Helper.ExecuteService("Constituency", "getSubdivision_ByConstituency", user.ConstituencyId);
                        user.PanchyatList = (ICollection<mPanchayat>)Helper.ExecuteService("Constituency", "getAllPanchayat", null);
                        // List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "get_subdevision", user.ConstituencyCode);
                        user.Mobile = user.MobileNo;
                        user.MailID = user.EmailId;
                        user.Sel_DistrictCode = user.DistrictCode;
                        //user.Selected_ConId = user.Selected_ConId;
                        user.Selected_PanchyatCode = user.PanchyatCode;

                        if (user.UserName != null)
                        {
                            user.AllMemberIds = user.UserName.Split(',').ToList();
                        }
                        return View("UpdateMemberUserDetails", user);
                    }
                }
                catch
                {
                    return RedirectToAction("LoginUP", "Account", new { area = "" });
                }
            }

            return RedirectToAction("LoginUP", "Account", new { area = "" });
        }


        public ActionResult GetMemberDetailsONLoginTime()
        {


            if (CurrentSession.UserID != null)
            {
                try
                {


                    mUsers user = new mUsers();
                    user.UserId = new Guid(CurrentSession.UserID);
                    user.IsMember = CurrentSession.IsMember;

                    user = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", user);
                    if (user != null)
                    {

                        mSecretory secretory = new mSecretory();
                        mOffice office = new mOffice();

                        user.mSecretory = (ICollection<mSecretory>)Helper.ExecuteService("Secretory", "GetAllSecratoryDetails", secretory);
                        if (user.DepartmentIDs != null)
                        {
                            user.AllDepartmentIds = user.DepartmentIDs.Split(',').ToList();
                        }
                        mDepartment depart = new mDepartment();
                        depart.deptId = user.DeptId;
                        user.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", user);
                        user.MembersList = (ICollection<mMember>)Helper.ExecuteService("Member", "GetAllMembersList", null);
                        user.HodsList = (ICollection<mHOD>)Helper.ExecuteService("HOD", "GetAllHODList", null);

                        user.UserTypeList = (ICollection<mUserType>)Helper.ExecuteService("Module", "GetUserTypeDropDown", null);
                        user.Mobile = user.MobileNo;
                        user.MailID = user.EmailId;

                        return PartialView("_UpdateMemberUserDetailsOnLoginTime", user);
                    }
                }
                catch
                {
                    return RedirectToAction("LoginUP", "Account", new { area = "" });
                }
            }

            return RedirectToAction("LoginUP", "Account", new { area = "" });
        }

        public ActionResult GetSecretaryDrpList(int usertypeid)
        {
            mUsers user = new mUsers();
            mSecretory secretory = new mSecretory();
            if (usertypeid != 3)
            {
                user.mSecretory = (ICollection<mSecretory>)Helper.ExecuteService("Secretory", "GetAllSecratoryDetails", secretory);
            }
            else
            {
                secretory.SecretoryID = 75;
                secretory = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretoryDetailsById", secretory);
                user.SecretoryId = secretory.SecretoryID;
                user.SecretoryName = secretory.SecretoryName;
            }

            return PartialView("_SecretaryList", user);
        }
        public ActionResult GetHodDrpList(int usertypeid)
        {
            mUsers user = new mUsers();
            mHOD hods = new mHOD();
            //if (usertypeid != null)
            //{
            user.HodsList = (ICollection<mHOD>)Helper.ExecuteService("HOD", "GetAllHODList", hods);
            //}


            return PartialView("_HodsList", user);
        }
        public ActionResult GetDepartmentDrpList(int usertypeid)
        {
            mUsers user = new mUsers();
            user.UserId = new Guid(CurrentSession.UserID);
            user.IsMember = CurrentSession.IsMember;

            user = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", user);
            if (user.DepartmentIDs != null)
            {
                user.AllDepartmentIds = user.DepartmentIDs.Split(',').ToList();
            }
            mDepartment depart = new mDepartment();

            if (usertypeid != 3)
            {
                user.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", user);
            }
            else
            {
                depart.deptId = "HPD0001";
                depart = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentDetailsById", depart);
                user.DepartmentIDs = depart.deptId;
                user.DepartmentName = depart.deptname;
            }

            return PartialView("_SecretoryDepartment", user);
        }
        public ActionResult OfficeDrpList(string deptid)
        {
            mUsers user = new mUsers();
            user.DeptId = deptid;
            user.OfficeDrpList = (ICollection<mOffice>)Helper.ExecuteService("Office", "GetOfficeListByDepartmentID", user);
            return PartialView("_OfficeList", user);

        }

        public ActionResult GetUserSubTypeDrp(string usertypeid)
        {
            mSubUserType Usbuser = new mSubUserType();
            Usbuser.UserTypeID = Convert.ToInt32(usertypeid);
            mUsers user = new mUsers();
            user.SubUserTypeList = (ICollection<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeByUserTypeId", Usbuser);


            return PartialView("GetUserSubTypeDrp", user);

        }

        public mUserModelClass FailedCaptcha(mUserModelClass user)
        {


            var radioButtonList = new List<KeyValuePair<int, string>>() { 
           new KeyValuePair<int, string>(1,Resources.UserRegistration.Honable),
           new KeyValuePair<int, string>(2,Resources.UserRegistration.Department),
            new KeyValuePair<int, string>(3,Resources.UserRegistration.Media)
            };
            user.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", user);
            mDepartment dep = new mDepartment();

            user.radioList = radioButtonList;
            mSecretory secretory = new mSecretory();
            user.mSecretory = (ICollection<mSecretory>)Helper.ExecuteService("Secretory", "GetAllSecratoryDetails", secretory);
            mSecretory sec = new mSecretory();

            List<SecretoryDepartmentModel> mSecretoryDepartment = new List<SecretoryDepartmentModel>();

            user.SecretoryDepartmentModel = mSecretoryDepartment;
            return user;

        }

        [HttpPost, ValidateAntiForgeryToken]
        [CaptchaVerify("Captcha Result is not valid")]
        public ActionResult SubmitUserDetails(mUserModelClass model1, HttpPostedFileBase photoUpload, string CaptchaText)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "OtpRequiredInRegistration" });
            ViewBag.OtpRequiredInRegistration = "0";//SiteSettingsSessn.SettingValue;
            if (ModelState.ContainsKey("OTPValue") && SiteSettingsSessn.SettingValue == "0")
                ModelState["OTPValue"].Errors.Clear();
            mUsers model = new mUsers();
            if (Session["CaptchaImageText"] != null)
            {
                if (this.Session["CaptchaImageText"].ToString() != CaptchaText)
                {
                    model1.CaptchaMessage = "Captcha Validation Failed!";
                    return View("UserRegistraitonForm", model1);
                }
            }
            //if (!ModelState.IsValid)
            //{
            //    model1.CaptchaMessage = "Captcha result is not valid...";
            //    model1 = FailedCaptcha(model1);
            //    return View("UserRegistraitonForm", model1);
            //}

           
            model.AdhaarID = model1.AdhaarID;
            model.MailID = model1.MailID;
            model.Address = model1.Address;
            model.Mobile = model1.Mobile;
            model.Photo = model1.Photo;
            model.UserId = model1.UserId;
            model.Gender = model1.Gender;
            model.FatherName = model1.FatherName;
            if (SiteSettingsSessn.SettingValue == "0")
                model.OTPValue = "0";
            else
                model.OTPValue = model1.OTPValue;

            model.DOB = model1.DOB;
            model.Name = model1.UserName;
            model.UserName = model1.UserName;
            if (photoUpload != null)
            {
                var fileName = Path.GetFileName(photoUpload.FileName);
                string DirPath = Server.MapPath("~/photos/");
                if (!Directory.Exists(DirPath))
                {
                    Directory.CreateDirectory(DirPath);
                }
                var path = Path.Combine(DirPath, fileName);
                photoUpload.SaveAs(path);
                ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                imgCompress.GetImage = new System.Drawing.Bitmap(path);
                imgCompress.Height = 160;
                imgCompress.Width = 120;
                imgCompress.Save("compressingImage.jpg", DirPath);

                string newPath = Path.Combine(DirPath, "compressingImage.jpg");
                byte[] image = ReadImage(newPath);

                model.Photo = "data:image/png;base64," + Convert.ToBase64String(image);
            }
            if (model.AdhaarID != null && model.AdhaarID != "")
            {
                AdhaarDetails details = new AdhaarDetails();
                //details = (AdhaarDetails)GetAdhaarDetails(model.AdhaarID, "True");
                //if (details.AdhaarID == null || details.Name == null)
                //{
                details.AdhaarID = model.AdhaarID;
                details.Name = model.Name;
                details.Gender = model.Gender;
                details.FatherName = model.FatherName;
                details.DOB = model.DOB;
                details.MobileNo = model.Mobile;
                details.Email = model.MailID;
                details.Address = model.Address;
                details.Photo = model.Photo;

                //}
                details = (AdhaarDetails)Helper.ExecuteService("User", "AddAadharDetails", details);

            }
            //model = users;



            //string password = Membership.GeneratePassword(8, 1);
            string password = GeneratePassword();
            #region Encrypt Password

            string ePassword = ComputeHash(password, "SHA1", null);

            #endregion


            #region Add user detailes

            // model.EmpId = model.EmpOrMemberCode;
            model.Password = ePassword;
            model.IsDSCAuthorized = false;
            model.UserId = Guid.NewGuid();
            model.EmailId = model.MailID;
            model.MobileNo = model.Mobile;
            //model.UserName = model.UserName;
            model.IsActive = true;
            model.IsNotification = true;
            model.Permission = false;
            model.WrongAttemptCount = 0;
            if (model.AdhaarID != null)
            {
                model.AadarId = model.AdhaarID;
            }
            else
            {
                model.AadarId = GenerateTempAdhaarID();
            }
            model.IsOther = false;
            model.IsHOD = false;
            model.IsMember = "False";
            model.IsMedia = "False";
            model.IsSecretoryId = false;
            model.IsOther = false;
            model.DeptId = null;
            model.OfficeId = 0;
            model.OfficeLevel = null;
            model.DepartmentIDs = null;
            model.EmpId = null;
            model.EmpOrMemberCode = "";
            try
            {
                SMSService Services = new SMSService();
                SMSMessageList smsMessage = new SMSMessageList();
                smsMessage.SMSText = "Thanks for registration on eVidhan.\n\n Your eVidhan Id is " + model.AadarId + " and password is " + password;
                smsMessage.ModuleActionID = 1;
                smsMessage.UniqueIdentificationID = 1;
                smsMessage.MobileNo.Add(model.MobileNo);
                Notification.Send(true, false, smsMessage, null);


                //Services.sendSingleSMS("hpgovt", "hpdit@1234", "hpgovt", model.MobileNo, "Thanks for registration on eVidhan.\n\n Your eVidhan Id is " + model.AadarId + " and password is " + password);

            }
            catch
            {

            }
            model = (mUsers)Helper.ExecuteService("User", "AddUserDetails", model);

            #endregion

            #region Add Usert DSC Detials
            if (model.DSCHash != null)
            {
                mUserDSHDetails DSCDetails = new mUserDSHDetails();
                DSCDetails.UserId = model.UserId;
                DSCDetails.HashKey = model.DSCHash;
                DSCDetails.Name = model.DSCName;
                DSCDetails.Type = model.DSCType;
                DSCDetails.EnrollDate = DateTime.Now;

                if (model.DSCValidity != null)
                {
                    DSCDetails.Validity = DateTime.ParseExact(model.DSCValidity, "ddd MMM dd HH:mm:ss IST yyyy", null);
                }
                if (model.DSCHash != null)
                {
                    DSCDetails.IsActive = true;
                }
                else { DSCDetails.IsActive = false; }
                DSCDetails = (mUserDSHDetails)Helper.ExecuteService("User", "ADDUserDSCDetails", DSCDetails);
            }
            #endregion

            #region Add Temp USer

            TempUser tempUser = new TempUser();
            tempUser.userGuid = model.UserId;
            tempUser.UserId = model.UserName;
            tempUser.password = password;
            tempUser = (TempUser)Helper.ExecuteService("User", "AddTempUSerData", tempUser);

            #endregion
            #region mail sending
            // bool test = MailSend(model.EmailId, model.EmpId, model.AadarId, password);
            //if (test)
            // {
            TempData["submitted"] = "Submitted";
            TempData["password"] = tempUser.password;
            //  }
            #endregion



            return RedirectToAction("LoginUP", "Account", new { area = "" });
        }

        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool MailSend(string userMail, string userName, string eVidhanID, string Password)
        {
            try
            {
                //                SMTP Server: mail.nic.in
                //SMTP Port: 465
                //Username: it-vs-hp@nic.in
                //Password: Bhandari$1
                var fromAddress = new MailAddress("it-vs-hp@nic.in", "eVidhan");
                var toAddress = new MailAddress(userMail, userName);
                const string fromPassword = "Bhandari$1";
                const string subject = "eVidhan ID and Password for Login Credentials";
                string body = "Welcome User " + userName + " !<br/><br/><br/>" + "This is your Login Credential <br/> eVidhan ID: " + userName + "<br/>Password: " + Password +
                    "for login eVidhan.<br/><br/><br/><br/>Thanks!<br/>eVidhan Team<br/><br/><br/><span style='color:red'> THIS IS SYSTEM GENERATED MAIL PLEASE, DO NOT REPLY</span>";

                var smtp = new SmtpClient
                {
                    Host = "mail.nic.in",
                    Port = 465,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                    return true;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return false;
            }


        }

        #region Encryptio/Decrytption

        public static string ComputeHash(string plainText, string hashAlgorithm, byte[] saltBytes)
        {
            // If salt is not specified, generate it on the fly.
            if (saltBytes == null)
            {
                // Define min and max salt sizes.
                int minSaltSize = 4;
                int maxSaltSize = 8;

                // Generate a random number for the size of the salt.
                Random random = new Random();
                int saltSize = random.Next(minSaltSize, maxSaltSize);

                // Allocate a byte array, which will hold the salt.
                saltBytes = new byte[saltSize];

                // Initialize a random number generator.
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

                // Fill the salt with cryptographically strong byte values.
                rng.GetNonZeroBytes(saltBytes);
            }

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes =
                    new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];


            HashAlgorithm hash;
            // Make sure hashing algorithm name is specified.
            if (hashAlgorithm == null)
                hashAlgorithm = "";

            // Initialize appropriate hashing algorithm class.
            switch (hashAlgorithm.ToUpper())
            {
                case "SHA1":
                    hash = new SHA1Managed();
                    break;

                case "SHA256":
                    hash = new SHA256Managed();
                    break;

                case "SHA384":
                    hash = new SHA384Managed();
                    break;

                case "SHA512":
                    hash = new SHA512Managed();
                    break;

                default:
                    hash = new MD5CryptoServiceProvider();
                    break;
            }

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(hashWithSaltBytes);

            // Return the result.
            return hashValue;
        }
        #endregion

        public ActionResult GoToNewLayOut()
        {
            if (!string.IsNullOrEmpty(CurrentSession.UserID))
            {
                mUsers user = new mUsers();
                user.DeptId = CurrentSession.DeptID;
                user.UserId = Guid.Parse(CurrentSession.UserID);
                user.SecretoryId = Int32.Parse(CurrentSession.SecretaryId);
                user.UserName = CurrentSession.UserName;

                user = (mUsers)Helper.ExecuteService("User", "GetUserDetails", user);
                user.Mobile = user.MobileNo;
                user.MailID = user.EmailId;
                //user.AllDepartmentIds = user.DepartmentIDs.Split(',').ToList();
                return View("GoToNewLayOut", user);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }



        public ActionResult UpdateAssemblyPassword(string txtAssemblyPassword)
        {
            mUsers mdl = new mUsers();
            if (!string.IsNullOrEmpty(CurrentSession.UserID))
            {
                mdl.AssemblyPassword = txtAssemblyPassword;
                mdl.UserId = Guid.Parse(CurrentSession.UserID);
                var mdls = (mUsers)Helper.ExecuteService("User", "UpdateAssemblyPassword", mdl);
                return Content("1");
            }
            else
            {
                return RedirectToAction("Index", "Home", new { @area = "" });
            }
        }
        public JsonResult GetDistrictDrpList()
        {
            mUsers user = new mUsers();
            // user.DeptId = deptid;
            user.DistrictList = (ICollection<DistrictModel>)Helper.ExecuteService("Constituency", "GetAllDistrict", null);
            // return PartialView("_OfficeList", user);
            return Json(user.DistrictList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetConstituencyDrpList(string DistCode)
        {
            mUsers user = new mUsers();

            user.ConstituencyList = (ICollection<mConstituency>)Helper.ExecuteService("Constituency", "getConstituency_ByDistrict", DistCode);

            // return Json(user.ConstituencyList, JsonRequestBehavior.AllowGet);
            return PartialView("_ConstituencyDrpList", user);
        }

        public ActionResult GetSubdevisionDrpList(string ConstituencyCode)
        {
            mUsers user = new mUsers();
            string sid = ConstituencyCode.TrimEnd(',');
            user.SubdivisionList = (ICollection<mSubDivision>)Helper.ExecuteService("Constituency", "getSubdivision_ByConstituency", sid);
            // return Json(user.SubdivisionList, JsonRequestBehavior.AllowGet);
            return PartialView("_SubDivisionDrpList", user);
        }

    }
}
