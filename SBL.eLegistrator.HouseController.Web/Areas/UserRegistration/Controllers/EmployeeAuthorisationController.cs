﻿using SBL.DomainModel.ComplexModel.UserComplexModel;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Role;
using SBL.DomainModel.Models.Secretory;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.UserRegistration.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class EmplyeeAuthorisationController : Controller
    {
        //
        // GET: /UserRegistration/EmplyeeAuthorisation/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetEmployeeAuthorizedList()
        {
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                string userName = CurrentSession.UserName;
                string deptId = CurrentSession.DeptID;
                mSecretory mSec = new mSecretory();
                mSec.DeptID = deptId;
                mSec.EmpCode = Convert.ToInt32(userName);
                mSec = (mSecretory)Helpers.Helper.ExecuteService("Secretory", "GetSecretaryByDetails", mSec);
                mUsers user = new mUsers();
                user.SecretoryId = mSec.SecretoryID;
                AgeList listOfPermission = new AgeList();
                listOfPermission.ID = 1;
                listOfPermission.permissionName = "Save";
                user.AgeList.Add(listOfPermission);
                listOfPermission = new AgeList();
                listOfPermission.ID = 2;
                listOfPermission.permissionName = "Save & Send";
                user.AgeList.Add(listOfPermission);
                user.mUserModelClass = (ICollection<mUserModelClass>)Helpers.Helper.ExecuteService("User", "GetAuthorizedUserDetailsBySecID", user);
                user.IsAuthorized = true;
                return PartialView("_GetEmployeeAuthorisationList", user);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        public ActionResult GetEmployeeAuthorisationList()
        {
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                string userName = CurrentSession.UserName;
                string deptId = CurrentSession.DeptID;
                mSecretory mSec = new mSecretory();
                mSec.DeptID = deptId;
                mSec.EmpCode = Convert.ToInt32(userName);
                mSec = (mSecretory)Helpers.Helper.ExecuteService("Secretory", "GetSecretaryByDetails", mSec);
                mUsers user = new mUsers();
                user.SecretoryId = mSec.SecretoryID;
                user.AdhaarID = mSec.AadhaarId;
                AgeList listOfPermission = new AgeList();
                listOfPermission.ID = 1;
                listOfPermission.permissionName = "Save";
                user.AgeList.Add(listOfPermission);
                listOfPermission = new AgeList();
                listOfPermission.ID = 2;
                listOfPermission.permissionName = "Save & Send";
                user.AgeList.Add(listOfPermission);
                user.mUserModelClass = (ICollection<mUserModelClass>)Helpers.Helper.ExecuteService("User", "GetUserDetailsBySecID", user);
                user.IsAuthorized = false;
                return PartialView("_GetEmployeeAuthorisationList", user);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        public object UnAuthorizedEmployees(string unAthorisedIds)
        {
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                string[] IDs = unAthorisedIds.Split(',');

                foreach (var uID in IDs)
                {
                    mUsers user = new mUsers();
                    user.UserId = new Guid(uID);
                    Helpers.Helper.ExecuteService("User", "UnAuthrorizedEmployee", user);
                }
                return null;
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        public object RemoveEmployeeList(string checkIds)
        {
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                string[] IDs = checkIds.Split(',');

                foreach (var uID in IDs)
                {
                    AuthorisedEmployee aEmpl = new AuthorisedEmployee();
                    aEmpl.UserId = new Guid(uID);
                    Helpers.Helper.ExecuteService("User", "RemoveEmployeeByUSerID", aEmpl);
                }
                return null;
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        public object SubmitAuthorizedEmployees(string CheckIDs, string permissionIds)
        {
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                string[] IDs = CheckIDs.Split(',');
                string[] pIds = permissionIds.Split(',');
                foreach (var uID in IDs)
                {
                    mUsers user = new mUsers();
                    user.UserId = new Guid(uID);
                    user = (mUsers)Helpers.Helper.ExecuteService("User", "GetUserDetailsByUserID", user);
                    string userName = CurrentSession.UserName;
                    string deptId = CurrentSession.DeptID;
                    mSecretory mSec = new mSecretory();
                    mSec.DeptID = deptId;
                    mSec.EmpCode = Convert.ToInt32(userName);
                    mSec = (mSecretory)Helpers.Helper.ExecuteService("Secretory", "GetSecretaryByDetails", mSec);
                    AuthorisedEmployee authEmployee = new AuthorisedEmployee();
                    authEmployee.UserId = user.UserId;
                    authEmployee = (AuthorisedEmployee)Helper.ExecuteService("Employee", "GetAuthorizedEmployeeDetails", authEmployee);
                    if (authEmployee != null)
                    {
                        authEmployee.AssociatedDepts = authEmployee.AssociatedDepts;
                        authEmployee.IsPermissions = authEmployee.IsPermissions;
                    }
                    else
                    {
                        authEmployee = new AuthorisedEmployee();
                        authEmployee.AssociatedDepts = user.DepartmentIDs;
                        authEmployee.IsPermissions = false;
                    }
                    authEmployee.SecretaryId = mSec.SecretoryID;
                    authEmployee.EmployeeId = user.UserName;
                    authEmployee.SecAadhaarID = mSec.AadhaarId;
                    authEmployee.EmpAadhaarID = user.AadarId;
                    authEmployee.ModifiedBy = userName;
                    authEmployee.ModifiedWhen = DateTime.Now;
                    authEmployee.UserId = user.UserId;
                    authEmployee.isAuthorized = true;
                    authEmployee.IsActive = true;
                    authEmployee.EmployeeSelectedDepts = user.DepartmentIDs;
                    authEmployee = (AuthorisedEmployee)Helper.ExecuteService("Employee", "SubmitAuthorizedEmployeeDetails", authEmployee);
                    string roleID = ConfigurationManager.AppSettings["roleDescription"];
                    tUserRoles userRoles = new tUserRoles();
                    userRoles.RoleDetailsID = Guid.NewGuid();
                    userRoles.Roleid = new Guid(roleID);
                    userRoles.UserID = new Guid(uID);
                    userRoles.IsActive = true;
                    Helpers.Helper.ExecuteService("User", "AssignRoles", userRoles);
                }
                //foreach (var userIdwithPermission in pIds)
                //{
                //    string[] userPermission = userIdwithPermission.Split(' ');
                //    mUsers user = new mUsers();
                //    user.UserId = new Guid(userPermission[0]);
                //    if (userPermission[1] == "1")
                //    {
                //        user.Permission = false;
                //    }
                //    else if (userPermission[1] == "2")
                //    { user.Permission = true; }
                //    Helpers.Helper.ExecuteService("User", "SetUserPermission", user);
                //}

                return null;
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        public ActionResult GetEmployeeFullDetails(string DeptId, string EmpCode, string UserID)
        {
            if (CurrentSession.UserName != null && CurrentSession.UserName != "" && UserID!="")
            {
                mUsers user = new mUsers();
                user.DeptId = DeptId;
                user.UserName = EmpCode;
                user.UserId = new Guid(UserID);
                mUserModelClass UserModel = new mUserModelClass();
                UserModel = (mUserModelClass)Helper.ExecuteService("Employee", "GetEmployeeFullDetailsDetails", user);
                if (UserModel != null)
                {
                    AgeList listOfPermission = new AgeList();
                    listOfPermission.ID = 1;
                    listOfPermission.permissionName = "Save";
                    UserModel.AgeList.Add(listOfPermission);
                    listOfPermission = new AgeList();
                    listOfPermission.ID = 2;
                    listOfPermission.permissionName = "Save & Send";
                    UserModel.AgeList.Add(listOfPermission);
                    AuthorisedEmployee authEmp = new AuthorisedEmployee();
                    authEmp.UserId = UserModel.UserId;
                    authEmp = (AuthorisedEmployee)Helper.ExecuteService("Employee", "GetAuthorizedEmployeeDetails", authEmp);
                    if (authEmp != null)
                    {
                        UserModel.DepartmentIDs = authEmp.EmployeeSelectedDepts;
                        UserModel.DeptId = authEmp.AssociatedDepts;
                        if (authEmp.IsPermissions)
                        {
                            UserModel.Age = 2;
                        }
                        else
                        {
                            UserModel.Age = 1;
                        }
                    }
                    else
                    {
                        UserModel.DeptId = null;
                    }
                    if (UserModel.DepartmentIDs != null && UserModel.DepartmentIDs != "")
                    {

                        mDepartment department = new mDepartment();
                        department.deptId = UserModel.DepartmentIDs;
                        UserModel.AllDepartmentIds = UserModel.DepartmentIDs.Split(',').ToList();
                        UserModel.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartmentByMultipleIDs", department);
                    }
                }
                return PartialView("_EmployeeFullDetails", UserModel);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        public object AddAuthorizedEmployeeDetails(string SelectedChkbxAssociatedDeptId, string EmployeeUserID, string Permission, string SecAadhaarId, string SecId, string EmpCode, string EmpAadhaarID, string UserId, string actualDepartments)
        {
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                AuthorisedEmployee authEmployee = new AuthorisedEmployee();
                authEmployee.AssociatedDepts = SelectedChkbxAssociatedDeptId;
                authEmployee.EmployeeId = EmpCode;
                if (Permission == "1")
                {
                    authEmployee.IsPermissions = false;
                }
                else
                {
                    authEmployee.IsPermissions = true;
                }
                authEmployee.SecretaryId = Convert.ToInt32(SecId);
                authEmployee.SecAadhaarID = SecAadhaarId;
                authEmployee.EmpAadhaarID = EmpAadhaarID;
                authEmployee.IsActive = true;
                authEmployee.ModifiedBy = CurrentSession.UserName;
                authEmployee.ModifiedWhen = DateTime.Now;
                authEmployee.isAuthorized = false;
                authEmployee.EmployeeSelectedDepts = actualDepartments;
                authEmployee.UserId = new Guid(UserId);
                authEmployee = (AuthorisedEmployee)Helper.ExecuteService("Employee", "SubmitAuthorizedEmployeeDetails", authEmployee);
                return null;
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

    }
}
