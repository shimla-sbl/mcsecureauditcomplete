﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.UserRegistration.Models
{
    [Serializable]
    public class ChangePasswordModel
    {
        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public string UserType { get; set; }

        [Required]
        [Display(Name = "Old Password")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required]
        [MinLength(6, ErrorMessage = "Password Must Have Minimum 6 Characters")]
        [Display(Name = "New Password")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,}", ErrorMessage = "Minimum 6 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character.")]
        public string NewPassword { get; set; }

        [Required]
        [Compare("NewPassword", ErrorMessage = "Both Password Should Be Same")]
        [Display(Name = "Confirmation Password")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,}", ErrorMessage = "Minimum 6 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character.")]
        public string ConfirmationPassword { get; set; }
        public string CaptchaMessage { get; set; }

        [Required]
       // [MinLength(5, ErrorMessage = "Captcha Required")]
     
        [Display(Name = "Captcha")]
        public string CaptchaText { get; set; }

    }
}