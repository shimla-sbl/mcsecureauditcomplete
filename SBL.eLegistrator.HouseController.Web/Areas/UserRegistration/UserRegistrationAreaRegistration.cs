﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.UserRegistration
{
    public class UserRegistrationAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "UserRegistration";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "UserRegistration_default",
                "UserRegistration/{controller}/{action}/{id}",
                new { action = "UserRegistraitonForm", id = UrlParameter.Optional }
            );
        }
    }
}
