﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PrintingPress
{
    public class PrintingPressAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PrintingPress";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PrintingPress_default",
                "PrintingPress/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
