﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.PrintingPress;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.PrintingPress.Controllers
{
    public class PrintingPressController : Controller
    {
        //
        // GET: /PrintingPress/PrintingPress/

        public ActionResult Index()
        {
            PrintingPressModel model = new PrintingPressModel();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            model = (PrintingPressModel)Helper.ExecuteService("PrintingPress", "GetAssemblySessionList", model);
            model.mDepartment = (List<mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", model);
            //model = (PrintingPressModel)Helper.ExecuteService("PrintingPress", "GetDepartment", model);
            model.mPaperType = (List<mPaperCategoryType>)Helper.ExecuteService("PrintingPress", "GetPaperType", model);
            model = (PrintingPressModel)Helper.ExecuteService("PrintingPress", "GetCountForPrinting", model);
            if (string.IsNullOrEmpty(CurrentSession.AssemblyId) || string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.AssemblyId = model.AssemblyID.ToString();
                CurrentSession.SessionId = model.SessionID.ToString();
            }
            return View(model);
        }
        public ActionResult GetRecieptList(int AssemblyId, int SessionId)
        {

            tPrintingPress model = new tPrintingPress();
            model.AssemblyId = AssemblyId;
            model.SessionId = SessionId;
            model = (tPrintingPress)Helper.ExecuteService("PrintingPress", "GetListtprint", model);
            return PartialView("_GetRecieptList", model);
        }

        public ActionResult GetCompListDept(int AssemblyId, int SessionId)
        {

            tPrintingPress model = new tPrintingPress();
            model.AssemblyId = AssemblyId;
            model.SessionId = SessionId;
            model = (tPrintingPress)Helper.ExecuteService("PrintingPress", "GetCompleteprintList", model);
            return PartialView("_GetDispatchList", model);
        }

        public ActionResult GetRecieptListByDepartment(int AssemblyId, int SessionId, int Papertype, string DeptmentId)
        {

            tPrintingPress model = new tPrintingPress();
            model.AssemblyId = AssemblyId;
            model.SessionId = SessionId;
            model.DepartmentId = DeptmentId;
            model.PaperTypeID = Papertype;
            model = (tPrintingPress)Helper.ExecuteService("PrintingPress", "GetListtprintByDeptId", model);
            return PartialView("_GetDispatchList", model);
        }



        public ActionResult GetDataID(int ID)
        {
            PrintingPressModel model = new PrintingPressModel();
            tPrintingTemp NewModel = new tPrintingTemp();
            NewModel.PrintSeqenceId = ID;
            model.PrintSeqenceId = ID;

            model.mDepartment = (List<mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartmentwithselect", model);
            //mDepartment noticeModel = new mDepartment();
            //noticeModel.deptId = "0";
            //noticeModel.deptname = "Select Department";            
            //model.mDepartment.Add(noticeModel);
            model.mPaperType = (List<mPaperCategoryType>)Helper.ExecuteService("PrintingPress", "GetPaperType", model);
            mPaperCategoryType testmodel = new mPaperCategoryType();
            testmodel.PaperCategoryTypeId = 0;
            testmodel.Name = "Select PaperType";
            model.mPaperType.Add(testmodel);
            model.tStatusPrintingPress = (List<tStatusPrintingPress>)Helper.ExecuteService("PrintingPress", "GetStatus", model);
            tStatusPrintingPress Status = new tStatusPrintingPress();
            Status.StatusId = 0;
            Status.StatusName = "Select Status";
            model.tStatusPrintingPress.Add(Status);
            NewModel = (tPrintingTemp)Helper.ExecuteService("PrintingPress", "GetDetails", NewModel);

            foreach (var item in NewModel.PrintingPressModel)
            {
                model.PrintSeqenceId = item.PrintSeqenceId;
                model.DepartmentId = item.DepartmentId;
                model.PaperCategoryTypeId = Convert.ToInt32(item.PaperTypeID);
                model.Title = item.Title;
                model.DepartmentId = item.DepartmentId;
                model.PaperTypeID = item.PaperTypeID;
                model.FileName = item.FileName;
                model.DocFileName = item.DocFileName;
                model.Acknowledge = item.Acknowledge;
                model.RecievedDate = item.RecievedDate;
                model.SubmitDate = item.SubmitDate;
                model.StatusId = item.StatusId;
                model.RefNo = item.RefNo;
                model.Remark = item.Remark;
                model.Dispatch = item.Dispatch;
               
            }
            var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetprintingFileSetting", null);
            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

            //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
            //string FileStructurePath = Acess.SettingValue;
            model.FileStructurePath = Acess.SettingValue + NewsSettings.SettingValue + "/";

            //string Month = "";
            //string Day = "";
            //string Year = "";
            //mBills model = new mBills();

            //int Id = Convert.ToInt32(ID);
            //model.PrintSeqenceId = ID;
            //model = (tPrintingTemp)Helper.ExecuteService("BillWorks", "GetBillsDetails", model);
            //foreach (var item in model.BillWorkModel)
            //{
            //    model.ID = item.ID;
            //    model.BillTitle = item.BillTitle;
            //    if (item.BillNo != "")
            //    {
            //        model.BillNo = item.BillNo;
            //        string[] parts = model.BillNo.Split('o', 'f');
            //        string fist = parts[0].ToString();
            //        string Second = parts[2].ToString();
            //        model.BillYearS = Convert.ToInt32(Second);
            //        model.BillNoS = Convert.ToInt32(fist);
            //    }
            //    //model.IntroductionDate = item.IntroductionDate;
            //    model.PassingDate = item.PassingDate;
            //    model.AssesntDate = item.AssesntDate;

            //    if (item.ActNo != null)
            //    {
            //        model.ActNo = item.ActNo;
            //        string[] Acts = model.ActNo.Split('o', 'f');
            //        string fistAct = Acts[0].ToString();
            //        string SecondYear = Acts[2].ToString();
            //        model.ActYearS = Convert.ToInt32(SecondYear);
            //        model.ActNoS = Convert.ToInt32(fistAct);
            //    }
            //    model.IntroductionFilePath = item.IntroductionFilePath;
            //    model.PassingFilePath = item.PassingFilePath;
            //    model.AccentedFilePath = item.AccentedFilePath;
            //    model.ActFilePath = item.ActFilePath;
            //    model.AssemblyId = item.AssemblyId;
            //    model.SessionId = item.SessionId;


            //    if (item.IntroductionDate != null)
            //    {
            //        string SDate = Convert.ToString(item.IntroductionDate.Value);

            //        SDate = Convert.ToString(item.IntroductionDate.Value);
            //        if (SDate != "")
            //        {
            //            if (SDate.IndexOf("/") > 0)
            //            {
            //                Month = SDate.Substring(0, SDate.IndexOf("/"));
            //                if (Month.Length < 2)
            //                {
            //                    Month = "0" + Month;
            //                }
            //                Day = SDate.Substring(SDate.IndexOf("/") + 1, (SDate.LastIndexOf("/") - SDate.IndexOf("/") - 1));
            //                Year = SDate.Substring(SDate.LastIndexOf("/") + 1, 4);
            //                SDate = Day + "/" + Month + "/" + Year;
            //                model.IDate = SDate;
            //            }
            //            if (SDate.IndexOf("-") > 0)
            //            {
            //                Month = SDate.Substring(0, SDate.IndexOf("-"));
            //                Day = SDate.Substring(SDate.IndexOf("-") + 1, (SDate.LastIndexOf("-") - SDate.IndexOf("-") - 1));
            //                Year = SDate.Substring(SDate.LastIndexOf("-") + 1, 4);
            //                SDate = Day + "/" + Month + "/" + Year;
            //                model.IDate = SDate;
            //            }
            //        }
            //    }
            //    if (item.PassingDate != null)
            //    {
            //        string PDate = Convert.ToString(item.PassingDate.Value);

            //        PDate = Convert.ToString(item.PassingDate.Value);
            //        if (PDate != "")
            //        {
            //            if (PDate.IndexOf("/") > 0)
            //            {
            //                Month = PDate.Substring(0, PDate.IndexOf("/"));
            //                if (Month.Length < 2)
            //                {
            //                    Month = "0" + Month;
            //                }
            //                Day = PDate.Substring(PDate.IndexOf("/") + 1, (PDate.LastIndexOf("/") - PDate.IndexOf("/") - 1));
            //                Year = PDate.Substring(PDate.LastIndexOf("/") + 1, 4);
            //                PDate = Day + "/" + Month + "/" + Year;
            //                model.PDate = PDate;
            //            }
            //            if (PDate.IndexOf("-") > 0)
            //            {
            //                Month = PDate.Substring(0, PDate.IndexOf("-"));
            //                Day = PDate.Substring(PDate.IndexOf("-") + 1, (PDate.LastIndexOf("-") - PDate.IndexOf("-") - 1));
            //                Year = PDate.Substring(PDate.LastIndexOf("-") + 1, 4);
            //                PDate = Day + "/" + Month + "/" + Year;
            //                model.PDate = PDate;
            //            }
            //        }
            //    }
            //    if (item.AssesntDate != null)
            //    {
            //        string ADate = Convert.ToString(item.AssesntDate.Value);

            //        ADate = Convert.ToString(item.AssesntDate.Value);
            //        if (ADate != "")
            //        {
            //            if (ADate.IndexOf("/") > 0)
            //            {
            //                Month = ADate.Substring(0, ADate.IndexOf("/"));
            //                if (Month.Length < 2)
            //                {
            //                    Month = "0" + Month;
            //                }
            //                Day = ADate.Substring(ADate.IndexOf("/") + 1, (ADate.LastIndexOf("/") - ADate.IndexOf("/") - 1));
            //                Year = ADate.Substring(ADate.LastIndexOf("/") + 1, 4);
            //                ADate = Day + "/" + Month + "/" + Year;
            //                model.ADate = ADate;
            //            }
            //            if (ADate.IndexOf("-") > 0)
            //            {
            //                Month = ADate.Substring(0, ADate.IndexOf("-"));
            //                Day = ADate.Substring(ADate.IndexOf("-") + 1, (ADate.LastIndexOf("-") - ADate.IndexOf("-") - 1));
            //                Year = ADate.Substring(ADate.LastIndexOf("-") + 1, 4);
            //                ADate = Day + "/" + Month + "/" + Year;
            //                model.ADate = ADate;
            //            }
            //        }
            //    }
            //}
            //model.AssemblyList = Helper.ExecuteService("BillWorks", "GetAssemblySessionList", null) as List<BillWorkModel>;
            //model.SessionLt = Helper.ExecuteService("BillWorks", "GetSessionsByAssemblyID", model) as List<BillWorkModel>;

            //var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetBillsFileSetting", null);
            ////var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            //var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

            ////string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
            //string FileStructurePath = Acess.SettingValue;
            //model.FileStructurePath = FileStructurePath + NewsSettings.SettingValue;
            //List<SelectListItem> years = new List<SelectListItem>();
            //int currentYear = DateTime.Now.Year;

            //for (int i = currentYear - 70; i < currentYear; i++)
            //{
            //    SelectListItem year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };

            //    years.Add(year);
            //}

            //for (int i = currentYear; i < currentYear + 5; i++)
            //{
            //    SelectListItem year = new SelectListItem();
            //    if (i == DateTime.Now.Year)
            //    {
            //        year = new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = true };
            //    }
            //    else
            //    {
            //        year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };
            //    }
            //    years.Add(year);
            //}
            //for (int i = currentYear ; i < currentYear + 1; i++)
            //{
            //    SelectListItem year = new SelectListItem { Text = i.ToString("Select"), Value = i.ToString("0") };

            //    years.Add(year);
            //}
            //model.yearList = years;
            //model.BillNumberYear = model.BillYearS;
            //model.ActYearS = model.ActYearS;
            model.AdhaarID = CurrentSession.AadharId;
            return PartialView("FormPrintingPress", model);
        }

        [HttpPost]
        public ContentResult UploadFiles()
        {

            string url = "~/PrintingPress/Pdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPrintingTemp>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                string s = ConvertBytesToMegabytes(hpf.ContentLength).ToString("0.00");
                
                // Convert bytes to megabytes.
                Console.WriteLine("{0} bytes = {1} megabytes",
                    hpf.ContentLength,
                    s);
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/PrintingPress/Pdf";
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/PrintingPress/Pdf"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPrintingTemp()
                {
                    Name = hpf.FileName,
                   // Length = hpf.ContentLength,
                    MB=s,
                    Type = hpf.ContentType,
                      
                });
                //int val = 176763 ;
                //
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} MB", r[0].MB) + "\"}", "application/json");
        }

        static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        static void Main()
        {
            // Get the file information.
            FileInfo info = new FileInfo("Anthem.html");

            // Now convert to a string in megabytes.
           
        }
        //public static string ToFileSize(this long source)
        //{
        //    const int byteConversion = 1024;
        //    double bytes = Convert.ToDouble(source);

        //    if (bytes >= Math.Pow(byteConversion, 3)) //GB Range
        //    {
        //        return string.Concat(Math.Round(bytes / Math.Pow(byteConversion, 3), 2), " GB");
        //    }
        //    else if (bytes >= Math.Pow(byteConversion, 2)) //MB Range
        //    {
        //        return string.Concat(Math.Round(bytes / Math.Pow(byteConversion, 2), 2), " MB");
        //    }
        //    else if (bytes >= byteConversion) //KB Range
        //    {
        //        return string.Concat(Math.Round(bytes / byteConversion, 2), " KB");
        //    }
        //    else //Bytes
        //    {
        //        return string.Concat(bytes, " Bytes");
        //    }
        //}
        [HttpPost]
        public ContentResult DocUploadFiles()
        {

            string url = "~/PrintingPress/Doc";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<tPrintingTemp>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                string s = ConvertBytesToMegabytes(hpf.ContentLength).ToString("0.00");

                // Convert bytes to megabytes.
                Console.WriteLine("{0} bytes = {1} megabytes",
                    hpf.ContentLength,
                    s);
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/PrintingPress/Doc";
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/PrintingPress/Doc"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new tPrintingTemp()
                {
                    Name = hpf.FileName,
                   // Length = hpf.ContentLength,
                   MB=s,
                    Type = hpf.ContentType
                });
            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} MB", r[0].MB) + "\"}", "application/json");
        }

        public JsonResult RemovePDFFiles()
        {
            string url = "~/PrintingPress/Pdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveDocFiles()
        {
            string url = "~/PrintingPress/Doc";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdatePrintEntry(Updatevalue model)
        {

            tPrintingTemp Updatemodel = new tPrintingTemp();
            Updatemodel.PrintSeqenceId = model.ID;
            int Assembly = model.AssemblyId;
            int SessionId = model.SessionId;
            Updatemodel.FileName = model.FileName;
            Updatemodel.DocFileName = model.DocFileName;
            Updatemodel.DepartmentId = model.DepartmentId;
            Updatemodel.DeparmentName = model.DeptName;
            Updatemodel.PaperCategoryTypeId = model.PaperCategoryTypeId;
            Updatemodel.Title = model.Title;
            Updatemodel.AssemblyId = model.AssemblyId;
            Updatemodel.SessionId = model.SessionId;
            Updatemodel.RefNo = model.RefNo;
            Updatemodel.Remark = model.Remark;
            Updatemodel.StatusID = model.Status;
            Updatemodel.SubmittedDate = DateTime.Now;
            Updatemodel.SubmittedBy = CurrentSession.AadharId; 
            if (model.ID == 0)
            {
                Updatemodel = Helper.ExecuteService("PrintingPress", "NewInsertEntry", Updatemodel) as tPrintingTemp;
                int NewPrintId = Updatemodel.PrintSeqenceId;
            }
            if (Updatemodel.FileName != null)
            {
                var NewsSettingsinitiaL = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetprintingFileSetting", null);
                Updatemodel.FilePath = NewsSettingsinitiaL.SettingValue;
            }
            if (Updatemodel.DocFileName != null)
            {
                var NewsSettingsinitiaL = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetprintingFileSetting", null);
                Updatemodel.DocFilePath = NewsSettingsinitiaL.SettingValue;
            }

            string url = "~/PrintingPress/Pdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath("~/PrintingPress/Pdf"));
                string SourceFile = savedFileName[0];
                foreach (string page in savedFileName)
                {
                    string name = Path.GetFileName(page);
                    string nameKey = Path.GetFileNameWithoutExtension(page);
                    string directory1 = Path.GetDirectoryName(page);
                    //
                    // Display the Path strings we extracted.
                    //
                    Console.WriteLine("{0}, {1}, {2}, {3}",
                    page, name, nameKey, directory1);
                    Updatemodel.FileName = name;
                }
                string ext = Path.GetExtension(Updatemodel.FileName);
                string fileName = Updatemodel.FileName.Replace(ext, "");
                string ChangeFileName = Assembly + "_" + SessionId + "_" + Updatemodel.PrintSeqenceId + ext;
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetprintingFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, ChangeFileName);
                Updatemodel.FilePath = NewsSettings.SettingValue;
                System.IO.File.Copy(SourceFile, path, true);
                Updatemodel.FileName = ChangeFileName;
            }
            string urlPassed = "~/PrintingPress/Doc";
            string directoryPassed = Server.MapPath(urlPassed);
            if (Directory.Exists(directoryPassed))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath("~/PrintingPress/Doc"));
                string SourceFile = savedFileName[0];
                foreach (string page in savedFileName)
                {
                    string name = Path.GetFileName(page);
                    string nameKey = Path.GetFileNameWithoutExtension(page);
                    string directory1 = Path.GetDirectoryName(page);
                    //
                    // Display the Path strings we extracted.
                    //
                    Console.WriteLine("{0}, {1}, {2}, {3}",
                    page, name, nameKey, directory1);
                    Updatemodel.DocFileName = name;
                }
                string ext = Path.GetExtension(Updatemodel.DocFileName);
                string fileName = Updatemodel.DocFileName.Replace(ext, "");
                string ChangeDocFileName = Assembly + "_" + SessionId + "_" + Updatemodel.PrintSeqenceId + ext;
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetprintingFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, ChangeDocFileName);
                Updatemodel.DocFilePath = NewsSettings.SettingValue;
                System.IO.File.Copy(SourceFile, path, true);
                Updatemodel.DocFileName = ChangeDocFileName;
            }
            //Updatemodel.SubmittedBy = CurrentSession.AadharId; 
            //Updatemodel.SubmittedDate = DateTime.Now;
            if (model.ID == 0)
            {
                Updatemodel = Helper.ExecuteService("PrintingPress", "NewEntry", Updatemodel) as tPrintingTemp;
            }
            else
            {
                Updatemodel = Helper.ExecuteService("PrintingPress", "UpdatetPrintTemp", Updatemodel) as tPrintingTemp;
            }


            string url5 = "~/PrintingPress/Pdf";
            string directory5 = Server.MapPath(url5);
            if (Directory.Exists(directory5))
            {
                System.IO.Directory.Delete(directory5, true);
            }
            string url6 = "~/PrintingPress/Doc";
            string directory6 = Server.MapPath(url6);
            if (Directory.Exists(directory6))
            {
                System.IO.Directory.Delete(directory6, true);
            }
            // return RedirectToAction("Index");
            PrintingPressModel Newmodel = new PrintingPressModel();
            Newmodel.AssemblyID = Assembly;
            Newmodel.SessionID = SessionId;
            if (CurrentSession.AadharId == "")
            {
                Newmodel = (PrintingPressModel)Helper.ExecuteService("PrintingPress", "GetCountFordept", Newmodel);
            }
            else
            {
                Newmodel = (PrintingPressModel)Helper.ExecuteService("PrintingPress", "GetCountForPrinting", Newmodel);
            }
            return Json(Newmodel, JsonRequestBehavior.AllowGet);
        }

        //tMemberNotice noticeModel = new tMemberNotice();
        //   mMinistry model = new mMinistry();
        //   model.MinistryID = MinistryId;
        //   noticeModel.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model) as List<tMemberNotice>;
        //   //    List<tMemberNotice> DepartmentList = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model) as List<tMemberNotice>;
        //   noticeModel.DepartmentId = Convert.ToString(0);
        //   noticeModel.DepartmentName = "Select Department";
        //   // DepartmentLt.Add(noticeModel);
        //   return Json(noticeModel.DepartmentLt, JsonRequestBehavior.AllowGet);
        public ActionResult GetDispatchedList(int AssemblyId, int SessionId)
        {

            tPrintingPress model = new tPrintingPress();
            model.AssemblyId = AssemblyId;
            model.SessionId = SessionId;
            model = (tPrintingPress)Helper.ExecuteService("PrintingPress", "GetDispatchList", model);
            return PartialView("_GetDispatchList", model);
        }

        public ActionResult GetDispatchListDept(int AssemblyId, int SessionId)
        {

            tPrintingPress model = new tPrintingPress();
            model.AssemblyId = AssemblyId;
            model.SessionId = SessionId;
            model = (tPrintingPress)Helper.ExecuteService("PrintingPress", "GetListtDispatchDept", model);
            return PartialView("_GetRecieptList", model);
        }
        public ActionResult GetDispachDataID(int ID)
        {

            PrintingPressModel model = new PrintingPressModel();
            tPrintingTemp NewModel = new tPrintingTemp();
            NewModel.PrintSeqenceId = ID;
            model.PrintSeqenceId = ID;
            model.mDepartment = (List<mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", model);
            mDepartment noticeModel = new mDepartment();
            noticeModel.deptId = "0";
            noticeModel.deptname = "Select Department";
            model.mDepartment.Add(noticeModel);
            //model.mDepartment = ListtMemberNotice;
            model.mPaperType = (List<mPaperCategoryType>)Helper.ExecuteService("PrintingPress", "GetPaperType", model);
            mPaperCategoryType testmodel = new mPaperCategoryType();
            testmodel.PaperCategoryTypeId = 0;
            testmodel.Name = "Select PaperType";
            model.mPaperType.Add(testmodel);
            model.tStatusPrintingPress = (List<tStatusPrintingPress>)Helper.ExecuteService("PrintingPress", "GetStatus", model);
            tStatusPrintingPress Status = new tStatusPrintingPress();
            Status.StatusId = 0;
            Status.StatusName = "Select Status";
            model.tStatusPrintingPress.Add(Status);

            NewModel = (tPrintingTemp)Helper.ExecuteService("PrintingPress", "GetFiles", NewModel);

            foreach (var item in NewModel.PrintingPressModel)
            {
                model.PrintSeqenceId = item.PrintSeqenceId;
                model.DepartmentId = item.DepartmentId;
                model.PaperCategoryTypeId = Convert.ToInt32(item.PaperTypeID);
                model.Title = item.Title;
                model.DepartmentId = item.DepartmentId;
                model.PaperTypeID = item.PaperTypeID;
                model.FileName = item.FileName;
                model.DocFileName = item.DocFileName;
                model.Acknowledge = item.Acknowledge;
                model.RecievedDate = item.RecievedDate;
                model.SubmitDate = item.SubmitDate;
                model.StatusId = item.StatusId;
                model.RefNo = item.RefNo;
                model.Remark = item.Remark;
                model.AdhaarID = item.AdhaarID;
            }
            var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetprintingFileSetting", null);
            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

            //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
            //string FileStructurePath = Acess.SettingValue;
            model.FileStructurePath = Acess.SettingValue + NewsSettings.SettingValue + "/";

            //////////////////////////////
            // tPrintingTemp modelGet = new tPrintingTemp();
            // modelGet.PrintSeqenceId = ID;
            //// modelGet.= model.ID;
            // modelGet = (tPrintingTemp)Helper.ExecuteService("PrintingPress", "GetFiles", modelGet);
            // foreach (var item in modelGet.PrintingPressModel)
            // {

            //     modelGet.FileName = item.FileName;
            //     modelGet.DocFileName = item.DocFileName;            

            // }


            //var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetprintingFileSetting", null);
            //var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

            //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
            //string FileStructurePath = Acess.SettingValue;
            //modelGet.FileStructurePath = Acess.SettingValue + NewsSettings.SettingValue + "/";

            return PartialView("FormDispachedPdf", model);
        }

        public ActionResult GetAdhaarDetails(string AdhaarID)
        {
            PrintingPressModel model = new PrintingPressModel();
            //int GroupId = Convert.ToInt32(Id);
            model.AdhaarID = AdhaarID;
            model = (PrintingPressModel)Helper.ExecuteService("PrintingPress", "GetAadharDetails", model);

            foreach (var item in model.objList)
            {

                model.AdhaarID = item.AdhaarID;
                model.AdhaarName = item.AdhaarName;
                model.FatherName = item.FatherName;
                model.Gender = item.Gender;
                model.Address = item.Address;
                model.DOB = item.DOB;
                model.Email = item.Email;
                model.District = item.District;
                model.PinCode = item.PinCode;
                model.Photo = item.Photo;
                model.MobileNo = item.MobileNo;

            }
            return PartialView("PopUpAadharDetails", model);
        }


        public JsonResult UpdatetAcknowledge(int ID, int AssemblyId, int SessionId)
        {

            tPrintingPress Updatemodel = new tPrintingPress();
            Updatemodel.PrintSeqenceId = ID;
            Updatemodel.Acknowledge = DateTime.Now;
            Updatemodel = Helper.ExecuteService("PrintingPress", "UpdatetAcknowledge", Updatemodel) as tPrintingPress;
            PrintingPressModel Newmodel = new PrintingPressModel();
            Newmodel.AssemblyID = AssemblyId;
            Newmodel.SessionID = SessionId;
            Newmodel = (PrintingPressModel)Helper.ExecuteService("PrintingPress", "GetCountForPrinting", Newmodel);
            return Json(Newmodel, JsonRequestBehavior.AllowGet);

        }

        public JsonResult uniqeRefNo()
        {

            PrintingPressModel obj = new PrintingPressModel();
            int currentYear = DateTime.Now.Year;
            var RandomVal = SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Extensions.ExtensionMethods.RandomValue();
            //int val = int.Parse(RandomVal);

            string val = currentYear + "_" + RandomVal;

            obj.RefNo= val;       
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult GetAdhaarDetails(string AdhaarID, string fileLoaction)
        //{
        //    PrintingPressModel model = new PrintingPressModel();
        //    AdhaarDetails details = new AdhaarDetails();
        //    AdhaarServices.ServiceSoapClient obj = new AdhaarServices.ServiceSoapClient();
        //    EncryptionDecryption.EncryptionDecryption objencr = new EncryptionDecryption.EncryptionDecryption();
        //    string inputValue = objencr.Encryption(AdhaarID.Trim());
        //    try
        //    {
        //        DataTable dt = obj.getHPKYCInDataTable(inputValue);
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            details.AdhaarID = AdhaarID;
        //            details.Name = objencr.Decryption(Convert.ToString(dt.Rows[0]["Name"]));
        //            details.FatherName = objencr.Decryption(Convert.ToString(dt.Rows[0]["FatherName"]));
        //            details.Gender = objencr.Decryption(Convert.ToString(dt.Rows[0]["Gender"]));
        //            details.Address = objencr.Decryption(Convert.ToString(dt.Rows[0]["Address"]).ToString());
        //            details.DOB = objencr.Decryption(Convert.ToString(dt.Rows[0]["DOB"]));
        //            details.MobileNo = objencr.Decryption(Convert.ToString(dt.Rows[0]["MobileNumber"]));
        //            details.Email = objencr.Decryption(Convert.ToString(dt.Rows[0]["EmailID"]));
        //            details.District = objencr.Decryption(Convert.ToString(dt.Rows[0]["DistrictName"]).ToString());
        //            details.PinCode = objencr.Decryption(Convert.ToString(dt.Rows[0]["PinCOde"]).ToString());


        //            //Calculate the age.
        //            DateTime dateOfBirth;

        //            if (!string.IsNullOrEmpty(details.DOB) && DateTime.TryParse(details.DOB, out dateOfBirth))
        //            {
        //                dateOfBirth = Convert.ToDateTime(details.DOB);
        //                DateTime today = DateTime.Today;
        //                int age = today.Year - dateOfBirth.Year;
        //                if (dateOfBirth > today.AddYears(-age))
        //                    age--;

        //                details.DOB = Convert.ToString(age);
        //            }
        //            else
        //            {
        //                details.DOB = "";
        //            }
        //            Byte[] bytes = (Byte[])Convert.FromBase64String(objencr.Decryption(dt.Rows[0]["photo"].ToString()));
        //            if (fileLoaction != null && fileLoaction != "" && (fileLoaction.IndexOf("/") != -1))
        //            {
        //                int indexof = fileLoaction.LastIndexOf("/");
        //                string url = fileLoaction.Substring(0, indexof + 1);
        //                string FileName = fileLoaction.Substring(indexof + 1);
        //                var path = Server.MapPath(url) + FileName;
        //                System.IO.File.WriteAllBytes(path, bytes);

        //                details.Photo = fileLoaction;
        //            }
        //            else
        //            {
        //                Guid PicName;
        //                PicName = Guid.NewGuid();
        //                SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();
        //                SiteSettings siteSettingMod = new SiteSettings();
        //                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
        //                session.AssemblyID = Convert.ToInt16(siteSettingMod.AssemblyCode);
        //                session.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);

        //                string Url = "/Images/Pass/Photo/" + session.AssemblyID + "/" + session.SessionCode + "/";
        //                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
        //                if (!Dir.Exists)
        //                {
        //                    Dir.Create();
        //                }
        //                var path = Server.MapPath(Url) + PicName + ".jpg";
        //                System.IO.File.WriteAllBytes(path, bytes);
        //                details.Photo = Url + PicName + ".jpg";
        //            }
        //            model.AdhaarID = details.AdhaarID;
        //            model.AdhaarName = details.Name;
        //            model.FatherName = details.FatherName;
        //            model.Gender=details.Gender;
        //            model.Address=details.Address;
        //            model.DOB=details.DOB;
        //            model.Email=details.Email;
        //            model.District=details.District;
        //            model.PinCode=details.PinCode;
        //            model.Photo = details.Photo;
        //            model.MobileNo = details.MobileNo;
        //            //model
        //            //return Json(details, JsonRequestBehavior.AllowGet);
        //            return PartialView("PopUpAadharDetails", model);
        //        }
        //        else
        //        {
        //           // return Json(false, JsonRequestBehavior.AllowGet);
        //            return PartialView("PopUpAadharDetails", model);
        //        }


        //    }
        //    catch
        //    {
        //       // return Json(details, JsonRequestBehavior.AllowGet);
        //        return PartialView("PopUpAadharDetails", model);
        //    }
        //}
        public JsonResult GetDatabyRefNo(string RefNo)
        {

            tPrintingPress obj = new tPrintingPress();
            obj.RefNo = RefNo;
            obj = (tPrintingPress)Helper.ExecuteService("PrintingPress", "GetdatabyRefNo", obj);
            //foreach (var item in obj.PrintingPressModel)
            //{
            //    obj.DepartmentId = item.DepartmentId;
            //    obj.PaperTypeID = item.PaperTypeID;
            //    obj.Title = item.Title;
            //    obj.Remarks = item.Remark;
            //}
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadPdf(string Wid)
        {
            string[] Val = Wid.Split(',', ',');
            string fistId = Val[0].ToString();
            string SecondType = Val[1].ToString();
            int Id = Convert.ToInt32(fistId);
            string path = string.Empty;
            string FileName = string.Empty;
            try
            {
                var NewModel = (tPrintingTemp)Helper.ExecuteService("PrintingPress", "GetDetails", new tPrintingTemp { PrintSeqenceId = Id });
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetprintingFileSetting", null);
                var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                var Filelocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string FileStructurePath = Acess.SettingValue;
                if (SecondType == "P")
                {
                    string url = FileStructurePath + NewsSettings.SettingValue + NewModel.PrintingPressModel.FirstOrDefault().FileName;
                    path = Filelocation.SettingValue + NewsSettings.SettingValue +"\\" + NewModel.PrintingPressModel.FirstOrDefault().FileName;
                }
                else if (SecondType == "D")
                {
                    string url = FileStructurePath + NewsSettings.SettingValue + NewModel.PrintingPressModel.FirstOrDefault().DocFileName;
                    path = Filelocation.SettingValue + NewsSettings.SettingValue + "\\" + NewModel.PrintingPressModel.FirstOrDefault().DocFileName;
                   // string filename = "BillChallan.docx";
                    Response.ContentType = "application/octet-stream";
                    Response.AppendHeader("Content-Disposition", "attachment;filename=" + NewModel.PrintingPressModel.FirstOrDefault().DocFileName);
                    Response.TransmitFile(path);
                    Response.End();
                    return new EmptyResult();
                }
              

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
          





            byte[] bytes = System.IO.File.ReadAllBytes(path);

            return File(bytes, "application/pdf");
           
        }
    }

    public class Updatevalue
    {
        public int ID { get; set; }
        public int AssemblyId { get; set; }
        public int SessionId { get; set; }
        public string FileName { get; set; }
        public string DocFileName { get; set; }
        public string DepartmentId { get; set; }
        public int PaperCategoryTypeId { get; set; }
        [AllowHtml]
        public string Title { get; set; }
        [AllowHtml]
        public string Remark { get; set; }
        public string RefNo { get; set; }        
        public string DeptName { get; set; }
        public int Status { get; set; }
        
    }
}
