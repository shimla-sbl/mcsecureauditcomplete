﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Committee;


namespace SBL.eLegistrator.HouseController.Web.Areas.eFile.Models
{
    [Serializable]
    public class eFileViewModel
    {
        public int eFileID { get; set; }
        public int eFileAttachmentId { get; set; }
        public string eFileNumber { get; set; }
        public string eFileSubject { get; set; }
        public string DepartmentId { get; set; }
        public bool Status { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string OldDescription { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string NewDescription { get; set; }
        public int CommitteeId { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string DepartmentName { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(4, ErrorMessage = "Max length 4 characters")]
        public string FromYear { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(4, ErrorMessage = "Max length 4 characters")]
        public string ToYear { get; set; }
        public string Type { get; set; }
        public string OfficeName { get; set; }
        public System.Web.Mvc.SelectList DepartmentList { get; set; }
        public System.Web.Mvc.SelectList OfficeList { get; set; }
        public List<SBL.DomainModel.Models.Document.mDocumentType> DocumentType { get; set; }
        public List<SBL.DomainModel.Models.eFile.eFileAttachment> eFileAttachment { get; set; }
        public SBL.DomainModel.Models.eFile.eFile eFileDetail { get; set; }
        public int Officecode { get; set; }
        public string Mode { get; set; }
        public List<SBL.DomainModel.Models.eFile.eFileNoting> eFileNotingList { get; set; }
		public int eFileTypeID { get; set; }
		public List<SBL.DomainModel.Models.eFile.eFileType> eFileTypeList { get; set; }
        [NotMapped]
        public virtual ICollection<HouseComModel> HouseComModel { get; set; }
        public string SelectedDeptId { get; set; }
        public int? SelectedBranchId { get; set; }
        [NotMapped]
        public string SelectedBranchName { get; set; }
       
    }

    public static class ModelMapping
    {
        public static SBL.DomainModel.Models.eFile.eFile ToDomainModel(this eFileViewModel model)
        {
            var DomainModel = new SBL.DomainModel.Models.eFile.eFile
            {
                eFileNumber = model.eFileNumber,
                eFileSubject = model.eFileSubject,
                DepartmentId = model.DepartmentId,
                Status = model.Status,
                OldDescription = model.OldDescription,
                NewDescription = model.NewDescription,
                CommitteeId = model.CommitteeId,
                CreatedBy = model.CreatedBy,
                CreatedDate = model.CreatedDate,
                ModifiedBy = model.ModifiedBy,
                ModifiedDate = model.ModifiedDate,
                FromYear = model.FromYear,
                ToYear = model.ToYear,
                Officecode = model.Officecode,
				eFileTypeID=model.eFileTypeID
            };
            return DomainModel;
        }

        public static eFileViewModel ToViewModel(this SBL.DomainModel.Models.eFile.eFileList model)
        {
            var ViewModel = new eFileViewModel
            {
                eFileID = model.eFileID,
                eFileNumber = model.eFileNumber,
                eFileSubject = model.eFileSubject,
                Status = model.Status,
                DepartmentName = model.Department
            };
            return ViewModel;
        }
    }
    
    [Serializable]
    public class MyClass
    {
        [ScriptIgnore]
        public int ID;
        public string Name;
        //[JsonIgnore]
        //[IgnoreDataMember]
        [ScriptIgnore]
        public int ParentID;
        public List<MyClass> children = new List<MyClass>();
        public MyClass(string name, int id, int parent_id)
        {
            Name = name +"("+ id +")";
            ID = id;
            ParentID = parent_id;

        }
    }
}
