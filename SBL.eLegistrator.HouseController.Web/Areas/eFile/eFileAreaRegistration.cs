﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.eFile
{
    public class eFileAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "eFile";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "eFile_default",
                "eFile/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
