﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models;
using System.IO;

namespace SBL.eLegistrator.HouseController.Web.Areas.eFile.Controllers
{
    public class DepartmentController : Controller
    {
        //
        // GET: /eFile/Department/

        public ActionResult Index()
        {

            return View();
        }
        public ActionResult CommitteeStaff()
        {

            return View();
        }
        public ActionResult _DepartmentNotices()
        {
            var result = (List<SBL.DomainModel.Models.eFile.eFileList>)Helper.ExecuteService("eFile", "GetAlleFileAttachmentsDep2", 0);
            return PartialView(result); 
        }
        public ActionResult _ReplyFromDepartment()
        {
            var result = (List<SBL.DomainModel.Models.eFile.eFileList>)Helper.ExecuteService("eFile", "GetAlleFileAttachmentsFromDep", 0);
            return PartialView(result);
        }
        public ActionResult _ReplyToCommittee(SBL.DomainModel.Models.eFile.eFileList model)
        {
            return PartialView(model); 
        }
        public ActionResult _ReplyToCommittee2(SBL.DomainModel.Models.eFile.eFileList model)
        {
            return PartialView(model);
        }
        public ActionResult _ComposeReplyCommittee(SBL.DomainModel.Models.eFile.eFileList model)
        {
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult _Temp()
        {
            return null;
        }
        [HttpPost]
        public ActionResult fileUpload(HttpPostedFileBase file)
        {
            foreach (string file1 in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file1] as HttpPostedFileBase;
            }
            return null;
        }
        [HttpPost]
        public ActionResult SubmitReply(SBL.DomainModel.Models.eFile.eFileList model, HttpPostedFileBase file)
        {
            try
            {
                SBL.DomainModel.Models.eFile.eFileAttachment newefile = new SBL.DomainModel.Models.eFile.eFileAttachment();
                //newefile.ParentId = model.eFileID;
                newefile = (SBL.DomainModel.Models.eFile.eFileAttachment)Helper.ExecuteService("eFile", "GeteFileAttachmentDep", model.eFileAttachmentId);
                newefile.ParentId = model.eFileAttachmentId;
                newefile.Type = "ReplyByDep";
                newefile.SendStatus = false;

                if (file != null)
                {
                    newefile.eFileName = Path.GetFileNameWithoutExtension(file.FileName);
                    if (!Directory.Exists(Server.MapPath("~/eFileAttachments/" + model.eFileID)))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/eFileAttachments/" + model.eFileID));

                    };
                    string destinationPath = Path.Combine(Server.MapPath("~/eFileAttachments/" + model.eFileID), Path.GetFileName(file.FileName));
                    file.SaveAs(destinationPath);
                }

                bool flag = (bool)Helper.ExecuteService("eFile", "AddeFileAttachments", newefile);
                if (flag)
                {
                    Helper.ExecuteService("eFile", "eFileAttachmentStatus", model.eFileAttachmentId);
                }
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return null;
            }
            //return RedirectToAction("_DepartmentNotices");
            return RedirectToAction("Index", "Department", new { area = "eFile" });

        }
        public ActionResult _GetPdf(string FilePath)
        {
            FilePath = "/eFileAttachments/" + FilePath + ".pdf";
            ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);
            return PartialView();
        }


        [HttpPost]
        public ContentResult UploadNoticeDocFiles()
        {

            string url = "~/DepartmentPdf/MainReplyDoc"  ;
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

           

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/DepartmentPdf/MainReplyDoc" ;
                string directory1 = Server.MapPath(url1);
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                string savedFileName = Path.Combine(Server.MapPath("~/DepartmentPdf/MainReplyDoc" ), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

               
            }

            return null;
        }
    }
}



