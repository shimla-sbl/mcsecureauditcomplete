﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.eFile.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.eFile;

namespace SBL.eLegistrator.HouseController.Web.Areas.eFile.Controllers
{
    public class eFilesController : Controller
    {
        //
        // GET: /eFile/eFiles/


        public ActionResult Index()
		{
			tPaperLaidV model = new tPaperLaidV();
			if (!string.IsNullOrEmpty(CurrentSession.DeptID))
			{	
				tPaperLaidV model1 = new tPaperLaidV();
				List<mDepartment> deptInfo = new List<mDepartment>();
				model1.DepartmentId = CurrentSession.DeptID;
				deptInfo = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartmentByIDs", model1);
				foreach (var item in deptInfo)
				{
					if (item != null)
					{
						model.DeparmentNameByids += item.deptname + ", ";
					}
				}
				if (model.DeparmentNameByids != null && model.DeparmentNameByids.LastIndexOf(",") > 0)
				{
					model.DeparmentNameByids = model.DeparmentNameByids.Substring(0, model.DeparmentNameByids.Length - 2);
				}
			}
			ViewBag.DeptName = model.DeparmentNameByids;
			Session["Deptname"] = model.DeparmentNameByids;
            return View();
        }
        public ActionResult eFile(int CommitteeId)
        {
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
            SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new Models.eFileViewModel();
            model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            model.DocumentType = (List<SBL.DomainModel.Models.Document.mDocumentType>)Helper.ExecuteService("eFile", "eFileDocumentIndex", 1);
            model.CommitteeId = CommitteeId;
            return View(model);
        }

        public ActionResult _eFileBookViewByIDPartial()
        {
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
            SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new Models.eFileViewModel();
            model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            model.DocumentType = (List<SBL.DomainModel.Models.Document.mDocumentType>)Helper.ExecuteService("eFile", "eFileDocumentIndex", 1);
            return PartialView(model);
        }
    //Shashi
		public ActionResult eFileCreate(int CommitteeId)
		{
			var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
			SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new Models.eFileViewModel();
			model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
			model.DocumentType = (List<SBL.DomainModel.Models.Document.mDocumentType>)Helper.ExecuteService("eFile", "eFileDocumentIndex", 1);
			model.CommitteeId = CommitteeId;
			model.Mode = "Add";
			return PartialView("_CreateeFile",model);
		}
		public ActionResult EditeFileDetails(int eFileId)
		{

			var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
			SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new Models.eFileViewModel();
			model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
			model.DocumentType = (List<SBL.DomainModel.Models.Document.mDocumentType>)Helper.ExecuteService("eFile", "eFileDocumentIndex", 1);
			//model.CommitteeId = CommitteeId;
			model.eFileDetail = (SBL.DomainModel.Models.eFile.eFile)Helper.ExecuteService("eFile", "GeteFileDetailsByFileId", eFileId);
			model.eFileID = eFileId;
			model.eFileNumber = model.eFileDetail.eFileNumber;
			model.eFileSubject = model.eFileDetail.eFileSubject;
			model.FromYear = model.eFileDetail.FromYear;
			model.ToYear = model.eFileDetail.ToYear;
			model.OldDescription = model.eFileDetail.OldDescription;
			model.NewDescription = model.eFileDetail.NewDescription;
			model.DepartmentId = model.eFileDetail.DepartmentId;
			model.Officecode = model.eFileDetail.Officecode;
			model.Mode = "Edit";
			return PartialView("_CreateeFile", model);
		}

      

    }

}
