﻿using EvoPdf;
using Newtonsoft.Json.Linq;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.eFile;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Areas.eFile.Models;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using SBL.eLegistrator.HouseController.Filters;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Employee;
using Lib.Web.Mvc.JQuery.JqGrid;
using Microsoft.Security.Application;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SMS.API;
using Email.API;
using SBL.DomainModel.Models.PrintingPress;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.CommitteeReport;

namespace SBL.eLegistrator.HouseController.Web.Areas.eFile.Controllers
{
    public class eFileController : Controller
    {
        //
        // GET: /eFile/eFile/


        public ActionResult eFileLogin()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            return null;
        }

        public ActionResult _eFileBookDetailPartial(eFileViewModel model)
        {
            return View(model);
        }

        public ActionResult _eFileBookViewByIDPartial(eFileViewModel model)
        {
            try
            {
                eFileAttachment documents = new eFileAttachment();
                List<int> PDFPageNumbers = new List<int>();
                documents.eFileID = model.eFileID;
                documents.DepartmentId = CurrentSession.DeptID;

                ViewBag.Subject = model.eFileSubject;
                ViewBag.eFileNum = model.eFileNumber;
                //model. = (List<string>)Helper.ExecuteService("eFile", "eFileDocumentIndex", model.eFileID);
                model.eFileNotingList = (List<eFileNoting>)Helper.ExecuteService("eFile", "GeteFileNotingDetailsByeFileID", model.eFileID);

            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return View(model);
            //return View(model);
        }

        public ActionResult _eFileViewByID(eFileViewModel model)
        {
            eFileLogin();
            try
            {
                eFileViewModel model2 = new eFileViewModel();

                //model2.DepartmentName = model.DepartmentName;
                //line for office
                model2.eFileNumber = model.eFileNumber;
                model2.eFileSubject = model.eFileSubject;
                model2.eFileID = model.eFileID;
                model2.eFileDetail = (SBL.DomainModel.Models.eFile.eFile)Helper.ExecuteService("eFile", "GeteFileDetailsByFileId", model.eFileID);
                model2.NewDescription = model2.eFileDetail.NewDescription;
                model2.DepartmentId = model2.eFileDetail.DepartmentId;
                model2.Officecode = model2.eFileDetail.Officecode;
                model2.DepartmentName = model2.eFileDetail.DepartmentName;
                return View(model2);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public ActionResult _eFileViewByID(eFileViewModel model)
        //{
        //    eFileLogin();
        //    try
        //    {
        //        eFileViewModel model2 = new eFileViewModel();

        //        model2.DepartmentName = model.DepartmentName;
        //        //line for office
        //        model2.eFileNumber = model.eFileNumber;
        //        model2.eFileSubject = model.eFileSubject;
        //        model2.eFileID = model.eFileID;
        //        model2.eFileDetail = (SBL.DomainModel.Models.eFile.eFile)Helper.ExecuteService("eFile", "GeteFileDetailsByFileId", model.eFileID);
        //        model2.NewDescription = model2.eFileDetail.NewDescription;
        //        model2.DepartmentId = model2.eFileDetail.DepartmentId;
        //        model2.Officecode = model2.eFileDetail.Officecode;     
        //        model2.DepartmentName = model2.eFileDetail.DepartmentName;
        //        return View(model2);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        /// <summary>
        /// Create a new eFile
        /// </summary>
        /// <returns>eFileViewModel</returns>
        [HttpGet]
        public ActionResult AddeFile(int CommitteeId)
        {
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new Models.eFileViewModel();
            model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            model.OldDescription = "PreText";
            model.NewDescription = "FutureText";
            return View(model);
        }

        [HttpPost]
        public JsonResult AddeFile(SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model)
        {
            eFileLogin();

            SBL.DomainModel.Models.eFile.eFile eFileModel = SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.ModelMapping.ToDomainModel(model);
            eFileModel.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
            eFileModel.CreatedDate = DateTime.Now;
            eFileModel.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
            eFileModel.ModifiedDate = DateTime.Now;
            // eFileModel.Officecode = lstOffice;// model.Officecode;
            eFileModel.eFileSubject = model.eFileSubject.Replace("'", "");
            eFileModel.eFileNumber = model.eFileNumber.Replace("'", "");
            eFileModel.BranchId = Convert.ToInt32(CurrentSession.BranchId);
            eFileModel.DepartmentId = CurrentSession.DeptID.ToString();
            eFileModel.SelectedDeptId = model.SelectedDeptId;
            eFileModel.SelectedBranchId = Convert.ToInt32(CurrentSession.BranchId);
            eFileModel.CommitteeId = (int)Helper.ExecuteService("eFile", "GetComtbyBranch", eFileModel);
            //eFileModel.CommitteeId = 1;
            Helper.ExecuteService("eFile", "AddeFile", eFileModel);
            //return RedirectToAction("eFileListAll", "eFile", routeValues: new { CommitteeId = model.CommitteeId });

            return Json("Save Successfully", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddeFileAttachments(string Id)
        {
            eFileLogin();
            SBL.DomainModel.Models.eFile.eFileAttachment model = new SBL.DomainModel.Models.eFile.eFileAttachment();
            //Apply eFileId
            model.eFileID = Convert.ToInt32(1);
            return View(model);
        }

        [HttpPost]
        public ActionResult AddeFileAttachments(SBL.DomainModel.Models.eFile.eFileAttachment model, HttpPostedFileBase file, int CommId = 0)
        {
            try
            {
                if (file != null)
                {
                    model.eFileName = Path.GetFileNameWithoutExtension(file.FileName);
                    if (!Directory.Exists(Server.MapPath("~/eFileAttachments/" + model.eFileID)))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/eFileAttachments/" + model.eFileID));
                    };
                    string destinationPath = Path.Combine(Server.MapPath("~/eFileAttachments/" + model.eFileID), Path.GetFileName(file.FileName));
                    file.SaveAs(destinationPath);

                    //PDF to Image.
                    List<int> PDFPageNumbers = new List<int>();
                    if (!Directory.Exists(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles"));
                        PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);
                    }
                    else
                    {
                        PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);
                    }

                    model.SplitFileCount = PDFPageNumbers.Count;
                    model.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
                    model.ModifiedDate = DateTime.Now;
                    model.eFileNameId = Guid.NewGuid();
                    //model.Approve = 1;
                    model.ParentId = model.ParentId == 0 ? 0 : model.ParentId;
                    var value = Helper.ExecuteService("eFile", "AddeFileAttachments", model);
                }

                //if (model.Type != "FromeFile")
                //{
                //    // var result = Helper.ExecuteService("eFile", "GetDepartmentMembers", null);
                //    List<SBL.DomainModel.Models.Member.DepartmentMembers> deptmembers = new List<DomainModel.Models.Member.DepartmentMembers>();
                //    deptmembers = (List<SBL.DomainModel.Models.Member.DepartmentMembers>)Helper.ExecuteService("eFile", "GetDepartmentMembers", null);
                //    CustomMailMessage emailMessage = new CustomMailMessage();

                //    emailMessage._from = "evidhanmail@gmail.com";
                //    emailMessage._isBodyHtml = false;
                //    emailMessage._subject = "Notice From Committee";
                //    emailMessage._body = "Notice From Committee.Please find the attachment.";
                //    emailMessage._toList.Add("test@gmail.com");
                //    //foreach (var item in deptmembers)
                //    //{
                //    //    emailMessage._toList.Add(item.Email);
                //    //}
                //    emailMessage.ProccessDate = DateTime.Now;
                //    EAttachment ea = new EAttachment { FileName = new FileInfo(Server.MapPath("~/EmailAttachment/Test.txt")).Name, FileContent = System.IO.File.ReadAllBytes(Server.MapPath("~/EmailAttachment/Test.txt")) };
                //    emailMessage._attachments.Add(ea);

                //    SMSMessageList smsMessage = new SMSMessageList();

                //    smsMessage.SMSText = "Notice to department";
                //    smsMessage.ModuleActionID = 1;
                //    smsMessage.UniqueIdentificationID = 2;
                //    smsMessage.MobileNo.Add("12345678");
                //    //foreach (var item in deptmembers)
                //    //{
                //    //    smsMessage.MobileNo.Add(item.Mobile);
                //    //}
                //    //string SendSMS = "1"; string SendEmail = "1";
                //    Notification.Send(true, true, smsMessage, emailMessage);
                //}

                if (model.Type == "FromDepartment")
                {
                    return RedirectToAction("ShowDepartmentNotices", "Committee", new { area = "SuperAdmin" });
                }
                else if (model.Type == "SendNoticetoDept")
                {
                    //return RedirectToAction("SendNoticeToDepartmentList", "Committee", new { area = "SuperAdmin" });
                    return RedirectToAction("eFileListAll", "eFile", new { area = "eFile", CommitteeId = CommId });
                }
                else if (model.Type == "AttachPaper")
                {
                    return RedirectToAction("eFileListAll", "eFile", new { area = "eFile", CommitteeId = CommId });
                }
                else if (model.Type == "Report")
                {
                    return RedirectToAction("eFileListAll", "eFile", new { area = "eFile", CommitteeId = CommId, eFileID = model.eFileID });
                }

                //else
                //{
                //    return RedirectToAction("eFileListAll", "eFile", new { area = "eFile" });
                //}
            }
            catch (Exception ex)
            {
                string exs = ex.ToString();
            }
            //return View();
            return RedirectToAction("eFileListAll", "eFile", new { area = "eFile", CommitteeId = CommId });
        }

        public ActionResult CheckBookViewById(eFileViewModel model)
        {
            eFileAttachment documents = new eFileAttachment();
            model.DocumentType = (List<SBL.DomainModel.Models.Document.mDocumentType>)Helper.ExecuteService("eFile", "eFileDocumentIndex", 1);
            model.eFileAttachment = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)Helper.ExecuteService("eFile", "GetAlleFileAttachments", model.eFileID);
            return View(model);
        }

        [HttpGet]
        public ActionResult EditeFile(int Id)
        {
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new Models.eFileViewModel();
            SBL.DomainModel.Models.eFile.eFile eFileModel = (SBL.DomainModel.Models.eFile.eFile)Helper.ExecuteService("eFile", "EditeFile", Id);
            model.eFileID = eFileModel.eFileID;
            model.eFileNumber = eFileModel.eFileNumber;
            model.eFileSubject = eFileModel.eFileSubject;
            model.DepartmentId = eFileModel.DepartmentId;
            model.Status = eFileModel.Status;
            model.OldDescription = eFileModel.OldDescription;
            model.NewDescription = eFileModel.NewDescription;
            model.CommitteeId = eFileModel.CommitteeId;
            model.CreatedBy = eFileModel.CreatedBy;
            model.CreatedDate = eFileModel.CreatedDate;
            model.ModifiedBy = eFileModel.ModifiedBy;
            model.ModifiedDate = eFileModel.ModifiedDate;
            model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            return View("AddeFile", model);
        }


        //[HttpGet]
        //public ActionResult EditeFile(int Id)
        //{
        //    var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
        //    SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new Models.eFileViewModel();
        //    SBL.DomainModel.Models.eFile.eFile eFileModel = (SBL.DomainModel.Models.eFile.eFile)Helper.ExecuteService("eFile", "EditeFile", Id);
        //    model.eFileID = eFileModel.eFileID;
        //    model.eFileNumber = eFileModel.eFileNumber;
        //    model.eFileSubject = eFileModel.eFileSubject;
        //    model.DepartmentId = eFileModel.DepartmentId;
        //    model.Status = eFileModel.Status;
        //    model.OldDescription = eFileModel.OldDescription;
        //    model.NewDescription = eFileModel.NewDescription;
        //    model.CommitteeId = eFileModel.CommitteeId;
        //    model.CreatedBy = eFileModel.CreatedBy;
        //    model.CreatedDate = eFileModel.CreatedDate;
        //    model.ModifiedBy = eFileModel.ModifiedBy;
        //    model.ModifiedDate = eFileModel.ModifiedDate;
        //    model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
        //    return View("AddeFile", model);
        //}

        public ActionResult eFileActive(int eFileId)
        {
            int CommitteeId = (int)Helper.ExecuteService("eFile", "eFileActive", eFileId);

            return Json("eFile Opened Successfully", JsonRequestBehavior.AllowGet);
            //return RedirectToAction("eFileListAll", new { CommitteeId = (int)Helper.ExecuteService("eFile", "eFileActive", eFileId) });
        }

        [HttpGet]
        public ActionResult eFileClose(string CommitteId)
        {
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);
            return PartialView("_eFileListView", eList.eFileLists);
        }

        public JsonResult eFileDeactive(int eFileId)
        {

            int CommitteeId = (int)Helper.ExecuteService("eFile", "eFileDeActive", eFileId);

            return Json("eFile Closed Successfully", JsonRequestBehavior.AllowGet);
            //return RedirectToAction("eFileListAll", new { CommitteeId = (int)Helper.ExecuteService("eFile", "eFileDeActive", eFileId) });
        }

        public ActionResult eFileList()
        {
            eFileLogin();
            List<SBL.DomainModel.Models.eFile.eFileList> eList = new List<DomainModel.Models.eFile.eFileList>();
            eList = (List<SBL.DomainModel.Models.eFile.eFileList>)Helper.ExecuteService("eFile", "GetAlleFile", null);
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            var x = deptList.ToList().Select(c => new SelectListItem { Text = c.deptname, Value = c.deptId }).ToList();
            ViewBag.Departments = x;
            //model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            return View(eList);
        }

        /// <summary>
        /// List all efiles for pericular committees
        /// </summary>
        /// <param name="CommitteeId"></param>
        /// <returns></returns>
        public ActionResult eFileListAll()  //int CommitteeId, int? eFileID
        {
            eFileLogin();
            //List<SBL.DomainModel.Models.eFile.eFileList> eList = new List<DomainModel.Models.eFile.eFileList>();
            //eList = (List<SBL.DomainModel.Models.eFile.eFileList>)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);
            //var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
            //var x = deptList.ToList().Select(c => new SelectListItem { Text = c.deptname, Value = c.deptId }).ToList();
            //ViewBag.Departments = x;
            //ViewBag.CommitteeId = CommitteeId;
            //return View(eList);
            CommitteeList comList = new CommitteeList();
            comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            ViewBag.CommitteeList = comList.CommitteeView.Select(a => new SelectListItem { Text = a.CommitteeName, Value = a.CommitteeId.ToString() }).ToList();

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);
            // var departmentsValues = eList.Departments.ToList().Select(c => new SelectListItem { Text = c.deptname, Value = c.deptId }).ToList();
            // ViewBag.Departments = departmentsValues;
            //ViewBag.CommitteeId = CommitteeId;
            //ViewBag.eFileID = eFileID;
            return View(eList);
        }

        public ActionResult eFileView(string Id)
        {
            SBL.DomainModel.Models.eFile.eFileList efile = (SBL.DomainModel.Models.eFile.eFileList)Helper.ExecuteService("eFile", "GeteFile", Id);
            efile.eAttachments = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)Helper.ExecuteService("eFile", "GetAlleFileAttachments", Id);
            ViewBag.eFileId = Id;
            return View(efile);
        }

        /// <summary>
        /// Get Committee Report
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetCommitteeReport()
        {
            List<SBL.DomainModel.Models.eFile.eFileAttachment> eFilesAttachments = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)Helper.ExecuteService("eFile", "GeteFileReport", null);
            return View("CommitteeReport", eFilesAttachments);
        }

        /// <summary>
        /// Get departments
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GeteFileBaseDept(string departmentId)
        {
            departmentId = "HPD1098";
            List<SBL.DomainModel.Models.eFile.eFile> eFiles = (List<SBL.DomainModel.Models.eFile.eFile>)Helper.ExecuteService("eFile", "GetCommitteeeFile", departmentId);
            return Json(eFiles, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        //[HttpPost]
        public ActionResult GeteFileSearch(string Department, string eFileNo, string Subject, int CommitteeId, string Status)
        {
            try
            {
                SBL.DomainModel.Models.eFile.eFileSearch Search = new eFileSearch();
                Search.Department = Department;
                Search.eFileNo = eFileNo;
                Search.Subject = Subject;
                Search.CommitteeId = CommitteeId;
                Search.Status = Convert.ToBoolean(Convert.ToInt16(Status.ToString()));
                SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
                eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GeteFileSearch", Search);
                ViewBag.Departments = eList.Departments.ToList().Select(c => new SelectListItem { Text = c.deptname, Value = c.deptId }).ToList();
                return PartialView("_eFileListView", eList.eFileLists);
            }
            catch (Exception ex)
            {
                string excep = ex.ToString();
            }
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }
        //[HttpPost]
        //public ActionResult AddeFileAttachments(SBL.DomainModel.Models.eFile.eFileAttachment model, HttpPostedFileBase file, int CommId = 0)
        //{
        //    if (file != null)
        //    {
        //        model.eFileName = Path.GetFileNameWithoutExtension(file.FileName);
        //        if (!Directory.Exists(Server.MapPath("~/eFileAttachments/" + model.eFileID)))
        //        {
        //            Directory.CreateDirectory(Server.MapPath("~/eFileAttachments/" + model.eFileID));

        //        };
        //        string destinationPath = Path.Combine(Server.MapPath("~/eFileAttachments/" + model.eFileID), Path.GetFileName(file.FileName));
        //        file.SaveAs(destinationPath);

        //        //PDF to Image.
        //        List<int> PDFPageNumbers = new List<int>();
        //        if (!Directory.Exists(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles")))
        //        {
        //            Directory.CreateDirectory(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles"));
        //            PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);

        //        }
        //        else
        //        {
        //            PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);
        //        }

        //        model.SplitFileCount = PDFPageNumbers.Count;
        //        model.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
        //        model.CreatedDate = DateTime.Now;
        //        model.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
        //        model.ModifiedDate = DateTime.Now;
        //        model.eFileNameId = Guid.NewGuid();
        //        model.ParentId = model.ParentId == 0 ? 0 : model.ParentId;
        //        var value = Helper.ExecuteService("eFile", "AddeFileAttachments", model);
        //    }

        //    if (model.Type != "FromeFile")
        //    {
        //        // var result = Helper.ExecuteService("eFile", "GetDepartmentMembers", null);
        //        List<SBL.DomainModel.Models.Member.DepartmentMembers> deptmembers = new List<DomainModel.Models.Member.DepartmentMembers>();
        //        deptmembers = (List<SBL.DomainModel.Models.Member.DepartmentMembers>)Helper.ExecuteService("eFile", "GetDepartmentMembers", null);
        //        CustomMailMessage emailMessage = new CustomMailMessage();

        //        emailMessage._from = "evidhanmail@gmail.com";
        //        emailMessage._isBodyHtml = false;
        //        emailMessage._subject = "Notice From Committee";
        //        emailMessage._body = "Notice From Committee.Please find the attachment.";
        //        emailMessage._toList.Add("test@gmail.com");
        //        //foreach (var item in deptmembers)
        //        //{
        //        //    emailMessage._toList.Add(item.Email);
        //        //}
        //        emailMessage.ProccessDate = DateTime.Now;
        //        EAttachment ea = new EAttachment { FileName = new FileInfo(Server.MapPath("~/EmailAttachment/Test.txt")).Name, FileContent = System.IO.File.ReadAllBytes(Server.MapPath("~/EmailAttachment/Test.txt")) };
        //        emailMessage._attachments.Add(ea);

        //        SMSMessageList smsMessage = new SMSMessageList();

        //        smsMessage.SMSText = "Notice to department";
        //        smsMessage.ModuleActionID = 1;
        //        smsMessage.UniqueIdentificationID = 2;
        //        smsMessage.MobileNo.Add("12345678");
        //        //foreach (var item in deptmembers)
        //        //{
        //        //    smsMessage.MobileNo.Add(item.Mobile);
        //        //}
        //        //string SendSMS = "1"; string SendEmail = "1";
        //        Notification.Send(true, true, smsMessage, emailMessage);
        //    }

        //    if (model.Type == "FromDepartment")
        //    {
        //        return RedirectToAction("ShowDepartmentNotices", "Committee", new { area = "SuperAdmin" });
        //    }
        //    else if (model.Type == "CommitteeToDept")
        //    {
        //        return RedirectToAction("SendNoticeToDepartmentList", "Committee", new { area = "SuperAdmin" });
        //    }
        //    else if (model.Type == "FromeFile")
        //    {
        //        return RedirectToAction("eFileListAll", "eFile", new { area = "eFile", CommitteeId = CommId });
        //    }

        //    else
        //    {
        //        return RedirectToAction("eFileListAll", "eFile", new { area = "eFile" });
        //    }
        //}
        public List<int> PDFToImage(string file, string outputPath, int dpi)
        {
            try
            {
                //string sOutputDirectory = Server.MapPath("~/eFileAttachments/" + eFileId + "/SplitedFiles");
                List<int> PDFPagesCount = new List<int>();
                Ghostscript.NET.Rasterizer.GhostscriptRasterizer rasterizer = null;
#pragma warning disable CS0219 // The variable 'img' is assigned but its value is never used
                System.Drawing.Image img = null;
#pragma warning restore CS0219 // The variable 'img' is assigned but its value is never used
                Ghostscript.NET.GhostscriptVersionInfo vesion = new Ghostscript.NET.GhostscriptVersionInfo(new Version(0, 0, 0), Server.MapPath("~/bin/gsdll32.dll"), string.Empty, Ghostscript.NET.GhostscriptLicense.GPL);

                using (rasterizer = new Ghostscript.NET.Rasterizer.GhostscriptRasterizer())
                {
                    rasterizer.Open(file, vesion, false);

                    for (int i = 1; i <= rasterizer.PageCount; i++)
                    {
                        PDFPagesCount.Add(i);
                        string pageFilePath = Path.Combine(outputPath, Path.GetFileNameWithoutExtension(file) + "-p" + i.ToString() + ".png");

                        //using (img = rasterizer.GetPage(dpi, dpi, i))
                        //{
                        using (Bitmap bmp = new Bitmap(rasterizer.GetPage(dpi, dpi, i)))
                        {
                            bmp.Save(pageFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                            //bmp.Save(pageFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                            //img.Save(pageFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);//
                        }
                    }

                    rasterizer.Close();
                }
                return PDFPagesCount;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
            }
            return null;
        }

        //public static byte[] ConvertImageToByteArray(Image imageToConvert)
        //{
        //    using (var ms = new MemoryStream())
        //    {
        //        Bitmap bmp = new Bitmap(imageToConvert);
        //        bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        return ms.ToArray();
        //    }
        //}

        //public List<int> PDFToImage(string file, string outputPath, int dpi)
        //{
        //    try
        //    {
        //        //string sOutputDirectory = Server.MapPath("~/eFileAttachments/" + eFileId + "/SplitedFiles");
        //        List<int> PDFPagesCount = new List<int>();
        //        Ghostscript.NET.Rasterizer.GhostscriptRasterizer rasterizer = null;
        //        System.Drawing.Image img = null;
        //        Ghostscript.NET.GhostscriptVersionInfo vesion = new Ghostscript.NET.GhostscriptVersionInfo(new Version(0, 0, 0), Server.MapPath("~/bin/gsdll32.dll"), string.Empty, Ghostscript.NET.GhostscriptLicense.GPL);

        //        using (rasterizer = new Ghostscript.NET.Rasterizer.GhostscriptRasterizer())
        //        {
        //            rasterizer.Open(file, vesion, false);

        //            for (int i = 1; i <= rasterizer.PageCount; i++)
        //            {
        //                PDFPagesCount.Add(i);
        //                string pageFilePath = Path.Combine(outputPath, Path.GetFileNameWithoutExtension(file) + "-p" + i.ToString() + ".png");

        //                //using (img = rasterizer.GetPage(dpi, dpi, i))
        //                //{
        //                using (Bitmap bmp = new Bitmap(rasterizer.GetPage(dpi, dpi, i)))
        //                {
        //                    bmp.Save(pageFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
        //                    //img.Save(pageFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);

        //                }
        //            }

        //            rasterizer.Close();
        //        }
        //        return PDFPagesCount;
        //    }

        //    catch (Exception ex)
        //    {
        //    }
        //    return null;
        //}

        //public void PDFToImage(string file, string outputPath, int dpi)
        //{
        //    string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        //    Ghostscript.NET.Rasterizer.GhostscriptRasterizer rasterizer = null;
        //    Ghostscript.NET.GhostscriptVersionInfo vesion = new Ghostscript.NET.GhostscriptVersionInfo(new Version(0, 0, 0), path + @"\gsdll64.dll", string.Empty, Ghostscript.NET.GhostscriptLicense.GPL);

        //    using (rasterizer = new Ghostscript.NET.Rasterizer.GhostscriptRasterizer())
        //    {
        //        rasterizer.Open(file, vesion, false);

        //        for (int i = 1; i <= rasterizer.PageCount; i++)
        //        {
        //            string pageFilePath = Path.Combine(outputPath, Path.GetFileNameWithoutExtension(file) + "-p" + i.ToString() + ".png");

        //            Image img = rasterizer.GetPage(dpi, dpi, i);
        //            img.Save(pageFilePath, ImageFormat.Jpeg);
        //        }

        //        rasterizer.Close();
        //    }
        //    MessageBox.Show("Done");
        //}

        /// <summary>
        /// Split PDF Files in to single files
        /// </summary>
        /// <param name="sourcePdfPath"></param>
        //public List<int> ExtractPages(string sourcePdfPath,int eFileId)// string outputPdfPath)//, int[] extractThesePages)
        //{
        //    iTextSharp.text.pdf.PdfReader reader = null;
        //    iTextSharp.text.Document sourceDocument = null;
        //    iTextSharp.text.pdf.PdfCopy pdfCopyProvider = null;
        //    iTextSharp.text.pdf.PdfImportedPage importedPage = null;
        //    List<int> PDFPagesCount;

        //    try
        //    {
        //        string sOutputDirectory = Server.MapPath("~/eFileAttachments/"+eFileId+"/SplitedFiles");
        //        if (!Directory.Exists(sOutputDirectory))
        //            Directory.CreateDirectory(sOutputDirectory);

        //        {
        //            //sourceDocument = new iTextSharp.text.Document(new iTextSharp.text.Rectangle(50f, 50f));
        //            reader = new iTextSharp.text.pdf.PdfReader(Server.MapPath("~/eFileAttachments/"+eFileId+"/"+sourcePdfPath));
        //            //pgbarLoadBook.Maximum = reader.NumberOfPages;
        //            PDFPagesCount = new List<int>(reader.NumberOfPages);

        //            //For Image convertion.

        //            //string file = System.IO.Path.Combine(Server.MapPath("~/eFileAttachments/"+eFileId+"/"+"GetAssemblyFilePdf.pdf"));
        //            //doc.LoadFromFile(file);

        //            for (int i = 1; i <= reader.NumberOfPages; i++)
        //            {
        //                PDFPagesCount.Add(i);

        //                //sourceDocument = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER, 10, 10, 10, 10);
        //                sourceDocument = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(1));
        //                //sourceDocument.SetPageSize();
        //                //sourceDocument = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER, 10, 10, 10, 10);
        //                // document and an output file stream:
        //                pdfCopyProvider = new iTextSharp.text.pdf.PdfCopy(sourceDocument,
        //                    new System.IO.FileStream(System.IO.Path.Combine(sOutputDirectory, i.ToString() + ".pdf"), System.IO.FileMode.Create));
        //                sourceDocument.Open();
        //                importedPage = pdfCopyProvider.GetImportedPage(reader, i);
        //                pdfCopyProvider.AddPage(importedPage);
        //                sourceDocument.Close();

        //            }

        //            reader.Close();

        //            //save to images
        //            //for (int i = 0; i < doc.Pages.Count; i++)
        //            //{
        //            //    String fileName = Server.MapPath("~/SplitFiles/" + i + ".png");
        //            //    using (Image image = doc.SaveAsImage(i))
        //            //    {
        //            //        image.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);
        //            //        System.Diagnostics.Process.Start(fileName);
        //            //    }
        //            //}

        //            //doc.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return PDFPagesCount;
        //}
        public ActionResult SendAttachmenttoEfile(eFileViewModel model)
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            //documents.eFileID = model.eFileID;
            documents.Type = model.Type;
            //documents.DepartmentId = model.DepartmentId;
            //documents.OfficeCode = model.Officecode;
            //ViewBag.CommitteeId = model.CommitteeId;
            return View(documents);
        }



        #region Committee Code By Shashi
        public ActionResult GetfFileListDetails(int CommitteeId)
        {
            eFileLogin();
            tCommittee mdl = new tCommittee();
            mdl = (tCommittee)Helper.ExecuteService("Committee", "GetCommitteeDetailsByCommitteeId", CommitteeId);

            ViewBag.CommName = mdl.CommitteeName;
            ViewBag.CommYear = mdl.CommitteeYear;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);
            ViewBag.Mode = "Committee";
            ViewBag.CommID = CommitteeId;
            return PartialView("_eFileListView", eList);
        }
        public ActionResult GetfFileListDetailsLayPaper(int CommitteeId)
        {
            eFileLogin();
            tCommittee mdl = new tCommittee();
            mdl = (tCommittee)Helper.ExecuteService("Committee", "GetCommitteeDetailsByCommitteeId", CommitteeId);

            ViewBag.CommName = mdl.CommitteeName;
            ViewBag.CommYear = mdl.CommitteeYear;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);
            ViewBag.Mode = "Committee";
            ViewBag.CommID = CommitteeId;
            return PartialView("_eFileListViewLayPaper", eList);
        }



        public JsonResult UpdateEFileNewDesc(eFileViewModel mdl)
        {
            eFileLogin();
            SBL.DomainModel.Models.eFile.eFile model = new DomainModel.Models.eFile.eFile();
            model.eFileID = mdl.eFileID;
            model.NewDescription = mdl.NewDescription;
            int res = (int)Helper.ExecuteService("eFile", "UpdateeFileNewDescriptionByFileId", model);

            if (res == 1)
            {
                return Json("New Description Updated Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UpdateeFileDetailsByFileId(SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel mdl)
        {
            eFileLogin();
            SBL.DomainModel.Models.eFile.eFile model = new DomainModel.Models.eFile.eFile();
            model.eFileID = mdl.eFileID;
            model.NewDescription = mdl.NewDescription;
            model.FromYear = mdl.FromYear;
            model.ToYear = mdl.ToYear;
            model.DepartmentId = CurrentSession.DeptID;
            //  model.Officecode = lstOffice;
            model.eFileNumber = mdl.eFileNumber;
            model.eFileSubject = mdl.eFileSubject;
            model.OldDescription = mdl.OldDescription;
            model.SelectedDeptId = mdl.SelectedDeptId;
            model.SelectedBranchId = Convert.ToInt32(CurrentSession.BranchId);
            model.BranchId = Convert.ToInt32(CurrentSession.BranchId);
            model.CommitteeId = (int)Helper.ExecuteService("eFile", "GetComtbyBranch", model);
            int res = (int)Helper.ExecuteService("eFile", "UpdateeFileDetailsByFileId", model);

            if (res == 1)
            {
                return Json("New Description Updated Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// For Updateing eFIle by Id- Created By Shubham
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public JsonResult UpdateeFileDetailsByFileIdNew(FormCollection coll)
        {
            eFileLogin();
            SBL.DomainModel.Models.eFile.eFile model = new DomainModel.Models.eFile.eFile();
            model.eFileID = int.Parse(coll["eFileID"]);
            model.NewDescription = coll["NewDescription"];
            model.FromYear = coll["FromYear"];// mdl.FromYear;
            model.ToYear = coll["ToYear"];// mdl.ToYear;
            model.DepartmentId = coll["hdnDepartmentId"];// mdl.DepartmentId;
            model.Officecode = int.Parse(coll["hdnOfficecode"]);// lstOffice;
            model.eFileNumber = coll["eFileNumber"];// mdl.eFileNumber;
            model.eFileSubject = coll["eFileSubject"];// mdl.eFileSubject;
            model.OldDescription = coll["OldDescription"];// mdl.OldDescription;

            int res = (int)Helper.ExecuteService("eFile", "UpdateeFileDetailsByFileId", model);

            if (res == 1)
            {
                return Json("New Description Updated Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult GetReceivePaperList(int pageId = 1, int pageSize = 25)
        {

            eFileLogin();
            eFileAttachments model = new eFileAttachments();
            model.RolesId = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID;
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[2];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            string[] str = new string[4];
            string[] strngBID = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[3] = value[0];


            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptId", str);
            //model.eFileAttachemts = ListModel;

            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();


            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileAttachemts = pagedRecord;
            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            model.eFileLists = eList.eFileLists;

            return PartialView("_receivePaperList", model);
        }
        public PartialViewResult NewReceivePaper()
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();
            documents.PaperRefNo = GetRefNo();
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            ///Get eFilePaperNature
            var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList_Receive", null);
            ViewBag.eFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);

            return PartialView("_ReceivePaper", documents);
        }
        public PartialViewResult NewReceivePaperFromeFileList(int eFileid, string eFileNumber)
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();
            documents.PaperRefNo = GetRefNo();
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            ///Get eFilePaperNature
            var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList_Receive", null);
            ViewBag.eFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);

            documents.eFileID = eFileid;
            ViewBag.eFileNumber = eFileNumber;

            return PartialView("_ReceivePaper", documents);
        }

        public PartialViewResult GetSendPaperList(int pageId = 1, int pageSize = 25)
        {
            eFileLogin();
            eFileAttachments model = new eFileAttachments();

            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            var CommitteeTypePermission = (CommitteeTypePermission)Helper.ExecuteService("eFile", "GetCommitteUser", CurrentSession.UserID);
            if (CommitteeTypePermission != null && CommitteeTypePermission.CommitteeTypeId > 0)
                ViewBag.CommitteeTypePermission = "Yes";
            else
                ViewBag.CommitteeTypePermission = "No";

            //string[] str = new string[2];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            string[] str = new string[4];
            string[] strngBID = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[3] = value[0];

            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendPaperDetailsByDeptId", str);
            //model.eFileAttachemts = ListModel;
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();


            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileAttachemts = pagedRecord;
            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            model.eFileLists = eList.eFileLists;


            return PartialView("_sendPaperList", model);
        }
        public PartialViewResult NewSendPaper()
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();

            documents.PaperRefNo = GetRefNo();
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            ///Get eFilePaperNature
            var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList", null);
            ViewBag._ListeFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);


            return PartialView("_SendPaper", documents);
        }
        public PartialViewResult NewSendPaperFromeFileList(int eFileid, string eFileNumber, int CommetteId)
        {
            eFileLogin();
            eFileAttachment documents = new eFileAttachment();

            documents.PaperRefNo = GetRefNo();
            if (!string.IsNullOrEmpty(CurrentSession.DeptID))
            {
                var UserDeptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartmentByDeptId", CurrentSession.DeptID);
                ViewBag.UserdeptList = new SelectList(UserDeptList, "deptId", "deptname", null);

            }
            else
            {
                ViewBag.UserdeptList = null;
            }
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            ///Get eFilePaperNature
            var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList", null);
            ViewBag._ListeFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);

            documents.eFileID = eFileid;
            ViewBag.eFileNumber = eFileNumber;
            ViewBag.CommetteId = CommetteId;
            return PartialView("_SendPaper", documents);
        }
        static string GetRefNo()
        {
            Random _no = new Random();
            var _Year = DateTime.Today.Year.ToString();
            var _Month = DateTime.Today.Month.ToString();
            int n = _no.Next(00001, 99999);
            string fullRefNo = "PREF" + "/" + _Year + "/" + _Month + "/" + n.ToString();
            return fullRefNo;
        }

        static int GetFileRandomNo()
        {
            Random ran = new Random();
            int rno = ran.Next(1, 99999);
            return rno;
        }
        [HttpPost]
        public JsonResult SaveReceivePaper(eFileAttachment model, HttpPostedFileBase file, int? lstOffice)
        {
            eFileAttachment mdl = new eFileAttachment();
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.eFilePath))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePath));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.eFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                        }

                    }

                }
                else
                {
                    return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }


            #region [ Word File
            string url1Word = "~/ePaper/FileWord";
            string directory1Word = Server.MapPath(url1Word);

            if (Directory.Exists(directory))
            {
                string[] savedFileNameWord = Directory.GetFiles(Server.MapPath(url1Word));
                if (savedFileNameWord.Length > 0)
                {
                    string SourceFile = savedFileNameWord[0];
                    foreach (string page in savedFileNameWord)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.eFilePathWord))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePathWord));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.eFilePathWord = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                        }

                    }

                }
                else
                {
                    mdl.eFilePathWord = null;
                    //return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }
            #endregion

            mdl.SplitFileCount = 0;
            mdl.CreatedBy = new Guid(CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.eFileNameId = Guid.NewGuid();
            model.ParentId = fileID;

            // i am receiver
            mdl.ToDepartmentID = CurrentSession.DeptID; //"HPD0001";
            int OfficeId = 0;
            int.TryParse(CurrentSession.OfficeId, out OfficeId);
            mdl.ToOfficeCode = OfficeId;// int.Parse(CurrentSession.OfficeId);// 0;
            ///


            // I am Sender
            mdl.DepartmentId = model.DepartmentId;//CurrentSession.DeptID; AddeFileAttachments
            string _lstOffice = Convert.ToString(lstOffice);
            if (!string.IsNullOrEmpty(_lstOffice))
            {
                mdl.OfficeCode = int.Parse(_lstOffice);
            }
            else
            {
                mdl.OfficeCode = 0;
            }
            //////

            if (model.PType == "4")
            {
                mdl.PType = "R";
            }
            else
            {
                mdl.PType = "O"; //model.PType;
            }


            mdl.DocumentTypeId = model.DocumentTypeId;
            mdl.Description = model.Description;
            mdl.PaperNature = model.PaperNature;
            mdl.PaperNatureDays = model.PaperNatureDays;
            mdl.Title = model.Title;
            mdl.PaperRefNo = model.PaperRefNo;
            mdl.RecvDetails = model.RecvDetails;
            mdl.RevedOption = model.RevedOption;
            mdl.ToRecvDetails = model.RecvDetails;
            mdl.PaperTicketValue = model.PaperTicketValue;
            mdl.PaperNo = model.PaperNo;
            mdl.DispatchtoWhom = model.DispatchtoWhom;
            mdl.eFileID = model.eFileID;
            //Helper.ExecuteService("eFile", "AddeFileAttachments", mdl);


            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@DocumentTypeId", mdl.DocumentTypeId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileName", mdl.eFileName));
            methodParameter.Add(new KeyValuePair<string, string>("@Description", mdl.Description));
            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", mdl.DepartmentId));
            methodParameter.Add(new KeyValuePair<string, string>("@Title", mdl.Title));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileNameId", mdl.eFileNameId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DocType", "ReceivePaperByDept"));
            methodParameter.Add(new KeyValuePair<string, string>("@SplitFileCount", mdl.SplitFileCount.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@OfficeCode", mdl.OfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNature", mdl.PaperNature.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNatureDays", mdl.PaperNatureDays));
            methodParameter.Add(new KeyValuePair<string, string>("@ToDepartmentID", mdl.ToDepartmentID));
            methodParameter.Add(new KeyValuePair<string, string>("@ToOfficeCode", mdl.ToOfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperStatus", "N"));
            methodParameter.Add(new KeyValuePair<string, string>("@PType", mdl.PType));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePath", mdl.eFilePath));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperRefNo", mdl.PaperRefNo));
            methodParameter.Add(new KeyValuePair<string, string>("@RecvDetails", mdl.RecvDetails));
            methodParameter.Add(new KeyValuePair<string, string>("@ToRecvDetails", mdl.ToRecvDetails));
            methodParameter.Add(new KeyValuePair<string, string>("@RevedOption", mdl.RevedOption));
            methodParameter.Add(new KeyValuePair<string, string>("@isSend", "0"));
            methodParameter.Add(new KeyValuePair<string, string>("@TicketValue", mdl.PaperTicketValue));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNo", mdl.PaperNo));
            methodParameter.Add(new KeyValuePair<string, string>("@WhomtoDispatch", mdl.DispatchtoWhom));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileId", "0"));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePathWord", mdl.eFilePathWord));
            methodParameter.Add(new KeyValuePair<string, string>("@ReFileId", mdl.eFileID.ToString()));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperEntry", methodParameter);
            string res = "", serno = "";
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                res = dataSet.Tables[0].Rows[0][0].ToString();
                serno = dataSet.Tables[0].Rows[0][1].ToString();
            }
            if (res == "Success")
            {

                return Json("Paper Received Successfully. Paper Ref No. : <span class='label label-xlg label-primary arrowed arrowed-right' style='  font-size: 16px;  height: 29px;'><b>" + serno + "</b></span>", JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json("Some Problem Occured. Please Try Again", JsonRequestBehavior.AllowGet);

            }

        }

        public PartialViewResult CreateeFile()
        {
            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new Models.eFileViewModel();
            model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            model.DocumentType = (List<SBL.DomainModel.Models.Document.mDocumentType>)Helper.ExecuteService("eFile", "eFileDocumentIndex", 1);

            //EFile Type List  added on 27 July 2015
            model.eFileTypeList = (List<SBL.DomainModel.Models.eFile.eFileType>)Helper.ExecuteService("eFile", "eFileTypeDetails", null);
            ///********//
            ///
            model.CommitteeId = 0;
            model.Mode = "Add";
            if (!string.IsNullOrEmpty(CurrentSession.DeptID))
            {
                model.DepartmentId = CurrentSession.DeptID;
            }

            int OfficeId = 0;
            int.TryParse(CurrentSession.OfficeId, out OfficeId);
            model.Officecode = OfficeId;// Convert.ToInt32(CurrentSession.OfficeId);
            ViewBag.OffICode = CurrentSession.OfficeId;
            model.SelectedBranchName = CurrentSession.BranchName.ToString();
            return PartialView("_CreateeFile", model);
        }

        public PartialViewResult eFileListDetails()
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[2];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            string[] str = new string[4];
            string[] strngBID = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[3] = value[0];

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            return PartialView("_eFileListView", eList);

        }
        public PartialViewResult eFileListDetailsALLStatus(int CommitteeId, int pageId = 1, int pageSize = 25)
        {
            SBL.DomainModel.Models.eFile.eFile mdl = new DomainModel.Models.eFile.eFile();
            mdl.DepartmentId = CurrentSession.DeptID;
            int OfficeId = 0;
            int.TryParse(CurrentSession.OfficeId, out OfficeId);

            mdl.Officecode = OfficeId;// Convert.ToInt32(CurrentSession.OfficeId);
            mdl.CommitteeId = CommitteeId;
            ViewBag.Mode = "Committee";
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListALLStatus", mdl);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            return PartialView("_eFileListViewAll", eList);

        }

        public PartialViewResult eFileListDetailsActiveForSEC(int pageId = 1, int pageSize = 25, int branchID = 1)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            var CommitteeTypePermission = (CommitteeTypePermission)Helper.ExecuteService("eFile", "GetCommitteUser", CurrentSession.UserID);
            if (CommitteeTypePermission != null && CommitteeTypePermission.CommitteeTypeId > 0)
                ViewBag.CommitteeTypePermission = "Yes";
            else
                ViewBag.CommitteeTypePermission = "No";

            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            //str[2] = CurrentSession.BranchId;
            str[2] = Convert.ToString(branchID);

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListR", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            return PartialView("_eFileListViewAll", eList);

        }

        public PartialViewResult eFileListDetailsActive(int pageId = 1, int pageSize = 25)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            var CommitteeTypePermission = (CommitteeTypePermission)Helper.ExecuteService("eFile", "GetCommitteUser", CurrentSession.UserID);
            if (CommitteeTypePermission != null && CommitteeTypePermission.CommitteeTypeId > 0)
                ViewBag.CommitteeTypePermission = "Yes";
            else
                ViewBag.CommitteeTypePermission = "No";

            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;

            //for secretary added by robin
            if (str[2] == "")
            {
                var BrnchList = (List<mBranches>)Helper.ExecuteService("eFile", "GetAllBranchList", null);//for multiple branches dropdown
                str[2] = Convert.ToString(BrnchList.FirstOrDefault().BranchId);
            }

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListR", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            //ViewBag.PageSize = pageSize;
            //List<eFileList> pagedRecord = new List<eFileList>();
            //if (pageId == 1)
            //{
            //    pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            //}
            //else
            //{
            //    int r = (pageId - 1) * pageSize;
            //    pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            //}

            //ViewBag.CurrentPage = pageId;
            //ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            //eList.eFileLists = pagedRecord;
            return PartialView("_eFileListViewAll", eList);

        }
        public PartialViewResult eFileListDetailsDeactive(int pageId = 1, int pageSize = 25)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            var CommitteeTypePermission = (CommitteeTypePermission)Helper.ExecuteService("eFile", "GetCommitteUser", CurrentSession.UserID);
            if (CommitteeTypePermission != null && CommitteeTypePermission.CommitteeTypeId > 0)
                ViewBag.CommitteeTypePermission = "Yes";
            else
                ViewBag.CommitteeTypePermission = "No";
            string[] str = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListDeactive", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            return PartialView("_eFileListViewAll", eList);

        }

        public PartialViewResult eFileListDetailsActiveCommAttach(int pageId = 1, int pageSize = 25)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            ViewBag.Mode = "Committee";
            ViewBag.CommAttach = "CommitteeAttach";

            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[2];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            string[] str = new string[4];
            string[] strngBID = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[3] = value[0];

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            //return PartialView("_eFileListViewAll", eList);
            return PartialView("_eFileListViewAllCommitteeAttach", eList);

        }
        public PartialViewResult eFileListDetailsDeactiveCommAttach(int pageId = 1, int pageSize = 25)
        {
            ViewBag.Mode = "Committee";
            ViewBag.CommAttach = "CommitteeAttach";
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListDeactive", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            //return PartialView("_eFileListViewAll", eList);
            return PartialView("_eFileListViewAllCommitteeAttach", eList);

        }

        public PartialViewResult eFileListDetailsActiveLayPaper(int CommitteeId, int pageId = 1, int pageSize = 25)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();


            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CommitteeId.ToString();

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListByCommitteeId", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            ViewBag.Mode = "Committee";
            return PartialView("_eFileListViewAllLayPaper", eList);

        }
        public PartialViewResult eFileListDetailsDeactiveLayPaper(int CommitteeId, int pageId = 1, int pageSize = 25)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            string[] str = new string[3];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CommitteeId.ToString();

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListDeactiveByCommitteeId", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            ViewBag.Mode = "Committee";
            return PartialView("_eFileListViewAllLayPaper", eList);

        }

        [HttpPost]
        public JsonResult SaveSendPaper(eFileAttachment model, int? lstOffice, int? CommetteId, string UserDepartmentId)
        {



            eFileAttachment mdl = new eFileAttachment();
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.eFilePath))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePath));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.eFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");

                        }

                    }

                }
                else
                {
                    return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }


            #region Upload Word File
            // string url1Word = "/ePaper/FileWord";
            string directory1Word = Server.MapPath(url);
            if (!System.IO.Directory.Exists(url))
            {
                System.IO.Directory.CreateDirectory(url);
            }
            if (Directory.Exists(directory))
            {

                string[] savedFileNameWord = Directory.GetFiles(Server.MapPath(url));
                if (savedFileNameWord.Length > 0)
                {
                    string SourceFiles = savedFileNameWord[0];
                    foreach (string page1 in savedFileNameWord)
                    {
                        Guid FileName1 = Guid.NewGuid();

                        string nametwo = Path.GetFileName(page1);

                        string path1 = System.IO.Path.Combine(directory, FileName1 + "_" + nametwo.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(nametwo))
                        {

                            if (!string.IsNullOrEmpty(mdl.eFilePathWord))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePathWord));

                            System.IO.File.Copy(SourceFiles, path1, true);
                            mdl.eFilePathWord = "/ePaper/" + FileName1 + "_" + nametwo.Replace(" ", "");
                        }

                    }

                }
                else
                {
                    mdl.eFilePathWord = null;
                    //return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

                //string[] savedFileNameWord = Directory.GetFiles(Server.MapPath(url1Word));
                //if (savedFileNameWord.Length > 0)
                //{
                //    string SourceFiles = savedFileNameWord[0];
                //    foreach (string page1 in savedFileNameWord)
                //    {
                //        Guid FileName1 = Guid.NewGuid();

                //        string nametwo = Path.GetFileName(page1);

                //        string path1 = System.IO.Path.Combine(directory, FileName1 + "_" + nametwo.Replace(" ", ""));


                //        if (!string.IsNullOrEmpty(nametwo))
                //        {

                //            if (!string.IsNullOrEmpty(mdl.eFilePathWord))
                //                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePathWord));

                //            System.IO.File.Copy(SourceFiles, path1, true);
                //            mdl.eFilePathWord = "/ePaper/" + FileName1 + "_" + nametwo.Replace(" ", "");
                //        }

                //    }

                //}
                //else
                //{
                //    mdl.eFilePathWord = null;
                //    //return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                //}

            }
            #endregion

            //mdl.eFilePathWord = null;
            mdl.SplitFileCount = 0;
            mdl.CreatedBy = new Guid(CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.eFileNameId = Guid.NewGuid();
            model.ParentId = fileID;


            // I am Receiver
            mdl.ToDepartmentID = model.DepartmentId;
            string _lstOffice = Convert.ToString(lstOffice);
            if (!string.IsNullOrEmpty(_lstOffice))
            {
                mdl.ToOfficeCode = int.Parse(_lstOffice);
            }
            else
            {
                mdl.ToOfficeCode = 0;
            }
            //////


            // I am Sender
            mdl.DepartmentId = UserDepartmentId;// CurrentSession.DeptID;
            int officecode = 0;
            int.TryParse(CurrentSession.OfficeId, out officecode);
            mdl.OfficeCode = officecode; //int.Parse(CurrentSession.OfficeId);// 0;

            ////

            if (model.PType == "4")
            {
                mdl.PType = "R";
            }
            else
            {
                mdl.PType = model.PType;
            }

            mdl.DocumentTypeId = model.DocumentTypeId;
            mdl.Description = model.Description;
            mdl.PaperNature = model.PaperNature;
            mdl.PaperNatureDays = model.PaperNatureDays;
            mdl.Title = model.Title;
            mdl.PaperRefNo = model.PaperRefNo;
            mdl.ToRecvDetails = model.ToRecvDetails;
            mdl.RecvDetails = model.ToRecvDetails;
            mdl.RevedOption = model.RevedOption;
            mdl.eFileID = model.eFileID;
            //Helper.ExecuteService("eFile", "AddeFileAttachments", mdl);

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@DocumentTypeId", mdl.DocumentTypeId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileName", mdl.eFileName));
            methodParameter.Add(new KeyValuePair<string, string>("@Description", mdl.Description));
            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", mdl.DepartmentId));
            methodParameter.Add(new KeyValuePair<string, string>("@Title", mdl.Title));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileNameId", mdl.eFileNameId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DocType", "SendPaperToDept"));
            methodParameter.Add(new KeyValuePair<string, string>("@SplitFileCount", mdl.SplitFileCount.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@OfficeCode", mdl.OfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNature", mdl.PaperNature.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNatureDays", mdl.PaperNatureDays));
            methodParameter.Add(new KeyValuePair<string, string>("@ToDepartmentID", mdl.ToDepartmentID));
            methodParameter.Add(new KeyValuePair<string, string>("@ToOfficeCode", mdl.ToOfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperStatus", "N"));
            methodParameter.Add(new KeyValuePair<string, string>("@PType", mdl.PType));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePath", mdl.eFilePath));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperRefNo", mdl.PaperRefNo));
            methodParameter.Add(new KeyValuePair<string, string>("@RecvDetails", mdl.RecvDetails));
            methodParameter.Add(new KeyValuePair<string, string>("@ToRecvDetails", mdl.ToRecvDetails));
            methodParameter.Add(new KeyValuePair<string, string>("@RevedOption", mdl.RevedOption));
            methodParameter.Add(new KeyValuePair<string, string>("@isSend", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileId", mdl.eFileID.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePathWord", mdl.eFilePathWord));
            methodParameter.Add(new KeyValuePair<string, string>("@ReFileId", "0"));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperEntry", methodParameter);

            string res = "", serno = "", eFIleAttachmentId = "";
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                res = dataSet.Tables[0].Rows[0][0].ToString();
                serno = dataSet.Tables[0].Rows[0][1].ToString();
                eFIleAttachmentId = dataSet.Tables[0].Rows[0][2].ToString();
            }
            if (res == "Success")
            {
                if (CommetteId.HasValue && CommetteId.Value > 0)
                    return Json(string.Format("{0},{1}", serno, "Commette"), JsonRequestBehavior.AllowGet);
                else
                    return Json(string.Format("{0},{1}", serno, eFIleAttachmentId), JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json("Some Problem Occured. Please Try Again", JsonRequestBehavior.AllowGet);

            }


        }
        public JsonResult SendPubishedTourSMS(string MobileNos, int FileAttachmentId, string EmailIds)
        {
            string Message = string.Empty;
            string[] mobnos = MobileNos.Split(',');
            if (mobnos.Count() > 0)
            {

                List<string> mobileList = new List<string>();
                List<string> EmailList = new List<string>();

                eFileAttachment File = (eFileAttachment)Helper.ExecuteService("eFile", "GetAlleFileAttachmentsByeFileID", new eFileAttachment { eFileAttachmentId = FileAttachmentId });
                string msgBody = string.Format(" Dear Sir/Madam, \n {0}. \n Secretary HP Vidhan Sabha", File.Title);

                foreach (var item in MobileNos.Split(','))
                {
                    mobileList.Add(item);
                }
                foreach (var item in EmailIds.Split(','))
                {
                    EmailList.Add(item);
                }
                SendEmailDetails(EmailList, mobileList, msgBody, "HP Vidhan Sabha", msgBody, true, true, string.Empty);
                Message = "Sucessfully send email and sms";
            }
            else
            {
                Message = "No Phone Number Found.";
            }

            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        #region Send Email
        public static void SendEmailDetails(List<string> emailList, List<string> mobileList, string msgBody, string mailSubject, string mailBody, bool mobileStatus, bool emailStatus, string SavedPdfPath)
        {
            #region SMS And Email

            List<string> emailAddresses = new List<string>();
            List<string> phoneNumbers = new List<string>();

            SMSMessageList smsMessage = new SMSMessageList();
            CustomMailMessage emailMessage = new CustomMailMessage();

            //# SMS Settings
            if (emailList != null)
            {
                emailAddresses.AddRange(emailList);

            }
            if (mobileList != null)
            {
                phoneNumbers.AddRange(mobileList);
            }
            if (emailAddresses != null && phoneNumbers != null)
            {
                foreach (var item in phoneNumbers)
                {
                    smsMessage.MobileNo.Add(item);
                }
                foreach (var item in emailAddresses)
                {
                    emailMessage._toList.Add(item);
                }
            }

            smsMessage.SMSText = msgBody;
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = 1;
            emailMessage._isBodyHtml = true;
            emailMessage._subject = mailSubject;
            emailMessage._body = mailBody;
            if (!string.IsNullOrEmpty(SavedPdfPath))
            {
                EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes((SavedPdfPath)) };
                emailMessage._attachments = new List<EAttachment>();
                emailMessage._attachments.Add(ea);
            }
            try
            {
                if (emailList != null || mobileList != null)
                {
                    Notification.Send(mobileStatus, emailStatus, smsMessage, emailMessage);
                }

            }
            catch (Exception exp)
            {
                string errorMsg = exp.Message;
                // throw;
            }

            #endregion SMS And Email
        }

        #endregion
        [HttpPost]
        public JsonResult SaveReceivePaperSingle(int eFileAttachId, int eFielId)
        {
            eFileAttachment model = new eFileAttachment();
            model.eFileAttachmentId = eFileAttachId;
            model.eFileID = eFielId;
            model.ReceivedDate = DateTime.Now;
            model.PaperStatus = "Y";
            int res = (int)Helper.ExecuteService("eFile", "UpdateReceivePaperSingle", model);

            if (res == 0)
            {
                return Json("Paper Received Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult AttachPaperToeFileMultiple(string eFileAttIds, int eFielId)
        {
            //int[] d=new  int[3];
            eFileAttachment model = new eFileAttachment();
            string[] d = eFileAttIds.Split(',');
            int[] brnds = new int[d.Length - 1];

            for (int i = 0; i < d.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(d[i].ToString()))
                {
                    brnds[i] = Convert.ToInt32(d[i]);
                }
            }

            model.eFileAttachmentIds = brnds;
            model.eFileID = eFielId;
            //model.ReceivedDate = DateTime.Now;
            //model.PaperStatus = "Y";
            int res = (int)Helper.ExecuteService("eFile", "AttachPapertoEFileMul", model);

            if (res == 0)
            {
                return Json("Paper Attached Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AttachPaperToeFileMultipleR(string eFileAttIds, int eFielId)
        {
            //int[] d=new  int[3];
            eFileAttachment model = new eFileAttachment();
            string[] d = eFileAttIds.Split(',');
            int[] brnds = new int[d.Length - 1];

            for (int i = 0; i < d.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(d[i].ToString()))
                {
                    brnds[i] = Convert.ToInt32(d[i]);
                }
            }

            model.eFileAttachmentIds = brnds;
            model.ReFileID = eFielId;
            //model.ReceivedDate = DateTime.Now;
            //model.PaperStatus = "Y";
            int res = (int)Helper.ExecuteService("eFile", "AttachPapertoEFileMulR", model);

            if (res == 0)
            {
                return Json("Paper Attached Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult ReplyPaperSingle(int eFileAttachmentId, string PaperRefNo, string DepartmentId, int OfficeCode, int DocumentTypeId, string LinkedRefNo, int PaperNature)
        {
            eFileAttachment documents = new eFileAttachment();
            if (PaperNature == 4 || PaperNature == 5)
            {
                documents.PaperRefNo = LinkedRefNo;
            }
            else
            {
                documents.PaperRefNo = PaperRefNo;
            }
            documents.eFileAttachmentId = eFileAttachmentId;
            documents.DepartmentId = DepartmentId;
            documents.OfficeCode = OfficeCode;
            documents.DocumentTypeId = DocumentTypeId;

            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);

            ///Get eFilePaperNature
            var _ListeFilePaperNature = (List<eFilePaperNature>)Helper.ExecuteService("eFile", "Get_eFilePaperNatureList", null);
            ViewBag.eFilePaperNature = new SelectList(_ListeFilePaperNature, "PaperNatureID", "PaperNature", null);

            ///Get eFilePaperType
            var _ListeFilePaperType = (List<eFilePaperType>)Helper.ExecuteService("eFile", "Get_eFilePaperTypeList", null);
            ViewBag.eFilePaperType = new SelectList(_ListeFilePaperType, "PaperTypeID", "PaperType", null);

            return PartialView("_Reply", documents);
        }

        [HttpPost]
        public JsonResult SaveReplyPaper(eFileAttachment model, HttpPostedFileBase file, int? lstOffice, string ReferenceNo)
        {
            eFileAttachment mdl = new eFileAttachment();
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;
            //string url = "~/ePaper/";
            //string directory = Server.MapPath(url);

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.eFilePath))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.eFilePath));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.eFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");

                        }

                    }

                }
                else
                {
                    return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }




            //mdl.eFileName = Path.GetFileNameWithoutExtension(file.FileName);

            //string OnlyFileName = mdl.eFileName + "_" + fileID;
            //string Extension = Path.GetExtension(file.FileName);

            //string FullFileName = OnlyFileName + Extension;


            //if (!Directory.Exists(Server.MapPath("~/ePaper")))
            //{
            //    Directory.CreateDirectory(Server.MapPath("~/ePaper"));
            //};
            //string destinationPath = Path.Combine(Server.MapPath("~/ePaper/"), FullFileName);
            //file.SaveAs(destinationPath);


            //string FilePath = "..\\ePaper" + "\\" + FullFileName;
            mdl.SplitFileCount = 0;
            mdl.CreatedBy = new Guid(CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.eFileNameId = Guid.NewGuid();
            model.ParentId = fileID;


            mdl.ToDepartmentID = model.DepartmentId;  //"HPD0001";
            string _lstOffice = Convert.ToString(lstOffice);
            if (!string.IsNullOrEmpty(_lstOffice))
            {
                mdl.ToOfficeCode = int.Parse(_lstOffice);
            }
            else
            {
                mdl.ToOfficeCode = 0;
            }
            mdl.DepartmentId = CurrentSession.DeptID;//CurrentSession.DeptID; AddeFileAttachments
            int officecode = 0;
            int.TryParse(CurrentSession.OfficeId, out officecode);
            mdl.OfficeCode = officecode; //int.Parse(CurrentSession.Offic
            //   mdl.OfficeCode = Convert.ToInt32(CurrentSession.OfficeId);

            mdl.PType = model.PType;
            mdl.DocumentTypeId = model.DocumentTypeId;
            mdl.Description = model.Description;
            mdl.PaperNature = model.PaperNature;
            mdl.PaperNatureDays = model.PaperNatureDays;
            mdl.Title = model.Title;
            mdl.PaperRefNo = model.PaperRefNo;
            mdl.eFileAttachmentId = model.eFileAttachmentId;
            //Helper.ExecuteService("eFile", "AddeFileAttachments", mdl);

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@DocumentTypeId", mdl.DocumentTypeId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileName", mdl.eFileName));
            methodParameter.Add(new KeyValuePair<string, string>("@Description", mdl.Description));
            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", mdl.DepartmentId));
            methodParameter.Add(new KeyValuePair<string, string>("@Title", mdl.Title));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileNameId", mdl.eFileNameId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@DocType", "ReceivePaperByDept"));
            methodParameter.Add(new KeyValuePair<string, string>("@SplitFileCount", mdl.SplitFileCount.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@OfficeCode", mdl.OfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNature", mdl.PaperNature.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNatureDays", mdl.PaperNatureDays));
            methodParameter.Add(new KeyValuePair<string, string>("@ToDepartmentID", mdl.ToDepartmentID));
            methodParameter.Add(new KeyValuePair<string, string>("@ToOfficeCode", mdl.ToOfficeCode.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperStatus", "N"));
            methodParameter.Add(new KeyValuePair<string, string>("@PType", mdl.PType));
            methodParameter.Add(new KeyValuePair<string, string>("@eFilePath", mdl.eFilePath));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperRefNo", ReferenceNo));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileAttachmentId", mdl.eFileAttachmentId.ToString()));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperEntry_Reply", methodParameter);
            string res = "", serno = "";
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                res = dataSet.Tables[0].Rows[0][0].ToString();
                serno = dataSet.Tables[0].Rows[0][1].ToString();
            }
            if (res == "Success")
            {

                return Json("Paper Replied Successfully. Paper Ref No. : <span class='label label-xlg label-primary arrowed arrowed-right' style='  font-size: 16px;  height: 29px;'><b>" + serno + "</b></span>", JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json("Some Problem Occured. Please Try Again", JsonRequestBehavior.AllowGet);

            }

        }


        [HttpPost]
        public JsonResult AttacheFileToCommitteeMultiple(string eFileAttIds, int eFielId)
        {
            //int[] d=new  int[3];
            eFileAttachment model = new eFileAttachment();
            string[] d = eFileAttIds.Split(',');
            int[] brnds = new int[d.Length - 1];

            for (int i = 0; i < d.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(d[i].ToString()))
                {
                    brnds[i] = Convert.ToInt32(d[i]);
                }
            }


            model.eFileAttachmentIds = brnds;
            model.eFileID = eFielId;
            //model.ReceivedDate = DateTime.Now;
            //model.PaperStatus = "Y";
            int res = (int)Helper.ExecuteService("eFile", "AttacheFiletoCommitteeMul", model);

            if (res == 0)
            {
                return Json("eFile attached to Committee Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Some Error Occured ! Please Try Again", JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult AddNewNoting(int eFileId, string eFileNumber)
        {
            eFileNoting model = new eFileNoting();
            model.eFileID = eFileId;
            ViewBag.eFileNum = eFileNumber;
            return PartialView("_AddNoting", model);

        }

        [HttpPost]
        public JsonResult AddNewNotingtoeFile(eFileNoting model, HttpPostedFileBase file, string hdnCheckVal)
        {

            eFileNoting mdl = new eFileNoting();

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/eFileAttachments/";
            string MainDirectory = FileSettings.SettingValue + url;

            string url1 = "~/ePaper/TempFile";
            string directory = Server.MapPath(url1);

            string UrlImage = "~/ePaper/FileWord";
            string directoryImage = Server.MapPath(UrlImage);

            string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
            if (savedFileName.Length > 0)
            {
                string SourceFile = savedFileName[0];
                foreach (string page in savedFileName)
                {
                    Guid FileName = Guid.NewGuid();

                    string name = Path.GetFileName(page);

                    string pathCompress = System.IO.Path.Combine(directory, name);
                    string pathCompressImage = System.IO.Path.Combine(directoryImage, name);
                    Extensions.ImageResizerExtensions imgex = new Extensions.ImageResizerExtensions(600);
                    System.IO.File.Copy(pathCompress, pathCompressImage, true);
                    imgex.Resize(pathCompress, pathCompressImage);

                    System.IO.File.Delete(pathCompress);
                    System.IO.File.Copy(pathCompressImage, pathCompress, true);
                    System.IO.File.Delete(pathCompressImage);




                    string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));

                    string extfile = Path.GetExtension(page);

                    if (extfile == ".jpg")  //|| extfile == ".png" || extfile == ".jpeg" || extfile == ".gif"
                    { }
                    else
                    {
                        return Json("Only Image File Required  '.jpg' Extension File ", JsonRequestBehavior.AllowGet); //or '.png' or 'jpeg' or '.gif'

                    }



                    if (!string.IsNullOrEmpty(name))
                    {
                        if (!string.IsNullOrEmpty(mdl.FilePath))
                            System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.FilePath));

                        model.eNotingFileName = Path.GetFileNameWithoutExtension(name);
                        string fileExt = Path.GetExtension(name);
                        if (!Directory.Exists(MainDirectory + model.eFileID))
                        {
                            Directory.CreateDirectory(MainDirectory + model.eFileID);
                        };
                        string destinationPath = Path.Combine(MainDirectory + model.eFileID, Path.GetFileName(name));
                        //System.IO.File.SaveAs(destinationPath);
                        //System.IO.File.Copy(SourceFile, destinationPath, true);
                        int PageSeq = (int)Helper.ExecuteService("eFile", "GetPNo", model.eFileID);
                        PageSeq += 1;
                        //PDF to Image.
                        List<int> PDFPageNumbers = new List<int>();

                        if (!Directory.Exists(MainDirectory + model.eFileID + "/SplitedFiles"))
                        {
                            Directory.CreateDirectory(MainDirectory + model.eFileID + "/SplitedFiles");


                            //string pageFilePath = Path.Combine(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles"),model.eNotingFileName + "-p" + PageSeq + "" + fileExt);
                            //System.IO.File.Copy(SourceFile, pageFilePath, true);
                            //PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eNotingFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);
                        }
                        //else
                        //{

                        /// Resize Code ////

                        //string basePath = System.IO.Path.Combine(directory, model.eNotingFileName);
                        //Extensions.ImageResizerExtensions imgex = new Extensions.ImageResizerExtensions(600);



                        //////////////////////

                        string pageFilePath = Path.Combine(MainDirectory + model.eFileID + "/SplitedFiles", model.eNotingFileName + "-p" + PageSeq + "" + fileExt);
                        System.IO.File.Copy(SourceFile, pageFilePath, true);
                        //PDFPageNumbers = PDFToImage(Server.MapPath("~/eFileAttachments/" + model.eFileID + "/" + model.eNotingFileName + ".pdf"), Server.MapPath("~/eFileAttachments/" + model.eFileID + "/SplitedFiles/"), 72);
                        //}


                        model.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
                        model.CreatedDate = DateTime.Now;
                        model.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
                        model.ModifiedDate = DateTime.Now;
                        model.FilePath = url + model.eFileID + "/SplitedFiles/" + model.eNotingFileName + "-p" + PageSeq + "" + fileExt;
                        model.NotingStatus = 1; //1 for active  2 for updated



                        mdl.FilePath = url + model.eFileID + "/" + model.eNotingFileName + "" + fileExt;

                        if (hdnCheckVal == "0")
                        {
                            model.FileUploadSeq = PageSeq;
                            var value = (Boolean)Helper.ExecuteService("eFile", "AddeFileNotings", model);
                            if (value == true)
                            {
                                return Json("Noting Addedd Successfully", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("Some error found.Please Try again", JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            model.FileUploadSeq = (int)Helper.ExecuteService("eFile", "GetPNo", model.eFileID);



                            if (model.FileUploadSeq != 0)
                            {
                                int eNotingID = (int)Helper.ExecuteService("eFile", "GeteNotingIDByeFileIdandSeqNo", model);

                                // Update Noting
                                model.eNotingID = eNotingID;
                                model.NotingStatus = 2;

                                var value = (Boolean)Helper.ExecuteService("eFile", "UpdateLasteFileNotings", model);
                                if (value == true)
                                {
                                    return Json("Noting Updated Successfully", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json("Some error found in Update.Please Try again", JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json("No Noting Details Found to Update", JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    return Json("Some error1 found.Please Try again", JsonRequestBehavior.AllowGet);
                }
                return Json("Some error2 found.Please Try again", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
            }


        }

        public PartialViewResult eFileByCommittee()
        {
            eFileLogin();

            CommitteeList comList = new CommitteeList();
            comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            ViewBag.CommitteeList = comList.CommitteeView.Select(a => new SelectListItem { Text = a.CommitteeName, Value = a.CommitteeId.ToString() }).ToList();

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            return PartialView("_eFileByCommittee", eList);
        }

        public ActionResult eFileIndexDetails(eFileViewModel model)
        {
            try
            {
                eFileAttachment documents = new eFileAttachment();
                List<int> PDFPageNumbers = new List<int>();
                documents.eFileID = model.eFileID;
                documents.DepartmentId = CurrentSession.DeptID;
                int officecode = 0;
                int.TryParse(CurrentSession.OfficeId, out officecode);
                documents.OfficeCode = officecode; //int.Parse(CurrentSession.Offic
                //   documents.OfficeCode = Convert.ToInt32(CurrentSession.OfficeId);

                ViewBag.Subject = model.eFileSubject;
                ViewBag.eFileNum = model.eFileNumber;
                //model. = (List<string>)Helper.ExecuteService("eFile", "eFileDocumentIndex", model.eFileID);
                //model.eFileAttachment = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)Helper.ExecuteService("eFile", "GetAlleFileAttachments", model.eFileID);
                //model.eFileAttachment = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)Helper.ExecuteService("eFile", "GetAlleFileAttachmentsByDeptIDandeFileID", documents);

                List<eFileAttachment> ListModel = new List<eFileAttachment>();
                eFileAttachment obj = new eFileAttachment();
                DataSet dataSet = new DataSet();
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@efileid", documents.eFileID.ToString()));
                methodParameter.Add(new KeyValuePair<string, string>("@DeptID", documents.DepartmentId));
                methodParameter.Add(new KeyValuePair<string, string>("@officecode", documents.OfficeCode.ToString()));

                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_GeteFileIndexData", methodParameter);
                if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        obj = new eFileAttachment();
                        obj.eFileAttachmentId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["efileattachmentid"]);
                        obj.PaperRefNo = dataSet.Tables[0].Rows[i]["PaperRefNo"].ToString();
                        obj.CreatedDate = Convert.ToDateTime(dataSet.Tables[0].Rows[i]["PDate"]);
                        obj.mode = dataSet.Tables[0].Rows[i]["PMode"].ToString();
                        obj.Title = dataSet.Tables[0].Rows[i]["Title"].ToString();
                        obj.eFilePath = dataSet.Tables[0].Rows[i]["efilepath"].ToString();
                        obj.LinkedRefNo = dataSet.Tables[0].Rows[i]["linkedrefno"].ToString();
                        obj.DepartmentId = dataSet.Tables[0].Rows[i]["deptid"].ToString();
                        obj.RecvDetails = dataSet.Tables[0].Rows[i]["details"].ToString();
                        obj.Name = dataSet.Tables[0].Rows[i]["FullSubject"].ToString();
                        obj.RevedOption = dataSet.Tables[0].Rows[i]["RevedOption"].ToString();
                        obj.PType = dataSet.Tables[0].Rows[i]["PType"].ToString();
                        obj.ReplyStatus = dataSet.Tables[0].Rows[i]["ReplyStatus"].ToString();
                        obj.eMode = Convert.ToInt32(dataSet.Tables[0].Rows[i]["emode"]);
                        obj.SendStatus = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["sendstatus"]);
                        obj.CommitteeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["CommitteeId"]);
                        obj.DocumentTypeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["DocumentTypeId"]);
                        obj.ReportLayingHouse = Convert.ToInt32(dataSet.Tables[0].Rows[i]["ReportLayingHouse"]);
                        obj.DocuemntPaperType = dataSet.Tables[0].Rows[i]["LinkRecv"].ToString();
                        if (obj.RevedOption == "H")
                        {
                            obj.DepartmentName = dataSet.Tables[0].Rows[i]["DeptName"].ToString() + ", Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();
                        }
                        else
                        {
                            obj.DepartmentName = "OTHER , Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();

                        }

                        ListModel.Add(obj);
                    }
                }

                //List<eFileAttachment> ListModel2 = new List<eFileAttachment>();
                //ListModel2 =(List<eFileAttachment>)( ListModel2.OrderBy(a => a.eFileAttachmentId));

                model.eFileAttachment = ListModel;// (List<eFileAttachment>)(ListModel.OrderBy(a => a.CreatedDate)); ;

                var ListAss = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                ViewBag.AssemblyList = new SelectList(ListAss, "AssemblyCode", "AssemblyName", null);


            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return View("_eFileBookViewByIDPartialIndexDet", model);
            //return View("eFileIndex", model.eFileAttachment);
            //return View(model); _eFileBookViewByIDPartialIndexDet
        }

        public PartialViewResult GetCommitteeDetByCommID(int CommitteeId)
        {

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeDetByID", CommitteeId);


            return PartialView("_CommitteeDetailsByCommID", eList);

        }

        public JsonResult GetSessionssByAssemblyId(int AssemblyID)
        {
            List<SBL.DomainModel.Models.Session.mSession> SessLst = new List<SBL.DomainModel.Models.Session.mSession>();
            SBL.DomainModel.Models.Session.mSession model = new DomainModel.Models.Session.mSession();
            model.AssemblyID = AssemblyID;
            SessLst = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", model);

            return Json(SessLst, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSessionDatesByAssemblyIdSessId(int AssemblyID, int SessionID)
        {
            List<SBL.DomainModel.Models.Session.mSessionDate> SessDateLst = new List<SBL.DomainModel.Models.Session.mSessionDate>();
            SBL.DomainModel.Models.Session.mSessionDate mdl = new DomainModel.Models.Session.mSessionDate();
            List<SBL.DomainModel.Models.Session.mSessionDate> SessDateLst1 = new List<SBL.DomainModel.Models.Session.mSessionDate>();
            SBL.DomainModel.Models.Session.mSession model = new DomainModel.Models.Session.mSession();
            model.AssemblyID = AssemblyID;
            model.SessionCode = SessionID;
            SessDateLst = (List<SBL.DomainModel.Models.Session.mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", model);

            foreach (var item in SessDateLst)
            {
                mdl = new DomainModel.Models.Session.mSessionDate();
                mdl.Id = item.Id;
                mdl.SessionDate_Local = Convert.ToDateTime(item.SessionDate).ToString("dd/MM/yyyy");
                SessDateLst1.Add(mdl);
            }


            return Json(SessDateLst1, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SavePaperLaidInHouse(int AssemblyID, int SessionID, int SessionDateId, int eAttchId, int CommitteeId, string Subject, string eFileName, string eFilePath, string Description)
        {



            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            string directory = FileSettings.SettingValue;
            string SourcePath = directory + eFilePath;
            //obj.AssemblyId + "_" + obj.SessionId + "_" + "R" + "_" + tPaper.PaperLaidId + "_V" + model.Count + ext;

            string FilePathTarget = "/PaperLaid/" + CurrentSession.AssemblyId + "/" + CurrentSession.SessionId + "/";

            if (!System.IO.Directory.Exists(directory + FilePathTarget))
            {
                System.IO.Directory.CreateDirectory(directory + FilePathTarget + "/Signed/");
            }




            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@AssemblyId", AssemblyID.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionId", SessionID.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@SessionDateId", SessionDateId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@Title", Subject));
            methodParameter.Add(new KeyValuePair<string, string>("@DepartmentId", CurrentSession.DeptID));
            methodParameter.Add(new KeyValuePair<string, string>("@DepartmentName", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@Description", Description));
            methodParameter.Add(new KeyValuePair<string, string>("@EventId", "8"));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeChairmanMemberId", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", CommitteeId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@Version", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@FilePath", FilePathTarget));
            methodParameter.Add(new KeyValuePair<string, string>("@SignedFilePath", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@FileName", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@efileattachmentid", eAttchId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedByUserCode", CurrentSession.UserID.ToString()));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperLaidInHouse", methodParameter);
            string FileName = "";
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                FileName = dataSet.Tables[0].Rows[0][1].ToString();

            }


            string UrlTarget = "/PaperLaid/" + CurrentSession.AssemblyId + "/" + CurrentSession.SessionId + "/Signed/" + FileName;

            string TargetPath = directory + UrlTarget;

            System.IO.File.Copy(SourcePath, TargetPath, true);



            return Json("Paper Laid In the House Successfully", JsonRequestBehavior.AllowGet);
        }

        //Update eFile
        public ActionResult EditeFileDetails(int eFileId)
        {

            var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
            SBL.eLegistrator.HouseController.Web.Areas.eFile.Models.eFileViewModel model = new Models.eFileViewModel();
            model.DepartmentList = new SelectList(deptList, "deptId", "deptname", null);
            model.DocumentType = (List<SBL.DomainModel.Models.Document.mDocumentType>)Helper.ExecuteService("eFile", "eFileDocumentIndex", 1);
            //model.CommitteeId = CommitteeId;
            //EFile Type List  added on 27 July 2015
            model.eFileTypeList = (List<SBL.DomainModel.Models.eFile.eFileType>)Helper.ExecuteService("eFile", "eFileTypeDetails", null);
            ///********//

            model.eFileDetail = (SBL.DomainModel.Models.eFile.eFile)Helper.ExecuteService("eFile", "GeteFileDetailsByFileId", eFileId);
            model.eFileID = eFileId;
            model.eFileNumber = model.eFileDetail.eFileNumber;
            model.CommitteeId = model.eFileDetail.CommitteeId;
            model.eFileSubject = model.eFileDetail.eFileSubject;
            model.FromYear = model.eFileDetail.FromYear;
            model.ToYear = model.eFileDetail.ToYear;
            model.OldDescription = model.eFileDetail.OldDescription;
            model.NewDescription = model.eFileDetail.NewDescription;
            model.DepartmentId = model.eFileDetail.DepartmentId;
            model.Officecode = model.eFileDetail.Officecode;
            model.eFileTypeID = model.eFileDetail.eFileTypeID;
            model.SelectedDeptId = model.eFileDetail.SelectedDeptId;
            model.Mode = "Edit";
            ViewBag.OffICode = CurrentSession.OfficeId;
            return PartialView("_CreateeFile", model);
        }


        public PartialViewResult eFileListForCommitteeAttach()
        {
            ViewBag.Mode = "CommitteeAttach";
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[2];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            string[] str = new string[4];
            string[] strngBID = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[3] = value[0];

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            return PartialView("_eFileListViewCommitteeAttach", eList);

        }

        public PartialViewResult eFileListDetailsALLStatusToAttach(int pageId = 1, int pageSize = 25)
        {
            ViewBag.Mode = "Committee";
            ViewBag.CommAttach = "CommitteeAttach";
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[2];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            string[] str = new string[4];
            string[] strngBID = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[3] = value[0];

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////
            eList.eFileLists = pagedRecord;
            return PartialView("_eFileListViewAllCommitteeAttach", eList);

        }

        public ActionResult eFileIndexDetailsPaperLaid(eFileViewModel model)
        {
            try
            {
                eFileAttachment documents = new eFileAttachment();
                List<int> PDFPageNumbers = new List<int>();
                documents.eFileID = model.eFileID;
                documents.DepartmentId = CurrentSession.DeptID;
                int OfficeId = 0;
                int.TryParse(CurrentSession.OfficeId, out OfficeId);

                documents.OfficeCode = OfficeId;// Convert.ToInt32(CurrentSession.OfficeId);
                ViewBag.eFileID = model.eFileID;
                ViewBag.Subject = model.eFileSubject;
                ViewBag.eFileNum = model.eFileNumber;

                List<eFileAttachment> ListModel = new List<eFileAttachment>();
                eFileAttachment obj = new eFileAttachment();
                DataSet dataSet = new DataSet();
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@efileid", documents.eFileID.ToString()));
                methodParameter.Add(new KeyValuePair<string, string>("@DeptID", documents.DepartmentId));
                methodParameter.Add(new KeyValuePair<string, string>("@officecode", documents.OfficeCode.ToString()));

                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_GeteFileIndexData", methodParameter);
                if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        obj = new eFileAttachment();
                        obj.eFileAttachmentId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["efileattachmentid"]);
                        obj.PaperRefNo = dataSet.Tables[0].Rows[i]["PaperRefNo"].ToString();
                        obj.CreatedDate = Convert.ToDateTime(dataSet.Tables[0].Rows[i]["PDate"]);
                        obj.mode = dataSet.Tables[0].Rows[i]["PMode"].ToString();
                        obj.Title = dataSet.Tables[0].Rows[i]["Title"].ToString();
                        obj.eFilePath = dataSet.Tables[0].Rows[i]["efilepath"].ToString();
                        obj.LinkedRefNo = dataSet.Tables[0].Rows[i]["linkedrefno"].ToString();
                        obj.DepartmentId = dataSet.Tables[0].Rows[i]["deptid"].ToString();
                        obj.RecvDetails = dataSet.Tables[0].Rows[i]["details"].ToString();
                        obj.Name = dataSet.Tables[0].Rows[i]["FullSubject"].ToString();
                        obj.RevedOption = dataSet.Tables[0].Rows[i]["RevedOption"].ToString();
                        obj.PType = dataSet.Tables[0].Rows[i]["PType"].ToString();
                        obj.ReplyStatus = dataSet.Tables[0].Rows[i]["ReplyStatus"].ToString();
                        obj.eMode = Convert.ToInt32(dataSet.Tables[0].Rows[i]["emode"]);
                        obj.SendStatus = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["sendstatus"]);
                        obj.CommitteeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["CommitteeId"]);
                        obj.DocumentTypeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["DocumentTypeId"]);
                        obj.ReportLayingHouse = Convert.ToInt32(dataSet.Tables[0].Rows[i]["ReportLayingHouse"]);
                        obj.DocuemntPaperType = dataSet.Tables[0].Rows[i]["LinkRecv"].ToString();
                        if (obj.RevedOption == "H")
                        {
                            obj.DepartmentName = dataSet.Tables[0].Rows[i]["DeptName"].ToString() + ", Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();
                        }
                        else
                        {
                            obj.DepartmentName = "OTHER , Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();

                        }

                        ListModel.Add(obj);
                    }
                }



                model.eFileAttachment = ListModel;

                var ListAss = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                ViewBag.AssemblyList = new SelectList(ListAss, "AssemblyCode", "AssemblyName", null);


            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return View("_eFileIndexPaperLaid", model.eFileAttachment);
            //return View(model);
        }
        public ActionResult eFileIndexDetailsPaperLaid_OnlyLaidPapers(eFileViewModel model)
        {
            try
            {
                eFileAttachment documents = new eFileAttachment();
                List<int> PDFPageNumbers = new List<int>();
                documents.eFileID = model.eFileID;
                documents.DepartmentId = CurrentSession.DeptID;
                int OfficeId = 0;
                int.TryParse(CurrentSession.OfficeId, out OfficeId);
                documents.OfficeCode = OfficeId;// Convert.ToInt32(CurrentSession.OfficeId);
                ViewBag.eFileID = model.eFileID;
                ViewBag.Subject = model.eFileSubject;
                ViewBag.eFileNum = model.eFileNumber;

                List<eFileAttachment> ListModel = new List<eFileAttachment>();
                eFileAttachment obj = new eFileAttachment();
                DataSet dataSet = new DataSet();
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@efileid", documents.eFileID.ToString()));
                methodParameter.Add(new KeyValuePair<string, string>("@DeptID", documents.DepartmentId));
                methodParameter.Add(new KeyValuePair<string, string>("@officecode", documents.OfficeCode.ToString()));

                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_GeteFileIndexData_OnlyLaidPapers", methodParameter);
                if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        obj = new eFileAttachment();
                        obj.eFileAttachmentId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["efileattachmentid"]);
                        obj.PaperRefNo = dataSet.Tables[0].Rows[i]["PaperRefNo"].ToString();
                        obj.CreatedDate = Convert.ToDateTime(dataSet.Tables[0].Rows[i]["PDate"]);
                        obj.mode = dataSet.Tables[0].Rows[i]["PMode"].ToString();
                        obj.Title = dataSet.Tables[0].Rows[i]["Title"].ToString();
                        obj.eFilePath = dataSet.Tables[0].Rows[i]["efilepath"].ToString();
                        obj.LinkedRefNo = dataSet.Tables[0].Rows[i]["linkedrefno"].ToString();
                        obj.DepartmentId = dataSet.Tables[0].Rows[i]["deptid"].ToString();
                        obj.RecvDetails = dataSet.Tables[0].Rows[i]["details"].ToString();
                        obj.Name = dataSet.Tables[0].Rows[i]["FullSubject"].ToString();
                        obj.RevedOption = dataSet.Tables[0].Rows[i]["RevedOption"].ToString();
                        obj.PType = dataSet.Tables[0].Rows[i]["PType"].ToString();
                        obj.ReplyStatus = dataSet.Tables[0].Rows[i]["ReplyStatus"].ToString();
                        obj.eMode = Convert.ToInt32(dataSet.Tables[0].Rows[i]["emode"]);
                        obj.SendStatus = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["sendstatus"]);
                        obj.CommitteeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["CommitteeId"]);
                        obj.DocumentTypeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["DocumentTypeId"]);
                        obj.ReportLayingHouse = Convert.ToInt32(dataSet.Tables[0].Rows[i]["ReportLayingHouse"]);
                        obj.DocuemntPaperType = dataSet.Tables[0].Rows[i]["LinkRecv"].ToString();
                        if (obj.RevedOption == "H")
                        {
                            obj.DepartmentName = dataSet.Tables[0].Rows[i]["DeptName"].ToString() + ", Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();
                        }
                        else
                        {
                            obj.DepartmentName = "OTHER , Details : " + dataSet.Tables[0].Rows[i]["DeptDetails"].ToString();

                        }

                        ListModel.Add(obj);
                    }
                }



                model.eFileAttachment = ListModel;

                var ListAss = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                ViewBag.AssemblyList = new SelectList(ListAss, "AssemblyCode", "AssemblyName", null);
                ViewBag.SearchPaper = "L";

            }
            catch (Exception ex)
            {
                string value = ex.ToString();
            }

            return View("_eFileIndexPaperLaid", model.eFileAttachment);
            //return View(model);
        }

        #region BulkSMS
        public ActionResult CustomizeSMSEMAILS()
        {
            SBL.DomainModel.ComplexModel.OnlineMemberQmodel model = new SBL.DomainModel.ComplexModel.OnlineMemberQmodel();
            ViewBag.GenericOnly = "GENRICONLY";

            model.intMsgLimit = GetMaxLimitCountSMSByMember();
            model.intMsgLeft = model.intMsgLimit - GetSpentCountSMSByMember();

            return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_CustomizeSMSEMAIL.cshtml", model);
        }
        #endregion

        #region Committee System
        public ActionResult CommitteeMembers(int ID, string Name)
        {
            CommitteeMembersUpload model = new CommitteeMembersUpload();
            model.CommitteeId = ID;
            ViewBag.CommitteeName = Name;
            return PartialView("_CommitteeMembers", model);
        }
        [HttpPost]
        public JsonResult UploadCommitteeMemnbers(CommitteeMembersUpload model, HttpPostedFileBase file)
        {

            CommitteeMembersUpload mdl = new CommitteeMembersUpload();

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);
                        mdl.CommFileName = name;
                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                        if (!string.IsNullOrEmpty(name))
                        {
                            if (!string.IsNullOrEmpty(mdl.CommFilePath))
                                System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.CommFilePath));
                            System.IO.File.Copy(SourceFile, path, true);
                            mdl.CommFilePath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                        }

                    }

                }
                else
                {
                    return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                }

            }

            mdl.Remark = model.Remark;
            mdl.CommStatus = 1;

            mdl.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
            mdl.CreatedDate = DateTime.Now;
            mdl.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;
            mdl.IsDeleted = false;
            mdl.CommitteeId = model.CommitteeId;

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@CommFileName", mdl.CommFileName));
            methodParameter.Add(new KeyValuePair<string, string>("@CommFilePath", mdl.CommFilePath));
            methodParameter.Add(new KeyValuePair<string, string>("@Remark", mdl.Remark));
            methodParameter.Add(new KeyValuePair<string, string>("@CommStatus", mdl.CommStatus.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", mdl.CommitteeId.ToString()));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_CommitteeMembersUpload", methodParameter);

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                string res = dataSet.Tables[0].Rows[0][0].ToString();

                if (res == "Success")
                {
                    return Json("Pdf Uploaded Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Some problem occured. Please Try again", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("Some error1 found.Please Try again", JsonRequestBehavior.AllowGet);

        }

        public PartialViewResult UploadedCommitteeMemebersPdf(int CommitteeId, string CommitteeName)
        {
            CommitteeMembersUpload model = new CommitteeMembersUpload();
            List<CommitteeMembersUpload> listComm = new List<CommitteeMembersUpload>();
            model.CommitteeId = CommitteeId;
            ViewBag.CommitteeName = CommitteeName;

            listComm = (List<CommitteeMembersUpload>)Helper.ExecuteService("eFile", "GetCommitteeUploadedPdf", model);

            return PartialView("_CommMembersUploadList", listComm);

        }
        public PartialViewResult UploadedGeneratedPdf()
        {

            List<CommitteeMembersUpload> listComm = new List<CommitteeMembersUpload>();


            listComm = (List<CommitteeMembersUpload>)Helper.ExecuteService("eFile", "GetGeneratedPdfList", null);

            return PartialView("_CommMembersUploadList", listComm);

        }

        public ActionResult GetAllNewComitteeList()
        {
            CommitteeList comList = new CommitteeList();
            List<tCommitteeMemberList> committeMember = new List<tCommitteeMemberList>();
            try
            {

                //comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);


                comList.committeeMemberList = (List<tCommitteeMemberList>)Helper.ExecuteService("Committee", "GetAllCommitteeMembers", null);
                comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
                ViewBag.CommitteeList = comList.CommitteeView.Select(a => new SelectListItem { Text = a.CommitteeName, Value = a.CommitteeId.ToString() }).ToList();
                int mode = (int)Helper.ExecuteService("Committee", "CommitteeExisit", null);
                ViewBag.mode = mode;
                //var list=committeMember
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Committee Details";
                return View("AdminErrorPage");
                throw ex;
            }

            return PartialView("_GetAllNewCommitteeList", comList);
        }
        public static SelectList GetCommitteeYear()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Select Committee Year", Value = "0" });
            for (int i = 2000; i <= 2050; i++)
            {
                result.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });
            }
            return new SelectList(result, "Value", "Text");
        }
        public ActionResult CreateNewCommittee()
        {
            SBL.DomainModel.Models.Assembly.mAssembly Amodel = new DomainModel.Models.Assembly.mAssembly();
            var AssemblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyData", Amodel);
            var CommitteTypeList = (List<SBL.DomainModel.Models.Committee.mCommitteeType>)Helper.ExecuteService("Committee", "GetAllCommiteeType", null);
            var CommitteeList = (List<SBL.DomainModel.Models.Committee.CommitteeSearchModel>)Helper.ExecuteService("Committee", "GetCommittees", 2);
            SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models.CommitteeViewModel model = new CommitteeViewModel();
            model.Assembly = new SelectList(AssemblyList, "AssemblyID", "AssemblyName", null);
            model.CommitteeType = new SelectList(CommitteTypeList, "CommitteeTypeId", "CommitteeTypeName", null);
            model.ParentCommittee = new SelectList(CommitteeList, "CommitteeId", "CommitteeName", null);
            ViewBag.CommitteeYear = GetCommitteeYear();
            model.Mode = "Add";
            return PartialView("_CreateCommittee", model);
        }

        [HttpGet]
        public ActionResult EditNewCommittee(int Id)
        {
            SBL.DomainModel.Models.Assembly.mAssembly Amodel = new DomainModel.Models.Assembly.mAssembly();
            tCommittee committeeToEdit = (tCommittee)Helper.ExecuteService("Committee", "GetSubCommitteById", Id);
            var AssemblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyData", Amodel);
            var CommitteTypeList = (List<SBL.DomainModel.Models.Committee.mCommitteeType>)Helper.ExecuteService("Committee", "GetAllCommiteeType", null);
            var CommitteeList = (List<SBL.DomainModel.Models.Committee.CommitteeSearchModel>)Helper.ExecuteService("Committee", "GetCommittees", 2);
            //var CommitteeList = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("Committee", "GetCommittee", committeeToEdit);
            SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models.CommitteeViewModel model = new CommitteeViewModel();
            model.Assembly = new SelectList(AssemblyList, "AssemblyID", "AssemblyName", null);
            model.CommitteeType = new SelectList(CommitteTypeList, "CommitteeTypeId", "CommitteeTypeName", null);
            model.ParentCommittee = new SelectList(CommitteeList, "CommitteeId", "CommitteeName", null);
            model.CommitteeName = committeeToEdit.CommitteeName;
            //model.DateofCreation = committeeToEdit.DateOfCreation;
            model.Description = committeeToEdit.Remark;
            model.IsActive = committeeToEdit.IsActive;
            model.AssemblyID = committeeToEdit.AssemblyID;
            model.CommitteeTypeId = committeeToEdit.CommitteeTypeId;
            model.ParentId = committeeToEdit.ParentId;
            model.CommitteeYear = committeeToEdit.CommitteeYear;
            model.DateofCreation = committeeToEdit.DateOfCreation;
            model.CommitteeId = committeeToEdit.CommitteeId;
            ViewBag.CommitteeYear = GetCommitteeYear();
            model.Mode = "Edit";
            return PartialView("_CreateCommittee", model);
        }


        [HttpPost]
        public ActionResult EditSubCommittee(CommitteeViewModel model, string Depddlmm)
        {
            tCommittee mdl = new tCommittee();
            mdl.CommitteeId = model.CommitteeId;
            mdl.AssemblyID = model.AssemblyID;
            mdl.CommitteeTypeId = model.CommitteeTypeId;
            mdl.ParentId = 0;
            mdl.CommitteeName = model.CommitteeName;
            mdl.DateOfCreation = model.DateofCreation;
            mdl.IsActive = model.IsActive;
            mdl.AutoSchedule = model.AutoSchedule;
            mdl.Remark = model.Remark;
            mdl.CommitteeYear = Depddlmm;
            Helper.ExecuteService("Committee", "UpdateSubCommittee", mdl);

            return Json("Committee Updated Successfully", JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllMembers()
        {


            List<SBL.DomainModel.Models.Member.mMember> Members = new List<DomainModel.Models.Member.mMember>();
            SBL.DomainModel.Models.Member.mMemberAssembly model = new DomainModel.Models.Member.mMemberAssembly();
            model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            //var Members = (List<SBL.DomainModel.Models.Member.mMember>)Helper.ExecuteService("Ministry", "GetAllMembers", null);
            Members = (List<SBL.DomainModel.Models.Member.mMember>)Helper.ExecuteService("eFile", "GetMembersByAssemblyID", model);


            return Json(Members, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ShowCommitteMemberByCommitteeId(string value)
        {
            string[] p = new string[2];
            p[0] = value;
            p[1] = CurrentSession.AssemblyId;
            var Members = (List<tCommitteeMemberList>)Helper.ExecuteService("Committee", "ShowCommitteMemberByCommitteeId", p);
            return Json(Members, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllStaff()
        {
            var Staff = (List<SBL.DomainModel.Models.StaffManagement.mStaff>)Helper.ExecuteService("eFile", "GetStaffDetails", null);
            return Json(Staff, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CreateCommitteMembers(string values, string ChairmanId, string Mode, string CommitteeId, int ChairmanIdComm)
        {

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", CommitteeId));
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_DeleteCommitteeMembers", methodParameter);


            string[] words = values.Split(',');
            tCommitteeMember model = new tCommitteeMember();
            for (int count = 0; count < words.Length; count++)
            {
                var models = new tCommitteeMember
                {
                    MemberId = Convert.ToInt32(words[count]),
                    CommitteeId = Convert.ToInt32(CommitteeId),
                    //IsChairMan = false,
                    RelievedDate = DateTime.Now,
                    JoinedDate = DateTime.Now
                };

                if (ChairmanId == models.MemberId.ToString())
                {
                    models.IsReportChairMan = true;
                }
                else
                {
                    models.IsReportChairMan = false;
                }


                models.StaffID = ChairmanIdComm;
                Helper.ExecuteService("Committee", "AddCommitteeMembers", models);

            }
            return Json("Committee Member Added Successfully", JsonRequestBehavior.AllowGet);
        }




        [HttpPost]
        public JsonResult CreateCommitteeNew(CommitteeViewModel model, string Depddlmm)
        {
            tCommittee mdl = new tCommittee();
            mdl.CommitteeId = model.CommitteeId;
            mdl.AssemblyID = model.AssemblyID;
            mdl.CommitteeTypeId = model.CommitteeTypeId;
            mdl.ParentId = 0;
            mdl.CommitteeName = model.CommitteeName;
            mdl.DateOfCreation = model.DateofCreation;
            mdl.IsActive = model.IsActive;
            mdl.AutoSchedule = model.AutoSchedule;
            mdl.Remark = model.Remark;
            mdl.CommitteeYear = Depddlmm;

            //tCommittee committeeModel = model.ToDomainModel();
            //committeeModel.CommitteeYear = Depddlmm;
            Helper.ExecuteService("Committee", "CreateCommittee", mdl);
            return Json("Committee Created Successfully", JsonRequestBehavior.AllowGet);


            //return RedirectToAction("GetAllNewComittee", "Committee");
        }

        //public static tCommittee ToDomainModel(this CommitteeViewModel model)
        //{
        //	return new tCommittee
        //	{
        //		CommitteeId = Convert.ToInt32(model.CommitteeId),
        //		AssemblyID = model.AssemblyID,
        //		CommitteeTypeId = model.CommitteeTypeId,
        //		ParentId = model.ParentId,
        //		CommitteeName = model.CommitteeName,
        //		DateOfCreation = model.DateofCreation,
        //		IsActive = model.IsActive,
        //		AutoSchedule = model.AutoSchedule,
        //		Remark = model.Remark,
        //		CommitteeYear = model.CommitteeYear
        //	};
        //}

        public ActionResult CommitteeProceeding(int pageId = 1, int pageSize = 25)
        {
            COmmitteeProceeding mdl = new COmmitteeProceeding();
            //   mdl.DeptID = CurrentSession.DeptID;
            string UserID = CurrentSession.UserID;
            //mdl.OfficeCode = 0;
            var Members = (List<COmmitteeProceeding>)Helper.ExecuteService("eFile", "GetCommitteeProoceedingByDept", UserID);

            ////////////////////////////Server Side Paging/////////////////////////////////// int pageId = 1, int pageSize = 25
            ViewBag.PageSize = pageSize;
            List<COmmitteeProceeding> pagedRecord = new List<COmmitteeProceeding>();
            if (pageId == 1)
            {
                pagedRecord = Members.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = Members.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(Members.Count, pageId, pageSize);
            ////////////////////////////Server Side Paging///////////////////////////////////
            return PartialView("_CommitteeProceeding", pagedRecord);
        }

        public ActionResult GetMemberPDFList(int CommitteeID)
        {
            CommitteeMembersUpload model = new CommitteeMembersUpload();
            model = (CommitteeMembersUpload)Helper.ExecuteService("Committee", "GetCommitteeMemberPDFListByCommitteeId", CommitteeID);
            return PartialView("_GetMemberPDFList", model);
        }

        public JsonResult DeleteGeneratedMemberPDF(int ID)
        {
            int res = (int)Helper.ExecuteService("Committee", "DeleteGeneratedMemberPDfByID", ID);

            if (res == 1)
            {
                return Json("Deleted Successfully", JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json("Some Problem Occured . Please try again", JsonRequestBehavior.AllowGet);

            }

        }

        public JsonResult MakeActivePDF(int ID)
        {
            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@CommUploadID", ID.ToString()));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_CommitteeMembersUpload_MakeActiveCurrent", methodParameter);
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                string res = dataSet.Tables[0].Rows[0][0].ToString();

                if (res == "Success")
                {
                    return Json("Activate Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Some problem occured. Please Try again", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("Some error1 found.Please Try again", JsonRequestBehavior.AllowGet);

        }





        #endregion

        #region Replace Committee Paper Laid

        public ActionResult GetOtherPaperLaidSubmitListGridCommitteeNew(int? QNumber, int? Bname)
        {
            tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

            PaperMovementModel objPaperModel = new PaperMovementModel();

            //added code venkat for dynamic 
            objPaperModel.QuestionNumber = QNumber ?? 0;
            objPaperModel.EventId = Bname ?? 0;
            //end------------------------------------
            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                tPaperLaidDepartmentModel.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", tPaperLaidDepartmentModel);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    objPaperModel.DepartmentId = item.AssociatedDepts;
                }

            }
            else
            {
                objPaperModel.DepartmentId = CurrentSession.DeptID;
            }

            /*End*/

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                tPaperLaidDepartmentModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                objPaperModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                tPaperLaidDepartmentModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                objPaperModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            var result = (List<PaperMovementModel>)Helper.ExecuteService("OtherPaperLaid", "GetSubmittedOtherPaperLaidListCommittee", objPaperModel);

            return PartialView("_GetCommitteePaperLaidList", result);

        }



        public ActionResult ShowOtherPaperLaidDetailByIDCommittee(string paperLaidId, string ReferenceNo)
        {
            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByIDCommittee", movementModel);
                movementModel.evidhanReferenceNumber = ReferenceNo;
                return PartialView("_GetOtherPaperLaidSubmittedListByIdCommittee", movementModel);
            }
            return null;
        }
        public ActionResult GetSubmittedOtherPaperLaidByIDCommittee(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByIDCommittee", movementModel);
                return PartialView("_GetSubmittedOtherPaperLaidByIDCommittee", movementModel);
            }
            return null;
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateOtherPaperLaidFileCommittee(HttpPostedFileBase file, tPaperLaidV tPaper)
        {
            if (CurrentSession.UserID != "")
            {
                tPaperLaidTemp tPaperTemp = new tPaperLaidTemp();
                PaperMovementModel obj = new PaperMovementModel();
                obj.PaperLaidId = tPaper.PaperLaidId;
                obj.actualFilePath = tPaper.FilePath;
                tPaperLaidV model = new tPaperLaidV();
                tPaperLaidV siteSetting = new tPaperLaidV();
                siteSetting = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", siteSetting);

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);

                }


                model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidFileVersionCommittee", tPaper);
                if (model != null)
                {

                    if (file == null)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        //string url = "/ePaper";

                        string FilePathTarget = "/PaperLaid/" + CurrentSession.AssemblyId + "/" + CurrentSession.SessionId + "/";
                        string directory = FileSettings.SettingValue;
                        if (!System.IO.Directory.Exists(directory + FilePathTarget))
                        {
                            System.IO.Directory.CreateDirectory(directory + FilePathTarget + "/Signed/");
                        }




                        Guid FileName = Guid.NewGuid();
                        string name = "";
                        //if (!System.IO.Directory.Exists(directory))
                        //{
                        //	System.IO.Directory.CreateDirectory(directory);
                        //}
                        if (Directory.Exists(directory))
                        {
                            string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                            string SourceFile = savedFileName[0];
                            foreach (string page in savedFileName)
                            {

                                name = Path.GetFileName(page);
                                string nameKey = Path.GetFileNameWithoutExtension(page);
                                string directory1 = Path.GetDirectoryName(page);
                                //
                                // Display the Path strings we extracted.
                                //
                                Console.WriteLine("{0}, {1}, {2}, {3}",
                                page, name, nameKey, directory1);
                                tPaperTemp.FileName = name;
                            }
                            string ext = Path.GetExtension(tPaperTemp.FileName);
                            string fileNam1 = tPaperTemp.FileName.Replace(ext, "");



                            model.Count = model.Count;

                            string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "R" + "_" + tPaper.PaperLaidId + "_V" + model.Count + ext;

                            string UrlTarget = "/PaperLaid/" + CurrentSession.AssemblyId + "/" + CurrentSession.SessionId + "/Signed/" + actualFileName;

                            string TargetPath = directory + UrlTarget;
                            //DirectoryInfo Dir = new DirectoryInfo(FilePathTarget);
                            //if (!Dir.Exists)
                            //{
                            //	Dir.Create();
                            //}

                            /////string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));

                            //string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);

                            System.IO.File.Copy(SourceFile, TargetPath, true);
                            tPaperTemp.FilePath = FilePathTarget;   // "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                            tPaperTemp.SignedFilePath = UrlTarget;
                            tPaperTemp.FileName = actualFileName;


                            tPaperTemp.PaperLaidId = tPaper.PaperLaidId;

                            tPaperTemp.Version = model.Count;

                        }


                        tPaperTemp.Version = model.Count;

                    }

                    DataSet dataSet = new DataSet();
                    var methodParameter = new List<KeyValuePair<string, string>>();

                    methodParameter.Add(new KeyValuePair<string, string>("@PaperLaidID", tPaperTemp.PaperLaidId.ToString()));
                    methodParameter.Add(new KeyValuePair<string, string>("@AssemblyId", CurrentSession.AssemblyId));
                    methodParameter.Add(new KeyValuePair<string, string>("@SessionId", CurrentSession.SessionId));
                    methodParameter.Add(new KeyValuePair<string, string>("@ModifiedByUserCode", CurrentSession.UserID));
                    methodParameter.Add(new KeyValuePair<string, string>("@Version", tPaperTemp.Version.ToString()));
                    methodParameter.Add(new KeyValuePair<string, string>("@FilePath", tPaperTemp.FilePath));
                    methodParameter.Add(new KeyValuePair<string, string>("@SignedFilePath", tPaperTemp.SignedFilePath));
                    methodParameter.Add(new KeyValuePair<string, string>("@FileName", tPaperTemp.FileName));

                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PaperLaidInHouse_Replace", methodParameter);


                }
            }


            if (Request.IsAjaxRequest())
            {
                tPaperLaidV mod = new tPaperLaidV();
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(tPaper.PaperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByIDCommittee", movementModel);
                return PartialView("/Areas/eFile/Views/eFile/_ShowPrevReplaceLaidPaper.cshtml", movementModel);


                //return Json("Uploaded Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {

            }
            return null;


        }
        #endregion


        #region Pendency Report
        public ActionResult eFilePendencyReport()
        {
            eFileAttachments model = new eFileAttachments();


            CommitteeList comList = new CommitteeList();
            comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            ViewBag.CommitteeList = comList.CommitteeView.Select(a => new SelectListItem { Text = a.CommitteeName, Value = a.CommitteeId.ToString() }).ToList();

            return PartialView("_eFilePendencyReport", model);

        }

        public ActionResult eFilePendencyReportList(string Year, string CommitteeId, string eFileId, string eFileStatus, string PaperOption, string PendingOnDate, string PaperNature, int pageId = 1, int pageSize = 25)
        {
            eFileAttachments model = new eFileAttachments();
            List<eFileAttachment> mdlList = new List<eFileAttachment>();
            ViewBag.Paper = PaperOption;
            eFileAttachment obj;
            string dt = "";
            if (string.IsNullOrEmpty(PendingOnDate))
            {
                dt = DateTime.Today.ToString("dd/MM/yyyy");
            }
            else
            {
                dt = PendingOnDate;
            }

            //if (string.IsNullOrEmpty(CommitteeId))
            //{
            //	eFileId = "All";
            //	eFileStatus = "All";
            //	CommitteeId = "All";
            //}
            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@Year", Year));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", CommitteeId));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileId", eFileId));
            methodParameter.Add(new KeyValuePair<string, string>("@eFileStatus", eFileStatus));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperOption", PaperOption));
            methodParameter.Add(new KeyValuePair<string, string>("@PendingAsOnDate", dt));
            methodParameter.Add(new KeyValuePair<string, string>("@DeptId", CurrentSession.DeptID));
            methodParameter.Add(new KeyValuePair<string, string>("@officecode", CurrentSession.OfficeId));
            methodParameter.Add(new KeyValuePair<string, string>("@PaperNature", PaperNature));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_PendencyReport", methodParameter);

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    obj = new eFileAttachment();
                    obj.eFileAttachmentId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["efileattachmentid"]);
                    obj.PaperRefNo = dataSet.Tables[0].Rows[i]["PaperRefNo"].ToString();
                    obj.CreatedDate = Convert.ToDateTime(dataSet.Tables[0].Rows[i]["CreatedDate"]);
                    obj.mode = dataSet.Tables[0].Rows[i]["PMode"].ToString();
                    obj.Title = dataSet.Tables[0].Rows[i]["Title"].ToString();
                    obj.eFilePath = dataSet.Tables[0].Rows[i]["efilepath"].ToString();
                    obj.LinkedRefNo = dataSet.Tables[0].Rows[i]["linkedrefno"].ToString();
                    obj.DepartmentId = dataSet.Tables[0].Rows[i]["DepartmentId"].ToString();
                    obj.RecvDetails = dataSet.Tables[0].Rows[i]["RecvDetails"].ToString();
                    obj.RevedOption = dataSet.Tables[0].Rows[i]["RevedOption"].ToString();
                    obj.PType = dataSet.Tables[0].Rows[i]["PType"].ToString();
                    obj.eMode = Convert.ToInt32(dataSet.Tables[0].Rows[i]["emode"]);
                    obj.SendStatus = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["sendstatus"]);
                    obj.CommitteeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["CommitteeId"]);
                    obj.DocumentTypeId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["DocumentTypeId"]);
                    obj.ReportLayingHouse = Convert.ToInt32(dataSet.Tables[0].Rows[i]["ReportLayingHouse"]);
                    obj.DeptAbbr = dataSet.Tables[0].Rows[i]["deptabbr"].ToString();
                    obj.eFilePathWord = dataSet.Tables[0].Rows[i]["eFilePathWord"].ToString();
                    obj.RecvdPaperNature = dataSet.Tables[0].Rows[i]["PaperNatureType"].ToString();
                    obj.PaperTicketValue = dataSet.Tables[0].Rows[i]["PendingDay"].ToString();
                    obj.PaperNo = dataSet.Tables[0].Rows[i]["eFileNumber"].ToString();
                    obj.DispatchtoWhom = dataSet.Tables[0].Rows[i]["CommitteeName"].ToString();
                    obj.ReplyStatus = dataSet.Tables[0].Rows[i]["ReplyStatus"].ToString();
                    obj.ReplyDate = Convert.ToDateTime(dataSet.Tables[0].Rows[i]["ReplyDate"]);

                    if (obj.RevedOption == "H")
                    {
                        obj.DepartmentName = dataSet.Tables[0].Rows[i]["DeptName"].ToString() + ", Details : " + dataSet.Tables[0].Rows[i]["RecvDetails"].ToString();
                    }
                    else
                    {
                        obj.DepartmentName = "OTHER";

                    }

                    mdlList.Add(obj);
                }
            }

            model.eFileAttachemts = mdlList;
            //////////////////////////Server Side Paging///////////////////////////////////
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = model.eFileAttachemts.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = model.eFileAttachemts.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(model.eFileAttachemts.Count, pageId, pageSize);
            //////////////////////////Server Side Paging///////////////////////////////////


            return PartialView("_eFilePendencyReportList", model);

        }

        public JsonResult GeteFileByCommitteeId(int CommitteeId)
        {
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);

            return Json(eList.eFileLists, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion Committee Code By Shashi

        #region Generate Pdf Runtime and save on folder
        public JsonResult GeneratePDFCommitteeMembers()
        {

            //string Result = string.Empty;

            //Result = PDFFormat();

            //Result = HttpUtility.UrlDecode(Result);
            //Response.Clear();
            //Response.AddHeader("content-disposition", "attachment;filename=CommitteeMembers.pdf");
            //Response.Charset = "";
            //Response.ContentType = "application/pdf";
            //Response.Write(Result);
            //Response.Flush();
            //Response.End();
            ////return new EmptyResult();
            MemoryStream output = new MemoryStream();
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = PDFFormat();

            string htmlStringToConvert = outXml;

            HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

            AddElementResult addResult = page.AddElement(htmlToPdfElement);

            byte[] pdfBytes = document1.Save();

            output.Write(pdfBytes, 0, pdfBytes.Length);

            output.Position = 0;

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;
            //string url = "/ePaper/" + "MemberTourPdf/";

            //string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            Guid id = Guid.NewGuid();
            string name = "CommitteeMembers";

            //string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + tourTitle + ".pdf");
            string path = System.IO.Path.Combine(directory, id + "_" + name + ".pdf");
            string FilePath = "/ePaper/" + id + "_" + name + ".pdf";

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();

            DataSet dataSet = new DataSet();


            CommitteeMembersUpload mdl = new CommitteeMembersUpload();
           
            mdl.CommFileName = "CommitteeMembers";
            mdl.CommFilePath = FilePath;
            mdl.Remark = "";
            mdl.CommStatus = 1;
            mdl.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
          
            mdl.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
            mdl.ModifiedDate = DateTime.Now;

            mdl.CommitteeId = 0 ;


            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@CommFileName", mdl.CommFileName));
            methodParameter.Add(new KeyValuePair<string, string>("@CommFilePath", mdl.CommFilePath));
            methodParameter.Add(new KeyValuePair<string, string>("@Remark", mdl.Remark));
            methodParameter.Add(new KeyValuePair<string, string>("@CommStatus", mdl.CommStatus.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", mdl.CreatedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModifiedBy", mdl.ModifiedBy.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@CommitteeId", mdl.CommitteeId.ToString()));




           

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_CommitteeMembersUpload", methodParameter);

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                string res = dataSet.Tables[0].Rows[0][0].ToString();

                if (res == "Success")
                {
                    return Json("Pdf Uploaded Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Some problem occured. Please Try again", JsonRequestBehavior.AllowGet);
                }
            }


            return Json("PDF Generated Successfully", JsonRequestBehavior.AllowGet);
        }

        public static string PDFFormat()
        {
            SBL.DomainModel.Models.SiteSetting.SiteSettings steSett1 = new SiteSettings();
            SBL.DomainModel.Models.SiteSetting.SiteSettings steSett2 = new SiteSettings();
            SBL.DomainModel.Models.SiteSetting.SiteSettings steSett3 = new SiteSettings();
            var FileSettings = (List<SBL.DomainModel.Models.SiteSetting.SiteSettings>)Helper.ExecuteService("SiteSetting", "GetCommitteeMemberPDFSetting", null);

            steSett1 = FileSettings.Single(s => s.SettingValue == "Heading1");
            steSett2 = FileSettings.Single(s => s.SettingValue == "Heading2");
            steSett3 = FileSettings.Single(s => s.SettingValue == "Heading3");

            StringBuilder ReportList = new StringBuilder();
            ReportList.Append("<body>");
            ReportList.Append("<div style='font-family:Calibri;font-size:17px;width:96%;'>");
            //ReportList.Append("<center>( हिमाचल प्रदेश सरकार के असाधारण राजपत्र में प्रकाशित किया जायेगा )</center>");
            //ReportList.Append("<center><h2>हिमाचल प्रदेश बारहवीं विधान सभा </h2></center>");
            ReportList.Append("<center><h1>" + steSett1.Description + " </h1></center>");
            //ReportList.Append("<center><h4>अधिसूचना </h4></center>");
            //	ReportList.Append("<center><h5>सं0: वि0स0 -विधायन -सिमित गठन/1-13/2013   दिनांक , 30 अप्रैल, 2015 </h5></center>");
            ReportList.Append("<center><h3> " + steSett2.Description + "</h3></center>");
            //ReportList.Append("<center><p style='width:60%;'>हिमाचल प्रदेश विधान  सभा की प्रक्रिया एवं कार्य संचालन नियमावली , 1973 के नियम , 209 और 211 के अनुसरण में माननीय अध्यक्ष महोदय ने वर्ष 2015-16 के लिए गठित सदन की निम्न समितियों हेतु सदस्यों को सभापति तथा सदस्य नामांकित किया है:-</p></center>");

            ReportList.Append("<center><p style='width:60%;'>" + steSett3.Description + "</p></center>");

            ReportList.Append("<br/><br/>");

            ReportList.Append("<span style='float:right;'><b>आयुक्त,</b></span> <br/>");
            ReportList.Append("<span style='float:right;'>नगर निगम शिमला |</span>");

            //GetAllCommittee
            List<CommitteesListView> modelList = new List<CommitteesListView>();
            modelList = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommittee", null);
            int MNo = 0;
            foreach (var item in modelList)
            {
                MNo++;
                List<tCommitteeMemberList> mdlList = new List<tCommitteeMemberList>();
                mdlList = (List<tCommitteeMemberList>)Helper.ExecuteService("Committee", "GetAllMembersByCommitteeIdPDF", item.CommitteeId.ToString());


                ReportList.Append("<center><h3>" + MNo + " <u>" + item.CommitteeNameLocal + " </u> </h3></center>");


                int No = 0;
                ReportList.Append("<center><table style='font-family:Calibri;font-size:19px;'>");
                foreach (var item1 in mdlList)
                {
                    No++;
                    if (item1.IsChairMan == true)
                    {
                        ReportList.Append("<tr>");
                        ReportList.Append("<td> <b>" + No + ".&nbsp;&nbsp;</b></td> <td><b>" + item1.CommitteMemberName + "</b>&nbsp;&nbsp; </td> <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td><td><b>माO महापौर-सभापति</b></td>");
                        ReportList.Append("</tr>");
                    }
                    else
                    {
                        ReportList.Append("<tr>");
                        ReportList.Append("<td>" + No + ".&nbsp;&nbsp;</td> <td>" + item1.CommitteMemberName + "&nbsp;&nbsp; </td> <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td><td> माO सदस्य </td>");
                        ReportList.Append("</tr>");
                    }

                }
                ReportList.Append("</table></center>");
            }


           
            ReportList.Append("</div>");
            ReportList.Append("</body>");
            return ReportList.ToString();
        }

        public ActionResult GetCommitteePDFSettingsList()
        {
            List<SiteSettings> mdlList = new List<SiteSettings>();
            mdlList = (List<SBL.DomainModel.Models.SiteSetting.SiteSettings>)Helper.ExecuteService("SiteSetting", "GetCommitteeMemberPDFSetting", null);
            return PartialView("_CommitteeMemberPDFSettingsList", mdlList);
        }
        public ActionResult GetEditSiteSetting(int SettingId, string SettingName, string SettingValue, string Description)
        {
            SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models.SiteSettingViewModel mdl = new SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models.SiteSettingViewModel();
            mdl.SettingId = SettingId;
            mdl.SettingName = SettingName;
            mdl.SettingValue = SettingValue;
            mdl.Description = Description;

            mdl.Mode = "Update";
            return PartialView("_CommitteeMemberPDFSettings", mdl);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveSiteSettingsCommittee(SiteSettingViewModel model)
        {

            try
            {

                if (ModelState.IsValid)
                {

                    SiteSettings mdlSett = new SiteSettings();
                    mdlSett.SettingId = model.SettingId;
                    mdlSett.SettingName = model.SettingName;
                    mdlSett.SettingValue = model.SettingValue;
                    mdlSett.Description = model.Description;
                    mdlSett.IsActive = true;
                    mdlSett.CreatedBy = CurrentSession.UserName;
                    mdlSett.ModifiedBy = CurrentSession.UserName;

                    //if (model.Mode == "Add")
                    //{

                    //	Helper.ExecuteService("SiteSetting", "CreateSiteSettings", mdlSett);
                    //}
                    //else
                    //{

                    Helper.ExecuteService("SiteSetting", "UpdateSiteSettings", mdlSett);
                    //}
                    return Json("Settings Updated Successfully", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Error found", JsonRequestBehavior.AllowGet);
                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("There is a Error While Saving Site Setting Details", JsonRequestBehavior.AllowGet);

            }
        }

        #endregion

        //ashwani
        public ActionResult GetValuesByRefID(string _ReferenceNo)
        {
            eFileAttachment documents = new eFileAttachment();
            var _ObjeFileAttachment = (eFileAttachment)Helper.ExecuteService("eFile", "GetValuesByRefID", _ReferenceNo);
            return Json(_ObjeFileAttachment, JsonRequestBehavior.AllowGet);
        }


        #region UploadFileAsyn
        [HttpPost]
        public ContentResult UploadFiles()
        {
            string url = "~/ePaper/TempFile";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/ePaper/TempFile"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }
            CurrentSession.IsUploadPdfFile = "Y";
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }

        public JsonResult RemovePDFFiles()
        {
            string url = "~/ePaper/TempFile";
            string directory = Server.MapPath(url);

            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }
            CurrentSession.IsUploadPdfFile = "N";
            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ContentResult UploadFilesWord()
        {
            string url = "~/ePaper/FileWord";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/ePaper/FileWord"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }
            CurrentSession.IsUploadWordFile = "Y";


            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }

        [HttpPost]
        public ContentResult UploadFilesWordNew()
        {

            string url = "~/ePaper/";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/ePaper/"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }
            CurrentSession.IsUploadWordFile = "Y";

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }

        public JsonResult RemovePDFFilesWord()
        {

            string url = "~/ePaper/FileWord";
            string directory = Server.MapPath(url);

            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }
            CurrentSession.IsUploadWordFile = "N";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveWordFilesNew()
        {
            string url = "~/ePaper/";
            string directory = Server.MapPath(url);

            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }
            CurrentSession.IsUploadWordFile = "N";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        //Upload Mla Diary Forward File

        public ContentResult UploadMlaForwardFiles()
        {
            string url = "~/MlaDiary/TempFile";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/MlaDiary/TempFile"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }
            CurrentSession.IsUploadPdfFile = "Y";
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }
        public ContentResult UploadMlaActionFiles()
        {
            string url = "~/MlaDiary/ActionTempFile";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/MlaDiary/ActionTempFile"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }
            CurrentSession.IsUploadPdfFile = "Y";
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }

        //public ContentResult UploadMlaActionFiles()
        //{
        //    string url = "~/MlaDiary/ActionTempFile";
        //    string directory = Server.MapPath(url);
        //    if (!System.IO.Directory.Exists(directory))
        //    {
        //        System.IO.Directory.CreateDirectory(directory);
        //    }
        //    //  if (Directory.Exists(directory))
        //    // {
        //    string[] filePaths = Directory.GetFiles(directory);
        //    //  foreach (string filePath in filePaths)
        //    //  {
        //    //  System.IO.File.Delete(filePath);
        //    // }
        //    //  }
        //    var r = new List<eFileAttachment>();
        //    var NFileName1 = "";
        //    foreach (string file in Request.Files)
        //    {
        //        HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
        //        if (hpf.ContentLength == 0)
        //            continue;
        //        var fName1 = Path.GetFileName(hpf.FileName);
        //        string ext = Path.GetExtension(hpf.FileName);
        //        NFileName1 = Guid.NewGuid().ToString() + ext;

        //        string savedFileName = Path.Combine(Server.MapPath("~/MlaDiary/ActionTempFile"), NFileName1);
        //        hpf.SaveAs(savedFileName);

        //        r.Add(new eFileAttachment()
        //        {
        //            Name = hpf.FileName,
        //            Length = hpf.ContentLength,
        //            Type = hpf.ContentType,
        //        });
        //    }
        //    CurrentSession.IsUploadPdfFile = "Y";
        //    // return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        //    return Content("{\"name\":\"" + NFileName1 + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");

        //}



        public ContentResult UploadMlaEnclosureFiles()
        {
            string url = "~/MlaDiary/TempEnclosureFile";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
           // if (Directory.Exists(directory))
           // {
                string[] filePaths = Directory.GetFiles(directory);
               // foreach (string filePath in filePaths)
                //{
                  //  System.IO.File.Delete(filePath);
                //}
           // }

            var r = new List<eFileAttachment>();
            var NFileName = "";
            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                var fileName1 = Path.GetFileName(hpf.FileName);
                string ext = Path.GetExtension(hpf.FileName);
                NFileName = Guid.NewGuid().ToString() + ext;

                string savedFileName = Path.Combine(Server.MapPath("~/MlaDiary/TempEnclosureFile"), NFileName);
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }
            CurrentSession.IsUploadPdfFile = "Y";
          //  return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
            return Content("{\"name\":\"" + NFileName + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
     
        
        }

        public JsonResult RemoveMlaForwardFiles()
        {
            string url = "~/MlaDiary/TempFile";
            string directory = Server.MapPath(url);

            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }
            // CurrentSession.IsUploadPdfFile = "N";
            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveMlaActionFiles()
        {
            string url = "~/MlaDiary/ActionTempFile";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }
            //CurrentSession.IsUploadPdfFile = "N";
            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveActionEnclosureFiles()
        {
            string url = "~/MlaDiary/TempEnclosureFile";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }
            //CurrentSession.IsUploadPdfFile = "N";
            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        #endregion


        public ActionResult GeteBookData(string URL)
        {
            return PartialView("_eBookByID", URL);
        }

        public ActionResult _eFileViewByIDAttached(eFileViewModel model)
        {
            eFileLogin();
            try
            {
                eFileViewModel model2 = new eFileViewModel();

                //model2.DepartmentName = model.DepartmentName;
                //line for office
                model2.eFileNumber = model.eFileNumber;
                model2.eFileSubject = model.eFileSubject;
                model2.eFileID = model.eFileID;
                model2.eFileDetail = (SBL.DomainModel.Models.eFile.eFile)Helper.ExecuteService("eFile", "GeteFileDetailsByFileId", model.eFileID);
                model2.DepartmentName = model2.eFileDetail.DepartmentName;
                model2.NewDescription = model2.eFileDetail.NewDescription;
                model2.DepartmentId = model2.eFileDetail.DepartmentId;
                model2.Officecode = model2.eFileDetail.Officecode;
                ViewBag.Attached = "Y";

                //var ListAss = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                //ViewBag.AssemblyList = new SelectList(ListAss, "AssemblyCode", "AssemblyName", null);


                return View("_eFileViewByID", model2);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public PartialViewResult GetPaperListByRefNo(string PaperRefNo)
        {
            eFileLogin();
            eFileAttachments model = new eFileAttachments();

            string RefNo = PaperRefNo; //"HPD0001";

            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetPaperDetailsByRefNo", RefNo);
            model.eFileAttachemts = ListModel;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[2];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            string[] str = new string[4];
            string[] strngBID = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[3] = value[0];

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);

            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }

            //SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            //int CommitteId = 0;
            //eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteId);

            model.eFileLists = eList.eFileLists;

            return PartialView("_eFileViewByRefNo", model);
        }


        #region ServerSidePaging
        public PartialViewResult GetSendPaperByPaging(int pageId = 1, int pageSize = 25)
        {
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[2];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            string[] str = new string[4];
            string[] strngBID = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[3] = value[0];

            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendPaperDetailsByDeptId", str);
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            eFileAttachments model = new eFileAttachments();
            model.eFileAttachemts = pagedRecord;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }

            model.eFileLists = eList.eFileLists;


            return PartialView("_sendPaperList", model);
        }
        public PartialViewResult GetSavedPaperByPaging(int pageId = 1, int pageSize = 25)
        {
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[2];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            string[] str = new string[4];
            string[] strngBID = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[3] = value[0];

            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptId", str);
            ViewBag.PageSize = pageSize;
            List<eFileAttachment> pagedRecord = new List<eFileAttachment>();
            if (pageId == 1)
            {
                pagedRecord = ListModel.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ListModel.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ListModel.Count, pageId, pageSize);
            eFileAttachments model = new eFileAttachments();
            model.eFileAttachemts = pagedRecord;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            var eFileList = eList.eFileLists;
            if (eFileList.Count > 0)
            {
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                ViewBag.EFilesListAll = null;
            }
            model.eFileLists = eList.eFileLists;

            return PartialView("_receivePaperList", model);
        }
        public PartialViewResult GeteFileByPaging(int pageId = 1, int pageSize = 25)
        {
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[2];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            string[] str = new string[4];
            string[] strngBID = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[3] = value[0];

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            eFileAttachments model = new eFileAttachments();
            eList.eFileLists = pagedRecord;


            return PartialView("_eFileListViewAll", eList);
        }
        public PartialViewResult GeteFileByPagingCommittee(int CommitteeId, int pageId = 1, int pageSize = 25)
        {

            tCommittee mdl = new tCommittee();
            mdl = (tCommittee)Helper.ExecuteService("Committee", "GetCommitteeDetailsByCommitteeId", CommitteeId);

            ViewBag.CommName = mdl.CommitteeName;
            ViewBag.CommYear = mdl.CommitteeYear;

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFile", CommitteeId);
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            eFileAttachments model = new eFileAttachments();
            eList.eFileLists = pagedRecord;
            ViewBag.Mode = "Committee";


            return PartialView("_eFileListViewAll", eList);
        }
        public PartialViewResult GeteFileByPagingAttach(int pageId = 1, int pageSize = 25)
        {
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[2];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            string[] str = new string[4];
            string[] strngBID = new string[2];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = CurrentSession.BranchId;
            strngBID[0] = CurrentSession.BranchId;
            var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
            str[3] = value[0];

            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileList", str);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            ViewBag.PageSize = pageSize;
            List<eFileList> pagedRecord = new List<eFileList>();
            if (pageId == 1)
            {
                pagedRecord = eList.eFileLists.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
            eFileAttachments model = new eFileAttachments();
            eList.eFileLists = pagedRecord;
            ViewBag.Mode = "Committee";
            ViewBag.CommAttach = "CommitteeAttach";

            return PartialView("_eFileListViewAllCommitteeAttach", eList);
        }

        public ActionResult GetCommitteeProceedingPaging(int pageId = 1, int pageSize = 25)
        {
            COmmitteeProceeding mdl = new COmmitteeProceeding();
            //  mdl.DeptID = CurrentSession.DeptID;
            string UserID = CurrentSession.UserID;
            //mdl.OfficeCode = 0;
            var Members = (List<COmmitteeProceeding>)Helper.ExecuteService("eFile", "GetCommitteeProoceedingByDept", UserID);

            ////////////////////////////Server Side Paging/////////////////////////////////// int pageId = 1, int pageSize = 25
            ViewBag.PageSize = pageSize;
            List<COmmitteeProceeding> pagedRecord = new List<COmmitteeProceeding>();
            if (pageId == 1)
            {
                pagedRecord = Members.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = Members.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(Members.Count, pageId, pageSize);
            ////////////////////////////Server Side Paging///////////////////////////////////
            return PartialView("_NewCommitteeProceeding", pagedRecord); //
        }
        #endregion

        public JsonResult GetTreeViewJSON_DATA()
        {
            TreeViewStructure objTreeViewStructure = new TreeViewStructure();
            var Result = (List<TreeViewStructure>)Helper.ExecuteService("eFile", "GetTreeViewStructure", null);
            List<MyClass> items = new List<MyClass>();
            foreach (var item in Result)
            {
                items.Add(new MyClass(item.Name, Convert.ToInt32(item.AadharID), Convert.ToInt32(item.Parent_AadharID)));
            }

            items.ForEach(item => item.children = items.Where(child => child.ParentID == item.ID)
                                           .ToList());
            MyClass topItems = items.Where(item => item.ParentID == 0).FirstOrDefault();
            //List<MyClass> topItems = items.Where(item => item.ParentID == 0).ToList();
            return new JsonResult { Data = topItems, ContentType = "Json", JsonRequestBehavior = JsonRequestBehavior.AllowGet };


            //JavaScriptSerializer jss = new JavaScriptSerializer();
            // string output = jss.Serialize(topItems1);
            //output.Property("field2").Remove();

        }

        public ActionResult GetTreeViewStructure()
        {
            return PartialView("_TreeViewStructure");
        }



        [HttpPost]
        public ContentResult UploadPdfFiles()
        {
            string url = "~/EFileCommettee/Pdf";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/EFileCommettee/Pdf"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");



            //string url = "~/EFileCommettee/Pdf";
            //string directory = Server.MapPath(url);
            //if (Directory.Exists(directory))
            //{
            //    System.IO.Directory.Delete(directory, true);
            //}

            //var r = new List<tPrintingTemp>();

            //foreach (string file in Request.Files)
            //{
            //    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
            //    string s = ConvertBytesToMegabytes(hpf.ContentLength).ToString("0.00");

            //    // Convert bytes to megabytes.
            //    Console.WriteLine("{0} bytes = {1} megabytes",
            //        hpf.ContentLength,
            //        s);
            //    if (hpf.ContentLength == 0)
            //        continue;
            //    string url1 = "~/EFileCommettee/Pdf";
            //    string directory1 = Server.MapPath(url1);
            //    if (!Directory.Exists(directory1))
            //    {
            //        Directory.CreateDirectory(directory1);
            //    }
            //    string savedFileName = Path.Combine(Server.MapPath("~/EFileCommettee/Pdf"), Path.GetFileName(hpf.FileName));
            //    hpf.SaveAs(savedFileName);

            //    r.Add(new tPrintingTemp()
            //    {
            //        Name = hpf.FileName,
            //        // Length = hpf.ContentLength,
            //        MB = s,
            //        Type = hpf.ContentType,

            //    });
            //    //int val = 176763 ;
            //    //
            //}

            //return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} MB", r[0].MB) + "\"}", "application/json");
        }

        static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        public JsonResult RemovePDFFilesCom()
        {
            string url = "~/EFileCommettee/Pdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                // System.IO.Directory.Delete(directory, true);

                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ContentResult DocUploadFiles()
        {
            string url = "~/EFileCommettee/doc";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<eFileAttachment>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/EFileCommettee/doc"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new eFileAttachment()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].Length / 1024)) + "\"}", "application/json");
        }

        public JsonResult RemoveDocFiles()
        {
            string url = "~/EFileCommettee/Doc";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                // System.IO.Directory.Delete(directory, true);
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        //Sameer Code
        public JsonResult DiaryEntryPaper(int EfileId)
        {
            eFileAttachment Update = new eFileAttachment();
            Update.eFileAttachmentId = EfileId;
            Update.Year = DateTime.Now.Year.ToString();
            Update = (eFileAttachment)Helper.ExecuteService("eFile", "CheckUpdateDiary", Update);
            // return PartialView("/Areas/eFile/Views/eFile/_MethodReceive.cshtml");
            // return RedirectToAction("GetReceivePaperList");
            return Json(Update, JsonRequestBehavior.AllowGet);
        }


        public ActionResult BranchModel(int Id)
        {

            return PartialView("_GetBranch");

        }

        //Sujeet Code
        //public PartialViewResult eFileAllListDetails(string eFileNumber, int pageId = 1, int pageSize = 25)
        //{

        //    SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
        //    string DepartmentId = CurrentSession.DeptID;
        //    string Officecode = CurrentSession.OfficeId;
        //    var CommitteeTypePermission = (CommitteeTypePermission)Helper.ExecuteService("eFile", "GetCommitteUser", CurrentSession.UserID);
        //    if (CommitteeTypePermission != null && CommitteeTypePermission.CommitteeTypeId > 0)
        //        ViewBag.CommitteeTypePermission = "Yes";
        //    else
        //        ViewBag.CommitteeTypePermission = "No";
        //    string[] str = new string[2];
        //    str[0] = DepartmentId;
        //    str[1] = Officecode;

        //    eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "eFileListDeactive", str);
        //    eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
        //    ////////////////////////Server Side Paging///////////////////////////////////
        //    ViewBag.PageSize = pageSize;
        //    List<eFileList> pagedRecord = new List<eFileList>();
        //    if (pageId == 1)
        //    {
        //        pagedRecord = eList.eFileLists.Take(pageSize).ToList();
        //    }
        //    else
        //    {
        //        int r = (pageId - 1) * pageSize;
        //        pagedRecord = eList.eFileLists.Skip(r).Take(pageSize).ToList();
        //    }

        //    ViewBag.CurrentPage = pageId;
        //    ViewData["PagedList"] = Helper.BindPager(eList.eFileLists.Count, pageId, pageSize);
        //    ////////////////////////Server Side Paging///////////////////////////////////
        //    eList.eFileLists = pagedRecord;
        //    return PartialView("_eFileListViewByFileNumber", eList);

        //}


        public int GetSpentCountSMSByMember()
        {
            int SentSmsCount = 0;
            int CurrentMonth = DateTime.Today.Month;

            string FirstDay = GetFirstDayOfMonth(CurrentMonth).ToShortDateString();
            // get day of week for first day

            string[] dateParts = FirstDay.Split('/');

            DateTime dtFirstTemp = new

                DateTime(Convert.ToInt32(dateParts[2]),

                Convert.ToInt32(dateParts[0]),

                Convert.ToInt32(dateParts[1]));

            string LastDay = GetLastDayOfMonth(CurrentMonth).ToShortDateString();

            // get day of week for last day

            string[] dateParts2 = LastDay.Split('/');

            DateTime dtLastTemp = new

                DateTime(Convert.ToInt32(dateParts2[2]),

                Convert.ToInt32(dateParts2[0]),

                Convert.ToInt32(dateParts2[1]));

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@from", dtFirstTemp.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@to", dtLastTemp.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", "1"));

            if (CurrentSession.IsMember.ToUpper() == "FALSE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login
            {
                methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.MemberUserID));
            }
            else
            {
                //methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.AadharId));
                methodParameter.Add(new KeyValuePair<string, string>("@UserID", (!string.IsNullOrEmpty(CurrentSession.AadharId) ? CurrentSession.AadharId : "999999999999")));
            }
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "getAllCountSMSByUserID", methodParameter);

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {

                SentSmsCount = Convert.ToInt32(Convert.ToString(dataSet.Tables[0].Rows[0][0]));
            }


            return SentSmsCount;
        }


        public int GetMaxLimitCountSMSByMember()
        {
            mMember model = new mMember();
            if (CurrentSession.IsMember.ToUpper() == "FALSE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login and admin
            {
                model.AadhaarNo = CurrentSession.AadharId;
                CurrentSession.IsMember = "true";
                model.isAlive = Convert.ToBoolean(CurrentSession.IsMember);
            }
            else
            {

                if (string.IsNullOrEmpty(CurrentSession.MemberUserID) == true)
                {
                    model.AadhaarNo = CurrentSession.AadharId;
                }
                else
                {
                    model.AadhaarNo = CurrentSession.MemberUserID;
                }


                //model.AadhaarNo = (!string.IsNullOrEmpty(CurrentSession.MemberUserID) ? CurrentSession.MemberUserID : "999999999999");

                model.isAlive = (!string.IsNullOrEmpty(CurrentSession.IsMember) ? Convert.ToBoolean(CurrentSession.IsMember) : false);
                //model.isAlive = Convert.ToBoolean(CurrentSession.IsMember);

            }


            model.DistrictID = DateTime.Today.Month;
            var MaxLimityCount = (int)Helper.ExecuteService("ContactGroups", "GetMAXLimitByMember", model);

            var ExtendMaxLimityCount = 0;
            if (CurrentSession.IsMember.ToUpper() == "TRUE")
            {
                model.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
                ExtendMaxLimityCount = (int)Helper.ExecuteService("ContactGroups", "GetExtendSMSLimitByMember", model);
            }
            else { ExtendMaxLimityCount = 0; }

            var total = MaxLimityCount + ExtendMaxLimityCount;

            if (CurrentSession.IsMember.ToUpper() == "TRUE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login
            {
                CurrentSession.IsMember = "false";
            }
            return total;
        }




        private DateTime GetFirstDayOfMonth(int iMonth)
        {
            DateTime dtFrom = new DateTime(DateTime.Now.Year, iMonth, 1);

            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));

            return dtFrom;
        }

        private DateTime GetLastDayOfMonth(int iMonth)
        {
            DateTime dtTo = new DateTime(DateTime.Now.Year, iMonth, 1);

            dtTo = dtTo.AddMonths(1);

            dtTo = dtTo.AddDays(-(dtTo.Day));

            return dtTo;
        }

        #region
        // priyanka code

        public ActionResult NewProceddingList(int pageId, Int32 pageSize)
        {
            COmmitteeProceeding mdl = new COmmitteeProceeding();
            // mdl.DeptID = CurrentSession.DeptID;
            // string UserID = CurrentSession.UserID;
            mdl.Aadharid = CurrentSession.AadharId;
            mdl.UserID = CurrentSession.UserID;

            if (CurrentSession.Designation == "Chief Reporter")
            {
                mdl.CurrentDesignation = "Chief Reporter";
            }
            if (CurrentSession.Designation == "Editor of Debate")
            {
                mdl.CurrentDesignation = "Editor of Debate";
            }
            var cprocedding = (List<COmmitteeProceeding>)Helper.ExecuteService("eFile", "GetCommitteeProoceeding_new", mdl);

            ////////////////////////////Server Side Paging/////////////////////////////////// int pageId = 1, int pageSize = 25
            ViewBag.PageSize = pageSize;
            List<COmmitteeProceeding> pagedRecord = new List<COmmitteeProceeding>();
            if (cprocedding != null)
            {
                //if (pageId == 1)
                //{
                //    pagedRecord = cprocedding.Take(pageSize).ToList();
                //}
                //else
                //{
                //    int r = (pageId - 1) * pageSize;
                //  //  pagedRecord = cprocedding.ToList();
                //    pagedRecord = cprocedding.Skip(r).Take(pageSize).ToList();
                //}
                pagedRecord = cprocedding.ToList();
            }
            // pagedRecord = cprocedding.ToList();

            #region For Draft List Count
            //string DepartmentId = CurrentSession.DeptID;
            //string Officecode = CurrentSession.OfficeId;
            //string AadharId = CurrentSession.AadharId;
            //string currtBId = CurrentSession.BranchId;
            //string[] strD = new string[6];
            //strD[0] = DepartmentId;
            //strD[1] = Officecode;
            //strD[2] = AadharId;
            //strD[3] = currtBId;
            //strD[4] = CurrentSession.Designation;
            //strD[5] = "1";
            //var DraftListCount = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
            //ViewBag.DraftListCount = DraftListCount.Count;
            #endregion


            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(cprocedding.Count, pageId, pageSize);
            ////////////////////////////Server Side Paging///////////////////////////////////
            return PartialView("_NewCommitteeProceeding", pagedRecord);
        }
        public ActionResult NewCommittProceeding()
        {
            COmmitteeProceeding model = new COmmitteeProceeding();
            model.CommitteeList = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            //model.DepartmentList = deptList;
            return PartialView("_NewCommittProceeding", model);
        }

        public ActionResult saveCommittProceeding(COmmitteeProceeding committe, HttpPostedFileBase file)
        {

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/EFileCommettee/Pdf"; //"~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);
            if (committe.IsPdf == null || committe.IsPdf == "" || committe.IsPdf == "N")
            {
                if (Directory.Exists(directory1))
                {
                    string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                    if (savedFileName.Length > 0)
                    {
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            Guid FileName = Guid.NewGuid();

                            string name = Path.GetFileName(page);

                            string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));
                            if (!string.IsNullOrEmpty(name))
                            {
                                if (!string.IsNullOrEmpty(committe.ProFilePathPdf))
                                    System.IO.File.Delete(System.IO.Path.Combine(directory, committe.ProFilePathPdf));
                                System.IO.File.Copy(SourceFile, path, true);

                                committe.ProFilePathPdf = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                                committe.ProFileNamePdf = name;
                                if (Directory.Exists(directory1))
                                {
                                    //string[] filePaths = Directory.GetFiles(directory1);
                                    //foreach (string filePath in filePaths)
                                    //{
                                    //    System.IO.File.Delete(filePath);
                                    //}
                                }


                            }

                        }

                    }
                    else
                    {
                        committe.ProFilePathPdf = null;
                        //COmmitteeProceeding mm = new COmmitteeProceeding();
                        //mm.CommitteeList = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
                        //var deptList1 = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
                        //mm.DepartmentList = deptList1;
                        // mm.IsPdf = "not";
                        // ViewData["chk"]="PDN";
                        //return PartialView("_NewCommittProceeding", mm);
                        return Content("PdfFail");
                    }
                }

            }

            else
            {
                committe.ProFilePathPdf = committe.ProFilePathPdf;
                committe.ProFileNamePdf = committe.ProFileNamePdf;
            }

            //upload word file//
            string urlwrd = "~/EFileCommettee/doc";
            //string directorywrd = Server.MapPath(url1);

            string directory1Word = Server.MapPath(urlwrd);
            if (committe.IsDoc == null || committe.IsDoc == "" || committe.IsDoc == "N")
            {
                if (Directory.Exists(directory1Word))
                {

                    string[] savedFileNameWord = Directory.GetFiles(Server.MapPath(urlwrd));
                    if (savedFileNameWord.Length > 0)
                    {
                        string SourceFiles = savedFileNameWord[0];
                        foreach (string page1 in savedFileNameWord)
                        {
                            Guid FileName1 = Guid.NewGuid();

                            string nametwo = Path.GetFileName(page1);

                            string path1 = System.IO.Path.Combine(directory, FileName1 + "_" + nametwo.Replace(" ", ""));


                            if (!string.IsNullOrEmpty(nametwo))
                            {

                                if (!string.IsNullOrEmpty(committe.ProFilePathWord))
                                    System.IO.File.Delete(System.IO.Path.Combine(directory, committe.ProFilePathWord));
                                System.IO.File.Copy(SourceFiles, path1, true);
                                committe.ProFilePathWord = "/ePaper/" + FileName1 + "_" + nametwo.Replace(" ", "");
                                committe.ProFileNameWord = nametwo;

                                if (Directory.Exists(directory1Word))
                                {
                                    string[] filePaths = Directory.GetFiles(directory1Word);
                                    foreach (string filePath in filePaths)
                                    {
                                        System.IO.File.Delete(filePath);
                                    }
                                }
                                if (Directory.Exists(directory1))
                                {
                                    string[] filePaths = Directory.GetFiles(directory1);
                                    foreach (string filePath in filePaths)
                                    {
                                        System.IO.File.Delete(filePath);
                                    }
                                }
                            }

                        }

                    }
                    else
                    {
                        committe.ProFilePathWord = null;
                        return Content("wordFail");
                    }
                }
            }
            else
            {
                committe.ProFileNameWord = committe.ProFileNameWord;
                committe.ProFilePathWord = committe.ProFilePathWord;
            }


            committe.Aadharid = CurrentSession.AadharId;
            //mdl.Remark = committe.Remark;
            //mdl.DeptID = committe.Aadharid;
            //mdl.CommitteeId = committe.CommitteeId;
            Helper.ExecuteService("eFile", "saveCommitteeProceeding", committe);
            COmmitteeProceeding model = new COmmitteeProceeding();
            model.CommitteeList = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            //model.DepartmentList = deptList;
            return PartialView("_NewCommittProceeding", model);
        }
        public ActionResult Get_CommittProceedingForUpdate(Int32 procId)
        {
            COmmitteeProceeding model = new COmmitteeProceeding();
            model.ProID = procId;
            // model.Aadharid = CurrentSession.AadharId;
            model = (COmmitteeProceeding)Helper.ExecuteService("eFile", "GetCommitteeProoceeding_ById", model);
            model.CommitteeList = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            //model.DepartmentList = deptList;

            return PartialView("_NewCommittProceeding", model);
        }

        public JsonResult LockProcedding(Int32 procId)
        {
            COmmitteeProceeding model = new COmmitteeProceeding();
            model.ProID = procId;
            model.ForwardBy = CurrentSession.AadharId;
            Helper.ExecuteService("eFile", "LockedCommitteeProceeding", model);
            //  model = (COmmitteeProceeding)Helper.ExecuteService("eFile", "LockedCommitteeProceeding", model);
            //model.CommitteeList = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            //var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            //model.DepartmentList = deptList;
            //return PartialView("_eCommitteeProceeding");
            return Json("success", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region meetingAgenda
        public ActionResult GetMeetingView()
        {
            MeetingAgenda model = new MeetingAgenda();
            //  COmmitteeProceeding model = new COmmitteeProceeding();
            //model.MemberList = (List<SBL.DomainModel.ComplexModel.QuestionModelCustom>)Helper.ExecuteService("Member", "GetMemberList_ByAssembly", "");
            model.CommitteeList = (List<CommitteesListView>)Helper.ExecuteService("Committee", "Get_ActiveCommittee", "");

            //CommitteeList comList = new CommitteeList();
            // comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
            model.FinancialYearlist = GetYearList();
            //model.DepartmentList = deptList;
            return PartialView("_MeetingFilter", model);
        }



        public List<SelectListItem> GetYearList()
        {
            int year = 0;
            year = System.DateTime.Today.Year - 6;
            int i = 1;
            List<SelectListItem> YearList = new List<SelectListItem>();
            YearList.Add(new SelectListItem { Text = "Select", Value = "" });
            while (year < System.DateTime.Today.Year + 1)
            {
                YearList.Add(new SelectListItem { Text = year.ToString(), Value = year.ToString() });
                year = year + 1;

                i = i + 1;
            }
            return YearList;
        }

        public ActionResult Get_MeetingDocuments(string ComitteeId, string Month, string Year)
        {

            DataSet dataSet = new DataSet();
            MeetingAgenda model = new MeetingAgenda();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@Year", Year));
            methodParameter.Add(new KeyValuePair<string, string>("@Month", Month));
            methodParameter.Add(new KeyValuePair<string, string>("@ComId", ComitteeId));
            model.UserId = CurrentSession.UserID;
            if (CurrentSession.UserID == "addae56b-31ea-4449-88bd-b8c30074c591")
            {

                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Get_Committee_Agenda_NA", methodParameter);
                List<MeetingAgenda> lists = new List<MeetingAgenda>();
                if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        MeetingAgenda obj = new MeetingAgenda();
                        obj.CommitteeId = dataSet.Tables[0].Rows[i]["CommitteeId"].ToString();
                        obj.MeetingDate = dataSet.Tables[0].Rows[i]["MeetingDate"].ToString();
                        obj.NewMeetingDate = dataSet.Tables[0].Rows[i]["NewMeetingDate"].ToString(); //MeetingDate
                        obj.Meetingtime = dataSet.Tables[0].Rows[i]["Meetingtime"].ToString();
                        obj.CommitteeName = dataSet.Tables[0].Rows[i]["CommitteeName"].ToString();
                        obj.Abbreviation = dataSet.Tables[0].Rows[i]["Abbreviation"].ToString();
                        obj.COMId = dataSet.Tables[0].Rows[i]["COMId"].ToString();
                        lists.Add(obj);
                    }
                }

                model.MeetingDataList = lists;
                return PartialView("_MeetingDocList", model);

            }
            else
            {

                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Get_Committee_Web", methodParameter);
                List<MeetingAgenda> lists = new List<MeetingAgenda>();
                if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        MeetingAgenda obj = new MeetingAgenda();
                        obj.CommitteeId = dataSet.Tables[0].Rows[i]["CommitteeId"].ToString();
                        obj.MeetingDate = dataSet.Tables[0].Rows[i]["MeetingDate"].ToString();
                        obj.NewMeetingDate = dataSet.Tables[0].Rows[i]["NewMeetingDate"].ToString(); //MeetingDate
                        obj.Meetingtime = dataSet.Tables[0].Rows[i]["Meetingtime"].ToString();
                        obj.CommitteeName = dataSet.Tables[0].Rows[i]["CommitteeName"].ToString();
                        obj.Abbreviation = dataSet.Tables[0].Rows[i]["Abbreviation"].ToString();
                        obj.COMId = dataSet.Tables[0].Rows[i]["COMId"].ToString();
                        lists.Add(obj);
                    }
                }
                model.MeetingDataList = lists;
                return PartialView("_MeetingDocList", model);

            }
        }


        //public ActionResult Get_MeetingDocuments(string ComitteeId, string Month, string Year)
        //{
        //    DataSet dataSet = new DataSet();
        //    MeetingAgenda model = new MeetingAgenda();
        //    var methodParameter = new List<KeyValuePair<string, string>>();
        //    methodParameter.Add(new KeyValuePair<string, string>("@Year", Year));
        //    methodParameter.Add(new KeyValuePair<string, string>("@Month", Month));
        //    //string[] strngBID = new string[2];
        //    //strngBID[0] = CurrentSession.BranchId;
        //    //var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBID);
        //    //string ComitteeId = value[0];
        //    methodParameter.Add(new KeyValuePair<string, string>("@ComId", ComitteeId));
        //    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Get_Committee_Agenda_NA", methodParameter);
        //    List<MeetingAgenda> lists = new List<MeetingAgenda>();
        //    if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
        //        {
        //            MeetingAgenda obj = new MeetingAgenda();
        //            obj.CommitteeId = dataSet.Tables[0].Rows[i]["CommitteeId"].ToString();
        //            obj.MeetingDate = dataSet.Tables[0].Rows[i]["MeetingDate"].ToString();
        //            obj.NewMeetingDate = dataSet.Tables[0].Rows[i]["NewMeetingDate"].ToString(); //MeetingDate
        //            obj.Meetingtime = dataSet.Tables[0].Rows[i]["Meetingtime"].ToString();
        //            obj.CommitteeName = dataSet.Tables[0].Rows[i]["CommitteeName"].ToString();
        //            obj.Abbreviation = dataSet.Tables[0].Rows[i]["Abbreviation"].ToString();
        //            obj.COMId = dataSet.Tables[0].Rows[i]["COMId"].ToString();
        //            lists.Add(obj);
        //        }
        //    }
        //    model.MeetingDataList = lists;
        //    return PartialView("_MeetingDocList", model);

        //}


        public ActionResult Get_MeetingpdfData(string ComId, string MeetingDate)
        {
            DataSet dataSet = new DataSet();
            MeetingAgenda model = new MeetingAgenda();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@ComId", ComId));
            methodParameter.Add(new KeyValuePair<string, string>("@MeetingDate", MeetingDate));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetCOMMember_Com", methodParameter);
            List<MeetingAgenda> listspdf = new List<MeetingAgenda>();
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    MeetingAgenda obj = new MeetingAgenda();
                    obj.TextCOM = dataSet.Tables[0].Rows[i]["TextCOM"].ToString();
                    obj.PDFLocation = dataSet.Tables[0].Rows[i]["PDFLocation"].ToString();
                    obj.SrNo1 = dataSet.Tables[0].Rows[i]["SrNo1"].ToString();
                    obj.SrNo2 = dataSet.Tables[0].Rows[i]["SrNo2"].ToString();
                    obj.SrNo3 = dataSet.Tables[0].Rows[i]["SrNo3"].ToString();
                    obj.MainSrNo = obj.SrNo1 + "." + obj.SrNo2 + "." + obj.SrNo3;
                    obj.IsDeleted = dataSet.Tables[0].Rows[i]["IsDeleted"].ToString();
                    obj.COMId = ComId;
                    obj.MeetingDate = MeetingDate;

                    listspdf.Add(obj);
                }
            }
            model.MeetingPdfList = listspdf;
            return PartialView("_MeetingpdfDataList", model);

        }
        #endregion


        public JsonResult ApproveAgenda(string ComId)
        {
            // if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string result = "";
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@ComId", ComId));
            DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SubmitCOMById", methodParameter);

            var res2 = Json(result, JsonRequestBehavior.AllowGet);
            return res2;
        }

        //New Code for efile listing by Lakshay

        public PartialViewResult GetAlleFileList(int pageId = 1, int pageSize = 25)
        {
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();

            var CommitteeTypePermission = (CommitteeTypePermission)Helper.ExecuteService("eFile", "GetCommitteUser", CurrentSession.UserID);
            if (CommitteeTypePermission != null && CommitteeTypePermission.CommitteeTypeId > 0)
                ViewBag.CommitteeTypePermission = "Yes";
            else
                ViewBag.CommitteeTypePermission = "No";

            //string DepartmentId = CurrentSession.DeptID;
            //string Officecode = CurrentSession.OfficeId;

            //string[] str = new string[3];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            //str[2] = CurrentSession.BranchId;

            /////////////////////////////////////
            pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
            List<tCommittee> CommitteeList = new List<tCommittee>();
            tPaperLaidV model = new tPaperLaidV();
            mBranches modelBranch = new mBranches();
            modelBranch.UserId = Guid.Parse(CurrentSession.UserID);
            model.BranchesList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "GBranchList", modelBranch);
            foreach (var i in model.BranchesList)
            {
                string[] strngBID = new string[2];
                strngBID[0] = Convert.ToString(i.BranchId);
                List<tCommittee> CommitteeList1 = new List<tCommittee>();
                CommitteeList1 = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("eFile", "nGetCommiteeIDAndNameByBranchID", strngBID);
                CommitteeList.AddRange(CommitteeList1);
            }
            mdl.tCommitteeList = CommitteeList;
            mdl.CurrentDeptId = CurrentSession.DeptID;
            /////////////////////////////////////




            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFiles", mdl);
            eList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);

            return PartialView("_ShowAlleFileList", eList);

        }

        public ActionResult CreateNewFile()
        {
            SBL.DomainModel.Models.CommitteeReport.tCommitteReplyPendency model = new tCommitteReplyPendency();
            model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);
            // model.mCommitteeReplyMasterList = (List<tCommitteReplyByMaster>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyByMasterRecord", null);
            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetFileItemList", null);
            model.mCommitteeReplyItemTypeList = _ListItems;

            /////////////////////////////////////
            tPaperLaidV modelnew = new tPaperLaidV();
            mBranches modelBranch = new mBranches();
            modelBranch.UserId = Guid.Parse(CurrentSession.UserID);
            modelnew.BranchesList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "GBranchList", modelBranch);
            List<tCommittee> CommitteeList = new List<tCommittee>();
            foreach (var i in modelnew.BranchesList)
            {
                string[] str1 = new string[4];
                str1[2] = Convert.ToString(i.BranchId);
                string[] strngBID = new string[2];
                strngBID[0] = Convert.ToString(i.BranchId);
                List<tCommittee> CommitteeList1 = new List<tCommittee>();
                CommitteeList1 = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("eFile", "nGetCommiteeIDAndNameByBranchID", strngBID);
                CommitteeList.AddRange(CommitteeList1);
            }
            model.tCommitteeList = CommitteeList;

            /////////////////////////////////////
            //new requirement to show selected committee only..                     
            string[] str = new string[3];
            str[0] = CurrentSession.BranchId;
            if (str[0] != "")
            {
                var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", str);
                //model.CommitteeId = Convert.ToInt16(value[0]);
                //ViewBag.CommitteeId = value[0];
                //ViewBag.CommitteeName = value[1];
                string[] str2 = new string[4];
                str2[3] = value[0];
                SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
                eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GeteFilesListToLink", str2);
                var eFileList = eList.eFileLists;
                ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);
            }
            else
            {
                var value = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndName", null);
                model.CommitteeId = Convert.ToInt16(value[0]);
                ViewBag.CommitteeId = value[0];
                ViewBag.CommitteeName = value[1];
            }
            model.eFilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);
            return PartialView("CreateNewFileEntryForm", model);
        }

        [HttpPost]
        public JsonResult SaveNewFileEntry(List<eFileList> LetterList)
        {
            int count = 0;
            foreach (var item in LetterList)
            {
                eFileList obj = new eFileList();
                obj.CommitteeId = item.CommitteeId;
                obj.DepartmentId = CurrentSession.DeptID;
                obj.SelectedDeptId = item.DepartmentId;
                obj.FromYear = item.FromYear;
                obj.ToYear = item.ToYear;
                obj.eFileTypeId = item.eFileTypeId;
                obj.eFileNumber = item.eFileNumber;
                obj.eFileSubject = item.eFileSubject;
                //if (item.FileAttched_DraftId == 0)
                //{
                //    obj.FileAttched_DraftId = null;
                //}
                //else 
                //{
                //    obj.FileAttched_DraftId = item.FileAttched_DraftId;
                //}
                if (item.PreviousLinkId == 0)
                {
                    obj.PreviousLinkId = null;
                }
                else
                {
                    obj.PreviousLinkId = item.PreviousLinkId;
                }

                obj.CreateDate = DateTime.Now;
                int res = (int)Helper.ExecuteService("eFile", "SaveNewFileEntry", obj);
            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditNewFile(int eFileId)
        {
            SBL.DomainModel.Models.eFile.eFileList eList = new DomainModel.Models.eFile.eFileList();
            SBL.DomainModel.Models.CommitteeReport.tCommitteReplyPendency model = new tCommitteReplyPendency();

            eList = (SBL.DomainModel.Models.eFile.eFileList)Helper.ExecuteService("eFile", "GeteFileById", eFileId);

            model.CommitteeId = Convert.ToInt16(eList.CommitteeId);
            model.tCommitteeList = (List<tCommittee>)Helper.ExecuteService("CommitteReplyPendency", "GetCommitteList", null);

            model.DepartmentId = eList.DepartmentId;
            model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            model.FromYear = eList.FromYear;
            model.ToYear = eList.ToYear;
            model.eFileID = eList.eFileID;
            model.eFileNumber = eList.eFileNumber;
            model.ItemDescription = eList.eFileSubject;
            model.eFileAttachmentID = Convert.ToInt16(eList.FileAttched_DraftId);
            //model.mCommitteeReplyMasterList = (List<tCommitteReplyByMaster>)Helper.ExecuteService("CommitteReplyPendency", "GetReplyByMasterRecord", null);
            var _ListItems = (List<mCommitteeReplyItemType>)Helper.ExecuteService("eFile", "GetFileItemList", null);
            model.mCommitteeReplyItemTypeList = _ListItems;

            model.ItemTypeId = eList.eFileTypeId;
            model.eFilePaperTypeList = (List<eFilePaperType>)Helper.ExecuteService("CommitteReplyPendency", "GeteFilePaperType", null);

            string[] str2 = new string[4];
            //str1[0] = CurrentSession.DeptID;
            //str1[3] = Convert.ToString(model.CommitteeId);
            //SBL.DomainModel.Models.eFile.eFileListAll newlist = new DomainModel.Models.eFile.eFileListAll();
            //newlist = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "NeFileList", str1);
            //var eFileList = newlist.eFileLists;
            //model.eFileList = eFileList;

            str2[3] = Convert.ToString(model.CommitteeId);
            SBL.DomainModel.Models.eFile.eFileListAll eList2 = new DomainModel.Models.eFile.eFileListAll();
            eList2 = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GeteFilesListToLink", str2);
            var eFileList = eList2.eFileLists;
            model.eFileList = eFileList;
            //ViewBag.EFilesListAll = new SelectList(eFileList, "eFileID", "eFileNumber", null);

            return PartialView("EditeFileForm", model);
        }


        public ActionResult ShowFileDocumentsAttached(string FileId)
        {
            SBL.DomainModel.Models.eFile.pHouseCommitteeFiles eList = new DomainModel.Models.eFile.pHouseCommitteeFiles();
            SBL.DomainModel.Models.eFile.eFileList eFilelist = new DomainModel.Models.eFile.eFileList();

            eFilelist = (SBL.DomainModel.Models.eFile.eFileList)Helper.ExecuteService("eFile", "GeteFileDetailsByID", Convert.ToInt16(FileId));
            var a = eFilelist;
            eList.eFileFromYear = a.FromYear;
            eList.eFileToYear = a.ToYear;
            eList.eFileNumber = a.eFileNumber;
            string info = System.Text.RegularExpressions.Regex.Replace(a.eFileSubject, "<[^>]*>", string.Empty);
            eList.eFileSubject = info;
            eList.FutureLinkId = a.FutureLinkId;
            eList.FutureFileNumber = a.FutureFileNumber;
            eList.PreviousFileNumber = a.PreviousFileNumber;
            eList.PreviousLinkId = a.PreviousLinkId;

            List<pHouseCommitteeFiles> ListModel = new List<pHouseCommitteeFiles>();

            //ListModel = (List<pHouseCommitteeFiles>)Helper.ExecuteService("eFile", "GetAllDocumentsByFileId", FileId);
            SBL.DomainModel.Models.eFile.pHouseCommitteeFiles pHouseCommitteeFiles = new DomainModel.Models.eFile.pHouseCommitteeFiles();
            pHouseCommitteeFiles.CurrentDeptId = CurrentSession.DeptID;
            pHouseCommitteeFiles.eFileID = Convert.ToInt16(FileId);
            ListModel = (List<pHouseCommitteeFiles>)Helper.ExecuteService("eFile", "GetAllDocumentsByFileId", pHouseCommitteeFiles);

            eList.HouseCommAllFilesList = ListModel;
            if (ListModel.Count > 0)
            {
                var Name = eList.HouseCommAllFilesList.FirstOrDefault().eFileName;
                eList.eFileName = Name;
            }

            return PartialView("_ShowFileDocumentsList", eList);
        }
        public JsonResult GetFilesByLinkedId(int LinkedFileId)
        {
            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetFilesByLinkedId", LinkedFileId);
            return Json(eList.eFileLists, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateFileEntry(List<eFileList> LetterList)
        {
            int count = 0;
            foreach (var item in LetterList)
            {
                eFileList obj = new eFileList();
                obj.eFileID = item.eFileID;
                obj.CommitteeId = item.CommitteeId;
                obj.DepartmentId = CurrentSession.DeptID;
                obj.SelectedDeptId = item.DepartmentId;
                obj.FromYear = item.FromYear;
                obj.ToYear = item.ToYear;
                obj.eFileTypeId = item.eFileTypeId;
                obj.eFileNumber = item.eFileNumber;
                obj.eFileSubject = item.eFileSubject;
                obj.Status = item.Status;
                //obj.FileAttched_DraftId = item.FileAttched_DraftId;
                if (item.PreviousLinkId == 0)
                {
                    obj.PreviousLinkId = null;
                }
                else
                {
                    obj.PreviousLinkId = item.PreviousLinkId;
                }
                obj.CreateDate = DateTime.Now;
                int res = (int)Helper.ExecuteService("eFile", "UpdateFileEntry", obj);
            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public FileStreamResult GenerateLOBPdf()
        {
            string outXml = "";                 
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }       
            string savedPDF = string.Empty;
            MemoryStream output = new MemoryStream();
            

            SBL.DomainModel.Models.eFile.eFileListAll eList = new DomainModel.Models.eFile.eFileListAll();               

            /////////////////////////////////////
            pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
            List<tCommittee> CommitteeList = new List<tCommittee>();
            tPaperLaidV model = new tPaperLaidV();
            mBranches modelBranch = new mBranches();
            modelBranch.UserId = Guid.Parse(CurrentSession.UserID);
            model.BranchesList = (ICollection<mBranches>)Helper.ExecuteService("eFile", "GBranchList", modelBranch);
            foreach (var i in model.BranchesList)
            {
                string[] strngBID = new string[2];
                strngBID[0] = Convert.ToString(i.BranchId);
                List<tCommittee> CommitteeList1 = new List<tCommittee>();
                CommitteeList1 = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("eFile", "nGetCommiteeIDAndNameByBranchID", strngBID);
                CommitteeList.AddRange(CommitteeList1);
            }
            mdl.tCommitteeList = CommitteeList;
            mdl.CurrentDeptId = CurrentSession.DeptID;
            /////////////////////////////////////
            eList = (SBL.DomainModel.Models.eFile.eFileListAll)Helper.ExecuteService("eFile", "GetAlleFiles", mdl);
           



                if (eList.eFileLists != null )
                {
                    outXml = @"<html>                           
                                 <body style='font-family:DVOT-Surekh;'> 
                                <div>
                                <div style='width: 100%;'>
                                <table style='width: 100%;'>";
                         outXml += @"<tr>
                                     <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
                                     Himachal Pradesh Vidhan Sabha" + @"    
                                     </td>
                                     </tr>
                                     <tr>
                                     <td style='text-align: center; font-size: 16px; font-weight: bold;'>               
                                      " + "e-File List" + @"    
                                     </td>
                                     </tr>";
                         outXml += "</table></div></div> </body> </html>";



                         outXml += @"<html>                           
                                       <body style='font-family:DVOT-Surekh;'> 
                                       <div>
                                       <div style='width: 100%;'>
                                       <table style='width: 100%;'>";
                         outXml += @"<tr>

                                     <td style='text-align: center; font-size: 14px; font-weight: bold;border: 1px solid;'>               
                                     Sr.No." + @"    
                                     </td> 
                                    <td style='text-align: center; font-size: 14px; font-weight: bold;border: 1px solid;'>               
                                     Year" + @"    
                                     </td> 
                                    <td style='text-align: center; font-size: 14px; font-weight: bold;border: 1px solid;'>               
                                     eFile Number" + @"    
                                     </td> 
                                    <td style='text-align: center; font-size: 14x; font-weight: bold;border: 1px solid;'>               
                                     File Type" + @"    
                                     </td> 
                                    <td style='text-align: center; font-size: 14px; font-weight: bold;border: 1px solid;'>               
                                     Committee" + @"    
                                     </td> 
                                    <td style='text-align: center; font-size: 14px; font-weight: bold;border: 1px solid;'>               
                                     Department" + @"    
                                     </td> 
                                    <td style='text-align: center; font-size: 14px; font-weight: bold;border: 1px solid;'>               
                                     Subject" + @"    
                                     </td> 
                                    <td style='text-align: center; font-size: 14px; font-weight: bold;border: 1px solid;'>               
                                     Status" + @"    
                                     </td> 



                                     </tr>";

                         int count = 0;
                    foreach (var item in eList.eFileLists)
                    {
                            count++;
                            outXml += @"<tr><td style='text-align: left; font-size: 14px; border: 1px solid;'>                                    
                                      " + count + @"";


                            string YearIs = item.FromYear + "-" + item.ToYear;
                            outXml += @"<td style='text-align: left; font-size: 14px; border: 1px solid;'>
                                    
                                      " + YearIs + @"";

                            outXml += @"<td style='text-align: left; font-size: 14px; border: 1px solid;'>
                                    
                                      " + item.eFileNumber + @"";

                            outXml += @"<td style='text-align: left; font-size: 14px; border: 1px solid;'>
                                    
                                      " + item.eFileTypeName + @"";

                            outXml += @"<td style='text-align: left; font-size: 14px; border: 1px solid;'>
                                    
                                      " + item.Committee + @"";
                            outXml += @"<td style='text-align: left; font-size: 12px; border: 1px solid;'>
                                    
                                      " + item.Department + @"";

                            outXml += @"<td style='text-align: left; font-size: 14px; border: 1px solid;'>
                                    
                                      " + item.eFileSubject + @"";

                            if (item.Status == true)
                            {
                                string StatusText = "Open";
                                outXml += @"<td style='text-align: left; font-size: 14px; border: 1px solid;'>
                                    
                                      " + StatusText + @"</tr>";
                            }
                            else
                            {
                                string StatusText = "Closed";
                                outXml += @"<td style='text-align: left; font-size: 14px; border: 1px solid;'>
                                    
                                      " + StatusText + @"</tr>";
                            }

                          

                            
                    }

                 outXml += "</table></div></div> </body> </html>";
                 EvoPdf.Document document1 = new EvoPdf.Document();
                AddElementResult addResult;
                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(50, 55, 40, 30);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(20, 55, 75, 65), PdfPageOrientation.Landscape);
                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(50, 20, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {                   
                    document1.Close();
                }
                ///To get File Path                
                string fileName = "";


                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=eFileList.pdf");
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string directory = FileSettings.SettingValue + "/eFileTemp/" + "EFiles" + "/";
                DirectoryInfo Dir = new DirectoryInfo(directory);
                if (!Dir.Exists)
                {
                    Dir.Create();
                }
                fileName = "eFileList.pdf";
                var path = Path.Combine(directory, fileName);
                string dbpath = "/eFileTemp/" + "EFiles" + "/";
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();


                var filepath = Path.Combine(Server.MapPath(dbpath));
                // Get the complete folder path and store the file inside it.  
                DirectoryInfo Dir1 = new DirectoryInfo(filepath);
                if (!Dir1.Exists)
                {
                    Dir1.Create();
                }
                string fname = Path.Combine(filepath, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(fname, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream1.Close();               
                }

               
            return File(output, "application/pdf");
        }

        public ActionResult DownloadPDF(string fileName)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }        

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);       
            string url =  "/eFileTemp/" + "EFiles" + "/";
            string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
            string path = System.IO.Path.Combine(directory, "eFileList.pdf");
            return File(path, "application/pdf");
        }
    }
}