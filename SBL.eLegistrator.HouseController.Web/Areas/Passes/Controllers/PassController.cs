﻿using SBL.eLegistrator.HouseController.Web.Areas.Passes.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Passes;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Passes.Controllers
{
    [SiteSettingsFilter]
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class PassController : Controller
    {

        private int AssemblyId = 12;
        private int SessionId = 4;

        #region Issue Pass (For Department User)

        public ActionResult Index(int PassCategoryId = 0, int Status = 0, int AssemblyId = 12, int SessionId = 4, string SearchText = "")
        {
            Sanitizer.GetSafeHtmlFragment(SearchText);
            var Parameter = new Pass { AssemblyID = AssemblyId, SessionID = SessionId, PassCategoryID = PassCategoryId };
            var Passes = new List<Pass>();

            switch (Status)
            {
                case 0: { Passes = (List<Pass>)Helper.ExecuteService("Pass", "GetAllPasses", Parameter); break; }
                case 1: { Passes = (List<Pass>)Helper.ExecuteService("Pass", "GetAllRequestedPasses", Parameter); break; }
                case 2: { Passes = (List<Pass>)Helper.ExecuteService("Pass", "GetAllApprovedPasses", Parameter); break; }
                case 3: { Passes = (List<Pass>)Helper.ExecuteService("Pass", "GetAllRejectedPasses", Parameter); break; }
            }

            var PassCategories = (List<PassCategory>)Helper.ExecuteService("Pass", "GetAllActivePassCategories", null);

            var model = new PassIndexViewModel
            {
                PassList = Passes.ToViewModel(),
                AssemblyId = AssemblyId,
                SessionId = SessionId,
                SearchText = SearchText,
                Status = Status,
                PassCategoryId = PassCategoryId,
                PassCategoryList = new SelectList(PassCategories, "PassCategoryID", "Name",PassCategoryId),
                Heading = (Status == 1) ? "Requested List" : (Status == 2) ? "Approved List" : (Status == 3) ? "Rejected List" : "Pass List"
            };


            if (!Request.IsAjaxRequest())
            {
                return View(model);
            }
            else
            {
                return PartialView("_PassListGrid", model);
            }

        }


        public ActionResult IssueNewPass()
        {
            var PassCategories = (List<PassCategory>)Helper.ExecuteService("Pass", "GetAllActivePassCategories", null);


            var model = new PassViewModel
            {
                SessionID = SessionId,
                AssemblyID = AssemblyId,
                Status = 1,
                Mode = "Add",
                PassCategoryList = new SelectList(PassCategories, "PassCategoryID", "Name"),
                PrefixList = ModelMapping.GetNameTitles()
            };
            return View("IssuePass", model);
        }


        public ActionResult EditPass(int Id)
        {
            var PassCategories = (List<PassCategory>)Helper.ExecuteService("Pass", "GetAllActivePassCategories", null);
            var Pass = (Pass)Helper.ExecuteService("Pass", "GetPassById", new Pass { PassID = Id });
            var model = Pass.ToViewModel("Edit");
            model.PassCategoryList = new SelectList(PassCategories, "PassCategoryID", "Name");
            return View("IssuePass", model);
        }


        public ActionResult ChangeStatus(int Id)
        {
            Helper.ExecuteService("Pass", "ChangePassStatus", new Pass { PassID = Id });
            return RedirectToAction("Index");
        }


        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SavePass(PassViewModel model, HttpPostedFileBase file)
        {
            var DepartmentId = "HPD0033";//CurrentSession.DeptID;
            var OrganizationName = CurrentSession.LoginDepartmentName;
            if (ModelState.IsValid)
            {
                var Pass = model.ToDomainModel();
                if (model.Mode == "Add")
                {
                    Guid FileName = Guid.NewGuid();
                    Pass.Photo = UploadPhoto(file, FileName);
                    Pass.IsActive = true;
                    Pass.IssueDate = DateTime.Now;
                    Pass.DepartmentID = DepartmentId;
                    Pass.OrgDeptName = OrganizationName;
                    Pass.RequestedBy = CurrentSession.UserName;
                    Helper.ExecuteService("Pass", "IssueNewPass", Pass);
                }
                else
                {
                    if (file != null)
                    {
                        RemoveExistingPhoto(Pass.Photo);
                        Guid FileName = Guid.NewGuid();
                        Pass.Photo = UploadPhoto(file, FileName);
                        Helper.ExecuteService("Pass", "UpdatePass", Pass);
                    }
                    else
                    {
                        Helper.ExecuteService("Pass", "UpdatePass", Pass);
                    }
                }
            }
            return RedirectToAction("Index");
        }


        public JsonResult CheckForVehicle(int Id)
        {
            var result = (bool)Helper.ExecuteService("Pass", "CheckPassCategoryForVehicle", new PassCategory { PassCategoryID = Id });
            if (result)
            {
                return Json(new { IsForVehicle = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { IsForVehicle = false }, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// For Showing the left menu
        /// </summary>
        /// <returns></returns>
        public PartialViewResult PassLeftMenu(bool IsDepartmentUser)
        {
            var Parameter = new Pass { AssemblyID = AssemblyId, SessionID = SessionId };
            var model = new PassLeftMenuModel()
               {
                   ApprovedPassCount = (int)Helper.ExecuteService("Pass", "GetApprovedPassesCount", Parameter),
                   RejectedPassCount = (int)Helper.ExecuteService("Pass", "GetRejectedPassesCount", Parameter),
                   RequestedPassCount = (int)Helper.ExecuteService("Pass", "GetRequestedPassesCount", Parameter)
               };
            if (IsDepartmentUser)
            {
                return PartialView("_PassLeftMenu", model);
            }
            else
            {
                return PartialView("_PassApproveLeftMenu", model);
            }
        }




        #endregion



        #region Approval (For Administrative User)

        public ActionResult ApprovePasses(int PassCategoryId = 0, int Status = 0, int AssemblyId = 12, int SessionId = 4, string SearchText = "")
        {
            Sanitizer.GetSafeHtmlFragment(SearchText);
            var Parameter = new Pass { AssemblyID = AssemblyId, SessionID = SessionId, PassCategoryID = PassCategoryId };
            var Passes = new List<Pass>();

            switch (Status)
            {
                case 0:
                case 1: { Passes = (List<Pass>)Helper.ExecuteService("Pass", "GetAllRequestedPasses", Parameter); break; }
                case 2: { Passes = (List<Pass>)Helper.ExecuteService("Pass", "GetAllApprovedPasses", Parameter); break; }
                case 3: { Passes = (List<Pass>)Helper.ExecuteService("Pass", "GetAllRejectedPasses", Parameter); break; }
            }

            var PassCategories = (List<PassCategory>)Helper.ExecuteService("Pass", "GetAllActivePassCategories", null);
            var model = new PassIndexViewModel
            {
                PassList = Passes.ToViewModel(),
                AssemblyId = AssemblyId,
                SessionId = SessionId,
                SearchText = SearchText,
                Status = Status,
                PassCategoryId = PassCategoryId,
                PassCategoryList = new SelectList(PassCategories, "PassCategoryID", "Name", PassCategoryId),
                Heading = (Status == 1) ? "Pending To Approve List" : (Status == 2) ? "Approved List" : (Status == 3) ? "Rejected List" : "Pass List"
            };
            if (!Request.IsAjaxRequest())
            {
                return View("ApprovePassForm", model);
            }
            else
            {
                return PartialView("_ApprovePassGrid", model);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PassIdArray"></param>
        /// <param name="Mode">Approve/Reject/Print</param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ApproveRejectPrintMultipleRequest(int[] PassIdArray, string Mode)
        {
            Mode = Sanitizer.GetSafeHtmlFragment(Mode);
            var PassesToProcess = new List<Pass>();
            foreach (var Id in PassIdArray)
            {
                PassesToProcess.Add(new Pass { PassID = Id, ApprovedBy = CurrentSession.UserName });
            }
            if (Mode == "Approve")
            {
                Helper.ExecuteService("Pass", "ApproveMultiplePassRequest", PassesToProcess);
            }
            else if (Mode == "Reject")
            {
                Helper.ExecuteService("Pass", "RejectMultiplePassRequest", PassesToProcess);
            }
            else if (Mode == "Print")
            {
                var PrintPreviewModel = (List<Pass>)Helper.ExecuteService("Pass", "PassesForPrinting", PassesToProcess);
                return PartialView("_PassPrintPreview", PrintPreviewModel.ToViewModel());
            }
            return RedirectToAction("ApprovePasses");
        }


        #endregion


        #region Private Methods
        private string UploadPhoto(HttpPostedFileBase File, Guid FileName)
        {
            if (File != null)
            {
                string extension = System.IO.Path.GetExtension(File.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Images/Pass/Photo"), FileName + extension);
                File.SaveAs(path);
                return (FileName + extension);
            }
            return null;
        }

        private void RemoveExistingPhoto(string OldPhoto)
        {
            string path = System.IO.Path.Combine(Server.MapPath("~/Images/Pass/Photo"), OldPhoto);

            if (System.IO.File.Exists(OldPhoto))
            {
                System.IO.File.Delete(OldPhoto);
            }


        }
        #endregion
    }
}
