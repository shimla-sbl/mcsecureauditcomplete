﻿using SBL.DomainModel.Models.Passes;
using SBL.eLegistrator.HouseController.Web.Areas.Passes.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System.Collections.Generic;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Passes.Controllers
{
    [SiteSettingsFilter]
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class PassCategoryController : Controller
    {
     
        public ActionResult Index()
        {
            var PassCategory = (List<PassCategory>)Helper.ExecuteService("Pass", "GetAllPassCategories", null);
            var model = PassCategory.ToViewModel();
            return View(model);
           
        }
        
        public ActionResult Create()
        {
            var model = new PassCategoryViewModel
            {
                Mode = "Add"
            };
            return View(model);
        }

        public ActionResult Edit(int Id)
        {

            var PassCategory = (PassCategory)Helper.ExecuteService("Pass", "GetPassCategoryById", new PassCategory { PassCategoryID = Id });

            return View("Create", PassCategory.ToViewModel("Edit"));

        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SavePassCategory(PassCategoryViewModel model)
        {
            if (model.Mode == "Add")
            {
                model.IsActive = true;
                Helper.ExecuteService("Pass", "CreatePassCategory", model.ToDomainModel());
            }
            else
                Helper.ExecuteService("Pass", "UpdatePassCategory", model.ToDomainModel());
            return RedirectToAction("Index");
        }

        public ActionResult DeletePassCategory(int Id)
        {
            var Organization = Helper.ExecuteService("Pass", "DeletePassCategory", new PassCategory { PassCategoryID = Id });

            return RedirectToAction("Index");
        }

        public ActionResult ChangePassCategory(int Id)
        {
            var PassCategory = Helper.ExecuteService("Pass", "ChangePassCategoryStatus", new PassCategory { PassCategoryID = Id });

            return RedirectToAction("Index");
        }

    }
}
