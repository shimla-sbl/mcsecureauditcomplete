﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Passes
{
    public class PassAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Passes";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
               "Passes_default",
               "Passes/{controller}/{action}/{Id}",
               new { action = "Index", Id = UrlParameter.Optional }
           );
            context.MapRoute(
                "Passes_Index",
                "Passes/{controller}/{action}/{Status}/{AssemblyId}/{SessionId}",
                new { action = "Index", Status = UrlParameter.Optional, AssemblyId = UrlParameter.Optional, SessionId=UrlParameter.Optional }
            );

            context.MapRoute(
                "Passes_ApproveForm",
                "Passes/{controller}/{action}/{Status}/{AssemblyId}/{SessionId}",
                new { action = "ApprovePasses", Status = UrlParameter.Optional, AssemblyId = UrlParameter.Optional, SessionId = UrlParameter.Optional }
            );
            
        }
    }
}
