﻿using SBL.DomainModel.Models.Passes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Passes.Models
{
    public class PassViewModel
    {

        public int PassID { get; set; }

        public PassCategory PassCategory { get; set; }
        public string PassCategoryName { get; set; }
        [Required(ErrorMessage = "Pass category required")]
        public int PassCategoryID { get; set; }
        
        
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }

       
        [Required(ErrorMessage = "Title required")]        
        public string Prefix { get; set; }

        [Required(ErrorMessage = "Name required")]
        [StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$", ErrorMessage =
            "Numbers and special characters are not allowed")]
        public string Name { get; set; }

        public string UID { get; set; }

        [Required(ErrorMessage = "Phone required")]
        [StringLength(15, ErrorMessage = "Maximum 15 characters allowed")]
        [RegularExpression(@"^[0-9\.\+\-\/]+$", ErrorMessage = "Invalid phone number")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Email required")]
        [EmailAddress(ErrorMessage = "Invalid Email")]
        [StringLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Designation required")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$", ErrorMessage =
           "Numbers and special characters are not allowed")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string Designation { get; set; }

         [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string Address { get; set; }

        public string Photo { get; set; }

        public string DepartmentID { get; set; }
        public int? OrganizationID { get; set; }

        public string OrgDeptName { get; set; }

        [Required(ErrorMessage = "Valid from date required")]
        [DisplayFormat(DataFormatString = "{0:d}",ApplyFormatInEditMode=true)]
        public DateTime? ValidFrom { get; set; }

        [Required(ErrorMessage = "Valid upto date required")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? ValidUpto { get; set; }

        public DateTime IssueDate { get; set; }

         [StringLength(15, ErrorMessage = "Maximum 15 characters allowed")]
        public string VehicleNumber { get; set; }

         [StringLength(2, ErrorMessage = "Maximum 2 characters allowed")]
        public string GateNumber { get; set; }
        /// <summary>
        /// 1:Requested 2:Approved 3: Rejected
        /// </summary>
        public int Status { get; set; }

        public string RequestedBy { get; set; }
        public string ApprovedBy { get; set; }

        public bool IsActive { get; set; }

        public bool ForVehicle { get; set; }
        public SelectList OrganizationList { get; set; }
        public SelectList DepartmentList { get; set; }
        public SelectList PassCategoryList { get; set; }
        public SelectList PrefixList { get; set; }


        public string Mode { get; set; }
        public bool IsForJournalist { get; set; }
    }
}