﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Passes.Models
{
    public class PassCategoryViewModel
    {

        public int PassCategoryID { get; set; }
        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Description is Required")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Template is Required")]

        [AllowHtml]
        public string Template { get; set; }

        public string Mode { get; set; }

        public bool IsActive { get; set; }

        public bool ForVehicle { get; set; }

        //public string ModifiedBy { get; set; }

        //public DateTime? ModifiedDate { get; set; }

        //public string CreatedBy { get; set; }

        //public DateTime? CreatedDate { get; set; }
    }
}