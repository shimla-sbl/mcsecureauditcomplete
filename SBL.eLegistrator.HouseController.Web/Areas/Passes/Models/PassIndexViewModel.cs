﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Passes.Models
{
    public class PassIndexViewModel
    {      
        public List<PassViewModel> PassList { get; set; }
        public int Status { get; set; }
        public string SearchText { get; set; }
        public int AssemblyId { get; set; }
        public int SessionId { get; set; }
        public int PassCategoryId { get; set; }
        public string Heading { get; set; }
        public SelectList PassCategoryList { get; set; }
    }
}