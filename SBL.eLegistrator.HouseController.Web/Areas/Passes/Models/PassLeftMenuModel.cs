﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Passes.Models
{
    public class PassLeftMenuModel
    {
        public int PassCount { get; set; }
        public int RequestedPassCount { get; set; }
        public int ApprovedPassCount { get; set; }
        public int RejectedPassCount { get; set; }
       
       
    }
}