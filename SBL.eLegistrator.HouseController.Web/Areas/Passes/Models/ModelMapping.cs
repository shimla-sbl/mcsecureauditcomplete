﻿using SBL.DomainModel.Models.Passes;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Passes.Models
{
    public static class ModelMapping
    {
        #region PassCategory
        public static PassCategoryViewModel ToViewModel(this PassCategory PassCategory, string Mode)
        {
            var ViewModel = new PassCategoryViewModel()
            {
                PassCategoryID = PassCategory.PassCategoryID,
                Name = PassCategory.Name,
                Description = PassCategory.Description,
                IsActive = PassCategory.IsActive,
                Template = PassCategory.Template,
                ForVehicle = PassCategory.ForVehicle,
                //CreatedBy = CurrentSession.UserName,
                //ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
            return ViewModel;
        }

        public static IEnumerable<PassCategoryViewModel> ToViewModel(this IEnumerable<PassCategory> PassCategoryList)
        {
            var ViewModel = PassCategoryList.Select(a => new PassCategoryViewModel()
            {
                PassCategoryID = a.PassCategoryID,
                Name = a.Name,
                Description = a.Description,
                IsActive = a.IsActive,
                Template = a.Template,
                ForVehicle = a.ForVehicle,

            });
            return ViewModel;
        }

        public static PassCategory ToDomainModel(this PassCategoryViewModel PassCategory)
        {
            var DomainModel = new PassCategory()
            {
                PassCategoryID = PassCategory.PassCategoryID,
                Name = PassCategory.Name,
                Description = PassCategory.Description,
                IsActive = PassCategory.IsActive,
                Template = PassCategory.Template,
                ForVehicle = PassCategory.ForVehicle,
                //CreatedBy = CurrentSession.UserName,
                //ModifiedBy = CurrentSession.UserName
            };
            return DomainModel;
        }

        public static IEnumerable<PassCategory> ToDomainModel(this IEnumerable<PassCategoryViewModel> PassCategoryList)
        {
            var DomainModel = PassCategoryList.Select(a => new PassCategory()
            {
                PassCategoryID = a.PassCategoryID,
                Name = a.Name,
                Description = a.Description,
                IsActive = a.IsActive,
                Template = a.Template,
                ForVehicle = a.ForVehicle,
            });
            return DomainModel;
        }

        #endregion


        #region Pass

        public static PassViewModel ToViewModel(this Pass Pass, string Mode)
        {
            var ViewModel = new PassViewModel()
            {
                Prefix = Pass.Prefix,
                Address = Pass.Address,
                ApprovedBy = Pass.ApprovedBy,
                AssemblyID = Pass.AssemblyID,
                DepartmentID = Pass.DepartmentID,
                Designation = Pass.Designation,
                Email = Pass.Email,
                GateNumber = Pass.GateNumber,
                IsActive = Pass.IsActive,
                IssueDate = Pass.IssueDate,
                Name = Pass.Name,
                OrganizationID = Pass.OrganizationID,
                OrgDeptName = Pass.OrgDeptName,
                PassCategoryID = Pass.PassCategoryID,
                PassID = Pass.PassID,
                Phone = Pass.Phone,
                Photo = Pass.Photo,
                RequestedBy = Pass.RequestedBy,
                SessionID = Pass.SessionID,
                Status = Pass.Status,
                UID = Pass.UID,
                ValidFrom = Pass.ValidFrom,
                ValidUpto = Pass.ValidUpto,
                VehicleNumber = Pass.VehicleNumber,
                Mode = Mode,
                PrefixList = GetNameTitles(),
                ForVehicle = Pass.PassCategory.ForVehicle,
                PassCategory = Pass.PassCategory
            };
            return ViewModel;
        }


        public static List<PassViewModel> ToViewModel(this List<Pass> PassList)
        {
            var ViewModel = PassList.Select(Pass => new PassViewModel()
            {
                Address = Pass.Address,
                ApprovedBy = Pass.ApprovedBy,
                AssemblyID = Pass.AssemblyID,
                DepartmentID = Pass.DepartmentID,
                Designation = Pass.Designation,
                Email = Pass.Email,
                GateNumber = Pass.GateNumber,
                IsActive = Pass.IsActive,
                IssueDate = Pass.IssueDate,
                Name = Pass.Name,
                OrganizationID = Pass.OrganizationID,
                OrgDeptName = Pass.OrgDeptName,
                PassCategoryID = Pass.PassCategoryID,
                PassID = Pass.PassID,
                Phone = Pass.Phone,
                Photo = Pass.Photo,
                RequestedBy = Pass.RequestedBy,
                SessionID = Pass.SessionID,
                Status = Pass.Status,
                UID = Pass.UID,
                ValidFrom = Pass.ValidFrom,
                ValidUpto = Pass.ValidUpto,
                VehicleNumber = Pass.VehicleNumber,
                PrefixList = GetNameTitles(),
                PassCategoryName = Pass.PassCategory.Name,
                ForVehicle = Pass.PassCategory.ForVehicle,
                PassCategory = Pass.PassCategory
            });
            return ViewModel.ToList();
        }


        public static Pass ToDomainModel(this PassViewModel Pass)
        {
            var DomainModel = new Pass()
            {
                Prefix = Pass.Prefix,
                Address = Pass.Address,
                ApprovedBy = Pass.ApprovedBy,
                AssemblyID = Pass.AssemblyID,
                DepartmentID = Pass.DepartmentID,
                Designation = Pass.Designation,
                Email = Pass.Email,
                GateNumber = Pass.GateNumber,
                IsActive = Pass.IsActive,
                IssueDate = Pass.IssueDate,
                Name = Pass.Name,
                OrganizationID = Pass.OrganizationID,
                OrgDeptName = Pass.OrgDeptName,
                PassCategoryID = Pass.PassCategoryID,
                PassID = Pass.PassID,
                Phone = Pass.Phone,
                Photo = Pass.Photo,
                RequestedBy = Pass.RequestedBy,
                SessionID = Pass.SessionID,
                Status = Pass.Status,
                UID = Pass.UID,
                ValidFrom = Pass.ValidFrom.Value,
                ValidUpto = Pass.ValidUpto.Value,
                VehicleNumber = Pass.VehicleNumber,

            };
            return DomainModel;
        }


        #endregion



        #region Private

        public static SelectList GetNameTitles()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Sri.", Value = "Sri." });
            result.Add(new SelectListItem() { Text = "Smt.", Value = "Smt." });

            return new SelectList(result, "Value", "Text");
        }
        #endregion
    }
}