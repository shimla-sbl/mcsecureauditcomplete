﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Models
{
    public class LegislationFixationModel
    {
        public LegislationFixationModel()
        {
            this.CustomSessionDateList = new List<LegislationFixationModel>();
            this.SessionDateList = new List<mSessionDate>();

        }

            public int Id { get; set; }
            public int SessionId { get; set; }

            public int AssemblyId { get; set; }

            public string SessionDate { get; set; }
            public List<mAssembly> mAssemblyList { get; set; }


           public List<mSession> sessionList { get; set; }
            public string SessionDateLocal { get; set; }
            public Nullable<System.TimeSpan> SessionTime { get; set; }

            public string SessionTimeLocal { get; set; }

            public string ModifiedBy { get; set; }
            public Nullable<System.DateTime> ModifiedWhen { get; set; }

            public string SessionDate_Local { get; set; }

            public string RotationMinister { get; set; }

            public string CreatedBy { get; set; }
            public Nullable<System.DateTime> CreatedDate { get; set; }

            public string DisplayDate { get; set; }

            public virtual ICollection<mSessionDate> SessionDateList { get; set; }
            public virtual ICollection<LegislationFixationModel> CustomSessionDateList { get; set; }


            #region Variable Declaration For Notices Grid (Pending and Published.)
            
            public int PageSize { get; set; }
            public int PageIndex { get; set; }
            public int ResultCount { get; set; }
            public int loopStart { get; set; }
            public int loopEnd { get; set; }
            public int selectedPage { get; set; }
            public int PageNumber { get; set; }
            public int RowsPerPage { get; set; }
            
            #endregion

           
    }

}