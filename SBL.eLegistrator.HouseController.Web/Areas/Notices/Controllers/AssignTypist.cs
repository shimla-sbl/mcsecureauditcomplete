﻿using Lib.Web.Mvc.JQuery.JqGrid;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Linq;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class AssignTypistController : Controller
    {
        // GET: /Notices/AssignTypist/
        public ActionResult Index()
        {

            tMemberNotice model = new tMemberNotice();
            //Get the Total count of All Type of question.
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForQuestionTypes", model);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForLegisUnstartQuestion", model);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetNotices", model);
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            int c = model.TotalStaredSubmitted + model.TotalUnStaredReceived + model.NoticeResultCount;
            model.TotalValue = c;
            return View(model);
        }

        public ActionResult PendingStaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            CurrentSession.SetSessionAlive = null;
            AssignQuestionModel model = new AssignQuestionModel();
            model.RoleName = "Vidhan Sabha Typist";
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetUserCode", model);
            model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetUserforAssigned", model);

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionID = siteSettingMod.SessionCode;
            //model.AssemblyID = siteSettingMod.AssemblyCode;
            
            return PartialView("NotAssign", model);
        }

        public JsonResult AssignQuestion(string Userid, string Starvals)
        {
            tMemberNotice Update = new tMemberNotice();
            tQuestion objModel = new tQuestion();
            objModel.QuestionValue = Starvals;
            objModel.TypistUserId = Userid;
            objModel.Message = Helper.ExecuteService("Notice", "AssignQuestions", objModel) as string;
            //Update.Message = "Update Sucessfully";
            return Json(objModel.Message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRoledList(string Userid, string TypistName, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            AssignQuestionModel model = new AssignQuestionModel();
            model.RoleName = "Vidhan Sabha Typist";
            model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetUserCode", model);
            model.UsId = Userid;
            model.TypsitUserName = TypistName;
            return View("NotAssignGrid", model);
        }

        public ActionResult GetRoledListGrid(JqGridRequest request, AssignQuestionModel customModel)
        {
            AssignQuestionModel model = new AssignQuestionModel();
            model.PAGE_SIZE = request.RecordsCount;
            model.PageIndex = request.PageIndex + 1;
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model.QuestionValue = customModel.UsId;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionID = siteSettingMod.SessionCode;
            //model.AssemblyID = siteSettingMod.AssemblyCode;
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            if (model.QuestionValue == null || model.QuestionValue == "00000000-0000-0000-0000-000000000000")
            {
                var result = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetPendingQuestionsForAssign", model);
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.tQuestionModel.AsQueryable();

                var resultForGrid = resultToSort.OrderBy<AssignQuestionModel>(request.SortingName, request.SortingOrder.ToString());

                response.Records.AddRange(from questionList in resultForGrid
                                          select new JqGridRecord<AssignQuestionModel>(Convert.ToString(questionList.QuestionID), new AssignQuestionModel(questionList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            {
                var result = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetAssignListQuestions", model);
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.tQuestionModel.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<AssignQuestionModel>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from questionList in resultForGrid
                                          select new JqGridRecord<AssignQuestionModel>(Convert.ToString(questionList.QuestionID), new AssignQuestionModel(questionList)));
                return new JqGridJsonResult() { Data = response };
            }
        }

        public JsonResult RoleBackQuestion(string Userid, string RolBvals)
        {
            tMemberNotice Update = new tMemberNotice();
            tQuestion objModel = new tQuestion();
            objModel.QuestionValue = RolBvals;
            objModel.TypistUserId = Userid;
            objModel = Helper.ExecuteService("Notice", "RoleBackQuestion", objModel) as tQuestion;
            Update.Message = "Update Sucessfully";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public ActionResult PendingUnstaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            AssignQuestionModel model = new AssignQuestionModel();
            model.RoleName = "Vidhan Sabha Typist";
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetUserCode", model);
            model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetUserforUnstaredAssigned", model);
            return PartialView("UnstaredNotAssign", model);
        }

        public ActionResult UnstartGetRoledList(string Userid, string TypistName, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            AssignQuestionModel model = new AssignQuestionModel();
            model.RoleName = "Vidhan Sabha Typist";
            model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetUserCode", model);
            model.UsId = Userid;
            model.TypsitUserName = TypistName;
            return View("UnstaredNotAssignGrid", model);
        }

        public ActionResult UnstartGetRoledListGrid(JqGridRequest request, AssignQuestionModel customModel)
        {
            AssignQuestionModel model = new AssignQuestionModel();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.QuestionValue = customModel.UsId;
            model.QuestionTypeId = 2;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);

            if (model.QuestionValue == null || model.QuestionValue == "00000000-0000-0000-0000-000000000000")
            {
                var result = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetUnstartForAssingList", model);
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.tQuestionModel.AsQueryable();

                var resultForGrid = resultToSort.OrderBy<AssignQuestionModel>(request.SortingName, request.SortingOrder.ToString());

                response.Records.AddRange(from questionList in resultForGrid
                                          select new JqGridRecord<AssignQuestionModel>(Convert.ToString(questionList.QuestionID), new AssignQuestionModel(questionList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            {
                var result = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetQuestionbyTypistId", model);
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.tQuestionModel.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<AssignQuestionModel>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from questionList in resultForGrid
                                          select new JqGridRecord<AssignQuestionModel>(Convert.ToString(questionList.QuestionID), new AssignQuestionModel(questionList)));
                return new JqGridJsonResult() { Data = response };
            }
        }

        public ActionResult GetAllStarredQuestionsGrid(JqGridRequest request, AssignQuestionModel customModel)
        {
            AssignQuestionModel model = new AssignQuestionModel();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.QuestionTypeId = 1;
            model.DepartmentId = CurrentSession.DeptID;
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = siteSettingMod.SessionCode;
            model.AssemblyID = siteSettingMod.AssemblyCode;
            var result = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetPendingQuestionsForAssign", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<AssignQuestionModel>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<AssignQuestionModel>(Convert.ToString(questionList.QuestionID), new AssignQuestionModel(questionList)));
            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult GetUnStarredQuestionsGrid(JqGridRequest request, AssignQuestionModel customModel)
        {
            AssignQuestionModel model = new AssignQuestionModel();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.QuestionTypeId = 2;
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = siteSettingMod.SessionCode;
            model.AssemblyID = siteSettingMod.AssemblyCode;
            var result = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetUnstartForAssingList", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<AssignQuestionModel>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<AssignQuestionModel>(Convert.ToString(questionList.QuestionID), new AssignQuestionModel(questionList)));
            return new JqGridJsonResult() { Data = response };
        }


        public ActionResult PendingNotice(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            NoticesAssignModel model = new NoticesAssignModel();
            model.RoleName = "Vidhan Sabha Typist";
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model = (NoticesAssignModel)Helper.ExecuteService("Notice", "GetNoticesUserCode", model);
            model = (NoticesAssignModel)Helper.ExecuteService("Notice", "GetNoticesUserforAssigned", model);

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionID = siteSettingMod.SessionCode;
            //model.AssemblyID = siteSettingMod.AssemblyCode;

            return PartialView("_NoticeNotAssign", model);
        }

        public ActionResult GetRoledNoticeListGrid(JqGridRequest request, NoticesAssignModel customModel)
        {
            NoticesAssignModel model = new NoticesAssignModel();
            model.PAGE_SIZE = request.RecordsCount;
            model.PageIndex = request.PageIndex + 1;
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model.QuestionValue = customModel.UsId;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionID = siteSettingMod.SessionCode;
            //model.AssemblyID = siteSettingMod.AssemblyCode;
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            if (model.QuestionValue == null || model.QuestionValue == "00000000-0000-0000-0000-000000000000")
            {
                var result = (NoticesAssignModel)Helper.ExecuteService("Notice", "GetPendingNoticesForAssign", model);
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.tQuestionModel.AsQueryable();

                var resultForGrid = resultToSort.OrderBy<NoticesAssignModel>(request.SortingName, request.SortingOrder.ToString());

                response.Records.AddRange(from questionList in resultForGrid
                                          select new JqGridRecord<NoticesAssignModel>(Convert.ToString(questionList.NoticeId), new NoticesAssignModel(questionList)));
                return new JqGridJsonResult() { Data = response };
            }
            else
            {
                var result = (NoticesAssignModel)Helper.ExecuteService("Notice", "GetNoticesAssignList", model);
                JqGridResponse response = new JqGridResponse()
                {
                    TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                    PageIndex = request.PageIndex,
                    TotalRecordsCount = result.ResultCount
                };

                var resultToSort = result.tQuestionModel.AsQueryable();
                var resultForGrid = resultToSort.OrderBy<NoticesAssignModel>(request.SortingName, request.SortingOrder.ToString());
                response.Records.AddRange(from questionList in resultForGrid
                                          select new JqGridRecord<NoticesAssignModel>(Convert.ToString(questionList.NoticeId), new NoticesAssignModel(questionList)));
                return new JqGridJsonResult() { Data = response };
            }
        }

        public JsonResult AssignNotices(string Userid, string Starvals)
        {
            tMemberNotice Update = new tMemberNotice();
            tMemberNotice objModel = new tMemberNotice();
            objModel.QuestionValue = Starvals;
            objModel.TypistUserId = Userid;
            objModel.Message = Helper.ExecuteService("Notice", "AssignNotices", objModel) as string;
            //Update.Message = "Update Sucessfully";
            return Json(objModel.Message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RoleBackNotices(string Userid, string RolBvals)
        {
            tMemberNotice Update = new tMemberNotice();
            tMemberNotice objModel = new tMemberNotice();
            objModel.QuestionValue = RolBvals;
            objModel.TypistUserId = Userid;
            objModel = Helper.ExecuteService("Notice", "RoleBackNotices", objModel) as tMemberNotice;
            Update.Message = "Update Sucessfully";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRoledListNotices(string Userid, string TypistName, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            NoticesAssignModel model = new NoticesAssignModel();
            model.RoleName = "Vidhan Sabha Typist";
            model = (NoticesAssignModel)Helper.ExecuteService("Notice", "GetNoticesUserCode", model);
            model.UsId = Userid;
            model.TypsitUserName = TypistName;
            return View("_NoticeNotAssignGrid", model);
        }

        //public ActionResult GetRoledListGrid(JqGridRequest request, AssignQuestionModel customModel)
        //{
        //    NoticesAssignModel model = new NoticesAssignModel();
        //    model.PAGE_SIZE = request.RecordsCount;
        //    model.PageIndex = request.PageIndex + 1;
        //    model.QuestionTypeId = (int)QuestionType.StartedQuestion;
        //    model.QuestionValue = customModel.UsId;
        //    //SiteSettings siteSettingMod = new SiteSettings();
        //    //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
        //    //model.SessionID = siteSettingMod.SessionCode;
        //    //model.AssemblyID = siteSettingMod.AssemblyCode;
        //    model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
        //    model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
        //    if (model.QuestionValue == null || model.QuestionValue == "00000000-0000-0000-0000-000000000000")
        //    {
        //        var result = (NoticesAssignModel)Helper.ExecuteService("Notice", "GetPendingNoticesForAssign", model);
        //        JqGridResponse response = new JqGridResponse()
        //        {
        //            TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
        //            PageIndex = request.PageIndex,
        //            TotalRecordsCount = result.ResultCount
        //        };

        //        var resultToSort = result.tQuestionModel.AsQueryable();

        //        var resultForGrid = resultToSort.OrderBy<NoticesAssignModel>(request.SortingName, request.SortingOrder.ToString());

        //        response.Records.AddRange(from questionList in resultForGrid
        //                                  select new JqGridRecord<NoticesAssignModel>(Convert.ToString(questionList.NoticeId), new NoticesAssignModel(questionList)));
        //        return new JqGridJsonResult() { Data = response };
        //    }
        //    else
        //    {
        //        var result = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetNoticesAssignList", model);
        //        JqGridResponse response = new JqGridResponse()
        //        {
        //            TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
        //            PageIndex = request.PageIndex,
        //            TotalRecordsCount = result.ResultCount
        //        };

        //        var resultToSort = result.tQuestionModel.AsQueryable();
        //        var resultForGrid = resultToSort.OrderBy<AssignQuestionModel>(request.SortingName, request.SortingOrder.ToString());
        //        response.Records.AddRange(from questionList in resultForGrid
        //                                  select new JqGridRecord<AssignQuestionModel>(Convert.ToString(questionList.QuestionID), new AssignQuestionModel(questionList)));
        //        return new JqGridJsonResult() { Data = response };
        //    }
        //}
    }
}
