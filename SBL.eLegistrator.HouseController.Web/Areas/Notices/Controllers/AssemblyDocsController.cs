﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lib.Web.Mvc.JQuery.JqGrid;

using SBL.eLegistrator.HouseController.Web.Utility;

using SBL.DomainModel.Models.PaperLaid;

using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Bill;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Committee;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    public class AssemblyDocsController : Controller
    {
        //
        // GET: /Notices/Bills/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _QuestionSearch()
        {
            SearchModel objSearch = new SearchModel();
            objSearch.AssemblyList = AssemblyList();
            objSearch.SessionList = new List<mSession>();
            objSearch.MinisterList = MinisterList();
            objSearch.mMinistryDepartments = new List<SearchModel>();
            objSearch.mMinistryDepartments.Add(new SearchModel { Department_Id = null, DepartmentName = "Select Department" });
            //objSearch.DepartmentList = DepartmentList();     
            objSearch.QuestionTypeList = QuestionTypeList();
            objSearch.MemberList = MemberList();
            objSearch.Subject = "  Type Subject Here";
            //objSearch.SubjectList = SubjectList();
            objSearch.SubjectList = new List<string>();
            return PartialView(objSearch);
        }
        public ActionResult _DebatesProceedingsSearch()
        {
            SearchModel objSearch = new SearchModel();
            objSearch.AssemblyList = AssemblyList();
            objSearch.SessionList = new List<mSession>();
            objSearch.MinisterList = MinisterList();
            objSearch.DepartmentList = DepartmentList();
            objSearch.QuestionTypeList = QuestionTypeList();
            objSearch.MemberList = MemberList();
            objSearch.MemberList[0].Name = "Select Member/Minister";
            objSearch.Subject = "  Type Subject Here";
            objSearch.SubjectList = SubjectListDebate();
            objSearch.EventList = EventList();
            return PartialView(objSearch);
        }
        [HttpGet]
        public ActionResult _QuestionsSearchResult(SearchModel model)
        {
            model = (SearchModel)Helper.ExecuteService("Questions", "GetSearchedQuestionsHistory", model);
            return PartialView(model);
        }
        [HttpGet]
        public ActionResult _GetPdf(string FilePath)
        {
            FilePath = "/AssemblyFiles/" + FilePath + ".pdf";
            ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);
            return PartialView();
        }

        [HttpGet]
        public ActionResult _GetDebatePdf(string FilePath)
        {
            string[] FilePaths = FilePath.Split('|');
            //FilePath = "/AssemblyFiles/" + FilePath + "/" + "7.pdf";
            FilePath = "/AssemblyFiles/" + FilePaths[0] + "/" + "8.pdf";
            ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);
            //ViewBag.PDFPathD = Microsoft.Security.Application.Encoder.UrlPathEncode("");            
            if (FilePaths[1] == "1")
                return PartialView("_GetPDFdq");
            return PartialView("_GetPdf");
        }
        [HttpGet]
        public ActionResult _GetDebatePdf3(string FilePath)
        {
            FilePath = "/AssemblyFiles/" + FilePath + "/" + "8.pdf";
            ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);
            return PartialView("_GetPdf2");
        }
        [HttpGet]
        public ActionResult _GetQuestionPdf2(string FilePath, string FilePathD)
        {
            string[] FilePaths = FilePath.Split('|');
            FilePath = FilePaths[0];
            FilePathD = FilePaths[1];
            FilePath = "/AssemblyFiles/" + FilePath + ".pdf";
            ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);
            ViewBag.PDFPathD = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePathD);
            return PartialView("_GetPdfTemp");
        }
        [HttpGet]
        public ActionResult _GetQuestionPdf3(string FilePath, string FilePathD)
        {
            string[] FilePaths = FilePath.Split('|');
            FilePath = FilePaths[0];
            FilePathD = FilePaths[1];
            FilePath = "/AssemblyFiles/" + FilePath + ".pdf";
            FilePathD = "/AssemblyFiles/" + FilePathD + "/" + "8.pdf";
            ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);
            ViewBag.PDFPathD = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePathD);
            return PartialView("_GetPDF4");
        }
        [HttpGet]
        public ActionResult _GetQuestionPNG(string FilePath, string FilePathD)
        {
            string[] FilePaths = FilePath.Split('|');
            FilePath = FilePaths[0];
            FilePathD = FilePaths[1];
            FilePath = "/AssemblyFiles/" + FilePath + ".pdf";
            FilePathD = "/AssemblyFiles/" + FilePathD + "/" + "8.pdf";
            ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);
            ViewBag.PDFPathD = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePathD);
            return PartialView();
        }
        //[HttpGet]
        //public ActionResult _GetQuestionPNG(string FilePath, string FilePathD)
        //{
        //    string[] FilePaths = FilePath.Split('|');
        //    FilePath = FilePaths[0];
        //    FilePathD = FilePaths[2];
        //    FilePath = "/AssemblyFiles/" + FilePath.Substring(0, (FilePath.Length - 1));
        //    ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);

        //    ViewBag.PDFPathD = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePathD);
        //    return PartialView("_GetPDF4");
        //}
        [HttpGet]
        public ActionResult _GetDebatePdf2(string FilePath, string FilePathD)
        {
            string[] FilePaths = FilePath.Split('|');
            FilePath = FilePaths[1];
            FilePath = "/AssemblyFiles/" + FilePath + "/" + "7.pdf";
            ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);
            return PartialView("_GetPdfTemp");
        }
        [HttpGet]
        public ActionResult _GetBillPdf(string FilePath)
        {
            FilePath = "";
            //FilePath = "/AssemblyFiles/" + FilePath + "/" + "7.pdf";
            //ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);
            return PartialView("_GetPdf");
        }
        [HttpGet]
        public ActionResult _DebatesSearchResult(SearchModel model)
        {
            model = (SearchModel)Helper.ExecuteService("Questions", "GetSearchedDebates", model);
            return PartialView(model);
        }


        [HttpGet]
        public ActionResult _CommitteeReportResult(CommitteeSearchModel model)
        {
            model = (CommitteeSearchModel)Helper.ExecuteService("Committee", "GetSearchedCommittee", model);
            return PartialView(model);

        }
        //[HttpGet]

        //public ActionResult _CommitteeReportResult(CommitteeSearchModel model)
        //{
        //    model = (CommitteeSearchModel)Helper.ExecuteService("Committee", "GetSearchedCommitteeReport", model);
        //    return PartialView(model);
        //}

        //public ActionResult _CommitteeReportResult(CommitteeSearchModel model)
        //{
        //SearchModel model = new SearchModel();

        //if (!string.IsNullOrEmpty(SearchEntry))
        //{
        //int EnteredIntValue = 0;
        //bool IsIntOrNot = false;
        //IsIntOrNot = int.TryParse(SearchEntry, out EnteredIntValue);
        //if (IsIntOrNot)
        //{
        //    //SearchModel model = new SearchModel();
        //    //model.Search = SearchEntry;
        //    model.AssemblyID = Convert.ToInt32(SearchEntry);
        //    model.SessionId = Convert.ToInt32(SearchEntry);
        //    // model.Event = SearchEntry;
        //    //model.Subject = SearchEntry;
        //    model = (SearchModel)Helper.ExecuteService("Questions", "GetSearchedData", model);
        //}
        //else
        //    {
        //        model = (CommitteeSearchModel)Helper.ExecuteService("Committee", "GetSearchedCommitteeReport", model);
        //    }
        //}

        //    return PartialView(model);
        //}

        [HttpGet]
        public ActionResult ReportNoList(int CommitteeId)
        {
            tCommitteeReport comitereprt = new tCommitteeReport();
            comitereprt.CommitteeId = CommitteeId;
            var ReportNoList = Helper.ExecuteService("Committee", "GetAllReportNoList", comitereprt);
            return Json(ReportNoList, JsonRequestBehavior.AllowGet);




        }


        [HttpGet]
        public ActionResult _GetPdfCommittee(string FilePath)
        {
            //FilePath = "/Documents/11/" + FilePath + ".pdf";
            FilePath = "/AssemblyFiles/" + FilePath + ".pdf";
            ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);
            return PartialView("_GetPdf");
        }
        [HttpGet]
        public ActionResult _CommitteeReportSearch()
        {
            CommitteeSearchModel objSearch = new CommitteeSearchModel();
            objSearch.AssemblyList = AssemblyList();
            //objSearch.SessionList = new List<mSession>();
            objSearch.MemberList = MemberList();
            objSearch.AllReporttypeList = AllReporttypeList();
            //objSearch.DateOfCreation = DateOfCreation();
            objSearch.MemberList = MemberList();
            objSearch.CommitteeTypeList = CommitteeTypeList();
            objSearch.ReportNoList = ReportNoList();
            objSearch.OriginalReportNoList = OriginalReportNoList();
            //objSearch.PeriodList = PeriodList();
            objSearch.AllTitleList = AllTitleList();
            //objSearch.Subject = "       Type Subject Here";
            objSearch.ChairmanList = ChairmanList();
            objSearch.SessionList = new List<mSession>();
            objSearch.CommitteeList = new List<tCommittee>();
            //objSearch.SubjectList = new List<string>();
            //objSearch.CommitteeReportTypeList = CommitteeReportTypeList();
            return PartialView(objSearch);

        }

        [HttpGet]

        public List<mCommitteeType> CommitteeTypeList()
        {
            mCommitteeType perid = new mCommitteeType();
            var mCommiteTypLis = (List<mCommitteeType>)Helper.ExecuteService("Committee", "GetAllCommiteeType", perid);
            mCommiteTypLis.Insert(0, new mCommitteeType { CommitteeTypeName = "Select CommitteeTypeName." });
            return (mCommiteTypLis);
        }

        [HttpGet]

        public List<mAssembly> PeriodList()
        {
            mAssembly perid = new mAssembly();
            var periodList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllPeriods", perid);
            periodList.Insert(0, new mAssembly { Period = "Select Period." });
            return (periodList);
        }
        //for all 
        [HttpGet]

        public List<mAssembly> AssemblyList()
        {
            mAssembly assembly = new mAssembly();
            var assemblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", assembly);
            assemblyList.Insert(0, new mAssembly { AssemblyName = "Select Assembly" });
            return (assemblyList);
        }
        [HttpGet]
        //public ActionResult AssemblyNameList(int CommitteeId)
        //{
        //    tCommitteeReport comitereprt = new tCommitteeReport();
        //    comitereprt.CommitteeId = CommitteeId;
        //    var AssemblyList = Helper.ExecuteService("Committee", "GetAllAssemblyReverseList", comitereprt);
        //    return Json(AssemblyList, JsonRequestBehavior.AllowGet);


        //}

        public List<CommitteeSearchModel> ChairmanList()
        {
            tCommitteeReport comitereprt = new tCommitteeReport();

            var ChairmanList = (List<CommitteeSearchModel>)Helper.ExecuteService("Committee", "GetAllChairmanList", comitereprt);
            ChairmanList.Insert(0, new CommitteeSearchModel { ChairmanName = "Select Chairman" });
            return (ChairmanList);

        }
        public ActionResult ChairmanList(int CommitteeId)
        {
            tCommitteeReport comitereprt = new tCommitteeReport();
            comitereprt.CommitteeId = CommitteeId;
            var ChairmanList = Helper.ExecuteService("Committee", "GetChairmanByCommId", comitereprt);
            return Json(ChairmanList, JsonRequestBehavior.AllowGet);
        }

        public List<tCommitteeReportType> AllReporttypeList()
        {
            tCommitteeReportType comitereprttype = new tCommitteeReportType();

            var ReporttypeList = (List<tCommitteeReportType>)Helper.ExecuteService("Committee", "GetReporttypeList", comitereprttype);
            ReporttypeList.Insert(0, new tCommitteeReportType { ReportTypeName = "Select Report Type" });
            return (ReporttypeList);


        }
        public ActionResult ReporttypeList(int CommitteeId)
        {
            tCommitteeReport comitereprt = new tCommitteeReport();
            comitereprt.CommitteeId = CommitteeId;
            var ReporttypeList = Helper.ExecuteService("Committee", "GetAllReporttypeList", comitereprt);
            return Json(ReporttypeList, JsonRequestBehavior.AllowGet);


        }
        //public ActionResult CommitteeList(int AssemblyId )
        //{
        //    tCommittee Committee = new tCommittee();
        //    Committee.AssemblyID = AssemblyId;
        //    var CommitteeList = Helper.ExecuteService("Committee", "GetCommittee", Committee);
        //    return Json(CommitteeList, JsonRequestBehavior.AllowGet);

        //}

        public ActionResult CommitteeList(int AssemblyId, int CommitteeTypeId)
        {
            tCommittee Committee = new tCommittee();
            Committee.AssemblyID = AssemblyId;
            Committee.CommitteeTypeId = CommitteeTypeId;
            var CommitteeList = Helper.ExecuteService("Committee", "GetCommitteeAssemlyComType", Committee);
            return Json(CommitteeList, JsonRequestBehavior.AllowGet);

        }



        //public ActionResult _CommitteeReportSearch()
        //{
        //    return PartialView();
        //}
        public ActionResult _BulletinsSearch()
        {
            return PartialView();
        }

        //public List<tCommitteeReportType> CommitteeReportTypeList()
        //{
        //    // List<tCommitteeReportType> nnn = new List<tCommitteeReportType>();

        //    tCommitteeReportType Chairman = new tCommitteeReportType();
        //    var ReportTypeList = (List<tCommitteeReportType>)Helper.ExecuteService("Committee", "GetCommitteeReportTypeList", Chairman);
        //    ReportTypeList.Insert(0, new tCommitteeReportType { ReportTypeName = "Select Report Type " });
        //    return ReportTypeList;

        //}

        /// <summary>
        /// Select list of assembly
        /// </summary>
        /// <returns>mAssembly</returns>

        public List<tQuestionType> QuestionTypeList()
        {
            tQuestionType questionType = new tQuestionType();
            var questionTypeList = (List<tQuestionType>)Helper.ExecuteService("Questions", "GetQuestionTypes", questionType);
            questionTypeList.Insert(0, new tQuestionType { QuestionTypeName = "QuestionType" });
            return questionTypeList;
        }
        public List<mMinsitryMinister> MinisterList()
        {
            mMinsitryMinister ministryMinister = new mMinsitryMinister();
            var ministerList = (List<mMinsitryMinister>)Helper.ExecuteService("MinistryMinister", "GetAllMinsitryMinister", ministryMinister);
            ministerList.Insert(0, new mMinsitryMinister { MinisterName = "Select Minister" });
            return ministerList;
        }
        public List<mDepartment> DepartmentList()
        {
            mDepartment department = new mDepartment();
            var departmentList = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", department);
            departmentList.Insert(0, new mDepartment { deptname = "Select Department" });
            return departmentList;
        }
        public List<mMember> MemberList()
        {
            mMember member = new mMember();
            var memberList = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembers", member);
            memberList.Insert(0, new mMember { Name = "Select Member" });
            return memberList;
        }
        [HttpGet]
        public ActionResult MemberList(int CommitteeId)
        {
            tCommitteeReport comitereprt = new tCommitteeReport();
            comitereprt.CommitteeId = CommitteeId;
            var memberList = Helper.ExecuteService("Committee", "ShowCommitteMemberByCommitteeId", CommitteeId);
            return Json(memberList, JsonRequestBehavior.AllowGet);

        }




        public List<String> SubjectList()
        {
            tQuestion questionSubjects = new tQuestion();
            var SubjectList = (List<String>)Helper.ExecuteService("Questions", "GetQuestionSubjects", questionSubjects);
            return SubjectList;
        }
        public List<String> SubjectListDebate()
        {
            tQuestion questionSubjects = new tQuestion();
            var SubjectList = (List<String>)Helper.ExecuteService("Questions", "GetDebatesSubjects", questionSubjects);
            return SubjectList;
        }
        public List<mEvent> EventList()
        {
            mEvent events = new mEvent();
            var EventList = (List<mEvent>)Helper.ExecuteService("Events", "GetEventsList", events);
            EventList.Insert(0, new mEvent { EventName = "Select Business" });
            return EventList;
        }
        //[HttpGet]
        //public ActionResult ministerDepartmentList(int AssemblyId)
        //{
        //    mMinistryDepartment department = new mMinistryDepartment();
        //    department.AssemblyID = AssemblyId;
        //    var departmentList = Helper.ExecuteService("Department", "GetAllMinistryDepartment", AssemblyId);
        //    return Json(departmentList, JsonRequestBehavior.AllowGet);
        //}


        [HttpGet]
        public ActionResult SubjectSearchCommittee(string subjectKey)
        {
            //tQuestion questionSubjects = new tQuestion();
            //questionSubjects.Subject = subjectKey;
            var SubjectList = (List<String>)Helper.ExecuteService("Committee", "GetCommitteSubjects_key", subjectKey);
            return Json(SubjectList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult SubjectSearch(string subjectKey)
        {
            //tQuestion questionSubjects = new tQuestion();
            //questionSubjects.Subject = subjectKey;
            var SubjectList = (List<String>)Helper.ExecuteService("Questions", "GetQuestionSubjects_key", subjectKey);
            return Json(SubjectList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult SessionList(int AssemblyId)
        {
            mSession msession = new mSession();

            msession.AssemblyID = AssemblyId;


            var msessionList = Helper.ExecuteService("Session", "GetSessionsByAssemblyID", msession);
            return Json(msessionList, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public List<String> AllTitleList()
        {
            tCommitteeReport comitereprt = new tCommitteeReport();
            //comitereprt.CommitteeId = CommitteeId;
            var TitleList = (List<String>)Helper.ExecuteService("Committee", "GetSearchedCommitteeGetTitleList", comitereprt);
            TitleList.Insert(0, "Select Title");
            return (TitleList);



        }
        //GetReportNoList

        //[HttpGet]
        public List<CommitteeSearchModel> ReportNoList()
        {
            tCommitteeReport comitereprt = new tCommitteeReport();
            //comitereprt.CommitteeId = CommitteeId;
            var TitleList = (List<CommitteeSearchModel>)Helper.ExecuteService("Committee", "GetReportNoList", comitereprt);
            TitleList.Insert(0, new CommitteeSearchModel { ReportNo = "Select ReportNo" });

            //TitleList.Add(repo
            return (TitleList);



        }


        public List<CommitteeSearchModel> OriginalReportNoList()
        {
            tCommitteeReport comitereprt = new tCommitteeReport();
            //comitereprt.CommitteeId = CommitteeId;
            var TitleList = (List<CommitteeSearchModel>)Helper.ExecuteService("Committee", "GetReportNoList", comitereprt);
            TitleList.Insert(0, new CommitteeSearchModel { OriginalReportNo = "Select OriginalReportNo" });

            //TitleList.Add(repo
            return (TitleList);



        }



        [HttpGet]
        public ActionResult TitleList(int CommitteeId)
        {
            tCommitteeReport comitereprt = new tCommitteeReport();
            comitereprt.CommitteeId = CommitteeId;
            var TitleList = Helper.ExecuteService("Committee", "GetAllTitleList", comitereprt);
            return Json(TitleList, JsonRequestBehavior.AllowGet);



        }
        //for dateoflaying



        //for period populate

        [HttpGet]
        public ActionResult CommittePeriodList(int CommitteeId)
        {
            tCommitteeReport comitereprt = new tCommitteeReport();
            comitereprt.CommitteeId = CommitteeId;
            var PeriodList = Helper.ExecuteService("Committee", "GetAllPeriodList", comitereprt);
            return Json(PeriodList, JsonRequestBehavior.AllowGet);

        }


        //[HttpGet]
        //public ActionResult Dateoflayinglist()
        //{
        //    tCommitteeReport comitereprt = new tCommitteeReport();

        //    var Dateoflayinglist = Helper.ExecuteService("Committee", "GetAllDateOfLaying", comitereprt);
        //    return Json(Dateoflayinglist, JsonRequestBehavior.AllowGet);

        //}




        [HttpGet]
        public ActionResult SessionDates(int AssemblyId, int SessionId)
        {
            mSessionDate msessiondate = new mSessionDate();
            msessiondate.AssemblyId = AssemblyId;
            msessiondate.SessionId = SessionId;
            if (SessionId == 0)
            {
                var sessiondates = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateAssembly", msessiondate);
                return Json(sessiondates, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var sessiondates = Helper.ExecuteService("Session", "GetSessionDate", msessiondate);
                return Json(sessiondates, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult _DebatePages(string ImagePath)
        {
            ViewBag.ImagePath = "/AssemblyFiles/" + ImagePath;
            return PartialView();
        }
        [HttpGet]
        public ActionResult _DebatesQImages(string ImagePath)
        {
            try
            {
                string[] QDebates = string.IsNullOrEmpty(ImagePath) ? "||||".Split('|') : ImagePath.Split('|');
                string ImageQ = @"/AssemblyFiles/" + QDebates[0] + ((QDebates[3].Trim() == "1") ? "Starred/" : "Unstarred/") + QDebates[4] + "/";
                //ViewBag.ImagePath = "/AssemblyFiles/" + ImagePath;
                ViewBag.ImagePath = @"/AssemblyFiles/" + QDebates[0] + "8/|" + QDebates[1] + "|" + QDebates[2];

                bool flag = Directory.Exists(Server.MapPath(ImageQ));
                string temp = "";
                if (flag)
                {
                    string[] paths = Directory.GetFiles(Server.MapPath(ImageQ));
                    for (int i = 0; i < paths.Count(); i++)
                    {
                        string[] file = paths[i].Split('\\');
                        temp += file[(file.Count() - 1)].Substring(0, file[(file.Count() - 1)].Length - 4);
                        if (i != paths.Count() - 1)
                            temp += ",";
                    }
                }

                ViewBag.ImagePathQ = ImageQ + "|" + temp;
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            { }
            return PartialView();
        }
        [HttpGet]
        public ActionResult _QDebatesImages(string ImagePath)
        {
            string[] QDebates = string.IsNullOrEmpty(ImagePath) ? "|".Split('|') : ImagePath.Split('|');
            //bool flag = Directory.Exists(Server.MapPath(@"/AssemblyFiles/12/6/20140806/Starred/370/"));
            QDebates[0] = @"/AssemblyFiles/" + QDebates[0];
            QDebates[1] = @"/AssemblyFiles/" + QDebates[1];
            string ttt = Server.MapPath(QDebates[0]);
            bool flag = Directory.Exists(Server.MapPath(QDebates[0]));
            string temp = "";
            if (flag)
            {
                string[] paths = Directory.GetFiles(Server.MapPath(QDebates[0]));
                for (int i = 0; i < paths.Count(); i++)
                {
                    string[] file = paths[i].Split('\\');
                    temp += file[(file.Count() - 1)].Substring(0, file[(file.Count() - 1)].Length - 4);
                    if (i != paths.Count() - 1)
                        temp += ",";
                }
            }
            //string[] tjk = "|".Split('|');
            //ViewBag.ImagePath = "/AssemblyFiles/" + ImagePath;
            ViewBag.ImagePath = QDebates[0] + "/|" + temp;
            ViewBag.ImagePathD = QDebates[1] + "/8/|" + QDebates[2];
            return PartialView();
        }
        [HttpGet]
        public ActionResult _QNoPDF(string QnoPath)
        {
            string[] path = QnoPath.Split('|');
            ViewBag.QnoPath = "/AssemblyFiles/" + path[0].ToString().Substring(0, path[0].ToString().Length - 2) + "Starred/370.pdf";
            return PartialView();
        }
    }
}
