﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.AssemblyFileSystem;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    public class MemberAttendanceController : Controller
    {
        //
        // GET: /Notices/MemberAttendance/

        public ActionResult Index()
        {
            AssemblyFileViewModel model = new AssemblyFileViewModel();

            //Code for AssemblyList 
            var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

            //Code for SessionList 
            SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
            data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
            var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

            //Code for SessionDateList 

            mSessionDate sesdate = new mSessionDate();
            sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
            sesdate.AssemblyId = data.AssemblyID;
            model.VAssemblyId = data.AssemblyID;
            model.VSessionId = sesdate.SessionId;
            var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);


            // Code for DoumentTypeList
            var doctypeList = (List<mAssemblyTypeofDocuments>)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyTypeofDocumentLst", null);
            model.SessionDateList = sessionDateList;
            model.AssemblyList = assmeblyList;
            model.SessionList = sessionList;
            model.AssemblyDocumentTypeList = doctypeList;
            model.StatusTypeList = ModelMapping.GetStatusTypes();
            model.Mode = "Add";

           // return View("MemberAttendence", model);

            return PartialView("Index",model);
        }


        public ActionResult AddIndex()
        {
            AssemblyFileViewModel model = new AssemblyFileViewModel();

            //Code for AssemblyList 
            var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

            //Code for SessionList 
            SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
            data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
            var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

            //Code for SessionDateList 

            mSessionDate sesdate = new mSessionDate();
            sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
            sesdate.AssemblyId = data.AssemblyID;
            model.VAssemblyId = data.AssemblyID;
            model.VSessionId = sesdate.SessionId;
            var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);


            // Code for DoumentTypeList
            var doctypeList = (List<mAssemblyTypeofDocuments>)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyTypeofDocumentLst", null);
            model.SessionDateList = sessionDateList;
            model.AssemblyList = assmeblyList;
            model.SessionList = sessionList;
            model.AssemblyDocumentTypeList = doctypeList;
            model.StatusTypeList = ModelMapping.GetStatusTypes();
            model.Mode = "Add";

            return PartialView("AddIndex", model);
        }

        public ActionResult GetofflineAttendance(int sessionDateId, int assemblyId, int sessionId, string SessionDate)
        {
            try
            {
                mSessionDate obj = new mSessionDate();
                obj.SessionId = sessionId;
                obj.AssemblyId = assemblyId;
                obj.Id = sessionDateId;

                var data = (List<MemberAttendanceViewModel>)Helper.ExecuteService("Notice", "GetOfflineMemberAttendance", obj);

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                ViewBag.FileLocation = FileSettings.SettingValue;
                ViewBag.datetime = SessionDate;

                return PartialView("_MemberAttendanceDetails", data);
            }
            catch (System.Exception)
            {
                
                throw;
            }
        }

        //public ActionResult SaveofflineAttendance(FormCollection model)
        //{
        //    try
        //    {
        //        //foreach (var a in model)
        //        //{
        //        //   var id = 
        //        //    Helper.ExecuteService("Notice", "SaveOfflineMemberAttendance", a);
        //        //}

        //        string PCRow = collection["hdnPCRow"];

        //        Helper.ExecuteService("Notice", "SaveOfflineMemberAttendance", model);
        //        return null;
        //    }
        //    catch (System.Exception)
        //    {

        //        throw;
        //    }
        //}

        public JsonResult SaveofflineAttendance(int AssemblyId, int SessionId, string Date, string AIds)
        {
            MemberAttendanceViewModel model = new MemberAttendanceViewModel();
            model.AssemblyID=AssemblyId;
            model.sessionId=SessionId;
            model.AttenIds =AIds;
            
            if (Date.IndexOf("-") > 0)
            {

                var Day = Date.Substring(0, Date.IndexOf("-"));
                var Month = Date.Substring(Date.IndexOf("-") + 1, (Date.LastIndexOf("-") - Date.IndexOf("-") - 1));
                var Year = Date.Substring(Date.LastIndexOf("-") + 1, 4);               
                var SDate = Year + "-" + Month + "-" + Day;
                model.Date = SDate;
            }
            Helper.ExecuteService("Notice", "SaveOfflineMemberAttendance", model);

            return Json(JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteofflineAttendance(int AssemblyId, int SessionId, string Date, int membercode)
        {
            MemberAttendanceViewModel model = new MemberAttendanceViewModel();
            model.AssemblyID = AssemblyId;
            model.sessionId = SessionId;
            model.MemberCode = membercode;

            if (Date.IndexOf("-") > 0)
            {

                var Day = Date.Substring(0, Date.IndexOf("-"));
                var Month = Date.Substring(Date.IndexOf("-") + 1, (Date.LastIndexOf("-") - Date.IndexOf("-") - 1));
                var Year = Date.Substring(Date.LastIndexOf("-") + 1, 4);
                //var  SDate = Day + "/" + Month + "/" + Year;
                var SDate = Year + "-" + Month + "-" + Day;
                model.Date = SDate;
            }
            Helper.ExecuteService("Notice", "deleteOfflineMemberAttendance", model);

            return Json(JsonRequestBehavior.AllowGet);
        }
    }
}
