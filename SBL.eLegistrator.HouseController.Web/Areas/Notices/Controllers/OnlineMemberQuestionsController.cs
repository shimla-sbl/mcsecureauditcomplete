﻿using EvoPdf;
using iTextSharp.text;
using Microsoft.Office.Interop.Excel;
using Microsoft.Security.Application;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.AlbumCategory;
using SBL.DomainModel.Models.Category;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Gallery;
using SBL.DomainModel.Models.Grievance;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.OTP;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.SystemModule;
using SBL.DomainModel.Models.Tour;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models;
using SBL.eLegistrator.HouseController.Web.EmailWebService;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SMS.API;
using SMSServices;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Activity;

//using SM.ListToExcel.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class OnlineMemberQuestionsController : Controller
    {
        //
        // GET: /Notices/OnlineMemberQuestions/

        public ActionResult GetDivData()
        {
            return PartialView("_DivData");
        }

        public ActionResult Index()
        {
            tMemberNotice model = new tMemberNotice();

            if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
            {
                model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            }

            model.UserID = new Guid(CurrentSession.UserID);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }

            //var profiler = MiniProfiler.Current; // it's ok if this is null

            //using (profiler.Step("Starred Online Time"))
            //{}
            //model = (tMemberNotice)Helper.ExecuteService("Notice", "GetAllCountForOnlineSubmission", model);

            //current cmnt
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDynamicMainMenuByUserId", model);
            if (!string.IsNullOrEmpty(CurrentSession.SubUserTypeID))
            {
                if (CurrentSession.SubUserTypeID == "38")
                {
                    model = FilterMenuByMemberCode(model);
                    mUsers usermodel = new mUsers();
                    usermodel.UserName = CurrentSession.UserName;
                    model.memberList = (List<mMember>)Helper.ExecuteService("Notice", "GetMemberNameByIds", usermodel);
                }
            }
            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyID.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionID.ToString();
            }

            if (Convert.ToString(TempData["Msg"]) != "")
            {
                model.Message = TempData["Msg"].ToString();
            }
            else
            {
                model.Message = "";
            }

            if ((string)TempData["CallingMethod"] == "Starred")
            {
                model.Msg = "Starred";
            }
            else if ((string)TempData["CallingMethod"] == "Unstarred")
            {
                model.Msg = "Unstarred";
            }
            else if ((string)TempData["CallingMethod"] == "Notice")
            {
                model.Msg = "Notice";
            }
            else
            {
                model.Msg = "";
            }

            ViewBag.MenuId = CurrentSession.MenuId;

            return View(model);
        }

        public ActionResult GetAllOnlineSubmission()
        {
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetAllOnlineSubmission", model);
            return PartialView("_AllOnlineSubmission", model);
        }
        /// <summary>
        /// //new method for change member 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public tMemberNotice FilterMenuByMemberCode(tMemberNotice model)
        {
            int membercode = Convert.ToInt32(CurrentSession.MemberCode);
            model.MenuList = model.MenuList.Where(x => x.MemberId == membercode).ToList();
            return model;
        }

        public ActionResult ChangeMember(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                CurrentSession.MemberCode = id;
            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }
        public ActionResult StaredQuestion()
        {
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetOnlineStarredQuestions", model);
            return PartialView("StarredQuestions", model);
        }

        public ActionResult NewEntryForm(string PaperType, int AssemId, int SessId)
        {
            if (PaperType == "Category Gallery")
            {
                //return PartialView("_GetAllMemberCategory");
                return RedirectToAction("CreateMemberCategory");
            }
            else if (PaperType == "Album Gallery")
            {
                return RedirectToAction("CreateGalleryAlbum");
            }
            else if (PaperType == "Gallery")
            {
                return RedirectToAction("CreateGallery");
            }
            else if (PaperType == "Today Tours" || PaperType == "Upcoming Tours"
            || PaperType == "Previous Tours" || PaperType == "UnPublished Tours")
            {
                return RedirectToAction("EntryFormTour");
            }
            else if (PaperType == "Generic Groups")
            {
                return new EmptyResult();   //RedirectToAction("AddRecipientGroup");
            }
            else if (PaperType == "Constituency Groups")
            {
                return RedirectToAction("AddConstituencyGroup");
            }
            else if (PaperType == "Panchayat Groups")
            {
                return RedirectToAction("AddPanchayatGroup");
            }
            else if (PaperType == "Generic Group Members")
            {
                return RedirectToAction("AddRecipientGroupMember");
            }
            else if (PaperType == "Constituency Group Members")
            {
                return RedirectToAction("AddConstituencyGroupMember");
            }
            else if (PaperType == "Panchayat Group Members")
            {
                return RedirectToAction("AddPanchayatGroupMember");
            }

            //New 12 feb 2015
            else if (PaperType == "ConstituencyOfficeGroups")
            {
                return RedirectToAction("AddConstituencyOfficeGroup");
            }
            else if (PaperType == "ConstituencyOfficeGroupMembers")
            {
                return RedirectToAction("AddConstituencyOfficeGroupMember");
            }
            else if (PaperType == "Councillor Diary")
            {
                MlaDiary model = new MlaDiary();
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                model.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
                var a = CurrentSession.SPMinId;
                //  model.memMinList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;  //GetDepartmentByMinistery
                //  model.DeptList = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment_maaped", model);  //GetAllDepartment
                // model.Districtist = (List<SBL.DomainModel.Models.District.DistrictModel>)Helper.ExecuteService("District", "GetAllDistrict", null);
                // model.DeptId = CurrentSession.DeptID;
                //model.OfficeList = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOffice", null);
                model.MappedMemberList = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "Get_MappedMemberList", model);
                string[] str = new string[3];
                str[0] = CurrentSession.AadharId;
                str[1] = Convert.ToString(model.SubDivisionId);
                str[2] = Convert.ToString(model.MlaCode);
                string[] strOutput = (string[])Helper.ExecuteService("Diary", "GetDistrict_ByAAdharID", str);
                if (strOutput != null)
                {
                    model.distcd = Convert.ToInt16(strOutput[0]);

                }
                model.DocumentTypelist = DocumentTypelist;
                model.ActionTypeList = ActionTypelist;
                model.FinanCialYearList = GetFinancialYearList();
                model.PaperEntryType = "NewEntry";
                model.BtnCaption = "Save";
                ////////////////////////////////////////////////////////////
                RecipientGroup Rmodel = new RecipientGroup();   // SubdivisionID
                Rmodel.DistId = Convert.ToString(model.distcd);
                Rmodel.DeptId = CurrentSession.DeptID;
                Rmodel.MemberCode = Convert.ToInt16(CurrentSession.MemberCode);
                Rmodel.AssId = Convert.ToInt16(CurrentSession.AssemblyId);
                if (CurrentSession.SubDivisionId == "")
                {
                    string constituencyID = CurrentSession.ConstituencyID;
                    string[] str1 = new string[2];
                    str1[0] = CurrentSession.AssemblyId;

                    str1[1] = constituencyID;
                    string[] strOutput1 = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str1);
                    if (strOutput1 != null)
                    {
                        constituencyID = Convert.ToString(strOutput1[0]);
                        //CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                    }
                    Rmodel.ConstituencyCode = Convert.ToInt16(constituencyID);
                    Rmodel.ConstituencyCodeForSubdivision = "0";
                }
                else
                {
                    Rmodel.ConstituencyCodeForSubdivision = CurrentSession.ConstituencyID;
                }

                model.OfficeList = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOfficemapped_ByDeptDistrict", Rmodel);
                //ViewData["multipleofc"] = model.OfficeList;
                //  var oflist = Helper.ExecuteService("ContactGroups", "GetAllOfficemapped_ByDeptDistrict", model);
                // List<fillListGenric1> _list = (List<fillListGenric1>)Helper.ExecuteService("ContactGroups", "GetAllOfficemapped_ByDeptDstForDialog", model);
                //  ViewData["multipleofc"] = _list;
                return PartialView("_MlaDiaryEntry", model);
            }
            else
            {
                DiaryModel model = new DiaryModel();
                model.PaperEntryType = PaperType;
                model.UserId = new Guid(CurrentSession.UserID);
                //model = (DiaryModel)Helper.ExecuteService("Notice", "NewEntryForm", model);

                List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                if (PaperType.ToLower() == "resolutions")
                    methodParameter.Add(new KeyValuePair<string, string>("@PaperCategoryTypeId", "4"));
                else
                    methodParameter.Add(new KeyValuePair<string, string>("@PaperCategoryTypeId", "2"));
                methodParameter.Add(new KeyValuePair<string, string>("@IsLocal", "0"));
                methodParameter.Add(new KeyValuePair<string, string>("@OnlineQuestionButtonShow", "OnlineQuestionButtonShow"));
                methodParameter.Add(new KeyValuePair<string, string>("@UserId", model.UserId.ToString()));
                System.Data.DataSet ds = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "sp_GetAllOnlineMemberQuesDDL", methodParameter);
                model.eventList = ds.Tables[0].AsEnumerable().Select(dataRow => new mEvent
                {
                    EventId = dataRow.Field<Int32>("EventId"),
                    EventName = dataRow.Field<string>("EventName"),
                }).ToList();
                model.memMinList = ds.Tables[1].AsEnumerable().Select(dataRow => new mMinisteryMinisterModel
                {
                    MinistryID = dataRow.Field<Int32>("MinistryID"),
                    MinisterMinistryName = dataRow.Field<string>("MinisterName"),
                }).ToList();
                model.DeptList = ds.Tables[4].AsEnumerable().Select(dataRow => new mDepartment
                {
                    deptId = dataRow.Field<string>("deptId"),
                    deptname = dataRow.Field<string>("deptname"),
                }).ToList();
                foreach (var test in model.DemandList)
                {
                    string val = test.DemandName + "(" + test.DemandNo + ")";
                    test.DemandName = val;
                }
                model.DemandNo = 0;
                model.SessionID = SessId;
                model.AssemblyID = AssemId;
                model.PaperEntryType = "NewEntry";
                model.RIds = CurrentSession.ActionIds;
                model.SignFilePath = CurrentSession.UserID.GetSignPath();
                //var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "OnlineQButtonShow", null);
                model.MainQuestion = "";
                //model.ButtonOnlineShow = NewsSettings.SettingValue;
                model.ButtonOnlineShow = ds.Tables[2].Rows[0]["SettingValue"].ToString();
                model.SignFilePath = ds.Tables[3].Rows[0]["SignaturePath"].ToString();
                if (PaperType.ToLower() == "questions")
                {
                    return PartialView("_NewStarredQEntryForm", model);
                }
                else if (PaperType == "Unstarred Questions")
                {
                    return PartialView("_NewUnstarredEntryForm", model);
                }
                else if (PaperType.ToLower() == "resolutions")
                {
                    return PartialView("_NoticeEntry", model);
                }
                return null;
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveUpdateEntry(DiaryModel model)
        {
            model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model = Helper.ExecuteService("Notice", "SaveUpdateQuestionsTable", model) as DiaryModel;
            SubmitQuestionById(model.QuestionId, model.AssemblyID, model.SessionID, model.QuestionTypeId);
            // Generate Pdf and Save in File System And Update in Table
            if (model.BtnCaption == "Submit")
            {
                var Location = GeneratePdf(model);
                model.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                model.FileName = Location.Substring(Location.IndexOf(",") + 1);
                model.SignFilePath = CurrentSession.UserID.GetSignPath();
                string imageFileLoc = Server.MapPath(model.SignFilePath);
                string textLine1 = model.MemberId.GetMemberNameByIDWithOutPrefix();
                string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                string CompleteFile = Path.Combine(model.FilePath, model.FileName);
                string NewFile = Path.Combine(model.FilePath, "sign_" + model.FileName);
                PDFExtensions.InsertImageToPdf(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);

                // Update file name and location in both table
                var result = Helper.ExecuteService("Notice", "UpdateFile", model) as string;

                if (result == "Updated")
                {
                    TempData["Msg"] = "Starred Question Submitted Successfully and Diary Number is " + model.DiaryNumber;
                }
                else
                {
                    TempData["Msg"] = result.ToString();
                }
            }

            //Show Message
            if (model.Message == "Saved")
            {
                TempData["Msg"] = "Starred Question Saved Successfully !";
            }
            else if (model.Message == "Updated")
            {
                TempData["Msg"] = "Starred Question Updated Successfully !";
            }
            else if (model.Message == "Submitted")
            {
                TempData["Msg"] = "Starred Question Submitted Successfully and Diary Number is " + model.DiaryNumber;
            }
            else
            {
                TempData["Msg"] = model.Message;
            }

            TempData["CallingMethod"] = "Starred";
            return RedirectToAction("Index");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveUpdateMlaDiary(MlaDiary model, HttpPostedFileBase file)
        {
            if (CurrentSession.MemberCode == "" || CurrentSession.MemberCode == null)
            {
                CurrentSession.MemberCode = "0";
            }
            model.MlaCode = Convert.ToInt32(CurrentSession.MemberCode);
            //  model.RecordNumber = Convert.ToInt32(1);

            //  model.BtnCaption = "Save";
            if (CurrentSession.AssemblyId == "" || CurrentSession.AssemblyId == null)
            {
                CurrentSession.AssemblyId = "0";
            }
            model.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);


            //if (CurrentSession.ConstituencyID == "" || CurrentSession.ConstituencyID == null)
            //{
            //    CurrentSession.ConstituencyID = "0";
            //}

            string res = "0";
            //  model.ConstituencyCode = (SBL.DomainModel.Models.Diaries.DiaryModel(Helper.ExecuteService("Diary", "GetConstByMemberId", null) as string));
            DiaryModel dobj = new DiaryModel();
            // dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            dobj.MemberId = Convert.ToInt16(model.MlaCode);
            dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
            model.ConstituencyCode = dobj.ConstituencyCode;
            if (CurrentSession.IsMember == "True")
            {
                model.IsMember = "True";
                model.ActionByMember = dobj.ConstituencyName;
            }
            //Getdeptid from officeid  
            if (model.MultipleOfcId == null)
            { model.MultipleOfcId = ""; }
            // if (Mdl.RecordNumber == 0 || Mdl.RecordNumber == null)
            //  {
            if (model.MultipleOfcId != "")
            {
                if (model.MultipleOfcId.Contains(','))
                {
                    //  List<long> ID = Mdl.MultipleOfcId.Split(',').Select(Int64.Parse).ToList();
                    string[] ids = model.MultipleOfcId.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);   //Mdl.MultipleOfcId.Split(',');
                    for (int i = 0; i <= ids.Length - 1; i++)
                    {
                        // string ofcname = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", Convert.ToInt16(ids[i]));
                        string Department = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", Convert.ToInt16(ids[i]));
                        model.DeptId += Department + ",";
                    }
                }
            }
            else
            {
                string Departmentid = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", model.OfficeId);
                model.DeptId = Departmentid;
            }

            if (model.BtnCaption == "Save")
            {
                model.RecordNumber = Convert.ToInt32(GetRecordNumber_mlaDiary(model.DocmentType));

                DateTime Date2 = DateTime.Now;
                string span = Convert.ToString(DateTime.Now.ToFileTime());
                model.DiaryRefId = model.RecordNumber + "_" + Convert.ToInt32(model.MemberCode) + "_" + model.DocmentType + "_" + span;
            }
            if (model.BtnCaption == "Update")
            {
                model.ActionOfficeId = Convert.ToInt16(CurrentSession.OfficeId);
            }


            if (model.ForwardDateForUse != null && model.ForwardDateForUse != "")
            {
                string Date = model.ForwardDateForUse;
                Int16 dd = Convert.ToInt16(model.ForwardDateForUse.Substring(0, 2));
                Int16 mm = Convert.ToInt16(model.ForwardDateForUse.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(model.ForwardDateForUse.Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime dtDate = new DateTime(yyyy, mm, dd);
                model.ForwardDate = dtDate;
            }
            if (model.EnclosureDateForUse != null && model.EnclosureDateForUse != "")
            {
                string Date = model.ForwardDateForUse;
                Int16 dd = Convert.ToInt16(model.EnclosureDateForUse.Substring(0, 2));
                Int16 mm = Convert.ToInt16(model.EnclosureDateForUse.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(model.EnclosureDateForUse.Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime dtDate = new DateTime(yyyy, mm, dd);
                model.EnclosureDate = dtDate;
            }
            if (model.ActiontakenDateForUse != null && model.ActiontakenDateForUse != "")
            {
                string Date = model.ForwardDateForUse;
                Int16 dd = Convert.ToInt16(model.ActiontakenDateForUse.Substring(0, 2));
                Int16 mm = Convert.ToInt16(model.ActiontakenDateForUse.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(model.ActiontakenDateForUse.Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime dtDate = new DateTime(yyyy, mm, dd);
                model.ActiontakenDate = dtDate;
            }
            model.CreatedBy = CurrentSession.UserID;

            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("forwardin");
            //  model.ForwardDate = DateTime.ParseExact(model.ForwardDateForUse, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("forwarddatget");
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            model.ForwardedFile = null;
            //if (model.IsForwardFile == "Y")
            //{
            //    model.ForwardedFile = model.ForwardedFile;
            //}
            //  else
            // {

            // Save Forward File

            //string urlforward = "/MlaDiary/ForwardFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
            //string directoryf = FileSettings.SettingValue + urlforward;

            //if (!System.IO.Directory.Exists(directoryf))
            //{
            //    System.IO.Directory.CreateDirectory(directoryf);
            //}
            //string tempurlforwrd = "~/MlaDiary/TempFile";
            //string Tempdirectoryforwrd = Server.MapPath(tempurlforwrd);


            //if (Directory.Exists(directoryf) && (model.ForwardDateForUse != null || model.ForwardDateForUse != ""))
            //{

            //    string[] savedFileName = Directory.GetFiles(Server.MapPath(tempurlforwrd));
            //    if (savedFileName.Length > 0 && (model.ForwardDateForUse != null || model.ForwardDateForUse != ""))
            //    {
            //        string SourceFile = savedFileName[0];
            //        foreach (string page in savedFileName)
            //        {
            //            // Guid FileName = Guid.NewGuid();
            //            string name = Path.GetFileName(page);
            //            string ext = Path.GetExtension(SourceFile);
            //            string path = System.IO.Path.Combine(directoryf, model.RecordNumber.ToString() + ext);

            //            if (!string.IsNullOrEmpty(model.RecordNumber.ToString()))
            //            {

            //                System.IO.File.Copy(SourceFile, path, true);

            //                model.ForwardedFile = urlforward + model.RecordNumber.ToString() + ext;
            //            }

            //        }


            //    }
            //    else
            //    {
            //        model.ForwardedFile = null;
            //        TempData["Msg"] = "Please select Forward File";

            //    }
            //}
            //     else
            //    {
            //        model.ForwardedFile = null;
            //        TempData["Msg"] = "Please select Forward File";

            //    }


            //if (Directory.Exists(Tempdirectoryforwrd))  //tempurlforwrd
            //{
            //    string[] filePaths = Directory.GetFiles(Server.MapPath(tempurlforwrd));
            //    foreach (string filePath in filePaths)
            //    {
            //        System.IO.File.Delete(filePath);
            //    }
            //}

            // }

            //upload action file 
            if (model.IsFile == "Y")
            {
                model.ActiontakenFile = model.ActiontakenFile;
            }
            else
            {
                //string urlA = "/MlaDiary/ActionFile";
                string urlA = "/MlaDiary/ActionFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
                string directoryA = FileSettings.SettingValue + urlA;

                if (!System.IO.Directory.Exists(directoryA))
                {
                    System.IO.Directory.CreateDirectory(directoryA);
                }
                // int fileID = GetFileRandomNo();

                string tempurlA1 = "~/MlaDiary/ActionTempFile";
                string tempdirectoryA1 = Server.MapPath(tempurlA1);
                //Save ActionTaken File
                if (!System.IO.Directory.Exists(tempdirectoryA1))
                {
                    System.IO.Directory.CreateDirectory(tempdirectoryA1);
                }
                if (Directory.Exists(directoryA))// && (model.ActiontakenDate != null ))
                {
                    string[] savedFileName = Directory.GetFiles(Server.MapPath(tempurlA1));
                    if (savedFileName.Length > 0)// && (model.ActiontakenDate != null ))   //&& model.ActiontakenDate != null && model.ActiontakenDate != ""
                    {
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {

                            // string path1 = System.IO.Path.Combine(directory, FileName1 + "_" + nametwo.Replace(" ", ""));

                            string ext = Path.GetExtension(SourceFile);
                            string name = Path.GetFileName(page);
                            string path1 = System.IO.Path.Combine(directoryA, model.RecordNumber.ToString() + ext);
                            if (!string.IsNullOrEmpty(name))
                            {
                                //  System.IO.File.Delete(System.IO.Path.Combine(directoryA, model.ActiontakenFile));
                                System.IO.File.Copy(SourceFile, path1, true);
                                model.ActiontakenFile = urlA + model.RecordNumber.ToString() + ext;

                            }
                        }


                    }
                    else
                    {
                        model.ActiontakenFile = null;
                        //   return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
                        //  TempData["Msg"] = "Please select Action Taken file";
                        // return RedirectToAction("Index");
                    }
                }
                else
                {
                    model.ActiontakenFile = null;
                }
                if (Directory.Exists(tempdirectoryA1))   //tempurlA1
                {
                    string[] filePaths = Directory.GetFiles(Server.MapPath(tempurlA1));
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

            }
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("1");
            //upload enclosure file 
            if (model.IsEnclosureFile == "Y")
            {
                model.EnclosureFile = model.EnclosureFile;
            }
            else
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("2");
                string urlEnclosure = "/MlaDiary/EnclosureFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
                //string urlEnclosure = "/MlaDiary/EnclosureFile";
                string directoryEnclosure = FileSettings.SettingValue + urlEnclosure;

                if (!System.IO.Directory.Exists(directoryEnclosure))
                {
                    System.IO.Directory.CreateDirectory(directoryEnclosure);
                }
                // int fileIDEnclosure = GetFileRandomNo();

                string TempurlEnclosure1 = "~/MlaDiary/TempEnclosureFile";
                string directoryEnclosure1 = Server.MapPath(TempurlEnclosure1);

                if (!System.IO.Directory.Exists(directoryEnclosure1))
                {
                    System.IO.Directory.CreateDirectory(directoryEnclosure1);
                }

                if (Directory.Exists(directoryEnclosure))
                {
                    string[] savedFileName = Directory.GetFiles(Server.MapPath(TempurlEnclosure1));
                    if (savedFileName.Length > 0 && (model.EnclosureDate != null))
                    {
                        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("3");
                        string SourceFileold = savedFileName[0];
                        string SourceFile = System.IO.Path.Combine(directoryEnclosure1, model.EnclosureFile);
                        foreach (string page in savedFileName)
                        {
                            // Guid FileName = Guid.NewGuid();
                            string ext = Path.GetExtension(SourceFile);
                            string name = Path.GetFileName(page);
                            if (name == model.EnclosureFile)
                            {

                                var NFileName = Guid.NewGuid().ToString() + model.RecordNumber.ToString() + ext;
                                string path1 = System.IO.Path.Combine(directoryEnclosure, NFileName);
                                if (!string.IsNullOrEmpty(name))
                                {
                                    //  System.IO.File.Delete(System.IO.Path.Combine(directoryEnclosure, model.EnclosureFile));
                                    System.IO.File.Copy(SourceFile, path1, true);
                                    model.EnclosureFile = urlEnclosure + NFileName;
                                    System.IO.File.Delete(SourceFile);
                                }
                            }
                        }
                    }
                    else
                    {
                        model.EnclosureFile = null;
                        //  return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    model.EnclosureFile = null;
                    //  return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
                }
                //if (Directory.Exists(directoryEnclosure1))  //TempurlEnclosure1
                //{

                //    string[] filePaths = Directory.GetFiles(Server.MapPath(TempurlEnclosure1));
                //    foreach (string filePath in filePaths)
                //    {
                //        System.IO.File.Delete(filePath);
                //    }
                //}
            }

            //Delete the temp files if exists

            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("4");
            res = (string)Helper.ExecuteService("Diary", "SaveMlaDiary", model);

            if (res != "0" && res != "-1")
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("5");
                return Json(res, JsonRequestBehavior.AllowGet);
                //  return RedirectToAction("Index");
            }
            else if (res == "0")
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("5");
                return Json("U", JsonRequestBehavior.AllowGet);
            }
            else
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("6");
                return Json("E", JsonRequestBehavior.AllowGet);
            }


            //if (res != 0 &&  res != -1 ||(model.BtnCaption=="Update"))
            //{
            //    if (model.BtnCaption == "Update")
            //    {
            //    }
            //    else
            //    {
            //        model.RecordNumber = res;
            //    }

            //    int reslt = (int)Helper.ExecuteService("Diary", "SaveMlaDiary", model);
            //    if (reslt == 0)
            //    {
            //        return RedirectToAction("Index");
            //    }
            //    else                
            //    {
            //        return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            //    }
            //}
            //else
            //{
            //    return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            //}


            // return RedirectToAction("Index");
        }

        //public ActionResult SaveUpdateMlaDiary(MlaDiary model, HttpPostedFileBase file)
        //{
        //    if (CurrentSession.MemberCode == "" || CurrentSession.MemberCode == null)
        //    {
        //        CurrentSession.MemberCode = "0";
        //    }
        //    model.MlaCode = Convert.ToInt32(CurrentSession.MemberCode);
        //    //  model.RecordNumber = Convert.ToInt32(1);

        //    //  model.BtnCaption = "Save";
        //    if (CurrentSession.AssemblyId == "" || CurrentSession.AssemblyId == null)
        //    {
        //        CurrentSession.AssemblyId = "0";
        //    }
        //    model.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);


        //    //if (CurrentSession.ConstituencyID == "" || CurrentSession.ConstituencyID == null)
        //    //{
        //    //    CurrentSession.ConstituencyID = "0";
        //    //}

        //    string res = "0";
        //    //  model.ConstituencyCode = (SBL.DomainModel.Models.Diaries.DiaryModel(Helper.ExecuteService("Diary", "GetConstByMemberId", null) as string));
        //    DiaryModel dobj = new DiaryModel();
        //    // dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
        //    dobj.MemberId = Convert.ToInt16(model.MlaCode);
        //    dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
        //    dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
        //    model.ConstituencyCode = dobj.ConstituencyCode;
        //    if (CurrentSession.IsMember == "True")
        //    {
        //        model.IsMember = "True";
        //        model.ActionByMember = dobj.ConstituencyName;
        //    }
        //    //Getdeptid from officeid  
        //    if (model.MultipleOfcId == null)
        //    { model.MultipleOfcId = ""; }
        //    // if (Mdl.RecordNumber == 0 || Mdl.RecordNumber == null)
        //    //  {
        //    if (model.MultipleOfcId != "")
        //    {
        //        if (model.MultipleOfcId.Contains(','))
        //        {
        //            //  List<long> ID = Mdl.MultipleOfcId.Split(',').Select(Int64.Parse).ToList();
        //            string[] ids = model.MultipleOfcId.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);   //Mdl.MultipleOfcId.Split(',');
        //            for (int i = 0; i <= ids.Length - 1; i++)
        //            {
        //                // string ofcname = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", Convert.ToInt16(ids[i]));
        //                string Department = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", Convert.ToInt16(ids[i]));
        //                model.DeptId += Department + ",";
        //            }
        //        }
        //    }
        //    else
        //    {
        //        string Departmentid = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", model.OfficeId);
        //        model.DeptId = Departmentid;
        //    }

        //    if (model.BtnCaption == "Save")
        //    {
        //        model.RecordNumber = Convert.ToInt32(GetRecordNumber_mlaDiary(model.DocmentType));

        //        DateTime Date2 = DateTime.Now;
        //        string span = Convert.ToString(DateTime.Now.ToFileTime());
        //        model.DiaryRefId = model.RecordNumber + "_" + Convert.ToInt32(model.MemberCode) + "_" + model.DocmentType + "_" + span;
        //    }
        //    if (model.BtnCaption == "Update")
        //    {
        //        model.ActionOfficeId = Convert.ToInt16(CurrentSession.OfficeId);
        //    }


        //    if (model.ForwardDateForUse != null && model.ForwardDateForUse != "")
        //    {
        //        string Date = model.ForwardDateForUse;
        //        Int16 dd = Convert.ToInt16(model.ForwardDateForUse.Substring(0, 2));
        //        Int16 mm = Convert.ToInt16(model.ForwardDateForUse.Substring(3, 2));
        //        Int16 yyyy = Convert.ToInt16(model.ForwardDateForUse.Substring(6, 4));
        //        // DateTime dtDate = new DateTime(2018, 2, 12);
        //        DateTime dtDate = new DateTime(yyyy, mm, dd);
        //        model.ForwardDate = dtDate;
        //    }
        //    if (model.EnclosureDateForUse != null && model.EnclosureDateForUse != "")
        //    {
        //        string Date = model.ForwardDateForUse;
        //        Int16 dd = Convert.ToInt16(model.EnclosureDateForUse.Substring(0, 2));
        //        Int16 mm = Convert.ToInt16(model.EnclosureDateForUse.Substring(3, 2));
        //        Int16 yyyy = Convert.ToInt16(model.EnclosureDateForUse.Substring(6, 4));
        //        // DateTime dtDate = new DateTime(2018, 2, 12);
        //        DateTime dtDate = new DateTime(yyyy, mm, dd);
        //        model.EnclosureDate = dtDate;
        //    }
        //    if (model.ActiontakenDateForUse != null && model.ActiontakenDateForUse != "")
        //    {
        //        string Date = model.ForwardDateForUse;
        //        Int16 dd = Convert.ToInt16(model.ActiontakenDateForUse.Substring(0, 2));
        //        Int16 mm = Convert.ToInt16(model.ActiontakenDateForUse.Substring(3, 2));
        //        Int16 yyyy = Convert.ToInt16(model.ActiontakenDateForUse.Substring(6, 4));
        //        // DateTime dtDate = new DateTime(2018, 2, 12);
        //        DateTime dtDate = new DateTime(yyyy, mm, dd);
        //        model.ActiontakenDate = dtDate;
        //    }
        //    model.CreatedBy = CurrentSession.UserID;

        //    SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("forwardin");
        //    //  model.ForwardDate = DateTime.ParseExact(model.ForwardDateForUse, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //    SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("forwarddatget");
        //    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
        //    model.ForwardedFile = null;
        //    //if (model.IsForwardFile == "Y")
        //    //{
        //    //    model.ForwardedFile = model.ForwardedFile;
        //    //}
        //    //  else
        //    // {

        //    // Save Forward File

        //    //string urlforward = "/MlaDiary/ForwardFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
        //    //string directoryf = FileSettings.SettingValue + urlforward;

        //    //if (!System.IO.Directory.Exists(directoryf))
        //    //{
        //    //    System.IO.Directory.CreateDirectory(directoryf);
        //    //}
        //    //string tempurlforwrd = "~/MlaDiary/TempFile";
        //    //string Tempdirectoryforwrd = Server.MapPath(tempurlforwrd);


        //    //if (Directory.Exists(directoryf) && (model.ForwardDateForUse != null || model.ForwardDateForUse != ""))
        //    //{

        //    //    string[] savedFileName = Directory.GetFiles(Server.MapPath(tempurlforwrd));
        //    //    if (savedFileName.Length > 0 && (model.ForwardDateForUse != null || model.ForwardDateForUse != ""))
        //    //    {
        //    //        string SourceFile = savedFileName[0];
        //    //        foreach (string page in savedFileName)
        //    //        {
        //    //            // Guid FileName = Guid.NewGuid();
        //    //            string name = Path.GetFileName(page);
        //    //            string ext = Path.GetExtension(SourceFile);
        //    //            string path = System.IO.Path.Combine(directoryf, model.RecordNumber.ToString() + ext);

        //    //            if (!string.IsNullOrEmpty(model.RecordNumber.ToString()))
        //    //            {

        //    //                System.IO.File.Copy(SourceFile, path, true);

        //    //                model.ForwardedFile = urlforward + model.RecordNumber.ToString() + ext;
        //    //            }

        //    //        }


        //    //    }
        //    //    else
        //    //    {
        //    //        model.ForwardedFile = null;
        //    //        TempData["Msg"] = "Please select Forward File";

        //    //    }
        //    //}
        //    //     else
        //    //    {
        //    //        model.ForwardedFile = null;
        //    //        TempData["Msg"] = "Please select Forward File";

        //    //    }


        //    //if (Directory.Exists(Tempdirectoryforwrd))  //tempurlforwrd
        //    //{
        //    //    string[] filePaths = Directory.GetFiles(Server.MapPath(tempurlforwrd));
        //    //    foreach (string filePath in filePaths)
        //    //    {
        //    //        System.IO.File.Delete(filePath);
        //    //    }
        //    //}

        //    // }

        //    //upload action file 
        //    if (model.IsFile == "Y")
        //    {
        //        model.ActiontakenFile = model.ActiontakenFile;
        //    }
        //    else
        //    {
        //        //string urlA = "/MlaDiary/ActionFile";
        //        string urlA = "/MlaDiary/ActionFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
        //        string directoryA = FileSettings.SettingValue + urlA;

        //        if (!System.IO.Directory.Exists(directoryA))
        //        {
        //            System.IO.Directory.CreateDirectory(directoryA);
        //        }
        //        // int fileID = GetFileRandomNo();

        //        string tempurlA1 = "~/MlaDiary/ActionTempFile";
        //        string tempdirectoryA1 = Server.MapPath(tempurlA1);

        //        //Save ActionTaken File
        //        if (!System.IO.Directory.Exists(tempdirectoryA1))
        //        {
        //            System.IO.Directory.CreateDirectory(tempdirectoryA1);
        //        }
        //        if (Directory.Exists(directoryA))// && (model.ActiontakenDate != null ))
        //        {
        //            string[] savedFileName = Directory.GetFiles(Server.MapPath(tempurlA1));
        //            if (savedFileName.Length > 0)// && (model.ActiontakenDate != null ))   //&& model.ActiontakenDate != null && model.ActiontakenDate != ""
        //            {
        //                string SourceFile = savedFileName[0];
        //                foreach (string page in savedFileName)
        //                {

        //                    // string path1 = System.IO.Path.Combine(directory, FileName1 + "_" + nametwo.Replace(" ", ""));

        //                    string ext = Path.GetExtension(SourceFile);
        //                    string name = Path.GetFileName(page);

        //                    if (name == model.ActiontakenFile)
        //                    {
        //                        var NFileName1 = Guid.NewGuid().ToString() + model.RecordNumber.ToString() + ext;
        //                        string path1 = System.IO.Path.Combine(directoryA, NFileName1);
        //                        if (!string.IsNullOrEmpty(name))
        //                        {
        //                            //  System.IO.File.Delete(System.IO.Path.Combine(directoryA, model.ActiontakenFile));
        //                            System.IO.File.Copy(SourceFile, path1, true);
        //                            model.ActiontakenFile = urlA + NFileName1;
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                model.ActiontakenFile = null;
        //                //   return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
        //                //  TempData["Msg"] = "Please select Action Taken file";
        //                // return RedirectToAction("Index");
        //            }
        //        }
        //        else
        //        {
        //            model.ActiontakenFile = null;
        //        }
        //        if (Directory.Exists(tempdirectoryA1))   //tempurlA1
        //        {
        //            string[] filePaths = Directory.GetFiles(Server.MapPath(tempurlA1));
        //            foreach (string filePath in filePaths)
        //            {
        //                System.IO.File.Delete(filePath);
        //            }
        //        }
        //    }
        //    SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("1");
        //    //upload enclosure file 
        //    if (model.IsEnclosureFile == "Y")
        //    {
        //        model.EnclosureFile = model.EnclosureFile;
        //    }
        //    else
        //    {
        //        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("2");
        //        string urlEnclosure = "/MlaDiary/EnclosureFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
        //        //string urlEnclosure = "/MlaDiary/EnclosureFile";
        //        string directoryEnclosure = FileSettings.SettingValue + urlEnclosure;

        //        if (!System.IO.Directory.Exists(directoryEnclosure))
        //        {
        //            System.IO.Directory.CreateDirectory(directoryEnclosure);
        //        }
        //        // int fileIDEnclosure = GetFileRandomNo();

        //        string TempurlEnclosure1 = "~/MlaDiary/TempEnclosureFile";
        //        string directoryEnclosure1 = Server.MapPath(TempurlEnclosure1);

        //        if (!System.IO.Directory.Exists(directoryEnclosure1))
        //        {
        //            System.IO.Directory.CreateDirectory(directoryEnclosure1);
        //        }

        //        if (Directory.Exists(directoryEnclosure))
        //        {
        //            string[] savedFileName = Directory.GetFiles(Server.MapPath(TempurlEnclosure1));
        //            if (savedFileName.Length > 0 && (model.EnclosureDate != null))
        //            {
        //                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("3");
        //                string SourceFileold = savedFileName[0];
        //                string SourceFile = System.IO.Path.Combine(directoryEnclosure1, model.EnclosureFile);
        //                foreach (string page in savedFileName)
        //                {
        //                    // Guid FileName = Guid.NewGuid();
        //                    string ext = Path.GetExtension(SourceFile);
        //                    string name = Path.GetFileName(page);
        //                    if (name == model.EnclosureFile)
        //                    {

        //                        var NFileName = Guid.NewGuid().ToString() + model.RecordNumber.ToString() + ext;
        //                        string path1 = System.IO.Path.Combine(directoryEnclosure, NFileName);
        //                        if (!string.IsNullOrEmpty(name))
        //                        {
        //                            //  System.IO.File.Delete(System.IO.Path.Combine(directoryEnclosure, model.EnclosureFile));
        //                            System.IO.File.Copy(SourceFile, path1, true);
        //                            model.EnclosureFile = urlEnclosure + NFileName;
        //                            System.IO.File.Delete(SourceFile);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                model.EnclosureFile = null;
        //                //  return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        else
        //        {
        //            model.EnclosureFile = null;
        //            //  return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
        //        }
        //        //if (Directory.Exists(directoryEnclosure1))  //TempurlEnclosure1
        //        //{

        //        //    string[] filePaths = Directory.GetFiles(Server.MapPath(TempurlEnclosure1));
        //        //    foreach (string filePath in filePaths)
        //        //    {
        //        //        System.IO.File.Delete(filePath);
        //        //    }
        //        //}
        //    }

        //    //Delete the temp files if exists

        //    SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("4");
        //    res = (string)Helper.ExecuteService("Diary", "SaveMlaDiary", model);

        //    if (res != "0" && res != "-1")
        //    {
        //        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("5");
        //        return Json(res, JsonRequestBehavior.AllowGet);
        //        //  return RedirectToAction("Index");
        //    }
        //    else if (res == "0")
        //    {
        //        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("5");
        //        return Json("U", JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("6");
        //        return Json("E", JsonRequestBehavior.AllowGet);
        //    }


        //    //if (res != 0 &&  res != -1 ||(model.BtnCaption=="Update"))
        //    //{
        //    //    if (model.BtnCaption == "Update")
        //    //    {
        //    //    }
        //    //    else
        //    //    {
        //    //        model.RecordNumber = res;
        //    //    }

        //    //    int reslt = (int)Helper.ExecuteService("Diary", "SaveMlaDiary", model);
        //    //    if (reslt == 0)
        //    //    {
        //    //        return RedirectToAction("Index");
        //    //    }
        //    //    else                
        //    //    {
        //    //        return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
        //    //}


        //    // return RedirectToAction("Index");
        //}




        public int GetRecordNumber_mlaDiary(string DocmentType)
        {
            MlaDiary mdl = new MlaDiary();
            mdl.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            mdl.MlaCode = Convert.ToInt32(CurrentSession.MemberCode);
            mdl.DocmentType = DocmentType;
            mdl.RecordNumber = (int)Helper.ExecuteService("Diary", "Chk_RecordNumber", mdl);
            if (mdl.RecordNumber == 0)
            {
                mdl.RecordNumber = 1;
            }
            else
            {
                mdl.RecordNumber = mdl.RecordNumber + 1;
            }
            int re = Convert.ToInt32(mdl.RecordNumber);
            return re;
        }

        public ActionResult EditMlaDairyForm(string ID)
        {
            try
            {
                var user = CurrentSession.IsMember;
                MlaDiary model = new MlaDiary();
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("1");
                model = (MlaDiary)Helper.ExecuteService("Diary", "GetMlaDairyRecord", ID);

                var ActionDetails = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionDetails", model);

                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("2");
                if (model.ForwardDate != null)
                {
                    model.ForwardDateForUse = Convert.ToDateTime(model.ForwardDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    model.ForwardDateForUse = "";
                }
                if (model.ActiontakenDate != null)
                {
                    model.ActiontakenDateForUse = Convert.ToDateTime(model.ActiontakenDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    model.ActiontakenDateForUse = "";
                }
                if (model.EnclosureDate != null)
                {
                    model.EnclosureDateForUse = Convert.ToDateTime(model.EnclosureDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    model.EnclosureDateForUse = "";
                }
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("3");
                var discd = Helper.ExecuteService("Diary", "GetDisCode", model);
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("4");
#pragma warning disable CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                if (discd != null || discd != "")
#pragma warning restore CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                {
                    model.distcd = Convert.ToInt16(discd);
                }
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("5");
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
                var a = CurrentSession.SPMinId;
                model.memMinList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;  //GetDepartmentByMinistery
                                                                                                                                          // model.DeptList = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment_maaped", model);  //DepartmentListCol
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("6");
                model.Districtist = (List<SBL.DomainModel.Models.District.DistrictModel>)Helper.ExecuteService("District", "GetAllDistrict", null);
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("7");
                //model.OfficeList = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOffice", null);
                model.MappedMemberList = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "Get_MappedMemberList", model);
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("8");
                model.DocumentTypelist = DocumentTypelist;
                model.ActionTypeList = ActionTypelist;
                model.FinanCialYearList = GetFinancialYearList();
                model.ActionDetailsList = ActionDetails;
                model.PaperEntryType = "EditEntry";
                model.BtnCaption = "Update";
                RecipientGroup Rmodel = new RecipientGroup();   // SubdivisionID
                Rmodel.DistId = Convert.ToString(model.distcd);
                Rmodel.DeptId = CurrentSession.DeptID;
                Rmodel.MemberCode = Convert.ToInt16(CurrentSession.MemberCode);
                Rmodel.AssId = Convert.ToInt16(CurrentSession.AssemblyId);
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("9");
                if (CurrentSession.SubDivisionId == "")
                {
                    string constituencyID = CurrentSession.ConstituencyID;
                    string[] str1 = new string[2];
                    str1[0] = CurrentSession.AssemblyId;
                    str1[1] = constituencyID;
                    string[] strOutput1 = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str1);
                    SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("10");
                    if (strOutput1 != null)
                    {
                        constituencyID = Convert.ToString(strOutput1[0]);
                        //CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                    }
                    Rmodel.ConstituencyCode = Convert.ToInt16(constituencyID);
                    Rmodel.ConstituencyCodeForSubdivision = "0";
                }
                else
                {
                    Rmodel.ConstituencyCodeForSubdivision = CurrentSession.ConstituencyID;
                }
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("11");
                model.OfficeList = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOfficemapped_ByDeptDistrict", Rmodel);
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("12");
                return PartialView("_MlaDiaryEntry", model);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult DeleteMlaDairyForm(int ID)
        {
            try
            {
                OnlineMemberQmodel model = new OnlineMemberQmodel();
                model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                model.SessionID = Convert.ToInt32(CurrentSession.SessionId);
                ViewBag.PageSize = 25;
                ViewBag.CurrentPage = 1;
                MlaDiary model1 = new MlaDiary();
                Helper.ExecuteService("Diary", "DeleteMlaDairyRecord", ID);
                ///  for search ui

                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);

                model1.DocmentType = "1";
                model1.ActionCode = 0;
                model1.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                model1.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
                model1.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryList", model1);

                ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCount", model1) as string;
                ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCount", model1) as string;
                ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCount", model1) as string;
                ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCount", model1) as string;
                model1.FinanCialYearList = GetFinancialYearList();
                model1.DocumentTypelist = DocumentTypelist;
                model1.ActionTypeList = ActionTypelist;
                model1.PaperEntryType = "Councillor Diary";
                ViewBag.DocType = "1";
                ViewBag.ActionType = "0";
                ViewBag.PendencySince = "0";

                return PartialView("_MlaDiaryList", model1);
                //return Content("Deleted");
            }
            catch (Exception)
            {

                throw;
            }
        }

        // [HttpPost]
        public FileStreamResult GenerateDirayPdf(int DiaryId)  //GeneratePublishedTourPDF
        {
            string msg = string.Empty;

            MlaDiary model = new MlaDiary();

            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDairyListForPdf", DiaryId);
            MemoryStream output = new MemoryStream();
            if (model.MlaDiaryList.Count > 0)
            {
                //mMember member = new mMember();
                string isCCItemExist = "";
                //  member.ase = Convert.ToInt32(CurrentSession.MemberCode);
                mMlaSignature obj = new mMlaSignature();
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj.MlaCode = CurrentSession.MemberCode;
                obj = (mMlaSignature)Helper.ExecuteService("Diary", "GetDiarySignatureData", obj);
                if (obj != null)
                {
                    int crntyear = DateTime.Now.Year;
                    EvoPdf.Document document1 = new EvoPdf.Document();
                    document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document1.CompressionLevel = PdfCompressionLevel.Best;
                    document1.Margins = new Margins(0, 0, 0, 0);

                    PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                    PdfPageOrientation.Portrait);

                    string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";
                    foreach (MlaDiary diary in model.MlaDiaryList)
                    {

                        string frwDate = "";
                        isCCItemExist = diary.DocumentCC;
                        if (diary.ForwardDate != null)
                        { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                        if (diary.DiaryLanguage == 1)
                        {
                            //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                            outXml += @"<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                            outXml += @"<center><h3>" + "" + "</h3></center>";
                            outXml += @"<table cellpadding='4' style='width:100%;font-size:18px;margin-left: 55px;'>";
                            outXml += @"<thead>";
                            outXml += @"<tr>";
                            //outXml += @"<th style='text-align:center;font-size:28px;color:#307ecc;font-weight: bold;'>" + obj.HeaderText1 + "</th></tr>";
                            //outXml += @"<th style='text-align:center;font-size:28px;color:#307ecc;font-weight: bold;'>" + obj.HeaderText2 + "</th></tr>";
                            //outXml += "<tr><th style='margin-top:20px'></th>";
                            outXml += @"</tr><thead><tbody><br>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                            outXml += @"<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + " <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2 + ",<br>" + obj.SignaturePin2 + "<br></strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                            outXml += @"<tr><td style='text-align:left;' >" + obj.Text1 + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate + ":" + frwDate + "<br></td></tr>";
                            //outXml += @"<tr><td style='text-align:left;' >UO No. SPS(Spk)/" + crntyear + "-" + diary.RecordNumber + "</td><td><div style='width:100%;'>Dated:" + Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy") + "</div><br></td></tr>";
                            //outXml += @"<tr><td colspan='2 style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";
                            //outXml += @"<tr><td colspan='2' style='text-align:right;' ><strong>" + "Speaker, <br> H.P.Vidhan Sabha, <br>Shimla-171004. <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + " <br>" + obj.SignaturePin1 + "<br><br>" + "</strong></td></tr>";
                            outXml += "";
                        }
                        else if (diary.DiaryLanguage == 2)
                        {
                            outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                            outXml += "<center><h3>" + "" + "</h3></center>";
                            outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                            outXml += "<thead>";
                            outXml += "<tr>";
                            //outXml += @"<th style='text-align:center;font-size:28px;color:#307ecc;font-weight: bold;'>" + obj.HeaderText1 + "</th></tr>";
                            //outXml += @"<th style='text-align:center;font-size:28px;color:#307ecc;font-weight: bold;'>" + obj.HeaderText2 + "</th></tr>";
                            //outXml += "<tr><th style='margin-top:20px'></th>";
                            outXml += "</tr><thead><tbody><br>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                            outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                            //outXml += "<tr><td colspan='2' style='text-align:right;' ><strong>" + "???????, <br> ??0???0 ????????, <br>?????-171004. <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                            outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                            outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

                            //outXml += "<tr><td colspan='2' style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";
                            //outXml += "<tr><td colspan='2' style='text-align:right;'><strong>" + "???????, <br> ??0???0 ????????, <br>?????-171004. <br><br>" + "<strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                            outXml += "";
                        }

                    }


                    outXml += @"</tbody>";

                    outXml += @"</div></div></body></html>";

                    string htmlStringToConvert2 = outXml;

                    HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

                    AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

                    byte[] pdfBytes = document1.Save();

                    output.Write(pdfBytes, 0, pdfBytes.Length);

                    output.Position = 0;

                    string url = "/mlaDiary/" + "DiaryPdf/";

                    string directory = Server.MapPath(url);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = Path.Combine(Server.MapPath("~" + url), model.MlaCode + "_" + model.RecordNumber + ".pdf");

                    FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                    System.IO.FileAccess.Write);

                    _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                    // close file stream
                    _FileStream.Close();
                }
            }
            // }

            return new FileStreamResult(output, "application/pdf");
        }

        public FileStreamResult GenerateOfficeCopyDirayPdf(int DiaryId)  //GeneratePublishedTourPDF
        {
            string msg = string.Empty;

            MlaDiary model = new MlaDiary();

            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDairyListForPdf", DiaryId);
            MemoryStream output = new MemoryStream();
            if (model.MlaDiaryList.Count > 0)
            {
                //mMember member = new mMember();
                string isCCItemExist = "";
                //  member.ase = Convert.ToInt32(CurrentSession.MemberCode);
                mMlaSignature obj = new mMlaSignature();
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj.MlaCode = CurrentSession.MemberCode;
                obj = (mMlaSignature)Helper.ExecuteService("Diary", "GetDiarySignatureData", obj);
                if (obj != null)
                {
                    //PdfConverter pdfConverter = new PdfConverter();
                    //  pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                    int crntyear = DateTime.Now.Year;
                    EvoPdf.Document document1 = new EvoPdf.Document();
                    document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document1.CompressionLevel = PdfCompressionLevel.Best;
                    document1.Margins = new Margins(0, 0, 0, 0);
                    //  document1.Templates.AddNewTemplate(
                    PdfPage page = document1.Pages.AddNewPage(PdfPageSize.Legal, new Margins(0, 0, 40, 40),
                    PdfPageOrientation.Portrait);

                    string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";
                    foreach (MlaDiary diary in model.MlaDiaryList)
                    {

                        string frwDate = "";
                        isCCItemExist = diary.DocumentCC;
                        if (diary.ForwardDate != null)
                        { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                        if (diary.DiaryLanguage == 1)
                        {
                            //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                            outXml += @"<style>table { }table, th{border:0px solid white;font-size:21px;}table, td{}</style>";
                            outXml += @"<center><h3>" + "" + "</h3></center>";
                            outXml += @"<table cellpadding='4' style='width:100%;font-size:18px;margin-left: 55px;'>";
                            outXml += @"<thead>";
                            outXml += @"<tr>";
                            // outXml += @"<th><br></th></tr>";
                            //outXml += @"<tr><th style='margin-top:30px'></th>";
                            outXml += @"</tr><thead><tbody><br><br><br><br><br><br><br><br>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                            outXml += @"<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + " <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2 + ",<br>" + obj.SignaturePin2 + "<br></strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                            outXml += @"<tr><td style='text-align:left;' >" + obj.Text1 + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate + ":" + frwDate + "<br></td></tr>";
                            //outXml += @"<tr><td style='text-align:left;' >UO No. SPS(Spk)/" + crntyear + "-" + diary.RecordNumber + "</td><td><div style='width:100%;'>Dated:" + Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy") + "</div><br></td></tr>";
                            outXml += @"<tr><td colspan='2 style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";
                            //outXml += @"<tr><td colspan='2' style='text-align:right;' ><strong>" + "Speaker, <br> H.P.Vidhan Sabha, <br>Shimla-171004. <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + "<br><br>" + "</strong></td></tr>";
                            outXml += "";
                        }
                        else if (diary.DiaryLanguage == 2)
                        {
                            outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                            outXml += "<center><h3>" + "" + "</h3></center>";
                            outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                            outXml += "<thead>";
                            outXml += "<tr>";
                            //outXml += "<th><br></th></tr>";
                            //outXml += "<tr><th style='margin-top:30px'></th>";
                            outXml += "</tr><thead><tbody><br><br><br><br><br><br>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                            outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                            //outXml += "<tr><td colspan='2' style='text-align:right;' ><strong>" + "???????, <br> ??0???0 ????????, <br>?????-171004. <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                            outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                            outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

                            outXml += "<tr><td colspan='2' style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";
                            //outXml += "<tr><td colspan='2' style='text-align:right;'><strong>" + "???????, <br> ??0???0 ????????, <br>?????-171004. <br><br>" + "<strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                            outXml += "";
                        }

                    }

                    outXml += @"</tbody>";

                    outXml += @"</div></div></body></html>";

                    string htmlStringToConvert2 = outXml;

                    HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

                    AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

                    byte[] pdfBytes = document1.Save();

                    output.Write(pdfBytes, 0, pdfBytes.Length);

                    output.Position = 0;

                    string url = "/mlaDiary/" + "DiaryPdf/";

                    string directory = Server.MapPath(url);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = Path.Combine(Server.MapPath("~" + url), model.MlaCode + "_" + model.RecordNumber + ".pdf");

                    FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                    System.IO.FileAccess.Write);

                    _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                    // close file stream
                    _FileStream.Close();
                }
            }
            // }

            return new FileStreamResult(output, "application/pdf");
        }

        public FileStreamResult GenerateLetterCopyDairyPdf(int DiaryId)  //GeneratePublishedTourPDF
        {
            string msg = string.Empty;

            MlaDiary model = new MlaDiary();

            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDairyListForPdf", DiaryId);
            MemoryStream output = new MemoryStream();
            if (model.MlaDiaryList.Count > 0)
            {
                //mMember member = new mMember();
                string isCCItemExist = "";
                //  member.ase = Convert.ToInt32(CurrentSession.MemberCode);
                mMlaSignature obj = new mMlaSignature();
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj.MlaCode = CurrentSession.MemberCode;
                obj = (mMlaSignature)Helper.ExecuteService("Diary", "GetDiarySignatureData", obj);
                if (obj != null)
                {
                    int crntyear = DateTime.Now.Year;
                    EvoPdf.Document document1 = new EvoPdf.Document();
                    document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document1.CompressionLevel = PdfCompressionLevel.Best;
                    document1.Margins = new Margins(0, 0, 0, 0);

                    PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                    PdfPageOrientation.Portrait);

                    string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";
                    foreach (MlaDiary diary in model.MlaDiaryList)
                    {

                        string frwDate = "";
                        isCCItemExist = diary.DocumentCC;
                        if (diary.ForwardDate != null)
                        { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                        if (diary.DiaryLanguage == 1)
                        {
                            //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                            outXml += @"<style>table { }table, th{border:0px solid white;font-size:21px;}table, td{}</style>";
                            outXml += @"<center><h3>" + "" + "</h3></center>";
                            outXml += @"<table cellpadding='4' style='width:100%;font-size:18px;margin-left: 55px;'>";
                            outXml += @"<thead>";
                            outXml += @"<tr>";
                            //outXml += @"<th><br></th></tr>";
                            // outXml += @"<tr><th style='margin-top:30px'></th>";
                            outXml += @"</tr><thead><tbody>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + diary.letterDescription + "</td></tr>";
                            outXml += @"<tr><td colspan='2 style='text-align:right;' ><br></td></tr>";
                            //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + " <br><br>" + "</strong></td></tr>";
                            //outXml += @"<tr><td colspan='2' style='text-align:left;' ><br><br><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2 + ",<br>" + obj.SignaturePin2 + "<br></strong></td></tr>";
                            outXml += "";
                        }
                        else if (diary.DiaryLanguage == 2)
                        {
                            outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                            outXml += "<center><h3>" + "" + "</h3></center>";
                            outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                            outXml += "<thead>";
                            outXml += "<tr>";
                            // outXml += "<th><br></th></tr>";
                            // outXml += "<tr><th style='margin-top:30px'></th>";
                            outXml += "</tr><thead><tbody>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.letterDescription + "</td></tr>";
                            outXml += "<tr><td colspan='2 style='text-align:right;' ><br></td></tr>";
                            // outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                            //outXml += "<tr><td colspan='2' style='text-align:right;' ><strong>" + "???????, <br> ??0???0 ????????, <br>?????-171004. <br><br>" + "</strong></td></tr>";
                            // outXml += @"<tr><td colspan='2'style='text-align:left;' ><br><br><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                            outXml += "";
                        }

                    }

                    outXml += @"</tbody>";

                    outXml += @"</div></div></body></html>";

                    string htmlStringToConvert2 = outXml;

                    HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

                    AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

                    byte[] pdfBytes = document1.Save();

                    output.Write(pdfBytes, 0, pdfBytes.Length);

                    output.Position = 0;

                    string url = "/mlaDiary/" + "DiaryPdf/";

                    string directory = Server.MapPath(url);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = Path.Combine(Server.MapPath("~" + url), model.MlaCode + "_" + model.RecordNumber + ".pdf");

                    FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                    System.IO.FileAccess.Write);

                    _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                    // close file stream
                    _FileStream.Close();
                }
            }
            // }

            return new FileStreamResult(output, "application/pdf");
        }

        public List<SelectListItem> GetFinancialYearList()
        {
            var methodparameter = new List<KeyValuePair<string, string>>();
            int year = 0;
            int year1 = 0;
            year = System.DateTime.Today.Year - 1;
            year1 = year + 1;
            string Year2 = year + "-" + year1;
            int i = 1;


            List<SelectListItem> YearList = new List<SelectListItem>();
            //YearList.Add(new SelectListItem { Text = "Select", Value = "0" });
            while (year < System.DateTime.Today.Year + 1)
            {
                YearList.Add(new SelectListItem { Text = Year2, Value = Year2 });

                year = year + 1;
                year1 = year + 1;
                Year2 = year + "-" + year1;
                i = i + 1;
            }
            return YearList;
        }

        public string GeneratePdf(DiaryModel DM)
        {
            try
            {
                string outXml = "";
#pragma warning disable CS0219 // The variable 'outInnerXml' is assigned but its value is never used
                string outInnerXml = "";
#pragma warning restore CS0219 // The variable 'outInnerXml' is assigned but its value is never used
                string LOBName = DM.QuestionId.ToString();
#pragma warning disable CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
                string CurrentSecretery = "";
#pragma warning restore CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SessionDate' is assigned but its value is never used
                DateTime SessionDate = new DateTime();
#pragma warning restore CS0219 // The variable 'SessionDate' is assigned but its value is never used
                string fileName = "";
                string savedPDF = string.Empty;
                //iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 10, 30);

                if (DM.QuestPdfLang == 1)//For English
                {
                    outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;color:#003366;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div>

					</div>

<html>
<head>
<style>
	.tempalte_main_div{
		margin: 0 auto;
		font-family: arial sans-serif;
		font-size: 16px;
	}
	.tempalte_main_div .main_iner{
		padding: 3px;
	}
</style>

</head>
<body>
	<div class='tempalte_main_div'>
		<div class='main_iner'>
			<table>
<tr><td colspan='3' style='Vertical-align:top;'>
<table>

<tr>
<td style='font-size:18px;font-weight:800'>
Diary Number: &nbsp;" + DM.DiaryNumber + @"
</td>
</tr>

<tr>
<td style='font-size:16px;font-weight:700'>
Date: &nbsp;" + DM.PDfDate + @"
</td>
</tr>

<tr>
<td style='font-size:18px;font-weight:800'>
Time: &nbsp;" + DM.PDfTime + @"
</td>
</tr>
<tr><td><br></td></tr>
<tr>
<td style='font-size:18px;font-weight:800;text-align: right;'>
Signature
</td>

</tr>

</table>
</td></tr>
				<tr>

					<td style='width: 130px; vertical-align: top;'>
						<div style='padding-top: 10px;'>FOR OFFICE USE </div>
					</td>

					<td style='border-left:1px solid #333; padding: 0 10px;'>
						<table>
							<tr>
<td>
<table>
								<tr>
								<td style='text-align: center; vertical-align: top; font-size:20px; font-weight:bold'>
									QUESTION
								</td>
								</tr>

								<tr>
								<td >Subject &nbsp;  " + DM.Subject + @"
								</td>
								</tr>
								<tr>
								<td >Date &nbsp;&nbsp;&nbsp;&nbsp; " + DM.PDfDate + @"
								</td>
								</tr>
								<tr>
								<td >Date of Answer&nbsp;&nbsp;..................................................................................................
								</td>
								</tr>
								<tr>
								
								</tr>
</table>
</td>



							</tr>
							<tr>
								<td colspan='2' style='border-bottom: 1px solid #333;'>&nbsp;</td>
							</tr>

							<tr>
								<td colspan='2'>
									From:<br>
									<div style='padding-left: 30px;'>" + DM.MemberId.GetMemberNameByID() + @" , Councellor</div>
								</td>
							</tr>
							<tr>
								<td colspan='2'>
								&nbsp;To:<br>
								<div  style='padding-left: 30px;'> The Commissioner</div>
								<div  style='padding-left: 30px;'> Himachal Pradesh Municipal Corporation </div>
								<div  style='padding-left: 30px;'> Shimla 171004 </div>

								</td>
							</tr>
							<tr>
								<td colspan='2'>
								Sir, <br>
								&nbsp;&nbsp;&nbsp;&nbsp; Under Rule 41 of the Rules of Procedure and Conduct of Business, I give notice of the
								<br>following question for the answer on ..............................................................
								</td>
							</tr>
<tr>
								<td >&nbsp;</td>
								<td style='text-align: right;'><br>Yours faithfully,<br><br><br></td>

							</tr>

						  <tr>

								<td>&nbsp;<br></td>
								<td  style='text-align: right;'> " + DM.MemberId.GetMemberNameByIDWithOutPrefix() + @"</td>
   <tr>
<tr>
								<td>&nbsp;</td>
								<td  style='text-align: right;'>Member.</td>
							</tr>
								<td >&nbsp;</td>
								<td  style='text-align: right;'> " + DM.PDfDate + " " + DM.PDfTime + @"</td>
							</tr>
							</tr>

				<tr>

							</tr>

							<tr>
								<td colspan='2' style='text-align: right;'>Ward No. and Name: " + DM.ConstituencyCode + "  " + DM.ConstituencyName + @"
								&nbsp;</td>
							</tr>

							<tr>
								<td colspan='2'>ORDER OF PREFERENCE :- &nbsp;</td>
							</tr>
							<tr>
								<td colspan='2'><div  style='padding-left: 30px;'> *Will the  " + DM.MinistryId.GetMinisterName() + @" be pleased to state :</div>  </td>
							</tr>

							<tr>
								<td colspan='2'>
" + DM.MainQuestion + @"
</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>
		</div>
	</div>
</body>
</html>";
                    outXml += @"

					 </body> </html>";
                }
                else if (DM.QuestPdfLang == 2)//For Hindi
                {
                    outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;color:#003366;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div>

					</div>

<html>
<head>
<style>
	.tempalte_main_div{
		margin: 0 auto;
		font-family: arial sans-serif;
		font-size: 16px;
	}
	.tempalte_main_div .main_iner{
		padding: 3px;
	}
</style>

</head>
<body>
	<div class='tempalte_main_div'>
		<div class='main_iner'>
			<table>
<tr><td colspan='3' style='Vertical-align:top'>
<table>

<tr>
<td style='font-size:18px;font-weight:800'>
संख्या: &nbsp;" + DM.DiaryNumber + @"
</td>
</tr>

<tr>
<td style='font-size:18px;font-weight:800'>
तिथि: &nbsp;" + DM.PDfDate + @"
</td>
</tr>

<tr>
<td style='font-size:18px;font-weight:800'>
पावती समय: &nbsp;" + DM.PDfTime + @"
</td>
</tr>
<tr><td><br></td></tr>
<tr>
<td style='font-size:18px;font-weight:800;text-align: right;'>
हस्ताक्षर
</td>
</tr>

</table>
</td></tr>
				<tr>
					<td style='width: 130px; vertical-align: top;'>
						<div style='padding-top: 10px;'>कार्यालय प्रयोग हेतु </div>
					</td>
					<td style='border-left:1px solid #333; padding: 0 10px;'>
						<table>
							<tr>
<td>
<table>
								<tr>
								<td style='text-align: center; vertical-align: top; font-size:20px; font-weight:bold'>
									प्रश्न
								</td>
								</tr>

								<tr>
								<td >विषय &nbsp;  " + DM.Subject + @"
								</td>
								</tr>
								<tr>
								<td >सूचना प्राप्ति दिनांक &nbsp;&nbsp;&nbsp;&nbsp; " + DM.PDfDate + @"
								</td>
								</tr>
								<tr>
								<td >प्रश्नोत्तर दिनांक स्वीकृत अवस्था में &nbsp;&nbsp;................................................................................
								</td>
								</tr>
								<tr>
								<td colspan='2'>प्राथमिकता, यदि कोई हो &nbsp; " + DM.Priority + @"
								</td>
								</tr>
</table>
</td>



							</tr>
							<tr>
								<td colspan='2' style='border-bottom: 1px solid #333;'>&nbsp;</td>
							</tr>

							<tr>
								<td colspan='2'>
									प्रेषक:<br>
									<div style='padding-left: 30px;'>" + DM.MemberId.GetMemberNameByIDHindi() + @" , नगर निगम सदस्य।</div>
								</td>
							</tr>
							<tr>
								<td colspan='2'>
								&nbsp;सेवा में<br>
								<div  style='padding-left: 30px;'> आयुक्त,</div>
								<div  style='padding-left: 30px;'> हिमाचल प्रदेश नगर निगम,</div>
								<div  style='padding-left: 30px;'> शिमला-171004</div>

								</td>
							</tr>
							<tr>
								<td colspan='2'>
								महोदय, <br>
								&nbsp;&nbsp;&nbsp;&nbsp; प्रक्रिया तथा कार्य संचालन सम्बन्धी नियमों के नियम 41 के अन्तर्गत मैं निम्नलिखित प्रश्न की सूचना
								<br>देता हूं जिसका उत्तर .....................................को दिया जाए।
								</td>
							</tr>
<tr>
								<td >&nbsp;</td>
								<td style='text-align: right;'><br>भवदीय,<br><br><br></td>

							</tr>

						  <tr>

								<td>&nbsp;<br></td>
								<td  style='text-align: right;'> " + DM.MemberId.GetMemberNameByIDHindi() + @"</td>
   <tr>
<tr>
								<td>&nbsp;</td>
								<td  style='text-align: right;'>सदस्य।</td>
							</tr>
								<td >&nbsp;</td>
								<td  style='text-align: right;'> " + DM.PDfDate + " " + DM.PDfTime + @"</td>
							</tr>
							</tr>

				<tr>

							</tr>

							<tr>
								<td colspan='2' style='text-align: right;'>मुहल्ला संख्या व नाम:" + DM.ConstituencyCode + "  " + DM.ConsituencyLocal + @"
								&nbsp;</td>
							</tr>

							<tr>
								<td colspan='2'>प्राथमिकता क्रम :- &nbsp;</td>
							</tr>
							<tr>
								<td colspan='2'><div  style='padding-left: 30px;'> * क्या  " + DM.MinistryId.GetMinistryNameHindi() + @" आयुक्त बतलाने की कृपा करेंगे :</div>  </td>
							</tr>

							<tr>
								<td colspan='2'>
" + DM.MainQuestion + @"
</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>
		</div>
	</div>
</body>
</html>

";
                    outXml += @"

					 </body> </html>";
                }



                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 10, 10), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_S_Org_" + DM.QuestionId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public ActionResult GetQuestionDetailById(int questionID)
        {
            DiaryModel model = new DiaryModel();
            model.QuestionId = questionID;
            //model = (DiaryModel)Helper.ExecuteService("Notice", "GetQuestionDetailById", model);
            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@QuestionId", questionID.ToString()));
            System.Data.DataSet ds = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "sp_GetQuestionDetailById", methodParameter);
            model.QuestionId = Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionId"]);
            model.AssemblyID = Convert.ToInt32(ds.Tables[0].Rows[0]["AssemblyID"]);
            model.SessionID = Convert.ToInt32(ds.Tables[0].Rows[0]["SessionID"]);
            model.QuestionTypeId = Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionType"]);
            model.DiaryNumber = ds.Tables[0].Rows[0]["DiaryNumber"].ToString();
            model.ConstituencyName = ds.Tables[0].Rows[0]["ConstituencyName"].ToString();
            model.CatchWordSubject = ds.Tables[0].Rows[0]["SubInCatchWord"].ToString();
            model.Subject = ds.Tables[0].Rows[0]["Subject"].ToString();
            model.MinistryId = Convert.ToInt32(ds.Tables[0].Rows[0]["MinistryId"]);
            model.MinisterName = ds.Tables[0].Rows[0]["MinisterName"].ToString();
            model.DepartmentId = ds.Tables[0].Rows[0]["DepartmentId"].ToString();
            model.DepartmentName = ds.Tables[0].Rows[0]["DepartmentName"].ToString();
            model.MainQuestion = ds.Tables[0].Rows[0]["MainQuestion"].ToString();
            if (!(ds.Tables[0].Rows[0]["MemberSubmitDate"] is DBNull))
                model.DateForBind = Convert.ToDateTime(ds.Tables[0].Rows[0]["MemberSubmitDate"]);
            model.EventName = ds.Tables[0].Rows[0]["EventName"].ToString();
            model.Priority = Convert.ToInt32(ds.Tables[0].Rows[0]["Priority"]);
            model.IsSubmitToVS = Convert.ToBoolean(ds.Tables[0].Rows[0]["SubmitToVS"]);
            model.FilePath = ds.Tables[0].Rows[0]["OriMemFilePath"].ToString();
            model.FileName = ds.Tables[0].Rows[0]["OriMemFileName"].ToString();
            model.QuestionNumber = ds.Tables[0].Rows[0]["QuestionNumber"] is DBNull ? 0 : Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionNumber"]);
            model.IsFinalApprove = ds.Tables[0].Rows[0]["IsFinalApproved"] is DBNull ? false : Convert.ToBoolean(ds.Tables[0].Rows[0]["IsFinalApproved"]);
            model.SessionDateId = ds.Tables[0].Rows[0]["SessionDateId"] is DBNull ? 0 : Convert.ToInt32(ds.Tables[0].Rows[0]["SessionDateId"]);
            if (!(ds.Tables[0].Rows[0]["FixedDate"] is DBNull))
                model.FixedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["FixedDate"]);
            model.QuestionStatus = ds.Tables[0].Rows[0]["QuestionType1"] is DBNull ? 0 : Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionType1"]);
            model.AcessPath = ds.Tables[1].Rows[0]["SettingValue"].ToString();
            model.SignFilePath = CurrentSession.UserID.GetSignPath();
            var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "OnlineQButtonShow", null);
            model.ButtonOnlineShow = NewsSettings.SettingValue;
            return PartialView("_QuestionDetail", model);
        }

        public ActionResult EditQuestionNotice(int Id, string EditType)
        {
            DiaryModel DMdl = new DiaryModel();
            DiaryModel DMdl2 = new DiaryModel();
            DMdl.UserId = new Guid(CurrentSession.UserID);

            if (EditType == "Question")
            {
                if (Id != 0)
                {
                    DMdl.QuestionId = Id;
                }

                DMdl2 = Helper.ExecuteService("Notice", "GetQuestionByIdForEdit", DMdl) as DiaryModel;
                DMdl = DMdl2;
                DMdl.RIds = CurrentSession.ActionIds;
                DMdl.PaperEntryType = "UpdateEntry";

                if (DMdl.QuestionTypeId == 1)
                {
                    return PartialView("_NewStarredQEntryForm", DMdl);
                }
                else if (DMdl.QuestionTypeId == 2)
                {
                    return PartialView("_NewUnstarredEntryForm", DMdl);
                }
            }
            else if (EditType == "Notice")
            {
                if (Id != 0)
                {
                    DMdl.NoticeId = Id;
                }

                DMdl2 = Helper.ExecuteService("Notice", "GetNoticeByIdForEdit", DMdl) as DiaryModel;
                foreach (var test in DMdl2.DemandList)
                {
                    string val = test.DemandName + "(" + test.DemandNo + ")";
                    test.DemandName = val;
                }
                DMdl = DMdl2;
                if (DMdl.DemandNo == null)
                {
                    DMdl.DemandNo = 0;
                }
                DMdl.RIds = CurrentSession.ActionIds;
                DMdl.PaperEntryType = "UpdateEntry";
                return PartialView("_NoticeEntry", DMdl);
            }

            return null;
        }

        public JsonResult SubmitQuestionById(int Id, int AssemId, int SessId, int QuesTypeId)
        {
            DiaryModel DMdl = new DiaryModel();

            if (AssemId != 0)
            {
                DMdl.AssemblyID = AssemId;
            }
            if (SessId != 0)
            {
                DMdl.SessionID = SessId;
            }

            if (QuesTypeId != 0)
            {
                DMdl.QuestionTypeId = QuesTypeId;
            }
            DMdl.DiaryNumber = Helper.ExecuteService("Diary", "CheckDiaryNumber", DMdl) as string;

            if (Id != 0)
            {
                DMdl.QuestionId = Id;
            }

            DMdl = Helper.ExecuteService("Notice", "SubmitQuestionById", DMdl) as DiaryModel;

            if (DMdl.Message == "Success")
            {
                var Location = "";
                if (QuesTypeId == 1)
                {
                    Location = GeneratePdf(DMdl);
                }
                else if (QuesTypeId == 2)
                {
                    Location = GenerateUnstarredPdf(DMdl);
                }
                DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                //string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                // string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                //string textLine2 = DMdl.PDfDate + " " + DMdl.PDfTime;
                string textLine1 = "";
                string textLine2 = "";
                string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);
                PDFExtensions.InsertImageToPdf(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);

                //Update file name and location in both table
                var result = Helper.ExecuteService("Notice", "UpdateFile", DMdl) as string;

                if (result == "Updated")
                {
                    TempData["Msg"] = "Question Submitted Successfully and Diary Number is " + DMdl.DiaryNumber;

                    #region Send SMS

                    string SMSMessageText = "Your Online Question Recieved by Vidhan Shabha With Diary Number is " + DMdl.DiaryNumber + " on Date  " + DMdl.PDfDate + " Time " + DMdl.PDfTime;
                    SMSMessageList smsMessage = new SMSMessageList();
                    smsMessage.SMSText = SMSMessageText;
                    smsMessage.ModuleActionID = 1;
                    smsMessage.UniqueIdentificationID = 1;
                    smsMessage.MobileNo.Add(CurrentSession.MemberMobile);

                    Notification.Send(true, false, smsMessage, null);

                    #endregion Send SMS

                    if (QuesTypeId == 1)
                    {
                        TempData["CallingMethod"] = "Starred";
                    }
                    else if (QuesTypeId == 2)
                    {
                        TempData["CallingMethod"] = "Unstarred";
                    }
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(DMdl.Message, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        public JsonResult SubmitNoticeById(int Id, int AssemId, int SessId)
        {
            DiaryModel DMdl = new DiaryModel();

            if (AssemId != 0)
            {
                DMdl.AssemblyID = AssemId;
            }
            if (SessId != 0)
            {
                DMdl.SessionID = SessId;
            }
            if (Id != 0)
            {
                DMdl.NoticeId = Id;
            }
            //System.Data.Common.DbTransaction 
            //NoticeContext pCtxt = new NoticeContext();
            //using (var dbContextTransaction = context.Database.BeginTransaction())
            //{
            //    try
            //    {
            //        Some stuff
            //        dbContextTransaction.Commit();
            //    }
            //    catch (Exception)
            //    {
            //        dbContextTransaction.Rollback();
            //    }
            //} 
            DMdl = Helper.ExecuteService("Notice", "SubmitNoticeById", DMdl) as DiaryModel;
            if (DMdl.Message == "Success")
            {
                var Location = "";
                if (DMdl.EventId == 42)//Rule 62
                {
                    Location = GenerateNoticePdfOfRule62(DMdl);
                    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);
                    PDFExtensions.InsertImageToPdfForNotice(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                }
                else if (DMdl.EventId == 37)//Rule 67
                {
                    Location = GenerateNoticePdfOfRule67(DMdl);
                    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);
                    PDFExtensions.InsertImageToPdfForNotice(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                }
                else if (DMdl.EventId == 39)//Rule 324
                {
                    Location = GenerateNoticePdfOfRule324(DMdl);
                    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);
                    PDFExtensions.InsertImageToPdfForNotice(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                }

                else if (DMdl.EventId == 10)//Rule 63
                {
                    Location = GenerateNoticePdfOfRule63(DMdl);
                    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);
                    PDFExtensions.InsertImageToPdfForNotice(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                }

                else if (DMdl.EventId == 50)//Rule 101
                {
                    Location = GenerateNoticePdfOfRule101(DMdl);
                    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);
                    PDFExtensions.InsertImageToPdfForNotice(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                }

                else if (DMdl.EventId == 71)//Short Notice Rule 46
                {
                    Location = GenerateNoticePdfOfRule71(DMdl);
                    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);
                    PDFExtensions.InsertImageToPdfForNoticeRule71(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                }
                else if (DMdl.EventId == 43 || DMdl.EventId == 44)//Rule 117 and 130
                {
                    Location = GenerateNoticePdfOfRule117_130(DMdl);
                    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);
                    if (DMdl.EventId == 44)
                    {
                        PDFExtensions.InsertImageToPdfForNotice44(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                    }
                    else
                    {
                        PDFExtensions.InsertImageToPdfForNotice44(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                    }
                }
                else if (DMdl.EventId == 41)//Rule 61
                {
                    Location = GenerateNoticePdfOfRule61(DMdl);
                    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);

                    PDFExtensions.InsertImageToPdfForNoticeRule61(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                }
                else if (DMdl.EventId == 74)//Rule 74 Disapproval of Policy
                {
                    Location = GenerateNoticePdfOfRule74CutMotion(DMdl);
                    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);

                    PDFExtensions.InsertImageToPdfForNoticeRuleDisaprroval(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                }

                else if (DMdl.EventId == 75)//Rule 75 Economy Cut 
                {
                    Location = GenerateNoticePdfOfRule75EconomyCut(DMdl);
                    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);

                    PDFExtensions.InsertImageToPdfForNoticeRuleDisaprroval(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                }

                else if (DMdl.EventId == 76)//Rule 76 Token Cut  
                {
                    Location = GenerateNoticePdfOfRule76TokenCut(DMdl);
                    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);

                    PDFExtensions.InsertImageToPdfForNoticeRuleDisaprroval(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                }


                //else if (DMdl.EventId == 43 || DMdl.EventId == 44)//Rule 117 and 130
                //{
                //    Location = GenerateNoticePdfOfRule117_130(DMdl);
                //    DMdl.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                //    DMdl.FileName = Location.Substring(Location.IndexOf(",") + 1);
                //    DMdl.SignFilePath = CurrentSession.UserID.GetSignPath();
                //    string imageFileLoc = Server.MapPath(DMdl.SignFilePath);
                //    string textLine1 = DMdl.MemberId.GetMemberNameByIDWithOutPrefix();
                //    string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                //    string CompleteFile = Path.Combine(DMdl.FilePath, DMdl.FileName);
                //    string NewFile = Path.Combine(DMdl.FilePath, "sign_" + DMdl.FileName);
                //    PDFExtensions.InsertImageToPdfForNotice(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);
                //}


                //Update file name and location in both table
                var result = Helper.ExecuteService("Notice", "UpdateNoticeFile", DMdl) as string;
                if (result == "Updated")
                {
                    TempData["Msg"] = "Success Notice Submitted Successfully and Notice Number is " + DMdl.NoticeNumber;

                    #region Send SMS

                    string SMSMessageText = "Your Online Notice Recieved by Vidhan Shabha With Notice Number is " + DMdl.NoticeNumber + " on Date  " + DMdl.PDfDate + " Time " + DMdl.PDfTime;
                    SMSMessageList smsMessage = new SMSMessageList();
                    smsMessage.SMSText = SMSMessageText;
                    smsMessage.ModuleActionID = 1;
                    smsMessage.UniqueIdentificationID = 1;
                    smsMessage.MobileNo.Add(CurrentSession.MemberMobile);

                    Notification.Send(true, false, smsMessage, null);

                    #endregion Send SMS

                    return Json(TempData["Msg"].ToString(), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(DMdl.Message, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        public ActionResult UnStarredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            OnlineMemberQmodel model = new OnlineMemberQmodel();
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetOnlineUnStarredQuestions", model);

            if (PageNumber != null && PageNumber != "")
            {
                model.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                model.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                model.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                model.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                model.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                model.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                model.loopEnd = Convert.ToInt32("5");
            }

            return PartialView("UnstarredQuestion", model);
        }

        public ActionResult EntryFormUnStarredQ()
        {
            DiaryModel model = new DiaryModel();
            mEvent mEventModel = new mEvent();
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = siteSettingMod.SessionCode;
            model.AssemblyID = siteSettingMod.AssemblyCode;

            model.eventList = (List<mEvent>)Helper.ExecuteService("Events", "GetQuestionEvent", mEventModel);
            model.memMinList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;

            List<DiaryModel> DMList = new List<DiaryModel>();
            DiaryModel DM = new DiaryModel();
            DM.DepartmentId = "";
            DM.DepartmentName = "Select Department";
            DMList.Add(DM);
            model.DiaryList = DMList;
            model.PaperEntryType = "NewEntry";
            return PartialView("_NewUnstarredEntryForm", model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveUpdateEntryUnstarred(DiaryModel model)
        {
            model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model = Helper.ExecuteService("Notice", "SaveUpdateQuestionsUnstarredTable", model) as DiaryModel;

            // Generate Pdf and Save in File System And Update in Table
            if (model.BtnCaption == "Submit")
            {
                var Location = GenerateUnstarredPdf(model);
                model.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                model.FileName = Location.Substring(Location.IndexOf(",") + 1);
                model.SignFilePath = CurrentSession.UserID.GetSignPath();
                string imageFileLoc = Server.MapPath(model.SignFilePath);
                string textLine1 = model.MemberId.GetMemberNameByIDWithOutPrefix();
                string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                string CompleteFile = Path.Combine(model.FilePath, model.FileName);
                string NewFile = Path.Combine(model.FilePath, "sign_" + model.FileName);
                PDFExtensions.InsertImageToPdf(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);

                //Update file name and location in both table
                var result = Helper.ExecuteService("Notice", "UpdateFile", model) as string;

                if (result == "Updated")
                {
                    TempData["Msg"] = "Unstarred Question Submitted Successfully and Diary Number is " + model.DiaryNumber;
                }
                else
                {
                    TempData["Msg"] = result.ToString();
                }
            }

            //Show Message
            if (model.Message == "Saved")
            {
                TempData["Msg"] = "Unstarred Question Saved Successfully !";
            }
            else if (model.Message == "Updated")
            {
                TempData["Msg"] = "Unstarred Question Updated Successfully !";
            }
            else if (model.Message == "Submitted")
            {
                TempData["Msg"] = "Unstarred Question Submitted Successfully and Diary Number is " + model.DiaryNumber;
            }
            else
            {
                TempData["Msg"] = model.Message;
            }
            TempData["CallingMethod"] = "Unstarred";
            return RedirectToAction("Index");
        }

        //        public string GenerateUnstarredPdf(DiaryModel DM)
        //        {
        //            try
        //            {
        //                string outXml = "";
        //                string outInnerXml = "";
        //                string LOBName = DM.QuestionId.ToString();
        //                string CurrentSecretery = "";
        //                DateTime SessionDate = new DateTime();
        //                string fileName = "";
        //                string savedPDF = string.Empty;

        //                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        //                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

        //                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);

        //                if (DM.QuestPdfLang == 1) //For English
        //                {
        //                    outXml = @"<html>
        //								 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
        //                    outXml += @"<div>
        //
        //					</div>
        //
        //<html>
        //<head>
        //<style>
        //	.tempalte_main_div{
        //		margin: 0 auto;
        //		font-family: arial sans-serif;
        //		font-size: 16px;
        //	}
        //	.tempalte_main_div .main_iner{
        //		padding: 3px;
        //	}
        //</style>
        //
        //</head>
        //<body>
        //	<div class='tempalte_main_div'>
        //		<div class='main_iner'>
        //			<table>
        //				<tr>
        //					<td style='width: 130px; vertical-align: top;'>
        //						<div style='padding-top: 10px;'>FOR OFFICE USE </div>
        //					</td>
        //					<td style='border-left:1px solid #333; padding: 0 10px;'>
        //						<table>
        //							<tr>
        //<td>
        //<table>
        //								<tr>
        //								<td style='text-align: center; vertical-align: top; font-size:20px; font-weight:bold'>
        //									UNSTARRED QUESTION
        //								</td>
        //								</tr>
        //
        //								<tr>
        //								<td >Subject &nbsp;  " + DM.Subject + @"
        //								</td>
        //								</tr>
        //								<tr>
        //								<td >Date &nbsp;&nbsp;&nbsp;&nbsp; " + DM.PDfDate + @"
        //								</td>
        //								</tr>
        //								<tr>
        //								<td >Date of Answer&nbsp;&nbsp;..................................................................................................
        //								</td>
        //								</tr>
        //								<tr>
        //								<td colspan='2'>Priority, if any &nbsp; " + DM.Priority + @"
        //								</td>
        //								</tr>
        //</table>
        //</td>
        //
        //<td style='Vertical-align:top'>
        //<table>
        //
        //<tr>
        //<td style='font-size:18px;font-weight:800'>
        //Diary Number: &nbsp;" + DM.DiaryNumber + @"
        //</td>
        //</tr>
        //
        //<tr>
        //<td style='font-size:18px;font-weight:800'>
        //Date: &nbsp;" + DM.PDfDate + @"
        //</td>
        //</tr>
        //
        //<tr>
        //<td style='font-size:18px;font-weight:800'>
        //Time: &nbsp;" + DM.PDfTime + @"
        //</td>
        //</tr>
        //
        //<tr>
        //<td style='font-size:18px;font-weight:800;text-align: right;'>
        //Signature
        //</td>
        //</tr>
        //
        //</table>
        //</td>
        //
        //							</tr>
        //							<tr>
        //								<td colspan='2' style='border-bottom: 1px solid #333;'>&nbsp;</td>
        //							</tr>
        //
        //							<tr>
        //								<td colspan='2'>
        //									From:<br>
        //									<div style='padding-left: 30px;'>" + DM.MemberId.GetMemberNameByID() + @" , M.L.A</div>
        //								</td> 
        //							</tr>
        //							<tr>
        //								<td colspan='2'>
        //								&nbsp;To:<br>
        //								<div  style='padding-left: 30px;'> The Secretary</div>
        //								<div  style='padding-left: 30px;'> Himachal Pradesh Legislative Assembly</div>
        //								<div  style='padding-left: 30px;'> Shimla 171004</div>
        //
        //								</td>
        //							</tr>
        //							<tr>
        //								<td colspan='2'>
        //								Sir, <br>
        //								&nbsp;&nbsp;&nbsp;&nbsp; Under Rule 44 of the Rules of Procedure and Conduct of Business, I give notice of the
        //								<br>following starred question for the answer on ..............................................................
        //								</td>
        //							</tr>
        //<tr>
        //								<td >&nbsp;</td>
        //								<td style='text-align: right;'><br>Yours faithfully,<br><br><br></td>
        //
        //							</tr>
        //
        //						  <tr>
        //
        //								<td>&nbsp;<br></td>
        //								<td  style='text-align: right;'> " + DM.MemberId.GetMemberNameByIDWithOutPrefix() + @"</td>
        //   <tr>
        //<tr>
        //								<td>&nbsp;</td>
        //								<td  style='text-align: right;'>Member.</td>
        //							</tr>
        //								<td >&nbsp;</td>
        //								<td  style='text-align: right;'> " + DM.PDfDate + " " + DM.PDfTime + @"</td>
        //							</tr>
        //							</tr>
        //
        //				<tr>
        //
        //							</tr>
        //
        //							<tr>
        //								<td colspan='2' style='text-align: right;'>Division No. and Name: " + DM.ConstituencyCode + "  " + DM.ConstituencyName + @"
        //								&nbsp;</td>
        //							</tr>
        //
        //							
        //							<tr>
        //								<td colspan='2'><div  style='padding-left: 30px;'> Will the  " + DM.MinistryId.GetMinisterName() + @" be pleased to state :</div>  </td>
        //							</tr>
        //
        //							<tr>
        //								<td colspan='2'>
        //" + DM.MainQuestion + @"
        //</td>
        //							</tr>
        //						</table>
        //					</td>
        //				</tr>
        //
        //			</table>
        //		</div>
        //	</div>
        //</body>
        //</html>
        //
        //"; outXml += @"
        //
        //					 </body> </html>";
        //                }
        //                else if (DM.QuestPdfLang == 2) //For Hindi
        //                {
        //                    outXml = @"<html>
        //								 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
        //                    outXml += @"<div>
        //
        //					</div>
        //
        //<html>
        //<head>
        //<style>
        //	.tempalte_main_div{
        //		margin: 0 auto;
        //		font-family: arial sans-serif;
        //		font-size: 16px;
        //	}
        //	.tempalte_main_div .main_iner{
        //		padding: 3px;
        //	}
        //</style>
        //
        //</head>
        //<body>
        //	<div class='tempalte_main_div'>
        //		<div class='main_iner'>
        //			<table>
        //				<tr>
        //					<td style='width: 130px; vertical-align: top;'>
        //						<div style='padding-top: 10px;'>कार्यालय प्रयोग हेतु </div>
        //					</td>
        //					<td style='border-left:1px solid #333; padding: 0 10px;'>
        //						<table>
        //							<tr>
        //<td>
        //<table>
        //								<tr>
        //								<td style='text-align: center; vertical-align: top; font-size:20px; font-weight:bold'>
        //									अतारांकित प्रश्न
        //								</td>
        //								</tr>
        //
        //								<tr>
        //								<td >विषय &nbsp;  " + DM.Subject + @"
        //								</td>
        //								</tr>
        //								<tr>
        //								<td >सूचना प्राप्ति दिनांक &nbsp;&nbsp;&nbsp;&nbsp; " + DM.PDfDate + @"
        //								</td>
        //								</tr>
        //								<tr>
        //								<td >प्रश्नोत्तर दिनांक स्वीकृत अवस्था में &nbsp;&nbsp;................................................................................
        //								</td>
        //								</tr>
        //								<tr>
        //								<td colspan='2'>प्राथमिकता, यदि कोई हो &nbsp; " + DM.Priority + @"
        //								</td>
        //								</tr>
        //</table>
        //</td>
        //
        //<td style='Vertical-align:top'>
        //<table>
        //
        //<tr>
        //<td style='font-size:18px;font-weight:800'>
        //संख्या: &nbsp;" + DM.DiaryNumber + @"
        //</td>
        //</tr>
        //
        //<tr>
        //<td style='font-size:18px;font-weight:800'>
        //तिथि: &nbsp;" + DM.PDfDate + @"
        //</td>
        //</tr>
        //
        //<tr>
        //<td style='font-size:18px;font-weight:800'>
        //पावती समय: &nbsp;" + DM.PDfTime + @"
        //</td>
        //</tr>
        //
        //<tr>
        //<td style='font-size:18px;font-weight:800;text-align: right;'>
        //हस्ताक्षर
        //</td>
        //</tr>
        //
        //</table>
        //</td>
        //
        //							</tr>
        //							<tr>
        //								<td colspan='2' style='border-bottom: 1px solid #333;'>&nbsp;</td>
        //							</tr>
        //
        //							<tr>
        //								<td colspan='2'>
        //									प्रेषक:<br>
        //									<div style='padding-left: 30px;'>" + DM.MemberId.GetMemberNameByIDHindi() + @" , विधान सभा सदस्य । </div>
        //								</td>
        //							</tr>
        //							<tr>
        //								<td colspan='2'>
        //								&nbsp;सेवा में<br>
        //								<div  style='padding-left: 30px;'> सचिव,</div>
        //								<div  style='padding-left: 30px;'> हिमाचल प्रदेश विधान सभा,</div>
        //								<div  style='padding-left: 30px;'> शिमला-171004.</div>
        //
        //								</td>
        //							</tr>
        //							<tr>
        //								<td colspan='2'>
        //								महोदय, <br>
        //								&nbsp;&nbsp;&nbsp;&nbsp; प्रकिया तथा कार्य संचालन सम्बन्धी नियमों के नियम 44 के  अन्तर्गत मैं निम्नलिखित अतारांकित प्रश्न
        //								<br>की सूचना देता हूँ  जिसका उत्तर दिनाकं............................... को दिया जाए:
        //								</td>
        //							</tr>
        //<tr>
        //								<td >&nbsp;</td>
        //								<td style='text-align: right;'><br>भवदीय,<br><br><br></td>
        //
        //							</tr>
        //
        //						  <tr>
        //
        //								<td>&nbsp;<br></td>
        //								<td  style='text-align: right;'> " + DM.MemberId.GetMemberNameByIDHindi() + @"</td>
        //   <tr>
        //<tr>
        //								<td>&nbsp;</td>
        //								<td  style='text-align: right;'>सदस्य।</td>
        //							</tr>
        //								<td >&nbsp;</td>
        //								<td  style='text-align: right;'> " + DM.PDfDate + " " + DM.PDfTime + @"</td>
        //							</tr>
        //							</tr>
        //
        //				<tr>
        //
        //							</tr>
        //
        //							<tr>
        //								<td colspan='2' style='text-align: right;'>निर्वाचन क्षेत्र संख्या व नाम: " + DM.ConstituencyCode + "  " + DM.ConsituencyLocal + @"
        //								&nbsp;</td>
        //							</tr>
        //
        //							
        //							<tr>
        //								<td colspan='2'><div  style='padding-left: 30px;'>  क्या  " + DM.MinistryId.GetMinistryNameHindi() + @" मन्त्री बतलाने की कृपा करेंगे :</div>  </td>
        //							</tr>
        //
        //							<tr>
        //								<td colspan='2'>
        //" + DM.MainQuestion + @"
        //</td>
        //							</tr>
        //						</table>
        //					</td>
        //				</tr>
        //
        //			</table>
        //		</div>
        //	</div>
        //</body>
        //</html>
        //
        //"; outXml += @"
        //
        //					 </body> </html>";
        //                }



        //                MemoryStream output = new MemoryStream();

        //                EvoPdf.Document document1 = new EvoPdf.Document();

        //                // set the license key
        //                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
        //                document1.CompressionLevel = PdfCompressionLevel.Best;
        //                document1.Margins = new Margins(0, 0, 0, 0);
        //                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

        //                AddElementResult addResult;

        //                HtmlToPdfElement htmlToPdfElement;
        //                string htmlStringToConvert = outXml;
        //                string baseURL = "";
        //                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

        //                addResult = page.AddElement(htmlToPdfElement);
        //                byte[] pdfBytes = document1.Save();

        //                try
        //                {
        //                    output.Write(pdfBytes, 0, pdfBytes.Length);
        //                    output.Position = 0;
        //                }
        //                finally
        //                {
        //                    // close the PDF document to release the resources
        //                    document1.Close();
        //                }
        //                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
        //                fileName = DM.AssemblyID + "_" + DM.SessionID + "_U_Org_" + DM.QuestionId + ".pdf";

        //                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

        //                string directory1 = DM.SaveFilePath + url;
        //                if (!Directory.Exists(directory1))
        //                {
        //                    Directory.CreateDirectory(directory1);
        //                }
        //                var path1 = Path.Combine(directory1, fileName);
        //                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
        //                // close file stream
        //                _FileStream1.Close();


        //                string directory = Server.MapPath(url);
        //                if (!Directory.Exists(directory))
        //                {
        //                    Directory.CreateDirectory(directory);
        //                }

        //                var path = Path.Combine(Server.MapPath("~" + url), fileName);
        //                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
        //                _FileStream.Close();

        //                return url + "," + fileName;
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //            finally
        //            {
        //            }
        //        }
        public string GenerateUnstarredPdf(DiaryModel DM)
        {
            try
            {
                string outXml = "";
#pragma warning disable CS0219 // The variable 'outInnerXml' is assigned but its value is never used
                string outInnerXml = "";
#pragma warning restore CS0219 // The variable 'outInnerXml' is assigned but its value is never used
                string LOBName = DM.QuestionId.ToString();
#pragma warning disable CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
                string CurrentSecretery = "";
#pragma warning restore CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SessionDate' is assigned but its value is never used
                DateTime SessionDate = new DateTime();
#pragma warning restore CS0219 // The variable 'SessionDate' is assigned but its value is never used
                string fileName = "";
                string savedPDF = string.Empty;

                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                //iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 10, 30);

                if (DM.QuestPdfLang == 1) //For English
                {
                    outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div>

					</div>

<html>
<head>
<style>
	.tempalte_main_div{
		margin: 0 auto;
		font-family: arial sans-serif;
		font-size: 16px;
	}
	.tempalte_main_div .main_iner{
		padding: 3px;
	}
</style>

</head>
<body>
	<div class='tempalte_main_div'>
		<div class='main_iner'>
			<table>
<tr><td colspan='3' style='Vertical-align:top'>
<table>

<tr>
<td style='font-size:18px;font-weight:800'>
Diary Number: &nbsp;" + DM.DiaryNumber + @"
</td>
</tr>

<tr>
<td style='font-size:18px;font-weight:800'>
Date: &nbsp;" + DM.PDfDate + @"
</td>
</tr>

<tr>
<td style='font-size:18px;font-weight:800'>
Time: &nbsp;" + DM.PDfTime + @"
</td>
</tr>
<tr><td><br/></td></tr>
<tr>
<td style='font-size:18px;font-weight:800;text-align: right;'>
Signature
</td>
</tr>

</table>
</td>
</tr>
				<tr>
					<td style='width: 130px; vertical-align: top;'>
						<div style='padding-top: 10px;'>FOR OFFICE USE </div>
					</td>
					<td style='border-left:1px solid #333; padding: 0 10px;'>
						<table>

							<tr>
<td>
<table>
								<tr>
								<td style='text-align: center; vertical-align: top; font-size:20px; font-weight:bold'>
									UNSTARRED QUESTION
								</td>
								</tr>

								<tr>
								<td >Subject &nbsp;  " + DM.Subject + @"
								</td>
								</tr>
								<tr>
								<td >Date &nbsp;&nbsp;&nbsp;&nbsp; " + DM.PDfDate + @"
								</td>
								</tr>
								<tr>
								<td >Date of Answer&nbsp;&nbsp;..................................................................................................
								</td>
								</tr>
								<tr>
								<td colspan='2'>Priority, if any &nbsp; " + DM.Priority + @"
								</td>
								</tr>
</table>
</td>


							</tr>
							<tr>
								<td colspan='2' style='border-bottom: 1px solid #333;'>&nbsp;</td>
							</tr>

							<tr>
								<td colspan='2'>
									From:<br>
									<div style='padding-left: 30px;'>" + DM.MemberId.GetMemberNameByID() + @" , M.L.A</div>
								</td> 
							</tr>
							<tr>
								<td colspan='2'>
								&nbsp;To:<br>
								<div  style='padding-left: 30px;'> The Secretary</div>
								<div  style='padding-left: 30px;'> Himachal Pradesh Legislative Assembly</div>
								<div  style='padding-left: 30px;'> Shimla 171004</div>

								</td>
							</tr>
							<tr>
								<td colspan='2'>
								Sir, <br>
								&nbsp;&nbsp;&nbsp;&nbsp; Under Rule 44 of the Rules of Procedure and Conduct of Business, I give notice of the
								<br>following starred question for the answer on ..............................................................
								</td>
							</tr>
<tr>
								<td >&nbsp;</td>
								<td style='text-align: right;'><br>Yours faithfully,<br><br><br></td>

							</tr>

						  <tr>

								<td>&nbsp;<br></td>
								<td  style='text-align: right;'> " + DM.MemberId.GetMemberNameByIDWithOutPrefix() + @"</td>
   <tr>
<tr>
								<td>&nbsp;</td>
								<td  style='text-align: right;'>Member.</td>
							</tr>
								<td >&nbsp;</td>
								<td  style='text-align: right;'> " + DM.PDfDate + " " + DM.PDfTime + @"</td>
							</tr>
							</tr>

				<tr>

							</tr>

							<tr>
								<td colspan='2' style='text-align: right;'>Division No. and Name: " + DM.ConstituencyCode + "  " + DM.ConstituencyName + @"
								&nbsp;</td>
							</tr>

							
							<tr>
								<td colspan='2'><div  style='padding-left: 30px;'> Will the  " + DM.MinistryId.GetMinisterName() + @" be pleased to state :</div>  </td>
							</tr>

							<tr>
								<td colspan='2'>
" + DM.MainQuestion + @"
</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>
		</div>
	</div>
</body>
</html>

"; outXml += @"

					 </body> </html>";
                }
                else if (DM.QuestPdfLang == 2) //For Hindi
                {
                    outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div>

					</div>

<html>
<head>
<style>
	.tempalte_main_div{
		margin: 0 auto;
		font-family: arial sans-serif;
		font-size: 16px;
	}
	.tempalte_main_div .main_iner{
		padding: 3px;
	}
</style>

</head>
<body>
	<div class='tempalte_main_div'>
		<div class='main_iner'>
			<table>
<tr><td colspan='3' style='Vertical-align:top'>
<table>

<tr>
<td style='font-size:18px;font-weight:800'>
संख्या: &nbsp;" + DM.DiaryNumber + @"
</td>
</tr>

<tr>
<td style='font-size:18px;font-weight:800'>
तिथि: &nbsp;" + DM.PDfDate + @"
</td>
</tr>

<tr>
<td style='font-size:18px;font-weight:800'>
पावती समय: &nbsp;" + DM.PDfTime + @"
</td>
</tr>
<tr><td><br/></td></tr>
<tr>
<td style='font-size:18px;font-weight:800;text-align: right;'>
हस्ताक्षर
</td>
</tr>

</table>
</td></tr>
				<tr>
					<td style='width: 130px; vertical-align: top;'>
						<div style='padding-top: 10px;'>कार्यालय प्रयोग हेतु </div>
					</td>
					<td style='border-left:1px solid #333; padding: 0 10px;'>
						<table>
							<tr>
<td>
<table>
								<tr>
								<td style='text-align: center; vertical-align: top; font-size:20px; font-weight:bold'>
									अतारांकित प्रश्न
								</td>
								</tr>

								<tr>
								<td >विषय &nbsp;  " + DM.Subject + @"
								</td>
								</tr>
								<tr>
								<td >सूचना प्राप्ति दिनांक &nbsp;&nbsp;&nbsp;&nbsp; " + DM.PDfDate + @"
								</td>
								</tr>
								<tr>
								<td >प्रश्नोत्तर दिनांक स्वीकृत अवस्था में &nbsp;&nbsp;................................................................................
								</td>
								</tr>
								<tr>
								<td colspan='2'>प्राथमिकता, यदि कोई हो &nbsp; " + DM.Priority + @"
								</td>
								</tr>
</table>
</td>



							</tr>
							<tr>
								<td colspan='2' style='border-bottom: 1px solid #333;'>&nbsp;</td>
							</tr>

							<tr>
								<td colspan='2'>
									प्रेषक:<br>
									<div style='padding-left: 30px;'>" + DM.MemberId.GetMemberNameByIDHindi() + @" , विधान सभा सदस्य । </div>
								</td>
							</tr>
							<tr>
								<td colspan='2'>
								&nbsp;सेवा में<br>
								<div  style='padding-left: 30px;'> सचिव,</div>
								<div  style='padding-left: 30px;'> हिमाचल प्रदेश विधान सभा,</div>
								<div  style='padding-left: 30px;'> शिमला-171004.</div>

								</td>
							</tr>
							<tr>
								<td colspan='2'>
								महोदय, <br>
								&nbsp;&nbsp;&nbsp;&nbsp; प्रकिया तथा कार्य संचालन सम्बन्धी नियमों के नियम 44 के  अन्तर्गत मैं निम्नलिखित अतारांकित प्रश्न
								<br>की सूचना देता हूँ  जिसका उत्तर दिनाकं............................... को दिया जाए:
								</td>
							</tr>
<tr>
								<td >&nbsp;</td>
								<td style='text-align: right;'><br>भवदीय,<br><br><br></td>

							</tr>

						  <tr>

								<td>&nbsp;<br></td>
								<td  style='text-align: right;'> " + DM.MemberId.GetMemberNameByIDHindi() + @"</td>
   <tr>
<tr>
								<td>&nbsp;</td>
								<td  style='text-align: right;'>सदस्य।</td>
							</tr>
								<td >&nbsp;</td>
								<td  style='text-align: right;'> " + DM.PDfDate + " " + DM.PDfTime + @"</td>
							</tr>
							</tr>

				<tr>

							</tr>

							<tr>
								<td colspan='2' style='text-align: right;'>निर्वाचन क्षेत्र संख्या व नाम: " + DM.ConstituencyCode + "  " + DM.ConsituencyLocal + @"
								&nbsp;</td>
							</tr>

							
							<tr>
								<td colspan='2'><div  style='padding-left: 30px;'>  क्या  " + DM.MinistryId.GetMinistryNameHindi() + @" मन्त्री बतलाने की कृपा करेंगे :</div>  </td>
							</tr>

							<tr>
								<td colspan='2'>
" + DM.MainQuestion + @"
</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>
		</div>
	</div>
</body>
</html>

"; outXml += @"

					 </body> </html>";
                }



                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 10, 10), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_U_Org_" + DM.QuestionId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();


                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public ActionResult GetAllOnlineNotices()
        {
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetAllOnlineNotices", model);
            return PartialView("_OnlineNotice", model);
        }

        public ActionResult MainOnlineDashboard()
        {
            tMemberNotice model = new tMemberNotice();
            try
            {
                model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetOnlineMemberDashboardItemsCounter", model);
                model.UserName = CurrentSession.UserName;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

            //mUsers user = new mUsers();
            //user.UserId = new Guid(CurrentSession.UserID);
            //user.IsMember = CurrentSession.IsMember;

            //user = (mUsers)Helper.ExecuteService("User", "GetIsMemberDetails", user);
            //if (user != null)
            //{
            //    model.UserName = user.Name;
            //}

            return PartialView("_OnlineMemberMainDashboard", model);
        }

        public ActionResult GetActionView(string PaperType, int? AID, int? SID, int? Count)
        {
            DiaryModel model = new DiaryModel();
            model.PaperEntryType = PaperType;
            model.SessionID = SID ?? 0;
            model.AssemblyID = AID ?? 0;
            model.ResultCount = Count ?? 0;
            //model.RIds = ActionId;
            CurrentSession.SPSubject = "";
            CurrentSession.SPMinId = "";
            CurrentSession.SPFDate = "";
            CurrentSession.SPTDate = "";
            CurrentSession.SPDNum = "";
            //if (PaperType == "View MLA Diary" || PaperType == "View Works")
            //{
            //    return PartialView("_ActionViewConstituency", model);
            //}
            return PartialView("_ActionView", model);
        }

        public ActionResult GetListByPaperType(string PaperType, int AssemId, int SessId)
        {
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            model.AssemblyID = AssemId;
            model.SessionID = SessId;
            //if (PaperType == "Starred Questions")
            if (PaperType.ToLower() == "questions" || PaperType == "Starred Questions")
            {
                model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetOnlineStarredQuestions", model);
                //List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
                //methodParameter.Add(new KeyValuePair<string, string>("@StartRowIndex", "0"));
                //methodParameter.Add(new KeyValuePair<string, string>("@PageSize", "3"));
                ////methodParameter.Add(new KeyValuePair<string, string>("@TotalCount", "1"));
                //System.Data.DataSet ds = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "sp_OnlineQuestionList", methodParameter);
                //model.tQuestionModel = ds.Tables[0].AsEnumerable().Select(dataRow => new OnlineMemberQmodel
                //{
                //    QuestionID = dataRow.Field<Int32>("QuestionID"),
                //    SessionID = dataRow.Field<Int32>("SessionID"),
                //    AssemblyID = dataRow.Field<Int32>("AssemblyID"),
                //    Subject = dataRow.Field<string>("Subject"),
                //    //MemberSubmitDate = dataRow.Field<string>("MemberSubmitDate")
                //}).ToList();
                return PartialView("StarredQuestions", model);
            }
            else if (PaperType == "Unstarred Questions")
            {
                model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetOnlineUnStarredQuestions", model);
                return PartialView("UnstarredQuestion", model);
            }
            else if (PaperType.ToLower() == "notices" || PaperType.ToLower() == "resolutions")
            {
                model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetAllOnlineNotices", model);
                return PartialView("_OnlineNotice", model);
            }
            else if (PaperType == "Category Gallery")
            {
                //model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetAllOnlineNotices", model);
                //return PartialView("_OnlineNotice", model);
                return RedirectToAction("GetAllMemberCategory");
            }
            else if (PaperType == "Album Gallery")
            {
                //model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetAllOnlineNotices", model);
                //return PartialView("_OnlineNotice", model);
                return RedirectToAction("GetGalleryAlbumIndex");
            }
            else if (PaperType == "Gallery")
            {
                //model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetAllOnlineNotices", model);
                //return PartialView("_OnlineNotice", model);
                return RedirectToAction("GetGalleryForm");
            }
            else if (PaperType == "Today Tours")
            {
                return RedirectToAction("TodayMemberTourList");
            }
            else if (PaperType == "Upcoming Tours")
            {
                return RedirectToAction("UpcomingMemberTourList");
            }
            else if (PaperType == "Previous Tours")
            {
                return RedirectToAction("PreviousMemberTourList");
            }
            else if (PaperType == "UnPublished Tours")
            {
                return RedirectToAction("UnPublishedTourList");
            }
            else if (PaperType == "Generic Groups")
            {
                return RedirectToAction("AllRecipientGroupList");
            }
            else if (PaperType == "Generic Group Members")
            {
                return RedirectToAction("AllRecipientGroupMemberList");
            }
            else if (PaperType == "Panchayat Groups")
            {
                return RedirectToAction("AllPanchayatGroupList");
            }
            else if (PaperType == "Panchayat Group Members")
            {
                return RedirectToAction("AllPanchayatGroupMemberList");
            }
            else if (PaperType == "Constituency Groups")
            {
                return RedirectToAction("AllConstituencyGroupList");
            }
            else if (PaperType == "Constituency Group Members")
            {
                return RedirectToAction("AllConstituencyGroupMemberList");
            }
            else if (PaperType == "Compose SMS")
            {
                return RedirectToAction("CustomizeSMSEMAIL");
            }
            //else if (PaperType == "Constituency and Panchayat")
            //{
            //	return RedirectToAction("GetAllDisrictPanchayat");
            //}
            else if (PaperType == "ConstituencyOfficeGroups")
            {
                return RedirectToAction("AllConstituencyOfficeGroupList");
            }
            else if (PaperType == "ConstituencyOfficeGroupMembers")
            {
                return RedirectToAction("AllConstituencyOfficeGroupMemberList");
            }
            else if (PaperType == "Pending Request")
            {
                return PartialView("_GetPendingListUserAccess");
            }
            else if (PaperType == "View/Update")
            {
                return PartialView("_GetGrievanceList");
            }
            else if (PaperType == "Accepted Request")
            {
                return PartialView("_GetAcceptedListUserAccess");
            }
            else if (PaperType == "Rejected Request")
            {
                return PartialView("_GetRejectedListUserAccess");
            }
            else if (PaperType == "Reimbursement Status")
            {
                return PartialView("_ReimbursementBills", model);
            }
            else if (PaperType == "Individual Passes")
            {
                return View("_passInd");
            }
            else if (PaperType == "Vehicle Passes")
            {
                return View("_passVehicle");
            }
            else if (PaperType == "Salary Slip")
            {
                return PartialView("_salary");
            }
            else if (PaperType == "Sent SMS Report")
            {
                return PartialView("_SMSRpt");
            }
            else if (PaperType == "SMS Report")
            {
                return PartialView("_SMSRpt");
            }
            else if (PaperType == "Sent Email Report")
            {
                return PartialView("_EmailRpt");
            }
            else if (PaperType == "Block and Panchayat")
            {
                return PartialView("_BlockPanchayat");
            }
            else if (PaperType == "Panchayat and Village")
            {
                return PartialView("_PanchayatAndVillage");
            }
            else if (PaperType == "Choose Budgeted Works")
            {
                return PartialView("_ChooseWorks");
            }
            else if (PaperType == "Manage Works")
            {
                return PartialView("_UpdateWorks");
            }

            else if (PaperType == "View Works")
            {
                return PartialView("ViewWorks");
            }
            else if (PaperType == "View Councillor Diary")
            {
                return PartialView("_ViewMLADiary");
            }
            else if (PaperType == "Department Wise Report")
            {
                return PartialView("_DepartmentWiseReport");
            }
            else if (PaperType == "Manage Proposals")
            {
                return PartialView("_ManageProposals");
            }
            else if (PaperType == "Sanctioned Proposals")
            {
                return PartialView("_SanctionedProposals");
            }
            else if (PaperType == "Offices/Institutions")
            {
                return PartialView("_pmisReports");
            }
            else if (PaperType == "Aminities")
            {
                return PartialView("_Amities");
            }

            else if (PaperType == "Old Clippings")
            {
                return PartialView("_OldPressClippingForMember");
            }
            else if (PaperType == "Todays Clipping")
            {
                return PartialView("_TodayPressClippingForMember");
            }
            else if (PaperType == "eFile")
            {
                return PartialView("/Areas/eFile/Views/eFile/_MethodeFile.cshtml");
            }
            else if (PaperType == "Receive Paper")
            {
                return PartialView("/Areas/eFile/Views/eFile/_MethodReceive.cshtml");
            }
            else if (PaperType == "Send Paper")
            {
                return PartialView("/Areas/eFile/Views/eFile/_MethodSend.cshtml");
            }
            else if (PaperType == "Feedback")
            {
                // return PartialView("_LettersView");
                return PartialView("_FeedbackIndexView");
            }
            else if (PaperType == "Helpline")
            {
                // return PartialView("_LettersView");
                return PartialView("_HelplineIndexView");
            }
            else if (PaperType == "Introduced Bills")
            {
                model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetIntroducedBills", model);
                var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                model.AceesPath = Acess.SettingValue;
                return PartialView("_IntroducedBillsList", model);
            }
            else if (PaperType == "Councillor Diary")
            {
                ViewBag.PageSize = 25;
                ViewBag.CurrentPage = 1;
                MlaDiary model1 = new MlaDiary();
                // model.AssemblyId = CurrentSession.AssemblyId;
                // model.SessionId = CurrentSession.SessionId;
                //var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                //var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
                //////ViewBag.sessiondates = new SelectList(SessionDate, "SessionDates", "SessionDates", null);
                //model1.DocumentTypelist = DocumentTypelist;
                //model1.ActionTypeList = ActionTypelist;
                //model1.BtnCaption = "Save";
                //model1.PaperEntryType = "NewEntry";
                if (CurrentSession.MemberCode != "" || CurrentSession.MemberCode != null)
                {
                    model1.MlaCode = Convert.ToInt32(CurrentSession.MemberCode);
                }
                else
                {

                    model1.MlaCode = 0;
                }
                model1.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
                model1.DocmentType = "1"; //Development
                model1.ActionCode = 0; //Development
                model1.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryList", model1);

                // search
                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
                ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCount", model1) as string;
                ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCount", model1) as string;
                ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCount", model1) as string;
                ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCount", model1) as string;

                SBL.DomainModel.Models.Office.tDepartmentOfficeMapped tmodel = new DomainModel.Models.Office.tDepartmentOfficeMapped();
                tmodel.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
                var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllMappedOfficeByMemberCode", tmodel);
                model1.OfficeList = GetData1;


                model1.DocumentTypelist = DocumentTypelist;
                model1.ActionTypeList = ActionTypelist;
                model1.FinanCialYearList = GetFinancialYearList();
                model1.PaperEntryType = PaperType;
                ViewBag.DocType = "1";
                ViewBag.ActionType = "0";
                ViewBag.PendencySince = "0";
                return PartialView("_MlaDiaryList", model1);
            }

            return null;
        }
        [ValidateInput(false)]
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveUpdateNoticeEntry(HttpPostedFileBase file, DiaryModel model, FormCollection col)
        {
            var subject1 = col["Subject2"];
            var subject2 = col["Subject3"];

            if (subject1 != "" && subject1 != null)
            {
                model.Subject = model.Subject + "ḝ" + subject1;

            }
            if (subject2 != "" && subject2 != null)
            {
                model.Subject = model.Subject + "ḝ" + subject2;
            }

            model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);

            if (file != null)
            {
                string url = "/OnlineMemberFile/" + model.AssemblyID + "/" + model.SessionID + "/" + "AttachDocByMember/";
                model.FileName = file.FileName;
                model.FilePath = url;

                string directory = Server.MapPath("~" + url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                file.SaveAs(Server.MapPath("~" + url + file.FileName));
            }
            model = Helper.ExecuteService("Notice", "SaveUpdateNotice", model) as DiaryModel;

            // Generate Pdf and Save in File System And Update in Table
            if (model.BtnCaption == "Submit")
            {
                var Location = "";
                if (model.EventId == 42)//Rule 62
                {
                    Location = GenerateNoticePdfOfRule62(model);
                }
                else if (model.EventId == 37)//Rule 67
                {
                    Location = GenerateNoticePdfOfRule67(model);
                }
                else if (model.EventId == 39)//Rule 324
                {
                    Location = GenerateNoticePdfOfRule324(model);
                }
                //else if (model.EventId == 43 || model.EventId == 44)//Rule 117 and 130
                //{
                //    Location = GenerateNoticePdfOfRule117_130(model);
                //}
                model.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                model.FileName = Location.Substring(Location.IndexOf(",") + 1);
                model.SignFilePath = CurrentSession.UserID.GetSignPath();
                string imageFileLoc = Server.MapPath(model.SignFilePath);
                string textLine1 = model.MemberId.GetMemberNameByIDWithOutPrefix();
                string textLine2 = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                string CompleteFile = Path.Combine(model.FilePath, model.FileName);
                string NewFile = Path.Combine(model.FilePath, "sign_" + model.FileName);
                PDFExtensions.InsertImageToPdf(Server.MapPath(CompleteFile), imageFileLoc, Server.MapPath(NewFile), 4, 4, textLine1, textLine2);

                //Update file name and location in both table
                var result = Helper.ExecuteService("Notice", "UpdateNoticeFile", model) as string;

                if (result == "Updated")
                {
                    TempData["Msg"] = "Resolution Submitted Successfully and Resolution Number is " + model.NoticeNumber;
                }
                else
                {
                    TempData["Msg"] = result.ToString();
                }
            }

            //Show Message
            if (model.Message == "Saved")
            {
                TempData["Msg"] = "Resolution Saved Successfully !";
            }
            else if (model.Message == "Updated")
            {
                TempData["Msg"] = "Resolution Updated Successfully !";
            }
            else if (model.Message == "Submitted")
            {
                TempData["Msg"] = "Resolution Submitted Successfully and Resolution Number is " + model.NoticeNumber;
            }
            else
            {
                TempData["Msg"] = model.Message;
            }

            TempData["CallingMethod"] = "Notice";
            return RedirectToAction("Index");
        }

        public ActionResult GetNoticeDetailById(int NoticeId)
        {
            DiaryModel model = new DiaryModel();
            model.NoticeId = NoticeId;
            model = (DiaryModel)Helper.ExecuteService("Notice", "GetNoticeDetailById", model);
            model.SignFilePath = CurrentSession.UserID.GetSignPath();
            return PartialView("_NoticeDetail", model);
        }

        public ActionResult SearchByType(string PaperType, int AssemId, int SessId)
        {
            if (PaperType == "Councillor Diary")
            {
                MlaDiary model1 = new MlaDiary();
                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);

                //int membercode = Convert.ToInt32(CurrentSession.MemberCode);
                //var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllMappedOfficeByMemberCode", membercode);

                //model1.OfficeList = GetData1;

                model1.FinanCialYearList = GetFinancialYearList();
                model1.DocumentTypelist = DocumentTypelist;
                model1.ActionTypeList = ActionTypelist;
                model1.PaperEntryType = PaperType;
                return PartialView("_SearchByType_MlaDiary", model1);
            }
            else
            {

                DiaryModel Dmdl = new DiaryModel();
                Dmdl.AssemblyID = AssemId;
                Dmdl.SessionID = SessId;
                //CurrentSession.ActionIds = ActionIds;
                Dmdl = (DiaryModel)Helper.ExecuteService("Notice", "GetMemberMinisterList", Dmdl);
                Dmdl.Subject = CurrentSession.SPSubject;
                if (!string.IsNullOrEmpty(CurrentSession.SPMinId))
                {
                    Dmdl.MinistryId = Convert.ToInt16(CurrentSession.SPMinId);
                }
                Dmdl.FromDate = CurrentSession.SPFDate;
                Dmdl.ToDate = CurrentSession.SPTDate;
                Dmdl.DiaryNumber = CurrentSession.SPDNum;
                Dmdl.PaperEntryType = PaperType;
                return PartialView("_SearchByType", Dmdl);
            }
        }
        public FileStreamResult PrintDiaryPdf(int DiaryId)
        {
            string msg = string.Empty;

            MlaDiary model = new MlaDiary();
            mdiaryno mdl1 = new mdiaryno();
            var resDiaryNo = (List<MlaDiary>)Helper.ExecuteService("Diary", "get_CommonDiaryNo", DiaryId);
            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDairyListForPdf", DiaryId);
            MemoryStream output = new MemoryStream();
            string DiariesRefNo = "";
            string documentCC = "";
            if (resDiaryNo.ToList() != null)
            {


                foreach (var item in resDiaryNo.ToList())
                {
                    // for (int i = 0; i < resDiaryNo.ToList().Count; i++)
                    // {
                    DiariesRefNo += item.RecordNumber + "/" + Convert.ToDateTime(item.DiaryDate).Year + " & ";
                    documentCC += item.DocumentTo + " & "; ;
                    // }
                }

            }
            if (model.MlaDiaryList.Count > 0)
            {

                string isCCItemExist = "";
                //  member.ase = Convert.ToInt32(CurrentSession.MemberCode);
                mMlaSignature obj = new mMlaSignature();
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj.MlaCode = CurrentSession.MemberCode;
                //  obj = (mMlaSignature)Helper.ExecuteService("Diary", "GetDiarySignatureData", obj);
                // if (obj != null)
                // {
                //PdfConverter pdfConverter = new PdfConverter();
                //  pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                int crntyear = DateTime.Now.Year;
                EvoPdf.Document document1 = new EvoPdf.Document();
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                //  document1.Templates.AddNewTemplate(
                PdfPage page = document1.Pages.AddNewPage(PdfPageSize.Legal, new Margins(0, 0, 40, 40),
                PdfPageOrientation.Portrait);

                string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";
                foreach (MlaDiary diary in model.MlaDiaryList)
                {

                    string frwDate = "";
                    isCCItemExist = diary.DocumentCC;
                    if (diary.ForwardDate != null)
                    { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                    //if (diary.DiaryLanguage == 1)
                    //{
                    //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                    outXml += @"<style>table { }table, th{border:0px solid white;font-size:21px;}table, td{}</style>";
                    outXml += @"<center><h3>" + "" + "</h3></center>";
                    outXml += @"<table cellpadding='4' style='width:100%;font-size:18px;margin-left: 55px;'>";
                    outXml += @"<thead>";
                    outXml += @"<tr>";
                    // outXml += @"<th><br></th></tr>";
                    //outXml += @"<tr><th style='margin-top:30px'></th>";
                    outXml += @"</tr><thead><tbody><br><br><br><br><br><br><br><br>";
                    outXml += "";

                    outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr><br>";
                    if (DiariesRefNo != "")
                    {
                        string diarysNo = DiariesRefNo.Trim().Substring(0, DiariesRefNo.Length - 2);
                        outXml += @"<tr><td colspan='2' style='text-align:justify;' ><hr style='border-width: 1px; border-color:grey;' /></td></tr><br>";
                        outXml += @"<tr><td colspan='2' style='text-align:left;'><span style='color:blue'>Copy To : </span> " + documentCC.Substring(0, documentCC.Length - 2) + " - Vide Refrence Number : <strong>" + diarysNo + "</strong></td></tr> ";
                        outXml += @"<tr><td colspan='2' style='text-align:right;'><span style='color:blue'>Dated : </span<strong>" + Convert.ToDateTime(diary.DiaryDate).ToString("dd/MM/yyyy") + " <br>" + "</strong></td></tr><br>";
                    }
                    //outXml += @"<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                    //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + " <br><br>" + "</strong></td></tr>";
                    //outXml += @"<tr><td colspan='2' style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2 + ",<br>" + obj.SignaturePin2 + "<br></strong></td></tr>";
                    //outXml += @"<tr><td colspan='2' style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                    //outXml += @"<tr><td style='text-align:left;' >" + obj.Text1 + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate + ":" + frwDate + "<br></td></tr>";

                    //outXml += @"<tr><td colspan='2 style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";

                    //outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + "<br><br>" + "</strong></td></tr>";
                    outXml += "";
                    // }
                    //else if (diary.DiaryLanguage == 2)
                    //{
                    //    outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                    //    outXml += "<center><h3>" + "" + "</h3></center>";
                    //    outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                    //    outXml += "<thead>";
                    //    outXml += "<tr>";
                    //    //outXml += "<th><br></th></tr>";
                    //    //outXml += "<tr><th style='margin-top:30px'></th>";
                    //    outXml += "</tr><thead><tbody><br><br><br><br><br><br>";
                    //    outXml += "";

                    //    //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                    //    outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                    //    //outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                    //    //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";

                    //    //outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                    //    //outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                    //    //outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

                    //    //outXml += "<tr><td colspan='2' style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";

                    //    //outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                    //    outXml += "";
                    //}

                }

                outXml += @"</tbody>";

                outXml += @"</div></div></body></html>";

                string htmlStringToConvert2 = outXml;

                HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

                AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;

                string url = "/mlaDiary/" + "DiaryPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(Server.MapPath("~" + url), model.MlaCode + "_" + model.RecordNumber + ".pdf");

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();
                // }
            }
            // }

            return new FileStreamResult(output, "application/pdf");
        }
        public ActionResult SearchDataByType(string PaperType, string Sub, int MinId, string FromDate, string Todate, string Number)
        {
            DiaryModel Dmdl = new DiaryModel();
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            Dmdl.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            Dmdl.PaperEntryType = PaperType;
            Dmdl.Subject = Sub;
            Dmdl.MinistryId = MinId;
            Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            CurrentSession.SPSubject = Sub;
            CurrentSession.SPMinId = MinId.ToString();
            CurrentSession.SPFDate = FromDate;
            CurrentSession.SPTDate = Todate;
            CurrentSession.SPDNum = Number;
            if (CurrentSession.LanguageCulture == "hi-IN")
            {
                if (FromDate != "")
                {
                    var FDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    Dmdl.FromDate_DateTime = Convert.ToDateTime(FDate);
                }

                if (Todate != "")
                {
                    var TDate = DateTime.ParseExact(Todate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    Dmdl.ToDate_DateTime = Convert.ToDateTime(TDate);
                }
            }
            else
            {
                if (FromDate != "")
                {
                    var FDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dmdl.FromDate_DateTime = Convert.ToDateTime(FDate);
                }
                if (Todate != "")
                {
                    var TDate = DateTime.ParseExact(Todate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dmdl.ToDate_DateTime = Convert.ToDateTime(TDate);
                }
            }

            Dmdl.DiaryNumber = Number;
            model = (OnlineMemberQmodel)Helper.ExecuteService("Notice", "GetSearchDataByType", Dmdl);

            if (PaperType == "Starred Questions")
            {
                return PartialView("StarredQuestions", model);
            }
            else if (PaperType == "Unstarred Questions")
            {
                return PartialView("UnstarredQuestion", model);
            }
            else if (PaperType == "Notices")
            {
                return PartialView("_OnlineNotice", model);
            }
            return null;
        }

        public ActionResult SearchMLADiary_DataByType(string PaperType, string DocType, int ActionType, int PendencySince, string FinancialYear, int Officeid)
        {
            ViewBag.PageSize = 25;
            ViewBag.CurrentPage = 1;
            MlaDiary Dmdl = new MlaDiary();

            // Dmdl.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            Dmdl.PaperEntryType = PaperType;
            Dmdl.DocmentType = DocType;
            Dmdl.ActionCode = ActionType;
            Dmdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            Dmdl.PendencySince = PendencySince;
            Dmdl.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
            Dmdl.FinancialYear = FinancialYear;
            Dmdl.OfficeId = Officeid;
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("in");

            if (Dmdl.DocmentType != "3")
            {
                Dmdl.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDiary_SearchDataByType", Dmdl);
            }

            else if (Dmdl.DocmentType == "3")
            {
                Dmdl.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDiary_SearchData_ForDoctype3", Dmdl);
                //ViewBag.TotalSanctionMoney = Helper.ExecuteService("Diary", "GetMlaDiary_AmountCount_ForDoctype3", Dmdl) as string;
                ViewBag.TotalDiaryAmountCount = Helper.ExecuteService("Diary", "Get_TotalDiaryAmountCount", Dmdl) as string;
                ViewBag.pendingDiaryAmountCount = Helper.ExecuteService("Diary", "pendingDiaryAmountCount", Dmdl) as string;
                ViewBag.forwardActionPendingAmountCount = Helper.ExecuteService("Diary", "Get_forwardActionPendingAmountCount", Dmdl) as string;
                ViewBag.forwardActionDoneAmountCount = Helper.ExecuteService("Diary", "Get_forwardActionDoneAmountCount", Dmdl) as string;

            }
            ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCount", Dmdl) as string;
            if (Dmdl.ActionCode == 0)
            {
                ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCount", Dmdl) as string;
                ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCount", Dmdl) as string;
                ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCount", Dmdl) as string;
            }
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("Out");

            var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
            var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);

            SBL.DomainModel.Models.Office.tDepartmentOfficeMapped tmodel = new DomainModel.Models.Office.tDepartmentOfficeMapped();
            tmodel.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllMappedOfficeByMemberCode", tmodel);
            Dmdl.OfficeList = GetData1;

            Dmdl.DocumentTypelist = DocumentTypelist;
            Dmdl.ActionTypeList = ActionTypelist;
            Dmdl.PaperEntryType = PaperType;
            Dmdl.PendencySince = PendencySince;
            ViewBag.DocType = DocType.ToString();
            Dmdl.FinanCialYearList = GetFinancialYearList();
            ViewBag.ActionType = ActionType.ToString();
            ViewBag.PendencySince = PendencySince;

            return PartialView("_MlaDiaryList", Dmdl);
        }




        public string GetDiaryPrint(string DocType, int ActionType, int PendencySince, string FinancialYear, int Officeid)  //GenerateDiaryPrint
        {
            string msg = string.Empty;
            MlaDiary model = new MlaDiary();
            model.PaperEntryType = "Councillor Diary";
            model.DocmentType = DocType;
            model.ActionCode = ActionType;
            model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            model.PendencySince = PendencySince;
            model.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
            if (Officeid == 0)
            {
                model.OfficeId = Convert.ToInt16(CurrentSession.OfficeId);
            }
            else
            {
                model.OfficeId = Officeid;
            }
            model.FinancialYear = FinancialYear;

            // SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("in");
            mTypeOfDocument md1 = new mTypeOfDocument();
            md1.DocumentTypeId = Convert.ToInt16(DocType);



            if (DocType != "3")
            {
                model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDiary_SearchDataByType", model);
            }

            else
            {
                model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDiary_SearchData_ForDoctype3", model);

            }






            //  model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDiary_SearchDataByType", model);
            //   var documenttypename = Helper.ExecuteService("Diary", "Get_DocumentTypeName", md1.DocumentTypeId);
            md1.DocumentTypeName = (string)Helper.ExecuteService("Diary", "Get_DocumentTypeName", md1.DocumentTypeId);
            var documenttypename = md1.DocumentTypeName;
            MemoryStream output = new MemoryStream();


            int crntyear = DateTime.Now.Year;
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";


            //if (diary.DiaryLanguage == 1)
            // {
            //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
            outXml += @"<style>table {border: solid 1px black; }table, th{border-left:1px solid black;font-size:18px;}table, td{border-top:solid 1px black;border-left:solid 1px black;font-size:18px;}</style>";
            outXml += @"<center><h3>" + "Diary Data" + "</h3></center>";
            outXml += @"<left><h3>Document Type : " + documenttypename + "</h3></left>";
            outXml += @"<table style='width:100%;font-size:18px;margin-left: 55px;'>";
            outXml += @"<thead>";
            outXml += @"<tr>";

            outXml += @"<tr><th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'> DiaryNumber</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>To</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>Subject</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>From</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>Forward On</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>Action On</th>";
            //outXml += "<tr><th style='margin-top:20px'></th>"; 
            outXml += @"</tr>  </thead></tbody><br>";
            //  outXml += @"</tr></tbody><br>";
            outXml += "";
            if (model.MlaDiaryList.Count > 0)
            {
                foreach (MlaDiary diary in model.MlaDiaryList)
                {

                    string frwDate = "";
                    string ActionTknDate = "";
                    // isCCItemExist = diary.DocumentCC;
                    if (diary.ForwardDate != null)
                    { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                    if (diary.ActiontakenDate != null)
                    { frwDate = Convert.ToDateTime(diary.ActiontakenDate).ToString("dd.MM.yyyy"); }
                    //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                    outXml += @"<tr><td style='text-align:center;'>" + diary.RecordNumber + "</td>";
                    outXml += @"<td  style='text-align:center;'>" + diary.DocumentTo + "</td>";
                    outXml += @"<td  style='text-align:center;'>" + diary.Subject + "</td>";
                    outXml += @"<td  style='text-align:center;' >" + diary.RecieveFrom + "</td>";
                    outXml += @"<td  style='text-align:center;' >" + frwDate + "</td>";
                    outXml += @"<td  style='text-align:center;' >" + ActionTknDate + "</td>";

                    outXml += "</tr>";
                    //}
                    //else if (diary.DiaryLanguage == 2)
                    //{
                    //    outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                    //    outXml += "<center><h3>" + "" + "</h3></center>";
                    //    outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                    //    outXml += "<thead>";
                    //    outXml += "<tr>";

                    //    outXml += "</tr><thead><tbody><br>";
                    //    outXml += "";


                    //    outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                    //    outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                    //    outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";

                    //    outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                    //    outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                    //    outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

                    //    outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                    //    outXml += "";
                    //}

                }
            }
            else
            {
                outXml += @"<tr><td style='text-align:center;'> No Data </td>";

                outXml += "</tr>";
            }
            outXml += @"</tbody>";

            outXml += @"</div></div></body></html>";

            string htmlStringToConvert2 = outXml;

            HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

            AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

            byte[] pdfBytes = document1.Save();

            output.Write(pdfBytes, 0, pdfBytes.Length);

            output.Position = 0;

            string url = "/mlaDiary/" + "PrintDiaryData/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), "DiaryList" + ".pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();
            //}
            // }
            // }

            //return new FileStreamResult(output, "application/pdf");
            return path;
        }
        [HttpGet]
        public FileResult GetReport(string parameter)
        {
            string ReportURL = parameter;
            byte[] FileBytes = System.IO.File.ReadAllBytes(ReportURL);
            return File(FileBytes, "application/pdf");
        }


        public string GenerateNoticePdfOfRule62(DiaryModel DM)
        {
            try
            {
                string outXml = "";
                //string outInnerXml = "";
                string LOBName = DM.NoticeId.ToString();
                //string CurrentSecretery = "";
                //DateTime SessionDate = new DateTime();
                string fileName = "";
                string savedPDF = string.Empty;

                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                outXml = @"<html>  ";
                //<body style='font-family:DVOT-Yogesh;margin-right:125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                //outXml += @"<div>                  </div>

                outXml += @"
<head>
<style>
	.tempalte_main_div{
		margin: 0 auto;
		font-family: arial sans-serif;
		font-size: 16px;
	}
	.tempalte_main_div .main_iner{
		padding: 3px;
	}
</style>

</head>
 <body style='font-family:DVOT-Yogesh;margin-right:125px;'>
	<div class='tempalte_main_div'>
		<div class='main_iner'>
			<table>
				<tr>
					<td style='width: 120px; vertical-align: top;'>
						&nbsp;
					</td>
					<td style='padding: 0 10px;'>
						<table>
									<tbody>
										<tr>
											<td colspan='2'>
												<div style='text-align: center;font-size:20px; height: 30px;'>
													CALLING ATTENTION NOTICE</div>
											</td>
										</tr>
									  <tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Diary Number: " + DM.NoticeNumber + @"
												</td>
										</tr>
										<tr>
											<td  style='text-align: right;font-size:20px;'>
												Place: Shimla
												</td>
										</tr>
										
										<tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Date: " + DM.PDfDate + @"
												</td>
										</tr>
								  <tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Time: " + DM.PDfTime + @"<br /><br />
												</td>
										</tr>
										<tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Signature</td>
										</tr>
										<tr>
											<td colspan='2' style='font-size:20px;'>
												From,<br />
												&nbsp;</td>
										</tr>
										<tr>
											<td colspan='2' style='font-size:20px;'>
												&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" + DM.MemberId.GetMemberNameByID() + @" M.L.A</td>
										</tr>
										<tr>
											<td colspan='2'>
												&nbsp;To,<br />
												<div style='padding-left: 30px;font-size:20px;'>
													&nbsp; &nbsp; The Secretary,</div>
												<div style='padding-left: 30px;font-size:20px;'>
													&nbsp; &nbsp; Himachal Pradesh Legislative Assembly</div>
												<div style='padding-left: 30px;font-size:20px;'>
													&nbsp; &nbsp; Shimla-171004.</div>
											</td>
										</tr>
										<tr>
											<td colspan='2'>
												<p style='font-size:20px;'>
													Sir,</p>
											<p style='text-align:justify;font-size:20px;'>
											Under rule 62 of the rules of Procedure and Conduct of Business,
I give notice of my intention to call the attention of the Minister
of...............................on.........................to the following
matter of urgent public importance and to request that the Minister may
make a statement thereon:-</p>
											</td>
										</tr>

										<tr>
											<td colspan='2'>
												&nbsp;</td>
										</tr>
	<tr>
											<td style='text-align: right;font-size:20px;'>
											 <br/> <br/> <br/> <br/> <br/> <br/>  <br/>
											Yours Faithfully,</td>
										</tr>
										<tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>

												<p> " + DM.MemberId.GetMemberNameByID() + @" </p>
												<p>&nbsp;</p>
												</td>
										</tr>
										<tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Member,</td>
										</tr>
										<tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Division No. " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")</td>
										</tr>
<tr>
											<td colspan='2' style='text-align:justify;font-size:20px;'>
 Subject: " + DM.Subject + "  " + DM.Notice + @"</td>
										</tr>
										<tr>
											<td colspan='2' style='font-size:20px;'>
											   <br/> Copy to-</td>
										</tr>

										<tr>
											<td colspan='2'>
												<div style='padding-left: 30px;font-size:20px;'>
													1. The Speaker.</div>
												<div style='padding-left: 30px;font-size:20px;'>
													2. The Minister of........................................................................................</div>
											</td>
										</tr>

									</tbody>
								</table>
					</td>
				</tr>

			</table>
		</div>

	</div>
</body>
</html>
";

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public string GenerateNoticePdfOfRule67(DiaryModel DM)
        {
            try
            {
                string outXml = "";
                //string outInnerXml = "";
                string LOBName = DM.NoticeId.ToString();
                //string CurrentSecretery = "";
                //DateTime SessionDate = new DateTime();
                string fileName = "";
                string savedPDF = string.Empty;

                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                outXml = @"<html>    ";
                //<body style='font-family:DVOT-Yogesh;margin-right:125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                outXml += @"

<head>
<style>
	.tempalte_main_div{
		margin: 0 auto;
		font-family: arial sans-serif;
		font-size: 16px;
	}
	.tempalte_main_div .main_iner{
		padding: 3px;
	}
</style>

</head>
 <body style='font-family:DVOT-Yogesh;margin-right:125px;'>
	<div class='tempalte_main_div'>
		<div class='main_iner'>
			<table>
				<tr>
					<td style='width: 120px; vertical-align: top;'>
						&nbsp;
					</td>
					<td style='padding: 0 10px;'>
						<table>
			<tbody>
<tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Diary Number: " + DM.NoticeNumber + @"
												&nbsp;</td>
										</tr>
				<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Place: Shimla &nbsp; &nbsp; </td>
				</tr>
				
				<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Date: " + DateTime.Now.ToString("dd/MM/yyyy") + @"
						&nbsp;</td>
				</tr>
<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Time: " + DM.PDfTime + @"
						&nbsp;<br /></td>
				</tr>
<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Signature <br />
						&nbsp; </td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: center;font-size:20px;'>
						ADJOURNMENT MOTION</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: center;font-size:20px;'>
						From,<br />
						&nbsp;</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: center;font-size:20px;'>
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Shri " + DM.MemberId.GetMemberNameByID() + @" M.L.A</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: center;font-size:20px;'>
						&nbsp;To,<br />
						<div style='padding-left: 30px;font-size:20px;' >
							&nbsp; &nbsp; The Secretary,</div>
						<div style='padding-left: 30px;font-size:20px;'>
							&nbsp; &nbsp; Himachal Pradesh Legislative Assembly</div>
						<div style='padding-left: 30px;font-size:20px;'>
							&nbsp; &nbsp; Shimla-171004.</div>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<p style='font-size:20px;'>
							Sir,</p>
							<p style='text-align: justify;font-size:20px;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Under rule 67 of the Rules of Procedure and Conduct of Business,
													I give &nbsp;notice of &nbsp;my intention to ask for leave to move a motion for the
													adjournment of the business of the House for the purpose of discussing</div>
													a definite matter of urgent public importance:-</p>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: center;font-size:20px;'>
						(Subject Matter of Motion)</td>
				</tr>

				<tr>
					<td colspan='2'>
						&nbsp;</td>
				</tr>
				<tr>
					<td style='text-align: right;font-size:20px;'>
					<br/>   <br/><br/><br/><br/>    <br/>
					Yours Faithfully,</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						<p>	" + DM.MemberId.GetMemberNameByID() + @" </p>
						<p>&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Member,</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Division No. " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: justify;font-size:20px;'>

					   Subject: " + DM.Subject + "  " + DM.Notice + @"</td>
				</tr>
				<tr>
					<td colspan='2' style='font-size:20px;'>
					   <br/> Copy to-</td>
				</tr>
				<tr>
					<td colspan='2'>
						<div style='padding-left: 30px;font-size:20px;'>
							1. The Speaker.</div>
						<div style='padding-left: 30px;font-size:20px;'>
							2. The Concerned Minister</div>
						<div style='padding-left: 30px;font-size:20px;'>
							3. The Minister of Parliamentary Affairs</div>
					</td>
				</tr>
			</tbody>
		</table>
					</td>
				</tr>

			</table>
		</div>
	</div>
</body>
</html>

";

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);
                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public string GenerateNoticePdfOfRule324(DiaryModel DM)
        {
            try
            {
                string outXml = "";
                //string outInnerXml = "";
                string LOBName = DM.NoticeId.ToString();
                //string CurrentSecretery = "";
                //DateTime SessionDate = new DateTime();
                string fileName = "";
                string savedPDF = string.Empty;

                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                outXml = @"<html>  ";

                outXml += @"
<head>
<style>
	.tempalte_main_div{
		margin: 0 auto;
		font-family: arial sans-serif;
		font-size: 16px;
	}
	.tempalte_main_div .main_iner{
		padding: 3px;
	}
</style>

</head>
<body style='font-family:DVOT-Yogesh;margin-right:125px;'>
	<div class='tempalte_main_div'>
		<div class='main_iner'>
			<table>
				<tr>
					<td style='width: 120px; vertical-align: top;'>
						&nbsp;
					</td>
					<td style='padding: 0 10px;'>
						<table>
			<tbody>
<tr>
					<td colspan='2' style='text-align: center;font-size:20px;'>
						NOTICE UNDER RULE 324</td>
				</tr>
<tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Diary Number: " + DM.NoticeNumber + @"
												&nbsp;</td>
										</tr>
				<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Place: Shimla &nbsp; &nbsp;
						</td>
				</tr>

				<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Date: " + DateTime.Now.ToString("dd/MM/yyyy") + @"
						&nbsp;</td>
				</tr>
<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Time: " + DM.PDfTime + @"
						&nbsp;</td>
				</tr>
<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Signature <br />
						&nbsp; </td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: left;font-size:20px;'>
						From,<br />
						&nbsp;</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: left;font-size:20px;'>
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" + DM.MemberId.GetMemberNameByID() + @" M.L.A</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: left;font-size:20px;'>
						&nbsp;To,<br />
						<div style='padding-left: 30px;font-size:20px;' >
							&nbsp; &nbsp; The Secretary,</div>
						<div style='padding-left: 30px;font-size:20px;'>
							&nbsp; &nbsp; Himachal Pradesh Legislative Assembly</div>
						<div style='padding-left: 30px;font-size:20px;'>
							&nbsp; &nbsp; Shimla-171004.</div>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<p style='font-size:20px;'>
							<p style='text-align: justify;font-size:20px;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; I request the Speaker to permit me under Rule 324 to mention the following matter of urgent public importance in the House on date...........................</p>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='font-size:20px;'>
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; If permitted I will make the statement as under:-</td>
				</tr>

				<tr>
					<td colspan='2'>
						&nbsp;</td>
				</tr>
				<tr>
					<td style='text-align: right;font-size:20px;'>
					 <br/>     <br/> <br/><br/>     <br/> <br/> <br/><br/> <br/> <br/> <br/><br/>
					Yours Faithfully,</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						<p>	" + DM.MemberId.GetMemberNameByID() + @" </p>
						<p>
							&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Member,Legislative Assembly,</td>
				</tr>
				<tr>
					<td colspan='2' style='text-align: right;font-size:20px;'>
						Division No. " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")</td>
				</tr>
	<tr>
					<td colspan='2' style='text-align: justify;font-size:20px;'>

					   Subject : " + DM.Subject + " " + DM.Notice + @"</td>
				</tr>
			</tbody>
		</table>
					</td>
				</tr>

			</table>
		</div>
	</div>
</body>
</html>

";

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //        public string GenerateNoticePdfOfRule117_130(DiaryModel DM)
        //        {
        //            try
        //            {
        //                string outXml = "";
        //                //string outInnerXml = "";
        //                string LOBName = DM.NoticeId.ToString();
        //                //string CurrentSecretery = "";
        //                //DateTime SessionDate = new DateTime();
        //                string fileName = "";
        //                string savedPDF = string.Empty;

        //                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
        //                outXml = @"<html>
        //								 <body style='font-family:DVOT-Yogesh;margin-right:125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
        //                outXml += @"<div>
        //
        //					</div>
        //
        //<html>
        //<head>
        //<style>
        //	.tempalte_main_div{
        //		margin: 0 auto;
        //		font-family: arial sans-serif;
        //		font-size: 16px;
        //	}
        //	.tempalte_main_div .main_iner{
        //		padding: 3px;
        //	}
        //</style>
        //
        //</head>
        //<body>
        //	<div class='tempalte_main_div'>
        //		<div class='main_iner'>
        //			<table>
        //				<tr>
        //					<td style='width: 120px; vertical-align: top;'>
        //						&nbsp;
        //					</td>
        //					<td style='padding: 0 10px;'>
        //						<table>
        //			<tbody>
        //				<tr>
        //					<td colspan='2' style='text-align: right;font-size:20px;'>
        //						Place: Shimla &nbsp; &nbsp; &nbsp;<br />
        //						&nbsp;</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2' style='text-align: right;font-size:20px;'>
        //						Date: " + DateTime.Now.ToString("dd/MM/yyyy") + @"<br />
        //						&nbsp;</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2' style='text-align: center;font-size:20px;'>
        //						ADJOURNMENT MOTION</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2' style='text-align: center;font-size:20px;'>
        //						From,<br />
        //						&nbsp;</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2' style='text-align: center;font-size:20px;'>
        //						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Shri " + DM.MemberId.GetMemberNameByID() + @" M.L.A</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2' style='text-align: center;font-size:20px;'>
        //						&nbsp;To,<br />
        //						<div style='padding-left: 30px;font-size:20px;' >
        //							&nbsp; &nbsp; The Secretary,</div>
        //						<div style='padding-left: 30px;font-size:20px;'>
        //							&nbsp; &nbsp; Himachal Pradesh Legislative Assembly</div>
        //						<div style='padding-left: 30px;font-size:20px;'>
        //							&nbsp; &nbsp; Shimla-171004.</div>
        //					</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2'>
        //						<p>
        //							Sir,</p>
        //							<p style='text-align: justify;font-size:20px;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Under rule 67 of the Rules of Procedure and Conduct of Business,
        //													I give &nbsp;notice of &nbsp;my intention to ask for leave to move a motion for the
        //													adjournment of the business of the House for the purpose of discussing</div>
        //													a definite matter of urgent public importance:-</p>
        //					</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2' style='text-align: center;font-size:20px;'>
        //						(Subject Matter of Motion)</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2' style='text-align: justify;font-size:20px;'>
        //						<br />
        //						" + DM.Notice + @"</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2'>
        //						&nbsp;</td>
        //				</tr>
        //				<tr>
        //					<td style='text-align: right;font-size:20px;'>
        //						Yours Faithfully,</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2' style='text-align: right;font-size:20px;'>
        //						<p>	" + DM.MemberId.GetMemberNameByID() + @" </p>
        //						<p>
        //							&nbsp;</p>
        //					</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2' style='text-align: right;font-size:20px;'>
        //						Member,</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2' style='text-align: right;font-size:20px;'>
        //						Division No. " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2' style='font-size:20px;'>
        //						Copy to-</td>
        //				</tr>
        //				<tr>
        //					<td colspan='2'>
        //						<div style='padding-left: 30px;font-size:20px;'>
        //							1. The Speaker.</div>
        //						<div style='padding-left: 30px;font-size:20px;'>
        //							2. The Concerned Minister</div>
        //						<div style='padding-left: 30px;font-size:20px;'>
        //							3. The Minister of Parliamentary Affairs</div>
        //					</td>
        //				</tr>
        //			</tbody>
        //		</table>
        //					</td>
        //				</tr>
        //
        //			</table>
        //		</div>
        //	</div>
        //</body>
        //</html>
        //
        //"; outXml += @"
        //
        //					 </body> </html>";

        //                MemoryStream output = new MemoryStream();

        //                EvoPdf.Document document1 = new EvoPdf.Document();

        //                // set the license key
        //                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
        //                document1.CompressionLevel = PdfCompressionLevel.Best;
        //                document1.Margins = new Margins(0, 0, 0, 0);
        //                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

        //                AddElementResult addResult;

        //                HtmlToPdfElement htmlToPdfElement;
        //                string htmlStringToConvert = outXml;
        //                string baseURL = "";
        //                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

        //                addResult = page.AddElement(htmlToPdfElement);
        //                byte[] pdfBytes = document1.Save();

        //                try
        //                {
        //                    output.Write(pdfBytes, 0, pdfBytes.Length);
        //                    output.Position = 0;
        //                }
        //                finally
        //                {
        //                    // close the PDF document to release the resources
        //                    document1.Close();
        //                }
        //                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
        //                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

        //                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

        //                string directory = Server.MapPath(url);
        //                if (!Directory.Exists(directory))
        //                {
        //                    Directory.CreateDirectory(directory);
        //                }

        //                var path = Path.Combine(Server.MapPath("~" + url), fileName);
        //                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
        //                _FileStream.Close();

        //                return url + "," + fileName;
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //            finally
        //            {
        //            }
        //        }

        [HttpGet]
        public ActionResult LoadNavigationMenu()
        {
            tMemberNotice model = new tMemberNotice();

            if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
            {
                model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            }

            model.UserID = new Guid(CurrentSession.UserID);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDynamicMainMenuByUserId", model);

            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyID.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionID.ToString();
            }

            if (Convert.ToString(TempData["Msg"]) != "")
            {
                model.Message = TempData["Msg"].ToString();
            }
            else
            {
                model.Message = "";
            }

            if ((string)TempData["CallingMethod"] == "Starred")
            {
                model.Msg = "Starred";
            }
            else if ((string)TempData["CallingMethod"] == "Unstarred")
            {
                model.Msg = "Unstarred";
            }
            else if ((string)TempData["CallingMethod"] == "Notice")
            {
                model.Msg = "Notice";
            }
            else
            {
                model.Msg = "";
            }

            ViewBag.MenuId = CurrentSession.MenuId;

            return PartialView("_LeftNavigationMenu", model);
        }

        public ActionResult GetSubMenu(int ModuleId, int MemberId, string ActIds, string Assigned_SubAccess)
        {
            mUserSubModules SM = new mUserSubModules();
            List<mUserSubModules> lstmUserSubModules = new List<mUserSubModules>();
            SM.ModuleId = ModuleId;
            SM = (mUserSubModules)Helper.ExecuteService("Notice", "GetSubMenuByMenuId", SM);
            SM.MemberId = MemberId;
            SM.ActionIds = ActIds;
            //////////////////////////////////////This code For Manage SubMenu By User//////////////////////////////////////
            if (SM != null && SM.AccessList.Count > 0)
            {
                if (!string.IsNullOrEmpty(Assigned_SubAccess))
                {
                    string[] SubAccessArry = Assigned_SubAccess.Split(',');
                    foreach (var item in SM.AccessList)
                    {
                        if (item != null)
                        {
                            if (SubAccessArry.Contains(item.SubModuleId.ToString()))
                            {
                                lstmUserSubModules.Add(item);
                            }
                        }
                    }
                    if (lstmUserSubModules.Count > 0)
                    {
                        SM.AccessList = lstmUserSubModules;
                    }
                }
            }
            //////////////////////////////////////End This code For Manage SubMenu By User////////////////////////////////////

            return PartialView("_SubMenu", SM);
        }

        public JsonResult GetSessionByAssemblyId(int AssemblyId)
        {
            mSession mdl = new mSession();
            List<mSession> SessLst = new List<mSession>();
            if (AssemblyId != 0)
            {
                mdl.AssemblyID = AssemblyId;
                SessLst = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", mdl);
            }
            return Json(SessLst, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateByJsonStarred(DiaryModel model)
        {
            model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model = Helper.ExecuteService("Notice", "SaveUpdateQuestionsTable", model) as DiaryModel;

            // Generate Pdf and Save in File System And Update in Table
            if (model.BtnCaption == "Submit")
            {
                var Location = GeneratePdf(model);
                model.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                model.FileName = Location.Substring(Location.IndexOf(",") + 1);

                //Update file name and location in both table
                var result = Helper.ExecuteService("Notice", "UpdateFile", model) as string;

                if (result == "Updated")
                {
                    TempData["Msg"] = "Starred Question Submitted Successfully and Diary Number is " + model.DiaryNumber;
                }
                else
                {
                    TempData["Msg"] = result.ToString();
                }
            }

            //Show Message
            if (model.Message == "Saved")
            {
                TempData["Msg"] = "Starred Question Saved Successfully !";
            }
            else if (model.Message == "Updated")
            {
                TempData["Msg"] = "Starred Question Updated Successfully !";
            }
            else if (model.Message == "Submitted")
            {
                TempData["Msg"] = "Starred Question Submitted Successfully and Diary Number is " + model.DiaryNumber;
            }
            else
            {
                TempData["Msg"] = model.Message;
            }
            return Json(TempData["Msg"].ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateByJsonUnstarred(DiaryModel model)
        {
            model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model = Helper.ExecuteService("Notice", "SaveUpdateQuestionsUnstarredTable", model) as DiaryModel;

            // Generate Pdf and Save in File System And Update in Table
            if (model.BtnCaption == "Submit")
            {
                var Location = GenerateUnstarredPdf(model);
                model.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                model.FileName = Location.Substring(Location.IndexOf(",") + 1);

                //Update file name and location in both table
                var result = Helper.ExecuteService("Notice", "UpdateFile", model) as string;

                if (result == "Updated")
                {
                    TempData["Msg"] = "Unstarred Question Submitted Successfully and Diary Number is " + model.DiaryNumber;
                }
                else
                {
                    TempData["Msg"] = result.ToString();
                }
            }

            //Show Message
            if (model.Message == "Saved")
            {
                TempData["Msg"] = "Unstarred Question Saved Successfully !";
            }
            else if (model.Message == "Updated")
            {
                TempData["Msg"] = "Unstarred Question Updated Successfully !";
            }
            else if (model.Message == "Submitted")
            {
                TempData["Msg"] = "Unstarred Question Submitted Successfully and Diary Number is " + model.DiaryNumber;
            }
            else
            {
                TempData["Msg"] = model.Message;
            }
            return Json(TempData["Msg"].ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateByJsonNotice(HttpPostedFileBase file, DiaryModel model)
        {

            //var subject1 = coll["Subject2"];
            //var subject2 = coll["Subject3"];

            //if (subject1 != "" && subject1 != null)
            //{
            //    model.Subject = model.Subject + "ḝ" + subject1;

            //}
            //if (subject2 != "" && subject2 != null)
            //{
            //    model.Subject = model.Subject + "ḝ" + subject2;
            //}
            model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);

            if (file != null)
            {
                string url = "/OnlineMemberFile/" + model.AssemblyID + "/" + model.SessionID + "/" + "AttachDocByMember/";
                model.FileName = file.FileName;
                model.FilePath = url;

                string directory = Server.MapPath("~" + url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                file.SaveAs(Server.MapPath("~" + url + file.FileName));
            }
            model = Helper.ExecuteService("Notice", "SaveUpdateNotice", model) as DiaryModel;

            // Generate Pdf and Save in File System And Update in Table
            if (model.BtnCaption == "Submit")
            {
                var Location = "";
                if (model.EventId == 42)//Rule 62
                {
                    Location = GenerateNoticePdfOfRule62(model);
                }
                else if (model.EventId == 37)//Rule 67
                {
                    Location = GenerateNoticePdfOfRule67(model);
                }
                else if (model.EventId == 39)//Rule 324
                {
                    Location = GenerateNoticePdfOfRule324(model);
                }
                //else if (model.EventId == 43 || model.EventId == 44)//Rule 117 and 130
                //{
                //    Location = GenerateNoticePdfOfRule117_130(model);
                //}
                model.FilePath = Location.Substring(0, (Location.IndexOf(",")));
                model.FileName = Location.Substring(Location.IndexOf(",") + 1);

                //Update file name and location in both table
                var result = Helper.ExecuteService("Notice", "UpdateNoticeFile", model) as string;

                if (result == "Updated")
                {
                    TempData["Msg"] = "Notice Submitted Successfully and Notice Number is " + model.NoticeNumber;
                }
                else
                {
                    TempData["Msg"] = result.ToString();
                }
            }

            //Show Message
            if (model.Message == "Saved")
            {
                TempData["Msg"] = "Notice Saved Successfully !";
            }
            else if (model.Message == "Updated")
            {
                TempData["Msg"] = "Notice Updated Successfully !";
            }
            else if (model.Message == "Submitted")
            {
                TempData["Msg"] = "Notice Submitted Successfully and Notice Number is " + model.NoticeNumber;
            }
            else
            {
                TempData["Msg"] = model.Message;
            }

            return Json(TempData["Msg"].ToString(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SignPopup(string SignType, int Id)
        {
            ViewBag.SignType = SignType;
            ViewBag.Id = Id;

            return PartialView("_SignPopup");
        }

        [HttpPost]
        public JsonResult SaveSignFile(HttpPostedFileBase file)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.UserId = new Guid(CurrentSession.UserID);
            //string Url = "/SignaturePath/";
            //DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~" + Url));
            //if (!Dir.Exists)
            //{
            //    Dir.Create();
            //}
            //file.SaveAs(Server.MapPath("~" + Url + file.FileName));

            //Dmdl.SignFilePath = Path.Combine(Url, file.FileName);

            /////
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            string urlsign = "/SignaturePath/";
            string directorysign = FileSettings.SettingValue + urlsign;

            if (!System.IO.Directory.Exists(directorysign))
            {
                System.IO.Directory.CreateDirectory(directorysign);
            }

            string Url = "/SignaturePath/";
            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~" + Url));
            if (!Dir.Exists)
            {
                Dir.Create();
            }
            var signaturelocalpath = Dir.FullName + file.FileName;
            file.SaveAs(Server.MapPath("~" + Url + file.FileName));

            Dmdl.SignFilePath = Path.Combine(Url, file.FileName);

            if (Directory.Exists(directorysign) && (Dmdl.SignFilePath != null || Dmdl.SignFilePath != ""))
            {
                //Convert.ToDateTime(model.ForwardDateForUse);
                // string[] savedFileNamet = Directory.GetFiles(Server.MapPath(Url));
                // string savedFileName = Directory.GetFiles(Dmdl.SignFilePath);
                // if (savedFileNamet.Length > 0)
                //{
                //    string SourceFile = savedFileNamet[0];
#pragma warning disable CS0219 // The variable 'SourceFile' is assigned but its value is never used
                string SourceFile = "";
#pragma warning restore CS0219 // The variable 'SourceFile' is assigned but its value is never used
                //  foreach (string page in savedFileNamet)
                // {
                //   string name = Path.GetFileName(page);
                ////   if (name == file.FileName)
                //  {
                //    SourceFile = Path.GetFullPath(page);

                //    break;

                //  }

                //string path = System.IO.Path.Combine(directorysign, name.Replace(" ", ""));
                string path1 = System.IO.Path.Combine(directorysign, file.FileName);

                //        string path = System.IO.Path.Combine(directoryf,model.RecordNumber.ToString());
                //        //if (!string.IsNullOrEmpty(model.RecordNumber.ToString()))
                //        //{
                //        //    //  System.IO.File.Delete(System.IO.Path.Combine(directoryf, model.ForwardedFile));
                //       // System.IO.File.Copy(SourceFile, path, true);
                //System.IO.File.Copy(SourceFile, path, true);
                // System.IO.File.Copy(SourceFile, path1, true);
                System.IO.File.Copy(signaturelocalpath, path1, true);
                // }
            }


            //        //}

            //   // }


            //}
            //else
            //{

            //}
            //  }




            //save update signature in database
            Dmdl.Message = Helper.ExecuteService("Notice", "SaveUpdateSignature", Dmdl) as string;

            if (Dmdl.Message == "Success")
            {
                return Json(Dmdl.SignFilePath, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(Dmdl.Message, JsonRequestBehavior.AllowGet);
            }
        }

        ////modified by Rahul

        #region member tours

        public ActionResult UnPublishedTourList()
        {
            string PageNumber = "1";
            string RowsPerPage = "10";
            string loopStart = "1";
            string loopEnd = "5";

            //string Month = "";
            //string Day = "";
            //string Year = "";
            //PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            //RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            //loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            //loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);
            model.QuestionTypeId = 2;
            model.DepartmentId = CurrentSession.DeptID;
            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;
            model.MemberId = Convert.ToInt32(CurrentSession.MemberCode);
            model.tMemberTourList = (List<tMemberTour>)Helper.ExecuteService("ATour", "GetUnpublishedMemberTour", new tMemberTour { MemberId = Convert.ToInt32(model.MemberId) });
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            RecipientGroup recipientGroup = new RecipientGroup();
            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            model.RecipientGroupList = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllGroupDetails", recipientGroup);
            //TempData["UpcomingTourCount"] = model.tMemberTourList.Count;
            if (PageNumber != null && PageNumber != "")
            {
                model.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                model.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                model.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                model.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                model.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                model.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                model.loopEnd = Convert.ToInt32("5");
            }
            ViewBag.PaperListTypeCode = "U";
            return PartialView("_AllMemberTourList", model);
        }

        public ActionResult AllMemberTourList()
        {
            string PageNumber = "1";
            string RowsPerPage = "10";
            string loopStart = "1";
            string loopEnd = "5";

            //string Month = "";
            //string Day = "";
            //string Year = "";
            //PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            //RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            //loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            //loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);
            model.QuestionTypeId = 2;
            model.DepartmentId = CurrentSession.DeptID;
            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;
            model.MemberId = Convert.ToInt32(CurrentSession.MemberCode);
            model.tMemberTourList = (List<tMemberTour>)Helper.ExecuteService("ATour", "GetAllMemberTour", new tMemberTour { MemberId = Convert.ToInt32(model.MemberId) });
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            RecipientGroup recipientGroup = new RecipientGroup();
            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            model.RecipientGroupList = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllGroupDetails", recipientGroup);
            TempData["AllTourCount"] = model.tMemberTourList.Count;
            if (PageNumber != null && PageNumber != "")
            {
                model.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                model.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                model.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                model.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                model.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                model.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                model.loopEnd = Convert.ToInt32("5");
            }

            return PartialView("_AllMemberTourList", model);
        }

        public ActionResult TodayMemberTourList()
        {
            //string Month = "";
            //string Day = "";
            //string Year = "";
            //PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            //RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            //loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            //loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
            string PageNumber = "1";
            string RowsPerPage = "10";
            string loopStart = "1";
            string loopEnd = "5";
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);
            model.QuestionTypeId = 2;
            model.DepartmentId = CurrentSession.DeptID;
            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;
            //model.MemberId = Convert.ToInt32(CurrentSession.UserName);
            model.MemberId = Convert.ToInt32(CurrentSession.MemberCode);
            model.tMemberTourList = (List<tMemberTour>)Helper.ExecuteService("ATour", "GetTodayMemberTour", new tMemberTour { MemberId = Convert.ToInt32(model.MemberId) });
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            RecipientGroup recipientGroup = new RecipientGroup();
            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            model.RecipientGroupList = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllGroupDetails", recipientGroup);
            //TempData["TodayTourCount"] = model.tMemberTourList.Count;
            if (PageNumber != null && PageNumber != "")
            {
                model.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                model.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                model.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                model.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                model.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                model.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                model.loopEnd = Convert.ToInt32("5");
            }
            ViewBag.PaperListTypeCode = "T";

            return PartialView("_AllMemberTourList", model);
        }

        public ActionResult PreviousMemberTourList()
        {
            string PageNumber = "1";
            string RowsPerPage = "10";
            string loopStart = "1";
            string loopEnd = "5";

            //string Month = "";
            //string Day = "";
            //string Year = "";
            //PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            //RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            //loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            //loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);
            model.QuestionTypeId = 2;
            model.DepartmentId = CurrentSession.DeptID;
            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;
            model.MemberId = Convert.ToInt32(CurrentSession.MemberCode);
            model.tMemberTourList = (List<tMemberTour>)Helper.ExecuteService("ATour", "GetPreviousMemberTour", new tMemberTour { MemberId = Convert.ToInt32(model.MemberId) });
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            RecipientGroup recipientGroup = new RecipientGroup();
            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            model.RecipientGroupList = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllGroupDetails", recipientGroup);
            //TempData["AllTourCount"] = model.tMemberTourList.Count;
            if (PageNumber != null && PageNumber != "")
            {
                model.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                model.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                model.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                model.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                model.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                model.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                model.loopEnd = Convert.ToInt32("5");
            }
            ViewBag.PaperListTypeCode = "P";
            return PartialView("_AllMemberTourList", model);
        }

        public ActionResult UpcomingMemberTourList()
        {
            string PageNumber = "1";
            string RowsPerPage = "10";
            string loopStart = "1";
            string loopEnd = "5";

            //string Month = "";
            //string Day = "";
            //string Year = "";
            //PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            //RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            //loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            //loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);
            model.QuestionTypeId = 2;
            model.DepartmentId = CurrentSession.DeptID;
            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;
            model.MemberId = Convert.ToInt32(CurrentSession.MemberCode);
            model.tMemberTourList = (List<tMemberTour>)Helper.ExecuteService("ATour", "GetUpcomingMemberTour", new tMemberTour { MemberId = Convert.ToInt32(model.MemberId) });
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            RecipientGroup recipientGroup = new RecipientGroup();
            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            model.RecipientGroupList = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllGroupDetails", recipientGroup);
            //TempData["UpcomingTourCount"] = model.tMemberTourList.Count;
            if (PageNumber != null && PageNumber != "")
            {
                model.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                model.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                model.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                model.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                model.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                model.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                model.loopEnd = Convert.ToInt32("5");
            }
            ViewBag.PaperListTypeCode = "C";
            return PartialView("_AllMemberTourList", model);
        }

        public ActionResult EntryFormTour()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            TourViewModel model = new TourViewModel();

            model.EditMode = "Add";
            ViewBag.MM = GetMM();

            return PartialView("_EntryFormTour", model);
        }

        public JsonResult SendPubishedTourSMS(string selectedTours, string MobileNos, int SMSLeft, bool msgType)
        {
            string msg = string.Empty;
            int counttour = 0;
            int smscount = 0;

            if (selectedTours != null && selectedTours.Length > 0)
            {
                TourViewModel model = new TourViewModel();

                string[] tblMemberTourListRows = selectedTours.Split(new string[] { "$%##**#" },
                        StringSplitOptions.RemoveEmptyEntries);

                if (tblMemberTourListRows != null && tblMemberTourListRows.Length > 0)
                {
                    foreach (string tblMemberTourRow in tblMemberTourListRows)
                    {
                        string[] tblMemberTourListCols = tblMemberTourRow.Split(new string[] { "$#**&##" },
                        StringSplitOptions.None);

                        if (tblMemberTourListCols != null && tblMemberTourListCols.Length > 0)
                        {
                            model.TourListCol.Add(new tMemberTour()
                            {
                                TourId = Convert.ToInt32(tblMemberTourListCols[0]),
                                Departure = tblMemberTourListCols[1].Substring(0, 10),
                                Arrival = tblMemberTourListCols[2].Substring(0, 10),
                                mode = tblMemberTourListCols[3],
                                Purpose = tblMemberTourListCols[4],
                                TourDescrption = tblMemberTourListCols[5],
                                DeparturePlace = tblMemberTourListCols[7],
                                DepartureTime = tblMemberTourListCols[8],
                                ArrivalPlace = tblMemberTourListCols[9],
                                ArrivalTime = tblMemberTourListCols[10],
                                isLocale = Convert.ToBoolean(tblMemberTourListCols[11]),//for locale
                            });
                            counttour++;
                        }
                    }
                }

                //if (counttour > 3)
                //{
                //	msg = "Only 3 Tour allowed.";
                //	return Json(msg, JsonRequestBehavior.AllowGet);
                //}

                if (model.TourListCol.Count > 0)
                {
                    mMember member = new mMember();

                    member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

                    member = (mMember)Helper.ExecuteService("Member", "GetMemberDetailsById", member);

                    string moduleName = ConfigurationManager.AppSettings["TourModuleName"];

                    string moduleActionName = ConfigurationManager.AppSettings["TourModuleActionName"];

                    SendNotificationsModel sendNotificationModel = new SendNotificationsModel();

                    List<RecipientGroup> contactGroups = new List<RecipientGroup>();

                    SystemModuleModel systemMoule = new SystemModuleModel();

                    systemMoule.Name = moduleName;

                    var moduleObj = Helper.ExecuteService("SystemModule", "GetSystemModuleByName", systemMoule) as SystemModuleModel;

                    SystemModuleModel actionObj = new SystemModuleModel();

                    SystemModuleModel moduleAction = new SystemModuleModel();

                    if (moduleObj != null)
                    {
                        moduleAction.ActionName = moduleActionName;

                        actionObj = Helper.ExecuteService("SystemModule", "GetModuleActionByName", moduleAction) as SystemModuleModel;
                    }

                    if (actionObj != null)
                    {
                        List<string> phoneNumbers = new List<string>();

                        //RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

                        //recipientGroupMember.GroupID = recipientGroupID ?? 0;

                        //var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", recipientGroupMember);

                        //if (recipientGroupMemberCol.Count > 0)
                        //{
                        string[] mobnos = MobileNos.Split(',');

                        for (int i = 0; i < mobnos.Length; i++)
                        {
                            if (!string.IsNullOrEmpty(mobnos[i]))
                            {
                                phoneNumbers.Add(mobnos[i].Trim());
                                smscount++;
                            }
                        }

                        if (smscount > SMSLeft)
                        {
                            msg = "You Have Only <span class='badge badge-warning'>" + SMSLeft + "</span> Left and You are Sending SMS to <span class='badge badge-warning'>" + smscount + "</span> Receipient. Please Minimize the SMS Receipient List";
                            return Json(msg, JsonRequestBehavior.AllowGet);
                        }
                        string SMSTemplate = string.Empty;
                        if (CurrentSession.MemberDesignation == "Member Legislative Assembly")
                        {
                            SMSTemplate += "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + " " + CurrentSession.ConstituencyName + ", HP :" + Environment.NewLine;
                        }
                        else
                        {
                            SMSTemplate += "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", HPVS :" + Environment.NewLine;
                        }
                        foreach (tMemberTour tour in model.TourListCol)
                        {
                            if (phoneNumbers.Count > 0)
                            {

                                SMSTemplate += Environment.NewLine;
                                string tourStartDate = tour.Departure.Substring(0, 10);
                                string tourEndDate = tour.Arrival.Substring(0, 10);
                                string TourDates = string.Empty;
                                DateTime dt1 = DateTime.ParseExact(tourStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                DateTime dt2 = DateTime.ParseExact(tourEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                if (dt1 == dt2)
                                {
                                    TourDates = tourStartDate;
                                }
                                else
                                {
                                    TourDates = tourStartDate + " to " + tourEndDate;
                                }

                                DateTime depdate = DateTime.ParseExact(tour.Departure, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                DateTime sdt = DateTime.ParseExact("23/10/2014", "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                DateTime tdt = DateTime.ParseExact("08/11/2014", "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                if ((string.IsNullOrEmpty(tour.DeparturePlace) || string.IsNullOrEmpty(tour.ArrivalPlace)) || (depdate >= sdt && depdate <= tdt))
                                {
                                    //SMSTemplate += tour.Departure + " to " + tour.Arrival + " " + tour.TourDescrption + "";
                                    SMSTemplate += "" + TourDates + " " + Environment.NewLine + "" + tour.TourDescrption + @"";
                                }
                                else
                                {
                                    string dTime = string.Empty;
                                    string ATime = string.Empty;
                                    if (string.IsNullOrEmpty(tour.DepartureTime) || tour.DepartureTime == "00:00 AM")
                                    { dTime = ""; }
                                    else { dTime = "(" + tour.DepartureTime + ")"; }

                                    if (string.IsNullOrEmpty(tour.ArrivalTime) || tour.ArrivalTime == "00:00 AM")
                                    { ATime = ""; }
                                    else { ATime = "(" + tour.ArrivalTime + ")"; }

                                    SMSTemplate += TourDates + " : Departure From  " + tour.DeparturePlace + " " + dTime + " and Arrival at " + tour.ArrivalPlace + " " + ATime + " " + Environment.NewLine + "" + tour.TourDescrption + @"";
                                }
                                SMSTemplate += Environment.NewLine;
                            }
                            else
                            {
                                msg = "No phone numbers found";
                            }
                        }

                        //foreach (tMemberTour tour in model.TourListCol)
                        //{
                        if (phoneNumbers.Count > 0)
                        {
                            string SMSTemplateText = actionObj.SMSTemplate;

                            //SMSTemplateText = SMSTemplateText.Replace("@Model.MemberName", member.Name);

                            //SMSTemplateText = SMSTemplateText.Replace("@Model.Departure", tour.Departure);

                            //SMSTemplateText = SMSTemplateText.Replace("@Model.Arrival", tour.Arrival);

                            //SMSTemplateText = SMSTemplateText.Replace("@Model.TourDescription", tour.TourDescrption);

                            //"Tour Programmer of " + member.Name + ", Hon'ble Speaker (HP) : " + tour.Departure + " Departure From : " + tour.DeparturePlace + " (" + tour.DepartureTime + ") and Arrival at " + tour.ArrivalPlace + " (" + tour.ArrivalTime + ")" + tour.TourDescrption + "<br/>-Speaker HP Vidhan Sabha";
                            SMSTemplate += Environment.NewLine;
                            if (CurrentSession.IsMember.ToUpper() == "FALSE" && CurrentSession.SubUserTypeID == "22")// For User under working of Member
                            {
                                SMSTemplate += "" + CurrentSession.Designation + " to Hon'ble " + CurrentSession.MemberDesignation + ", HP Vidhan Sabha";
                            }
                            else
                            {
                                if (CurrentSession.MemberDesignation == "Speaker" || CurrentSession.MemberDesignation == "Deputy Speaker")
                                {
                                    SMSTemplate += "" + CurrentSession.MemberDesignation + ", HP Vidhan Sabha";
                                }
                                else
                                {
                                    SMSTemplate += "" + CurrentSession.MemberDesignation + ", HP";
                                }
                            }

                            SMSTemplate += Environment.NewLine;
                            SMSTemplate += "Also available at Website http://evidhan.nic.in";
                            //SMSTemplate += CurrentSession.Name + " (" + CurrentSession.MemberDesignation + ") HP Vidhan Sabha";

                            sendNotificationModel.SMSTemplate = SMSTemplate; //SMSTemplateText.Replace("\t", "").Replace("\n", "");

                            sendNotificationModel.SendSMS = actionObj.SendSMS;

                            //Messgae List
                            SMSMessageList smsMessage = new SMSMessageList();
                            smsMessage.SMSText = sendNotificationModel.SMSTemplate;

                            smsMessage.isLocale = msgType;

                            smsMessage.ModuleActionID = 1;

                            smsMessage.UniqueIdentificationID = Convert.ToInt64(CurrentSession.AadharId);

                            foreach (var item in phoneNumbers)
                            {
                                var ismob = ValidationAtServer.CheckMobileNumber(item.Trim());
                                if (Convert.ToBoolean(ismob) == true)
                                {
                                    smsMessage.MobileNo.Add(item.Trim());
                                }
                            }
                            // End Message list

                            //Notification.Send(sendNotificationModel.SendSMS, sendNotificationModel.SendEmail, smsMessage, null);
                            Notification.Send(sendNotificationModel.SendSMS, false, smsMessage, null);
                        }
                        else
                        {
                            msg = "No phone numbers found";
                        }
                        //}
                        if (smscount > 0)
                        {
                            msg = "Total (" + smscount + ") SMS Sent Successfully";
                        }
                        //}
                        //else{
                        //	msg = "No Recipient Members found in the selected group";
                        //}

                        //if (!string.IsNullOrEmpty(actionObj.AssociatedContactGroups))
                        //{
                        //    RecipientGroupMember groupMembers = new RecipientGroupMember();
                        //    groupMembers.AssociatedContactGroups = actionObj.AssociatedContactGroups;

                        //    //pass the Comma seperated Associated Contact Groups to a Function and get all the group details at a time.
                        //    contactGroups = (List<RecipientGroup>)Helper.ExecuteService("ATour", "GetContactGroupsByGroupIDs", groupMembers);

                        //    sendNotificationModel.selectedRecipientGroups = contactGroups;

                        //    int groupID = 0;

                        //    foreach (var item in sendNotificationModel.selectedRecipientGroups)
                        //    {
                        //        groupID = item.GroupID;
                        //    }

                        //    var contactGroupMembers = Helper.ExecuteService("ContactGroups",
                        //    "GetContactGroupMembersByGroupID",
                        //    new RecipientGroupMember
                        //    {
                        //        GroupID = groupID
                        //    }) as List<RecipientGroupMember>;

                        //    foreach (var item in contactGroupMembers)
                        //    {
                        //        phoneNumbers.Add(item.MobileNo);
                        //    }
                        //}
                    }
                }
            }

            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public FileStreamResult GeneratePublishedTourPDF(OnlineMemberQmodel qModel)
        {
            string msg = string.Empty;

            string selectedTours = qModel.hdnTourList;

            MemoryStream output = new MemoryStream();

            if (selectedTours != null && selectedTours.Length > 0)
            {
                TourViewModel model = new TourViewModel();

                string[] tblMemberTourListRows = selectedTours.Split(new string[] { "$%##**#" },
                        StringSplitOptions.RemoveEmptyEntries);

                if (tblMemberTourListRows != null && tblMemberTourListRows.Length > 0)
                {
                    foreach (string tblMemberTourRow in tblMemberTourListRows)
                    {
                        string[] tblMemberTourListCols = tblMemberTourRow.Split(new string[] { "$#**&##" },
                        StringSplitOptions.None);

                        if (tblMemberTourListCols != null && tblMemberTourListCols.Length > 0)
                        {
                            model.TourListCol.Add(new tMemberTour()
                            {
                                TourId = Convert.ToInt32(tblMemberTourListCols[0]),
                                Departure = tblMemberTourListCols[1],
                                Arrival = tblMemberTourListCols[2],
                                mode = tblMemberTourListCols[3],
                                Purpose = tblMemberTourListCols[4],
                                TourDescrption = tblMemberTourListCols[5],
                            });
                        }
                    }
                }

                if (model.TourListCol.Count > 0)
                {
                    mMember member = new mMember();

                    member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

                    member = (mMember)Helper.ExecuteService("Member", "GetMemberDetailsById", member);

                    EvoPdf.Document document1 = new EvoPdf.Document();
                    document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document1.CompressionLevel = PdfCompressionLevel.Best;
                    document1.Margins = new Margins(0, 0, 0, 0);

                    PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                    PdfPageOrientation.Portrait);

                    string outXml = "<html><body style='font-family:Times new Roman;;font-size-10px;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

                    DateTime Startdate = DateTime.Today;
                    DateTime endate = DateTime.Today;
                    int StartdateCount = 0;
                    foreach (tMemberTour tour in model.TourListCol)
                    {
                        if (StartdateCount == 0)
                        {
                            Startdate = DateTime.ParseExact(tour.Departure.Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            StartdateCount++;
                        }
                        endate = DateTime.ParseExact(tour.Departure.Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    string tourTitle = model.TourListCol.ElementAt(0).TourTitle;
                    string HeadingPdf = "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                    outXml += "<style>table {  border-collapse: collapse;}table, th{border:1px solid black;}table, td{border:1px solid black;}</style>";
                    outXml += @"<center><h3>" + HeadingPdf + "</h3></center>";
                    outXml += @"<table cellpadding='4' style='width:100%;'>";
                    outXml += "<thead>";
                    outXml += "<tr>";
                    outXml += "<th>Date</th>";
                    outXml += "<th>Departure</th>";
                    outXml += "<th>Arrival</th>";
                    outXml += "<th>Mode of Journey</th>";
                    outXml += "<th>Purpose</th>";
                    outXml += "<th>Description</th>";
                    outXml += "</tr><thead><tbody>";

                    foreach (tMemberTour tour in model.TourListCol)
                    {
                        string[] tourDepartureInfo = tour.Departure.Split(new string[] { " " },
                        StringSplitOptions.None);

                        string[] tourArrivalInfo = tour.Arrival.Split(new string[] { " " },
                        StringSplitOptions.None);

                        string tourStartDate = tour.Departure.Substring(0, 10);

                        string tourEndDate = tour.Arrival.Substring(0, 10);

                        tour.Departure = tour.Departure;

                        //tour.DepartureTime = tourDepartureInfo[2] + " " + tourDepartureInfo[3];

                        tour.Arrival = tour.Arrival;

                        //tour.ArrivalTime = tourArrivalInfo[2] + " " + tourArrivalInfo[3];

                        outXml += "<tr>";
                        DateTime dt1 = DateTime.ParseExact(tourStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime dt2 = DateTime.ParseExact(tourEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        if (dt1 == dt2)
                        {
                            outXml += "<td align=\"text-align:left;\" >" + tourStartDate + "</td>";
                        }
                        else
                        {
                            outXml += "<td align=\"text-align:left;\" >" + tourStartDate + " to " + tourEndDate + "</td>";
                        }

                        //outXml += "<td align=\"text-align:left;\">" + tour.Departure + " " + tour.DepartureTime + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + tour.Departure.Substring(10) + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + tour.Arrival.Substring(10) + "</td>";
                        //outXml += "<td align=\"text-align:left;\">" + tour.Arrival + " " + tour.ArrivalTime + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + tour.mode + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + tour.TourDescrption + @"</td>";
                        outXml += "</tr>";
                    }

                    outXml += "</tbody>";

                    outXml += @"</div></div></body></html>";

                    string htmlStringToConvert = outXml;

                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                    AddElementResult addResult = page.AddElement(htmlToPdfElement);

                    byte[] pdfBytes = document1.Save();

                    output.Write(pdfBytes, 0, pdfBytes.Length);

                    output.Position = 0;

                    string url = "/QuestionList/" + "MemberTourPdf/";

                    string directory = Server.MapPath(url);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + tourTitle + ".pdf");

                    FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                    System.IO.FileAccess.Write);

                    _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                    // close file stream
                    _FileStream.Close();
                }
            }

            return new FileStreamResult(output, "application/pdf");
        }

        //public ActionResult HTML(string str)
        //{
        //	return Content(string str,"application/html");
        //}

        public JsonResult PublishTours(string selectedTours)
        {
            tMemberTour tour = new tMemberTour();

            string msg = string.Empty;

            if (selectedTours != null && selectedTours.Length > 0)
            {
                TourViewModel model = new TourViewModel();

                string[] tblMemberTourListRows = selectedTours.Split(new string[] { "$%##**#" },
                        StringSplitOptions.RemoveEmptyEntries);

                if (tblMemberTourListRows != null && tblMemberTourListRows.Length > 0)
                {
                    foreach (string tblMemberTourRow in tblMemberTourListRows)
                    {
                        string[] tblMemberTourListCols = tblMemberTourRow.Split(new string[] { "$#**&##" },
                        StringSplitOptions.RemoveEmptyEntries);

                        if (tblMemberTourListCols != null && tblMemberTourListCols.Length > 0)
                        {
                            model.TourListCol.Add(new tMemberTour()
                            {
                                TourId = Convert.ToInt32(tblMemberTourListCols[0])
                            });
                        }
                    }
                }

                if (model.TourListCol.Count > 0)
                {
                    msg = Helper.ExecuteService("ATour", "Publishtour", model.TourListCol).ToString();

                    tour.MemberId = Convert.ToInt32(CurrentSession.MemberCode);

                    tour = (tMemberTour)Helper.ExecuteService("ATour", "GetAllUpdatedTourCounts", tour);

                    tour.Msg = msg;
                }
            }

            return Json(tour, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SaveMemberTour(HttpPostedFileBase file)
        {
            tMemberTour tour = new tMemberTour();

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                tPaperLaidV sessiondata = new tPaperLaidV();

                var assmebly = (SBL.DomainModel.Models.PaperLaid.tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", sessiondata);

                TourViewModel model = new TourViewModel();

                model.TourId = Convert.ToInt32(Request.Form["TourId"].ToString());

                model.TourFromDateTime = Request.Form["txtStartDate"].ToString();

                model.TourToDateTime = Request.Form["txtEndDate"].ToString();

                model.Departure = Request.Form["txtDeparturePlace"].ToString();

                model.DepartureTime = string.IsNullOrEmpty(Request.Form["txtDepartureTime"].ToString()) ? "00:00 AM" : Request.Form["txtDepartureTime"].ToString();

                model.Arrival = Request.Form["txtArrivalPlace"].ToString();

                model.ArrivalTime = string.IsNullOrEmpty(Request.Form["txtArrivalTime"].ToString()) ? "00:00 AM" : Request.Form["txtArrivalTime"].ToString();

                model.mode = Request.Form["txtModeOfJourney"].ToString();

                model.Purpose = Request.Form["Purpose"].ToString();

                model.TourDescrption = Request.Unvalidated["TourDescrption"].ToString();

                model.TourTitle = string.IsNullOrWhiteSpace(Request.Unvalidated["TourDescrption"].ToString()) ? "title" : Request.Unvalidated["TourDescrption"].ToString();

                model.EditMode = Request.Form["EditMode"].ToString();

                model.Attachement = Request.Form["Attachement"].ToString();

                model.AliasAttachment = Request.Form["AliasAttachement"].ToString();

                model.isLocale = Convert.ToBoolean(Request.Form["chkislocale"].ToString());

                if (model.EditMode == "Add")
                {
                    //string FileName = file.FileName;

                    var data = model.ToDomainModel();

                    data.AssemblyId = assmebly.AssemblyCode;

                    data.CreatedBy = Guid.Parse(CurrentSession.UserID);

                    data.ModifiedBy = Guid.Parse(CurrentSession.UserID);

                    data.MemberId = Convert.ToInt32(CurrentSession.MemberCode);

                    data.MemberName = CurrentSession.Name;

                    string realName = "";

                    string aliasName = "";

                    if (file != null)
                    {
                        UploadFile(file, file.FileName, out realName, out aliasName);

                        data.Attachement = aliasName;

                        data.AliasAttachment = aliasName;
                    }
                    else
                    {
                        data.Attachement = "";

                        data.AliasAttachment = "";
                    }

                    Helper.ExecuteService("ATour", "AddTour", data);

                    tour = (tMemberTour)Helper.ExecuteService("ATour", "GetAllUpdatedTourCounts", data);

                    tour.Msg = "Tour Saved Successfully";
                }
                else
                {
                    var data = model.ToDomainModel();

                    data.CreatedBy = Guid.Parse(CurrentSession.UserID);

                    data.ModifiedBy = Guid.Parse(CurrentSession.UserID);

                    data.MemberId = Convert.ToInt32(CurrentSession.MemberCode);

                    data.MemberName = CurrentSession.Name;

                    string realName = "";

                    string aliasName = "";

                    if (file != null && !string.IsNullOrEmpty(data.AliasAttachment))
                    {
                        RemoveExistingFile(data.AliasAttachment);
                    }
                    if (file != null)
                    {
                        UploadFile(file, file.FileName, out realName, out aliasName);

                        data.Attachement = realName;

                        data.AliasAttachment = aliasName;
                    }

                    Helper.ExecuteService("ATour", "UpdateTour", data);

                    tour = (tMemberTour)Helper.ExecuteService("ATour", "GetAllUpdatedTourCounts", data);

                    tour.Msg = "Tour Updated Successfully";
                }
            }
            catch (Exception ex)
            {
                RecordError(ex, "Save Tour Details");

                ViewBag.ErrorMessage = "There was an Error While Saving Tour Details";

                return View("AdminErrorPage");
            }

            return Json(tour, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddRecipientGroup()
        {
            RecipientGroupViewModel model = new RecipientGroupViewModel();

            model.EditMode = "Add";

            model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);

            return PartialView("_AddRecipientGroup", model);
        }

        [HttpGet]
        public ActionResult AddRecipientGroupMember()
        {
            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.EditMode = "Add";

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            model.RecipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllContactGroupsByMember", data);

            model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);

            model.GenderList.Add("Male");

            model.GenderList.Add("Female");

            return PartialView("_AddRecipientGroupMember", model);
        }

        [HttpPost]
        public ActionResult SaveRecipientGroupMember(int? GroupMemberId, int? GroupId,
        string GroupMemberName, string Gender, string Designation, string DeptCode,
        string Mobile, string Email, string Address, string PinCode, bool IsActive,
        string Mode, int? RankingOrder, string LandLineNo)
        {
            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                model.GroupMemberID = GroupMemberId ?? 0;

                model.GroupId = GroupId ?? 0;

                model.Name = GroupMemberName;

                model.Gender = Gender;

                model.Designation = Designation;

                model.DepartmentID = DeptCode;

                model.Mobile = Mobile;

                model.EMail = Email;

                model.Address = Address;

                model.PinCode = PinCode;

                model.IsActive = true;

                model.EditMode = Mode;

                model.CreatedBy = CurrentSession.UserID;

                model.CreatedDate = DateTime.Now;

                //
                mMember member = new mMember();

                member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

                model.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
                //
                model.RankingOrder = RankingOrder;
                model.LandLineNumber = LandLineNo;
                RecipientGroupMember data = model.ToDomainModel();

                if (Mode == "Add")
                {
                    Helper.ExecuteService("ContactGroups", "CreateNewContactGroupMember", data);
                    model.Message = "Group Member Saved Successfully";
                }
                else
                {
                    Helper.ExecuteService("ContactGroups", "UpdateContactGroupMember", data);
                    model.Message = "Group Member Updated Successfully";
                }

                //model.RecipientGroupMemberCnt = ((DiaryModel)Helper.ExecuteService("Notice", "GetRecipientGroupMemberCnt", Convert.ToInt32(CurrentSession.MemberCode))).ResultCount;
            }
            catch (Exception ex)
            {
                RecordError(ex, "Save Recipient Group Member Details");

                ViewBag.ErrorMessage = "There was an Error While Saving Recipient Group Member Details";

                return View("AdminErrorPage");
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveRecipientGroup(int? GroupId, string GroupName,
        string DeptCode, string GroupDesc, bool IsPublic, bool IsActive, string Mode, int Officecode, int? RankingOrder)
        {
            RecipientGroupViewModel model = new RecipientGroupViewModel();

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                model.GroupId = GroupId ?? 0;

                model.GroupName = GroupName;

                model.DepartmentID = DeptCode;

                model.GroupDesc = GroupDesc;

                model.IsPublic = IsPublic;

                model.IsActive = IsActive;
                model.OfficeId = 0;
                if (DeptCode == "0" && Officecode == 1)
                {
                    model.IsConstituencyOffice = true;
                }
                else
                {
                    model.IsConstituencyOffice = false;
                }
                model.RankingOrder = RankingOrder;
                RecipientGroup data = model.ToDomainModel();

                mMember member = new mMember();

                member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

                data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

                data.MemberCode = member.MemberCode;

                data.CreatedBy = CurrentSession.UserID;

                data.CreatedDate = DateTime.Now;

                if (Mode == "Add")
                {
                    Helper.ExecuteService("ContactGroups", "CreateNewContactGroup", data);
                    model.Message = "Group Saved Successfully";
                }
                else
                {
                    Helper.ExecuteService("ContactGroups", "UpdateContactGroup", data);
                    model.Message = "Group Updated Successfully";
                }

                //model.RecipientGroupCount = ((DiaryModel)Helper.ExecuteService("Notice", "GetRecipientGroupCnt", data.MemberCode)).ResultCount;
            }
            catch (Exception ex)
            {
                RecordError(ex, "Save Recipient Group Details");

                ViewBag.ErrorMessage = "There was an Error While Saving Recipient Group Details";

                return View("AdminErrorPage");
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetRecipientGroupId(int? GroupID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            RecipientGroup data = new RecipientGroup();

            data.GroupID = GroupID ?? 0;

            var recipientGroup = (RecipientGroup)Helper.ExecuteService("ContactGroups", "GetContactGroupByMember", data);

            RecipientGroupViewModel model = recipientGroup.ToViewModel();

            model.EditMode = "Edit";

            model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);

            return PartialView("_AddRecipientGroup", model);
        }

        [HttpGet]
        public ActionResult GetRecipientGroupMemberId(int? GroupMemberID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupMemberID = GroupMemberID ?? 0;

            recipientGroupMember = (RecipientGroupMember)Helper.ExecuteService("ContactGroups", "GetContactGroupMemberByID", recipientGroupMember);

            model = recipientGroupMember.ToViewModel();

            model.RecipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllContactGroupsByMember", recipientGroup);

            model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);

            model.GenderList.Add("Male");

            model.GenderList.Add("Female");

            model.EditMode = "Edit";

            return PartialView("_AddRecipientGroupMember", model);
        }

        [HttpGet]
        public ActionResult AllRecipientGroupList()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllContactGroupsByMember", data);

            RecipientGroupViewModel model = new RecipientGroupViewModel();

            model.RecipientGroupsCol = recipientGroupCol.ToViewModel();

            return PartialView("_AllRecipientGroupList", model);
        }

        [HttpGet]
        public ActionResult AllRecipientGroupMemberList(int? GroupID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = GroupID ?? 0;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllContactGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

            return PartialView("_AllRecipientGroupMemberList", model);
        }

        [HttpGet]
        public ActionResult DeleteRecipientGroupId(int GroupID)
        {
            RecipientGroup data = new RecipientGroup();

            data.GroupID = GroupID;

            Helper.ExecuteService("ContactGroups", "DeleteContactGroup", data);

            ViewBag.DeleteMsg = "Deleted Successfully";

            return Json(ViewBag.DeleteMsg, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult DeleteRecipientGroupMemberId(int GroupMemberID)
        {
            RecipientGroupMember data = new RecipientGroupMember();

            data.GroupMemberID = GroupMemberID;

            Helper.ExecuteService("ContactGroups", "DeleteContactGroupMember", data);

            ViewBag.DeleteMsg = "Deleted Successfully";

            return Json(ViewBag.DeleteMsg, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        private string UploadPhoto(HttpPostedFileBase File, string FileName)
        {
            try
            {
                if (File != null)
                {
                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string extension = System.IO.Path.GetExtension(File.FileName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, FileName);//Server.MapPath("~/Images/News/"), FileName);

                    //if (System.IO.File.Exists(path))
                    //{
                    File.SaveAs(path);
                    //}
                    return (File.FileName);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UploadFile(HttpPostedFileBase File, string FileName, out string realFile, out string aliasFile)
        {
            try
            {
                realFile = "";
                aliasFile = "";

                if (File != null)
                {
                    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    realFile = FileName;
                    string fileAliasName = GetAliasNameForFile(FileName);
                    //string extension = System.IO.Path.GetExtension(fileAliasName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, fileAliasName);
                    File.SaveAs(path);
                    aliasFile = fileAliasName;
                    //return (File.FileName);
                }
                //return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RemoveExistingFile(string fileName)
        {
            try
            {
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, fileName);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RemoveExistingPhoto(string OldPhoto)
        {
            try
            {
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                //var disFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, OldPhoto);//Server.MapPath("~/Images/News/")

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetTourDataByID(int tourid)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            var tMemberTour = (tMemberTour)Helper.ExecuteService("ATour", "GetTourById", new tMemberTour { TourId = tourid });

            var model = tMemberTour.ToViewModel1("Edit");
            string[] st = null;
            string[] stampm = null;
            string[] dt = null;
            string[] dtampm = null;

            if (model.ArrivalTime == "00:00 AM" || string.IsNullOrEmpty(model.ArrivalTime))
            {
                ViewBag.StartTimeHH = " ";
                ViewBag.StartTimeMM = " ";
                ViewBag.StartTimeSS = " ";
            }
            else
            {
                st = model.ArrivalTime.Split(':');
                stampm = st[1].Split(' ');
                ViewBag.StartTimeHH = st[0];
                ViewBag.StartTimeMM = stampm[0];
                ViewBag.StartTimeSS = stampm[1];
            }
            if (model.DepartureTime == "00:00 AM" || string.IsNullOrEmpty(model.DepartureTime))
            {
                ViewBag.DeptTimeHH = " ";
                ViewBag.DeptTimeMM = " ";
                ViewBag.DeptTimeSS = " ";
            }
            else
            {
                dt = model.DepartureTime.Split(':');
                dtampm = dt[1].Split(' ');
                ViewBag.DeptTimeHH = dt[0];
                ViewBag.DeptTimeMM = dtampm[0];
                ViewBag.DeptTimeSS = dtampm[1];
            }
            model.EditMode = "Edit";
            ViewBag.MM = GetMM();
            return PartialView("_EntryFormTour", model);
        }

        public static SelectList GetMM()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = " ", Value = " " });
            result.Add(new SelectListItem() { Text = "00", Value = "00" });
            result.Add(new SelectListItem() { Text = "15", Value = "15" });
            result.Add(new SelectListItem() { Text = "30", Value = "30" });
            result.Add(new SelectListItem() { Text = "45", Value = "45" });

            return new SelectList(result, "Value", "Text");
        }

        public ActionResult DeletetMemberTour(int Id)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            var ActualTour = (tMemberTour)Helper.ExecuteService("ATour", "GetTourById", new tMemberTour { TourId = Id });
            var tMemberTour = Helper.ExecuteService("ATour", "DeleteTour", new tMemberTour { TourId = Id });
            if (!string.IsNullOrEmpty(ActualTour.Attachement))
                RemoveExistingPhoto(ActualTour.Attachement);

            return RedirectToAction("Index");
        }

        #endregion member tours

        //// added By Badri
        public ActionResult CreateMemberCategory()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            var model = new AlbumCategoryViewModel()
            {
                Mode = "Add",
                UserId = CurrentSession.UserName
            };
            //ViewBag.userId = CurrentSession.UserID;
            return PartialView("_CreateMemberCategory", model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveMemberAlbumCategory(AlbumCategoryViewModel model)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            if (model.Mode == "Add")
            {
                Helper.ExecuteService("AlbumCategory", "AddAlbumCategory", model.AlbumCategoryDomainModel());
            }
            else
            {
                Helper.ExecuteService("AlbumCategory", "UpdateAlbumCategory", model.AlbumCategoryDomainModel());
            }

            return RedirectToAction("Index");
        }

        public ActionResult GetAllMemberCategory()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            int Id = Convert.ToInt32(CurrentSession.UserName);
            var Gallery = (List<AlbumCategory>)Helper.ExecuteService("AlbumCategory", "GetAlbumCategory", Id);

            var model = Gallery.AlbumCategoryViewModel();

            return PartialView("_GetAllMemberCategory", model);
        }

        public ActionResult EditMemberAlbumCategory(int Id)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            AlbumCategory Gallery = (AlbumCategory)Helper.ExecuteService("AlbumCategory", "EditAlbumCategory", Id);
            int UserId = Convert.ToInt32(CurrentSession.UserName);
            var model = Gallery.ToEditAlbumCategoryViewModel("Edit");
            model.UserId = UserId.ToString();

            return PartialView("_EditMemberAlbumCategory", model);
        }

        public ActionResult DeleteMemberAlbumCategory(int Id)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            AlbumCategory Gallery = (AlbumCategory)Helper.ExecuteService("AlbumCategory", "DeleteAlbumCategory", Id);

            return RedirectToAction("Index");
        }

        // Functionality for  Album Gallery By Badri
        public ActionResult GetGalleryAlbumIndex()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            int Id = Convert.ToInt32(CurrentSession.UserName);
            var Gallery = (List<Category>)Helper.ExecuteService("Gallery", "GetAllGalleryCategory1", Id);
            var filacessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

            var model = Gallery.ToViewModel();
            ViewBag.FileAcessPath = filacessingSettings.SettingValue;
            return View("_GetGalleryAlbumIndex", model);
            //return View(model);

            //catch (Exception ex)
            //{
            //    RecordError(ex, "Getting the Gallery Category List");
            //    ViewBag.ErrorMessage = "There was an Error While Getting the Gallery Category List";
            //    return View("AdminErrorPage");
            //}
        }

        public ActionResult CreateGalleryAlbum()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                AlbumCategoryViewModel model1 = new AlbumCategoryViewModel();
                int Id = Convert.ToInt32(CurrentSession.UserName);
                var Res = (List<AlbumCategory>)Helper.ExecuteService("Gallery", "GetDropDownData1", Id);

                var filacessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                var model = new CategoryViewModel()
                {
                    Mode = "Add",
                    TypeList = ModelMapping.GetStatusTypes(),
                    ImagesList = ModelMapping.GetGalleryImages(),
                    Status = 1,

                    Dropdownlist = new SelectList(Res, "AlbumCategoryID", "CategoryName"),
                };

                ViewBag.FileAcessingPath = filacessingSettings.SettingValue;
                return PartialView("_CreateGalleryAlbum", model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Creating Gallery Category");
                ViewBag.ErrorMessage = "There was an Error While Creating the Gallery Category Details";
                return View("AdminErrorPage");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveAlbumGallery(CategoryViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                if (model.Mode == "Add")
                {
                    // model.IsActive = true;
                    model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("Gallery", "AddGalleryCategory", model.ToDomainModel());
                }
                else
                {
                    // model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    var result = model.ToDomainModel();
                    result.GalleryId = model.GalleryImageId.HasValue ? model.GalleryImageId.Value : 0;
                    Helper.ExecuteService("Gallery", "UpdateGalleryCategory", result);
                }

                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateGallery");
                }
                else
                {
                    // return RedirectToAction("GalleryCategoryIndex");
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                RecordError(ex, "Saving Gallery Category");
                ViewBag.ErrorMessage = "There was an Error While saving the Gallery Category Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult EditAlbumGallery(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                int Idd = Convert.ToInt32(CurrentSession.UserName);
                var Gallery = (Category)Helper.ExecuteService("Gallery", "GetGalleryByIdCategory", new Category { CategoryID = Id });
                var Res = (List<AlbumCategory>)Helper.ExecuteService("Gallery", "GetDropDownData1", Idd);

                var gallerycategorymode = Gallery.ToViewModel("Edit");
                gallerycategorymode.Dropdownlist = new SelectList(Res, "AlbumCategoryID", "CategoryName");

                gallerycategorymode.GalleryImageId = Gallery.GalleryId;
                var galImagesLst = (List<tGallery>)Helper.ExecuteService("Gallery", "GetGalleryImagesByCategoryId", new Category { CategoryID = Id });

                gallerycategorymode.GalleryImageList = new SelectList(galImagesLst, "GalleryId", "FileName", null);
                var settingdata = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                ViewBag.FileAcessingPath = settingdata.SettingValue;
                // return View("CreateGallery", gallerycategorymode);
                return PartialView("_EditAlbumGallery", gallerycategorymode);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Edit Gallery Category");
                ViewBag.ErrorMessage = "There was an Error While editing the Gallery Category Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult DeleteAlbumGallery(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Gallery = Helper.ExecuteService("Gallery", "DeleteGalleryCategory", new Category { CategoryID = Id });

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                RecordError(ex, "Delete Gallery Category");
                ViewBag.ErrorMessage = "There was an Error While Deleting the Gallery Category";
                return View("AdminErrorPage");
            }
        }

        //Gallery Images
        public ActionResult GetGalleryForm()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                int Id = Convert.ToInt32(CurrentSession.UserName);

                var Gallery = (List<tGallery>)Helper.ExecuteService("Gallery", "GetAllGallery1", Id);
                var model = Gallery.ToViewModel();
                // return View(model);
                return PartialView("_GetGalleryForm", model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Create Gallery Category");
                ViewBag.ErrorMessage = "There was an Error While Create the Gallery Categroy";
                return RedirectToAction("AdminErrorPage");
            }
        }

        public ActionResult CreateGallery()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                int Id = Convert.ToInt32(CurrentSession.UserName);
                var categorytype = (List<Category>)Helper.ExecuteService("Gallery", "GetAllCategoryType1", Id);
                var Res = (List<AlbumCategory>)Helper.ExecuteService("Gallery", "GetDropDownData", null);

                var model = new GalleryViewModel()
                {
                    Mode = "Add",
                    TypeList = ModelMapping.GetStatusTypes(),
                    ImagesList = ModelMapping.GetGalleryImages(),
                    CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null),
                    GalleryTypeList = ModelMapping.GetGalleryType(),
                    Status = 1
                };
                return PartialView("_CreateGallery", model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Creating Gallery");
                ViewBag.ErrorMessage = "There was an Error While Creating the Gallery Details";
                return View("AdminErrorPage");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveGalleryForm(GalleryViewModel model, HttpPostedFileBase file)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var gallery = model.ToDomainModel();
                if (model.Mode == "Add")
                {
                    // Guid FileName = Guid.NewGuid();
                    gallery.FilePath = "";//UploadPhoto(file, file.FileName);
                    gallery.FileName = "";//file.FileName;
                    gallery.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    gallery.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    model.IsActive = true;
                    var galleryId = (int)Helper.ExecuteService("Gallery", "AddGallery", gallery);

                    gallery.GalleryId = galleryId;
                    // Code for change the Gallery File name

                    if (file != null)
                    {
                        var filename = galleryId + "_" + file.FileName;
                        gallery.FilePath = UploadPhoto1(file, filename);
                        gallery.FileName = filename;
                        gallery.ThumbName = filename;
                        Helper.ExecuteService("Gallery", "UpdateGallery", gallery);
                    }
                }
                else
                {
                    if (file != null)
                    {
                        if (gallery.FileName != null)
                        {
                            RemoveExistingPhoto(gallery.FileName);
                        }
                        var upfileName = gallery.GalleryId + "_" + file.FileName;

                        gallery.FilePath = UploadPhoto1(file, upfileName);

                        gallery.FileName = upfileName;
                        gallery.ThumbName = upfileName;
                        //  Guid FileName = Guid.NewGuid();

                        gallery.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                        Helper.ExecuteService("Gallery", "UpdateGallery", gallery);
                    }
                    else
                    {
                        Helper.ExecuteService("Gallery", "UpdateGallery", gallery);
                    }
                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateGallery");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                RecordError(ex, "Saving Gallery");
                ViewBag.ErrorMessage = "There was an Error While saving the Gallery Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult GalleryEditEntry(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                int Idd = Convert.ToInt32(CurrentSession.UserName);
                var categorytype = (List<Category>)Helper.ExecuteService("Gallery", "GetAllCategoryType1", Idd);
                var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                var Gallery = (tGallery)Helper.ExecuteService("Gallery", "GetGalleryById", new tGallery { GalleryId = Id });
                var model = Gallery.ToViewModel("Edit");
                model.ImageLocation = fileAcessingSettings.SettingValue + model.FileLocation + "/" + model.FullImage;
                model.CategoryList = new SelectList(categorytype, "CategoryID", "CategoryName", null);

                //var newPath = model.FileLocation + "\\" + model.FullImage;
                //byte[] image = ReadImage(newPath);
                // System.IO.File.Delete(path);
                // model.bytesImage = "data:image/png;base64," + Convert.ToBase64String(image);
                // return View("CreateGalleryEntry", model);
                return PartialView("_GalleryEditEntry", model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Editing");
                ViewBag.ErrorMessage = "There was an Error While Editing the Gallery Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult DeleteGalleryEntry(int Id)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            try
            {
                //Code for remove image
                var Galleryimage = (tGallery)Helper.ExecuteService("Gallery", "GetGalleryById", new tGallery { GalleryId = Id });

                //Code for remove thumb image for category
                var Gallerythumbimage = (bool)Helper.ExecuteService("Gallery", "DeleteGalleryThumbForCategory", new tGallery { GalleryId = Id });

                var Gallery = Helper.ExecuteService("Gallery", "DeleteGallery", new tGallery { GalleryId = Id });//DeleteGalleryThumbForCategory
                if (!string.IsNullOrEmpty(Galleryimage.FileName))
                    RemoveExistingPhoto(Galleryimage.FileName);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                RecordError(ex, "Deleting Gallery");
                ViewBag.ErrorMessage = "There was an Error While Deleting the Gallery Details";
                return View("AdminErrorPage");
            }
        }

        //Added By Badri for Member Panchayat
        public ActionResult GetAllDisrictPanchayat()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                int Id = Convert.ToInt32(CurrentSession.UserName);
                string[] str = new string[2];
                str[0] = Id.ToString();
                str[1] = CurrentSession.AssemblyId;

                var GetData1 = (List<PanchayatConstituencyModal>)Helper.ExecuteService("Notice", "GetDistricts", str);

                var DName1 = (List<PanchayatConstituencyModal>)Helper.ExecuteService("Notice", "GetDName", str);

                //  var model = Gallery.ToViewModel();
                // return View(model);
                // var model = GetData;
                PanchayatConstituencyModal model = new PanchayatConstituencyModal();

                model.ShowData = GetData1;
                model.DNamePanchayat = DName1;

                return PartialView("_GetAllDisrictPanchayat", model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Create Gallery Category");
                ViewBag.ErrorMessage = "There was an Error While Create the Gallery Categroy";
                return RedirectToAction("AdminErrorPage");
            }
        }

        public ActionResult GetAllDisrictPanchayat1()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                int Id = Convert.ToInt32(CurrentSession.UserName);
                string[] str = new string[2];
                str[0] = Id.ToString();
                str[1] = CurrentSession.AssemblyId;
                var GetData1 = (List<PanchayatConstituencyModal>)Helper.ExecuteService("Notice", "GetAllPanchayat", str);
                var CName1 = (mConstituency)Helper.ExecuteService("Notice", "GetCName", str);

                PanchayatConstituencyModal model = new PanchayatConstituencyModal();

                model.tCPanchayat = GetData1;
                model.CName = CName1;

                return PartialView("_GetAllDisrictPanchayat1", model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Create Gallery Category");
                ViewBag.ErrorMessage = "There was an Error While Create the Gallery Categroy";
                return RedirectToAction("AdminErrorPage");
            }
        }

        public ActionResult GetDisricts2(string Id, int DstCode, string SCode, string Name, string CCode)
        {
            //Helper.ExecuteService("Notice", "AddPanchayat", new { Id,DstCode,SCode});
            tConstituencyPanchayat model = new tConstituencyPanchayat();
            model.Ids = Id;
            model.DistrictCode = DstCode;
            model.StateCode = Convert.ToInt32(SCode);
            model.ConstituencyCode = Convert.ToInt32(CCode);

            Helper.ExecuteService("Notice", "AddPanchayat", model);

            int Id1 = Convert.ToInt32(CurrentSession.UserName);
            string[] str = new string[2];
            str[0] = Id1.ToString();
            str[1] = CurrentSession.AssemblyId;
            var GetData1 = (List<PanchayatConstituencyModal>)Helper.ExecuteService("Notice", "GetAllPanchayat", str);
            var CName1 = (mConstituency)Helper.ExecuteService("Notice", "GetCName", str);

            PanchayatConstituencyModal model1 = new PanchayatConstituencyModal();

            model1.tCPanchayat = GetData1;
            model1.CName = CName1;

            return PartialView("_GetDisricts2", model1);
        }

        public ActionResult DeletePanchayat(string Id)
        {
            tConstituencyPanchayat model = new tConstituencyPanchayat();
            model.Ids = Id;

            Helper.ExecuteService("Notice", "DeletePanchayat", model);
            int Id1 = Convert.ToInt32(CurrentSession.UserName);
            string[] str = new string[2];
            str[0] = Id1.ToString();
            str[1] = CurrentSession.AssemblyId;
            var GetData1 = (List<PanchayatConstituencyModal>)Helper.ExecuteService("Notice", "GetAllPanchayat", str); //id1
            var CName1 = (mConstituency)Helper.ExecuteService("Notice", "GetCName", str);

            PanchayatConstituencyModal model1 = new PanchayatConstituencyModal();

            model1.tCPanchayat = GetData1;
            model1.CName = CName1;

            return PartialView("_DeletePanchayat", model1);
        }

        public string UploadPhoto1(HttpPostedFileBase File, string FileName)
        {
            try
            {
                if (File != null)
                {
                    var gallerySettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetGalleryFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    var galleryThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetGalleryThumbFileSetting", null);
                    string extension = System.IO.Path.GetExtension(File.FileName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + gallerySettings.SettingValue, FileName);//Server.MapPath("~/Images/News/"), FileName);
                    string path1 = System.IO.Path.Combine(FileSettings.SettingValue + galleryThumbSettings.SettingValue, FileName);//Server.MapPath("~/Images/News/"), FileName);
                    string basePath = System.IO.Path.Combine(FileSettings.SettingValue + gallerySettings.SettingValue + "\\Temp", FileName);//Server.MapPath("~/Images/News/"), FileName);
                    //ExtensionMethods eximg = new ExtensionMethods();
                    ImageResizerExtensions sg = new ImageResizerExtensions(600);
                    File.SaveAs(basePath);
                    sg.Resize(basePath, path);
                    //  Extensions.ImageResizerExtensions imgex = new Extensions.ImageResizerExtensions(600);

                    // imgex.Resize(basePath, path);
                    System.IO.File.Delete(basePath);
                    // Extensions.ImageResizerExtensions imgext = new Extensions.ImageResizerExtensions(85);
                    ImageResizerExtensions imgext = new ImageResizerExtensions(85);
                    File.SaveAs(basePath);
                    imgext.Resize(basePath, path1);

                    System.IO.File.Delete(basePath);
                    return ("/Gallery");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (File != null)
            {
                string extension = System.IO.Path.GetExtension(File.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Images/Gallery/"), FileName);
                File.SaveAs(path);
                return ("/Images/Gallery/");
            }
            return null;
        }

        private string GetAliasNameForFile(string fileName)
        {
            string realName = Path.GetFileNameWithoutExtension(fileName);

            string appendName = GetTimestamp(DateTime.Now);

            string appendFileName = realName + "_" + appendName + Path.GetExtension(fileName);

            return appendFileName;
        }

        private string GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        public FileResult DownloadAttachment(string realAttachment, string aliasAttachment)
        {
            try
            {
                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue, aliasAttachment);
                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = realAttachment;
                }

                return pathRes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "galleryError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {
                }

                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }

        //New Actions

        #region Constituency Group & Group Members

        // Constituency Group Actions
        public ActionResult AddConstituencyGroup()
        {
            RecipientGroupViewModel model = new RecipientGroupViewModel();

            model.EditMode = "Add";

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            model.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            int code = Convert.ToInt32(model.ConstituencyCode);

            ViewBag.ConstituencyName = (string)Helper.ExecuteService("ContactGroups", "GetConstituencyNameByCode", code);

            return PartialView("_AddConstituencyGroups", model);
        }

        [HttpGet]
        public ActionResult AllConstituencyGroupList()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            int code = Convert.ToInt32(data.ConstituencyCode);
            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyByMember", data);

            RecipientGroupViewModel model = new RecipientGroupViewModel();

            model.RecipientGroupsCol = recipientGroupCol.ToViewModel();
            ViewBag.ConstituencyName = (string)Helper.ExecuteService("ContactGroups", "GetConstituencyNameByCode", code);
            return PartialView("_AllConstituencyGroupList", model);
        }

        [HttpGet]
        public ActionResult GetConstituencyGroupId(int? GroupID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            RecipientGroup data = new RecipientGroup();

            data.GroupID = GroupID ?? 0;

            var recipientGroup = (RecipientGroup)Helper.ExecuteService("ContactGroups", "GetConstituencyGroupByMember", data);

            RecipientGroupViewModel model = recipientGroup.ToViewModel();

            model.EditMode = "Edit";
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            int code = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            //model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);
            ViewBag.ConstituencyName = (string)Helper.ExecuteService("ContactGroups", "GetConstituencyNameByCode", code);
            return PartialView("_AddConstituencyGroups", model);
        }

        // Constituency Group Member Actions

        [HttpGet]
        public ActionResult AddConstituencyGroupMember()
        {
            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.EditMode = "Add";

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            int code = Convert.ToInt32(data.ConstituencyCode);

            model.RecipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyGroupsByMember", data);

            //model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);

            //model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);

            model.ConstituencyCode = data.ConstituencyCode;

            model.GenderList.Add("Male");

            model.GenderList.Add("Female");
            ViewBag.ConstituencyName = (string)Helper.ExecuteService("ContactGroups", "GetConstituencyNameByCode", code);
            return PartialView("_AddConstituencyGroupMember", model);
        }

        [HttpGet]
        public ActionResult AllConstituencyGroupMemberList(int? GroupID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = GroupID ?? 0;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetConstituenxyGroupListByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            int code = Convert.ToInt32(recipientGroup.ConstituencyCode);
            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();
            ViewBag.ConstituencyName = (string)Helper.ExecuteService("ContactGroups", "GetConstituencyNameByCode", code);
            return PartialView("_AllConstituencyGroupMemberList", model);
        }

        [HttpGet]
        public ActionResult GetConstituencyGroupMemberId(int? GroupMemberID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            int code = Convert.ToInt32(recipientGroup.ConstituencyCode);
            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupMemberID = GroupMemberID ?? 0;

            recipientGroupMember = (RecipientGroupMember)Helper.ExecuteService("ContactGroups", "GetConstituencyGroupMemberByMemberID", recipientGroupMember);

            model = recipientGroupMember.ToViewModel();

            model.RecipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyGroupsByMember", recipientGroup);

            //model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);

            model.GenderList.Add("Male");

            model.GenderList.Add("Female");
            model.EditMode = "Edit";
            ViewBag.ConstituencyName = (string)Helper.ExecuteService("ContactGroups", "GetConstituencyNameByCode", code);
            return PartialView("_AddConstituencyGroupMember", model);
        }

        // Panchayat Group
        public ActionResult AddPanchayatGroup()
        {
            string consCode = CurrentSession.ConstituencyID;

            RecipientGroupViewModel model = new RecipientGroupViewModel();

            model.EditMode = "Add";

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            model.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            model.PanchayatListCol = (List<mPanchayat>)Helper.ExecuteService("ContactGroups", "GetAllPanchayat", null);

            return PartialView("_AddPanchayatGroups", model);
        }

        [HttpPost]
        public ActionResult SavePanchayatGroup(int? GroupId, string GroupName, int PachayatCode, string GroupDesc, bool IsPublic, bool IsActive, string Mode, int? RankingOrder)
        {
            RecipientGroupViewModel model = new RecipientGroupViewModel();

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                model.GroupId = GroupId ?? 0;

                model.GroupName = GroupName;

                model.PanchayatCode = PachayatCode;

                model.GroupDesc = GroupDesc;

                model.IsPublic = IsPublic;

                model.DepartmentID = "0";

                model.IsActive = IsActive;

                model.RankingOrder = RankingOrder;
                RecipientGroup data = model.ToDomainModelPanchayat();

                mMember member = new mMember();

                member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

                data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

                data.MemberCode = member.MemberCode;

                data.CreatedBy = CurrentSession.UserID;

                data.CreatedDate = DateTime.Now;

                //data.DepartmentCode = "0";

                if (Mode == "Add")
                {
                    Helper.ExecuteService("ContactGroups", "CreateNewContactGroup", data);
                    model.Message = "Group Saved Successfully";
                }
                else
                {
                    Helper.ExecuteService("ContactGroups", "UpdateContactGroup", data);
                    model.Message = "Group Updated Successfully";
                }

                //model.RecipientGroupCount = ((DiaryModel)Helper.ExecuteService("Notice", "GetRecipientGroupCnt", data.MemberCode)).ResultCount;
            }
            catch (Exception ex)
            {
                RecordError(ex, "Save Recipient Group Details");

                ViewBag.ErrorMessage = "There was an Error While Saving Recipient Group Details";

                return View("AdminErrorPage");
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPanchayatGroupId(int? GroupID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            RecipientGroup data = new RecipientGroup();

            data.GroupID = GroupID ?? 0;

            var recipientGroup = (RecipientGroup)Helper.ExecuteService("ContactGroups", "GePanchayatGroupByMember", data);

            RecipientGroupViewModel model = recipientGroup.ToViewModelPanchayat();

            model.EditMode = "Edit";

            model.PanchayatListCol = (List<mPanchayat>)Helper.ExecuteService("ContactGroups", "GetAllPanchayat", null);

            return PartialView("_AddPanchayatGroups", model);
        }

        [HttpGet]
        public ActionResult AllPanchayatGroupList()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllPanchayatGroupsByMember", data);

            RecipientGroupViewModel model = new RecipientGroupViewModel();

            model.RecipientGroupsCol = recipientGroupCol.ToViewModel();

            return PartialView("_AllPanchayatGroupList", model);
        }

        //Panchayat Group Member
        [HttpGet]
        public ActionResult AddPanchayatGroupMember()
        {
            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.EditMode = "Add";

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            model.RecipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllPanchayatGroupsByMember", data);

            model.GenderList.Add("Male");

            model.GenderList.Add("Female");

            return PartialView("_AddPanchayatGroupMember", model);
        }

        [HttpGet]
        public ActionResult AllPanchayatGroupMemberList(int? GroupID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = GroupID ?? 0;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetPanchayatGroupMembersByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllPanchayatGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

            return PartialView("_AllPanchayatGroupMemberList", model);
        }

        [HttpGet]
        public ActionResult GetPanchayatGroupMemberId(int? GroupMemberID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupMemberID = GroupMemberID ?? 0;

            recipientGroupMember = (RecipientGroupMember)Helper.ExecuteService("ContactGroups", "GetPanchayatGroupMemberByID", recipientGroupMember);

            model = recipientGroupMember.ToViewModel();

            model.RecipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllPanchayatGroupsByMember", recipientGroup);

            model.GenderList.Add("Male");

            model.GenderList.Add("Female");
            model.EditMode = "Edit";
            return PartialView("_AddPanchayatGroupMember", model);
        }

        [HttpGet]
        public ActionResult GetMembersByGroupId(int? GroupID)
        {
            RecipientGroupMember data = new RecipientGroupMember();

            data.GroupID = GroupID ?? 0;

            var GroupMembers = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", data);

            return Json(GroupMembers, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetMembersPartial(int? GroupID)
        {
            RecipientGroupMember model = new RecipientGroupMember();

            List<RecipientGroupMember> modelList = new List<RecipientGroupMember>();
            model.GroupID = GroupID ?? 0;

            ViewBag.GroupMembers = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", model);

            modelList = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetGroupMemberByID", model);

            return PartialView("_GroupMemberListmdl", model);
        }

        #endregion Constituency Group & Group Members

        #region Customize SMS and EMAIL

        //Shashi 5 feb
        public ActionResult CustomizeSMSEMAIL()
        {
            OnlineMemberQmodel model = new OnlineMemberQmodel();
            model.intMsgLimit = GetMaxLimitCountSMSByMember();
            model.intMsgLeft = model.intMsgLimit - GetSpentCountSMSByMember();
            return PartialView("_CustomizeSMSEMAIL", model);
        }

        public JsonResult SendSMSEmail(string EmailIds, string MobileNos, string MsgBody, string IsSms, string IsEmail, int SMSLeft, string chklocal, string attachment)
        {
            string msg = string.Empty;
            int smscount = 0;
            int mailcount = 0;
            List<string> phoneNumbers = new List<string>();
            List<string> eids = new List<string>();
            string emailSubject = string.Empty;
            string MSGTemplate = string.Empty;

            if (CurrentSession.IsMember.ToUpper() == "TRUE")
            {
                mMember member = new mMember();
                member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
                member = (mMember)Helper.ExecuteService("Member", "GetMemberDetailsById", member);

                if (CurrentSession.MemberDesignation == "Member Legislative Assembly")
                {
                    emailSubject = "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + " " + CurrentSession.ConstituencyName + ", HP";
                }
                else
                {
                    emailSubject = "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", HPVS";
                }


                MSGTemplate += MsgBody;
                if (IsEmail == "true")
                {
                    MSGTemplate += "<br />";
                }
                else
                {
                    MSGTemplate += Environment.NewLine;
                }
                if (CurrentSession.IsMember.ToUpper() == "FALSE" && CurrentSession.SubUserTypeID == "22")// For User under working of Member
                {
                    MSGTemplate += "" + CurrentSession.Designation + " to Hon'ble " + CurrentSession.MemberDesignation + ", HP Vidhan Sabha";
                }
                else
                {
                    if (CurrentSession.MemberDesignation == "Speaker" || CurrentSession.MemberDesignation == "Deputy Speaker")
                    {
                        MSGTemplate += "" + CurrentSession.MemberDesignation + ", HP Vidhan Sabha";
                    }
                    else
                    {
                        MSGTemplate += "" + CurrentSession.MemberDesignation + ", HP";
                    }
                }
            }
            else
            {
                MSGTemplate = MsgBody;

            }
            string[] emails = EmailIds.Split(',');

            for (int i = 0; i < emails.Length; i++)
            {
                if (!string.IsNullOrEmpty(emails[i]))
                {
                    eids.Add(emails[i]);
                    mailcount++;
                }
            }

            string[] mobnos = MobileNos.Split(',');

            for (int i = 0; i < mobnos.Length; i++)
            {
                if (!string.IsNullOrEmpty(mobnos[i]))
                {
                    var ismob = ValidationAtServer.CheckMobileNumber(mobnos[i].Trim());
                    if (Convert.ToBoolean(ismob) == true)
                    {
                        phoneNumbers.Add(mobnos[i].Trim());
                        smscount++;
                    }
                }
            }

            if (CurrentSession.UserName != "admin")
            {
                if (smscount > SMSLeft)
                {
                    msg = "You Have Only <span class='badge badge-warning'>" + SMSLeft + "</span> Left and You are Sending SMS to <span class='badge badge-purple'>" + smscount + "</span> Receipient. Please Minimize the SMS Receipient List";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
            if (IsSms == "true")
            {
                if (phoneNumbers.Count > 0)
                {
                }
                else
                {
                    msg = "No phone numbers found";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
            if (IsEmail == "true")
            {
                if (eids.Count > 0)
                {
                }
                else
                {
                    msg = "No Email Id found";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }

            Email.API.CustomMailMessage emailmsg = new Email.API.CustomMailMessage();
            emailmsg._body = MSGTemplate;
            emailmsg._isBodyHtml = true;
            emailmsg._from = CurrentSession.MemberEmail;
            if (IsEmail == "true")
            {
                Email.API.EAttachment ea = new Email.API.EAttachment { FileName = new FileInfo(attachment).Name, FileContent = System.IO.File.ReadAllBytes(attachment) };
                emailmsg._attachments = new List<Email.API.EAttachment>();
                emailmsg._attachments.Add(ea);
            }
            emailmsg._subject = emailSubject;
            foreach (var em in eids)
            {
                emailmsg._toList.Add(em.Trim());
            }

            SMSMessageList smsMessage = new SMSMessageList();
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = Convert.ToInt64(CurrentSession.AadharId ?? "999999999999");
            smsMessage.SMSText = MSGTemplate;
            smsMessage.isLocale = Convert.ToBoolean(chklocal);

            foreach (var item in phoneNumbers)
            {
                smsMessage.MobileNo.Add(item.Trim());
            }

            Notification.Send(Convert.ToBoolean(IsSms), Convert.ToBoolean(IsEmail), smsMessage, emailmsg);
            string IsMemberAuthority = "";
            if (Convert.ToString(CurrentSession.MemberCode) != "")
            {
                IsMemberAuthority = "MLAUsers:";
            }
            else
            {
                IsMemberAuthority = "VSUsers:";
            }

            msg = "" + IsMemberAuthority + "Total (" + smscount + ") SMS & (" + mailcount + ") Email Sent Successfully";

            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        private DateTime GetFirstDayOfMonth(int iMonth)
        {
            DateTime dtFrom = new DateTime(DateTime.Now.Year, iMonth, 1);

            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));

            return dtFrom;
        }

        private DateTime GetLastDayOfMonth(int iMonth)
        {
            DateTime dtTo = new DateTime(DateTime.Now.Year, iMonth, 1);

            dtTo = dtTo.AddMonths(1);

            dtTo = dtTo.AddDays(-(dtTo.Day));

            return dtTo;
        }

        [HttpGet]
        public JsonResult GetSpentSMSByMember()
        {
            int SentSmsCount = 0;
            int CurrentMonth = DateTime.Today.Month;

            string FirstDay = GetFirstDayOfMonth(CurrentMonth).ToShortDateString();
            // get day of week for first day

            string[] dateParts = FirstDay.Split('/');

            DateTime dtFirstTemp = new

                DateTime(Convert.ToInt32(dateParts[2]),

                Convert.ToInt32(dateParts[0]),

                Convert.ToInt32(dateParts[1]));

            string LastDay = GetLastDayOfMonth(CurrentMonth).ToShortDateString();

            // get day of week for last day

            string[] dateParts2 = LastDay.Split('/');

            DateTime dtLastTemp = new

                DateTime(Convert.ToInt32(dateParts2[2]),

                Convert.ToInt32(dateParts2[0]),

                Convert.ToInt32(dateParts2[1]));

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@from", dtFirstTemp.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@to", dtLastTemp.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", "1"));

            if (CurrentSession.IsMember.ToUpper() == "FALSE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login
            {
                methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.MemberUserID));
            }
            else
            {
                //methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.AadharId));
                methodParameter.Add(new KeyValuePair<string, string>("@UserID", (!string.IsNullOrEmpty(CurrentSession.AadharId) ? CurrentSession.AadharId : "999999999999")));
            }
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "getAllCountSMSByUserID", methodParameter);

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {

                SentSmsCount = Convert.ToInt32(Convert.ToString(dataSet.Tables[0].Rows[0][0]));
            }


            return Json(SentSmsCount, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetMaxLimitSMSByMember()
        {
            mMember model = new mMember();
            if (CurrentSession.IsMember.ToUpper() == "FALSE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login and admin
            {
                model.AadhaarNo = CurrentSession.AadharId;
                CurrentSession.IsMember = "true";
                model.isAlive = Convert.ToBoolean(CurrentSession.IsMember);
            }
            else
            {

                if (string.IsNullOrEmpty(CurrentSession.MemberUserID) == true)
                {
                    model.AadhaarNo = CurrentSession.AadharId;
                }


                //model.AadhaarNo = (!string.IsNullOrEmpty(CurrentSession.MemberUserID) ? CurrentSession.MemberUserID : "999999999999");

                model.isAlive = (!string.IsNullOrEmpty(CurrentSession.IsMember) ? Convert.ToBoolean(CurrentSession.IsMember) : false);
                //model.isAlive = Convert.ToBoolean(CurrentSession.IsMember);

            }


            model.DistrictID = DateTime.Today.Month;
            var MaxLimityCount = (int)Helper.ExecuteService("ContactGroups", "GetMAXLimitByMember", model);

            var ExtendMaxLimityCount = 0;
            if (CurrentSession.IsMember.ToUpper() == "TRUE")
            {
                model.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
                ExtendMaxLimityCount = (int)Helper.ExecuteService("ContactGroups", "GetExtendSMSLimitByMember", model);
            }
            else { ExtendMaxLimityCount = 0; }

            var total = MaxLimityCount + ExtendMaxLimityCount;

            if (CurrentSession.IsMember.ToUpper() == "TRUE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login
            {
                CurrentSession.IsMember = "false";
            }
            return Json(total, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetMembersForALLGroups()
        {
            RecipientGroupMember model = new RecipientGroupMember();

            List<RecipientGroupMember> modelList = new List<RecipientGroupMember>();
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            model.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            //modelList = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetAllMembersForAllGroup", model);
            ViewBag.GroupMembers = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetAllMembersForAllGroup", model);

            return PartialView("_GroupMemberListmdl", model);
        }

        #endregion Customize SMS and EMAIL

        #region Constituency Office

        //Group
        [HttpGet]
        public ActionResult AddConstituencyOfficeGroup()
        {
            RecipientGroupViewModel model = new RecipientGroupViewModel();

            model.EditMode = "Add";

            model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);

            return PartialView("_AddConstituencyOfficeGroup", model);
        }

        [HttpGet]
        public ActionResult AllConstituencyOfficeGroupList()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyOfficeGroupsByMember", data);

            RecipientGroupViewModel model = new RecipientGroupViewModel();

            model.RecipientGroupsCol = recipientGroupCol.ToViewModel();

            return PartialView("_AllConstituencyOfficeGroupList", model);
        }

        [HttpGet]
        public ActionResult GetConstituencyOfficeGroupId(int? GroupID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            RecipientGroup data = new RecipientGroup();

            data.GroupID = GroupID ?? 0;

            var recipientGroup = (RecipientGroup)Helper.ExecuteService("ContactGroups", "GetConstituencyOfficeGroupByMember", data);

            RecipientGroupViewModel model = recipientGroup.ToViewModel();

            model.EditMode = "Edit";

            model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);

            return PartialView("_AddConstituencyOfficeGroup", model);
        }

        //Get Office
        [HttpGet]
        public ActionResult GetOfficesByDeptId(string DeptId)
        {
            RecipientGroupViewModel model = new RecipientGroupViewModel();

            model.OfficeList = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOffice", DeptId);

            return Json(model.OfficeList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetOffices_ByDptDistrictWise(string DistrictId, string Deptid)
        {
            //  RecipientGroupViewModel model1 = new RecipientGroupViewModel();
            RecipientGroup model = new RecipientGroup();   // SubdivisionID
            model.DistId = DistrictId;
            model.DeptId = Deptid;
            model.MemberCode = Convert.ToInt16(CurrentSession.MemberCode);
            model.AssId = Convert.ToInt16(CurrentSession.AssemblyId);
            if (CurrentSession.SubDivisionId == "")
            {
                string constituencyID = CurrentSession.ConstituencyID;
                string[] str = new string[2];
                str[0] = CurrentSession.AssemblyId;
                str[1] = constituencyID;
                string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
                if (strOutput != null)
                {
                    constituencyID = Convert.ToString(strOutput[0]);
                    //CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                }
                model.ConstituencyCode = Convert.ToInt16(constituencyID);
                model.ConstituencyCodeForSubdivision = "0";
            }
            else
            {
                model.ConstituencyCodeForSubdivision = CurrentSession.ConstituencyID;
            }

            model.OfficeList = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOfficemapped_ByDeptDistrict", model);

            return Json(model.OfficeList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAllOffices()
        {
            RecipientGroupViewModel model = new RecipientGroupViewModel();

            model.OfficeList = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOffice", null);

            return Json(model.OfficeList, JsonRequestBehavior.AllowGet);
        }

        //Group member
        [HttpGet]
        public ActionResult AddConstituencyOfficeGroupMember()
        {
            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.EditMode = "Add";

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            model.RecipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyOfficeGroupsByMember", data);

            model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);

            model.GenderList.Add("Male");

            model.GenderList.Add("Female");

            return PartialView("_AddConstituencyOfficeGroupMember", model);
        }


        public ActionResult Test(string value)
        {
            CurrentSession.MemberCode = value;

            return RedirectToAction("AllConstituencyOfficeGroupMemberList");
        }

        public JsonResult SetSession(string value)
        {
            CurrentSession.MemberCode = value;
            return Json("Sucsess", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AllConstituencyOfficeGroupMemberList(int? GroupID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            //    member.MemberCode = 48;
            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = GroupID ?? 0;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetConstituencyOfficeGroupMembersByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyOfficeGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

            return PartialView("_AllConstituencyOfficeGroupMemberList", model);
        }

        [HttpGet]
        public ActionResult GetConstituencyOfficeGroupMemberId(int? GroupMemberID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupMemberID = GroupMemberID ?? 0;

            recipientGroupMember = (RecipientGroupMember)Helper.ExecuteService("ContactGroups", "GetContactOfficeGroupMemberByID", recipientGroupMember);

            model = recipientGroupMember.ToViewModel();

            model.RecipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyOfficeGroupsByMember", recipientGroup);

            model.DepartmentListCol = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);

            model.GenderList.Add("Male");

            model.GenderList.Add("Female");

            model.EditMode = "Edit";

            return PartialView("_AddConstituencyOfficeGroupMember", model);
        }

        #endregion Constituency Office

        public ActionResult GenerateOTP(string PType)
        {
            DiaryModel Dmdl = new DiaryModel();
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            Dmdl.MobileNumber = Helper.ExecuteService("Member", "GetMobileNoByMembercode", member) as string;
            Dmdl.MobileNumber = Dmdl.MobileNumber.Trim();
            ///generate OTP
            string OTP = GenerateOTPNmuber();
            tOTPDetails otp = new tOTPDetails();
            otp.MobileNo = Dmdl.MobileNumber;
            // otp.UserName = empId.ToString();
            otp.OTP = OTP;
            otp = (tOTPDetails)Helper.ExecuteService("OTPDetails", "AddOTP", otp);
            Dmdl.OTPQ = OTP;
            Dmdl.OTPID = otp.OTPid;

            try
            {
                SMSService Services = new SMSService();
                SMSMessageList smsMessage = new SMSMessageList();
                smsMessage.SMSText = "Your eVidhan OTP is " + otp.OTP + " .";
                smsMessage.ModuleActionID = 1;
                smsMessage.UniqueIdentificationID = 1;
                smsMessage.MobileNo.Add(Dmdl.MobileNumber);
                Notification.Send(true, false, smsMessage, null);
            }
            catch
            {
            }
            Dmdl.PaperEntryType = PType;
            return PartialView("_GenerateOTP", Dmdl);
        }

        //New OTP Sameer

        public ActionResult OTPPopup(int Id, string PType)
        {
            DiaryModel Dmdl = new DiaryModel();
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            Dmdl.MobileNumber = Helper.ExecuteService("Member", "GetMobileNoByMembercode", member) as string;
            ///generate OTP
            string OTP = GenerateOTPNmuber();
            tOTPDetails otp = new tOTPDetails();
            otp.MobileNo = Dmdl.MobileNumber;
            // otp.UserName = empId.ToString();
            otp.OTP = OTP;
            otp = (tOTPDetails)Helper.ExecuteService("OTPDetails", "AddOTP", otp);
            Dmdl.OTPQ = OTP;
            Dmdl.OTPID = otp.OTPid;
            Dmdl.QuestionId = Id;

            try
            {
                SMSService Services = new SMSService();
                SMSMessageList smsMessage = new SMSMessageList();
                smsMessage.SMSText = "Your eVidhan OTP is " + otp.OTP + " .";
                smsMessage.ModuleActionID = 1;
                smsMessage.UniqueIdentificationID = 1;
                smsMessage.MobileNo.Add(Dmdl.MobileNumber);
                Notification.Send(true, false, smsMessage, null);
            }
            catch
            {
            }
            Dmdl.PaperEntryType = PType;
            return PartialView("_OTPEntryPopUp", Dmdl);
        }

        public ActionResult GetOTP(string empId, string mobile)
        {
            ///generate OTP
            string OTP = GenerateOTPNmuber();
            tOTPDetails otp = new tOTPDetails();
            otp.MobileNo = mobile;
            otp.UserName = empId.ToString();
            otp.OTP = OTP;
            otp = (tOTPDetails)Helper.ExecuteService("OTPDetails", "AddOTP", otp);

            try
            {
                SMSService Services = new SMSService();
                SMSMessageList smsMessage = new SMSMessageList();
                smsMessage.SMSText = "Your eVidhan One Time Password  for Submitting online question is " + otp.OTP + " .";
                smsMessage.ModuleActionID = 1;
                smsMessage.UniqueIdentificationID = 1;
                smsMessage.MobileNo.Add(mobile);
                Notification.Send(true, false, smsMessage, null);

                //Services.sendSingleSMS("hpgovt", "hpdit@1234", "hpgovt", mobile, "Your eVidhan OTP is " + otp.OTP + " .");
            }
            catch
            {
            }
            return PartialView("_OTP", otp);
        }

        public static string GetUniqueKey()
        {
            int maxSize = 6; // whatever length you want
            // char[] chars = new char[62];
            string a;
            a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            char[] chars = new char[a.Length];
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }

        public string GenerateOTPNmuber()
        {
            string OTP = GetUniqueKey();

            return OTP;
        }

        [HttpPost]
        public JsonResult CheckOTPNo(string OTP, string OTPId)
        {
            DiaryModel obj = new DiaryModel();
            OTP = Sanitizer.GetSafeHtmlFragment(OTP);
            obj.EnterOTP = OTP;
            obj.OTPID = Convert.ToInt32(OTPId);
            string st = Helper.ExecuteService("Notice", "CheckOTP", obj) as string;
            //ServiceAdaptor serv = new ServiceAdaptor();
            //int Count = (int)serv.CallService(serParam);
            //if (Count > 0)
            //{
            //    obj.ResultCount = Count;
            //}
            obj.OTPQ = st;
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        #region Getting Groups optional

        public ActionResult GenericGroups()
        {
            OnlineMemberQmodel partMd = new OnlineMemberQmodel();
            //mMember member = new mMember();

            //member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            //data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllContactGroupsByMember", data);

            partMd.RecipientGroupList = recipientGroupCol;

            return PartialView("_GroupListmdl", partMd);
        }

        public ActionResult ConstituencyGroups()
        {
            OnlineMemberQmodel partMd = new OnlineMemberQmodel();
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            int code = Convert.ToInt32(data.ConstituencyCode);
            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyByMember", data);

            partMd.RecipientGroupList = recipientGroupCol;

            return PartialView("_GroupListmdl", partMd);
        }

        public ActionResult PanchayatGroups()
        {
            OnlineMemberQmodel partMd = new OnlineMemberQmodel();
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllPanchayatGroupsByMember", data);

            partMd.RecipientGroupList = recipientGroupCol;

            return PartialView("_GroupListmdl", partMd);
        }

        public ActionResult ConstituencyOfficeGroups()
        {
            OnlineMemberQmodel partMd = new OnlineMemberQmodel();
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup data = new RecipientGroup();

            data.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyOfficeGroupsByMember", data);

            partMd.RecipientGroupList = recipientGroupCol;

            return PartialView("_GroupListmdl", partMd);
        }

        #endregion Getting Groups optional

        #region Export To Excel

        #region Recipient Group

        public ActionResult ExportExcel(int GroupId, string GroupName)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = GroupId;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllContactGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

            #region Export in Excel

            int i = 0;
            int j = 0;
            //string sql = null;
            string data = null;
            Application xlApp;
            Workbook xlWorkBook;
            Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlApp = new Application();
            xlApp.Visible = false;
            xlWorkBook = (Workbook)(xlApp.Workbooks.Add(Missing.Value));
            xlWorkSheet = (Worksheet)xlWorkBook.ActiveSheet;
            if (GroupName.Length > 31)// Restrict For 31 Charchters
            {
                xlWorkSheet.Name = GroupName.Substring(0, 31);
            }
            else
            {
                xlWorkSheet.Name = GroupName;
            }

            List<RecipientGroupMember> recipientGroupMemberdt = new List<RecipientGroupMember>();
            recipientGroupMemberdt = recipientGroupMemberCol;

            System.Data.DataTable dt = new System.Data.DataTable();
            dt = ConvertToDatatable(recipientGroupMemberdt);

            // For Getting Column Names
            string[] colNames = new string[dt.Columns.Count];
            int col = 0;
            foreach (DataColumn dc in dt.Columns)
                colNames[col++] = dc.ColumnName;

            char lastColumn = (char)(65 + dt.Columns.Count - 1);

            xlWorkSheet.get_Range("A1", lastColumn + "1").Value2 = colNames;
            xlWorkSheet.get_Range("A1", lastColumn + "1").Font.Bold = true;
            xlWorkSheet.get_Range("A1", lastColumn + "1").VerticalAlignment
                        = XlVAlign.xlVAlignCenter;

            for (i = 0; i <= dt.Rows.Count - 1; i++)
            {
                var newj = 0;
                for (j = 0; j <= dt.Columns.Count - 1; j++)
                {
                    data = dt.Rows[i][j].ToString();
                    xlWorkSheet.Cells[i + 2, newj + 1] = data;
                    newj++;
                }
            }
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);

            #endregion Export in Excel

            return Json("Export Successfully", JsonRequestBehavior.AllowGet);
        }

        //Export All Group to Excel
        public ActionResult ExportExcelALL()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllContactGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            int i = 0;
            int j = 0;
            //string sql = null;
            string data = null;
            Application xlApp;
            Workbook xlWorkBook = null;
            Worksheet xlWorkSheet = null;
            object misValue = System.Reflection.Missing.Value;
            xlApp = new Application();
            xlApp.Visible = false;
            xlWorkBook = xlApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            //xlWorkBook = (Workbook)(xlApp.Workbooks.Add(Missing.Value));
            //xlWorkSheet = (Worksheet)xlWorkBook.ActiveSheet;
            //foreach (var item in model.RecipientGroupCol)
            //{
            //	xlWorkBook.Worksheets.Add();
            //}
            if (model.RecipientGroupCol.Count > 0)
            {
                foreach (var item in model.RecipientGroupCol)
                {
                    RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

                    recipientGroupMember.GroupID = item.GroupID;

                    var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", recipientGroupMember);

                    model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

                    #region Export in Excel

                    xlWorkBook.Worksheets.Add();
                    int k = 0;
                    xlWorkSheet = xlWorkBook.Worksheets[k + 1];

                    List<RecipientGroupMember> recipientGroupMemberdt = new List<RecipientGroupMember>();
                    recipientGroupMemberdt = recipientGroupMemberCol;

                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = ConvertToDatatable(recipientGroupMemberdt);

                    // For Getting Column Names
                    string[] colNames = new string[dt.Columns.Count];
                    int col = 0;
                    foreach (DataColumn dc in dt.Columns)
                        colNames[col++] = dc.ColumnName;

                    char lastColumn = (char)(65 + dt.Columns.Count - 1);

                    xlWorkSheet.get_Range("A1", lastColumn + "1").Value2 = colNames;
                    xlWorkSheet.get_Range("A1", lastColumn + "1").Font.Bold = true;
                    xlWorkSheet.get_Range("A1", lastColumn + "1").VerticalAlignment
                                = XlVAlign.xlVAlignCenter;

                    for (i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        var newj = 0;
                        for (j = 0; j <= dt.Columns.Count - 1; j++)
                        {
                            data = dt.Rows[i][j].ToString();
                            xlWorkSheet.Cells[i + 2, newj + 1] = data;
                            newj++;
                        }
                    }
                    if (item.Name.Length > 30)// Restrict For 31 Charchters
                    {
                        xlWorkSheet.Name = item.Name.Substring(0, 30);
                    }
                    else
                    {
                        xlWorkSheet.Name = item.Name;
                    }

                    #endregion Export in Excel
                }
                //xlWorkBook.SaveAs("");
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();
                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                return Json("Export Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("No Group Found", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Recipient Group

        #region Constituency Group

        public ActionResult ExportExcelConstituency(int GroupId, string GroupName)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = GroupId;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetConstituenxyGroupListByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

            #region Export in Excel

            int i = 0;
            int j = 0;
            //string sql = null;
            string data = null;
            Application xlApp;
            Workbook xlWorkBook;
            Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlApp = new Application();
            xlApp.Visible = false;
            xlWorkBook = (Workbook)(xlApp.Workbooks.Add(Missing.Value));
            xlWorkSheet = (Worksheet)xlWorkBook.ActiveSheet;
            if (GroupName.Length > 31)// Restrict For 31 Charchters
            {
                xlWorkSheet.Name = GroupName.Substring(0, 31);
            }
            else
            {
                xlWorkSheet.Name = GroupName;
            }

            List<RecipientGroupMember> recipientGroupMemberdt = new List<RecipientGroupMember>();
            recipientGroupMemberdt = recipientGroupMemberCol;

            System.Data.DataTable dt = new System.Data.DataTable();
            dt = ConvertToDatatable(recipientGroupMemberdt);

            // For Getting Column Names
            string[] colNames = new string[dt.Columns.Count];
            int col = 0;
            foreach (DataColumn dc in dt.Columns)
                colNames[col++] = dc.ColumnName;

            char lastColumn = (char)(65 + dt.Columns.Count - 1);

            xlWorkSheet.get_Range("A1", lastColumn + "1").Value2 = colNames;
            xlWorkSheet.get_Range("A1", lastColumn + "1").Font.Bold = true;
            xlWorkSheet.get_Range("A1", lastColumn + "1").VerticalAlignment
                        = XlVAlign.xlVAlignCenter;

            for (i = 0; i <= dt.Rows.Count - 1; i++)
            {
                var newj = 0;
                for (j = 0; j <= dt.Columns.Count - 1; j++)
                {
                    data = dt.Rows[i][j].ToString();
                    xlWorkSheet.Cells[i + 2, newj + 1] = data;
                    newj++;
                }
            }
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);

            #endregion Export in Excel

            return Json("Export Successfully", JsonRequestBehavior.AllowGet);
        }

        //Export All Group to Excel
        public ActionResult ExportExcelALLConstituency()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            int i = 0;
            int j = 0;
            //string sql = null;
            string data = null;
            Application xlApp;
            Workbook xlWorkBook = null;
            Worksheet xlWorkSheet = null;
            object misValue = System.Reflection.Missing.Value;
            xlApp = new Application();
            xlApp.Visible = false;
            xlWorkBook = xlApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            //xlWorkBook = (Workbook)(xlApp.Workbooks.Add(Missing.Value));
            //xlWorkSheet = (Worksheet)xlWorkBook.ActiveSheet;
            //foreach (var item in model.RecipientGroupCol)
            //{
            //	xlWorkBook.Worksheets.Add();
            //}
            if (model.RecipientGroupCol.Count > 0)
            {
                foreach (var item in model.RecipientGroupCol)
                {
                    RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

                    recipientGroupMember.GroupID = item.GroupID;

                    var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetConstituenxyGroupListByGroupID", recipientGroupMember);

                    model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

                    #region Export in Excel

                    xlWorkBook.Worksheets.Add();
                    int k = 0;
                    xlWorkSheet = xlWorkBook.Worksheets[k + 1];

                    List<RecipientGroupMember> recipientGroupMemberdt = new List<RecipientGroupMember>();
                    recipientGroupMemberdt = recipientGroupMemberCol;

                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = ConvertToDatatable(recipientGroupMemberdt);

                    // For Getting Column Names
                    string[] colNames = new string[dt.Columns.Count];
                    int col = 0;
                    foreach (DataColumn dc in dt.Columns)
                        colNames[col++] = dc.ColumnName;

                    char lastColumn = (char)(65 + dt.Columns.Count - 1);

                    xlWorkSheet.get_Range("A1", lastColumn + "1").Value2 = colNames;
                    xlWorkSheet.get_Range("A1", lastColumn + "1").Font.Bold = true;
                    xlWorkSheet.get_Range("A1", lastColumn + "1").VerticalAlignment
                                = XlVAlign.xlVAlignCenter;

                    for (i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        var newj = 0;
                        for (j = 0; j <= dt.Columns.Count - 1; j++)
                        {
                            data = dt.Rows[i][j].ToString();
                            xlWorkSheet.Cells[i + 2, newj + 1] = data;
                            newj++;
                        }
                    }
                    if (item.Name.Length > 30)// Restrict For 31 Charchters
                    {
                        xlWorkSheet.Name = item.Name.Substring(0, 30);
                    }
                    else
                    {
                        xlWorkSheet.Name = item.Name;
                    }

                    #endregion Export in Excel
                }
                //xlWorkBook.SaveAs("");
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();
                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                return Json("Export Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("No Group Found", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Constituency Group

        #region Panchayat Group

        public ActionResult ExportExcelPanchayat(int GroupId, string GroupName)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = GroupId;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetPanchayatGroupMembersByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllPanchayatGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

            #region Export in Excel

            int i = 0;
            int j = 0;
            //string sql = null;
            string data = null;
            Application xlApp;
            Workbook xlWorkBook;
            Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlApp = new Application();
            xlApp.Visible = false;
            xlWorkBook = (Workbook)(xlApp.Workbooks.Add(Missing.Value));
            xlWorkSheet = (Worksheet)xlWorkBook.ActiveSheet;
            if (GroupName.Length > 31)// Restrict For 31 Charchters
            {
                xlWorkSheet.Name = GroupName.Substring(0, 31);
            }
            else
            {
                xlWorkSheet.Name = GroupName;
            }

            List<RecipientGroupMember> recipientGroupMemberdt = new List<RecipientGroupMember>();
            recipientGroupMemberdt = recipientGroupMemberCol;

            System.Data.DataTable dt = new System.Data.DataTable();
            dt = ConvertToDatatable(recipientGroupMemberdt);

            // For Getting Column Names
            string[] colNames = new string[dt.Columns.Count];
            int col = 0;
            foreach (DataColumn dc in dt.Columns)
                colNames[col++] = dc.ColumnName;

            char lastColumn = (char)(65 + dt.Columns.Count - 1);

            xlWorkSheet.get_Range("A1", lastColumn + "1").Value2 = colNames;
            xlWorkSheet.get_Range("A1", lastColumn + "1").Font.Bold = true;
            xlWorkSheet.get_Range("A1", lastColumn + "1").VerticalAlignment
                        = XlVAlign.xlVAlignCenter;

            for (i = 0; i <= dt.Rows.Count - 1; i++)
            {
                var newj = 0;
                for (j = 0; j <= dt.Columns.Count - 1; j++)
                {
                    data = dt.Rows[i][j].ToString();
                    xlWorkSheet.Cells[i + 2, newj + 1] = data;
                    newj++;
                }
            }
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);

            #endregion Export in Excel

            return Json("Export Successfully", JsonRequestBehavior.AllowGet);
        }

        //Export All Group to Excel
        public ActionResult ExportExcelALLPanchayat()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllPanchayatGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            int i = 0;
            int j = 0;
            //string sql = null;
            string data = null;
            Application xlApp;
            Workbook xlWorkBook = null;
            Worksheet xlWorkSheet = null;
            object misValue = System.Reflection.Missing.Value;
            xlApp = new Application();
            xlApp.Visible = false;
            xlWorkBook = xlApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            //xlWorkBook = (Workbook)(xlApp.Workbooks.Add(Missing.Value));
            //xlWorkSheet = (Worksheet)xlWorkBook.ActiveSheet;
            //foreach (var item in model.RecipientGroupCol)
            //{
            //	xlWorkBook.Worksheets.Add();
            //}
            if (model.RecipientGroupCol.Count > 0)
            {
                foreach (var item in model.RecipientGroupCol)
                {
                    RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

                    recipientGroupMember.GroupID = item.GroupID;

                    var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetPanchayatGroupMembersByGroupID", recipientGroupMember);

                    model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

                    #region Export in Excel

                    xlWorkBook.Worksheets.Add();
                    int k = 0;
                    xlWorkSheet = xlWorkBook.Worksheets[k + 1];

                    List<RecipientGroupMember> recipientGroupMemberdt = new List<RecipientGroupMember>();
                    recipientGroupMemberdt = recipientGroupMemberCol;

                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = ConvertToDatatable(recipientGroupMemberdt);

                    // For Getting Column Names
                    string[] colNames = new string[dt.Columns.Count];
                    int col = 0;
                    foreach (DataColumn dc in dt.Columns)
                        colNames[col++] = dc.ColumnName;

                    char lastColumn = (char)(65 + dt.Columns.Count - 1);

                    xlWorkSheet.get_Range("A1", lastColumn + "1").Value2 = colNames;
                    xlWorkSheet.get_Range("A1", lastColumn + "1").Font.Bold = true;
                    xlWorkSheet.get_Range("A1", lastColumn + "1").VerticalAlignment
                                = XlVAlign.xlVAlignCenter;

                    for (i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        var newj = 0;
                        for (j = 0; j <= dt.Columns.Count - 1; j++)
                        {
                            data = dt.Rows[i][j].ToString();
                            xlWorkSheet.Cells[i + 2, newj + 1] = data;
                            newj++;
                        }
                    }
                    if (item.Name.Length > 30)// Restrict For 31 Charchters
                    {
                        xlWorkSheet.Name = item.Name.Substring(0, 30);
                    }
                    else
                    {
                        xlWorkSheet.Name = item.Name;
                    }

                    #endregion Export in Excel
                }
                //xlWorkBook.SaveAs("");
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();
                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                return Json("Export Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("No Group Found", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Panchayat Group

        #region Constitunecy Office Group

        public ActionResult ExportExcelConstituencyOffice(int GroupId, string GroupName)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = GroupId;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetConstituencyOfficeGroupMembersByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyOfficeGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

            #region Export in Excel

            int i = 0;
            int j = 0;
            //string sql = null;
            string data = null;
            Application xlApp;
            Workbook xlWorkBook;
            Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlApp = new Application();
            xlApp.Visible = false;
            xlWorkBook = (Workbook)(xlApp.Workbooks.Add(Missing.Value));
            xlWorkSheet = (Worksheet)xlWorkBook.ActiveSheet;
            if (GroupName.Length > 31)// Restrict For 31 Charchters
            {
                xlWorkSheet.Name = GroupName.Substring(0, 31);
            }
            else
            {
                xlWorkSheet.Name = GroupName;
            }

            List<RecipientGroupMember> recipientGroupMemberdt = new List<RecipientGroupMember>();
            recipientGroupMemberdt = recipientGroupMemberCol;

            System.Data.DataTable dt = new System.Data.DataTable();
            dt = ConvertToDatatable(recipientGroupMemberdt);

            // For Getting Column Names
            string[] colNames = new string[dt.Columns.Count];
            int col = 0;
            foreach (DataColumn dc in dt.Columns)
                colNames[col++] = dc.ColumnName;

            char lastColumn = (char)(65 + dt.Columns.Count - 1);

            xlWorkSheet.get_Range("A1", lastColumn + "1").Value2 = colNames;
            xlWorkSheet.get_Range("A1", lastColumn + "1").Font.Bold = true;
            xlWorkSheet.get_Range("A1", lastColumn + "1").VerticalAlignment
                        = XlVAlign.xlVAlignCenter;

            for (i = 0; i <= dt.Rows.Count - 1; i++)
            {
                var newj = 0;
                for (j = 0; j <= dt.Columns.Count - 1; j++)
                {
                    data = dt.Rows[i][j].ToString();
                    xlWorkSheet.Cells[i + 2, newj + 1] = data;
                    newj++;
                }
            }
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);

            #endregion Export in Excel

            return Json("Export Successfully", JsonRequestBehavior.AllowGet);
        }

        //Export All Group to Excel
        public ActionResult ExportExcelALLConstituencyOffice()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyOfficeGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            int i = 0;
            int j = 0;
            //string sql = null;
            string data = null;
            Application xlApp;
            Workbook xlWorkBook = null;
            Worksheet xlWorkSheet = null;
            object misValue = System.Reflection.Missing.Value;
            xlApp = new Application();
            xlApp.Visible = false;
            xlWorkBook = xlApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            //xlWorkBook = (Workbook)(xlApp.Workbooks.Add(Missing.Value));
            //xlWorkSheet = (Worksheet)xlWorkBook.ActiveSheet;
            //foreach (var item in model.RecipientGroupCol)
            //{
            //	xlWorkBook.Worksheets.Add();
            //}
            if (model.RecipientGroupCol.Count > 0)
            {
                foreach (var item in model.RecipientGroupCol)
                {
                    RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

                    recipientGroupMember.GroupID = item.GroupID;

                    var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetConstituencyOfficeGroupMembersByGroupID", recipientGroupMember);

                    model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

                    #region Export in Excel

                    xlWorkBook.Worksheets.Add();
                    int k = 0;
                    xlWorkSheet = xlWorkBook.Worksheets[k + 1];

                    List<RecipientGroupMember> recipientGroupMemberdt = new List<RecipientGroupMember>();
                    recipientGroupMemberdt = recipientGroupMemberCol;

                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = ConvertToDatatable(recipientGroupMemberdt);

                    // For Getting Column Names
                    string[] colNames = new string[dt.Columns.Count];
                    int col = 0;
                    foreach (DataColumn dc in dt.Columns)
                        colNames[col++] = dc.ColumnName;

                    char lastColumn = (char)(65 + dt.Columns.Count - 1);

                    xlWorkSheet.get_Range("A1", lastColumn + "1").Value2 = colNames;
                    xlWorkSheet.get_Range("A1", lastColumn + "1").Font.Bold = true;
                    xlWorkSheet.get_Range("A1", lastColumn + "1").VerticalAlignment
                                = XlVAlign.xlVAlignCenter;

                    for (i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        var newj = 0;
                        for (j = 0; j <= dt.Columns.Count - 1; j++)
                        {
                            data = dt.Rows[i][j].ToString();
                            xlWorkSheet.Cells[i + 2, newj + 1] = data;
                            newj++;
                        }
                    }
                    if (item.Name.Length > 30)// Restrict For 31 Charchters
                    {
                        xlWorkSheet.Name = item.Name.Substring(0, 30);
                    }
                    else
                    {
                        xlWorkSheet.Name = item.Name;
                    }

                    #endregion Export in Excel
                }
                //xlWorkBook.SaveAs("");
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();
                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                return Json("Export Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("No Group Found", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Constitunecy Office Group

        private static System.Data.DataTable ConvertToDatatable(List<RecipientGroupMember> list)
        {
            System.Data.DataTable dt = new System.Data.DataTable();

            dt.Columns.Add("Name");
            dt.Columns.Add("Designation");
            dt.Columns.Add("Email");
            dt.Columns.Add("MobileNo");
            foreach (var item in list)
            {
                var row = dt.NewRow();

                row["Name"] = item.Name;
                row["Designation"] = item.Designation;
                row["Email"] = item.Email;
                row["MobileNo"] = item.MobileNo;

                dt.Rows.Add(row);
            }

            return dt;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch
            {
                obj = null;
                //MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        #endregion Export To Excel

        #region Export To PDF

        #region Generic Group

        public ActionResult ExportPDFGeneric(RecipientGroupMemberViewModel mdl)
        {
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = mdl.GroupId;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllContactGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

            MemoryStream output = new MemoryStream();
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = "<html><body style='font-family:Times new Roman;;font-size-10px;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";
            outXml += "<style>table {  border-collapse: collapse;}table, th{border:1px solid black;}table, td{border:1px solid black;}</style>";
            outXml += @"<center><h3>Generic Group Member(s)</h3></center>";
            outXml += @"<center><h4>Group Name : <b>" + mdl.Name + "</b></h4></center>";
            outXml += @"<table cellpadding='4' style='width:100%;'>";
            outXml += "<thead>";
            outXml += "<tr>";
            outXml += "<th>Name</th>";
            outXml += "<th>Designation</th>";
            outXml += "<th>Email</th>";
            outXml += "<th>Mobile No.</th>";
            outXml += "</tr></thead><tbody>";

            foreach (var item in model.RecipientGroupMemberCol)
            {
                outXml += "<tr>";
                outXml += "<td align=\"text-align:left;\" >" + item.Name + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.Designation + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.EMail + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.Mobile + "</td>";
                outXml += "</tr>";
            }
            outXml += "</tbody></table>";

            outXml += @"</div></div></body></html>";

            string htmlStringToConvert = outXml;

            HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

            AddElementResult addResult = page.AddElement(htmlToPdfElement);

            byte[] pdfBytes = document1.Save();

            output.Write(pdfBytes, 0, pdfBytes.Length);

            output.Position = 0;
            string url = "/QuestionList/" + "MemberTourPdf/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string NamePdf = "GenericGroup";
            string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + NamePdf + ".pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();
            return new FileStreamResult(output, "application/pdf");
        }

        //Export All Group to Pdf
        public ActionResult ExportPDFALLGeneric()
        {
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllContactGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            MemoryStream output = new MemoryStream();
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = "<html><body style='font-family:Times new Roman;;font-size-10px;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";
            outXml += "<style>table {  border-collapse: collapse;}table, th{border:1px solid black;}table, td{border:1px solid black;}</style>";
            outXml += @"<center><h3>Generic Group Member(s)</h3></center>";
            if (model.RecipientGroupCol.Count > 0)
            {
                foreach (var item in model.RecipientGroupCol)
                {
                    RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

                    recipientGroupMember.GroupID = item.GroupID;

                    var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", recipientGroupMember);

                    model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

                    outXml += @"<h4>Group Name : <b>" + item.Name + "</b></h4>";
                    outXml += @"<table cellpadding='4' style='width:100%;'>";
                    outXml += "<thead>";
                    outXml += "<tr>";
                    outXml += "<th>Name</th>";
                    outXml += "<th>Designation</th>";
                    outXml += "<th>Email</th>";
                    outXml += "<th>Mobile No.</th>";
                    outXml += "</tr></thead><tbody>";

                    foreach (var itemMember in model.RecipientGroupMemberCol)
                    {
                        outXml += "<tr>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Name + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Designation + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.EMail + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Mobile + "</td>";
                        outXml += "</tr>";
                    }
                    outXml += "</tbody></table>";
                }

                outXml += @"</div></div></body></html>";

                string htmlStringToConvert = outXml;

                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                AddElementResult addResult = page.AddElement(htmlToPdfElement);

                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;
                string url = "/QuestionList/" + "MemberTourPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                string a = Guid.NewGuid().ToString();
                string NamePdf = "GenericGroupAll" + a;
                string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + NamePdf + ".pdf");

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                return new FileStreamResult(output, "application/pdf");
            }
            else
            {
                return Json("No Group Found", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Generic Group

        #region Constituency Group

        public ActionResult ExportPDFConstituency(RecipientGroupMemberViewModel mdl)
        {
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = mdl.GroupId;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetConstituenxyGroupListByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

            MemoryStream output = new MemoryStream();
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = "<html><body style='font-family:Times new Roman;;font-size-10px;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";
            outXml += "<style>table {  border-collapse: collapse;}table, th{border:1px solid black;}table, td{border:1px solid black;}</style>";
            outXml += @"<center><h3>Constituency Group Member(s)</h3></center>";
            outXml += @"<center><h4>Group Name : <b>" + mdl.Name + "</b></h4></center>";
            outXml += @"<table cellpadding='4' style='width:100%;'>";
            outXml += "<thead>";
            outXml += "<tr>";
            outXml += "<th>Name</th>";
            outXml += "<th>Designation</th>";
            outXml += "<th>Email</th>";
            outXml += "<th>Mobile No.</th>";
            outXml += "</tr></thead><tbody>";

            foreach (var item in model.RecipientGroupMemberCol)
            {
                outXml += "<tr>";
                outXml += "<td align=\"text-align:left;\" >" + item.Name + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.Designation + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.EMail + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.Mobile + "</td>";
                outXml += "</tr>";
            }
            outXml += "</tbody></table>";

            outXml += @"</div></div></body></html>";

            string htmlStringToConvert = outXml;

            HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

            AddElementResult addResult = page.AddElement(htmlToPdfElement);

            byte[] pdfBytes = document1.Save();

            output.Write(pdfBytes, 0, pdfBytes.Length);

            output.Position = 0;
            string url = "/QuestionList/" + "MemberTourPdf/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string NamePdf = "ConstituencyGroup";
            string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + NamePdf + ".pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();
            return new FileStreamResult(output, "application/pdf");
        }

        //Export All Group to Pdf
        public ActionResult ExportPDFALLConstituency()
        {
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            MemoryStream output = new MemoryStream();
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = "<html><body style='font-family:Times new Roman;;font-size-10px;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";
            outXml += "<style>table {  border-collapse: collapse;}table, th{border:1px solid black;}table, td{border:1px solid black;}</style>";
            outXml += @"<center><h3>Constituency Group Member(s)</h3></center>";
            if (model.RecipientGroupCol.Count > 0)
            {
                foreach (var item in model.RecipientGroupCol)
                {
                    RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

                    recipientGroupMember.GroupID = item.GroupID;

                    var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetConstituenxyGroupListByGroupID", recipientGroupMember);

                    model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

                    outXml += @"<h4>Group Name : <b>" + item.Name + "</b></h4>";
                    outXml += @"<table cellpadding='4' style='width:100%;'>";
                    outXml += "<thead>";
                    outXml += "<tr>";
                    outXml += "<th>Name</th>";
                    outXml += "<th>Designation</th>";
                    outXml += "<th>Email</th>";
                    outXml += "<th>Mobile No.</th>";
                    outXml += "</tr></thead><tbody>";

                    foreach (var itemMember in model.RecipientGroupMemberCol)
                    {
                        outXml += "<tr>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Name + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Designation + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.EMail + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Mobile + "</td>";
                        outXml += "</tr>";
                    }
                    outXml += "</tbody></table>";
                }

                outXml += @"</div></div></body></html>";

                string htmlStringToConvert = outXml;

                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                AddElementResult addResult = page.AddElement(htmlToPdfElement);

                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;
                string url = "/QuestionList/" + "MemberTourPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                string a = Guid.NewGuid().ToString();
                string NamePdf = "ConstituencyGroupAll" + a;
                string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + NamePdf + ".pdf");

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                return new FileStreamResult(output, "application/pdf");
            }
            else
            {
                return Json("No Group Found", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Constituency Group

        #region Panchayat Group

        public ActionResult ExportPDFPanchayat(RecipientGroupMemberViewModel mdl)
        {
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = mdl.GroupId;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetPanchayatGroupMembersByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllPanchayatGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

            MemoryStream output = new MemoryStream();
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = "<html><body style='font-family:Times new Roman;;font-size-10px;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";
            outXml += "<style>table {  border-collapse: collapse;}table, th{border:1px solid black;}table, td{border:1px solid black;}</style>";
            outXml += @"<center><h3>Panchayat Group Member(s)</h3></center>";
            outXml += @"<center><h4>Group Name : <b>" + mdl.Name + "</b></h4></center>";
            outXml += @"<table cellpadding='4' style='width:100%;'>";
            outXml += "<thead>";
            outXml += "<tr>";
            outXml += "<th>Name</th>";
            outXml += "<th>Designation</th>";
            outXml += "<th>Email</th>";
            outXml += "<th>Mobile No.</th>";
            outXml += "</tr></thead><tbody>";

            foreach (var item in model.RecipientGroupMemberCol)
            {
                outXml += "<tr>";
                outXml += "<td align=\"text-align:left;\" >" + item.Name + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.Designation + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.EMail + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.Mobile + "</td>";
                outXml += "</tr>";
            }
            outXml += "</tbody></table>";

            outXml += @"</div></div></body></html>";

            string htmlStringToConvert = outXml;

            HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

            AddElementResult addResult = page.AddElement(htmlToPdfElement);

            byte[] pdfBytes = document1.Save();

            output.Write(pdfBytes, 0, pdfBytes.Length);

            output.Position = 0;
            string url = "/QuestionList/" + "MemberTourPdf/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string NamePdf = "PanchayatGroup";
            string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + NamePdf + ".pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();
            return new FileStreamResult(output, "application/pdf");
        }

        //Export All Group to Pdf
        public ActionResult ExportPDFALLPanchayat()
        {
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllPanchayatGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            MemoryStream output = new MemoryStream();
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = "<html><body style='font-family:Times new Roman;;font-size-10px;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";
            outXml += "<style>table {  border-collapse: collapse;}table, th{border:1px solid black;}table, td{border:1px solid black;}</style>";
            outXml += @"<center><h3>Panchayat Group Member(s)</h3></center>";
            if (model.RecipientGroupCol.Count > 0)
            {
                foreach (var item in model.RecipientGroupCol)
                {
                    RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

                    recipientGroupMember.GroupID = item.GroupID;

                    var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetPanchayatGroupMembersByGroupID", recipientGroupMember);

                    model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

                    outXml += @"<h4>Group Name : <b>" + item.Name + "</b></h4>";
                    outXml += @"<table cellpadding='4' style='width:100%;'>";
                    outXml += "<thead>";
                    outXml += "<tr>";
                    outXml += "<th>Name</th>";
                    outXml += "<th>Designation</th>";
                    outXml += "<th>Email</th>";
                    outXml += "<th>Mobile No.</th>";
                    outXml += "</tr></thead><tbody>";

                    foreach (var itemMember in model.RecipientGroupMemberCol)
                    {
                        outXml += "<tr>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Name + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Designation + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.EMail + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Mobile + "</td>";
                        outXml += "</tr>";
                    }
                    outXml += "</tbody></table>";
                }

                outXml += @"</div></div></body></html>";

                string htmlStringToConvert = outXml;

                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                AddElementResult addResult = page.AddElement(htmlToPdfElement);

                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;
                string url = "/QuestionList/" + "MemberTourPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                string a = Guid.NewGuid().ToString();
                string NamePdf = "PanchayatGroupAll" + a;
                string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + NamePdf + ".pdf");

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                return new FileStreamResult(output, "application/pdf");
            }
            else
            {
                return Json("No Group Found", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Panchayat Group

        #region Constituency Office Group

        public ActionResult ExportPDFConstituencyOffice(RecipientGroupMemberViewModel mdl)
        {
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

            recipientGroupMember.GroupID = mdl.GroupId;

            var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetConstituencyOfficeGroupMembersByGroupID", recipientGroupMember);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyOfficeGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

            MemoryStream output = new MemoryStream();
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = "<html><body style='font-family:Times new Roman;;font-size-10px;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";
            outXml += "<style>table {  border-collapse: collapse;}table, th{border:1px solid black;}table, td{border:1px solid black;}</style>";
            outXml += @"<center><h3>Constituency Office Group Member(s)</h3></center>";
            outXml += @"<center><h4>Group Name : <b>" + mdl.Name + "</b></h4></center>";
            outXml += @"<table cellpadding='4' style='width:100%;'>";
            outXml += "<thead>";
            outXml += "<tr>";
            outXml += "<th>Name</th>";
            outXml += "<th>Designation</th>";
            outXml += "<th>Email</th>";
            outXml += "<th>Mobile No.</th>";
            outXml += "</tr></thead><tbody>";

            foreach (var item in model.RecipientGroupMemberCol)
            {
                outXml += "<tr>";
                outXml += "<td align=\"text-align:left;\" >" + item.Name + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.Designation + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.EMail + "</td>";
                outXml += "<td align=\"text-align:left;\" >" + item.Mobile + "</td>";
                outXml += "</tr>";
            }
            outXml += "</tbody></table>";

            outXml += @"</div></div></body></html>";

            string htmlStringToConvert = outXml;

            HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

            AddElementResult addResult = page.AddElement(htmlToPdfElement);

            byte[] pdfBytes = document1.Save();

            output.Write(pdfBytes, 0, pdfBytes.Length);

            output.Position = 0;
            string url = "/QuestionList/" + "MemberTourPdf/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string NamePdf = "ConstituencyOfficeGroup";
            string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + NamePdf + ".pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();
            return new FileStreamResult(output, "application/pdf");
        }

        //Export All Group to Pdf
        public ActionResult ExportPDFALLConstituencyOffice()
        {
            mMember member = new mMember();

            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            RecipientGroup recipientGroup = new RecipientGroup();

            recipientGroup.ConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            var recipientGroupCol = (List<RecipientGroup>)Helper.ExecuteService("ContactGroups", "GetAllConstituencyOfficeGroupsByMember", recipientGroup);

            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();

            model.RecipientGroupCol = recipientGroupCol;

            MemoryStream output = new MemoryStream();
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = "<html><body style='font-family:Times new Roman;;font-size-10px;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";
            outXml += "<style>table {  border-collapse: collapse;}table, th{border:1px solid black;}table, td{border:1px solid black;}</style>";
            outXml += @"<center><h3>Constituency Office Group Member(s)</h3></center>";
            if (model.RecipientGroupCol.Count > 0)
            {
                foreach (var item in model.RecipientGroupCol)
                {
                    RecipientGroupMember recipientGroupMember = new RecipientGroupMember();

                    recipientGroupMember.GroupID = item.GroupID;

                    var recipientGroupMemberCol = (List<RecipientGroupMember>)Helper.ExecuteService("ContactGroups", "GetConstituencyOfficeGroupMembersByGroupID", recipientGroupMember);

                    model.RecipientGroupMemberCol = recipientGroupMemberCol.ToViewModel();

                    outXml += @"<h4>Group Name : <b>" + item.Name + "</b></h4>";
                    outXml += @"<table cellpadding='4' style='width:100%;'>";
                    outXml += "<thead>";
                    outXml += "<tr>";
                    outXml += "<th>Name</th>";
                    outXml += "<th>Designation</th>";
                    outXml += "<th>Email</th>";
                    outXml += "<th>Mobile No.</th>";
                    outXml += "</tr></thead><tbody>";

                    foreach (var itemMember in model.RecipientGroupMemberCol)
                    {
                        outXml += "<tr>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Name + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Designation + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.EMail + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + itemMember.Mobile + "</td>";
                        outXml += "</tr>";
                    }
                    outXml += "</tbody></table>";
                }

                outXml += @"</div></div></body></html>";

                string htmlStringToConvert = outXml;

                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                AddElementResult addResult = page.AddElement(htmlToPdfElement);

                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;
                string url = "/QuestionList/" + "MemberTourPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                string a = Guid.NewGuid().ToString();
                string NamePdf = "ConstituencyOfficeGroupAll" + a;
                string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + NamePdf + ".pdf");

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                return new FileStreamResult(output, "application/pdf");
            }
            else
            {
                return Json("No Group Found", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Constituency Office Group

        #endregion Export To PDF

        #region SMS and Email Report

        public ActionResult FillModuleList()
        {
            List<ModuleList> list = (List<ModuleList>)Helper.ExecuteService("SmsAndEmailReport", "getAllModuleList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillModuleUserList(string moduleID)
        {
            List<ModuleUserList> list = (List<ModuleUserList>)Helper.ExecuteService("SmsAndEmailReport", "getModuleUserList", moduleID);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmailResult(string from, string to, string ModuleID, string UserID)
        {
            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            DateTime date1 = new DateTime(Convert.ToInt32(from.Split('/')[2]), Convert.ToInt32(from.Split('/')[0]), Convert.ToInt32(from.Split('/')[1]));
            DateTime date2 = new DateTime(Convert.ToInt32(to.Split('/')[2]), Convert.ToInt32(to.Split('/')[0]), Convert.ToInt32(to.Split('/')[1]));
            methodParameter.Add(new KeyValuePair<string, string>("@from", date1.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@to", date2.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.AadharId));
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbEMAIL", "SelectMSSql", "getAllEmailByUserID", methodParameter);
            //if (ModuleID == "0")
            //{
            //	dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbEMAIL", "SelectMSSql", "getAllEmail", methodParameter);
            //}
            //else
            //{
            //	if (UserID == "0")
            //	{
            //		methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", ModuleID));
            //		dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbEMAIL", "SelectMSSql", "getAllEmailByModuleID", methodParameter);
            //	}
            //	else
            //	{
            //		methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", ModuleID));
            //		methodParameter.Add(new KeyValuePair<string, string>("@UserID", UserID));
            //		dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbEMAIL", "SelectMSSql", "getAllEmailByUserID", methodParameter);
            //	}
            //}

            List<SMSEmailReport> reportList = new List<SMSEmailReport>();
            System.Data.DataTable dt = dataSet.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SMSEmailReport report = new SMSEmailReport();
                int c = dt.Rows[i][0].ToString().IndexOf("sent to");
                string str = dt.Rows[i][0].ToString().Substring(c + 8, dt.Rows[i][0].ToString().Length - (c + 8));
                string[] arr = str.Split(',');
                List<string> lst = new List<string>();
                lst.AddRange(arr);
                report.count = arr.Length;
                report.subject = dt.Rows[i][2].ToString();
                report.Body = dt.Rows[i][3].ToString();
                report.timeSpan = Convert.ToDateTime(dt.Rows[i][1].ToString());
                report.MailorMobile = lst;
                reportList.Add(report);
            }
            return PartialView("_reportEmailList", reportList);
        }


        public string getNameFromMobileNumber(string MobileNo)//by robin
        {
            DataSet dataSet1 = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@MobileNumber", MobileNo.ToString()));
            dataSet1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "getAllNameNDeptForSMS", methodParameter);
            if (dataSet1 != null && dataSet1.Tables.Count > 0)
            {
                //System.Data.DataTable dt = dataSet1.Tables[0];
                if (dataSet1.Tables[0].Rows.Count > 0)
                {
                    string GetName = Convert.ToString(dataSet1.Tables[0].Rows[0]["DisplayName"]);
                    return MobileNo + "\n" + GetName.TrimEnd(',');
                }
                else
                {
                    return MobileNo;
                }
            }
            else
            {
                return MobileNo;
            }
        }
        [HttpPost]
        public ActionResult SMSResult(string from, string to, string ModuleID, string UserID, int StatusID)
        {
            DataSet dataSet = new DataSet();

            var methodParameter = new List<KeyValuePair<string, string>>();
            DateTime date1 = new DateTime(Convert.ToInt32(from.Split('/')[2]), Convert.ToInt32(from.Split('/')[0]), Convert.ToInt32(from.Split('/')[1]));
            DateTime date2 = new DateTime(Convert.ToInt32(to.Split('/')[2]), Convert.ToInt32(to.Split('/')[0]), Convert.ToInt32(to.Split('/')[1]));
            methodParameter.Add(new KeyValuePair<string, string>("@from", date1.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@to", date2.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", "1"));
            methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.AadharId));
            methodParameter.Add(new KeyValuePair<string, string>("@StatusID", Convert.ToString(StatusID)));
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "getAllSMSByUserID", methodParameter);


            //if (ModuleID == "0")
            //{
            //	dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "getAllSMS", methodParameter);
            //}
            //else
            //{
            //	if (UserID == "0")
            //	{
            //		methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", ModuleID));
            //		dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "getAllSMSByModuleID", methodParameter);
            //	}
            //	else
            //	{
            //		methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", ModuleID));
            //		methodParameter.Add(new KeyValuePair<string, string>("@UserID", UserID));
            //		dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "getAllSMSByUserID", methodParameter);
            //	}
            //}

            List<SMSEmailReport> reportList = new List<SMSEmailReport>();

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                System.Data.DataTable dt = dataSet.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SMSEmailReport report = new SMSEmailReport();
                    report.Name = getNameFromMobileNumber(dt.Rows[i][1].ToString());//by robin
                    report.subject = dt.Rows[i][1].ToString();
                    report.Body = dt.Rows[i][2].ToString();
                    report.timeSpan = Convert.ToDateTime(dt.Rows[i][0].ToString());
                    report.status = dt.Rows[i][3].ToString();
                    report.MessageID = Convert.ToInt32(Convert.ToString(dt.Rows[i][4].ToString()));
                    reportList.Add(report);
                }
                ViewBag.MSG = "Total SMS sent :" + dt.Rows.Count;
                return PartialView("_reportSMSList", reportList);
            }
            else
            {
                ViewBag.MSG = "No Data Found";
                return PartialView("_reportSMSList", reportList);
            }
        }

        public JsonResult SendAgainSMS(string sMSIds)
        {
            if (!string.IsNullOrEmpty(sMSIds))
            {
                sMSIds = sMSIds.TrimEnd(',');
            }
            List<int> TagIds = sMSIds.Split(',').Select(int.Parse).ToList();
            int TotalSMSCount = 0;
            foreach (var item in TagIds)
            {
                DataSet dataSet = new DataSet();
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@MessageId", Convert.ToString(item)));
                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "sendSMSByMessageID", methodParameter);
                if (dataSet != null)
                {
                    TotalSMSCount += 1;

                }
            }
            if (TotalSMSCount > 0)
            {
                return Json("Success : Total (" + TotalSMSCount + ") SMS Sent Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Failed : Total (" + TotalSMSCount + ") SMS Sent Successfully", JsonRequestBehavior.AllowGet);
            }

        }


        #endregion SMS and Email Report

        //html View
        [HttpPost]
        public PartialViewResult GeneratePublishedTourPDFHtmlView(OnlineMemberQmodel qModel)
        {
            string msg = string.Empty;

            string selectedTours = qModel.hdnTourList;

            MemoryStream output = new MemoryStream();

            if (selectedTours != null && selectedTours.Length > 0)
            {
                TourViewModel model = new TourViewModel();

                string[] tblMemberTourListRows = selectedTours.Split(new string[] { "$%##**#" },
                        StringSplitOptions.RemoveEmptyEntries);

                if (tblMemberTourListRows != null && tblMemberTourListRows.Length > 0)
                {
                    foreach (string tblMemberTourRow in tblMemberTourListRows)
                    {
                        string[] tblMemberTourListCols = tblMemberTourRow.Split(new string[] { "$#**&##" },
                        StringSplitOptions.None);

                        if (tblMemberTourListCols != null && tblMemberTourListCols.Length > 0)
                        {
                            model.TourListCol.Add(new tMemberTour()
                            {
                                TourId = Convert.ToInt32(tblMemberTourListCols[0]),
                                Departure = tblMemberTourListCols[1],
                                Arrival = tblMemberTourListCols[2],
                                mode = tblMemberTourListCols[3],
                                Purpose = tblMemberTourListCols[4],
                                TourDescrption = tblMemberTourListCols[5],
                            });
                        }
                    }
                }

                if (model.TourListCol.Count > 0)
                {
                    mMember member = new mMember();

                    member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

                    member = (mMember)Helper.ExecuteService("Member", "GetMemberDetailsById", member);

                    EvoPdf.Document document1 = new EvoPdf.Document();
                    document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document1.CompressionLevel = PdfCompressionLevel.Best;
                    document1.Margins = new Margins(0, 0, 0, 0);

                    PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                    PdfPageOrientation.Portrait);

                    string outXml = "<html><body style='font-family:Times new Roman;;font-size-10px;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

                    DateTime Startdate = DateTime.Today;
                    DateTime endate = DateTime.Today;
                    int StartdateCount = 0;
                    foreach (tMemberTour tour in model.TourListCol)
                    {
                        if (StartdateCount == 0)
                        {
                            Startdate = DateTime.ParseExact(tour.Departure.Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            StartdateCount++;
                        }
                        endate = DateTime.ParseExact(tour.Departure.Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    string tourTitle = model.TourListCol.ElementAt(0).TourTitle;
                    string HeadingPdf = "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                    //outXml += "<style>table {  border-collapse: collapse;}table, th{border:1px solid black;}table, td{border:1px solid black;}</style>";
                    outXml += @"<center><h3>" + HeadingPdf + "</h3></center>";
                    outXml += @"<table cellpadding='4' border='1' style='width:100%;'>";
                    outXml += "<thead>";
                    outXml += "<tr>";
                    outXml += "<th>Date</th>";
                    outXml += "<th>Departure</th>";
                    outXml += "<th>Arrival</th>";
                    outXml += "<th>Mode of Journey</th>";
                    outXml += "<th>Purpose</th>";
                    outXml += "<th>Description</th>";
                    outXml += "</tr></thead><tbody>";

                    foreach (tMemberTour tour in model.TourListCol)
                    {
                        string[] tourDepartureInfo = tour.Departure.Split(new string[] { " " },
                        StringSplitOptions.None);

                        string[] tourArrivalInfo = tour.Arrival.Split(new string[] { " " },
                        StringSplitOptions.None);

                        string tourStartDate = tour.Departure.Substring(0, 10);

                        string tourEndDate = tour.Arrival.Substring(0, 10);

                        tour.Departure = tour.Departure;

                        //tour.DepartureTime = tourDepartureInfo[2] + " " + tourDepartureInfo[3];

                        tour.Arrival = tour.Arrival;

                        //tour.ArrivalTime = tourArrivalInfo[2] + " " + tourArrivalInfo[3];

                        outXml += "<tr>";
                        DateTime dt1 = DateTime.ParseExact(tourStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime dt2 = DateTime.ParseExact(tourEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        if (dt1 == dt2)
                        {
                            outXml += "<td align=\"text-align:left;\" >" + tourStartDate + "</td>";
                        }
                        else
                        {
                            outXml += "<td align=\"text-align:left;\" >" + tourStartDate + " to " + tourEndDate + "</td>";
                        }

                        //outXml += "<td align=\"text-align:left;\">" + tour.Departure + " " + tour.DepartureTime + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + tour.Departure.Substring(10) + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + tour.Arrival.Substring(10) + "</td>";
                        //outXml += "<td align=\"text-align:left;\">" + tour.Arrival + " " + tour.ArrivalTime + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + tour.mode + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                        outXml += "<td align=\"text-align:left;\" >" + tour.TourDescrption + "</td>";
                        outXml += "</tr>";
                    }

                    outXml += "</tbody></table>";

                    outXml += @"</div></div></body></html>";

                    string htmlStringToConvert = outXml;

                    ViewBag.tourhtml = htmlStringToConvert;
                    //HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                    //AddElementResult addResult = page.AddElement(htmlToPdfElement);

                    //byte[] pdfBytes = document1.Save();

                    //output.Write(pdfBytes, 0, pdfBytes.Length);

                    //output.Position = 0;

                    //string url = "/QuestionList/" + "MemberTourPdf/";

                    //string directory = Server.MapPath(url);

                    //if (!Directory.Exists(directory))
                    //{
                    //	Directory.CreateDirectory(directory);
                    //}

                    //string path = Path.Combine(Server.MapPath("~" + url), member.MemberCode + "_" + tourTitle + ".pdf");

                    //FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                    //System.IO.FileAccess.Write);

                    //_FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                    //// close file stream
                    //_FileStream.Close();
                }
            }
            return PartialView("_TourHtmlView");
            //return new FileStreamResult(output, "application/pdf");
        }

        #region Office Mapping with Department

        public ActionResult GetAllDepartmentoffice()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var GetData1 = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);

                SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

                model.mDepartment = GetData1;

                return PartialView("_GetAllDepartmentOffice", model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Error on Data Fetching");
                ViewBag.ErrorMessage = "Error on Data Fetching";
                return RedirectToAction("AdminErrorPage");
            }
        }

        public ActionResult GetAllOfficeListByDeptId(string DeptId)
        {
            var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetOfficeByDeptId", DeptId);

            SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

            model.OfficeList = GetData1;

            return PartialView("_GetOfficeByDept", model);
        }

        public ActionResult GetAllDepartmentofficeMapped(string DeptId)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var GetData1 = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);

                SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

                model.mDepartment = GetData1;

                ViewBag.Deptid = DeptId;
                return PartialView("_GetAllDepartmentOfficeMapped", model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Error on Data Fetching");
                ViewBag.ErrorMessage = "Error on Data Fetching";
                return RedirectToAction("AdminErrorPage");
            }
        }

        public ActionResult GetAllMappedOfficeListByDeptId(string DeptId)
        {
            SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model1 = new DomainModel.Models.Office.tDepartmentOfficeMapped();
            model1.DeptId = DeptId;
            model1.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetMappedOfficeByDeptId", model1);

            SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

            model.OfficeList = GetData1;
            model.DepartmentId = DeptId;
            return PartialView("_GetOfficeByDeptMapped", model);
        }

        public ActionResult Getoffice2(string Id, string Deptid)
        {
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            RecipientGroup recipientGroup = new RecipientGroup();
            int CConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            //Helper.ExecuteService("Notice", "AddPanchayat", new { Id,DstCode,SCode});
            SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model = new DomainModel.Models.Office.tDepartmentOfficeMapped();
            model.Ids = Id;
            model.DeptId = Deptid;
            model.AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
            model.ConstituencyCode = CConstituencyCode;// 12;// Convert.ToInt32(CurrentSession.ConstituencyID);
            model.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            // model.ConstituencyCode = Convert.ToInt32(CurrentSession.ConstituencyID);
            //string[] str = new string[2];
            //str[0] = CurrentSession.AssemblyId;
            //str[1] = CurrentSession.ConstituencyID;
            //string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
            //string constituencyID = "";
            //if (strOutput != null)
            //{
            //    constituencyID = Convert.ToString(strOutput[0]);
            //   // CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
            //}
            model.ConstituencyCode = CConstituencyCode;
            Helper.ExecuteService("ContactGroups", "AddOffice", model);

            var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetMappedOfficeByDeptId", model);

            SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model1 = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

            model1.OfficeList = GetData1;
            model1.DepartmentId = Deptid;
            return PartialView("_GetOfficeByDeptMapped", model1);
        }

        public ActionResult GetAllMappedoffice2()
        {
            SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model1 = new DomainModel.Models.Office.tDepartmentOfficeMapped();

            model1.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllMappedOfficeByMemberCode", model1);

            SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

            model.OfficeList = GetData1;

            return PartialView("_GetOfficeByDeptMapped", model);
        }

        public ActionResult DeleteOffice(string Id, string Deptid)
        {
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            RecipientGroup recipientGroup = new RecipientGroup();
            int CConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model = new DomainModel.Models.Office.tDepartmentOfficeMapped();
            model.Ids = Id;
            model.DeptId = Deptid;
            model.AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
            model.ConstituencyCode = CConstituencyCode;// Convert.ToInt32(CurrentSession.ConstituencyID);
            model.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            Helper.ExecuteService("ContactGroups", "DeleteOffice", model);

            var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetMappedOfficeByDeptId", model);

            SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model1 = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

            model1.OfficeList = GetData1;
            model1.DepartmentId = Deptid;
            return PartialView("_GetOfficeByDeptMapped", model1);
        }

        #endregion Office Mapping with Department

        #region Grievnce

        public PartialViewResult PopulateOfficer()
        {
            int MemberCode = int.Parse(CurrentSession.MemberCode);
            var OfficerInfoList = (List<ForwardGrievance>)Helper.ExecuteService("Grievance", "GetAllFieldOfficerByMemberCode", new ForwardGrievance { MemberCode = MemberCode });
            return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_PopulateOfficer.cshtml", OfficerInfoList);
        }

        public PartialViewResult AddOfficerFromGroup(string OfficerId, string AddharIdO)
        {
            int MemberCode = int.Parse(CurrentSession.MemberCode);
            tGrievanceOfficerDeleted obj = new tGrievanceOfficerDeleted();
            obj.AdharId = OfficerId;
            obj.CreatedDate = DateTime.Now;
            obj.MemberCode = MemberCode;
            obj.OfficeId = Convert.ToInt32(AddharIdO);

            Helper.ExecuteService("Grievance", "AddGrievanceOfficerDeleted", obj);
            var OfficerInfoList = (List<ForwardGrievance>)Helper.ExecuteService("Grievance", "GetAllFieldOfficerByMemberCode", new ForwardGrievance { MemberCode = MemberCode });
            return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_PopulateOfficer.cshtml", OfficerInfoList);
        }

        public PartialViewResult DeleteOfficerFromGroup(string OfficerId)
        {
            int MemberCode = int.Parse(CurrentSession.MemberCode);
            tGrievanceOfficerDeleted obj = new tGrievanceOfficerDeleted();
            obj.AdharId = OfficerId;
            obj.MemberCode = MemberCode;

            Helper.ExecuteService("Grievance", "DeleteGrievanceOfficer", obj);
            var OfficerInfoList = (List<ForwardGrievance>)Helper.ExecuteService("Grievance", "GetAllFieldOfficerByMemberCode", new ForwardGrievance { MemberCode = MemberCode });
            return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_PopulateOfficer.cshtml", OfficerInfoList);
        }

        #endregion Grievnce

        #region Import from Excel Sheet Group Members

        #region UploadFileAsyn

        [HttpPost]
        public ContentResult UploadFiles()
        {
            string url = "~/ePaper/TempFile";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<RecipientGroupMemberViewModel>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/ePaper/TempFile"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new RecipientGroupMemberViewModel()
                {
                    FileName = hpf.FileName,
                    FileLength = hpf.ContentLength,
                    FileType = hpf.ContentType,
                });
            }

            return Content("{\"name\":\"" + r[0].FileName + "\",\"type\":\"" + r[0].FileType + "\",\"size\":\"" + string.Format("{0} KB", Convert.ToInt32(r[0].FileLength / 1024)) + "\"}", "application/json");
        }

        public JsonResult RemovePDFFiles()
        {
            string url = "~/ePaper/TempFile";
            string directory = Server.MapPath(url);

            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        #endregion UploadFileAsyn

        public ActionResult ImportFromExcelGroupMembers(int GroupID, string GroupName)
        {
            RecipientGroupMemberViewModel model = new RecipientGroupMemberViewModel();
            model.GroupId = GroupID;
            ViewBag.GroupName = GroupName;
            return PartialView("_ImportFromExcelGroupMembers", model);
        }

        private System.Data.DataTable Validate_AddToDataTable(System.Data.DataTable dt)
        {
            System.Data.DataTable dtAdd = new System.Data.DataTable();

            System.Data.DataRow row;
            if (dt != null && dt.Rows.Count > 0)
            {
                dtAdd.Columns.Clear();
                dtAdd.Columns.Add("GroupMemberName");
                dtAdd.Columns.Add("Gender");
                dtAdd.Columns.Add("Designation");
                dtAdd.Columns.Add("Email");
                dtAdd.Columns.Add("Mobile");
                dtAdd.Columns.Add("Address");
                dtAdd.Columns.Add("Pincode");
                dtAdd.Columns.Add("RankingOrder");
                dtAdd.Columns.Add("LandLineNo");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    row = dtAdd.NewRow();
                    string GroupMemberName = dt.Rows[i]["GroupMemberName"].ToString();
                    int rowno = i + 1;
                    if (!string.IsNullOrEmpty(GroupMemberName))
                    {
                        row["GroupMemberName"] = GroupMemberName;
                    }
                    else { row["GroupMemberName"] = "Invalid Group Member Name at Row [" + rowno + "] Column [GroupMemberName]."; }

                    row["Gender"] = dt.Rows[i]["Gender"].ToString();
                    row["Designation"] = dt.Rows[i]["Designation"].ToString();
                    row["Email"] = dt.Rows[i]["Email"].ToString();
                    string Mobiles = string.IsNullOrEmpty(dt.Rows[i]["Mobile"].ToString()) ? "" : dt.Rows[i]["Mobile"].ToString();
                    string MultipleMobiles = "";
                    if (Mobiles.Contains(','))
                    {
                        string[] mobnos = Mobiles.Split(',');

                        for (int j = 0; j < mobnos.Length; j++)
                        {
                            if (!string.IsNullOrEmpty(mobnos[j]))
                            {
                                var ismob = ValidationAtServer.CheckMobileNumber(mobnos[j].Trim());
                                if (Convert.ToBoolean(ismob) == true)
                                {
                                    MultipleMobiles += mobnos[j] + ',';
                                }
                                else
                                {
                                    MultipleMobiles += "Invalid Mobile No  at Row [" + rowno + "] Coulmn [Mobile] - " + mobnos[j] + ',';
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Mobiles))
                        {
                            var ismob = ValidationAtServer.CheckMobileNumber(Mobiles.Trim());
                            if (Convert.ToBoolean(ismob) == true)
                            {
                                MultipleMobiles = Mobiles;
                            }
                            else
                            {
                                MultipleMobiles = "Invalid Mobile No  at Row [" + rowno + "] Coulmn [Mobile] - " + Mobiles + ',';
                            }
                        }
                    }

                    row["Mobile"] = MultipleMobiles;
                    row["Address"] = dt.Rows[i]["Address"].ToString();
                    row["Pincode"] = dt.Rows[i]["Pincode"].ToString();
                    row["RankingOrder"] = dt.Rows[i]["RankingOrder"].ToString();
                    row["LandLineNo"] = dt.Rows[i]["LandLineNo"].ToString();
                    dtAdd.Rows.Add(row);
                }
            }
            else
            {
                dtAdd.Columns.Clear();
                dtAdd.Columns.Add("Error");
                row = dtAdd.NewRow();
                row["Error"] = "No Data Found in Excel";
                dtAdd.Rows.Add(row);
            }
            return dtAdd;
        }

        [HttpPost]
        public JsonResult SaveImportedData(RecipientGroupMemberViewModel model, HttpPostedFileBase file)
        {
            RecipientGroupMemberViewModel mdl = new RecipientGroupMemberViewModel();
            string res = "";
            DataSet ds = new DataSet();
            string fileLocation = "";

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;  //url;//

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);

            if (Directory.Exists(directory))
            {
                string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                if (savedFileName.Length > 0)
                {
                    string SourceFile = savedFileName[0];
                    foreach (string page in savedFileName)
                    {
                        Guid FileName = Guid.NewGuid();

                        string name = Path.GetFileName(page);

                        string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));

                        //string path = System.IO.Path.Combine(Server.MapPath(directory), FileName + "_" + name.Replace(" ", ""));

                        if (!string.IsNullOrEmpty(name))
                        {
                            System.IO.File.Copy(SourceFile, path, true);
                            string fileExtension = System.IO.Path.GetExtension(name);

                            if (fileExtension == ".xls" || fileExtension == ".xlsx")
                            {
                                fileLocation = path;

                                string excelConnectionString = string.Empty;
                                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                                //connection String for xls file format.
                                if (fileExtension == ".xls")
                                {
                                    excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                                    fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                                }
                                //connection String for xlsx file format.
                                else if (fileExtension == ".xlsx")
                                {
                                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                    fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                                }
                                //Create Connection to Excel work book and add oledb namespace
                                OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                                excelConnection.Open();
                                System.Data.DataTable dt = new System.Data.DataTable();

                                dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                if (dt == null)
                                {
                                    return null;
                                }

                                String[] excelSheets = new String[dt.Rows.Count];
                                int t = 0;
                                //excel data saves in temp file here.
                                foreach (DataRow row in dt.Rows)
                                {
                                    excelSheets[t] = row["TABLE_NAME"].ToString();
                                    t++;
                                }
                                OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);

                                string query = string.Format("Select * from [{0}]", excelSheets[0]);
                                using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                                {
                                    dataAdapter.Fill(ds);
                                }
                                excelConnection.Close();
                                System.IO.File.Delete(fileLocation);
                            }
                            else
                            {
                                return Json("Allowed File Format Only (.xls or .xlsx)", JsonRequestBehavior.AllowGet);
                            }

                            #region Validate Excel Data

                            System.Data.DataTable dtValidated = Validate_AddToDataTable(ds.Tables[0]);

                            if (dtValidated.Rows[0][0].ToString() == "No Data Found in Excel")
                            {
                                mdl.ErrorList = ConvertDataTable<ErrorList>(dtValidated);
                                //var result = JsonConvert.SerializeObject(mdl, Newtonsoft.Json.Formatting.Indented,
                                //			  new JsonSerializerSettings
                                //					 {
                                //						 ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                                //					 });
                                StringBuilder sb = new StringBuilder();
                                sb.Append("<div class='row'><div class='col-xs-12'><div class='table-header'><span class='label label-lg label-yellow arrowed-right' style='height: 24px; font-weight: bold; font-size: 14px;'>Error Details </span></div>");
                                sb.Append("<table class='table table-bordered  dataTable' id='sample-table-2' aria-describedby='sample-table-2_info'>");
                                sb.Append("<tbody role='alert' aria-live='polite' aria-relevant='all'>");
                                sb.Append("<tr>");
                                if (mdl.ErrorList != null)
                                {
                                    foreach (var item in mdl.ErrorList)
                                    {
                                        sb.Append("<td style='text-align:center;font-size:16px;'>" + item.Error + "</td>");
                                    }
                                }
                                sb.Append("</tr>");
                                sb.Append("</tbody></table></div></div>");

                                return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
                            }

                            if (dtValidated.Columns.Count > 1)
                            {
                                bool isValid = true;
                                for (int k = 0; k < dtValidated.Rows.Count; k++)
                                {
                                    if (dtValidated.Rows[k][0].ToString().Contains("Invalid"))
                                    {
                                        isValid = false;
                                    }
                                    if (dtValidated.Rows[k][4].ToString().Contains("Invalid"))
                                    {
                                        isValid = false;
                                    }
                                }

                                if (isValid == false)
                                {
                                    //mdl.DataTableDt = dtValidated;

                                    mdl.ErrorList = ConvertDataTable<ErrorList>(dtValidated);

                                    StringBuilder sb = new StringBuilder();
                                    sb.Append("<div class='row'><div class='col-xs-12'><div class='table-header'><span class='label label-lg label-yellow arrowed-right' style='height: 24px; font-weight: bold; font-size: 14px;'>Error Details </span></div>");
                                    sb.Append("<table class='table table-bordered  dataTable' id='sample-table-2' aria-describedby='sample-table-2_info' style='font-size:10px'>");
                                    sb.Append("<tbody role='alert' aria-live='polite' aria-relevant='all'>");

                                    sb.Append("<tr>");
                                    sb.Append("<th></th>");
                                    sb.Append("<th style='text-align:center'><b>A</b></th>");
                                    sb.Append("<th style='text-align:center'><b>B</b></th>");
                                    sb.Append("<th style='text-align:center'><b>C</b></th>");
                                    sb.Append("<th style='text-align:center'><b>D</b></th>");
                                    sb.Append("<th style='text-align:center'><b>E</b></th>");
                                    sb.Append("<th style='text-align:center'><b>F</b></th>");
                                    sb.Append("<th style='text-align:center'><b>G</b></th>");
                                    sb.Append("<th style='text-align:center'><b>H</b></th>");
                                    sb.Append("<th style='text-align:center'><b>I</b></th>");
                                    sb.Append("</tr>");

                                    sb.Append("<tr style='background-color:yellow;'>");
                                    sb.Append("<th></th>");
                                    sb.Append("<th>GroupMemberName</th>");
                                    sb.Append("<th>Gender</th>");
                                    sb.Append("<th>Designation</th>");
                                    sb.Append("<th>Mobile</th>");
                                    sb.Append("<th>LandLineNo</th>");
                                    sb.Append("<th>Email</th>");
                                    sb.Append("<th>Address</th>");
                                    sb.Append("<th>PinCode</th>");
                                    sb.Append("<th>RankingOrder</th>");
                                    sb.Append("</tr>");
                                    int count = 0;
                                    if (mdl.ErrorList != null)
                                    {
                                        foreach (var item in mdl.ErrorList)
                                        {
                                            count += 1;
                                            sb.Append("<tr>");

                                            sb.Append("<td>" + count + "</td>");
                                            if (item.GroupMemberName.Contains("Invalid"))
                                            { sb.Append("<td style='border:2px solid red'>" + item.GroupMemberName + "</td>"); }
                                            else { sb.Append("<td>" + item.GroupMemberName + "</td>"); }
                                            sb.Append("<td>" + item.Gender + "</td>");
                                            sb.Append("<td>" + item.Designation + "</td>");
                                            if (item.Mobile.Contains("Invalid"))
                                            { sb.Append("<td style='border:2px solid red'>" + item.Mobile + "</td>"); }
                                            else { sb.Append("<td>" + item.Mobile + "</td>"); }
                                            sb.Append("<td>" + item.LandLineNo + "</td>");
                                            sb.Append("<td>" + item.Email + "</td>");
                                            sb.Append("<td>" + item.Address + "</td>");
                                            sb.Append("<td>" + item.Pincode + "</td>");
                                            sb.Append("<td>" + item.RankingOrder + "</td>");
                                            sb.Append("</tr>");
                                        }
                                    }

                                    sb.Append("</tbody></table></div></div>");

                                    //var result = JsonConvert.SerializeObject(mdl, Newtonsoft.Json.Formatting.Indented,
                                    //		  new JsonSerializerSettings
                                    //		  {
                                    //			  ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                                    //		  });

                                    return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
                                }
                            }

                            #endregion Validate Excel Data

                            MemoryStream ms = new MemoryStream();
                            System.Data.DataTable xmldt = new System.Data.DataTable();
                            xmldt.Clear();
                            xmldt = ds.Tables[0];
                            xmldt.WriteXml(ms, true);
                            ms.Seek(0, SeekOrigin.Begin);
                            StreamReader sr = new StreamReader(ms);
                            string xmlString = sr.ReadToEnd();
                            sr.Close();
                            sr.Dispose();

                            DataSet dataSet = new DataSet();
                            var methodParameter = new List<KeyValuePair<string, string>>();
                            methodParameter.Add(new KeyValuePair<string, string>("@GroupID", model.GroupId.ToString()));
                            methodParameter.Add(new KeyValuePair<string, string>("@XmlData", xmlString));
                            methodParameter.Add(new KeyValuePair<string, string>("@CreatedBy", CurrentSession.UserID));
                            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_Committee_ImportFromExcelContactMemberGroups", methodParameter);

                            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                            {
                                res = dataSet.Tables[0].Rows[0][0].ToString();
                                if (res == "Success")
                                {
                                    return Json("Data Imported Successfully", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(res, JsonRequestBehavior.AllowGet);
                                }
                            }
                        }
                    }
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Please Select Excel File", JsonRequestBehavior.AllowGet);
                }
            }
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        #region Convert DataTable to List by Generic Method

        private static List<T> ConvertDataTable<T>(System.Data.DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }

        #endregion Convert DataTable to List by Generic Method

        #endregion Import from Excel Sheet Group Members



        public string GenerateNoticePdfOfRule63(DiaryModel DM)
        {
            try
            {
                string outXml = "";
                string LOBName = DM.NoticeId.ToString();
                string fileName = "";
                string savedPDF = string.Empty;
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-Left:125px;color:green;font-size:20px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                outXml += @"<div>

					</div>
<body>

<table  style='width:30% ;float:right; border-collapse: collapse'>
	
<tr>
											<td colspan='2'>
												<div style='text-align: center;font-size:20px; height: 30px;'>
													</div>
											</td>
										</tr>
									  <tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Diary Number: " + DM.NoticeNumber + @"
												</td>
										</tr>
										<tr>
											<td  style='text-align: right;font-size:20px;'>
												Place: Shimla
												</td>
										</tr>
										
										<tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Date: " + DM.PDfDate + @"
												</td>
										</tr>
								  <tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Time: " + DM.PDfTime + @"<br /><br />
												</td>
										</tr>
										<tr>
											<td colspan='2' style='text-align: right;font-size:20px;'>
												Signature</td>
										</tr>
</table>

<p>&nbsp;</p>
<p align=left>From</p>
<p align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Shri " + DM.MemberId.GetMemberNameByID() + @" <b>M.L.A</b>.</p>
<p align=left>To</p>
<p align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
The Secretary,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Himachal&nbsp; Pradesh Vidhan Sabha,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Shimla-4.
</p>
<p align=left>Sir,</p>
<p align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
Under rule 63 of the Rules of Procedure and Conduct of Business, I give of my 
intention to raise a discussion for short duration of the fillowing matter of 
urgent public importance :-</p>
<br>
<br>
<br>
<br>
<br>
<p align=right>Your&nbsp;&nbsp; Faithfully,<br>
 " + DM.MemberId.GetMemberNameByID() + @" <br>

Member,<br>
Division No . " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")<br>
<p align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
An explanatory note starting the reasons for raising the discussion is attached 
herewith.</p>
Subject: " + DM.Subject + "  " + DM.Notice + @"</td>

<p align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Supported by:-</p>
<p align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
1.&nbsp; --------------------------------------------</p>
<p align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
2.&nbsp; --------------------------------------------</p>
</body>

</html>

";

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public string GenerateNoticePdfOfRule101(DiaryModel DM)
        {
            try
            {
                string outXml = "";
                string LOBName = DM.NoticeId.ToString();
                string fileName = "";
                string savedPDF = string.Empty;

                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-Left:125px;font-size:20px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                outXml += @"<div>

					</div>

<body>

<table  style='border-collapse: collapse; width:100%'>
	<tr>
		<td  width='70%'></td>
		<td width='30%'>Diary Number: " + DM.NoticeNumber + @"<br />
		 Place : Shimla <br>
		Date : " + DM.PDfDate + @"<br />
		Time: " + DM.PDfTime + @"<br /><br />
		Signature
</td>
	</tr>
</table>
<p align=center><b>RESOLUTIONS</b></p>
<p align=left>From <br />
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Shri " + DM.MemberId.GetMemberNameByID() + @" <b>M.L.A</b>.<br />
To</p>
<p align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
The Secretary,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Himachal&nbsp; Pradesh Vidhan Sabha,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Shimla-4.
</p>
<p align=left>Sir,</p>
<p align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I/We hereby give notice to my/our 
intention under rule 101 of the Rules of Procedure and Conduct of Business to 
move the follwing resolutions during the ensuring/current session
 of Vidhan Sabha.
</p>
<br>
<br>
<br>
<p align=left>&nbsp;</p>
<p align=right>Your&nbsp;&nbsp; Faithfully,<br>
 " + DM.MemberId.GetMemberNameByID() + @" <br>
Member,<br>
Division No . " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")<br>
<p align=CENTER>(TEXT OF RESOLUTION)</p>
Subject: " + DM.Subject + "  " + DM.Notice + @"</td>

</body>

</html>
";

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 10, 10), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();


                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }



        public string GenerateNoticePdfOfRule71(DiaryModel DM)
        {
            try
            {
                string outXml = "";
                string LOBName = DM.NoticeId.ToString();
                string fileName = "";
                string savedPDF = string.Empty;
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-Left:125px;font-size:20px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                outXml += @"<div>

					</div>

<html>

<body>
<center>
<h3> SHORT NOTICE QUESTION </h3>
<div style='width:90%'>
<table width='100%'>
<tr>
<td style='vertical-align:top' width='50%'>
<table width='100%'>
<tr>
<td colspan='2' style='text-align: right;'><b> Diary Number: " + DM.NoticeNumber + @"</b></td>
</tr>
<tr>
<td style='text-align:right' colspan='2'>
<span style='float:right'><b>Place Shimla </b></span>

</td>
</tr>
<tr>
<td colspan='2' style='text-align: right;'><b> Date of Notice " + DateTime.Now.ToString("dd/MM/yyyy") + @" </b></td>
</tr>
  <tr>
											<td colspan='2' style='text-align: right;'>
												<b>Time: " + DM.PDfTime + @"</b><br />
												</td>
										</tr>
										<tr>
											<td colspan='2' style='text-align: right;'>
												<b>Signature</b></td>
										</tr>
<tr>
<td><b> Subject " + DM.Subject + @"</b></td>
</tr>
<tr>
<td> <hr/></td>
</tr>
<tr>
<td > 

</td>
</tr>


<tr>
<td >
<span ><b>From :</b></span>
 

</td>
</tr>
<tr><td><b style='margin-left:80px'>Shri/Smt " + DM.MemberId.GetMemberNameByID() + @" , M.L.A.</b><br/></td></tr>

<tr><td><b>To</b></td></tr>
<tr><td></td></tr>
<tr><td><span style='margin-left:80px'>
<b>The Secretary,</b></span><br/>
 <span style='margin-left:80px'><b>Himachal Pradesh Legislative Assembly,</b></span><br/>
 <span style='margin-left:80px'><b>Shimla-4</b></span>
 </td></tr>
 <tr><td></td></tr>
<tr><td><b>Sir,</b></td></tr>
 <tr><td></td></tr>
<tr><td ><span style='margin-left:80px;'>
<b style='text-align:justify'>Under Rule 46 of the Rules of Procedure and Conduct of Business, I give notice of the following Short Notice Question.</b><br/><br/><br/><br/></span>
</td></tr>
<tr><td ><span style='margin-left:80px;'>
<b style='text-align:justify'>The reasons for asking the question at Short Notice are :-</b><br/><br/></span><br/><br/>
</td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
 
<tr><td></td></tr>
<tr><td></td></tr>
 
<tr><td></td></tr>
<tr><td></td></tr>
 
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<br/>
<tr><td><b style='float: right;'>Your faithfully,</b><br/>

</td></tr>
<tr><td><b style='float: right;'>" + DM.MemberId.GetMemberNameByID() + @" </b></td></tr>
<tr><td></td></tr>
<tr><td><b style='float: right;'>Member.</b></td></tr>
<tr><td><b style='float: right;'>Division No. " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")</b><br/></td></tr>
<tr><td><b style='float: left;'>Short Notice Question</b><br/><br/><br/></td></tr>


<tr><td><b>Will the Minister of .....................................................................................................................................be pleased to State :</b></td></tr>
<tr><td><b>" + DM.Notice + @"</b></td></tr>
</table>

</td>
</table>
</td>
</tr>
</center>
</body>
</html>
";

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);
                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();


                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public string GenerateNoticePdfOfRule117_130(DiaryModel DM)
        {
            try
            {
                string outXml = "";
                string LOBName = DM.NoticeId.ToString();
                string fileName = "";
                string savedPDF = string.Empty;
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-Left:125px;font-size:20px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                outXml += @"<div>

					</div>

<html>

<body>

<center>
<h3> हिमाचल प्रदेश विधान सभा </h3>
<div style='width:90%'>
<table width='100%'>
<tr>
<td style='vertical-align:top' width='50%'>
<table width='100%'>

<tr>
<td > 

</td>
</tr>
<tr>
<td style='text-align:right'>

<table style='float:right;text-align:right'>
<tr><td>डायरी संख्या: " + DM.NoticeNumber + @"</td></tr>
<tr><td>शिमला : शिमला</td></tr>
<tr><td>दिनांक :" + DateTime.Now.ToString("dd/MM/yyyy") + @"</td></tr>
<tr><td>समय :" + DM.PDfTime + @"</td></tr>
<tr><td>हस्ताक्षर</td></tr>
</table>
</td>
</tr>
 <tr>
<td >
<span style='font-size:20px;' ><b>प्रेषक</b></span><br/><br/>
 

</td>
</tr>
<tr><td><b style='margin-left:80px;font-size:20px;'>श्री /श्रीमती " + DM.MemberId.GetMemberNameByID() + @" , विधान सभा सदस्य ।</td></tr>
<tr><td><b style='font-size:20px;>सेवा में </b></td></tr>
<tr><td></td></tr>
<tr><td><span style='margin-left:80px;font-size:20px;'>
<b>सचिव,</b></span><br/>
 <span style='margin-left:80px;font-size:20px;'><b>हिमाचल प्रदेश विधान सभा,</b></span><br/>
 <span style='margin-left:80px;font-size:20px;'><b>शिमला - 171004</b></span><br/>
 </td></tr>
<tr><td style='font-size:20px;'><b>महोदय,</b></td></tr>
 <tr><td></td></tr>
<tr><td ><span style='margin-left:80px;'>
<b style='text-align:justify;font-size:20px;'>मैं हिमाचल प्रदेश विधान सभा , प्रक्रिया एवं कार्य संचालन नियमावली के नियम 117 या 130 के अन्तर्गत चालू विधान सभा सत्र के दौरान एतद्द्वारा निम्न संकल्प उठाने की सूचना देता हूं : </b><br/><br/></span>
</td></tr>
<tr><td ><span style='margin-left:80px;'>
<b style='text-align:justify;font-size:20px;'>कि <b style='text-align:justify;font-size:20px;'>Subject: " + DM.Subject + @"</b><br/><br/></span>

</td></tr>
<tr><td><b style='float: right;font-size:20px;'>भवदीय ,</b><br/><br/><br/></td></tr>
<br>
<br>
<tr><td><b style='float: right;font-size:20px;'>" + DM.MemberId.GetMemberNameByID() + @"</b> </td></tr>
<tr><td><b style='float: right;font-size:20px;'>सदस्य ।</b></td></tr>
<tr><td><b style='float: right;font-size:20px;'>डिवीजन नं0 " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")</b><br/></td></tr>
<tr><td><b style='float: left;font-size:20px;'>व्याख्यात्मक टिप्पणी चर्चा उठाने हेतु</b><br/><br/><br/>
<span style='margin-left:80px;'>
<b style='text-align:justify;font-size:20px;'> " + DM.Notice + @"</b><br/><br/></span>
</td></tr>
</table>

</td>

 


</table>
</td>

</tr>


</center>
</body>
</html>
";

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public string GenerateNoticePdfOfRule61(DiaryModel DM)
        {
            try
            {
                string outXml = "";
                string LOBName = DM.NoticeId.ToString();
                string fileName = "";
                string savedPDF = string.Empty;
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-Left:125px;font-size:20px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                outXml += @"<div>

					</div>

<html>

<body>
<center>
<div style='width:90%'>
<table width='100%'>
<tr>
<td style='border-right:1px solid black;vertical-align:top' width='50%'>
<table width='100%'>
<tr>
<td><u>Counter Foil</u><br/></td>
</tr>
<tr>
<td >
<span style='margin-left:100px'>
<b>H.P. VIDHAN SABHA</b></span><br/><br/>
 <span style='margin-left:100px'><b>Half-an-Hour Discussion</b></span>

</td>
</tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><b>1. Subject " + DM.Subject + @"</b><br/><br/><br/><br/></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><b>2. Date of Notice " + DateTime.Now.ToString("dd/MM/yyyy") + @"</b><br/><br/><br/><br/></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><b>3. Question No. and date to which the discussion pertains..........................................</b><br/><br/><br/><br/></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><b>4. Date on which discussion is intended to be raised.................................................</b><br/><br/><br/><br/></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><b>The points on which the discussion is proposed to be raised :-</b><br/><br/></td></tr>
<tr><td><b> " + DM.Notice + @"</b></td></tr>

</table>

</td>
<td width='50%' style='vertical-align:top'>
<table width='100%'>

<tr>
<td style='text-align:right'>
<table style='float:right;text-align:right'>
<tr><td>Diary Number: " + DM.NoticeNumber + @"</td></tr>
<tr><td>Place: Shimla</td></tr>
<tr><td>Date" + DateTime.Now.ToString("dd/MM/yyyy") + @"</td></tr>
<tr><td>Time" + DM.PDfTime + @"</td></tr>
<tr><td>Signature</td></tr>
</table>

</td>
</tr>
<tr>
<td >
<span ><b>From :</b></span><br/>
 

</td>
</tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><b style='margin-left:80px'>" + DM.MemberId.GetMemberNameByID() + @", M.L.A.</b><br/></td></tr>

<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><b>To</b></td></tr>
<tr><td></td></tr>
<tr><td><span style='margin-left:80px'>
<b>The Secretary,</b></span><br/>
 <span style='margin-left:80px'><b>H.P. Legislative Assembly</b></span><br/>
 <span style='margin-left:80px'><b>Shimla-4</b></span><br/><br/>
 </td></tr>
 <tr><td></td></tr>
<tr><td><b>Sir,</b><br/></td></tr>
 <tr><td></td></tr>
<tr><td ><span style='margin-left:80px;'>
<b style='text-align:justify'>Under Rule 61 the Rules of Procedure and Conduct of Business, I give notice of intention to raise discussion on the following points arising out of the answergiven to Starred/Unstarred/Short Notice Question No. ...................... on ..................... 200</b><br/><br/></span>
</td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><b>(i)</b><br/><br/><br/><br/></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><b>(ii)</b><br/><br/><br/><br/></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><b>(iii)</b><br/><br/><br/></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><span style='margin-left:80px'><b>I request that permission may be given to raise the discussion on ................... 200 . An explanatory note stating reasons for raising the discussion is attached herewith.</b></span><br/><br/><br/></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td><b style='float: right;'>Your faithfully,</b><br/><br/><br/></td></tr>
<tr><td><b style='float: right;'>" + DM.MemberId.GetMemberNameByID() + @" </b></td></tr>
<tr><td></td></tr>
<tr><td><b style='float: right;'>Member.</b></td></tr>
<tr><td><b style='float: right;'>Division No." + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")</b></td></tr>

</table>
</td>

</tr>

</table>
</div>
</center>
</body>
</html>
";

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public string GenerateNoticePdfOfRule74CutMotion(DiaryModel DM)
        {
            try
            {
                string outXml = "";
                string Subject3 = "";
                string LOBName = DM.NoticeId.ToString();
                string fileName = "";
                string savedPDF = string.Empty;
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                if (DM.Subject.IndexOf("ḝ") != -1)
                {


                    string[] subjects = new string[3];
                    subjects = DM.Subject.Split('ḝ');
                    if (subjects.Length == 3)
                    {
                        Subject3 = subjects[2];
                    }
                    outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-Left:125px;font-size:20px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div>

					</div>

<html>
	<body>
		<div id='mainDiv' style='margin:3%;'>
			<div='header'>			
					<p style='text-align:center'>FORM OF MOTIONS FOR DISAPPROVAL OF POLICY</p>
                      <p style='text-align:right'>
                    <span style='text-align:right'>Diary Number: " + DM.NoticeNumber + @"</span><br/>
                       <span style='text-align:right'> Place : Shimla</span><br/>
                 <span style='text-align:right'>Date : " + DM.PDfDate + @"</span><br/>
                 <span style='text-align:right'>Time: " + DM.PDfTime + @"</span><br/><br/>
                 <span style='text-align:right'>Signature</span></p>
                                                            					
			</div>
			<div id='body'>
				<p>
					Dear Sir,<br/>
					        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp I hearby give notice of the following cut motions :-
				</p>
				<ol>
				<p style='text-align:center'>Demand No <b> " + DM.DemandNo + @" " + DM.DemandName + @"</b> </p>
					
						<li>
							That the Demand under the head " + DM.DemandName + @"
							(Pages....................) be reduced to Rs.1(Disapproval of Policy regarding) <b> " + subjects[0] + @"</b>
						</li>					
					<p style='text-align:center'>Demand No....................</p>					
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs.1(Disapproval of Policy regarding)  <b> " + subjects[1] + @"</b>
						</li>					
					<p style='text-align:center'>Demand No....................</p>					
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs.1(Disapproval of Policy regarding)  <b> " + Subject3 + @"</b>
						</li>
					</ol>
			</div>
			<br/>
			<br/>
			<div id='footer'>
				<div style='float:left'>
					To<br/>
					<br/>
					The Secretary,<br/>
					H.P Vidhan Sabha,<br/>
					Shimla-4.
				</div>
				<div style='float:right;text-align:right'>
					Yours Faithfully,<br/>	
	<br/>
					<br/>
<br/>
				<br/>
<br/>
					M.L.A.<br/>
					H.P.Vidhan Sabha<br/><br/>
					Division No " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")<br/>
				</div>
			</div>			
		</div>
	</body>
</html>
";
                }
                else
                {
                    string[] subjects = DM.Subject.Split('ḝ');
                    outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-Left:125px;font-size:20px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div>

					</div>

<html>
	<body>
		<div id='mainDiv' style='margin:3%;'>
			<div='header'>			
					<p style='text-align:center'>FORM OF MOTIONS FOR DISAPPROVAL OF POLICY</p>
                      <p style='text-align:right'>
                    <span style='text-align:right'>Diary Number: " + DM.NoticeNumber + @"</span><br/>
                       <span style='text-align:right'> Place : Shimla</span><br/>
                 <span style='text-align:right'>Date : " + DM.PDfDate + @"</span><br/>
                 <span style='text-align:right'>Time: " + DM.PDfTime + @"</span><br/><br/>
                 <span style='text-align:right'>Signature</span></p>
                                                            					
			</div>
			<div id='body'>
				<p>
					Dear Sir,<br/>
					        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp I hearby give notice of the following cut motions :-
				</p>
				<ol>
				<p style='text-align:center'>Demand No <b> " + DM.DemandNo + @" " + DM.DemandName + @"</b> </p>
					
						<li>
							That the Demand under the head " + DM.DemandName + @"
							(Pages....................) be reduced to Rs.1(Disapproval of Policy regarding) <b> " + DM.Subject + @"</b>
						</li>					
					<p style='text-align:center'>Demand No....................</p>					
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs.1(Disapproval of Policy regarding) 
						</li>					
					<p style='text-align:center'>Demand No....................</p>					
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs.1(Disapproval of Policy regarding) 
						</li>
					</ol>
			</div>
			<br/>
			<br/>
			<div id='footer'>
				<div style='float:left'>
					To<br/>
					<br/>
					The Secretary,<br/>
					H.P Vidhan Sabha,<br/>
					Shimla-4.
				</div>
				<div style='float:right;text-align:right'>
					Yours Faithfully,<br/>	
	<br/>
					<br/>
<br/>
				<br/>
<br/>
					M.L.A.<br/>
					H.P.Vidhan Sabha<br/><br/>
					Division No " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")<br/>
				</div>
			</div>			
		</div>
	</body>
</html>
";
                }

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }



        public string GenerateNoticePdfOfRule75EconomyCut(DiaryModel DM)
        {
            try
            {
                string outXml = "";
                string Subject3 = "";
                string LOBName = DM.NoticeId.ToString();
                string fileName = "";
                string savedPDF = string.Empty;
                if (DM.Subject.IndexOf("ḝ") != -1)
                {
                    string[] subjects = new string[3];
                    subjects = DM.Subject.Split('ḝ');
                    if (subjects.Length == 3)
                    {
                        Subject3 = subjects[2];
                    }
                    outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-Left:125px;font-size:20px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div>

					</div>

<html>
	<body>
		<div id='mainDiv' style='margin:3%;'>
			<div='header'>			
					<p style='text-align:center'>FORM OF CUT MOTIONS FOR ECONOMY CUT</p>
                      <p style='text-align:right'>
                    <span style='text-align:right'>Diary Number: " + DM.NoticeNumber + @"</span><br/>
                       <span style='text-align:right'> Place : Shimla</span><br/>
                 <span style='text-align:right'>Date : " + DM.PDfDate + @"</span><br/>
                 <span style='text-align:right'>Time: " + DM.PDfTime + @"</span><br/><br/>
                 <span style='text-align:right'>Signature</span></p>
                                                            					
			</div>
			<div id='body'>
				<p>
					Dear Sir,<br/>
					        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp I hearby give notice of the following cut motions :-
				</p>
				<ol>
				<p style='text-align:center'>Demand No <b> " + DM.DemandNo + @" " + DM.DemandName + @"</b> </p>
					
						<li>
							That the Demand under the head " + DM.DemandName + @"
							(Pages....................) be reduced to Rs......................................(Economy) <b> " + subjects[0] + @"</b>
						</li>					
					<p style='text-align:center'>Demand No ...........................</b> </p>		
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs......................................(Economy)<b> " + subjects[1] + @"</b>
						</li>					
					<p style='text-align:center'>Demand No ...........................</b> </p>				
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs......................................(Economy)<b> " + Subject3 + @"</b>
						</li>
					</ol>
			</div>
			<br/>
			<br/>
			<div id='footer'>
				<div style='float:left'>
					To<br/>
					<br/>
					The Secretary,<br/>
					H.P Vidhan Sabha,<br/>
					Shimla-4.
				</div>
				<div style='float:right;text-align:right'>
					Yours Faithfully,<br/>	
	<br/>
					<br/>
<br/>
				<br/>
<br/>
					M.L.A.<br/>
					H.P.Vidhan Sabha<br/><br/>
					Division No " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")<br/>
				</div>
			</div>			
		</div>
	</body>
</html>
";
                }
                else
                {
                    outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-Left:125px;font-size:20px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div>

					</div>

<html>
	<body>
		<div id='mainDiv' style='margin:3%;'>
			<div='header'>			
					<p style='text-align:center'>FORM OF CUT MOTIONS FOR ECONOMY CUT</p>
                      <p style='text-align:right'>
                    <span style='text-align:right'>Diary Number: " + DM.NoticeNumber + @"</span><br/>
                       <span style='text-align:right'> Place : Shimla</span><br/>
                 <span style='text-align:right'>Date : " + DM.PDfDate + @"</span><br/>
                 <span style='text-align:right'>Time: " + DM.PDfTime + @"</span><br/><br/>
                 <span style='text-align:right'>Signature</span></p>
                                                            					
			</div>
			<div id='body'>
				<p>
					Dear Sir,<br/>
					        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp I hearby give notice of the following cut motions :-
				</p>
				<ol>
				<p style='text-align:center'>Demand No <b> " + DM.DemandNo + @" " + DM.DemandName + @"</b> </p>
					
						<li>
							That the Demand under the head " + DM.DemandName + @"
							(Pages....................) be reduced to Rs......................................(Economy) <b> " + DM.Subject + @"</b>
						</li>					
					<p style='text-align:center'>Demand No....................</p>					
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs......................................(Economy)
						</li>					
					<p style='text-align:center'>Demand No....................</p>					
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs......................................(Economy)
						</li>
					</ol>
			</div>
			<br/>
			<br/>
			<div id='footer'>
				<div style='float:left'>
					To<br/>
					<br/>
					The Secretary,<br/>
					H.P Vidhan Sabha,<br/>
					Shimla-4.
				</div>
				<div style='float:right;text-align:right'>
					Yours Faithfully,<br/>	
	<br/>
					<br/>
<br/>
				<br/>
<br/>
					M.L.A.<br/>
					H.P.Vidhan Sabha<br/><br/>
					Division No " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")<br/>
				</div>
			</div>			
		</div>
	</body>
</html>
";
                }
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);


                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public string GenerateNoticePdfOfRule76TokenCut(DiaryModel DM)
        {
            try
            {
                string outXml = "";
                string Subject3 = "";
                string LOBName = DM.NoticeId.ToString();
                string fileName = "";
                string savedPDF = string.Empty;
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                if (DM.Subject.IndexOf("ḝ") != -1)
                {
                    string[] subjects = new string[3];
                    subjects = DM.Subject.Split('ḝ');
                    if (subjects.Length == 3)
                    {
                        Subject3 = subjects[2];
                    }
                    outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-Left:125px;font-size:20px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div>

					</div>

<html>
	<body>
		<div id='mainDiv' style='margin:3%;'>
			<div='header'>			
					<p style='text-align:center'>FORM OF CUT MOTIONS FOR TOKEN CUT</p>
                      <p style='text-align:right'>
                    <span style='text-align:right'>Diary Number: " + DM.NoticeNumber + @"</span><br/>
                       <span style='text-align:right'> Place : Shimla</span><br/>
                 <span style='text-align:right'>Date : " + DM.PDfDate + @"</span><br/>
                 <span style='text-align:right'>Time: " + DM.PDfTime + @"</span><br/><br/>
                 <span style='text-align:right'>Signature</span></p>
                                                            					
			</div>
			<div id='body'>
				<p>
					Dear Sir,<br/>
					        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp I hearby give notice of the following cut motions :-
				</p>
				<ol>
				<p style='text-align:center'>Demand No <b> " + DM.DemandNo + @" " + DM.DemandName + @"</b> </p>
					
						<li>
							That the Demand under the head " + DM.DemandName + @"
							(Pages....................) be reduced to Rs.100(To Discuss) <b> " + subjects[0] + @"</b>
						</li>					
					<p style='text-align:center'>Demand No....................</p>					
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs.100(To Discuss) <b> " + subjects[1] + @"</b>
						</li>					
					<p style='text-align:center'>Demand No....................</p>					
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs.100(To Discuss) <b> " + Subject3 + @"</b>
						</li>
					</ol>
			</div>
			<br/>
			<br/>
			<div id='footer'>
				<div style='float:left'>
					To<br/>
					<br/>
					The Secretary,<br/>
					H.P Vidhan Sabha,<br/>
					Shimla-4.
				</div>
				<div style='float:right;text-align:right'>
					Yours Faithfully,<br/>	
	<br/>
					<br/>
<br/>
				<br/>
<br/>
					M.L.A.<br/>
					H.P.Vidhan Sabha<br/><br/>
					Division No " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")<br/>
				</div>
			</div>			
		</div>
	</body>
</html>
";
                }
                else
                {
                    outXml = @"<html>
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-Left:125px;font-size:20px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div>

					</div>

<html>
	<body>
		<div id='mainDiv' style='margin:3%;'>
			<div='header'>			
					<p style='text-align:center'>FORM OF CUT MOTIONS FOR TOKEN CUT</p>
                      <p style='text-align:right'>
                    <span style='text-align:right'>Diary Number: " + DM.NoticeNumber + @"</span><br/>
                       <span style='text-align:right'> Place : Shimla</span><br/>
                 <span style='text-align:right'>Date : " + DM.PDfDate + @"</span><br/>
                 <span style='text-align:right'>Time: " + DM.PDfTime + @"</span><br/><br/>
                 <span style='text-align:right'>Signature</span></p>
                                                            					
			</div>
			<div id='body'>
				<p>
					Dear Sir,<br/>
					        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp I hearby give notice of the following cut motions :-
				</p>
				<ol>
				<p style='text-align:center'>Demand No <b> " + DM.DemandNo + @" " + DM.DemandName + @"</b> </p>
					
						<li>
							That the Demand under the head " + DM.DemandName + @"
							(Pages....................) be reduced to Rs.100(To Discuss) <b> " + DM.Subject + @"</b>
						</li>					
					<p style='text-align:center'>Demand No....................</p>					
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs.100(To Discuss) 
						</li>					
					<p style='text-align:center'>Demand No....................</p>					
						<li>
							That the Demand under the head........................
							(Pages....................) be reduced to Rs.100(To Discuss) 
						</li>
					</ol>
			</div>
			<br/>
			<br/>
			<div id='footer'>
				<div style='float:left'>
					To<br/>
					<br/>
					The Secretary,<br/>
					H.P Vidhan Sabha,<br/>
					Shimla-4.
				</div>
				<div style='float:right;text-align:right'>
					Yours Faithfully,<br/>	
	<br/>
					<br/>
<br/>
				<br/>
<br/>
					M.L.A.<br/>
					H.P.Vidhan Sabha<br/><br/>
					Division No " + DM.ConstituencyName + @" (" + DM.ConstituencyCode + @")<br/>
				</div>
			</div>			
		</div>
	</body>
</html>
";
                }

                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;
                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/OnlineMemberFile/" + DM.AssemblyID + "/" + DM.SessionID + "/";
                fileName = DM.AssemblyID + "_" + DM.SessionID + "_N_Org_" + DM.NoticeId + ".pdf";

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory1 = DM.SaveFilePath + url;
                if (!Directory.Exists(directory1))
                {
                    Directory.CreateDirectory(directory1);
                }
                var path1 = Path.Combine(directory1, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(path1, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);
                // close file stream
                _FileStream1.Close();

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);
                _FileStream.Close();

                return url + "," + fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }



        public JsonResult AssignNodal(string Starvals)
        {
            tMemberNotice Update = new tMemberNotice();
            tQuestion objModel = new tQuestion();
            objModel.QuestionValue = Starvals;
            objModel.Message = Helper.ExecuteService("Notice", "AssignNodalOffice", objModel) as string;
            //Update.Message = "Nodal Office created Successfully !";
            return Json(objModel.Message, JsonRequestBehavior.AllowGet);

        }

        public JsonResult DeleteNodal(string Starvals)
        {
            tMemberNotice Update = new tMemberNotice();
            tQuestion objModel = new tQuestion();
            objModel.QuestionValue = Starvals;
            objModel.Message = Helper.ExecuteService("Notice", "DeleteNodalOffice", objModel) as string;
            //Update.Message = "Nodal Office created Successfully !";
            return Json(objModel.Message, JsonRequestBehavior.AllowGet);

        }



        public int GetSpentCountSMSByMember()
        {
            int SentSmsCount = 0;
            int CurrentMonth = DateTime.Today.Month;

            string FirstDay = GetFirstDayOfMonth(CurrentMonth).ToShortDateString();
            // get day of week for first day

            string[] dateParts = FirstDay.Split('/');

            DateTime dtFirstTemp = new

                DateTime(Convert.ToInt32(dateParts[2]),

                Convert.ToInt32(dateParts[0]),

                Convert.ToInt32(dateParts[1]));

            string LastDay = GetLastDayOfMonth(CurrentMonth).ToShortDateString();

            // get day of week for last day

            string[] dateParts2 = LastDay.Split('/');

            DateTime dtLastTemp = new

                DateTime(Convert.ToInt32(dateParts2[2]),

                Convert.ToInt32(dateParts2[0]),

                Convert.ToInt32(dateParts2[1]));

            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@from", dtFirstTemp.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@to", dtLastTemp.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", "1"));

            if (CurrentSession.IsMember.ToUpper() == "FALSE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login
            {
                methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.MemberUserID));
            }
            else
            {
                //methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.AadharId));
                methodParameter.Add(new KeyValuePair<string, string>("@UserID", (!string.IsNullOrEmpty(CurrentSession.AadharId) ? CurrentSession.AadharId : "999999999999")));
            }
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "getAllCountSMSByUserID", methodParameter);

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {

                SentSmsCount = Convert.ToInt32(Convert.ToString(dataSet.Tables[0].Rows[0][0]));
            }


            return SentSmsCount;
        }


        public int GetMaxLimitCountSMSByMember()
        {
            mMember model = new mMember();
            if (CurrentSession.IsMember.ToUpper() == "FALSE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login and admin
            {
                model.AadhaarNo = CurrentSession.AadharId;
                CurrentSession.IsMember = "true";
                model.isAlive = Convert.ToBoolean(CurrentSession.IsMember);
            }
            else
            {

                if (string.IsNullOrEmpty(CurrentSession.MemberUserID) == true)
                {
                    model.AadhaarNo = CurrentSession.AadharId;
                }
                else
                {
                    model.AadhaarNo = CurrentSession.MemberUserID;
                }


                //model.AadhaarNo = (!string.IsNullOrEmpty(CurrentSession.MemberUserID) ? CurrentSession.MemberUserID : "999999999999");

                model.isAlive = (!string.IsNullOrEmpty(CurrentSession.IsMember) ? Convert.ToBoolean(CurrentSession.IsMember) : false);
                //model.isAlive = Convert.ToBoolean(CurrentSession.IsMember);

            }


            model.DistrictID = DateTime.Today.Month;
            var MaxLimityCount = (int)Helper.ExecuteService("ContactGroups", "GetMAXLimitByMember", model);

            var ExtendMaxLimityCount = 0;
            if (CurrentSession.IsMember.ToUpper() == "TRUE")
            {
                model.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
                ExtendMaxLimityCount = (int)Helper.ExecuteService("ContactGroups", "GetExtendSMSLimitByMember", model);
            }
            else { ExtendMaxLimityCount = 0; }

            var total = MaxLimityCount + ExtendMaxLimityCount;

            if (CurrentSession.IsMember.ToUpper() == "TRUE" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login
            {
                CurrentSession.IsMember = "false";
            }
            return total;
        }

        static int GetFileRandomNo()
        {
            Random ran = new Random();
            int rno = ran.Next(1, 99999);
            return rno;
        }
        public FileStreamResult PrintDetailedDiaryPdf(int DiaryId)
        {
            string msg = string.Empty;

            MlaDiary model = new MlaDiary();
            mdiaryno mdl1 = new mdiaryno();
            var resDiaryNo = (List<MlaDiary>)Helper.ExecuteService("Diary", "get_CommonDiaryNo", DiaryId);
            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDairyListForPdf", DiaryId);
            MemoryStream output = new MemoryStream();
            string DiariesRefNo = "";
            string documentCC = "";
            if (resDiaryNo.ToList() != null)
            {


                foreach (var item in resDiaryNo.ToList())
                {
                    // for (int i = 0; i < resDiaryNo.ToList().Count; i++)
                    // {
                    DiariesRefNo += item.RecordNumber + "/" + Convert.ToDateTime(item.DiaryDate).Year + " & ";
                    documentCC += item.DocumentTo + " & "; ;
                    // }
                }

            }
            if (model.MlaDiaryList.Count > 0)
            {

                string isCCItemExist = "";
                //  member.ase = Convert.ToInt32(CurrentSession.MemberCode);
                mMlaSignature obj = new mMlaSignature();
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj.MlaCode = CurrentSession.MemberCode;
                //  obj = (mMlaSignature)Helper.ExecuteService("Diary", "GetDiarySignatureData", obj);
                // if (obj != null)
                // {
                //PdfConverter pdfConverter = new PdfConverter();
                //  pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                int crntyear = DateTime.Now.Year;
                EvoPdf.Document document1 = new EvoPdf.Document();
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                //  document1.Templates.AddNewTemplate(
                PdfPage page = document1.Pages.AddNewPage(PdfPageSize.Legal, new Margins(0, 0, 40, 40),
                PdfPageOrientation.Portrait);

                string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";
                foreach (MlaDiary diary in model.MlaDiaryList)
                {

                    string frwDate = "";
                    isCCItemExist = diary.DocumentCC;
                    if (diary.ForwardDate != null)
                    { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                    //if (diary.DiaryLanguage == 1)
                    //{
                    //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                    outXml += @"<style>table { }table, th{border:0px solid white;font-size:21px;}table, td{}</style>";
                    outXml += @"<center><h3>" + "" + "</h3></center>";
                    outXml += @"<table cellpadding='4' style='width:100%;font-size:18px;margin-left: 55px;'>";
                    outXml += @"<thead>";
                    outXml += @"<tr>";
                    // outXml += @"<th><br></th></tr>";
                    //outXml += @"<tr><th style='margin-top:30px'></th>";
                    outXml += @"</tr><thead><tbody><br><br><br><br><br><br><br><br>";
                    outXml += "";

                    outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + "Subject: " + diary.Subject + "</td></tr><br>";
                    outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr><br>";
                    if (DiariesRefNo != "")
                    {
                        string diarysNo = DiariesRefNo.Trim().Substring(0, DiariesRefNo.Length - 2);
                        outXml += @"<tr><td colspan='2' style='text-align:justify;' ><hr style='border-width: 1px; border-color:grey;' /></td></tr><br>";
                        outXml += @"<tr><td colspan='2' style='text-align:left;'><span style='color:blue'>Copy To : </span> " + documentCC.Substring(0, documentCC.Length - 2) + " - Vide Refrence Number : <strong>" + diarysNo + "</strong></td></tr> ";
                        outXml += @"<tr><td colspan='2' style='text-align:right;'><span style='color:blue'>Dated : </span<strong>" + Convert.ToDateTime(diary.DiaryDate).ToString("dd/MM/yyyy") + " <br>" + "</strong></td></tr><br>";
                    }
                    //outXml += @"<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                    //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + " <br><br>" + "</strong></td></tr>";
                    //outXml += @"<tr><td colspan='2' style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2 + ",<br>" + obj.SignaturePin2 + "<br></strong></td></tr>";
                    //outXml += @"<tr><td colspan='2' style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                    //outXml += @"<tr><td style='text-align:left;' >" + obj.Text1 + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate + ":" + frwDate + "<br></td></tr>";

                    //outXml += @"<tr><td colspan='2 style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";

                    //outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + "<br><br>" + "</strong></td></tr>";
                    outXml += "";
                    // }
                    //else if (diary.DiaryLanguage == 2)
                    //{
                    //    outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                    //    outXml += "<center><h3>" + "" + "</h3></center>";
                    //    outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                    //    outXml += "<thead>";
                    //    outXml += "<tr>";
                    //    //outXml += "<th><br></th></tr>";
                    //    //outXml += "<tr><th style='margin-top:30px'></th>";
                    //    outXml += "</tr><thead><tbody><br><br><br><br><br><br>";
                    //    outXml += "";

                    //    //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                    //    outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                    //    //outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                    //    //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";

                    //    //outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                    //    //outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                    //    //outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

                    //    //outXml += "<tr><td colspan='2' style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";

                    //    //outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                    //    outXml += "";
                    //}

                }

                outXml += @"</tbody>";

                outXml += @"</div></div></body></html>";

                string htmlStringToConvert2 = outXml;

                HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

                AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;

                string url = "/mlaDiary/" + "DiarydDetailPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(Server.MapPath("~" + url), model.MlaCode + "_" + model.RecordNumber + ".pdf");

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();
                // }
            }
            // }

            return new FileStreamResult(output, "application/pdf");
        }


        //
        public ActionResult ActivityMasterPartial(int MemberCode)
        {
            //List<ActivitySearchzResult> list = (List<ActivitySearchzResult>)Helper.ExecuteService("Member", "getMemberActivityList", null);

            //var model = new ActivitySearchzResult()
            //{
            var ActivityDataList = (List<ActivitySearchzResult>)Helper.ExecuteService("Member", "getMemberActivityList", MemberCode);
            var Actdrp = (List<ActivityResultDrp>)Helper.ExecuteService("Member", "GetDropDownData", null);

            ActivitySearchzResult model = new ActivitySearchzResult();
            model.ActivityImagesList = (List<ActivitySearchzResult>)Helper.ExecuteService("Member", "getActivityImagesList", null);
            model.ActivityDataList = ActivityDataList;
            model.Actdrp = Actdrp;
            model.Actdrp.Insert(0, new ActivityResultDrp { ActivityName_Local = "-----Select-----" });

            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            model.FileName = Acess.SettingValue;

            //ViewBag.Organisations = new SelectList(Res, "Id", "Organisation");
            return PartialView("_ActivityMaster", model);
        }

        public ActionResult getDefaultresultActivity(string from, string to, int Id, int MemberCode)
        {
            string[] param = new string[4];
            param[0] = from;
            param[1] = to;
            param[2] = Id.ToString();
            param[3] = MemberCode.ToString();
            var ActivityDataList = (List<ActivitySearchzResult>)Helper.ExecuteService("Member", "getMemberActivityListByCategory", param);
            var ActivityDataListAll = (List<ActivitySearchzResult>)Helper.ExecuteService("Member", "getMemberActivityListByCategoryAll", param);
            //;
            ActivitySearchzResult model = new ActivitySearchzResult();
            model.ActivityImagesList = (List<ActivitySearchzResult>)Helper.ExecuteService("Member", "getActivityImagesList", null);
            if (Id != 0)
            {
                model.ActivityDataList = ActivityDataList;
            }
            else
            {
                model.ActivityDataList = ActivityDataListAll;
            }
            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            model.FileName = Acess.SettingValue;

            return PartialView("_searchResult", model);
        }

        public ActionResult Slider(int EventId)
        {
            ActivitySearchzResult model = new ActivitySearchzResult();
            var ActivityImagesList = (List<ActivitySearchzResult>)Helper.ExecuteService("Member", "getActivityImagesListById", EventId);
            model.ActivityImagesList = ActivityImagesList;
            var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            model.FileName = Acess.SettingValue;
            return PartialView(model);
        }


        [HttpGet]
        public FileStreamResult GeneratePdfByOfficeid(string Officeid, string OfficeName, string ActionTypeCode, string DocumenttypeSelected,
            string ConstituencyId, bool WithDetails)
        {
            string outXml = "";
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }
            string savedPDF = string.Empty;
            MemoryStream output = new MemoryStream();

            /////////////////////////////////////
            MlaDiary mdl = new MlaDiary();
            //  string ConstituencyId = "14";
            if (Officeid == "")
            {
                Officeid = null;
            }
            if (ActionTypeCode == "")
            {
                ActionTypeCode = null;
            }

            if (DocumenttypeSelected == "0")
            {
                DocumenttypeSelected = null;
            }

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@ActionType", ActionTypeCode));
            methodParameter.Add(new KeyValuePair<string, string>("@DocumentType", DocumenttypeSelected));
            methodParameter.Add(new KeyValuePair<string, string>("@OfficeId", Officeid));
            methodParameter.Add(new KeyValuePair<string, string>("@ConstituencyCode", ConstituencyId));
            System.Data.DataSet ds = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "ReportForMlaDairy", methodParameter);


            if (ds != null)
            {
                outXml = @"<html>   
                     
                                 <body style='font-family:DVOT-Yogesh;color:black;'> 
                                <div>
                                <div>
                                <table>";

                string OfficeNameIs = "";
                if (OfficeName != "")
                {
                    OfficeNameIs = "(" + OfficeName + ")";
                }

                outXml += @"<tr>
                                     <td style='text-align: center;font-weight:bold'>               
                                     Councillor Diary Status Report" + " " + OfficeNameIs + @"    
                                     </td>                                   
                                     </tr>";
                outXml += "</table></div></div> </body> </html>";



                outXml += @"<html>  
<head>
<style>
table {
  width:100%;
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
font-size:20px;
}
th, td {
  padding: 5px;
  text-align: left;
font-size:20px;
}

}


</style>
</head>                         
                                       <body style='font-family:DVOT-Surekh;'> 
                                       <div>
                                       <div style='width: 100%;'>
                                       <table style='width: 100%;'>";
                outXml += @"<tr>

                                     <td style='text-align: center;font-size:18px;padding:10px;border: 1px solid;'>               
                                     Sr.No." + @"    
                                     </td> 
                                    <td style='text-align: center;padding:10px;border: 1px solid; '>               
                                     Diary No." + @"    
                                     </td> 
                                    <td style='text-align: center;padding:10px;border: 1px solid;'>               
                                     Forwarded Date" + @"    
                                     </td>                                     
                                    <td style='text-align: center;padding:10px;border: 1px solid;'>               
                                     Subject" + @"    
                                     </td> ";
                if (DocumenttypeSelected == "3")
                {
                    outXml += @"
                                    <td style='text-align: center;padding:10px;border: 1px solid;'>               
                                     Financial Year & Amount" + @"    
                                     </td>";

                }
                if (WithDetails == true)
                {
                    outXml += @"
                                    <td style='text-align: center;padding:10px;border: 1px solid;'>               
                                     Details" + @"    
                                     </td>";

                }



                outXml += @"                                        
                                   <td style='text-align: center;padding:10px;border: 1px solid; '>               
                                     Revieved From" + @"    
                                     </td> 
                                    <td style='text-align: center;padding:10px;border: 1px solid;'>               
                                     Mobile" + @"    
                                     </td> 
                                    <td style='text-align: center;padding:10px;border: 1px solid;'>               
                                     Status" + @"    
                                     </td>                                 
                                    <td style='text-align: center;;padding:10px;border: 1px solid;'>               
                                     Action details" + @"    
                                     </td> </tr>                                     
                                    ";


                int count = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    MlaDiary objPerson = new MlaDiary();

                    count++;
                    outXml += @"<tr style='vertical-align:text-top;'><td style='text-align: left; font-size: 14px; border: 1px solid;'>                                    
                                      " + count + @"";


                    string DiaryNo = Convert.IsDBNull(ds.Tables[0].Rows[i]["Diary Number"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Diary Number"]);
                    outXml += @"<td style='text-align: left;padding:10px; font-size: 18px; border: 1px solid;'>   
                                    
                                      " + DiaryNo + @"";

                    string ForwardedDate = Convert.IsDBNull(ds.Tables[0].Rows[i]["ForwardDate"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["ForwardDate"]);
                    outXml += @"<td style='text-align: left;padding:10px; font-size: 18px; border: 1px solid;'>   
                                    
                                      " + ForwardedDate + @"";
                    string Subject = Convert.IsDBNull(ds.Tables[0].Rows[i]["Subject"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Subject"]);
                    outXml += @"<td style='text-align: left;padding:10px; font-size: 18px; border: 1px solid;'>   
                                    
                                      " + Subject + @"";

                    if (DocumenttypeSelected == "3")
                    {

                        string FinYear = Convert.ToString(ds.Tables[0].Rows[i]["FinancialYear"]);
                        string Amount = Convert.ToString(ds.Tables[0].Rows[i]["Amount"]);
                        string FinAmt = FinYear + "<br>" + "₹ " + Amount;
                        outXml += @"<td style='text-align: left;padding:10px; font-size: 18px; border: 1px solid;'>   
                                    
                                      " + FinAmt + @"";



                    }
                    if (WithDetails == true)
                    {

                        string Details = Convert.IsDBNull(ds.Tables[0].Rows[i]["Details"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Details"]);

                        outXml += @"<td style='text-align: left;padding:10px; font-size: 18px; border: 1px solid;'>   
                                    
                                      " + Details.Replace("&nbsp;", "") + @"";

                    }


                    string RecieveFrom = Convert.IsDBNull(ds.Tables[0].Rows[i]["RecieveFrom"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["RecieveFrom"]);
                    outXml += @"<td style='text-align: left;padding:10px; font-size: 18px; border: 1px solid;'>   
                                      " + RecieveFrom + @"";

                    string ApplicantMobile = Convert.IsDBNull(ds.Tables[0].Rows[i]["MobileNumber"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["MobileNumber"]);
                    outXml += @"<td style='text-align: left;padding:10px; font-size: 18px; border: 1px solid;'>   
                                    
                                      " + ApplicantMobile + @"";


                    string Status = Convert.IsDBNull(ds.Tables[0].Rows[i]["Status"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Status"]);
                    outXml += @"<td style='text-align: left;padding:10px; font-size: 18px; border: 1px solid;'>   
                                    
                                      " + Status + @"";

                    string ActionDetail = Convert.IsDBNull(ds.Tables[0].Rows[i]["ActionDetail"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["ActionDetail"]);
                    outXml += @"<td style='text-align: left;padding:10px; font-size: 18px; border: 1px solid;'>   
                                    
                                      " + ActionDetail + @"";



                }

                outXml += "</table></div></div> </body> </html>";
                EvoPdf.Document document1 = new EvoPdf.Document();
                AddElementResult addResult;
                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(40, 45, 30, 20);
                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(10, 45, 65, 55), PdfPageOrientation.Landscape);
                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(40, 10, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    document1.Close();
                }
                ///To get File Path                
                string fileName = "";


                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=eFileList.pdf");
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string directory = FileSettings.SettingValue + "/MLADiary/" + "PdfByOfficeId" + "/";
                DirectoryInfo Dir = new DirectoryInfo(directory);
                if (!Dir.Exists)
                {
                    Dir.Create();
                }
                fileName = "PdfbyOfficeId.pdf";
                var path = Path.Combine(directory, fileName);
                string dbpath = "/MLADiary/" + "PdfByOfficeId" + "/";
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();


                var filepath = Path.Combine(Server.MapPath(dbpath));
                // Get the complete folder path and store the file inside it.  
                DirectoryInfo Dir1 = new DirectoryInfo(filepath);
                if (!Dir1.Exists)
                {
                    Dir1.Create();
                }
                string fname = Path.Combine(filepath, fileName);
                System.IO.FileStream _FileStream1 = new System.IO.FileStream(fname, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream1.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream1.Close();
            }


            return File(output, "application/pdf");
        }
        public ActionResult DownloadPDF(string fileName)
        {
            if (CurrentSession.UserID == null || CurrentSession.UserID == "") { RedirectToAction("LoginUP", "Account"); }

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/MLADiary/" + "PdfByOfficeId" + "/";
            string directory = System.IO.Path.Combine(FileSettings.SettingValue + url);
            string path = System.IO.Path.Combine(directory, "PdfbyOfficeId.pdf");
            return File(path, "application/pdf");
        }


    }

}