﻿using Newtonsoft.Json;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.ComplexModel.Constituency;
using SBL.DomainModel.Models.ConstituencyVS;
using SBL.DomainModel.Models.Grievance;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Constituency.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.eFile;
using EvoPdf;
using iTextSharp.text;
using System.Linq;
using SBL.DomainModel.Models.Assembly;
using System.Collections.Generic;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.ComplexModel.Conzstituency;
using SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.SiteSetting;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    public class WorksController : Controller
    {
        public string myConsituency = CurrentSession.ConstituencyID;

        public string MemberCode = CurrentSession.MemberCode;
        public string currentUser = CurrentSession.UserID;
        public string aadharID = CurrentSession.AadharId;
        public string currentAssemby = CurrentSession.AssemblyId;

        //
        // GET: /Admin/Works/
        
        public ActionResult Index()
        {
            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                DiaryModel DMdl = new DiaryModel();
                PaperLaidSummaryViewModel model = new PaperLaidSummaryViewModel();

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }
                DMdl.AskedBy = CurrentSession.UserID;
                model.mAssemblyList = (List<mAssembly>)Helper.ExecuteService("LegislationFixation", "GetAssemblyList", null);
                model.sessionList = (List<mSession>)Helper.ExecuteService("LegislationFixation", "GetSessionLsitbyAssemblyId", DMdl);
                model.PendingNoticesAssignCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeAssignPendingCount", DMdl);
                model.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCount", DMdl);
                // model.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCountLegislatin", DMdl);
                model.SentNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeSentCount", DMdl);
                model.RecievedNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperRecievedCount", DMdl);
                model.CurrentLobNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperCurrentLobCount", DMdl);
                model.LaidInHomeNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperLaidInHomeCount", DMdl);
                ////pending to lay Notice
                model.PendingToLayNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperPendingToLayListCount", DMdl);

                model.RecievedBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperRecievedCount", DMdl);
                model.CurrentLobBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperCurrentLobCount", DMdl);
                model.LaidInHomeBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperLaidInHomeCount", DMdl);
                ////Pending to lay Bills
                model.PendingToLayBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperPendingToLayListCount", DMdl);

                model.RecievedCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperRecievedCount", DMdl);
                model.CurrentLobCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperCurrentLobCount", DMdl);
                model.LaidInHomeCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperLaidInHomeCount", DMdl);

                ////pending to lay committee
                model.PendingToLayCommitteeCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperPendingToLayListCount", DMdl);

                model.PendingStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPendingCount", DMdl);
                model.SentStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionSentCount", DMdl);
                model.RecievedStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperRecievedCount", DMdl);
                model.CurrentLobStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperCurrentLobCount", DMdl);
                model.LaidInHomeStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperLaidInHomeCount", DMdl);

                ////starred Question pending to lay
                model.PendingToLayStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperPendingToLayListCount", DMdl);

                model.PendingUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPendingCount", DMdl);
                model.SentUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionSentCount", DMdl);
                model.RecievedUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperRecievedCount", DMdl);
                model.CurrentLobUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperCurrentLobCount", DMdl);
                model.LaidInHomeUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperLaidInHomeCount", DMdl);
                ////pending to lay Unstarred
                model.PendingToLayUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperPendingToLayListCount", DMdl);


                model.RecievedPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayRecievedCount", DMdl);
                model.CurrentLobPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayCurrentLobCount", DMdl);
                model.LaidInHomePaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayLaidInHomeCount", DMdl);
                ////Pending to lay
                model.PendingToLayOtherPaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetOtherPaperPendingToLayListCount", DMdl);

                //// Fixed and UnFixed Question Count For Starred And UnStarred Question type Created by Sunil on 02-July-2014  
                model.Starredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredFixedQuesCount", DMdl);
                model.StarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredUnFixedQuesCount", DMdl);
                model.Unstarredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredFixedQuesCount", DMdl);
                model.UnstarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredUnFixedQuesCount", DMdl);

                model.StarredCountForWithdrawn = (int)Helper.ExecuteService("LegislationFixation", "GetStarredforWithdrawnCount", DMdl);
                model.StarredWithdrawnCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredListWithdrawnCount", DMdl);
                model.UnstarredCountForWithdrawn = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredforWithdrawnCount", DMdl);
                model.UntarredWithdrawnCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredListWithdrawnCount", DMdl);

                tPaperLaidV mdl = new tPaperLaidV();
                SiteSettings siteSettingMod = new SiteSettings();
                mSession Mdl = new mSession();
                mAssembly assmblyMdl = new mAssembly();
                tMemberNotice model1 = new tMemberNotice();
                CutMotionModel Noticecoiunt = new CutMotionModel();
                if (DMdl.AssemblyID != 0 && DMdl.SessionID != 0)
                {
                    mdl.AssemblyCode = DMdl.AssemblyID;
                    mdl.SessionCode = DMdl.SessionID;

                    //Add Value for Session object
                    Mdl.SessionCode = DMdl.SessionID;
                    Mdl.AssemblyID = DMdl.AssemblyID;
                    //Add Value for Assembly Object 
                    assmblyMdl.AssemblyID = DMdl.AssemblyID;
                    // Add Value for tMemberNoticec Object
                    model1.AssemblyCode = DMdl.AssemblyID;
                    model1.SessionID = DMdl.SessionID;
                    model.AssemblyID = DMdl.AssemblyID;
                    model.SessionID = DMdl.SessionID;

                }
                else
                {
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    mdl.SessionCode = siteSettingMod.SessionCode;
                    mdl.AssemblyCode = siteSettingMod.AssemblyCode;
                    //Add Value for Session object
                    Mdl.SessionCode = siteSettingMod.SessionCode;
                    Mdl.AssemblyID = siteSettingMod.AssemblyCode;
                    //Add Value for Assembly Object 
                    assmblyMdl.AssemblyID = siteSettingMod.AssemblyCode;
                    // Add Value for tMemberNoticec Object
                    model1.AssemblyCode = siteSettingMod.AssemblyCode;
                    model1.SessionID = siteSettingMod.SessionCode;
                    model.AssemblyID = siteSettingMod.AssemblyCode;
                    model.SessionID = siteSettingMod.SessionCode;
                    model.AccessFilePath = siteSettingMod.AccessFilePath;
                    CurrentSession.AssemblyId = siteSettingMod.AssemblyCode.ToString();
                    CurrentSession.SessionId = siteSettingMod.SessionCode.ToString();
                    CurrentSession.FileAccessPath = siteSettingMod.AccessFilePath;

                }

                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
                mdl = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "GetUpdatedOtherPaperLaidCounters", mdl);
                if (mdl != null)
                {
                    model.TotalUpdatedFileCounter = mdl.TotalOtherpaperLaidUpdated;

                }
                mDepartment deptInfo = new mDepartment();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID))
                {
                    deptInfo.deptId = CurrentSession.DeptID;
                    deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
                }

                if (deptInfo != null)
                {
                    model.DepartmentName = deptInfo.deptname;
                }

                model.CurrentUserName = CurrentSession.UserName;
                model.UserDesignation = CurrentSession.MemberDesignation;
                model.UserName = CurrentSession.Name;
                // added by Sameer
                // Get the Total count of All Type of question.
                model1.QuestionTypeId = (int)QuestionType.StartedQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForProofReader", model1);
                model.TotalStaredReceived = model1.TotalStaredReceived;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForClubbed", model1);
                model.TotalStaredReceivedCl = model1.TotalStaredReceivedCl;
                model.TotalSBracktedQuestion = model1.TotalSBracktedQuestion;
                model.TotalInActiveQuestion = model1.TotalInActiveQuestion;
                model.TotalClubbedQuestion = model1.TotalClubbedQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForUnStaredClubbed", model1);
                model.TotalUnStaredReceivedCl = model1.TotalUnStaredReceivedCl;
                model.TotalUBractedQuestion = model1.TotalUBractedQuestion;
                model.UnstaredTotalClubbedQuestion = model1.UnstaredTotalClubbedQuestion;
                model1.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForLegisUnstartQuestion", model1);
                model.TotalUnStaredReceived = model1.TotalUnStaredReceived;
                int c = model.TotalStaredReceived + model.TotalUnStaredReceived;

                model.TotalValue = c;
                int d = model.TotalStaredReceivedCl + model.TotalClubbedQuestion + model.TotalInActiveQuestion;
                model.TotalValueCl = d;

                model.StarredCountExess = (int)Helper.ExecuteService("LegislationFixation", "GetCountQuestionsForExcess", DMdl);
                model.StarredCountExessed = (int)Helper.ExecuteService("LegislationFixation", "GetCountExcessedQuestions", DMdl);

                model.UnStarredCountExess = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredCountQuestionsForExcess", DMdl);
                model.UnStarredCountExessed = (int)Helper.ExecuteService("LegislationFixation", "GetCountUnStarredExcessedQuestions", DMdl);

                // Add Code for Rejected and StarredToUnstarred Count By type Created by Sunil on 28-July-2014 

                model.StarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredRejectedCount", DMdl);
                model.UnstarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredRejectedCount", DMdl);
                model.StarredToUnstarredCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredToUnstarredCount", DMdl);
                model.ChangedRuleNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetChangedRuleNoticeCount", DMdl);
                Noticecoiunt.AssemblyID = model.AssemblyID;
                Noticecoiunt.SessionID = model.SessionID;
                Noticecoiunt = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesForClubWithBracket", Noticecoiunt);
                model.CountForClub = Noticecoiunt.ResultCount;
                Noticecoiunt = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesClubbed", Noticecoiunt);
                model.CountForClubbed = Noticecoiunt.Noticeclubbed;

                model.CustomMessage = (string)TempData["CallingMethod"];
                if ((string)TempData["CallingMethod"] == "StarredPending")
                {
                    ViewBag.TagID = "liStarred";
                }
                else if ((string)TempData["CallingMethod"] == "UnStarredPending")
                {
                    ViewBag.TagID = "liUnstarred";
                }
                else if ((string)TempData["CallingMethod"] == "NoticePending")
                {
                    ViewBag.TagID = "liNotice";
                }

                //add code for house committee count by robin - 23-june2017
                #region For received list count
                string DepartmentId = CurrentSession.DeptID;
                string Officecode = CurrentSession.OfficeId;
                string AadharId = CurrentSession.AadharId;
                string currtBId = CurrentSession.BranchId;
                string Year = "5";
                string papers = "1";
                string[] strngBIDN = new string[2];
                strngBIDN[0] = CurrentSession.BranchId;
                var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
                string[] str = new string[6];
                str[0] = DepartmentId;
                str[1] = Officecode;
                str[2] = AadharId;
                str[3] = value123[0];
                str[4] = Year;
                str[5] = papers;
                var ReceivedList = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
                model.ReceivedListCount = ReceivedList.Count;
                #endregion

                #region For Draft List Count
                string[] strD = new string[6];
                strD[0] = DepartmentId;
                strD[1] = Officecode;
                strD[2] = AadharId;
                strD[3] = currtBId;
                strD[4] = CurrentSession.Designation;
                strD[5] = "1";
                var DraftListCount = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
                model.DraftListCount = DraftListCount.Count;
                #endregion

                #region For Send List Count
                string[] strS = new string[6];
                strS[0] = DepartmentId;
                strS[1] = Officecode;
                strS[2] = AadharId;
                strS[3] = currtBId;
                strS[4] = "5";
                strS[5] = "1";
                List<eFileAttachment> ListModelForSend = new List<eFileAttachment>();
                ListModelForSend = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendList", strS);
                model.SentPaperListCount = ListModelForSend.Count;
                #endregion

                Session["Model"] = model;

                return View();
            }

            else
            {
                return RedirectToAction("Login", "Account", new { @area = "" });
            }
            
        }
        public ActionResult UpdateWorks()
        {

            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                DiaryModel DMdl = new DiaryModel();
                PaperLaidSummaryViewModel model = new PaperLaidSummaryViewModel();

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }
                DMdl.AskedBy = CurrentSession.UserID;
                model.mAssemblyList = (List<mAssembly>)Helper.ExecuteService("LegislationFixation", "GetAssemblyList", null);
                model.sessionList = (List<mSession>)Helper.ExecuteService("LegislationFixation", "GetSessionLsitbyAssemblyId", DMdl);
                model.PendingNoticesAssignCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeAssignPendingCount", DMdl);
                model.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCount", DMdl);
                // model.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCountLegislatin", DMdl);
                model.SentNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeSentCount", DMdl);
                model.RecievedNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperRecievedCount", DMdl);
                model.CurrentLobNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperCurrentLobCount", DMdl);
                model.LaidInHomeNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperLaidInHomeCount", DMdl);
                ////pending to lay Notice
                model.PendingToLayNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperPendingToLayListCount", DMdl);

                model.RecievedBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperRecievedCount", DMdl);
                model.CurrentLobBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperCurrentLobCount", DMdl);
                model.LaidInHomeBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperLaidInHomeCount", DMdl);
                ////Pending to lay Bills
                model.PendingToLayBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperPendingToLayListCount", DMdl);

                model.RecievedCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperRecievedCount", DMdl);
                model.CurrentLobCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperCurrentLobCount", DMdl);
                model.LaidInHomeCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperLaidInHomeCount", DMdl);

                ////pending to lay committee
                model.PendingToLayCommitteeCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperPendingToLayListCount", DMdl);

                model.PendingStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPendingCount", DMdl);
                model.SentStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionSentCount", DMdl);
                model.RecievedStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperRecievedCount", DMdl);
                model.CurrentLobStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperCurrentLobCount", DMdl);
                model.LaidInHomeStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperLaidInHomeCount", DMdl);

                ////starred Question pending to lay
                model.PendingToLayStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperPendingToLayListCount", DMdl);

                model.PendingUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPendingCount", DMdl);
                model.SentUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionSentCount", DMdl);
                model.RecievedUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperRecievedCount", DMdl);
                model.CurrentLobUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperCurrentLobCount", DMdl);
                model.LaidInHomeUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperLaidInHomeCount", DMdl);
                ////pending to lay Unstarred
                model.PendingToLayUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperPendingToLayListCount", DMdl);


                model.RecievedPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayRecievedCount", DMdl);
                model.CurrentLobPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayCurrentLobCount", DMdl);
                model.LaidInHomePaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayLaidInHomeCount", DMdl);
                ////Pending to lay
                model.PendingToLayOtherPaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetOtherPaperPendingToLayListCount", DMdl);

                //// Fixed and UnFixed Question Count For Starred And UnStarred Question type Created by Sunil on 02-July-2014  
                model.Starredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredFixedQuesCount", DMdl);
                model.StarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredUnFixedQuesCount", DMdl);
                model.Unstarredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredFixedQuesCount", DMdl);
                model.UnstarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredUnFixedQuesCount", DMdl);

                model.StarredCountForWithdrawn = (int)Helper.ExecuteService("LegislationFixation", "GetStarredforWithdrawnCount", DMdl);
                model.StarredWithdrawnCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredListWithdrawnCount", DMdl);
                model.UnstarredCountForWithdrawn = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredforWithdrawnCount", DMdl);
                model.UntarredWithdrawnCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredListWithdrawnCount", DMdl);

                tPaperLaidV mdl = new tPaperLaidV();
                SiteSettings siteSettingMod = new SiteSettings();
                mSession Mdl = new mSession();
                mAssembly assmblyMdl = new mAssembly();
                tMemberNotice model1 = new tMemberNotice();
                CutMotionModel Noticecoiunt = new CutMotionModel();
                if (DMdl.AssemblyID != 0 && DMdl.SessionID != 0)
                {
                    mdl.AssemblyCode = DMdl.AssemblyID;
                    mdl.SessionCode = DMdl.SessionID;

                    //Add Value for Session object
                    Mdl.SessionCode = DMdl.SessionID;
                    Mdl.AssemblyID = DMdl.AssemblyID;
                    //Add Value for Assembly Object 
                    assmblyMdl.AssemblyID = DMdl.AssemblyID;
                    // Add Value for tMemberNoticec Object
                    model1.AssemblyCode = DMdl.AssemblyID;
                    model1.SessionID = DMdl.SessionID;
                    model.AssemblyID = DMdl.AssemblyID;
                    model.SessionID = DMdl.SessionID;

                }
                else
                {
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    mdl.SessionCode = siteSettingMod.SessionCode;
                    mdl.AssemblyCode = siteSettingMod.AssemblyCode;
                    //Add Value for Session object
                    Mdl.SessionCode = siteSettingMod.SessionCode;
                    Mdl.AssemblyID = siteSettingMod.AssemblyCode;
                    //Add Value for Assembly Object 
                    assmblyMdl.AssemblyID = siteSettingMod.AssemblyCode;
                    // Add Value for tMemberNoticec Object
                    model1.AssemblyCode = siteSettingMod.AssemblyCode;
                    model1.SessionID = siteSettingMod.SessionCode;
                    model.AssemblyID = siteSettingMod.AssemblyCode;
                    model.SessionID = siteSettingMod.SessionCode;
                    model.AccessFilePath = siteSettingMod.AccessFilePath;
                    CurrentSession.AssemblyId = siteSettingMod.AssemblyCode.ToString();
                    CurrentSession.SessionId = siteSettingMod.SessionCode.ToString();
                    CurrentSession.FileAccessPath = siteSettingMod.AccessFilePath;

                }

                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
                mdl = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "GetUpdatedOtherPaperLaidCounters", mdl);
                if (mdl != null)
                {
                    model.TotalUpdatedFileCounter = mdl.TotalOtherpaperLaidUpdated;

                }
                mDepartment deptInfo = new mDepartment();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID))
                {
                    deptInfo.deptId = CurrentSession.DeptID;
                    deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
                }

                if (deptInfo != null)
                {
                    model.DepartmentName = deptInfo.deptname;
                }

                model.CurrentUserName = CurrentSession.UserName;
                model.UserDesignation = CurrentSession.MemberDesignation;
                model.UserName = CurrentSession.Name;
                // added by Sameer
                // Get the Total count of All Type of question.
                model1.QuestionTypeId = (int)QuestionType.StartedQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForProofReader", model1);
                model.TotalStaredReceived = model1.TotalStaredReceived;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForClubbed", model1);
                model.TotalStaredReceivedCl = model1.TotalStaredReceivedCl;
                model.TotalSBracktedQuestion = model1.TotalSBracktedQuestion;
                model.TotalInActiveQuestion = model1.TotalInActiveQuestion;
                model.TotalClubbedQuestion = model1.TotalClubbedQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForUnStaredClubbed", model1);
                model.TotalUnStaredReceivedCl = model1.TotalUnStaredReceivedCl;
                model.TotalUBractedQuestion = model1.TotalUBractedQuestion;
                model.UnstaredTotalClubbedQuestion = model1.UnstaredTotalClubbedQuestion;
                model1.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForLegisUnstartQuestion", model1);
                model.TotalUnStaredReceived = model1.TotalUnStaredReceived;
                int c = model.TotalStaredReceived + model.TotalUnStaredReceived;

                model.TotalValue = c;
                int d = model.TotalStaredReceivedCl + model.TotalClubbedQuestion + model.TotalInActiveQuestion;
                model.TotalValueCl = d;

                model.StarredCountExess = (int)Helper.ExecuteService("LegislationFixation", "GetCountQuestionsForExcess", DMdl);
                model.StarredCountExessed = (int)Helper.ExecuteService("LegislationFixation", "GetCountExcessedQuestions", DMdl);

                model.UnStarredCountExess = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredCountQuestionsForExcess", DMdl);
                model.UnStarredCountExessed = (int)Helper.ExecuteService("LegislationFixation", "GetCountUnStarredExcessedQuestions", DMdl);

                // Add Code for Rejected and StarredToUnstarred Count By type Created by Sunil on 28-July-2014 

                model.StarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredRejectedCount", DMdl);
                model.UnstarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredRejectedCount", DMdl);
                model.StarredToUnstarredCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredToUnstarredCount", DMdl);
                model.ChangedRuleNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetChangedRuleNoticeCount", DMdl);
                Noticecoiunt.AssemblyID = model.AssemblyID;
                Noticecoiunt.SessionID = model.SessionID;
                Noticecoiunt = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesForClubWithBracket", Noticecoiunt);
                model.CountForClub = Noticecoiunt.ResultCount;
                Noticecoiunt = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesClubbed", Noticecoiunt);
                model.CountForClubbed = Noticecoiunt.Noticeclubbed;

                model.CustomMessage = (string)TempData["CallingMethod"];
                if ((string)TempData["CallingMethod"] == "StarredPending")
                {
                    ViewBag.TagID = "liStarred";
                }
                else if ((string)TempData["CallingMethod"] == "UnStarredPending")
                {
                    ViewBag.TagID = "liUnstarred";
                }
                else if ((string)TempData["CallingMethod"] == "NoticePending")
                {
                    ViewBag.TagID = "liNotice";
                }

                //add code for house committee count by robin - 23-june2017
                #region For received list count
                string DepartmentId = CurrentSession.DeptID;
                string Officecode = CurrentSession.OfficeId;
                string AadharId = CurrentSession.AadharId;
                string currtBId = CurrentSession.BranchId;
                string Year = "5";
                string papers = "1";
                string[] strngBIDN = new string[2];
                strngBIDN[0] = CurrentSession.BranchId;
                var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
                string[] str = new string[6];
                str[0] = DepartmentId;
                str[1] = Officecode;
                str[2] = AadharId;
                str[3] = value123[0];
                str[4] = Year;
                str[5] = papers;
                var ReceivedList = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
                model.ReceivedListCount = ReceivedList.Count;
                #endregion

                #region For Draft List Count
                string[] strD = new string[6];
                strD[0] = DepartmentId;
                strD[1] = Officecode;
                strD[2] = AadharId;
                strD[3] = currtBId;
                strD[4] = CurrentSession.Designation;
                strD[5] = "1";
                var DraftListCount = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
                model.DraftListCount = DraftListCount.Count;
                #endregion

                #region For Send List Count
                string[] strS = new string[6];
                strS[0] = DepartmentId;
                strS[1] = Officecode;
                strS[2] = AadharId;
                strS[3] = currtBId;
                strS[4] = "5";
                strS[5] = "1";
                List<eFileAttachment> ListModelForSend = new List<eFileAttachment>();
                ListModelForSend = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendList", strS);
                model.SentPaperListCount = ListModelForSend.Count;
                #endregion

                Session["Model"] = model;

                return View();
            }

            else
            {
                return RedirectToAction("Login", "Account", new { @area = "" });
            }

        }


        public void SetSession()
        {

            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                DiaryModel DMdl = new DiaryModel();
                PaperLaidSummaryViewModel model = new PaperLaidSummaryViewModel();

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }
                DMdl.AskedBy = CurrentSession.UserID;
                model.mAssemblyList = (List<mAssembly>)Helper.ExecuteService("LegislationFixation", "GetAssemblyList", null);
                model.sessionList = (List<mSession>)Helper.ExecuteService("LegislationFixation", "GetSessionLsitbyAssemblyId", DMdl);
                model.PendingNoticesAssignCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeAssignPendingCount", DMdl);
                model.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCount", DMdl);
                // model.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCountLegislatin", DMdl);
                model.SentNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeSentCount", DMdl);
                model.RecievedNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperRecievedCount", DMdl);
                model.CurrentLobNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperCurrentLobCount", DMdl);
                model.LaidInHomeNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperLaidInHomeCount", DMdl);
                ////pending to lay Notice
                model.PendingToLayNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperPendingToLayListCount", DMdl);

                model.RecievedBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperRecievedCount", DMdl);
                model.CurrentLobBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperCurrentLobCount", DMdl);
                model.LaidInHomeBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperLaidInHomeCount", DMdl);
                ////Pending to lay Bills
                model.PendingToLayBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperPendingToLayListCount", DMdl);

                model.RecievedCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperRecievedCount", DMdl);
                model.CurrentLobCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperCurrentLobCount", DMdl);
                model.LaidInHomeCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperLaidInHomeCount", DMdl);

                ////pending to lay committee
                model.PendingToLayCommitteeCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperPendingToLayListCount", DMdl);

                model.PendingStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPendingCount", DMdl);
                model.SentStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionSentCount", DMdl);
                model.RecievedStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperRecievedCount", DMdl);
                model.CurrentLobStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperCurrentLobCount", DMdl);
                model.LaidInHomeStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperLaidInHomeCount", DMdl);

                ////starred Question pending to lay
                model.PendingToLayStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperPendingToLayListCount", DMdl);

                model.PendingUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPendingCount", DMdl);
                model.SentUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionSentCount", DMdl);
                model.RecievedUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperRecievedCount", DMdl);
                model.CurrentLobUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperCurrentLobCount", DMdl);
                model.LaidInHomeUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperLaidInHomeCount", DMdl);
                ////pending to lay Unstarred
                model.PendingToLayUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperPendingToLayListCount", DMdl);


                model.RecievedPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayRecievedCount", DMdl);
                model.CurrentLobPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayCurrentLobCount", DMdl);
                model.LaidInHomePaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayLaidInHomeCount", DMdl);
                ////Pending to lay
                model.PendingToLayOtherPaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetOtherPaperPendingToLayListCount", DMdl);

                //// Fixed and UnFixed Question Count For Starred And UnStarred Question type Created by Sunil on 02-July-2014  
                model.Starredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredFixedQuesCount", DMdl);
                model.StarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredUnFixedQuesCount", DMdl);
                model.Unstarredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredFixedQuesCount", DMdl);
                model.UnstarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredUnFixedQuesCount", DMdl);

                model.StarredCountForWithdrawn = (int)Helper.ExecuteService("LegislationFixation", "GetStarredforWithdrawnCount", DMdl);
                model.StarredWithdrawnCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredListWithdrawnCount", DMdl);
                model.UnstarredCountForWithdrawn = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredforWithdrawnCount", DMdl);
                model.UntarredWithdrawnCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredListWithdrawnCount", DMdl);

                tPaperLaidV mdl = new tPaperLaidV();
                SiteSettings siteSettingMod = new SiteSettings();
                mSession Mdl = new mSession();
                mAssembly assmblyMdl = new mAssembly();
                tMemberNotice model1 = new tMemberNotice();
                CutMotionModel Noticecoiunt = new CutMotionModel();
                if (DMdl.AssemblyID != 0 && DMdl.SessionID != 0)
                {
                    mdl.AssemblyCode = DMdl.AssemblyID;
                    mdl.SessionCode = DMdl.SessionID;

                    //Add Value for Session object
                    Mdl.SessionCode = DMdl.SessionID;
                    Mdl.AssemblyID = DMdl.AssemblyID;
                    //Add Value for Assembly Object 
                    assmblyMdl.AssemblyID = DMdl.AssemblyID;
                    // Add Value for tMemberNoticec Object
                    model1.AssemblyCode = DMdl.AssemblyID;
                    model1.SessionID = DMdl.SessionID;
                    model.AssemblyID = DMdl.AssemblyID;
                    model.SessionID = DMdl.SessionID;

                }
                else
                {
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    mdl.SessionCode = siteSettingMod.SessionCode;
                    mdl.AssemblyCode = siteSettingMod.AssemblyCode;
                    //Add Value for Session object
                    Mdl.SessionCode = siteSettingMod.SessionCode;
                    Mdl.AssemblyID = siteSettingMod.AssemblyCode;
                    //Add Value for Assembly Object 
                    assmblyMdl.AssemblyID = siteSettingMod.AssemblyCode;
                    // Add Value for tMemberNoticec Object
                    model1.AssemblyCode = siteSettingMod.AssemblyCode;
                    model1.SessionID = siteSettingMod.SessionCode;
                    model.AssemblyID = siteSettingMod.AssemblyCode;
                    model.SessionID = siteSettingMod.SessionCode;
                    model.AccessFilePath = siteSettingMod.AccessFilePath;
                    CurrentSession.AssemblyId = siteSettingMod.AssemblyCode.ToString();
                    CurrentSession.SessionId = siteSettingMod.SessionCode.ToString();
                    CurrentSession.FileAccessPath = siteSettingMod.AccessFilePath;

                }

                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
                mdl = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "GetUpdatedOtherPaperLaidCounters", mdl);
                if (mdl != null)
                {
                    model.TotalUpdatedFileCounter = mdl.TotalOtherpaperLaidUpdated;

                }
                mDepartment deptInfo = new mDepartment();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID))
                {
                    deptInfo.deptId = CurrentSession.DeptID;
                    deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
                }

                if (deptInfo != null)
                {
                    model.DepartmentName = deptInfo.deptname;
                }

                model.CurrentUserName = CurrentSession.UserName;
                model.UserDesignation = CurrentSession.MemberDesignation;
                model.UserName = CurrentSession.Name;
                // added by Sameer
                // Get the Total count of All Type of question.
                model1.QuestionTypeId = (int)QuestionType.StartedQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForProofReader", model1);
                model.TotalStaredReceived = model1.TotalStaredReceived;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForClubbed", model1);
                model.TotalStaredReceivedCl = model1.TotalStaredReceivedCl;
                model.TotalSBracktedQuestion = model1.TotalSBracktedQuestion;
                model.TotalInActiveQuestion = model1.TotalInActiveQuestion;
                model.TotalClubbedQuestion = model1.TotalClubbedQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForUnStaredClubbed", model1);
                model.TotalUnStaredReceivedCl = model1.TotalUnStaredReceivedCl;
                model.TotalUBractedQuestion = model1.TotalUBractedQuestion;
                model.UnstaredTotalClubbedQuestion = model1.UnstaredTotalClubbedQuestion;
                model1.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForLegisUnstartQuestion", model1);
                model.TotalUnStaredReceived = model1.TotalUnStaredReceived;
                int c = model.TotalStaredReceived + model.TotalUnStaredReceived;

                model.TotalValue = c;
                int d = model.TotalStaredReceivedCl + model.TotalClubbedQuestion + model.TotalInActiveQuestion;
                model.TotalValueCl = d;

                model.StarredCountExess = (int)Helper.ExecuteService("LegislationFixation", "GetCountQuestionsForExcess", DMdl);
                model.StarredCountExessed = (int)Helper.ExecuteService("LegislationFixation", "GetCountExcessedQuestions", DMdl);

                model.UnStarredCountExess = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredCountQuestionsForExcess", DMdl);
                model.UnStarredCountExessed = (int)Helper.ExecuteService("LegislationFixation", "GetCountUnStarredExcessedQuestions", DMdl);

                // Add Code for Rejected and StarredToUnstarred Count By type Created by Sunil on 28-July-2014 

                model.StarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredRejectedCount", DMdl);
                model.UnstarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredRejectedCount", DMdl);
                model.StarredToUnstarredCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredToUnstarredCount", DMdl);
                model.ChangedRuleNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetChangedRuleNoticeCount", DMdl);
                Noticecoiunt.AssemblyID = model.AssemblyID;
                Noticecoiunt.SessionID = model.SessionID;
                Noticecoiunt = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesForClubWithBracket", Noticecoiunt);
                model.CountForClub = Noticecoiunt.ResultCount;
                Noticecoiunt = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesClubbed", Noticecoiunt);
                model.CountForClubbed = Noticecoiunt.Noticeclubbed;

                model.CustomMessage = (string)TempData["CallingMethod"];
                if ((string)TempData["CallingMethod"] == "StarredPending")
                {
                    ViewBag.TagID = "liStarred";
                }
                else if ((string)TempData["CallingMethod"] == "UnStarredPending")
                {
                    ViewBag.TagID = "liUnstarred";
                }
                else if ((string)TempData["CallingMethod"] == "NoticePending")
                {
                    ViewBag.TagID = "liNotice";
                }

                //add code for house committee count by robin - 23-june2017
                #region For received list count
                string DepartmentId = CurrentSession.DeptID;
                string Officecode = CurrentSession.OfficeId;
                string AadharId = CurrentSession.AadharId;
                string currtBId = CurrentSession.BranchId;
                string Year = "5";
                string papers = "1";
                string[] strngBIDN = new string[2];
                strngBIDN[0] = CurrentSession.BranchId;
                var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
                string[] str = new string[6];
                str[0] = DepartmentId;
                str[1] = Officecode;
                str[2] = AadharId;
                str[3] = value123[0];
                str[4] = Year;
                str[5] = papers;
                var ReceivedList = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
                model.ReceivedListCount = ReceivedList.Count;
                #endregion

                #region For Draft List Count
                string[] strD = new string[6];
                strD[0] = DepartmentId;
                strD[1] = Officecode;
                strD[2] = AadharId;
                strD[3] = currtBId;
                strD[4] = CurrentSession.Designation;
                strD[5] = "1";
                var DraftListCount = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
                model.DraftListCount = DraftListCount.Count;
                #endregion

                #region For Send List Count
                string[] strS = new string[6];
                strS[0] = DepartmentId;
                strS[1] = Officecode;
                strS[2] = AadharId;
                strS[3] = currtBId;
                strS[4] = "5";
                strS[5] = "1";
                List<eFileAttachment> ListModelForSend = new List<eFileAttachment>();
                ListModelForSend = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendList", strS);
                model.SentPaperListCount = ListModelForSend.Count;
                #endregion

                Session["Model"] = model;

               
            }

            else
            {
                
            }
        }
        public ActionResult ViewWorks()
        {


            SetSession();


            string PassesDeptmement = ""; string PassedOffice = ""; string DistrictCode = "";

            var methodParameter = new List<KeyValuePair<string, string>>();
            schemeMapping _scheme = new schemeMapping();
            var santioned1 = _scheme.mySchemeList;
           
            ViewBag.DeptID = "0";
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            MlaDiary model1 = new MlaDiary();
            //CurrentSession.DeptID = "0";
            //CurrentSession.OfficeId = "0";
            //CurrentSession.DistrictCode = "0";
            var Agency = "0";
            _scheme = (schemeMapping)Helper.ExecuteService("ConstituencyVS", "gelFilteredMySchemeListByTypeCount", new string[] { Agency });
            methodParameter.Add(new KeyValuePair<string, string>("@Department", Agency));
            santioned1 = _scheme.mySchemeList.ToList();
            PassesDeptmement = "0";
            PassedOffice = "0";
            DistrictCode = "0";


            DataSet dataSetUser = new DataSet();
            dataSetUser = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql",

            //"getAllLatestProgressImages", methodParameter);
            "getAllLatestProgressImagesMC", methodParameter);




            DataSet dataSetUser1 = new DataSet();
            dataSetUser1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "getAllLatestwork", methodParameter);
            var empList = dataSetUser.Tables[0].AsEnumerable()
             .Select(dataRow => new ConstituencyWorkProgressImages
             {
                 filePath = dataRow.Field<string>("filePath"),
                 fileName = dataRow.Field<string>("fileName"),
                 tFileCaption = dataRow.Field<string>("workName"),
                 UploadedDate = dataRow.Field<DateTime>("UploadedDate"),
                 constituencyCode = dataRow.Field<Int32>("Number"),
                 schemeCode = dataRow.Field<long>("schemeCode"),
                 ConstituencyName = dataRow.Field<string>("ConstituencyName"),
                 ControllingAuthoritydept = dataRow.Field<string>("ControllingAuthoritydept"),
                 DepartmentIdOwner = dataRow.Field<string>("DepartmentIdOwner"),
                 workStatus = dataRow.Field<string>("workStatus"),
             }).ToList();

            var getAllLatestworkList = dataSetUser1.Tables[0].AsEnumerable()
           .Select(dataRow => new myShemeList()
           {
               workName = dataRow.Field<string>("workName"),
               ExecutiveDepartment = dataRow.Field<string>("DepartmentName"),
               ExecutiveOffice = dataRow.Field<string>("ExecutingAgency"),
               workStatus = dataRow.IsNull("WorkStatus") ? "" : dataRow.Field<string>("WorkStatus"),
               schemeTypeId = dataRow.IsNull("Physical Progress till Date") ? 0 : dataRow.Field<int>("Physical Progress till Date"),
               physicalProgress = dataRow.IsNull("financial Progress") ? 0 : dataRow.Field<double>("financial Progress"),
               sanctionedDate = dataRow.IsNull("LastUpdatedDate") ? "" : Convert.ToDateTime(dataRow.Field<DateTime>("LastUpdatedDate")).ToString("dd/MM/yyyy"),
           }).ToList();


            ViewBag.Images = empList;
            ViewBag.LatestUpdate = getAllLatestworkList;
            var santioned = santioned1.Where(x => x.workStatusCode == 1).Count();
            var Inprogress = santioned1.Where(x => x.workStatusCode == 2).Count();
            var Completed = santioned1.Where(x => x.workStatusCode == 3).Count();
            var PendingCourt = santioned1.Where(x => x.workStatusCode == 5).Count();
            var Canceled = santioned1.Where(x => x.workStatusCode == 4).Count();
            var PendingClear = santioned1.Where(x => x.workStatusCode == 6).Count();
            var PendingClearDPR = santioned1.Where(x => x.workStatusCode == 7).Count();

            return View(new string[] { PassedOffice, PassesDeptmement, CurrentSession.UserTypeConstituencyId, DistrictCode, Convert.ToString(santioned), Convert.ToString(Inprogress), Convert.ToString(Completed), Convert.ToString(PendingCourt), Convert.ToString(Canceled), Convert.ToString(PendingClear), Convert.ToString(PendingClearDPR) });
        }

        public ActionResult UpdateImagesbyOfficer(ConstituencyWorkProgressImages images, string FileCaption)
        {
            string pathString = "";
            string TemppathString = "";

            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            // string directory = FileSettings.SettingValue + SavedPdfPath;
            string url = "\\constituencyImage\\" + currentAssemby + "\\" + images.schemeCode + "\\";

            string path = FileSettings.SettingValue + url;
            var originalDirectory = new DirectoryInfo(string.Format("{0}", path));
            pathString = System.IO.Path.Combine(originalDirectory.ToString(), "Full");
            bool isExists = System.IO.Directory.Exists(pathString);

            if (!isExists)
                System.IO.Directory.CreateDirectory(pathString);

            var tempDir = new DirectoryInfo(string.Format("{0}tempimg\\", Server.MapPath(@"\")));
            TemppathString = System.IO.Path.Combine(tempDir.ToString(), "conimg");
            ResizeImage(600, TemppathString + "/" + images.tempPath, pathString + "\\" + images.tempPath);

            pathString = System.IO.Path.Combine(originalDirectory.ToString(), "thumb");
            isExists = System.IO.Directory.Exists(pathString);
            if (!isExists)
                System.IO.Directory.CreateDirectory(pathString);
            ResizeImage(150, TemppathString + "/" + images.tempPath, pathString + "\\" + images.tempPath);
            isExists = System.IO.File.Exists(TemppathString + "/" + images.tempPath);
            if (!isExists)
                System.IO.File.Delete(pathString);
            images.fileName = images.tempPath;
            images.thumbFilePath = url + "thumb";
            images.filePath = url + "Full"; ;
            images.createdBY = currentUser;
            images.createdDate = DateTime.Now;
            images.isActive = true;
            images.isDeleted = false;
            // images.
            WorkDetails _scheme = (WorkDetails)Helper.ExecuteService("ConstituencyVS", "getWorkDetailsByID", images.schemeCode);
            images.workCode = _scheme.workCode;
            images.constituencyCode = _scheme.eConstituencyID.Value;
            images.UploadedDate = DateTime.Now;
            Helper.ExecuteService("ConstituencyVS", "UpdateImagesbyOfficer", images);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateProgressbyOfficer(ConstituencyWorkProgress progress)
        {
            WorkDetails _scheme = (WorkDetails)Helper.ExecuteService("ConstituencyVS", "getWorkDetailsByID", progress.schemeCode);
            progress.workCode = _scheme.workCode;
            progress.constituencyCode = _scheme.eConstituencyID.Value;
            progress.submitDate = DateTime.Now;
            progress.createdBY = currentUser;
            progress.createdDate = DateTime.Now;
            progress.isActive = true;
            progress.isDeleted = false;

            Helper.ExecuteService("ConstituencyVS", "UpdateProgressbyOfficer", progress);
            return Json(progress.schemeCode, JsonRequestBehavior.AllowGet);
        }

        public ActionResult updateWorksbyMembers(WorkDetails work, HttpPostedFileBase photo, HttpPostedFileBase photo1)
        {
            string checkedData = "";
            string Statecode = Helper.ExecuteService("SiteSetting", "Get_StateCode", null) as string;
            work.StateCode = Convert.ToInt32(Statecode);
            work.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
            }


            //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            //string url = "/constituencyDPR";
            //string directory = FileSettings.SettingValue + url;

            //if (!System.IO.Directory.Exists(directory))
            //{
            //    System.IO.Directory.CreateDirectory(directory);
            //}
            //int fileID = GetFileRandomNo();

            //string url1 = "~/constituencyDPR";
            //string directory1 = Server.MapPath(url1);

            //if (Directory.Exists(directory))
            //{
            //    string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
            //    if (savedFileName.Length > 0)
            //    {
            //        string SourceFile = savedFileName[0];
            //        foreach (string page in savedFileName)
            //        {
            //            Guid FileName = Guid.NewGuid();

            //            string name = Path.GetFileName(page);

            //            string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


            //            if (!string.IsNullOrEmpty(name))
            //            {
            //                if (!string.IsNullOrEmpty(work.dprFile))
            //                    System.IO.File.Delete(System.IO.Path.Combine(directory, work.dprFile));
            //                System.IO.File.Copy(SourceFile, path, true);
            //                work.dprFile = "/constituencyDPR/" + FileName + "_" + name.Replace(" ", "");
            //            }

            //        }

            //    }
            //    else
            //    {
            //        return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
            //    }

            //}
            //foreach (var item in photo)
            //{
            //    var s = item.FileName;
            //}



#pragma warning disable CS0219 // The variable 'isSavedSuccessfully' is assigned but its value is never used
            bool isSavedSuccessfully = true;
#pragma warning restore CS0219 // The variable 'isSavedSuccessfully' is assigned but its value is never used
            string fName = "";
            string pathString = "";
            string NFileName = "";

            string fName1 = "";
            string pathString1 = "";
            string NFileName1 = "";

            if (photo != null)
            {
                //Save file content goes here
                fName = photo.FileName;
                if (photo != null && photo.ContentLength > 0)
                {


                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    // string directory = FileSettings.SettingValue + SavedPdfPath;
                    string url = "\\constituencyDPR\\" + currentAssemby + "\\" + work.workCode + "\\";
                    pathString = FileSettings.SettingValue + url;

                    var fileName1 = Path.GetFileName(photo.FileName);
                    string ext = Path.GetExtension(photo.FileName);
                    NFileName = Guid.NewGuid().ToString() + ext;

                    bool isExists = System.IO.Directory.Exists(pathString);

                    if (!isExists)
                        System.IO.Directory.CreateDirectory(pathString);

                    var path = string.Format("{0}\\{1}", pathString, NFileName);
                    photo.SaveAs(path);
                    work.dprFile = url + NFileName;
                }
            }
            if (photo1 != null)
            {
                //Save file content goes here
                fName1 = photo1.FileName;
                if (photo1 != null && photo1.ContentLength > 0)
                {


                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    // string directory = FileSettings.SettingValue + SavedPdfPath;
                    string url = "\\salientfeatures\\" + currentAssemby + "\\" + work.workCode + "\\";
                    pathString1 = FileSettings.SettingValue + url;

                    var fileName1 = Path.GetFileName(photo1.FileName);
                    string ext = Path.GetExtension(photo1.FileName);
                    NFileName1 = Guid.NewGuid().ToString() + ext;

                    bool isExists = System.IO.Directory.Exists(pathString1);

                    if (!isExists)
                        System.IO.Directory.CreateDirectory(pathString1);

                    var path = string.Format("{0}\\{1}", pathString1, NFileName1);
                    photo1.SaveAs(path);
                    work.featuresfile = url + NFileName1;
                }
            }
            work.userid = currentUser;
            //if (work.eConstituencyID == null)
            //{
            //    work.eConstituencyID = Convert.ToInt64(work.ConId_ForSDM_);
            //}
            //  work.eConstituencyID = Convert.ToInt64(work.ConId_ForSDM_);
            work.memberCode = string.IsNullOrEmpty(MemberCode) ? work.memberCode : Convert.ToInt32(MemberCode);
            string membercode = "";
            if (work.memberCode == null)
            {
                work.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
                membercode = Helper.ExecuteService("ConstituencyVS", "getMemberCodeByConstituency", work) as string;
                work.memberCode = Convert.ToInt32(membercode);
            }

            work.isMember = string.IsNullOrEmpty(CurrentSession.IsMember) ? false : Convert.ToBoolean(CurrentSession.IsMember);
            //  work.WorkTypeID = "work";
            if (work.WorkTypeID.Contains("work") || (work.programName.Contains("program")))
            {
                return Json("Please Wait To Fill All Input field", JsonRequestBehavior.AllowGet);
            };
            if (work.Mode == "New")
            {
                checkedData = Helper.ExecuteService("ConstituencyVS", "CheckWorkCode_ByCID", work) as string;
                if (checkedData != "N")
                {
                    checkedData = "Y";
                }
                else
                {
                    Helper.ExecuteService("ConstituencyVS", "CreateNewWorkbyMembers", work);


                }
                // Helper.ExecuteService("ConstituencyVS", "CreateNewWorkbyMembers", work);
            }
            else
            {
                Helper.ExecuteService("ConstituencyVS", "updateWorksbyMembers", work);
            }

            return Json(checkedData, JsonRequestBehavior.AllowGet); ;
        }
        public static void ResizeImage(int size, string filePath, string saveFilePath)
        {
            //variables for image dimension/scale
            double newHeight = 0;
            double newWidth = 0;
            double scale = 0;

            //create new image object
            Bitmap curImage = new Bitmap(filePath);

            //Determine image scaling
            if (curImage.Height > curImage.Width)
            {
                scale = Convert.ToSingle(size) / curImage.Height;
            }
            else
            {
                scale = Convert.ToSingle(size) / curImage.Width;
            }
            if (scale < 0 || scale > 1) { scale = 1; }

            //New image dimension
            newHeight = Math.Floor(Convert.ToSingle(curImage.Height) * scale);
            newWidth = Math.Floor(Convert.ToSingle(curImage.Width) * scale);

            //Create new object image
            Bitmap newImage = new Bitmap(curImage, Convert.ToInt32(newWidth), Convert.ToInt32(newHeight));
            Graphics imgDest = Graphics.FromImage(newImage);
            imgDest.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            imgDest.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            imgDest.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            imgDest.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            ImageCodecInfo[] info = ImageCodecInfo.GetImageEncoders();
            EncoderParameters param = new EncoderParameters(1);
            param.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);

            //Draw the object image
            imgDest.DrawImage(curImage, 0, 0, newImage.Width, newImage.Height);

            //Save image file
            newImage.Save(saveFilePath, info[1], param);

            //Dispose the image objects
            curImage.Dispose();
            newImage.Dispose();
            imgDest.Dispose();
        }

    }
}
