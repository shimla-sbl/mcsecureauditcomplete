﻿using EvoPdf;
using Ionic.Zip;
using iTextSharp.text;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Passes;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Session;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Notices/Report/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult IndexUnstarred()
        {
            return View();
        }



        public ActionResult GetUnstarredReportList(string CDateFrom, string CDateTo)
        {

            try
            {
                AssignQuestionModel model = new AssignQuestionModel();
                model.DateFrom = DateTime.ParseExact(CDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                model.DateTo = DateTime.ParseExact(CDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetDataByUnstarredDate", model);
                model.CurrentDateFrom = CDateFrom;
                model.CurrentDateTo = CDateTo;

                return PartialView("_GetUnstarredQData", model);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }




        public ActionResult GetReportList(string CDateFrom, string CDateTo)
        {

            try
            {
                AssignQuestionModel model = new AssignQuestionModel();
                model.DateFrom = DateTime.ParseExact(CDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                model.DateTo = DateTime.ParseExact(CDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetDataByDate", model);
                //List<mPasses> FinalList = new List<mPasses>();
                //var masterPassesFromDB = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetMasterPassesByStatus", new mPasses { Status = 2, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                //var masterPassesFromDBToModel = masterPassesFromDB.ToModelListForGrid();
                //var DepartmentApproved = masterPassesFromDB.Where(m => m.Status == 2 && m.RecommendationType == "Department").ToList();
                //var MinisterApproved = masterPassesFromDB.Where(m => m.Status == 2 && m.RecommendationType == "Minister").ToList();
                //var OfficerPApproved = masterPassesFromDB.Where(m => m.Status == 2 && m.RecommendationType == "Officer").ToList();
                //var MemberPApproved = masterPassesFromDB.Where(m => m.Status == 2 && m.RecommendationType == "Member").ToList();
                //var JournalisApproved = masterPassesFromDB.Where(m => m.Status == 2 && m.RecommendationType == "Media").ToList();
                //if (SessionDateFrom != null)
                //{

                //mpass.SessionDateF = DateTime.ParseExact(SessionDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //mpass.SessionDateT = DateTime.ParseExact(SessionDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //    model  = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetPassesApprovedByDate", mpass);
                //model.CurrentDateFrom = SessionDateFrom;
                //model.CurrentDateTo = SessionDateTo;
                model.CurrentDateFrom = CDateFrom;
                model.CurrentDateTo = CDateTo;

                return PartialView("_GetQData", model);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        //[HttpGet]
        //public ActionResult PdfStarredDatewise(string CDateFrom, string CDateTo)
        //{
        //    AssignQuestionModel model = new AssignQuestionModel();
        //    model.DateFrom = DateTime.ParseExact(CDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //    model.DateTo = DateTime.ParseExact(CDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //    model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetDataByDate", model);
        //    var Location = GeneratePdf(model); 

        //    string res = "Question List Generated";

        //    return Json(res, JsonRequestBehavior.AllowGet);
        //}

        public object PdfUnStarredDatewise(string CurrentDateFrom, string CurrentDateTo)
        {
            string CDateFrom = CurrentDateFrom;
            string CDateTo = CurrentDateTo;
            AssignQuestionModel model = new AssignQuestionModel();
            model.DateFrom = DateTime.ParseExact(CDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            model.DateTo = DateTime.ParseExact(CDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetDataByUnstarredDate", model);
            var Location = UnGeneratePdf(model);

            try
            {

                string url = "/ReportUNSPdfFile";

                string directory = Server.MapPath(url);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    if (Directory.Exists(directory))
                    {
                        zip.AddDirectory(directory, "eVidhan DIS  Paper Laid Summary");
                    }
                    else
                    {

                    }
                    Response.Clear();
                    Response.BufferOutput = false;
                    string zipName = String.Format("{0}.zip", "UnstarredQuestions");
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    Response.End();
                    string directory1 = Server.MapPath(url);
                    if (Directory.Exists(directory1))
                    {
                        System.IO.Directory.Delete(directory1, true);
                    }
                    return Response;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return "No File Available!";
            }

#pragma warning disable CS0162 // Unreachable code detected
            return Response;
#pragma warning restore CS0162 // Unreachable code detected
        }


        public Object UnGeneratePdf(AssignQuestionModel model)
        {
            try
            {
                int count = 0;
                string outXml = "";
#pragma warning disable CS0219 // The variable 'outInnerXml' is assigned but its value is never used
                string outInnerXml = "";
#pragma warning restore CS0219 // The variable 'outInnerXml' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'LOBName' is assigned but its value is never used
                string LOBName = "Question List";
#pragma warning restore CS0219 // The variable 'LOBName' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
                string CurrentSecretery = "";
#pragma warning restore CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SessionDate' is assigned but its value is never used
                DateTime SessionDate = new DateTime();
#pragma warning restore CS0219 // The variable 'SessionDate' is assigned but its value is never used
                string fileName = "";
                string savedPDF = string.Empty;
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 0, 0, 30, 30);
                if (model.UserList != null)
                {

                    outXml = @"<html>                           
                                <body style='font-family:DVOT-Yogesh;color:#003366;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div> 

 
                    </div>
            
<table style='width:100%'>
 
      <tr>
          <td style='text-align:center;font-size:28px;' >
             <b>Unstarred Questions</b>
              </td>
              </tr>
      </table>
<br />

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
            <b>" + Convert.ToDateTime(model.DateFrom).ToString("dd/MM/yyyy") + " To " + Convert.ToDateTime(model.DateTo).ToString("dd/MM/yyyy") + @"</b>
              </td>
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
<table>
 <div style='width:100%;'>
       <table style='border-collapse: collapse;border: 1px solid;'>
               <thead>
                       <tr style='background:rgb(166, 154, 154);;color:white;'>
                               <th style='border: 1px solid grey;text-align:center;width: 50px;'>Sr.No </th>
                               <th style='border: 1px solid grey;text-align:center;width: 100px;'>Diary No</th>
                               <th style='border: 1px solid grey;text-align:center;width: 120px;'>Subject</th>
                               <th style='border: 1px solid grey;text-align:center;width: 350px;'>Main Question</th>
                               <th style='border: 1px solid grey;text-align:center;width: 100px;'>Deaprtment </th>
                               <th style='border: 1px solid grey;text-align:center;width: 130px;'> Asked To </th>
                               <th style='border: 1px solid grey;text-align:center;width: 130px;'>Asked By</th>
                       </tr>
               </thead>

               <tbody>

           </table>";

                }
                foreach (var item in model.UserList)
                {
                    count = count + 1;
                    string style = (count % 2 == 0) ? "odd" : "even";
                    outXml += @"<table style='width:100%'>
  
                    <tr style='page-break-inside : avoid'>


                       <tr>
                               <td  style='border: 1px solid grey;text-align:left;width: 50px;'> " + count + @"  </td>
                               <td  style='border: 1px solid grey;text-align:left;width: 100px;'>" + item.DiaryNumber + @"</td>
                               <td  style='border: 1px solid grey;text-align:left;width: 120px;'>" + item.Subject + @"</td>
                               <td  style='border: 1px solid grey;text-align:left;'>" + item.MainQ + @"</td>
                                 <td  style='border: 1px solid grey;text-align:left;width: 100px;'>" + item.DepartmentName + @"</td>
                               <td  style='border: 1px solid grey;text-align:left;width: 130px;'>" + item.MinisterName + @"</td>
                               <td  style='border: 1px solid grey;text-align:left;width: 130px;'>" + item.MemberName + @"</td>
                       </tr>
               </tbody>
       </table>
</div>
                             
</table>

";
                }



                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/ReportUNSPdfFile/";
                fileName = "QuestionList" + "DateFrom" + "_" + Convert.ToDateTime(model.DateFrom).ToString("dd/MM/yyyy") + "_" + "DateTo" + "_" + Convert.ToDateTime(model.DateTo).ToString("dd/MM/yyyy") + "_" + "U" + ".pdf";
                fileName = fileName.Replace("/", "-");
                //  HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();
                //DownloadDateWiseQuestion();
                return url + "," + fileName;


            }

            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }



        public object PdfStarredDatewise(string CurrentDateFrom, string CurrentDateTo)
        {
            string CDateFrom = CurrentDateFrom;
            string CDateTo = CurrentDateTo;
            AssignQuestionModel model = new AssignQuestionModel();
            model.DateFrom = DateTime.ParseExact(CDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            model.DateTo = DateTime.ParseExact(CDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetDataByDate", model);
            var Location = GeneratePdf(model);

            try
            {

                string url = "/ReportPdfFile";

                string directory = Server.MapPath(url);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    if (Directory.Exists(directory))
                    {
                        zip.AddDirectory(directory, "eVidhan DIS  Paper Laid Summary");
                    }
                    else
                    {

                    }
                    Response.Clear();
                    Response.BufferOutput = false;
                    string zipName = String.Format("{0}.zip", "StarredQuestions");
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    Response.End();
                    string directory1 = Server.MapPath(url);
                    if (Directory.Exists(directory1))
                    {
                        System.IO.Directory.Delete(directory1, true);
                    }
                    return Response;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return "No File Available!";
            }

#pragma warning disable CS0162 // Unreachable code detected
            return Response;
#pragma warning restore CS0162 // Unreachable code detected
        }

        public Object GeneratePdf(AssignQuestionModel model)
        {
            try
            {
                int count = 0;
                string outXml = "";
#pragma warning disable CS0219 // The variable 'outInnerXml' is assigned but its value is never used
                string outInnerXml = "";
#pragma warning restore CS0219 // The variable 'outInnerXml' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'LOBName' is assigned but its value is never used
                string LOBName = "Question List";
#pragma warning restore CS0219 // The variable 'LOBName' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
                string CurrentSecretery = "";
#pragma warning restore CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SessionDate' is assigned but its value is never used
                DateTime SessionDate = new DateTime();
#pragma warning restore CS0219 // The variable 'SessionDate' is assigned but its value is never used
                string fileName = "";
                string savedPDF = string.Empty;
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 0, 0, 30, 30);
                if (model.UserList != null)
                {

                    outXml = @"<html>                           
                                <body style='font-family:DVOT-Yogesh;color:#003366;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div> 

 
                    </div>
            
<table style='width:100%'>
 
      <tr>
          <td style='text-align:center;font-size:28px;' >
             <b>Starred Questions</b>
              </td>
              </tr>
      </table>
<br />

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
            <b>" + Convert.ToDateTime(model.DateFrom).ToString("dd/MM/yyyy") + " To " + Convert.ToDateTime(model.DateTo).ToString("dd/MM/yyyy") + @"</b>
              </td>
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
<table>
 <div style='width:100%;'>
       <table style='border-collapse: collapse;border: 1px solid;'>
               <thead>
                       <tr style='background:rgb(166, 154, 154);;color:white;'>
                               <th style='border: 1px solid grey;text-align:center;width: 50px;'>Sr.No </th>
                               <th style='border: 1px solid grey;text-align:center;width: 100px;'>Diary No</th>
                               <th style='border: 1px solid grey;text-align:center;width: 120px;'>Subject</th>
                               <th style='border: 1px solid grey;text-align:center;width: 350px;'>Main Question</th>
                               <th style='border: 1px solid grey;text-align:center;width: 100px;'>Deaprtment </th>
                               <th style='border: 1px solid grey;text-align:center;width: 130px;'> Asked To </th>
                               <th style='border: 1px solid grey;text-align:center;width: 130px;'>Asked By</th>
                       </tr>
               </thead>

               <tbody>

           </table>";

                }
                foreach (var item in model.UserList)
                {
                    count = count + 1;
                    string style = (count % 2 == 0) ? "odd" : "even";
                    outXml += @"<table style='width:100%'>
  
                    <tr style='page-break-inside : avoid'>


                       <tr>
                               <td  style='border: 1px solid grey;text-align:left;width: 50px;'> " + count + @"  </td>
                               <td  style='border: 1px solid grey;text-align:left;width: 100px;'>" + item.DiaryNumber + @"</td>
                               <td  style='border: 1px solid grey;text-align:left;width: 120px;'>" + item.Subject + @"</td>
                               <td  style='border: 1px solid grey;text-align:left;'>" + item.MainQ + @"</td>
                                 <td  style='border: 1px solid grey;text-align:left;width: 100px;'>" + item.DepartmentName + @"</td>
                               <td  style='border: 1px solid grey;text-align:left;width: 130px;'>" + item.MinisterName + @"</td>
                               <td  style='border: 1px solid grey;text-align:left;width: 130px;'>" + item.MemberName + @"</td>
                       </tr>
               </tbody>
       </table>
</div>
                             
</table>

";
                }



                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/ReportPdfFile/";
                fileName = "QuestionList" + "DateFrom" + "_" + Convert.ToDateTime(model.DateFrom).ToString("dd/MM/yyyy") + "_" + "DateTo" + "_" + Convert.ToDateTime(model.DateTo).ToString("dd/MM/yyyy") + "_" + "S" + ".pdf";
                fileName = fileName.Replace("/", "-");
                //  HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();
                //DownloadDateWiseQuestion();
                return url + "," + fileName;


            }

            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }

        #region New report
        public ActionResult MemberWiseReport()
        {
            CurrentSession.SetSessionAlive = null;
            return PartialView("MemberWiseReport");
        }

        public ActionResult MemberWiseCompleteReport()
        {
            CurrentSession.SetSessionAlive = null;

            return PartialView("_MemberWiseCompleteReport");
        }
        public ActionResult memberwiseReportPartial(string fromDate, string toDate, string memberID)
        {
            if (memberID != "0")
            {
                List<memberWiseQuestionReport> ws = (List<memberWiseQuestionReport>)Helper.ExecuteService("Questions", "StartedQuestioncount", new string[] { memberID, CurrentSession.AssemblyId, CurrentSession.SessionId });
                ViewBag.MemberCode = memberID;
                return PartialView("_reportMemberWise", ws);
            }
            else
            {
                List<memberWiseQuestionReport> ws = (List<memberWiseQuestionReport>)Helper.ExecuteService("Questions", "StartedQuestioncountForAll", new string[] { CurrentSession.AssemblyId, CurrentSession.SessionId });
                ViewBag.MemberCode = memberID;
                return PartialView("_reportMemberWise", ws);
            }
        }
        public ActionResult memberwiseReportCompletePartial(string fromDate, string toDate, string memberID)
        {

            List<memberWiseQuestionReport> ws = (List<memberWiseQuestionReport>)Helper.ExecuteService("Questions", "CompleteStatusReport", new string[] { memberID, CurrentSession.AssemblyId, CurrentSession.SessionId });
            ViewBag.MemberCode = memberID;
            return PartialView("_CompleteReportMemberWise", ws);

        }
        public ActionResult getNoticeListByMemberCode(String memberCode, string type, string qtype, string ruleno)
        {
            if (memberCode != "0")
            {
                List<memberWiseQuestionList> ws = (List<memberWiseQuestionList>)Helper.ExecuteService("Questions", "getNoticeListByMemberCode", new string[] { memberCode, CurrentSession.AssemblyId, CurrentSession.SessionId, type, ruleno });
                return PartialView("_NoticeList", ws);
            }
            else
            {
                List<memberWiseQuestionList> ws = (List<memberWiseQuestionList>)Helper.ExecuteService("Questions", "getNoticeListForAll", new string[] { CurrentSession.AssemblyId, CurrentSession.SessionId, type, ruleno });
                return PartialView("_NoticeList", ws);
            }
        }
        public ActionResult getQuestionListByMemberCode(string memberCode, string type, string qtype)
        {
            if (memberCode != "0")
            {
                List<memberWiseQuestionList> ws = (List<memberWiseQuestionList>)Helper.ExecuteService("Questions", "getQuestionListByMemberCode", new string[] { memberCode, CurrentSession.AssemblyId, CurrentSession.SessionId, qtype, type });
                ViewBag.Heading = Helper.ExecuteService("salaryheads", "getMemberName", memberCode) as string + "<br/>" + qtype + "   " + type + " Questions(" + ws.Count + ")";
                return PartialView("_QuestionList", ws);
            }
            else
            {
                List<memberWiseQuestionList> ws = (List<memberWiseQuestionList>)Helper.ExecuteService("Questions", "getQuestionListForAll", new string[] { CurrentSession.AssemblyId, CurrentSession.SessionId, qtype, type });
                ViewBag.Heading = "All Member's <br/>" + qtype + "   " + type + " Questions(" + ws.Count + ")";
                return PartialView("_QuestionList", ws);

            }
        }
        public ActionResult getQuestionByID(string QuestionID)
        {
            memberWiseQuestionList ws = (memberWiseQuestionList)Helper.ExecuteService("Questions", "getQuestionByID", QuestionID);
            ViewBag.Heading = Helper.ExecuteService("salaryheads", "getMemberName", ws.memberID) as string + "<br/>" + ws.DiaryNumber + "   " + ws.subject;
            return PartialView("_QuestionView", ws);
        }


        #endregion
        #region passes report
        public ActionResult issuePasses()
        {
            return PartialView("_issuePasses");
        }
        public ActionResult getPassReport(string GroupBy, string fromDate, string toDate)
        {
            return PartialView("_issuePassesReport");
        }
        public ActionResult entryPasses()
        {
            return PartialView("_issuePasses");
        }
        public ActionResult getEntryPassReport(string GroupBy, string fromDate, string toDate)
        {
            return PartialView("_issuePassesReport");
        }
        #endregion



        public ActionResult QuestionsNoticesReplyReport()
        {
            CurrentSession.SetSessionAlive = null;

            LegislationFixationModel model = new LegislationFixationModel();

            CurrentSession.SetSessionAlive = null;
            tPaperLaidV model1 = new tPaperLaidV();
            //Get the Total count of All Type of question.
            model1 = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model1);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            else
            {

                model.AssemblyId = model1.AssemblyId;//Convert.ToInt16(CurrentSession.AssemblyId);
                model.SessionId = model1.SessionId;// Convert.ToInt16(CurrentSession.SessionId);
            }



            // model.AssemblyId = model1.AssemblyId;
            //model.SessionId = model1.SessionId;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;

            mSessionDate sessionDates = new mSessionDate();

            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();


            mSession.SessionCode = model.SessionId;
            mSession.AssemblyID = model.AssemblyId;
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {

                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
                LegislationFixationModel selectList = new LegislationFixationModel();
                selectList.Id = 0;
                selectList.SessionDate = "Select Session Date";
                model.CustomSessionDateList.Add(selectList);


            }
            return PartialView("_QuestionsNoticesReplyReport", model);
        }

        public ActionResult QuestionsNoticesReplyReportPartial(string DepartmentID, string MemberID, string SessionDate)
        {

            List<DepartmentWiseQuestionNoticeReport> ws = (List<DepartmentWiseQuestionNoticeReport>)Helper.ExecuteService("Questions", "DepartmentWiseQuestionNoticeReport", new string[] { DepartmentID, MemberID, SessionDate, CurrentSession.AssemblyId, CurrentSession.SessionId });
            ViewBag.DepartmentID = DepartmentID;
            ViewBag.MemberID = MemberID;
            ViewBag.SessionDate = SessionDate;
           
            return PartialView("_QuestionsNoticesReplyPartial", ws);

        }

        public JsonResult fillDepartment()
        {
            List<fillDepartment> list = (List<fillDepartment>)Helper.ExecuteService("loan", "getDepartmentList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadPDF(string DepartmentID, string MemberID, string SessionDate)
        {
            DepartmentWiseQuestionNoticeReport model = new DepartmentWiseQuestionNoticeReport();

            List<DepartmentWiseQuestionNoticeReport> ws = (List<DepartmentWiseQuestionNoticeReport>)Helper.ExecuteService("Questions", "DepartmentWiseQuestionNoticeReport", new string[] { DepartmentID, MemberID, SessionDate, CurrentSession.AssemblyId, CurrentSession.SessionId });

            string url = "/QuesNoticeList/";

            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            string path = "";



            string Result = this.GetPassContList(DepartmentID, MemberID, SessionDate);
            string FileName = CreatePassCountListPdf(Result, url);
            try
            {

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                path = Path.Combine(Server.MapPath("~" + url), FileName);

#pragma warning disable CS0219 // The variable 'contentType' is assigned but its value is never used
                string contentType = "application/octet-stream";
#pragma warning restore CS0219 // The variable 'contentType' is assigned but its value is never used
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

            byte[] bytes = System.IO.File.ReadAllBytes(path);
            return File(bytes, "application/pdf");
        }


        public string GetPassContList(string DepartmentID, string MemberID, string SessionDate)
        {
            List<DepartmentWiseQuestionNoticeReport> ws = (List<DepartmentWiseQuestionNoticeReport>)Helper.ExecuteService("Questions", "DepartmentWiseQuestionNoticeReport", new string[] { DepartmentID, MemberID, SessionDate, CurrentSession.AssemblyId, CurrentSession.SessionId });

            string AssemblyId = CurrentSession.AssemblyId;
            string SessionId = CurrentSession.SessionId;

            StringBuilder ReplyList = new StringBuilder();


            if (ws != null && ws.Count > 0)
            {
                ReplyList.Append(string.Format("<div style='text-align:center;'><h2>Question/Notice Count List</h2></div>"));

                ReplyList.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                ReplyList.Append("<thead class='header' ><tr style='background-color: #428bca ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:30px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px;'>SR. NO.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Type</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Total</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Acknowledged</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Not Acknowledged</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Accepted</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Not Accepted</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Fixed Question</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Reply  Received</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Reply Not Received</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Questions Sent</th>");

                ReplyList.Append("</tr></thead>");


                ReplyList.Append("<tbody  class='body'>");
                int count = 1;
                foreach (var item in ws)
	{
                     ReplyList.Append("<tr>");
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", count));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.ModuleName));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.totalCount));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.Acknowledged));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.notAcknowledged));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.Accepted));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.notAccepted));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.Fixed));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.ReplyRecieved));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.ReplyNotRecieved));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.QuestionSent));
                    ReplyList.Append("</tr>");
                    count++;
		 
	}
              


                ReplyList.Append("</tbody></table></div> ");
            }
            else
            {
                ReplyList.Append("No Record Found");
            }
            return ReplyList.ToString();
        }

        public string CreatePassCountListPdf(string Report, string Url)
        {
            try
            {
                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 20, 20), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = Report;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }



                Guid FId = Guid.NewGuid();
                string fileName = FId + "_QuesNoticeReportList.pdf";


                string directory = Server.MapPath(Url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(Server.MapPath("~" + Url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();



                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {


            }
        }



        [HttpPost]
        public ActionResult DownloadExcel(string DepartmentID, string MemberID, string SessionDate)
        {

            string Result = this.GetQuestionContList(DepartmentID, MemberID, SessionDate);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=QuestionsCountList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }

        public string GetQuestionContList(string DepartmentID, string MemberID, string SessionDate)
        {

           

            List<DepartmentWiseQuestionNoticeReport> ws = (List<DepartmentWiseQuestionNoticeReport>)Helper.ExecuteService("Questions", "DepartmentWiseQuestionNoticeReport", new string[] { DepartmentID, MemberID, SessionDate, CurrentSession.AssemblyId, CurrentSession.SessionId });

            string AssemblyId = CurrentSession.AssemblyId;
            string SessionId = CurrentSession.SessionId;

            StringBuilder ReplyList = new StringBuilder();


            if (ws != null && ws.Count > 0)
            {
                ReplyList.Append(string.Format("<div style='text-align:center;'><h2>Question/Notice Count List</h2></div>"));

                ReplyList.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                ReplyList.Append("<thead class='header' ><tr style='background-color: #428bca ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:30px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px;'>SR. NO.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Type</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Total</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Acknowledged</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Not Acknowledged</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Accepted</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Not Accepted</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Fixed Question</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Reply  Received</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Reply Not Received</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Questions Sent</th>");

                ReplyList.Append("</tr></thead>");


                ReplyList.Append("<tbody  class='body'>");
                int count = 1;
                foreach (var item in ws)
                {
                    ReplyList.Append("<tr>");
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", count));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.ModuleName));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.totalCount));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.Acknowledged));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.notAcknowledged));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.Accepted));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.notAccepted));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.Fixed));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.ReplyRecieved));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.ReplyNotRecieved));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", item.QuestionSent));
                    ReplyList.Append("</tr>");
                    count++;

                }



                ReplyList.Append("</tbody></table></div> ");
            }
            else
            {
                ReplyList.Append("No Record Found");
            }
            return ReplyList.ToString();
        }

        [HttpPost]
        [ValidateInput(false)]        
        public ActionResult Export(string GridHtml)
        {
            GridHtml = GridHtml.Replace("View Details", "");
            GridHtml = GridHtml.Replace("View", "");
            GridHtml = GridHtml.Replace("View Attachment", "");
            GridHtml = GridHtml.Replace("Attachment", "");
            MemoryStream output = new MemoryStream();
            EvoPdf.Document document1 = new EvoPdf.Document();
            // set the license key
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

            AddElementResult addResult;

            HtmlToPdfElement htmlToPdfElement;
            string htmlStringToConvert = GridHtml;
            string baseURL = "";
            htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

            addResult = page.AddElement(htmlToPdfElement);
            byte[] pdfBytes = document1.Save();

            try
            {
                output.Write(pdfBytes, 0, pdfBytes.Length);
                output.Position = 0;

            }
            finally
            {
                // close the PDF document to release the resources
                document1.Close();
            }
            string fileName = "";
            string url = "/ReportPdfFile/";
            fileName = "QuestionList" + "DateFrom" + "_" + "_" + "DateTo" + "_" + "_" + "S" + ".pdf";
            fileName = fileName.Replace("/", "-");
            //  HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

            string directory = Server.MapPath(url);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
           

            var path = Path.Combine(directory, fileName);
            
            System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);          
            _FileStream.Close();        
            DownloadPDF(url);
            System.IO.File.Delete(path);
            return View();


        }

        public object DownloadPDF(string url)
        {
            try
            {  
                string directory = Server.MapPath(url);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    //zip.AddDirectoryByName("Files");
                    if (Directory.Exists(directory))
                    {
                        zip.AddDirectory(directory, "Report");
                    }
                    else
                    {
                    }

                    Response.Clear();
                    Response.BufferOutput = false;
                    string zipName = String.Format("{0}.zip", "Loan Statement");
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);

                    zip.Save(Response.OutputStream);
                    Response.End();

                    return Response;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return "No File Available!";
            }

            // return Response;
        }

      
    }
}
