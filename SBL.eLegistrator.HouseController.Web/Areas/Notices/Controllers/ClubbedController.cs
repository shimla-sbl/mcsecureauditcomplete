﻿using Lib.Web.Mvc.JQuery.JqGrid;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Filters;
using EvoPdf;
using System.IO;
using iTextSharp.text;
using System.Drawing;
using System.Net;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    [SBLAuthorize(Allow = "Authenticated")]
    [Audit]
    [NoCache]
    public class ClubbedController : Controller
    {
        //
        // GET: /Notices/Clubbed/

        public ActionResult Index()
        {
            tMemberNotice model = new tMemberNotice();

            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForClubbed", model);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForUnStaredClubbed", model);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;

            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            int c = model.TotalStaredReceived + model.TotalClubbedQuestion + model.TotalInActiveQuestion;
            model.TotalValue = c;
            return View(model);
        }
        public ActionResult PendingStaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            tMemberNotice model = new tMemberNotice();
            List<tMemberNotice> DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartment", model) as List<tMemberNotice>;
            model.DepartmentId = Convert.ToString(0);
            model.DepartmentName = "Select Department";
            model.DepartmentLt = DepartmentLt;
            DepartmentLt.Add(model);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            if (PageNumber != null && PageNumber != "")
            {
                model.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                model.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                model.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                model.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                model.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                model.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                model.loopEnd = Convert.ToInt32("5");
            }

            return PartialView("_DropdeptList", model);
        }
        public ActionResult StaredQuestion(string DeptId, string AssemblyID, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            ClubbedQuestionModel model = new ClubbedQuestionModel();

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            //model.DepartmentId = DeptId;
            //model.AssemblyID = Convert.ToInt32(AssemblyID);
            //model.SessionID = Convert.ToInt32(SessionId);
            model = (ClubbedQuestionModel)Helper.ExecuteService("Notice", "GetQForClubWithBracket", model);

            return PartialView("_QuestionListForClubbing", model);
        }
        public ActionResult UnStaredQuestion(string DeptId, string AssemblyID, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            ClubbedQuestionModel model = new ClubbedQuestionModel();

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            //model.DepartmentId = DeptId;
            //model.AssemblyID = Convert.ToInt32(AssemblyID);
            //model.SessionID = Convert.ToInt32(SessionId);
            model = (ClubbedQuestionModel)Helper.ExecuteService("Notice", "UnstaredGetQForClubWithBracket", model);

            return PartialView("_UnstaredClubbedQuestionList", model);
        }
        public ActionResult GetAllStarredQuestionsGrid(JqGridRequest request, ClubbedQuestionModel customModel)
        {
            ClubbedQuestionModel model = new ClubbedQuestionModel();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model.DepartmentId = customModel.DepartmentId;
            var result = (ClubbedQuestionModel)Helper.ExecuteService("Notice", "GetQForClubWithBracket", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<ClubbedQuestionModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<ClubbedQuestionModel>(Convert.ToString(questionList.QuestionID), new ClubbedQuestionModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        public ActionResult GetUnStarredQuestionsGrid(JqGridRequest request, ClubbedQuestionModel customModel)
        {
            ClubbedQuestionModel model = new ClubbedQuestionModel();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = customModel.DepartmentId;
            var result = (ClubbedQuestionModel)Helper.ExecuteService("Notice", "UnstaredGetQForClubWithBracket", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<ClubbedQuestionModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<ClubbedQuestionModel>(Convert.ToString(questionList.QuestionID), new ClubbedQuestionModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult ClubbedQuestion(string chkvals, string DepartmentId, string AssemblyID, string SessionID)
        {
            tMemberNotice model = new tMemberNotice();
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.QuestionID = Convert.ToInt32(chkvals);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetQuestionDetailsForMerging", model);

            foreach (var item in model.tQuestionModel)
            {
                model.Title += item.Subject + ",";
                model.MemberName += item.MemberName + ",";
                model.Details += item.MainQuestion + ",";
                model.DiaryNo += item.DiaryNumber + ",";
                string values = Convert.ToString(item.MemberId) + ",";
                model.MemberId = item.MemberId;
                model.MemIds += values;
                string Qval = Convert.ToString(item.QuestionID) + ",";
                model.QuesIds += Qval;
                model.EventId = item.EventId;
                model.OriDiaryFileName = item.OriDiaryFileName;
                model.OriDiaryFilePath = item.OriDiaryFilePath;
                model.TypistUserId = item.TypistUserId;
                model.SubInCatchWord = item.SubjectCatchWords;
                model.MinistryId = item.MinistryId;
            }
            model.Title = model.Title.TrimEnd(',');
            model.MemberName = model.MemberName.TrimEnd(',');
            model.Details = model.Details.TrimEnd(',');
            model.DiaryNo = model.DiaryNo.TrimEnd(',');
            model.MemIds = model.MemIds.TrimEnd(',');
            model.MemberId = model.MemberId;
            model.EventId = model.EventId;
            model.OriDiaryFileName = model.OriDiaryFileName;
            model.OriDiaryFilePath = model.OriDiaryFilePath;
            model.TypistUserId = model.TypistUserId;
            model.QuesIds = model.QuesIds.TrimEnd(',');
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.DepartmentId = DepartmentId;
            model.SubInCatchWord = model.SubInCatchWord;
            model.MinistryId = model.MinistryId;
            model.QuestionID = Convert.ToInt32(chkvals);
            return PartialView("_GetMergeQuestions", model);
        }

        public ActionResult UnstaredClubbedQuestion(string chkvals, string DepartmentId, string AssemblyID, string SessionID)
        {
#pragma warning disable CS0219 // The variable 'Month' is assigned but its value is never used
            string Month = "";
#pragma warning restore CS0219 // The variable 'Month' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Day' is assigned but its value is never used
            string Day = "";
#pragma warning restore CS0219 // The variable 'Day' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Year' is assigned but its value is never used
            string Year = "";
#pragma warning restore CS0219 // The variable 'Year' is assigned but its value is never used
            tMemberNotice model = new tMemberNotice();
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.QuestionValue = chkvals;
            model.QuestionID = Convert.ToInt32(chkvals);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetQuestionDetailsForMerging", model);

            foreach (var item in model.tQuestionModel)
            {
                //model.MainDiaryNo = item.DiaryNumber;
                model.Title += item.Subject + ",";
                model.MemberName += item.MemberName + ",";
                model.Details += item.MainQuestion + ",";
                model.DiaryNo += item.DiaryNumber + ",";
                string values = Convert.ToString(item.MemberId) + ",";
                model.MemberId = item.MemberId;
                model.MemIds += values;
                string Qval = Convert.ToString(item.QuestionID) + ",";
                model.QuesIds += Qval;
                model.EventId = item.EventId;
                model.OriDiaryFileName = item.OriDiaryFileName;
                model.OriDiaryFilePath = item.OriDiaryFilePath;
                model.TypistUserId = item.TypistUserId;
                model.SubInCatchWord = item.SubjectCatchWords;
                model.MinistryId = item.MinistryId;
            }
            model.Title = model.Title.TrimEnd(',');
            model.MemberName = model.MemberName.TrimEnd(',');
            model.Details = model.Details.TrimEnd(',');
            model.DiaryNo = model.DiaryNo.TrimEnd(',');
            model.MemIds = model.MemIds.TrimEnd(',');
            model.MemberId = model.MemberId;
            model.EventId = model.EventId;
            model.OriDiaryFileName = model.OriDiaryFileName;
            model.OriDiaryFilePath = model.OriDiaryFilePath;
            model.TypistUserId = model.TypistUserId;
            model.QuesIds = model.QuesIds.TrimEnd(',');
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.DepartmentId = DepartmentId;
            model.SubInCatchWord = model.SubInCatchWord;
            model.MinistryId = model.MinistryId;
            model.QuestionID = Convert.ToInt32(chkvals);
            return PartialView("_GetUnstaredMergeQuestion", model);
        }

        public JsonResult SaveNewMergeEntry(SaveValue model)
        {
            tMemberNotice Update = new tMemberNotice();

            Update.AssemblyID = model.AssemblyId;
            Update.SessionID = model.SessionId;
            Update.QuestionTypeId = (int)QuestionType.StartedQuestion;
            Update.MemberId = Convert.ToInt32(model.SMem);
            Update.MainQuestion = model.Details;
            Update.Subject = model.Title;
            Update.MemIds = model.MemIds;
            Update.QuesIds = model.QuestIDs;
            Update.DepartmentId = model.DepartmentId;
            Update.EventId = model.EventId;
            Update.TypistUserId = model.TypistUserId;
            //Update.OriDiaryFileName = model.OriDiaryFileName;
            //Update.OriDiaryFilePath = model.OriDiaryFilePath;
            //Update.SubInCatchWord = model.SubCatch;
            Update.MinistryId = model.MinistryId;
            Update.RoleName = CurrentSession.UserID;
            Update.DiaryNo = model.DiaryNo;
            Update.QuestionID = model.MId;
            Update.Msg = model.SequenceDiaryN;

            Update.Msg = Helper.ExecuteService("Notice", "SaveMergeEntry", Update) as string;
            if (Update.Msg.Substring(0, 2) == "SF")
            {
                GeneratePdfByQuesIdStarred_Fix(Update.QuestionID.ToString());
            }
            else if (Update.Msg.Substring(0, 2) == "SU")
            {
                GeneratePdfByQuesIdStarred_Unfix(Update.QuestionID);
            }
            else if (Update.Msg.Substring(0, 2) == "UF")
            {
                GeneratePdfByQuesIdUnstarred_Fix(Update.QuestionID.ToString());
            }
            else if (Update.Msg.Substring(0, 2) == "UU")
            {
                GeneratePdfByQuesIdUnstarred_Unfix(Update.QuestionID);
            }
            Update.Msg = Update.Msg.Substring(3);
            return Json(Update, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetClubbedQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            ShowListModelCl model = new ShowListModelCl();
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            return PartialView("_ShowClubbedList", model);
        }
        public ActionResult UnstaredGetClubbedQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            ShowListModelCl model = new ShowListModelCl();

            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            return PartialView("_ShowBracketedQList", model);
        }
        public ActionResult GetClubbedQuestionGrid(JqGridRequest request, ShowListModelCl customModel)
        {


            ShowListModelCl model = new ShowListModelCl();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            //model.DepartmentId = customModel.DepartmentId;
            var result = (ShowListModelCl)Helper.ExecuteService("Notice", "GetClubbedQuestionList", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };
            //model.RoleName = "Vidhan Sabha Typist";
            //model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetUserCode", model);
            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<ShowListModelCl>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<ShowListModelCl>(Convert.ToString(questionList.QuestionID), new ShowListModelCl(questionList)));

            return new JqGridJsonResult() { Data = response };

        }
        public ActionResult UnstaredGetClubbedQuestionGrid(JqGridRequest request, ShowListModelCl customModel)
        {


            ShowListModelCl model = new ShowListModelCl();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            //model.DepartmentId = customModel.DepartmentId;
            var result = (ShowListModelCl)Helper.ExecuteService("Notice", "UnstaredGetBracketedQuestionList", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };
            //model.RoleName = "Vidhan Sabha Typist";
            //model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetUserCode", model);
            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<ShowListModelCl>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<ShowListModelCl>(Convert.ToString(questionList.QuestionID), new ShowListModelCl(questionList)));

            return new JqGridJsonResult() { Data = response };

        }
        public ActionResult GetInactiveClubbed(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            //string Month = "";
            //string Day = "";
            //string Year = "";
            //PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            //RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            //loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            //loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            //tMemberNotice model = new tMemberNotice();

            //model.loopStart = Convert.ToInt16(loopStart);
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            //model.PageIndex = Convert.ToInt16(PageNumber);

            ////Get the Questions related to Stared Question.
            //model.QuestionTypeId = 1;
            //model = (tMemberNotice)Helper.ExecuteService("Notice", "GetInactiveQuestion", model);

            //if (PageNumber != null && PageNumber != "")
            //{
            //    model.PageNumber = Convert.ToInt32(PageNumber);
            //}
            //else
            //{
            //    model.PageNumber = Convert.ToInt32("1");
            //}
            //if (RowsPerPage != null && RowsPerPage != "")
            //{
            //    model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            //}
            //else
            //{
            //    model.RowsPerPage = Convert.ToInt32("10");
            //}
            //if (PageNumber != null && PageNumber != "")
            //{
            //    model.selectedPage = Convert.ToInt32(PageNumber);
            //}
            //else
            //{
            //    model.selectedPage = Convert.ToInt32("1");
            //}
            //if (loopStart != null && loopStart != "")
            //{
            //    model.loopStart = Convert.ToInt32(loopStart);
            //}
            //else
            //{
            //    model.loopStart = Convert.ToInt32("1");
            //}
            //if (loopEnd != null && loopEnd != "")
            //{
            //    model.loopEnd = Convert.ToInt32(loopEnd);
            //}
            //else
            //{
            //    model.loopEnd = Convert.ToInt32("5");
            //}
            ShowListModelCl model = new ShowListModelCl();
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            return PartialView("_ShowInActiveList", model);
        }
        public ActionResult GetInactiveClubbedGrid(JqGridRequest request, ShowListModelCl customModel)
        {


            ShowListModelCl model = new ShowListModelCl();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            //model.DepartmentId = customModel.DepartmentId;
            var result = (ShowListModelCl)Helper.ExecuteService("Notice", "GetInactiveQuestionList", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };
            //model.RoleName = "Vidhan Sabha Typist";
            //model = (AssignQuestionModel)Helper.ExecuteService("Notice", "GetUserCode", model);
            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<ShowListModelCl>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<ShowListModelCl>(Convert.ToString(questionList.QuestionID), new ShowListModelCl(questionList)));

            return new JqGridJsonResult() { Data = response };

        }

        public ActionResult GetClubbingDeatails(string questionID, string AssemblyID, string SessionID, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

#pragma warning disable CS0219 // The variable 'Month' is assigned but its value is never used
            string Month = "";
#pragma warning restore CS0219 // The variable 'Month' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Day' is assigned but its value is never used
            string Day = "";
#pragma warning restore CS0219 // The variable 'Day' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Year' is assigned but its value is never used
            string Year = "";
#pragma warning restore CS0219 // The variable 'Year' is assigned but its value is never used

            tMemberNotice model = new tMemberNotice();
            model.QuestionTypeId = 1;

            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.QuestionID = Convert.ToInt32(questionID);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDetailsForClubbing", model);
            foreach (var item in model.tQuestionModel)
            {

                model.Title = item.Subject;
                model.MemberName = item.MemberName;
                model.Details = item.MainQuestion;
                model.DiaryNo = item.DiaryNumber;

            }

            return PartialView("_ClubbingQuestionDetails", model);
        }


        public ActionResult GetCDeatailsQuestion(string questionID, string AssemblyID, string SessionID, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

#pragma warning disable CS0219 // The variable 'Month' is assigned but its value is never used
            string Month = "";
#pragma warning restore CS0219 // The variable 'Month' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Day' is assigned but its value is never used
            string Day = "";
#pragma warning restore CS0219 // The variable 'Day' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Year' is assigned but its value is never used
            string Year = "";
#pragma warning restore CS0219 // The variable 'Year' is assigned but its value is never used
            //PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            //RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            //loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            //loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tMemberNotice model = new tMemberNotice();

            //model.loopStart = Convert.ToInt16(loopStart);
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            //model.PageIndex = Convert.ToInt16(PageNumber);

            //Get the Questions related to Stared Question.
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;

            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.QuestionID = Convert.ToInt32(questionID);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetQuestionDetails", model);
            foreach (var item in model.tQuestionModel)
            {

                model.Title = item.Subject;
                model.MemberName = item.MemberName;
                model.Details = item.MainQuestion;
                model.DiaryNo = item.DiaryNumber;

            }

            return PartialView("_GetDetails", model);
        }

        public ActionResult GetInActiveDeatailsQuestion(string questionID, string AssemblyID, string SessionID, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

#pragma warning disable CS0219 // The variable 'Month' is assigned but its value is never used
            string Month = "";
#pragma warning restore CS0219 // The variable 'Month' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Day' is assigned but its value is never used
            string Day = "";
#pragma warning restore CS0219 // The variable 'Day' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Year' is assigned but its value is never used
            string Year = "";
#pragma warning restore CS0219 // The variable 'Year' is assigned but its value is never used


            tMemberNotice model = new tMemberNotice();

            model.QuestionTypeId = 1;

            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.QuestionID = Convert.ToInt32(questionID);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetInActiveQuestionDetails", model);
            foreach (var item in model.tQuestionModel)
            {

                model.Title = item.Subject;
                model.MemberName = item.MemberName;
                model.Details = item.MainQuestion;
                model.DiaryNo = item.DiaryNumber;

            }

            return PartialView("_GetInActiveDetails", model);
        }

        #region "Add Menthod to GetClubbed Questions for starred and  Unstarred added By Sunil on 28/07/2014"

        public ActionResult GetTotalstarredClubbedQuestions()
        {
            tMemberNotice ObjNotice = new tMemberNotice();
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            ObjNotice.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            ObjNotice.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);
            ObjNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetTotalStarredClubbedQuestion", ObjNotice);
            return PartialView("_TotalStarredClubbedQuestion", ObjNotice);
        }

        public ActionResult GetTotalUnstarredClubbedQuestions()
        {
            tMemberNotice ObjNotice = new tMemberNotice();
            ObjNotice.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            ObjNotice.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);
            ObjNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetTotalUnStarredClubbedQuestion", ObjNotice);
            return PartialView("_TotalUnstarredClubbedQuestion", ObjNotice);
        }




        public void GeneratePdfByQuesIdStarred_Fix(string QIds)
        {
            try
            {
                string str1 = "";
                string empty = string.Empty;
                tMemberNotice tMemberNotice = new tMemberNotice();
                string str2 = QIds;
                char[] chArray = new char[1] { ',' };
                foreach (string str3 in str2.Split(chArray))
                {
                    int int16 = (int)Convert.ToInt16(str3);
                    tMemberNotice.QuestionID = int16;
                    tMemberNotice.Updatedate = new DateTime?(DateTime.Now);
                    tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)tMemberNotice);
                    MemoryStream memoryStream = new MemoryStream();
                    EvoPdf.Document document = new EvoPdf.Document();
                    document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document.CompressionLevel = PdfCompressionLevel.Best;
                    document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                    if (tMemberNotice.tQuestionModel != null)
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            bool? isHindi = questionModelCustom.IsHindi;
                            bool flag = true;
                            if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                            {
                                str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n           नगर निगम प्रश्न   \r\n              </td>\r\n              </tr>\r\n      </table>";
                            }
                            else
                            {
                                str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:27px;' >\r\n            QUESTION FOR MUNCIPAL CORPORATION  \r\n              </td>\r\n              </tr>\r\n      </table>";
                            }
                        }
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            if (questionModelCustom.IsClubbed.HasValue)
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag1 = true;
                                string str4;
                                if (isHindi.GetValueOrDefault() == flag1 & isHindi.HasValue)
                                {
                                    string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                    }
                                }
                                else
                                {
                                    string[] strArray = questionModelCustom.CMemName.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                    }
                                }
                                isHindi = questionModelCustom.IsHindi;
                                bool flag2 = true;
                                if (isHindi.GetValueOrDefault() == flag2 & isHindi.HasValue)
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          प्रश्न संख्या: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          निश्चित दिनांक: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str4 + "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              D. No.: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Question Number: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          Fixed On: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str4 + "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be Pleased to state that:\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                            else
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag = true;
                                if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          प्रश्न संख्या: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          निश्चित दिनांक: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberNameLocal + "(" + questionModelCustom.ConstituencyName_Local + ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>     \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             Starred D. No.: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Question Number: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          Fixed On: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberName + "(" + questionModelCustom.ConstituencyName + ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be Pleased to state that:\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    string htmlStringBaseURL = "";
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                        }
                        str1 += "\r\n\r\n\r\n\r\n                   \r\n                \r\n                     </body> </html>";
                    }
                    byte[] buffer = document.Save();
                    try
                    {
                        memoryStream.Write(buffer, 0, buffer.Length);
                        memoryStream.Position = 0L;
                    }
                    finally
                    {
                        document.Close();
                    }
                    string str5 = "Question/BeforeFixation/" + tMemberNotice.AssemblyID.ToString() + "/" + tMemberNotice.SessionID.ToString() + "/";
                    string[] strArray1 = new string[6];
                    int num = tMemberNotice.AssemblyID;
                    strArray1[0] = num.ToString();
                    strArray1[1] = "_";
                    num = tMemberNotice.SessionID;
                    strArray1[2] = num.ToString();
                    strArray1[3] = "_S_";
                    num = tMemberNotice.QuestionID;
                    strArray1[4] = num.ToString();
                    strArray1[5] = ".pdf";
                    string path2 = string.Concat(strArray1);
                    this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                    string str6 = tMemberNotice.FilePath + str5;
                    if (!Directory.Exists(str6))
                        Directory.CreateDirectory(str6);
                    FileStream fileStream = new FileStream(Path.Combine(str6, path2), FileMode.Create, FileAccess.Write);
                    fileStream.Write(buffer, 0, buffer.Length);
                    fileStream.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GeneratePdfByQuesIdUnstarred_Fix(string QIds)
        {
            try
            {
                string str1 = "";
                string empty = string.Empty;
                tMemberNotice tMemberNotice = new tMemberNotice();
                string str2 = QIds;
                char[] chArray = new char[1] { ',' };
                foreach (string str3 in str2.Split(chArray))
                {
                    int int16 = (int)Convert.ToInt16(str3);
                    tMemberNotice.QuestionID = int16;
                    tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)tMemberNotice);
                    MemoryStream memoryStream = new MemoryStream();
                    EvoPdf.Document document = new EvoPdf.Document();
                    document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document.CompressionLevel = PdfCompressionLevel.Best;
                    document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                    if (tMemberNotice.tQuestionModel != null)
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            bool? isHindi = questionModelCustom.IsHindi;
                            bool flag = true;
                            if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                            {
                                str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n            नगर निगम प्रश्न   \r\n              </td>\r\n              </tr>\r\n      </table>";
                            }
                            else
                            {
                                str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n            QUESTION FOR MUNCIPAL CORPORATION   \r\n              </td>\r\n              </tr>\r\n      </table>";
                            }
                        }
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            if (questionModelCustom.IsClubbed.HasValue)
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag1 = true;
                                string str4;
                                if (isHindi.GetValueOrDefault() == flag1 & isHindi.HasValue)
                                {
                                    string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                    }
                                }
                                else
                                {
                                    string[] strArray = questionModelCustom.CMemName.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                    }
                                }
                                isHindi = questionModelCustom.IsHindi;
                                bool flag2 = true;
                                if (isHindi.GetValueOrDefault() == flag2 & isHindi.HasValue)
                                {
                                    string[] strArray = new string[22];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              डायरी संख्या: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          प्रश्न संख्या: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          निश्चित दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = str4;
                                    strArray[17] = "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                    strArray[18] = questionModelCustom.MinistryNameLocal;
                                    strArray[19] = " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[20] = questionModelCustom.MainQuestion;
                                    strArray[21] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    string[] strArray = new string[22];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              D. No.: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Question Number: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          Fixed On: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = str4;
                                    strArray[17] = "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align:top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                    strArray[18] = questionModelCustom.MinistryName;
                                    strArray[19] = " be Pleased to state that:\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         \r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[20] = questionModelCustom.MainQuestion;
                                    strArray[21] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                            else
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag = true;
                                if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                                {
                                    string[] strArray = new string[24];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              डायरी संख्या: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          प्रश्न संख्या: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          निश्चित दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = questionModelCustom.MemberNameLocal;
                                    strArray[17] = "(";
                                    strArray[18] = questionModelCustom.ConstituencyName_Local;
                                    strArray[19] = ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                    strArray[20] = questionModelCustom.MinistryNameLocal;
                                    strArray[21] = " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[22] = questionModelCustom.MainQuestion;
                                    strArray[23] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    string[] strArray = new string[24];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              D. No.: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          प्रश्न संख्या: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          निश्चित दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = questionModelCustom.MemberName;
                                    strArray[17] = "(";
                                    strArray[18] = questionModelCustom.ConstituencyName;
                                    strArray[19] = ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                    strArray[20] = questionModelCustom.MinistryName;
                                    strArray[21] = " be Pleased to state that:\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n<table style='width:80%'>\r\n     <tr>\r\n        <td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[22] = questionModelCustom.MainQuestion;
                                    strArray[23] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    string htmlStringBaseURL = "";
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                        }
                        str1 += "\r\n\r\n\r\n\r\n                   \r\n                \r\n                     </body> </html>";
                    }
                    byte[] buffer = document.Save();
                    try
                    {
                        memoryStream.Write(buffer, 0, buffer.Length);
                        memoryStream.Position = 0L;
                    }
                    finally
                    {
                        document.Close();
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        if (questionModelCustom.MainQuestion != null)
                        {
                            string str4 = "Question/BeforeFixation/" + tMemberNotice.AssemblyID.ToString() + "/" + tMemberNotice.SessionID.ToString() + "/";
                            string[] strArray = new string[6];
                            int num = tMemberNotice.AssemblyID;
                            strArray[0] = num.ToString();
                            strArray[1] = "_";
                            num = tMemberNotice.SessionID;
                            strArray[2] = num.ToString();
                            strArray[3] = "_U_";
                            num = tMemberNotice.QuestionID;
                            strArray[4] = num.ToString();
                            strArray[5] = ".pdf";
                            string path2 = string.Concat(strArray);
                            this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                            string str5 = tMemberNotice.FilePath + str4;
                            if (!Directory.Exists(str5))
                                Directory.CreateDirectory(str5);
                            FileStream fileStream = new FileStream(Path.Combine(str5, path2), FileMode.Create, FileAccess.Write);
                            fileStream.Write(buffer, 0, buffer.Length);
                            fileStream.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GeneratePdfByQuesIdStarred_Unfix(int QuestionId)
        {
            try
            {
                string str1 = "";
                string empty = string.Empty;
                tMemberNotice tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)new tMemberNotice()
                {
                    QuestionID = QuestionId,
                    Updatedate = new DateTime?(DateTime.Now)
                });
                MemoryStream memoryStream = new MemoryStream();
                EvoPdf.Document document = new EvoPdf.Document();
                document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document.CompressionLevel = PdfCompressionLevel.Best;
                document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                if (tMemberNotice.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? isHindi = questionModelCustom.IsHindi;
                        bool flag = true;
                        if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                        {
                            str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                            str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n            नगर निगम प्रश्न   \r\n              </td>\r\n              </tr>\r\n      </table>";
                        }
                        else
                        {
                            str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                            str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:27px;' >\r\n            QUESTION FOR MUNCIPAL CORPORATION  \r\n              </td>\r\n              </tr>\r\n      </table>";
                        }
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? nullable = questionModelCustom.IsClubbed;
                        if (nullable.HasValue)
                        {
                            nullable = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            string str2;
                            if (nullable.GetValueOrDefault() == flag1 & nullable.HasValue)
                            {
                                string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                str2 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str2 = str2 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                }
                            }
                            else
                            {
                                string[] strArray = questionModelCustom.CMemName.Split(',');
                                str2 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str2 = str2 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                }
                            }
                            nullable = questionModelCustom.IsHindi;
                            bool flag2 = true;
                            if (nullable.GetValueOrDefault() == flag2 & nullable.HasValue)
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str2 + "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             D. No.: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str2 + "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be Pleased to state that:\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                        else
                        {
                            nullable = questionModelCustom.IsHindi;
                            bool flag = true;
                            if (nullable.GetValueOrDefault() == flag & nullable.HasValue)
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberNameLocal + "(" + questionModelCustom.ConstituencyName_Local + ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>     \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              D. No.: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberName + "(" + questionModelCustom.ConstituencyName + ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be Pleased to state that:\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                string htmlStringBaseURL = "";
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                    }
                    string str3 = str1 + "\r\n\r\n\r\n\r\n                   \r\n                \r\n                     </body> </html>";
                }
                byte[] buffer = document.Save();
                try
                {
                    memoryStream.Write(buffer, 0, buffer.Length);
                    memoryStream.Position = 0L;
                }
                finally
                {
                    document.Close();
                }
                string str4 = "Question/BeforeFixation/" + tMemberNotice.AssemblyID.ToString() + "/" + tMemberNotice.SessionID.ToString() + "/";
                string[] strArray1 = new string[6];
                int num = tMemberNotice.AssemblyID;
                strArray1[0] = num.ToString();
                strArray1[1] = "_";
                num = tMemberNotice.SessionID;
                strArray1[2] = num.ToString();
                strArray1[3] = "_S_";
                num = tMemberNotice.QuestionID;
                strArray1[4] = num.ToString();
                strArray1[5] = ".pdf";
                string path2 = string.Concat(strArray1);
                this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                string str5 = tMemberNotice.FilePath + str4;
                if (!Directory.Exists(str5))
                    Directory.CreateDirectory(str5);
                FileStream fileStream = new FileStream(Path.Combine(str5, path2), FileMode.Create, FileAccess.Write);
                fileStream.Write(buffer, 0, buffer.Length);
                fileStream.Close();
                return str5 + path2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GeneratePdfByQuesIdUnstarred_Unfix(int QuestionId)
        {
            try
            {
                string str1 = "";
                string str2 = string.Empty;
                tMemberNotice tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)new tMemberNotice()
                {
                    QuestionID = QuestionId
                });
                MemoryStream memoryStream = new MemoryStream();
                EvoPdf.Document document = new EvoPdf.Document();
                document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document.CompressionLevel = PdfCompressionLevel.Best;
                document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                if (tMemberNotice.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? isHindi = questionModelCustom.IsHindi;
                        bool flag = true;
                        if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                        {
                            str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n        नगर निगम प्रश्न   \r\n              </td>\r\n              </tr>\r\n      </table>";
                        }
                        else
                        {
                            str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            str1 += "<div> \r\n                    </div>\r\n            \r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n            QUESTION FOR MUNCIPAL CORPORATION  \r\n              </td>\r\n              </tr>\r\n      </table>";
                        }
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? nullable = questionModelCustom.IsClubbed;
                        DateTime dateTime;
                        if (nullable.HasValue)
                        {
                            nullable = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            string str3;
                            if (nullable.GetValueOrDefault() == flag1 & nullable.HasValue)
                            {
                                string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                str3 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str3 = str3 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                }
                            }
                            else
                            {
                                string[] strArray = questionModelCustom.CMemName.Split(',');
                                str3 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str3 = str3 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                }
                            }
                            nullable = questionModelCustom.IsHindi;
                            bool flag2 = true;
                            if (nullable.GetValueOrDefault() == flag2 & nullable.HasValue)
                            {
                                string[] strArray = new string[18];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              डायरी संख्या: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = str3;
                                strArray[13] = "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                strArray[14] = questionModelCustom.MinistryNameLocal;
                                strArray[15] = " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[16] = questionModelCustom.MainQuestion;
                                strArray[17] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                string[] strArray = new string[18];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              D. No.: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = str3;
                                strArray[13] = "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align:top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                strArray[14] = questionModelCustom.MinistryName;
                                strArray[15] = " be Pleased to state that:\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         \r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[16] = questionModelCustom.MainQuestion;
                                strArray[17] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                        else
                        {
                            nullable = questionModelCustom.IsHindi;
                            bool flag = true;
                            if (nullable.GetValueOrDefault() == flag & nullable.HasValue)
                            {
                                string[] strArray = new string[20];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             डायरी संख्या: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = questionModelCustom.MemberNameLocal;
                                strArray[13] = "(";
                                strArray[14] = questionModelCustom.ConstituencyName_Local;
                                strArray[15] = ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                strArray[16] = questionModelCustom.MinistryNameLocal;
                                strArray[17] = " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[18] = questionModelCustom.MainQuestion;
                                strArray[19] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                string[] strArray = new string[20];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              D. No.: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = questionModelCustom.MemberName;
                                strArray[13] = "(";
                                strArray[14] = questionModelCustom.ConstituencyName;
                                strArray[15] = ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                strArray[16] = questionModelCustom.MinistryName;
                                strArray[17] = " be Pleased to state that:\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n<table style='width:80%'>\r\n     <tr>\r\n        <td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[18] = questionModelCustom.MainQuestion;
                                strArray[19] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                string htmlStringBaseURL = "";
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                    }
                    string str4 = str1 + "\r\n\r\n\r\n\r\n                   \r\n                \r\n                     </body> </html>";
                }
                byte[] buffer = document.Save();
                try
                {
                    memoryStream.Write(buffer, 0, buffer.Length);
                    memoryStream.Position = 0L;
                }
                finally
                {
                    document.Close();
                }
                foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                {
                    if (questionModelCustom.MainQuestion != null)
                    {
                        string[] strArray1 = new string[5]
                        {
              "Question/BeforeFixation/",
              null,
              null,
              null,
              null
                        };
                        int num = tMemberNotice.AssemblyID;
                        strArray1[1] = num.ToString();
                        strArray1[2] = "/";
                        num = tMemberNotice.SessionID;
                        strArray1[3] = num.ToString();
                        strArray1[4] = "/";
                        string str3 = string.Concat(strArray1);
                        string[] strArray2 = new string[6];
                        num = tMemberNotice.AssemblyID;
                        strArray2[0] = num.ToString();
                        strArray2[1] = "_";
                        num = tMemberNotice.SessionID;
                        strArray2[2] = num.ToString();
                        strArray2[3] = "_U_";
                        num = tMemberNotice.QuestionID;
                        strArray2[4] = num.ToString();
                        strArray2[5] = ".pdf";
                        string path2 = string.Concat(strArray2);
                        this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                        string str4 = tMemberNotice.FilePath + str3;
                        if (!Directory.Exists(str4))
                            Directory.CreateDirectory(str4);
                        FileStream fileStream = new FileStream(Path.Combine(str4, path2), FileMode.Create, FileAccess.Write);
                        fileStream.Write(buffer, 0, buffer.Length);
                        fileStream.Close();
                        str2 = str4 + path2;
                    }
                }
                return str2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        public ActionResult CutMotions(string DeptId, string AssemblyID, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            CutMotionModel model = new CutMotionModel();

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            //model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            //model.DepartmentId = DeptId;
            //model.AssemblyID = Convert.ToInt32(AssemblyID);
            //model.SessionID = Convert.ToInt32(SessionId);
            model = (CutMotionModel)Helper.ExecuteService("Notice", "GetCutMotionsForClubWithBracket", model);

            return PartialView("_CutMotionListForClub", model);
        }

        public ActionResult ClubbingCutMotion(string chkvals, string DepartmentId, string AssemblyID, string SessionID)
        {

            tMemberNotice model = new tMemberNotice();
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.NoticeId = Convert.ToInt32(chkvals);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCutMotionDetailsForMerging", model);
            foreach (var item in model.tQuestionModel)
            {
                model.Title += item.Subject + ",";
                model.MemberName += item.MemberName + ",";
                model.Details += item.Notice + ",";
                model.NoticeNumber += item.NoticeNumber + ",";
                string values = Convert.ToString(item.MemberId) + ",";
                model.MemberId = item.MemberId;
                model.MemIds += values;
                string Qval = Convert.ToString(item.NoticeId) + ",";
                model.QuesIds += Qval;
                model.EventId = item.NoticeTypeId;
                //model.OriDiaryFileName = item.OriDiaryFileName;
                //model.OriDiaryFilePath = item.OriDiaryFilePath;
                //model.TypistUserId = item.TypistUserId;
                model.SubInCatchWord = item.SubjectCatchWords;
                model.MinistryId = item.MinistryId;
            }
            model.Title = model.Title.TrimEnd(',');
            model.MemberName = model.MemberName.TrimEnd(',');
            model.Details = model.Details.TrimEnd(',');
            model.NoticeNumber = model.NoticeNumber.TrimEnd(',');
            model.MemIds = model.MemIds.TrimEnd(',');
            model.MemberId = model.MemberId;
            model.EventId = model.EventId;
            //model.OriDiaryFileName = model.OriDiaryFileName;
            //model.OriDiaryFilePath = model.OriDiaryFilePath;
            //model.TypistUserId = model.TypistUserId;
            model.QuesIds = model.QuesIds.TrimEnd(',');
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.DepartmentId = DepartmentId;
            model.SubInCatchWord = model.SubInCatchWord;
            model.MinistryId = model.MinistryId;
            model.NoticeId = Convert.ToInt32(chkvals);
            return PartialView("_GetMergeCutMotion", model);
        }



        public JsonResult SaveNewCutMotionMergeEntry(SaveValue model)
        {
            tMemberNotice Update = new tMemberNotice();
          //  Update.AssemblyID = model.AssemblyId;
           // Update.SessionID = model.SessionId;
          //  Update.QuestionTypeId = (int)QuestionType.StartedQuestion;
          //  Update.MemberId = Convert.ToInt32(model.SMem);
              Update.Notice = model.Details;
              Update.Subject = model.Title;
           //  Update.MemIds = model.MemIds;
          //  Update.QuesIds = model.QuestIDs;
          //  Update.DepartmentId = model.DepartmentId;
          //  Update.EventId = model.EventId;
         //   Update.TypistUserId = model.TypistUserId;
            //Update.OriDiaryFileName = model.OriDiaryFileName;
            //Update.OriDiaryFilePath = model.OriDiaryFilePath;
            //Update.SubInCatchWord = model.SubCatch;
          //  Update.MinistryId = model.MinistryId;
         //   Update.RoleName = CurrentSession.UserID;
          //  Update.DiaryNo = model.DiaryNo;
            Update.NoticeId = model.MId;
            Update.NoticeNumber = model.DiaryNo;
           // Update.Msg = model.SequenceDiaryN;
            Update.IsCMClubbedDate = DateTime.Now;
            Update.Msg = Helper.ExecuteService("Notice", "SaveMergeCutmotionsEntry", Update) as string;

            GeneratePdfForCutMotion(Update.NoticeId.ToString());

            //if (Update.Msg.Substring(0, 2) == "SF")
            //{
            //    GeneratePdfByQuesIdStarred_Fix(Update.QuestionID.ToString());
            //}
            //else if (Update.Msg.Substring(0, 2) == "SU")
            //{
            //    GeneratePdfByQuesIdStarred_Unfix(Update.QuestionID);
            //}
            //else if (Update.Msg.Substring(0, 2) == "UF")
            //{
            //    GeneratePdfByQuesIdUnstarred_Fix(Update.QuestionID.ToString());
            //}
            //else if (Update.Msg.Substring(0, 2) == "UU")
            //{
            //    GeneratePdfByQuesIdUnstarred_Unfix(Update.QuestionID);
            //}
            Update.Msg = Update.Msg;
            return Json(Update, JsonRequestBehavior.AllowGet);
        }



        public ActionResult GetCutMotionsClubbed(string DeptId, string AssemblyID, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            CutMotionModel model = new CutMotionModel();            
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model = (CutMotionModel)Helper.ExecuteService("Notice", "GetCutMotionsClubbed", model);

            return PartialView("_GetClubbedList", model);
        }
        public ActionResult ClubbedCutMotionForView(string chkvals, string DepartmentId, string AssemblyID, string SessionID)
        {

            tMemberNotice model = new tMemberNotice();
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.NoticeId = Convert.ToInt32(chkvals);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetclubbedCutMotions", model);
            //foreach (var item in model.tQuestionModel)
            //{
            //    model.Title += item.Subject + ",";
            //    model.MemberName += item.MemberName + ",";
            //    model.Details += item.Notice + ",";
            //    model.NoticeNumber += item.NoticeNumber + ",";
            //    string values = Convert.ToString(item.MemberId) + ",";
            //    model.MemberId = item.MemberId;
            //    model.MemIds += values;
            //    string Qval = Convert.ToString(item.NoticeId) + ",";
            //    model.QuesIds += Qval;
            //    model.EventId = item.NoticeTypeId;
            //    //model.OriDiaryFileName = item.OriDiaryFileName;
            //    //model.OriDiaryFilePath = item.OriDiaryFilePath;
            //    //model.TypistUserId = item.TypistUserId;
            //    model.SubInCatchWord = item.SubjectCatchWords;
            //    model.MinistryId = item.MinistryId;
            //}
            //model.Title = model.Title.TrimEnd(',');
            //model.MemberName = model.MemberName.TrimEnd(',');
            //model.Details = model.Details.TrimEnd(',');
            //model.NoticeNumber = model.NoticeNumber.TrimEnd(',');
            //model.MemIds = model.MemIds.TrimEnd(',');
            //model.MemberId = model.MemberId;
            //model.EventId = model.EventId;
            ////model.OriDiaryFileName = model.OriDiaryFileName;
            ////model.OriDiaryFilePath = model.OriDiaryFilePath;
            ////model.TypistUserId = model.TypistUserId;
            //model.QuesIds = model.QuesIds.TrimEnd(',');
            //model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            //model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            //model.DepartmentId = DepartmentId;
            //model.SubInCatchWord = model.SubInCatchWord;
            //model.MinistryId = model.MinistryId;
            //model.NoticeId = Convert.ToInt32(chkvals);
           
            return PartialView("_GetClubbedCutMotion", model);
        }





        public string GeneratePdfForCutMotion(string Id)
        {
            try
            {
                string outXml = "";
                string outInnerXml = "";
                string LOBName = Id;
                string fileName = "";
                string savedPDF = string.Empty;
#pragma warning disable CS0219 // The variable 'HeadingText' is assigned but its value is never used
                string HeadingText = "";
#pragma warning restore CS0219 // The variable 'HeadingText' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'HeadingTextEnglish' is assigned but its value is never used
                string HeadingTextEnglish = "";
#pragma warning restore CS0219 // The variable 'HeadingTextEnglish' is assigned but its value is never used
                string ViewBy = "";

                if (ViewBy == "CQ")
                {
                    HeadingText = "मौखिक उत्तर हेतु प्रश्न";
                    HeadingTextEnglish = "Starred Question";
                }
                else if (ViewBy == "PQ")
                {
                    HeadingText = "मौखिक उत्तर हेतु स्थगित प्रश्न";
                    HeadingTextEnglish = "Postponed Questions For Oral Answer";
                }

                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);



                tMemberNotice model = new tMemberNotice();
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
              //  string strDate = Id;
              //  DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
              //  model.NoticeDate = date;
              //  model.DataStatus = ViewBy;
                model.NoticeId = Convert.ToInt32(Id);
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetAllDetailsForCutmotionPdfpdf", model);


                //if (model.tQuestionModel != null)
                //{
                //    foreach (var item in model.tQuestionModel)
                //    {
                //        model.SessionDateId = item.SessionDateId;
                //        model.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", model);
                //        model.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", model);
                //    }
                //}
                //    model = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", model);

                if (model.tQuestionModel != null)
                {

                    //foreach (var item in model.tQuestionModel)
                    //{
                       
                            outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:40px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            outXml += @"<div> 

 <style type='text/css'>
@page {
    @bottom-left {
        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
        font-family: sans-serif;
        font-size: 80%;
    }
    @bottom-right {
        content: 'Page ' counter(page) ' of ' counter(pages);
        font-family: sans-serif;
        font-size: 80%;
    }
}
</style>
                    </div>
            
<table style='width:100%'>
 
      <tr>
          <td style='text-align:center;font-size:28px;' >
             <b>हिमाचल प्रदेश तेरहवीं विधान सभा</b>
              </td>
              </tr>
      </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
            <b>" + "(" + "स्वीकृत कटौती प्रस्ताव" + ")" + @"</b> 
              </td>
              </tr>
           </table>
<br />
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;'>
            <b>" + "बजट अनुमान 2019-2020 की अनुदान मांगें" + @"</b>          
              </td>    
              </tr>
           </table>
<table style='width:100%'>
      <tr>
          <td style='text-align:center;font-size:20px;' >
          <b>" + "कटौती प्रस्ताव की सूचना" + @"</b>   
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
      <tr>
          <td style='text-align:center;font-size:20px;' >
          <b>" + "मांग संख्या: " + model.CMDemandId + " - " + model.DemandName + @"</b>   
              </td>
         
              </tr>
           </table>
<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:20%; vertical-align: top; font-size:20px;cfont-weight: bold;text-align:left;'>
  <b>" + "क्र0 सं0" + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + "सदस्य का नाम" + @"</b>
</td>
<td style='width:20%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + "कटौती प्रस्ताव" + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;text-align:right;'>
<b>" + "मांग संख्या" + @"</b>
</td>
</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
 .............................................................................................................................
            
              </td>
              </tr>
           </table>";

                    foreach (var item in model.tQuestionModel)
                    {
                        if (item.IsClubbed != null)
                        {                          
                                string[] values = item.CMemNameHindi.Split(',');
                                outInnerXml = "";
                                for (int i = 0; i < values.Length; i++)
                                {
                                    values[i] = values[i].Trim();
                                    outInnerXml += @"<div>
          " + values[i] + @" :

                               </div>
                             ";
                                }

                                string MQues = WebUtility.HtmlDecode(item.Notice);
                                MQues = MQues.Replace("<p>", "");
                                MQues = MQues.Replace("</p>", "");
                                outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>

</td>
<td style='font-size:20px;padding-bottom: 20px;font-weight: bold;text-align:left;'>
<b>" + "1" + @"</b>
</td>

<td style='font-size:20px;padding-bottom: 20px;font-weight: bold;text-align:center;'>
<b>" + model.EventName + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 20px;font-weight: bold;text-align:right;'>
<b>" + model.CMDemandId + @"</b>
</td>
</tr>

<tr>
<td>
</td>
</tr>                   
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>

</td>
<td style='font-size:20px;padding-bottom: 13px;'>
</td>

<td style='font-size:20px;padding-bottom: 13px;text-align:center;'>
<b>" + item.Subject + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
</td>
</tr>

<tr>
<td>
</td>

</tr>

</table>



<table style='width:100%;text-align:left;'>
<tr>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>
</table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='font-size:20px;padding-bottom: 13px;width:200px'>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.Notice + @"</b>
</td>
</tr>
</table>
";

                      

                        }


                        if (model.StampTypeHindi == null)
                        {
                            // foreach (var item1 in model.Values)
                            //{

                                outXml += @"
 

<table style='width:100%'>
     <tr>
          <td style='text-align:left;font-size:20px;' >
      
<b>" + "शिमला-171004." + @"</b>
              </td>
<td style='text-align:right;font-size:20px;' >
                      <b>" + "सचिव" + @",</b>
              </td>
         
              </tr>
<tr>
          <td style='text-align:left;font-size:20px;' >
           
         <b>दिनांक " + ""  + @".</b>
              </td>
<td style='text-align:right;font-size:20px;' >
       
  <b>" + "विधान सभा।" + @"</b>          
              </td>
         
              </tr>
           </table>
       
                     </body> </html>";
                            //}
                        
                        }
                        else
                        {
                            foreach (var item1 in model.Values)
                            {

                                outXml += @"
 

<table style='width:100%'>
     <tr>
          <td style='text-align:left;font-size:20px;color:#3621E8;' >
      
<b>" + item.SignaturePlace + @".</b>
              </td>
<td style='text-align:right;font-size:20px;color:#3621E8;' >
                      <b>" + item.SignatureName + @",</b>
              </td>
         
              </tr>
<tr>
          <td style='text-align:left;font-size:20px;color:#3621E8;' >
           
         <b>Dated: " + item.SignatureDate + @".</b>
              </td>
<td style='text-align:right;font-size:20px;color:#3621E8;' >
       
  <b>" + item.SignatureDesignations + @".</b>          
              </td>
         
              </tr>
           </table>
       
                     </body> </html>";
                            }
                        }
                    }
                }


                MemoryStream output = new MemoryStream();

                PdfConverter pdfConverter = new PdfConverter();

                // set the license key
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";


                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 60;


                pdfConverter.PdfFooterOptions.FooterHeight = 60;
                //write the page number
                TextElement footerTextElement = new TextElement(0, 30, "&p;",
                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }

                string url = "Question" + "/CutMotion/" + model.AssemblyID + "/" + model.SessionID + "/";
                fileName = model.AssemblyID + "_" + model.SessionID + "_CutMotion_" + model.NoticeId + ".pdf";
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                //string directory = Server.MapPath(url);
                string directory = model.FilePath + url;
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                //var path = Path.Combine(Server.MapPath("~" + url), fileName);
                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);


         
                // close file stream
                _FileStream.Close();
                model.FilePath = url;
                model.FileName =  fileName;

                // DocFilePath is using for Accessing File 
               // CurrentSession.FileAccessPath = model.DocFilePath;
              //  var AccessPath = "/" + url + fileName;
                model.Msg = Helper.ExecuteService("Notice", "UpdateFilePathandName", model) as string;


                return "Success" ;
            }

            catch (Exception ex)
            {

                //throw ex;
                return ex.Message;
            }
            finally
            {

            }


        }


        public ActionResult NoticeListBracketted(string DeptId, string AssemblyID, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            CutMotionModel model = new CutMotionModel();

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            //model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            //model.DepartmentId = DeptId;
            //model.AssemblyID = Convert.ToInt32(AssemblyID);
            //model.SessionID = Convert.ToInt32(SessionId);
            model = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesForClubWithBracket", model);

            return PartialView("_NoticesListforClub", model);
        }

        public ActionResult ClubbingNotices(string chkvals, string DepartmentId, string AssemblyID, string SessionID)
        {

            tMemberNotice model = new tMemberNotice();
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.NoticeId = Convert.ToInt32(chkvals);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCutMotionDetailsForMerging", model);
            foreach (var item in model.tQuestionModel)
            {
                model.Title += item.Subject + ",";
                model.MemberName += item.MemberName + ",";
                model.Details += item.Notice + ",";
                model.NoticeNumber += item.NoticeNumber + ",";
                string values = Convert.ToString(item.MemberId) + ",";
                model.MemberId = item.MemberId;
                model.MemIds += values;
                string Qval = Convert.ToString(item.NoticeId) + ",";
                model.QuesIds += Qval;
                model.EventId = item.NoticeTypeId;
                //model.OriDiaryFileName = item.OriDiaryFileName;
                //model.OriDiaryFilePath = item.OriDiaryFilePath;
                //model.TypistUserId = item.TypistUserId;
                model.SubInCatchWord = item.SubjectCatchWords;
                model.MinistryId = item.MinistryId;
            }
            model.Title = model.Title.TrimEnd(',');
            model.MemberName = model.MemberName.TrimEnd(',');
            model.Details = model.Details.TrimEnd(',');
            model.NoticeNumber = model.NoticeNumber.TrimEnd(',');
            model.MemIds = model.MemIds.TrimEnd(',');
            model.MemberId = model.MemberId;
            model.EventId = model.EventId;
            //model.OriDiaryFileName = model.OriDiaryFileName;
            //model.OriDiaryFilePath = model.OriDiaryFilePath;
            //model.TypistUserId = model.TypistUserId;
            model.QuesIds = model.QuesIds.TrimEnd(',');
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.DepartmentId = DepartmentId;
            model.SubInCatchWord = model.SubInCatchWord;
            model.MinistryId = model.MinistryId;
            model.NoticeId = Convert.ToInt32(chkvals);
            return PartialView("_GetMergeNoticesforclub", model);
        }

        public ActionResult GetNoticesClubbed(string DeptId, string AssemblyID, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            CutMotionModel model = new CutMotionModel();
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesClubbed", model);

            return PartialView("_GetCluubedNotices", model);
        }

        public ActionResult ClubbedNoticesForView(string chkvals, string DepartmentId, string AssemblyID, string SessionID)
        {

            tMemberNotice model = new tMemberNotice();
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.NoticeId = Convert.ToInt32(chkvals);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetclubbedCutMotions", model);
            //foreach (var item in model.tQuestionModel)
            //{
            //    model.Title += item.Subject + ",";
            //    model.MemberName += item.MemberName + ",";
            //    model.Details += item.Notice + ",";
            //    model.NoticeNumber += item.NoticeNumber + ",";
            //    string values = Convert.ToString(item.MemberId) + ",";
            //    model.MemberId = item.MemberId;
            //    model.MemIds += values;
            //    string Qval = Convert.ToString(item.NoticeId) + ",";
            //    model.QuesIds += Qval;
            //    model.EventId = item.NoticeTypeId;
            //    //model.OriDiaryFileName = item.OriDiaryFileName;
            //    //model.OriDiaryFilePath = item.OriDiaryFilePath;
            //    //model.TypistUserId = item.TypistUserId;
            //    model.SubInCatchWord = item.SubjectCatchWords;
            //    model.MinistryId = item.MinistryId;
            //}
            //model.Title = model.Title.TrimEnd(',');
            //model.MemberName = model.MemberName.TrimEnd(',');
            //model.Details = model.Details.TrimEnd(',');
            //model.NoticeNumber = model.NoticeNumber.TrimEnd(',');
            //model.MemIds = model.MemIds.TrimEnd(',');
            //model.MemberId = model.MemberId;
            //model.EventId = model.EventId;
            ////model.OriDiaryFileName = model.OriDiaryFileName;
            ////model.OriDiaryFilePath = model.OriDiaryFilePath;
            ////model.TypistUserId = model.TypistUserId;
            //model.QuesIds = model.QuesIds.TrimEnd(',');
            //model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            //model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            //model.DepartmentId = DepartmentId;
            //model.SubInCatchWord = model.SubInCatchWord;
            //model.MinistryId = model.MinistryId;
            //model.NoticeId = Convert.ToInt32(chkvals);

            return PartialView("_GetClubbedNoticeforView", model);
        }
        public JsonResult SaveNewNoticeEntry(SaveValue model)
        {
            tMemberNotice Update = new tMemberNotice();
         
            Update.Notice = model.Details;
            Update.Subject = model.Title;         
            Update.NoticeId = model.MId;
            Update.NoticeNumber = model.DiaryNo;
            Update.IsCMClubbedDate = DateTime.Now;
            Update.AssemblyID = model.AssemblyId;
            Update.SessionID = model.SessionId;      
            Update = (tMemberNotice)Helper.ExecuteService("Notice", "SaveMergeNoticeEntry", Update);
            return Json(Update, JsonRequestBehavior.AllowGet);
        }




    }



    [Serializable]
    public class SaveValue
    {
        public SaveValue()
        {
        }
        public string MemIds { get; set; }
        public string QuestIDs { get; set; }
        public string SMem { get; set; }

        [AllowHtml]
        public string Title { get; set; }
        [AllowHtml]
        public string Details { get; set; }
        public int SessionId { get; set; }
        public int AssemblyId { get; set; }
        public string DepartmentId { get; set; }
        public string TypistUserId { get; set; }
        public int EventId { get; set; }
        public string OriDiaryFileName { get; set; }
        public string OriDiaryFilePath { get; set; }
        public string SubCatch { get; set; }
        public int MinistryId { get; set; }
        public string DiaryNo { get; set; }
        public int MId { get; set; }
        public string SequenceDiaryN { get; set; }
    }
}
