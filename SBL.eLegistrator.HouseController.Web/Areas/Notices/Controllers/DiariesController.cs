﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Lib.Web.Mvc.JQuery.JqGrid;
using Microsoft.Security.Application;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using SBL.DomainModel.Models.SystemModule;
using SBL.DomainModel.Models.RecipientGroups;
using SMS.API;
using Email.API;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
	[Audit]
	[NoCache]
	[SBLAuthorize(Allow = "Authenticated")]
	public class DiariesController : Controller
	{
		//
		// GET: /Notices/Diaries/

		#region "Create Diary for particular paper  created By Sunil on 12/05/2014"

		public ActionResult DiariesDashboard()
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModel DMdl = new DiaryModel();
				DiaryModel DMDl2 = new DiaryModel();


				if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
				{
					DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
					DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
				}

				DMDl2 = (DiaryModel)Helper.ExecuteService("Diary", "GetInitialCount", DMdl);

				DMdl = DMDl2;

				if (string.IsNullOrEmpty(CurrentSession.AssemblyId) || string.IsNullOrEmpty(CurrentSession.SessionId))
				{
					CurrentSession.AssemblyId = DMdl.AssemblyID.ToString();
					CurrentSession.SessionId = DMdl.SessionID.ToString();
				}

				mDepartment deptInfo = new mDepartment();

				deptInfo.deptId = CurrentSession.DeptID;
				deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
				if (deptInfo != null)
				{
					DMdl.DepartmentName = deptInfo.deptname;
				}
				else
				{

				}


				DMdl.CurrentUserName = CurrentSession.UserName;
				DMdl.UserDesignation = CurrentSession.MemberDesignation;
				DMdl.UserName = CurrentSession.Name;

				if (Convert.ToString(TempData["Msg"]) != "")
				{
					DMdl.Message = TempData["Msg"].ToString();
				}
				else
				{
					DMdl.Message = "";
				}

				if (Convert.ToString(TempData["PaperEntryType"]) != "")
				{
					DMdl.PaperEntryType = TempData["PaperEntryType"].ToString();
				}
				else
				{
					DMdl.PaperEntryType = "";
				}


				return View(DMdl);
			}
			else
			{
				return RedirectToAction("Login", "Account", new { @area = "" });
			}
		}
		public ActionResult GetAllList()
		{

			if (!String.IsNullOrEmpty(CurrentSession.UserID))
			{
				DiaryModel DMdl = new DiaryModel();
				return PartialView("_NewDiaryGetAllList", DMdl);
			}
			else
			{
				return RedirectToAction("LoginUP", "Account", new { @area = "" });

			}
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult NewDiaryGetAllList(JqGridRequest request)
		{
			DiaryModelCustom DMdl = new DiaryModelCustom();
			DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
			DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);

			DMdl.PageSize = request.RecordsCount;
			DMdl.PageIndex = request.PageIndex + 1;

			var result = (DiaryModelCustom)Helper.ExecuteService("Diary", "GetAllDiaryList", DMdl);

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.DiaryList.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<DiaryModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from diaryList in resultForGrid
									  select new JqGridRecord<DiaryModelCustom>(Convert.ToString(diaryList.UniqueId), new DiaryModelCustom(diaryList)));

			return new JqGridJsonResult() { Data = response };
		}
		public ActionResult GetStarredDiary(int AssemId, int SessId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModel DMdl = new DiaryModel();
				//DiaryModel DMdl2 = new DiaryModel();

				//PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
				//RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
				//loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
				//loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

				//DMdl.loopStart = Convert.ToInt16(loopStart);
				//DMdl.loopEnd = Convert.ToInt16(loopEnd);
				//DMdl.PageSize = Convert.ToInt16(RowsPerPage);
				//DMdl.PageIndex = Convert.ToInt16(PageNumber);

				//if (AssemId != 0)
				//{
				//    DMdl.AssemblyID = AssemId;
				//}

				//if (SessId != 0)
				//{
				//    DMdl.SessionID = SessId;
				//}

				//DMdl2 = Helper.ExecuteService("Diary", "GetStarredDiary", DMdl) as DiaryModel;
				//DMdl.DiaryList = DMdl2.DiaryList;
				//DMdl.ResultCount = DMdl2.ResultCount;

				//if (PageNumber != null && PageNumber != "")
				//{
				//    DMdl.PageNumber = Convert.ToInt32(PageNumber);
				//}
				//else
				//{
				//    DMdl.PageNumber = Convert.ToInt32("1");
				//}
				//if (RowsPerPage != null && RowsPerPage != "")
				//{
				//    DMdl.RowsPerPage = Convert.ToInt32(RowsPerPage);
				//}
				//else
				//{
				//    DMdl.RowsPerPage = Convert.ToInt32("10");
				//}
				//if (PageNumber != null && PageNumber != "")
				//{
				//    DMdl.selectedPage = Convert.ToInt32(PageNumber);
				//}
				//else
				//{
				//    DMdl.selectedPage = Convert.ToInt32("1");
				//}
				//if (loopStart != null && loopStart != "")
				//{
				//    DMdl.loopStart = Convert.ToInt32(loopStart);
				//}
				//else
				//{
				//    DMdl.loopStart = Convert.ToInt32("1");
				//}
				//if (loopEnd != null && loopEnd != "")
				//{
				//    DMdl.loopEnd = Convert.ToInt32(loopEnd);
				//}
				//else
				//{
				//    DMdl.loopEnd = Convert.ToInt32("5");
				//}
				return PartialView("_GetStarredDiaryList", DMdl);
			}
			else
			{
				//return RedirectToAction("Login", "Account", new { @area = "" });
				return RedirectToAction("DiariesDashboard", "Diaries");
			}

		}
        public ActionResult Test()
        {
            return View();
        }
		public ActionResult GetStarredDiaryJqGrid(JqGridRequest request)
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModelCustom DMdl = new DiaryModelCustom();

				DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
				DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);

				DMdl.PageSize = request.RecordsCount;
				DMdl.PageIndex = request.PageIndex + 1;

				var result = (DiaryModelCustom)Helper.ExecuteService("Diary", "GetStarredDiary", DMdl);


				JqGridResponse response = new JqGridResponse()
				{
					TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
					PageIndex = request.PageIndex,
					TotalRecordsCount = result.ResultCount
				};

				var resultToSort = result.DiaryList.AsQueryable();

				var resultForGrid = resultToSort.OrderBy<DiaryModelCustom>(request.SortingName, request.SortingOrder.ToString());

				response.Records.AddRange(from diaryList in resultForGrid
										  select new JqGridRecord<DiaryModelCustom>(Convert.ToString(diaryList.QuestionId), new DiaryModelCustom(diaryList)));

				return new JqGridJsonResult() { Data = response };

			}
			else
			{

				return RedirectToAction("DiariesDashboard", "Diaries");
			}

		}

		public ActionResult GetPPQuestionsByType(int AssemId, int SessId, string Qtype)
		{
			DiaryModel DMdl = new DiaryModel();
			DMdl.AssemblyID = AssemId;
			DMdl.SessionID = SessId;
			if (Qtype == "Starred")
			{
				DMdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
			}
			else if (Qtype == "Unstarred")
			{
				DMdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
			}
			DMdl = (DiaryModel)Helper.ExecuteService("Diary", "GetPPQuestionsByType", DMdl);
			return PartialView("_PPQuestionsList", DMdl);

		}

        public ActionResult GetPPNoticeType(int AssemId, int SessId)
        {
            DiaryModel DMdl = new DiaryModel();
            DMdl.AssemblyID = AssemId;
            DMdl.SessionID = SessId;

            DMdl = (DiaryModel)Helper.ExecuteService("Diary", "GetPPNoticesByType", DMdl);
            return PartialView("_PPNoticeList", DMdl);

        }


		public ActionResult GetUnstarredDiary(int AssemId, int SessId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModel DMdl = new DiaryModel();
				//DiaryModel DMdl2 = new DiaryModel();

				//PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
				//RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
				//loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
				//loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

				//DMdl.loopStart = Convert.ToInt16(loopStart);
				//DMdl.loopEnd = Convert.ToInt16(loopEnd);
				//DMdl.PageSize = Convert.ToInt16(RowsPerPage);
				//DMdl.PageIndex = Convert.ToInt16(PageNumber);

				//if (AssemId != 0)
				//{
				//    DMdl.AssemblyID = AssemId;
				//}

				//if (SessId != 0)
				//{
				//    DMdl.SessionID = SessId;
				//}

				//DMdl2 = Helper.ExecuteService("Diary", "GetUnstarredDiary", DMdl) as DiaryModel;
				//DMdl.DiaryList = DMdl2.DiaryList;
				//DMdl.ResultCount = DMdl2.ResultCount;

				//if (PageNumber != null && PageNumber != "")
				//{
				//    DMdl.PageNumber = Convert.ToInt32(PageNumber);
				//}
				//else
				//{
				//    DMdl.PageNumber = Convert.ToInt32("1");
				//}
				//if (RowsPerPage != null && RowsPerPage != "")
				//{
				//    DMdl.RowsPerPage = Convert.ToInt32(RowsPerPage);
				//}
				//else
				//{
				//    DMdl.RowsPerPage = Convert.ToInt32("10");
				//}
				//if (PageNumber != null && PageNumber != "")
				//{
				//    DMdl.selectedPage = Convert.ToInt32(PageNumber);
				//}
				//else
				//{
				//    DMdl.selectedPage = Convert.ToInt32("1");
				//}
				//if (loopStart != null && loopStart != "")
				//{
				//    DMdl.loopStart = Convert.ToInt32(loopStart);
				//}
				//else
				//{
				//    DMdl.loopStart = Convert.ToInt32("1");
				//}
				//if (loopEnd != null && loopEnd != "")
				//{
				//    DMdl.loopEnd = Convert.ToInt32(loopEnd);
				//}
				//else
				//{
				//    DMdl.loopEnd = Convert.ToInt32("5");
				//}
				return PartialView("_GetUnstarredDiary", DMdl);

			}
			else
			{
				//return RedirectToAction("Login", "Account", new { @area = "" });
				return RedirectToAction("DiariesDashboard", "Diaries");
			}

		}

		public ActionResult GetUnstarredDiaryJqGrid(JqGridRequest request)
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModelCustom DMdl = new DiaryModelCustom();

				DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
				DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
				DMdl.PageSize = request.RecordsCount;
				DMdl.PageIndex = request.PageIndex + 1;


				var result = (DiaryModelCustom)Helper.ExecuteService("Diary", "GetUnstarredDiary", DMdl);


				JqGridResponse response = new JqGridResponse()
				{
					TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
					PageIndex = request.PageIndex,
					TotalRecordsCount = result.ResultCount
				};

				var resultToSort = result.DiaryList.AsQueryable();

				var resultForGrid = resultToSort.OrderBy<DiaryModelCustom>(request.SortingName, request.SortingOrder.ToString());

				response.Records.AddRange(from diaryList in resultForGrid
										  select new JqGridRecord<DiaryModelCustom>(Convert.ToString(diaryList.QuestionId), new DiaryModelCustom(diaryList)));

				return new JqGridJsonResult() { Data = response };

			}
			else
			{
				//return RedirectToAction("Login", "Account", new { @area = "" });
				return RedirectToAction("DiariesDashboard", "Diaries");
			}

		}

		public ActionResult GetNoticesDiary(int AssemId, int SessId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModel DMdl = new DiaryModel();
				//DiaryModel DMdl2 = new DiaryModel();

				//PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
				//RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
				//loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
				//loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

				//DMdl.loopStart = Convert.ToInt16(loopStart);
				//DMdl.loopEnd = Convert.ToInt16(loopEnd);
				//DMdl.PageSize = Convert.ToInt16(RowsPerPage);
				//DMdl.PageIndex = Convert.ToInt16(PageNumber);

				//if (AssemId != 0)
				//{
				//    DMdl.AssemblyID = AssemId;
				//}

				//if (SessId != 0)
				//{
				//    DMdl.SessionID = SessId;
				//}

				//DMdl2 = Helper.ExecuteService("Diary", "GetNoticesDiary", DMdl) as DiaryModel;
				//DMdl.DiaryList = DMdl2.DiaryList;
				//DMdl.ResultCount = DMdl2.ResultCount;

				//if (PageNumber != null && PageNumber != "")
				//{
				//    DMdl.PageNumber = Convert.ToInt32(PageNumber);
				//}
				//else
				//{
				//    DMdl.PageNumber = Convert.ToInt32("1");
				//}
				//if (RowsPerPage != null && RowsPerPage != "")
				//{
				//    DMdl.RowsPerPage = Convert.ToInt32(RowsPerPage);
				//}
				//else
				//{
				//    DMdl.RowsPerPage = Convert.ToInt32("10");
				//}
				//if (PageNumber != null && PageNumber != "")
				//{
				//    DMdl.selectedPage = Convert.ToInt32(PageNumber);
				//}
				//else
				//{
				//    DMdl.selectedPage = Convert.ToInt32("1");
				//}
				//if (loopStart != null && loopStart != "")
				//{
				//    DMdl.loopStart = Convert.ToInt32(loopStart);
				//}
				//else
				//{
				//    DMdl.loopStart = Convert.ToInt32("1");
				//}
				//if (loopEnd != null && loopEnd != "")
				//{
				//    DMdl.loopEnd = Convert.ToInt32(loopEnd);
				//}
				//else
				//{
				//    DMdl.loopEnd = Convert.ToInt32("5");
				//}
				return PartialView("_GetNoticesDiary", DMdl);

			}
			else
			{
				//return RedirectToAction("Login", "Account", new { @area = "" });
				return RedirectToAction("DiariesDashboard", "Diaries");
			}

		}

		public ActionResult GetNoticesDiaryJqGrid(JqGridRequest request)
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModelCustom DMdl = new DiaryModelCustom();
				DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
				DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);

				DMdl.PageSize = request.RecordsCount;
				DMdl.PageIndex = request.PageIndex + 1;

				var result = (DiaryModelCustom)Helper.ExecuteService("Diary", "GetNoticesDiary", DMdl);


				JqGridResponse response = new JqGridResponse()
				{
					TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
					PageIndex = request.PageIndex,
					TotalRecordsCount = result.ResultCount
				};

				var resultToSort = result.DiaryList.AsQueryable();

				var resultForGrid = resultToSort.OrderBy<DiaryModelCustom>(request.SortingName, request.SortingOrder.ToString());

				response.Records.AddRange(from diaryList in resultForGrid
										  select new JqGridRecord<DiaryModelCustom>(Convert.ToString(diaryList.NoticeId), new DiaryModelCustom(diaryList)));

				return new JqGridJsonResult() { Data = response };

			}
			else
			{
				//return RedirectToAction("Login", "Account", new { @area = "" });
				return RedirectToAction("DiariesDashboard", "Diaries");
			}

		}

		public ActionResult GetPaperEntry(int AssemId, int SessId, string EntryType)
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModel DMdl = new DiaryModel();
				DiaryModel DMdl2 = new DiaryModel();

				if (AssemId != 0)
				{
					DMdl.AssemblyID = AssemId;
				}

				if (SessId != 0)
				{
					DMdl.SessionID = SessId;
				}
				if (EntryType != "" && EntryType != null)
				{
					DMdl.PaperEntryType = EntryType;
				}

				DMdl2 = Helper.ExecuteService("Diary", "GetPaperEntry", DMdl) as DiaryModel;

				DMdl.eventList = DMdl2.eventList;
				DMdl.memberList = DMdl2.memberList;
				DMdl.memMinList = DMdl2.memMinList;
				DMdl.DiaryList = DMdl2.DiaryList;
				DMdl.stringDate = DateTime.Now.ToString("dd/MM/yyyy");

				if (EntryType == "Starred")
				{
					DMdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
                    DMdl.MemberId = DMdl2.MemberId;
                    DMdl.ConstituencyCode = DMdl2.ConstituencyCode;
                    DMdl.ConstituencyName = DMdl2.ConstituencyName;
				}
				else if (EntryType == "Unstarred")
				{
					DMdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                    DMdl.MemberId = DMdl2.MemberId;
                    DMdl.ConstituencyCode = DMdl2.ConstituencyCode;
                    DMdl.ConstituencyName = DMdl2.ConstituencyName;
				}
                else if (EntryType == "Notice")
                {                 
                    DMdl.MemberId = DMdl2.MemberId;
                 
                }

				return PartialView("_GetPaperEntry", DMdl);
			}
			else
			{
				//return RedirectToAction("Login", "Account", new { @area = "" });
				return RedirectToAction("DiariesDashboard", "Diaries");
			}
		}
		[HttpPost, ValidateAntiForgeryToken]
        [ValidateInput(false)]
		public ActionResult SavePaperEntry(DiaryModel Dmdl)
		{


			string Msg = "";
			//string Url = "";
			//string returnMsg = "";

			//Url = "/OriginalDiaryFile/" + Dmdl.AssemblyID + "/" + Dmdl.SessionID + "/";

			//if (file != null)
			//{
			//    Dmdl.FilePath = Url;
			//}

			//CultureInfo provider = CultureInfo.InvariantCulture;
			//provider = new CultureInfo("fr-FR");
			//var Date = DateTime.ParseExact(Dmdl.stringDate, "d", provider);
			//Dmdl.NoticeDate = Convert.ToDateTime(Date);

			if (Dmdl.IsQuestionInPart == null)
			{
				Dmdl.IsQuestionInPart = false;
			}


			if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageCulture == "hi-IN")
			{
				var Date = DateTime.ParseExact(Dmdl.stringDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
				Dmdl.NoticeDate = Convert.ToDateTime(Date);
			}
			else
			{
				var Date = DateTime.ParseExact(Dmdl.stringDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
				Dmdl.NoticeDate = Convert.ToDateTime(Date);
			}

			if (Dmdl.PaperEntryType != "Notice")
			{
				Dmdl.DiaryNumber = Helper.ExecuteService("Diary", "CheckDiaryNumber", Dmdl) as string;

			}
			else
			{
				Dmdl.NoticeNumber = Helper.ExecuteService("Diary", "CheckNoticeNumber", Dmdl) as string;
			}


			var TimeSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "TimeBarredSettings", null);
			Dmdl.TimeBarredDt = TimeSettings.SettingValue;
			Dmdl.MatchDateTimeBarred = Convert.ToDateTime(Dmdl.TimeBarredDt);
			Msg = Helper.ExecuteService("Diary", "SavePaperEntry", Dmdl) as string;


			if (Dmdl.PaperEntryType != "Notice")
			{
				Dmdl.QuestionId = Convert.ToInt16(Msg.Substring(0, Msg.IndexOf("_")));

			}
			else
			{
				Dmdl.NoticeId = Convert.ToInt16(Msg.Substring(0, Msg.IndexOf("_")));
			}

			Dmdl.TimeForBind = Dmdl.NoticeTime;
			if (Dmdl.TimeForBind != null)
			{
				TimeSpan timespan = new TimeSpan(Dmdl.TimeForBind.Value.Hours, Dmdl.TimeForBind.Value.Minutes, 00);
				DateTime dtTemp = DateTime.ParseExact(timespan.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
				Dmdl.StringTime = dtTemp.ToString("hh:mm tt");
			}

			//Show Message 
			if (Dmdl.PaperEntryType == "Starred")
			{
				TempData["Msg"] = "Last : Question Diaried Successfully and Diary Number is " + Dmdl.DiaryNumber + " at " + Dmdl.StringTime;
				TempData["PaperEntryType"] = "Starred";
			}
			else if (Dmdl.PaperEntryType == "Unstarred")
			{
				TempData["Msg"] = "Last : Unstarred Question Diaried Successfully and Diary Number is " + Dmdl.DiaryNumber + " at " + Dmdl.StringTime;
				TempData["PaperEntryType"] = "Unstarred";
			}
			else if (Dmdl.PaperEntryType == "Notice")
			{
				TempData["Msg"] = "Last : Notice Diaried Successfully and Diary Number is " + Dmdl.NoticeNumber + " at " + Dmdl.StringTime;
				TempData["PaperEntryType"] = "Notice";
			}


			return RedirectToAction("DiariesDashboard");
		}

		public string SavefileToDirectory(HttpPostedFileBase file, DiaryModel Dmdl, string Url)
		{
			if (file != null)
			{
				try
				{
					//Save file in filesystem
					DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~" + Url));
					if (!Dir.Exists)
					{
						Dir.Create();
					}
					if (Dmdl.PaperEntryType != "Notice")
					{
						if (Dmdl.QuestionTypeId == 1)
						{
							file.SaveAs(Server.MapPath("~" + Url + Dmdl.AssemblyID + "_" + Dmdl.SessionID + "_S_Org_" + Dmdl.QuestionId + ".pdf"));
						}

						else if (Dmdl.QuestionTypeId == 2)
						{
							file.SaveAs(Server.MapPath("~" + Url + Dmdl.AssemblyID + "_" + Dmdl.SessionID + "_U_Org_" + Dmdl.QuestionId + ".pdf"));
						}
					}
					else if (Dmdl.PaperEntryType == "Notice")
					{
						file.SaveAs(Server.MapPath("~" + Url + Dmdl.AssemblyID + "_" + Dmdl.SessionID + "_N_Org_" + Dmdl.NoticeId + ".pdf"));
					}

					return "Success";
				}


				catch (Exception ex)
				{
					return ex.Message;
				}
			}
			return null;
		}

        public string Savefile(HttpPostedFileBase file, int QuestionId, int NoticeId, int QuestionTypeId, int AssemblyID, int SessionID, bool IsOnline, bool IsQuestionInPart)
		{

			string Msg = "";
			DiaryModel Dmdl = new DiaryModel();


			if (file != null)
			{
				try
				{

					//Save file in filesystem
					string Url = "OriginalDiaryFile/" + AssemblyID + "/" + SessionID + "/";

					Dmdl.FilePath = Url;
					Dmdl.AssemblyID = AssemblyID;
					Dmdl.SessionID = SessionID;
					if (IsOnline == true)
					{
						Dmdl.IsSubmitToVS = true;
						if (CurrentSession.LanguageCulture == "hi-IN")
						{
							var Date = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
							Dmdl.NoticeDate = Convert.ToDateTime(Date);
						}
						else
						{
							var Date = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);
							Dmdl.NoticeDate = Convert.ToDateTime(Date);
						}
					}

					Dmdl.IsQuestionInPart = IsQuestionInPart;
                    
					//DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~" + Url));
					DirectoryInfo Dir = new DirectoryInfo(CurrentSession.FileStorePath + Url);
					if (!Dir.Exists)
					{
						Dir.Create();
					}
					if (QuestionId != 0)
					{
						Dmdl.QuestionId = QuestionId;

						Msg = Helper.ExecuteService("Diary", "SaveFile", Dmdl) as string;

						if (QuestionTypeId == 1)
						{
							//file.SaveAs(Server.MapPath("~" + Url + AssemblyID + "_" + SessionID + "_S_Org_" + QuestionId + ".pdf"));
							file.SaveAs(CurrentSession.FileStorePath + Url + AssemblyID + "_" + SessionID + "_S_Org_" + QuestionId + ".pdf");
						}

						else if (QuestionTypeId == 2)
						{
							//file.SaveAs(Server.MapPath("~" + Url + AssemblyID + "_" + SessionID + "_U_Org_" + QuestionId + ".pdf"));
							file.SaveAs(CurrentSession.FileStorePath + Url + AssemblyID + "_" + SessionID + "_U_Org_" + QuestionId + ".pdf");
						}
					}
					else if (NoticeId != 0)
					{
						Dmdl.NoticeId = NoticeId;

						Msg = Helper.ExecuteService("Diary", "SaveFile", Dmdl) as string;

						//file.SaveAs(Server.MapPath("~" + Url + AssemblyID + "_" + SessionID + "_N_Org_" + NoticeId + ".pdf"));
						file.SaveAs(CurrentSession.FileStorePath + Url + AssemblyID + "_" + SessionID + "_N_Org_" + NoticeId + ".pdf");
					}

					return Msg;
				}


				catch (Exception ex)
				{
					return ex.Message;
				}
			}
			return null;
		}

		public ActionResult GetQuestionDetails(int QuesId)
		{
			DiaryModel Dmdl = new DiaryModel();
			if (QuesId != 0)
			{
				Dmdl.QuestionId = QuesId;
			}

			Dmdl = Helper.ExecuteService("Diary", "GetQuestionDetails", Dmdl) as DiaryModel;
			CurrentSession.FileStorePath = Dmdl.AttachFilePath;
			if (Dmdl.TimeForBind != null)
			{
				TimeSpan timespan = new TimeSpan(Dmdl.TimeForBind.Value.Hours, Dmdl.TimeForBind.Value.Minutes, 00);
				DateTime dtTemp = DateTime.ParseExact(timespan.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
				Dmdl.StringTime = dtTemp.ToString("hh:mm tt");
			}

			Dmdl.PaperEntryType = "Question";
			///changes dharmendra

			return PartialView("_ShowDetails", Dmdl);

		}
		//public ActionResult GetQuestionDetailsWithEdit(int QuesId)
		//{
		//    DiaryModel Dmdl = new DiaryModel();
		//    if (QuesId != 0)
		//    {
		//        Dmdl.QuestionId = QuesId;
		//    }

		//    Dmdl = Helper.ExecuteService("Diary", "GetQuestionDetails", Dmdl) as DiaryModel;
		//    CurrentSession.FileStorePath = Dmdl.AttachFilePath;
		//    if (Dmdl.TimeForBind != null)
		//    {
		//        TimeSpan timespan = new TimeSpan(Dmdl.TimeForBind.Value.Hours, Dmdl.TimeForBind.Value.Minutes, 00);
		//        DateTime dtTemp = DateTime.ParseExact(timespan.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
		//        Dmdl.StringTime = dtTemp.ToString("hh:mm tt");
		//    }

		//    Dmdl.PaperEntryType = "Question";
		//    ///changes dharmendra
		//    ///
		//    return PartialView("_ShowDetailsWithEdit", Dmdl);

		//}
		public ActionResult GetNoticeDetails(int NtsId)
		{
			DiaryModel Dmdl = new DiaryModel();
			if (NtsId != 0)
			{
				Dmdl.NoticeId = NtsId;

			}

			Dmdl = Helper.ExecuteService("Diary", "GetNoticeDetails", Dmdl) as DiaryModel;
			CurrentSession.FileStorePath = Dmdl.AttachFilePath;
			if (Dmdl.TimeForBind != null)
			{
				TimeSpan timespan = new TimeSpan(Dmdl.TimeForBind.Value.Hours, Dmdl.TimeForBind.Value.Minutes, 00);
				DateTime dtTemp = DateTime.ParseExact(timespan.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
				Dmdl.StringTime = dtTemp.ToString("hh:mm tt");
			}
			Dmdl.PaperEntryType = "Notice";
			return PartialView("_ShowDetails", Dmdl);
		}

        public ActionResult IsprintedForNotice(int NoticeId)
        {
            DiaryModel Dmdl = new DiaryModel();
            if (NoticeId != 0)
            {
                Dmdl.NoticeId = NoticeId;
                Dmdl.IsNoticePrinted = true;
            }

            Dmdl = Helper.ExecuteService("Diary", "GetNoticeDetailsIsprinted", Dmdl) as DiaryModel;
            CurrentSession.FileStorePath = Dmdl.AttachFilePath;
            if (Dmdl.TimeForBind != null)
            {
                TimeSpan timespan = new TimeSpan(Dmdl.TimeForBind.Value.Hours, Dmdl.TimeForBind.Value.Minutes, 00);
                DateTime dtTemp = DateTime.ParseExact(timespan.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
                Dmdl.StringTime = dtTemp.ToString("hh:mm tt");
            }
            Dmdl.PaperEntryType = "Notice";
            return PartialView("_ShowDetails", Dmdl);
        }


        public ActionResult IsPrintTaken(int QuesId)
        {
            ///by robin
            DiaryModel Dmdl = new DiaryModel();
            if (QuesId != 0)
            {
                Dmdl.QuestionId = QuesId;
                Dmdl.IsPrinted = true;
            }

            Dmdl = Helper.ExecuteService("Diary", "GetQuestionDetailsIsprinted", Dmdl) as DiaryModel;
            CurrentSession.FileStorePath = Dmdl.AttachFilePath;
            if (Dmdl.TimeForBind != null)
            {
                TimeSpan timespan = new TimeSpan(Dmdl.TimeForBind.Value.Hours, Dmdl.TimeForBind.Value.Minutes, 00);
                DateTime dtTemp = DateTime.ParseExact(timespan.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
                Dmdl.StringTime = dtTemp.ToString("hh:mm tt");
            }

            Dmdl.PaperEntryType = "Question";
            return PartialView("_ShowDetails", Dmdl);
        }

		#endregion

		#region"Submit Diaried paper to Corresponding Department created by Sunil on 15/05/2014"

		public ActionResult SubmitDashboard()
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModel DMdl = new DiaryModel();
				DiaryModel DMDl2 = new DiaryModel();
				mSession SessMdl = new mSession();
				mAssembly AssemMdl = new mAssembly();

				SiteSettings siteSettingModel = new SiteSettings();
				siteSettingModel = Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingModel) as SiteSettings;

				DMdl.AssemblyID = siteSettingModel.AssemblyCode;
				DMdl.SessionID = siteSettingModel.SessionCode;

				DMDl2 = (DiaryModel)Helper.ExecuteService("Diary", "GetInitialCountForDiaried", DMdl);

				mDepartment deptInfo = new mDepartment();

				deptInfo.deptId = CurrentSession.DeptID;
				deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
				DMdl.DepartmentName = deptInfo.deptname;

				SessMdl.AssemblyID = DMdl.AssemblyID;
				SessMdl.SessionCode = DMdl.SessionID;
				AssemMdl.AssemblyID = DMdl.AssemblyID;

				DMdl.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", SessMdl);
				DMdl.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", AssemMdl);

				DMdl.CurrentUserName = CurrentSession.UserName;
				DMdl.UserDesignation = CurrentSession.MemberDesignation;
				DMdl.UserName = CurrentSession.Name;

				DMdl.SInbox = DMDl2.SPending + DMDl2.SSent;
				DMdl.UInbox = DMDl2.UPending + DMDl2.USent;
				DMdl.NInbox = DMDl2.NPending + DMDl2.NSent;
				DMdl.SPending = DMDl2.SPending;
				DMdl.UPending = DMDl2.UPending;
				DMdl.NPending = DMDl2.NPending;
				DMdl.SSent = DMDl2.SSent;
				DMdl.USent = DMDl2.USent;
				DMdl.NSent = DMDl2.NSent;


				return View(DMdl);
			}
			else
			{
				return RedirectToAction("Login", "Account", new { @area = "" });
			}
		}

		public ActionResult Inbox(int AssemId, int SessId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string PaperType)
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModel DMdl = new DiaryModel();
				DiaryModel DMdl2 = new DiaryModel();

				PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
				RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
				loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
				loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

				DMdl.loopStart = Convert.ToInt16(loopStart);
				DMdl.loopEnd = Convert.ToInt16(loopEnd);
				DMdl.PageSize = Convert.ToInt16(RowsPerPage);
				DMdl.PageIndex = Convert.ToInt16(PageNumber);

				if (AssemId != 0)
				{
					DMdl.AssemblyID = AssemId;
				}

				if (SessId != 0)
				{
					DMdl.SessionID = SessId;
				}

				if (PaperType != "" && PaperType != null)
				{
					DMdl.PaperEntryType = PaperType;
				}

				if (PaperType == "Starred")
				{
					DMdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
				}
				else if (PaperType == "Unstarred")
				{
					DMdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
				}

				DMdl2 = Helper.ExecuteService("Diary", "GetInboxDiaried", DMdl) as DiaryModel;
				DMdl.DiaryList = DMdl2.DiaryList;
				DMdl.ResultCount = DMdl2.ResultCount;




				if (PageNumber != null && PageNumber != "")
				{
					DMdl.PageNumber = Convert.ToInt32(PageNumber);
				}
				else
				{
					DMdl.PageNumber = Convert.ToInt32("1");
				}
				if (RowsPerPage != null && RowsPerPage != "")
				{
					DMdl.RowsPerPage = Convert.ToInt32(RowsPerPage);
				}
				else
				{
					DMdl.RowsPerPage = Convert.ToInt32("10");
				}
				if (PageNumber != null && PageNumber != "")
				{
					DMdl.selectedPage = Convert.ToInt32(PageNumber);
				}
				else
				{
					DMdl.selectedPage = Convert.ToInt32("1");
				}
				if (loopStart != null && loopStart != "")
				{
					DMdl.loopStart = Convert.ToInt32(loopStart);
				}
				else
				{
					DMdl.loopStart = Convert.ToInt32("1");
				}
				if (loopEnd != null && loopEnd != "")
				{
					DMdl.loopEnd = Convert.ToInt32(loopEnd);
				}
				else
				{
					DMdl.loopEnd = Convert.ToInt32("5");
				}
				return PartialView("_Inbox", DMdl);

			}
			else
			{
				//return RedirectToAction("Login", "Account", new { @area = "" });
				return RedirectToAction("SubmitDashboard", "Diaries");
			}
		}

		public ActionResult Pending(int AssemId, int SessId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string PaperType)
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModel DMdl = new DiaryModel();
				DiaryModel DMdl2 = new DiaryModel();

				PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
				RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
				loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
				loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

				DMdl.loopStart = Convert.ToInt16(loopStart);
				DMdl.loopEnd = Convert.ToInt16(loopEnd);
				DMdl.PageSize = Convert.ToInt16(RowsPerPage);
				DMdl.PageIndex = Convert.ToInt16(PageNumber);

				if (AssemId != 0)
				{
					DMdl.AssemblyID = AssemId;
				}

				if (SessId != 0)
				{
					DMdl.SessionID = SessId;
				}

				if (PaperType != "" && PaperType != null)
				{
					DMdl.PaperEntryType = PaperType;
				}

				if (PaperType == "Starred")
				{
					DMdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
				}
				else if (PaperType == "Unstarred")
				{
					DMdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
				}

				DMdl2 = Helper.ExecuteService("Diary", "GetPendingDiaried", DMdl) as DiaryModel;
				DMdl.DiaryList = DMdl2.DiaryList;
				DMdl.ResultCount = DMdl2.ResultCount;

				if (PageNumber != null && PageNumber != "")
				{
					DMdl.PageNumber = Convert.ToInt32(PageNumber);
				}
				else
				{
					DMdl.PageNumber = Convert.ToInt32("1");
				}
				if (RowsPerPage != null && RowsPerPage != "")
				{
					DMdl.RowsPerPage = Convert.ToInt32(RowsPerPage);
				}
				else
				{
					DMdl.RowsPerPage = Convert.ToInt32("10");
				}
				if (PageNumber != null && PageNumber != "")
				{
					DMdl.selectedPage = Convert.ToInt32(PageNumber);
				}
				else
				{
					DMdl.selectedPage = Convert.ToInt32("1");
				}
				if (loopStart != null && loopStart != "")
				{
					DMdl.loopStart = Convert.ToInt32(loopStart);
				}
				else
				{
					DMdl.loopStart = Convert.ToInt32("1");
				}
				if (loopEnd != null && loopEnd != "")
				{
					DMdl.loopEnd = Convert.ToInt32(loopEnd);
				}
				else
				{
					DMdl.loopEnd = Convert.ToInt32("5");
				}
				return PartialView("_Pending", DMdl);

			}
			else
			{
				//return RedirectToAction("Login", "Account", new { @area = "" });
				return RedirectToAction("SubmitDashboard", "Diaries");
			}
		}

		public ActionResult Sent(int AssemId, int SessId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string PaperType)
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModel DMdl = new DiaryModel();
				DiaryModel DMdl2 = new DiaryModel();

				PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
				RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
				loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
				loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

				DMdl.loopStart = Convert.ToInt16(loopStart);
				DMdl.loopEnd = Convert.ToInt16(loopEnd);
				DMdl.PageSize = Convert.ToInt16(RowsPerPage);
				DMdl.PageIndex = Convert.ToInt16(PageNumber);

				if (AssemId != 0)
				{
					DMdl.AssemblyID = AssemId;
				}

				if (SessId != 0)
				{
					DMdl.SessionID = SessId;
				}

				if (PaperType != "" && PaperType != null)
				{
					DMdl.PaperEntryType = PaperType;
				}

				if (PaperType == "Starred")
				{
					DMdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
				}
				else if (PaperType == "Unstarred")
				{
					DMdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
				}

				DMdl2 = Helper.ExecuteService("Diary", "GetSentDiaried", DMdl) as DiaryModel;
				DMdl.DiaryList = DMdl2.DiaryList;
				DMdl.ResultCount = DMdl2.ResultCount;

				if (PageNumber != null && PageNumber != "")
				{
					DMdl.PageNumber = Convert.ToInt32(PageNumber);
				}
				else
				{
					DMdl.PageNumber = Convert.ToInt32("1");
				}
				if (RowsPerPage != null && RowsPerPage != "")
				{
					DMdl.RowsPerPage = Convert.ToInt32(RowsPerPage);
				}
				else
				{
					DMdl.RowsPerPage = Convert.ToInt32("10");
				}
				if (PageNumber != null && PageNumber != "")
				{
					DMdl.selectedPage = Convert.ToInt32(PageNumber);
				}
				else
				{
					DMdl.selectedPage = Convert.ToInt32("1");
				}
				if (loopStart != null && loopStart != "")
				{
					DMdl.loopStart = Convert.ToInt32(loopStart);
				}
				else
				{
					DMdl.loopStart = Convert.ToInt32("1");
				}
				if (loopEnd != null && loopEnd != "")
				{
					DMdl.loopEnd = Convert.ToInt32(loopEnd);
				}
				else
				{
					DMdl.loopEnd = Convert.ToInt32("5");
				}
				return PartialView("_Sent", DMdl);

			}
			else
			{
				//return RedirectToAction("Login", "Account", new { @area = "" });
				return RedirectToAction("SubmitDashboard", "Diaries");
			}
		}

		public ActionResult EditQuestionNotice(int Id, string EditType)
		{
			DiaryModel DMdl = new DiaryModel();
			DiaryModel DMdl2 = new DiaryModel();

			if (EditType == "QInbox")
			{
				if (Id != 0)
				{

					DMdl.QuestionId = Id;
				}

				DMdl2 = Helper.ExecuteService("Diary", "GetQuestionByIdForEdit", DMdl) as DiaryModel;
				DMdl = DMdl2;
				DMdl.ViewType = "QInbox";
				return PartialView("_EditQuestion", DMdl);

			}
			else if (EditType == "NInbox")
			{
				if (Id != 0)
				{

					DMdl.NoticeId = Id;
				}

				DMdl2 = Helper.ExecuteService("Diary", "GetNoticeByIdForEdit", DMdl) as DiaryModel;
				DMdl = DMdl2;

				if (DMdl.DateForBind != null)
				{
					DMdl.stringDate = Convert.ToDateTime(DMdl.DateForBind).ToString("dd/MM/yy");
				}
				if (DMdl.TimeForBind != null)
				{
					DMdl.NoticeTime = (TimeSpan)DMdl.TimeForBind;
				}
				DMdl.ViewType = "NInbox";
				return PartialView("_EditNotice", DMdl);
			}

			else if (EditType == "QDraft")
			{
				if (Id != 0)
				{

					DMdl.QuestionId = Id;
				}

				DMdl2 = Helper.ExecuteService("Diary", "GetQuestionByIdForEdit", DMdl) as DiaryModel;
				DMdl = DMdl2;
				DMdl.ViewType = "QDraft";
				return PartialView("_EditQuestion", DMdl);
			}

			else if (EditType == "NDraft")
			{
				if (Id != 0)
				{

					DMdl.NoticeId = Id;
				}

				DMdl2 = Helper.ExecuteService("Diary", "GetNoticeByIdForEdit", DMdl) as DiaryModel;
				DMdl = DMdl2;

				if (DMdl.DateForBind != null)
				{
					DMdl.stringDate = Convert.ToDateTime(DMdl.DateForBind).ToString("dd/MM/yy");
				}
				if (DMdl.TimeForBind != null)
				{
					DMdl.NoticeTime = (TimeSpan)DMdl.TimeForBind;
				}

				DMdl.ViewType = "NDraft";
				return PartialView("_EditNotice", DMdl);
			}

			return null;
		}

		public JsonResult GetDepartmentByMinister(int MinistryId)
		{
			DiaryModel DMdl = new DiaryModel();
			DMdl.MinistryId = MinistryId;
			DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("Diary", "GetDepartmentByMinister", DMdl);
			return Json(DMdl.DiaryList, JsonRequestBehavior.AllowGet);
		}
		public JsonResult GetConstByMemberId(int MemberId, int AssemId)
		{
			DiaryModel DMdl = new DiaryModel();
			if (MemberId != 0)
			{
				DMdl.MemberId = MemberId;
			}
			if (AssemId != 0)
			{
				DMdl.AssemblyID = AssemId;
			}

			DMdl = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", DMdl);
			return Json(DMdl, JsonRequestBehavior.AllowGet);
		}

		[HttpPost, ValidateAntiForgeryToken]
		public ActionResult SaveNotice(DiaryModel Dmdl)
		{
			if (CurrentSession.UserID != null && CurrentSession.UserID != "")
			{
				Dmdl.SubmittedBy = CurrentSession.UserID;
			}

			TempData["Message"] = Helper.ExecuteService("Diary", "SaveNotice", Dmdl) as string;
			return RedirectToAction("SubmitDashboard", "Diaries");
		}

		[HttpPost, ValidateAntiForgeryToken]
		public ActionResult SaveQuestion(DiaryModel Dmdl)
		{
			if (CurrentSession.UserID != null && CurrentSession.UserID != "")
			{
				Dmdl.SubmittedBy = CurrentSession.UserID;
			}

			//CultureInfo provider = CultureInfo.InvariantCulture;
			//provider = new CultureInfo("fr-FR");
			//var Date = DateTime.ParseExact(Dmdl.SessionDate, "d", provider);
			//Dmdl.FixedDate = Convert.ToDateTime(Date);

			TempData["Message"] = Helper.ExecuteService("Diary", "SaveQuestion", Dmdl) as string;
			return RedirectToAction("SubmitDashboard", "Diaries");
		}

		public ActionResult GetQuestionNoticeDetails(int Id, string PaperType)
		{
			DiaryModel DMdl = new DiaryModel();
			DiaryModel DMdl2 = new DiaryModel();


			if (PaperType == "Question")
			{
				if (Id != 0)
				{
					DMdl.QuestionId = Id;

				}

				DMdl2 = Helper.ExecuteService("Diary", "GetQuestionDetailById", DMdl) as DiaryModel;
				DMdl = DMdl2;
				DMdl.PaperEntryType = PaperType;
				return PartialView("_ViewDetailsById", DMdl);

			}
			else if (PaperType == "Notice")
			{
				if (Id != 0)
				{

					DMdl.NoticeId = Id;
				}

				DMdl2 = Helper.ExecuteService("Diary", "GetNoticeDetailById", DMdl) as DiaryModel;
				DMdl = DMdl2;
				DMdl.PaperEntryType = PaperType;
				return PartialView("_ViewDetailsById", DMdl);
			}
			return null;
		}

		[HttpPost]
		public JsonResult CheckQuestionNo(string QuestionNumber, int QtypeId)
		{

			DiaryModel obj = new DiaryModel();

			QuestionNumber = Sanitizer.GetSafeHtmlFragment(QuestionNumber);
			obj.QuestionNumber = Convert.ToInt32(QuestionNumber);
			if (QtypeId > 0)
			{
				obj.QuestionTypeId = QtypeId;
			}
			string st = Helper.ExecuteService("Diary", "CheckQuestionNo", obj) as string;
			return Json(st, JsonRequestBehavior.AllowGet);
		}
		public ActionResult GetAllRecord(int AssemId, int SessId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string PaperType)
		{
			if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
			{
				DiaryModel DMdl = new DiaryModel();
				DiaryModel DMdl2 = new DiaryModel();

				PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
				RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
				loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
				loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

				DMdl.loopStart = Convert.ToInt16(loopStart);
				DMdl.loopEnd = Convert.ToInt16(loopEnd);
				DMdl.PageSize = Convert.ToInt16(RowsPerPage);
				DMdl.PageIndex = Convert.ToInt16(PageNumber);

				if (AssemId != 0)
				{
					DMdl.AssemblyID = AssemId;
				}

				if (SessId != 0)
				{
					DMdl.SessionID = SessId;
				}

				if (PaperType != "" && PaperType != null)
				{
					DMdl.PaperEntryType = PaperType;
				}

				if (PaperType == "Starred")
				{
					DMdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
				}
				else if (PaperType == "Unstarred")
				{
					DMdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
				}

				DMdl2 = Helper.ExecuteService("Diary", "GetAllDiariedRecord", DMdl) as DiaryModel;
				DMdl.DiaryList = DMdl2.DiaryList;
				DMdl.ResultCount = DMdl2.ResultCount;




				if (PageNumber != null && PageNumber != "")
				{
					DMdl.PageNumber = Convert.ToInt32(PageNumber);
				}
				else
				{
					DMdl.PageNumber = Convert.ToInt32("1");
				}
				if (RowsPerPage != null && RowsPerPage != "")
				{
					DMdl.RowsPerPage = Convert.ToInt32(RowsPerPage);
				}
				else
				{
					DMdl.RowsPerPage = Convert.ToInt32("10");
				}
				if (PageNumber != null && PageNumber != "")
				{
					DMdl.selectedPage = Convert.ToInt32(PageNumber);
				}
				else
				{
					DMdl.selectedPage = Convert.ToInt32("1");
				}
				if (loopStart != null && loopStart != "")
				{
					DMdl.loopStart = Convert.ToInt32(loopStart);
				}
				else
				{
					DMdl.loopStart = Convert.ToInt32("1");
				}
				if (loopEnd != null && loopEnd != "")
				{
					DMdl.loopEnd = Convert.ToInt32(loopEnd);
				}
				else
				{
					DMdl.loopEnd = Convert.ToInt32("5");
				}
				return PartialView("_GetAllDiariedRecord", DMdl);

			}
			else
			{
				//return RedirectToAction("Login", "Account", new { @area = "" });
				return RedirectToAction("SubmitDashboard", "Diaries");
			}
		}

		public ActionResult GetFileAttachment(string filepathName)
		{
			DiaryModel DMdl = new DiaryModel();

			if (CurrentSession.FileAccessPath == null || CurrentSession.FileAccessPath == "")
			{
				SiteSettings siteSettingModel = new SiteSettings();
				siteSettingModel = Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingModel) as SiteSettings;
				CurrentSession.FileAccessPath = siteSettingModel.AccessFilePath;
			}

			filepathName = CurrentSession.FileAccessPath + filepathName;
			DMdl.FilePath = filepathName;

			//try
			//{
			//    Stream stream = null;
			//    filepathName = CurrentSession.FileAccessPath + filepathName;
			//    int bytesToRead = 10000;
			//    byte[] buffer = new Byte[bytesToRead];
			//    HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(filepathName);
			//    HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

			//    if (fileReq.ContentLength > 0)
			//        fileResp.ContentLength = fileReq.ContentLength;

			//    stream = fileResp.GetResponseStream();
			//    return File(stream, "application/pdf");
			//}
			//catch (Exception e)
			//{
			//    return View("FileNotFound");
			//}

			//return View("testPDF", DMdl);

			byte[] bytes = System.IO.File.ReadAllBytes(filepathName);
			return File(bytes, "application/pdf");
		}
		public ActionResult DisplayQuestionPDFByID(int id, string PaperType)
		{
			DiaryModel DMdl = new DiaryModel();
			if (PaperType != "Notice")
			{
				DMdl.QuestionId = id;
			}
			else if (PaperType == "Notice")
			{
				DMdl.NoticeId = id;
			}
			DMdl.PaperEntryType = PaperType;
			DMdl = Helper.ExecuteService("Diary", "DisplayQuestionPDFByID", DMdl) as DiaryModel;
			//var AccessPath = DMdl.AttachFilePath + "/" + DMdl.FilePath + DMdl.FileName;
			var AccessPath = CurrentSession.FileStorePath + "/" + DMdl.FilePath + DMdl.FileName;
			DMdl.FilePath = AccessPath;
			try
			{
				//Stream stream = null;

				//int bytesToRead = 10000;
				//byte[] buffer = new Byte[bytesToRead];

				//HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(AccessPath);
				//fileReq.Proxy = null;
				//fileReq.Credentials = CredentialCache.DefaultCredentials;
				//ServicePointManager.ServerCertificateValidationCallback += new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
				//HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

				//if (fileReq.ContentLength > 0)
				//    fileResp.ContentLength = fileReq.ContentLength;

				//stream = fileResp.GetResponseStream();
				byte[] bytes = System.IO.File.ReadAllBytes(AccessPath);
				return File(bytes, "application/pdf");
			}
#pragma warning disable CS0168 // The variable 'e' is declared but never used
			catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
			{
				//ViewBag.Exception = e.Message;
				return View("FileNotFound");
			}

			//return View("testPDF", DMdl);
		}

		public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
		{
			return true;
		}

		#endregion

		public JsonResult CheckExcessQuestion(int MemberId, int AssemId, int SessId, int QTId)
		{
			DiaryModel Dmdl = new DiaryModel();
			Dmdl.MemId = MemberId;
			Dmdl.AssemblyID = AssemId;
			Dmdl.SessionID = SessId;
			Dmdl.QuestionTypeId = QTId;
			Dmdl.Message = Helper.ExecuteService("Diary", "CheckExcessQuestion", Dmdl) as string;
			return Json("test", JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetDataQuestionID(int questionID)
		{
			string Month = "";
			string Day = "";
			string Year = "";
			tMemberNotice model = new tMemberNotice();
			model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
			model = (tMemberNotice)Helper.ExecuteService("Notice", "GetMembers", model);
			tQuestion mdl = new tQuestion();
			int Id = Convert.ToInt32(questionID);
			model.QuestionID = Id;
			model = (tMemberNotice)Helper.ExecuteService("Notice", "GetQDetails", model);
			foreach (var item in model.tQuestionModel)
			{
				model.Title = item.Subject;
				model.MemberId = item.MemberId;
				model.QuestionID = item.QuestionID;
				model.Details = item.MainQuestion;
				model.EventId = item.EventId;
				model.NoticeDate = item.NoticeDate.Value;
				model.NoticeTime = item.NoticeTime.Value;
				model.IsContentFreeze = item.IsContentFreeze;
				model.MinisterId = Convert.ToInt32(item.MinisterID);
				model.DepartmentId = item.DepartmentId;
				model.Checked = item.Checked;
				model.IsTypistFreeze = item.IsTypistFreeze;
				model.ConstituencyName = item.ConstituencyName;
				model.DiaryNo = item.DiaryNumber;
				model.IsClubChecked = Convert.ToBoolean(item.IsBracketed);
				model.ClubDiaryNo = item.BracketedDNo;
				model.MemberName = item.MemberName;
				model.IsQuestionInPart = item.IsQuestionInPart;
				model.IsHindi = item.IsHindi;
				model.QuestionTypeId = item.QuestionTypeId;
				model.ConstituencyCode = Convert.ToInt32(item.ConstituencyName_Local);

				string SDate = Convert.ToString(item.NoticeDate.Value);

				SDate = Convert.ToString(item.NoticeDate.Value);
				if (SDate != "")
				{
					if (SDate.IndexOf("/") > 0)
					{
						Month = SDate.Substring(0, SDate.IndexOf("/"));
						if (Month.Length < 2)
						{
							Month = "0" + Month;
						}
						Day = SDate.Substring(SDate.IndexOf("/") + 1, (SDate.LastIndexOf("/") - SDate.IndexOf("/") - 1));
						Year = SDate.Substring(SDate.LastIndexOf("/") + 1, 4);
						SDate = Day + "/" + Month + "/" + Year;
						model.NDate = SDate;
					}
					if (SDate.IndexOf("-") > 0)
					{
						Month = SDate.Substring(0, SDate.IndexOf("-"));
						Day = SDate.Substring(SDate.IndexOf("-") + 1, (SDate.LastIndexOf("-") - SDate.IndexOf("-") - 1));
						Year = SDate.Substring(SDate.LastIndexOf("-") + 1, 4);
						SDate = Day + "/" + Month + "/" + Year;
						model.NDate = SDate;
					}
				}
				mAssembly model4 = new mAssembly();
				model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;
				if (model.DepartmentId == null)
				{

					tMemberNotice noticeModel1 = new tMemberNotice();
					mMinistry model1 = new mMinistry();
					model1.MinistryID = model.MinisterId;
                    model1.AssemblyID = model.AssemblyID;
					noticeModel1.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model1) as List<tMemberNotice>;
					model.DepartmentLt = noticeModel1.DepartmentLt;

				}
				if (model.DepartmentId != null)
				{
					tMemberNotice noticeModel1 = new tMemberNotice();
					mMinistry model1 = new mMinistry();
					model1.MinistryID = model.MinisterId;
					noticeModel1.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model1) as List<tMemberNotice>;
					model.DepartmentLt = noticeModel1.DepartmentLt;
				}
			}

			return PartialView("_EditStarredQuestion", model);
		}

		public JsonResult UpdateDiaryEntry(NewModel NewModel)
		{
			tMemberNotice Update = new tMemberNotice();
			tQuestion objModel = new tQuestion();
			Update.NoticeId = NewModel.NID;
			Update.Subject = NewModel.Subject;
			Update.MinistryId = NewModel.MinirId;
			Update.DepartmentId = NewModel.DepartId;
			Update.MemberId = NewModel.MemId;
			Update.EventId = NewModel.EventId;
			Update = Helper.ExecuteService("Notice", "UpdateDiaryNotices", Update) as tMemberNotice;
			return Json("Update.Message", JsonRequestBehavior.AllowGet);
		}



		public JsonResult UpdateQuestionDiaryEntry(NewModel NewModel)
		{
			tMemberNotice Update = new tMemberNotice();
			tQuestion objModel = new tQuestion();
			objModel.QuestionID = NewModel.QID;
			objModel.Subject = NewModel.Subject;
			objModel.MinistryId = NewModel.MinirId;
			objModel.DepartmentID = NewModel.DepartId;
			objModel.ConstituencyNo = NewModel.ConsId;
			objModel.ConstituencyName = NewModel.ConsName;
			objModel.MemberID = NewModel.MemId;
			objModel.QuestionType = NewModel.QuestionType;
			objModel = Helper.ExecuteService("Notice", "UpdateDiaryQuestions", objModel) as tQuestion;
			Update.Message = "Update Sucessfully";

			// return RedirectToAction("DiariesDashboard", "Diaries");
			return Json("Update.Message", JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetNoticeDetailsForUpdate(int NtsId)
		{

			DiaryModel DMdl = new DiaryModel();
			DiaryModel DMdl2 = new DiaryModel();
			if (NtsId != 0)
			{

				DMdl.NoticeId = NtsId;
			}

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
				DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
			}

			DMdl2 = Helper.ExecuteService("LegislationFixation", "GetNoticeEditDiary", DMdl) as DiaryModel;
			DMdl = DMdl2;

			if (DMdl.DateForBind != null)
			{
				DMdl.stringDate = Convert.ToDateTime(DMdl.DateForBind).ToString("dd/MM/yy");
			}
			if (DMdl.TimeForBind != null)
			{
				DMdl.NoticeTime = (TimeSpan)DMdl.TimeForBind;
			}
			DMdl.ViewType = "NDraft";
			return PartialView("_EditDiaryView", DMdl);
		}


		#region paper verify
		public ActionResult VerifyPapers()
		{
			return PartialView("VerifyPapers");
		}
		public ActionResult fillReferenceNumber()
		{
			List<fillListGenric> list = (List<fillListGenric>)Helper.ExecuteService("Diary", "fillReferenceNumber", new string[] { CurrentSession.AssemblyId, CurrentSession.SessionId });
			return Json(list, JsonRequestBehavior.AllowGet);
		}
		public ActionResult getPaperLaidList(string ReferenceNumber)
		{
			List<tAuditTrialViewModel> list = (List<tAuditTrialViewModel>)Helper.ExecuteService("Diary", "getPaperLaidList", ReferenceNumber);
			foreach (var item in list)
			{
				string[] filepathName1 = item.filePath.Split('/');
				item.filePathName = filepathName1[filepathName1.Length - 1];
			}
			return PartialView("_paperLaidList", list);
		}
		public ActionResult changeStatusofAudit(string paperLaidTempID)
		{
			string ReferenceNumber = (string)Helper.ExecuteService("Diary", "changeStatusofAudit", new string[] { paperLaidTempID, CurrentSession.UserID });
			List<tAuditTrialViewModel> list = (List<tAuditTrialViewModel>)Helper.ExecuteService("Diary", "getPaperLaidList", ReferenceNumber);
			foreach (var item in list)
			{
				string[] filepathName1 = item.filePath.Split('/');
				item.filePathName = filepathName1[filepathName1.Length - 1];
                string moduleName = ConfigurationManager.AppSettings["QuestionsmoduleName"];
                string moduleActionName = ConfigurationManager.AppSettings["QuestionsmoduleActionName"];
               
                    SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models.SendNotificationsModel model = new SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models.SendNotificationsModel();

                    List<SBL.DomainModel.Models.RecipientGroups.RecipientGroup> contactGroups = new List<SBL.DomainModel.Models.RecipientGroups.RecipientGroup>();
                    //Get the Record from the SystemModules on the basis of the moduleName.
                    SystemModuleModel systemMoule = new SystemModuleModel();
                    systemMoule.Name = moduleName;
                    var moduleObj = Helper.ExecuteService("SystemModule", "GetSystemModuleByName", systemMoule) as SystemModuleModel;

                    SystemModuleModel actionObj = new SystemModuleModel();

                    if (moduleObj != null)
                    {
                        //Get the record from the SystemFunctions on the basis of moduleName and actionName.
                        SystemModuleModel moduleAction = new SystemModuleModel();
                        moduleAction.ActionName = moduleActionName;
                        actionObj = Helper.ExecuteService("SystemModule", "GetModuleActionByName", moduleAction) as SystemModuleModel;
                    }

                    if (actionObj != null)
                    {
                        #region Get All The Contacts Associated to the action.
                        if (!string.IsNullOrEmpty(actionObj.AssociatedContactGroups))
                        {
                            //pass the Comma seperated Associated Contact Groups to a Function and get all the group details at a time.
                            contactGroups = Helper.ExecuteService("ContactGroups", "GetContactGroupsByGroupIDs", new RecipientGroupMember { AssociatedContactGroups = actionObj.AssociatedContactGroups, DepartmentCode = item.departmentID }) as List<RecipientGroup>;
                        }
                        #endregion
                    }

                    
                  //  var modelSMS = new { DiaryNumber = Dmdl.DiaryNumber, IsInitialApprovedDate = ApprovedDate };

                    string SMSTemplateText = actionObj.SMSTemplate;
                    string emailTemplateText = actionObj.EmailTemplate;
                    int groupID = 0;
                    List<string> emailAddresses = new List<string>();
                    List<string> phoneNumbers = new List<string>();

                    try
                    {
                        //SMS Text
                        //SMSTemplateText = SMSTemplateText.Replace("@Model.DiaryNumber", modelSMS.DiaryNumber);
                        //SMSTemplateText = SMSTemplateText.Replace("@Model.IsInitialApprovedDate", modelSMS.IsInitialApprovedDate);
                      //  model.SMSTemplate = SMSTemplateText;
                        model.SMSTemplate =  "For Ref No:- " +  item.documentReferenceNumber + " , the online document has been received and made available in the Assembly House, Secretary H.P vidhan sabha"; 
                        //Email Text Acknowledgement : For eVidhan Ref No:- " + item.documentReferenceNumber + ", the online document has been received 

                        //emailTemplateText = emailTemplateText.Replace("@Model.DiaryNumber", modelSMS.DiaryNumber);
                        //emailTemplateText = emailTemplateText.Replace("@Model.IsInitialApprovedDate", modelSMS.IsInitialApprovedDate);
                        //model.EmailTemplate = emailTemplateText;
                        model.EmailTemplate = "For Ref No:- " +  item.documentReferenceNumber + " , the online document has been received and made available in the Assembly House, Secretary H.P vidhan sabha"; 
                        //model.SMSTemplate = Razor.Parse(SMSTemplateText, modelSMS);
                        //model.EmailTemplate = Razor.Parse(emailTemplateText, modelSMS);

                        model.availableRecipientGroups = contactGroups != null ? contactGroups : new List<RecipientGroup>();
                        model.selectedRecipientGroups = contactGroups != null ? contactGroups : new List<RecipientGroup>();
                        model.SendSMS = actionObj.SendSMS;
                        model.SendEmail = actionObj.SendEmail;

                        foreach (var item1 in model.selectedRecipientGroups)
                        {
                            groupID = item1.GroupID;
                        }
                        //var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", new RecipientGroupMember { GroupID = groupID }) as List<RecipientGroupMember>;
                        var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByNewFixed", new RecipientGroupMember { DepartmentCode = item.departmentID }) as List<RecipientGroupMember>;

                        if (contactGroupMembers != null)
                        {
                            foreach (var item2 in contactGroupMembers)
                            {
                                if (item2.Email.Contains(','))
                                {
                                    string[] emails = item2.Email.Split(',');
                                    foreach (var em in emails)
                                    {
                                        emailAddresses.Add(em);
                                    }
                                }

                                else
                                    emailAddresses.Add(item2.Email);

                                if (item2.MobileNo.Contains(','))
                                {
                                    string[] numbers = item2.MobileNo.Split(',');
                                    foreach (var mb in numbers)
                                    {
                                        phoneNumbers.Add(mb);
                                    }
                                }
                                else
                                    phoneNumbers.Add(item2.MobileNo);
                            
                            }
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }


                    //Sending Mail



                    if (!string.IsNullOrEmpty(model.EmailAddresses))
                        emailAddresses = model.EmailAddresses.Split(',').Select(s => s).ToList();

                    if (!string.IsNullOrEmpty(model.PhoneNumbers))
                        phoneNumbers = model.PhoneNumbers.Split(',').Select(s => s).ToList();

                    SMSMessageList smsMessage = new SMSMessageList();
                    CustomMailMessage emailMessage = new CustomMailMessage();


                    // SMS Settings                      
                    smsMessage.SMSText = model.SMSTemplate;
                    smsMessage.ModuleActionID = 1;
                    smsMessage.UniqueIdentificationID = 1;
                    //smsMessage.MobileNo = new List<string>();
                    //smsMessage.MobileNo.Add("8894681909");

                    // Email Settings 

                    //emailMessage.ModuleActionID = 1;
                    //emailMessage.UniqueIdentificationID = 1;
                    //EAttachment ea = new EAttachment { FileName = new FileInfo(Server.MapPath(SavedPdfPath)).Name, FileContent = System.IO.File.ReadAllBytes(Server.MapPath(SavedPdfPath)) };
                   // EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes(SavedPdfPath) };
                    emailMessage._attachments = new List<EAttachment>();
                   // emailMessage._attachments.Add(ea);

                    emailMessage._isBodyHtml = true;
                    emailMessage._subject = item.subject;
                    emailMessage._body = model.EmailTemplate;




                    foreach (var item3 in phoneNumbers)
                    {
                        smsMessage.MobileNo.Add(item3);
                    }

                    foreach (var item3 in emailAddresses)
                    {
                        emailMessage._toList.Add(item3);
                    }

                    Notification.Send(model.SendSMS, model.SendEmail, smsMessage, emailMessage);
                
			}
			return PartialView("_paperLaidList", list);
		}
		public ActionResult paperverifyReport()
		{
			//getVerifiedRecordsList
			return PartialView("paperverifyReport");
		}
		public ActionResult getVerifiedRecordsList(string date)
		{
			DateTime date1 = new DateTime(Convert.ToInt32(date.Split('-')[2]), Convert.ToInt32(date.Split('-')[1]), Convert.ToInt32(date.Split('-')[0]));
			List<tAuditTrialViewModel> list = (List<tAuditTrialViewModel>)Helper.ExecuteService("Diary", "getVerifiedRecordsList", new string[] { date1.ToString(), CurrentSession.UserID });
			foreach (var item in list)
			{
				//item.DateTime=ite

				string[] filepathName1 = item.filePath.Split('/');
				item.filePathName = filepathName1[filepathName1.Length - 1];
			}
			return PartialView("_paperLaidList", list);
		}

		#endregion

		public ActionResult DiaryofQuestionsNotices()
		{
			return View("DiaryofQuestionsNotices");
		}
		public ActionResult QuestionsNoticesDetails()
		{
			return View();
		}

		public PartialViewResult GetQNoticeDetails(string Option, string Number)
		{

			//SBL.DomainModel.Models.PaperLaid.tPaperLaidV m = new DomainModel.Models.PaperLaid.tPaperLaidV();
			//m = (SBL.DomainModel.Models.PaperLaid.tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", null);
            List<tQuestion> modelList = new List<tQuestion>();
            if (Number.Contains('/'))
            {

                string[] a = Number.Split('/');
                int Aid = Convert.ToInt32(a[0]);
                int sid = Convert.ToInt32(a[1]);

               
                ViewBag.option = Option;
                DiaryModel dml = new DiaryModel();
                dml.OTPQ = Option;
                dml.DiaryNumber = Number;
                dml.AssemblyID = Aid;
                dml.SessionID = sid;
                modelList = (List<tQuestion>)Helper.ExecuteService("Diary", "GetQuesNoticeDetailsByID", dml);
            }

			return PartialView("_QNoticesDetails", modelList);
		}

		[HttpGet]
		public FileResult ExportToPDF(string DIVData)
		{

			Document document = new Document(new Rectangle(288f, 144f), 10, 10, 10, 10);
			document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
			//Document document = new Document(PageSize.A4, 30, 30, 35, 35);
			MemoryStream ms = new MemoryStream();
			string outXml = DIVData;
			byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(outXml);
			MemoryStream msInput = new MemoryStream(byteArray);

			PdfWriter writer = PdfWriter.GetInstance(document, ms);
			document.Open();

			XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msInput, System.IO.MemoryStream.Null, System.Text.Encoding.Unicode);
			document.Close();

			byte[] file = ms.ToArray();
			MemoryStream output = new MemoryStream();
			output.Write(file, 0, file.Length);
			output.Position = 0;

			return File(output, "application/pdf");
		}

	}



	public class NewModel
	{
		public int QID { get; set; }
		[AllowHtml]
		public string Subject { get; set; }
		public string Details { get; set; }
		public int MinirId { get; set; }
		public string DepartId { get; set; }
		public int MemId { get; set; }
		public int QuestionType { get; set; }
		public string ConsId { get; set; }
		public string ConsName { get; set; }
		public int NID { get; set; }
		public int EventId { get; set; }

	}
}
