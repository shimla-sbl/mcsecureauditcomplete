﻿using Microsoft.Security.Application;
using SBL.DomainModel;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Extensions;
using SBL.DomainModel.Models.Event;
using System.IO;
using SBL.DomainModel.Models.Enums;
using Lib.Web.Mvc.JQuery.JqGrid;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.LOB;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Models;
using SBL.DomainModel.ComplexModel;
using System.Globalization;
using SBL.eLegistrator.HouseController.Web.Areas.Reporters.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.Models.SystemModule;
using System.Configuration;
using RazorEngine;
using SMS.API;
using EvoPdf;
using Email.API;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using System.Net;
using iTextSharp.text;
using System.Drawing;
using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.DomainModel.Models.AssemblyFileSystem;
using System.Web;
using SBL.DomainModel.Models.eFile;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.Speaker;
using System.Data;
using Resources;
using SBL.eLegistrator.HouseController.Web.Connection;
using System.Data.SqlClient;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Bill;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class LegislationFixationController : Controller
    {
        //
        // GET: /Notices/LegislationFixation/
        static List<DiaryModel> StaticListForSerch = new List<DiaryModel>();

        const int pageSize = 10;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PaperLaidList()
        {
            return View();
        }

        public ActionResult PartialPaperLaidListCategoryWise(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            LegislativeModel legislativeModel = new LegislativeModel();

            int count = 5;

            List<LegislativeModel> ListLeg = new List<LegislativeModel>();

            for (int i = 0; i <= count; i++)
            {
                LegislativeModel legislative = new LegislativeModel();
                legislative.PaperCategoryType = "Q/A";
                legislative.PaperSent = "80";
                legislative.PaperReceived = "40";
                legislative.PaperLaidinHouse = "16";
                legislative.PaperPendingForLay = "24";
                legislative.PaperCurrentLOB = "6";
                legislative.PaperUpdatedForCurrent = "2";
                ListLeg.Add(legislative);
            }
            legislativeModel.ListLegislative = ListLeg;

            legislativeModel.TotalPaperSent = "400";
            legislativeModel.TotalPaperReceived = "200";
            legislativeModel.TotalPaperLaidinHouse = "80";
            legislativeModel.TotalPaperPendingForLay = "120";
            legislativeModel.TotalPaperCurrentLOB = "30";
            legislativeModel.TotalPaperUpdatedForCurrent = "10";

            return PartialView("PartialPaperLaidListCategoryWise", legislativeModel);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PaperLaidSubmitt(HttpPostedFileBase file, HttpPostedFileBase DocFile, tPaperLaidV obj, string submitButton)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV mdl = new tPaperLaidV();
                tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    mdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    mdl.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                //mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", mdl);


                //if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                //{
                //    mdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                //}

                //if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                //{
                //    mdl.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                //}
                submitButton = "Update";
                if (submitButton == "Update")
                {
                    switch (submitButton)
                    {
                        case "Send":
                            {
                                tPaperLaidV billDetails = new tPaperLaidV();
                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                                billDetails = obj;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                                mdl.EventId = obj.EventId;
                                // int paperLaidId = (int)mdl.PaperLaidId;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetFileSaveDetails", mdl);

                                string categoryType = mdl.RespectivePaper;
                                if (file != null)
                                {
                                    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                    //string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + mdl.RespectivePaper + "/";
                                    string Url = "/PaperLaid/" + mdl.AssemblyId + "/" + mdl.SessionId + "/";
                                    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                    if (!Dir.Exists)
                                    {
                                        Dir.Create();
                                    }

                                    var ver = obj.FileVersion.GetValueOrDefault() + 1;//uday updated
                                    string ext = Path.GetExtension(file.FileName);
                                    string fileName = file.FileName.Replace(ext, "");
                                    string actualFileName = mdl.AssemblyId + "_" + mdl.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + ver + ext;
                                    file.SaveAs(Server.MapPath(Url + actualFileName));
                                    mdlTemp.FileName = actualFileName;
                                    mdlTemp.FilePath = Url;
                                    mdlTemp.Version = obj.FileVersion + 1;
                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    mdlTemp.DeptSubmittedDate = DateTime.Now;
                                    mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                    mdlTemp.AssemblyId = CurrentSession.AssemblyId;
                                    mdlTemp.SessionId = CurrentSession.SessionId;
                                }
                                else
                                {
                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    mdlTemp.DeptSubmittedDate = DateTime.Now;
                                    mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                    mdlTemp.AssemblyId = CurrentSession.AssemblyId;
                                    mdlTemp.SessionId = CurrentSession.SessionId;
                                }

                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                                //if (categoryType.IndexOf("bill", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                                //  {
                                billDetails.PaperLaidId = mdl.PaperLaidId;
                                billDetails.AssemblyId = mdl.AssemblyId;
                                billDetails.SessionId = mdl.SessionId;
                                billDetails.BillId = obj.BillId;
                                if (obj.BillTypeId == 1)
                                {

                                }
                                tBillRegister bills = new tBillRegister();
                                if (obj.BillTypeId == 1)
                                {
                                    billDetails.IsAmendment = false;
                                }
                                else
                                {
                                    billDetails.IsAmendment = true;
                                }
                                bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "InsertBillDetails", billDetails);
                                //  }

                                mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                                mdl.ModifiedBy = CurrentSession.UserName;
                                mdl.ModifiedWhen = DateTime.Now;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntryForBilReplace", mdl);
                                //SendBillPaperByDept(mdl);
                                break;
                            }
                        case "Update":
                            {
                                obj.tpaperLaidTempId = 0;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);
                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", mdl);
                                //if (obj.isPdfFile != "1")
                                //{
                                //    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                //    //string Url = "~/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + mdl.RespectivePaper + "/";
                                //    string Url = mdlTemp.FilePath;
                                //    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                //    if (!Dir.Exists)
                                //    {
                                //        Dir.Create();
                                //    }
                                //    var ver = obj.FileVersion.GetValueOrDefault();
                                //    string ext = Path.GetExtension(file.FileName);
                                //    string fileName = file.FileName.Replace(ext, "");
                                //    string actualFileName = mdl.AssemblyId + "_" + mdl.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + ver + ext;
                                //    file.SaveAs(Server.MapPath(Url + actualFileName));
                                //    mdlTemp.FileName = actualFileName;
                                //}

                                if (obj.isPdfFile != "1")
                                {
                                    mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetFileSaveDetails", mdl);
                                    string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.FileName = name;
                                        }
                                        string ext = Path.GetExtension(mdlTemp.FileName);
                                        string fileNam1 = mdlTemp.FileName.Replace(ext, "");


                                        var vers = obj.FileVersion.GetValueOrDefault() + 1;


                                        string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + vers + ext;


                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);
                                        string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.FileName = actualFileName;
                                        mdlTemp.Version = vers;

                                    }
                                    else
                                    {
                                        mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                        mdlTemp.Version = mdl.Count;
                                        mdlTemp.FileName = mdl.FileName;
                                        mdlTemp.DocFileName = mdl.DocFileName;
                                        mdlTemp.FilePath = mdl.FilePath;
                                    }


                                }


                                if (obj.isDocFile == "1")
                                {

                                    string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.DocFileName = name;
                                        }

                                        string ext = Path.GetExtension(mdlTemp.DocFileName);

                                        var DocVer = obj.DocFileVersion.GetValueOrDefault() + 1;


                                        string fileName = mdlTemp.DocFileName.Replace(ext, "");


                                        string actualDocFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + DocVer + ext;



                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualDocFileName);
                                        //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.DocFileName = actualDocFileName;
                                        mdlTemp.DocFileVersion = DocVer;
                                    }

                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    mdlTemp.DocFileVersion = obj.MainDocCount;

                                }


                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaid", mdlTemp);
                                tBillRegister billDetails = new tBillRegister();
                                billDetails.PaperLaidId = obj.PaperLaidId;
                                billDetails.BillTypeId = obj.BillTypeId;
                                billDetails.BillNumber = obj.BOTStatus.ToString() + " of " + obj.BillNumberYear.ToString();
                                billDetails.ShortTitle = obj.Description;
                                billDetails.MinisterId = obj.MinistryId;
                                billDetails.BillId = obj.BillId;
                                CultureInfo provider = CultureInfo.InvariantCulture;
                                //provider = new CultureInfo("en-US");
                                //DateTime dart = new DateTime();
                                string[] formats = { "dd/MM/yyyy","dd-MM-yyyy", "dd/M/yyyy","dd-M-yyyy", "d/M/yyyy","d-M-yyyy", "d/MM/yyyy","d-MM-yyyy","MM/dd/yyyy","MM-dd-yyyy","MM/dd/yy","MM-dd-yy",
                    "dd/MM/yy","dd-MM-yy", "dd/M/yy","dd-M-yy","d/M/yy","d-M-yy","d/MM/yy","d-MM-yy"};
                                //bool fsdf = DateTime.TryParseExact(obj.BillDate,"d",provider,DateTimeStyles.AllowWhiteSpaces,out dart);
                                var datetstring = obj.BillDate.Split(' ')[0];
                                billDetails.BillDate = string.IsNullOrEmpty(obj.BillDate) ? (DateTime?)null : DateTime.ParseExact(datetstring, formats, CultureInfo.InvariantCulture, DateTimeStyles.None); //DateTime.ParseExact(obj.BillDate, "d", provider);

                                tBillRegister bills = new tBillRegister();
                                bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "UpdateBillDetails", billDetails);

                                mdl.DeptActivePaperId = mdl.DeptActivePaperId;
                                mdl.ModifiedBy = CurrentSession.UserName;
                                mdl.ModifiedWhen = DateTime.Now;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntryForBilReplace", mdl);

                                break;
                            }
                        case "Save":
                            {

                                tPaperLaidV billDetails = new tPaperLaidV();

                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                                billDetails = obj;

                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                                mdl.EventId = obj.EventId;
                                int paperLaidId = (int)mdl.PaperLaidId;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetFileSaveDetails", mdl);

                                //tPaperLaidV versioncount = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetFileVersion", obj);
                                string categoryType = mdl.RespectivePaper;


                                if (file == null)
                                {
                                    string url = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.FileName = name;
                                        }
                                        string ext = Path.GetExtension(mdlTemp.FileName);
                                        string fileNam1 = mdlTemp.FileName.Replace(ext, "");


                                        var vers = obj.FileVersion.GetValueOrDefault() + 1;


                                        string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + vers + ext;


                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualFileName);
                                        string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        //string FileStructurePath = System.IO.Path.Combine(FileSettings.SettingValue + NewsSettings.SettingValue);
                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.FileName = actualFileName;
                                        mdlTemp.Version = vers;

                                    }
                                    else
                                    {
                                        mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                        mdlTemp.Version = mdl.Count;
                                        mdlTemp.FileName = mdl.FileName;
                                        mdlTemp.DocFileName = mdl.DocFileName;
                                        mdlTemp.FilePath = mdl.FilePath;
                                    }


                                }

                                else
                                {
                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    mdlTemp.DeptSubmittedDate = DateTime.Now;
                                    mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                }


                                if (DocFile == null)
                                {

                                    string url = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                                    string directory = Server.MapPath(url);
                                    if (Directory.Exists(directory))
                                    {
                                        string[] savedFileName = Directory.GetFiles(Server.MapPath("~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID));
                                        string SourceFile = savedFileName[0];
                                        foreach (string page in savedFileName)
                                        {
                                            string name = Path.GetFileName(page);
                                            string nameKey = Path.GetFileNameWithoutExtension(page);
                                            string directory1 = Path.GetDirectoryName(page);
                                            //
                                            // Display the Path strings we extracted.
                                            //
                                            Console.WriteLine("{0}, {1}, {2}, {3}",
                                            page, name, nameKey, directory1);
                                            mdlTemp.DocFileName = name;
                                        }

                                        string ext = Path.GetExtension(mdlTemp.DocFileName);

                                        var DocVer = obj.DocFileVersion.GetValueOrDefault() + 1;


                                        string fileName = mdlTemp.DocFileName.Replace(ext, "");


                                        string actualDocFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + DocVer + ext;



                                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDepartmentFileSetting", null);
                                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                                        string Url = FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                        DirectoryInfo Dir = new DirectoryInfo(Url);
                                        if (!Dir.Exists)
                                        {
                                            Dir.Create();
                                        }

                                        string path = System.IO.Path.Combine(FileSettings.SettingValue + "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/", actualDocFileName);
                                        //string SaveIntroPath = "/IntroductionPdf/" + actualFileName;
                                        System.IO.File.Copy(SourceFile, path, true);
                                        //mdlTemp.FilePath = "/Department/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        mdlTemp.FilePath = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";

                                        var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                                        string FileStructurePath = Acess.SettingValue;
                                        mdlTemp.FileStructurePath = FileStructurePath;
                                        mdlTemp.DocFileName = actualDocFileName;
                                        mdlTemp.DocFileVersion = DocVer;
                                    }

                                    mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                    mdlTemp.DocFileVersion = obj.MainDocCount;

                                }

                                mdlTemp.AssemblyId = CurrentSession.AssemblyId;
                                mdlTemp.SessionId = CurrentSession.SessionId;
                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                                //if (categoryType.IndexOf("bill", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                                //{
                                billDetails.PaperLaidId = mdl.PaperLaidId;
                                billDetails.AssemblyCode = mdl.AssemblyId;
                                billDetails.SessionCode = mdl.SessionId;
                                billDetails.BillId = obj.BillId;



                                tBillRegister bills = new tBillRegister();
                                bills.BillName = mdl.Title;
                                bills.PaperLaidId = obj.PaperLaidId;
                                bills.AssemblyId = mdl.AssemblyId;
                                bills.SessionId = mdl.SessionId;
                                bills.BillId = obj.BillId;
                                bills.MinisterId = obj.MinistryId;

                                bills.ShortTitle = obj.Title;

                                bills.BillNo = obj.BillNUmberValue;
                                bills.BillYear = obj.BillNumberYear;

                                //bills.BillDate = obj.BillDate;
                                bills.BillTypeId = obj.BillTypeId;




                                if (obj.BillTypeId == 1)
                                {
                                    billDetails.IsAmendment = false;
                                }
                                else
                                {
                                    bills.IsAmendment = true;
                                }
                                if (obj.BillTypeId == 1)
                                {
                                    bills.IsAmendment = false;
                                }
                                else
                                {
                                    billDetails.IsAmendment = true;
                                }
                                if (bills.PaperLaidId != 0 && bills.BillId != 0)
                                {
                                    var data = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "UpdateBillDetails", bills);

                                    // bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "UpdateBillDetails", billDetails);
                                }
                                else
                                {
                                    bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "InsertBillDetails", billDetails);
                                }


                                //}


                                mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;

                                mdl.ModifiedBy = CurrentSession.UserName;
                                mdl.ModifiedWhen = DateTime.Now;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntryForBilReplace", mdl);


                                string url1 = "~/DepartmentPdf/MainReplyPdf" + "/" + CurrentSession.UserID;
                                string MainPDFdirectory1 = Server.MapPath(url1);
                                if (Directory.Exists(MainPDFdirectory1))
                                {
                                    System.IO.Directory.Delete(MainPDFdirectory1, true);
                                }
                                string urlMainDoc = "~/DepartmentPdf/MainReplyDoc" + "/" + CurrentSession.UserID;
                                string MainDocdirectory = Server.MapPath(urlMainDoc);
                                if (Directory.Exists(MainDocdirectory))
                                {
                                    System.IO.Directory.Delete(MainDocdirectory, true);
                                }


                                break;
                            }
                        case "Update Send":
                            {
                                // obj.tpaperLaidTempId = 0;

                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", obj);
                                obj.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);

                                if (file != null)
                                {
                                    obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                    //  string Url = "~/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + mdl.RespectivePaper + "/";
                                    string Url = mdlTemp.FilePath;
                                    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                    if (!Dir.Exists)
                                    {
                                        Dir.Create();
                                    }
                                    var ver = obj.FileVersion.GetValueOrDefault();
                                    string ext = Path.GetExtension(file.FileName);
                                    string fileName = file.FileName.Replace(ext, "");
                                    string actualFileName = mdl.AssemblyId + "_" + mdl.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + ver + ext;
                                    file.SaveAs(Server.MapPath(Url + actualFileName));
                                    mdlTemp.FileName = actualFileName;

                                }

                                mdlTemp.DeptSubmittedDate = DateTime.Now;
                                mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaid", mdlTemp);

                                tBillRegister billDetails = new tBillRegister();
                                billDetails.PaperLaidId = obj.PaperLaidId;
                                billDetails.BillTypeId = obj.BillTypeId;
                                billDetails.PreviousBillId = obj.BillId;
                                billDetails.BillNumber = obj.BillNUmberValue.ToString() + " of " + obj.BillNumberYear.ToString();
                                string[] formats = { "dd/MM/yyyy","dd-MM-yyyy", "dd/M/yyyy","dd-M-yyyy", "d/M/yyyy","d-M-yyyy", "d/MM/yyyy","d-MM-yyyy","MM/dd/yyyy","MM-dd-yyyy","MM/dd/yy","MM-dd-yy",
                    "dd/MM/yy","dd-MM-yy", "dd/M/yy","dd-M-yy","d/M/yy","d-M-yy","d/MM/yy","d-MM-yy"};
                                //bool fsdf = DateTime.TryParseExact(obj.BillDate,"d",provider,DateTimeStyles.AllowWhiteSpaces,out dart);
                                var datetstring = obj.BillDate.Split(' ')[0];
                                billDetails.BillDate = string.IsNullOrEmpty(obj.BillDate) ? (DateTime?)null : DateTime.ParseExact(datetstring, formats, CultureInfo.InvariantCulture, DateTimeStyles.None); //DateTime.ParseExact(obj.BillDate, "d", provider);
                                billDetails.ShortTitle = obj.Title;
                                billDetails.MinisterId = obj.MinistryId;
                                billDetails.SessionId = mdl.SessionId;
                                billDetails.AssemblyId = mdl.AssemblyId;
                                billDetails.BillId = obj.BillId;
                                tBillRegister bills = new tBillRegister();
                                if (obj.BillTypeId == 1)
                                {
                                    billDetails.IsAmendment = false;
                                }
                                else
                                {
                                    billDetails.IsAmendment = true;
                                }
                                bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "UpdateBillDetails", billDetails);
                                break;
                            }


                    }
                }

                if (Request.IsAjaxRequest())
                {
                    tPaperLaidV mod = new tPaperLaidV();

                    return Json(mod, JsonRequestBehavior.AllowGet);
                }
                else
                {


                }
                return null;

                //TempData["lSM"] = "BLL";
                //TempData["Msg"] = "Save";
                //return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment"); //RedirectToAction("DepartmentDashboard");
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        public ActionResult ShowDraftBillById(int paperLaidId)
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tPaperLaidV mdl = new tPaperLaidV();
                mdl.PaperLaidId = Convert.ToInt64(paperLaidId);
                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "ShowDraftBillById", mdl);
                mEvent mEventModel = new mEvent();

                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    mdl.UserID = new Guid(CurrentSession.UserID);
                }

                mdl.DepartmentId = CurrentSession.DeptID;
                mdl.IsPermissions = true;
                mdl.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryByDepartmentIds", mdl);

                mDepartment mdep = new mDepartment();
                mdep.deptId = mdl.DepartmentId;
                mdl.mDepartment = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);

                if (paperLaidId != 0)
                {
                    mdl.PaperLaidId = paperLaidId;
                    string dptId = (string)Helper.ExecuteService("PaperLaid", "getDepartmentIdByPaperId", mdl);
                    if (dptId != "")
                    {
                        mdl.DepartmentId = dptId;
                    }
                    else
                    {
                        mdl.DepartmentId = CurrentSession.DeptID;
                    }
                }

                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                mdl.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", mdl);

                mdl.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("BillPaperLaid", "GetDocumentType", mdl);
                mdl.tBillAmendment = (ICollection<tBillRegister>)Helper.ExecuteService("BillPaperLaid", "GetAmendmentBills", mdl);

                List<mEvent> events = (List<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
                mdl.mEvents = events;
                //.FindAll(a => a.EventId == 5 || a.EventId == 6);

                mdl.mBillTypes = (List<mBillType>)Helper.ExecuteService("BillPaperLaid", "GetBillTypes", "Testdata");

                foreach (var item in mdl.mBillTypes)
                {
                    mdl.BillTypeId = item.BillTypeId;
                }

                List<SelectListItem> years = new List<SelectListItem>();
                int currentYear = DateTime.Now.Year;
                for (int i = currentYear - 5; i < currentYear; i++)
                {
                    SelectListItem year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };
                    years.Add(year);
                }

                for (int i = currentYear; i < currentYear + 5; i++)
                {
                    SelectListItem year = new SelectListItem();
                    if (i == DateTime.Now.Year)
                    {
                        year = new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = true };
                    }
                    else
                    {
                        year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };
                    }
                    years.Add(year);
                }

                //ICollection<int> yearList = new List<int>();

                //for (int i = currentYear - 5; i < currentYear; i++)
                //{
                //    yearList.Add(i);
                //}
                //for (int i = currentYear; i < currentYear + 5; i++)
                //{
                //    yearList.Add(i);
                //}
                mdl.yearList = years;
                //mdl.BillNumberYear = mdl;

                return PartialView("_CreateNewBillDraft", mdl);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        #region Paper Laid Summary
        public ActionResult CreateNewBillDraft()
        {
            tPaperLaidV paperLaidModel = new tPaperLaidV();
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                mEvent mEventModel = new mEvent();
                //New changes according  employee authorizations
                /*Start*/
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    paperLaidModel.UserID = new Guid(CurrentSession.UserID);
                }

                paperLaidModel.DepartmentId = CurrentSession.DeptID;
                paperLaidModel.IsPermissions = true;
                // paperLaidModel.DepartmentId = CurrentSession.DeptID;
                mDepartment mdep = new mDepartment();
                mdep.deptId = paperLaidModel.DepartmentId;
                paperLaidModel.mDepartment = (List<mDepartment>)Helper.ExecuteService("BillPaperLaid", "GetDepartmentByid", mdep);

                tPaperLaidV ministerDetails = new tPaperLaidV();
                ministerDetails.DepartmentId = paperLaidModel.DepartmentId;
                paperLaidModel.SessionId = int.Parse(CurrentSession.SessionId);
                paperLaidModel.AssemblyCode = int.Parse(CurrentSession.AssemblyId);
                paperLaidModel.AssemblyId = int.Parse(CurrentSession.AssemblyId);
                paperLaidModel.SessionCode = int.Parse(CurrentSession.SessionId);

                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                paperLaidModel.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", paperLaidModel);

                paperLaidModel.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("BillPaperLaid", "GetDocumentType", paperLaidModel);

                paperLaidModel.tBillAmendment = (ICollection<tBillRegister>)Helper.ExecuteService("BillPaperLaid", "GetAmendmentBills", paperLaidModel);

                List<mEvent> events = (List<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
                paperLaidModel.mEvents = events;

                paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryByDepartmentIds", ministerDetails);
                paperLaidModel.mBillTypes = (List<mBillType>)Helper.ExecuteService("BillPaperLaid", "GetBillTypes", "Testdata");

                foreach (var item in paperLaidModel.mBillTypes)
                {
                    paperLaidModel.BillTypeId = item.BillTypeId;
                }

                List<SelectListItem> years = new List<SelectListItem>();
                int currentYear = DateTime.Now.Year;
                for (int i = currentYear - 5; i < currentYear; i++)
                {
                    SelectListItem year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };
                    years.Add(year);
                }

                for (int i = currentYear; i < currentYear + 5; i++)
                {
                    SelectListItem year = new SelectListItem();
                    if (i == DateTime.Now.Year)
                    {
                        year = new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = true };
                    }
                    else
                    {
                        year = new SelectListItem { Text = i.ToString(), Value = i.ToString() };
                    }
                    years.Add(year);
                }

                paperLaidModel.yearList = years;
                paperLaidModel.BillNumberYear = DateTime.Now.Year;

                return PartialView("_CreateNewBillDraft", paperLaidModel);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }
        public ActionResult PaperLaidSummary()
        {
            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                DiaryModel DMdl = new DiaryModel();
                PaperLaidSummaryViewModel model = new PaperLaidSummaryViewModel();

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }
                DMdl.AskedBy = CurrentSession.UserID;
                model.mAssemblyList = (List<mAssembly>)Helper.ExecuteService("LegislationFixation", "GetAssemblyList", null);
                model.sessionList = (List<mSession>)Helper.ExecuteService("LegislationFixation", "GetSessionLsitbyAssemblyId", DMdl);
                model.PendingNoticesAssignCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeAssignPendingCount", DMdl);
                model.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCount", DMdl);
                // model.PendingNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePendingCountLegislatin", DMdl);
                model.SentNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticeSentCount", DMdl);
                model.RecievedNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperRecievedCount", DMdl);
                model.CurrentLobNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperCurrentLobCount", DMdl);
                model.LaidInHomeNoticesCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperLaidInHomeCount", DMdl);
                ////pending to lay Notice
                model.PendingToLayNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetNoticePaperPendingToLayListCount", DMdl);

                model.RecievedBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperRecievedCount", DMdl);
                model.CurrentLobBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperCurrentLobCount", DMdl);
                model.LaidInHomeBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperLaidInHomeCount", DMdl);
                ////Pending to lay Bills
                model.PendingToLayBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetBillsPaperPendingToLayListCount", DMdl);

                model.RecievedCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperRecievedCount", DMdl);
                model.CurrentLobCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperCurrentLobCount", DMdl);
                model.LaidInHomeCommitteePaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperLaidInHomeCount", DMdl);

                ////pending to lay committee
                model.PendingToLayCommitteeCount = (int)Helper.ExecuteService("LegislationFixation", "GetCommittePaperPendingToLayListCount", DMdl);

                model.PendingStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPendingCount", DMdl);
                model.SentStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionSentCount", DMdl);
                model.RecievedStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperRecievedCount", DMdl);
                model.CurrentLobStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperCurrentLobCount", DMdl);
                model.LaidInHomeStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperLaidInHomeCount", DMdl);

                ////starred Question pending to lay
                model.PendingToLayStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionPaperPendingToLayListCount", DMdl);

                model.PendingUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPendingCount", DMdl);
                model.SentUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionSentCount", DMdl);
                model.RecievedUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperRecievedCount", DMdl);
                model.CurrentLobUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperCurrentLobCount", DMdl);
                model.LaidInHomeUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperLaidInHomeCount", DMdl);
                ////pending to lay Unstarred
                model.PendingToLayUnStarredQuestionCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredQuestionPaperPendingToLayListCount", DMdl);


                model.RecievedPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayRecievedCount", DMdl);
                model.CurrentLobPaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayCurrentLobCount", DMdl);
                model.LaidInHomePaperToLayCount = (int)Helper.ExecuteService("LegislationFixation", "GetPaperToLayLaidInHomeCount", DMdl);
                ////Pending to lay
                model.PendingToLayOtherPaperCount = (int)Helper.ExecuteService("LegislationFixation", "GetOtherPaperPendingToLayListCount", DMdl);

                //// Fixed and UnFixed Question Count For Starred And UnStarred Question type Created by Sunil on 02-July-2014  
                model.Starredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredFixedQuesCount", DMdl);
                model.StarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetStarredUnFixedQuesCount", DMdl);
                model.Unstarredfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredFixedQuesCount", DMdl);
                model.UnstarredUnfixed = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredUnFixedQuesCount", DMdl);

                model.StarredCountForWithdrawn = (int)Helper.ExecuteService("LegislationFixation", "GetStarredforWithdrawnCount", DMdl);
                model.StarredWithdrawnCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredListWithdrawnCount", DMdl);
                model.UnstarredCountForWithdrawn = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredforWithdrawnCount", DMdl);
                model.UntarredWithdrawnCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredListWithdrawnCount", DMdl);
                model.SentBillsCount = (int)Helper.ExecuteService("LegislationFixation", "GetMemoCount", DMdl);
                tPaperLaidV mdl = new tPaperLaidV();
                SiteSettings siteSettingMod = new SiteSettings();
                mSession Mdl = new mSession();
                mAssembly assmblyMdl = new mAssembly();
                tMemberNotice model1 = new tMemberNotice();
                CutMotionModel Noticecoiunt = new CutMotionModel();
                if (DMdl.AssemblyID != 0 && DMdl.SessionID != 0)
                {
                    mdl.AssemblyCode = DMdl.AssemblyID;
                    mdl.SessionCode = DMdl.SessionID;

                    //Add Value for Session object
                    Mdl.SessionCode = DMdl.SessionID;
                    Mdl.AssemblyID = DMdl.AssemblyID;
                    //Add Value for Assembly Object 
                    assmblyMdl.AssemblyID = DMdl.AssemblyID;
                    // Add Value for tMemberNoticec Object
                    model1.AssemblyCode = DMdl.AssemblyID;
                    model1.SessionID = DMdl.SessionID;
                    model.AssemblyID = DMdl.AssemblyID;
                    model.SessionID = DMdl.SessionID;

                }
                else
                {
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    mdl.SessionCode = siteSettingMod.SessionCode;
                    mdl.AssemblyCode = siteSettingMod.AssemblyCode;
                    //Add Value for Session object
                    Mdl.SessionCode = siteSettingMod.SessionCode;
                    Mdl.AssemblyID = siteSettingMod.AssemblyCode;
                    //Add Value for Assembly Object 
                    assmblyMdl.AssemblyID = siteSettingMod.AssemblyCode;
                    // Add Value for tMemberNoticec Object
                    model1.AssemblyCode = siteSettingMod.AssemblyCode;
                    model1.SessionID = siteSettingMod.SessionCode;
                    model.AssemblyID = siteSettingMod.AssemblyCode;
                    model.SessionID = siteSettingMod.SessionCode;
                    model.AccessFilePath = siteSettingMod.AccessFilePath;
                    CurrentSession.AssemblyId = siteSettingMod.AssemblyCode.ToString();
                    CurrentSession.SessionId = siteSettingMod.SessionCode.ToString();
                    CurrentSession.FileAccessPath = siteSettingMod.AccessFilePath;

                }

                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
                mdl = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "GetUpdatedOtherPaperLaidCounters", mdl);
                if (mdl != null)
                {
                    model.TotalUpdatedFileCounter = mdl.TotalOtherpaperLaidUpdated;

                }
                mDepartment deptInfo = new mDepartment();
                if (!string.IsNullOrEmpty(CurrentSession.DeptID))
                {
                    deptInfo.deptId = CurrentSession.DeptID;
                    deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
                }

                if (deptInfo != null)
                {
                    model.DepartmentName = deptInfo.deptname;
                }

                model.CurrentUserName = CurrentSession.UserName;
                model.UserDesignation = CurrentSession.MemberDesignation;
                model.UserName = CurrentSession.Name;
                // added by Sameer
                // Get the Total count of All Type of question.
                model1.QuestionTypeId = (int)QuestionType.StartedQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForProofReader", model1);
                model.TotalStaredReceived = model1.TotalStaredReceived;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForClubbed", model1);
                model.TotalStaredReceivedCl = model1.TotalStaredReceivedCl;
                model.TotalSBracktedQuestion = model1.TotalSBracktedQuestion;
                model.TotalInActiveQuestion = model1.TotalInActiveQuestion;
                model.TotalClubbedQuestion = model1.TotalClubbedQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForUnStaredClubbed", model1);
                model.TotalUnStaredReceivedCl = model1.TotalUnStaredReceivedCl;
                model.TotalUBractedQuestion = model1.TotalUBractedQuestion;
                model.UnstaredTotalClubbedQuestion = model1.UnstaredTotalClubbedQuestion;
                model1.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForLegisUnstartQuestion", model1);
                model.TotalUnStaredReceived = model1.TotalUnStaredReceived;
                int c = model.TotalStaredReceived + model.TotalUnStaredReceived;

                model.TotalValue = c;
                int d = model.TotalStaredReceivedCl + model.TotalClubbedQuestion + model.TotalInActiveQuestion;
                model.TotalValueCl = d;

                model.StarredCountExess = (int)Helper.ExecuteService("LegislationFixation", "GetCountQuestionsForExcess", DMdl);
                model.StarredCountExessed = (int)Helper.ExecuteService("LegislationFixation", "GetCountExcessedQuestions", DMdl);

                model.UnStarredCountExess = (int)Helper.ExecuteService("LegislationFixation", "GetUnStarredCountQuestionsForExcess", DMdl);
                model.UnStarredCountExessed = (int)Helper.ExecuteService("LegislationFixation", "GetCountUnStarredExcessedQuestions", DMdl);

                // Add Code for Rejected and StarredToUnstarred Count By type Created by Sunil on 28-July-2014 

                model.StarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredRejectedCount", DMdl);
                model.UnstarredRejectedCount = (int)Helper.ExecuteService("LegislationFixation", "GetUnstarredRejectedCount", DMdl);
                model.StarredToUnstarredCount = (int)Helper.ExecuteService("LegislationFixation", "GetStarredToUnstarredCount", DMdl);
                model.ChangedRuleNoticeCount = (int)Helper.ExecuteService("LegislationFixation", "GetChangedRuleNoticeCount", DMdl);
                Noticecoiunt.AssemblyID = model.AssemblyID;
                Noticecoiunt.SessionID = model.SessionID;
                Noticecoiunt = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesForClubWithBracket", Noticecoiunt);
                model.CountForClub = Noticecoiunt.ResultCount;
                Noticecoiunt = (CutMotionModel)Helper.ExecuteService("Notice", "GetNoticesClubbed", Noticecoiunt);
                model.CountForClubbed = Noticecoiunt.Noticeclubbed;

                model.CustomMessage = (string)TempData["CallingMethod"];
                if ((string)TempData["CallingMethod"] == "StarredPending")
                {
                    ViewBag.TagID = "liStarred";
                }
                else if ((string)TempData["CallingMethod"] == "UnStarredPending")
                {
                    ViewBag.TagID = "liUnstarred";
                }
                else if ((string)TempData["CallingMethod"] == "NoticePending")
                {
                    ViewBag.TagID = "liNotice";
                }

                //add code for house committee count by robin - 23-june2017
                #region For received list count
                string DepartmentId = CurrentSession.DeptID;
                string Officecode = CurrentSession.OfficeId;
                string AadharId = CurrentSession.AadharId;
                string currtBId = CurrentSession.BranchId;
                string Year = "5";
                string papers = "1";
                string[] strngBIDN = new string[2];
                strngBIDN[0] = CurrentSession.BranchId;
                var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
                string[] str = new string[6];
                str[0] = DepartmentId;
                str[1] = Officecode;
                str[2] = AadharId;
                str[3] = value123[0];
                str[4] = Year;
                str[5] = papers;
                var ReceivedList = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
                model.ReceivedListCount = ReceivedList.Count;
                #endregion

                #region For Draft List Count
                string[] strD = new string[6];
                strD[0] = DepartmentId;
                strD[1] = Officecode;
                strD[2] = AadharId;
                strD[3] = currtBId;
                strD[4] = CurrentSession.Designation;
                strD[5] = "1";
                var DraftListCount = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
                model.DraftListCount = DraftListCount.Count;
                #endregion

                #region For Send List Count
                string[] strS = new string[6];
                strS[0] = DepartmentId;
                strS[1] = Officecode;
                strS[2] = AadharId;
                strS[3] = currtBId;
                strS[4] = "5";
                strS[5] = "1";
                List<eFileAttachment> ListModelForSend = new List<eFileAttachment>();
                ListModelForSend = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetSendList", strS);
                model.SentPaperListCount = ListModelForSend.Count;
                #endregion

                Session["Model"] = model;

                return View(model);
            }

            else
            {
                return RedirectToAction("Login", "Account", new { @area = "" });
            }
        }

        public PartialViewResult GetAllDetailedSummary(int PaperCategoryTypeId, bool IsStarredQuestion = false)
        {


            return PartialView("_CompletePaperLaidDetails");

        }


        public PartialViewResult PaperLaidDetailedSummary(int PaperCategoryTypeId, bool ByMinistry = true, bool IsStarredQuestion = false)
        {
            var model = new PaperLaidDetailedSummaryViewModel()
            {
                PaperCategoryTypeId = PaperCategoryTypeId,
                IsByMinistery = ByMinistry,
                IsStarredQuestion = IsStarredQuestion,

            };
            var parameter = new tPaperLaidV()
            {
                EventId = PaperCategoryTypeId,
                AssemblyId = (IsStarredQuestion) ? 1 : 0,
                SessionId = (ByMinistry) ? 1 : 0
            };
            var Summary = (List<PaperLaidByPaperCategoryType>)Helper.ExecuteService("LegislationFixation", "GetPaperLaidSummaryForPaperCategory", parameter);
            var GroupedData = (from s in Summary
                               group s by new { s.Id, s.Name } into grp
                               select new Group<PaperLaidByPaperCategoryType, string, string> { IdKey = grp.Key.Id, NameKey = grp.Key.Name, Values = grp }).ToList();

            var PaperLaidSummary = new List<PaperLaidSummary>();

            foreach (var item in GroupedData)
            {
                var data = new PaperLaidSummary();
                data.Name = item.NameKey;
                data.Id = item.IdKey;
                int Recieved = 0;
                int CurrentLob = 0;
                int LaidInHome = 0;
                foreach (var sub in item.Values)
                {
                    Recieved = (sub.IsRecieved) ? Recieved + sub.Count : Recieved;
                    CurrentLob = (sub.IsCurrentLob) ? CurrentLob + sub.Count : CurrentLob;
                    LaidInHome = (sub.IsLaidInHome) ? LaidInHome + sub.Count : LaidInHome;
                }
                data.RecievedCount = Recieved;
                data.CurrentLobCount = CurrentLob;
                data.LaidInHomeCount = LaidInHome;
                PaperLaidSummary.Add(data);
            }
            model.PaperLaidList = PaperLaidSummary;
            return PartialView("_PaperLaidDetailedSummary", model);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id">Ministry id or depatrment id</param>
        /// <param name="PaperCategoryTypeId"></param>
        /// <param name="IsByMinistry">True: By Ministry,False: By Department, Null: By Paper Laid</param>
        /// <returns></returns>
        /// 

        #region "Changes Regarding New Template Implementations Start here"

        public ActionResult CompletePaperLaidInformation(string Id, int PaperCategoryTypeId, bool IsStarredQuestion = false, bool? IsByMinistry = null,
            int PageNumber = 1, int LoopStart = 1, int LoopEnd = 5, string Status = null, bool? ItemType = null)
        {
            CurrentSession.SetSessionAlive = null;

            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                Id = Sanitizer.GetSafeHtmlFragment(Id);
                Status = Sanitizer.GetSafeHtmlFragment(Status);
                var model = new CompletePaperLaidViewModel()
                {
                    PaperCategoryTypeId = PaperCategoryTypeId,
                    Id = Id,
                    IsStarredQuestion = IsStarredQuestion,
                    Status = Status,
                    ItemType = ItemType,


                    PagedList = new PagedList<PaperLaidViewModel>()
                    {
                        CurrentPage = PageNumber,
                        RowsPerPage = pageSize,
                        LoopEnd = LoopEnd,
                        LoopStart = LoopStart
                    }
                };
                return PartialView("_NewCompletePaperLaidDetails", model);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");

            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetCompletePaperLaidInformation(JqGridRequest request, int PaperCategoryTypeId, string Id, bool IsStarredQuestion, string Status, bool? ItemType)
        {
            var model = new CompletePaperLaidViewModel();

            var PaperCategoryType = (mPaperCategoryType)Helper.ExecuteService("LegislationFixation", "GetPapercategoryDetailsById",
                new mPaperCategoryType { PaperCategoryTypeId = PaperCategoryTypeId });

            model.PaperCategoryType = PaperCategoryType.Name;

            var ObjPaperLaid = new tPaperLaidV();
            var ObjQuestion = new tQuestion();
            var ObjNotice = new tMemberNotice();

            if (Status != "")
            {
                if (ItemType == null) //For Paper Laid
                {
                    ObjPaperLaid = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "GetCompletePaperLaidDetailsByStatus",
                        new tPaperLaidV { PaperCategoryTypeId = PaperCategoryTypeId, QuestionID = (!IsStarredQuestion) ? 0 : 1, Title = Status,UserID=Guid .Parse ( CurrentSession.UserID) , PAGE_SIZE = request.RecordsCount, PageIndex = request.PageIndex + 1, AssemblyId = (string.IsNullOrEmpty(CurrentSession.AssemblyId) ? 0 : Convert.ToInt16(CurrentSession.AssemblyId)), SessionId = (string.IsNullOrEmpty(CurrentSession.SessionId) ? 0 : Convert.ToInt16(CurrentSession.SessionId)) });
                }
                else if (ItemType == true) //For Question
                {
                    ObjQuestion = (tQuestion)Helper.ExecuteService("LegislationFixation", "GetCompletePaperLaidDetailsByStatus",
                        new tPaperLaidV { PaperCategoryTypeId = PaperCategoryTypeId, QuestionID = (!IsStarredQuestion) ? 0 : 1, Title = Status, UserID = Guid.Parse(CurrentSession.UserID), PAGE_SIZE = request.RecordsCount, PageIndex = request.PageIndex + 1, AssemblyId = (string.IsNullOrEmpty(CurrentSession.AssemblyId) ? 0 : Convert.ToInt16(CurrentSession.AssemblyId)), SessionId = (string.IsNullOrEmpty(CurrentSession.SessionId) ? 0 : Convert.ToInt16(CurrentSession.SessionId)) });
                }
                if (ItemType == false) //For Notice
                {
                    ObjNotice = (tMemberNotice)Helper.ExecuteService("LegislationFixation", "GetCompletePaperLaidDetailsByStatus",
                       new tPaperLaidV { PaperCategoryTypeId = PaperCategoryTypeId, QuestionID = (!IsStarredQuestion) ? 0 : 1, Title = Status, UserID = Guid.Parse(CurrentSession.UserID), PAGE_SIZE = request.RecordsCount, PageIndex = request.PageIndex + 1, AssemblyId = (string.IsNullOrEmpty(CurrentSession.AssemblyId) ? 0 : Convert.ToInt16(CurrentSession.AssemblyId)), SessionId = (string.IsNullOrEmpty(CurrentSession.SessionId) ? 0 : Convert.ToInt16(CurrentSession.SessionId)) });
                }
            }

            //Sorting based on Minister name
            //if (IsByMinistry != null && IsByMinistry == true)
            //{
            //    PaperLaidList = PaperLaidList.OrderBy(a => a.MinistryName).ToList();
            //    NoticeList = NoticeList.OrderBy(a => a.MinisterName).ToList();
            //    QuestionList = QuestionList.OrderBy(a => a.MinisterID).ToList();
            //    model.IsByMinisterWise = true;
            //}
            //else if (IsByMinistry != null && IsByMinistry == false)
            //{
            //    PaperLaidList = PaperLaidList.OrderBy(a => a.DeparmentName).ToList();
            //    NoticeList = NoticeList.OrderBy(a => a.DepartmentName).ToList();
            //    QuestionList = QuestionList.OrderBy(a => a.DepartmentID).ToList();

            //    model.IsByDepartmentWise = true;
            //}
            //else
            //{
            //    model.IsByPaperLaidWise = true;
            //}

            //Final Viewmodel values


            if (ItemType == null) //Paper Laid
            {
                //model.PagedList.TotalRecordCount = PaperLaidList == null ? 0 : PaperLaidList.Count();
                ////Sorting based on priority
                //PaperLaidList = (model.IsByDepartmentWise) ? PaperLaidList.OrderBy(a => a.DepartmentwisePriority).ToList() :
                //                (model.IsByMinisterWise) ? PaperLaidList.OrderBy(a => a.MinistrywisePriority).ToList() :
                //                (model.IsByPaperLaidWise) ? PaperLaidList.OrderBy(a => a.PaperlaidPriority).ToList() : PaperLaidList.OrderBy(a => a.PaperLaidId).ToList();

                //model.PagedList.List = PaperLaidList.ToViewModel().ToList();
                model.ResultCount = ObjPaperLaid.ResultCount;
                model.ListofData = ObjPaperLaid.ListtPaperLaidV.ToViewModel1().ToList();


            }
            else if (ItemType == true) //Question
            {
                //model.PagedList.TotalRecordCount = QuestionList == null ? 0 : QuestionList.Count();
                //model.PagedList.List = QuestionList.ToViewModel().ToList();
                model.ResultCount = ObjQuestion.ResultCount;
                model.ListofData = ObjQuestion.QuestionListForTrasData.ToViewModel1().ToList();

            }
            else if (ItemType == false) //Notice
            {
                //model.PagedList.TotalRecordCount = NoticeList == null ? 0 : NoticeList.Count();
                model.ResultCount = ObjNotice.ResultCount;
                model.ListofData = ObjNotice.memberNoticeList.ToList().ToViewModel1().ToList();

            }



            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)model.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = model.ResultCount,

            };


            var resultToSort = model.ListofData.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<CompletePaperLaidViewModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<CompletePaperLaidViewModel>(Convert.ToString(questionList.ItemId), new CompletePaperLaidViewModel(questionList)));

            return new JqGridJsonResult() { Data = response };

        }


        public PartialViewResult ViewPaperLaidDetails(string ItemId, bool? ITemType)
        {
            switch (ITemType)
            {
                case null:
                    {
                        var PaperLaid = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "GetPaperLaidDetailsById", new tPaperLaidV { PaperLaidId = Convert.ToInt64(ItemId) });

                        switch (PaperLaid.BOTStatus)
                        {
                            case 0:
                                PaperLaid.StatusText = "Sent By: " + PaperLaid.DeparmentName;
                                break;

                            case 1:
                                PaperLaid.StatusText = "Approved By Additional Commissioner and Send to Commissioner.";
                                break;

                            case 2:
                                PaperLaid.StatusText = "Approved By Commissioner and Send to Mayor.";
                                break;

                            case 3:
                                PaperLaid.StatusText = "Approved By Mayor and Send Back to Commissioner";
                                break;

                            case 4:
                                PaperLaid.StatusText = "Approved By Commissioner and Send back to Additional Commissioner";
                                break;

                            case 5:
                                PaperLaid.StatusText = "Approved By Additional Commissioner and Send to Meeting Assistance";
                                break;

                            case 6:
                                PaperLaid.StatusText = "Committe Meeting Discussion Approved By Meeting Assistance and Send to Additional Commissioner";
                                break;

                            case 7:
                                PaperLaid.StatusText = "Committe Meeting Discussion Approved By Additional Commissioner and Send to Commissioner";
                                break;

                            case 8:
                                PaperLaid.StatusText = "Committe Meeting Discussion Approved By Commissioner and Send to Mayor";
                                break;

                            case 9:
                                PaperLaid.StatusText = "Committe Meeting Discussion Approved By Mayor and Send Back to Commissioner";
                                break;

                            case 10:
                                PaperLaid.StatusText = "Committe Meeting Discussion Approved By Commissioner and Send back to Additional Commissioner";
                                break;

                            case 11:
                                PaperLaid.StatusText = "Committe Meeting Discussion Final Approved By Additional Commissioner and Send to Meeting Assistance for Memorandum to be laid in house";
                                break;

                            default:
                                break;

                        }


                        return PartialView("_PaperLaidDetailPopup", PaperLaid);
                    }
                case true:
                    {
                        var Question = (tQuestion)Helper.ExecuteService("LegislationFixation", "GetQuestionDetailsById", new tQuestion { QuestionID = Convert.ToInt32(ItemId) });
                        return PartialView("_SentQuestionDetails", Question);
                    }
                case false:
                    {
                        var Notice = (tMemberNotice)Helper.ExecuteService("LegislationFixation", "GetNoticeDetailsById", new tMemberNotice { NoticeId = Convert.ToInt32(ItemId) });
                        return PartialView("_SentNoticeDetails", Notice);
                    }
                default: return null;
            }

        }


        [HttpPost]
        public JsonResult UpdatePriority(long PaperlaidId, string Prioritytype, int Priority)
        {
            var parameter = new tPaperLaidV
            {
                PaperLaidId = PaperlaidId,
                PaperlaidPriority = Priority,
                MinistryName = Prioritytype

            };
            var model = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "UpdatePaperLaidPriority", parameter);

            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ApproveByCommisioner(long PaperlaidId, string remarks,string status)
        {
            SqlConnection con = ClsConnection.GetConnection();
            if (con.State == ConnectionState.Closed || con.State == ConnectionState.Broken)
                con.Open();
            try
            {
               
               
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@PaperlaidId", PaperlaidId);
                param[1] = new SqlParameter("@UserId", CurrentSession.UserID );
                param[2] = new SqlParameter("@remarks", remarks);
                param[3] = new SqlParameter("@status", Convert.ToInt32 (status)+1);
                SqlHelper.ExecuteNonQuery(con, "[ApproveMemo]", param);
               
               
            }
            finally
            {

                con.Close();
            }
           
        

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AssignPrioritybyId(string PLIds, string PNs)
        {
            DiaryModel Obj = new DiaryModel();

            // This Diary model is used as container for just passing parameter
            Obj.QuesIds = PLIds;
            Obj.FixNums = PNs;
            Obj.Message = (string)Helper.ExecuteService("LegislationFixation", "AssignPrioritybyId", Obj);

            return Json(Obj.Message, JsonRequestBehavior.AllowGet);
        }

        #endregion




        #region Private Methods

        #endregion

        #region Added By Nitin

        public ActionResult GetUpdatedPaperList(string type, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
        {

            if (CurrentSession.UserID != null)
            {

                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);


                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mEvent> PaperCategoryTypeList = Helper.ExecuteService("LegislationFixation", "GetOtherPaperLaidPaperCategoryType", PaperCategoryTypeListMod) as List<mEvent>;
                tPaperLaidDepartmentModel.mEvents = PaperCategoryTypeList;

                #endregion

                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                objPaperModel.ResultCount = int.Parse(ResultCount);
                tPaperLaidV UpdatedPapers = new tPaperLaidV();
                UpdatedPapers.PageIndex = int.Parse(PageNumber);
                UpdatedPapers.PAGE_SIZE = int.Parse(RowsPerPage);
                UpdatedPapers = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "GetUpdatedOtherPaperList", UpdatedPapers);
                if (UpdatedPapers != null)
                {
                    if (UpdatedPapers.DepartmentSubmitList.Count > 0)
                    {
                        tPaperLaidDepartmentModel.DepartmentSubmitList = UpdatedPapers.DepartmentSubmitList;
                    }
                }
                if (tPaperLaidDepartmentModel.DepartmentSubmitList.Count > 0)
                {
                    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentSubmitList.Count;
                }
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("20");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetUpdatedPapersList", tPaperLaidDepartmentModel);
            }
            return RedirectPermanent("/Notices/LegislationFixation/PaperLaidSummary");
        }

        public ActionResult FullViewUpdatedPaperLaidDetailByID(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {

                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("LegislationFixation", "ShowUpdatedOtherPaperLaidDetailByID", movementModel);
                return PartialView("_ShowUpdatedOtherPaperLaidDetailByID", movementModel);
            }
            return null;
        }

        public ActionResult SubmittAllCheckedIds(string PaperIds)
        {
            bool doRedirect = false;
            if (!String.IsNullOrEmpty(PaperIds))
            {
                int[] Ids = PaperIds.Split(',').Select(i => Convert.ToInt32(i)).ToArray();
                foreach (int id in Ids)
                {
                    PaperMovementModel model = new PaperMovementModel();
                    model.PaperLaidId = id;
                    model = (PaperMovementModel)Helper.ExecuteService("LegislationFixation", "GetDetailsByPaperLaidId", model);
                    string existFileUrl = model.actualFilePath;
                    string replaceFileUrl = model.FilePath;
                    string DirName = Path.GetDirectoryName(replaceFileUrl);
                    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(DirName));
                    if (!Dir.Exists)
                    { Dir.Create(); }
                    string actualFileName = replaceFileUrl;
                    string fullSourceFile = Server.MapPath(existFileUrl);
                    string fullDestinationFile = Server.MapPath(replaceFileUrl);
                    System.IO.File.Copy(fullSourceFile, fullDestinationFile, true);
                    //HttpPostedFile file;
                    //file.FileName = existFileUrl;
                    //file.SaveAs(Server.MapPath(actualFileName));
                    model = (PaperMovementModel)Helper.ExecuteService("LegislationFixation", "UpdateLOBTempIdByPaperLiadID", model);
                    doRedirect = true;
                }
                if (doRedirect)
                {
                    return Json(new
                    {
                        RedirectUrl = Url.Action("PaperLaidSummary", "LegislationFixation")
                    });
                }

            }
            return Json(new
            {
                RedirectUrl = Url.Action("PaperLaidSummary", "LegislationFixation")
            });
        }


        #endregion

        #region "Method Added By Sunil"
        public PartialViewResult PaperDetailWithAction(string ItemId, bool? ITemType)
        {

            switch (ITemType)
            {
                case null:
                    {
                        var PaperLaid = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "GetPaperLaidDetailsById", new tPaperLaidV { PaperLaidId = Convert.ToInt64(ItemId) });
                        PaperLaid.UserID =Guid.Parse (CurrentSession.UserID);
                        switch (PaperLaid.BOTStatus)
                        {
                            case 0:
                                PaperLaid.StatusText = "Sent By: " + PaperLaid.DeparmentName;
                                break;

                            case 1:
                                PaperLaid.StatusText = "Approved By Additional Commissioner and Send to Commissioner.";
                                break;

                            case 2:
                                 PaperLaid.StatusText  = "Approved By Commissioner and Send to Mayor.";
                                break;

                            case 3:
                                 PaperLaid.StatusText  = "Approved By Mayor and Send Back to Commissioner";
                                break;

                            case 4:
                                 PaperLaid.StatusText  = "Approved By Commissioner and Send back to Additional Commissioner";
                                break;

                            case 5:
                                 PaperLaid.StatusText  = "Approved By Additional Commissioner and Send to Meeting Assistance";
                                break;

                            case 6:
                                 PaperLaid.StatusText  = "Committe Meeting Discussion Approved By Meeting Assistance and Send to Additional Commissioner";
                                break;

                            case 7:
                                 PaperLaid.StatusText  = "Committe Meeting Discussion Approved By Additional Commissioner and Send to Commissioner";
                                break;

                            case 8:
                                 PaperLaid.StatusText  = "Committe Meeting Discussion Approved By Commissioner and Send to Mayor";
                                break;

                            case 9:
                                 PaperLaid.StatusText  = "Committe Meeting Discussion Approved By Mayor and Send Back to Commissioner";
                                break;

                            case 10:
                                 PaperLaid.StatusText  = "Committe Meeting Discussion Approved By Commissioner and Send back to Additional Commissioner";
                                break;

                            case 11:
                                 PaperLaid.StatusText  = "Committe Meeting Discussion Final Approved By Additional Commissioner and Send to Meeting Assistance for Memorandum to be laid in house";
                                break;

                            default:
                                break;

                        }




                        return PartialView("_PaperLaidDetailPopup", PaperLaid);
                    }
                case true:
                    {
                        var Question = (tQuestion)Helper.ExecuteService("LegislationFixation", "GetQuestionDetailsById", new tQuestion { QuestionID = Convert.ToInt32(ItemId) });
                        if (Question.NoticeRecievedTime != null)
                        {
                            TimeSpan timespan = new TimeSpan(Question.NoticeRecievedTime.Value.Hours, Question.NoticeRecievedTime.Value.Minutes, 00);
                            DateTime dtTemp = DateTime.ParseExact(timespan.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
                            Question.StringTime = dtTemp.ToString("hh:mm tt");
                        }

                        var filacessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                        ViewBag.FileAcessPath = filacessingSettings.SettingValue;
                        return PartialView("_DetailWithAction", Question);
                    }
                case false:
                    {
                        var Notice = (tMemberNotice)Helper.ExecuteService("LegislationFixation", "GetNoticeDetailsById", new tMemberNotice { NoticeId = Convert.ToInt32(ItemId) });
                        return PartialView("_DetailWithActionNotice", Notice);
                    }
                default: return null;
            }

        }


        public ActionResult EditQuestionNotice(int Id, string EditType, string IsQues, string IsLocal)
        {
            DiaryModel DMdl = new DiaryModel();
            DiaryModel DMdl2 = new DiaryModel();

            if (EditType == "Question")
            {
                if (Id != 0)
                {

                    DMdl.QuestionId = Id;
                }

                DMdl2 = Helper.ExecuteService("LegislationFixation", "GetQuestionByIdForEdit", DMdl) as DiaryModel;
                DMdl = DMdl2;

                if (DMdl.DateForBind != null)
                {
                    DMdl.stringDate = Convert.ToDateTime(DMdl.DateForBind).ToString("dd/MM/yyyy");
                }

                if (IsQues == null || IsQues == "")
                {
                    IsQues = "false";
                }

                DMdl.IsQuestionInPart = Convert.ToBoolean(IsQues);

                if (IsLocal == null || IsLocal == "")
                {
                    IsLocal = "false";
                }
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                if (Id != null)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                {
                    string savedPDF = Helpers.Helper.ExecuteService("Questions", "GetAttachment", Id) as string;
                    ViewBag.PDFPath = savedPDF;
                }
                DMdl.IsHindi = Convert.ToBoolean(IsLocal);
                DMdl.ViewType = "QDraft";
                return PartialView("_EditQuestion", DMdl);

            }
            else if (EditType == "Notice")
            {
                if (Id != 0)
                {

                    DMdl.NoticeId = Id;
                }

                DMdl2 = Helper.ExecuteService("LegislationFixation", "GetNoticeByIdForEdit", DMdl) as DiaryModel;
                DMdl = DMdl2;
                foreach (var test in DMdl.DemandList)
                {
                    string val = test.DemandName + "(" + test.DemandNo + ")";
                    test.DemandName = val;
                }
                if (DMdl.DateForBind != null)
                {
                    DMdl.stringDate = Convert.ToDateTime(DMdl.DateForBind).ToString("dd/MM/yy");
                }
                if (DMdl.TimeForBind != null)
                {
                    DMdl.NoticeTime = (TimeSpan)DMdl.TimeForBind;
                }
                DMdl.ViewType = "NDraft";
                return PartialView("_EditNotice", DMdl);
            }



            return null;
        }
        public JsonResult GetDepartmentByMinister(int MinistryId)
        {
            DiaryModel DMdl = new DiaryModel();
            DMdl.MinistryId = MinistryId;
            DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);
            return Json(DMdl.DiaryList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetConstByMemberId(int MemberId, int AssemId)
        {
            DiaryModel DMdl = new DiaryModel();
            if (MemberId != 0)
            {
                DMdl.MemberId = MemberId;
            }
            if (AssemId != 0)
            {
                DMdl.AssemblyID = AssemId;
            }

            DMdl = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetConstByMemberId", DMdl);

            char[] a = DMdl.ConstituencyName.ToLower().ToCharArray();

            for (int i = 0; i < a.Count(); i++)
            {
                a[i] = i == 0 || a[i - 1] == ' ' ? char.ToUpper(a[i]) : a[i];

            }
            string result = string.Join("", a);
            DMdl.ConstituencyName = result;
            return Json(DMdl, JsonRequestBehavior.AllowGet);
        }

        public string UppercaseFirstEach(string s)
        {
            char[] a = s.ToLower().ToCharArray();

            for (int i = 0; i < a.Count(); i++)
            {
                a[i] = i == 0 || a[i - 1] == ' ' ? char.ToUpper(a[i]) : a[i];

            }
            string result = string.Join("", a);
            return result;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveNotice(DiaryModel Dmdl)
        {
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                Dmdl.SubmittedBy = CurrentSession.UserID;
            }

            TempData["CallingMethod"] = "NoticePending";


            TempData["Message"] = Helper.ExecuteService("LegislationFixation", "SaveNotice", Dmdl) as string;

            if (Dmdl.EventId == 74 || Dmdl.EventId == 75 || Dmdl.EventId == 76)
            {
                GeneratePdfForCutMotion(Dmdl.NoticeId.ToString());
            }
            if (Dmdl.EventId == 39)
            {
                GeneratePdfForNotices324(Dmdl.NoticeId.ToString());
            }

            return RedirectToAction("PaperLaidSummary", "LegislationFixation");
        }


        public string GeneratePdfForCutMotion(string Id)
        {
            try
            {
                string outXml = "";
                string outInnerXml = "";
                string LOBName = Id;
                string fileName = "";
                string savedPDF = string.Empty;
#pragma warning disable CS0219 // The variable 'HeadingText' is assigned but its value is never used
                string HeadingText = "";
#pragma warning restore CS0219 // The variable 'HeadingText' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'HeadingTextEnglish' is assigned but its value is never used
                string HeadingTextEnglish = "";
#pragma warning restore CS0219 // The variable 'HeadingTextEnglish' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'ViewBy' is assigned but its value is never used
                string ViewBy = "";
#pragma warning restore CS0219 // The variable 'ViewBy' is assigned but its value is never used

                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);



                tMemberNotice model = new tMemberNotice();
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                //  string strDate = Id;
                //  DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
                //  model.NoticeDate = date;
                //  model.DataStatus = ViewBy;
                model.NoticeId = Convert.ToInt32(Id);
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetAllDetailsForCutmotionPdfpdf", model);

                //if (model.tQuestionModel != null)
                //{
                //    foreach (var item in model.tQuestionModel)
                //    {
                //        model.SessionDateId = item.SessionDateId;
                //        model.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", model);
                //        model.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", model);
                //    }
                //}
                //    model = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", model);

                if (model.tQuestionModel != null)
                {

                    //foreach (var item in model.tQuestionModel)
                    //{

                    outXml = @"<html>                           
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:40px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                    outXml += @"<div> 

 <style type='text/css'>
@page {
	@bottom-left {
		content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
		font-family: sans-serif;
		font-size: 80%;
	}
	@bottom-right {
		content: 'Page ' counter(page) ' of ' counter(pages);
		font-family: sans-serif;
		font-size: 80%;
	}
}
</style>
					</div>
			
<table style='width:100%'>
 
	  <tr>
		  <td style='text-align:center;font-size:28px;' >
			 <b>हिमाचल प्रदेश तेरहवीं विधान सभा</b>
			  </td>
			  </tr>
	  </table>
<table style='width:100%'>
	 <tr>
		  <td style='text-align:center;font-size:20px;' >
			<b>" + "(" + "स्वीकृत कटौती प्रस्ताव" + ")" + @"</b> 
			  </td>
			  </tr>
		   </table>
<br />
<table style='width:100%'>
	 <tr>
		  <td style='text-align:center;font-size:20px;'>
			<b>" + "बजट अनुमान 2019-2020 की अनुदान मांगें" + @"</b>          
			  </td>    
			  </tr>
		   </table>
<table style='width:100%'>
	  <tr>
		  <td style='text-align:center;font-size:20px;' >
		  <b>" + "कटौती प्रस्ताव की सूचना" + @"</b>   
			  </td>
		 
			  </tr>
		   </table>
<table style='width:100%'>
	  <tr>
		  <td style='text-align:center;font-size:20px;' >
		  <b>" + "मांग संख्या: " + model.CMDemandId + " - " + model.DemandName + @"</b>   
			  </td>
		 
			  </tr>
		   </table>
<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:20%; vertical-align: top; font-size:20px;cfont-weight: bold;text-align:left;'>
  <b>" + "क्र0 सं0" + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + "सदस्य का नाम" + @"</b>
</td>
<td style='width:20%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + "कटौती प्रस्ताव" + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;text-align:right;'>
<b>" + "मांग संख्या" + @"</b>
</td>
</tr>

</table>
<table style='width:100%'>
	 <tr>
		  <td style='text-align:center;font-size:20px;' >
 .............................................................................................................................
			
			  </td>
			  </tr>
		   </table>";

                    foreach (var item in model.tQuestionModel)
                    {
                        if (item.IsClubbed != null)
                        {
                            string[] values = item.CMemNameHindi.Split(',');
                            outInnerXml = "";
                            for (int i = 0; i < values.Length; i++)
                            {
                                values[i] = values[i].Trim();
                                outInnerXml += @"<div>
		  " + values[i] + @" :

							   </div>
							 ";
                            }

                            string MQues = WebUtility.HtmlDecode(item.Notice);
                            MQues = MQues.Replace("<p>", "");
                            MQues = MQues.Replace("</p>", "");
                            outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>

</td>
<td style='font-size:20px;padding-bottom: 20px;font-weight: bold;text-align:left;'>
<b>" + "1" + @"</b>
</td>

<td style='font-size:20px;padding-bottom: 20px;font-weight: bold;text-align:center;'>
<b>" + model.EventName + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 20px;font-weight: bold;text-align:right;'>
<b>" + model.CMDemandId + @"</b>
</td>
</tr>

<tr>
<td>
</td>
</tr>                   
		 </tr>
		   </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>

</td>
<td style='font-size:20px;padding-bottom: 13px;'>
</td>

<td style='font-size:20px;padding-bottom: 13px;text-align:center;'>
<b>" + item.Subject + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
</td>
</tr>

<tr>
<td>
</td>

</tr>

</table>



<table style='width:100%;text-align:left;'>
<tr>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>
</table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='font-size:20px;padding-bottom: 13px;width:200px'>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.Notice + @"</b>
</td>
</tr>
</table>
";



                        }


                        if (model.StampTypeHindi == null)
                        {
                            // foreach (var item1 in model.Values)
                            //{

                            outXml += @"
 

<table style='width:100%'>
	 <tr>
		  <td style='text-align:left;font-size:20px;' >
	  
<b>" + "शिमला-171004." + @"</b>
			  </td>
<td style='text-align:right;font-size:20px;' >
					  <b>" + "सचिव" + @",</b>
			  </td>
		 
			  </tr>
<tr>
		  <td style='text-align:left;font-size:20px;' >
		   
		 <b>दिनांक " + Convert.ToDateTime(model.SubmittedDate).ToString("dd.MM.yyyy") + @".</b>
			  </td>
<td style='text-align:right;font-size:20px;' >
	   
  <b>" + "विधान सभा।" + @"</b>          
			  </td>
		 
			  </tr>
		   </table>
	   
					 </body> </html>";
                            //}

                        }
                        else
                        {
                            foreach (var item1 in model.Values)
                            {

                                outXml += @"
 

<table style='width:100%'>
	 <tr>
		  <td style='text-align:left;font-size:20px;color:#3621E8;' >
	  
<b>" + item.SignaturePlace + @".</b>
			  </td>
<td style='text-align:right;font-size:20px;color:#3621E8;' >
					  <b>" + item.SignatureName + @",</b>
			  </td>
		 
			  </tr>
<tr>
		  <td style='text-align:left;font-size:20px;color:#3621E8;' >
		   
		 <b>Dated: " + item.SignatureDate + @".</b>
			  </td>
<td style='text-align:right;font-size:20px;color:#3621E8;' >
	   
  <b>" + item.SignatureDesignations + @".</b>          
			  </td>
		 
			  </tr>
		   </table>
	   
					 </body> </html>";
                            }
                        }
                    }
                }


                MemoryStream output = new MemoryStream();

                PdfConverter pdfConverter = new PdfConverter();

                // set the license key
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";


                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 60;


                pdfConverter.PdfFooterOptions.FooterHeight = 60;
                //write the page number
                TextElement footerTextElement = new TextElement(0, 30, "&p;",
                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }

                string url = "Question" + "/CutMotion/" + model.AssemblyID + "/" + model.SessionID + "/";
                fileName = model.AssemblyID + "_" + model.SessionID + "_CutMotion_" + model.NoticeId + ".pdf";
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                //string directory = Server.MapPath(url);
                string directory = model.FilePath + url;
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                //var path = Path.Combine(Server.MapPath("~" + url), fileName);
                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);



                // close file stream
                _FileStream.Close();
                model.FilePath = url;
                model.FileName = fileName;

                // DocFilePath is using for Accessing File 
                // CurrentSession.FileAccessPath = model.DocFilePath;
                //  var AccessPath = "/" + url + fileName;
                model.Msg = Helper.ExecuteService("Notice", "UpdateFilePathandName", model) as string;


                return "Success";
            }

            catch (Exception ex)
            {

                //throw ex;
                return ex.Message;
            }
            finally
            {

            }


        }


        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveQuestion(DiaryModel Dmdl)
        {
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                Dmdl.SubmittedBy = CurrentSession.UserID;
            }

            if (Dmdl.QuestionTypeId == (int)QuestionType.StartedQuestion)
            {
                TempData["CallingMethod"] = "StarredPending";
            }
            else if (Dmdl.QuestionTypeId == (int)QuestionType.UnstaredQuestion)
            {
                TempData["CallingMethod"] = "UnstarredPending";
            }

            TempData["Message"] = Helper.ExecuteService("LegislationFixation", "SaveQuestion", Dmdl) as string;

            if (Convert.ToString(TempData["Message"]) != "")
            {
                if (TempData["Message"].ToString().IndexOf("_") > 0)
                {
                    string ApprovedDate = TempData["Message"].ToString().Substring(0, TempData["Message"].ToString().IndexOf("_"));
                    TempData["Message"] = TempData["Message"].ToString().Substring(TempData["Message"].ToString().IndexOf("_") + 1);
                    if (TempData["Message"].ToString() == "Approved Successfully !")
                    {

                        String SavedPdfPath = "";
                        //Generate Pdf
                        if (Dmdl.QuestionTypeId == (int)QuestionType.StartedQuestion)
                        {
                            SavedPdfPath = GeneratePdfByQuesIdStarred_Unfix(Dmdl.QuestionId);
                        }
                        else if (Dmdl.QuestionTypeId == (int)QuestionType.UnstaredQuestion)
                        {
                            SavedPdfPath = GeneratePdfByQuesIdUnstarred_Unfix(Dmdl.QuestionId);
                        }

                        string moduleName = ConfigurationManager.AppSettings["QuestionsmoduleName"];
                        string moduleActionName = ConfigurationManager.AppSettings["QuestionsmoduleActionName"];
                        var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "FinalApprovedSMSEMAIL", null);

                        if (NewsSettings.SettingValue == "True")
                        {
                            SendNotificationsModel model = new SendNotificationsModel();

                            List<RecipientGroup> contactGroups = new List<RecipientGroup>();
                            //Get the Record from the SystemModules on the basis of the moduleName.
                            SystemModuleModel systemMoule = new SystemModuleModel();
                            systemMoule.Name = moduleName;
                            var moduleObj = Helper.ExecuteService("SystemModule", "GetSystemModuleByName", systemMoule) as SystemModuleModel;

                            SystemModuleModel actionObj = new SystemModuleModel();

                            if (moduleObj != null)
                            {
                                //Get the record from the SystemFunctions on the basis of moduleName and actionName.
                                SystemModuleModel moduleAction = new SystemModuleModel();
                                moduleAction.ActionName = moduleActionName;
                                actionObj = Helper.ExecuteService("SystemModule", "GetModuleActionByName", moduleAction) as SystemModuleModel;
                            }

                            if (actionObj != null)
                            {
                                #region Get All The Contacts Associated to the action.
                                if (!string.IsNullOrEmpty(actionObj.AssociatedContactGroups))
                                {
                                    //pass the Comma seperated Associated Contact Groups to a Function and get all the group details at a time.
                                    contactGroups = Helper.ExecuteService("ContactGroups", "GetContactGroupsByGroupIDs", new RecipientGroupMember { AssociatedContactGroups = actionObj.AssociatedContactGroups, DepartmentCode = Dmdl.DepartmentId }) as List<RecipientGroup>;
                                }
                                #endregion
                            }

                            if (ApprovedDate != "")
                            {
                                // ApprovedDate = Convert.ToDateTime(ApprovedDate).ToString("dd/MM/yyyy");
                            }
                            var modelSMS = new { DiaryNumber = Dmdl.DiaryNumber, IsInitialApprovedDate = ApprovedDate };

                            string SMSTemplateText = actionObj.SMSTemplate;
                            string emailTemplateText = actionObj.EmailTemplate;
                            int groupID = 0;
                            List<string> emailAddresses = new List<string>();
                            List<string> phoneNumbers = new List<string>();

                            try
                            {
                                //SMS Text
                                SMSTemplateText = SMSTemplateText.Replace("@Model.DiaryNumber", modelSMS.DiaryNumber);
                                SMSTemplateText = SMSTemplateText.Replace("@Model.IsInitialApprovedDate", modelSMS.IsInitialApprovedDate);
                                model.SMSTemplate = SMSTemplateText;

                                //Email Text
                                emailTemplateText = emailTemplateText.Replace("@Model.DiaryNumber", modelSMS.DiaryNumber);
                                emailTemplateText = emailTemplateText.Replace("@Model.IsInitialApprovedDate", modelSMS.IsInitialApprovedDate);
                                model.EmailTemplate = emailTemplateText;

                                //model.SMSTemplate = Razor.Parse(SMSTemplateText, modelSMS);
                                //model.EmailTemplate = Razor.Parse(emailTemplateText, modelSMS);

                                model.availableRecipientGroups = contactGroups != null ? contactGroups : new List<RecipientGroup>();
                                model.selectedRecipientGroups = contactGroups != null ? contactGroups : new List<RecipientGroup>();
                                model.SendSMS = actionObj.SendSMS;
                                model.SendEmail = actionObj.SendEmail;

                                foreach (var item in model.selectedRecipientGroups)
                                {
                                    groupID = item.GroupID;
                                }
                                //var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", new RecipientGroupMember { GroupID = groupID }) as List<RecipientGroupMember>;
                                var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByNewFixed", new RecipientGroupMember { DepartmentCode = Dmdl.DepartmentId }) as List<RecipientGroupMember>;

                                if (contactGroupMembers != null)
                                {
                                    foreach (var item in contactGroupMembers)
                                    {
                                        if (item.Email.Contains(','))
                                        {
                                            string[] emails = item.Email.Split(',');
                                            foreach (var em in emails)
                                            {
                                                emailAddresses.Add(em);
                                            }
                                        }

                                        else
                                            emailAddresses.Add(item.Email);

                                        if (item.MobileNo.Contains(','))
                                        {
                                            string[] numbers = item.MobileNo.Split(',');
                                            foreach (var mb in numbers)
                                            {
                                                phoneNumbers.Add(mb);
                                            }
                                        }
                                        else
                                            phoneNumbers.Add(item.MobileNo);

                                        // phoneNumbers.Add(item.MobileNo);
                                        //model.EmailAddresses = item.Email;
                                        //model.PhoneNumbers = item.MobileNo;
                                    }
                                }
                            }
                            catch (Exception)
                            {

                                throw;
                            }


                            //Sending Mail



                            if (!string.IsNullOrEmpty(model.EmailAddresses))
                                emailAddresses = model.EmailAddresses.Split(',').Select(s => s).ToList();

                            if (!string.IsNullOrEmpty(model.PhoneNumbers))
                                phoneNumbers = model.PhoneNumbers.Split(',').Select(s => s).ToList();

                            SMSMessageList smsMessage = new SMSMessageList();
                            CustomMailMessage emailMessage = new CustomMailMessage();


                            // SMS Settings                      
                            smsMessage.SMSText = model.SMSTemplate;
                            smsMessage.ModuleActionID = 1;
                            smsMessage.UniqueIdentificationID = 1;
                            //smsMessage.MobileNo = new List<string>();
                            //smsMessage.MobileNo.Add("8894681909");

                            // Email Settings 

                            //emailMessage.ModuleActionID = 1;
                            //emailMessage.UniqueIdentificationID = 1;
                            //EAttachment ea = new EAttachment { FileName = new FileInfo(Server.MapPath(SavedPdfPath)).Name, FileContent = System.IO.File.ReadAllBytes(Server.MapPath(SavedPdfPath)) };
                            EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes(SavedPdfPath) };
                            emailMessage._attachments = new List<EAttachment>();
                            emailMessage._attachments.Add(ea);

                            emailMessage._isBodyHtml = true;
                            emailMessage._subject = "Question with Diary Number : " + Dmdl.DiaryNumber + " Iniatially Approved ";
                            emailMessage._body = model.EmailTemplate;




                            foreach (var item in phoneNumbers)
                            {
                                smsMessage.MobileNo.Add(item);
                            }

                            foreach (var item in emailAddresses)
                            {
                                emailMessage._toList.Add(item);
                            }

                            Notification.Send(model.SendSMS, model.SendEmail, smsMessage, emailMessage);
                        }
                    }




                }
            }


            return RedirectToAction("PaperLaidSummary", "LegislationFixation");
        }

        // change name of function GenerateDeptbyPdf to GeneratePdfByQuesIdStarred_Unfix and Also create GeneratePdfByQuesIdUnstarred for Unstarred
        public string GeneratePdfByQuesIdStarred_Unfix(int QuestionId)
        {
            try
            {
                string str1 = "";
                string empty = string.Empty;
                tMemberNotice tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)new tMemberNotice()
                {
                    QuestionID = QuestionId,
                    Updatedate = new DateTime?(DateTime.Now)
                });
                MemoryStream memoryStream = new MemoryStream();
                EvoPdf.Document document = new EvoPdf.Document();
                document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document.CompressionLevel = PdfCompressionLevel.Best;
                document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                if (tMemberNotice.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? isHindi = questionModelCustom.IsHindi;
                        bool flag = true;
                        if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                        {
                            str1 = "<html >                           \r\n\t\t\t\t\t\t\t\t <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                            str1 += "<div> \r\n\t\t\t\t\t</div>\r\n\t\t\t\r\n\r\n<table style='width:100%'>\r\n \r\n\t  <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n\t\t\tनगर निगम प्रश्न   \r\n\t\t\t  </td>\r\n\t\t\t  </tr>\r\n\t  </table>";
                        }
                        else
                        {
                            str1 = "<html >                           \r\n\t\t\t\t\t\t\t\t <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                            str1 += "<div> \r\n\t\t\t\t\t</div>\r\n\t\t\t\r\n\r\n<table style='width:100%'>\r\n \r\n\t  <tr>\r\n  <td style='text-align:center;font-size:27px;' >\r\n\t\t\tQUESTION FOR MUNCIPAL CORPORATION  \r\n\t\t\t  </td>\r\n\t\t\t  </tr>\r\n\t  </table>";
                        }
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? nullable = questionModelCustom.IsClubbed;
                        if (nullable.HasValue)
                        {
                            nullable = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            string str2;
                            if (nullable.GetValueOrDefault() == flag1 & nullable.HasValue)
                            {
                                string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                str2 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str2 = str2 + "<div>\r\n\t\t  " + strArray[index] + " \r\n\r\n\t\t\t\t\t\t\t   </div>\r\n\t\t\t\t\t\t\t ";
                                }
                            }
                            else
                            {
                                string[] strArray = questionModelCustom.CMemName.Split(',');
                                str2 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str2 = str2 + "<div>\r\n\t\t  " + strArray[index] + " \r\n\r\n\t\t\t\t\t\t\t   </div>\r\n\t\t\t\t\t\t\t ";
                                }
                            }
                            nullable = questionModelCustom.IsHindi;
                            bool flag2 = true;
                            if (nullable.GetValueOrDefault() == flag2 & nullable.HasValue)
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n\r\n\t<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t विषय: " + questionModelCustom.Subject + "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t*" + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str2 + "\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n\t\t\t\t\t </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n\r\n\t<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t  D. No.: " + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t subject: " + questionModelCustom.Subject + "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t*" + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str2 + "\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be pleased to state :\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n\t\t <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n\t\t\t\t\t </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                        else
                        {
                            int? ministerId = questionModelCustom.MinisterID;
                            int num = 90;
                            if (ministerId.GetValueOrDefault() == num & ministerId.HasValue)
                            {
                                nullable = questionModelCustom.IsHindi;
                                bool flag = true;
                                if (nullable.GetValueOrDefault() == flag & nullable.HasValue)
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t  डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t विषय: " + questionModelCustom.Subject + "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t*" + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberNameLocal + "(" + questionModelCustom.ConstituencyName_Local + ")\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n\t\t <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>     \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n\t\t\t\t\t </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                    continue;
                                }
                            }
                            nullable = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            if (nullable.GetValueOrDefault() == flag1 & nullable.HasValue)
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t विषय: " + questionModelCustom.Subject + "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t*" + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberNameLocal + "(" + questionModelCustom.ConstituencyName_Local + ")\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n\t\t <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>     \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n\t\t\t\t\t </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t  D. No.: " + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t subject: " + questionModelCustom.Subject + "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t*" + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberName + "(" + questionModelCustom.ConstituencyName + ")\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be pleased to state :\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n\t\t <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n\t\t\t\t\t </body> </html>";
                                string htmlStringToConvert = str1;
                                string htmlStringBaseURL = "";
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                    }
                    string str3 = str1 + "\r\n\r\n\r\n\r\n\t\t\t\t   \r\n\t\t\t\t\r\n\t\t\t\t\t </body> </html>";
                }
                byte[] buffer = document.Save();
                try
                {
                    memoryStream.Write(buffer, 0, buffer.Length);
                    memoryStream.Position = 0L;
                }
                finally
                {
                    document.Close();
                }
                string str4 = "Question/BeforeFixation/" + tMemberNotice.AssemblyID.ToString() + "/" + tMemberNotice.SessionID.ToString() + "/";
                string[] strArray1 = new string[6];
                int num1 = tMemberNotice.AssemblyID;
                strArray1[0] = num1.ToString();
                strArray1[1] = "_";
                num1 = tMemberNotice.SessionID;
                strArray1[2] = num1.ToString();
                strArray1[3] = "_S_";
                num1 = tMemberNotice.QuestionID;
                strArray1[4] = num1.ToString();
                strArray1[5] = ".pdf";
                string path2 = string.Concat(strArray1);
                this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                string str5 = tMemberNotice.FilePath + str4;
                if (!Directory.Exists(str5))
                    Directory.CreateDirectory(str5);
                FileStream fileStream = new FileStream(Path.Combine(str5, path2), FileMode.Create, FileAccess.Write);
                fileStream.Write(buffer, 0, buffer.Length);
                fileStream.Close();
                return str5 + path2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GeneratePdfByQuesIdUnstarred_Unfix(int QuestionId)
        {
            try
            {
                string str1 = "";
                string str2 = string.Empty;
                tMemberNotice tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)new tMemberNotice()
                {
                    QuestionID = QuestionId
                });
                MemoryStream memoryStream = new MemoryStream();
                EvoPdf.Document document = new EvoPdf.Document();
                document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document.CompressionLevel = PdfCompressionLevel.Best;
                document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                if (tMemberNotice.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? isHindi = questionModelCustom.IsHindi;
                        bool flag = true;
                        if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                        {
                            str1 = "<html >                           \r\n\t\t\t\t\t\t\t\t <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            str1 += "<div> \r\n\t\t\t\t\t</div>\r\n\t\t\t\r\n\r\n<table style='width:100%'>\r\n \r\n\t  <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n\t\t\tनगर निगम प्रश्न   \r\n\t\t\t  </td>\r\n\t\t\t  </tr>\r\n\t  </table>";
                        }
                        else
                        {
                            str1 = "<html >                           \r\n\t\t\t\t\t\t\t\t <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            str1 += "<div> \r\n\t\t\t\t\t</div>\r\n\t\t\t\r\n\r\n<table style='width:100%'>\r\n \r\n\t  <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n\t\t\tQUESTION FOR MUNCIPAL CORPORATION  \r\n\t\t\t  </td>\r\n\t\t\t  </tr>\r\n\t  </table>";
                        }
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? nullable = questionModelCustom.IsClubbed;
                        DateTime dateTime;
                        if (nullable.HasValue)
                        {
                            nullable = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            string str3;
                            if (nullable.GetValueOrDefault() == flag1 & nullable.HasValue)
                            {
                                string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                str3 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str3 = str3 + "<div>\r\n\t\t  " + strArray[index] + " \r\n\r\n\t\t\t\t\t\t\t   </div>\r\n\t\t\t\t\t\t\t ";
                                }
                            }
                            else
                            {
                                string[] strArray = questionModelCustom.CMemName.Split(',');
                                str3 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str3 = str3 + "<div>\r\n\t\t  " + strArray[index] + " \r\n\r\n\t\t\t\t\t\t\t   </div>\r\n\t\t\t\t\t\t\t ";
                                }
                            }
                            nullable = questionModelCustom.IsHindi;
                            bool flag2 = true;
                            if (nullable.GetValueOrDefault() == flag2 & nullable.HasValue)
                            {
                                string[] strArray = new string[18];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n\r\n\t<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t डायरी संख्या: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t विषय: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  सूचना प्राप्ति दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  स्वीकृत सूचना दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = str3;
                                strArray[13] = "\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                strArray[14] = questionModelCustom.MinistryNameLocal;
                                strArray[15] = " बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n\t\t <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[16] = questionModelCustom.MainQuestion;
                                strArray[17] = "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n\t\t\t\t\t </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                string[] strArray = new string[18];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n\r\n\t<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t  D. No.: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t subject: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  Notice Recieved on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t Notice of Admission sent on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = str3;
                                strArray[13] = "\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align:top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                strArray[14] = questionModelCustom.MinistryName;
                                strArray[15] = " be pleased to state :\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n\t\t \r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[16] = questionModelCustom.MainQuestion;
                                strArray[17] = "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n\t\t\t\t\t </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                        else
                        {
                            int? ministerId = questionModelCustom.MinisterID;
                            int num = 90;
                            if (ministerId.GetValueOrDefault() == num & ministerId.HasValue)
                            {
                                nullable = questionModelCustom.IsHindi;
                                bool flag = true;
                                if (nullable.GetValueOrDefault() == flag & nullable.HasValue)
                                {
                                    string[] strArray = new string[20];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t  डायरी संख्या: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t विषय: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  सूचना प्राप्ति दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  स्वीकृत सूचना दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t";
                                    strArray[10] = questionModelCustom.DiaryNumber;
                                    strArray[11] = "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[12] = questionModelCustom.MemberNameLocal;
                                    strArray[13] = "(";
                                    strArray[14] = questionModelCustom.ConstituencyName_Local;
                                    strArray[15] = ")\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                    strArray[16] = questionModelCustom.MinistryNameLocal;
                                    strArray[17] = " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[18] = questionModelCustom.MainQuestion;
                                    strArray[19] = "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n\t\t\t\t\t </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                    continue;
                                }
                            }
                            nullable = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            if (nullable.GetValueOrDefault() == flag1 & nullable.HasValue)
                            {
                                string[] strArray = new string[20];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t डायरी संख्या: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t विषय: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  सूचना प्राप्ति दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  स्वीकृत सूचना दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = questionModelCustom.MemberNameLocal;
                                strArray[13] = "(";
                                strArray[14] = questionModelCustom.ConstituencyName_Local;
                                strArray[15] = ")\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                strArray[16] = questionModelCustom.MinistryNameLocal;
                                strArray[17] = " बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[18] = questionModelCustom.MainQuestion;
                                strArray[19] = "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n\t\t\t\t\t </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                string[] strArray = new string[20];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t  D. No.: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t subject: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  Notice Recieved on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t Notice of Admission sent on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = questionModelCustom.MemberName;
                                strArray[13] = "(";
                                strArray[14] = questionModelCustom.ConstituencyName;
                                strArray[15] = ")\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                strArray[16] = questionModelCustom.MinistryName;
                                strArray[17] = " be pleased to state :\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t<td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[18] = questionModelCustom.MainQuestion;
                                strArray[19] = "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n\t\t\t\t\t </body> </html>";
                                string htmlStringToConvert = str1;
                                string htmlStringBaseURL = "";
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                    }
                    string str4 = str1 + "\r\n\r\n\r\n\r\n\t\t\t\t   \r\n\t\t\t\t\r\n\t\t\t\t\t </body> </html>";
                }
                byte[] buffer = document.Save();
                try
                {
                    memoryStream.Write(buffer, 0, buffer.Length);
                    memoryStream.Position = 0L;
                }
                finally
                {
                    document.Close();
                }
                foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                {
                    if (questionModelCustom.MainQuestion != null)
                    {
                        string[] strArray1 = new string[5]
                        {
              "Question/BeforeFixation/",
              null,
              null,
              null,
              null
                        };
                        int num = tMemberNotice.AssemblyID;
                        strArray1[1] = num.ToString();
                        strArray1[2] = "/";
                        num = tMemberNotice.SessionID;
                        strArray1[3] = num.ToString();
                        strArray1[4] = "/";
                        string str3 = string.Concat(strArray1);
                        string[] strArray2 = new string[6];
                        num = tMemberNotice.AssemblyID;
                        strArray2[0] = num.ToString();
                        strArray2[1] = "_";
                        num = tMemberNotice.SessionID;
                        strArray2[2] = num.ToString();
                        strArray2[3] = "_U_";
                        num = tMemberNotice.QuestionID;
                        strArray2[4] = num.ToString();
                        strArray2[5] = ".pdf";
                        string path2 = string.Concat(strArray2);
                        this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                        string str4 = tMemberNotice.FilePath + str3;
                        if (!Directory.Exists(str4))
                            Directory.CreateDirectory(str4);
                        FileStream fileStream = new FileStream(Path.Combine(str4, path2), FileMode.Create, FileAccess.Write);
                        fileStream.Write(buffer, 0, buffer.Length);
                        fileStream.Close();
                        str2 = str4 + path2;
                    }
                }
                return str2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetQuestionNoticeDetails(int Id, string PaperType)
        {
            DiaryModel DMdl = new DiaryModel();
            DiaryModel DMdl2 = new DiaryModel();


            if (PaperType == "Question")
            {
                if (Id != 0)
                {
                    DMdl.QuestionId = Id;

                }

                DMdl2 = Helper.ExecuteService("LegislationFixation", "GetQuestionDetailById", DMdl) as DiaryModel;
                DMdl = DMdl2;
                DMdl.PaperEntryType = PaperType;
                return PartialView("_ViewDetailsById", DMdl);

            }
            else if (PaperType == "Notice")
            {
                if (Id != 0)
                {

                    DMdl.NoticeId = Id;
                }

                DMdl2 = Helper.ExecuteService("LegislationFixation", "GetNoticeDetailById", DMdl) as DiaryModel;
                DMdl = DMdl2;
                DMdl.PaperEntryType = PaperType;
                return PartialView("_ViewDetailsById", DMdl);
            }
            return null;
        }

        [HttpPost]
        public JsonResult CheckQuestionNo(string QuestionNumber, int QtypeId, int AssemId)
        {

            DiaryModel obj = new DiaryModel();

            QuestionNumber = Sanitizer.GetSafeHtmlFragment(QuestionNumber);
            obj.QuestionNumber = Convert.ToInt32(QuestionNumber);
            if (QtypeId > 0)
            {
                obj.QuestionTypeId = QtypeId;
            }
            if (AssemId != 0)
            {
                obj.AssemblyID = AssemId;
            }
            string st = Helper.ExecuteService("LegislationFixation", "CheckQuestionNo", obj) as string;
            return Json(st, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllRecord(int AssemId, int SessId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string PaperType)
        {
            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                DiaryModel DMdl = new DiaryModel();
                DiaryModel DMdl2 = new DiaryModel();

                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

                DMdl.loopStart = Convert.ToInt16(loopStart);
                DMdl.loopEnd = Convert.ToInt16(loopEnd);
                DMdl.PageSize = Convert.ToInt16(RowsPerPage);
                DMdl.PageIndex = Convert.ToInt16(PageNumber);

                if (AssemId != 0)
                {
                    DMdl.AssemblyID = AssemId;
                }

                if (SessId != 0)
                {
                    DMdl.SessionID = SessId;
                }

                if (PaperType != "" && PaperType != null)
                {
                    DMdl.PaperEntryType = PaperType;
                }

                if (PaperType == "Starred")
                {
                    DMdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
                }
                else if (PaperType == "Unstarred")
                {
                    DMdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                }

                DMdl2 = Helper.ExecuteService("LegislationFixation", "GetAllDiariedRecord", DMdl) as DiaryModel;
                DMdl.DiaryList = DMdl2.DiaryList;
                DMdl.ResultCount = DMdl2.ResultCount;




                if (PageNumber != null && PageNumber != "")
                {
                    DMdl.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    DMdl.PageNumber = Convert.ToInt32("1");
                }
                if (RowsPerPage != null && RowsPerPage != "")
                {
                    DMdl.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    DMdl.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    DMdl.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    DMdl.selectedPage = Convert.ToInt32("1");
                }
                if (loopStart != null && loopStart != "")
                {
                    DMdl.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    DMdl.loopStart = Convert.ToInt32("1");
                }
                if (loopEnd != null && loopEnd != "")
                {
                    DMdl.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    DMdl.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetAllDiariedRecord", DMdl);

            }
            else
            {
                //return RedirectToAction("Login", "Account", new { @area = "" });
                return RedirectToAction("PaperLaidSummary", "LegislationFixation");
            }
        }

        public ActionResult GetQuesByIdForRejection(int QuesId, string ActionType)
        {
            DiaryModel obj = new DiaryModel();

            if (QuesId != 0)
            {
                obj.QuestionId = QuesId;
            }
            obj = Helper.ExecuteService("LegislationFixation", "GetQuesByIdForRejection", obj) as DiaryModel;

            if (obj.DateForBind != null)
            {
                obj.stringDate = Convert.ToDateTime(obj.DateForBind).ToString("dd/MM/yy");
            }
            if (ActionType == "Reject")
            {
                return PartialView("_RejectQuestion", obj);
            }
            else if (ActionType == "TypeChange")
            {
                return PartialView("_ChangeQType", obj);
            }
            return null;

        }

        public ActionResult GetRejectRule()
        {
            DiaryModel obj = new DiaryModel();

            obj.RejectRules = Helper.ExecuteService("LegislationFixation", "GetRejectRule", obj) as List<mQuestionRules>;
            return PartialView("_RejectRule", obj);
        }

        public JsonResult RejectQuestionById(int QuesId, string RIds, string RejectRmk)
        {
            DiaryModel DMdl = new DiaryModel();

            if (QuesId != 0)
            {
                DMdl.QuestionId = QuesId;
            }

            if (RIds != null)
            {
                DMdl.RIds = RIds;
            }

            DMdl.RejectRemark = RejectRmk;

            DMdl.Message = (string)Helper.ExecuteService("LegislationFixation", "RejectQuestionById", DMdl);
            return Json(DMdl.Message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeQuestionTypeById(int QuesId, string ChangeRmk)
        {
            DiaryModel DMdl = new DiaryModel();

            if (QuesId != 0)
            {
                DMdl.QuestionId = QuesId;
            }
            if (ChangeRmk != null)
            {
                DMdl.ChangeTypeRemarks = ChangeRmk;
            }

            //SiteSettings siteSettingModel = new SiteSettings();
            //siteSettingModel = Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingModel) as SiteSettings;

            DMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            DMdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            DMdl.Message = (string)Helper.ExecuteService("LegislationFixation", "ChangeQuestionTypeById", DMdl);

            //if (TempData["LFMsg"] == "Success")
            //{

            //    TempData["LFMsg"] = "Type of Question Changed Successfully !";
            //}

            return Json(DMdl.Message, JsonRequestBehavior.AllowGet);
        }
        public JsonResult MoveToUnstarred(string QuesId, string ChangeRmk)
        {
            DiaryModel DMdl = new DiaryModel();

            if (QuesId != null && QuesId != "")
            {
                DMdl.QuesIds = QuesId;
            }
            if (ChangeRmk != null)
            {
                DMdl.ChangeTypeRemarks = ChangeRmk;
            }

            DMdl = Helper.ExecuteService("LegislationFixation", "MoveToUnstarredByIds", DMdl) as DiaryModel;
            return Json(DMdl, JsonRequestBehavior.AllowGet);
        }


        public ActionResult MainDepartmentDashboard()

        {
            DiaryModel model = new DiaryModel();
            //Get the Total count of All Type of question.

            //model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            //model.DepartmentId = CurrentSession.DeptID;
            model.UserDesignation = CurrentSession.Designation;
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetLegislationDashboardItemsCounter", model);

            mUsers user = new mUsers();
            user.UserId = new Guid(CurrentSession.UserID);
            user.IsMember = CurrentSession.IsMember;

            user = (mUsers)Helper.ExecuteService("User", "GetIsMemberDetails", user);
            if (user != null)
            {

                model.CurrentUserName = user.Name;

            }

            //by robin for new requirement in dashboard
            string DepartmentId = CurrentSession.DeptID;
            string Officecode = CurrentSession.OfficeId;
            string AadharId = CurrentSession.AadharId;
            string currtBId = CurrentSession.BranchId;
            string Year = "5";
            string papers = "1";
            string[] strngBIDN = new string[2];
            strngBIDN[0] = CurrentSession.BranchId;
            var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
            string[] str = new string[6];
            str[0] = DepartmentId;
            str[1] = Officecode;
            str[2] = AadharId;
            str[3] = value123[0];
            str[4] = Year;
            str[5] = papers;
            List<eFileAttachment> ListModel = new List<eFileAttachment>();
            //for draftlistCOunt
            List<eFileAttachment> ListModelDraft = new List<eFileAttachment>();
            string[] strD = new string[6];
            strD[0] = DepartmentId;
            strD[1] = Officecode;
            strD[2] = AadharId;
            strD[3] = currtBId;
            strD[4] = CurrentSession.Designation;
            strD[5] = "1";
            List<ReadTableData> tableList = new List<ReadTableData>();
            ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
            ListModelDraft = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
            tableList = (List<ReadTableData>)Helper.ExecuteService("eFile", "GetReadDataTableList", str);
            model.InboxCount = ListModel.Count - tableList.Count;
            model.DraftCount = ListModelDraft.Count;
            //  model.DashboardSession = "True";
            SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SetSessionAlive = "1";
            return PartialView("_MainDashboard", model);//
        }


        public JsonResult GetdashBaoardData()
        {
            string chkruningsession = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SetSessionAlive;

            tQuestion objModel = new tQuestion();
            if (chkruningsession == "1")
            {

                string DepartmentId = CurrentSession.DeptID;
                string Officecode = CurrentSession.OfficeId;
                string AadharId = CurrentSession.AadharId;
                string currtBId = CurrentSession.BranchId;
                string Year = "5";
                string papers = "1";
                string[] strngBIDN = new string[2];
                strngBIDN[0] = CurrentSession.BranchId;
                var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
                string[] str = new string[6];
                str[0] = DepartmentId;
                str[1] = Officecode;
                str[2] = AadharId;
                str[3] = value123[0];
                str[4] = Year;
                str[5] = papers;
                List<eFileAttachment> ListModel = new List<eFileAttachment>();
                //for draftlistCOunt
                List<eFileAttachment> ListModelDraft = new List<eFileAttachment>();
                string[] strD = new string[6];
                strD[0] = DepartmentId;
                strD[1] = Officecode;
                strD[2] = AadharId;
                strD[3] = currtBId;
                strD[4] = CurrentSession.Designation;
                strD[5] = "1";
                List<ReadTableData> tableList = new List<ReadTableData>();
                ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
                ListModelDraft = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
                tableList = (List<ReadTableData>)Helper.ExecuteService("eFile", "GetReadDataTableList", str);
                objModel.CountInbox = ListModel.Count - tableList.Count;
                objModel.CountDraft = ListModelDraft.Count;
                objModel.DataType = "Success";
                //   return new JsonResult { Data = "Success" };
                return Json(objModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(objModel, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllDetailByPaperType(string PaperType)
        {
            CurrentSession.SetSessionAlive = null;
            tPaperLaidV Obj = new tPaperLaidV();
            Obj.PaperTypeName = PaperType;
            Obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            Obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            Obj.ListtPaperLaidV = (List<tPaperLaidV>)Helper.ExecuteService("LegislationFixation", "GetAllDetailByPaperType", Obj);
            //if (PaperType == "Bill")
            //{

            //}
            return PartialView("_BillAllList", Obj);
            //return null;
        }
        public ActionResult GetDetailsByPaperTypeId(int Id)
        {
            var PaperLaid = (tPaperLaidV)Helper.ExecuteService("LegislationFixation", "GetPaperLaidDetailsById", new tPaperLaidV { PaperLaidId = Id });
            switch (PaperLaid.BOTStatus)
            {
                case 0:
                    PaperLaid.StatusText = "Sent By: " + PaperLaid.DeparmentName;
                    break;

                case 1:
                    PaperLaid.StatusText = "Approved By Additional Commissioner and Send to Commissioner.";
                    break;

                case 2:
                    PaperLaid.StatusText = "Approved By Commissioner and Send to Mayor.";
                    break;

                case 3:
                    PaperLaid.StatusText = "Approved By Mayor and Send Back to Commissioner";
                    break;

                case 4:
                    PaperLaid.StatusText = "Approved By Commissioner and Send back to Additional Commissioner";
                    break;

                case 5:
                    PaperLaid.StatusText = "Approved By Additional Commissioner and Send to Meeting Assistance";
                    break;

                case 6:
                    PaperLaid.StatusText = "Committe Meeting Discussion Approved By Meeting Assistance and Send to Additional Commissioner";
                    break;

                case 7:
                    PaperLaid.StatusText = "Committe Meeting Discussion Approved By Additional Commissioner and Send to Commissioner";
                    break;

                case 8:
                    PaperLaid.StatusText = "Committe Meeting Discussion Approved By Commissioner and Send to Mayor";
                    break;

                case 9:
                    PaperLaid.StatusText = "Committe Meeting Discussion Approved By Mayor and Send Back to Commissioner";
                    break;

                case 10:
                    PaperLaid.StatusText = "Committe Meeting Discussion Approved By Commissioner and Send back to Additional Commissioner";
                    break;

                case 11:
                    PaperLaid.StatusText = "Committe Meeting Discussion Final Approved By Additional Commissioner and Send to Meeting Assistance for Memorandum to be laid in house";
                    break;

                default:
                    break;

            }



            return PartialView("_PaperLaidDetailPopup", PaperLaid);
        }

        #region Fixed and Unfixed Question Related
        public ActionResult GetStarredUnfixedQuestions()
        {
            DiaryModel model = new DiaryModel();
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            model = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetStarredUnfixedQuestions", model);
            model.RoolId = CurrentSession.RoleID;
            return PartialView("_GetStarredUnfix", model);
        }
        public ActionResult GetStarredfixedQuestions()
        {

            DiaryModel model = new DiaryModel();
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            model = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetStarredfixedQuestions", model);
            model.RoolId = CurrentSession.RoleID;
            return PartialView("_GetStarredfix", model);

        }

        public ActionResult GetUnstarredUnfixedQuestions()
        {
            DiaryModel model = new DiaryModel();

            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            model = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetUnstarredUnfixedQuestions", model);

            return PartialView("_GetUnStarredUnfix", model);
        }
        public ActionResult GetUnstarredfixedQuestions()
        {
            DiaryModel model = new DiaryModel();

            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            model = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetUnstarredfixedQuestions", model);
            model.RoolId = CurrentSession.RoleID;
            return PartialView("_GetUnstarredfix", model);
        }
        public ActionResult SearchBySessionDate(int SDateId, string QuesType)
        {

            DiaryModel Obj = new DiaryModel();
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID == "e5144dad-7a5d-4308-976a-8a5d2a462bd1")
            {
                Obj.UserName = "e5144dad-7a5d-4308-976a-8a5d2a462bd1";

            }
            else if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID == "f601de42-22f5-4b77-92b2-cd506564da25")
            {
                Obj.UserName = "f601de42-22f5-4b77-92b2-cd506564da25";
            }

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            Obj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            Obj.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            Obj.SessionDateId = SDateId;

            if (QuesType == "Starred")
            {
                Obj.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QuesType == "Unstarred")
            {
                Obj.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }

            Obj.QuestionStatus = (int)Questionstatus.QuestionSent;
            Obj.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "SearchBySessionDate", Obj);
            StaticListForSerch = (List<DiaryModel>)Obj.DiaryList;

            if (QuesType == "Starred")
            {
                return PartialView("_GetStarredUnfixList", Obj);
            }
            else if (QuesType == "Unstarred")
            {
                return PartialView("_GetUnStarredUnfixList", Obj);
            }
            return null;
        }
        public ActionResult SearchBySessionDateForFix(int SDateId, string QuesType)
        {
            DiaryModel Obj = new DiaryModel();
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID == "e5144dad-7a5d-4308-976a-8a5d2a462bd1")
            {
                Obj.UserName = "e5144dad-7a5d-4308-976a-8a5d2a462bd1";

            }
            else if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID == "f601de42-22f5-4b77-92b2-cd506564da25")
            {
                Obj.UserName = "f601de42-22f5-4b77-92b2-cd506564da25";
            }
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            Obj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            Obj.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            Obj.SessionDateId = SDateId;

            if (QuesType == "Starred")
            {
                Obj.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QuesType == "Unstarred")
            {
                Obj.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }

            Obj.QuestionStatus = (int)Questionstatus.QuestionSent;
            Obj.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "SearchBySessionDateForFix", Obj);
            Obj = (DiaryModel)Helper.ExecuteService("LegislationFixation", "QuestionFixedSessionDateCommasep", Obj);
            StaticListForSerch = (List<DiaryModel>)Obj.DiaryList;
            foreach (var item in Obj.DiaryList)
            {
                Obj.Flag = item.Flag;
            }

            if (QuesType == "Starred")
            {
                return PartialView("_GetStarredfixList", Obj);
            }
            else if (QuesType == "Unstarred")
            {
                return PartialView("_GetUnStarredfixList", Obj);
            }

            return null;
        }

        public ActionResult SearchByMember(int MemberId, string QuesType)
        {
            DiaryModel Obj = new DiaryModel();
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID == "e5144dad-7a5d-4308-976a-8a5d2a462bd1")
            {
                Obj.UserName = "e5144dad-7a5d-4308-976a-8a5d2a462bd1";

            }
            else if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID == "f601de42-22f5-4b77-92b2-cd506564da25")
            {
                Obj.UserName = "f601de42-22f5-4b77-92b2-cd506564da25";
            }


            List<DiaryModel> BindList = new List<DiaryModel>();

            if (MemberId == 0)
            {
                Obj.DiaryList = StaticListForSerch;
                if (QuesType == "Starred")
                {
                    return PartialView("_GetStarredUnfixList", Obj);
                }
                else if (QuesType == "Unstarred")
                {
                    return PartialView("_GetUnStarredUnfixList", Obj);
                }
            }
            else
            {
                var data = StaticListForSerch.Where(m => m.MemberId == MemberId);

                BindList.AddRange(data);

                Obj.DiaryList = BindList;
                if (QuesType == "Starred")
                {
                    return PartialView("_GetStarredUnfixList", Obj);
                }
                else if (QuesType == "Unstarred")
                {
                    return PartialView("_GetUnStarredUnfixList", Obj);
                }
            }
            return null;
        }
        public ActionResult SearchByMaxMinister(int MaxMinisAsk, string QuesType)
        {
            DiaryModel Obj = new DiaryModel();
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID == "e5144dad-7a5d-4308-976a-8a5d2a462bd1")
            {
                Obj.UserName = "e5144dad-7a5d-4308-976a-8a5d2a462bd1";

            }
            else if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID == "f601de42-22f5-4b77-92b2-cd506564da25")
            {
                Obj.UserName = "f601de42-22f5-4b77-92b2-cd506564da25";
            }

            int? uniqueID = 0;
            List<DiaryModel> BindList = new List<DiaryModel>();

            if (MaxMinisAsk == 0)
            {
                Obj.DiaryList = StaticListForSerch;
                if (QuesType == "Starred")
                {
                    return PartialView("_GetStarredUnfixList", Obj);
                }
                else if (QuesType == "Unstarred")
                {
                    return PartialView("_GetUnStarredUnfixList", Obj);
                }

            }
            else
            {
                int?[] distictMinsId = (from A in StaticListForSerch
                                        select A.MinistryId).Distinct().ToArray();

                for (int i = 0; i < distictMinsId.Length; i++)
                {
                    if (uniqueID != distictMinsId[i])
                    {
                        uniqueID = distictMinsId[i];
                        var data = StaticListForSerch.Where(m => m.MinistryId == uniqueID).Take(MaxMinisAsk);
                        BindList.AddRange(data);
                    }
                }
                Obj.DiaryList = BindList;
                if (QuesType == "Starred")
                {
                    return PartialView("_GetStarredUnfixList", Obj);
                }
                else if (QuesType == "Unstarred")
                {
                    return PartialView("_GetUnStarredUnfixList", Obj);
                }

            }
            return null;
        }
        public ActionResult SearchByMaxMember(int MaxMemAsk, string QuesType)
        {
            DiaryModel Obj = new DiaryModel();
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID == "e5144dad-7a5d-4308-976a-8a5d2a462bd1")
            {
                Obj.UserName = "e5144dad-7a5d-4308-976a-8a5d2a462bd1";

            }
            else if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.RoleID == "f601de42-22f5-4b77-92b2-cd506564da25")
            {
                Obj.UserName = "f601de42-22f5-4b77-92b2-cd506564da25";
            }

            int? uniqueID = 0;
            List<DiaryModel> BindList = new List<DiaryModel>();

            if (MaxMemAsk == 0)
            {
                Obj.DiaryList = StaticListForSerch;
                if (QuesType == "Starred")
                {
                    return PartialView("_GetStarredUnfixList", Obj);
                }
                else if (QuesType == "Unstarred")
                {
                    return PartialView("_GetUnStarredUnfixList", Obj);
                }
            }
            else
            {
                int[] distictMemId = (from A in StaticListForSerch
                                      select A.MemberId).Distinct().ToArray();

                for (int i = 0; i < distictMemId.Length; i++)
                {
                    if (uniqueID != distictMemId[i])
                    {
                        uniqueID = distictMemId[i];
                        var data = StaticListForSerch.Where(m => m.MemberId == uniqueID).Take(MaxMemAsk);
                        BindList.AddRange(data);
                    }
                }
                Obj.DiaryList = BindList;
                if (QuesType == "Starred")
                {
                    return PartialView("_GetStarredUnfixList", Obj);
                }
                else if (QuesType == "Unstarred")
                {
                    return PartialView("_GetUnStarredUnfixList", Obj);
                }
            }
            return null;
        }



        public JsonResult GetRotationalMiniterName(int SDateId)
        {
            DiaryModel Obj = new DiaryModel();
            Obj.SessionDateId = SDateId;
            Obj.MinisterName = (string)Helper.ExecuteService("LegislationFixation", "GetRotationalMiniterName", Obj);
            return Json(Obj.MinisterName, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetResetByQuestionId(int QuesId)
        {
            DiaryModel Obj = new DiaryModel();
            Obj.QuestionId = QuesId;
            Obj = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetResetDataByQuestionId", Obj);
            return Json(Obj, JsonRequestBehavior.AllowGet);

        }

        public JsonResult BackToPRByQuestionId(int QuesId)
        {
            DiaryModel Obj = new DiaryModel();
            Obj.QuestionId = QuesId;
            Obj.Message = Helper.ExecuteService("Notice", "BackToPRByQuestionId", Obj) as string;
            return Json(Obj.Message, JsonRequestBehavior.AllowGet);

        }

        public JsonResult FixingQuestion(int SDateId, string Qids, string FixingNums, string ManualFixNo)
        {
            DiaryModel Dmdl = new DiaryModel();

            Dmdl.QuesIds = Qids;
            Dmdl.FixNums = FixingNums;
            Dmdl.SessionDateId = SDateId;
            Dmdl.ManualFixNo = ManualFixNo;
            Dmdl.Message = (string)Helper.ExecuteService("LegislationFixation", "FixingQuestionById", Dmdl);
            return Json(Dmdl.Message, JsonRequestBehavior.AllowGet);

        }
        public ActionResult FullDetailById(int Id)
        {
            tQuestion Obj = new tQuestion();
            Obj = (tQuestion)Helper.ExecuteService("LegislationFixation", "GetQuestionDetailsById", new tQuestion { QuestionID = Id });
            return PartialView("_FullDetails", Obj);

        }

        public JsonResult ApproveFixingQuestion(string Qids)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.QuesIds = Qids;
            Dmdl.Message = (string)Helper.ExecuteService("LegislationFixation", "ApproveFixingQuestion", Dmdl);
            if (Dmdl.Message.Substring(1) == "Fixed Question(s) Finally Approved !")
            {
                // Generate pdf after fixation
                if (Convert.ToInt16(Dmdl.Message.Substring(0, 1)) == (int)QuestionType.StartedQuestion)
                {
                    GeneratePdfByQuesIdStarred_Fix(Qids);
                }
                else if (Convert.ToInt16(Dmdl.Message.Substring(0, 1)) == (int)QuestionType.UnstaredQuestion)
                {
                    GeneratePdfByQuesIdUnstarred_Fix(Qids);
                }


                var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "FinalApprovedSMSEMAIL", null);

                if (NewsSettings.SettingValue == "True")
                {
                    tMemberNotice Valmodel = new tMemberNotice();

                    if (Dmdl.QuesIds != null)
                    {

                        string[] Ids = Dmdl.QuesIds.Split(',');
                        foreach (var cc in Ids)
                        {
                            Valmodel.QuestionID = Convert.ToInt32(cc);
                            Valmodel = (SBL.DomainModel.Models.Notice.tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionIdForFixing", Valmodel);

                            foreach (var Nwn in Valmodel.tQuestionModel)
                            {
                                Valmodel.DiaryNo = Nwn.DiaryNumber;
                                Valmodel.QuestionSequence = Nwn.QuestionNumber;
                                Valmodel.SubmittedDate = Nwn.SubmittedDate;
                                Valmodel.DepartmentId = Nwn.DepartmentId;
                                Valmodel.FixedDate = Nwn.SessionDate;
                            }


                            string moduleName = ConfigurationManager.AppSettings["QuestionsmoduleName"];
                            string moduleActionName = ConfigurationManager.AppSettings["QuestionsmoduleActionName"];

                            SendNotificationsModel model = new SendNotificationsModel();
                            List<RecipientGroup> contactGroups = new List<RecipientGroup>();
                            //Get the Record from the SystemModules on the basis of the moduleName.
                            SystemModuleModel systemMoule = new SystemModuleModel();
                            systemMoule.Name = moduleName;
                            var moduleObj = Helper.ExecuteService("SystemModule", "GetSystemModuleByName", systemMoule) as SystemModuleModel;

                            SystemModuleModel actionObj = new SystemModuleModel();

                            if (moduleObj != null)
                            {
                                //Get the record from the SystemFunctions on the basis of moduleName and actionName.
                                SystemModuleModel moduleAction = new SystemModuleModel();
                                moduleAction.ActionName = moduleActionName;
                                actionObj = Helper.ExecuteService("SystemModule", "GetModuleActionByName", moduleAction) as SystemModuleModel;
                            }

                            if (actionObj != null)
                            {
                                #region Get All The Contacts Associated to the action.
                                if (!string.IsNullOrEmpty(actionObj.AssociatedContactGroups))
                                {
                                    //pass the Comma seperated Associated Contact Groups to a Function and get all the group details at a time.
                                    contactGroups = Helper.ExecuteService("ContactGroups", "GetContactGroupsByGroupIDs", new RecipientGroupMember { AssociatedContactGroups = actionObj.AssociatedContactGroups, DepartmentCode = Valmodel.DepartmentId }) as List<RecipientGroup>;
                                }
                                #endregion
                            }


                            var modelSMS = new { DiaryNumber = Dmdl.DiaryNumber, SubmittedDate = Valmodel.SubmittedDate };

                            // string SMSTemplateText = actionObj.SMSTemplate;
                            // string emailTemplateText = actionObj.EmailTemplate;
                            int groupID = 0;
                            List<string> emailAddresses = new List<string>();
                            List<string> phoneNumbers = new List<string>();

                            try
                            {
                                //SMS Text
                                //SMSTemplateText = SMSTemplateText.Replace("@Model.DiaryNumber", modelSMS.DiaryNumber);
                                //  SMSTemplateText = SMSTemplateText.Replace("@Model.SubmittedDate", modelSMS.SubmittedDate);
                                //model.SMSTemplate = SMSTemplateText;
                                //foreach (var Nwn in Valmodel.tQuestionModel)
                                //      {
                                model.SMSTemplate = "Question Number : " + Valmodel.QuestionSequence + " with Diary Number : " + Valmodel.DiaryNo + "is Finally Approved On " + Convert.ToDateTime(Valmodel.SubmittedDate).ToShortDateString() + " for Session date : " + Convert.ToDateTime(Valmodel.FixedDate).ToShortDateString();
                                model.EmailTemplate = "Question Number : " + Valmodel.QuestionSequence + " with Diary Number : " + Valmodel.DiaryNo + "is Finally Approved On " + Convert.ToDateTime(Valmodel.SubmittedDate).ToShortDateString() + " for Session date : " + Convert.ToDateTime(Valmodel.FixedDate).ToShortDateString();

                                // }



                                //Email Text
                                // emailTemplateText = emailTemplateText.Replace("@Model.DiaryNumber", modelSMS.DiaryNumber);
                                //    emailTemplateText = emailTemplateText.Replace("@Model.IsInitialApprovedDate", modelSMS.IsInitialApprovedDate);
                                // model.EmailTemplate = emailTemplateText;


                                //model.SMSTemplate = Razor.Parse(SMSTemplateText, modelSMS);
                                //model.EmailTemplate = Razor.Parse(emailTemplateText, modelSMS);

                                model.availableRecipientGroups = contactGroups != null ? contactGroups : new List<RecipientGroup>();
                                model.selectedRecipientGroups = contactGroups != null ? contactGroups : new List<RecipientGroup>();
                                model.SendSMS = actionObj.SendSMS;
                                model.SendEmail = actionObj.SendEmail;

                                foreach (var item in model.selectedRecipientGroups)
                                {
                                    groupID = item.GroupID;
                                }
                                //var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", new RecipientGroupMember { GroupID = groupID }) as List<RecipientGroupMember>;
                                var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByNewFixed", new RecipientGroupMember { DepartmentCode = Valmodel.DepartmentId }) as List<RecipientGroupMember>;

                                if (contactGroupMembers != null)
                                {
                                    foreach (var item in contactGroupMembers)
                                    {
                                        if (item.Email.Contains(','))
                                        {
                                            string[] emails = item.Email.Split(',');
                                            foreach (var em in emails)
                                            {
                                                emailAddresses.Add(em);
                                            }
                                        }

                                        else
                                            emailAddresses.Add(item.Email);

                                        if (item.MobileNo.Contains(','))
                                        {
                                            string[] numbers = item.MobileNo.Split(',');
                                            foreach (var mb in numbers)
                                            {
                                                phoneNumbers.Add(mb);
                                            }
                                        }
                                        else
                                            phoneNumbers.Add(item.MobileNo);

                                        // phoneNumbers.Add(item.MobileNo);
                                        //model.EmailAddresses = item.Email;
                                        //model.PhoneNumbers = item.MobileNo;
                                    }
                                }
                            }
                            catch (Exception)
                            {

                                throw;
                            }


                            //Sending Mail



                            if (!string.IsNullOrEmpty(model.EmailAddresses))
                                emailAddresses = model.EmailAddresses.Split(',').Select(s => s).ToList();

                            if (!string.IsNullOrEmpty(model.PhoneNumbers))
                                phoneNumbers = model.PhoneNumbers.Split(',').Select(s => s).ToList();

                            SMSMessageList smsMessage = new SMSMessageList();
                            CustomMailMessage emailMessage = new CustomMailMessage();


                            // SMS Settings                      
                            smsMessage.SMSText = model.SMSTemplate;
                            smsMessage.ModuleActionID = 1;
                            smsMessage.UniqueIdentificationID = 1;
                            //smsMessage.MobileNo = new List<string>();
                            //smsMessage.MobileNo.Add("8894681909");

                            // Email Settings 

                            //emailMessage.ModuleActionID = 1;
                            //emailMessage.UniqueIdentificationID = 1;
                            //EAttachment ea = new EAttachment { FileName = new FileInfo(Server.MapPath(SavedPdfPath)).Name, FileContent = System.IO.File.ReadAllBytes(Server.MapPath(SavedPdfPath)) };
                            ////EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes(SavedPdfPath) };
                            ////emailMessage._attachments = new List<EAttachment>();
                            ////emailMessage._attachments.Add(ea);

                            emailMessage._isBodyHtml = true;
                            emailMessage._subject = "Question with Question Number : " + Valmodel.QuestionSequence + " Finally Approved ";
                            emailMessage._body = model.EmailTemplate;




                            foreach (var item in phoneNumbers)
                            {
                                smsMessage.MobileNo.Add(item);
                            }

                            foreach (var item in emailAddresses)
                            {
                                emailMessage._toList.Add(item);
                            }

                            Notification.Send(model.SendSMS, model.SendEmail, smsMessage, emailMessage);

                        }
                    }
                }


            }


            return Json(Dmdl.Message.Substring(1), JsonRequestBehavior.AllowGet);

        }

        public void GeneratePdfByQuesIdStarred_Fix(string QIds)
        {
            try
            {
                string str1 = "";
                string empty = string.Empty;
                tMemberNotice tMemberNotice = new tMemberNotice();
                string str2 = QIds;
                char[] chArray = new char[1] { ',' };
                foreach (string str3 in str2.Split(chArray))
                {
                    int int16 = (int)Convert.ToInt16(str3);
                    tMemberNotice.QuestionID = int16;
                    tMemberNotice.Updatedate = new DateTime?(DateTime.Now);
                    tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)tMemberNotice);
                    MemoryStream memoryStream = new MemoryStream();
                    EvoPdf.Document document = new EvoPdf.Document();
                    document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document.CompressionLevel = PdfCompressionLevel.Best;
                    document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                    if (tMemberNotice.tQuestionModel != null)
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            bool? isHindi = questionModelCustom.IsHindi;
                            bool flag = true;
                            if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                            {
                                str1 = "<html >                           \r\n\t\t\t\t\t\t\t\t <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                str1 += "<div> \r\n\t\t\t\t\t</div>\r\n\t\t\t\r\n\r\n<table style='width:100%'>\r\n \r\n\t  <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n\t\t\tनगर निगम प्रश्न   \r\n\t\t\t  </td>\r\n\t\t\t  </tr>\r\n\t  </table>";
                            }
                            else
                            {
                                str1 = "<html >                           \r\n\t\t\t\t\t\t\t\t <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                str1 += "<div> \r\n\t\t\t\t\t</div>\r\n\t\t\t\r\n\r\n<table style='width:100%'>\r\n \r\n\t  <tr>\r\n  <td style='text-align:center;font-size:27px;' >\r\n\t\t\tQUESTION FOR MUNCIPAL CORPORATION \r\n\t\t\t  </td>\r\n\t\t\t  </tr>\r\n\t  </table>";
                            }
                        }
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            if (questionModelCustom.IsClubbed.HasValue)
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag1 = true;
                                string str4;
                                if (isHindi.GetValueOrDefault() == flag1 & isHindi.HasValue)
                                {
                                    string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n\t\t  " + strArray[index] + " \r\n\r\n\t\t\t\t\t\t\t   </div>\r\n\t\t\t\t\t\t\t ";
                                    }
                                }
                                else
                                {
                                    string[] strArray = questionModelCustom.CMemName.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n\t\t  " + strArray[index] + " \r\n\r\n\t\t\t\t\t\t\t   </div>\r\n\t\t\t\t\t\t\t ";
                                    }
                                }
                                isHindi = questionModelCustom.IsHindi;
                                bool flag2 = true;
                                if (isHindi.GetValueOrDefault() == flag2 & isHindi.HasValue)
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n\r\n\t<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t  डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t विषय: " + questionModelCustom.Subject + "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n<tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  प्रश्न संख्या: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  निश्चित दिनांक: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t*" + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str4 + "\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n\t\t\t\t\t </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n\r\n\t<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t  D. No.: " + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t subject: " + questionModelCustom.Subject + "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n<tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  Question Number: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  Fixed On: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t*" + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str4 + "\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be Pleased to state :\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n\t\t <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n\t\t\t\t\t </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                            else
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag = true;
                                if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t विषय: " + questionModelCustom.Subject + "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n<tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  प्रश्न संख्या: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  निश्चित दिनांक: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t*" + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberNameLocal + "(" + questionModelCustom.ConstituencyName_Local + ")\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n\t\t <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>     \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n\t\t\t\t\t </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t  D. No.: " + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t subject: " + questionModelCustom.Subject + "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n<tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  Question Number: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  Fixed On: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t*" + questionModelCustom.DiaryNumber + "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberName + "(" + questionModelCustom.ConstituencyName + ")\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be Pleased to state :\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n\t\t <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n\t\t\t\t\t </body> </html>";
                                    string htmlStringToConvert = str1;
                                    string htmlStringBaseURL = "";
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                        }
                        str1 += "\r\n\r\n\r\n\r\n\t\t\t\t   \r\n\t\t\t\t\r\n\t\t\t\t\t </body> </html>";
                    }
                    byte[] buffer = document.Save();
                    try
                    {
                        memoryStream.Write(buffer, 0, buffer.Length);
                        memoryStream.Position = 0L;
                    }
                    finally
                    {
                        document.Close();
                    }
                    string str5 = "Question/BeforeFixation/" + tMemberNotice.AssemblyID.ToString() + "/" + tMemberNotice.SessionID.ToString() + "/";
                    string[] strArray1 = new string[6];
                    int num = tMemberNotice.AssemblyID;
                    strArray1[0] = num.ToString();
                    strArray1[1] = "_";
                    num = tMemberNotice.SessionID;
                    strArray1[2] = num.ToString();
                    strArray1[3] = "_S_";
                    num = tMemberNotice.QuestionID;
                    strArray1[4] = num.ToString();
                    strArray1[5] = ".pdf";
                    string path2 = string.Concat(strArray1);
                    this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                    string str6 = tMemberNotice.FilePath + str5;
                    if (!Directory.Exists(str6))
                        Directory.CreateDirectory(str6);
                    FileStream fileStream = new FileStream(Path.Combine(str6, path2), FileMode.Create, FileAccess.Write);
                    fileStream.Write(buffer, 0, buffer.Length);
                    fileStream.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GeneratePdfByQuesIdUnstarred_Fix(string QIds)
        {
            try
            {
                string str1 = "";
                string empty = string.Empty;
                tMemberNotice tMemberNotice = new tMemberNotice();
                string str2 = QIds;
                char[] chArray = new char[1] { ',' };
                foreach (string str3 in str2.Split(chArray))
                {
                    int int16 = (int)Convert.ToInt16(str3);
                    tMemberNotice.QuestionID = int16;
                    tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)tMemberNotice);
                    MemoryStream memoryStream = new MemoryStream();
                    EvoPdf.Document document = new EvoPdf.Document();
                    document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document.CompressionLevel = PdfCompressionLevel.Best;
                    document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                    if (tMemberNotice.tQuestionModel != null)
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            bool? isHindi = questionModelCustom.IsHindi;
                            bool flag = true;
                            if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                            {
                                str1 = "<html >                           \r\n\t\t\t\t\t\t\t\t <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                str1 += "<div> \r\n\t\t\t\t\t</div>\r\n\t\t\t\r\n\r\n<table style='width:100%'>\r\n \r\n\t  <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n\t\t\tनगर निगम प्रश्न   \r\n\t\t\t  </td>\r\n\t\t\t  </tr>\r\n\t  </table>";
                            }
                            else
                            {
                                str1 = "<html >                           \r\n\t\t\t\t\t\t\t\t <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                str1 += "<div> \r\n\t\t\t\t\t</div>\r\n\t\t\t\r\n\r\n<table style='width:100%'>\r\n \r\n\t  <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n\t\t\tQUESTION FOR MUNCIPAL CORPORATION\r\n\t\t\t  </td>\r\n\t\t\t  </tr>\r\n\t  </table>";
                            }
                        }
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            if (questionModelCustom.IsClubbed.HasValue)
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag1 = true;
                                string str4;
                                if (isHindi.GetValueOrDefault() == flag1 & isHindi.HasValue)
                                {
                                    string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n\t\t  " + strArray[index] + " \r\n\r\n\t\t\t\t\t\t\t   </div>\r\n\t\t\t\t\t\t\t ";
                                    }
                                }
                                else
                                {
                                    string[] strArray = questionModelCustom.CMemName.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n\t\t  " + strArray[index] + " \r\n\r\n\t\t\t\t\t\t\t   </div>\r\n\t\t\t\t\t\t\t ";
                                    }
                                }
                                isHindi = questionModelCustom.IsHindi;
                                bool flag2 = true;
                                if (isHindi.GetValueOrDefault() == flag2 & isHindi.HasValue)
                                {
                                    string[] strArray = new string[22];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n\r\n\t<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\tडायरी संख्या: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t विषय: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  सूचना प्राप्ति दिनांक: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  स्वीकृत सूचना दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n<tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  प्रश्न संख्या: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  निश्चित दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = str4;
                                    strArray[17] = "\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                    strArray[18] = questionModelCustom.MinistryNameLocal;
                                    strArray[19] = " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n\t\t <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[20] = questionModelCustom.MainQuestion;
                                    strArray[21] = "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n\t\t\t\t\t </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    string[] strArray = new string[22];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n\r\n\t<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t  D. No.: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t subject: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  Notice Recieved on: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t Notice of Admission sent on: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n<tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  Question Number: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  Fixed On: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = str4;
                                    strArray[17] = "\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align:top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                    strArray[18] = questionModelCustom.MinistryName;
                                    strArray[19] = " be Pleased to state :\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n\t\t \r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[20] = questionModelCustom.MainQuestion;
                                    strArray[21] = "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n\t\t\t\t\t </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                            else
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag = true;
                                if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                                {
                                    string[] strArray = new string[24];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t डायरी संख्या: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t विषय: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  सूचना प्राप्ति दिनांक: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  स्वीकृत सूचना दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\r\n<tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  प्रश्न संख्या: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  निश्चित दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = questionModelCustom.MemberNameLocal;
                                    strArray[17] = "(";
                                    strArray[18] = questionModelCustom.ConstituencyName_Local;
                                    strArray[19] = ")\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                    strArray[20] = questionModelCustom.MinistryNameLocal;
                                    strArray[21] = " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n\r\n<table style='width:80%'>\r\n\t <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[22] = questionModelCustom.MainQuestion;
                                    strArray[23] = "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n\t\t\t\t\t </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    string[] strArray = new string[24];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t  D. No.: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n\t\t\t  </td>\r\n\t   \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t\t subject: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n\t\t\t  </td>\r\n\t\t\r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t  <tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  Notice Recieved on: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t Notice of Admission sent on: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n<tr>\r\n\t\t  <td style='text-align:left;font-size:20px;' >\r\n\t\t  प्रश्न संख्या: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n\t\t\t  </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n\t\t  निश्चित दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n\t\t\t  </td>\r\n \r\n\t\t \r\n\t\t\t  </tr>\r\n\r\n\t   \r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n\t\t\t\t";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = questionModelCustom.MemberName;
                                    strArray[17] = "(";
                                    strArray[18] = questionModelCustom.ConstituencyName;
                                    strArray[19] = ")\r\n</td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n\t\t <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n\t\t\t\r\n\t\t\t  </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                    strArray[20] = questionModelCustom.MinistryName;
                                    strArray[21] = " be Pleased to state :\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n   \r\n\t\t   </table>\r\n<table style='width:80%'>\r\n\t <tr>\r\n\t\t<td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[22] = questionModelCustom.MainQuestion;
                                    strArray[23] = "\r\n</td>\r\n\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n\r\n<table style='width:100%'>\r\n\t <tr>\r\n\t\t  <td style='text-align:center;font-size:20px;' >\r\n\t\t   ----            \r\n\t\t\t  </td>\r\n\t\t \r\n\t\t\t  </tr>\r\n\t\t   </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n\t\t\t\t\t </body> </html>";
                                    string htmlStringToConvert = str1;
                                    string htmlStringBaseURL = "";
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                        }
                        str1 += "\r\n\r\n\r\n\r\n\t\t\t\t   \r\n\t\t\t\t\r\n\t\t\t\t\t </body> </html>";
                    }
                    byte[] buffer = document.Save();
                    try
                    {
                        memoryStream.Write(buffer, 0, buffer.Length);
                        memoryStream.Position = 0L;
                    }
                    finally
                    {
                        document.Close();
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        if (questionModelCustom.MainQuestion != null)
                        {
                            string str4 = "Question/BeforeFixation/" + tMemberNotice.AssemblyID.ToString() + "/" + tMemberNotice.SessionID.ToString() + "/";
                            string[] strArray = new string[6];
                            int num = tMemberNotice.AssemblyID;
                            strArray[0] = num.ToString();
                            strArray[1] = "_";
                            num = tMemberNotice.SessionID;
                            strArray[2] = num.ToString();
                            strArray[3] = "_U_";
                            num = tMemberNotice.QuestionID;
                            strArray[4] = num.ToString();
                            strArray[5] = ".pdf";
                            string path2 = string.Concat(strArray);
                            this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                            string str5 = tMemberNotice.FilePath + str4;
                            if (!Directory.Exists(str5))
                                Directory.CreateDirectory(str5);
                            FileStream fileStream = new FileStream(Path.Combine(str5, path2), FileMode.Create, FileAccess.Write);
                            fileStream.Write(buffer, 0, buffer.Length);
                            fileStream.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ShowAllPostPoneQues(string QuesType)
        {
            DiaryModel Dmdl = new DiaryModel();

            Dmdl = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetSessionAndSessionDateforPostpone", Dmdl);
            if (QuesType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QuesType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }

            return PartialView("_ShowAllPostPoneQues", Dmdl);
        }

        public ActionResult PostPoneQues(string QuesType)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            Dmdl = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetSessionDateforPostpone", Dmdl);
            if (QuesType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QuesType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }

            return PartialView("_SessionDateToPostPone", Dmdl);


        }

        public ActionResult GetQuestionForPostpone(int AssemblyId, int SessionId, int SDateId, int QuesType)
        {
            DiaryModel Obj = new DiaryModel();
            Obj.AssemblyID = AssemblyId;
            Obj.SessionID = SessionId;
            Obj.SessionDateId = SDateId;
            Obj.QuestionTypeId = QuesType;

            Obj.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetQuestionForPostpone", Obj);
            return PartialView("_AllQuestionToPostPone", Obj);

        }

        public JsonResult PostponeQuestionbyId(int SessiondtId, string Qids)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.QuesIds = Qids;
            Dmdl.SessionDateId = SessiondtId;
            Dmdl.Message = (string)Helper.ExecuteService("LegislationFixation", "PostponeQuestionbyId", Dmdl);
            return Json(Dmdl.Message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FixPostPoneQues(string QuesType)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            Dmdl = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetSessionAndSessionDateforPostpone", Dmdl);
            if (QuesType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QuesType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }

            return PartialView("_PostPoneSessionAndDate", Dmdl);

        }

        public JsonResult GetSessionDateBySId(int AssemblyId, int SessionId)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.AssemblyID = AssemblyId;
            Dmdl.SessionID = SessionId;
            Dmdl.SessDateList = (List<mSessionDate>)Helper.ExecuteService("LegislationFixation", "GetAllSessionDate", Dmdl);
            return Json(Dmdl.SessDateList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchPostPoneBySessionDateForFix(int AssemblyId, int SessionId, int SDateId, int QuesType)
        {
            DiaryModel Obj = new DiaryModel();
            Obj.AssemblyID = AssemblyId;
            Obj.SessionID = SessionId;
            Obj.SessionDateId = SDateId;
            Obj.QuestionTypeId = QuesType;

            Obj = (DiaryModel)Helper.ExecuteService("LegislationFixation", "SearchPostPoneBySessionDateForFix", Obj);
            if (QuesType == 1)
            {
                return PartialView("_GetStarredPostPoneList", Obj);
            }
            else if (QuesType == 2)
            {
                return PartialView("_GetUnStarredPostPoneList", Obj);
            }

            return null;
        }

        public JsonResult ReFixPostPoneQuestionbyId(int PPSessionid, int SDateId, string Qids)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.SessionID = PPSessionid;
            Dmdl.QuesIds = Qids;
            Dmdl.SessionDateId = SDateId;
            Dmdl.Message = (string)Helper.ExecuteService("LegislationFixation", "ReFixPostPoneQuestionbyId", Dmdl);

            return Json(Dmdl.Message, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetLastQuestionNumber()
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.QuestionNumber = (int)Helper.ExecuteService("LegislationFixation", "GetLastQuestionNumber", Dmdl);
            return Json(Dmdl.QuestionNumber, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetUnstarredRejectedQuestions()
        {
            tQuestion ObjQues = new tQuestion();
            DiaryModel Dmdl = new DiaryModel();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            ObjQues.QuestionListForTrasData = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetUnstarredRejectedList", Dmdl);
            return PartialView("_UnstarredRejectedList", ObjQues);
        }
        public ActionResult GetStarredRejectedQuestions()
        {
            tQuestion ObjQues = new tQuestion();
            DiaryModel Dmdl = new DiaryModel();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            ObjQues.QuestionListForTrasData = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetStarredRejectedList", Dmdl);
            return PartialView("_StarredRejectedList", ObjQues);

        }
        public ActionResult GetStarredToUnstarredQuestions()
        {
            tQuestion ObjQues = new tQuestion();
            DiaryModel Dmdl = new DiaryModel();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }

            ObjQues.QuestionListForTrasData = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetStarredToUnstarredList", Dmdl);
            return PartialView("_StarredToUnstarredList", ObjQues);

        }
        public ActionResult GetStarredQuestionsForTypeChange()
        {
            tQuestion ObjQues = new tQuestion();
            DiaryModel Dmdl = new DiaryModel();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }

            ObjQues.QuestionListForTrasData = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionsForTypeChange", Dmdl);


            return PartialView("_StarredForTypeChangeList", ObjQues);
        }

        public ActionResult GetQuestionsForBracket(string QType)
        {
            tQuestion ObjQues = new tQuestion();
            DiaryModel Dmdl = new DiaryModel();
            if (QType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }

            ObjQues.QuestionListForTrasData = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetQuestionsForBracket", Dmdl);

            ObjQues.QuestionValue = QType;
            return PartialView("_QuestionsForBracketing", ObjQues);
        }
        public JsonResult BackToStarred(int QuesID)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.QuestionId = QuesID;
            Dmdl = Helper.ExecuteService("LegislationFixation", "BackToStarredByQuesId", Dmdl) as DiaryModel;

            return Json(Dmdl, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckBracktingByDiaryNumber(string DiaryNum, string QType, string BIds)
        {
            DiaryModel Dmdl = new DiaryModel();
            if (DiaryNum != null && DiaryNum != "")
            {

                Dmdl.DiaryNumber = DiaryNum.Trim();

                if (QType == "Starred")
                {
                    Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
                }
                else if (QType == "Unstarred")
                {
                    Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                }
                Dmdl.QuesIds = BIds;
                Dmdl = Helper.ExecuteService("LegislationFixation", "CheckBracktingByDiaryNumber", Dmdl) as DiaryModel;
            }

            return Json(Dmdl.Message, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BracketQuestionByDiaryNumber(string DiaryNum, string QType, string BIds)
        {
            DiaryModel Dmdl = new DiaryModel();
            if (DiaryNum != null && DiaryNum != "")
            {
                Dmdl.DiaryNumber = DiaryNum;

                if (QType == "Starred")
                {
                    Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
                }
                else if (QType == "Unstarred")
                {
                    Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                }
                Dmdl.QuesIds = BIds;
                Dmdl = Helper.ExecuteService("LegislationFixation", "BracketQuestionByDiaryNumber", Dmdl) as DiaryModel;
            }

            return Json(Dmdl, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveBracketingByQuestionId(int QId)
        {
            DiaryModel Dmdl = new DiaryModel();
            if (QId != 0)
            {
                Dmdl.QuestionId = QId;
                Dmdl = Helper.ExecuteService("LegislationFixation", "RemoveBracketingByQuestionId", Dmdl) as DiaryModel;
            }

            return Json(Dmdl, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetChangedNotice()
        {
            tMemberNotice ObjNotice = new tMemberNotice();
            DiaryModel Dmdl = new DiaryModel();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            }
            ObjNotice = (tMemberNotice)Helper.ExecuteService("LegislationFixation", "GetChangedNotice", Dmdl);
            ObjNotice.Msg = "Changed";
            return PartialView("_ChangedNoticeTypeList", ObjNotice);
        }
        public ActionResult GetNoticeForTypeChange()
        {
            tMemberNotice ObjNotice = new tMemberNotice();
            DiaryModel Dmdl = new DiaryModel();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            ObjNotice = (tMemberNotice)Helper.ExecuteService("LegislationFixation", "GetNoticeForTypeChange", Dmdl);
            ObjNotice.Msg = "For Change";
            return PartialView("_ChangedNoticeTypeList", ObjNotice);
        }

        public JsonResult ChangeNoticeRuleById(string RIds, string ChangeRuleId)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.RIds = RIds;
            Dmdl.RuleId = Convert.ToInt16(ChangeRuleId);
            Dmdl = Helper.ExecuteService("LegislationFixation", "ChangeNoticeRuleById", Dmdl) as DiaryModel;

            return Json(Dmdl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPdfByNoticeID(int NoticeId)
        {
            List<tQuestionModel> Obj = new List<tQuestionModel>();

            string savedPDF = "";
            if (NoticeId != 0)
            {
                //Obj = (List<tQuestionModel>)Helpers.Helper.ExecuteService("LegislationFixation", "GetAttachmentByNoticeId", NoticeId);
                savedPDF = Helpers.Helper.ExecuteService("LegislationFixation", "GetAttachmentByNoticeId", NoticeId) as string;
                //foreach (var item in Obj)
                //{
                //    savedPDF = item.AnswerAttachLocation;
                //    ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(savedPDF);
                //}
                ViewBag.PDFPath = savedPDF;
            }

            return PartialView("_GetPdfForNotice");
        }

        public ActionResult FixedQuesPDF(string QuesType)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            Dmdl = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetSessionDateforPostpone", Dmdl);
            if (QuesType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QuesType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }

            return PartialView("_SessionDateToPDF", Dmdl);
        }

        public ActionResult GetFixedQuesPDFByDate(int AssemblyId, int SessionId, int SDateId, int QuesType)
        {
            DiaryModel Obj = new DiaryModel();
            Obj.AssemblyID = AssemblyId;
            Obj.SessionID = SessionId;
            Obj.SessionDateId = SDateId;
            Obj.QuestionTypeId = QuesType;
            Obj = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetFixedQuesPDFByDate", Obj);
            return PartialView("_FixedQuesPDFByDate", Obj);
        }

        public JsonResult AdmitRejectedQuesById(int QuesID)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.QuestionId = QuesID;
            Dmdl = Helper.ExecuteService("LegislationFixation", "AdmitRejectedQuesById", Dmdl) as DiaryModel;

            return Json(Dmdl, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
        #endregion

        #region "Old Design template code"
        //public PartialViewResult CompletePaperLaidInformation(string Id, int PaperCategoryTypeId, bool IsStarredQuestion = false, bool? IsByMinistry = null,
        //   int PageNumber = 1, int LoopStart = 1, int LoopEnd = 5, string Status = null, bool? ItemType = null)
        //{
        //    Id = Sanitizer.GetSafeHtmlFragment(Id);
        //    Status = Sanitizer.GetSafeHtmlFragment(Status);
        //    var model = new CompletePaperLaidViewModel()
        //    {
        //        PaperCategoryTypeId = PaperCategoryTypeId,
        //        Id = Id,
        //        IsStarredQuestion = IsStarredQuestion,
        //        Status = Status,
        //        ItemType = ItemType,

        //        PagedList = new PagedList<PaperLaidViewModel>()
        //        {
        //            CurrentPage = PageNumber,
        //            RowsPerPage = pageSize,
        //            LoopEnd = LoopEnd,
        //            LoopStart = LoopStart
        //        }
        //    };

        //    var PaperCategoryType = (mPaperCategoryType)Helper.ExecuteService("LegislationFixation", "GetPapercategoryDetailsById",
        //        new mPaperCategoryType { PaperCategoryTypeId = PaperCategoryTypeId });

        //    model.PaperCategoryType = PaperCategoryType.Name;

        //    var PaperLaidList = new List<tPaperLaidV>();
        //    var QuestionList = new List<tQuestion>();
        //    var NoticeList = new List<tMemberNotice>();

        //    if (Status != "")
        //    {
        //        if (ItemType == null) //For Paper Laid
        //        {
        //            PaperLaidList = (List<tPaperLaidV>)Helper.ExecuteService("LegislationFixation", "GetCompletePaperLaidDetailsByStatus",
        //                new tPaperLaidV { PaperCategoryTypeId = PaperCategoryTypeId, QuestionID = (!IsStarredQuestion) ? 0 : 1, Title = Status });

        //        }
        //        else if (ItemType == true) //For Question
        //        {
        //            QuestionList = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetCompletePaperLaidDetailsByStatus",
        //               new tPaperLaidV { PaperCategoryTypeId = PaperCategoryTypeId, QuestionID = (!IsStarredQuestion) ? 0 : 1, Title = Status });
        //        }
        //        if (ItemType == false) //For Notice
        //        {
        //            NoticeList = (List<tMemberNotice>)Helper.ExecuteService("LegislationFixation", "GetCompletePaperLaidDetailsByStatus",
        //               new tPaperLaidV { PaperCategoryTypeId = PaperCategoryTypeId, QuestionID = (!IsStarredQuestion) ? 0 : 1, Title = Status });
        //        }
        //    }

        //    //Sorting based on Minister name
        //    if (IsByMinistry != null && IsByMinistry == true)
        //    {
        //        PaperLaidList = PaperLaidList.OrderBy(a => a.MinistryName).ToList();
        //        NoticeList = NoticeList.OrderBy(a => a.MinisterName).ToList();
        //        QuestionList = QuestionList.OrderBy(a => a.MinisterID).ToList();
        //        model.IsByMinisterWise = true;
        //    }
        //    else if (IsByMinistry != null && IsByMinistry == false)
        //    {
        //        PaperLaidList = PaperLaidList.OrderBy(a => a.DeparmentName).ToList();
        //        NoticeList = NoticeList.OrderBy(a => a.DepartmentName).ToList();
        //        QuestionList = QuestionList.OrderBy(a => a.DepartmentID).ToList();

        //        model.IsByDepartmentWise = true;
        //    }
        //    else
        //    {
        //        model.IsByPaperLaidWise = true;
        //    }

        //    //Final Viewmodel values

        //    if (ItemType == null) //Paper Laid
        //    {
        //        model.PagedList.TotalRecordCount = PaperLaidList == null ? 0 : PaperLaidList.Count();
        //        //Sorting based on priority
        //        PaperLaidList = (model.IsByDepartmentWise) ? PaperLaidList.OrderBy(a => a.DepartmentwisePriority).ToList() :
        //                        (model.IsByMinisterWise) ? PaperLaidList.OrderBy(a => a.MinistrywisePriority).ToList() :
        //                        (model.IsByPaperLaidWise) ? PaperLaidList.OrderBy(a => a.PaperlaidPriority).ToList() : PaperLaidList.OrderBy(a => a.PaperLaidId).ToList();

        //        model.PagedList.List = PaperLaidList.ToViewModel().Skip((PageNumber - 1) * pageSize).Take(pageSize).ToList();
        //    }
        //    else if (ItemType == true) //Question
        //    {
        //        model.PagedList.TotalRecordCount = QuestionList == null ? 0 : QuestionList.Count();
        //        model.PagedList.List = QuestionList.ToViewModel().Skip((PageNumber - 1) * pageSize).Take(pageSize).ToList();
        //    }
        //    else if (ItemType == false) //Notice
        //    {
        //        model.PagedList.TotalRecordCount = NoticeList == null ? 0 : NoticeList.Count();
        //        model.PagedList.List = NoticeList.ToViewModel().Skip((PageNumber - 1) * pageSize).Take(pageSize).ToList();
        //    }
        //    return PartialView("_CompletePaperLaidDetails", model);
        //}

        #endregion


        #region Updated LOB Papers

        public ActionResult GetSessionList(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyID = siteSettingMod.AssemblyCode;
            mSessionDate sessionDates = new mSessionDate();
            LegislationFixationModel model = new LegislationFixationModel();
            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();


            mSession.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
            mSession.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {

                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
                LegislationFixationModel selectList = new LegislationFixationModel();
                selectList.Id = 0;
                selectList.SessionDate = "Select Sesion Date";
                model.CustomSessionDateList.Add(selectList);

            }
            model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);



            if (PageNumber != null && PageNumber != "")
            {
                model.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                model.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                model.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                model.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                model.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                model.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                model.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                model.loopEnd = Convert.ToInt32("5");
            }

            return PartialView("_DropSessionDateList", model);
        }


        public ActionResult GetLOBPapersBySessionID(string SessionDate, string AssemblyID, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            AdminLOB LOB = new AdminLOB();

            LOB.SessionDate = DateTime.ParseExact(SessionDate, "dd/MM/yyyy", null);
            LOB.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            LOB.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            UpdatedLOBPapersModelcs LobModel = new UpdatedLOBPapersModelcs();
            LobModel.UpdatedLOBPapersList = (ICollection<UpdatedLOBPapersModelcs>)Helper.ExecuteService("LOB", "GetLOBUpdatedPapersByparameters", LOB);

            return PartialView("GetLOBPapersBySessionID", LobModel);
        }
        public bool SubmitSelectedLOBPapers(string selectedId)
        {
            try
            {
                string[] Ids = selectedId.Split(' ');
                foreach (var id in Ids)
                {
                    UpdatedLOBPapersModelcs LobModel = new UpdatedLOBPapersModelcs();
                    LobModel = (UpdatedLOBPapersModelcs)Helper.ExecuteService("LOB", "UpdateLOBpapers", id);
                    if (LobModel != null)
                    {
                        string copyPath = Server.MapPath(LobModel.PreviousFile);
                        string pastePath = Server.MapPath(LobModel.LatestFile);
                        System.IO.File.Copy(copyPath, pastePath, true);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }

        }
        public JsonResult GetLOBTextByLOBPoint(string id)
        {
            if (id != "")
            {
                int adminLOBID = Convert.ToInt32(id);
                AdminLOB LOB = new AdminLOB();
                LOB.Id = adminLOBID;
                LOB = (AdminLOB)Helper.ExecuteService("LOB", "GetLOBTextById", LOB);
                if (LOB != null)
                {
                    return Json(LOB.TextLOB, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "PartialLOBView"


        public ActionResult PartialLOBView()
        {
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyID = siteSettingMod.AssemblyCode;
            mSessionDate sessionDates = new mSessionDate();
            LegislationFixationModel model = new LegislationFixationModel();
            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();


            mSession.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
            mSession.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
            if (model.SessionDateList.Count > 0)
            {

                foreach (var session in model.SessionDateList)
                {
                    LegislationFixationModel fixModel = new LegislationFixationModel();
                    fixModel.Id = session.Id;
                    fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                    model.CustomSessionDateList.Add(fixModel);
                }
                LegislationFixationModel selectList = new LegislationFixationModel();
                selectList.Id = 0;
                selectList.SessionDate = "Select Session Date";
                model.CustomSessionDateList.Add(selectList);

            }
            model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);




            return PartialView("PartialLOBView", model);
        }

        public ActionResult ShowLOBByDate(string SessionDate)
        {
            AdminLOB adminLOB = new AdminLOB();

            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Date = DateTime.ParseExact(SessionDate, "d", provider);

            DateTime sessDate = Convert.ToDateTime(Date);
            adminLOB.SessionDate = sessDate;
            // LOBID = 0;
            LOBModel lob = new LOBModel();
            lob.ListAdminLOB = (List<AdminLOB>)Helper.ExecuteService("LOB", "GetLOBBySessionDate", adminLOB);
            string outXml = "";
            string LOBName = "";
            string SrNo1 = "";
            string SrNo2 = "";
            string SrNo3 = "";
            int SrNo1Count = 1;
            int SrNo2Count = 1;
            int SrNo3Count = 1;
            DateTime SessionDate1 = new DateTime();

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            //     SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
            //   AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);
            string CurrentSecretery = siteSettingMod.CurrentSecretery;
            string SubittedDate = "";
            string SubittedTime = "";
            if (lob.ListAdminLOB != null && lob.ListAdminLOB.Count() > 0)
            {
                outXml = outXml + @"<html>                           
								 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                for (int i = 0; i < lob.ListAdminLOB.Count; i++)
                {
                    if (i == 0)
                    {

                        // LOBID = lob.ListAdminLOB[i].LOBId;
                        // sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        SessionDate1 = Convert.ToDateTime(lob.ListAdminLOB[i].SessionDate);
                        string date = ConvertSQLDate(SessionDate1);
                        date = date.Replace("/", "");
                        date = date.Replace("-", "");
                        LOBName = date + "_" + "LOB" + "_" + Convert.ToString(lob.ListAdminLOB[i].LOBId);


                        outXml += @"<tr>
													<td style='text-align: center; font-size: 30px; font-weight: bold;'>               
														हिमाचल प्रदेश&nbsp; " + Convert.ToString(lob.ListAdminLOB[i].AssemblyNameLocal) + @"    
													</td>
												</tr>
												<tr>
													<td style='text-align: center; font-size: 30px; font-weight: bold;text-decoration: underline;'>
																 " + "कार्यसूची" + @"                             
													</td>
												</tr>

												<tr>
													<td style='text-align: center; font-size: 30px; font-weight: bold'>
															" + Convert.ToString(lob.ListAdminLOB[i].SessionNameLocal) + @"               
													</td>
												</tr>

												<tr>
													<td style='text-align: center; font-size: 28px; font-weight: bold;'>               
														   " + Convert.ToString(lob.ListAdminLOB[i].SessionDateLocal) + @"              
													</td>
												</tr>

												<tr>
													<td style='text-align: center; font-size: 28px; font-weight: bold;text-decoration: underline;'>               
															 " + Convert.ToString(lob.ListAdminLOB[i].SessionTimeLocal) + @"              
													</td>
												</tr>";
                    }

                    if (lob.ListAdminLOB[i].SrNo1 != null && lob.ListAdminLOB[i].SrNo2 == null && lob.ListAdminLOB[i].SrNo3 == null)
                    {

                        SrNo1 = Convert.ToString(SrNo1Count) + ".";
                        outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
									 <div style='padding-left:10px;text-align:justify;'>
								  <b>  " + SrNo1 + @" </b> &nbsp;&nbsp;&nbsp; <div style='padding-left:20px; margin: -34px 0 0 2%;text-align:justify;'>";
                        SrNo1Count = SrNo1Count + 1;
                        SrNo2Count = 1;


                    }
                    else if (lob.ListAdminLOB[i].SrNo1 != null && lob.ListAdminLOB[i].SrNo2 != null && lob.ListAdminLOB[i].SrNo3 == null)
                    {

                        SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                        outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:30px;text-align:justify;'>
									<b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px; margin: -31px 0 0 4.5%;text-align:justify;'>";
                        SrNo2Count = SrNo2Count + 1;
                        SrNo3Count = 1;

                    }
                    else if (lob.ListAdminLOB[i].SrNo1 != null && lob.ListAdminLOB[i].SrNo2 != null && lob.ListAdminLOB[i].SrNo3 != null)
                    {

                        SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                        outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:60px;text-align:justify;'>
									<b>" + SrNo3 + @"</b>&nbsp;&nbsp;&nbsp; <div style='padding-left:15px; margin: -31px 0 0 6%;text-align:justify;'>";
                        SrNo3Count = SrNo3Count + 1;

                    }

                    string LOBText = Convert.ToString(lob.ListAdminLOB[i].TextLOB);
                    if (LOBText != "")
                    {
                        LOBText = LOBText.Replace("<p>", "");
                        LOBText = LOBText.Replace("</p>", "");
                    }


                    outXml += @"" + LOBText + "<br/>";

                    if (lob.ListAdminLOB[i].PDFLocation != null && lob.ListAdminLOB[i].PDFLocation != "")
                    {
                        string pdfpath = lob.ListAdminLOB[i].PDFLocation;
                        pdfpath = pdfpath.Substring(1);
                        pdfpath = pdfpath.Replace(" ", "%20");
                        outXml += @" </td> </tr> <tr><td style='text-align: right;font-size:16px;font-weight:bold'>Paper :  <a href=" + pdfpath + " target='_blank' > <img style='width: 25px; height: 25px' title='PDF' src='/Images/Common/PDF.png'  />  </a>";
                        outXml += @"</td></tr>";
                    }
                    else
                    {
                        outXml += @"<br/> </td></tr>";
                    }

                    SubittedDate = Convert.ToDateTime(lob.ListAdminLOB[i].ApprovedDate).ToString("D", new CultureInfo("hi"));
                    if (Convert.ToString(lob.ListAdminLOB[i].ApprovedTime) != "")
                    {
                        TimeSpan interval = TimeSpan.Parse(Convert.ToString(lob.ListAdminLOB[i].ApprovedTime));
                        DateTime time = DateTime.Today.Add(interval);
                        string displayTime = time.ToString("hh:mm tt");

                        SubittedTime = displayTime;
                    }
                    else
                    {
                        SubittedTime = "";
                    }

                    if (i == lob.ListAdminLOB.Count - 1)
                    {
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

												<tr><td>
												<table style='width:100%;'>
												<tr>
												<td style='width:70%;float:left;font-size: 24px;'>
												   <b>शिमला-171004</b>
												</td>
												<td style='width:30%;float:left;font-size: 24px;'>
												  <b> " + CurrentSecretery + @",  </b>
												</td>
												</tr>
												<br/>
												<tr>
												  <td style='width:70%;float:left;font-size: 24px;'>
												 <b> दिनांक: " + SubittedDate + @"</b>
											   <b> &nbsp;&nbsp;" + SubittedTime + @"</b>
												</td>
												<td style='width:30%;float:right;text-align:right;padding-right:85px;font-size: 24px;'>
												  <b>  सचिव।</b>
												</td>
												</tr>
												</table>           
													</td>
													</tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }

                }

            }
            lob.sessDate = sessDate;

            lob.outXml = outXml;

            return PartialView("PartialLOB", lob);
        }

        #endregion

        #region "Common"

        /// <summary>
        /// Convert SQL date to dd/MM/yyyy format
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string ConvertSQLDate(DateTime? dt)
        {
            DateTime ConvtDate = Convert.ToDateTime(dt);
            string month = ConvtDate.Month.ToString();
            if (month.Length < 2)
            {
                month = "0" + month;
            }
            string day = ConvtDate.Day.ToString();
            string year = ConvtDate.Year.ToString();
            string Date = day + "/" + month + "/" + year;
            return Date;
        }





        /// <summary>
        /// convert numbers in to roman format
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "m" + ToRoman(number - 1000);
            if (number >= 900) return "cm" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "d" + ToRoman(number - 500);
            if (number >= 400) return "cd" + ToRoman(number - 400);
            if (number >= 100) return "c" + ToRoman(number - 100);
            if (number >= 90) return "xc" + ToRoman(number - 90);
            if (number >= 50) return "l" + ToRoman(number - 50);
            if (number >= 40) return "xl" + ToRoman(number - 40);
            if (number >= 10) return "x" + ToRoman(number - 10);
            if (number >= 9) return "ix" + ToRoman(number - 9);
            if (number >= 5) return "v" + ToRoman(number - 5);
            if (number >= 4) return "iv" + ToRoman(number - 4);
            if (number >= 1) return "i" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }


        /// <summary>
        /// Test page action Method
        /// </summary>
        /// <returns></returns>
        public ActionResult Test()
        {
            return View();
        }

        #endregion


        // Question For Cancel

        public PartialViewResult ShowQuestionToCancel()
        {
            CurrentSession.SetSessionAlive = null;
            ViewBag.IsList = "N";
            return PartialView("_searchForQuestionToCancel");
        }
        public PartialViewResult ShowQuestionToCancelList()
        {
            CurrentSession.SetSessionAlive = null;
            ViewBag.IsList = "Y";
            return PartialView("_searchForQuestionToCancel");
        }

        public ActionResult QuestionCancel(int IsStarred)
        {
            CurrentSession.SetSessionAlive = null;

            TransferQuestionModel model = new TransferQuestionModel();
            //CompletePaperLaidViewModel modelV = new CompletePaperLaidViewModel();

            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                //tQuestion model = new tQuestion();

                //List<tQuestion> ListQues = new List<tQuestion>();
                if (IsStarred == 1)
                {
                    ViewBag.Heading = "List of Starred Questions For Transferring To  Current Session";
                }
                else
                {
                    ViewBag.Heading = "List of Starred Questions For Transferring To  Current Session";
                }
                model.QuestionStatus = IsStarred;
                model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                model.SessionID = Convert.ToInt32(CurrentSession.SessionId);
                model = (TransferQuestionModel)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionListForCancel", model);
                //ListQues = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetStarredQuestionListForCancel", model);

                //modelV.ListofData = ListQues.ToViewModel1().ToList();

                return PartialView("_QuestionCancel", model);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");

            }
        }

        public ActionResult EditQuestionNoticeForCancel(int Id, string EditType)
        {
            DiaryModel DMdl = new DiaryModel();
            DiaryModel DMdl2 = new DiaryModel();

            if (EditType == "Question")
            {
                if (Id != 0)
                {

                    DMdl.QuestionId = Id;
                }

                DMdl2 = Helper.ExecuteService("LegislationFixation", "GetQuestionByIdForEdit", DMdl) as DiaryModel;
                DMdl = DMdl2;

                if (DMdl.DateForBind != null)
                {
                    DMdl.stringDate = Convert.ToDateTime(DMdl.DateForBind).ToString("dd/MM/yyyy");
                }
                SBL.DomainModel.Models.Passes.PublicPass mPass = new DomainModel.Models.Passes.PublicPass();
                mPass.SessionCode = DMdl.SessionID;
                mPass.AssemblyCode = DMdl.AssemblyID;

                ViewBag.SessionDateList = GetSessionDates(mPass);

                ViewBag.UpdateStatus = "CancelQues";
                DMdl.ViewType = "QDraft";
                return PartialView("_EditQuestionBeforeCancel", DMdl);

            }




            return null;
        }


        public static SelectList GetSessionDates(DomainModel.Models.Passes.PublicPass modelPass)
        {

            var result1 = Helper.ExecuteService("PublicPass", "GetSessionDatesList", modelPass) as List<mSessionDate>;

            var result = new List<SelectListItem>();
            foreach (var item in result1)
            {
                result.Add(new SelectListItem() { Text = item.SessionDate.ToString("dd/MM/yyyy"), Value = item.Id.ToString() });
            }


            return new SelectList(result, "Value", "Text");
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveQuestionToCancel(DiaryModel Dmdl, FormCollection Col)
        {
            string FixSessDate = Col["hdnFixSessDate"];
            DateTime DateSess = new DateTime(Convert.ToInt32(FixSessDate.Split('/')[2]), Convert.ToInt32(FixSessDate.Split('/')[1]), Convert.ToInt32(FixSessDate.Split('/')[0]));

            Dmdl.FixSessionDate = DateSess;
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                Dmdl.SubmittedBy = CurrentSession.UserID;
            }

            if (Dmdl.QuestionTypeId == (int)QuestionType.StartedQuestion)
            {
                TempData["CallingMethod"] = "StarredPending";
            }
            else if (Dmdl.QuestionTypeId == (int)QuestionType.UnstaredQuestion)
            {
                TempData["CallingMethod"] = "UnstarredPending";
            }

            TempData["Message"] = Helper.ExecuteService("LegislationFixation", "SaveQuestion", Dmdl) as string;

            if (Convert.ToString(TempData["Message"]) != "")
            {
                if (TempData["Message"].ToString().IndexOf("_") > 0)
                {
                    string ApprovedDate = TempData["Message"].ToString().Substring(0, TempData["Message"].ToString().IndexOf("_"));
                    TempData["Message"] = TempData["Message"].ToString().Substring(TempData["Message"].ToString().IndexOf("_") + 1);
                    if (TempData["Message"].ToString() == "Approved Successfully !")
                    {

                        String SavedPdfPath = "";
                        //Generate Pdf
                        if (Dmdl.QuestionTypeId == (int)QuestionType.StartedQuestion)
                        {
                            SavedPdfPath = GeneratePdfByQuesIdStarred_Unfix(Dmdl.QuestionId);
                        }
                        else if (Dmdl.QuestionTypeId == (int)QuestionType.UnstaredQuestion)
                        {
                            SavedPdfPath = GeneratePdfByQuesIdUnstarred_Unfix(Dmdl.QuestionId);
                        }

                        string moduleName = ConfigurationManager.AppSettings["QuestionsmoduleName"];
                        string moduleActionName = ConfigurationManager.AppSettings["QuestionsmoduleActionName"];

                        SendNotificationsModel model = new SendNotificationsModel();

                        List<RecipientGroup> contactGroups = new List<RecipientGroup>();
                        //Get the Record from the SystemModules on the basis of the moduleName.
                        SystemModuleModel systemMoule = new SystemModuleModel();
                        systemMoule.Name = moduleName;
                        var moduleObj = Helper.ExecuteService("SystemModule", "GetSystemModuleByName", systemMoule) as SystemModuleModel;

                        SystemModuleModel actionObj = new SystemModuleModel();

                        if (moduleObj != null)
                        {
                            //Get the record from the SystemFunctions on the basis of moduleName and actionName.
                            SystemModuleModel moduleAction = new SystemModuleModel();
                            moduleAction.ActionName = moduleActionName;
                            actionObj = Helper.ExecuteService("SystemModule", "GetModuleActionByName", moduleAction) as SystemModuleModel;
                        }

                        if (actionObj != null)
                        {
                            #region Get All The Contacts Associated to the action.
                            if (!string.IsNullOrEmpty(actionObj.AssociatedContactGroups))
                            {
                                //pass the Comma seperated Associated Contact Groups to a Function and get all the group details at a time.
                                contactGroups = Helper.ExecuteService("ContactGroups", "GetContactGroupsByGroupIDs", new RecipientGroupMember { AssociatedContactGroups = actionObj.AssociatedContactGroups, DepartmentCode = Dmdl.DepartmentId }) as List<RecipientGroup>;
                            }
                            #endregion
                        }

                        if (ApprovedDate != "")
                        {
                            ApprovedDate = Convert.ToDateTime(ApprovedDate).ToString("dd/MM/yyyy");
                        }
                        var modelSMS = new { DiaryNumber = Dmdl.DiaryNumber, IsInitialApprovedDate = ApprovedDate };

                        string SMSTemplateText = actionObj.SMSTemplate;
                        string emailTemplateText = actionObj.EmailTemplate;
                        int groupID = 0;
                        List<string> emailAddresses = new List<string>();
                        List<string> phoneNumbers = new List<string>();

                        try
                        {
                            //SMS Text
                            SMSTemplateText = SMSTemplateText.Replace("@Model.DiaryNumber", modelSMS.DiaryNumber);
                            SMSTemplateText = SMSTemplateText.Replace("@Model.IsInitialApprovedDate", modelSMS.IsInitialApprovedDate);
                            model.SMSTemplate = SMSTemplateText;

                            //Email Text
                            emailTemplateText = emailTemplateText.Replace("@Model.DiaryNumber", modelSMS.DiaryNumber);
                            emailTemplateText = emailTemplateText.Replace("@Model.IsInitialApprovedDate", modelSMS.IsInitialApprovedDate);
                            model.EmailTemplate = emailTemplateText;

                            //model.SMSTemplate = Razor.Parse(SMSTemplateText, modelSMS);
                            //model.EmailTemplate = Razor.Parse(emailTemplateText, modelSMS);

                            model.availableRecipientGroups = contactGroups != null ? contactGroups : new List<RecipientGroup>();
                            model.selectedRecipientGroups = contactGroups != null ? contactGroups : new List<RecipientGroup>();
                            model.SendSMS = actionObj.SendSMS;
                            model.SendEmail = actionObj.SendEmail;

                            foreach (var item in model.selectedRecipientGroups)
                            {
                                groupID = item.GroupID;
                            }
                            var contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByGroupID", new RecipientGroupMember { GroupID = groupID }) as List<RecipientGroupMember>;

                            if (contactGroupMembers != null)
                            {
                                foreach (var item in contactGroupMembers)
                                {
                                    emailAddresses.Add(item.Email);
                                    phoneNumbers.Add(item.MobileNo);
                                }
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }


                        //Sending Mail



                        if (!string.IsNullOrEmpty(model.EmailAddresses))
                            emailAddresses = model.EmailAddresses.Split(',').Select(s => s).ToList();

                        if (!string.IsNullOrEmpty(model.PhoneNumbers))
                            phoneNumbers = model.PhoneNumbers.Split(',').Select(s => s).ToList();

                        SMSMessageList smsMessage = new SMSMessageList();
                        CustomMailMessage emailMessage = new CustomMailMessage();


                        // SMS Settings                      
                        smsMessage.SMSText = model.SMSTemplate;
                        smsMessage.ModuleActionID = 1;
                        smsMessage.UniqueIdentificationID = 1;
                        //smsMessage.MobileNo = new List<string>();
                        //smsMessage.MobileNo.Add("8894681909");

                        // Email Settings 

                        //emailMessage.ModuleActionID = 1;
                        //emailMessage.UniqueIdentificationID = 1;
                        //EAttachment ea = new EAttachment { FileName = new FileInfo(Server.MapPath(SavedPdfPath)).Name, FileContent = System.IO.File.ReadAllBytes(Server.MapPath(SavedPdfPath)) };
                        EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes(SavedPdfPath) };
                        emailMessage._attachments = new List<EAttachment>();
                        emailMessage._attachments.Add(ea);

                        emailMessage._isBodyHtml = true;
                        emailMessage._subject = "Question with Diary Number : " + Dmdl.DiaryNumber + " Iniatially Approved ";
                        emailMessage._body = model.EmailTemplate;




                        foreach (var item in phoneNumbers)
                        {
                            smsMessage.MobileNo.Add(item);
                        }

                        foreach (var item in emailAddresses)
                        {
                            emailMessage._toList.Add(item);
                        }

                        Notification.Send(model.SendSMS, model.SendEmail, smsMessage, emailMessage);
                    }

                }
            }

            return Json("Save Successfully", JsonRequestBehavior.AllowGet);
            //return RedirectToAction("QuestionCancel", new { IsStarred = Dmdl.Message });
        }
        [HttpPost]
        public JsonResult CancelQuestionFinally(string QuesNumber, string DiaryNo)
        {


            TransferQuestionModel model = new TransferQuestionModel();

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                model.SessionID = Convert.ToInt32(CurrentSession.SessionId);
            }
            model.QuestionNumber = Convert.ToInt32(QuesNumber);
            model.DiaryNumber = DiaryNo;
            model = (TransferQuestionModel)Helper.ExecuteService("LegislationFixation", "UpdatetransferQuestions", model);
            return Json("Transfer Successfully", JsonRequestBehavior.AllowGet);
            //var methodParameter = new List<KeyValuePair<string, string>>();
            //methodParameter.Add(new KeyValuePair<string, string>("@QuesNo", QuesNumber));
            //methodParameter.Add(new KeyValuePair<string, string>("@DiaryNo", DiaryNo));

            //System.Data.DataSet ds = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_CancelQuestion", methodParameter);

            //string res = ds.Tables[0].Rows[0][0].ToString();

            //if (res == "Success")
            //{
            //    return Json("Transfer Successfully", JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json("Transfer Failed", JsonRequestBehavior.AllowGet);
            //}
        }

        public ActionResult GetCancelledQuestions(int IsStarred)
        {

            CompletePaperLaidViewModel modelV = new CompletePaperLaidViewModel();

            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tQuestion model = new tQuestion();

                List<tQuestion> ListQues = new List<tQuestion>();
                if (IsStarred == 1)
                {
                    ViewBag.Heading = "List of Starred Questions For Transferred To  Current Session ";
                }
                else
                {
                    ViewBag.Heading = "List of UnStarred Questions For Transferred To  Current Session ";
                }
                model.QuestionStatus = IsStarred;
                model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                model.SessionID = Convert.ToInt32(CurrentSession.SessionId);

                ListQues = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetCancelledQuestionList", model);
                modelV.ListofData = ListQues.ToViewModel1().ToList();

                return PartialView("_QuestionCancelledList", modelV);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");

            }
        }
        //for Cut Motions
        public ActionResult CutMotions()
        {
            CutMotionModel model = new CutMotionModel();
            model = (CutMotionModel)Helper.ExecuteService("LegislationFixation", "GetSearchEventDemands", model);
            foreach (var test in model.DemandList)
            {
                string val = test.DemandName + "(" + test.DemandNo + ")";
                test.DemandName = val;
            }

            return PartialView("_CutMotionDropDown", model);
        }

        public ActionResult GetCutMotionList(int EventId, int DemandId, int AssemblyId, int SessionId)
        {

            try
            {
                CutMotionModel model = new CutMotionModel();
                model.EventId = EventId;
                model.CMDemandId = DemandId;
                model.AssemblyID = AssemblyId;
                model.SessionID = SessionId;
                model = (CutMotionModel)Helper.ExecuteService("Notice", "GetDataForcutmotion", model);

                //model. = CDateFrom;
                //model.CurrentDateTo = CDateTo;

                return PartialView("_GetListCutData", model);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult FullDetailByNoticeID(int ItemId)
        {
            CutMotionModel model = new CutMotionModel();
            model.NoticeId = ItemId;
            model = (CutMotionModel)Helper.ExecuteService("Notice", "GetDataByIdcutmotion", model);
            return PartialView("_FullDetailsCutMotions", model);

        }


        public JsonResult CheckBracktingByNoticeNumber(string NoticeNum, string BIds)
        {
            DiaryModel Dmdl = new DiaryModel();
            if (NoticeNum != null && NoticeNum != "")
            {

                Dmdl.NoticeNumber = NoticeNum.Trim();
                Dmdl.QuesIds = BIds;
                Dmdl = Helper.ExecuteService("LegislationFixation", "CheckBracktingByNoticeNumber", Dmdl) as DiaryModel;
            }

            return Json(Dmdl.Message, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BracketCutMotionByNoticeNumber(string NoticeNum, string BIds)
        {
            DiaryModel Dmdl = new DiaryModel();
            if (NoticeNum != null && NoticeNum != "")
            {
                Dmdl.NoticeNumber = NoticeNum;
                Dmdl.QuesIds = BIds;
                Dmdl = Helper.ExecuteService("LegislationFixation", "BracketCutMotionByNoticeNumber", Dmdl) as DiaryModel;
            }

            return Json(Dmdl, JsonRequestBehavior.AllowGet);
        }
        #region paper verify
        public ActionResult VerifyPapers()
        {
            return PartialView("VerifyPapers");
        }
        public ActionResult fillReferenceNumber()
        {
            List<fillListGenric> list = (List<fillListGenric>)Helper.ExecuteService("Diary", "fillReferenceNumber", new string[] { CurrentSession.AssemblyId, CurrentSession.SessionId });
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getPaperLaidList(string ReferenceNumber)
        {
            List<tAuditTrialViewModel> list = (List<tAuditTrialViewModel>)Helper.ExecuteService("Diary", "getPaperLaidList", ReferenceNumber);
            foreach (var item in list)
            {
                string[] filepathName1 = item.filePath.Split('/');
                item.filePathName = filepathName1[filepathName1.Length - 1];
            }
            return PartialView("_paperLaidList", list);
        }
        public ActionResult changeStatusofAudit(string paperLaidTempID)
        {
            string ReferenceNumber = (string)Helper.ExecuteService("Diary", "changeStatusofAudit", new string[] { paperLaidTempID, CurrentSession.UserID });
            List<tAuditTrialViewModel> list = (List<tAuditTrialViewModel>)Helper.ExecuteService("Diary", "getPaperLaidList", ReferenceNumber);
            foreach (var item in list)
            {
                string[] filepathName1 = item.filePath.Split('/');
                item.filePathName = filepathName1[filepathName1.Length - 1];
            }
            return PartialView("_paperLaidList", list);
        }
        public ActionResult paperverifyReport()
        {
            return View();
        }

        #endregion

        //Change Dept 

        public ActionResult AllQuestionFix(string DeptId, string AssemblyID, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            CurrentSession.SetSessionAlive = null;

            ChangeDeptModule model = new ChangeDeptModule();

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            //model.DepartmentId = DeptId;
            //model.AssemblyID = Convert.ToInt32(AssemblyID);
            //model.SessionID = Convert.ToInt32(SessionId);
            model = (ChangeDeptModule)Helper.ExecuteService("LegislationFixation", "GetFixedQuestion", model);

            return PartialView("_QuestionListforchangeDept", model);
        }

        public ActionResult GetDataQuestionID(int questionID)
        {

            ChangeDeptModule model = new ChangeDeptModule();

            int Id = Convert.ToInt32(questionID);
            model.QuestionID = Id;

            model = (ChangeDeptModule)Helper.ExecuteService("LegislationFixation", "GetAllQDetailById", model);

            foreach (var item in model.tQuestionModel)
            {
                model.QuestionID = item.QuestionID;
                model.DiaryNumber = item.DiaryNumber;
                model.Subject = item.Subject;
                model.MemberId = item.MemberId;
                model.MemberName = item.MemberName;
                model.MainQuestion = item.MainQuestion;
                model.DepartmentName = item.DepartmentName;
                model.DeptId = item.DepartmentId;
                model.SecAadharId = item.SecAadharId;
                model.SecAadharName = item.SecAadharName;
                model.AssemblyID = item.AssemblyID;
                model.SessionID = item.SessionID;
                model.QuestionTypeId = item.QuestionTypeId;
                model.MinisterId = item.MinistryId.Value;
                model.MinisterName = item.MinisterName;
            };
            mAssembly model4 = new mAssembly();
            model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;
            if (model.DepartmentId == null)
            {
                List<tMemberNotice> ListtMemberNotice = new List<tMemberNotice>();
                tMemberNotice noticeModel = new tMemberNotice();
                noticeModel.DepartmentId = "0";
                noticeModel.DepartmentName = "Select Department";
                ListtMemberNotice.Add(noticeModel);
                model.DepartmentLt = ListtMemberNotice;
            }
            if (model.DepartmentId != null)
            {
                tMemberNotice noticeModel1 = new tMemberNotice();
                mMinistry model1 = new mMinistry();
                model1.MinistryID = model.MinisterId;
                noticeModel1.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model1) as List<tMemberNotice>;
                //noticeModel1.DepartmentId = Convert.ToString(0);
                //noticeModel1.DepartmentName = "Select Department";
                model.DepartmentLt = noticeModel1.DepartmentLt;
            }
            //  model.mDepartment = (List<mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", model);

            return PartialView("_UpdateDepartmentView", model);
        }

        public JsonResult UpdateDeptment(Newvalue model)
        {
            ChangeDeptModule Update = new ChangeDeptModule();
            tQuestion objModel = new tQuestion();
            objModel.QuestionID = model.QID;
            objModel.DepartmentID = model.TransferDeptId;
            objModel.OldDepartmentId = model.CurrentDeptId;
            objModel.AssemblyID = model.Assembly;
            objModel.SessionID = model.Session;
            objModel.QuestionType = model.Qtype;
            objModel.DiaryNumber = model.DiaryNo;
            objModel.AnswerAttachLocation = model.SecAdharId;
            objModel.AnswerRemarks = model.SecAdharName;
            objModel.FileName = CurrentSession.AadharId;
            objModel.MinistryId = model.Minister;
            objModel.OldMinistryId = model.OLdMinister;
            Update.Message = Helper.ExecuteService("LegislationFixation", "UpdateDeptChangeQuestions", objModel) as string;

            return Json(Update.Message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AllNoticesSent(string DeptId, string AssemblyID, string SessionId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            CurrentSession.SetSessionAlive = null;
            ChangeDeptModule model = new ChangeDeptModule();
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            //model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            //model.DepartmentId = DeptId;
            //model.AssemblyID = Convert.ToInt32(AssemblyID);
            //model.SessionID = Convert.ToInt32(SessionId);
            model = (ChangeDeptModule)Helper.ExecuteService("LegislationFixation", "GetSentNotices", model);

            return PartialView("_NoticeListforchangeDept", model);
        }

        public ActionResult GetDataNoticeID(int NoticeId)
        {

            ChangeDeptModule model = new ChangeDeptModule();

            int Id = Convert.ToInt32(NoticeId);
            model.NoticeId = Id;

            model = (ChangeDeptModule)Helper.ExecuteService("LegislationFixation", "GetAllNoticeDetailById", model);

            foreach (var item in model.tQuestionModel)
            {
                model.QuestionID = item.QuestionID;
                model.DiaryNumber = item.DiaryNumber;
                model.Subject = item.Subject;
                model.MemberId = item.MemberId;
                model.MemberName = item.MemberName;
                model.MainQuestion = item.MainQuestion;
                model.DepartmentName = item.DepartmentName;
                model.DeptId = item.DepartmentId;
                model.SecAadharId = item.SecAadharId;
                model.SecAadharName = item.SecAadharName;
                model.AssemblyID = item.AssemblyID;
                model.SessionID = item.SessionID;
                model.QuestionTypeId = item.QuestionTypeId;
                model.MinisterId = item.MinistryId.Value;
                model.MinisterName = item.MinisterName;
            };
            mAssembly model4 = new mAssembly();
            model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;
            if (model.DepartmentId == null)
            {
                List<tMemberNotice> ListtMemberNotice = new List<tMemberNotice>();
                tMemberNotice noticeModel = new tMemberNotice();
                noticeModel.DepartmentId = "0";
                noticeModel.DepartmentName = "Select Department";
                ListtMemberNotice.Add(noticeModel);
                model.DepartmentLt = ListtMemberNotice;
            }
            if (model.DepartmentId != null)
            {
                tMemberNotice noticeModel1 = new tMemberNotice();
                mMinistry model1 = new mMinistry();
                model1.MinistryID = model.MinisterId;
                noticeModel1.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model1) as List<tMemberNotice>;
                //noticeModel1.DepartmentId = Convert.ToString(0);
                //noticeModel1.DepartmentName = "Select Department";
                model.DepartmentLt = noticeModel1.DepartmentLt;
            }
            //  model.mDepartment = (List<mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", model);

            return PartialView("_UpdateNoticeDeptView", model);
        }

        public JsonResult UpdateDeptmentNotices(Newvalue model)
        {
            ChangeDeptModule Update = new ChangeDeptModule();
            tMemberNotice objModel = new tMemberNotice();
            objModel.NoticeId = model.QID;
            objModel.DepartmentId = model.TransferDeptId;
            objModel.OldDepartmentId = model.CurrentDeptId;
            objModel.AssemblyID = model.Assembly;
            objModel.SessionID = model.Session;
            objModel.NoticeNumber = model.DiaryNo;
            objModel.FileName = CurrentSession.AadharId;
            objModel.MinistryId = model.Minister;
            objModel.OldMinistryId = model.OLdMinister;
            Update.Message = Helper.ExecuteService("LegislationFixation", "UpdateDeptChangeNotices", objModel) as string;

            return Json(Update.Message, JsonRequestBehavior.AllowGet);
        }

        public string GeneratePdfForNotices324(string Id)
        {
            try
            {
                string outXml = "";
#pragma warning disable CS0219 // The variable 'outInnerXml' is assigned but its value is never used
                string outInnerXml = "";
#pragma warning restore CS0219 // The variable 'outInnerXml' is assigned but its value is never used
                string LOBName = Id;
                string fileName = "";
                string savedPDF = string.Empty;
#pragma warning disable CS0219 // The variable 'HeadingText' is assigned but its value is never used
                string HeadingText = "";
#pragma warning restore CS0219 // The variable 'HeadingText' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'HeadingTextEnglish' is assigned but its value is never used
                string HeadingTextEnglish = "";
#pragma warning restore CS0219 // The variable 'HeadingTextEnglish' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'ViewBy' is assigned but its value is never used
                string ViewBy = "";
#pragma warning restore CS0219 // The variable 'ViewBy' is assigned but its value is never used

                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);



                tMemberNotice model = new tMemberNotice();
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                model.NoticeId = Convert.ToInt32(Id);
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDetailsNoPdf", model);

                //if (model.tQuestionModel != null)
                //{
                //    foreach (var item in model.tQuestionModel)
                //    {
                //        model.SessionDateId = item.SessionDateId;
                //        model.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", model);
                //        model.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", model);
                //    }
                //}
                //    model = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", model);

                if (model.tQuestionModel != null)
                {

                    foreach (var item in model.tQuestionModel)
                    {
                        if (item.IsHindi == true)
                        {
                            outXml = @"<html>                           
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:40px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            outXml += @"<div> 

 <style type='text/css'>
@page {
	@bottom-left {
		content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
		font-family: sans-serif;
		font-size: 80%;
	}
	@bottom-right {
		content: 'Page ' counter(page) ' of ' counter(pages);
		font-family: sans-serif;
		font-size: 80%;
	}
}
</style>
					</div>
<table style='width:100%'>
 
	  <tr>
		  <td style='text-align:right;font-size:20px;' >
			 <b>" + item.NoticeNumber + @"</b>
			  </td>
			  </tr>
	  </table>			
<table style='width:100%'>
 
	  <tr>
		  <td style='text-align:center;font-size:28px;' >
			 <b>हिमाचल प्रदेश बारहवीं विधान सभा</b>
			  </td>
			  </tr>
	  </table>
<table style='width:100%'>
	 <tr>
		  <td style='text-align:center;font-size:20px;' >
			<b>" + "टैक्स्ट" + @"</b> 
			  </td>
			  </tr>
		   </table>
<br />
<table style='width:100%'>
	 <tr>
		  <td style='text-align:center;font-size:20px;'>
			<b>" + "नियम-324" + @"</b>          
			  </td>    
			  </tr>
		   </table>


<table style='width:100%'>
	 <tr>
		  <td style='text-align:center;font-size:20px;' >

			  </td>
			  </tr>
		   </table>";



                            string MQues = WebUtility.HtmlDecode(item.Notice);
                            MQues = MQues.Replace("<p>", "");
                            MQues = MQues.Replace("</p>", "");
                            outXml += @"<table style='width:100%'>

		   </table>

<table style='width:100%;text-align:left;'>
<tr>
<td style='font-size:26px;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + "(" + item.ConstituencyID + "-" + item.ConstituencyName_Local + ")" + @"</b>
</td>
</tr>
</table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='font-size:28px;padding-bottom: 13px;width:200px'>
</td>
<td style='font-size:28px;padding-bottom: 13px;'>
<b>" + item.Notice + @"</b>
</td>
</tr>
</table>
";





                        }
                        else
                        {
                            outXml = @"<html>                           
								 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:40px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            outXml += @"<div> 

 <style type='text/css'>
@page {
	@bottom-left {
		content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
		font-family: sans-serif;
		font-size: 80%;
	}
	@bottom-right {
		content: 'Page ' counter(page) ' of ' counter(pages);
		font-family: sans-serif;
		font-size: 80%;
	}
}
</style>
					</div>
			<table style='width:100%'>
 
	  <tr>
		  <td style='text-align:right;font-size:20px;' >
			 <b>" + item.NoticeNumber + @"</b>
			  </td>
			  </tr>
	  </table>	
<table style='width:100%'>
 
	  <tr>
		  <td style='text-align:center;font-size:28px;' >
			 <b>HIMACHAL PRADESH THIRTEENTH VIDHAN SABHA</b>
			  </td>
			  </tr>
	  </table>
<table style='width:100%'>
	 <tr>
		  <td style='text-align:center;font-size:20px;' >
			<b>" + "Text" + @"</b> 
			  </td>
			  </tr>
		   </table>
<br />
<table style='width:100%'>
	 <tr>
		  <td style='text-align:center;font-size:20px;'>
			<b>" + "Rule-324" + @"</b>          
			  </td>    
			  </tr>
		   </table>


<table style='width:100%'>
	 <tr>
		  <td style='text-align:center;font-size:20px;' >

			  </td>
			  </tr>
		   </table>";



                            string MQues = WebUtility.HtmlDecode(item.Notice);
                            MQues = MQues.Replace("<p>", "");
                            MQues = MQues.Replace("</p>", "");
                            outXml += @"<table style='width:100%'>

		   </table>

<table style='width:100%;text-align:left;'>
<tr>
<td style='font-size:26px;padding-bottom: 13px;'>
<b>" + item.MemberName + "(" + item.ConstituencyID + "-" + item.ConstituencyName + ")" + @"</b>
</td>
</tr>
</table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='font-size:28px;padding-bottom: 13px;width:200px'>
</td>
<td style='font-size:28px;padding-bottom: 13px;'>
<b>" + item.Notice + @"</b>
</td>
</tr>
</table>
";



                        }
                    }
                }





                MemoryStream output = new MemoryStream();

                PdfConverter pdfConverter = new PdfConverter();

                // set the license key
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";


                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 60;


                pdfConverter.PdfFooterOptions.FooterHeight = 60;
                //write the page number
                TextElement footerTextElement = new TextElement(0, 30, "&p;",
                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }

                string url = "Question" + "/Notices/" + model.AssemblyID + "/" + model.SessionID + "/";
                fileName = model.AssemblyID + "_" + model.SessionID + "_Notices_" + model.NoticeId + ".pdf";
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                //string directory = Server.MapPath(url);
                string directory = model.FilePath + url;
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                //var path = Path.Combine(Server.MapPath("~" + url), fileName);
                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);



                // close file stream
                _FileStream.Close();
                model.FilePath = url;
                model.FileName = fileName;

                // DocFilePath is using for Accessing File 
                // CurrentSession.FileAccessPath = model.DocFilePath;
                //  var AccessPath = "/" + url + fileName;
                model.Msg = Helper.ExecuteService("Notice", "UpdateFilePathandName", model) as string;


                return "Success";
            }

            catch (Exception ex)
            {

                //throw ex;
                return ex.Message;
            }
            finally
            {

            }


        }

        #region Fixed Ques PDf in Question Sent

        public ActionResult GetFixedQuesPDFByDateQuesSent(int AssemblyId, int SessionId, int SDateId, int QuesType)
        {
            DiaryModel Obj = new DiaryModel();
            Obj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId); ;
            Obj.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            Obj.SessionDateId = SDateId;
            Obj.QuestionTypeId = QuesType;
            Obj.QuestionStatus = 3;
            Obj = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetFixedQuesPDFByDate", Obj);
            Obj = (DiaryModel)Helper.ExecuteService("LegislationFixation", "QuestionFixedSessionDateCommasep", Obj);
            if (QuesType == 1)
            {
                return PartialView("_FixedQuesPDFByDateinQuesSent", Obj);
            }
            else
            {
                return PartialView("_FixedQuesPDFByDateinUnstarredQuesSent", Obj);
            }
        }
        public ActionResult ShowPdfinView(string FilePath)
        {
            //if (FilePath != null)
            //{
            //string FileAccessPath = "";
            //if (CurrentSession.FileAccessPath == null || CurrentSession.FileAccessPath == "")
            //{
            SiteSettings siteSettingModel = new SiteSettings();
            siteSettingModel = Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingModel) as SiteSettings;
            string FileAccessPath = siteSettingModel.AccessFilePathPdfView;
            //}
            string savedPDF = FileAccessPath + FilePath;



            ViewBag.PDFPath = savedPDF;
            //}
            return PartialView("_GetPdfForFixedQues");
        }

        public ActionResult GetAcknowledgelistStarred()
        {
            tMemberNotice ObjNotice = new tMemberNotice();
            ObjNotice.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            ObjNotice.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);
            ObjNotice = (tMemberNotice)Helper.ExecuteService("LegislationFixation", "GetAcknowledgeStarredQuestion", ObjNotice);
            return PartialView("_AcknowlegeStarred", ObjNotice);
        }

        public ActionResult GetAcknowledgelistUnStarred()
        {
            tMemberNotice ObjNotice = new tMemberNotice();
            ObjNotice.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            ObjNotice.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);
            ObjNotice = (tMemberNotice)Helper.ExecuteService("LegislationFixation", "GetAcknowledgeUnStarredQuestion", ObjNotice);
            return PartialView("_AcknowlegeUnStarred", ObjNotice);
        }

        #endregion
        #region Excess Quetions

        public ActionResult GetQuestionsForExcess(string QType)
        {
            tQuestion ObjQues = new tQuestion();
            DiaryModel Dmdl = new DiaryModel();
            if (QType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }

            ObjQues.QuestionListForTrasData = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetQuestionsForExcess", Dmdl);

            ObjQues.QuestionValue = QType;
            return PartialView("_QuestionsForExcess", ObjQues);
        }



        public JsonResult ExcesQuesting(string QType, string BIds)
        {
            DiaryModel Dmdl = new DiaryModel();

            if (QType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }
            Dmdl.QuesIds = BIds;
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            Dmdl = Helper.ExecuteService("LegislationFixation", "ExcessingQuestions", Dmdl) as DiaryModel;
            return Json(Dmdl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetExcessedQuestions(string QType)
        {
            tQuestion ObjQues = new tQuestion();
            DiaryModel Dmdl = new DiaryModel();
            if (QType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }

            ObjQues.QuestionListForTrasData = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetExcessedQuestions", Dmdl);

            ObjQues.QuestionValue = QType;
            return PartialView("_ExcessedQuestionsList", ObjQues);
        }


        public JsonResult RemoveExcesQuesting(string QType, string BIds)
        {
            DiaryModel Dmdl = new DiaryModel();

            if (QType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            Dmdl.QuesIds = BIds;
            Dmdl = Helper.ExecuteService("LegislationFixation", "RemoveExcessingQuestions", Dmdl) as DiaryModel;
            return Json(Dmdl, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Wthdrawn By Member

        public ActionResult GetQuestionsForWithdrawn(string QType)
        {
            tQuestion ObjQues = new tQuestion();
            DiaryModel Dmdl = new DiaryModel();
            if (QType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }

            ObjQues.QuestionListForTrasData = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetQuestionsForWithdrawn", Dmdl);

            ObjQues.QuestionValue = QType;
            return PartialView("_QuestionsListForWithdrawn", ObjQues);
        }

        public JsonResult Withdrawing(string QType, string BIds)
        {
            DiaryModel Dmdl = new DiaryModel();

            if (QType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }
            Dmdl.QuesIds = BIds;
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            Dmdl = Helper.ExecuteService("LegislationFixation", "WithdranwQuestions", Dmdl) as DiaryModel;
            return Json(Dmdl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetListofWithdrawnQuest(string QType)
        {
            tQuestion ObjQues = new tQuestion();
            DiaryModel Dmdl = new DiaryModel();
            if (QType == "Starred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
            }
            else if (QType == "Unstarred")
            {
                Dmdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            }
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }

            ObjQues.QuestionListForTrasData = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "GetListOfWithdrawnQuest", Dmdl);

            ObjQues.QuestionValue = QType;
            return PartialView("_WithdrawnList", ObjQues);
        }

        #endregion
        public ActionResult NoticesDropdown()
        {
            CutMotionModel model = new CutMotionModel();
            model = (CutMotionModel)Helper.ExecuteService("LegislationFixation", "GetRulesDepartment", model);

            return PartialView("_NoticesDropDeptRule", model);
        }
        public ActionResult GetNoticeBracktList(int EventId, string DeptId, int AssemblyId, int SessionId)
        {

            try
            {
                CutMotionModel model = new CutMotionModel();
                model.EventId = EventId;
                model.DepartmentId = DeptId;
                model.AssemblyID = AssemblyId;
                model.SessionID = SessionId;
                model = (CutMotionModel)Helper.ExecuteService("Notice", "GetDataForNoticeBrackt", model);

                //model. = CDateFrom;
                //model.CurrentDateTo = CDateTo;

                return PartialView("_GetListNoticeBract", model);

            }
            catch (Exception ex)
            {
                throw ex;
            }




        }

        public JsonResult BracktingForNoticeNumber(string NoticeNum, string BIds)
        {
            DiaryModel Dmdl = new DiaryModel();
            if (NoticeNum != null && NoticeNum != "")
            {

                Dmdl.NoticeNumber = NoticeNum.Trim();
                Dmdl.QuesIds = BIds;
                Dmdl = Helper.ExecuteService("LegislationFixation", "BracktingForNoticeNumber", Dmdl) as DiaryModel;
            }

            return Json(Dmdl.Message, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BracketNoticeByNotice(string NoticeNum, string BIds)
        {
            DiaryModel Dmdl = new DiaryModel();
            if (NoticeNum != null && NoticeNum != "")
            {
                Dmdl.NoticeNumber = NoticeNum;
                Dmdl.QuesIds = BIds;
                Dmdl = Helper.ExecuteService("LegislationFixation", "BracketNoticeByNoticeNumber", Dmdl) as DiaryModel;
            }

            return Json(Dmdl, JsonRequestBehavior.AllowGet);
        }
        #region Pass Count

        public PartialViewResult ShowPassCount()
        {
            CurrentSession.SetSessionAlive = null;
            UploadMetaViewModel model = new UploadMetaViewModel();
            var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            mSession data = new mSession();
            data.AssemblyID = 12;// assmeblyList.FirstOrDefault().AssemblyCode;
            var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
            mSessionDate sesdate = new mSessionDate();
            sesdate.SessionId = 6;// sessionList.FirstOrDefault().SessionCode;
            sesdate.AssemblyId = data.AssemblyID;
            model.AssemblyId = data.AssemblyID;
            model.SessionId = sesdate.SessionId;
            var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);
            model.SessionDateList = sessionDateList;
            model.AssemblyList = assmeblyList;
            model.SessionList = sessionList;
            return PartialView("/Areas/Notices/Views/LegislationFixation/_PassCountList.cshtml", model);
        }

        #endregion

        #region Question Notices Details

        public PartialViewResult QuestionNoticesDetails()
        {
            CurrentSession.SetSessionAlive = null;
            return PartialView("/Areas/Notices/Views/LegislationFixation/_QuestionNoticeDetails.cshtml");
        }

        #endregion


        public ActionResult GetNoticesForPostpone(int AssemblyId, int SessionId)
        {
            DiaryModel Obj = new DiaryModel();
            Obj.AssemblyID = AssemblyId;
            Obj.SessionID = SessionId;
            Obj.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetNoticesForPostpone", Obj);
            return PartialView("_AllPostponeNotices", Obj);
        }

        public JsonResult PostponeNoticesbyId(string Qids)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.QuesIds = Qids;
            Dmdl.Message = (string)Helper.ExecuteService("LegislationFixation", "PostponeNoticesbyId", Dmdl);
            return Json(Dmdl.Message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FixPostPoneNotice()
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            Dmdl = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetSessionAndSessionDateforPostpone", Dmdl);

            return PartialView("_PostPoneSessionNotice", Dmdl);

        }


        public ActionResult SearchPostPoneBySessionFix(int AssemblyId, int SessionId)
        {
            DiaryModel Obj = new DiaryModel();
            Obj.AssemblyID = AssemblyId;
            Obj.SessionID = SessionId;
            Obj = (DiaryModel)Helper.ExecuteService("LegislationFixation", "SearchPostPoneBySessionFixNotice", Obj);
            return PartialView("_GetNoticesPostPoneList", Obj);
        }

        public JsonResult ReFixPostPoneNoticeId(int PPSessionid, int SDateId, string Qids)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.SessionID = PPSessionid;
            Dmdl.QuesIds = Qids;
            Dmdl.SessionDateId = SDateId;
            Dmdl.Message = (string)Helper.ExecuteService("LegislationFixation", "ReFixPostPoneNoticebyId", Dmdl);

            return Json(Dmdl.Message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllAssemblyFiles(int pageId = 1, int pageSize = 50)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var tassemblyfile = (List<tAssemblyFiles>)Helper.ExecuteService("AssemblyFileSystem", "GetAllAssemblyFiles", null);
                var model = tassemblyfile.ToViewModel1();
                ViewBag.PageSize = pageSize;
                List<AssemblyFileViewModel> pagedRecord = new List<AssemblyFileViewModel>();
                if (pageId == 1)
                {
                    pagedRecord = model.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = model.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(tassemblyfile.Count, pageId, pageSize);
                return PartialView(pagedRecord);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Assembly File Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateAssemblyFile()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                AssemblyFileViewModel model = new AssemblyFileViewModel();

                //Code for AssemblyList 
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                model.VAssemblyId = data.AssemblyID;
                model.VSessionId = sesdate.SessionId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                // Code for DoumentTypeList
                var doctypeList = (List<mAssemblyTypeofDocuments>)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyTypeofDocumentLstForDepartment", null);
                model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;
                model.AssemblyDocumentTypeList = doctypeList;
                model.StatusTypeList = ModelMapping.GetStatusTypes();
                model.Mode = "Add";
                return PartialView("CreateAssemblyFile", model);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                // RecordError(ex, "Creat Assembly File");
                ViewBag.ErrorMessage = "Their is a Error While Create the AssemblyFile Details";
                return View("AdminErrorPage");
            }

        }

        [HttpPost, ValidateInput(false)]
        public string SaveAssemblyFile(HttpPostedFileBase file, int VAssemblyId, int VSessionId, int VSessionDateId, string VSessionDate, int DocumentTypeId, string VTitle, string VDescription, int VStatus)
        {

            try
            {

                bool Status;
                if (VStatus == 1)
                {
                    Status = true;
                }
                else
                {
                    Status = false;
                }
                AssemblyFileViewModel model = new AssemblyFileViewModel();
                string Msg = "";
                model.VAssemblyId = VAssemblyId;
                model.VSessionId = VSessionId;
                model.Mode = "Add";
                model.VSessionDateId = VSessionDateId;
                model.VSessionDate = VSessionDate;
                model.VTypeofDocumentId = DocumentTypeId;
                model.VDescription = VTitle;
                model.VStatus = Status;
                string FileName = file.FileName;

                var data = model.ToDomainModel();

                data.CreatedBy = CurrentSession.UserID;
                data.ModifiedBy = CurrentSession.UserID;
                if (file != null)
                {
                    data.UploadFile = UploadPhoto(file, model);
                }
                else
                {
                    data.UploadFile = null;
                }
                if (model.Mode == "Add")
                {
                    NoticeViewModel model1 = new NoticeViewModel();
                    if (DocumentTypeId == 1)
                    {
                        model1.CategoryID = 33;
                    }
                    if (DocumentTypeId == 2)
                    {
                        model1.CategoryID = 33;
                    }

                    if (DocumentTypeId == 3)
                    {
                        model1.CategoryID = 373;
                    }
                    model1.NoticeTitle = VTitle;
                    model1.NoticeDescription = VDescription;
                    var notice = model1.ToDomainModel();
                    if (file != null)
                    {

                        Guid FileName1 = Guid.NewGuid();
                        var FileName2 = FileName1 + ".pdf";
                        notice.Attachments = NoticeUploadPhoto(file, FileName2);

                    }
                    else
                    {
                        notice.Attachments = "";
                    }
                    notice.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    notice.ModifiedBy = Guid.Parse(CurrentSession.UserID);
                    Helper.ExecuteService("ANotice", "AddNotice", notice);

                }
                Msg = Helper.ExecuteService("AssemblyFileSystem", "AddAssemblyFile", data) as string;
                return Msg;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }


        }

        private string UploadPhoto(HttpPostedFileBase File, AssemblyFileViewModel model)
        {
            try
            {

                if (File != null)
                {
                    // var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetToursFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    //var disFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);


                    // DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~/Images/News/"));//C:\DIS_FileStructure\Notices
                    //Code for Session Date Folder creation
                    DirectoryInfo AssemblyDir = new DirectoryInfo(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId);  //"C:\\DIS_FileStructure\\Notices");
                    //DirectoryInfo MDir = new DirectoryInfo(disFileSettings.SettingValue);
                    if (!AssemblyDir.Exists)
                    {
                        AssemblyDir.Create();
                    }

                    //Code for Session Date Folder creation
                    DirectoryInfo sessionDir = new DirectoryInfo(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId);

                    if (!sessionDir.Exists)
                    {
                        sessionDir.Create();
                    }


                    if (model.VTypeofDocumentId != 1 && model.VTypeofDocumentId != 2)
                    {
                        //Code for Session Date Folder creation
                        string[] sessionDateFile = new string[3];
                        var sessioondatestring = "";
                        if (model.VSessionDate.Contains("/"))
                        {
                            sessionDateFile = model.VSessionDate.Split('/');
                        }
                        else if (model.VSessionDate.Contains("-"))
                        {
                            sessionDateFile = model.VSessionDate.Split('-');
                        }
                        else if (model.VSessionDate.Contains(" "))
                        {
                            sessionDateFile = model.VSessionDate.Split(' ');
                        }

                        if (Convert.ToInt16(sessionDateFile[1]) < 10)
                        {
                            sessionDateFile[1] = "0" + sessionDateFile[1];
                        }
                        if (Convert.ToInt16(sessionDateFile[0]) < 10)
                        {
                            sessionDateFile[0] = "0" + sessionDateFile[0];
                        }

                        sessioondatestring = sessionDateFile[2] + sessionDateFile[1] + sessionDateFile[0];
                        DirectoryInfo sessiondateDir = new DirectoryInfo(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + sessioondatestring);

                        if (!sessiondateDir.Exists)
                        {
                            sessiondateDir.Create();
                        }


                        //Code for Session Date Folder File Existense Check

                        var filepath = System.IO.Path.Combine(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + sessioondatestring + "\\" + model.VTypeofDocumentId + ".pdf");

                        if (System.IO.File.Exists(filepath))
                        {
                            // Remove Existence File

                            RemoveExistingFile(filepath);
                            File.SaveAs(filepath);

                            var path = "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + sessioondatestring + "\\" + model.VTypeofDocumentId + ".pdf";
                            return path.Replace('\\', '/');
                        }
                        else
                        {
                            File.SaveAs(filepath);

                            var path = "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + sessioondatestring + "\\" + model.VTypeofDocumentId + ".pdf";
                            return path.Replace('\\', '/');

                        }

                    }
                    else
                    {

                        var filepath = System.IO.Path.Combine(FileSettings.SettingValue + "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + model.VTypeofDocumentId + ".pdf");

                        if (System.IO.File.Exists(filepath))
                        {
                            // Remove Existence File

                            RemoveExistingFile(filepath);
                            File.SaveAs(filepath);
                            var path = "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + model.VTypeofDocumentId + ".pdf";
                            return path.Replace('\\', '/');
                        }
                        else
                        {
                            File.SaveAs(filepath);

                            var path = "\\AssemblyFiles\\" + model.VAssemblyId + "\\" + model.VSessionId + "\\" + model.VTypeofDocumentId + ".pdf";
                            return path.Replace('\\', '/');

                        }

                    }


                }


            }
            catch (Exception ex)
            {


                throw ex;
            }


            return null;


        }



        private void RemoveExistingFile(string OldFile)
        {
            try
            {
                if (System.IO.File.Exists(OldFile))
                {
                    System.IO.File.Delete(OldFile);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private string NoticeUploadPhoto(HttpPostedFileBase File, string FileName)
        {

            try
            {
                if (File != null)
                {
                    var NoticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNoticeFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                    //var disFileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string FilePath = System.IO.Path.Combine(FileSettings.SettingValue + NoticesSettings.SettingValue);
                    DirectoryInfo MDir = new DirectoryInfo(FilePath);
                    if (!MDir.Exists)
                    {
                        MDir.Create();
                    }
                    //DirectoryInfo MDir = new DirectoryInfo(disFileSettings.SettingValue);
                    //if (!MDir.Exists)
                    //{
                    //    MDir.Create();

                    //}

                    //DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~/Images/Notice/"));
                    //if (!Dir.Exists)
                    //{
                    //    Dir.Create();
                    //}

                    //string extension = System.IO.Path.GetExtension(File.FileName);
                    //string path = System.IO.Path.Combine(NoticesSettings.SettingValue + "\\", FileName);//Server.MapPath("~/Images/News/"), FileName);
                    //File.SaveAs(path);
                    //return (File.FileName);

                    //  string extension = System.IO.Path.GetExtension(File.FileName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + NoticesSettings.SettingValue, FileName);//Server.MapPath("~/Images/Notice/")
                    //if (!System.IO.File.Exists(path))
                    // {
                    File.SaveAs(path);
                    //  }
                    return (FileName);
                }
                return null;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        private void NoticeRemoveExistingPhoto(string OldPhoto)
        {
            // string path = System.IO.Path.Combine(Server.MapPath("~/Images/Notice/"), OldPhoto);

            try
            {
                var noticesSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetNoticeFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + noticesSettings.SettingValue + "\\", OldPhoto);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [HttpPost]
        public ActionResult GetSearchList(string Type, int pageId, int pageSize)
        {
            List<tAssemblyFiles> AssemblyFile = new List<tAssemblyFiles>();

            int TypeId = (int)Helper.ExecuteService("AssemblyFileSystem", "SearchGetAssemblyTypeofDocumentLst", Type);
            if (TypeId > 0)
            {

                AssemblyFile = (List<tAssemblyFiles>)Helper.ExecuteService("AssemblyFileSystem", "GetAllAssemblyFilesSearch", TypeId);

            }
            else
            {
                AssemblyFile = (List<tAssemblyFiles>)Helper.ExecuteService("AssemblyFileSystem", "GetAllAssemblyFiles", null);

            }

            ViewBag.PageSize = pageSize;
            List<tAssemblyFiles> pagedRecord = new List<tAssemblyFiles>();
            if (pageId == 1)
            {
                pagedRecord = AssemblyFile.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = AssemblyFile.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;

            ViewData["PagedList"] = Helper.BindPager(AssemblyFile.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel1();
            return PartialView("_Indexx", model);
            //return PartialView("/Areas/Notices/Views/LegislationFixation/_Indexx.cshtml", model);
        }

        [HttpPost]
        public ActionResult GetAssemblyByPaging(int pageId, int pageSize)
        {
            var tassemblyfile = (List<tAssemblyFiles>)Helper.ExecuteService("AssemblyFileSystem", "GetAllAssemblyFiles", null);
            ViewBag.PageSize = pageSize;
            List<tAssemblyFiles> pagedRecord = new List<tAssemblyFiles>();
            if (pageId == 1)
            {
                pagedRecord = tassemblyfile.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = tassemblyfile.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(tassemblyfile.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel1();
            return PartialView("_Index", model);
            // return PartialView("/Areas/Notices/Views/LegislationFixation/_Index.cshtml", model);
        }

        public ActionResult HijackView()
        {
            CurrentSession.SetSessionAlive = null;

            return PartialView("HijackQView");
        }


        public ActionResult GetDataDiaryNo(string DiaryNo)
        {

            tQuestion obj = new tQuestion();
            DiaryNo = Sanitizer.GetSafeHtmlFragment(DiaryNo);
            obj.DiaryNumber = DiaryNo;
            //ServiceParameter serParam = ServiceParameter.Create("Questions", "CheckQuestionNo", obj);
            // string st = Helper.ExecuteService("Notice", "GetDQuestionNo", obj) as string;
            // obj = Helper.ExecuteService("Notice", "GetDQuestionNo", obj) as string;
            obj = (tQuestion)Helper.ExecuteService("Notice", "GetDQuestionNo", obj);
            foreach (var Nwn in obj.tQuestionModel)
            {

                obj.DiaryNumber = Nwn.DiaryNumber;
                obj.QuestionNumber = Nwn.QuestionNumber;
                obj.Subject = Nwn.Subject;
                obj.MemberN = Nwn.MemberN;
                obj.IsFixedDate = Nwn.IsFixedDate;
                obj.QuestionType = Nwn.QuestionTypeId;
            }
            if (obj.tQuestionModel.Count() == 0)
            {
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            if (obj.QuestionNumber != null)
            {
                return PartialView("DiaryData", obj);
            }
            else
            {
                return Json(obj, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GetDataDiaryNoToFix(string DiaryNo)
        {

            tQuestion obj = new tQuestion();
            DiaryNo = Sanitizer.GetSafeHtmlFragment(DiaryNo);
            obj.DiaryNumber = DiaryNo;
            //ServiceParameter serParam = ServiceParameter.Create("Questions", "CheckQuestionNo", obj);
            // string st = Helper.ExecuteService("Notice", "GetDQuestionNo", obj) as string;
            // obj = Helper.ExecuteService("Notice", "GetDQuestionNo", obj) as string;
            obj = (tQuestion)Helper.ExecuteService("Notice", "GetDQuestionNo", obj);
            foreach (var Nwn in obj.tQuestionModel)
            {

                obj.DiaryNumber = Nwn.DiaryNumber;
                obj.QuestionNumber = Nwn.QuestionNumber;
                obj.Subject = Nwn.Subject;
                obj.MemberN = Nwn.MemberN;
                obj.IsFixedDate = Nwn.IsFixedDate;
                obj.QuestionType = Nwn.QuestionTypeId;
            }
            if (obj.tQuestionModel.Count() == 0)
            {
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            else if (obj.QuestionNumber == null)
            {
                return PartialView("DiaryDataTwo", obj);
            }
            else
            {

                return Json(obj, JsonRequestBehavior.AllowGet);

            }
        }
        public JsonResult Changedata(string DiaryNumberFix, string DUnfix)
        {

            tQuestion obj = new tQuestion();
            obj.DiaryNumber = DiaryNumberFix;
            obj.BracketedWithQNo = DUnfix;
            obj = (tQuestion)Helper.ExecuteService("Notice", "UpdatefixtoUnfix", obj);

            return Json(obj, JsonRequestBehavior.AllowGet);

        }

        #region
        public PartialViewResult ShowspeakerPadpdf(int pageId = 1, int pageSize = 25)
        {
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;
            mspeakerPadPdf model = new mspeakerPadPdf();
            model.AssemblyId = CurrentSession.AssemblyId;
            model.SessionId = CurrentSession.SessionId;

            model.SpeakerPadPdfList = (List<mspeakerPadPdf>)Helper.ExecuteService("Session", "Get_SpeakerPadPdfList", model);
            // ViewData["PagedList"] = Helper.BindPager(model.SpeakerPadPdfList.Count, pageId, pageSize);
            return PartialView("_speakerPadlist", model);
        }
        public PartialViewResult createSpeakerPdf()
        {

            mspeakerPadPdf mdl = new mspeakerPadPdf();
            mdl.AssemblyId = CurrentSession.AssemblyId;
            mdl.SessionId = CurrentSession.SessionId;
            var SessionDate = (List<SBL.DomainModel.Models.Speaker.mspeakerPadPdf>)Helper.ExecuteService("Session", "GetSessionDateForPdf", mdl);

            //ViewBag.sessiondates = new SelectList(SessionDate, "SessionDates", "SessionDates", null);
            mdl.SessionDatesList = SessionDate;
            return PartialView("_CreateSpeakerPadPdf", mdl);
        }

        [HttpPost]
        public JsonResult NewPdfSave(mspeakerPadPdf model, HttpPostedFileBase file)
        {
            mspeakerPadPdf mdl = new mspeakerPadPdf();
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string url = "/ePaper";
            string directory = FileSettings.SettingValue + url;

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            int fileID = GetFileRandomNo();

            string url1 = "~/ePaper/TempFile";
            string directory1 = Server.MapPath(url1);
            if (model.IsPdf == "Y")
            {
                mdl.Pdfpath = model.Pdfpath;
            }
            else
            {
                if (Directory.Exists(directory))
                {
                    string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                    if (savedFileName.Length > 0)
                    {
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            Guid FileName = Guid.NewGuid();

                            string name = Path.GetFileName(page);

                            string path = System.IO.Path.Combine(directory, FileName + "_" + name.Replace(" ", ""));


                            if (!string.IsNullOrEmpty(name))
                            {
                                if (!string.IsNullOrEmpty(mdl.Pdfpath))
                                    System.IO.File.Delete(System.IO.Path.Combine(directory, mdl.Pdfpath));
                                System.IO.File.Copy(SourceFile, path, true);
                                mdl.Pdfpath = "/ePaper/" + FileName + "_" + name.Replace(" ", "");
                            }

                        }

                    }
                    else
                    {
                        mdl.Pdfpath = null;
                        return Json("Please select pdf file", JsonRequestBehavior.AllowGet);
                    }

                }

            }

            mdl.PdfDateTime = DateTime.Now;
            mdl.SessionDates = model.SessionDates;
            if (model.Id != 0)
            {
                mdl.Pdfvesion = model.Pdfvesion;
                mdl.Id = model.Id;
            }
            else
            {
                int VersionID = GetPDfVersion(mdl.SessionDates);
                mdl.Pdfvesion = VersionID;
            }

            int res = (int)Helper.ExecuteService("Session", "NewSpeakerPad_Pdf", mdl);
            if (res == 1)
            {                //ViewBag.ForNewDraftCount = 
                return Json("Speaker Pad Pdf save Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            }



        }

        public PartialViewResult GetPdfRecord_ID(int PdfId)
        {

            mspeakerPadPdf mdl = new mspeakerPadPdf();
            mdl.Id = PdfId;
            mdl.AssemblyId = CurrentSession.AssemblyId;
            mdl.SessionId = CurrentSession.SessionId;

            var SessionDate = (List<SBL.DomainModel.Models.Speaker.mspeakerPadPdf>)Helper.ExecuteService("Session", "GetSessionDateForPdf", mdl);
            mdl.SessionDatesList = SessionDate;
            mdl.SpeakerPadPdfList = (List<SBL.DomainModel.Models.Speaker.mspeakerPadPdf>)Helper.ExecuteService("Session", "GetSpeakrPdfData_ID", mdl);
            mdl.SessionDates = mdl.SpeakerPadPdfList.FirstOrDefault().SessionDates;
            mdl.Pdfpath = mdl.SpeakerPadPdfList.FirstOrDefault().Pdfpath;
            mdl.Pdfvesion = mdl.SpeakerPadPdfList.FirstOrDefault().Pdfvesion;
            //foreach (string _record in mdl.SpeakerPadPdfList)
            //{
            //    System.IO.File.Delete(filePath);
            //}
            return PartialView("_CreateSpeakerPadPdf", mdl);
        }

        public int GetPDfVersion(string SessionDates)
        {
            mspeakerPadPdf mdl = new mspeakerPadPdf();
            mdl.SessionDates = SessionDates;
            mdl.Pdfvesion = (int)Helper.ExecuteService("Session", "Chk_PdfVersion", mdl);
            if (mdl.Pdfvesion == 0)
            {
                mdl.Pdfvesion = 1;
            }
            else
            {
                mdl.Pdfvesion = mdl.Pdfvesion + 1;
            }
            return mdl.Pdfvesion;
        }


        static int GetFileRandomNo()
        {
            Random ran = new Random();
            int rno = ran.Next(1, 99999);
            return rno;
        }

        #endregion

        public void StarredExportPDF()
        {
            string outXml = "";
            tQuestion ObjQues = new tQuestion();
            DiaryModel Dmdl = new DiaryModel();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            ObjQues.QuestionListForTrasData = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "RejectedList", Dmdl);

            outXml = @"<html>
                            <head>
                            <style>
                            /* Style Definitions */
                            p.MsoNormal, li.MsoNormal, div.MsoNormal {
                                margin: 0in;
                                margin-bottom: .0001pt;
                                font-size: 12.0pt;
                                font-family: 'Times New Roman','serif';
                            } </style>

                    </head>
                    <body>                   
                    <br/>
                    <div>  
                    <p class='MsoNormal' style='margin-top: 0in; margin-right: .25in; margin-bottom: 0in; margin-bottom: .0001pt'>
                    <b><span style='font-size: 14.0pt'>Rejected Questions Starred</span></b></p>
                    <hr />
                    <br />

                    <table style='font-size: 10.0pt; margin-left: .10in;width: 100%'>
                   
                    <tr>
                     
                       <th style='text-align:left;'>Diary Number</th><th style='text-align:left;'>Subject</th><th style='text-align:left;'>Main Question</th><th style='text-align:right; padding-right:20px;'>Member Name</th><th style='text-align:right; padding-right:20px;'>Minister Name</th><th style='text-align:right; padding-right:20px;'>Reason</th>
                  </tr>";


            foreach (var item1 in ObjQues.QuestionListForTrasData)
            {


                outXml += @"<tr>
                                        <td style='border: 1px dotted #808080;'>" + item1.DiaryNumber + "</td> <td style='border: 1px dotted #808080;'>" + item1.Subject + @"</td><td style='border: 1px dotted #808080;'>" + item1.MainQuestion + @"</td> <td style='text-align:right; padding-right:20px;border: 1px dotted #808080;'>" + item1.MemberID.GetMemberNameByID() + @"</td> <td style='text-align:right; padding-right:20px;border: 1px dotted #808080;'>" + item1.MinistryId.GetMinisterName() + @"</td> <td style='text-align:right; padding-right:20px;border: 1px dotted #808080;'>" + item1.RejectRemarks + @"</td> 
                                      

                                       </tr>";
            }


            outXml += @"</table>
                            </head></body></html>";


            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
            byte[] outPdfBuffer = null;
            string htmlString = outXml;
            string baseUrl = "";
            outPdfBuffer = pdfConverter.ConvertHtml(outXml, baseUrl);
            Response.AddHeader("Content-Type", "application/pdf");
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename=Question.pdf; size={1}", "attachment", outPdfBuffer.Length.ToString()));
            Response.BinaryWrite(outPdfBuffer);
            Response.End();
        }

        public void UnstarredExportPDF()
        {
            string outXml = "";
            tQuestion ObjQues = new tQuestion();
            DiaryModel Dmdl = new DiaryModel();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            ObjQues.QuestionListForTrasData = (List<tQuestion>)Helper.ExecuteService("LegislationFixation", "UnRejectedList", Dmdl);

            outXml = @"<html>
                            <head>
                            <style>
                            /* Style Definitions */
                            p.MsoNormal, li.MsoNormal, div.MsoNormal {
                                margin: 0in;
                                margin-bottom: .0001pt;
                                font-size: 12.0pt;
                                font-family: 'Times New Roman','serif';
                            } </style>

                    </head>
                    <body>                   
                    <br/>
                    <div>  
                    <p class='MsoNormal' style='margin-top: 0in; margin-right: .25in; margin-bottom: 0in; margin-bottom: .0001pt'>
                    <b><span style='font-size: 14.0pt'>Rejected Questions UnStarred</span></b></p>
                    <hr />
                    <br />

                    <table style='font-size: 10.0pt; margin-left: .10in;width: 100%'>
                   
                    <tr>
                     
                       <th style='text-align:left;'>Diary Number</th><th style='text-align:left;'>Subject</th><th style='text-align:left;'>Main Question</th><th style='text-align:right; padding-right:20px;'>Member Name</th><th style='text-align:right; padding-right:20px;'>Minister Name</th><th style='text-align:right; padding-right:20px;'>Reason</th>
                  </tr>";


            foreach (var item1 in ObjQues.QuestionListForTrasData)
            {


                outXml += @"<tr>
                                        <td style='border: 1px dotted #808080;'>" + item1.DiaryNumber + "</td> <td style='border: 1px dotted #808080;'>" + item1.Subject + @"</td><td style='border: 1px dotted #808080;'>" + item1.MainQuestion + @"</td> <td style='text-align:right; padding-right:20px;border: 1px dotted #808080;'>" + item1.MemberID.GetMemberNameByID() + @"</td> <td style='text-align:right; padding-right:20px;border: 1px dotted #808080;'>" + item1.MinistryId.GetMinisterName() + @"</td> <td style='text-align:right; padding-right:20px;border: 1px dotted #808080;'>" + item1.RejectRemarks + @"</td> 
                                      

                                       </tr>";
            }


            outXml += @"</table>
                            </head></body></html>";


            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
            byte[] outPdfBuffer = null;
            string htmlString = outXml;
            string baseUrl = "";
            outPdfBuffer = pdfConverter.ConvertHtml(outXml, baseUrl);
            Response.AddHeader("Content-Type", "application/pdf");
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename=Question.pdf; size={1}", "attachment", outPdfBuffer.Length.ToString()));
            Response.BinaryWrite(outPdfBuffer);
            Response.End();
        }

        public ActionResult GetDataDiary()
        {
            tQuestionModel Nwn = new tQuestionModel();
            List<tQuestion> objList = new List<tQuestion>();

            Nwn.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            Nwn.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            List<KeyValuePair<string, string>> mParameterWithoutParameter = new List<KeyValuePair<string, string>>();
            var lists = (List<tQuestionModel>)Helper.ExecuteService("Notice", "GetDQuestions", Nwn);
            ViewData["Data"] = lists;

            return PartialView("GetDataDiary", Nwn);
        }





        public ActionResult GetNoticesData()
        {
            tMemberNoticeModel Nwn = new tMemberNoticeModel();
            List<tMemberNotice> objList = new List<tMemberNotice>();

            Nwn.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            Nwn.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            List<KeyValuePair<string, string>> mParameterWithoutParameter = new List<KeyValuePair<string, string>>();
            var lists = (List<tMemberNoticeModel>)Helper.ExecuteService("Notice", "GetDNotices", Nwn);
            ViewData["Data"] = lists;
            ViewBag.NoticeTypeText = "All Diaried Notices List";
            //ViewBag.FirstLoad = "1";


            return PartialView("GetNoticesData", Nwn);
        }



        public JsonResult GetAllNoticeType()
        {
            //Configuration configuration = ConfigurationManager.
            //OpenExeConfiguration(System.Reflection.Assembly.GetExecutingAssembly().Location);
            //configuration.AppSettings.Settings["logPath"].Value = DateTime.Now.ToString("yyyy-MM-dd");
            //configuration.Save();
            //ConfigurationManager.RefreshSection("appSettings");

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter = new List<KeyValuePair<string, string>>();
            DataSet dataSetEvent = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectNoticeEvents", methodParameter);
            List<mEvent> EventList = new List<mEvent>();
            mEvent Event1 = new mEvent();
            Event1.EventId = 0;
            Event1.EventName = "Select";
            EventList.Add(Event1);

            for (int i = 0; i < dataSetEvent.Tables[0].Rows.Count; i++)
            {
                mEvent Event2 = new mEvent();
                Event2.EventId = Convert.ToInt16(dataSetEvent.Tables[0].Rows[i]["EventID"]);
                Event2.EventName = Convert.ToString(dataSetEvent.Tables[0].Rows[i]["EventName"]);
                EventList.Add(Event2);
            }

            EventList = EventList.ToList();
            return Json(EventList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SearchNoticesData(int NoticeTypeId, string NoticeTypeText)
        {
            tMemberNoticeModel Nwn = new tMemberNoticeModel();
            List<tMemberNotice> objList = new List<tMemberNotice>();

            Nwn.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            Nwn.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            Nwn.NoticeTypeID = NoticeTypeId;
            List<KeyValuePair<string, string>> mParameterWithoutParameter = new List<KeyValuePair<string, string>>();
            var lists = (List<tMemberNoticeModel>)Helper.ExecuteService("Notice", "GetNoticesByTypeId", Nwn);
            ViewData["Data"] = lists;
            ViewBag.NoticeTypeId = NoticeTypeId;
            ViewBag.NoticeTypeText = "Notices Diaried List- " + NoticeTypeText;
            return PartialView("GetNoticesData", Nwn);
        }


        public ActionResult SearchQuesData(string QuesTypeId, string QuesTypeText)
        {


            tQuestionModel Nwn = new tQuestionModel();
            List<tQuestion> objList = new List<tQuestion>();

            Nwn.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            Nwn.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            Nwn.QuestionType = QuesTypeId;
            List<KeyValuePair<string, string>> mParameterWithoutParameter = new List<KeyValuePair<string, string>>();
            var lists = (List<tQuestionModel>)Helper.ExecuteService("Notice", "GetDQuestionsByTypeId", Nwn);
            ViewData["Data"] = lists;
            ViewBag.QuesTypeId = QuesTypeId;
            ViewBag.QuesTypeText = "Diaried List- " + QuesTypeText;
            return PartialView("GetDataDiary", Nwn);
        }
    }

    public class Newvalue
    {
        public int QID { get; set; }
        public string CurrentDeptId { get; set; }
        public string TransferDeptId { get; set; }
        public string SecAdharId { get; set; }
        public string SecAdharName { get; set; }
        public int Assembly { get; set; }
        public int Session { get; set; }
        public int Qtype { get; set; }
        public string DiaryNo { get; set; }
        public int Minister { get; set; }
        public int OLdMinister { get; set; }


    }

}



