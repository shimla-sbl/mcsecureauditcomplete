﻿using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SBL.DomainModel.Models.LOB;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Models;
using System.Globalization;
using SBL.eLegistrator.HouseController.Web.Areas.Reporters.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class LOBViewController : Controller
    {
        //
        // GET: /Notices/LOBView/

        public ActionResult Index()
        {
            return View();
        }

        #region "PartialLOBView"


        public ActionResult PartialLOBView()
        {
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyID = siteSettingMod.AssemblyCode;
            mSessionDate sessionDates = new mSessionDate();
            LegislationFixationModel model = new LegislationFixationModel();
            //ListOfBusiness.Models.mSession customModel = new ListOfBusiness.Models.mSession();
            //customModel.Se
            mSession mSession = new mSession();
            if (siteSettingMod != null)
            {

                mSession.SessionCode = siteSettingMod.SessionCode;
                mSession.AssemblyID = siteSettingMod.AssemblyCode;
                model.SessionDateList = (ICollection<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", mSession);
                if (model.SessionDateList.Count > 0)
                {

                    foreach (var session in model.SessionDateList)
                    {
                        LegislationFixationModel fixModel = new LegislationFixationModel();
                        fixModel.Id = session.Id;
                        fixModel.SessionDate = String.Format("{0:dd/MM/yyyy}", session.SessionDate); //session.SessionDate.ToShortDateString("DD/MM/YYYY");
                        model.CustomSessionDateList.Add(fixModel);
                    }
                    LegislationFixationModel selectList = new LegislationFixationModel();
                    selectList.Id = 0;
                    selectList.SessionDate = "Select Session Date";
                    model.CustomSessionDateList.Add(selectList);

                } model.SessionId = siteSettingMod.SessionCode;
                model.AssemblyId = siteSettingMod.AssemblyCode;


            }

            return PartialView("PartialLOBView", model);
        }

        public ActionResult ShowLOBByDate(string SessionDate)
        {
            AdminLOB adminLOB = new AdminLOB();

            CultureInfo provider = CultureInfo.InvariantCulture;
            provider = new CultureInfo("fr-FR");
            var Date = DateTime.ParseExact(SessionDate, "d", provider);

            DateTime sessDate = Convert.ToDateTime(Date);
            adminLOB.SessionDate = sessDate;
            // LOBID = 0;
            LOBModel lob = new LOBModel();
            lob.ListAdminLOB = (List<AdminLOB>)Helper.ExecuteService("LOB", "GetLOBBySessionDate", adminLOB);
            string outXml = "";
            string LOBName = "";
            string SrNo1 = "";
            string SrNo2 = "";
            string SrNo3 = "";
            int SrNo1Count = 1;
            int SrNo2Count = 1;
            int SrNo3Count = 1;
            DateTime SessionDate1 = new DateTime();

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            //     SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
            //   AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);
            string CurrentSecretery = siteSettingMod.CurrentSecretery;
            string SubittedDate = "";
            string SubittedTime = "";
            if (lob.ListAdminLOB != null && lob.ListAdminLOB.Count() > 0)
            {
                outXml = outXml + @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                for (int i = 0; i < lob.ListAdminLOB.Count; i++)
                {
                    if (i == 0)
                    {

                        // LOBID = lob.ListAdminLOB[i].LOBId;
                        // sessiondate = Convert.ToString(SummaryDataSet.Tables[0].Rows[i]["SessionDate"]);
                        SessionDate1 = Convert.ToDateTime(lob.ListAdminLOB[i].SessionDate);
                        string date = ConvertSQLDate(SessionDate1);
                        date = date.Replace("/", "");
                        date = date.Replace("-", "");
                        LOBName = date + "_" + "LOB" + "_" + Convert.ToString(lob.ListAdminLOB[i].LOBId);


                        outXml += @"<tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;'>               
                                                        हिमाचल प्रदेश&nbsp; " + Convert.ToString(lob.ListAdminLOB[i].AssemblyNameLocal) + @"    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold;text-decoration: underline;'>
                                                                 " + "कार्यसूची" + @"                             
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 30px; font-weight: bold'>
                                                            " + Convert.ToString(lob.ListAdminLOB[i].SessionNameLocal) + @"               
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;'>               
                                                           " + Convert.ToString(lob.ListAdminLOB[i].SessionDateLocal) + @"              
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style='text-align: center; font-size: 28px; font-weight: bold;text-decoration: underline;'>               
                                                             " + Convert.ToString(lob.ListAdminLOB[i].SessionTimeLocal) + @"              
                                                    </td>
                                                </tr>";
                    }

                    if (lob.ListAdminLOB[i].SrNo1 != null && lob.ListAdminLOB[i].SrNo2 == null && lob.ListAdminLOB[i].SrNo3 == null)
                    {

                        SrNo1 = Convert.ToString(SrNo1Count) + ".";
                        outXml += @"<tr><td style='text-align: left; font-size: 24px;'>
                                     <div style='padding-left:10px;text-align:justify;'>
                                  <b>  " + SrNo1 + @" </b> &nbsp;&nbsp;&nbsp; <div style='padding-left:20px; margin: -34px 0 0 2%;text-align:justify;'>";
                        SrNo1Count = SrNo1Count + 1;
                        SrNo2Count = 1;


                    }
                    else if (lob.ListAdminLOB[i].SrNo1 != null && lob.ListAdminLOB[i].SrNo2 != null && lob.ListAdminLOB[i].SrNo3 == null)
                    {

                        SrNo2 = "&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(SrNo2Count) + ")";
                        outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:30px;text-align:justify;'>
                                    <b>" + SrNo2 + @"</b>&nbsp;&nbsp;&nbsp;<div style='padding-left:15px; margin: -31px 0 0 4.5%;text-align:justify;'>";
                        SrNo2Count = SrNo2Count + 1;
                        SrNo3Count = 1;

                    }
                    else if (lob.ListAdminLOB[i].SrNo1 != null && lob.ListAdminLOB[i].SrNo2 != null && lob.ListAdminLOB[i].SrNo3 != null)
                    {

                        SrNo3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "(" + Convert.ToString(ToRoman(SrNo3Count)) + ")";
                        outXml += @"<tr><td style='text-align: left; font-size: 22px;'><div style='padding-left:60px;text-align:justify;'>
                                    <b>" + SrNo3 + @"</b>&nbsp;&nbsp;&nbsp; <div style='padding-left:15px; margin: -31px 0 0 6%;text-align:justify;'>";
                        SrNo3Count = SrNo3Count + 1;

                    }

                    string LOBText = Convert.ToString(lob.ListAdminLOB[i].TextLOB);
                    if (LOBText != "")
                    {
                        LOBText = LOBText.Replace("<p>", "");
                        LOBText = LOBText.Replace("</p>", "");
                    }


                    outXml += @"" + LOBText + "<br/>";

                    if (lob.ListAdminLOB[i].PDFLocation != null && lob.ListAdminLOB[i].PDFLocation != "")
                    {
                        string pdfpath = lob.ListAdminLOB[i].PDFLocation;
                        pdfpath = pdfpath.Substring(1);
                        pdfpath = pdfpath.Replace(" ", "%20");
                        outXml += @" </td> </tr> <tr><td style='text-align: right;font-size:16px;font-weight:bold'>Paper :  <a href=" + pdfpath + " target='_blank' > <img style='width: 25px; height: 25px' title='PDF' src='/Images/Common/PDF.png'  />  </a>";
                        outXml += @"</td></tr>";
                    }
                    else
                    {
                        outXml += @"<br/> </td></tr>";
                    }

                    SubittedDate = Convert.ToDateTime(lob.ListAdminLOB[i].ApprovedDate).ToString("D", new CultureInfo("hi"));
                    if (Convert.ToString(lob.ListAdminLOB[i].ApprovedTime) != "")
                    {
                        TimeSpan interval = TimeSpan.Parse(Convert.ToString(lob.ListAdminLOB[i].ApprovedTime));
                        DateTime time = DateTime.Today.Add(interval);
                        string displayTime = time.ToString("hh:mm tt");

                        SubittedTime = displayTime;
                    }
                    else
                    {
                        SubittedTime = "";
                    }

                    if (i == lob.ListAdminLOB.Count - 1)
                    {
                        outXml += @" <tr><td colspan='2'><br /><br /></td></tr>

                                                <tr><td>
                                                <table style='width:100%;'>
                                                <tr>
                                                <td style='width:70%;float:left;font-size: 24px;'>
                                                   <b>शिमला-171004</b>
                                                </td>
                                                <td style='width:30%;float:left;font-size: 24px;'>
                                                  <b> " + CurrentSecretery + @",  </b>
                                                </td>
                                                </tr>
                                                <br/>
                                                <tr>
                                                  <td style='width:70%;float:left;font-size: 24px;'>
                                                 <b> दिनांक: " + SubittedDate + @"</b>
                                               <b> &nbsp;&nbsp;" + SubittedTime + @"</b>
                                                </td>
                                                <td style='width:30%;float:right;text-align:right;padding-right:85px;font-size: 24px;'>
                                                  <b>  सचिव।</b>
                                                </td>
                                                </tr>
                                                </table>           
                                                    </td>
                                                    </tr>";

                        outXml += "</table></div></div> </body> </html>";
                    }

                }

            }
            lob.sessDate = sessDate;

            lob.outXml = outXml;

            return PartialView("PartialLOB", lob);
        }


        /// <summary>
        /// convert numbers in to roman format
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "m" + ToRoman(number - 1000);
            if (number >= 900) return "cm" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "d" + ToRoman(number - 500);
            if (number >= 400) return "cd" + ToRoman(number - 400);
            if (number >= 100) return "c" + ToRoman(number - 100);
            if (number >= 90) return "xc" + ToRoman(number - 90);
            if (number >= 50) return "l" + ToRoman(number - 50);
            if (number >= 40) return "xl" + ToRoman(number - 40);
            if (number >= 10) return "x" + ToRoman(number - 10);
            if (number >= 9) return "ix" + ToRoman(number - 9);
            if (number >= 5) return "v" + ToRoman(number - 5);
            if (number >= 4) return "iv" + ToRoman(number - 4);
            if (number >= 1) return "i" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        /// <summary>
        /// Convert SQL date to dd/MM/yyyy format
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string ConvertSQLDate(DateTime? dt)
        {
            DateTime ConvtDate = Convert.ToDateTime(dt);
            string month = ConvtDate.Month.ToString();
            if (month.Length < 2)
            {
                month = "0" + month;
            }
            string day = ConvtDate.Day.ToString();
            string year = ConvtDate.Year.ToString();
            string Date = day + "/" + month + "/" + year;
            return Date;
        }

        #endregion

    }
}
