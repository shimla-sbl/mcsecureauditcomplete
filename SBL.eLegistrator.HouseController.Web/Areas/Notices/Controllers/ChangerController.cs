﻿using Lib.Web.Mvc.JQuery.JqGrid;
using Microsoft.Security.Application;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Linq;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Question;
using System.ComponentModel.DataAnnotations;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Diaries;
using System.IO;
using EvoPdf;
using System.Collections.Generic;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]

     
    public class ChangerController : Controller
    {
        //
        // GET: /Notices/Changer/
        
        public ActionResult Index()
        {
            String Userid = CurrentSession.RoleName;
            if (CurrentSession.RoleName == "VSTranslator")
            {


                string Type = CurrentSession.RoleName;
               
                tMemberNotice model = new tMemberNotice();
                model.RoleName = CurrentSession.RoleName;
               
                
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }

                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStarredAndUnstarredCnt", model);

                CurrentSession.AssemblyId = model.AssemblyID.ToString();
                CurrentSession.SessionId = model.SessionID.ToString();

                model.TotalValue = model.TotalStaredReceived + model.TotalUnStaredReceived;
                return View(model);
                
            }
            else if (CurrentSession.RoleName == "Admin")
            {
                string Type = CurrentSession.RoleName;

                tMemberNotice model = new tMemberNotice();
                model.RoleName = CurrentSession.RoleName;

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }

                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStarredAndUnstarredCnt", model);

                CurrentSession.AssemblyId = model.AssemblyID.ToString();
                CurrentSession.SessionId = model.SessionID.ToString();

                model.TotalValue = model.TotalStaredReceived + model.TotalUnStaredReceived;
                return View(model);
            }
            else if (CurrentSession.RoleName == "VS_Secretary")
            {
                string Type = CurrentSession.RoleName;

                tMemberNotice model = new tMemberNotice();
                model.RoleName = CurrentSession.RoleName;

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }

                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStarredAndUnstarredCnt", model);

                CurrentSession.AssemblyId = model.AssemblyID.ToString();
                CurrentSession.SessionId = model.SessionID.ToString();

                model.TotalValue = model.TotalStaredReceived + model.TotalUnStaredReceived;
                return View(model);
            }
            else
            {
                string Type = CurrentSession.RoleName;

                tMemberNotice model = new tMemberNotice();
                model.RoleName = CurrentSession.RoleName;

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }

                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStarredAndUnstarredCnt", model);

                CurrentSession.AssemblyId = model.AssemblyID.ToString();
                CurrentSession.SessionId = model.SessionID.ToString();

                model.TotalValue = model.TotalStaredReceived + model.TotalUnStaredReceived;
                return View(model);
              //  return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
          

        
        }
        
        public ActionResult MainPageAdmin()
        {
           
              if (CurrentSession.RoleName == "Admin")
            {
                string Type = CurrentSession.RoleName;

                tMemberNotice model = new tMemberNotice();
                model.RoleName = CurrentSession.RoleName;

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }

                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStarredAndUnstarredCnt", model);

                CurrentSession.AssemblyId = model.AssemblyID.ToString();
                CurrentSession.SessionId = model.SessionID.ToString();

                model.TotalValue = model.TotalStaredReceived + model.TotalUnStaredReceived;
                return View(model);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }



        }
        public ActionResult MainPageunstaredAdmin()
        {
           
              if (CurrentSession.RoleName == "Admin")
            {
                string Type = CurrentSession.RoleName;

                tMemberNotice model = new tMemberNotice();
                model.RoleName = CurrentSession.RoleName;

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }

                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStarredAndUnstarredCnt", model);

                CurrentSession.AssemblyId = model.AssemblyID.ToString();
                CurrentSession.SessionId = model.SessionID.ToString();

                model.TotalValue = model.TotalStaredReceived + model.TotalUnStaredReceived;
                return PartialView("MainPageunstaredAdmin", model);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }



        }
        public ActionResult PendingStaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            TypistQuestionModel model = new TypistQuestionModel();

            //string Month = "";
            //string Day = "";
            //string Year = "";
            //PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            //RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            //loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            //loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.QuestionTypeId = 1;
            //model.DepartmentId = CurrentSession.DeptID;
            //model.UId = CurrentSession.UserID;

            model.RoleName = CurrentSession.RoleName;
            model.UId = CurrentSession.UserID;
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            //model.AssemblyID = 12;
            //model.SessionID = 8;
            //model = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetStarredQuestions", model);

            model = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetStarredQuestionsForChanger", model);

            //if (PageNumber != null && PageNumber != "")
            //{
            //    model.PageNumber = Convert.ToInt32(PageNumber);
            //}
            //else
            //{
            //    model.PageNumber = Convert.ToInt32("1");
            //}
            //if (RowsPerPage != null && RowsPerPage != "")
            //{
            //    model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            //}
            //else
            //{
            //    model.RowsPerPage = Convert.ToInt32("10");
            //}
            //if (PageNumber != null && PageNumber != "")
            //{
            //    model.selectedPage = Convert.ToInt32(PageNumber);
            //}
            //else
            //{
            //    model.selectedPage = Convert.ToInt32("1");
            //}
            //if (loopStart != null && loopStart != "")
            //{
            //    model.loopStart = Convert.ToInt32(loopStart);
            //}
            //else
            //{
            //    model.loopStart = Convert.ToInt32("1");
            //}
            //if (loopEnd != null && loopEnd != "")
            //{
            //    model.loopEnd = Convert.ToInt32(loopEnd);
            //}
            //else
            //{
            //    model.loopEnd = Convert.ToInt32("5");
            //}

            return PartialView("StarredQuestions", model);
        }

     
        public ActionResult M_PendingStaredQuestion(int SDateId, string ViewBy)
        {
            TypistQuestionModel model = new TypistQuestionModel();
            model.RoleName = CurrentSession.RoleName;
            if (model.RoleName=="Admin") 
            {
                model.RoleName = "AdminPdfCorner";
            }

            
            model.SessionDateId = SDateId;
            model.DataStatus = ViewBy;
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetStarredQuestionsForChanger", model);
            return PartialView("StarredQuestions", model);
        }

      
        public ActionResult M_PendingUnstaredQuestion(int SDateId, string ViewBy)
        {
            TypistQuestionModel model = new TypistQuestionModel();
            model.RoleName = CurrentSession.RoleName;
            if (model.RoleName == "Admin")
            {
                model.RoleName = "AdminPdfCorner";
            }

            model.SessionDateId = SDateId;
            model.DataStatus = ViewBy;
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetUnStarredQuestionsForChanger", model);
            return PartialView("UnstarredQuestions", model);
        }

        public ActionResult GetAllStarredQuestionsGrid(JqGridRequest request, TypistQuestionModel customModel)
        {
            TypistQuestionModel model = new TypistQuestionModel();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.QuestionTypeId = 1;
            model.DepartmentId = CurrentSession.DeptID;
            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            var result = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetStarredQuestions", model);
            model.ProofReaderName = result.ProofReaderName;

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<TypistQuestionModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<TypistQuestionModel>(Convert.ToString(questionList.QuestionID), new TypistQuestionModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        public ActionResult GetDataQuestionID(int questionID)
        {
#pragma warning disable CS0219 // The variable 'Month' is assigned but its value is never used
            string Month = "";
#pragma warning restore CS0219 // The variable 'Month' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Day' is assigned but its value is never used
            string Day = "";
#pragma warning restore CS0219 // The variable 'Day' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Year' is assigned but its value is never used
            string Year = "";
#pragma warning restore CS0219 // The variable 'Year' is assigned but its value is never used
            tMemberNotice model = new tMemberNotice();
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            int Id = Convert.ToInt32(questionID);
            model.QuestionID = Id;
            model.UserID = new Guid(CurrentSession.UserID);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStrarredQDetailsChanger", model);

            foreach (var item in model.tQuestionModel)
            {
                model.TitleChanger = item.Subject;
                model.MemberId = item.MemberId;
                model.QuestionID = item.QuestionID;
                model.DetailsChanger = item.MainQuestion;
                model.IsHindi = item.IsHindi;
                model.IsQuestionInPart = item.IsQuestionInPart;
				model.ConstituencyName = item.ConstituencyName;
                model.QuestionSequence = item.Questionsequence;
                model.IsTranceLock = item.IsLockedTranslator;
                model.IsClubbed=item.IsClubbed;
                model.ReferenceCMMemberCode = item.ReferenceMemberCode;
                model.QuestionTypeId = item.QuestionTypeId;
            };
            model.RoleName = CurrentSession.RoleName;
            return PartialView("_EditFormStartQuestion", model);
        }
        public JsonResult UpdateContentChanger(value model)
        {
            tMemberNotice Update = new tMemberNotice();
            tQuestion objModel = new tQuestion();
            objModel.QuestionID = model.QuestionID;
            objModel.Subject = model.TitleChanger;
            objModel.MainQuestion = model.DetailsChanger;
            objModel.IsQuestionInPart = model.IsQuestionInPart;
            objModel.IsHindi = model.IsHindi;
			objModel.ConstituencyName = model.ConstituencyName;
            objModel.QuestionSequence = model.Questionsequence;
            objModel.userId = model.userid;
            objModel.UserGuId = new Guid(model.userid);
            objModel.ReferenceMemberCode = model.ReferenceMemcode;
            objModel.QuestionType = model.QuestionType;
            Update.Message = Helper.ExecuteService("Notice", "SaveChangerTQuestions", objModel) as string;

            if (Update.Message.Substring(0, 2) == "SF")
            {
                GeneratePdfByQuesIdStarred_Fix(objModel.QuestionID.ToString());
            }
            else if (Update.Message.Substring(0, 2) == "SU")
            {
                GeneratePdfByQuesIdStarred_Unfix(objModel.QuestionID);
            }
            else if (Update.Message.Substring(0, 2) == "UF")
            {
                GeneratePdfByQuesIdUnstarred_Fix(objModel.QuestionID.ToString());
            }
            else if (Update.Message.Substring(0, 2) == "UU")
            {
                GeneratePdfByQuesIdUnstarred_Unfix(objModel.QuestionID);
            }
            Update.Message = Update.Message.Substring(3);
            return Json(Update.Message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PendingUnstaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            TypistQuestionModel model = new TypistQuestionModel();

            //PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            //RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            //loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            //loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.UId = CurrentSession.UserID;
            //model.QuestionTypeId = 2;
            //model.DepartmentId = CurrentSession.DeptID;

            model.RoleName = CurrentSession.RoleName;
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            //model = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetUnStarredQuestions", model);

            model = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetUnStarredQuestionsForChanger", model);

            //if (PageNumber != null && PageNumber != "")
            //{
            //    model.PageNumber = Convert.ToInt32(PageNumber);
            //}
            //else
            //{
            //    model.PageNumber = Convert.ToInt32("1");
            //}
            //if (RowsPerPage != null && RowsPerPage != "")
            //{
            //    model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            //}
            //else
            //{
            //    model.RowsPerPage = Convert.ToInt32("10");
            //}
            //if (PageNumber != null && PageNumber != "")
            //{
            //    model.selectedPage = Convert.ToInt32(PageNumber);
            //}
            //else
            //{
            //    model.selectedPage = Convert.ToInt32("1");
            //}
            //if (loopStart != null && loopStart != "")
            //{
            //    model.loopStart = Convert.ToInt32(loopStart);
            //}
            //else
            //{
            //    model.loopStart = Convert.ToInt32("1");
            //}
            //if (loopEnd != null && loopEnd != "")
            //{
            //    model.loopEnd = Convert.ToInt32(loopEnd);
            //}
            //else
            //{
            //    model.loopEnd = Convert.ToInt32("5");
            //}

            return PartialView("UnstarredQuestions", model);
        }

        public ActionResult GetUnStarredQuestionsGrid(JqGridRequest request, TypistQuestionModel customModel)
        {
            TypistQuestionModel model = new TypistQuestionModel();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.QuestionTypeId = 2;
            model.DepartmentId = CurrentSession.DeptID;
            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            var result = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetUnStarredQuestions", model);
            model.ProofReaderName = result.ProofReaderName;
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<TypistQuestionModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<TypistQuestionModel>(Convert.ToString(questionList.QuestionID), new TypistQuestionModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        public ActionResult GetDataUnstart(int questionID)
        {
            tMemberNotice model = new tMemberNotice();
            int Id = Convert.ToInt32(questionID);
            model.QuestionID = Id;
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.UserID = new Guid(CurrentSession.UserID);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStrarredQDetailsChanger", model);
            foreach (var item in model.tQuestionModel)
            {
                model.TitleChanger = item.Subject;
                model.QuestionID = item.QuestionID;
                model.DetailsChanger = item.MainQuestion;
                model.IsQuestionInPart = item.IsQuestionInPart;
                model.IsHindi = item.IsHindi;
				model.ConstituencyName = item.ConstituencyName;
                model.QuestionSequence = item.Questionsequence;
                model.IsTranceLock = item.IsLockedTranslator;
                model.IsClubbed = item.IsClubbed;
                model.ReferenceCMMemberCode = item.ReferenceMemberCode;
                model.QuestionTypeId = item.QuestionTypeId;
            }
            model.RoleName = CurrentSession.RoleName;
            return PartialView("_EditFormUnStartQuestion", model);


        }


        public ActionResult GetStarredfixedQuestions(int Qtype)
        {
            DiaryModel model = new DiaryModel();
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
           
            if (CurrentSession.RoleName == "VSTranslator")
            {
                
                model = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetStarredfixedQuestions", model);

                if (Qtype == (int)QuestionType.StartedQuestion)
                {
                    model.DeActivateFlag = CurrentSession.UserID;
                    return PartialView("_SGetSessionDate", model);
                }

                else if (Qtype == (int)QuestionType.UnstaredQuestion)
                {
                    model.DeActivateFlag = CurrentSession.UserID;
                    return PartialView("_UGetSessionDate", model);
                }
            }
            else
            {
                if (Qtype == (int)QuestionType.StartedQuestion)
                {
                    return RedirectToAction("M_PendingStaredQuestion", new { SDateId = 0, ViewBy = "All" });
                }
                else if (Qtype == (int)QuestionType.UnstaredQuestion)
                {
                    return RedirectToAction("M_PendingUnstaredQuestion", new { SDateId = 0, ViewBy = "All" });
                }
            }

            return null;
        }

        public JsonResult ApproveGeneratedPDFByDate(int SDateId, string PType, string ViewBy)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.UserName = CurrentSession.RoleName; //this field is using for RoleName
            Dmdl.SessionDateId = SDateId;
            Dmdl.PaperEntryType = PType;
            Dmdl.ViewType = ViewBy;
            Dmdl.IsPostPoneDate = DateTime.Now;
            Dmdl.DateEntry = DateTime.Now;  
            Dmdl.UserGuid = CurrentSession.UserID;
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            Dmdl.CutMotionFilePath = FileSettings.SettingValue;
            Dmdl.Message = Helper.ExecuteService("Notice", "ApproveGeneratedPDFByDate", Dmdl) as string;
            return Json(Dmdl.Message, JsonRequestBehavior.AllowGet);

        }

        public JsonResult BackToTranByDate(int SDateId, string PType, string ViewBy)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.SessionDateId = SDateId;
            Dmdl.PaperEntryType = PType;
            Dmdl.ViewType = ViewBy;
            Dmdl.Message = Helper.ExecuteService("Notice", "BackToTranByDate", Dmdl) as string;
            return Json(Dmdl.Message, JsonRequestBehavior.AllowGet);

        }

        public void GeneratePdfByQuesIdStarred_Fix(string QIds)
        {
            try
            {
                string str1 = "";
                string empty = string.Empty;
                tMemberNotice tMemberNotice = new tMemberNotice();
                string str2 = QIds;
                char[] chArray = new char[1] { ',' };
                foreach (string str3 in str2.Split(chArray))
                {
                    int int16 = (int)Convert.ToInt16(str3);
                    tMemberNotice.QuestionID = int16;
                    tMemberNotice.Updatedate = new DateTime?(DateTime.Now);
                    tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)tMemberNotice);
                    MemoryStream memoryStream = new MemoryStream();
                    Document document = new Document();
                    document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document.CompressionLevel = PdfCompressionLevel.Best;
                    document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                    if (tMemberNotice.tQuestionModel != null)
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            bool? isHindi = questionModelCustom.IsHindi;
                            bool flag = true;
                            if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                            {
                                str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n            नगर निगम प्रश्न   \r\n              </td>\r\n              </tr>\r\n      </table>";
                            }
                            else
                            {
                                str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:27px;' >\r\n            QUESTION FOR MUNCIPAL CORPORATION  \r\n              </td>\r\n              </tr>\r\n      </table>";
                            }
                        }
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            if (questionModelCustom.IsClubbed.HasValue)
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag1 = true;
                                string str4;
                                if (isHindi.GetValueOrDefault() == flag1 & isHindi.HasValue)
                                {
                                    string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                    }
                                }
                                else
                                {
                                    string[] strArray = questionModelCustom.CMemName.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                    }
                                }
                                isHindi = questionModelCustom.IsHindi;
                                bool flag2 = true;
                                if (isHindi.GetValueOrDefault() == flag2 & isHindi.HasValue)
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          प्रश्न संख्या: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          निश्चित दिनांक: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str4 + "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             D. No.: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Question Number: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          Fixed On: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str4 + "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be Pleased to state :\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                            else
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag = true;
                                if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             तारांकित डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          प्रश्न संख्या: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          निश्चित दिनांक: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberNameLocal + "(" + questionModelCustom.ConstituencyName_Local + ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>     \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             Starred D. No.: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Question Number: " + questionModelCustom.QuestionNumber.ToString() + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          Fixed On: " + Convert.ToDateTime((object)questionModelCustom.DesireLayingDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberName + "(" + questionModelCustom.ConstituencyName + ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be Pleased to state :\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    string htmlStringBaseURL = "";
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                        }
                        str1 += "\r\n\r\n\r\n\r\n                   \r\n                \r\n                     </body> </html>";
                    }
                    byte[] buffer = document.Save();
                    try
                    {
                        memoryStream.Write(buffer, 0, buffer.Length);
                        memoryStream.Position = 0L;
                    }
                    finally
                    {
                        document.Close();
                    }
                    string str5 = "Question/BeforeFixation/" + tMemberNotice.AssemblyID.ToString() + "/" + tMemberNotice.SessionID.ToString() + "/";
                    string[] strArray1 = new string[6];
                    int num = tMemberNotice.AssemblyID;
                    strArray1[0] = num.ToString();
                    strArray1[1] = "_";
                    num = tMemberNotice.SessionID;
                    strArray1[2] = num.ToString();
                    strArray1[3] = "_S_";
                    num = tMemberNotice.QuestionID;
                    strArray1[4] = num.ToString();
                    strArray1[5] = ".pdf";
                    string path2 = string.Concat(strArray1);
                    this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                    string str6 = tMemberNotice.FilePath + str5;
                    if (!Directory.Exists(str6))
                        Directory.CreateDirectory(str6);
                    FileStream fileStream = new FileStream(Path.Combine(str6, path2), FileMode.Create, FileAccess.Write);
                    fileStream.Write(buffer, 0, buffer.Length);
                    fileStream.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GeneratePdfByQuesIdUnstarred_Fix(string QIds)
        {
            try
            {
                string str1 = "";
                string empty = string.Empty;
                tMemberNotice tMemberNotice = new tMemberNotice();
                string str2 = QIds;
                char[] chArray = new char[1] { ',' };
                foreach (string str3 in str2.Split(chArray))
                {
                    int int16 = (int)Convert.ToInt16(str3);
                    tMemberNotice.QuestionID = int16;
                    tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)tMemberNotice);
                    MemoryStream memoryStream = new MemoryStream();
                    Document document = new Document();
                    document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document.CompressionLevel = PdfCompressionLevel.Best;
                    document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                    if (tMemberNotice.tQuestionModel != null)
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            bool? isHindi = questionModelCustom.IsHindi;
                            bool flag = true;
                            if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                            {
                                str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n            नगर निगम प्रश्न   \r\n              </td>\r\n              </tr>\r\n      </table>";
                            }
                            else
                            {
                                str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n            QUESTION FOR MUNCIPAL CORPORATION \r\n              </td>\r\n              </tr>\r\n      </table>";
                            }
                        }
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                        {
                            if (questionModelCustom.IsClubbed.HasValue)
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag1 = true;
                                string str4;
                                if (isHindi.GetValueOrDefault() == flag1 & isHindi.HasValue)
                                {
                                    string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                    }
                                }
                                else
                                {
                                    string[] strArray = questionModelCustom.CMemName.Split(',');
                                    str4 = "";
                                    for (int index = 0; index < strArray.Length; ++index)
                                    {
                                        strArray[index] = strArray[index].Trim();
                                        str4 = str4 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                    }
                                }
                                isHindi = questionModelCustom.IsHindi;
                                bool flag2 = true;
                                if (isHindi.GetValueOrDefault() == flag2 & isHindi.HasValue)
                                {
                                    string[] strArray = new string[22];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             डायरी संख्या: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          प्रश्न संख्या: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          निश्चित दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = str4;
                                    strArray[17] = "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                    strArray[18] = questionModelCustom.MinistryNameLocal;
                                    strArray[19] = " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[20] = questionModelCustom.MainQuestion;
                                    strArray[21] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    string[] strArray = new string[22];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              D. No.: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Question Number: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          Fixed On: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = str4;
                                    strArray[17] = "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align:top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                    strArray[18] = questionModelCustom.MinistryName;
                                    strArray[19] = " be Pleased to state :\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         \r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[20] = questionModelCustom.MainQuestion;
                                    strArray[21] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                            else
                            {
                                bool? isHindi = questionModelCustom.IsHindi;
                                bool flag = true;
                                if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                                {
                                    string[] strArray = new string[24];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             डायरी संख्या: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          प्रश्न संख्या: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          निश्चित दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = questionModelCustom.MemberNameLocal;
                                    strArray[17] = "(";
                                    strArray[18] = questionModelCustom.ConstituencyName_Local;
                                    strArray[19] = ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                    strArray[20] = questionModelCustom.MinistryNameLocal;
                                    strArray[21] = " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[22] = questionModelCustom.MainQuestion;
                                    strArray[23] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    string htmlStringBaseURL = "";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                                else
                                {
                                    string[] strArray = new string[24];
                                    strArray[0] = str1;
                                    strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              D. No.: ";
                                    strArray[2] = questionModelCustom.DiaryNumber;
                                    strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: ";
                                    strArray[4] = questionModelCustom.Subject;
                                    strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: ";
                                    DateTime dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                    strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          प्रश्न संख्या: ";
                                    strArray[10] = questionModelCustom.QuestionNumber.ToString();
                                    strArray[11] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          निश्चित दिनांक: ";
                                    dateTime = Convert.ToDateTime((object)questionModelCustom.DesireLayingDate);
                                    strArray[12] = dateTime.ToString("dd/MM/yyyy");
                                    strArray[13] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                    strArray[14] = questionModelCustom.DiaryNumber;
                                    strArray[15] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                    strArray[16] = questionModelCustom.MemberName;
                                    strArray[17] = "(";
                                    strArray[18] = questionModelCustom.ConstituencyName;
                                    strArray[19] = ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                    strArray[20] = questionModelCustom.MinistryName;
                                    strArray[21] = " be Pleased to state :\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n<table style='width:80%'>\r\n     <tr>\r\n        <td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                    strArray[22] = questionModelCustom.MainQuestion;
                                    strArray[23] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    str1 = string.Concat(strArray);
                                    PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                    str1 += "\r\n                     </body> </html>";
                                    string htmlStringToConvert = str1;
                                    string htmlStringBaseURL = "";
                                    str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                    pdfPage.AddElement((PageElement)htmlToPdfElement);
                                }
                            }
                        }
                        str1 += "\r\n\r\n\r\n\r\n                   \r\n                \r\n                     </body> </html>";
                    }
                    byte[] buffer = document.Save();
                    try
                    {
                        memoryStream.Write(buffer, 0, buffer.Length);
                        memoryStream.Position = 0L;
                    }
                    finally
                    {
                        document.Close();
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        if (questionModelCustom.MainQuestion != null)
                        {
                            string str4 = "Question/BeforeFixation/" + tMemberNotice.AssemblyID.ToString() + "/" + tMemberNotice.SessionID.ToString() + "/";
                            string[] strArray = new string[6];
                            int num = tMemberNotice.AssemblyID;
                            strArray[0] = num.ToString();
                            strArray[1] = "_";
                            num = tMemberNotice.SessionID;
                            strArray[2] = num.ToString();
                            strArray[3] = "_U_";
                            num = tMemberNotice.QuestionID;
                            strArray[4] = num.ToString();
                            strArray[5] = ".pdf";
                            string path2 = string.Concat(strArray);
                            this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                            string str5 = tMemberNotice.FilePath + str4;
                            if (!Directory.Exists(str5))
                                Directory.CreateDirectory(str5);
                            FileStream fileStream = new FileStream(Path.Combine(str5, path2), FileMode.Create, FileAccess.Write);
                            fileStream.Write(buffer, 0, buffer.Length);
                            fileStream.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GeneratePdfByQuesIdStarred_Unfix(int QuestionId)
        {
            try
            {
                string str1 = "";
                string empty = string.Empty;
                tMemberNotice tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)new tMemberNotice()
                {
                    QuestionID = QuestionId,
                    Updatedate = new DateTime?(DateTime.Now)
                });
                MemoryStream memoryStream = new MemoryStream();
                Document document = new Document();
                document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document.CompressionLevel = PdfCompressionLevel.Best;
                document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                if (tMemberNotice.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? isHindi = questionModelCustom.IsHindi;
                        bool flag = true;
                        if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                        {
                            str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                            str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n            नगर निगम प्रश्न   \r\n              </td>\r\n              </tr>\r\n      </table>";
                        }
                        else
                        {
                            str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                            str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:27px;' >\r\n            QUESTION FOR MUNCIPAL CORPORATION  \r\n              </td>\r\n              </tr>\r\n      </table>";
                        }
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? nullable = questionModelCustom.IsClubbed;
                        if (nullable.HasValue)
                        {
                            nullable = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            string str2;
                            if (nullable.GetValueOrDefault() == flag1 & nullable.HasValue)
                            {
                                string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                str2 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str2 = str2 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                }
                            }
                            else
                            {
                                string[] strArray = questionModelCustom.CMemName.Split(',');
                                str2 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str2 = str2 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                }
                            }
                            nullable = questionModelCustom.IsHindi;
                            bool flag2 = true;
                            if (nullable.GetValueOrDefault() == flag2 & nullable.HasValue)
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str2 + "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              D. No.: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + str2 + "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be Pleased to state :\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                        else
                        {
                            nullable = questionModelCustom.IsHindi;
                            bool flag = true;
                            if (nullable.GetValueOrDefault() == flag & nullable.HasValue)
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             तारांकित डायरी संख्या: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberNameLocal + "(" + questionModelCustom.ConstituencyName_Local + ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या " + questionModelCustom.MinistryNameLocal + " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>     \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                str1 = str1 + "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             Starred D. No.: " + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: " + questionModelCustom.Subject + "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: " + Convert.ToDateTime((object)questionModelCustom.NoticeDate).ToString("dd/MM/yyyy") + "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                *" + questionModelCustom.DiaryNumber + "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n" + questionModelCustom.MemberName + "(" + questionModelCustom.ConstituencyName + ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the " + questionModelCustom.MinistryName + " be Pleased to state :\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  " + questionModelCustom.MainQuestion + "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                string htmlStringBaseURL = "";
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                    }
                    string str3 = str1 + "\r\n\r\n\r\n\r\n                   \r\n                \r\n                     </body> </html>";
                }
                byte[] buffer = document.Save();
                try
                {
                    memoryStream.Write(buffer, 0, buffer.Length);
                    memoryStream.Position = 0L;
                }
                finally
                {
                    document.Close();
                }
                string str4 = "Question/BeforeFixation/" + tMemberNotice.AssemblyID.ToString() + "/" + tMemberNotice.SessionID.ToString() + "/";
                string[] strArray1 = new string[6];
                int num = tMemberNotice.AssemblyID;
                strArray1[0] = num.ToString();
                strArray1[1] = "_";
                num = tMemberNotice.SessionID;
                strArray1[2] = num.ToString();
                strArray1[3] = "_S_";
                num = tMemberNotice.QuestionID;
                strArray1[4] = num.ToString();
                strArray1[5] = ".pdf";
                string path2 = string.Concat(strArray1);
                this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                string str5 = tMemberNotice.FilePath + str4;
                if (!Directory.Exists(str5))
                    Directory.CreateDirectory(str5);
                FileStream fileStream = new FileStream(Path.Combine(str5, path2), FileMode.Create, FileAccess.Write);
                fileStream.Write(buffer, 0, buffer.Length);
                fileStream.Close();
                return str5 + path2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GeneratePdfByQuesIdUnstarred_Unfix(int QuestionId)
        {
            try
            {
                string str1 = "";
                string str2 = string.Empty;
                tMemberNotice tMemberNotice = (tMemberNotice)Helper.ExecuteService("Notice", "GetDataByQuestionId", (object)new tMemberNotice()
                {
                    QuestionID = QuestionId
                });
                MemoryStream memoryStream = new MemoryStream();
                Document document = new Document();
                document.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document.CompressionLevel = PdfCompressionLevel.Best;
                document.Margins = new Margins(0.0f, 0.0f, 0.0f, 0.0f);
                if (tMemberNotice.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? isHindi = questionModelCustom.IsHindi;
                        bool flag = true;
                        if (isHindi.GetValueOrDefault() == flag & isHindi.HasValue)
                        {
                            str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            str1 += "<div> \r\n                    </div>\r\n            \r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n            नगर निगम प्रश्न   \r\n              </td>\r\n              </tr>\r\n      </table>";
                        }
                        else
                        {
                            str1 = "<html >                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            str1 += "<div> \r\n                    </div>\r\n\r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n  <td style='text-align:center;font-size:28px;' >\r\n            QUESTION FOR MUNCIAPAL CORPORATION  \r\n              </td>\r\n              </tr>\r\n      </table>";
                        }
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                    {
                        bool? nullable = questionModelCustom.IsClubbed;
                        DateTime dateTime;
                        if (nullable.HasValue)
                        {
                            nullable = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            string str3;
                            if (nullable.GetValueOrDefault() == flag1 & nullable.HasValue)
                            {
                                string[] strArray = questionModelCustom.CMemNameHindi.Split(',');
                                str3 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str3 = str3 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                }
                            }
                            else
                            {
                                string[] strArray = questionModelCustom.CMemName.Split(',');
                                str3 = "";
                                for (int index = 0; index < strArray.Length; ++index)
                                {
                                    strArray[index] = strArray[index].Trim();
                                    str3 = str3 + "<div>\r\n          " + strArray[index] + " \r\n\r\n                               </div>\r\n                             ";
                                }
                            }
                            nullable = questionModelCustom.IsHindi;
                            bool flag2 = true;
                            if (nullable.GetValueOrDefault() == flag2 & nullable.HasValue)
                            {
                                string[] strArray = new string[18];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             डायरी संख्या: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = str3;
                                strArray[13] = "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                strArray[14] = questionModelCustom.MinistryNameLocal;
                                strArray[15] = " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         <td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[16] = questionModelCustom.MainQuestion;
                                strArray[17] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                string[] strArray = new string[18];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n\r\n    <table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n              D. No.: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = str3;
                                strArray[13] = "\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align:top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                strArray[14] = questionModelCustom.MinistryName;
                                strArray[15] = " be Pleased to state :\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n         \r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[16] = questionModelCustom.MainQuestion;
                                strArray[17] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                        else
                        {
                            nullable = questionModelCustom.IsHindi;
                            bool flag = true;
                            if (nullable.GetValueOrDefault() == flag & nullable.HasValue)
                            {
                                string[] strArray = new string[20];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             अतारांकित डायरी संख्या: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             विषय: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          सूचना प्राप्ति दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n          स्वीकृत सूचना दिनांक: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = questionModelCustom.MemberNameLocal;
                                strArray[13] = "(";
                                strArray[14] = questionModelCustom.ConstituencyName_Local;
                                strArray[15] = ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  क्या ";
                                strArray[16] = questionModelCustom.MinistryNameLocal;
                                strArray[17] = " बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n\r\n<table style='width:80%'>\r\n     <tr><td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[18] = questionModelCustom.MainQuestion;
                                strArray[19] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                string htmlStringBaseURL = "";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                            else
                            {
                                string[] strArray = new string[20];
                                strArray[0] = str1;
                                strArray[1] = "<table style='width:100%'>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             Unstarred D. No.: ";
                                strArray[2] = questionModelCustom.DiaryNumber;
                                strArray[3] = "\r\n              </td>\r\n       \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n      <td style='text-align:left;font-size:20px;' >\r\n             subject: ";
                                strArray[4] = questionModelCustom.Subject;
                                strArray[5] = "\r\n              </td>\r\n        \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n          Notice Recieved on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[6] = dateTime.ToString("dd/MM/yyyy");
                                strArray[7] = "      \r\n              </td>\r\n \r\n <td style='text-align:left;font-size:20px;' >\r\n         Notice of Admission sent on: ";
                                dateTime = Convert.ToDateTime((object)questionModelCustom.NoticeDate);
                                strArray[8] = dateTime.ToString("dd/MM/yyyy");
                                strArray[9] = "\r\n              </td>\r\n \r\n         \r\n              </tr>\r\n       \r\n<table style='width:80%'>\r\n     <tr>\r\n         <td style='text-align:left;vertical-align: top; font-size:20px;'>\r\n                ";
                                strArray[10] = questionModelCustom.DiaryNumber;
                                strArray[11] = "\r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n";
                                strArray[12] = questionModelCustom.MemberName;
                                strArray[13] = "(";
                                strArray[14] = questionModelCustom.ConstituencyName;
                                strArray[15] = ")\r\n</td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n <tr>\r\n         <td style='text-align:left; vertical-align: top; font-size:20px;'>\r\n            \r\n              </td>\r\n<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>\r\n  Will the ";
                                strArray[16] = questionModelCustom.MinistryName;
                                strArray[17] = " be Pleased to state :\r\n</td>\r\n\r\n         \r\n              </tr>\r\n   \r\n           </table>\r\n<table style='width:80%'>\r\n     <tr>\r\n        <td style='width:3%'>&nbsp;</td>\r\n<td style='text-align:justify; font-size:20px;padding-bottom: 25px;'>\r\n  ";
                                strArray[18] = questionModelCustom.MainQuestion;
                                strArray[19] = "\r\n</td>\r\n\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                str1 = string.Concat(strArray);
                                PdfPage pdfPage = document.Pages.AddNewPage(PdfPageSize.A4, new Margins(0.0f, 0.0f, 40f, 40f), PdfPageOrientation.Portrait);
                                str1 += "\r\n                     </body> </html>";
                                string htmlStringToConvert = str1;
                                string htmlStringBaseURL = "";
                                str1 = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0.0f, 0.0f, 0.0f, 0.0f, htmlStringToConvert, htmlStringBaseURL);
                                pdfPage.AddElement((PageElement)htmlToPdfElement);
                            }
                        }
                    }
                    string str4 = str1 + "\r\n\r\n\r\n\r\n                   \r\n                \r\n                     </body> </html>";
                }
                byte[] buffer = document.Save();
                try
                {
                    memoryStream.Write(buffer, 0, buffer.Length);
                    memoryStream.Position = 0L;
                }
                finally
                {
                    document.Close();
                }
                foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice.tQuestionModel)
                {
                    if (questionModelCustom.MainQuestion != null)
                    {
                        string[] strArray1 = new string[5]
                        {
              "Question/BeforeFixation/",
              null,
              null,
              null,
              null
                        };
                        int num = tMemberNotice.AssemblyID;
                        strArray1[1] = num.ToString();
                        strArray1[2] = "/";
                        num = tMemberNotice.SessionID;
                        strArray1[3] = num.ToString();
                        strArray1[4] = "/";
                        string str3 = string.Concat(strArray1);
                        string[] strArray2 = new string[6];
                        num = tMemberNotice.AssemblyID;
                        strArray2[0] = num.ToString();
                        strArray2[1] = "_";
                        num = tMemberNotice.SessionID;
                        strArray2[2] = num.ToString();
                        strArray2[3] = "_U_";
                        num = tMemberNotice.QuestionID;
                        strArray2[4] = num.ToString();
                        strArray2[5] = ".pdf";
                        string path2 = string.Concat(strArray2);
                        this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                        string str4 = tMemberNotice.FilePath + str3;
                        if (!Directory.Exists(str4))
                            Directory.CreateDirectory(str4);
                        FileStream fileStream = new FileStream(Path.Combine(str4, path2), FileMode.Create, FileAccess.Write);
                        fileStream.Write(buffer, 0, buffer.Length);
                        fileStream.Close();
                        str2 = str4 + path2;
                    }
                }
                return str2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult lockTranslatorunstarred(string Qids)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.QuesIds = Qids;
            Dmdl.Message = (string)Helper.ExecuteService("Notice", "LocktransUnstarred", Dmdl);
                     
            return Json( JsonRequestBehavior.AllowGet);

        }

     
        public JsonResult UnlockTranslatorunstarred(string Qids)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.QuesIds = Qids;
            Dmdl.Message = (string)Helper.ExecuteService("Notice", "UnLocktransUnstarred", Dmdl);

            return Json(JsonRequestBehavior.AllowGet);

        }

        #region Starred-Unstarred-Question-PDF
        public ActionResult StarredQuestionsPDF()
        {
            if (CurrentSession.RoleName == "Admin")
            {
                string Type = CurrentSession.RoleName;

                tMemberNotice model = new tMemberNotice();
                model.RoleName = CurrentSession.RoleName;

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }

                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStarredAndUnstarredCnt", model);

                CurrentSession.AssemblyId = model.AssemblyID.ToString();
                CurrentSession.SessionId = model.SessionID.ToString();

                model.TotalValue = model.TotalStaredReceived + model.TotalUnStaredReceived;
                return View("StarredQuestionsPDF", model);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }



        }
        public ActionResult UnstarredQuestionsPDF()
        {
            if (CurrentSession.RoleName == "Admin")
            {
                string Type = CurrentSession.RoleName;

                tMemberNotice model = new tMemberNotice();
                model.RoleName = CurrentSession.RoleName;

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                }

                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetStarredAndUnstarredCnt", model);

                CurrentSession.AssemblyId = model.AssemblyID.ToString();
                CurrentSession.SessionId = model.SessionID.ToString();

                model.TotalValue = model.TotalStaredReceived + model.TotalUnStaredReceived;
                return PartialView("UnstarredQuestionsPDF", model);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }



        }
        public ActionResult GetStarredfixedQuestionsPDF(int Qtype)
        {
            DiaryModel model = new DiaryModel();
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
           
            if (true)
            {
                model = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetStarredfixedQuestions", model);

                if (Qtype == (int)QuestionType.StartedQuestion)
                {
                    return PartialView("_SGetSessionDate", model);
                }

                else if (Qtype == (int)QuestionType.UnstaredQuestion)
                {
                    return PartialView("_UGetSessionDate", model);
                }
            }
            else
            {
#pragma warning disable CS0162 // Unreachable code detected
                if (Qtype == (int)QuestionType.StartedQuestion)
#pragma warning restore CS0162 // Unreachable code detected
                {
                    return RedirectToAction("M_PendingStaredQuestion", new { SDateId = 0, ViewBy = "All" });
                }
                else if (Qtype == (int)QuestionType.UnstaredQuestion)
                {
                    return RedirectToAction("M_PendingUnstaredQuestion", new { SDateId = 0, ViewBy = "All" });
                }
            }

            return null;
        }
        #endregion

    }
    public class value
    {
        public int QuestionID { get; set; }
        [AllowHtml]
        public string TitleChanger { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string DetailsChanger { get; set; }
        public bool? IsQuestionInPart { get; set; }
        public bool? IsHindi { get; set; }
		public string ConstituencyName { get; set; }
        public int? Questionsequence { get; set; }
        public string userid { get; set; }
        public string ReferenceMemcode { get; set; }
        public int QuestionType { get; set; }
        
    }

    
}
