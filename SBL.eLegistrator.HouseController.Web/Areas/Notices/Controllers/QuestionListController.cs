﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using EvoPdf;
//using iTextSharp.text;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
//using iTextSharp.text.html.simpleparser;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Assembly;
using Ionic.Zip;
using System.Net;
//using iTextSharp.text.pdf;
using SBL.DomainModel.Models.SessionDateSignature;
using SBL.DomainModel.ComplexModel;
using System.Drawing;
using iTextSharp.text;
using System.Collections.Generic;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class QuestionListController : Controller
    {
        //
        // GET: /Notices/QuestionList/

        public ActionResult Index()
        {
            tMemberNotice model = new tMemberNotice();
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionDates", model);
            model.RoleID = CurrentSession.RoleID;
            return View(model);
        }
        public ActionResult Unstared()
        {
            tMemberNotice model = new tMemberNotice();
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionDatesUnstart", model);
            model.RoleID = CurrentSession.RoleID;
            return View(model);
        }
        public ActionResult UnstaredHtml(string Id)
        {
            tMemberNotice model = new tMemberNotice();

            //objModel.NoticeRecievedDate = date;

            UnstaresGeneratePdf(Id);
            model.Message = "Question List Generated";

            return PartialView("Unstared", model);
        }

        static string UppercaseWords(string value)
        {
            char[] array = value.ToCharArray();
            // Handle the first letter in the string.
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            // Scan through the letters, checking for spaces.
            // ... Uppercase the lowercase letters following spaces.
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }


        public string UnstaresGeneratePdf(string Id)
        {
            try
            {
                string outXml = "";
                string outInnerXml = "";
                string LOBName = Id;
                string fileName = "";
                string savedPDF = string.Empty;


                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);
                tMemberNotice model = new tMemberNotice();

                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                string strDate = Id;
                DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
                model.NoticeDate = date;
                model = (tMemberNotice)Helper.ExecuteService("Notice", "UnstaredGetAllDetailsForSpdf", model);
                if (model.tQuestionModel != null)
                {
                    foreach (var item in model.tQuestionModel)
                    {
                        model.SessionDateId = item.SessionDateId;
                        model.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", model);
                    }
                }
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", model);


                if (model.tQuestionModel != null)
                {
                    foreach (var item in model.tQuestionModel)
                    {
                        outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                        outXml += @"<div> 


                    </div>
            
<table style='width:100%'>
 
      <tr>
          <td style='text-align:center;font-size:28px;' >
             <b>हिमाचल प्रदेश तेरहवीं विधान सभा</b>
              </td>
              </tr>
      </table>
<br />
<br />
<br />
<br />
<br />
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
            <b>" + "(" + model.SessionName + ")" + @"</b>
              </td>
              </tr>
           </table>
<br />
<br />
<br />
<br />
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
            <b>लिखित उत्तर हेतु प्रश्न</b>          
              </td>    
              </tr>
           </table>
<table style='width:100%'>
      <tr>
          <td style='text-align:center;font-size:28px;' >
          <b>" + item.SessionDateLocal + @"</b>   
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
            <b>" + "[" + model.MinisterName + "]" + @".</b>
            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           <b>कुल प्रश्न - " + model.TotalQCount + @"</b>
            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----
            
              </td>
              </tr>
           </table>";

                    }
                    foreach (var item in model.tQuestionModel)
                    {
                        if (item.IsClubbed != null)
                        {
                            if (item.IsHindi == true)
                            {
                                string[] values = item.CMemNameHindi.Split(',');
                                outInnerXml = "";
                                for (int i = 0; i < values.Length; i++)
                                {
                                    values[i] = values[i].Trim();
                                    outInnerXml += @"<div>
          " + values[i] + @" :

                               </div>
                             ";
                                }

                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>

";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;'>
 क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>

";
                                }



                            }
                            else
                            {
                                string[] values = item.CMemName.Split(',');
                                outInnerXml = "";
                                for (int i = 0; i < values.Length; i++)
                                {
                                    values[i] = values[i].Trim();
                                    outInnerXml += @"<div>
          " + values[i] + @" :

                               </div>
                             ";
                                }
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>
<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
   Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>

";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>
<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;'>
Will the <b>" + item.MinistryName + @"</b> be pleased to state :-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>

";
                                }


                            }




                        }
                        else
                        {

                            if (item.MinisterID == 90 && item.IsHindi == true)
                            {
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>

";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;'>
 क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि:-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>

";
                                }


                            }
                            else if (item.IsHindi == true)
                            {
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>

";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;'>
 क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>

";
                                }

                            }
                            else
                            {
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
   Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>

";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;'>
Will the <b>" + item.MinistryName + @"</b> be pleased to state :-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>

";
                                }

                            }


                        }
                    }


                    if (model.StampTypeHindi == true)
                    {
                        foreach (var item in model.Values)
                        {

                            outXml += @"
 

<table style='width:100%'>
     <tr>
          <td style='text-align:left;font-size:20px;' >
      
<b>" + item.SignaturePlaceLocal + @".</b>
              </td>
<td style='text-align:right;font-size:20px;' >
                      <b>" + item.SignatureNameLocal + @",</b>
              </td>
         
              </tr>
<tr>
          <td style='text-align:left;font-size:20px;' >
           
         <b>दिनांक: " + item.SignatureDateLocal + @".</b>
              </td>
<td style='text-align:right;font-size:20px;' >
       
  <b>" + item.SignatureDesignationsLocal + @"।</b>          
              </td>
         
              </tr>
           </table>
       
                     </body> </html>";

                        }
                    }
                    else
                    {
                        foreach (var item in model.Values)
                        {

                            outXml += @"
 

<table style='width:100%'>
     <tr>
          <td style='text-align:left;font-size:20px;' >
      
<b>" + item.SignaturePlace + @".</b>
              </td>
<td style='text-align:right;font-size:20px;' >
                      <b>" + item.SignatureName + @",</b>
              </td>
         
              </tr>
<tr>
          <td style='text-align:left;font-size:20px;' >
           
         <b>Date: " + item.SignatureDate + @".</b>
              </td>
<td style='text-align:right;font-size:20px;' >
       
  <b>" + item.SignatureDesignations + @".</b>          
              </td>
         
              </tr>
           </table>
       
                     </body> </html>";
                        }
                    }
                }
                MemoryStream output = new MemoryStream();

                PdfConverter pdfConverter = new PdfConverter();

                // set the license key
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";


                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 60;
                pdfConverter.PdfFooterOptions.FooterHeight = 60;

                TextElement footerTextElement = new TextElement(0, 30, "&p;",
                        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }
                string url = "Question" + "/AfterFixation/" + model.AssemblyID + "/" + model.SessionID + "/";
                Id = Id.Replace("/", "-");
                fileName = model.AssemblyID + "_" + model.SessionID + "_U_" + Id + ".pdf";
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=UnstaredQuestionList/" + fileName);

                //string directory = Server.MapPath(model.FilePath + url);
                string directory = model.FilePath + url;
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                //var path = Path.Combine(Server.MapPath(model.FilePath + url), fileName);
                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                //model.FilePath = directory + fileName;
                model.FilePath = url + fileName;

                CurrentSession.FileAccessPath = model.DocFilePath;
                var AccessPath = "/" + url + fileName;

                model = Helper.ExecuteService("Notice", "UnstaredUpdatemSessionDate", model) as tMemberNotice;

                return "Success" + AccessPath;
                //byte[] bytes = System.IO.File.ReadAllBytes(AccessPath);
                //return File(bytes, "application/pdf");
            }

            catch (Exception ex)
            {

                //throw ex;
                return ex.Message;
            }
            finally
            {

            }


        }
        public ActionResult GetPdf(string Id)
        {
#pragma warning disable CS0219 // The variable 'url' is assigned but its value is never used
            string url = "/QuestionList/";
#pragma warning restore CS0219 // The variable 'url' is assigned but its value is never used
            tMemberNotice Attachment = new tMemberNotice();
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            Attachment.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            Attachment.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            string strDate = Id;
            DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
            Attachment.NoticeDate = date;
            if (Id != null)
            {

                Attachment.FilePath = (string)Helper.ExecuteService("Notice", "GetPdfBySDate", Attachment);
                string savedPDF = "";
                string directory = Server.MapPath(Attachment.FilePath);
                //var path = Path.Combine(Server.MapPath("~" + url), Attachment.FilePath);
                savedPDF = Attachment.FilePath;
                //    ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(savedPDF);

                ViewBag.PDFPath = savedPDF;
            }

            return PartialView("_GetPdf");
        }
        public ActionResult Html(string Id)
        {
            tMemberNotice model = new tMemberNotice();

            //objModel.NoticeRecievedDate = date;

            GeneratePdf(Id);
            model.Message = "Question List Generated";
            return PartialView("Index", model);
            // return View(model);
        }

        public string GeneratePdf(string Id)
        {
            try
            {
                string htmlString = "";
                string empty = string.Empty;
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25f, 25f, 30f, 30f);
                tMemberNotice tMemberNotice1 = new tMemberNotice();
                tMemberNotice1.SessionID = (int)Convert.ToInt16(CurrentSession.SessionId);
                tMemberNotice1.AssemblyID = (int)Convert.ToInt16(CurrentSession.AssemblyId);
                DateTime exact = DateTime.ParseExact(Id, "dd/MM/yyyy", (IFormatProvider)null);
                tMemberNotice1.NoticeDate = exact;
                tMemberNotice tMemberNotice2 = (tMemberNotice)Helper.ExecuteService("Notice", "GetAllDetailsForSpdf", (object)tMemberNotice1);
                if (tMemberNotice2.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice2.tQuestionModel)
                    {
                        tMemberNotice2.SessionDateId = questionModelCustom.SessionDateId;
                        tMemberNotice2.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", (object)tMemberNotice2);
                    }
                }
                tMemberNotice tMemberNotice3 = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", (object)tMemberNotice2);
                if (tMemberNotice3.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.tQuestionModel)
                    {
                        htmlString = "<html>                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                        htmlString = htmlString + "<div> \r\n\r\n <style type='text/css'>\r\n@page {\r\n    @bottom-left {\r\n        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n    @bottom-right {\r\n        content: 'Page ' counter(page) ' of ' counter(pages);\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n}\r\n</style>\r\n                    </div>\r\n            \r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;color:#003366;' >\r\n             <b>हिमाचल प्रदेश तेरहवीं विधान सभा</b>\r\n              </td>\r\n              </tr>\r\n      </table>\r\n<br />\r\n<br />\r\n<br />\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n            <b>(" + tMemberNotice3.SessionName + ")</b> \r\n              </td>\r\n              </tr>\r\n           </table>\r\n<br />\r\n<br />\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n            <b>मौखिक उत्तर हेतु प्रश्न</b>          \r\n              </td>    \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;color:#003366;' >\r\n          <b>" + questionModelCustom.SessionDateLocal + "</b>   \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n            <b>[" + tMemberNotice3.MinisterName + "]. </b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           <b>कुल प्रश्न - " + tMemberNotice3.TotalQCount.ToString() + "</b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----\r\n            \r\n              </td>\r\n              </tr>\r\n           </table>";
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.tQuestionModel)
                    {
                        bool? nullable1 = questionModelCustom.IsClubbed;
                        int? nullable2;
                        if (nullable1.HasValue)
                        {
                            nullable1 = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            if (nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue)
                            {
                                nullable2 = questionModelCustom.MinisterID;
                                int num = 90;
                                if (!(nullable2.GetValueOrDefault() == num & nullable2.HasValue))
                                {
                                    string[] strArray1 = questionModelCustom.CMemNameHindi.Split(',');
                                    string str1 = "";
                                    for (int index = 0; index < strArray1.Length; ++index)
                                    {
                                        strArray1[index] = strArray1[index].Trim();
                                        str1 = str1 + "<div>\r\n          " + strArray1[index] + " :\r\n\r\n                               </div>\r\n                             ";
                                    }
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray2 = new string[12];
                                            strArray2[0] = htmlString;
                                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray2[4] = nullable2.ToString();
                                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                                            strArray2[6] = str1;
                                            strArray2[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n क्या <b>";
                                            strArray2[8] = questionModelCustom.MinistryNameLocal;
                                            strArray2[9] = "</b> बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray2[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray2[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                            htmlString = string.Concat(strArray2);
                                            continue;
                                        }
                                    }
                                    string str2 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray3 = new string[12];
                                    strArray3[0] = htmlString;
                                    strArray3[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray3[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray3[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray3[4] = nullable2.ToString();
                                    strArray3[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                                    strArray3[6] = str1;
                                    strArray3[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray3[8] = questionModelCustom.MinistryNameLocal;
                                    strArray3[9] = "</b> बतलाने की कृपा करेंगे कि ";
                                    strArray3[10] = str2;
                                    strArray3[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    htmlString = string.Concat(strArray3);
                                    continue;
                                }
                            }
                            nullable2 = questionModelCustom.MinisterID;
                            int num1 = 90;
                            if (nullable2.GetValueOrDefault() == num1 & nullable2.HasValue)
                            {
                                nullable1 = questionModelCustom.IsHindi;
                                bool flag2 = true;
                                if (nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue)
                                {
                                    string[] strArray1 = questionModelCustom.CMemNameHindi.Split(',');
                                    string str1 = "";
                                    for (int index = 0; index < strArray1.Length; ++index)
                                    {
                                        strArray1[index] = strArray1[index].Trim();
                                        str1 = str1 + "<div>\r\n          " + strArray1[index] + " :\r\n\r\n                               </div>\r\n                             ";
                                    }
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag3 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag3 & nullable1.HasValue))
                                        {
                                            string[] strArray2 = new string[12];
                                            strArray2[0] = htmlString;
                                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray2[4] = nullable2.ToString();
                                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                                            strArray2[6] = str1;
                                            strArray2[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n क्या <b>";
                                            strArray2[8] = questionModelCustom.MinistryNameLocal;
                                            strArray2[9] = "</b> बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray2[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray2[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                            htmlString = string.Concat(strArray2);
                                            continue;
                                        }
                                    }
                                    string str2 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray3 = new string[12];
                                    strArray3[0] = htmlString;
                                    strArray3[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray3[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray3[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray3[4] = nullable2.ToString();
                                    strArray3[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                                    strArray3[6] = str1;
                                    strArray3[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray3[8] = questionModelCustom.MinistryNameLocal;
                                    strArray3[9] = "</b> बतलाने की कृपा करेंगी कि ";
                                    strArray3[10] = str2;
                                    strArray3[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    htmlString = string.Concat(strArray3);
                                    continue;
                                }
                            }
                            string[] strArray4 = questionModelCustom.CMemName.Split(',');
                            string str3 = "";
                            for (int index = 0; index < strArray4.Length; ++index)
                            {
                                strArray4[index] = strArray4[index].Trim();
                                str3 = str3 + "<div>\r\n          " + strArray4[index] + " : \r\n\r\n                               </div>\r\n                             ";
                            }
                            nullable1 = questionModelCustom.IsQuestionInPart;
                            if (nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsQuestionInPart;
                                bool flag2 = false;
                                if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                {
                                    string[] strArray1 = new string[12];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = str3;
                                    strArray1[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\nWill the <b>";
                                    strArray1[8] = questionModelCustom.MinistryName;
                                    strArray1[9] = "</b> be pleased to state :-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                    strArray1[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                    strArray1[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                            string[] strArray5 = new string[12];
                            strArray5[0] = htmlString;
                            strArray5[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;' >";
                            strArray5[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                            strArray5[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n  <b>*";
                            nullable2 = questionModelCustom.QuestionNumber;
                            strArray5[4] = nullable2.ToString();
                            strArray5[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                            strArray5[6] = str3;
                            strArray5[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  Will the <b>";
                            strArray5[8] = questionModelCustom.MinistryName;
                            strArray5[9] = "</b> be pleased to state  ";
                            strArray5[10] = str4;
                            strArray5[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                            htmlString = string.Concat(strArray5);
                        }
                        else
                        {
                            nullable2 = questionModelCustom.MinisterID;
                            int num = 90;
                            if (nullable2.GetValueOrDefault() == num & nullable2.HasValue)
                            {
                                nullable1 = questionModelCustom.IsHindi;
                                bool flag1 = true;
                                if (nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue)
                                {
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray = new string[14];
                                            strArray[0] = htmlString;
                                            strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;'>";
                                            strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n</table>\r\n\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray[4] = nullable2.ToString();
                                            strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                                            strArray[6] = questionModelCustom.MemberNameLocal;
                                            strArray[7] = " (";
                                            strArray[8] = questionModelCustom.ConstituencyName_Local;
                                            strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\nक्या <b>";
                                            strArray[10] = questionModelCustom.MinistryNameLocal;
                                            strArray[11] = "</b> बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n           \r\n  \r\n";
                                            htmlString = string.Concat(strArray);
                                            continue;
                                        }
                                    }
                                    string str = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray1 = new string[14];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;'>";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n</table>\r\n\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = questionModelCustom.MemberNameLocal;
                                    strArray1[7] = " (";
                                    strArray1[8] = questionModelCustom.ConstituencyName_Local;
                                    strArray1[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray1[10] = questionModelCustom.MinistryNameLocal;
                                    strArray1[11] = "</b> बतलाने की कृपा करेंगी कि ";
                                    strArray1[12] = str;
                                    strArray1[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n           \r\n  \r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            nullable1 = questionModelCustom.IsHindi;
                            bool flag3 = true;
                            if (nullable1.GetValueOrDefault() == flag3 & nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsClubbed;
                                bool flag1 = true;
                                if (!(nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue))
                                {
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray = new string[14];
                                            strArray[0] = htmlString;
                                            strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray[4] = nullable2.ToString();
                                            strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                                            strArray[6] = questionModelCustom.MemberNameLocal;
                                            strArray[7] = " (";
                                            strArray[8] = questionModelCustom.ConstituencyName_Local;
                                            strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\nक्या <b>";
                                            strArray[10] = questionModelCustom.MinistryNameLocal;
                                            strArray[11] = "</b> बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n\r\n";
                                            htmlString = string.Concat(strArray);
                                            continue;
                                        }
                                    }
                                    string str = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray1 = new string[14];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = questionModelCustom.MemberNameLocal;
                                    strArray1[7] = " (";
                                    strArray1[8] = questionModelCustom.ConstituencyName_Local;
                                    strArray1[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray1[10] = questionModelCustom.MinistryNameLocal;
                                    strArray1[11] = "</b> बतलाने की कृपा करेंगे कि ";
                                    strArray1[12] = str;
                                    strArray1[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n\r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            nullable1 = questionModelCustom.IsQuestionInPart;
                            if (nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsQuestionInPart;
                                bool flag1 = false;
                                if (!(nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue))
                                {
                                    string[] strArray = new string[14];
                                    strArray[0] = htmlString;
                                    strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n<b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray[4] = nullable2.ToString();
                                    strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                                    strArray[6] = questionModelCustom.MemberName;
                                    strArray[7] = " (";
                                    strArray[8] = questionModelCustom.ConstituencyName;
                                    strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\nWill the <b>";
                                    strArray[10] = questionModelCustom.MinistryName;
                                    strArray[11] = "</b> be pleased to state :-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                    strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                    strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n \r\n\r\n";
                                    htmlString = string.Concat(strArray);
                                    continue;
                                }
                            }
                            string str1 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                            string[] strArray2 = new string[14];
                            strArray2[0] = htmlString;
                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#003366;padding-bottom: 20px;font-weight: bold;' >";
                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#003366;font-weight: bold;'>\r\n<b>*";
                            nullable2 = questionModelCustom.QuestionNumber;
                            strArray2[4] = nullable2.ToString();
                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#003366;padding-bottom: 13px;'>\r\n<b>";
                            strArray2[6] = questionModelCustom.MemberName;
                            strArray2[7] = " (";
                            strArray2[8] = questionModelCustom.ConstituencyName;
                            strArray2[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#003366;'>\r\n <p style='text-align:justify;'>\r\n  Will the <b>";
                            strArray2[10] = questionModelCustom.MinistryName;
                            strArray2[11] = "</b> be pleased to state  ";
                            strArray2[12] = str1;
                            strArray2[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#003366;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n \r\n\r\n";
                            htmlString = string.Concat(strArray2);
                        }
                    }
                    bool? stampTypeHindi = tMemberNotice3.StampTypeHindi;
                    bool flag = true;
                    if (stampTypeHindi.GetValueOrDefault() == flag & stampTypeHindi.HasValue)
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                            htmlString = htmlString + "\r\n \r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:left;font-size:20px;color:#003366;' >\r\n      \r\n<b>" + questionModelCustom.SignaturePlaceLocal + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#003366;' >\r\n                      <b>" + questionModelCustom.SignatureNameLocal + ",</b>\r\n              </td>\r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;color:#003366;' >\r\n           \r\n         <b>दिनांक: " + questionModelCustom.SignatureDateLocal + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#003366;' >\r\n       \r\n  <b>" + questionModelCustom.SignatureDesignationsLocal + "।</b>          \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n       \r\n                     </body> </html>";
                    }
                    else
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                            htmlString = htmlString + "\r\n \r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:left;font-size:20px;color:#003366;' >\r\n      \r\n<b>" + questionModelCustom.SignaturePlace + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#003366;' >\r\n                      <b>" + questionModelCustom.SignatureName + ",</b>\r\n              </td>\r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;color:#003366;' >\r\n           \r\n         <b>Date: " + questionModelCustom.SignatureDate + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#003366;' >\r\n       \r\n  <b>" + questionModelCustom.SignatureDesignations + ".</b>          \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n       \r\n                     </body> </html>";
                    }
                }
                MemoryStream memoryStream = new MemoryStream();
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;
                pdfConverter.PdfHeaderOptions.HeaderHeight = 60f;
                pdfConverter.PdfFooterOptions.FooterHeight = 60f;
                TextElement textElement = new TextElement(0.0f, 30f, "&p;", new System.Drawing.Font(new System.Drawing.FontFamily("Times New Roman"), 10f, GraphicsUnit.Point));
                textElement.ForeColor = (PdfColor)Color.MidnightBlue;
                textElement.TextAlign = HorizontalTextAlign.Center;
                pdfConverter.PdfFooterOptions.AddElement((PageElement)textElement);
                EvoPdf.Document objectFromHtmlString = pdfConverter.GetPdfDocumentObjectFromHtmlString(htmlString);
                byte[] buffer = (byte[])null;
                try
                {
                    buffer = objectFromHtmlString.Save();
                }
                finally
                {
                    objectFromHtmlString.Close();
                }
                string str5 = "Question/AfterFixation/" + tMemberNotice3.AssemblyID.ToString() + "/" + tMemberNotice3.SessionID.ToString() + "/";
                Id = Id.Replace("/", "-");
                string path2 = tMemberNotice3.AssemblyID.ToString() + "_" + tMemberNotice3.SessionID.ToString() + "_S_" + Id + ".pdf";
                this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                string str6 = tMemberNotice3.FilePath + str5;
                if (!Directory.Exists(str6))
                    Directory.CreateDirectory(str6);
                FileStream fileStream = new FileStream(Path.Combine(str6, path2), FileMode.Create, FileAccess.Write);
                fileStream.Write(buffer, 0, buffer.Length);
                fileStream.Close();
                tMemberNotice3.FilePath = str5 + path2;
                CurrentSession.FileAccessPath = tMemberNotice3.DocFilePath;
                string str7 = "/" + str5 + path2;
                tMemberNotice tMemberNotice4 = Helper.ExecuteService("Notice", "UpdatemSessionDate", (object)tMemberNotice3) as tMemberNotice;
                return "Success" + str7;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        //Code Already exist
        public string S_GeneratePdfByDate_ByView(string Id, string ViewBy)
        {
            try
            {
                string htmlString = "";
                string path2 = "";
                string empty = string.Empty;
                string str1 = "";
                string str2 = "";
                if (ViewBy == "CQ")
                {
                    str1 = "मौखिक उत्तर हेतु प्रश्न";
                    str2 = "Questions For Oral Answer";
                }
                else if (ViewBy == "PQ")
                {
                    str1 = "मौखिक उत्तर हेतु स्थगित प्रश्न";
                    str2 = "Postponed Questions For Oral Answer";
                }
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25f, 25f, 30f, 30f);
                tMemberNotice tMemberNotice1 = new tMemberNotice();
                tMemberNotice1.SessionID = (int)Convert.ToInt16(CurrentSession.SessionId);
                tMemberNotice1.AssemblyID = (int)Convert.ToInt16(CurrentSession.AssemblyId);
                DateTime exact = DateTime.ParseExact(Id, "dd/MM/yyyy", (IFormatProvider)null);
                tMemberNotice1.NoticeDate = exact;
                tMemberNotice1.DataStatus = ViewBy;
                tMemberNotice tMemberNotice2 = (tMemberNotice)Helper.ExecuteService("Notice", "GetAllDetailsForSpdf", (object)tMemberNotice1);
                if (tMemberNotice2.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice2.tQuestionModel)
                    {
                        tMemberNotice2.SessionDateId = questionModelCustom.SessionDateId;
                        tMemberNotice2.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", (object)tMemberNotice2);
                        tMemberNotice2.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", (object)tMemberNotice2);
                    }
                }
                tMemberNotice tMemberNotice3 = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", (object)tMemberNotice2);
                foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                    tMemberNotice3.SesNameEnglish = questionModelCustom.SesNameEnglish;
                if (tMemberNotice3.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.tQuestionModel)
                    {
                        bool? headerHindi = tMemberNotice3.HeaderHindi;
                        bool flag = true;
                        if (headerHindi.GetValueOrDefault() == flag & headerHindi.HasValue)
                        {
                            htmlString = "<html>                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            htmlString = htmlString + "<div> \r\n\r\n <style type='text/css'>\r\n@page {\r\n    @bottom-left {\r\n        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n    @bottom-right {\r\n        content: 'Page ' counter(page) ' of ' counter(pages);\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n}\r\n</style>\r\n                    </div>\r\n            \r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;color:#3621E8;' >\r\n             <b>हिo प्रo नगर निगम शिमला</b>\r\n              </td>\r\n              </tr>\r\n      </table>\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n            <b>(" + tMemberNotice3.AssesmblyName + ")</b> \r\n              </td>\r\n              </tr>\r\n           </table>\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n            <b>(" + tMemberNotice3.SessionName + ")</b> \r\n              </td>\r\n              </tr>\r\n           </table>\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;'>\r\n            <b>" + str1 + "</b>          \r\n              </td>    \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;color:#3621E8;' >\r\n          <b>" + questionModelCustom.SessionDateLocal + "</b>   \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n            <b>[" + tMemberNotice3.MinisterName + "]. </b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           <b>कुल प्रश्न - " + tMemberNotice3.TotalQCount.ToString() + "</b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----\r\n            \r\n              </td>\r\n              </tr>\r\n           </table>";
                        }
                        else
                        {
                            htmlString = "<html>                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            htmlString = htmlString + "<div> \r\n\r\n <style type='text/css'>\r\n@page {\r\n    @bottom-left {\r\n        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n    @bottom-right {\r\n        content: 'Page ' counter(page) ' of ' counter(pages);\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n}\r\n</style>\r\n                    </div>\r\n\r\n            \r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;color:#3621E8;' >\r\n             <b> MUNCIPAL CORPORATION " + tMemberNotice3.AssesmblyName + " </b>\r\n              </td>\r\n              </tr>\r\n      </table>\r\n<br />\r\n<br />\r\n\r\n<table style='width:100%'>\r\n   <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n            <b>(" + tMemberNotice3.SesNameEnglish + ")</b> \r\n              </td>\r\n              </tr>\r\n           </table>\r\n<br />\r\n<br />\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;'>\r\n            <b>" + str2 + "</b>         \r\n              </td>    \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;color:#3621E8;' >\r\n          <b>" + questionModelCustom.SessionDateEnglish + "</b>   \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:justify;font-size:20px;color:#3621E8;' >\r\n            <b>[" + tMemberNotice3.MinisterNameEnglish + "]. </b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           <b>Total No. of Questions - " + tMemberNotice3.TotalQCount.ToString() + "</b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----\r\n            \r\n              </td>\r\n              </tr>\r\n           </table>";
                        }
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.tQuestionModel)
                    {
                        bool? nullable1 = questionModelCustom.IsClubbed;
                        int? nullable2;
                        if (nullable1.HasValue)
                        {
                            nullable1 = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            if (nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue)
                            {
                                nullable2 = questionModelCustom.MinisterID;
                                int num = 90;
                                if (!(nullable2.GetValueOrDefault() == num & nullable2.HasValue))
                                {
                                    string[] strArray1 = questionModelCustom.CMemNameHindi.Split(',');
                                    string str3 = "";
                                    for (int index = 0; index < strArray1.Length; ++index)
                                    {
                                        strArray1[index] = strArray1[index].Trim();
                                        str3 = str3 + "<div>\r\n          " + strArray1[index] + " :\r\n\r\n                               </div>\r\n                             ";
                                    }
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray2 = new string[12];
                                            strArray2[0] = htmlString;
                                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray2[4] = nullable2.ToString();
                                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                            strArray2[6] = str3;
                                            strArray2[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n क्या <b>";
                                            strArray2[8] = questionModelCustom.MinistryNameLocal;
                                            strArray2[9] = "</b> बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray2[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray2[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                                            htmlString = string.Concat(strArray2);
                                            continue;
                                        }
                                    }
                                    string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray3 = new string[12];
                                    strArray3[0] = htmlString;
                                    strArray3[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray3[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray3[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray3[4] = nullable2.ToString();
                                    strArray3[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray3[6] = str3;
                                    strArray3[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n\r\n  क्या <b>";
                                    strArray3[8] = questionModelCustom.MinistryNameLocal;
                                    strArray3[9] = "</b> बतलाने की कृपा करेंगे कि ";
                                    strArray3[10] = str4;
                                    strArray3[11] = "\r\n\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                                    htmlString = string.Concat(strArray3);
                                    continue;
                                }
                            }
                            nullable2 = questionModelCustom.MinisterID;
                            int num1 = 90;
                            if (nullable2.GetValueOrDefault() == num1 & nullable2.HasValue)
                            {
                                nullable1 = questionModelCustom.IsHindi;
                                bool flag2 = true;
                                if (nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue)
                                {
                                    string[] strArray1 = questionModelCustom.CMemNameHindi.Split(',');
                                    string str3 = "";
                                    for (int index = 0; index < strArray1.Length; ++index)
                                    {
                                        strArray1[index] = strArray1[index].Trim();
                                        str3 = str3 + "<div>\r\n          " + strArray1[index] + " :\r\n\r\n                               </div>\r\n                             ";
                                    }
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag3 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag3 & nullable1.HasValue))
                                        {
                                            string[] strArray2 = new string[12];
                                            strArray2[0] = htmlString;
                                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray2[4] = nullable2.ToString();
                                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                            strArray2[6] = str3;
                                            strArray2[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n क्या <b>";
                                            strArray2[8] = questionModelCustom.MinistryNameLocal;
                                            strArray2[9] = "</b> बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray2[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray2[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                                            htmlString = string.Concat(strArray2);
                                            continue;
                                        }
                                    }
                                    string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray3 = new string[12];
                                    strArray3[0] = htmlString;
                                    strArray3[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray3[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray3[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray3[4] = nullable2.ToString();
                                    strArray3[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray3[6] = str3;
                                    strArray3[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray3[8] = questionModelCustom.MinistryNameLocal;
                                    strArray3[9] = "</b> बतलाने की कृपा करेंगी कि ";
                                    strArray3[10] = str4;
                                    strArray3[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                                    htmlString = string.Concat(strArray3);
                                    continue;
                                }
                            }
                            string[] strArray4 = questionModelCustom.CMemName.Split(',');
                            string str5 = "";
                            for (int index = 0; index < strArray4.Length; ++index)
                            {
                                strArray4[index] = strArray4[index].Trim();
                                str5 = str5 + "<div>\r\n          " + strArray4[index] + " : \r\n\r\n                               </div>\r\n                             ";
                            }
                            nullable1 = questionModelCustom.IsQuestionInPart;
                            if (nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsQuestionInPart;
                                bool flag2 = false;
                                if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                {
                                    string[] strArray1 = new string[12];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = str5;
                                    strArray1[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\nWill the <b>";
                                    strArray1[8] = questionModelCustom.MinistryName;
                                    strArray1[9] = "</b> be pleased to state :-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                    strArray1[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                    strArray1[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            string str6 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                            string[] strArray5 = new string[12];
                            strArray5[0] = htmlString;
                            strArray5[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                            strArray5[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                            strArray5[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                            nullable2 = questionModelCustom.QuestionNumber;
                            strArray5[4] = nullable2.ToString();
                            strArray5[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                            strArray5[6] = str5;
                            strArray5[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  Will the <b>";
                            strArray5[8] = questionModelCustom.MinistryName;
                            strArray5[9] = "</b> be pleased to state  ";
                            strArray5[10] = str6;
                            strArray5[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                            htmlString = string.Concat(strArray5);
                        }
                        else
                        {
                            nullable2 = questionModelCustom.MinisterID;
                            int num = 90;
                            if (nullable2.GetValueOrDefault() == num & nullable2.HasValue)
                            {
                                nullable1 = questionModelCustom.IsHindi;
                                bool flag1 = true;
                                if (nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue)
                                {
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray = new string[14];
                                            strArray[0] = htmlString;
                                            strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;'>";
                                            strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n</table>\r\n\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray[4] = nullable2.ToString();
                                            strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                            strArray[6] = questionModelCustom.MemberNameLocal;
                                            strArray[7] = " (";
                                            strArray[8] = questionModelCustom.ConstituencyName_Local;
                                            strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\nक्या <b>";
                                            strArray[10] = questionModelCustom.MinistryNameLocal;
                                            strArray[11] = "</b> बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n           \r\n  \r\n";
                                            htmlString = string.Concat(strArray);
                                            continue;
                                        }
                                    }
                                    string str3 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray1 = new string[14];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;'>";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n</table>\r\n\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = questionModelCustom.MemberNameLocal;
                                    strArray1[7] = " (";
                                    strArray1[8] = questionModelCustom.ConstituencyName_Local;
                                    strArray1[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray1[10] = questionModelCustom.MinistryNameLocal;
                                    strArray1[11] = "</b> बतलाने की कृपा करेंगी कि ";
                                    strArray1[12] = str3;
                                    strArray1[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n           \r\n  \r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            nullable1 = questionModelCustom.IsHindi;
                            bool flag3 = true;
                            if (nullable1.GetValueOrDefault() == flag3 & nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsClubbed;
                                bool flag1 = true;
                                if (!(nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue))
                                {
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray = new string[14];
                                            strArray[0] = htmlString;
                                            strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray[4] = nullable2.ToString();
                                            strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                            strArray[6] = questionModelCustom.MemberNameLocal;
                                            strArray[7] = " (";
                                            strArray[8] = questionModelCustom.ConstituencyName_Local;
                                            strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\nक्या <b>";
                                            strArray[10] = questionModelCustom.MinistryNameLocal;
                                            strArray[11] = "</b> बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n\r\n\r\n";
                                            htmlString = string.Concat(strArray);
                                            continue;
                                        }
                                    }
                                    string str3 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray1 = new string[14];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = questionModelCustom.MemberNameLocal;
                                    strArray1[7] = " (";
                                    strArray1[8] = questionModelCustom.ConstituencyName_Local;
                                    strArray1[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray1[10] = questionModelCustom.MinistryNameLocal;
                                    strArray1[11] = "</b> बतलाने की कृपा करेंगे कि ";
                                    strArray1[12] = str3;
                                    strArray1[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n\r\n\r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            nullable1 = questionModelCustom.IsQuestionInPart;
                            if (nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsQuestionInPart;
                                bool flag1 = false;
                                if (!(nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue))
                                {
                                    string[] strArray = new string[14];
                                    strArray[0] = htmlString;
                                    strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n<b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray[4] = nullable2.ToString();
                                    strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray[6] = questionModelCustom.MemberName;
                                    strArray[7] = " (";
                                    strArray[8] = questionModelCustom.ConstituencyName;
                                    strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\nWill the <b>";
                                    strArray[10] = questionModelCustom.MinistryName;
                                    strArray[11] = "</b> be pleased to state :-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                    strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                    strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n \r\n\r\n";
                                    htmlString = string.Concat(strArray);
                                    continue;
                                }
                            }
                            string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                            string[] strArray2 = new string[14];
                            strArray2[0] = htmlString;
                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n<b>*";
                            nullable2 = questionModelCustom.QuestionNumber;
                            strArray2[4] = nullable2.ToString();
                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                            strArray2[6] = questionModelCustom.MemberName;
                            strArray2[7] = " (";
                            strArray2[8] = questionModelCustom.ConstituencyName;
                            strArray2[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  Will the <b>";
                            strArray2[10] = questionModelCustom.MinistryName;
                            strArray2[11] = "</b> be pleased to state  ";
                            strArray2[12] = str4;
                            strArray2[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n \r\n\r\n";
                            htmlString = string.Concat(strArray2);
                        }
                    }
                    bool? stampTypeHindi = tMemberNotice3.StampTypeHindi;
                    bool flag4 = true;
                    if (stampTypeHindi.GetValueOrDefault() == flag4 & stampTypeHindi.HasValue)
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                            htmlString = htmlString + "\r\n \r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:left;font-size:20px;color:#3621E8;' >\r\n      \r\n<b>" + questionModelCustom.SignaturePlaceLocal + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#3621E8;' >\r\n                      <b>" + questionModelCustom.SignatureNameLocal + ",</b>\r\n              </td>\r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;color:#3621E8;' >\r\n           \r\n         <b>दिनांक: " + questionModelCustom.SignatureDateLocal + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#3621E8;' >\r\n       \r\n  <b>" + questionModelCustom.SignatureDesignationsLocal + "।</b>          \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n       \r\n                     </body> </html>";
                    }
                    else
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                            htmlString = htmlString + "\r\n \r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:left;font-size:20px;color:#3621E8;' >\r\n      \r\n<b>" + questionModelCustom.SignaturePlace + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#3621E8;' >\r\n                      <b>" + questionModelCustom.SignatureName + ",</b>\r\n              </td>\r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;color:#3621E8;' >\r\n           \r\n         <b>Dated: " + questionModelCustom.SignatureDate + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#3621E8;' >\r\n       \r\n  <b>" + questionModelCustom.SignatureDesignations + ".</b>          \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n       \r\n                     </body> </html>";
                    }
                }
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;
                pdfConverter.PdfHeaderOptions.HeaderHeight = 75f;
                pdfConverter.PdfFooterOptions.FooterHeight = 60f;
                TextElement textElement = new TextElement(0.0f, 30f, "&p;", new System.Drawing.Font(new System.Drawing.FontFamily("Times New Roman"), 10f, GraphicsUnit.Point));
                textElement.ForeColor = (PdfColor)Color.MidnightBlue;
                textElement.TextAlign = HorizontalTextAlign.Center;
                pdfConverter.PdfFooterOptions.AddElement((PageElement)textElement);
                string path1 = Path.Combine(this.Server.MapPath("~"), "Images");
                ImageElement imageElement1 = new ImageElement(250f, 10f, Path.Combine(path1, "notapproved.png"));
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                pdfConverter.PdfHeaderOptions.AddElement((PageElement)imageElement1);
                ImageElement imageElement2 = new ImageElement(0.0f, 0.0f, Path.Combine(path1, "notapproved.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 15;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388f;
                EvoPdf.Document objectFromHtmlString = pdfConverter.GetPdfDocumentObjectFromHtmlString(htmlString);
                float width = float.Parse("400");
                float height = float.Parse("400");
                RectangleF bounds = new RectangleF((float)(((double)objectFromHtmlString.Pages[0].ClientRectangle.Width - (double)width) / 2.0), 150f, width, height);
                objectFromHtmlString.AddTemplate(bounds).AddElement((PageElement)imageElement2);
                byte[] buffer = (byte[])null;
                try
                {
                    buffer = objectFromHtmlString.Save();
                }
                finally
                {
                    objectFromHtmlString.Close();
                }
                string[] strArray6 = new string[5]
                {
          "Question/AfterFixation/",
          null,
          null,
          null,
          null
                };
                int num2 = tMemberNotice3.AssemblyID;
                strArray6[1] = num2.ToString();
                strArray6[2] = "/";
                num2 = tMemberNotice3.SessionID;
                strArray6[3] = num2.ToString();
                strArray6[4] = "/";
                string str7 = string.Concat(strArray6);
                Id = Id.Replace("/", "-");
                if (ViewBy == "CQ")
                {
                    string[] strArray1 = new string[6];
                    num2 = tMemberNotice3.AssemblyID;
                    strArray1[0] = num2.ToString();
                    strArray1[1] = "_";
                    num2 = tMemberNotice3.SessionID;
                    strArray1[2] = num2.ToString();
                    strArray1[3] = "_S_";
                    strArray1[4] = Id;
                    strArray1[5] = ".pdf";
                    path2 = string.Concat(strArray1);
                }
                else if (ViewBy == "PQ")
                {
                    string[] strArray1 = new string[6];
                    num2 = tMemberNotice3.AssemblyID;
                    strArray1[0] = num2.ToString();
                    strArray1[1] = "_";
                    num2 = tMemberNotice3.SessionID;
                    strArray1[2] = num2.ToString();
                    strArray1[3] = "_S_PQ_";
                    strArray1[4] = Id;
                    strArray1[5] = ".pdf";
                    path2 = string.Concat(strArray1);
                }
                this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                string str8 = tMemberNotice3.FilePath + str7;
                if (!Directory.Exists(str8))
                    Directory.CreateDirectory(str8);
                FileStream fileStream = new FileStream(Path.Combine(str8, path2), FileMode.Create, FileAccess.Write);
                fileStream.Write(buffer, 0, buffer.Length);
                fileStream.Close();
                tMemberNotice3.FilePath = str7 + path2;
                CurrentSession.FileAccessPath = tMemberNotice3.DocFilePath;
                string str9 = "/" + str7 + path2;
                tMemberNotice tMemberNotice4 = Helper.ExecuteService("Notice", "UpdatemSessionDate", (object)tMemberNotice3) as tMemberNotice;
                return "Success" + str9;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //Code Mergerd
        public string S_GeneratePdfByDate_ByViewForSec(string Id, string ViewBy)
        {
            try
            {
                string htmlString = "";
                string path2 = "";
                string empty = string.Empty;
                string str1 = "";
                string str2 = "";
                if (ViewBy == "CQ")
                {
                    str1 = "मौखिक उत्तर हेतु प्रश्न";
                    str2 = "Questions For Oral Answer";
                }
                else if (ViewBy == "PQ")
                {
                    str1 = "मौखिक उत्तर हेतु स्थगित प्रश्न";
                    str2 = "Postponed Questions For Oral Answer";
                }
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25f, 25f, 30f, 30f);
                tMemberNotice tMemberNotice1 = new tMemberNotice();
                tMemberNotice1.SessionID = (int)Convert.ToInt16(CurrentSession.SessionId);
                tMemberNotice1.AssemblyID = (int)Convert.ToInt16(CurrentSession.AssemblyId);
                DateTime exact = DateTime.ParseExact(Id, "dd/MM/yyyy", (IFormatProvider)null);
                tMemberNotice1.NoticeDate = exact;
                tMemberNotice1.DataStatus = ViewBy;
                tMemberNotice tMemberNotice2 = (tMemberNotice)Helper.ExecuteService("Notice", "GetAllDetailsForSpdf", (object)tMemberNotice1);
                if (tMemberNotice2.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice2.tQuestionModel)
                    {
                        tMemberNotice2.SessionDateId = questionModelCustom.SessionDateId;
                        tMemberNotice2.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", (object)tMemberNotice2);
                        tMemberNotice2.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", (object)tMemberNotice2);
                    }
                }
                tMemberNotice tMemberNotice3 = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", (object)tMemberNotice2);
                foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                    tMemberNotice3.SesNameEnglish = questionModelCustom.SesNameEnglish;
                if (tMemberNotice3.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.tQuestionModel)
                    {
                        bool? headerHindi = tMemberNotice3.HeaderHindi;
                        bool flag = true;
                        if (headerHindi.GetValueOrDefault() == flag & headerHindi.HasValue)
                        {
                            htmlString = "<html>                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            htmlString = htmlString + "<div> \r\n\r\n <style type='text/css'>\r\n@page {\r\n    @bottom-left {\r\n        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n    @bottom-right {\r\n        content: 'Page ' counter(page) ' of ' counter(pages);\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n}\r\n</style>\r\n                    </div>\r\n            \r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;color:#3621E8;' >\r\n             <b>हिo प्रo नगर निगम शिमला</b>\r\n              </td>\r\n              </tr>\r\n      </table>\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n            <b>(" + tMemberNotice3.AssesmblyName + ")</b> \r\n              </td>\r\n              </tr>\r\n           </table>\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n            <b>(" + tMemberNotice3.SessionName + ")</b> \r\n              </td>\r\n              </tr>\r\n           </table>\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;'>\r\n            <b>" + str1 + "</b>          \r\n              </td>    \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;color:#3621E8;' >\r\n          <b>" + questionModelCustom.SessionDateLocal + "</b>   \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n            <b>[" + tMemberNotice3.MinisterName + "]. </b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           <b>कुल प्रश्न - " + tMemberNotice3.TotalQCount.ToString() + "</b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----\r\n            \r\n              </td>\r\n              </tr>\r\n           </table>";
                        }
                        else
                        {
                            htmlString = "<html>                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            htmlString = htmlString + "<div> \r\n\r\n <style type='text/css'>\r\n@page {\r\n    @bottom-left {\r\n        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n    @bottom-right {\r\n        content: 'Page ' counter(page) ' of ' counter(pages);\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n}\r\n</style>\r\n                    </div>\r\n\r\n            \r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;color:#3621E8;' >\r\n             <b> MUNCIPAL CORPORATION " + tMemberNotice3.AssesmblyName + " </b>\r\n              </td>\r\n              </tr>\r\n      </table>\r\n<br />\r\n<br />\r\n\r\n<table style='width:100%'>\r\n   <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n            <b>(" + tMemberNotice3.SesNameEnglish + ")</b> \r\n              </td>\r\n              </tr>\r\n           </table>\r\n<br />\r\n<br />\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;'>\r\n            <b>" + str2 + "</b>         \r\n              </td>    \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;color:#3621E8;' >\r\n          <b>" + questionModelCustom.SessionDateEnglish + "</b>   \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:justify;font-size:20px;color:#3621E8;' >\r\n            <b>[" + tMemberNotice3.MinisterNameEnglish + "]. </b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           <b>Total No. of Questions - " + tMemberNotice3.TotalQCount.ToString() + "</b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----\r\n            \r\n              </td>\r\n              </tr>\r\n           </table>";
                        }
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.tQuestionModel)
                    {
                        bool? nullable1 = questionModelCustom.IsClubbed;
                        int? nullable2;
                        if (nullable1.HasValue)
                        {
                            nullable1 = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            if (nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue)
                            {
                                nullable2 = questionModelCustom.MinisterID;
                                int num = 90;
                                if (!(nullable2.GetValueOrDefault() == num & nullable2.HasValue))
                                {
                                    string[] strArray1 = questionModelCustom.CMemNameHindi.Split(',');
                                    string str3 = "";
                                    for (int index = 0; index < strArray1.Length; ++index)
                                    {
                                        strArray1[index] = strArray1[index].Trim();
                                        str3 = str3 + "<div>\r\n          " + strArray1[index] + " :\r\n\r\n                               </div>\r\n                             ";
                                    }
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray2 = new string[12];
                                            strArray2[0] = htmlString;
                                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray2[4] = nullable2.ToString();
                                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                            strArray2[6] = str3;
                                            strArray2[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n क्या <b>";
                                            strArray2[8] = questionModelCustom.MinistryNameLocal;
                                            strArray2[9] = "</b> बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray2[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray2[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                                            htmlString = string.Concat(strArray2);
                                            continue;
                                        }
                                    }
                                    string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray3 = new string[12];
                                    strArray3[0] = htmlString;
                                    strArray3[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray3[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray3[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray3[4] = nullable2.ToString();
                                    strArray3[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray3[6] = str3;
                                    strArray3[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray3[8] = questionModelCustom.MinistryNameLocal;
                                    strArray3[9] = "</b> बतलाने की कृपा करेंगे कि ";
                                    strArray3[10] = str4;
                                    strArray3[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                                    htmlString = string.Concat(strArray3);
                                    continue;
                                }
                            }
                            nullable2 = questionModelCustom.MinisterID;
                            int num1 = 90;
                            if (nullable2.GetValueOrDefault() == num1 & nullable2.HasValue)
                            {
                                nullable1 = questionModelCustom.IsHindi;
                                bool flag2 = true;
                                if (nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue)
                                {
                                    string[] strArray1 = questionModelCustom.CMemNameHindi.Split(',');
                                    string str3 = "";
                                    for (int index = 0; index < strArray1.Length; ++index)
                                    {
                                        strArray1[index] = strArray1[index].Trim();
                                        str3 = str3 + "<div>\r\n          " + strArray1[index] + " :\r\n\r\n                               </div>\r\n                             ";
                                    }
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag3 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag3 & nullable1.HasValue))
                                        {
                                            string[] strArray2 = new string[12];
                                            strArray2[0] = htmlString;
                                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray2[4] = nullable2.ToString();
                                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                            strArray2[6] = str3;
                                            strArray2[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n क्या <b>";
                                            strArray2[8] = questionModelCustom.MinistryNameLocal;
                                            strArray2[9] = "</b> बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray2[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray2[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                                            htmlString = string.Concat(strArray2);
                                            continue;
                                        }
                                    }
                                    string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray3 = new string[12];
                                    strArray3[0] = htmlString;
                                    strArray3[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray3[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray3[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray3[4] = nullable2.ToString();
                                    strArray3[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray3[6] = str3;
                                    strArray3[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray3[8] = questionModelCustom.MinistryNameLocal;
                                    strArray3[9] = "</b> बतलाने की कृपा करेंगी कि ";
                                    strArray3[10] = str4;
                                    strArray3[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                                    htmlString = string.Concat(strArray3);
                                    continue;
                                }
                            }
                            string[] strArray4 = questionModelCustom.CMemName.Split(',');
                            string str5 = "";
                            for (int index = 0; index < strArray4.Length; ++index)
                            {
                                strArray4[index] = strArray4[index].Trim();
                                str5 = str5 + "<div>\r\n          " + strArray4[index] + " : \r\n\r\n                               </div>\r\n                             ";
                            }
                            nullable1 = questionModelCustom.IsQuestionInPart;
                            if (nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsQuestionInPart;
                                bool flag2 = false;
                                if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                {
                                    string[] strArray1 = new string[12];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = str5;
                                    strArray1[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\nWill the <b>";
                                    strArray1[8] = questionModelCustom.MinistryName;
                                    strArray1[9] = "</b> be pleased to state :-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                    strArray1[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                    strArray1[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            string str6 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                            string[] strArray5 = new string[12];
                            strArray5[0] = htmlString;
                            strArray5[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                            strArray5[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                            strArray5[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                            nullable2 = questionModelCustom.QuestionNumber;
                            strArray5[4] = nullable2.ToString();
                            strArray5[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                            strArray5[6] = str5;
                            strArray5[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  Will the <b>";
                            strArray5[8] = questionModelCustom.MinistryName;
                            strArray5[9] = "</b> be pleased to state  ";
                            strArray5[10] = str6;
                            strArray5[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n";
                            htmlString = string.Concat(strArray5);
                        }
                        else
                        {
                            nullable2 = questionModelCustom.MinisterID;
                            int num = 90;
                            if (nullable2.GetValueOrDefault() == num & nullable2.HasValue)
                            {
                                nullable1 = questionModelCustom.IsHindi;
                                bool flag1 = true;
                                if (nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue)
                                {
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray = new string[14];
                                            strArray[0] = htmlString;
                                            strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;'>";
                                            strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n</table>\r\n\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray[4] = nullable2.ToString();
                                            strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                            strArray[6] = questionModelCustom.MemberNameLocal;
                                            strArray[7] = " (";
                                            strArray[8] = questionModelCustom.ConstituencyName_Local;
                                            strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\nक्या <b>";
                                            strArray[10] = questionModelCustom.MinistryNameLocal;
                                            strArray[11] = "</b> बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n           \r\n  \r\n";
                                            htmlString = string.Concat(strArray);
                                            continue;
                                        }
                                    }
                                    string str3 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray1 = new string[14];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;'>";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n</table>\r\n\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = questionModelCustom.MemberNameLocal;
                                    strArray1[7] = " (";
                                    strArray1[8] = questionModelCustom.ConstituencyName_Local;
                                    strArray1[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray1[10] = questionModelCustom.MinistryNameLocal;
                                    strArray1[11] = "</b> बतलाने की कृपा करेंगी कि ";
                                    strArray1[12] = str3;
                                    strArray1[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n           \r\n  \r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            nullable1 = questionModelCustom.IsHindi;
                            bool flag3 = true;
                            if (nullable1.GetValueOrDefault() == flag3 & nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsClubbed;
                                bool flag1 = true;
                                if (!(nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue))
                                {
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray = new string[14];
                                            strArray[0] = htmlString;
                                            strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray[4] = nullable2.ToString();
                                            strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                            strArray[6] = questionModelCustom.MemberNameLocal;
                                            strArray[7] = " (";
                                            strArray[8] = questionModelCustom.ConstituencyName_Local;
                                            strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\nक्या <b>";
                                            strArray[10] = questionModelCustom.MinistryNameLocal;
                                            strArray[11] = "</b> बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n\r\n\r\n";
                                            htmlString = string.Concat(strArray);
                                            continue;
                                        }
                                    }
                                    string str3 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray1 = new string[14];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n  <b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = questionModelCustom.MemberNameLocal;
                                    strArray1[7] = " (";
                                    strArray1[8] = questionModelCustom.ConstituencyName_Local;
                                    strArray1[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray1[10] = questionModelCustom.MinistryNameLocal;
                                    strArray1[11] = "</b> बतलाने की कृपा करेंगे कि ";
                                    strArray1[12] = str3;
                                    strArray1[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n\r\n\r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            nullable1 = questionModelCustom.IsQuestionInPart;
                            if (nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsQuestionInPart;
                                bool flag1 = false;
                                if (!(nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue))
                                {
                                    string[] strArray = new string[14];
                                    strArray[0] = htmlString;
                                    strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n<b>*";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray[4] = nullable2.ToString();
                                    strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                                    strArray[6] = questionModelCustom.MemberName;
                                    strArray[7] = " (";
                                    strArray[8] = questionModelCustom.ConstituencyName;
                                    strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\nWill the <b>";
                                    strArray[10] = questionModelCustom.MinistryName;
                                    strArray[11] = "</b> be pleased to state :-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                    strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                    strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n \r\n\r\n";
                                    htmlString = string.Concat(strArray);
                                    continue;
                                }
                            }
                            string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                            string[] strArray2 = new string[14];
                            strArray2[0] = htmlString;
                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >";
                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>\r\n<b>*";
                            nullable2 = questionModelCustom.QuestionNumber;
                            strArray2[4] = nullable2.ToString();
                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>\r\n<b>";
                            strArray2[6] = questionModelCustom.MemberName;
                            strArray2[7] = " (";
                            strArray2[8] = questionModelCustom.ConstituencyName;
                            strArray2[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;color:#3621E8;'>\r\n <p style='text-align:justify;'>\r\n  Will the <b>";
                            strArray2[10] = questionModelCustom.MinistryName;
                            strArray2[11] = "</b> be pleased to state  ";
                            strArray2[12] = str4;
                            strArray2[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;color:#3621E8;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table></br>\r\n \r\n\r\n";
                            htmlString = string.Concat(strArray2);
                        }
                    }
                    bool? stampTypeHindi = tMemberNotice3.StampTypeHindi;
                    bool flag4 = true;
                    if (stampTypeHindi.GetValueOrDefault() == flag4 & stampTypeHindi.HasValue)
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                            htmlString = htmlString + "\r\n \r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:left;font-size:20px;color:#3621E8;' >\r\n      \r\n<b>" + questionModelCustom.SignaturePlaceLocal + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#3621E8;' >\r\n                      <b>" + questionModelCustom.SignatureNameLocal + ",</b>\r\n              </td>\r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;color:#3621E8;' >\r\n           \r\n         <b>दिनांक: " + questionModelCustom.SignatureDateLocal + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#3621E8;' >\r\n       \r\n  <b>" + questionModelCustom.SignatureDesignationsLocal + "।</b>          \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n       \r\n                     </body> </html>";
                    }
                    else
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                            htmlString = htmlString + "\r\n \r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:left;font-size:20px;color:#3621E8;' >\r\n      \r\n<b>" + questionModelCustom.SignaturePlace + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#3621E8;' >\r\n                      <b>" + questionModelCustom.SignatureName + ",</b>\r\n              </td>\r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;color:#3621E8;' >\r\n           \r\n         <b>Dated: " + questionModelCustom.SignatureDate + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;color:#3621E8;' >\r\n       \r\n  <b>" + questionModelCustom.SignatureDesignations + ".</b>          \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n       \r\n                     </body> </html>";
                    }
                }
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;
                pdfConverter.PdfHeaderOptions.HeaderHeight = 75f;
                pdfConverter.PdfFooterOptions.FooterHeight = 60f;
                TextElement textElement = new TextElement(0.0f, 30f, "&p;", new System.Drawing.Font(new System.Drawing.FontFamily("Times New Roman"), 10f, GraphicsUnit.Point));
                textElement.ForeColor = (PdfColor)Color.MidnightBlue;
                textElement.TextAlign = HorizontalTextAlign.Center;
                pdfConverter.PdfFooterOptions.AddElement((PageElement)textElement);
                string path1 = Path.Combine(this.Server.MapPath("~"), "Images");
                ImageElement imageElement1 = new ImageElement(250f, 10f, Path.Combine(path1, "Blanknotapproved.png"));
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                pdfConverter.PdfHeaderOptions.AddElement((PageElement)imageElement1);
                ImageElement imageElement2 = new ImageElement(0.0f, 0.0f, Path.Combine(path1, "Blanknotapproved.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 15;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388f;
                EvoPdf.Document objectFromHtmlString = pdfConverter.GetPdfDocumentObjectFromHtmlString(htmlString);
                float width = float.Parse("400");
                float height = float.Parse("400");
                RectangleF bounds = new RectangleF((float)(((double)objectFromHtmlString.Pages[0].ClientRectangle.Width - (double)width) / 2.0), 150f, width, height);
                objectFromHtmlString.AddTemplate(bounds).AddElement((PageElement)imageElement2);
                byte[] buffer = (byte[])null;
                try
                {
                    buffer = objectFromHtmlString.Save();
                }
                finally
                {
                    objectFromHtmlString.Close();
                }
                string[] strArray6 = new string[5]
                {
          "Question/AfterFixation/",
          null,
          null,
          null,
          null
                };
                int num2 = tMemberNotice3.AssemblyID;
                strArray6[1] = num2.ToString();
                strArray6[2] = "/";
                num2 = tMemberNotice3.SessionID;
                strArray6[3] = num2.ToString();
                strArray6[4] = "/";
                string str7 = string.Concat(strArray6);
                Id = Id.Replace("/", "-");
                if (ViewBy == "CQ")
                {
                    string[] strArray1 = new string[6];
                    num2 = tMemberNotice3.AssemblyID;
                    strArray1[0] = num2.ToString();
                    strArray1[1] = "_";
                    num2 = tMemberNotice3.SessionID;
                    strArray1[2] = num2.ToString();
                    strArray1[3] = "_S_";
                    strArray1[4] = Id;
                    strArray1[5] = ".pdf";
                    path2 = string.Concat(strArray1);
                }
                else if (ViewBy == "PQ")
                {
                    string[] strArray1 = new string[6];
                    num2 = tMemberNotice3.AssemblyID;
                    strArray1[0] = num2.ToString();
                    strArray1[1] = "_";
                    num2 = tMemberNotice3.SessionID;
                    strArray1[2] = num2.ToString();
                    strArray1[3] = "_S_PQ_";
                    strArray1[4] = Id;
                    strArray1[5] = ".pdf";
                    path2 = string.Concat(strArray1);
                }
                this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                string str8 = tMemberNotice3.FilePath + str7;
                if (!Directory.Exists(str8))
                    Directory.CreateDirectory(str8);
                FileStream fileStream = new FileStream(Path.Combine(str8, path2), FileMode.Create, FileAccess.Write);
                fileStream.Write(buffer, 0, buffer.Length);
                fileStream.Close();
                tMemberNotice3.FilePath = str7 + path2;
                CurrentSession.FileAccessPath = tMemberNotice3.DocFilePath;
                string str9 = "/" + str7 + path2;
                tMemberNotice tMemberNotice4 = Helper.ExecuteService("Notice", "UpdatemSessionDate", (object)tMemberNotice3) as tMemberNotice;
                return "Success" + str9;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string U_GeneratePdfByDate_ByViewForSecUnstarred(string Id, string ViewBy)
        {
            try
            {
                string htmlString = "";
                string path2 = "";
                string empty = string.Empty;
                string str1 = "";
                string str2 = "";
                if (ViewBy == "CQ")
                {
                    str1 = "लिखित उत्तर हेतु प्रश्न";
                    str2 = "Questions For Written Answers";
                }
                else if (ViewBy == "PQ")
                {
                    str1 = "लिखित उत्तर हेतु स्थगित प्रश्न";
                    str2 = "Postponed Questions For Written Answer";
                }
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25f, 25f, 30f, 30f);
                tMemberNotice tMemberNotice1 = new tMemberNotice();
                tMemberNotice1.SessionID = (int)Convert.ToInt16(CurrentSession.SessionId);
                tMemberNotice1.AssemblyID = (int)Convert.ToInt16(CurrentSession.AssemblyId);
                DateTime exact = DateTime.ParseExact(Id, "dd/MM/yyyy", (IFormatProvider)null);
                tMemberNotice1.NoticeDate = exact;
                tMemberNotice1.DataStatus = ViewBy;
                tMemberNotice tMemberNotice2 = (tMemberNotice)Helper.ExecuteService("Notice", "UnstaredGetAllDetailsForSpdf", (object)tMemberNotice1);
                if (tMemberNotice2.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice2.tQuestionModel)
                    {
                        tMemberNotice2.SessionDateId = questionModelCustom.SessionDateId;
                        tMemberNotice2.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", (object)tMemberNotice2);
                        tMemberNotice2.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", (object)tMemberNotice2);
                    }
                }
                tMemberNotice tMemberNotice3 = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", (object)tMemberNotice2);
                foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                    tMemberNotice3.SesNameEnglish = questionModelCustom.SesNameEnglish;
                if (tMemberNotice3.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.tQuestionModel)
                    {
                        bool? headerHindi = tMemberNotice3.HeaderHindi;
                        bool flag = true;
                        if (headerHindi.GetValueOrDefault() == flag & headerHindi.HasValue)
                        {
                            htmlString = "<html>                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            htmlString = htmlString + "<div> \r\n\r\n <style type='text/css'>\r\n@page {\r\n    @bottom-left {\r\n        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n    @bottom-right {\r\n        content: 'Page ' counter(page) ' of ' counter(pages);\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n}\r\n</style>\r\n                    </div>\r\n            \r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;' >\r\n             <b>हिमाचल प्रदेश तेरहवीं विधान सभा</b>\r\n              </td>\r\n              </tr>\r\n      </table>\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n            <b>(" + tMemberNotice3.SessionName + ")</b> \r\n              </td>\r\n              </tr>\r\n           </table>\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;'>\r\n            <b>" + str1 + "</b>          \r\n              </td>    \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;' >\r\n          <b>" + questionModelCustom.SessionDateLocal + "</b>   \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n            <b>[" + tMemberNotice3.MinisterName + "]. </b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           <b>कुल प्रश्न - " + tMemberNotice3.TotalQCount.ToString() + "</b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----\r\n            \r\n              </td>\r\n              </tr>\r\n           </table>";
                        }
                        else
                        {
                            htmlString = "<html>                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            htmlString = htmlString + "<div> \r\n\r\n <style type='text/css'>\r\n@page {\r\n    @bottom-left {\r\n        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n    @bottom-right {\r\n        content: 'Page ' counter(page) ' of ' counter(pages);\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n}\r\n</style>\r\n                    </div>\r\n\r\n            \r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;' >\r\n             <b>MUNCIPAL CORPORATION " + tMemberNotice3.AssesmblyName + " </b>\r\n              </td>\r\n              </tr>\r\n      </table>\r\n<br />\r\n<br />\r\n\r\n<table style='width:100%'>\r\n   <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n            <b>(" + tMemberNotice3.SesNameEnglish + ")</b> \r\n              </td>\r\n              </tr>\r\n           </table>\r\n<br />\r\n<br />\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;'>\r\n            <b>" + str2 + "</b>         \r\n              </td>    \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;' >\r\n          <b>" + questionModelCustom.SessionDateEnglish + "</b>   \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:justify;font-size:20px;' >\r\n            <b>[" + tMemberNotice3.MinisterNameEnglish + "]. </b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           <b>Total No. of Questions - " + tMemberNotice3.TotalQCount.ToString() + "</b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----\r\n            \r\n              </td>\r\n              </tr>\r\n           </table>";
                        }
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.tQuestionModel)
                    {
                        bool? nullable1 = questionModelCustom.IsClubbed;
                        int? nullable2;
                        if (nullable1.HasValue)
                        {
                            nullable1 = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            if (nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue)
                            {
                                nullable2 = questionModelCustom.MinisterID;
                                int num = 90;
                                if (!(nullable2.GetValueOrDefault() == num & nullable2.HasValue))
                                {
                                    string[] strArray1 = questionModelCustom.CMemNameHindi.Split(',');
                                    string str3 = "";
                                    for (int index = 0; index < strArray1.Length; ++index)
                                    {
                                        strArray1[index] = strArray1[index].Trim();
                                        str3 = str3 + "<div>\r\n          " + strArray1[index] + " :\r\n\r\n                               </div>\r\n                             ";
                                    }
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray2 = new string[12];
                                            strArray2[0] = htmlString;
                                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray2[4] = nullable2.ToString();
                                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                            strArray2[6] = str3;
                                            strArray2[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n क्या <b>";
                                            strArray2[8] = questionModelCustom.MinistryNameLocal;
                                            strArray2[9] = "</b> बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray2[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray2[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                            htmlString = string.Concat(strArray2);
                                            continue;
                                        }
                                    }
                                    string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray3 = new string[12];
                                    strArray3[0] = htmlString;
                                    strArray3[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray3[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray3[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray3[4] = nullable2.ToString();
                                    strArray3[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray3[6] = str3;
                                    strArray3[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray3[8] = questionModelCustom.MinistryNameLocal;
                                    strArray3[9] = "</b> बतलाने की कृपा करेंगे कि ";
                                    strArray3[10] = str4;
                                    strArray3[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    htmlString = string.Concat(strArray3);
                                    continue;
                                }
                            }
                            nullable2 = questionModelCustom.MinisterID;
                            int num1 = 90;
                            if (nullable2.GetValueOrDefault() == num1 & nullable2.HasValue)
                            {
                                nullable1 = questionModelCustom.IsHindi;
                                bool flag2 = true;
                                if (nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue)
                                {
                                    string[] strArray1 = questionModelCustom.CMemNameHindi.Split(',');
                                    string str3 = "";
                                    for (int index = 0; index < strArray1.Length; ++index)
                                    {
                                        strArray1[index] = strArray1[index].Trim();
                                        str3 = str3 + "<div>\r\n          " + strArray1[index] + " :\r\n\r\n                               </div>\r\n                             ";
                                    }
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag3 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag3 & nullable1.HasValue))
                                        {
                                            string[] strArray2 = new string[12];
                                            strArray2[0] = htmlString;
                                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray2[4] = nullable2.ToString();
                                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                            strArray2[6] = str3;
                                            strArray2[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n क्या <b>";
                                            strArray2[8] = questionModelCustom.MinistryNameLocal;
                                            strArray2[9] = "</b> बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray2[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray2[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                            htmlString = string.Concat(strArray2);
                                            continue;
                                        }
                                    }
                                    string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray3 = new string[12];
                                    strArray3[0] = htmlString;
                                    strArray3[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray3[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray3[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray3[4] = nullable2.ToString();
                                    strArray3[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray3[6] = str3;
                                    strArray3[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray3[8] = questionModelCustom.MinistryNameLocal;
                                    strArray3[9] = "</b> बतलाने की कृपा करेंगी कि ";
                                    strArray3[10] = str4;
                                    strArray3[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    htmlString = string.Concat(strArray3);
                                    continue;
                                }
                            }
                            string[] strArray4 = questionModelCustom.CMemName.Split(',');
                            string str5 = "";
                            for (int index = 0; index < strArray4.Length; ++index)
                            {
                                strArray4[index] = strArray4[index].Trim();
                                str5 = str5 + "<div>\r\n          " + strArray4[index] + " : \r\n\r\n                               </div>\r\n                             ";
                            }
                            nullable1 = questionModelCustom.IsQuestionInPart;
                            if (nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsQuestionInPart;
                                bool flag2 = false;
                                if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                {
                                    string[] strArray1 = new string[12];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = str5;
                                    strArray1[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\nWill the <b>";
                                    strArray1[8] = questionModelCustom.MinistryName;
                                    strArray1[9] = "</b> be pleased to state :-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                    strArray1[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                    strArray1[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            string str6 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                            string[] strArray5 = new string[12];
                            strArray5[0] = htmlString;
                            strArray5[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                            strArray5[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                            strArray5[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                            nullable2 = questionModelCustom.QuestionNumber;
                            strArray5[4] = nullable2.ToString();
                            strArray5[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                            strArray5[6] = str5;
                            strArray5[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  Will the <b>";
                            strArray5[8] = questionModelCustom.MinistryName;
                            strArray5[9] = "</b> be pleased to state ";
                            strArray5[10] = str6;
                            strArray5[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                            htmlString = string.Concat(strArray5);
                        }
                        else
                        {
                            nullable2 = questionModelCustom.MinisterID;
                            int num = 90;
                            if (nullable2.GetValueOrDefault() == num & nullable2.HasValue)
                            {
                                nullable1 = questionModelCustom.IsHindi;
                                bool flag1 = true;
                                if (nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue)
                                {
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray = new string[14];
                                            strArray[0] = htmlString;
                                            strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>";
                                            strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n</table>\r\n\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray[4] = nullable2.ToString();
                                            strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                            strArray[6] = questionModelCustom.MemberNameLocal;
                                            strArray[7] = " (";
                                            strArray[8] = questionModelCustom.ConstituencyName_Local;
                                            strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\nक्या <b>";
                                            strArray[10] = questionModelCustom.MinistryNameLocal;
                                            strArray[11] = "</b> बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n           \r\n  \r\n";
                                            htmlString = string.Concat(strArray);
                                            continue;
                                        }
                                    }
                                    string str3 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray1 = new string[14];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n</table>\r\n\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = questionModelCustom.MemberNameLocal;
                                    strArray1[7] = " (";
                                    strArray1[8] = questionModelCustom.ConstituencyName_Local;
                                    strArray1[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray1[10] = questionModelCustom.MinistryNameLocal;
                                    strArray1[11] = "</b> बतलाने की कृपा करेंगी कि ";
                                    strArray1[12] = str3;
                                    strArray1[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n           \r\n  \r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            nullable1 = questionModelCustom.IsHindi;
                            bool flag3 = true;
                            if (nullable1.GetValueOrDefault() == flag3 & nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsClubbed;
                                bool flag1 = true;
                                if (!(nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue))
                                {
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray = new string[14];
                                            strArray[0] = htmlString;
                                            strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray[4] = nullable2.ToString();
                                            strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                            strArray[6] = questionModelCustom.MemberNameLocal;
                                            strArray[7] = " (";
                                            strArray[8] = questionModelCustom.ConstituencyName_Local;
                                            strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\nक्या <b>";
                                            strArray[10] = questionModelCustom.MinistryNameLocal;
                                            strArray[11] = "</b> बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n\r\n";
                                            htmlString = string.Concat(strArray);
                                            continue;
                                        }
                                    }
                                    string str3 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray1 = new string[14];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = questionModelCustom.MemberNameLocal;
                                    strArray1[7] = " (";
                                    strArray1[8] = questionModelCustom.ConstituencyName_Local;
                                    strArray1[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray1[10] = questionModelCustom.MinistryNameLocal;
                                    strArray1[11] = "</b> बतलाने की कृपा करेंगे कि ";
                                    strArray1[12] = str3;
                                    strArray1[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n\r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            nullable1 = questionModelCustom.IsQuestionInPart;
                            if (nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsQuestionInPart;
                                bool flag1 = false;
                                if (!(nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue))
                                {
                                    string[] strArray = new string[14];
                                    strArray[0] = htmlString;
                                    strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n<b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray[4] = nullable2.ToString();
                                    strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray[6] = questionModelCustom.MemberName;
                                    strArray[7] = " (";
                                    strArray[8] = questionModelCustom.ConstituencyName;
                                    strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\nWill the <b>";
                                    strArray[10] = questionModelCustom.MinistryName;
                                    strArray[11] = "</b> be pleased to state :-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                    strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                    strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n \r\n\r\n";
                                    htmlString = string.Concat(strArray);
                                    continue;
                                }
                            }
                            string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                            string[] strArray2 = new string[14];
                            strArray2[0] = htmlString;
                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n<b>";
                            nullable2 = questionModelCustom.QuestionNumber;
                            strArray2[4] = nullable2.ToString();
                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                            strArray2[6] = questionModelCustom.MemberName;
                            strArray2[7] = " (";
                            strArray2[8] = questionModelCustom.ConstituencyName;
                            strArray2[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  Will the <b>";
                            strArray2[10] = questionModelCustom.MinistryName;
                            strArray2[11] = "</b> be pleased to state ";
                            strArray2[12] = str4;
                            strArray2[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n \r\n\r\n";
                            htmlString = string.Concat(strArray2);
                        }
                    }
                    bool? stampTypeHindi = tMemberNotice3.StampTypeHindi;
                    bool flag4 = true;
                    if (stampTypeHindi.GetValueOrDefault() == flag4 & stampTypeHindi.HasValue)
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                            htmlString = htmlString + "\r\n \r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n      \r\n<b>" + questionModelCustom.SignaturePlaceLocal + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;' >\r\n                      <b>" + questionModelCustom.SignatureNameLocal + ",</b>\r\n              </td>\r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n           \r\n         <b>दिनांक: " + questionModelCustom.SignatureDateLocal + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;' >\r\n       \r\n  <b>" + questionModelCustom.SignatureDesignationsLocal + "।</b>          \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n       \r\n                     </body> </html>";
                    }
                    else
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                            htmlString = htmlString + "\r\n \r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n      \r\n<b>" + questionModelCustom.SignaturePlace + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;' >\r\n                      <b>" + questionModelCustom.SignatureName + ",</b>\r\n              </td>\r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n           \r\n         <b>Dated: " + questionModelCustom.SignatureDate + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;' >\r\n       \r\n  <b>" + questionModelCustom.SignatureDesignations + ".</b>          \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n       \r\n                     </body> </html>";
                    }
                }
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;
                pdfConverter.PdfHeaderOptions.HeaderHeight = 75f;
                pdfConverter.PdfFooterOptions.FooterHeight = 60f;
                TextElement textElement = new TextElement(0.0f, 30f, "&p;", new System.Drawing.Font(new System.Drawing.FontFamily("Times New Roman"), 10f, GraphicsUnit.Point));
                textElement.ForeColor = (PdfColor)Color.MidnightBlue;
                textElement.TextAlign = HorizontalTextAlign.Center;
                pdfConverter.PdfFooterOptions.AddElement((PageElement)textElement);
                string path1 = Path.Combine(this.Server.MapPath("~"), "Images");
                ImageElement imageElement1 = new ImageElement(250f, 10f, Path.Combine(path1, "Blanknotapproved.png"));
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                pdfConverter.PdfHeaderOptions.AddElement((PageElement)imageElement1);
                ImageElement imageElement2 = new ImageElement(0.0f, 0.0f, Path.Combine(path1, "Blanknotapproved.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 15;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388f;
                EvoPdf.Document objectFromHtmlString = pdfConverter.GetPdfDocumentObjectFromHtmlString(htmlString);
                float width = float.Parse("400");
                float height = float.Parse("400");
                RectangleF bounds = new RectangleF((float)(((double)objectFromHtmlString.Pages[0].ClientRectangle.Width - (double)width) / 2.0), 150f, width, height);
                objectFromHtmlString.AddTemplate(bounds).AddElement((PageElement)imageElement2);
                byte[] buffer = (byte[])null;
                try
                {
                    buffer = objectFromHtmlString.Save();
                }
                finally
                {
                    objectFromHtmlString.Close();
                }
                string[] strArray6 = new string[5]
                {
          "Question/AfterFixation/",
          null,
          null,
          null,
          null
                };
                int num2 = tMemberNotice3.AssemblyID;
                strArray6[1] = num2.ToString();
                strArray6[2] = "/";
                num2 = tMemberNotice3.SessionID;
                strArray6[3] = num2.ToString();
                strArray6[4] = "/";
                string str7 = string.Concat(strArray6);
                Id = Id.Replace("/", "-");
                if (ViewBy == "CQ")
                {
                    string[] strArray1 = new string[6];
                    num2 = tMemberNotice3.AssemblyID;
                    strArray1[0] = num2.ToString();
                    strArray1[1] = "_";
                    num2 = tMemberNotice3.SessionID;
                    strArray1[2] = num2.ToString();
                    strArray1[3] = "_U_";
                    strArray1[4] = Id;
                    strArray1[5] = ".pdf";
                    path2 = string.Concat(strArray1);
                }
                else if (ViewBy == "PQ")
                {
                    string[] strArray1 = new string[6];
                    num2 = tMemberNotice3.AssemblyID;
                    strArray1[0] = num2.ToString();
                    strArray1[1] = "_";
                    num2 = tMemberNotice3.SessionID;
                    strArray1[2] = num2.ToString();
                    strArray1[3] = "_U_PQ_";
                    strArray1[4] = Id;
                    strArray1[5] = ".pdf";
                    path2 = string.Concat(strArray1);
                }
                this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=UnstaredQuestionList/" + path2);
                string str8 = tMemberNotice3.FilePath + str7;
                if (!Directory.Exists(str8))
                    Directory.CreateDirectory(str8);
                FileStream fileStream = new FileStream(Path.Combine(str8, path2), FileMode.Create, FileAccess.Write);
                fileStream.Write(buffer, 0, buffer.Length);
                fileStream.Close();
                tMemberNotice3.FilePath = str7 + path2;
                CurrentSession.FileAccessPath = tMemberNotice3.DocFilePath;
                string str9 = "/" + str7 + path2;
                tMemberNotice tMemberNotice4 = Helper.ExecuteService("Notice", "UnstaredUpdatemSessionDate", (object)tMemberNotice3) as tMemberNotice;
                return "Success" + str9;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string U_GeneratePdfByDate_ByView(string Id, string ViewBy)
        {
            try
            {
                string htmlString = "";
                string path2 = "";
                string empty = string.Empty;
                string str1 = "";
                string str2 = "";
                if (ViewBy == "CQ")
                {
                    str1 = "लिखित उत्तर हेतु प्रश्न";
                    str2 = "Questions For Written Answers";
                }
                else if (ViewBy == "PQ")
                {
                    str1 = "लिखित उत्तर हेतु स्थगित प्रश्न";
                    str2 = "Postponed Questions For Written Answer";
                }
                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25f, 25f, 30f, 30f);
                tMemberNotice tMemberNotice1 = new tMemberNotice();
                tMemberNotice1.SessionID = (int)Convert.ToInt16(CurrentSession.SessionId);
                tMemberNotice1.AssemblyID = (int)Convert.ToInt16(CurrentSession.AssemblyId);
                DateTime exact = DateTime.ParseExact(Id, "dd/MM/yyyy", (IFormatProvider)null);
                tMemberNotice1.NoticeDate = exact;
                tMemberNotice1.DataStatus = ViewBy;
                tMemberNotice tMemberNotice2 = (tMemberNotice)Helper.ExecuteService("Notice", "UnstaredGetAllDetailsForSpdf", (object)tMemberNotice1);
                if (tMemberNotice2.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice2.tQuestionModel)
                    {
                        tMemberNotice2.SessionDateId = questionModelCustom.SessionDateId;
                        tMemberNotice2.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", (object)tMemberNotice2);
                        tMemberNotice2.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", (object)tMemberNotice2);
                    }
                }
                tMemberNotice tMemberNotice3 = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", (object)tMemberNotice2);
                foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                    tMemberNotice3.SesNameEnglish = questionModelCustom.SesNameEnglish;
                if (tMemberNotice3.tQuestionModel != null)
                {
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.tQuestionModel)
                    {
                        bool? headerHindi = tMemberNotice3.HeaderHindi;
                        bool flag = true;
                        if (headerHindi.GetValueOrDefault() == flag & headerHindi.HasValue)
                        {
                            htmlString = "<html>                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            htmlString = htmlString + "<div> \r\n\r\n <style type='text/css'>\r\n@page {\r\n    @bottom-left {\r\n        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n    @bottom-right {\r\n        content: 'Page ' counter(page) ' of ' counter(pages);\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n}\r\n</style>\r\n                    </div>\r\n            \r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;' >\r\n             <b>हिमाचल प्रदेश तेरहवीं विधान सभा</b>\r\n              </td>\r\n              </tr>\r\n      </table>\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n            <b>(" + tMemberNotice3.SessionName + ")</b> \r\n              </td>\r\n              </tr>\r\n           </table>\r\n<br />\r\n<br />\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;'>\r\n            <b>" + str1 + "</b>          \r\n              </td>    \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;' >\r\n          <b>" + questionModelCustom.SessionDateLocal + "</b>   \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n            <b>[" + tMemberNotice3.MinisterName + "]. </b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           <b>कुल प्रश्न - " + tMemberNotice3.TotalQCount.ToString() + "</b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----\r\n            \r\n              </td>\r\n              </tr>\r\n           </table>";
                        }
                        else
                        {
                            htmlString = "<html>                           \r\n                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            htmlString = htmlString + "<div> \r\n\r\n <style type='text/css'>\r\n@page {\r\n    @bottom-left {\r\n        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n    @bottom-right {\r\n        content: 'Page ' counter(page) ' of ' counter(pages);\r\n        font-family: sans-serif;\r\n        font-size: 80%;\r\n    }\r\n}\r\n</style>\r\n                    </div>\r\n\r\n            \r\n<table style='width:100%'>\r\n \r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;' >\r\n                   <b> MUNCIPAL CORPORATION " + tMemberNotice3.AssesmblyName + " </b>\r\n              </td>\r\n              </tr>\r\n      </table>\r\n<br />\r\n<br />\r\n\r\n<table style='width:100%'>\r\n   <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n            <b>(" + tMemberNotice3.SesNameEnglish + ")</b> \r\n              </td>\r\n              </tr>\r\n           </table>\r\n<br />\r\n<br />\r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;'>\r\n            <b>" + str2 + "</b>         \r\n              </td>    \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n      <tr>\r\n          <td style='text-align:center;font-size:28px;' >\r\n          <b>" + questionModelCustom.SessionDateEnglish + "</b>   \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:justify;font-size:20px;' >\r\n            <b>[" + tMemberNotice3.MinisterNameEnglish + "]. </b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           <b>Total No. of Questions - " + tMemberNotice3.TotalQCount.ToString() + "</b>\r\n            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----\r\n            \r\n              </td>\r\n              </tr>\r\n           </table>";
                        }
                    }
                    foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.tQuestionModel)
                    {
                        bool? nullable1 = questionModelCustom.IsClubbed;
                        int? nullable2;
                        if (nullable1.HasValue)
                        {
                            nullable1 = questionModelCustom.IsHindi;
                            bool flag1 = true;
                            if (nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue)
                            {
                                nullable2 = questionModelCustom.MinisterID;
                                int num = 90;
                                if (!(nullable2.GetValueOrDefault() == num & nullable2.HasValue))
                                {
                                    string[] strArray1 = questionModelCustom.CMemNameHindi.Split(',');
                                    string str3 = "";
                                    for (int index = 0; index < strArray1.Length; ++index)
                                    {
                                        strArray1[index] = strArray1[index].Trim();
                                        str3 = str3 + "<div>\r\n          " + strArray1[index] + " :\r\n\r\n                               </div>\r\n                             ";
                                    }
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray2 = new string[12];
                                            strArray2[0] = htmlString;
                                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray2[4] = nullable2.ToString();
                                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                            strArray2[6] = str3;
                                            strArray2[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n क्या <b>";
                                            strArray2[8] = questionModelCustom.MinistryNameLocal;
                                            strArray2[9] = "</b> बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray2[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray2[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                            htmlString = string.Concat(strArray2);
                                            continue;
                                        }
                                    }
                                    string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray3 = new string[12];
                                    strArray3[0] = htmlString;
                                    strArray3[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray3[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray3[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray3[4] = nullable2.ToString();
                                    strArray3[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray3[6] = str3;
                                    strArray3[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray3[8] = questionModelCustom.MinistryNameLocal;
                                    strArray3[9] = "</b> बतलाने की कृपा करेंगे कि ";
                                    strArray3[10] = str4;
                                    strArray3[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    htmlString = string.Concat(strArray3);
                                    continue;
                                }
                            }
                            nullable2 = questionModelCustom.MinisterID;
                            int num1 = 90;
                            if (nullable2.GetValueOrDefault() == num1 & nullable2.HasValue)
                            {
                                nullable1 = questionModelCustom.IsHindi;
                                bool flag2 = true;
                                if (nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue)
                                {
                                    string[] strArray1 = questionModelCustom.CMemNameHindi.Split(',');
                                    string str3 = "";
                                    for (int index = 0; index < strArray1.Length; ++index)
                                    {
                                        strArray1[index] = strArray1[index].Trim();
                                        str3 = str3 + "<div>\r\n          " + strArray1[index] + " :\r\n\r\n                               </div>\r\n                             ";
                                    }
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag3 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag3 & nullable1.HasValue))
                                        {
                                            string[] strArray2 = new string[12];
                                            strArray2[0] = htmlString;
                                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray2[4] = nullable2.ToString();
                                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                            strArray2[6] = str3;
                                            strArray2[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n क्या <b>";
                                            strArray2[8] = questionModelCustom.MinistryNameLocal;
                                            strArray2[9] = "</b> बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray2[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray2[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                            htmlString = string.Concat(strArray2);
                                            continue;
                                        }
                                    }
                                    string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray3 = new string[12];
                                    strArray3[0] = htmlString;
                                    strArray3[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray3[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray3[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray3[4] = nullable2.ToString();
                                    strArray3[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray3[6] = str3;
                                    strArray3[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray3[8] = questionModelCustom.MinistryNameLocal;
                                    strArray3[9] = "</b> बतलाने की कृपा करेंगी कि ";
                                    strArray3[10] = str4;
                                    strArray3[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    htmlString = string.Concat(strArray3);
                                    continue;
                                }
                            }
                            string[] strArray4 = questionModelCustom.CMemName.Split(',');
                            string str5 = "";
                            for (int index = 0; index < strArray4.Length; ++index)
                            {
                                strArray4[index] = strArray4[index].Trim();
                                str5 = str5 + "<div>\r\n          " + strArray4[index] + " : \r\n\r\n                               </div>\r\n                             ";
                            }
                            nullable1 = questionModelCustom.IsQuestionInPart;
                            if (nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsQuestionInPart;
                                bool flag2 = false;
                                if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                {
                                    string[] strArray1 = new string[12];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = str5;
                                    strArray1[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\nWill the <b>";
                                    strArray1[8] = questionModelCustom.MinistryName;
                                    strArray1[9] = "</b> be pleased to state :-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                    strArray1[10] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                    strArray1[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            string str6 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                            string[] strArray5 = new string[12];
                            strArray5[0] = htmlString;
                            strArray5[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                            strArray5[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                            strArray5[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                            nullable2 = questionModelCustom.QuestionNumber;
                            strArray5[4] = nullable2.ToString();
                            strArray5[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                            strArray5[6] = str5;
                            strArray5[7] = "</b>\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  Will the <b>";
                            strArray5[8] = questionModelCustom.MinistryName;
                            strArray5[9] = "</b> be pleased to state ";
                            strArray5[10] = str6;
                            strArray5[11] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n";
                            htmlString = string.Concat(strArray5);
                        }
                        else
                        {
                            nullable2 = questionModelCustom.MinisterID;
                            int num = 90;
                            if (nullable2.GetValueOrDefault() == num & nullable2.HasValue)
                            {
                                nullable1 = questionModelCustom.IsHindi;
                                bool flag1 = true;
                                if (nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue)
                                {
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray = new string[14];
                                            strArray[0] = htmlString;
                                            strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>";
                                            strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n</table>\r\n\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray[4] = nullable2.ToString();
                                            strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                            strArray[6] = questionModelCustom.MemberNameLocal;
                                            strArray[7] = " (";
                                            strArray[8] = questionModelCustom.ConstituencyName_Local;
                                            strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\nक्या <b>";
                                            strArray[10] = questionModelCustom.MinistryNameLocal;
                                            strArray[11] = "</b> बतलाने की कृपा करेंगी कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n           \r\n  \r\n";
                                            htmlString = string.Concat(strArray);
                                            continue;
                                        }
                                    }
                                    string str3 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray1 = new string[14];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n</table>\r\n\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = questionModelCustom.MemberNameLocal;
                                    strArray1[7] = " (";
                                    strArray1[8] = questionModelCustom.ConstituencyName_Local;
                                    strArray1[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray1[10] = questionModelCustom.MinistryNameLocal;
                                    strArray1[11] = "</b> बतलाने की कृपा करेंगी कि ";
                                    strArray1[12] = str3;
                                    strArray1[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n           \r\n  \r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            nullable1 = questionModelCustom.IsHindi;
                            bool flag3 = true;
                            if (nullable1.GetValueOrDefault() == flag3 & nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsClubbed;
                                bool flag1 = true;
                                if (!(nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue))
                                {
                                    nullable1 = questionModelCustom.IsQuestionInPart;
                                    if (nullable1.HasValue)
                                    {
                                        nullable1 = questionModelCustom.IsQuestionInPart;
                                        bool flag2 = false;
                                        if (!(nullable1.GetValueOrDefault() == flag2 & nullable1.HasValue))
                                        {
                                            string[] strArray = new string[14];
                                            strArray[0] = htmlString;
                                            strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                            strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                            strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                            nullable2 = questionModelCustom.QuestionNumber;
                                            strArray[4] = nullable2.ToString();
                                            strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                            strArray[6] = questionModelCustom.MemberNameLocal;
                                            strArray[7] = " (";
                                            strArray[8] = questionModelCustom.ConstituencyName_Local;
                                            strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\nक्या <b>";
                                            strArray[10] = questionModelCustom.MinistryNameLocal;
                                            strArray[11] = "</b> बतलाने की कृपा करेंगे कि:-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                            strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                            strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n\r\n";
                                            htmlString = string.Concat(strArray);
                                            continue;
                                        }
                                    }
                                    string str3 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                                    string[] strArray1 = new string[14];
                                    strArray1[0] = htmlString;
                                    strArray1[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n     \r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray1[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray1[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n  <b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray1[4] = nullable2.ToString();
                                    strArray1[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray1[6] = questionModelCustom.MemberNameLocal;
                                    strArray1[7] = " (";
                                    strArray1[8] = questionModelCustom.ConstituencyName_Local;
                                    strArray1[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  क्या <b>";
                                    strArray1[10] = questionModelCustom.MinistryNameLocal;
                                    strArray1[11] = "</b> बतलाने की कृपा करेंगे कि ";
                                    strArray1[12] = str3;
                                    strArray1[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n\r\n\r\n";
                                    htmlString = string.Concat(strArray1);
                                    continue;
                                }
                            }
                            nullable1 = questionModelCustom.IsQuestionInPart;
                            if (nullable1.HasValue)
                            {
                                nullable1 = questionModelCustom.IsQuestionInPart;
                                bool flag1 = false;
                                if (!(nullable1.GetValueOrDefault() == flag1 & nullable1.HasValue))
                                {
                                    string[] strArray = new string[14];
                                    strArray[0] = htmlString;
                                    strArray[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                                    strArray[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                                    strArray[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n<b>";
                                    nullable2 = questionModelCustom.QuestionNumber;
                                    strArray[4] = nullable2.ToString();
                                    strArray[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                                    strArray[6] = questionModelCustom.MemberName;
                                    strArray[7] = " (";
                                    strArray[8] = questionModelCustom.ConstituencyName;
                                    strArray[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\nWill the <b>";
                                    strArray[10] = questionModelCustom.MinistryName;
                                    strArray[11] = "</b> be pleased to state :-\r\n</td>\r\n\r\n</tr>\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  ";
                                    strArray[12] = WebUtility.HtmlDecode(questionModelCustom.MainQuestion);
                                    strArray[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n \r\n\r\n";
                                    htmlString = string.Concat(strArray);
                                    continue;
                                }
                            }
                            string str4 = WebUtility.HtmlDecode(questionModelCustom.MainQuestion).Replace("<p>", "").Replace("</p>", "");
                            string[] strArray2 = new string[14];
                            strArray2[0] = htmlString;
                            strArray2[1] = "<table style='width:100%'>\r\n\r\n<tr style='page-break-inside : avoid'>\r\n\r\n          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >";
                            strArray2[2] = WebUtility.HtmlDecode(questionModelCustom.Subject);
                            strArray2[3] = "\r\n       \r\n              </td>      \r\n         \r\n         </tr>\r\n           </table>\r\n\r\n<table style='width:100%;text-align:justify;'>\r\n<tr>\r\n<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>\r\n<b>";
                            nullable2 = questionModelCustom.QuestionNumber;
                            strArray2[4] = nullable2.ToString();
                            strArray2[5] = "</b>\r\n</td>\r\n<td style='font-size:20px;padding-bottom: 13px;'>\r\n<b>";
                            strArray2[6] = questionModelCustom.MemberName;
                            strArray2[7] = " (";
                            strArray2[8] = questionModelCustom.ConstituencyName;
                            strArray2[9] = ")</b>:\r\n</td>\r\n</tr>\r\n\r\n<tr>\r\n<td>\r\n</td>\r\n<td style='font-size:20px;'>\r\n <p style='text-align:justify;'>\r\n  Will the <b>";
                            strArray2[10] = questionModelCustom.MinistryName;
                            strArray2[11] = "</b> be pleased to state ";
                            strArray2[12] = str4;
                            strArray2[13] = "\r\n</p>\r\n</td>\r\n\r\n</tr>\r\n\r\n</table>\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:center;font-size:20px;' >\r\n           ----            \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n \r\n\r\n";
                            htmlString = string.Concat(strArray2);
                        }
                    }
                    bool? stampTypeHindi = tMemberNotice3.StampTypeHindi;
                    bool flag4 = true;
                    if (stampTypeHindi.GetValueOrDefault() == flag4 & stampTypeHindi.HasValue)
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                            htmlString = htmlString + "\r\n \r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n      \r\n<b>" + questionModelCustom.SignaturePlaceLocal + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;' >\r\n                      <b>" + questionModelCustom.SignatureNameLocal + ",</b>\r\n              </td>\r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n           \r\n         <b>दिनांक: " + questionModelCustom.SignatureDateLocal + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;' >\r\n       \r\n  <b>" + questionModelCustom.SignatureDesignationsLocal + "।</b>          \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n       \r\n                     </body> </html>";
                    }
                    else
                    {
                        foreach (QuestionModelCustom questionModelCustom in (IEnumerable<QuestionModelCustom>)tMemberNotice3.Values)
                            htmlString = htmlString + "\r\n \r\n\r\n<table style='width:100%'>\r\n     <tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n      \r\n<b>" + questionModelCustom.SignaturePlace + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;' >\r\n                      <b>" + questionModelCustom.SignatureName + ",</b>\r\n              </td>\r\n         \r\n              </tr>\r\n<tr>\r\n          <td style='text-align:left;font-size:20px;' >\r\n           \r\n         <b>Dated: " + questionModelCustom.SignatureDate + ".</b>\r\n              </td>\r\n<td style='text-align:right;font-size:20px;' >\r\n       \r\n  <b>" + questionModelCustom.SignatureDesignations + ".</b>          \r\n              </td>\r\n         \r\n              </tr>\r\n           </table>\r\n       \r\n                     </body> </html>";
                    }
                }
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;
                pdfConverter.PdfHeaderOptions.HeaderHeight = 75f;
                pdfConverter.PdfFooterOptions.FooterHeight = 60f;
                TextElement textElement = new TextElement(0.0f, 30f, "&p;", new System.Drawing.Font(new System.Drawing.FontFamily("Times New Roman"), 10f, GraphicsUnit.Point));
                textElement.ForeColor = (PdfColor)Color.MidnightBlue;
                textElement.TextAlign = HorizontalTextAlign.Center;
                pdfConverter.PdfFooterOptions.AddElement((PageElement)textElement);
                string path1 = Path.Combine(this.Server.MapPath("~"), "Images");
                ImageElement imageElement1 = new ImageElement(250f, 10f, Path.Combine(path1, "notapproved.png"));
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                pdfConverter.PdfHeaderOptions.AddElement((PageElement)imageElement1);
                ImageElement imageElement2 = new ImageElement(0.0f, 0.0f, Path.Combine(path1, "notapproved.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 15;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388f;
                EvoPdf.Document objectFromHtmlString = pdfConverter.GetPdfDocumentObjectFromHtmlString(htmlString);
                float width = float.Parse("400");
                float height = float.Parse("400");
                RectangleF bounds = new RectangleF((float)(((double)objectFromHtmlString.Pages[0].ClientRectangle.Width - (double)width) / 2.0), 150f, width, height);
                objectFromHtmlString.AddTemplate(bounds).AddElement((PageElement)imageElement2);
                byte[] buffer = (byte[])null;
                try
                {
                    buffer = objectFromHtmlString.Save();
                }
                finally
                {
                    objectFromHtmlString.Close();
                }
                string[] strArray6 = new string[5]
                {
          "Question/AfterFixation/",
          null,
          null,
          null,
          null
                };
                int num2 = tMemberNotice3.AssemblyID;
                strArray6[1] = num2.ToString();
                strArray6[2] = "/";
                num2 = tMemberNotice3.SessionID;
                strArray6[3] = num2.ToString();
                strArray6[4] = "/";
                string str7 = string.Concat(strArray6);
                Id = Id.Replace("/", "-");
                if (ViewBy == "CQ")
                {
                    string[] strArray1 = new string[6];
                    num2 = tMemberNotice3.AssemblyID;
                    strArray1[0] = num2.ToString();
                    strArray1[1] = "_";
                    num2 = tMemberNotice3.SessionID;
                    strArray1[2] = num2.ToString();
                    strArray1[3] = "_U_";
                    strArray1[4] = Id;
                    strArray1[5] = ".pdf";
                    path2 = string.Concat(strArray1);
                }
                else if (ViewBy == "PQ")
                {
                    string[] strArray1 = new string[6];
                    num2 = tMemberNotice3.AssemblyID;
                    strArray1[0] = num2.ToString();
                    strArray1[1] = "_";
                    num2 = tMemberNotice3.SessionID;
                    strArray1[2] = num2.ToString();
                    strArray1[3] = "_U_PQ_";
                    strArray1[4] = Id;
                    strArray1[5] = ".pdf";
                    path2 = string.Concat(strArray1);
                }
                this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + path2);
                string str8 = tMemberNotice3.FilePath + str7;
                if (!Directory.Exists(str8))
                    Directory.CreateDirectory(str8);
                FileStream fileStream = new FileStream(Path.Combine(str8, path2), FileMode.Create, FileAccess.Write);
                fileStream.Write(buffer, 0, buffer.Length);
                fileStream.Close();
                tMemberNotice3.FilePath = str7 + path2;
                CurrentSession.FileAccessPath = tMemberNotice3.DocFilePath;
                string str9 = "/" + str7 + path2;
                tMemberNotice tMemberNotice4 = Helper.ExecuteService("Notice", "UnstaredUpdatemSessionDate", (object)tMemberNotice3) as tMemberNotice;
                return "Success" + str9;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        //        public string U_GeneratePdfByDate_ByView(string Id, string ViewBy)
        //        {
        //            try
        //            {
        //                string outXml = "";
        //                string outInnerXml = "";
        //                string LOBName = Id;
        //                string fileName = "";
        //                string savedPDF = string.Empty;
        //                string HeadingText = "";
        //                string HeadingTextEnglish = "";

        //                if (ViewBy == "CQ")
        //                {
        //                    HeadingText = "लिखित उत्तर हेतु प्रश्न";
        //                    HeadingTextEnglish = "Questions For Written Answers";
        //                }
        //                else if (ViewBy == "PQ")
        //                {
        //                    HeadingText = "लिखित उत्तर हेतु स्थगित प्रश्न";
        //                    HeadingTextEnglish = "Postponed Questions For Written Answer";
        //                }


        //                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        //                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);
        //                tMemberNotice model = new tMemberNotice();

        //                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
        //                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
        //                string strDate = Id;
        //                DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
        //                model.NoticeDate = date;
        //                model.DataStatus = ViewBy;
        //                model = (tMemberNotice)Helper.ExecuteService("Notice", "UnstaredGetAllDetailsForSpdf", model);
        //                if (model.tQuestionModel != null)
        //                {
        //                    foreach (var item in model.tQuestionModel)
        //                    {
        //                        model.SessionDateId = item.SessionDateId;
        //                        model.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", model);
        //                        model.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", model);
        //                    }
        //                }
        //                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", model);
        //                foreach (var item in model.Values)
        //                {
        //                    model.SesNameEnglish = item.SesNameEnglish;
        //                }

        //                if (model.tQuestionModel != null)
        //                {
        //                    foreach (var item in model.tQuestionModel)
        //                    {
        //                        if (model.HeaderHindi == true)
        //                        {
        //                            outXml = @"<html>                           
        //                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
        //                            outXml += @"<div> 
        //
        // <style type='text/css'>
        //@page {
        //    @bottom-left {
        //        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //    @bottom-right {
        //        content: 'Page ' counter(page) ' of ' counter(pages);
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //}
        //</style>
        //                    </div>
        //            
        //<table style='width:100%'>
        // 
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //             <b>हिमाचल प्रदेश बारहवीं विधान सभा</b>
        //              </td>
        //              </tr>
        //      </table>
        //<br />
        //<br />
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "(" + model.SessionName + ")" + @"</b> 
        //              </td>
        //              </tr>
        //           </table>
        //<br />
        //<br />
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;'>
        //            <b>" + HeadingText + @"</b>          
        //              </td>    
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //          <b>" + item.SessionDateLocal + @"</b>   
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "[" + model.MinisterName + "]" + @". </b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           <b>कुल प्रश्न - " + model.TotalQCount + @"</b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----
        //            
        //              </td>
        //              </tr>
        //           </table>";

        //                        }
        //                        else
        //                        {
        //                            outXml = @"<html>                           
        //                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
        //                            outXml += @"<div> 
        //
        // <style type='text/css'>
        //@page {
        //    @bottom-left {
        //        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //    @bottom-right {
        //        content: 'Page ' counter(page) ' of ' counter(pages);
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //}
        //</style>
        //                    </div>
        //
        //            
        //<table style='width:100%'>
        // 
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //             <b>HIMACHAL PRADESH THIRTEENTH VIDHAN SABHA</b>
        //              </td>
        //              </tr>
        //      </table>
        //<br />
        //<br />
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "(" + model.SesNameEnglish + ")" + @"</b> 
        //              </td>
        //              </tr>
        //           </table>
        //<br />
        //<br />
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;'>
        //            <b>" + HeadingTextEnglish + @"</b>         
        //              </td>    
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //          <b>" + item.SessionDateEnglish + @"</b>   
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "[" + model.MinisterNameEnglish + "]" + @". </b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           <b>Total No. of Questions - " + model.TotalQCount + @"</b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----
        //            
        //              </td>
        //              </tr>
        //           </table>";

        //                        }

        //                    }
        //                    foreach (var item in model.tQuestionModel)
        //                    {
        //                        if (item.IsClubbed != null)
        //                        {
        //                            if (item.IsHindi == true && item.MinisterID != 2)
        //                            {
        //                                string[] values = item.CMemNameHindi.Split(',');
        //                                outInnerXml = "";
        //                                for (int i = 0; i < values.Length; i++)
        //                                {
        //                                    values[i] = values[i].Trim();
        //                                    outInnerXml += @"<div>
        //          " + values[i] + @" :
        //
        //                               </div>
        //                             ";
        //                                }

        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }



        //                            }
        //                            else
        //                            {
        //                                string[] values = item.CMemName.Split(',');
        //                                outInnerXml = "";
        //                                for (int i = 0; i < values.Length; i++)
        //                                {
        //                                    values[i] = values[i].Trim();
        //                                    outInnerXml += @"<div>
        //          " + values[i] + @" :
        //
        //                               </div>
        //                             ";
        //                                }
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        //Will the <b>" + item.MinistryName + @"</b> be pleased to state :-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }


        //                            }




        //                        }
        //                        else
        //                        {

        //                            if (item.MinisterID == 2 && item.IsHindi == true)
        //                            {
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि:-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }


        //                            }
        //                            else if (item.IsHindi == true)
        //                            {
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }

        //                            }
        //                            else
        //                            {
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        //Will the <b>" + item.MinistryName + @"</b> be pleased to state :-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }

        //                            }


        //                        }
        //                    }


        //                    if (model.StampTypeHindi == true)
        //                    {
        //                        foreach (var item in model.Values)
        //                        {

        //                            outXml += @"
        // 
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:left;font-size:20px;' >
        //      
        //<b>" + item.SignaturePlaceLocal + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //                      <b>" + item.SignatureNameLocal + @",</b>
        //              </td>
        //         
        //              </tr>
        //<tr>
        //          <td style='text-align:left;font-size:20px;' >
        //           
        //         <b>दिनांक: " + item.SignatureDateLocal + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //       
        //  <b>" + item.SignatureDesignationsLocal + @"।</b>          
        //              </td>
        //         
        //              </tr>
        //           </table>
        //       
        //                     </body> </html>";

        //                        }
        //                    }
        //                    else
        //                    {
        //                        foreach (var item in model.Values)
        //                        {

        //                            outXml += @"
        // 
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:left;font-size:20px;' >
        //      
        //<b>" + item.SignaturePlace + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //                      <b>" + item.SignatureName + @",</b>
        //              </td>
        //         
        //              </tr>
        //<tr>
        //          <td style='text-align:left;font-size:20px;' >
        //           
        //         <b>Dated: " + item.SignatureDate + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //       
        //  <b>" + item.SignatureDesignations + @".</b>          
        //              </td>
        //         
        //              </tr>
        //           </table>
        //       
        //                     </body> </html>";
        //                        }
        //                    }
        //                }
        //                string htmlStringToConvert = outXml;
        //                PdfConverter pdfConverter = new PdfConverter();
        //                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

        //                pdfConverter.PdfDocumentOptions.ShowFooter = true;
        //                pdfConverter.PdfDocumentOptions.ShowHeader = true;

        //                // set the header height in points
        //                pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
        //                pdfConverter.PdfFooterOptions.FooterHeight = 60;

        //                //TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
        //                //        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //                //footerTextElement.TextAlign = HorizontalTextAlign.Center;
        //                TextElement footerTextElement = new TextElement(0, 30, "&p;",
        //                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //                footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
        //                footerTextElement.TextAlign = HorizontalTextAlign.Center;

        //                //TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan",
        //                //       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //                //footerTextElement1.TextAlign = HorizontalTextAlign.Left;
        //                //footerTextElement1.ForeColor = System.Drawing.Color.MidnightBlue;
        //                //footerTextElement1.TextAlign = HorizontalTextAlign.Center;

        //                //TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
        //                //       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //                //footerTextElement3.TextAlign = HorizontalTextAlign.Right;
        //                //footerTextElement3.ForeColor = System.Drawing.Color.MidnightBlue;
        //                //footerTextElement3.TextAlign = HorizontalTextAlign.Center;
        //                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
        //                //pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
        //                //pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

        //                string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

        //                ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "notapproved.png"));
        //                // imageElement1.KeepAspectRatio = true;
        //                imageElement1.Opacity = 100;
        //                imageElement1.DestHeight = 97f;
        //                imageElement1.DestWidth = 71f;
        //                pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
        //                ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "notapproved.png"));
        //                imageElement2.KeepAspectRatio = false;
        //                imageElement2.Opacity = 15;
        //                imageElement2.DestHeight = 284f;
        //                imageElement2.DestWidth = 388F;

        //                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
        //                float stampWidth = float.Parse("400");
        //                float stampHeight = float.Parse("400");

        //                // Center the stamp at the top of PDF page
        //                float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
        //                float stampYLocation = 150;

        //                RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

        //                Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
        //                stampTemplate.AddElement(imageElement2);
        //                byte[] pdfBytes = null;

        //                try
        //                {
        //                    pdfBytes = pdfDocument.Save();
        //                }
        //                finally
        //                {
        //                    // close the Document to realease all the resources
        //                    pdfDocument.Close();
        //                }
        //                string url = "Question" + "/AfterFixation/" + model.AssemblyID + "/" + model.SessionID + "/";
        //                Id = Id.Replace("/", "-");
        //                if (ViewBy == "CQ")
        //                {
        //                    fileName = model.AssemblyID + "_" + model.SessionID + "_U_" + Id + ".pdf";
        //                }
        //                else if (ViewBy == "PQ")
        //                {
        //                    fileName = model.AssemblyID + "_" + model.SessionID + "_U_PQ_" + Id + ".pdf";
        //                }
        //                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=UnstaredQuestionList/" + fileName);

        //                //string directory = Server.MapPath(model.FilePath + url);
        //                string directory = model.FilePath + url;
        //                if (!Directory.Exists(directory))
        //                {
        //                    Directory.CreateDirectory(directory);
        //                }

        //                //var path = Path.Combine(Server.MapPath(model.FilePath + url), fileName);
        //                var path = Path.Combine(directory, fileName);
        //                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

        //                // close file stream
        //                _FileStream.Close();

        //                //model.FilePath = directory + fileName;
        //                model.FilePath = url + fileName;

        //                CurrentSession.FileAccessPath = model.DocFilePath;
        //                var AccessPath = "/" + url + fileName;

        //                model = Helper.ExecuteService("Notice", "UnstaredUpdatemSessionDate", model) as tMemberNotice;

        //                return "Success" + AccessPath;
        //                //byte[] bytes = System.IO.File.ReadAllBytes(AccessPath);
        //                //return File(bytes, "application/pdf");
        //            }

        //            catch (Exception ex)
        //            {

        //                //throw ex;
        //                return ex.Message;
        //            }
        //            finally
        //            {

        //            }


        //        }


        public ActionResult CallPdfDept()
        {
            tMemberNotice model = new tMemberNotice();

            //objModel.NoticeRecievedDate = date;

            GenerateDeptbyPdf();
            model.Message = "Question List Generated";

            return PartialView("_deptListS", model);
        }
        public void GenerateDeptbyPdf()
        {
            try
            {
                string outXml = "";
                string outInnerXml = "";

#pragma warning disable CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
                string CurrentSecretery = "";
#pragma warning restore CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SessionDate' is assigned but its value is never used
                DateTime SessionDate = new DateTime();
#pragma warning restore CS0219 // The variable 'SessionDate' is assigned but its value is never used
                string fileName = "";
                string savedPDF = string.Empty;

                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                // iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);


                tMemberNotice model = new tMemberNotice();
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                int Asem = model.AssemblyID;
                int Sessi = model.SessionID;
                mSession Mdl = new mSession();
                Mdl.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
                Mdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                mAssembly assmblyMdl = new mAssembly();
                assmblyMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCodeHindi", Mdl);
                // model.DepartmentId = Id;
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDepartment", model);
                model.Updatedate = DateTime.Now;
                if (model.tQuestionModel != null)
                {

                    foreach (var item in model.tQuestionModel)
                    {
                        model.DepartmentName = item.DepartmentName;
                        model.DepartmentId = item.DepartmentId;
                        model = (tMemberNotice)Helper.ExecuteService("Notice", "GetAllDetailsForDeptwise", model);
                        //string LOBName = item.DepartmentId;



                        MemoryStream output = new MemoryStream();

                        EvoPdf.Document document1 = new EvoPdf.Document();

                        // set the license key

                        string LOBName = "";
                        document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                        document1.CompressionLevel = PdfCompressionLevel.Best;
                        document1.Margins = new Margins(0, 0, 0, 0);

                        LOBName = item.DepartmentName.Trim();
                        if (model.tQuestionModel != null)
                        {
                            foreach (var cc in model.tQuestionModel)
                            {

                                outXml = @"<html >                           
                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                outXml += @"<div> 


                    </div>
            
<table style='width:100%'>
 
      <tr>
  <td style='text-align:left;font-size:22px;' >
            तारांकित   
              </td>
        
<td style='text-align:Right;font-size:22px;' >
            वीO एसO एसO (मिसO) 2   
              </td>
              </tr>
      </table>
<table style='width:100%'>
 
      <tr>
  <td style='text-align:center;font-size:28px;' >
            विधान सभा प्रश्न   
              </td>
              </tr>
      </table>
 <table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           कुल प्रश्न - " + model.TotalQCount + @"
            
              </td>
         
              </tr>
           </table>



<table style='width:100%'>
     <tr>
          <td style='text-align:justify;font-size:20px' >
           ----
            
              </td>
         
              </tr>
           </table>";
                            }
                            foreach (var cc in model.tQuestionModel)
                            {
                                if (cc.IsClubbed != null)
                                {
                                    if (cc.IsHindi == true)
                                    {
                                        string[] values = cc.CMemNameHindi.Split(',');
                                        outInnerXml = "";
                                        for (int i = 0; i < values.Length; i++)
                                        {
                                            values[i] = values[i].Trim();
                                            outInnerXml += @"<div>
          " + values[i] + @" 

                               </div>
                             ";
                                        }
                                    }
                                    else
                                    {
                                        string[] values = cc.CMemName.Split(',');
                                        outInnerXml = "";
                                        for (int i = 0; i < values.Length; i++)
                                        {
                                            values[i] = values[i].Trim();
                                            outInnerXml += @"<div>
          " + values[i] + @" 

                               </div>
                             ";
                                        }
                                    }


                                    if (cc.IsHindi == true)
                                    {
                                        outXml += @"<table style='width:100%'>

    <table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             तारांकित डायरी संख्या : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             विषय:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          सूचना प्राप्ति दिनांक : " + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
          स्वीकृत सूचना दिनांक  :" + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + outInnerXml + @"
</td>
         
              </tr>
           </table>
<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  क्या " + cc.MinistryNameLocal + @" बतलाने की कृपा करेंगी कि:-
</td>

         
              </tr>
   
           </table>

<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";


                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                    else
                                    {
                                        outXml += @"<table style='width:100%'>

    <table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             Starred D. No. : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             subject:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          Notice Recieved on: " + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
         Notice of Admission sent on  :" + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + outInnerXml + @"
</td>
         
              </tr>
           </table>
<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  Will the " + cc.MinistryName + @" be Pleased to state :
</td>

         
              </tr>
   
           </table>

<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";


                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                }
                                else
                                {
                                    if (cc.IsHindi == true)
                                    {
                                        outXml += @"<table style='width:100%'>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             तारांकित डायरी संख्या : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             विषय:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          सूचना प्राप्ति दिनांक : " + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
          स्वीकृत सूचना दिनांक  :" + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + cc.MemberNameLocal + "(" + cc.ConstituencyName_Local + ")" + @"
</td>
         
              </tr>
           </table>
<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  क्या " + cc.MinistryNameLocal + @" बतलाने की कृपा करेंगी कि:-
</td>

         
              </tr>
   
           </table>

<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";

                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                    else
                                    {
                                        outXml += @"<table style='width:100%'>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             Starred D. No. : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             subject:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          Notice Recieved on: " + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
         Notice of Admission sent on  :" + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + cc.MemberName + "(" + cc.ConstituencyName + ")" + @"
</td>
         
              </tr>
           </table>

<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  Will the " + cc.MinistryName + @" be Pleased to state :
</td>

         
              </tr>
   
           </table>
<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";

                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        string baseURL = "";
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);


                                    }
                                }
                            }
                            outXml += @"



                   
                
                     </body> </html>";
                        }

                        byte[] pdfBytes = document1.Save();

                        try
                        {
                            output.Write(pdfBytes, 0, pdfBytes.Length);
                            output.Position = 0;

                        }
                        finally
                        {
                            // close the PDF document to release the resources
                            document1.Close();
                        }
                        foreach (var dd in model.tQuestionModel)
                        {
                            if (dd.MainQuestion != null)
                            {
                                string url = "/QuestionList/" + "BeforeFixation/" + Asem + "/" + Sessi + "/";

                                string LOBN = "";
                                LOBN = LOBName.Replace('>', ' ');
                                LOBN = LOBN.Trim();

                                //if (LOBName.IndexOf('>') > 0)
                                //{
                                //    LOBN = LOBName.Substring(1);
                                //    LOBN=LOBN.Trim();
                                //}
                                //else
                                //{
                                //    LOBN = LOBN.Trim();
                                //}

                                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + LOBN.Replace("/", "_") + ".pdf");

                                string directory = Server.MapPath(url);
                                if (!Directory.Exists(directory))
                                {
                                    Directory.CreateDirectory(directory);
                                }
                                fileName = LOBN.Replace("/", "_") + "_S_" + ".pdf";
                                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                                // close file stream
                                _FileStream.Close();
                                model.FilePath = url + fileName;
                                string value = model.DepartmentName;
                                model.DepartmentName = value.Replace('>', ' ');
                                //    var msg = Helper.ExecuteService("Notice", "UpdatemDepartment", model) as tMemberNotice;
                                var msg = Helper.ExecuteService("Notice", "UpdatemDepartmentPdfPath", model) as tMemberNotice;

                            }
                        }
                    }



                }
            }
            //



            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }
        //DiaryWiseBefore Fix Pdf


        public ActionResult DiaryWise()
        {
            tMemberNotice model = new tMemberNotice();

            return PartialView("_DiaryWise", model);

        }

        [HttpGet]
        public ActionResult PdfDiaryWise()
        {
            GenerateDiarywisePdf();

            string res = "Question List Generated";

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public void GenerateDiarywisePdf()
        {
            try
            {
                string outXml = "";
                string outInnerXml = "";

#pragma warning disable CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
                string CurrentSecretery = "";
#pragma warning restore CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SessionDate' is assigned but its value is never used
                DateTime SessionDate = new DateTime();
#pragma warning restore CS0219 // The variable 'SessionDate' is assigned but its value is never used
                string fileName = "";
                string savedPDF = string.Empty;

                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                //  iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);


                tMemberNotice model = new tMemberNotice();
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                int Asem = model.AssemblyID;
                int Sessi = model.SessionID;
                mSession Mdl = new mSession();
                Mdl.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
                Mdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                mAssembly assmblyMdl = new mAssembly();
                assmblyMdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCodeHindi", Mdl);
                // model.DepartmentId = Id;
                // model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDepartment", model);
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDiaryNoForPdfDiaryWiseStarred", model);

                model.Updatedate = DateTime.Now;
                if (model.tQuestionModel != null)
                {

                    foreach (var item in model.tQuestionModel)
                    {
                        model.DiaryNo = item.DiaryNumber;
                        // model.DepartmentId = item.DepartmentId;
                        model = (tMemberNotice)Helper.ExecuteService("Notice", "GetAllDetailsByDiaryNo", model);
                        //string LOBName = item.DepartmentId;



                        MemoryStream output = new MemoryStream();

                        EvoPdf.Document document1 = new EvoPdf.Document();

                        // set the license key

                        string LOBName = "";
                        document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                        document1.CompressionLevel = PdfCompressionLevel.Best;
                        document1.Margins = new Margins(0, 0, 0, 0);

                        LOBName = item.DiaryNumber.Trim();
                        if (model.tQuestionModel != null)
                        {
                            foreach (var cc in model.tQuestionModel)
                            {

                                outXml = @"<html >                           
                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                outXml += @"<div> 


                    </div>
            
<table style='width:100%'>
 
      <tr>
  <td style='text-align:left;font-size:22px;' >
            तारांकित   
              </td>
        
<td style='text-align:Right;font-size:22px;' >
            वीO एसO एसO (मिसO) 2   
              </td>
              </tr>
      </table>
<table style='width:100%'>
 
      <tr>
  <td style='text-align:center;font-size:28px;' >
            विधान सभा प्रश्न   
              </td>
              </tr>
      </table>
 <table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           कुल प्रश्न - " + model.TotalQCount + @"
            
              </td>
         
              </tr>
           </table>



<table style='width:100%'>
     <tr>
          <td style='text-align:justify;font-size:20px' >
           ----
            
              </td>
         
              </tr>
           </table>";
                            }
                            foreach (var cc in model.tQuestionModel)
                            {
                                if (cc.IsClubbed != null)
                                {
                                    if (cc.IsHindi == true)
                                    {
                                        string[] values = cc.CMemNameHindi.Split(',');
                                        outInnerXml = "";
                                        for (int i = 0; i < values.Length; i++)
                                        {
                                            values[i] = values[i].Trim();
                                            outInnerXml += @"<div>
          " + values[i] + @" 

                               </div>
                             ";
                                        }
                                    }
                                    else
                                    {
                                        string[] values = cc.CMemName.Split(',');
                                        outInnerXml = "";
                                        for (int i = 0; i < values.Length; i++)
                                        {
                                            values[i] = values[i].Trim();
                                            outInnerXml += @"<div>
          " + values[i] + @" 

                               </div>
                             ";
                                        }
                                    }


                                    if (cc.IsHindi == true)
                                    {
                                        outXml += @"<table style='width:100%'>

    <table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             तारांकित डायरी संख्या : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             विषय:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          सूचना प्राप्ति दिनांक : " + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
          स्वीकृत सूचना दिनांक  :" + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + outInnerXml + @"
</td>
         
              </tr>
           </table>
<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  क्या " + cc.MinistryNameLocal + @" बतलाने की कृपा करेंगी कि:-
</td>

         
              </tr>
   
           </table>

<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";


                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                    else
                                    {
                                        outXml += @"<table style='width:100%'>

    <table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             Starred D. No. : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             subject:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          Notice Recieved on: " + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
         Notice of Admission sent on  :" + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + outInnerXml + @"
</td>
         
              </tr>
           </table>
<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  Will the " + cc.MinistryName + @" be Pleased to state :
</td>

         
              </tr>
   
           </table>

<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";


                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                }
                                else
                                {
                                    if (cc.IsHindi == true)
                                    {
                                        outXml += @"<table style='width:100%'>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             तारांकित डायरी संख्या : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             विषय:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          सूचना प्राप्ति दिनांक : " + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
          स्वीकृत सूचना दिनांक  :" + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + cc.MemberNameLocal + "(" + cc.ConstituencyName_Local + ")" + @"
</td>
         
              </tr>
           </table>
<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  क्या " + cc.MinistryNameLocal + @" बतलाने की कृपा करेंगी कि:-
</td>

         
              </tr>
   
           </table>

<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";

                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                    else
                                    {
                                        outXml += @"<table style='width:100%'>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             Starred D. No. : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             subject:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          Notice Recieved on: " + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
         Notice of Admission sent on  :" + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + cc.MemberName + "(" + cc.ConstituencyName + ")" + @"
</td>
         
              </tr>
           </table>

<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  Will the " + cc.MinistryName + @" be Pleased to state :
</td>

         
              </tr>
   
           </table>
<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";

                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        string baseURL = "";
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);


                                    }
                                }
                            }
                            outXml += @"



                   
                
                     </body> </html>";
                        }

                        byte[] pdfBytes = document1.Save();

                        try
                        {
                            output.Write(pdfBytes, 0, pdfBytes.Length);
                            output.Position = 0;

                        }
                        finally
                        {
                            // close the PDF document to release the resources
                            document1.Close();
                        }
                        foreach (var dd in model.tQuestionModel)
                        {
                            if (dd.MainQuestion != null)
                            {
                                string url = "/QuestionList/" + "DiaryWiseStarred/" + Asem + "/" + Sessi + "/";

                                string LOBN = "";
                                LOBN = LOBName.Replace('>', ' ');
                                LOBN = LOBN.Trim();

                                //if (LOBName.IndexOf('>') > 0)
                                //{
                                //    LOBN = LOBName.Substring(1);
                                //    LOBN=LOBN.Trim();
                                //}
                                //else
                                //{
                                //    LOBN = LOBN.Trim();
                                //}

                                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + LOBN.Replace("/", "_") + ".pdf");

                                string directory = Server.MapPath(url);
                                if (!Directory.Exists(directory))
                                {
                                    Directory.CreateDirectory(directory);
                                }
                                fileName = LOBN.Replace("/", "_") + "_S_" + ".pdf";
                                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                                // close file stream
                                _FileStream.Close();
                                //model.FilePath = url + fileName;
                                //string value = model.DepartmentName;
                                //model.DepartmentName = value.Replace('>', ' ');
                                //    var msg = Helper.ExecuteService("Notice", "UpdatemDepartment", model) as tMemberNotice;
                                // var msg = Helper.ExecuteService("Notice", "UpdatemDepartmentPdfPath", model) as tMemberNotice;

                            }
                        }
                    }



                }
            }
            //



            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }


        public object DownloadStarredQuestions()
        {

            try
            {

                string url = "/QuestionList" + "/DiaryWiseStarred/" + "12/" + "7";

                string directory = Server.MapPath(url);


                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    //zip.AddDirectoryByName("Files");


                    if (Directory.Exists(directory))
                    {
                        zip.AddDirectory(directory, "QuestionList");
                    }
                    else
                    {

                    }



                    Response.Clear();
                    Response.BufferOutput = false;
                    string zipName = String.Format("{0}.zip", "StarredQuestions");
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    Response.End();

                    return Response;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return "No File Available!";
            }

#pragma warning disable CS0162 // Unreachable code detected
            return Response;
#pragma warning restore CS0162 // Unreachable code detected
        }


        public object DownloadUnStarredQuestions()
        {

            try
            {

                string url = "/QuestionList" + "/DiarywiseBeforeUnstarred/" + "12/" + "7";

                string directory = Server.MapPath(url);


                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    //zip.AddDirectoryByName("Files");


                    if (Directory.Exists(directory))
                    {
                        zip.AddDirectory(directory, "QuestionList");
                    }
                    else
                    {

                    }



                    Response.Clear();
                    Response.BufferOutput = false;
                    string zipName = String.Format("{0}.zip", "UnStarredQuestions");
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    Response.End();

                    return Response;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return "No File Available!";
            }

#pragma warning disable CS0162 // Unreachable code detected
            return Response;
#pragma warning restore CS0162 // Unreachable code detected
        }

        public ActionResult UnstarredPdfDiaryWise()
        {
            tMemberNotice model = new tMemberNotice();

            //objModel.NoticeRecievedDate = date;

            // UnstarredGenerateDiarywisePdf();
            // model.Message = "Question List Generated";

            return PartialView("_DiaryWiseUnstarred", model);
        }

        [HttpGet]
        public ActionResult PdfUnstarredDiaryWise()
        {
            tMemberNotice model = new tMemberNotice();

            //objModel.NoticeRecievedDate = date;

            UnstarredGenerateDiarywisePdf();

            string res = "Question List Generated";

            return Json(res, JsonRequestBehavior.AllowGet);
        }


        public void UnstarredGenerateDiarywisePdf()
        {
            try
            {
                string outXml = "";
                string outInnerXml = "";

#pragma warning disable CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
                string CurrentSecretery = "";
#pragma warning restore CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SessionDate' is assigned but its value is never used
                DateTime SessionDate = new DateTime();
#pragma warning restore CS0219 // The variable 'SessionDate' is assigned but its value is never used
                string fileName = "";
                string savedPDF = string.Empty;

                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                //   iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);


                tMemberNotice model = new tMemberNotice();
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                int Asem = model.AssemblyID;
                int Sessi = model.SessionID;
                mSession Mdl = new mSession();
                Mdl.SessionCode = model.SessionID;
                Mdl.AssemblyID = model.AssemblyID;
                mAssembly assmblyMdl = new mAssembly();
                assmblyMdl.AssemblyID = model.AssemblyID;
                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCodeHindi", Mdl);
                // model.DepartmentId = Id;
                model = (tMemberNotice)Helper.ExecuteService("Notice", "UnstarredGetDiaryNoForPdfDiaryWiseStarred", model);
                model.Updatedate = DateTime.Now;
                if (model.tQuestionModel != null)
                {

                    foreach (var item in model.tQuestionModel)
                    {
                        //model.DepartmentName = item.DepartmentName;
                        //model.DepartmentId = item.DepartmentId;
                        model.DiaryNo = item.DiaryNumber;
                        model = (tMemberNotice)Helper.ExecuteService("Notice", "UnstarredGetAllDetailsByDiaryNo", model);
                        //string LOBName = item.DepartmentId;



                        MemoryStream output = new MemoryStream();

                        EvoPdf.Document document1 = new EvoPdf.Document();

                        // set the license key

                        string LOBName = "";
                        document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                        document1.CompressionLevel = PdfCompressionLevel.Best;
                        document1.Margins = new Margins(0, 0, 0, 0);
                        LOBName = item.DiaryNumber.Trim();
                        if (model.tQuestionModel != null)
                        {
                            foreach (var cc in model.tQuestionModel)
                            {

                                outXml = @"<html >                           
                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                outXml += @"<div> 


                    </div>
            
<table style='width:100%'>
 
      <tr>
  <td style='text-align:left;font-size:22px;' >
            अतारांकित   
              </td>
        
<td style='text-align:Right;font-size:22px;' >
            वीO एसO एसO (मिसO) 2   
              </td>
              </tr>
      </table>
<table style='width:100%'>
 
      <tr>
  <td style='text-align:center;font-size:28px;' >
            विधान सभा प्रश्न   
              </td>
              </tr>
      </table>
 <table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           कुल प्रश्न - " + model.TotalQCount + @"
            
              </td>
         
              </tr>
           </table>



<table style='width:100%'>
     <tr>
          <td style='text-align:justify;font-size:20px' >
           ----
            
              </td>
         
              </tr>
           </table>";
                            }
                            foreach (var cc in model.tQuestionModel)
                            {
                                if (cc.IsClubbed != null)
                                {
                                    if (cc.IsHindi == true)
                                    {
                                        string[] values = cc.CMemNameHindi.Split(',');
                                        for (int i = 0; i < values.Length; i++)
                                        {
                                            values[i] = values[i].Trim();
                                            outInnerXml += @"<div>
          " + values[i] + @" 

                               </div>
                             ";
                                        }
                                    }
                                    else
                                    {
                                        string[] values = cc.CMemName.Split(',');
                                        for (int i = 0; i < values.Length; i++)
                                        {
                                            values[i] = values[i].Trim();
                                            outInnerXml += @"<div>
          " + values[i] + @" 

                               </div>
                             ";
                                        }
                                    }


                                    if (cc.IsHindi == true)
                                    {
                                        outXml += @"<table style='width:100%'>

    <table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             अतारांकित डायरी संख्या : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             विषय:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          सूचना प्राप्ति दिनांक : " + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
          स्वीकृत सूचना दिनांक  :" + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + outInnerXml + @"
</td>
         
              </tr>
           </table>

<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  क्या " + cc.MinistryNameLocal + @" बतलाने की कृपा करेंगी कि:-
</td>

         
              </tr>
   
           </table>
<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";


                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                    else
                                    {
                                        outXml += @"<table style='width:100%'>

    <table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             Unstarred D. No. : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             subject:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          Notice Recieved on: " + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
         Notice of Admission sent on  :" + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + outInnerXml + @"
</td>
         
              </tr>
           </table>

</table>

<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  Will the " + cc.MinistryName + @" be Pleased to state :
</td>

         
              </tr>
   
           </table>
<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";


                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                }
                                else
                                {
                                    if (cc.IsHindi == true)
                                    {
                                        outXml += @"<table style='width:100%'>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             अतारांकित डायरी संख्या : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             विषय:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          सूचना प्राप्ति दिनांक : " + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
          स्वीकृत सूचना दिनांक  :" + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + cc.MemberNameLocal + "(" + cc.ConstituencyName_Local + ")" + @"
</td>
         
              </tr>
           </table>
<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  क्या " + cc.MinistryNameLocal + @" बतलाने की कृपा करेंगी कि:-
</td>

         
              </tr>
   
           </table>

<table style='width:100%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";

                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                    else
                                    {
                                        outXml += @"<table style='width:100%'>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             Unstarred D. No. : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             subject:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          Notice Recieved on: " + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
         Notice of Admission sent on  :" + Convert.ToDateTime(cc.NoticeDate).ToShortDateString() + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + cc.MemberName + "(" + cc.ConstituencyName + ")" + @"
</td>
         
              </tr>
           </table>

</table>

<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  Will the " + cc.MinistryName + @" be Pleased to state :
</td>

         
              </tr>
   
           </table>
<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";

                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        string baseURL = "";
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);


                                    }
                                }
                            }
                            outXml += @"



                   
                
                     </body> </html>";
                        }

                        byte[] pdfBytes = document1.Save();

                        try
                        {
                            output.Write(pdfBytes, 0, pdfBytes.Length);
                            output.Position = 0;

                        }
                        finally
                        {
                            // close the PDF document to release the resources
                            document1.Close();
                        }
                        foreach (var dd in model.tQuestionModel)
                        {
                            if (dd.MainQuestion != null)
                            {
                                string url = "/QuestionList/" + "DiarywiseBeforeUnstarred/" + Asem + "/" + Sessi + "/";
                                string LOBN = "";
                                LOBN = LOBName.Replace('>', ' ');
                                LOBN = LOBN.Trim();
                                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + LOBN.Replace("/", "_") + ".pdf");

                                string directory = Server.MapPath(url);
                                if (!Directory.Exists(directory))
                                {
                                    Directory.CreateDirectory(directory);
                                }
                                fileName = LOBN.Replace("/", "_") + "_U_" + ".pdf";
                                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                                // close file stream
                                _FileStream.Close();
                                //model.FilePath = url + fileName;
                                //string value = model.DepartmentName;
                                //model.DepartmentName = value.Replace('>', ' ');
                                // var msg = Helper.ExecuteService("Notice", "UpdatemDepartmentUnstarred", model) as tMemberNotice;
                                // var msg = Helper.ExecuteService("Notice", "UpdatemDepartmentPdfUnstarred", model) as tMemberNotice;
                            }
                        }
                    }



                }
            }
            //



            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }



        public JsonResult Approved(string Id)
        {

            tMemberNotice model = new tMemberNotice();
            string strDate = Id;
            DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
            model.NoticeDate = date;
            model = Helper.ExecuteService("Notice", "ApprovedSessionDate", model) as tMemberNotice;
            //model.Message = "Approved Sucessfully";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        public JsonResult UnstaredApproved(string Id)
        {

            tMemberNotice model = new tMemberNotice();
            string strDate = Id;
            DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
            model.NoticeDate = date;
            model = Helper.ExecuteService("Notice", "UnstaredApprovedSessionDate", model) as tMemberNotice;
            //model.Message = "Approved Sucessfully";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        public ActionResult DepwiseSHtml()
        {
            tMemberNotice model = new tMemberNotice();
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            //   model.MinisterName = (string)Helper.ExecuteService("LegislationFixation", "GetRotationalMiniterName", model);
            // model = (SBL.DomainModel.Models.Notice.tMemberNotice)Helper.ExecuteService("Notice", "GetSessionDates", model);
            //model = (SBL.DomainModel.Models.Notice.tMemberNotice)Helper.ExecuteService("Notice", "GetPdfForBeforeFix", model);
            // model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDepartmentByValue", model);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetValuemDepartmentPdf", model);
            // List<tMemberNotice> DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartment", model) as List<tMemberNotice>;
            model.RoleID = CurrentSession.RoleID;
            return PartialView("_deptListS", model);
        }



        //Unstarred DeptWise Pdf Generation

        public ActionResult UnstaredDepwiseSHtml()
        {
            tMemberNotice model = new tMemberNotice();
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            //   model.MinisterName = (string)Helper.ExecuteService("LegislationFixation", "GetRotationalMiniterName", model);
            // model = (SBL.DomainModel.Models.Notice.tMemberNotice)Helper.ExecuteService("Notice", "GetSessionDates", model);
            //model = (SBL.DomainModel.Models.Notice.tMemberNotice)Helper.ExecuteService("Notice", "GetPdfForBeforeFix", model);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDepartmentByValueUnstared", model);
            // List<tMemberNotice> DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartment", model) as List<tMemberNotice>;
            model.RoleID = CurrentSession.RoleID;
            return PartialView("_deptListUnStarred", model);
        }
        public ActionResult CallPdfDeptUnstarred()
        {
            tMemberNotice model = new tMemberNotice();

            //objModel.NoticeRecievedDate = date;

            UnstarredGenerateDeptbyPdf();
            model.Message = "Question List Generated";

            return PartialView("_deptListUnStarred", model);
        }
        public void UnstarredGenerateDeptbyPdf()
        {
            try
            {
                string outXml = "";
                string outInnerXml = "";

#pragma warning disable CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
                string CurrentSecretery = "";
#pragma warning restore CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SessionDate' is assigned but its value is never used
                DateTime SessionDate = new DateTime();
#pragma warning restore CS0219 // The variable 'SessionDate' is assigned but its value is never used
                string fileName = "";
                string savedPDF = string.Empty;

                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                // iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);


                tMemberNotice model = new tMemberNotice();
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                int Asem = model.AssemblyID;
                int Sessi = model.SessionID;
                mSession Mdl = new mSession();
                Mdl.SessionCode = model.SessionID;
                Mdl.AssemblyID = model.AssemblyID;
                mAssembly assmblyMdl = new mAssembly();
                assmblyMdl.AssemblyID = model.AssemblyID;
                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCodeHindi", Mdl);
                // model.DepartmentId = Id;
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDepartment", model);
                model.Updatedate = DateTime.Now;
                if (model.tQuestionModel != null)
                {

                    foreach (var item in model.tQuestionModel)
                    {
                        model.DepartmentName = item.DepartmentName;
                        model.DepartmentId = item.DepartmentId;
                        model = (tMemberNotice)Helper.ExecuteService("Notice", "UnstarredGetAllDetailsForDeptwise", model);
                        //string LOBName = item.DepartmentId;



                        MemoryStream output = new MemoryStream();

                        EvoPdf.Document document1 = new EvoPdf.Document();

                        // set the license key

                        string LOBName = "";
                        document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                        document1.CompressionLevel = PdfCompressionLevel.Best;
                        document1.Margins = new Margins(0, 0, 0, 0);
                        LOBName = item.DepartmentName.Trim();
                        if (model.tQuestionModel != null)
                        {
                            foreach (var cc in model.tQuestionModel)
                            {

                                outXml = @"<html >                           
                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                outXml += @"<div> 


                    </div>
            
<table style='width:100%'>
 
      <tr>
  <td style='text-align:left;font-size:22px;' >
            अतारांकित   
              </td>
        
<td style='text-align:Right;font-size:22px;' >
            वीO एसO एसO (मिसO) 2   
              </td>
              </tr>
      </table>
<table style='width:100%'>
 
      <tr>
  <td style='text-align:center;font-size:28px;' >
            विधान सभा प्रश्न   
              </td>
              </tr>
      </table>
 <table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           कुल प्रश्न - " + model.TotalQCount + @"
            
              </td>
         
              </tr>
           </table>



<table style='width:100%'>
     <tr>
          <td style='text-align:justify;font-size:20px' >
           ----
            
              </td>
         
              </tr>
           </table>";
                            }
                            foreach (var cc in model.tQuestionModel)
                            {
                                if (cc.IsClubbed != null)
                                {
                                    if (cc.IsHindi == true)
                                    {
                                        string[] values = cc.CMemNameHindi.Split(',');
                                        for (int i = 0; i < values.Length; i++)
                                        {
                                            values[i] = values[i].Trim();
                                            outInnerXml += @"<div>
          " + values[i] + @" 

                               </div>
                             ";
                                        }
                                    }
                                    else
                                    {
                                        string[] values = cc.CMemName.Split(',');
                                        for (int i = 0; i < values.Length; i++)
                                        {
                                            values[i] = values[i].Trim();
                                            outInnerXml += @"<div>
          " + values[i] + @" 

                               </div>
                             ";
                                        }
                                    }


                                    if (cc.IsHindi == true)
                                    {
                                        outXml += @"<table style='width:100%'>

    <table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             अतारांकित डायरी संख्या : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             विषय:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          सूचना प्राप्ति दिनांक : " + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
          स्वीकृत सूचना दिनांक  :" + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + outInnerXml + @"
</td>
         
              </tr>
           </table>

<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  क्या " + cc.MinistryNameLocal + @" बतलाने की कृपा करेंगी कि:-
</td>

         
              </tr>
   
           </table>
<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";


                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                    else
                                    {
                                        outXml += @"<table style='width:100%'>

    <table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             Unstarred D. No. : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             subject:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          Notice Recieved on: " + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
         Notice of Admission sent on  :" + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + outInnerXml + @"
</td>
         
              </tr>
           </table>

</table>

<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  Will the " + cc.MinistryName + @" be Pleased to state :
</td>

         
              </tr>
   
           </table>
<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";


                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                }
                                else
                                {
                                    if (cc.IsHindi == true)
                                    {
                                        outXml += @"<table style='width:100%'>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             अतारांकित डायरी संख्या : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             विषय:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          सूचना प्राप्ति दिनांक : " + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
          स्वीकृत सूचना दिनांक  :" + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + cc.MemberNameLocal + "(" + cc.ConstituencyName_Local + ")" + @"
</td>
         
              </tr>
           </table>
<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  क्या " + cc.MinistryNameLocal + @" बतलाने की कृपा करेंगी कि:-
</td>

         
              </tr>
   
           </table>

<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";

                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        string baseURL = "";

                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);

                                    }
                                    else
                                    {
                                        outXml += @"<table style='width:100%'>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             Unstarred D. No. : " + cc.DiaryNumber + @"
              </td>
       
              </tr>
           </table>
<table style='width:100%'>
     <tr>
      <td style='text-align:left;font-size:20px;' >
             subject:  " + cc.Subject + @"
              </td>
        
              </tr>
           </table>

<table style='width:100%'>
      <tr>
          <td style='text-align:left;font-size:20px;' >
          Notice Recieved on: " + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"      
              </td>
 
 <td style='text-align:left;font-size:20px;' >
         Notice of Admission sent on  :" + Convert.ToDateTime(cc.NoticeDate).ToString("dd/MM/yyyy") + @"
              </td>
 
         
              </tr>
       
<table style='width:80%'>
     <tr>
         <td style='text-align:left;vertical-align: top; font-size:20px;'>
                " + "*" + cc.DiaryNumber + @"
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
" + cc.MemberName + "(" + cc.ConstituencyName + ")" + @"
</td>
         
              </tr>
           </table>

</table>

<table style='width:100%'>
 <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>
            
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  Will the " + cc.MinistryName + @" be Pleased to state :
</td>

         
              </tr>
   
           </table>
<table style='width:80%'>
     <tr>
         <td style='text-align:left; vertical-align: top; font-size:20px;'>

                " + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + @"
               
              </td>
<td style='text-align:left; font-size:20px;padding-bottom: 25px;'>
  " + cc.MainQuestion + @"
</td>

         
              </tr>
           </table>

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";

                                        EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                        AddElementResult addResult;

                                        HtmlToPdfElement htmlToPdfElement;
                                        outXml += @"
                     </body> </html>";
                                        string htmlStringToConvert = outXml;
                                        string baseURL = "";
                                        outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                        htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                        //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                        addResult = page.AddElement(htmlToPdfElement);


                                    }
                                }
                            }
                            outXml += @"



                   
                
                     </body> </html>";
                        }

                        byte[] pdfBytes = document1.Save();

                        try
                        {
                            output.Write(pdfBytes, 0, pdfBytes.Length);
                            output.Position = 0;

                        }
                        finally
                        {
                            // close the PDF document to release the resources
                            document1.Close();
                        }
                        foreach (var dd in model.tQuestionModel)
                        {
                            if (dd.MainQuestion != null)
                            {
                                string url = "/QuestionList/" + "BeforeFixationUnstarred/" + Asem + "/" + Sessi + "/";
                                string LOBN = "";
                                LOBN = LOBName.Replace('>', ' ');
                                LOBN = LOBN.Trim();
                                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + LOBN.Replace("/", "_") + ".pdf");

                                string directory = Server.MapPath(url);
                                if (!Directory.Exists(directory))
                                {
                                    Directory.CreateDirectory(directory);
                                }
                                fileName = LOBN.Replace("/", "_") + "_U_" + ".pdf";
                                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                                // close file stream
                                _FileStream.Close();
                                model.FilePath = url + fileName;
                                string value = model.DepartmentName;
                                model.DepartmentName = value.Replace('>', ' ');
                                // var msg = Helper.ExecuteService("Notice", "UpdatemDepartmentUnstarred", model) as tMemberNotice;
                                var msg = Helper.ExecuteService("Notice", "UpdatemDepartmentPdfUnstarred", model) as tMemberNotice;
                            }
                        }
                    }



                }
            }
            //



            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }

        public ActionResult DepwiseNotices()
        {
            tMemberNotice model = new tMemberNotice();
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            //   model.MinisterName = (string)Helper.ExecuteService("LegislationFixation", "GetRotationalMiniterName", model);
            // model = (SBL.DomainModel.Models.Notice.tMemberNotice)Helper.ExecuteService("Notice", "GetSessionDates", model);
            //model = (SBL.DomainModel.Models.Notice.tMemberNotice)Helper.ExecuteService("Notice", "GetPdfForBeforeFix", model);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetNoticesForView", model);
            // List<tMemberNotice> DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartment", model) as List<tMemberNotice>;
            model.RoleID = CurrentSession.RoleID;
            return PartialView("_NoticesForPdf", model);
        }


        public ActionResult CallPdfForNotices()
        {
            tMemberNotice model = new tMemberNotice();

            //objModel.NoticeRecievedDate = date;

            GenerateDeptForNotices();
            model.Message = "Question List Generated";

            return PartialView("_deptListS", model);
        }

        public void GenerateDeptForNotices()
        {
            try
            {
                string outXml = "";
#pragma warning disable CS0219 // The variable 'outInnerXml' is assigned but its value is never used
                string outInnerXml = "";
#pragma warning restore CS0219 // The variable 'outInnerXml' is assigned but its value is never used
                int PagesToApplySignatureID = 1;
                int PositionToApplySignatureID = 4;
#pragma warning disable CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
                string CurrentSecretery = "";
#pragma warning restore CS0219 // The variable 'CurrentSecretery' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SessionDate' is assigned but its value is never used
                DateTime SessionDate = new DateTime();
#pragma warning restore CS0219 // The variable 'SessionDate' is assigned but its value is never used
                string fileName = "";

                string savedPDF = string.Empty;

                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                //    iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);


                tMemberNotice model = new tMemberNotice();
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                int Asem = model.AssemblyID;
                int Sessi = model.SessionID;
                mSession Mdl = new mSession();
                Mdl.SessionCode = model.SessionID;
                Mdl.AssemblyID = model.AssemblyID;
                mAssembly assmblyMdl = new mAssembly();
                assmblyMdl.AssemblyID = model.AssemblyID;
                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

                model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCodeHindi", Mdl);
                // model.DepartmentId = Id;
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetNoticesForGeneratedPdf", model);
                model.Updatedate = DateTime.Now;
                string path1 = "";
                string Files = "";
                string Prifix = "";
                if (model.tQuestionModel != null)
                {

                    foreach (var item in model.tQuestionModel)
                    {

                        model.NoticeId = item.NoticeId;
                        // model.DepartmentId = item.DepartmentId;
                        model = (tMemberNotice)Helper.ExecuteService("Notice", "GetDetailsForNoticesPDF", model);

                        MemoryStream output = new MemoryStream();

                        EvoPdf.Document document1 = new EvoPdf.Document();

                        // set the license key

                        string LOBName = "";
                        //LOBName = "test";
                        document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                        document1.CompressionLevel = PdfCompressionLevel.Best;
                        document1.Margins = new Margins(0, 0, 0, 0);
                        LOBName = item.NoticeId.ToString();
                        if (model.tQuestionModel != null)
                        {
                            foreach (var cc in model.tQuestionModel)
                            {

                                outXml = @"<html >                           
                                 <body style='font-family:DVOT-Yogesh;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                                outXml += @"<div> 


                    </div>
            
<br />
    <br />
<table style='width:90%'>
 
      <tr>
          <td style='text-align:right;font-size:14px;' >
             विधान सभा कार्य
              <br />
                     तुरन्त	

              </td>
        
              </tr>
      </table>
<table style='width:100%'>
 
      <tr>
          <td style='text-align:center;font-size:20px;' >
             हिमाचल प्रदेश विधान सभा सचिवालय
              </td>
              </tr>
      </table>
<br />
<br />
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; संख्या वि. स. –विधायन-नियम- " + cc.EventName + @" /1-7/2014
              </td>
              </tr>
           </table>
<br />

<table style='width:30%'>
     <tr>
          <td style='text-align:center;font-size:20px;'>
         प्रेषक :           
              </td>    
              </tr>
           </table>
<table style='width:58%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
         सचिव,
              <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp हिमाचल प्रदेश विधान सभा।
        
              </td>    
              </tr>
           </table>
<table style='width:30%'>
     <tr>
          <td style='text-align:center;font-size:20px;'>
         प्रेषक :           
              </td>    
              </tr>
           </table>
<table style='width:60%'>
     <tr>
          <td style='text-align:center;font-size:20px;'>
       
        " + cc.MinisterName + @" 
<br>
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; माननीय " + cc.MinistryName + @" 
<br>
हिमाचल प्रदेश
        
              </td>    
              </tr>
           </table>

<table style='width:70%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
       
        शिमला-171004,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	दिनांक :
        
              </td>    
              </tr>
           </table>
<br />
<br />

<table style='width:90%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
         विषय :--नियम " + cc.EventName + @" के अन्तर्गत प्राप्त प्रस्ताव/नोटिस से सम्बन्धित‍(दै0 सं0 " + cc.NoticeNumber + @")।         
              </td>    
              </tr>
           </table>
<br />
<table style='width:30%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
         महोदय,           
              </td>    
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
       &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;  " + cc.MemberName + @" विधायक से हिमाचल प्रदेश विधान सभा की प्रक्रिया एवं कार्य <br />
                 
              </td>   
        
              </tr>
           </table>
<table style='width:93%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
       संचालन नियमावली के नियम " + cc.EventName + @" के अनतर्गत प्रस्ताव/नोटिस की एक प्रति मुझे आपको <br />
                 
              </td>   
        
              </tr>
           </table>
           
<table style='width:56%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
       सूचनार्थ संलग्न  करने का निदेश हुआ है।   
                 
              </td>   
        
              </tr>
           </table>
   
          
<br />
<br />
<br /><br />
<br />
<br /><br />
<br />
<br />
<br />
<br /><br />
<table style='width:85%'>
     <tr>
          <td style='text-align:right;font-size:20px;' >
      भवदीय,   
                 
              </td>   
        
              </tr>
           </table>

<br /><br />
<br />
<br />
<br />
<table style='width:85%'>
     <tr>
          <td style='text-align:right;font-size:20px;' >
     सचिव, हिमाचल प्रदेश विधान सभा। 
                 
              </td>   
        
              </tr>
           </table>
";





                                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                                AddElementResult addResult;

                                //docWorkingDocument.Add(addLogo);
                                HtmlToPdfElement htmlToPdfElement;
                                //                                        outXml += @"
                                //                     </body> </html>";
                                string htmlStringToConvert = outXml;
                                outXml = " <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
                                string baseURL = "";

                                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                                //htmlToPdfElement.BackColor = System.Drawing.Color.SkyBlue;


                                addResult = page.AddElement(htmlToPdfElement);


                            }

                            outXml += @"



                   
                
                     </body> </html>";
                        }

                        byte[] pdfBytes = document1.Save();

                        try
                        {
                            output.Write(pdfBytes, 0, pdfBytes.Length);
                            output.Position = 0;

                        }
                        finally
                        {
                            // close the PDF document to release the resources
                            document1.Close();
                        }
                        foreach (var dd in model.tQuestionModel)
                        {
                            if (dd.NoticeNumber != null)
                            {
                                string url = "/QuestionList/" + "Notices/" + Asem + "/" + Sessi + "/";
                                string LOBN = "";
                                LOBN = LOBName.Replace('>', ' ');
                                LOBN = LOBN.Trim();
                                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + LOBN.Replace("/", "_") + ".pdf");

                                string directory = Server.MapPath(url);
                                if (!Directory.Exists(directory))
                                {
                                    Directory.CreateDirectory(directory);
                                }
                                fileName = LOBN.Replace("/", "_") + "_N_" + ".pdf";
                                var path = Path.Combine(Server.MapPath("~" + url), fileName);
                                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                                // close file stream
                                _FileStream.Close();
                                model.FilePath = url + fileName;
                                model.NoticeId = model.NoticeId;
                                var msg = Helper.ExecuteService("Notice", "UpdateTmemberFornotices", model) as tMemberNotice;

                                path1 = model.FilePath;
                                string imageFileLoc = Server.MapPath("/assets/css/img/p-trans.gif");
                                string newFileLocation = model.FilePath + "HWSigned/" + Path.GetFileNameWithoutExtension(path) + ".pdf";
                                string RFileLocation = model.FilePath + "HW/" + ".pdf";
                                string FileLocation = model.FilePath;
                                PDFExtensions.InsertImageToPdf(Server.MapPath(path1), imageFileLoc, Server.MapPath(newFileLocation), PositionToApplySignatureID, PagesToApplySignatureID);

                                path1 = path1.Replace(" ", "%20");
                                var test = Request.Url.AbsoluteUri;
                                string[] abc = test.Split('/');
                                Prifix = abc[0] + "//" + abc[2];
                                if (Files == "")
                                {
                                    Files = Prifix + path1;
                                }
                                else
                                {
                                    Files = Files + "," + Prifix + path1;
                                }


                            }
                        }
                    }



                }

            }

            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }


        //Code Merged
        public string S_GeneratePdfByDate_ByViewForAdminPdf(string Id, string ViewBy)
        {
            try
            {
                string outXml = "";
                string outInnerXml = "";
                string LOBName = Id;
                string fileName = "";
                string savedPDF = string.Empty;
                string HeadingText = "";
                string HeadingTextEnglish = "";

                if (ViewBy == "CQ")
                {
                    HeadingText = "मौखिक उत्तर हेतु प्रश्न";
                    HeadingTextEnglish = "Questions For Oral Answer";
                }
                else if (ViewBy == "PQ")
                {
                    HeadingText = "मौखिक उत्तर हेतु स्थगित प्रश्न";
                    HeadingTextEnglish = "Postponed Questions For Oral Answer";
                }

                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);



                tMemberNotice model = new tMemberNotice();
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                string strDate = Id;
                DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
                model.NoticeDate = date;
                model.DataStatus = ViewBy;
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetAllDetailsForSpdf", model);


                if (model.tQuestionModel != null)
                {
                    foreach (var item in model.tQuestionModel)
                    {
                        model.SessionDateId = item.SessionDateId;
                        model.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", model);
                        model.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", model);
                    }
                }
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", model);
                foreach (var item in model.Values)
                {
                    model.SesNameEnglish = item.SesNameEnglish;
                }


                if (model.tQuestionModel != null)
                {

                    foreach (var item in model.tQuestionModel)
                    {
                        if (model.HeaderHindi == true)
                        {
                            outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            outXml += @"<div> 

 <style type='text/css'>
@page {
    @bottom-left {
        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
        font-family: sans-serif;
        font-size: 80%;
    }
    @bottom-right {
        content: 'Page ' counter(page) ' of ' counter(pages);
        font-family: sans-serif;
        font-size: 80%;
    }
}
</style>
                    </div>
            
<table style='width:100%'>
 
      <tr>
          <td style='text-align:center;font-size:28px;color:#3621E8;' >
             <b>हिमाचल प्रदेश तेरहवीं विधान सभा</b>
              </td>
              </tr>
      </table>
<br />
<br />
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
            <b>" + "(" + model.SessionName + ")" + @"</b> 
              </td>
              </tr>
           </table>
<br />
<br />
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;'>
            <b>" + HeadingText + @"</b>          
              </td>    
              </tr>
           </table>
<table style='width:100%'>
      <tr>
          <td style='text-align:center;font-size:28px;color:#3621E8;' >
          <b>" + item.SessionDateLocal + @"</b>   
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
            <b>" + "[" + model.MinisterName + "]" + @". </b>
            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           <b>कुल प्रश्न - " + model.TotalQCount + @"</b>
            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----
            
              </td>
              </tr>
           </table>";

                        }
                        else
                        {
                            outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            outXml += @"<div> 

 <style type='text/css'>
@page {
    @bottom-left {
        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
        font-family: sans-serif;
        font-size: 80%;
    }
    @bottom-right {
        content: 'Page ' counter(page) ' of ' counter(pages);
        font-family: sans-serif;
        font-size: 80%;
    }
}
</style>
                    </div>

            
<table style='width:100%'>
 
      <tr>
          <td style='text-align:center;font-size:28px;color:#3621E8;' >
             <b>HIMACHAL PRADESH THIRTEENTH VIDHAN SABHA</b>
              </td>
              </tr>
      </table>
<br />
<br />

<table style='width:100%'>
   <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
            <b>" + "(" + model.SesNameEnglish + ")" + @"</b> 
              </td>
              </tr>
           </table>
<br />
<br />

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;'>
            <b>" + HeadingTextEnglish + @"</b>         
              </td>    
              </tr>
           </table>
<table style='width:100%'>
      <tr>
          <td style='text-align:center;font-size:28px;color:#3621E8;' >
          <b>" + item.SessionDateEnglish + @"</b>   
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:justify;font-size:20px;color:#3621E8;' >
            <b>" + "[" + model.MinisterNameEnglish + "]" + @". </b>
            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           <b>Total No. of Questions - " + model.TotalQCount + @"</b>
            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----
            
              </td>
              </tr>
           </table>";

                        }

                    }
                    foreach (var item in model.tQuestionModel)
                    {
                        if (item.IsClubbed != null)
                        {
                            if (item.IsHindi == true && item.MinisterID != 90)
                            {
                                string[] values = item.CMemNameHindi.Split(',');
                                outInnerXml = "";
                                for (int i = 0; i < values.Length; i++)
                                {
                                    values[i] = values[i].Trim();
                                    outInnerXml += @"<div>
          " + values[i] + @" :

                               </div>
                             ";
                                }

                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
  <b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>
";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
  <b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>
";
                                }


                            }
                            else if (item.MinisterID == 90 && item.IsHindi == true)
                            {
                                string[] values = item.CMemNameHindi.Split(',');
                                outInnerXml = "";
                                for (int i = 0; i < values.Length; i++)
                                {
                                    values[i] = values[i].Trim();
                                    outInnerXml += @"<div>
          " + values[i] + @" :

                               </div>
                             ";
                                }

                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
  <b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>
";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
  <b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि:-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>
";
                                }


                            }

                            else
                            {
                                string[] values = item.CMemName.Split(',');
                                outInnerXml = "";
                                for (int i = 0; i < values.Length; i++)
                                {
                                    values[i] = values[i].Trim();
                                    outInnerXml += @"<div>
          " + values[i] + @" : 

                               </div>
                             ";
                                }
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
  <b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>
";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
  <b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
Will the <b>" + item.MinistryName + @"</b> be pleased to state :-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>
";
                                }


                            }



                        }

                        else
                        {

                            if (item.MinisterID == 90 && item.IsHindi == true)
                            {
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
</table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
  <b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>
           
  
";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
</table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
  <b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि:-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>
           
  
";
                                }

                            }
                            else if (item.IsHindi == true && item.IsClubbed != true)
                            {
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
  <b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
</p>
</td>

</tr>
</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>


";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
  <b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>
</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>


";
                                }


                            }
                            else
                            {
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");



                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
<b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>
 

";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;color:#3621E8;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;color:#3621E8;font-weight: bold;'>
<b>" + "*" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;color:#3621E8;padding-bottom: 13px;'>
<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
Will the <b>" + item.MinistryName + @"</b> be pleased to state :-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;color:#3621E8;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;color:#3621E8;' >
           ----            
              </td>
         
              </tr>
           </table></br>
 

";
                                }


                            }



                        }


                    }


                    if (model.StampTypeHindi == true)
                    {
                        foreach (var item in model.Values)
                        {

                            outXml += @"
 

<table style='width:100%'>
     <tr>
          <td style='text-align:left;font-size:20px;color:#3621E8;' >
      
<b>" + item.SignaturePlaceLocal + @".</b>
              </td>
<td style='text-align:right;font-size:20px;color:#3621E8;' >
                      <b>" + item.SignatureNameLocal + @",</b>
              </td>
         
              </tr>
<tr>
          <td style='text-align:left;font-size:20px;color:#3621E8;' >
           
         <b>दिनांक: " + item.SignatureDateLocal + @".</b>
              </td>
<td style='text-align:right;font-size:20px;color:#3621E8;' >
       
  <b>" + item.SignatureDesignationsLocal + @"।</b>          
              </td>
         
              </tr>
           </table>
       
                     </body> </html>";

                        }
                    }
                    else
                    {
                        foreach (var item in model.Values)
                        {

                            outXml += @"
 

<table style='width:100%'>
     <tr>
          <td style='text-align:left;font-size:20px;color:#3621E8;' >
      
<b>" + item.SignaturePlace + @".</b>
              </td>
<td style='text-align:right;font-size:20px;color:#3621E8;' >
                      <b>" + item.SignatureName + @",</b>
              </td>
         
              </tr>
<tr>
          <td style='text-align:left;font-size:20px;color:#3621E8;' >
           
         <b>Dated: " + item.SignatureDate + @".</b>
              </td>
<td style='text-align:right;font-size:20px;color:#3621E8;' >
       
  <b>" + item.SignatureDesignations + @".</b>          
              </td>
         
              </tr>
           </table>
       
                     </body> </html>";
                        }
                    }
                }


                string htmlStringToConvert = outXml;
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
                pdfConverter.PdfFooterOptions.FooterHeight = 60;

                //TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
                //        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                //footerTextElement.TextAlign = HorizontalTextAlign.Center;
                TextElement footerTextElement = new TextElement(0, 30, "&p;",
                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                //TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan",
                //       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                //footerTextElement1.TextAlign = HorizontalTextAlign.Left;
                //footerTextElement1.ForeColor = System.Drawing.Color.MidnightBlue;
                //footerTextElement1.TextAlign = HorizontalTextAlign.Center;

                //TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                //       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                //footerTextElement3.TextAlign = HorizontalTextAlign.Right;
                //footerTextElement3.ForeColor = System.Drawing.Color.MidnightBlue;
                //footerTextElement3.TextAlign = HorizontalTextAlign.Center;
                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
                //pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
                //pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

                string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

                ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "Blanknotapproved.png"));
                // imageElement1.KeepAspectRatio = true;
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
                ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "Blanknotapproved.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 15;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388F;

                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
                float stampWidth = float.Parse("400");
                float stampHeight = float.Parse("400");

                // Center the stamp at the top of PDF page
                float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
                float stampYLocation = 150;

                RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

                Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
                stampTemplate.AddElement(imageElement2);
                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }
                //MemoryStream output = new MemoryStream();

                //PdfConverter pdfConverter = new PdfConverter();

                //// set the license key
                //pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";


                //pdfConverter.PdfDocumentOptions.ShowFooter = true;
                //pdfConverter.PdfDocumentOptions.ShowHeader = true;

                //// set the header height in points
                //pdfConverter.PdfHeaderOptions.HeaderHeight = 60;


                //pdfConverter.PdfFooterOptions.FooterHeight = 60;
                ////write the page number
                //TextElement footerTextElement = new TextElement(0, 30, "&p;",
                //new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                //footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
                //footerTextElement.TextAlign = HorizontalTextAlign.Center;

                //pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


                //EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


                //byte[] pdfBytes = null;

                //try
                //{
                //    pdfBytes = pdfDocument.Save();
                //}
                //finally
                //{
                //    // close the Document to realease all the resources
                //    pdfDocument.Close();
                //}
                string url = "Question" + "/AfterFixation/" + model.AssemblyID + "/" + model.SessionID + "/";
                //fileName = LOBName.Replace("/", "_") + "_S_Org_" + ".pdf";
                Id = Id.Replace("/", "-");
                if (ViewBy == "CQ")
                {
                    fileName = model.AssemblyID + "_" + model.SessionID + "_S_" + Id + ".pdf";
                }
                else if (ViewBy == "PQ")
                {
                    fileName = model.AssemblyID + "_" + model.SessionID + "_S_PQ_" + Id + ".pdf";
                }
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);

                //string directory = Server.MapPath(url);
                // Filepath is using for storing file 
                string directory = model.FilePath + url;
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                //var path = Path.Combine(Server.MapPath("~" + url), fileName);
                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();
                //model.FilePath = directory + fileName;
                model.FilePath = url + fileName;

                // DocFilePath is using for Accessing File 
                CurrentSession.FileAccessPath = model.DocFilePath;
                var AccessPath = "/" + url + fileName;
                model = Helper.ExecuteService("Notice", "UpdatemSessionDate", model) as tMemberNotice;

                return "Success" + AccessPath;
            }

            catch (Exception ex)
            {

                //throw ex;
                return ex.Message;
            }
            finally
            {

            }


        }

        public string U_GeneratePdfByDate_ByViewForAdminUnstarredPDf(string Id, string ViewBy)
        {
            try
            {
                string outXml = "";
                string outInnerXml = "";
                string LOBName = Id;
                string fileName = "";
                string savedPDF = string.Empty;
                string HeadingText = "";
                string HeadingTextEnglish = "";

                if (ViewBy == "CQ")
                {
                    HeadingText = "लिखित उत्तर हेतु प्रश्न";
                    HeadingTextEnglish = "Questions For Written Answers";
                }
                else if (ViewBy == "PQ")
                {
                    HeadingText = "लिखित उत्तर हेतु स्थगित प्रश्न";
                    HeadingTextEnglish = "Postponed Questions For Written Answer";
                }


                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);



                tMemberNotice model = new tMemberNotice();
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                string strDate = Id;
                DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
                model.NoticeDate = date;
                model.DataStatus = ViewBy;
                model = (tMemberNotice)Helper.ExecuteService("Notice", "UnstaredGetAllDetailsForSpdf", model);


                if (model.tQuestionModel != null)
                {
                    foreach (var item in model.tQuestionModel)
                    {
                        model.SessionDateId = item.SessionDateId;
                        model.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", model);
                        model.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", model);
                    }
                }
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", model);
                foreach (var item in model.Values)
                {
                    model.SesNameEnglish = item.SesNameEnglish;
                }


                if (model.tQuestionModel != null)
                {

                    foreach (var item in model.tQuestionModel)
                    {
                        if (model.HeaderHindi == true)
                        {
                            outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            outXml += @"<div> 

 <style type='text/css'>
@page {
    @bottom-left {
        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
        font-family: sans-serif;
        font-size: 80%;
    }
    @bottom-right {
        content: 'Page ' counter(page) ' of ' counter(pages);
        font-family: sans-serif;
        font-size: 80%;
    }
}
</style>
                    </div>
            
<table style='width:100%'>
 
      <tr>
          <td style='text-align:center;font-size:28px;' >
             <b>हिमाचल प्रदेश तेरहवीं विधान सभा</b>
              </td>
              </tr>
      </table>
<br />
<br />
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
            <b>" + "(" + model.SessionName + ")" + @"</b> 
              </td>
              </tr>
           </table>
<br />
<br />
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;'>
            <b>" + HeadingText + @"</b>          
              </td>    
              </tr>
           </table>
<table style='width:100%'>
      <tr>
          <td style='text-align:center;font-size:28px;' >
          <b>" + item.SessionDateLocal + @"</b>   
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
            <b>" + "[" + model.MinisterName + "]" + @". </b>
            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           <b>कुल प्रश्न - " + model.TotalQCount + @"</b>
            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----
            
              </td>
              </tr>
           </table>";

                        }
                        else
                        {
                            outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                            outXml += @"<div> 

 <style type='text/css'>
@page {
    @bottom-left {
        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
        font-family: sans-serif;
        font-size: 80%;
    }
    @bottom-right {
        content: 'Page ' counter(page) ' of ' counter(pages);
        font-family: sans-serif;
        font-size: 80%;
    }
}
</style>
                    </div>

            
<table style='width:100%'>
 
      <tr>
          <td style='text-align:center;font-size:28px;' >
             <b>HIMACHAL PRADESH THIRTEENTH VIDHAN SABHA</b>
              </td>
              </tr>
      </table>
<br />
<br />

<table style='width:100%'>
   <tr>
          <td style='text-align:center;font-size:20px;' >
            <b>" + "(" + model.SesNameEnglish + ")" + @"</b> 
              </td>
              </tr>
           </table>
<br />
<br />

<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;'>
            <b>" + HeadingTextEnglish + @"</b>         
              </td>    
              </tr>
           </table>
<table style='width:100%'>
      <tr>
          <td style='text-align:center;font-size:28px;' >
          <b>" + item.SessionDateEnglish + @"</b>   
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:justify;font-size:20px;' >
            <b>" + "[" + model.MinisterNameEnglish + "]" + @". </b>
            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           <b>Total No. of Questions - " + model.TotalQCount + @"</b>
            
              </td>
         
              </tr>
           </table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----
            
              </td>
              </tr>
           </table>";

                        }

                    }
                    foreach (var item in model.tQuestionModel)
                    {
                        if (item.IsClubbed != null)
                        {
                            if (item.IsHindi == true && item.MinisterID != 90)
                            {
                                string[] values = item.CMemNameHindi.Split(',');
                                outInnerXml = "";
                                for (int i = 0; i < values.Length; i++)
                                {
                                    values[i] = values[i].Trim();
                                    outInnerXml += @"<div>
          " + values[i] + @" :

                               </div>
                             ";
                                }

                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;'>
 क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";
                                }


                            }
                            else if (item.MinisterID == 90 && item.IsHindi == true)
                            {
                                string[] values = item.CMemNameHindi.Split(',');
                                outInnerXml = "";
                                for (int i = 0; i < values.Length; i++)
                                {
                                    values[i] = values[i].Trim();
                                    outInnerXml += @"<div>
          " + values[i] + @" :

                               </div>
                             ";
                                }

                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;'>
 क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि:-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";
                                }


                            }

                            else
                            {
                                string[] values = item.CMemName.Split(',');
                                outInnerXml = "";
                                for (int i = 0; i < values.Length; i++)
                                {
                                    values[i] = values[i].Trim();
                                    outInnerXml += @"<div>
          " + values[i] + @" : 

                               </div>
                             ";
                                }
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + outInnerXml + @"</b>
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;'>
Will the <b>" + item.MinistryName + @"</b> be pleased to state :-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
";
                                }


                            }



                        }

                        else
                        {

                            if (item.MinisterID == 90 && item.IsHindi == true)
                            {
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
</table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
           
  
";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
</table>


<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;'>
क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि:-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
           
  
";
                                }

                            }
                            else if (item.IsHindi == true && item.IsClubbed != true)
                            {
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
</p>
</td>

</tr>
</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>


";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

     
          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
  <b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;'>
क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>
</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>


";
                                }


                            }
                            else
                            {
                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
                                {
                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
                                    MQues = MQues.Replace("<p>", "");
                                    MQues = MQues.Replace("</p>", "");


                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
</td>
</tr>

<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
 

";
                                }
                                else
                                {
                                    outXml += @"<table style='width:100%'>

<tr style='page-break-inside : avoid'>

          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
       
              </td>      
         
         </tr>
           </table>

<table style='width:100%;text-align:justify;'>
<tr>
<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
<b>" + item.QuestionNumber + @"</b>
</td>
<td style='font-size:20px;padding-bottom: 13px;'>
<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
</td>
</tr>


<tr>
<td>
</td>
<td style='font-size:20px;'>
Will the <b>" + item.MinistryName + @"</b> be pleased to state :-
</td>

</tr>
<tr>
<td>
</td>
<td style='font-size:20px;'>
 <p style='text-align:justify;'>
  " + WebUtility.HtmlDecode(item.MainQuestion) + @"
</p>
</td>

</tr>

</table>
<table style='width:100%'>
     <tr>
          <td style='text-align:center;font-size:20px;' >
           ----            
              </td>
         
              </tr>
           </table>
 

";
                                }


                            }



                        }


                    }


                    if (model.StampTypeHindi == true)
                    {
                        foreach (var item in model.Values)
                        {

                            outXml += @"
 

<table style='width:100%'>
     <tr>
          <td style='text-align:left;font-size:20px;' >
      
<b>" + item.SignaturePlaceLocal + @".</b>
              </td>
<td style='text-align:right;font-size:20px;' >
                      <b>" + item.SignatureNameLocal + @",</b>
              </td>
         
              </tr>
<tr>
          <td style='text-align:left;font-size:20px;' >
           
         <b>दिनांक: " + item.SignatureDateLocal + @".</b>
              </td>
<td style='text-align:right;font-size:20px;' >
       
  <b>" + item.SignatureDesignationsLocal + @"।</b>          
              </td>
         
              </tr>
           </table>
       
                     </body> </html>";

                        }
                    }
                    else
                    {
                        foreach (var item in model.Values)
                        {

                            outXml += @"
 

<table style='width:100%'>
     <tr>
          <td style='text-align:left;font-size:20px;' >
      
<b>" + item.SignaturePlace + @".</b>
              </td>
<td style='text-align:right;font-size:20px;' >
                      <b>" + item.SignatureName + @",</b>
              </td>
         
              </tr>
<tr>
          <td style='text-align:left;font-size:20px;' >
           
         <b>Dated: " + item.SignatureDate + @".</b>
              </td>
<td style='text-align:right;font-size:20px;' >
       
  <b>" + item.SignatureDesignations + @".</b>          
              </td>
         
              </tr>
           </table>
       
                     </body> </html>";
                        }
                    }
                }


                string htmlStringToConvert = outXml;
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
                pdfConverter.PdfFooterOptions.FooterHeight = 60;

                //TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
                //        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                //footerTextElement.TextAlign = HorizontalTextAlign.Center;
                TextElement footerTextElement = new TextElement(0, 30, "&p;",
                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                //TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan",
                //       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                //footerTextElement1.TextAlign = HorizontalTextAlign.Left;
                //footerTextElement1.ForeColor = System.Drawing.Color.MidnightBlue;
                //footerTextElement1.TextAlign = HorizontalTextAlign.Center;

                //TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                //       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                //footerTextElement3.TextAlign = HorizontalTextAlign.Right;
                //footerTextElement3.ForeColor = System.Drawing.Color.MidnightBlue;
                //footerTextElement3.TextAlign = HorizontalTextAlign.Center;
                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
                //pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
                //pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

                string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

                ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "Blanknotapproved.png"));
                // imageElement1.KeepAspectRatio = true;
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
                ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "Blanknotapproved.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 15;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388F;

                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
                float stampWidth = float.Parse("400");
                float stampHeight = float.Parse("400");

                // Center the stamp at the top of PDF page
                float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
                float stampYLocation = 150;

                RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

                Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
                stampTemplate.AddElement(imageElement2);
                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }
                string url = "Question" + "/AfterFixation/" + model.AssemblyID + "/" + model.SessionID + "/";
                Id = Id.Replace("/", "-");
                if (ViewBy == "CQ")
                {
                    fileName = model.AssemblyID + "_" + model.SessionID + "_U_" + Id + ".pdf";
                }
                else if (ViewBy == "PQ")
                {
                    fileName = model.AssemblyID + "_" + model.SessionID + "_U_PQ_" + Id + ".pdf";
                }
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=UnstaredQuestionList/" + fileName);

                //string directory = Server.MapPath(model.FilePath + url);
                string directory = model.FilePath + url;
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                //var path = Path.Combine(Server.MapPath(model.FilePath + url), fileName);
                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                //model.FilePath = directory + fileName;
                model.FilePath = url + fileName;

                CurrentSession.FileAccessPath = model.DocFilePath;
                var AccessPath = "/" + url + fileName;

                model = Helper.ExecuteService("Notice", "UnstaredUpdatemSessionDate", model) as tMemberNotice;

                return "Success" + AccessPath;
                //byte[] bytes = System.IO.File.ReadAllBytes(AccessPath);
                //return File(bytes, "application/pdf");
            }

            catch (Exception ex)
            {

                //throw ex;
                return ex.Message;
            }
            finally
            {

            }


        }
        //        public string U_GeneratePdfByDate_ByViewForAdminUnstarredPDf(string Id, string ViewBy)
        //        {
        //            try
        //            {
        //                string outXml = "";
        //                string outInnerXml = "";
        //                string LOBName = Id;
        //                string fileName = "";
        //                string savedPDF = string.Empty;
        //                string HeadingText = "";
        //                string HeadingTextEnglish = "";

        //                if (ViewBy == "CQ")
        //                {
        //                    HeadingText = "लिखित उत्तर हेतु प्रश्न";
        //                    HeadingTextEnglish = "Questions For Written Answers";
        //                }
        //                else if (ViewBy == "PQ")
        //                {
        //                    HeadingText = "लिखित उत्तर हेतु स्थगित प्रश्न";
        //                    HeadingTextEnglish = "Postponed Questions For Written Answer";
        //                }


        //                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        //                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);
        //                tMemberNotice model = new tMemberNotice();

        //                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
        //                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
        //                string strDate = Id;
        //                DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
        //                model.NoticeDate = date;
        //                model.DataStatus = ViewBy;
        //                model = (tMemberNotice)Helper.ExecuteService("Notice", "UnstaredGetAllDetailsForSpdf", model);
        //                if (model.tQuestionModel != null)
        //                {
        //                    foreach (var item in model.tQuestionModel)
        //                    {
        //                        model.SessionDateId = item.SessionDateId;
        //                        model.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", model);
        //                        model.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", model);
        //                    }
        //                }
        //                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", model);
        //                foreach (var item in model.Values)
        //                {
        //                    model.SesNameEnglish = item.SesNameEnglish;
        //                }

        //                if (model.tQuestionModel != null)
        //                {
        //                    foreach (var item in model.tQuestionModel)
        //                    {
        //                        if (model.HeaderHindi == true)
        //                        {
        //                            outXml = @"<html>                           
        //                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
        //                            outXml += @"<div> 
        //
        // <style type='text/css'>
        //@page {
        //    @bottom-left {
        //        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //    @bottom-right {
        //        content: 'Page ' counter(page) ' of ' counter(pages);
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //}
        //</style>
        //                    </div>
        //            
        //<table style='width:100%'>
        // 
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //             <b>हिमाचल प्रदेश बारहवीं विधान सभा</b>
        //              </td>
        //              </tr>
        //      </table>
        //<br />
        //<br />
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "(" + model.SessionName + ")" + @"</b> 
        //              </td>
        //              </tr>
        //           </table>
        //<br />
        //<br />
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;'>
        //            <b>" + HeadingText + @"</b>          
        //              </td>    
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //          <b>" + item.SessionDateLocal + @"</b>   
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "[" + model.MinisterName + "]" + @". </b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           <b>कुल प्रश्न - " + model.TotalQCount + @"</b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----
        //            
        //              </td>
        //              </tr>
        //           </table>";

        //                        }
        //                        else
        //                        {
        //                            outXml = @"<html>                           
        //                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
        //                            outXml += @"<div> 
        //
        // <style type='text/css'>
        //@page {
        //    @bottom-left {
        //        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //    @bottom-right {
        //        content: 'Page ' counter(page) ' of ' counter(pages);
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //}
        //</style>
        //                    </div>
        //
        //            
        //<table style='width:100%'>
        // 
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //             <b>HIMACHAL PRADESH THIRTEENTH VIDHAN SABHA</b>
        //              </td>
        //              </tr>
        //      </table>
        //<br />
        //<br />
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "(" + model.SesNameEnglish + ")" + @"</b> 
        //              </td>
        //              </tr>
        //           </table>
        //<br />
        //<br />
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;'>
        //            <b>" + HeadingTextEnglish + @"</b>         
        //              </td>    
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //          <b>" + item.SessionDateEnglish + @"</b>   
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "[" + model.MinisterNameEnglish + "]" + @". </b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           <b>Total No. of Questions - " + model.TotalQCount + @"</b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----
        //            
        //              </td>
        //              </tr>
        //           </table>";

        //                        }

        //                    }
        //                    foreach (var item in model.tQuestionModel)
        //                    {
        //                        if (item.IsClubbed != null)
        //                        {
        //                            if (item.IsHindi == true)
        //                            {
        //                                string[] values = item.CMemNameHindi.Split(',');
        //                                outInnerXml = "";
        //                                for (int i = 0; i < values.Length; i++)
        //                                {
        //                                    values[i] = values[i].Trim();
        //                                    outInnerXml += @"<div>
        //          " + values[i] + @" :
        //
        //                               </div>
        //                             ";
        //                                }

        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }



        //                            }
        //                            else
        //                            {
        //                                string[] values = item.CMemName.Split(',');
        //                                outInnerXml = "";
        //                                for (int i = 0; i < values.Length; i++)
        //                                {
        //                                    values[i] = values[i].Trim();
        //                                    outInnerXml += @"<div>
        //          " + values[i] + @" :
        //
        //                               </div>
        //                             ";
        //                                }
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        //Will the <b>" + item.MinistryName + @"</b> be pleased to state :-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }


        //                            }




        //                        }
        //                        else
        //                        {

        //                            if (item.MinisterID == 2 && item.IsHindi == true)
        //                            {
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि:-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }


        //                            }
        //                            else if (item.IsHindi == true)
        //                            {
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }

        //                            }
        //                            else
        //                            {
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        //Will the <b>" + item.MinistryName + @"</b> be pleased to state :-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }

        //                            }


        //                        }
        //                    }


        //                    if (model.StampTypeHindi == true)
        //                    {
        //                        foreach (var item in model.Values)
        //                        {

        //                            outXml += @"
        // 
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:left;font-size:20px;' >
        //      
        //<b>" + item.SignaturePlaceLocal + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //                      <b>" + item.SignatureNameLocal + @",</b>
        //              </td>
        //         
        //              </tr>
        //<tr>
        //          <td style='text-align:left;font-size:20px;' >
        //           
        //         <b>दिनांक: " + item.SignatureDateLocal + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //       
        //  <b>" + item.SignatureDesignationsLocal + @"।</b>          
        //              </td>
        //         
        //              </tr>
        //           </table>
        //       
        //                     </body> </html>";

        //                        }
        //                    }
        //                    else
        //                    {
        //                        foreach (var item in model.Values)
        //                        {

        //                            outXml += @"
        // 
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:left;font-size:20px;' >
        //      
        //<b>" + item.SignaturePlace + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //                      <b>" + item.SignatureName + @",</b>
        //              </td>
        //         
        //              </tr>
        //<tr>
        //          <td style='text-align:left;font-size:20px;' >
        //           
        //         <b>Dated: " + item.SignatureDate + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //       
        //  <b>" + item.SignatureDesignations + @".</b>          
        //              </td>
        //         
        //              </tr>
        //           </table>
        //       
        //                     </body> </html>";
        //                        }
        //                    }
        //                }
        //                string htmlStringToConvert = outXml;
        //                PdfConverter pdfConverter = new PdfConverter();
        //                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

        //                pdfConverter.PdfDocumentOptions.ShowFooter = true;
        //                pdfConverter.PdfDocumentOptions.ShowHeader = true;

        //                // set the header height in points
        //                pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
        //                pdfConverter.PdfFooterOptions.FooterHeight = 60;

        //                //TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
        //                //        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //                //footerTextElement.TextAlign = HorizontalTextAlign.Center;
        //                TextElement footerTextElement = new TextElement(0, 30, "&p;",
        //                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //                footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
        //                footerTextElement.TextAlign = HorizontalTextAlign.Center;

        //                //TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan",
        //                //       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //                //footerTextElement1.TextAlign = HorizontalTextAlign.Left;
        //                //footerTextElement1.ForeColor = System.Drawing.Color.MidnightBlue;
        //                //footerTextElement1.TextAlign = HorizontalTextAlign.Center;

        //                //TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
        //                //       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //                //footerTextElement3.TextAlign = HorizontalTextAlign.Right;
        //                //footerTextElement3.ForeColor = System.Drawing.Color.MidnightBlue;
        //                //footerTextElement3.TextAlign = HorizontalTextAlign.Center;
        //                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
        //                //pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
        //                //pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

        //                string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

        //                ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "Blanknotapproved.png"));
        //                // imageElement1.KeepAspectRatio = true;
        //                imageElement1.Opacity = 100;
        //                imageElement1.DestHeight = 97f;
        //                imageElement1.DestWidth = 71f;
        //                pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
        //                ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "Blanknotapproved.png"));
        //                imageElement2.KeepAspectRatio = false;
        //                imageElement2.Opacity = 15;
        //                imageElement2.DestHeight = 284f;
        //                imageElement2.DestWidth = 388F;

        //                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
        //                float stampWidth = float.Parse("400");
        //                float stampHeight = float.Parse("400");

        //                // Center the stamp at the top of PDF page
        //                float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
        //                float stampYLocation = 150;

        //                RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

        //                Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
        //                stampTemplate.AddElement(imageElement2);
        //                byte[] pdfBytes = null;

        //                try
        //                {
        //                    pdfBytes = pdfDocument.Save();
        //                }
        //                finally
        //                {
        //                    // close the Document to realease all the resources
        //                    pdfDocument.Close();
        //                }
        //                string url = "Question" + "/AfterFixation/" + model.AssemblyID + "/" + model.SessionID + "/";
        //                Id = Id.Replace("/", "-");
        //                if (ViewBy == "CQ")
        //                {
        //                    fileName = model.AssemblyID + "_" + model.SessionID + "_U_" + Id + ".pdf";
        //                }
        //                else if (ViewBy == "PQ")
        //                {
        //                    fileName = model.AssemblyID + "_" + model.SessionID + "_U_PQ_" + Id + ".pdf";
        //                }
        //                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=UnstaredQuestionList/" + fileName);

        //                //string directory = Server.MapPath(model.FilePath + url);
        //                string directory = model.FilePath + url;
        //                if (!Directory.Exists(directory))
        //                {
        //                    Directory.CreateDirectory(directory);
        //                }

        //                //var path = Path.Combine(Server.MapPath(model.FilePath + url), fileName);
        //                var path = Path.Combine(directory, fileName);
        //                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

        //                // close file stream
        //                _FileStream.Close();

        //                //model.FilePath = directory + fileName;
        //                model.FilePath = url + fileName;

        //                CurrentSession.FileAccessPath = model.DocFilePath;
        //                var AccessPath = "/" + url + fileName;

        //                model = Helper.ExecuteService("Notice", "UnstaredUpdatemSessionDate", model) as tMemberNotice;

        //                return "Success" + AccessPath;
        //                //byte[] bytes = System.IO.File.ReadAllBytes(AccessPath);
        //                //return File(bytes, "application/pdf");
        //            }

        //            catch (Exception ex)
        //            {

        //                //throw ex;
        //                return ex.Message;
        //            }
        //            finally
        //            {

        //            }


        //        }

        //        public string U_GeneratePdfByDate_ByViewForAdminUnstarredPDf(string Id, string ViewBy)
        //        {
        //            try
        //            {
        //                string outXml = "";
        //                string outInnerXml = "";
        //                string LOBName = Id;
        //                string fileName = "";
        //                string savedPDF = string.Empty;
        //                string HeadingText = "";
        //                string HeadingTextEnglish = "";

        //                if (ViewBy == "CQ")
        //                {
        //                    HeadingText = "लिखित उत्तर हेतु प्रश्न";
        //                    HeadingTextEnglish = "Questions For Written Answers";
        //                }
        //                else if (ViewBy == "PQ")
        //                {
        //                    HeadingText = "लिखित उत्तर हेतु स्थगित प्रश्न";
        //                    HeadingTextEnglish = "Postponed Questions For Written Answer";
        //                }


        //                // BaseFont Hindi = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\CDACOTYGN.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        //                // iTextSharp.text.Font font = new iTextSharp.text.Font(Hindi, 10, iTextSharp.text.Font.NORMAL);
        //                tMemberNotice model = new tMemberNotice();

        //                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
        //                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
        //                string strDate = Id;
        //                DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
        //                model.NoticeDate = date;
        //                model.DataStatus = ViewBy;
        //                model = (tMemberNotice)Helper.ExecuteService("Notice", "UnstaredGetAllDetailsForSpdf", model);
        //                if (model.tQuestionModel != null)
        //                {
        //                    foreach (var item in model.tQuestionModel)
        //                    {
        //                        model.SessionDateId = item.SessionDateId;
        //                        model.MinisterName = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterName", model);
        //                        model.MinisterNameEnglish = (string)Helper.ExecuteService("Notice", "GetRotationalMiniterNameEnglish", model);
        //                    }
        //                }
        //                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetSessionStamp", model);
        //                foreach (var item in model.Values)
        //                {
        //                    model.SesNameEnglish = item.SesNameEnglish;
        //                }

        //                if (model.tQuestionModel != null)
        //                {
        //                    foreach (var item in model.tQuestionModel)
        //                    {
        //                        if (model.HeaderHindi == true)
        //                        {
        //                            outXml = @"<html>                           
        //                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
        //                            outXml += @"<div> 
        //
        // <style type='text/css'>
        //@page {
        //    @bottom-left {
        //        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //    @bottom-right {
        //        content: 'Page ' counter(page) ' of ' counter(pages);
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //}
        //</style>
        //                    </div>
        //            
        //<table style='width:100%'>
        // 
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //             <b>हिमाचल प्रदेश बारहवीं विधान सभा</b>
        //              </td>
        //              </tr>
        //      </table>
        //<br />
        //<br />
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "(" + model.SessionName + ")" + @"</b> 
        //              </td>
        //              </tr>
        //           </table>
        //<br />
        //<br />
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;'>
        //            <b>" + HeadingText + @"</b>          
        //              </td>    
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //          <b>" + item.SessionDateLocal + @"</b>   
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "[" + model.MinisterName + "]" + @". </b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           <b>कुल प्रश्न - " + model.TotalQCount + @"</b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----
        //            
        //              </td>
        //              </tr>
        //           </table>";

        //                        }
        //                        else
        //                        {
        //                            outXml = @"<html>                           
        //                                 <body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;margin-top:70px;margin-bottom:200px;'> <div><div style='width: 100%;'><table style='width: 100%;'>";
        //                            outXml += @"<div> 
        //
        // <style type='text/css'>
        //@page {
        //    @bottom-left {
        //        content: 'Date {!DAY(TODAY())}.{!MONTH(TODAY())}.{!YEAR(TODAY())}';
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //    @bottom-right {
        //        content: 'Page ' counter(page) ' of ' counter(pages);
        //        font-family: sans-serif;
        //        font-size: 80%;
        //    }
        //}
        //</style>
        //                    </div>
        //
        //            
        //<table style='width:100%'>
        // 
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //             <b>HIMACHAL PRADESH THIRTEENTH VIDHAN SABHA</b>
        //              </td>
        //              </tr>
        //      </table>
        //<br />
        //<br />
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "(" + model.SesNameEnglish + ")" + @"</b> 
        //              </td>
        //              </tr>
        //           </table>
        //<br />
        //<br />
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;'>
        //            <b>" + HeadingTextEnglish + @"</b>         
        //              </td>    
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //      <tr>
        //          <td style='text-align:center;font-size:28px;' >
        //          <b>" + item.SessionDateEnglish + @"</b>   
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //            <b>" + "[" + model.MinisterNameEnglish + "]" + @". </b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           <b>Total No. of Questions - " + model.TotalQCount + @"</b>
        //            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----
        //            
        //              </td>
        //              </tr>
        //           </table>";

        //                        }

        //                    }
        //                    foreach (var item in model.tQuestionModel)
        //                    {
        //                        if (item.IsClubbed != null)
        //                        {
        //                            if (item.IsHindi == true)
        //                            {
        //                                string[] values = item.CMemNameHindi.Split(',');
        //                                outInnerXml = "";
        //                                for (int i = 0; i < values.Length; i++)
        //                                {
        //                                    values[i] = values[i].Trim();
        //                                    outInnerXml += @"<div>
        //          " + values[i] + @" :
        //
        //                               </div>
        //                             ";
        //                                }

        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }



        //                            }
        //                            else
        //                            {
        //                                string[] values = item.CMemName.Split(',');
        //                                outInnerXml = "";
        //                                for (int i = 0; i < values.Length; i++)
        //                                {
        //                                    values[i] = values[i].Trim();
        //                                    outInnerXml += @"<div>
        //          " + values[i] + @" :
        //
        //                               </div>
        //                             ";
        //                                }
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + outInnerXml + @"</b>
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        //Will the <b>" + item.MinistryName + @"</b> be pleased to state:-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }


        //                            }




        //                        }
        //                        else
        //                        {

        //                            if (item.MinisterID == 2 && item.IsHindi == true)
        //                            {
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;'>" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगी कि:-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }


        //                            }
        //                            else if (item.IsHindi == true)
        //                            {
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //     
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberNameLocal + " (" + item.ConstituencyName_Local + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // क्या <b>" + item.MinistryNameLocal + @"</b> बतलाने की कृपा करेंगे कि:-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }

        //                            }
        //                            else
        //                            {
        //                                if (item.IsQuestionInPart == null || item.IsQuestionInPart == false)
        //                                {
        //                                    string MQues = WebUtility.HtmlDecode(item.MainQuestion);
        //                                    MQues = MQues.Replace("<p>", "");
        //                                    MQues = MQues.Replace("</p>", "");
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   Will the <b>" + item.MinistryName + @"</b> be pleased to state  " + MQues + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }
        //                                else
        //                                {
        //                                    outXml += @"<table style='width:100%'>
        //
        //<tr style='page-break-inside : avoid'>
        //
        //          <td style='text-align:center;font-size:21px;padding-bottom: 20px;font-weight: bold;' >" + WebUtility.HtmlDecode(item.Subject) + @"
        //       
        //              </td>      
        //         
        //         </tr>
        //           </table>
        //
        //<table style='width:100%;text-align:justify;'>
        //<tr>
        //<td style='width:10%; vertical-align: top; font-size:20px;font-weight: bold;'>
        //<b>" + item.QuestionNumber + @"</b>
        //</td>
        //<td style='font-size:20px;padding-bottom: 13px;'>
        //<b>" + item.MemberName + " (" + item.ConstituencyName + ")" + @"</b>:
        //</td>
        //</tr>
        //
        //
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        //Will the <b>" + item.MinistryName + @"</b> be pleased to state:-
        //</td>
        //
        //</tr>
        //<tr>
        //<td>
        //</td>
        //<td style='font-size:20px;'>
        // <p style='text-align:justify;'>
        //   " + WebUtility.HtmlDecode(item.MainQuestion) + @"
        //</p>
        //</td>
        //
        //</tr>
        //
        //</table>
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:center;font-size:20px;' >
        //           ----            
        //              </td>
        //         
        //              </tr>
        //           </table>
        //
        //";
        //                                }

        //                            }


        //                        }
        //                    }


        //                    if (model.StampTypeHindi == true)
        //                    {
        //                        foreach (var item in model.Values)
        //                        {

        //                            outXml += @"
        // 
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:left;font-size:20px;' >
        //      
        //<b>" + item.SignaturePlaceLocal + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //                      <b>" + item.SignatureNameLocal + @",</b>
        //              </td>
        //         
        //              </tr>
        //<tr>
        //          <td style='text-align:left;font-size:20px;' >
        //           
        //         <b>दिनांक: " + item.SignatureDateLocal + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //       
        //  <b>" + item.SignatureDesignationsLocal + @"।</b>          
        //              </td>
        //         
        //              </tr>
        //           </table>
        //       
        //                     </body> </html>";

        //                        }
        //                    }
        //                    else
        //                    {
        //                        foreach (var item in model.Values)
        //                        {

        //                            outXml += @"
        // 
        //
        //<table style='width:100%'>
        //     <tr>
        //          <td style='text-align:left;font-size:20px;' >
        //      
        //<b>" + item.SignaturePlace + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //                      <b>" + item.SignatureName + @",</b>
        //              </td>
        //         
        //              </tr>
        //<tr>
        //          <td style='text-align:left;font-size:20px;' >
        //           
        //         <b>Dated: " + item.SignatureDate + @".</b>
        //              </td>
        //<td style='text-align:right;font-size:20px;' >
        //       
        //  <b>" + item.SignatureDesignations + @".</b>          
        //              </td>
        //         
        //              </tr>
        //           </table>
        //       
        //                     </body> </html>";
        //                        }
        //                    }
        //                }
        //                MemoryStream output = new MemoryStream();

        //                PdfConverter pdfConverter = new PdfConverter();

        //                // set the license key
        //                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";


        //                pdfConverter.PdfDocumentOptions.ShowFooter = true;
        //                pdfConverter.PdfDocumentOptions.ShowHeader = true;

        //                // set the header height in points
        //                pdfConverter.PdfHeaderOptions.HeaderHeight = 60;
        //                pdfConverter.PdfFooterOptions.FooterHeight = 60;

        //                TextElement footerTextElement = new TextElement(0, 30, "&p;",
        //                        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //                footerTextElement.TextAlign = HorizontalTextAlign.Center;

        //                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


        //                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


        //                byte[] pdfBytes = null;

        //                try
        //                {
        //                    pdfBytes = pdfDocument.Save();
        //                }
        //                finally
        //                {
        //                    // close the Document to realease all the resources
        //                    pdfDocument.Close();
        //                }
        //                string url = "Question" + "/AfterFixation/" + model.AssemblyID + "/" + model.SessionID + "/";
        //                Id = Id.Replace("/", "-");
        //                if (ViewBy == "CQ")
        //                {
        //                    fileName = model.AssemblyID + "_" + model.SessionID + "_U_" + Id + ".pdf";
        //                }
        //                else if (ViewBy == "PQ")
        //                {
        //                    fileName = model.AssemblyID + "_" + model.SessionID + "_U_PQ_" + Id + ".pdf";
        //                }
        //                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=UnstaredQuestionList/" + fileName);

        //                //string directory = Server.MapPath(model.FilePath + url);
        //                string directory = model.FilePath + url;
        //                if (!Directory.Exists(directory))
        //                {
        //                    Directory.CreateDirectory(directory);
        //                }

        //                //var path = Path.Combine(Server.MapPath(model.FilePath + url), fileName);
        //                var path = Path.Combine(directory, fileName);
        //                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
        //                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

        //                // close file stream
        //                _FileStream.Close();

        //                //model.FilePath = directory + fileName;
        //                model.FilePath = url + fileName;

        //                CurrentSession.FileAccessPath = model.DocFilePath;
        //                var AccessPath = "/" + url + fileName;

        //                model = Helper.ExecuteService("Notice", "UnstaredUpdatemSessionDate", model) as tMemberNotice;

        //                return "Success" + AccessPath;
        //                //byte[] bytes = System.IO.File.ReadAllBytes(AccessPath);
        //                //return File(bytes, "application/pdf");
        //            }

        //            catch (Exception ex)
        //            {

        //                //throw ex;
        //                return ex.Message;
        //            }
        //            finally
        //            {

        //            }


        //        }
    }
}
