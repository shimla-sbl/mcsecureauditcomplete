﻿using Microsoft.Security.Application;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using SBL.DomainModel.Models.Diaries;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class NoticeController : Controller
    {
        //
        // GET: /Notices/Notice/
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NoticeEntry()
        {
            tMemberNotice noticeModel = new tMemberNotice();
            mSession modelSession = new mSession();
            mAssembly modelAssembly = new mAssembly();

            SiteSettings siteSettingModel = new SiteSettings();
            siteSettingModel = Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingModel) as SiteSettings;

            noticeModel.AssemblyID = siteSettingModel.AssemblyCode;
            noticeModel.SessionID = siteSettingModel.SessionCode;

            noticeModel.mAssemblyList = Helper.ExecuteService("Assembly", "GetAllAssembly", modelAssembly) as List<mAssembly>;

            modelSession.AssemblyID = siteSettingModel.AssemblyCode;
            noticeModel.sessionList = Helper.ExecuteService("Session", "GetSessionsByAssemblyID", modelSession) as List<mSession>;

            return View(noticeModel);
        }

        [HttpGet]
        public JsonResult GetSessionsByAssemblyID(string AssemblyID)
        {
            ServiceParameter serParam = new ServiceParameter();
            mSession sessionMod = new mSession();
            sessionMod.AssemblyID = Convert.ToInt32(AssemblyID);
            List<mSession> sessionList = Helper.ExecuteService("Session", "GetSessionsByAssemblyID", sessionMod) as List<mSession>;
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem()
            {
                Text = "Select Session",
                Value = Convert.ToString(0)
            });
            if (sessionList != null || sessionList.Count != 0)
            {
                foreach (var session in sessionList)
                {
                    list.Add(new SelectListItem()
                    {
                        Text = Convert.ToString(session.SessionName),
                        Value = Convert.ToString(session.SessionCode)
                    });
                }
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDepartmentByMinistery(int MinistryId)
        {
            tMemberNotice noticeModel = new tMemberNotice();

            mMinistry model = new mMinistry();
            model.MinistryID = MinistryId;
            List<tMemberNotice> deptartmentList = new List<tMemberNotice>();

            tMemberNotice model1 = new tMemberNotice();
            model1.DepartmentId = "";
            model1.DepartmentName = "Select Department";
            deptartmentList.Add(model1);

            var objDeptList = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model) as List<tMemberNotice>;
            if (objDeptList != null)
            {
                deptartmentList.AddRange(objDeptList);
            }

            return Json(deptartmentList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreateNotice()
        {
            tMemberNotice noticeModel = new tMemberNotice();

            mSession modelSession = new mSession();
            mAssembly modelAssembly = new mAssembly();
            SiteSettings siteSettingModel = new SiteSettings();
            siteSettingModel = Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingModel) as SiteSettings;

            noticeModel.AssemblyID = siteSettingModel.AssemblyCode;
            noticeModel.SessionID = siteSettingModel.SessionCode;
            noticeModel.mAssemblyList = Helper.ExecuteService("Assembly", "GetAllAssembly", modelAssembly) as List<mAssembly>;            
            //noticeModel.noticeTypeList = Helper.ExecuteService("Notice", "GetAllNoticeTypes", null) as List<mNoticeType>;

            //Bind business types for notices.
            mEvent eventModel = new mEvent();
            eventModel.PaperCategoryTypeId = 4;
            noticeModel.eventList = Helper.ExecuteService("Events", "GetEventsByPaperCategoryID", eventModel) as List<mEvent>;
            eventModel.EventId = 0;
            eventModel.EventName = "Select Business Type";
            noticeModel.eventList.Add(eventModel);

            modelSession.AssemblyID = siteSettingModel.AssemblyCode;
            noticeModel.sessionList = Helper.ExecuteService("Session", "GetSessionsByAssemblyID", modelSession) as List<mSession>;

            //Get all the members for current assembly.
            mMember memberModel = new mMember();
            modelAssembly.AssemblyID = noticeModel.AssemblyID;
            noticeModel.memberList = Helper.ExecuteService("Member", "GetMemberByAssemblyID", modelAssembly) as List<mMember>;
            memberModel.MemberCode = 0;
            memberModel.Name = "Select Member";
            noticeModel.memberList.Add(memberModel);

            if (noticeModel.MemberId == null)
            {
                noticeModel.MemberId = 0;
            }
            return PartialView("_CreateNotice", noticeModel);
        }

        [HttpPost]
        public ActionResult CreateNotice(tMemberNotice model, HttpPostedFileBase NoticeFile)
        {
            tMemberNotice noticeModel = new tMemberNotice();
            //model = this.SaveNoticeFile(model, NoticeFile);           

            model.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
            model.Actice = true;
            var modelReturned = Helper.ExecuteService("Notice", "CreateNotice", model) as tMemberNotice;
            return RedirectToAction("NoticeEntry");
        }

        [NonAction]
        public tMemberNotice SaveNoticeFile(tMemberNotice model, HttpPostedFileBase NoticeFile)
        {
            #region SaveFile_NoticeFile
            //Save Locally.
            if (NoticeFile != null)
            {
                string fileLocation = "";
                string FileFolderLoc = "/PaperLaid/" + model.AssemblyID + "/" + model.SessionID;
                string FileFolderDirLoc = "/PaperLaid/" + model.AssemblyID + "/" + model.SessionID + "/Notices/";
                string fileName = "";

                fileName = model.SessionID + "_Notice_" + System.IO.Path.GetFileName(NoticeFile.FileName);
                string directory = Server.MapPath(FileFolderDirLoc);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = System.IO.Path.Combine(Server.MapPath("~" + FileFolderDirLoc), fileName);
                NoticeFile.SaveAs(path);

                //Save remotly.
                int fileLengthNotice = NoticeFile.ContentLength;
                byte[] dataNotice = new byte[fileLengthNotice];
                NoticeFile.InputStream.Read(dataNotice, 0, fileLengthNotice);
                XmlSerializer XMLSer_NoticeData = new XmlSerializer(dataNotice.GetType());
                System.Text.StringBuilder sbNotice = new System.Text.StringBuilder();
                System.IO.StringWriter wrNotice = new System.IO.StringWriter(sbNotice);
                XMLSer_NoticeData.Serialize(wrNotice, dataNotice);
                XmlDocument XMLNotice = new XmlDocument();
                XMLNotice.LoadXml(sbNotice.ToString());

                //ServiceAdaptor.CallUploadFile(sbNotice.ToString(), fileName, fileLengthNotice, FileFolderLoc, "Notices");

                wrNotice.Dispose();
                wrNotice.Close();
                sbNotice.Clear();

                fileLocation = FileFolderDirLoc + fileName;
               // model.NoticePath = fileLocation;
            }
            return model;

            #endregion
        }

        public ActionResult CreateNoticeForward(int noticeID)
        {
            tMemberNotice noticeModel = new tMemberNotice();
            noticeModel.NoticeId = noticeID;
            noticeModel = Helper.ExecuteService("Notice", "GetNoticeByNoticeID", noticeModel) as tMemberNotice;
           
            mAssembly modelAssembly = new mAssembly();
            mSession sessionMod = new mSession();

            noticeModel.mAssemblyList = Helper.ExecuteService("Assembly", "GetAllAssembly", modelAssembly) as List<mAssembly>;
            //noticeModel.noticeTypeList = Helper.ExecuteService("Notice", "GetAllNoticeTypes", null) as List<mNoticeType>;

            mEvent eventModel = new mEvent();
            eventModel.PaperCategoryTypeId = 4;
            noticeModel.eventList = Helper.ExecuteService("Events", "GetEventsByPaperCategoryID", eventModel) as List<mEvent>;
            eventModel.EventId = 0;
            eventModel.EventName = "Select Business Type";
            noticeModel.eventList.Add(eventModel);

            sessionMod.AssemblyID = Convert.ToInt32(noticeModel.AssemblyID);
            noticeModel.sessionList = Helper.ExecuteService("Session", "GetSessionsByAssemblyID", sessionMod) as List<mSession>;

            mMinisteryMinisterModel mmModel = new mMinisteryMinisterModel();
            mmModel.MinistryID = 0;
            mmModel.MinisterMinistryName = "Select Minister";
            noticeModel.MinistryList = Helper.ExecuteService("MinistryMinister", "GetMinisterMinistry", null) as List<mMinisteryMinisterModel>;
            noticeModel.MinistryList.Add(mmModel);

            //Get all the members for current assembly.
            mMember memberModel = new mMember();
            modelAssembly.AssemblyID = noticeModel.AssemblyID;
            noticeModel.memberList = Helper.ExecuteService("Member", "GetMemberByAssemblyID", modelAssembly) as List<mMember>;
            memberModel.MemberCode = 0;
            memberModel.Name = "Select Member";
            noticeModel.memberList.Add(memberModel);

            return PartialView("_CreateNoticeForward", noticeModel);
        }

        [HttpPost]
        public ActionResult CreateNoticeForward(tMemberNotice model, HttpPostedFileBase NoticeFile)
        {
            tMemberNotice noticeModel = new tMemberNotice();

            model.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
            model.Actice = true;

            var modelReturned = Helper.ExecuteService("Notice", "CreateNotice", model) as tMemberNotice;
            return RedirectToAction("ForwardNotice");
        }

        public ActionResult PublishNotice(int noticeID, int pageNumber)
        {
            ServiceParameter serParam = new ServiceParameter();
            tMemberNotice model = new tMemberNotice();
            try
            {
                if (noticeID != 0)
                {
                    model.NoticeId = noticeID;
                    tMemberNotice returnedModel = Helper.ExecuteService("Notice", "PublishNotice", model) as tMemberNotice;
                }
                return Json(noticeID, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var errMsg = ex.Message;
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PublishNoticeForward(int noticeID, int pageNumber)
        {
            ServiceParameter serParam = new ServiceParameter();
            tMemberNotice model = new tMemberNotice();
            try
            {
                if (noticeID != 0)
                {
                    model.NoticeId = noticeID;
                    tMemberNotice returnedModel = Helper.ExecuteService("Notice", "PublishNoticeForward", model) as tMemberNotice;
                }
                return Json(noticeID, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var errMsg = ex.Message;
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteNotice(int noticeID, int pageNumber)
        {
            ServiceParameter serParam = new ServiceParameter();
            tMemberNotice model = new tMemberNotice();
            try
            {
                if (noticeID != 0)
                {
                    model.NoticeId = noticeID;
                    tMemberNotice returnedModel = Helper.ExecuteService("Notice", "DeleteNotice", model) as tMemberNotice;
                }
                return Json(noticeID, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var errMsg = ex.Message;
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditNotice(int noticeID)
        {
            tMemberNotice noticeModel = new tMemberNotice();

            noticeModel.NoticeId = noticeID;
            noticeModel = Helper.ExecuteService("Notice", "GetNoticeByNoticeID", noticeModel) as tMemberNotice;
            
            mSession modelSession = new mSession();
            mAssembly modelAssembly = new mAssembly();

            noticeModel.mAssemblyList = Helper.ExecuteService("Assembly", "GetAllAssembly", modelAssembly) as List<mAssembly>;

            mEvent eventModel = new mEvent();
            eventModel.PaperCategoryTypeId = 4;
            noticeModel.eventList = Helper.ExecuteService("Events", "GetEventsByPaperCategoryID", eventModel) as List<mEvent>;

            eventModel.EventId = 0;
            eventModel.EventName = "Select Business Type";
            noticeModel.eventList.Add(eventModel);

            mSession sessionMod = new mSession();
            sessionMod.AssemblyID = Convert.ToInt32(noticeModel.AssemblyID);
            noticeModel.sessionList = Helper.ExecuteService("Session", "GetSessionsByAssemblyID", sessionMod) as List<mSession>;

            //Get all the members for current assembly.
            mMember memberModel = new mMember();
            modelAssembly.AssemblyID = noticeModel.AssemblyID;
            noticeModel.memberList = Helper.ExecuteService("Member", "GetMemberByAssemblyID", modelAssembly) as List<mMember>;
            memberModel.MemberCode = 0;
            memberModel.Name = "Select Member";
            noticeModel.memberList.Add(memberModel);
            
            return PartialView("_CreateNotice", noticeModel);
        }

        #region Helper methods for various Gridview's in module.
        public ActionResult PendingNotices(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tMemberNotice noticeModel = new tMemberNotice();
            List<tMemberNotice> tempNoticeModel = new List<tMemberNotice>();

            noticeModel.loopStart = Convert.ToInt16(loopStart);
            noticeModel.loopEnd = Convert.ToInt16(loopEnd);
            noticeModel.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
            noticeModel.PageSize = Convert.ToInt16(RowsPerPage);
            noticeModel.PageIndex = Convert.ToInt16(PageNumber);

            noticeModel = Helper.ExecuteService("Notice", "GetNoticesByStatusID", noticeModel) as tMemberNotice;

            if (PageNumber != null && PageNumber != "")
            {
                noticeModel.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                noticeModel.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                noticeModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                noticeModel.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                noticeModel.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                noticeModel.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                noticeModel.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                noticeModel.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                noticeModel.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                noticeModel.loopEnd = Convert.ToInt32("5");
            }

            return PartialView("_PendingNotices", noticeModel);
        }

        public ActionResult SubmittedNotices(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tMemberNotice noticeModel = new tMemberNotice();
            List<tMemberNotice> tempNoticeModel = new List<tMemberNotice>();

            noticeModel.NoticeStatus = (int)NoticeStatusEnum.NoticePublished;
            noticeModel.PageSize = Convert.ToInt16(RowsPerPage);
            noticeModel.PageIndex = Convert.ToInt16(PageNumber);

            noticeModel = Helper.ExecuteService("Notice", "GetNoticesByStatusID", noticeModel) as tMemberNotice;

            if (PageNumber != null && PageNumber != "")
            {
                noticeModel.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                noticeModel.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                noticeModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                noticeModel.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                noticeModel.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                noticeModel.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                noticeModel.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                noticeModel.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                noticeModel.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                noticeModel.loopEnd = Convert.ToInt32("5");
            }

            return PartialView("_SubmittedNotices", noticeModel);
        }

        public ActionResult PendingForwardNotices(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tMemberNotice noticeModel = new tMemberNotice();
            List<tMemberNotice> tempNoticeModel = new List<tMemberNotice>();
            noticeModel.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
            noticeModel.PageSize = Convert.ToInt16(RowsPerPage);
            noticeModel.PageIndex = Convert.ToInt16(PageNumber);

            noticeModel = Helper.ExecuteService("Notice", "GetNoticesByStatusID", noticeModel) as tMemberNotice;

            if (PageNumber != null && PageNumber != "")
            {
                noticeModel.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                noticeModel.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                noticeModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                noticeModel.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                noticeModel.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                noticeModel.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                noticeModel.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                noticeModel.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                noticeModel.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                noticeModel.loopEnd = Convert.ToInt32("5");
            }

            return PartialView("_PendingForwardNotices", noticeModel);
        }

        public ActionResult PublishedForwardNotices(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tMemberNotice noticeModel = new tMemberNotice();
            List<tMemberNotice> tempNoticeModel = new List<tMemberNotice>();

            noticeModel.NoticeStatus = (int)NoticeStatusEnum.NoticePublished;
            noticeModel.PageSize = Convert.ToInt16(RowsPerPage);
            noticeModel.PageIndex = Convert.ToInt16(PageNumber);

            noticeModel = Helper.ExecuteService("Notice", "GetNoticesByStatusID", noticeModel) as tMemberNotice;

            if (PageNumber != null && PageNumber != "")
            {
                noticeModel.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                noticeModel.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                noticeModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                noticeModel.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                noticeModel.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                noticeModel.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                noticeModel.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                noticeModel.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                noticeModel.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                noticeModel.loopEnd = Convert.ToInt32("5");
            }

            return PartialView("_PublishedForwardNotices", noticeModel);
        }
        #endregion

        public ActionResult ForwardNotice()
        {
            tMemberNotice noticeModel = new tMemberNotice();
            mSession modelSession = new mSession();
            mAssembly modelAssembly = new mAssembly();

            SiteSettings siteSettingModel = new SiteSettings();
            siteSettingModel = Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingModel) as SiteSettings;

            noticeModel.AssemblyID = siteSettingModel.AssemblyCode;
            noticeModel.SessionID = siteSettingModel.SessionCode;

            noticeModel.mAssemblyList = Helper.ExecuteService("Assembly", "GetAllAssembly", modelAssembly) as List<mAssembly>;

            modelSession.AssemblyID = siteSettingModel.AssemblyCode;
            noticeModel.sessionList = Helper.ExecuteService("Session", "GetSessionsByAssemblyID", modelSession) as List<mSession>;

            return View(noticeModel);
        }

            ///Regarding Assigning Notices
         
         public JsonResult GetTypistRequired(int EventId)
        {

              DiaryModel DMdl = new DiaryModel();
              DMdl.EventId = EventId;
              DMdl = Helper.ExecuteService("Notice", "Istyoistrequired", DMdl) as DiaryModel;
              return Json(DMdl, JsonRequestBehavior.AllowGet);
        }

        }
    }

