﻿using Lib.Web.Mvc.JQuery.JqGrid;
using Microsoft.Security.Application;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models;
using System.ComponentModel.DataAnnotations;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class NoticeDetailsController : Controller
    {
        //
        // GET: /Notices/NoticeDetails/



        public ActionResult Index()
        {
            tMemberNotice model = new tMemberNotice();

            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId) && !string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            }
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForQuestionTypes", model);
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForNotrices", model);
            if (string.IsNullOrEmpty(CurrentSession.AssemblyId) || string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.AssemblyId = model.AssemblyID.ToString();
                CurrentSession.SessionId = model.SessionID.ToString();
            }
            return View(model);
        }
        public ActionResult PendingStaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            TypistQuestionModel model = new TypistQuestionModel();
            model.RoleName = CurrentSession.RoleName;
            //PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            //RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            //loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            //loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
            //TypistQuestionModel model = new TypistQuestionModel();
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.QuestionTypeId = 1;
            //model.DepartmentId = CurrentSession.DeptID;
            //model.UId = CurrentSession.UserID;
            //
            //model = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetPendingQuestionsByType", model);

            //if (PageNumber != null && PageNumber != "")
            //{
            //    model.PageNumber = Convert.ToInt32(PageNumber);
            //}
            //else
            //{
            //    model.PageNumber = Convert.ToInt32("1");
            //}
            //if (RowsPerPage != null && RowsPerPage != "")
            //{
            //    model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            //}
            //else
            //{
            //    model.RowsPerPage = Convert.ToInt32("10");
            //}
            //if (PageNumber != null && PageNumber != "")
            //{
            //    model.selectedPage = Convert.ToInt32(PageNumber);
            //}
            //else
            //{
            //    model.selectedPage = Convert.ToInt32("1");
            //}
            //if (loopStart != null && loopStart != "")
            //{
            //    model.loopStart = Convert.ToInt32(loopStart);
            //}
            //else
            //{
            //    model.loopStart = Convert.ToInt32("1");
            //}
            //if (loopEnd != null && loopEnd != "")
            //{
            //    model.loopEnd = Convert.ToInt32(loopEnd);
            //}
            //else
            //{
            //    model.loopEnd = Convert.ToInt32("5");
            //}

            return PartialView("_PendingStaredQuestion", model);
        }
        public ActionResult GetAllStarredQuestionsGrid(JqGridRequest request, TypistQuestionModel customModel)
        {
            TypistQuestionModel model = new TypistQuestionModel();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.QuestionTypeId = 1;
            model.DepartmentId = CurrentSession.DeptID;
            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;

            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            var result = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetPendingQuestionsByType", model);
            model.ProofReaderName = result.ProofReaderName;


            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<TypistQuestionModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<TypistQuestionModel>(Convert.ToString(questionList.QuestionID), new TypistQuestionModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        public ActionResult GetPdfByQuestionID(int? questionID)
        {
            tQuestion Attachment = new tQuestion();

            if (questionID != null)
            {
                //Attachment.tQuestionModel = (ICollection<tQuestionModel>)Helpers.Helper.ExecuteService("Questions", "GetAttachment", questionID);
                string savedPDF = Helpers.Helper.ExecuteService("Questions", "GetAttachment", questionID) as string;

                //foreach (var item in Attachment.tQuestionModel)
                //{
                //    savedPDF = item.AnswerAttachLocation;
                //    ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(savedPDF);
                //}


                ViewBag.PDFPath = savedPDF;
            }
            return PartialView("_GetPdf");
        }
        public ActionResult GetDataQuestionID(int questionID)
        {
            string Month = "";
            string Day = "";
            string Year = "";
            tMemberNotice model = new tMemberNotice();
            model.RoleName = CurrentSession.RoleName;
            SBL.DomainModel.Models.Event.mEvent mEventModel = new SBL.DomainModel.Models.Event.mEvent();
            model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            model.mEvents = (ICollection<SBL.DomainModel.Models.Event.mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
            if (model.RoleName == "Vidhan Sabha Typist")
            {
                model.mMemberModel = Helper.ExecuteService("Member", "GetAllMembers", null) as List<mMember>;
            }
            else
            {
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetMembers", model);
            }


            model.UId = CurrentSession.UserID;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetButtons", model);
            tQuestion mdl = new tQuestion();
            int Id = Convert.ToInt32(questionID);
            model.QuestionID = Id;

            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetQDetails", model);
            model.UId = CurrentSession.UserID;
            foreach (var item in model.tQuestionModel)
            {
                model.Title = item.Subject;
                model.MemberId = item.MemberId;
                model.QuestionID = item.QuestionID;
                model.Details = item.MainQuestion;
                model.EventId = item.EventId;
                model.NoticeDate = item.NoticeDate.Value;
                model.NoticeTime = item.NoticeTime.Value;
                model.IsContentFreeze = item.IsContentFreeze;
                model.MinisterId = Convert.ToInt32(item.MinisterID);
                model.DepartmentId = item.DepartmentId;
                model.Checked = item.Checked;
                model.IsTypistFreeze = item.IsTypistFreeze;
                model.ConstituencyName = item.ConstituencyName;
                model.ConstituencyCode = Convert.ToInt32(item.ConstituencyName_Local);
                model.DiaryNo = item.DiaryNumber;
                model.IsClubChecked = Convert.ToBoolean(item.IsBracketed);
                model.ClubDiaryNo = item.BracketedDNo;
                model.MemberName = item.MemberName;
                model.IsQuestionInPart = item.IsQuestionInPart;
                model.IsHindi = item.IsHindi;
                string SDate = Convert.ToString(item.NoticeDate.Value);

                SDate = Convert.ToString(item.NoticeDate.Value);
                if (SDate != "")
                {
                    if (SDate.IndexOf("/") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("/"));
                        if (Month.Length < 2)
                        {
                            Month = "0" + Month;
                        }
                        Day = SDate.Substring(SDate.IndexOf("/") + 1, (SDate.LastIndexOf("/") - SDate.IndexOf("/") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("/") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                    if (SDate.IndexOf("-") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("-"));
                        Day = SDate.Substring(SDate.IndexOf("-") + 1, (SDate.LastIndexOf("-") - SDate.IndexOf("-") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("-") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                }
                mAssembly model4 = new mAssembly();
                //   model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;
                model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinistersProofTypist", model) as List<mMinisteryMinisterModel>;
                //model.IsClubChecked = false;
                //mMinisteryMinisterModel obj2 = new mMinisteryMinisterModel();
                //obj2.MinistryID = 0;
                //obj2.MinisterMinistryName = "Select Minister";
                //model.ministerList.Add(obj2);
                if (model.DepartmentId == null)
                {
                    //List<tMemberNotice> ListtMemberNotice = new List<tMemberNotice>();
                    //tMemberNotice noticeModel = new tMemberNotice();

                    tMemberNotice noticeModel1 = new tMemberNotice();
                    mMinistry model1 = new mMinistry();
                    model1.MinistryID = model.MinisterId;
                    noticeModel1.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model1) as List<tMemberNotice>;
                    model.DepartmentLt = noticeModel1.DepartmentLt;
                    //noticeModel.DepartmentId = "0";
                    //noticeModel.DepartmentName = "Select Department";
                    //ListtMemberNotice.Add(noticeModel);
                    //model.DepartmentLt = ListtMemberNotice;
                }
                if (model.DepartmentId != null)
                {
                    tMemberNotice noticeModel1 = new tMemberNotice();
                    mMinistry model1 = new mMinistry();
                    model1.MinistryID = model.MinisterId;
                    model1.AssemblyID = model.AssemblyID;
                    noticeModel1.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model1) as List<tMemberNotice>;
                    //noticeModel1.DepartmentId = Convert.ToString(0);
                    //noticeModel1.DepartmentName = "Select Department";
                    model.DepartmentLt = noticeModel1.DepartmentLt;
                }
            }
            if (model.RoleName == "Vidhan Sabha Typist")
            {
                return PartialView("_EditFormStartQuestion", model);
            }

            return PartialView("_startProof", model);
        }


        public ActionResult GetDataAuditTrialQuestionID(int questionID)
        {
            string Month = "";
            string Day = "";
            string Year = "";
            tMemberNotice model = new tMemberNotice();
            SBL.DomainModel.Models.Event.mEvent mEventModel = new SBL.DomainModel.Models.Event.mEvent();
            model.mEvents = (ICollection<SBL.DomainModel.Models.Event.mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
            model.mMemberModel = Helper.ExecuteService("Member", "GetAllMembers", null) as List<mMember>;
            model.UId = CurrentSession.UserID;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetButtons", model);
            tQuestion mdl = new tQuestion();
            int Id = Convert.ToInt32(questionID);
            model.QuestionID = Id;
            model.RoleName = CurrentSession.RoleName;
            model.RecordBy = "Typist";
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetQDetailsByAuditTrial", model);
            model.UId = CurrentSession.UserID;
            foreach (var item in model.tQuestionModel)
            {
                model.Title = item.Subject;
                model.MemberId = item.MemberId;
                model.QuestionID = item.QuestionID;
                model.Details = item.MainQuestion;
                model.EventId = item.EventId;
                model.NoticeDate = item.NoticeDate.Value;
                model.NoticeTime = item.NoticeTime.Value;
                model.IsContentFreeze = item.IsContentFreeze;
                model.MinisterId = Convert.ToInt32(item.MinisterID);
                model.DepartmentId = item.DepartmentId;
                model.Checked = item.Checked;
                model.IsTypistFreeze = item.IsTypistFreeze;
                model.IsClubChecked = item.IsBracketed.HasValue;
                model.ClubDiaryNo = item.BracketedDNo;
                model.ConstituencyName = item.ConstituencyName;
                model.IsQuestionInPart = item.IsQuestionInPart;
                model.IsHindi = item.IsHindi;
                model.MemberName = item.MemberName;
                string SDate = Convert.ToString(item.NoticeDate.Value);

                SDate = Convert.ToString(item.NoticeDate.Value);
                if (SDate != "")
                {
                    if (SDate.IndexOf("/") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("/"));
                        if (Month.Length < 2)
                        {
                            Month = "0" + Month;
                        }
                        Day = SDate.Substring(SDate.IndexOf("/") + 1, (SDate.LastIndexOf("/") - SDate.IndexOf("/") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("/") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                    if (SDate.IndexOf("-") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("-"));
                        Day = SDate.Substring(SDate.IndexOf("-") + 1, (SDate.LastIndexOf("-") - SDate.IndexOf("-") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("-") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                }
                mAssembly model4 = new mAssembly();
                model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;
                mMinisteryMinisterModel obj2 = new mMinisteryMinisterModel();
                //obj2.MinistryID = 0;
                obj2.MinisterMinistryName = "Select Minister";
                model.ministerList.Add(obj2);
                if (model.DepartmentId == null)
                {
                    List<tMemberNotice> ListtMemberNotice = new List<tMemberNotice>();
                    tMemberNotice noticeModel = new tMemberNotice();
                    noticeModel.DepartmentId = Convert.ToString(0);
                    noticeModel.DepartmentName = "Select Department";
                    ListtMemberNotice.Add(noticeModel);
                    model.DepartmentLt = ListtMemberNotice;
                }
                if (model.DepartmentId != null)
                {
                    tMemberNotice noticeModel1 = new tMemberNotice();
                    mMinistry model1 = new mMinistry();
                    model1.MinistryID = model.MinisterId;
                    noticeModel1.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model1) as List<tMemberNotice>;
                    noticeModel1.DepartmentId = Convert.ToString(0);
                    noticeModel1.DepartmentName = "Select Department";
                    model.DepartmentLt = noticeModel1.DepartmentLt;
                }
            }
            if (model.RoleName == "Vidhan Sabha Typist")
            {
                return PartialView("_EditFormStartQuestion", model);
            }

            return PartialView("_startProof", model);
        }

        public JsonResult UpdateEntry(Updatevalue model)
        {

            tMemberNotice Update = new tMemberNotice();
            tQuestion objModel = new tQuestion();
            objModel.QuestionID = model.QuestionID;
            objModel.Subject = model.Title;
            objModel.MainQuestion = model.Details;
            objModel.MinistryId = model.MinisterId;
            objModel.DepartmentID = model.DepartmentId;
            objModel.ProofReaderUserId = model.ProofUId;
            objModel.EventId = model.EventId;
            objModel.MemberID = model.MemberId;
            objModel.ConstituencyName = model.ConstName;
            //objModel.NoticeRecievedDate = Convert.ToDateTime(model.NDate);
            objModel.NoticeRecievedTime = model.NoticeTime;
            // objModel.IsBracket = model.IsClubChecked;
            objModel.BracketedWithDNo = model.ClubDiaryNo;
            objModel.DiaryNumber = model.DiaryNo;
            objModel.IsQuestionInPart = model.IsQuestionInPart;
            objModel.IsHindi = model.IsHindi;
            string strDate = model.NDate;
            DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
            objModel.NoticeRecievedDate = date;

            objModel = Helper.ExecuteService("Notice", "UpdateQuestions", objModel) as tQuestion;
            Update.Message = "Update Sucessfully";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public JsonResult UnstaredUpdateEntry(Updatevalue model)
        {

            tMemberNotice Update = new tMemberNotice();
            tQuestion objModel = new tQuestion();
            objModel.QuestionID = model.QuestionID;
            objModel.Subject = model.Title;
            objModel.MainQuestion = model.Details;
            objModel.MinistryId = model.MinisterId;
            objModel.DepartmentID = model.DepartmentId;
            objModel.ProofReaderUserId = model.ProofUId;
            objModel.EventId = model.EventId;
            objModel.MemberID = model.MemberId;
            objModel.ConstituencyName = model.ConstName;
            //objModel.NoticeRecievedDate = Convert.ToDateTime(model.NDate);
            objModel.NoticeRecievedTime = model.NoticeTime;
            // objModel.IsBracket = model.IsClubChecked;
            objModel.BracketedWithDNo = model.ClubDiaryNo;
            objModel.DiaryNumber = model.DiaryNo;
            objModel.IsQuestionInPart = model.IsQuestionInPart;
            objModel.IsHindi = model.IsHindi;

            string strDate = model.NDate;
            DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
            objModel.NoticeRecievedDate = date;

            objModel = Helper.ExecuteService("Notice", "UnstaredUpdateQuestions", objModel) as tQuestion;
            Update.Message = "Update Sucessfully";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }
        public JsonResult StayUpdateEntry(Updatevalue model)
        {
            tMemberNotice Update = new tMemberNotice();
            tQuestion objModel = new tQuestion();
            objModel.RoleName = CurrentSession.RoleName;
            objModel.QuestionID = model.QuestionID;
            objModel.Subject = model.Title;
            objModel.MainQuestion = model.Details;
            objModel.MinistryId = model.MinisterId;
            objModel.DepartmentID = model.DepartmentId;
            objModel.BracketedWithDNo = model.ClubDiaryNo;
            objModel.IsQuestionInPart = model.IsQuestionInPart;
            if (objModel.RoleName == "Vidhan Sabha Proof Reader")
            {
                objModel.MemberID = model.MemberId;
                objModel.ConstituencyNo = model.ConsiId;
                objModel.ConstituencyName = model.ConName;
            }

            objModel = Helper.ExecuteService("Notice", "StayUpdateQuestions", objModel) as tQuestion;
            Update.Message = "Update Sucessfully";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateContentFreeze(Updatevalue model)
        {
            tMemberNotice Update = new tMemberNotice();
            tQuestion objModel = new tQuestion();
            objModel.RoleName = CurrentSession.RoleName;
            objModel.QuestionID = model.QuestionID;
            objModel.Subject = model.Title;
            objModel.MainQuestion = model.Details;
            objModel.IsContentFreeze = true;
            objModel.IsProofReading = true;
            objModel.EventId = model.EventId;
            objModel.MinistryId = model.MinisterId;
            objModel.DepartmentID = model.DepartmentId;
            objModel.MemberID = model.MemberId;
            objModel.IsProofReadingDate = DateTime.Now;
            objModel.ProofReaderUserId = model.ProofUId;
            objModel.BracketedWithDNo = model.ClubDiaryNo;
            objModel.IsQuestionInPart = model.IsQuestionInPart;
            objModel.IsHindi = model.IsHindi;
            if (objModel.RoleName == "Vidhan Sabha Proof Reader")
            {
                objModel.ConstituencyNo = model.ConsiId;
                objModel.ConstituencyName = model.ConName;
            }
            objModel = Helper.ExecuteService("Notice", "SaveFreezeTrialTable", objModel) as tQuestion;
            //    objModel = Helper.ExecuteService("Notice", "UpdateContentFreeze", objModel) as tQuestion;

            Update.Message = "Update Sucessfully";

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }


        public ActionResult PendingUnstaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            TypistQuestionModel model = new TypistQuestionModel();
            model.RoleName = CurrentSession.RoleName;
            //PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            //RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            //loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            //loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);



            //model.loopStart = Convert.ToInt16(loopStart);
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.UId = CurrentSession.UserID;
            //model.QuestionTypeId = 2;
            //model.DepartmentId = CurrentSession.DeptID;
            //
            //model = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetUnStraredTypistView", model);

            //if (PageNumber != null && PageNumber != "")
            //{
            //    model.PageNumber = Convert.ToInt32(PageNumber);
            //}
            //else
            //{
            //    model.PageNumber = Convert.ToInt32("1");
            //}
            //if (RowsPerPage != null && RowsPerPage != "")
            //{
            //    model.RowsPerPage = Convert.ToInt32(RowsPerPage);
            //}
            //else
            //{
            //    model.RowsPerPage = Convert.ToInt32("10");
            //}
            //if (PageNumber != null && PageNumber != "")
            //{
            //    model.selectedPage = Convert.ToInt32(PageNumber);
            //}
            //else
            //{
            //    model.selectedPage = Convert.ToInt32("1");
            //}
            //if (loopStart != null && loopStart != "")
            //{
            //    model.loopStart = Convert.ToInt32(loopStart);
            //}
            //else
            //{
            //    model.loopStart = Convert.ToInt32("1");
            //}
            //if (loopEnd != null && loopEnd != "")
            //{
            //    model.loopEnd = Convert.ToInt32(loopEnd);
            //}
            //else
            //{
            //    model.loopEnd = Convert.ToInt32("5");
            //}

            return PartialView("_PendingUnStaredQuestion", model);
        }

        public ActionResult GetUnStarredQuestionsGrid(JqGridRequest request, TypistQuestionModel customModel)
        {
            TypistQuestionModel model = new TypistQuestionModel();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.QuestionTypeId = 2;
            model.DepartmentId = CurrentSession.DeptID;
            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;

            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            var result = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetUnStraredTypistView", model);
            model.ProofReaderName = result.ProofReaderName;
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.tQuestionModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<TypistQuestionModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<TypistQuestionModel>(Convert.ToString(questionList.QuestionID), new TypistQuestionModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        public ActionResult GetDataUnstart(int questionID)
        {
            string Month = "";
            string Day = "";
            string Year = "";
            tMemberNotice model = new tMemberNotice();
            model.RoleName = CurrentSession.RoleName;
            model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
            SBL.DomainModel.Models.Event.mEvent mEventModel = new SBL.DomainModel.Models.Event.mEvent();
            model.mEvents = (ICollection<SBL.DomainModel.Models.Event.mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
            if (model.RoleName == "Vidhan Sabha Typist")
            {
                model.mMemberModel = Helper.ExecuteService("Member", "GetAllMembers", null) as List<mMember>;
            }
            else
            {
                model = (tMemberNotice)Helper.ExecuteService("Notice", "GetMembers", model);
            }
            model.UId = CurrentSession.UserID;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetButtons", model);
            int Id = Convert.ToInt32(questionID);
            model.QuestionID = Id;

            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetQDetails", model);
            foreach (var item in model.tQuestionModel)
            {
                model.Title = item.Subject;
                model.MemberId = item.MemberId;
                model.QuestionID = item.QuestionID;
                model.Details = item.MainQuestion;
                model.EventId = item.EventId;
                model.NoticeDate = item.NoticeDate.Value;
                model.NoticeTime = item.NoticeTime.Value;
                model.IsContentFreeze = item.IsContentFreeze;
                model.MinisterId = Convert.ToInt32(item.MinisterID);
                model.DepartmentId = item.DepartmentId;
                model.Checked = item.Checked;
                model.IsTypistFreeze = item.IsTypistFreeze;
                model.ConstituencyName = item.ConstituencyName;
                model.DiaryNo = item.DiaryNumber;
                model.IsClubChecked = Convert.ToBoolean(item.IsBracketed);
                model.ClubDiaryNo = item.BracketedDNo;
                model.MemberName = item.MemberName;
                model.IsQuestionInPart = item.IsQuestionInPart;
                model.IsHindi = item.IsHindi;
                string SDate = Convert.ToString(item.NoticeDate.Value);

                SDate = Convert.ToString(item.NoticeDate.Value);
                if (SDate != "")
                {
                    if (SDate.IndexOf("/") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("/"));
                        if (Month.Length < 2)
                        {
                            Month = "0" + Month;
                        }
                        Day = SDate.Substring(SDate.IndexOf("/") + 1, (SDate.LastIndexOf("/") - SDate.IndexOf("/") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("/") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                    if (SDate.IndexOf("-") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("-"));
                        Day = SDate.Substring(SDate.IndexOf("-") + 1, (SDate.LastIndexOf("-") - SDate.IndexOf("-") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("-") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                }
                mAssembly model4 = new mAssembly();
                model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinisterMinistry", null) as List<mMinisteryMinisterModel>;

                if (model.DepartmentId == null)
                {
                    List<tMemberNotice> ListtMemberNotice = new List<tMemberNotice>();
                    tMemberNotice noticeModel = new tMemberNotice();
                    noticeModel.DepartmentId = "0";
                    noticeModel.DepartmentName = "Select Department";
                    ListtMemberNotice.Add(noticeModel);
                    model.DepartmentLt = ListtMemberNotice;
                }
                if (model.DepartmentId != null)
                {
                    tMemberNotice noticeModel1 = new tMemberNotice();
                    mMinistry model1 = new mMinistry();
                    model1.MinistryID = model.MinisterId;
                    noticeModel1.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model1) as List<tMemberNotice>;
                    model.DepartmentLt = noticeModel1.DepartmentLt;

                }
            }

            if (model.RoleName == "Vidhan Sabha Typist")
            {
                return PartialView("_EditFormUnStartQuestion", model);
            }

            return PartialView("_UnstartProof", model);


        }

        [HttpGet]
        public JsonResult GetDepartmentByMinistery(int MinistryId)
        {
            tMemberNotice noticeModel = new tMemberNotice();
            mMinistry model = new mMinistry();
            model.MinistryID = MinistryId;
            noticeModel.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model) as List<tMemberNotice>;
            //    List<tMemberNotice> DepartmentList = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model) as List<tMemberNotice>;
            noticeModel.DepartmentId = Convert.ToString(0);
            noticeModel.DepartmentName = "Select Department";
            // DepartmentLt.Add(noticeModel);
            return Json(noticeModel.DepartmentLt, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CheckDiaryNo(string DiaryNo, string DepartmentId)
        {

            tQuestionModel obj = new tQuestionModel();

            DiaryNo = Sanitizer.GetSafeHtmlFragment(DiaryNo);
            obj.DiaryNumber = DiaryNo;
            obj.DepartmentId = DepartmentId;
            //ServiceParameter serParam = ServiceParameter.Create("Questions", "CheckQuestionNo", obj);
            string st = Helper.ExecuteService("Notice", "CheckDQuestionNo", obj) as string;
            //ServiceAdaptor serv = new ServiceAdaptor();
            //int Count = (int)serv.CallService(serParam);
            //if (Count > 0)
            //{
            //    obj.ResultCount = Count;
            //}
            obj.DiaryNumber = st;
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UnstaredCheckDiaryNo(string DiaryNo, string DepartmentId)
        {

            tQuestionModel obj = new tQuestionModel();

            DiaryNo = Sanitizer.GetSafeHtmlFragment(DiaryNo);
            obj.DiaryNumber = DiaryNo;
            obj.DepartmentId = DepartmentId;
            //ServiceParameter serParam = ServiceParameter.Create("Questions", "CheckQuestionNo", obj);
            string st = Helper.ExecuteService("Notice", "UnstaredCheckDQuestionNo", obj) as string;
            //ServiceAdaptor serv = new ServiceAdaptor();
            //int Count = (int)serv.CallService(serParam);
            //if (Count > 0)
            //{
            //    obj.ResultCount = Count;
            //}
            obj.DiaryNumber = st;
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FullDetailById(string Id)
        {
            string Month = "";
            string Day = "";
            string Year = "";
            tMemberNotice model = new tMemberNotice();
            SBL.DomainModel.Models.Event.mEvent mEventModel = new SBL.DomainModel.Models.Event.mEvent();
            model.mEvents = (ICollection<SBL.DomainModel.Models.Event.mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
            model.mMemberModel = Helper.ExecuteService("Member", "GetAllMembers", null) as List<mMember>;
            model.UId = CurrentSession.UserID;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetButtons", model);
            tQuestion mdl = new tQuestion();
            model.DiaryNo = Id;
            model.RoleName = CurrentSession.RoleName;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetQDetailsByDiaryNo", model);
            model.UId = CurrentSession.UserID;
            foreach (var item in model.tQuestionModel)
            {
                model.Title = item.Subject;
                model.MemberId = item.MemberId;
                model.QuestionID = item.QuestionID;
                model.Details = item.MainQuestion;
                model.EventId = item.EventId;
                model.NoticeDate = item.NoticeDate.Value;
                model.NoticeTime = item.NoticeTime.Value;
                model.IsContentFreeze = item.IsContentFreeze;
                model.MinisterId = Convert.ToInt32(item.MinisterID);
                model.DepartmentId = item.DepartmentId;
                model.Checked = item.Checked;
                model.IsTypistFreeze = item.IsTypistFreeze;
                model.ConstituencyName = item.ConstituencyName;
                model.DiaryNo = item.DiaryNumber;
                model.IsClubChecked = item.IsBracketed.HasValue;
                model.ClubDiaryNo = item.BracketedDNo;
                string SDate = Convert.ToString(item.NoticeDate.Value);

                SDate = Convert.ToString(item.NoticeDate.Value);
                if (SDate != "")
                {
                    if (SDate.IndexOf("/") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("/"));
                        if (Month.Length < 2)
                        {
                            Month = "0" + Month;
                        }
                        Day = SDate.Substring(SDate.IndexOf("/") + 1, (SDate.LastIndexOf("/") - SDate.IndexOf("/") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("/") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                    if (SDate.IndexOf("-") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("-"));
                        Day = SDate.Substring(SDate.IndexOf("-") + 1, (SDate.LastIndexOf("-") - SDate.IndexOf("-") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("-") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                }
                mAssembly model4 = new mAssembly();
                model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;
                mMinisteryMinisterModel obj2 = new mMinisteryMinisterModel();
                //obj2.MinistryID = 0;
                obj2.MinisterMinistryName = "Select Minister";
                model.ministerList.Add(obj2);
                if (model.DepartmentId == null)
                {
                    List<tMemberNotice> ListtMemberNotice = new List<tMemberNotice>();
                    tMemberNotice noticeModel = new tMemberNotice();
                    noticeModel.DepartmentId = Convert.ToString(0);
                    noticeModel.DepartmentName = "Select Department";
                    ListtMemberNotice.Add(noticeModel);
                    model.DepartmentLt = ListtMemberNotice;
                }
                if (model.DepartmentId != null)
                {
                    tMemberNotice noticeModel1 = new tMemberNotice();
                    mMinistry model1 = new mMinistry();
                    model1.MinistryID = model.MinisterId;
                    noticeModel1.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model1) as List<tMemberNotice>;
                    noticeModel1.DepartmentId = Convert.ToString(0);
                    noticeModel1.DepartmentName = "Select Department";
                    model.DepartmentLt = noticeModel1.DepartmentLt;
                }
            }
            return PartialView("_PopUpView", model);

        }
        public ActionResult UnstaredFullDetailById(string Id)
        {
            string Month = "";
            string Day = "";
            string Year = "";
            tMemberNotice model = new tMemberNotice();
            SBL.DomainModel.Models.Event.mEvent mEventModel = new SBL.DomainModel.Models.Event.mEvent();
            model.mEvents = (ICollection<SBL.DomainModel.Models.Event.mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
            model.mMemberModel = Helper.ExecuteService("Member", "GetAllMembers", null) as List<mMember>;
            model.UId = CurrentSession.UserID;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetButtons", model);
            tQuestion mdl = new tQuestion();
            model.DiaryNo = Id;
            // model.RoleName = CurrentSession.RoleName;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "UnstaredGetQDetailsByDiaryNo", model);
            // model.UId = CurrentSession.UserID;
            foreach (var item in model.tQuestionModel)
            {
                model.Title = item.Subject;
                model.MemberId = item.MemberId;
                model.QuestionID = item.QuestionID;
                model.Details = item.MainQuestion;
                model.EventId = item.EventId;
                model.NoticeDate = item.NoticeDate.Value;
                model.NoticeTime = item.NoticeTime.Value;
                model.IsContentFreeze = item.IsContentFreeze;
                model.MinisterId = Convert.ToInt32(item.MinisterID);
                model.DepartmentId = item.DepartmentId;
                model.Checked = item.Checked;
                model.IsTypistFreeze = item.IsTypistFreeze;
                model.ConstituencyName = item.ConstituencyName;
                model.DiaryNo = item.DiaryNumber;
                model.IsClubChecked = item.IsBracketed.HasValue;
                model.ClubDiaryNo = item.BracketedDNo;
                string SDate = Convert.ToString(item.NoticeDate.Value);

                SDate = Convert.ToString(item.NoticeDate.Value);
                if (SDate != "")
                {
                    if (SDate.IndexOf("/") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("/"));
                        if (Month.Length < 2)
                        {
                            Month = "0" + Month;
                        }
                        Day = SDate.Substring(SDate.IndexOf("/") + 1, (SDate.LastIndexOf("/") - SDate.IndexOf("/") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("/") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                    if (SDate.IndexOf("-") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("-"));
                        Day = SDate.Substring(SDate.IndexOf("-") + 1, (SDate.LastIndexOf("-") - SDate.IndexOf("-") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("-") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                }
                mAssembly model4 = new mAssembly();
                model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;
                mMinisteryMinisterModel obj2 = new mMinisteryMinisterModel();
                //obj2.MinistryID = 0;
                obj2.MinisterMinistryName = "Select Minister";
                model.ministerList.Add(obj2);
                if (model.DepartmentId == null)
                {
                    List<tMemberNotice> ListtMemberNotice = new List<tMemberNotice>();
                    tMemberNotice noticeModel = new tMemberNotice();
                    noticeModel.DepartmentId = Convert.ToString(0);
                    noticeModel.DepartmentName = "Select Department";
                    ListtMemberNotice.Add(noticeModel);
                    model.DepartmentLt = ListtMemberNotice;
                }
                if (model.DepartmentId != null)
                {
                    tMemberNotice noticeModel1 = new tMemberNotice();
                    mMinistry model1 = new mMinistry();
                    model1.MinistryID = model.MinisterId;
                    noticeModel1.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model1) as List<tMemberNotice>;
                    noticeModel1.DepartmentId = Convert.ToString(0);
                    noticeModel1.DepartmentName = "Select Department";
                    model.DepartmentLt = noticeModel1.DepartmentLt;
                }
            }
            return PartialView("_PopUpView", model);

        }

        public JsonResult BackToTypistById(int Qid)
        {
            tMemberNotice MNdl = new tMemberNotice();
            MNdl.QuestionID = Qid;
            MNdl.Message = Helper.ExecuteService("Notice", "BackToTypistById", MNdl) as string;
            return Json(MNdl.Message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PendingNotices(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            TypistQuestionModel model = new TypistQuestionModel();
            model.UId = CurrentSession.UserID;
            model.RoleName = CurrentSession.RoleName;
            model.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            model.SessionID = Convert.ToInt16(CurrentSession.SessionId);
            model = (TypistQuestionModel)Helper.ExecuteService("Notice", "GetNoticesforTypist", model);
            return PartialView("_NoticeGrid", model);
        }
        public ActionResult GetPdfByNoticeID(int? NoticeID)
        {
            tMemberNotice Attachment = new tMemberNotice();

            if (NoticeID != null)
            {
                //Attachment.tQuestionModel = (ICollection<tQuestionModel>)Helpers.Helper.ExecuteService("Questions", "GetAttachment", questionID);
                string savedPDF = Helpers.Helper.ExecuteService("Notice", "GetAttachmentNotices", NoticeID) as string;

                //foreach (var item in Attachment.tQuestionModel)
                //{
                //    savedPDF = item.AnswerAttachLocation;
                //    ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(savedPDF);
                //}


                ViewBag.PDFPath = savedPDF;
            }
            return PartialView("_GetPdf");
        }

        public ActionResult GetDatatNotices(int NoticeId)
        {
            string Month = "";
            string Day = "";
            string Year = "";
            tMemberNotice model = new tMemberNotice();
            SBL.DomainModel.Models.Event.mEvent mEventModel = new SBL.DomainModel.Models.Event.mEvent();
            model.mEvents = (ICollection<SBL.DomainModel.Models.Event.mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
            model.mMemberModel = Helper.ExecuteService("Member", "GetAllMembers", null) as List<mMember>;
            model.UId = CurrentSession.UserID;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetButtons", model);
            int Id = Convert.ToInt32(NoticeId);
            model.NoticeId = Id;
            model.RoleName = CurrentSession.RoleName;
            model = (tMemberNotice)Helper.ExecuteService("Notice", "GetNoticeTypistDetails", model);
            foreach (var item in model.tQuestionModel)
            {
                model.Title = item.Subject;
                model.MemberId = item.MemberId;
                model.NoticeId = item.NoticeId;
                model.Details = item.Notice;
                model.EventId = item.EventId;
                model.NoticeDate = item.NoticeDate.Value;
                model.NoticeTime = item.NoticeTime.Value;
                model.MinisterId = Convert.ToInt32(item.MinisterID);
                model.DepartmentId = item.DepartmentId;
                model.TypistFreezed = item.IsTypistFreeze;
                model.ConstituencyName = item.ConstituencyName;
                model.NoticeNumber = item.NoticeNumber;
                model.MemberName = item.MemberName;
                model.IsProofReading = item.IsProofReading;
                string SDate = Convert.ToString(item.NoticeDate.Value);

                SDate = Convert.ToString(item.NoticeDate.Value);
                if (SDate != "")
                {
                    if (SDate.IndexOf("/") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("/"));
                        if (Month.Length < 2)
                        {
                            Month = "0" + Month;
                        }
                        Day = SDate.Substring(SDate.IndexOf("/") + 1, (SDate.LastIndexOf("/") - SDate.IndexOf("/") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("/") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                    if (SDate.IndexOf("-") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("-"));
                        Day = SDate.Substring(SDate.IndexOf("-") + 1, (SDate.LastIndexOf("-") - SDate.IndexOf("-") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("-") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        model.NDate = SDate;
                    }
                }
                mAssembly model4 = new mAssembly();
                model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinisterMinistry", null) as List<mMinisteryMinisterModel>;

                if (model.DepartmentId == null)
                {
                    List<tMemberNotice> ListtMemberNotice = new List<tMemberNotice>();
                    tMemberNotice noticeModel = new tMemberNotice();
                    noticeModel.DepartmentId = "0";
                    noticeModel.DepartmentName = "Select Department";
                    ListtMemberNotice.Add(noticeModel);
                    model.DepartmentLt = ListtMemberNotice;
                }
                if (model.DepartmentId != null)
                {
                    tMemberNotice noticeModel1 = new tMemberNotice();
                    mMinistry model1 = new mMinistry();
                    model1.MinistryID = model.MinisterId;
                    noticeModel1.DepartmentLt = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model1) as List<tMemberNotice>;
                    model.DepartmentLt = noticeModel1.DepartmentLt;

                }
            }
            if (model.RoleName == "Vidhan Sabha Typist")
            {
                return PartialView("_NoticeTypistView", model);
            }

            return PartialView("_ProfViewNotice", model);

        }

        public JsonResult BackToTypistNotices(Updatevalue model)
        {
            tMemberNotice Update = new tMemberNotice();
            Update.NoticeId = model.NoticeId;
            Update = Helper.ExecuteService("Notice", "NoticeBackToTypist", Update) as tMemberNotice;
            Update.Message = "Update Sucessfully";
            return Json(Update, JsonRequestBehavior.AllowGet);
        }


        public JsonResult StayNoticeUpdateEntry(Updatevalue model)
        {
            tMemberNotice Update = new tMemberNotice();
            Update.NoticeId = model.NoticeId;
            Update.Subject = model.Title;
            Update.Notice = model.Details;
            Update.MinistryId = model.MinisterId;
            Update.DepartmentId = model.DepartmentId;
            Update = Helper.ExecuteService("Notice", "StayUpdateNotice", Update) as tMemberNotice;
            Update.Message = "Update Sucessfully";
            return Json(Update, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateEntryNotices(Updatevalue model)
        {

            tMemberNotice Update = new tMemberNotice();
            Update.NoticeId = model.NoticeId;
            Update.UId = CurrentSession.UserID;
            Update.Subject = model.Title;
            Update.Notice = model.Details;
            Update.MinistryId = model.MinisterId;
            Update.DepartmentId = model.DepartmentId;
            Update = Helper.ExecuteService("Notice", "UpdateNoticesTypist", Update) as tMemberNotice;
            Update.Message = "Update Sucessfully";

            return Json(Update, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateSentProofNotices(Updatevalue model)
        {

            tMemberNotice Update = new tMemberNotice();
            Update.NoticeId = model.NoticeId;
            Update.Subject = model.Title;
            Update.Notice = model.Details;
            Update.MinistryId = model.MinisterId;
            Update.DepartmentId = model.DepartmentId;
            Update = Helper.ExecuteService("Notice", "SentToProofReader", Update) as tMemberNotice;
            Update.Message = "Update Sucessfully";

            return Json(Update, JsonRequestBehavior.AllowGet);
        }



    }
    public class Updatevalue
    {
        public int QuestionID { get; set; }
        [AllowHtml]
        public string Title { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Details { get; set; }
        public int NoticeId { get; set; }
        public int MinisterId { get; set; }
        public string DepartmentId { get; set; }
        public string NDate { get; set; }
        public TimeSpan NoticeTime { get; set; }
        public int MemberId { get; set; }
        public int EventId { get; set; }
        public string ProofUId { get; set; }
        // public bool IsClubChecked { get; set; }
        public string ClubDiaryNo { get; set; }
        public string DiaryNo { get; set; }
        public string ConstName { get; set; }
        public bool? IsQuestionInPart { get; set; }
        public bool? IsHindi { get; set; }
        public string ConsiId { get; set; }
        public string ConName { get; set; }
    }
}
