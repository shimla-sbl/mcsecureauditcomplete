﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Notices
{
    public class NoticesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Notices";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Notices_default",
                "Notices/{controller}/{action}/{id}",
                new { action = "NoticeEntry", id = UrlParameter.Optional }
            );
        }
    }
}
