﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;



namespace SBL.eLegistrator.HouseController.Web.Areas.Notices.Extensions
{
    public static class LegislationFixationExtensions
    {
        public static List<PaperLaidViewModel> ToViewModel(this List<tPaperLaidV> list)
        {
            var result = list.Select(paper => new PaperLaidViewModel()
            {
                DepartmentName = paper.DeparmentName,
                MinistryName = paper.MinistryName,
                ItemId = paper.PaperLaidId,
                Title = paper.Title,
                IsPaperLaid = null
            });
            return result.ToList();
        }

        public static List<CompletePaperLaidViewModel> ToViewModel1(this List<tPaperLaidV> list)
        {
            var result = list.Select(paper => new CompletePaperLaidViewModel()
            {
                DepartmentName = paper.DeparmentName,
                MinistryName = paper.MinistryName,
                ItemId = paper.PaperLaidId,
                Title = paper.Title,
                IsPaperLaid = null,
                AskedBy = paper.MinisterName,
                CommonNumberForAll = paper.BillNumber,
                QuesStage = paper.Status,
                Subject = paper.Title,
                PaperlaidPriority = paper.PaperlaidPriority,
                BussinessType=paper.BussinessType,
                StatusText=paper.StatusText,
               
            });
            return result.ToList();
        }



        public static List<PaperLaidViewModel> ToViewModel(this List<tQuestion> list)
        {
            var result = list.Select(paper => new PaperLaidViewModel()
            {
                DepartmentName = paper.DepartmentID.GetDepartmentName(),
                DiaryNumber = paper.DiaryNumber,
                QuestionNumber = paper.QuestionNumber,
                MinistryName = paper.MinistryId.GetMinisterName(),
                ItemId = paper.QuestionID,
                Subject = paper.Subject.ToString(),
                AskedBy = paper.MemberID.GetMemberNameByID(),
                IsPending = paper.IsPending,
                IsContentFreeze = paper.IsContentFreeze,
                IsDetailFreeze = paper.IsQuestionDetailFreez,
                IsQuestionFreeze = paper.IsQuestionFreeze,
                IsInitialApprove = paper.IsInitialApproved,
                IsRejected = paper.IsRejected,
                IsTypeChanged = paper.IsTypeChange,
                IsPaperLaid = true
            });
            return result.ToList();
        }

        public static List<CompletePaperLaidViewModel> ToViewModel1(this List<tQuestion> list)
        {
            var result = list.Select(paper => new CompletePaperLaidViewModel()
            {
                DepartmentName = paper.DepartmentID.GetDepartmentName(),
                CommonNumberForAll = paper.DiaryNumber,
                QuestionNumber = paper.QuestionNumber,
                MinistryName = paper.MinistryId.GetMinisterName(),
                ItemId = paper.QuestionID,
                Subject = Convert.ToString(paper.Subject),
                AskedBy = paper.MemberID.GetMemberNameByID(),
                IsPending = paper.IsPending,
                IsContentFreeze = paper.IsContentFreeze,
                IsDetailFreeze = paper.IsQuestionDetailFreez,
                IsQuestionFreeze = paper.IsQuestionFreeze,
                IsInitialApprove = paper.IsInitialApproved,
                IsRejected = paper.IsRejected,
                IsTypeChanged = paper.IsTypeChange,
                IsPaperLaid = true,
                QuesStage = paper.QuestionStatus,
                IsFinalApprove = paper.IsFinalApproved,
                Priority = paper.Priority,
                IsTranslation = paper.IsTranslation,
                IsClubbed = paper.IsClubbed,
                IsBracketed = paper.IsBracket,
                PaperCategoryTypeId = paper.QuestionType,
                IsFixedDate = Convert.ToDateTime(paper.IsFixedDate)



                //PaperLaidId = paper.PaperLaidId,
                //DepartmentSubmittedDate = paper.DepartmentSubmittedDate


            });

            return result.ToList();
        }

        public static List<PaperLaidViewModel> ToViewModel(this List<tMemberNotice> list)
        {
            var result = list.Select(paper => new PaperLaidViewModel()
            {
                DepartmentName = paper.DepartmentId.GetDepartmentName(),
                NoticeNumber = paper.NoticeNumber,
                MinistryName = paper.MinistryId.GetMinisterName(),
                ItemId = paper.NoticeId,
                Subject = paper.Subject.ToString(),
                AskedBy = paper.MemberId.GetMemberNameByID(),
                IsPending = paper.IsPending,
                IsPaperLaid = false
                //DepartmentName = paper.DepartmentName,
                //MinistryName = paper.MinisterName,
                //ItemId = paper.NoticeId.ToString(),
                //Title = paper.Notice.ToString(),
                //IsPaperLaid = false,
                //NoticeNumber = paper.NoticeNumber
            });
            return result.ToList();
        }

        public static List<CompletePaperLaidViewModel> ToViewModel1(this List<tMemberNotice> list)
        {
            var result = list.Select(paper => new CompletePaperLaidViewModel()
            {
                DepartmentName = paper.DepartmentId.GetDepartmentName(),
                CommonNumberForAll = paper.NoticeNumber,
                MinistryName = paper.MinistryId.GetMinisterName(),
                ItemId = paper.NoticeId,
                Subject = string.IsNullOrEmpty(paper.Subject) ? string.Empty : paper.Subject.ToString(),
                AskedBy = paper.MemberId.GetMemberNameByID(),
                IsPending = paper.IsPending,
                IsPaperLaid = false,
                Priority = paper.Priority,
                RuleNo = paper.RuleNo

            });
            return result.ToList();
        }

        public static List<RecipientGroupViewModel> ToViewModel(this List<RecipientGroup> list)
        {
            var result = list.Select(RecipientGroup => new RecipientGroupViewModel()
            {
                GroupId = RecipientGroup.GroupID,
                GroupName = RecipientGroup.Name,
                GroupDesc = RecipientGroup.Description,
                IsPublic = RecipientGroup.IsPublic,
                IsActive = RecipientGroup.IsActive,
                ConstituencyCode = RecipientGroup.ConstituencyCode,
                DepartmentID = RecipientGroup.DepartmentCode,
                RankingOrder = RecipientGroup.RankingOrder
            });

            return result.ToList();
        }
        public static RecipientGroup ToDomainModelPanchayat(this RecipientGroupViewModel model)
        {
            return new RecipientGroup
            {
                GroupID = model.GroupId,
                Name = model.GroupName,
                PancayatCode = model.PanchayatCode,
                Description = model.GroupDesc,
                IsPublic = model.IsPublic,
                IsActive = model.IsActive,
                ConstituencyCode = model.ConstituencyCode,
                DepartmentCode = model.DepartmentID,
                RankingOrder = model.RankingOrder
            };
        }

        public static RecipientGroupViewModel ToViewModelPanchayat(this RecipientGroup model)
        {
            return new RecipientGroupViewModel
            {
                GroupId = model.GroupID,
                GroupName = model.Name,
                IsActive = model.IsActive,
                IsPublic = model.IsPublic,
                ConstituencyCode = model.ConstituencyCode,
                GroupDesc = model.Description,
                DepartmentID = model.DepartmentCode,
                PanchayatCode = model.PancayatCode,
                RankingOrder = model.RankingOrder
            };
        }
        public static RecipientGroupViewModel ToViewModel(this RecipientGroup model)
        {
            return new RecipientGroupViewModel
            {
                GroupId = model.GroupID,
                GroupName = model.Name,
                IsActive = model.IsActive,
                IsPublic = model.IsPublic,
                ConstituencyCode = model.ConstituencyCode,
                GroupDesc = model.Description,
                DepartmentID = model.DepartmentCode,
                OfficeId = model.OfficeId,
                RankingOrder = model.RankingOrder,
                IsConstituencyOffice = model.IsConstituencyOffice
            };
        }

        public static List<RecipientGroupMemberViewModel> ToViewModel(this List<RecipientGroupMember> list)
        {
            var result = list.Select(RecipientGroupMember => new RecipientGroupMemberViewModel()
            {
                GroupMemberID = RecipientGroupMember.GroupMemberID,
                GroupId = RecipientGroupMember.GroupID,
                Name = RecipientGroupMember.Name,
                Gender = RecipientGroupMember.Gender,
                Designation = RecipientGroupMember.Designation,
                DepartmentID = RecipientGroupMember.DepartmentCode,
                Mobile = RecipientGroupMember.MobileNo,
                EMail = RecipientGroupMember.Email,
                Address = RecipientGroupMember.Address,
                PinCode = RecipientGroupMember.PinCode,
                IsActive = RecipientGroupMember.IsActive,
                CreatedBy = RecipientGroupMember.CreatedBy,
                CreatedDate = RecipientGroupMember.CreatedDate,
                RankingOrder = RecipientGroupMember.RankingOrder
            });

            return result.ToList();
        }

        public static RecipientGroupMemberViewModel ToViewModel(this RecipientGroupMember model)
        {
            return new RecipientGroupMemberViewModel
            {
                GroupMemberID = model.GroupMemberID,
                GroupId = model.GroupID,
                Name = model.Name,
                Gender = model.Gender,
                Designation = model.Designation,
                DepartmentID = model.DepartmentCode,
                Mobile = model.MobileNo,
                EMail = model.Email,
                Address = model.Address,
                IsActive = model.IsActive,
                PinCode = model.PinCode,
                CreatedDate = model.CreatedDate,
                CreatedBy = model.CreatedBy,
                RankingOrder = model.RankingOrder,
                LandLineNumber = model.LandLineNumber

            };
        }

        public static RecipientGroupMember ToDomainModel(this RecipientGroupMemberViewModel model)
        {
            return new RecipientGroupMember
            {
                GroupMemberID = model.GroupMemberID,
                GroupID = model.GroupId,
                Name = model.Name,
                Gender = model.Gender,
                Designation = model.Designation,
                DepartmentCode = model.DepartmentID,
                MobileNo = model.Mobile,
                Email = model.EMail,
                Address = model.Address,
                PinCode = model.PinCode,
                IsActive = model.IsActive,
                CreatedBy = model.CreatedBy,
                CreatedDate = model.CreatedDate,
                ConstituencyCode = model.ConstituencyCode,
                RankingOrder = model.RankingOrder,
                LandLineNumber = model.LandLineNumber

            };
        }

        public static RecipientGroup ToDomainModel(this RecipientGroupViewModel model)
        {
            return new RecipientGroup
            {
                GroupID = model.GroupId,
                Name = model.GroupName,
                DepartmentCode = model.DepartmentID,
                Description = model.GroupDesc,
                IsPublic = model.IsPublic,
                IsActive = model.IsActive,
                ConstituencyCode = model.ConstituencyCode,
                OfficeId = model.OfficeId,
                RankingOrder = model.RankingOrder,
                IsConstituencyOffice = model.IsConstituencyOffice
            };
        }

        public static string GetDepartmentName(this String Id)
        {
            if (Id != null && Id != "")
            {
                if (Id != "0")
                {
                    var result = (mDepartment)Helper.ExecuteService("Legislative", "GetDepartmentById", new mDepartment { deptId = Id });
                    if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
                    {
                        return result.deptname;
                    }
                    else
                    {
                        return result.deptnameLocal;
                    }

                }
                else
                {
                    return "";
                }
            }
            return null;
        }

        public static string GetDepartmentNameLocal(this String Id)
        {
            if (Id != null && Id != "")
            {
                if (Id != "0")
                {
                    var result = (mDepartment)Helper.ExecuteService("Legislative", "GetDepartmentById", new mDepartment { deptId = Id });

                    return result.deptnameLocal;

                }
                else
                {
                    return "";
                }
            }
            return null;
        }


        public static string GetMinisterName(this int? Id)
        {
            if (Id != null && Id != 0)
            {
                var result = (mMinistry)Helper.ExecuteService("Legislative", "GetMinistryById", new tQuestion { MinistryId = Id });
                return result.MinistryName;
            }

            return null;
        }

        public static string GetMinistryNameHindi(this int? Id)
        {
            if (Id != null && Id != 0)
            {
                var result = (mMinistry)Helper.ExecuteService("Legislative", "GetMinistryById", new tQuestion { MinistryId = Id });
                return result.MinistryNameLocal;
            }

            return null;
        }
        public static string GetMinisterNameLocal(this int? Id)
        {
            if (Id != null && Id != 0)
            {
                var result = (mMinistry)Helper.ExecuteService("Legislative", "GetMinistryById", new tQuestion { MinistryId = Id });
                return result.MinistryNameLocal;
            }

            return null;
        }

        public static string GetMemberNameByID(this int Id)
        {
            if (Id != 0)
            {
                var result = (mMember)Helper.ExecuteService("Legislative", "GetMemberNameByID", new mMember { MemberCode = Id });
                return result.Prefix.Trim() + " " + result.Name.Trim();
            }
            return null;
        }

        public static string GetMemberNameByIDHindi(this int Id)
        {
            if (Id != 0)
            {
                var result = (mMember)Helper.ExecuteService("Legislative", "GetMemberNameByID", new mMember { MemberCode = Id });
                return result.NameLocal.Trim();
            }
            return null;
        }
        public static string GetMemberNameByIDWithOutPrefix(this int Id)
        {
            if (Id != 0)
            {
                var result = (mMember)Helper.ExecuteService("Legislative", "GetMemberNameByID", new mMember { MemberCode = Id });
                return result.Name.Trim();
            }
            return null;
        }

        public static string GetMemberNameByID(this int? Id)
        {
            if (Id != 0 && Id != null)
            {
                var result = (mMember)Helper.ExecuteService("Legislative", "GetMemberNameByID", new mMember { MemberCode = (int)Id });
                return result.Prefix.Trim() + " " + result.Name.Trim();
            }
            return null;
        }
        public static string GetMemberNameLocalByID(this int Id)
        {
            if (Id != 0)
            {
                var result = (mMember)Helper.ExecuteService("Legislative", "GetMemberNameByID", new mMember { MemberCode = (int)Id });
                return result.NameLocal.Trim();
            }
            return null;
        }



        public static List<DiaryModel> RuleList(this int QuesId)
        {
            if (QuesId != 0)
            {
                var result = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetRulesforRejectedQuestionById", QuesId);
                return result;
            }
            return null;
        }

        public static string TypistEmpName(this string Eid)
        {
            if (Eid != null)
            {
                var result = (string)Helper.ExecuteService("LegislationFixation", "GetEmpNameByEid", Eid);
                return result;
            }


            return null;
        }

        public static string GetFileNamePath(this long? PaperLaidId)
        {
            if (PaperLaidId != null && PaperLaidId != 0)
            {
                var data = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetFileNamePath", PaperLaidId);
                return data.FilePath + "," + data.version;
            }
            return null;
        }

        public static string GetFileNamePath1(this long PaperLaidId)
        {
            if (PaperLaidId != 0)
            {
                var data = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetFileNamePath", PaperLaidId);
                return data.FilePath + "," + data.version;
            }
            return null;
        }

        public static string GetFileOfOnlineMember(this int QId)
        {
            if (QId != 0)
            {
                var data = (DiaryModel)Helper.ExecuteService("Diary", "GetFileOfOnlineMember", QId);
                return data.FilePath + "," + "sign_" + data.FileName;
            }
            return null;
        }
        public static string GetFileOfOnlineMemberNotices(this int QId)
        {
            if (QId != 0)
            {
                var data = (DiaryModel)Helper.ExecuteService("Diary", "GetFileOfOnlineMemberNotice", QId);
                return data.FilePath + "," + "sign_" + data.FileName;
            }
            return null;
        }

        public static string GetDeptSubmittedDate(this long? PaperLaidId)
        {
            if (PaperLaidId != 0)
            {
                var data = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetDeptSubmittedDate", PaperLaidId);
                return data.DeptSubmittedDate.ToString();
            }
            return null;
        }

        public static string GetFixedDateBySDtId(this int? SdtId)
        {
            if (SdtId != 0 && SdtId != null)
            {
                var data = (DateTime)Helper.ExecuteService("LegislationFixation", "GetFixedDateBySDtId", SdtId);
                return data.ToString("dd/MM/yyyy");
            }
            return null;
        }

        public static string GetMinisterSubmittedDate(this long PaperLaidId)
        {
            if (PaperLaidId != 0)
            {
                var data = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetMinisterSubmittedDate", PaperLaidId);
                return data.MinisterSubmittedDate.ToString();
            }
            return null;
        }

        public static string GetConstNameLocalByNo(this string ConstNo)
        {
            if (ConstNo != null && ConstNo != "" && ConstNo != "0")
            {
                var data = (DiaryModel)Helper.ExecuteService("LegislationFixation", "GetConstNameLocalByNo", ConstNo);
                return data.ConstituencyName.ToString();
            }
            return null;
        }

        public static int GetTotalCountByMenu(this string Menu, int MdleId, int? MemberId)
        {

            if (Menu != null && Menu != "")
            {

                switch (Menu)
                {
                    case "Online Submission":
                        {
                            var result = (mUserModules)Helper.ExecuteService("Notice", "GetTotalOnlineSubmissionCnt", new mUserModules { ModuleName = Menu, ModuleId = MdleId, MemberId = MemberId });
                            return result.TotalCount;
                        }
                    //case "Tours":
                    //    {
                    //        var result = (int)Helper.ExecuteService("Notice", "GetTotalTourCnt", new mUserModules { ModuleName = Menu, ModuleId = MdleId, MemberId = MemberId });
                    //    }
                    //case "Member Gallery":
                    //    {
                    //        var result = (int)Helper.ExecuteService("Notice", "GetTotalGalleryCnt", new mUserModules { ModuleName = Menu, ModuleId = MdleId, MemberId = MemberId });
                    //    }
                }
            }
            return 0;
        }
        public static int GetTotalCountBySubMenu(this string SubMenu, int? MemberId)
        {
            int Cnt = 0;
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.MemberId = Convert.ToInt16(MemberId);
            Dmdl.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            Dmdl.SessionID = Convert.ToInt16(CurrentSession.SessionId);

            tUserAccessRequest requstModel = new tUserAccessRequest();
            requstModel.DomainAdminstratorId = Convert.ToInt16(CurrentSession.SubUserTypeID);
            mUsers usermdl = new mUsers();
            usermdl.UserId = new Guid(CurrentSession.UserID);
            usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
            if (usermdl.IsSecretoryId == true)
            {
                requstModel.SecreataryId = Convert.ToInt16(usermdl.SecretoryId);
            }
            else if (usermdl.IsMember == "True")
            {
                requstModel.MemberId = Convert.ToInt16(usermdl.UserName);
            }
            else if (usermdl.IsHOD == true)
            {
                requstModel.HODId = Convert.ToInt16(usermdl.SecretoryId);
            }


            if (SubMenu != null && SubMenu != "")
            {

                switch (SubMenu)
                {
                    case "Starred Questions":
                        {
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalOnlineStarredCnt", Dmdl);
                            return result.ResultCount;
                        }
                    case "Unstarred Questions":
                        {
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalUnstarredCnt", Dmdl);

                            return result.ResultCount;
                        }
                    case "Notices":
                        {
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalNoticesCnt", Dmdl);
                            return result.ResultCount;
                        }
                    case "Category Gallery":
                        {
                            //var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalGalleryCount", null);
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalGalleryCount", Dmdl.MemberId);
                            return result.ResultCount;
                        }
                    case "Album Gallery":
                        {
                            //var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalAlbumGalleryCnt", null);
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalAlbumGalleryCnt", Dmdl.MemberId);
                            return result.ResultCount;
                        }
                    case "Gallery":
                        {
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalGalleriesCnt", Dmdl.MemberId);
                            return result.ResultCount;
                        }
                    case "MemberPanchayats":
                        {
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalPanchayatCnt", null);
                            return result.ResultCount;
                        }
                    case "Today Tours":
                        {
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalTodayTourCnt", Dmdl.MemberId);
                            Cnt = result.ResultCount;
                            return result.ResultCount;
                        }
                    case "Upcoming Tours":
                        {
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalupcomingTourCnt", Dmdl.MemberId);
                            Cnt = result.ResultCount;
                            return result.ResultCount;
                        }
                    case "Previous Tours":
                        {
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalRecentTourCnt", Dmdl.MemberId);
                            Cnt = result.ResultCount;
                            return result.ResultCount;
                        }
                    case "UnPublished Tours":
                        {
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetTotalUnpublishedTourCnt", Dmdl.MemberId);
                            Cnt = result.ResultCount;
                            return result.ResultCount;
                        }
                    case "Recipient Groups":
                        {
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetRecipientGroupCnt", Dmdl.MemberId);
                            Cnt = result.ResultCount;
                            return result.ResultCount;
                        }
                    case "Recipient Group Members":
                        {
                            var result = (DiaryModel)Helper.ExecuteService("Notice", "GetRecipientGroupMemberCnt", Dmdl.MemberId);
                            Cnt = result.ResultCount;
                            return result.ResultCount;
                        }

                    case "Pending Request":
                        {
                            var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetPendingUserRequestCount", requstModel);
                            return result.Count;
                        }
                    case "Accepted Request":
                        {
                            var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetAcceptedUserRequestCount", requstModel);
                            return result.Count;
                        }
                    case "Rejected Request":
                        {
                            var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetRejectedUserRequestCount", requstModel);
                            return result.Count;
                        }



                }
            }
            return Cnt;
        }

        public static string GetSignPath(this string UserId)
        {
            DiaryModel Dmdl = new DiaryModel();
            Dmdl.UserId = new Guid(CurrentSession.UserID);
            Dmdl.SignFilePath = Helper.ExecuteService("Notice", "GetSignPathByUserId", Dmdl) as string;
            return Dmdl.SignFilePath;
        }


        public static string GetRuleNo(this int RuleId)
        {
            string RuleName = "";
            RuleName = Helper.ExecuteService("LegislationFixation", "GetRuleNameByRuleId", RuleId) as string;
            return RuleName;
        }

        #region Added venkat code for Department Dynamic Counting
        public static int GetTotalCountBySubMenuforMinster(this string SubMenu, int? MemberId, string Menu)
        {
            SubMenu = SubMenu.Trim();
            Menu = Menu.Trim();
            int Cnt = 0;
            // int ResultCount = 0;
            tPaperLaidV Dmdl = new tPaperLaidV();
            tUserAccessRequest requstModel = new tUserAccessRequest();
            requstModel.DomainAdminstratorId = Convert.ToInt16(CurrentSession.SubUserTypeID);
            //requstModel.UserID = new Guid(CurrentSession.UserID);
            mUsers usermdl = new mUsers();
            usermdl.UserId = new Guid(CurrentSession.UserID);
            usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
            if (usermdl.IsSecretoryId == true)
            {
                requstModel.SecreataryId = Convert.ToInt16(usermdl.SecretoryId);
            }
            else if (usermdl.IsMember == "True")
            {
                requstModel.MemberId = Convert.ToInt16(usermdl.UserName);
            }
            else if (usermdl.IsHOD == true)
            {
                requstModel.HODId = Convert.ToInt16(usermdl.SecretoryId);
            }

            Dmdl.MemberId = Convert.ToInt16(MemberId);
            Dmdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            Dmdl.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            tPaperLaidV model = new tPaperLaidV();
            model.MemberId = Convert.ToInt32(CurrentSession.UserName);

            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

           
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            //model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            //model.DSCApplicable = siteSettingMod.DSCApplicable;

            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model.MinistryId = model.MinistryId;
            model.DepartmentId = CurrentSession.DeptID;


            if (CurrentSession.RoleID.ToUpper() == "0FC60E41-C78E-46B0-B8AC-F6EB709BE999")
            {
                model.RequestMessage = "ChiefMinisterAdmin";
            }

           // model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetCountForBillsandNotice", model);

            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
            {

                if (SubMenu != null && SubMenu != "" && Menu == "Starred Questions")
                {

                    switch (SubMenu)
                    {
                        case "Pending By Department":
                            {

                                model.Type = "Pending By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetStarredQuestionCounterforMinisters", model);
                                // Cnt = result1;


                                return result1.ResultCount;
                            }

                        case "Sent By Department":
                            {
                                model.Type = "Sent By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetStarredQuestionCounterforMinisters", model);
                                return result1.ResultCount;
                            }
                        case "Upcoming Lob":
                            {
                                model.Type = "Upcoming Lob";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetStarredQuestionCounterforMinisters", model);
                                return result1.ResultCount;
                            }
                        case "Laid In The House":
                            {
                                model.Type = "Laid In The House";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetStarredQuestionCounterforMinisters", model);
                                return result1.ResultCount;
                            }


                    }
                }
                else if (Menu == "UNStarred Questions")
                {
                    switch (SubMenu)
                    {
                        case "Pending By Department":
                            {
                                model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                                model.Type = "Pending By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetUnStarredQuestionCounterforMinisters", model);
                                //  Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                        case "Sent By Department":
                            {
                                model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                                model.Type = "Sent By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetUnStarredQuestionCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Upcoming Lob":
                            {
                                model.Type = "Upcoming Lob";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetUnStarredQuestionCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Laid In The House":
                            {
                                model.Type = "Laid In The House";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetUnStarredQuestionCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }


                    }
                }
                else if (Menu == "Notices")
                {
                    switch (SubMenu)
                    {
                        case "Pending By Department":
                            {

                                model.Type = "Pending By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticesCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                        case "Sent By Department":
                            {
                                model.Type = "Sent By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticesCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Upcoming Lob":
                            {
                                model.Type = "Upcoming Lob";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticesCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Laid In The House":
                            {
                                model.Type = "Laid In The House";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticesCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                    }
                }
                else if (Menu == "Bills")
                {
                    switch (SubMenu)
                    {
                        case "Pending By Department":
                            {

                                model.Type = "Pending By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillsCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                        case "Sent By Department":
                            {
                                model.Type = "Sent By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillsCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Upcoming Lob":
                            {
                                model.Type = "Upcoming Lob";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillsCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Laid In The House":
                            {
                                model.Type = "Laid In The House";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillsCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                    }
                }
                else if (Menu == "Others Papers")
                {
                    switch (SubMenu)
                    {
                        case "Pending By Department":
                            {

                                model.Type = "Pending by Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetCountForOtherPaperLaidTypesMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                        case "Sent By Department":
                            {
                                model.Type = "Sent By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetCountForOtherPaperLaidTypesMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Upcoming Lob":
                            {
                                model.Type = "Upcoming Lob";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPapersCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Laid In The House":
                            {
                                model.Type = "Laid In The House";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPapersCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Pending To Lay":
                            {
                                model.Type = "Pending To Lay";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPapersCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                    }
                }
                else if (Menu == "Access Control")
                {
                    switch (SubMenu)
                    {
                        case "Pending Request":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetPendingUserRequestCount", requstModel);
                                return result.Count;
                            }
                        case "Accepted Request":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetAcceptedUserRequestCount", requstModel);
                                return result.Count;
                            }
                        case "Rejected Request":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetRejectedUserRequestCount", requstModel);
                                return result.Count;
                            }
                    }
                }




                else if (Menu == "Bills Reimbursement")
                {
                    switch (SubMenu)
                    {
                        case "Bill Information":
                            {
                                var result = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", null)).Where(m => m.ClaimantId == int.Parse(CurrentSession.MemberCode)).ToList();
                                return result.Count();
                            }

                    }
                }

            }
            else
            {
                if (SubMenu != null && SubMenu != "" && Menu == "तारांकित प्रश्न")
                {

                    switch (SubMenu)
                    {
                        case "विभाग द्वारा अपूर्ण":
                            {

                                model.Type = "Pending By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetStarredQuestionCounterforMinisters", model);
                                // Cnt = result1;


                                return result1.ResultCount;
                            }

                        case "विभाग द्वारा भेजे गए":
                            {
                                model.Type = "Sent By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetStarredQuestionCounterforMinisters", model);
                                return result1.ResultCount;
                            }
                        case "Upcoming Lob":
                            {
                                model.Type = "Upcoming Lob";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetStarredQuestionCounterforMinisters", model);
                                return result1.ResultCount;
                            }
                        case "Laid In The House":
                            {
                                model.Type = "Laid In The House";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetStarredQuestionCounterforMinisters", model);
                                return result1.ResultCount;
                            }


                    }
                }
                else if (Menu == "अतारांकित प्रश्न")
                {
                    switch (SubMenu)
                    {
                        case "विभाग द्वारा अपूर्ण":
                            {
                                model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                                model.Type = "Pending By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetUnStarredQuestionCounterforMinisters", model);
                                //  Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                        case "विभाग द्वारा भेजे गए":
                            {
                                model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                                model.Type = "Sent By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetUnStarredQuestionCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Upcoming Lob":
                            {
                                model.Type = "Upcoming Lob";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetUnStarredQuestionCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Laid In The House":
                            {
                                model.Type = "Laid In The House";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetUnStarredQuestionCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }


                    }
                }
                else if (Menu == "सूचनाएं")
                {
                    switch (SubMenu)
                    {
                        case "विभाग द्वारा अपूर्ण":
                            {

                                model.Type = "Pending By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticesCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                        case "विभाग द्वारा भेजे गए":
                            {
                                model.Type = "Sent By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticesCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Upcoming Lob":
                            {
                                model.Type = "Upcoming Lob";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticesCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Laid In The House":
                            {
                                model.Type = "Laid In The House";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticesCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                    }
                }
                else if (Menu == "विधेयक")
                {
                    switch (SubMenu)
                    {
                        case "विभाग द्वारा अपूर्ण":
                            {

                                model.Type = "Pending By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillsCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                        case "विभाग द्वारा भेजे गए":
                            {
                                model.Type = "Sent By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillsCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Upcoming Lob":
                            {
                                model.Type = "Upcoming Lob";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillsCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Laid In The House":
                            {
                                model.Type = "Laid In The House";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillsCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                    }
                }
                else if (Menu == "अन्य के कागजात")
                {
                    switch (SubMenu)
                    {
                        case "विभाग द्वारा अपूर्ण":
                            {

                                model.Type = "Pending by Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetCountForOtherPaperLaidTypesMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                        case "विभाग द्वारा भेजे गए":
                            {
                                model.Type = "Sent By Department";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetCountForOtherPaperLaidTypesMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Upcoming Lob":
                            {
                                model.Type = "Upcoming Lob";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPapersCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Laid In The House":
                            {
                                model.Type = "Laid In The House";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPapersCounterforMinisters", model);
                                //Cnt = (int)result1;
                                return result1.ResultCount;
                            }
                        case "Pending To Lay":
                            {
                                model.Type = "Pending To Lay";

                                var result1 = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPapersCounterforMinisters", model);
                                // Cnt = (int)result1;
                                return result1.ResultCount;
                            }

                    }
                }
                else if (Menu == "अभिगम नियंत्रण")
                {
                    switch (SubMenu)
                    {
                        case "लंबित अनुरोध":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetPendingUserRequestCount", requstModel);
                                return result.Count;
                            }
                        case "स्वीकृत अनुरोध":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetAcceptedUserRequestCount", requstModel);
                                return result.Count;
                            }
                        case "अस्वीकृत अनुरोध":
                            {
                                var result = (tUserAccessRequest)Helper.ExecuteService("Module", "GetRejectedUserRequestCount", requstModel);
                                return result.Count;
                            }
                    }
                }




                else if (Menu == "Bills Reimbursement")
                {
                    switch (SubMenu)
                    {
                        case "Bill Information":
                            {
                                var result = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", null)).Where(m => m.ClaimantId == int.Parse(CurrentSession.MemberCode)).ToList();
                                return result.Count();
                            }

                    }
                }
            }

            return Cnt;

        }
        #endregion

    }
}