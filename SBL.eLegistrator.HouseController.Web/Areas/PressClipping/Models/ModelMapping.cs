﻿using SBL.DomainModel.Models.PressClipping;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PressClipping.Models
{
    public static class ModelMapping
    {
        // Extenstion Method for PressClipping
        public static List<PressClipViewModel> ToViewModel(this IEnumerable<mPressClipping> model)
        {

            var ViewModel = model.Select(part => new PressClipViewModel()
            {
                PressClipID = part.PressClipID,
                NewsPaperName = part.NewsPaperName,
                NewsDate = part.NewsDate,
                PageNumber = part.PageNumber,
                Subject = part.Subject,
                ContentKeywords = part.ContentKeywords,
                ScannedCopyImg = part.ScannedCopyImg,
                AliasNameOfImg = part.AliasNameOfImg,
                ContentText = part.ContentText,
                Category = part.Category,
                Status = part.Status,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            });
            return ViewModel.ToList();
        }

        public static PressClipViewModel ToViewModel1(this mPressClipping model, string Mode)
        {
            return new PressClipViewModel
            {
                PressClipID = model.PressClipID,
                NewsPaperName = model.NewsPaperName,
                NewsDate = model.NewsDate,
                PageNumber = model.PageNumber,
                Subject = model.Subject,
                ContentKeywords = model.ContentKeywords,
                ScannedCopyImg = model.ScannedCopyImg,
                AliasNameOfImg = model.AliasNameOfImg,
                ContentText = model.ContentText,
                Category = model.Category,
                Status = model.Status,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static mPressClipping ToDomainModel(this PressClipViewModel model)
        {
            return new mPressClipping
            {
                PressClipID = model.PressClipID,
                NewsPaperName = model.NewsPaperName,
                NewsDate = model.NewsDate,
                PageNumber = model.PageNumber,
                Subject = model.Subject,
                ContentKeywords = model.ContentKeywords,
                ScannedCopyImg = model.ScannedCopyImg,
                AliasNameOfImg = model.AliasNameOfImg,
                ContentText = model.ContentText,
                Category = model.Category,
                Status = model.Status,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }

        #region Private

        public static SelectList GetCategoryList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Genral", Value = "Genral" });
            result.Add(new SelectListItem() { Text = "Vidhan Sabha", Value = "VidhanSabha" });

            return new SelectList(result, "Value", "Text");
        }
        #endregion
    }
}