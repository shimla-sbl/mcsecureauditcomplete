﻿using SBL.DomainModel.Models.PressClipping;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.PressClipping.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace SBL.eLegistrator.HouseController.Web.Areas.PressClipping.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class PressClippingController : Controller
    {
        //
        // GET: /PressClipping/PressClipping/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mPressClipping>)Helper.ExecuteService("PressClipping", "GetAllPressClipDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public ActionResult SetAvailability()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mPressClipping>)Helper.ExecuteService("PressClipping", "GetAllPressClipDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public ActionResult CreatePressClipping()
        {
            PressClipViewModel model = new PressClipViewModel();
            model.CategoryList = ModelMapping.GetCategoryList();
            model.Mode = "Add";
            return View(model);
        }

        public ActionResult EditPressClip(int Id)
        {

            mPressClipping stateToEdit = (mPressClipping)Helper.ExecuteService("PressClipping", "GetPressClipById", new mPressClipping { PressClipID = Id });

            var model = stateToEdit.ToViewModel1("Edit");
            model.CategoryList = ModelMapping.GetCategoryList();
            //  model.Mode = "Edit";

            return View("CreatePressClipping", model);
        }

        public ActionResult DeletePressClip(int Id)
        {
            mPressClipping stateToDelete = (mPressClipping)Helper.ExecuteService("PressClipping", "GetPressClipById", new mPressClipping { PressClipID = Id });
            if (stateToDelete != null && stateToDelete.ScannedCopyImg != null && stateToDelete.ScannedCopyImg.Length > 0)
                RemoveExistingPhoto(stateToDelete.ScannedCopyImg);
            Helper.ExecuteService("PressClipping", "DeletePressClip", stateToDelete);
            TempData["Msg"] = "Sucessfully Deleted";
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SavePressClip(PressClipViewModel model, HttpPostedFileBase file, string OldImageUrl)
        {
            if (ModelState.IsValid)
            {
                Guid FileName = Guid.NewGuid();
                var mPressClipping = model.ToDomainModel();
             
                if (model.Mode == "Add")
                {
                    mPressClipping.ScannedCopyImg = UploadPhoto(file, FileName);
                    mPressClipping.Status = false;
                    Helper.ExecuteService("PressClipping", "CreatePressClip", mPressClipping);
                }
                else
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        mPressClipping.ScannedCopyImg = UploadPhoto(file, FileName);
                        RemoveExistingPhoto(OldImageUrl);
                    }
                      
                    Helper.ExecuteService("PressClipping", "UpdatePressClip", mPressClipping);
                }
            }

            return RedirectToAction("Index");
        }

        public JsonResult UpdatePressCliping(FormCollection collection)
        {
            string Result = string.Empty;

            string PCRow = collection["hdnPCRow"];

            // Add QUalification
            if (PCRow != null && PCRow.Length > 0)
            {
                string[] RowCount = PCRow.Split(',');

                foreach (var val in RowCount)
                {

                    try
                    {
                        int Id = 0;
                        int.TryParse(collection["hdnPCId-" + val], out Id);
                        mPressClipping stateToEdit = (mPressClipping)Helper.ExecuteService("PressClipping", "GetPressClipById", new mPressClipping { PressClipID = Id });

                        if (collection["chbStatus-" + val] != null && collection["chbStatus-" + val] == "on")
                            stateToEdit.Status = true;
                        else
                            stateToEdit.Status = false;

                        Helper.ExecuteService("PressClipping", "UpdatePressClip", stateToEdit);
                        Result = "Availability saved sucessfully ";
                    }
                    catch (Exception ex)
                    {

                        Result = ex.ToString();
                    }
                }
            }
            else
            {
                Result = "Unable to recived data";

            }
            return Json(Result, JsonRequestBehavior.AllowGet);


        }

        public PartialViewResult SearchPressClip(string NewsPaperName, string Subject, string ContKey, string NewsDateFrom, string NewsDateTo, string Category)
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(NewsPaperName))
                sb.Append(NewsPaperName + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Subject))
                sb.Append(Subject + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(ContKey))
                sb.Append(ContKey + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(NewsDateFrom))
                sb.Append(NewsDateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(NewsDateTo))
                sb.Append(NewsDateTo + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Category))
                sb.Append(Category + ",");
            else
                sb.Append(" " + ",");

           
            // Call Split method
            var State = (List<mPressClipping>)Helper.ExecuteService("PressClipping", "SearchPressClipping", sb.ToString());
            var model = State.ToViewModel();

            return PartialView("/Areas/PressClipping/Views/PressClipping/_PressClippingList.cshtml", model);


        }
        #region Private Methods
        private string UploadPhoto(HttpPostedFileBase File, Guid FileName)
        {
            if (File != null)
            {

                string directory = Server.MapPath("~/Images/PressClip");
                string path = System.IO.Path.Combine(directory, FileName + "_" + File.FileName);
                try
                {
                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }
                    File.SaveAs(path);
                }
                catch (Exception)
                {

                    throw;
                }

                return (FileName + "_" + File.FileName);
            }
            return null;
        }

        private void RemoveExistingPhoto(string OldPhoto)
        {
            if (!string.IsNullOrEmpty(OldPhoto))
            {
                string path = System.IO.Path.Combine(Server.MapPath("~/Images/PressClip"), OldPhoto);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
            }

        }
        #endregion

        #region Set Availability
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SetAvailability(int[] PassIdArray, string Mode)
        {
            Mode = "Approve";// Sanitizer.GetSafeHtmlFragment(Mode);
            var PassesToProcess = new List<mPressClipping>();
            foreach (var Id in PassIdArray)
            {
                PassesToProcess.Add(new mPressClipping { PressClipID = Id });
            }
            if (Mode == "Approve")
            {
                Helper.ExecuteService("Pass", "ApproveMultiplePassRequest", PassesToProcess);
            }
            else if (Mode == "Reject")
            {
                Helper.ExecuteService("Pass", "RejectMultiplePassRequest", PassesToProcess);
            }

            return RedirectToAction("ApprovePasses");
        }
        #endregion
    }

}
