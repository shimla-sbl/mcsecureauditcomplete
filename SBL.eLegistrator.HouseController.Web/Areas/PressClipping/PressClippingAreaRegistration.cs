﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PressClipping
{
    public class PressClippingAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PressClipping";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PressClipping_default",
                "PressClipping/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
