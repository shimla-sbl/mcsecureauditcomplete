﻿using EvoPdf;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.User;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class AuthorityLetterController : Controller
    {
        //
        // GET: /AccountsAdmin/AuthorityLetter/

        public ActionResult Index()
        {
            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var State = (List<mAuthorityLetter>)Helper.ExecuteService("Budget", "GetAllAuthorityLetter", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();

                var model = State.ToViewModel();



                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult AuthorityLetterIndex()
        {
            try
            {

                var State = (List<mAuthorityLetter>)Helper.ExecuteService("Budget", "GetAllAuthorityLetter", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();

                var model = State.ToViewModel();

                return PartialView("/Areas/AccountsAdmin/Views/AuthorityLetter/_AuthorityLetterIndex.cshtml", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }


        public PartialViewResult PopulateAuthorityLetter(int Id, string Action)
        {
            mAuthorityLetter AlObj = (mAuthorityLetter)Helper.ExecuteService("Budget", "GetAuthorityLetterById", new mAuthorityLetter { AuthorityLetterID = Id });

            AuthorityLetterViewModel model = new AuthorityLetterViewModel();
            if (Id > 0)
                model = AlObj.ToViewModel1("Edit");

            model.Mode = Action;
            model.LetterTypeList = new SelectList(Enum.GetValues(typeof(AuthorityLetterType)).Cast<AuthorityLetterType>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");

            return PartialView("/Areas/AccountsAdmin/Views/AuthorityLetter/_PopulateAuthorityLetter.cshtml", model);
        }


        public ActionResult GenerateAuthorityLetter()
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "AuthorityBy" });

            AuthorityLetterViewModel model = new AuthorityLetterViewModel();
            model.Mode = "Add";
            model.LetterTypeList = new SelectList(Enum.GetValues(typeof(AuthorityLetterType)).Cast<AuthorityLetterType>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            if (SiteSettingsSessn != null)
                model.AuthorityBy = SiteSettingsSessn.SettingValue;
            else
                model.AuthorityBy = string.Empty;
            return View(model);
        }

        public ActionResult ViewAuthorityLetter(int Id)
        {
            mAuthorityLetter AlObj = (mAuthorityLetter)Helper.ExecuteService("Budget", "GetAuthorityLetterById", new mAuthorityLetter { AuthorityLetterID = Id });
            AuthorityLetterViewModel model = new AuthorityLetterViewModel();
            model = AlObj.ToViewModel1("Edit");
            return View(model);
        }


        [HttpPost]
        public JsonResult AuthorityLetterResult(int AuthorityLetterID, DateTime EncashmentDate, int LetterType, string BillIds, string AuthorityBy)
        {
            string BillNumber = string.Empty;
            string Result = string.Empty;
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "AuthorityBy" });

            if (SiteSettingsSessn != null && SiteSettingsSessn.SettingValue != AuthorityBy)
            {
                SiteSettingsSessn.SettingValue = AuthorityBy;
                Helper.ExecuteService("User", "UpdateSiteSettings", SiteSettingsSessn);
            }

            string LetterTypeS = ((SBL.DomainModel.Models.Enums.AuthorityLetterType)LetterType).ToString();
            DateTime GenerateDate = DateTime.Now;

            //SBL.DomainModel.Models.Enums.BillType.Member -4

            if (LetterTypeS == "Member")
            {
                var DesignationType = (int)((SBL.DomainModel.Models.Enums.DesignationType)Enum.Parse(typeof(SBL.DomainModel.Models.Enums.DesignationType), "Member"));

                StringBuilder Parameter = new StringBuilder();
                Parameter.Append(EncashmentDate.ToShortDateString() + ",");
                Parameter.Append(DesignationType);

                var MemberList = (List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForALMemeberAndOther", Parameter.ToString());


                StringBuilder EstablishIds = new StringBuilder();
                StringBuilder BillNumbers = new StringBuilder();
                foreach (var item in MemberList)
                {
                    EstablishIds.Append(item.EstablishBillID + ",");
                    BillNumbers.Append(item.BillNumber + ",");
                }
                BillIds = EstablishIds.ToString();
                BillNumber = BillNumbers.ToString();
                Result = GetMemberLetter(MemberList, EncashmentDate, SiteSettingsSessn.SettingValue);

            }
            else if (LetterTypeS == "SpeakerAndDeputySpeaker")
            {

                var DesignationTypeS = (int)((SBL.DomainModel.Models.Enums.DesignationType)Enum.Parse(typeof(SBL.DomainModel.Models.Enums.DesignationType), "Speaker"));
                var DesignationTypeDS = (int)((SBL.DomainModel.Models.Enums.DesignationType)Enum.Parse(typeof(SBL.DomainModel.Models.Enums.DesignationType), "DeputySpeaker"));

                StringBuilder Parameter = new StringBuilder();
                Parameter.Append(EncashmentDate.ToShortDateString() + ",");
                Parameter.Append(string.Format("{0}-{1}", DesignationTypeS, DesignationTypeDS));

                StringBuilder EstablishIds = new StringBuilder();
                StringBuilder BillNumbers = new StringBuilder();
                var MemberList = (List<AccountDetail>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForSpeakerAndDeputySpeaker", Parameter.ToString());
                string EtsblishId = string.Empty;
                foreach (var item in MemberList)
                {
                    if (EtsblishId != item.EstablishId.ToString())
                    {
                        EstablishIds.Append(item.EstablishId + ",");
                        BillNumbers.Append(item.BillNumber + ",");
                        EtsblishId = item.EstablishId.ToString();
                    }
                }

                BillIds = EstablishIds.ToString();
                BillNumber = BillNumbers.ToString();
                Result = GetSpeakerAndDeputySpeakerLetter(MemberList, EncashmentDate, SiteSettingsSessn.SettingValue);

            }

            else if (LetterTypeS == "Staff")
            {

                var DesignationTypeG = (int)((SBL.DomainModel.Models.Enums.DesignationType)Enum.Parse(typeof(SBL.DomainModel.Models.Enums.DesignationType), "GazettedStaff"));
                var DesignationTypeNG = (int)((SBL.DomainModel.Models.Enums.DesignationType)Enum.Parse(typeof(SBL.DomainModel.Models.Enums.DesignationType), "NonGazettedStaff"));

                StringBuilder Parameter = new StringBuilder();
                Parameter.Append(EncashmentDate.ToShortDateString() + ",");
                Parameter.Append(string.Format("{0}-{1}", DesignationTypeG, DesignationTypeNG));

                StringBuilder EstablishIds = new StringBuilder();
                StringBuilder BillNumbers = new StringBuilder();
                var MemberList = (List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBillForStaff", Parameter.ToString());
                string EtsblishId = string.Empty;
                foreach (var item in MemberList)
                {
                    EstablishIds.Append(item.EstablishBillID + ",");
                    BillNumbers.Append(item.BillNumber + ",");
                }
                BillIds = EstablishIds.ToString();
                BillNumber = BillNumbers.ToString();
                Result = GetLetterForStaff(MemberList, EncashmentDate, SiteSettingsSessn.SettingValue);

            }
            else if (LetterTypeS == "CustomBillNumbers")
            {

                StringBuilder Parameter = new StringBuilder();
                Parameter.Append(EncashmentDate.ToShortDateString() + "-");
                Parameter.Append(BillIds);

                var MemberList = (List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllReimburBillForCustom", Parameter.ToString());



                StringBuilder BillNumbers = new StringBuilder();
                StringBuilder EstablishIds = new StringBuilder();
                foreach (var item in MemberList)
                {
                    EstablishIds.Append(item.EstablishBillID + ",");
                    BillNumbers.Append(item.BillNumber + ",");
                }
                BillIds = EstablishIds.ToString();
                BillNumber = BillNumbers.ToString();
                Result = GetCustomLetter(MemberList, EncashmentDate, SiteSettingsSessn.SettingValue);

            }
            else if (LetterTypeS == "ExMember")
            {
                var DesignationType = (int)((SBL.DomainModel.Models.Enums.DesignationType)Enum.Parse(typeof(SBL.DomainModel.Models.Enums.DesignationType), "ExMember"));

                StringBuilder Parameter = new StringBuilder();
                Parameter.Append(EncashmentDate.ToShortDateString() + ",");
                Parameter.Append(DesignationType);

                var MemberList = (List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForALMemeberAndOther", Parameter.ToString());

                StringBuilder EstablishIds = new StringBuilder();
                StringBuilder BillNumbers = new StringBuilder();
                foreach (var item in MemberList)
                {
                    EstablishIds.Append(item.EstablishBillID + ",");
                    BillNumbers.Append(item.BillNumber + ",");
                }
                BillIds = EstablishIds.ToString();
                BillNumber = BillNumbers.ToString();
                Result = GetLetterForExMember(MemberList, EncashmentDate, SiteSettingsSessn.SettingValue);

            }
            else if (LetterTypeS == "Others")
            {
                StringBuilder EstablishIds = new StringBuilder();
                StringBuilder BillNumbers = new StringBuilder();
                var DesignationType = (int)((SBL.DomainModel.Models.Enums.DesignationType)Enum.Parse(typeof(SBL.DomainModel.Models.Enums.DesignationType), "Other"));
                var ExMemberDesignationType = (int)((SBL.DomainModel.Models.Enums.DesignationType)Enum.Parse(typeof(SBL.DomainModel.Models.Enums.DesignationType), "ExMember"));
                var ExStaffDesignationType = (int)((SBL.DomainModel.Models.Enums.DesignationType)Enum.Parse(typeof(SBL.DomainModel.Models.Enums.DesignationType), "ExStaff"));

                StringBuilder Parameter = new StringBuilder();
                Parameter.Append(EncashmentDate.ToShortDateString() + ",");
                Parameter.Append(string.Format("{0}-{1}-{2}", DesignationType, ExMemberDesignationType, ExStaffDesignationType));

                var List = (List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllBillForOtherExStaffExMember", Parameter.ToString());

                foreach (var item in List)
                {
                    EstablishIds.Append(item.EstablishBillID + ",");
                    BillNumbers.Append(item.BillNumber + ",");
                }

                BillIds = EstablishIds.ToString();
                BillNumber = BillNumbers.ToString();
                Result = GetLetterForOtherDoc(List, EncashmentDate, SiteSettingsSessn.SettingValue);

            }
            else if (LetterTypeS == "ExStaff")
            {
                var DesignationType = (int)((SBL.DomainModel.Models.Enums.DesignationType)Enum.Parse(typeof(SBL.DomainModel.Models.Enums.DesignationType), "ExStaff"));

                StringBuilder Parameter = new StringBuilder();
                Parameter.Append(EncashmentDate.ToShortDateString() + ",");
                Parameter.Append(DesignationType);

                var MemberList = (List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForALMemeberAndOther", Parameter.ToString());

                StringBuilder EstablishIds = new StringBuilder();
                StringBuilder BillNumbers = new StringBuilder();
                foreach (var item in MemberList)
                {
                    EstablishIds.Append(item.EstablishBillID + ",");
                    BillNumbers.Append(item.BillNumber + ",");
                }
                BillIds = EstablishIds.ToString();
                BillNumber = BillNumbers.ToString();
                Result = GetLetterForOtherDoc(MemberList, EncashmentDate, SiteSettingsSessn.SettingValue);

            }
            StringBuilder sb = new StringBuilder();
            if (!Result.Contains("No bill found"))
            {
                string Filename = string.Empty;
                //if (LetterTypeS == "Others")
                //    Filename = CreteWordDoc(Result, LetterType);
                //else
                //    Filename = CretePdf(Result, LetterType);

                Filename = CreteWordDoc(Result, LetterType);
                if (BillIds == null)
                    BillIds = string.Empty;
                if (AuthorityLetterID == 0)
                {
                    mAuthorityLetter obj = new mAuthorityLetter();
                    obj.EncashmentDate = EncashmentDate;
                    obj.IsActive = true;
                    obj.LetterType = LetterType.ToString();
                    obj.ModifiedWhen = DateTime.Now;
                    obj.CreationDate = DateTime.Now;
                    obj.BillIds = BillIds;
                    obj.BillNumbers = BillNumber;
                    obj.CreatedBy = CurrentSession.UserName;
                    obj.ModifiedBy = CurrentSession.UserName;
                    obj.FileName = Filename;

                    Helper.ExecuteService("Budget", "CreateAuthorityLetter", obj);


                }
                else
                {
                    var Model = (mAuthorityLetter)Helper.ExecuteService("Budget", "GetAuthorityLetterById", new mAuthorityLetter { AuthorityLetterID = AuthorityLetterID });

                    Model.IsActive = true;
                    Model.ModifiedWhen = DateTime.Now;
                    Model.BillIds = BillIds;
                    Model.ModifiedBy = CurrentSession.UserName;
                    Model.FileName = Filename;
                    Model.BillNumbers = BillNumber;
                    Helper.ExecuteService("Budget", "UpdateAuthorityLetter", Model);
                }


                sb.Append(Result);
                sb.Append(string.Format("<input type='hidden' name='FileName' id='FileName' value='{0}' />", Filename));
            }
            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }

        public string CretePdf(string Report, int LetterType)
        {
            string LetterTypeS = ((SBL.DomainModel.Models.Enums.AuthorityLetterType)LetterType).ToString();
            string Result = Report;

            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;

            // set the header height in points
            pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;

            TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan v1.0.0",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement1.TextAlign = HorizontalTextAlign.Left;
            TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement3.TextAlign = HorizontalTextAlign.Right;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(Result);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }
            string path = "";
            try
            {

                Guid FId = Guid.NewGuid();
                string fileName = FId + "_AuthorityLetter.pdf";



                string url = string.Format("~/AuthorityLetter/{0}/", LetterTypeS);
                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                path = Path.Combine(Server.MapPath(url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();



                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {


            }
        }

        public string CreteWordDoc(string Report, int LetterType)
        {
            string LetterTypeS = ((SBL.DomainModel.Models.Enums.AuthorityLetterType)LetterType).ToString();

            try
            {

                Guid FId = Guid.NewGuid();
                string fileName = FId + "_AuthorityLetter.doc";
                string url = string.Format("~/AuthorityLetter/{0}/", LetterTypeS);
                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(Server.MapPath(url), fileName);

                StreamWriter sw;
                sw = System.IO.File.CreateText(path);
                sw.WriteLine(Report);
                sw.Close();
                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {


            }
        }
        public ActionResult DownloadPdf(int LetterType, string FileName)
        {

            try
            {
                string LetterTypeS = ((SBL.DomainModel.Models.Enums.AuthorityLetterType)LetterType).ToString();
                string path = "";

                string url = string.Format("~/AuthorityLetter/{0}/", LetterTypeS);
                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                path = Path.Combine(Server.MapPath(url), FileName);

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    if (FileName.Contains(".doc"))
                        pathRes.FileDownloadName = string.Format("AuthorityLetterFor{0}.doc", LetterTypeS);
                    else
                        pathRes.FileDownloadName = string.Format("AuthorityLetterFor{0}.pdf", LetterTypeS);
                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }


        public static string GetMemberLetter(List<mEstablishBill> MemberList, DateTime GenerateDate, string AuthorityBy)
        {

            StringBuilder Result = new StringBuilder();
            StringBuilder EstablishIds = new StringBuilder();


            // Crete Html Template for Authority Letter
            //Result.Append("<body style='width:700px;margin:auto;'><section><header style='text-align:center;'>");
            Result.Append("<body style='width:600px;text-align:center;margin:auto;'><section><header style='text-align:center;'>");
            if (MemberList != null && MemberList.Count() > 0)
            {
                Result.Append("<table style='page-break-after: always; width: 100%;  margin-left:10%;'>  <tr> <td style='vertical-align: middle; text-align: center'>");
                Result.Append("<h4 style='text-align:center;font-weight:bold'>No: 3-40/80-VS</h4><h4 style='text-align:center;font-weight:bold'>Himachal Pradesh Vidhan Sabha Secretariat</h4><header>");
                Result.Append("<div><table style='width:100%'> <tr> <td style='width: 100px;'></td>  </tr> <tr>   <td style='text-align:center;font-weight:bold'></td> </tr>");
                Result.Append(" <tr><td style='text-align: left;left;font-weight:bold'>To</td> <td></td>   </tr>");
                Result.Append(string.Format("<tr><td></td> <td  style='text-align: left;font-weight:bold'>The Manager,<br>UCO Bank,<br>H.P.V.S. Branch, Shimla-4.<br><br> Shimla-171004,Dated: {0}.<br></td></tr> <br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("<tr><td style='text-align: left;left;font-weight:bold'>Subject:<br><br></td><td style='text-align: left;left;font-weight:bold'>Authority Letter for drawal of Bills of Honourable Members.<br><br></td> ");
                Result.Append("<tr></tr> </tr>  <tr><td style='text-align: left;left;font-weight:bold' >Sir/Madam,</td> <td></td> </tr>");
                Result.Append("<tr><td></td> <td style='text-align: left;'>");
                Result.Append("1.Please collect the amount of the following bills (duly passed by the Treasury) from SBI Shimla.<br>");
                Result.Append("2.Credit the amount thereof to the Saving Bank Accounts of the concerned Members as per the enclosed list.<br>");
                Result.Append("3.Please acknowledge the receipt.<br>");
                Result.Append("</td> </tr>  </table><br>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr><th style='border:1px solid black;border-collapse:collapse;'>S.NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill Detail</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");

                // Create Record for all bill Detail
                Decimal TotalAmount = 0;

                int Count = 1;

                foreach (var item in MemberList)
                {
                    mEstablishBill stateToEdit = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = item.EstablishBillID });
                    if (!stateToEdit.IsValidAuthority)
                    {
                        stateToEdit.IsValidAuthority = true;
                        Helper.ExecuteService("Budget", "UpdateEstablishBill", stateToEdit);
                    }
                    EstablishIds.Append(item.EstablishBillID + ",");
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));

                    mBudget Budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = item.BudgetId });
                    string BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}/{6}", Budget.MajorHead, Budget.SubMajorHead, Budget.MinorHead, Budget.SubHead, Budget.Plan, Budget.VotedCharged, Budget.ObjectCode);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'><b>{0}</b> of  <b>{1}</b> <b>{2}</b> </td>", item.BillNumber, item.FinancialYear, BudgetName));

                    DataSet dataSet = new DataSet();
                    var EstablishBillIds = new List<KeyValuePair<string, string>>();
                    EstablishBillIds.Add(new KeyValuePair<string, string>("@EstablishBillId", item.EstablishBillID.ToString()));

                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillIds);

                    string BillGrossAmount = dataSet.Tables[0].Rows[0][0].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][0]);
                    string BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][1]); ;
                    string BillDeduction = dataSet.Tables[0].Rows[0][2].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][2]);

                    decimal NetAmount = Convert.ToDecimal(dataSet.Tables[0].Rows[0][1]);
                    string SAmount = NetAmount.ToString("0.#####");// string.Format("{0:n0}", NetAmount);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                    Result.Append("</tr>");
                    Count++;
                    TotalAmount = TotalAmount + NetAmount;

                }
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b>", string.Empty));
                string TotalAmounts = TotalAmount.ToString("0.#####");// string.Format("{0:n0}", TotalAmount);
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalAmounts));
                Result.Append("</tr>");


                Result.Append("</table>");
                if (TotalAmount > 0)
                {
                    Result.Append(string.Format("<br><p style='text-align: left;'><b>Rs. {0}</b></p>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalAmount))));
                }
                Result.Append(string.Format("<div style='text-align:right; font-weight:bold;width: 90%;'><p>Yours Sincerely,<br><br><br></p><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div></div><br> <br></section>", AuthorityBy));

                Result.Append("</td></tr> </table><br/>");
                Result.Append("<br style='page-break-before: always'>");
                // for Binding Sub Voucher with account Details

                decimal TotalSubVoucher = 0;
                var AccountList = (List<AccountDetail>)Helper.ExecuteService("Budget", "GetAllAccountDetail", EstablishIds.ToString());
                Result.Append(" <table style='page-break-after: always; width: 100%; margin-left:10%;'> <tr> <td style='vertical-align: middle; text-align: center'>");
                Result.Append("<div style='text-align:center;'><h2>Bill Breakup</h2></div>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Sr. No.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Name of the Member</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill No./Object Code Text</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Account Number</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");
                if (AccountList != null && AccountList.Count() > 0)
                {
                    int AccCount = 1;

                    foreach (var item in AccountList)
                    {

                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", AccCount));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}/{1}</td>", item.BillNumber, item.ObjectCodeText));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.AccountNo));
                        string SAmount = item.VoucherAmount.ToString("0.#####");// string.Format("{0:n0}", item.VoucherAmount);
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                        Result.Append("</tr>");
                        AccCount++;
                        TotalSubVoucher = TotalSubVoucher + item.VoucherAmount;
                    }

                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b></td>"));
                    string TotalSubVoucherAmounts = TotalSubVoucher.ToString("0.#####");// string.Format("{0:n0}", TotalSubVoucher);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", TotalSubVoucherAmounts));
                    Result.Append("</tr></table> ");

                    if (TotalSubVoucher > 0)
                        Result.Append(string.Format("<br/><p  style='text-align: left;'><b>Rs. {0}</b></p><br>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalSubVoucher))));
                    Result.Append(string.Format("<div style='text-align:right; font-weight:bold;width: 90%;'><p>Yours Sincerely,<br><br><br></p><p>{0},<br>H.P. Vidhan Sabha<br>DDO Code: 039</p></div><br/><br/>", AuthorityBy));

                }
                else
                { Result.Append("<tr><td colspan='5'>No Sub Voucher with Account detail found</td></tr>"); }
                Result.Append("</td></tr> </table>");
                Result.Append("<br style='page-break-before: always'>");

                // this is for SBI bank

                Result.Append(" <table style='width: 100%; margin-left:10%;'> <tr> <td style='vertical-align: middle; text-align: center'>");
                Result.Append("<h4 style='text-align:center;font-weight:bold'>No: 3-40/80-VS</h4><h4 style='text-align:center;font-weight:bold'>Commissioner (Department)</h4><header>");
                Result.Append("<div><table style='width:90%'> <tr> <td style='width: 10px;'></td>  </tr> <tr>   <td style='text-align:center;font-weight:bold'></td> </tr>");
                Result.Append(" <tr><td style='text-align: left;left;font-weight:bold;width:10%;'>To</td> <td style='width:90%'></td>   </tr>");
                Result.Append(string.Format("<tr><td></td> <td  style='text-align: left;font-weight:bold'>The Manager,<br>State Bank of India,<br>Shimla-171003.<br><br>Shimla-171004,Dated:  {0}.<br></td></tr> <br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("<tr><td style='text-align: left;left;font-weight:bold'>Subject:<br><br></td><td style='text-align: left;left;font-weight:bold'>Authority Letter for drawal of Bills of Gazetted Staff.<br><br></td> ");
                Result.Append("<tr><td style='text-align: left;left;font-weight:bold' >Sir/Madam,</td> <td></td> </tr>");
                Result.Append("<tr> <td colspan=2 style='text-align: left;'>");
                Result.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;");
                Result.Append("I hereby authorise the Manager, UCO Bank, H.P. Vidhan Sabha Branch Shimla-171004 to encash the below mentioned bills on behalf of this Secretariat.<br>");
                Result.Append("</td> </tr>  </table><br><br>");
                Result.Append("<table style='width:90%;border: 1px solid black;border-collapse:collapse;'><tr><th style='border:1px solid black;border-collapse:collapse;'>S.NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill Detail</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");

                // Create Record for all bill Detail

                int CountSBI = 1;

                foreach (var item in MemberList)
                {

                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", CountSBI));
                    mBudget Budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = item.BudgetId });
                    string BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}/{6}", Budget.MajorHead, Budget.SubMajorHead, Budget.MinorHead, Budget.SubHead, Budget.Plan, Budget.VotedCharged, Budget.ObjectCode);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'><b>{0}</b> of <b>{1}</b>  <b>{2}</b> </td>", item.BillNumber, item.FinancialYear, BudgetName));
                    DataSet dataSetNew = new DataSet();
                    var EstablishBillIds = new List<KeyValuePair<string, string>>();
                    EstablishBillIds.Add(new KeyValuePair<string, string>("@EstablishBillId", item.EstablishBillID.ToString()));

                    dataSetNew = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillIds);

                    string BillGrossAmount = dataSetNew.Tables[0].Rows[0][0].ToString();// string.Format("{0:n0}", dataSetNew.Tables[0].Rows[0][0]);
                    string BillSanctionAmount = dataSetNew.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSetNew.Tables[0].Rows[0][1]); ;
                    string BillDeduction = dataSetNew.Tables[0].Rows[0][2].ToString();// string.Format("{0:n0}", dataSetNew.Tables[0].Rows[0][2]);

                    decimal NetAmount = Convert.ToDecimal(dataSetNew.Tables[0].Rows[0][1]);

                    string SAmount = NetAmount.ToString("0.#####");// string.Format("{0:n0}", NetAmount);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                    Result.Append("</tr>");
                    CountSBI++;
                }
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalAmounts));
                Result.Append("</tr>");

                Result.Append("</table>");
                if (TotalAmount > 0)
                {
                    Result.Append(string.Format("<br><p style='text-align: left;width: 100%;margin-left:3%; '><b>Rs. {0}</b></p>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalAmount))));
                }
                Result.Append(string.Format("<div style='text-align:right; font-weight:bold;width:90%;'><p>Yours Sincerely,<br><br><br></p><p>{0},<br>H.P. Vidhan Sabha<br>DDO Code: 039</p></div></div><br> <br></section>", AuthorityBy));
                //   Result.Append(string.Format("<br><br><p style='text-align:right;'>Generated On: <b>{0}</b></p><br><br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("</td></tr> </table><br/>");

            }
            else
            {
                Result.Append("<p><b>No bill found</b><p>");
            }
            Result.Append("</body>");
            return Result.ToString();
        }


        public static string GetSpeakerAndDeputySpeakerLetter(List<AccountDetail> MemberList, DateTime GenerateDate, string AuthorityBy)
        {

            StringBuilder Result = new StringBuilder();
            StringBuilder EstablishId = new StringBuilder();

            // Crete Html Template for Authority Letter
            Result.Append("<body style='width:600px;text-align:center;margin:auto;'><section><header style='text-align:center;'>");
            if (MemberList != null && MemberList.Count() > 0)
            {
                Result.Append("<h4 style='text-align:center;font-weight:bold'>No: 3-40/80-VS</h4><h4 style='text-align:center;font-weight:bold'>Commissioner (Department)</h4><header>");
                Result.Append("<div><table style='width:90%;  margin-left:10%;'> <tr> <td style='width: 100px;'></td>  </tr> <tr>   <td style='text-align:center;font-weight:bold'></td> </tr>");
                Result.Append(" <tr><td style='text-align: left;left;font-weight:bold;width:10%;'>To</td> <td style='width:90%;'></td>   </tr>");
                Result.Append(string.Format("<tr><td></td> <td  style='text-align: left;font-weight:bold'>The Manager,<br>State Bank of India,<br>Shimla-171003.<br><br> Shimla-171004,Dated: {0}.<br></td></tr> <br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("<tr><td style='text-align: left;left;font-weight:bold'>Subject:<br><br></td><td style='text-align: left;left;font-weight:bold'>Authority Letter for drawal of Bills of Honourable Speaker/Deputy Speaker.<br><br></td> ");
                Result.Append("<tr></tr> </tr>  <tr><td style='text-align: left;left;font-weight:bold' >Sir/Madam,</td> <td></td> </tr>");
                Result.Append("<tr>  <td colspan=2 style='text-align: left;'>");
                Result.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;");
                Result.Append("I hereby authorise the Manager, UCO Bank, H.P. Vidhan Sabha Branch Shimla-171004 to collect the amount of the following bills in respect of Honourable Speaker/Deputy Speaker.<br>");
                Result.Append("</td> </tr>  </table><br>");
                Result.Append("<table style='width:90%;margin-left:10%;border: 1px solid black;border-collapse:collapse;'><tr><th style='border:1px solid black;border-collapse:collapse;'>S.NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill No.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Name of the Member</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Account No.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");

                // Create Record for all bill Detail
                Decimal TotalAmount = 0;

                int Count = 1;

                foreach (var item in MemberList)
                {
                    mEstablishBill stateToEdit = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = item.EstablishId });
                    if (!stateToEdit.IsValidAuthority)
                    {
                        stateToEdit.IsValidAuthority = true;
                        Helper.ExecuteService("Budget", "UpdateEstablishBill", stateToEdit);
                    }
                    EstablishId.Append(item.EstablishId + ",");
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));
                    mBudget Budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = stateToEdit.BudgetId });
                    string BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}/{6}", Budget.MajorHead, Budget.SubMajorHead, Budget.MinorHead, Budget.SubHead, Budget.Plan, Budget.VotedCharged, Budget.ObjectCode);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'> <b>{0}</b> of  <b>{1}</b>  <b>{2}</b> </td>", item.BillNumber, item.FinancialYear, BudgetName));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.AccountNo));
                    string SAmount = item.VoucherAmount.ToString("0.#####");// string.Format("{0:n0}", item.VoucherAmount);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                    Result.Append("</tr>");
                    Count++;
                    TotalAmount = TotalAmount + item.VoucherAmount;

                }
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b>", string.Empty));
                string TotalAmounts = TotalAmount.ToString("0.#####");// string.Format("{0:n0}", TotalAmount);
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalAmounts));
                Result.Append("</tr>");

                Result.Append("</table>");
                if (TotalAmount > 0)
                {
                    Result.Append(string.Format("<br><p style='text-align: left;width:90%;  margin-left:4%;'><b>Rs. {0}</b></p>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalAmount))));
                }
                Result.Append(string.Format("<div style='text-align:right; font-weight:bold;width:90%;  margin-left:10%;'><p>Yours Sincerely,<br><br><br></p><p>{0},<br>H.P. Vidhan Sabha </p></div><br/><br/>", AuthorityBy));

                Result.Append("<div><p style='text-align: justify;width:90%;  margin-left:4%;'><b>Copy is forwarded to:</b> The Manager, UCO Bank, H.P. Vidhan Sabha, Shimla-171004. He is requested that the amount of the said bills may please be credited to the savings bank accounts as shown above.</p></div><br/><br/>");
                Result.Append(string.Format("<div style='text-align:right;width:90%;  margin-left:10%; font-weight:bold;'><p>Yours Sincerely,<br><br><br></p><p>{0},<br>H.P. Vidhan Sabha </p></div><br/><br/>", AuthorityBy));
                Result.Append("</div><br> <br></section>");

                //Result.Append(string.Format("<br><p style='text-align:right;'>Generated On: <b>{0}</b></p><br><br>", GenerateDate.ToString("dd/MM/yyyy")));
            }
            else
            {
                Result.Append("<tr><td colspan='5'>No bill found</td></tr>");
            }
            Result.Append("</body>");


            return Result.ToString();
        }

        public static string GetCustomLetter(List<mEstablishBill> MemberList, DateTime GenerateDate, string AuthorityBy)
        {

            StringBuilder Result = new StringBuilder();

            // Crete Html Template for Authority Letter
            Result.Append("<body style='width:600px;text-align:center;margin:auto;'><section><header style='text-align:center;'>");
            if (MemberList != null && MemberList.Count() > 0)
            {
                Result.Append("<h4 style='text-align:center;font-weight:bold'>No: 3-40/80-VS</h4><h4 style='text-align:center;font-weight:bold'>Commissioner (Department)</h4><header>");
                Result.Append("<div><table style='width:100%;  margin-left:10%;'> <tr> <td style='width: 50px;'></td><td></td>  </tr> <tr>   <td style='text-align:center;font-weight:bold'></td><td></td> </tr>");
                Result.Append(" <tr><td style='text-align: left;left;font-weight:bold;width: 10%;'>To</td> <td></td>   </tr>");
                Result.Append(string.Format("<tr><td></td><td  style='text-align: left;font-weight:bold'>The Manager,<br>State Bank of India,<br>Shimla-171003.<br> <br>Shimla-171004, Dated:{0}.<br></td></tr> <br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("<tr><td style='text-align: left;font-weight:bold'>Subject:<br><br></td><td style='text-align: left;left;font-weight:bold'>Authority Letter for drawal of Bills.<br><br></td> ");
                Result.Append("</tr>  <tr><td style='text-align: left;left;font-weight:bold' >Sir/Madam,</td> <td></td> </tr>");
                Result.Append("<tr> <td colspan=2 style='text-align: left;'>");
                Result.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;");
                Result.Append("I hereby authorise Sh._______________________ Cashier of H.P. Vidhan Sabha Secretariat to encash the bills noted below. The specimen signatures of the Cashier is attested below :<br>");
                Result.Append("</td> </tr>  </table><br>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr><th style='border:1px solid black;border-collapse:collapse;'>S.NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill Detail</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");

                // Create Record for all bill Detail
                Decimal TotalAmount = 0;

                int Count = 1;

                foreach (var item in MemberList)
                {
                    mEstablishBill stateToEdit = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = item.EstablishBillID });
                    if (!stateToEdit.IsValidAuthority)
                    {
                        stateToEdit.IsValidAuthority = true;
                        Helper.ExecuteService("Budget", "UpdateEstablishBill", stateToEdit);
                    }

                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));
                    mBudget Budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = item.BudgetId });
                    string BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}/{6}", Budget.MajorHead, Budget.SubMajorHead, Budget.MinorHead, Budget.SubHead, Budget.Plan, Budget.VotedCharged, Budget.ObjectCode);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'> <b>{0}</b> of   <b>{1}</b>   <b>{2}</b> </td>", item.BillNumber, item.FinancialYear, BudgetName));

                    DataSet dataSet = new DataSet();
                    var EstablishBillId = new List<KeyValuePair<string, string>>();
                    EstablishBillId.Add(new KeyValuePair<string, string>("@EstablishBillId", item.EstablishBillID.ToString()));

                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillId);

                    string BillGrossAmount = dataSet.Tables[0].Rows[0][0].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][0]);
                    string BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][1]); ;
                    string BillDeduction = dataSet.Tables[0].Rows[0][2].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][2]);

                    decimal NetAmount = Convert.ToDecimal(dataSet.Tables[0].Rows[0][1]);
                    string SAmount = NetAmount.ToString("0.#####");// string.Format("{0:n0}", NetAmount);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                    Result.Append("</tr>");
                    Count++;
                    TotalAmount = TotalAmount + NetAmount;

                }
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b>", string.Empty));
                string TotalAmounts = TotalAmount.ToString("0.#####");// string.Format("{0:n0}", TotalAmount);
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalAmounts));
                Result.Append("</tr>");

                Result.Append("</table>");
                if (TotalAmount > 0)
                {
                    Result.Append(string.Format("<p style='text-align: left;'><b>Rs. {0}</b></p>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalAmount))));
                }
                Result.Append(string.Format("<div style='text-align:right; font-weight:bold;'><p>Yours Sincerely,<br><br></p><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div><br/>", AuthorityBy));

                Result.Append(string.Format("<div  style='text-align: left;font-weight:bold;'><p><b>Attested</b></p><p>Signature of Cashier</p><br/><br/><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div>", AuthorityBy));
                Result.Append("</div><br> <br></section>");

                //   Result.Append(string.Format("<br><p style='text-align:right;'>Generated On: <b>{0}</b></p><br><br>", GenerateDate.ToString("dd/MM/yyyy")));
            }
            else
            {
                Result.Append("<tr><td colspan='5'>No bill found</td></tr>");
            }
            Result.Append("</body>");
            return Result.ToString();
        }

        public static string GetLetterForExMember(List<mEstablishBill> MemberList, DateTime GenerateDate, string AuthorityBy)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "RemitterDetail" });
            string[] RemDetail = SiteSettingsSessn.SettingValue.Split('-');

            string EmployeeName = (RemDetail.Count() > 4 ? RemDetail[4] : "");
            string EmpMobile = (RemDetail.Count() > 5 ? RemDetail[5] : "");
            StringBuilder Result = new StringBuilder();
            StringBuilder EstablishIds = new StringBuilder();

            // Crete Html Template for Authority Letter
            Result.Append("<body style='width:600px;text-align:center;margin:auto;'><section><header style='text-align:center;'>");
            if (MemberList != null && MemberList.Count() > 0)
            {
                Result.Append("<table style='width: 100%;  margin-right:10%;'>  <tr> <td style='vertical-align: middle; text-align: center'>");
                Result.Append("<h2 style='text-align:center;font-weight:bold'>Himachal  Pradesh Vidhan Sabha Secretariat</h2><h4 style='text-align:center;font-weight:bold'>No: 3-40/80-VS</h4><header>");
                Result.Append("<div><table style='width:100%'> <tr> <td style='width: 100px;'></td>  </tr> <tr>   <td style='text-align:center;font-weight:bold'></td> </tr>");
                Result.Append(" <tr><td style='text-align: left;left;font-weight:bold'>To</td> <td></td>   </tr>");
                Result.Append(string.Format("<tr><td></td> <td  style='text-align: left;font-weight:bold'>The Manager,<br>State Bank of India,<br>Shimla-171003.<br><br> Shimla-171004,  Dated: {0}.<br></td></tr> <br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("<tr><td style='text-align: left;left;font-weight:bold'>Subject:<br><br></td><td style='text-align: left;left;font-weight:bold'>Authority letter for drawal of BilIs through RTGS/NEFT.<br><br></td> ");
                Result.Append("<tr></tr> </tr>  <tr><td style='text-align: left;left;font-weight:bold' >Sir/Madam,</td> <td></td> </tr>");
                Result.Append("<tr><  <td colspan=2 style='text-align: left;'>");
                Result.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;");
                Result.Append(string.Format("I hereby authorise Sh. {0}, Cashier whose Mobile No.{1} of HP Vidhan Sabha Secretariat for drawl of the bills noted below through RTGS/NEFT as per particulars of the beneficiary's recorded in the authority.<br>", EmployeeName, EmpMobile));
                Result.Append("</td> </tr>  </table><br><h3 style='text-align:left'>DETAIL OF BILLS</h3>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr><th style='border:1px solid black;border-collapse:collapse;'>Sr.NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill Detail</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");

                // Create Record for all bill Detail
                Decimal TotalAmount = 0;

                int Count = 1;

                foreach (var item in MemberList)
                {
                    mEstablishBill stateToEdit = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = item.EstablishBillID });
                    if (!stateToEdit.IsValidAuthority)
                    {
                        stateToEdit.IsValidAuthority = true;
                        Helper.ExecuteService("Budget", "UpdateEstablishBill", stateToEdit);
                    }
                    EstablishIds.Append(item.EstablishBillID + ",");
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));
                    mBudget Budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = item.BudgetId });
                    string BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}/{6}", Budget.MajorHead, Budget.SubMajorHead, Budget.MinorHead, Budget.SubHead, Budget.Plan, Budget.VotedCharged, Budget.ObjectCode);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'> <b>{0}</b> of   <b>{1}</b>   <b>{2}</b> </td>", item.BillNumber, item.FinancialYear, BudgetName));


                    DataSet dataSet = new DataSet();
                    var EstablishBillIds = new List<KeyValuePair<string, string>>();
                    EstablishBillIds.Add(new KeyValuePair<string, string>("@EstablishBillId", item.EstablishBillID.ToString()));

                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillIds);

                    string BillGrossAmount = dataSet.Tables[0].Rows[0][0].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][0]);
                    string BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][1]); ;
                    string BillDeduction = dataSet.Tables[0].Rows[0][2].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][2]);

                    decimal NetAmount = Convert.ToDecimal(dataSet.Tables[0].Rows[0][1]);


                    string SAmount = NetAmount.ToString("0.#####");// string.Format("{0:n0}", NetAmount);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                    Result.Append("</tr>");
                    Count++;
                    TotalAmount = TotalAmount + NetAmount;

                }
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'><b>Total</b>", string.Empty));
                string TotalAmounts = TotalAmount.ToString("0.#####");// string.Format("{0:n0}", TotalAmount);
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalAmounts));
                Result.Append("</tr>");


                Result.Append("</table>");
                if (TotalAmount > 0)
                {
                    Result.Append(string.Format("<br><p style='text-align: left;'><b>Rs. {0}</b></p>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalAmount))));
                }
                //Result.Append("<div style='text-align:right; font-weight:bold;'><p>Yours Sincerely,</p><p>Dy. Controller(F&A),<br>H.P. Vidhan Sabha.</p></div></div><br> <br></section>");

                Result.Append("</div><br> <br></section></td></tr> </table><br/>");
                // for Biding Sub Voucher with account Details

                decimal TotalSubVoucher = 0;

                // this is for all list

                var AccountList = (List<AccountDetail>)Helper.ExecuteService("Budget", "GetAllAccountDetail", EstablishIds.ToString());

                // this is for SBI Bank List

                var SBIList = AccountList.Where(m => m.BankName == "SBI").ToList(); ;
                Result.Append(" <table style='page-break-after: always; width: 100%;'> <tr> <td style='vertical-align: middle; text-align: left'>");
                Result.Append("<div style='text-align:left;'><h3>DETAILS OF MISC BANK PAYMENTS</h3></div>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>SR. NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>NAME OF BENEFICIARIES</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>A/C NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>IFSC CODE</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>BANK</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>AMOUNT</th></tr>");
                if (AccountList != null && AccountList.Count() > 0)
                {
                    int AccCount = 1;

                    foreach (var item in AccountList)
                    {
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", AccCount));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.AccountNo));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.IFSCCode));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.BankName));
                        //    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.SubVoucherAmount.ToString()));
                        string SAmount = item.VoucherAmount.ToString("0.#####");// string.Format("{0:n0}", item.VoucherAmount);
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                        Result.Append("</tr>");
                        AccCount++;
                        TotalSubVoucher = TotalSubVoucher + item.VoucherAmount;
                    }

                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b></td>"));
                    string TotalSubVoucherAmounts = TotalSubVoucher.ToString("0.#####");// string.Format("{0:n0}", TotalSubVoucher);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", TotalSubVoucherAmounts));
                    Result.Append("</tr></table> ");

                    if (TotalSubVoucher > 0)
                        Result.Append(string.Format("<br/><p  style='text-align: left;'><b>Rs. {0}</b></p><br>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalSubVoucher))));
                    Result.Append(string.Format("<div style='text-align:right; font-weight:bold;'><p>Yours faithfully,<br></p><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div><br/><br/>", AuthorityBy));

                }
                else
                { Result.Append("<tr><td colspan='6'>No Sub Voucher with Account detail found</td></tr>"); }
                Result.Append("</td></tr> </table>");




                //------

                // this is for DETAILS OF SBI BANK PAYMENTS

                Result.Append(" <table  style='width: 100%;'> <tr> <td style='vertical-align: middle; text-align: left'>");
                Result.Append("<div style='text-align:left;'><h3>DETAILS OF SBI BANK PAYMENTS</h3></div>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>SR. NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>NAME OF BENEFICIARIES</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>A/C NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>IFSC CODE</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>BANK</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>AMOUNT</th></tr>");

                // Create Record for all SBI Voucher  Detail

                int CountSBI = 1;
                decimal TotalSubVoucherSBI = 0;
                foreach (var item in SBIList)
                {
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", CountSBI));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.AccountNo));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.IFSCCode));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.BankName));
                    string SAmount = item.VoucherAmount.ToString("0.#####");// string.Format("{0:n0}", item.VoucherAmount);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                    Result.Append("</tr>");
                    CountSBI++;
                    TotalSubVoucherSBI = TotalSubVoucherSBI + item.VoucherAmount;

                }
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b></td>"));
                string TotalSubVoucherAmountsSBI = TotalSubVoucherSBI.ToString("0.#####");// string.Format("{0:n0}", TotalSubVoucherSBI);
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", TotalSubVoucherAmountsSBI));
                Result.Append("</tr>");
                Result.Append("</table>");

                Result.Append("</td></tr> </table><br/>");


                decimal TotalOfOther = TotalSubVoucher - TotalSubVoucherSBI;

                // this is for DETAILS OF SBI BANK PAYMENTS

                Result.Append(" <table style='page-break-after: always; width:100%;'> <tr> <td style='vertical-align: middle; text-align: left'>");
                Result.Append("<div style='text-align:left;'><h3>GROSS DETAILS</h3></div>");
                Result.Append("<table style='width:50%;border: 1px solid black;border-collapse:collapse;'><tr>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>SR. NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Account Type</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");


                // Create Record for shwoing All SBI and Others Bank Totals

                // this is for Other Bank Details
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>1</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>OTHER Banks</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalOfOther.ToString("0.#####")));
                Result.Append("</tr>");

                // this is for SBI Bank Details
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>2</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>SBI</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalSubVoucherSBI.ToString("0.#####")));
                Result.Append("</tr>");

                // this is for Total of SBI & Other Bank Acc
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'> </td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>Total</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", (TotalSubVoucherSBI + TotalOfOther).ToString("0.#####")));
                Result.Append("</tr>");
                Result.Append("</table>");
                Result.Append(string.Format("<div style='text-align:right; font-weight:bold;'><p>Yours faithfully,</p><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div><br/><br/>", AuthorityBy));
                Result.Append("</td></tr> </table><br/>");


                // Notification Letter for Ex-Employee

                Result.Append("<table style='width: 100%;'>  <tr> <td style='vertical-align: middle; text-align: center'>");
                Result.Append("<h2 style='text-align:center;font-weight:bold'>Himachal  Pradesh Vidhan Sabha Secretariat</h2><h2 style='text-align:center;font-weight:bold'>No: 3-40/80-VS</h2><header>");
                Result.Append("<div><table style='width:100%'>");
                // this is from
                Result.Append(" <tr><td style='text-align: left;left;font-weight:bold'>From,</td> <td></td>   </tr>");
                Result.Append("<tr><td></td> <td  style='text-align: left;font-weight:bold'>The Secretary,H.P. Vidhan Sabha.</td></tr> <br><br>");
                // this is for to
                Result.Append(" <tr><td style='text-align: left;left;font-weight:bold'>To,</td> <td></td>   </tr>");
                Result.Append(string.Format("<tr><td></td> <td  style='text-align: left;font-weight:bold'>Shri Kishori Lal, Ex-MLAVill.<br>PanwiP.O. DurahTehsil ,<br>NirmandDistt. Kullu<br><br> Shimla-171004, Dated: {0}.<br><br><br></td></tr> ", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("<tr><td style='text-align: left;left;font-weight:bold'>Subject:<br><br></td><td style='text-align: left;left;font-weight:bold'>Information of  payment through e.banking/RTGS/NEFT.<br><br></td> ");
                Result.Append("<tr></tr> </tr>  <tr><td style='text-align: left;left;font-weight:bold' >Sir/Madam,</td> <td></td> </tr>");
                Result.Append("<tr><  <td colspan=2 style='text-align: left;'>");
                Result.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;");
                Result.Append("It is intimated that the payment against your Medical Reimbursement/TA bill/bills has been deposited in your bank A/C through RTGS/NEFT  as under:<br>");
                Result.Append("</td> </tr>  </table><br>");
                Result.Append("<div>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Sr.NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill Detail</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>DATE</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>TOTAL AMOUNT PAID</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>DATE OF RTGS</th></tr>");
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>1</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>MR/TA</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>Nil</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", TotalAmounts));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>16-10-2014</td>"));
                Result.Append("</tr>");
                Result.Append("</table><br>");
                Result.Append("</div>");


                Result.Append("<div style='text-align:left;padding-left:8px; font-weight:bold;'><p>You are, therefore, requested to check your  account from the bank & stamped receipt for the remittance amount may please be sent to this Secretariat for record at the earliest.</p></div>");
                Result.Append(string.Format("<div style='text-align:right; font-weight:bold;'><p>Yours faithfully,</p><br><p>( P.C. Jaswal )</p><p>({0})<br>H.P. Vidhan Sabha ,<br>DDO Code: 039</p><p>Tel No. 0177-2652716</p></div><br/><br/>", AuthorityBy));
                Result.Append("<div style='border-bottom:2px solid black;'></div>");

                Result.Append("<div style='text-align:center;'><h2><u>RECEIPT</u></h2></div>");
                Result.Append(string.Format("<div style='text-align:left; padding-left:8px; font-weight:bold;'><p>Received a sum of Rs.{0}/- (Rupees {1}) on A/c of Medical Reimbursement/TA through RTGS/ NEFT from the Secretary HP Vidhan Sabha Secretariat, Shimla-4.<br>Dated____________	<br></p></div>", TotalAmounts, NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalAmount))));
                Result.Append("<div style='text-align:right; font-weight:bold;'><p>Revenue Stamp of Rs. One only </p><br> <p>Signature of MLA/Ex-MLA/RETD.</p><p>KISHORI LAL, Ex MLA</p></div><br/><br/>");
                //  Result.Append(string.Format("<br><p style='text-align:right;'>Generated On: <b>{0}</b></p><br><br>", GenerateDate.ToString("dd/MM/yyyy")));

                Result.Append("</td></tr> </table><br/>");

            }
            else
            {
                Result.Append("<p><b>No bill found</b><p>");
            }
            Result.Append("</body>");
            return Result.ToString();
        }




        public static string GetLetterForOther(List<mEstablishBill> MemberList, DateTime GenerateDate, string AuthorityBy)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "RemitterDetail" });
            string[] RemDetail = SiteSettingsSessn.SettingValue.Split('-');

            string EmployeeName = (RemDetail.Count() > 4 ? RemDetail[4] : "");
            string EmpMobile = (RemDetail.Count() > 5 ? RemDetail[5] : "");

            StringBuilder Result = new StringBuilder();
            StringBuilder EstablishIds = new StringBuilder();

            // Crete Html Template for Authority Letter
            Result.Append("<body style='width:700px;margin:auto;'><section><header style='text-align:center;'>");
            if (MemberList != null && MemberList.Count() > 0)
            {
                Result.Append("<table style='width: 100%;'>  <tr> <td style='vertical-align: middle; text-align: center'>");
                Result.Append("<h2 style='text-align:center;font-weight:bold'>Himachal  Pradesh Vidhan Sabha Secretariat</h2><h4 style='text-align:center;font-weight:bold'>No: 3-40/80-VS</h4><header>");
                Result.Append("<div><table style='width:100%'> <tr> <td style='width: 100px;'></td>  </tr> <tr>   <td style='text-align:center;font-weight:bold'></td> </tr>");
                Result.Append(" <tr><td style='text-align: left;left;font-weight:bold'>To</td> <td></td>   </tr>");
                Result.Append(string.Format("<tr><td></td> <td  style='text-align: left;font-weight:bold'>The Manager,<br>State Bank of India,<br>Shimla-171003.<br><br> Shimla-171004,  Dated: {0}.<br></td></tr> <br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("<tr><td style='text-align: left;left;font-weight:bold'>Subject:<br><br></td><td style='text-align: left;left;font-weight:bold'>Authority letter for drawal of Bills through RTGS/NEFT.<br><br></td> ");
                Result.Append("<tr></tr> </tr>  <tr><td style='text-align: left;left;font-weight:bold' >Sir/Madam,</td> <td></td> </tr>");
                Result.Append("<tr><  <td colspan=2 style='text-align: left;'>");
                Result.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;");
                Result.Append(string.Format("I hereby authorise Sh. {0}, Cashier whose Mobile No. {1} of HP Vidhan Sabha Secretariat for drawl of the bills noted below through RTGS/NEFT as per particulars of the beneficiary's recorded in the authority.<br>", EmployeeName, EmpMobile));
                Result.Append("</td> </tr>  </table><br><h3 style='text-align:left'>DETAIL OF BILLS</h3>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr><th style='border:1px solid black;border-collapse:collapse;'>Sr.NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill Detail</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");

                // Create Record for all bill Detail
                Decimal TotalAmount = 0;

                int Count = 1;

                foreach (var item in MemberList)
                {
                    mEstablishBill stateToEdit = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = item.EstablishBillID });
                    if (!stateToEdit.IsValidAuthority)
                    {
                        stateToEdit.IsValidAuthority = true;
                        Helper.ExecuteService("Budget", "UpdateEstablishBill", stateToEdit);
                    }
                    EstablishIds.Append(item.EstablishBillID + ",");
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));
                    mBudget Budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = item.BudgetId });
                    string BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}/{6}", Budget.MajorHead, Budget.SubMajorHead, Budget.MinorHead, Budget.SubHead, Budget.Plan, Budget.VotedCharged, Budget.ObjectCode);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'> <b>{0}</b> of   <b>{1}</b>  <b>{2}</b> </td>", item.BillNumber, item.FinancialYear, BudgetName));


                    DataSet dataSet = new DataSet();
                    var EstablishBillIds = new List<KeyValuePair<string, string>>();
                    EstablishBillIds.Add(new KeyValuePair<string, string>("@EstablishBillId", item.EstablishBillID.ToString()));

                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillIds);

                    string BillGrossAmount = dataSet.Tables[0].Rows[0][0].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][0]);
                    string BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][1]); ;
                    string BillDeduction = dataSet.Tables[0].Rows[0][2].ToString();//string.Format("{0:n0}", dataSet.Tables[0].Rows[0][2]);

                    decimal NetAmount = Convert.ToDecimal(dataSet.Tables[0].Rows[0][1]);


                    string SAmount = NetAmount.ToString("0.#####");// string.Format("{0:n0}", NetAmount);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                    Result.Append("</tr>");
                    Count++;
                    TotalAmount = TotalAmount + NetAmount;

                }
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'><b>Total</b>", string.Empty));
                string TotalAmounts = TotalAmount.ToString("0.#####");// string.Format("{0:n0}", TotalAmount);
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalAmounts));
                Result.Append("</tr>");


                Result.Append("</table>");
                if (TotalAmount > 0)
                {
                    Result.Append(string.Format("<br><p style='text-align: right;'><b>Rs. {0} ONLY</b></p>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalAmount))));
                }
                //Result.Append("<div style='text-align:right; font-weight:bold;'><p>Yours Sincerely,</p><p>Dy. Controller(F&A),<br>H.P. Vidhan Sabha.</p></div></div><br> <br></section>");

                Result.Append("</div><br> <br></section></td></tr> </table><br/>");
                // for Biding Sub Voucher with account Details

                decimal TotalSubVoucher = 0;

                // this is for all list

                var AccountList = (List<AccountDetail>)Helper.ExecuteService("Budget", "GetAllAccountDetail", EstablishIds.ToString());

                // this is for SBI Bank List

                var SBIList = AccountList.Where(m => m.BankName == "SBI").ToList();

                Result.Append(" <table style='page-break-after: always; width: 100%;'> <tr> <td style='vertical-align: middle; text-align: left'>");
                Result.Append("<div style='text-align:left;'><h3>DETAILS OF MISC BANK PAYMENTS</h3></div>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>SR. NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>NAME OF BENEFICIARIES</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>A/C NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>IFSC CODE</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>BANK</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>AMOUNT</th></tr>");
                if (AccountList != null && AccountList.Count() > 0)
                {
                    int AccCount = 1;

                    foreach (var item in AccountList)
                    {
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", AccCount));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.AccountNo));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.IFSCCode));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.BankName));
                        //    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.SubVoucherAmount.ToString()));
                        string SAmount = item.VoucherAmount.ToString("0.#####");// string.Format("{0:n0}", item.VoucherAmount);
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                        Result.Append("</tr>");
                        AccCount++;
                        TotalSubVoucher = TotalSubVoucher + item.VoucherAmount;
                    }

                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b></td>"));
                    string TotalSubVoucherAmounts = TotalSubVoucher.ToString("0.#####");// string.Format("{0:n0}", TotalSubVoucher);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", TotalSubVoucherAmounts));
                    Result.Append("</tr></table> ");

                    if (TotalSubVoucher > 0)
                        Result.Append(string.Format("<br/><p  style='text-align: left;'><b>Rs. {0}</b></p><br>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalSubVoucher))));
                    Result.Append(string.Format("<div style='text-align:right; font-weight:bold;'><p>Yours faithfully,</p><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div><br/><br/>", AuthorityBy));

                }
                else
                { Result.Append("<tr><td colspan='6'>No Sub Voucher with Account detail found</td></tr>"); }
                Result.Append("</td></tr> </table>");




                //------

                // this is for DETAILS OF SBI BANK PAYMENTS

                Result.Append(" <table  style='width: 100%;'> <tr> <td style='vertical-align: middle; text-align: left'>");
                Result.Append("<div style='text-align:left;'><h3>DETAILS OF SBI BANK PAYMENTS</h3></div>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>SR. NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>NAME OF BENEFICIARIES</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>A/C NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>IFSC CODE</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>BANK</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>AMOUNT</th></tr>");

                // Create Record for all SBI Voucher  Detail

                int CountSBI = 1;
                decimal TotalSubVoucherSBI = 0;
                foreach (var item in SBIList)
                {
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", CountSBI));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.AccountNo));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.IFSCCode));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.BankName));
                    string SAmount = item.VoucherAmount.ToString("0.#####");// string.Format("{0:n0}", item.VoucherAmount);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                    Result.Append("</tr>");
                    CountSBI++;
                    TotalSubVoucherSBI = TotalSubVoucherSBI + item.VoucherAmount;

                }
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b></td>"));
                string TotalSubVoucherAmountsSBI = TotalSubVoucherSBI.ToString("0.#####");// string.Format("{0:n0}", TotalSubVoucherSBI);
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", TotalSubVoucherAmountsSBI));
                Result.Append("</tr>");
                Result.Append("</table>");

                Result.Append("</td></tr> </table><br/>");


                decimal TotalOfOther = TotalSubVoucher - TotalSubVoucherSBI;

                // this is for DETAILS OF SBI BANK PAYMENTS

                Result.Append(" <table style=' width:100%;'> <tr> <td style='vertical-align: middle; text-align: left'>");
                Result.Append("<div style='text-align:left;'><h3>GROSS DETAILS</h3></div>");
                Result.Append("<table style='width:50%;border: 1px solid black;border-collapse:collapse;'><tr>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>SR. NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Account Type</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");


                // Create Record for shwoing All SBI and Others Bank Totals

                // this is for Other Bank Details
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>1</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>OTHER Banks</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalOfOther.ToString("0.#####")));
                Result.Append("</tr>");

                // this is for SBI Bank Details
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>2</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>SBI</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalSubVoucherSBI.ToString("0.#####")));
                Result.Append("</tr>");

                // this is for Total of SBI & Other Bank Acc
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'> </td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>Total</td>"));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", (TotalSubVoucherSBI + TotalOfOther).ToString("0.#####")));
                Result.Append("</tr>");
                Result.Append("</table>");
                Result.Append(string.Format("<div style='text-align:right; font-weight:bold;'><p>Yours faithfully,</p><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div><br/><br/>", AuthorityBy));
                //  Result.Append(string.Format("<br><p style='text-align:right;'>Generated On: <b>{0}</b></p><br><br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("</td></tr> </table><br/>");
                Result.Append("</td></tr> </table><br/>");


            }
            else
            {
                Result.Append("<p><b>No bill found</b><p>");
            }
            Result.Append("</body>");
            return Result.ToString();
        }


        public static string GetLetterForOtherDoc(List<mEstablishBill> List, DateTime GenerateDate, string AuthorityBy)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "RemitterDetail" });
            string[] RemDetail = SiteSettingsSessn.SettingValue.Split('-');
            string EmpEmail = (RemDetail.Count() > 3 ? RemDetail[3] : "");
            string EmployeeName = (RemDetail.Count() > 4 ? RemDetail[4] : "");

            string EmpMobile = (RemDetail.Count() > 5 ? RemDetail[5] : "");

            StringBuilder Result = new StringBuilder();
            StringBuilder EstablishIds = new StringBuilder();

            // Crete Html Template for Authority Letter
            Result.Append("<body style='width:600px;margin:auto; text-align:center;'><section><header style='text-align:center;'>");
            if (List != null && List.Count() > 0)
            {
                Result.Append("<table style='width: 100%;margin-right:10%;'>  <tr> <td style='vertical-align: middle; text-align: center'>");
                Result.Append("<h2 style='text-align:center;font-weight:bold'>Himachal  Pradesh Vidhan Sabha Secretariat</h2><h4 style='text-align:center;font-weight:bold'>No: 3-40/80-VS</h4><header>");
                Result.Append("<div><table style='width:100%'> <tr> <td style='width: 100px;'></td>  </tr> <tr>   <td style='text-align:center;font-weight:bold'></td> </tr>");
                Result.Append(" <tr><td style='text-align: left;left;font-weight:bold;width:10%'>To</td> <td style='width:90%'></td>   </tr>");
                Result.Append(string.Format("<tr><td></td> <td  style='text-align: left;font-weight:bold'>The Manager,<br>State Bank of India,<br>Shimla-171003.<br><br> Shimla-171004,  Dated: {0}.<br></td></tr> <br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("<tr><td style='text-align: left;left;font-weight:bold'>Subject:<br><br></td><td style='text-align: left;left;font-weight:bold'>Authority letter for drawal of Bills through RTGS/NEFT.<br><br></td> ");
                Result.Append("<tr><td style='text-align: left;left;font-weight:bold' >Sir/Madam,</td> <td></td> </tr>");
                Result.Append("<tr> <td colspan=2 style='text-align: left;'>");
                Result.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;");
                Result.Append(string.Format("I hereby authorise Sh. {0}, Cashier whose Mobile No. {1} & Email Id :  <b>{2}</b> of HP Vidhan Sabha Secretariat for drawl of the bills noted below through RTGS/NEFT as per particulars of the beneficiary's recorded in the authority.<br>", EmployeeName, EmpMobile, EmpEmail));
                Result.Append("</td> </tr>  </table><br><h3 style='text-align:left'>DETAIL OF BILLS</h3>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr><th style='border:1px solid black;border-collapse:collapse;'>Sr.NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill Detail</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");

                // Create Record for all bill Detail
                Decimal TotalAmount = 0;

                int Count = 1;
                // this is for OtherFirm
                if (List != null && List.Count() > 0)
                {
                    foreach (var item in List)
                    {
                        mEstablishBill stateToEdit = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = item.EstablishBillID });
                        if (!stateToEdit.IsValidAuthority)
                        {
                            stateToEdit.IsValidAuthority = true;
                            Helper.ExecuteService("Budget", "UpdateEstablishBill", stateToEdit);
                        }
                        EstablishIds.Append(item.EstablishBillID + ",");
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));
                        mBudget Budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = item.BudgetId });
                        string BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}/{6}", Budget.MajorHead, Budget.SubMajorHead, Budget.MinorHead, Budget.SubHead, Budget.Plan, Budget.VotedCharged, Budget.ObjectCode);
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'><b>{0}</b> of  <b>{1}</b>  <b>{2}</b> </td>", item.BillNumber, item.FinancialYear, BudgetName));


                        DataSet dataSet = new DataSet();
                        var EstablishBillIds = new List<KeyValuePair<string, string>>();
                        EstablishBillIds.Add(new KeyValuePair<string, string>("@EstablishBillId", item.EstablishBillID.ToString()));

                        dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillIds);

                        string BillGrossAmount = dataSet.Tables[0].Rows[0][0].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][0]);
                        string BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][1]); ;
                        string BillDeduction = dataSet.Tables[0].Rows[0][2].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][2]);

                        decimal NetAmount = Convert.ToDecimal(dataSet.Tables[0].Rows[0][1]);


                        string SAmount = NetAmount.ToString("0.#####");// string.Format("{0:n0}", NetAmount);
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                        Result.Append("</tr>");
                        Count++;
                        TotalAmount = TotalAmount + NetAmount;

                    }
                }

                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'><b>Total</b>", string.Empty));
                string TotalAmounts = TotalAmount.ToString("0.#####"); // string.Format("{0:n0}", TotalAmount);
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalAmounts));
                Result.Append("</tr>");


                Result.Append("</table>");
                if (TotalAmount > 0)
                {
                    Result.Append(string.Format("<br><p style='text-align: left;width:100%;'><b>Rs. {0} ONLY</b></p>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalAmount))));
                }
                //Result.Append("<div style='text-align:right; font-weight:bold;'><p>Yours Sincerely,</p><p>Dy. Controller(F&A),<br>H.P. Vidhan Sabha.</p></div></div><br> <br></section>");

                Result.Append("</div><br> <br></section></td></tr> </table><br/>");
                // for Biding Sub Voucher with account Details

                decimal TotalSubVoucher = 0;

                // this is for all list

                var AccountList = (List<AccountDetail>)Helper.ExecuteService("Budget", "GetAllAccountDetail", EstablishIds.ToString());

                // this is for SBI Bank List

                var SBIList = AccountList.Where(m => m.BankName == "SBI").ToList();
                //  <br  style='mso-special-character:line-break;page-break-before:always'>
                Result.Append(" <table style='page-break-after: always; width: 100%;margin-right:10%;'> <tr> <td style='vertical-align: middle; text-align: left'>");
                Result.Append("<div style='text-align:left;'><h3>DETAILS OF MISC BANK PAYMENTS</h3></div>");
                Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>SR. NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>NAME OF BENEFICIARIES</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>A/C NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>IFSC CODE</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>BANK</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>AMOUNT</th></tr>");
                if (AccountList != null && AccountList.Count() > 0)
                {
                    int AccCount = 1;

                    foreach (var item in AccountList)
                    {
                        if (item.BankName != "SBI")
                        {
                            Result.Append("<tr>");
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", AccCount));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.AccountNo));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.IFSCCode));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.BankName));
                            //    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.SubVoucherAmount.ToString()));
                            string SAmount = item.VoucherAmount.ToString("0.#####");// string.Format("{0:n0}", item.VoucherAmount);
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                            Result.Append("</tr>");
                            AccCount++;
                            TotalSubVoucher = TotalSubVoucher + item.VoucherAmount;
                        }

                    }

                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b></td>"));
                    string TotalSubVoucherAmounts = TotalSubVoucher.ToString("0.#####");// string.Format("{0:n0}", TotalSubVoucher);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", TotalSubVoucherAmounts));
                    Result.Append("</tr></table> ");

                    if (TotalSubVoucher > 0)
                        Result.Append(string.Format("<br/><p  style='text-align: left;'><b>Rs. {0}</b></p><br><br><br>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalSubVoucher))));
                    //Result.Append(string.Format("<div style='text-align:right; font-weight:bold;'><p>Yours faithfully,<br><br></p><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div><br/><br/>", AuthorityBy));

                }
                else
                { Result.Append("<tr><td colspan='6'>No Sub Voucher with Account detail found</td></tr>"); }
                Result.Append("</td></tr> </table>");




                //------
                if (SBIList != null && SBIList.Count() > 0)
                {
                    decimal TotalSubVoucherSBI = 0;
                    // this is for DETAILS OF SBI BANK PAYMENTS

                    Result.Append(" <table  style='width: 100%;margin-right:10%;'> <tr> <td style='vertical-align: middle; text-align: left'>");
                    Result.Append("<div style='text-align:left;'><h3>DETAILS OF SBI BANK PAYMENTS</h3></div>");
                    Result.Append("<table style='width:100%;border: 1px solid black;border-collapse:collapse;'><tr>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>SR. NO.</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>NAME OF BENEFICIARIES</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>A/C NO.</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>IFSC CODE</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>BANK</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>AMOUNT</th></tr>");

                    // Create Record for all SBI Voucher  Detail

                    int CountSBI = 1;

                    foreach (var item in SBIList)
                    {
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", CountSBI));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.AccountNo));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.IFSCCode));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.BankName));
                        string SAmount = item.VoucherAmount.ToString("0.#####");// string.Format("{0:n0}", item.VoucherAmount);
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                        Result.Append("</tr>");
                        CountSBI++;
                        TotalSubVoucherSBI = TotalSubVoucherSBI + item.VoucherAmount;

                    }
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b></td>"));
                    string TotalSubVoucherAmountsSBI = TotalSubVoucherSBI.ToString("0.#####");// string.Format("{0:n0}", TotalSubVoucherSBI);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", TotalSubVoucherAmountsSBI));
                    Result.Append("</tr>");
                    Result.Append("</table>");

                    Result.Append("</td></tr> </table><br/>");


                    decimal TotalOfOther = TotalSubVoucher;// -TotalSubVoucherSBI;

                    // this is for DETAILS OF SBI BANK PAYMENTS

                    Result.Append(" <table style=' width: 100%;margin-right:10%;'> <tr> <td style='vertical-align: middle; text-align: left'>");
                    Result.Append("<div style='text-align:left;'><h3>GROSS DETAILS</h3></div>");
                    Result.Append("<table style='width:50%;border: 1px solid black;border-collapse:collapse;'><tr>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>SR. NO.</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Account Type</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");


                    // Create Record for shwoing All SBI and Others Bank Totals

                    // this is for Other Bank Details
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>1</td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>OTHER Banks</td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalOfOther.ToString("0.#####")));
                    Result.Append("</tr>");

                    // this is for SBI Bank Details
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>2</td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>SBI</td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalSubVoucherSBI.ToString("0.#####")));
                    Result.Append("</tr>");

                    // this is for Total of SBI & Other Bank Acc
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'> </td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>Total</td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", (TotalSubVoucherSBI + TotalOfOther).ToString("0.#####")));
                    Result.Append("</tr>");
                    Result.Append("</table>");
                    Result.Append(string.Format("<div style='text-align:right; font-weight:bold;'><p>Yours faithfully,<br><br></p><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div><br/><br/>", AuthorityBy));
                }
                // Result.Append(string.Format("<br><p style='text-align:right;'>Generated On: <b>{0}</b></p><br><br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("</td></tr> </table><br/>");


                Result.Append("<br style='page-break-after: always;' /> <div style='page-break-after: always;'></div>");
                // final break up
                Result.Append(string.Format("<div style='text-align:center; font-weight:bold;'><h4>Break up of the bill/bills of RTGS/NEFT</h3><br>Dated: {0}</div><hr/>", GenerateDate.ToString("dd/MM/yyyy")));
                foreach (var Establish in List)
                {
                    Result.Append(string.Format("<div style='text-align:center; font-size:16px; font-weight:bold;'> Bill Number : <b>{0}</b> &nbsp; &nbsp; &nbsp; &nbsp; Amount : <b>{1}</b> </div>", Establish.BillNumber, Establish.TotalGrossAmount.ToString("0.#####")));
                    Result.Append(" <table style='page-break-after: always; width: 100%;margin-right:10%;'>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>SR. NO.</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>NAME OF BENEFICIARIES</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>A/C NO.</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>IFSC CODE</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>BANK</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>AMOUNT</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>UTR No.</th></tr>");

                    if (AccountList != null && AccountList.Count() > 0)
                    {
                        var BreakAccountList = AccountList.Where(m => m.BillNumber == Establish.BillNumber).ToList();
                        int AccCount = 1;

                        foreach (var item in BreakAccountList)
                        {
                            Result.Append("<tr>");
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", AccCount));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.AccountNo));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.IFSCCode));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.BankName));
                            //    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.SubVoucherAmount.ToString()));
                            string SAmount = item.VoucherAmount.ToString("0.#####");// string.Format("{0:n0}", item.VoucherAmount);
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", string.Empty));
                            Result.Append("</tr>");
                            AccCount++;
                        }
                    }
                    Result.Append("</table><br> <br>");
                }
                Result.Append(string.Format("<div style='text-align:right; font-weight:bold;'><p> Total Bill Amount : {0} </p><br/>", TotalAmounts));
                Result.Append(string.Format("<div style='text-align:right; font-weight:bold;'><p>Yours faithfully,<br><br></p><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div><br/><br/>", AuthorityBy));
                Result.Append("</td></tr> </table><br/>");
            }
            else
            {
                Result.Append("<p><b>No bill found</b><p>");
            }
            Result.Append("</body>");
            return Result.ToString();
        }


        public static string GetLetterForStaff(List<mEstablishBill> MemberList, DateTime GenerateDate, string AuthorityBy)
        {

            StringBuilder Result = new StringBuilder();
            StringBuilder EstablishIds = new StringBuilder();


            // Crete Html Template for Authority Letter
            Result.Append("<body style='width:600px;margin:auto;text-align:center;'><section><header style='text-align:center;'>");
            if (MemberList != null && MemberList.Count() > 0)
            {
                Result.Append("<table style='page-break-after: always; width: 100%;  margin-left:10%;'>  <tr> <td style='vertical-align: middle; text-align: center'>");
                Result.Append("<h4 style='text-align:center;font-weight:bold'>No: 3-40/80-VS</h4><h4 style='text-align:center;font-weight:bold'>Himachal Pradesh Vidhan Sabha Secretariat</h4><header>");
                Result.Append("<div><table style='width:90%'> <tr> <td style='width: 100px;'></td>  </tr> <tr>   <td style='text-align:center;font-weight:bold'></td> </tr>");
                Result.Append(" <tr><td style='text-align: left;left;font-weight:bold;width:10%'>To</td> <td style='width:90%'></td>   </tr>");
                Result.Append(string.Format("<tr><td></td> <td  style='text-align: left;font-weight:bold'>The Manager,<br>State Bank of India,<br>Shimla-171003.<br><br> Shimla-171004,Dated: {0}.<br></td></tr> <br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("<tr><td style='text-align: left;left;font-weight:bold'>Subject:<br><br></td><td style='text-align: left;left;font-weight:bold'>Authority Letter for drawal of Bills of Gazetted/Non-Gazetted Staff.<br><br></td> ");
                Result.Append("<tr></tr> </tr>  <tr><td style='text-align: left;left;font-weight:bold' >Sir/Madam,</td> <td></td> </tr>");
                Result.Append("<tr> <td colspan=2 style='text-align: left;'>");
                Result.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;");
                Result.Append("I hereby authorise the Manager, UCO Bank, H.P. Vidhan Sabha Branch Shimla-171004 to encash the below mentioned bills on behalf of this Secretariat :-<br>");
                Result.Append("</td> </tr>  </table><br>");
                Result.Append("<table style='width:90%; border: 1px solid black;border-collapse:collapse;'><tr><th style='border:1px solid black;border-collapse:collapse;'>S.NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill Detail</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");

                // Create Record for all bill Detail
                Decimal TotalAmount = 0;

                int Count = 1;

                foreach (var item in MemberList)
                {
                    mEstablishBill stateToEdit = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = item.EstablishBillID });
                    if (!stateToEdit.IsValidAuthority)
                    {
                        stateToEdit.IsValidAuthority = true;
                        Helper.ExecuteService("Budget", "UpdateEstablishBill", stateToEdit);
                    }
                    EstablishIds.Append(item.EstablishBillID + ",");
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));
                    mBudget Budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = item.BudgetId });
                    string BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}/{6}", Budget.MajorHead, Budget.SubMajorHead, Budget.MinorHead, Budget.SubHead, Budget.Plan, Budget.VotedCharged, Budget.ObjectCode);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'> <b>{0}</b> of <b>{1}</b>  <b>{2}</b> </td>", item.BillNumber, item.FinancialYear, BudgetName));

                    DataSet dataSet = new DataSet();
                    var EstablishBillIds = new List<KeyValuePair<string, string>>();
                    EstablishBillIds.Add(new KeyValuePair<string, string>("@EstablishBillId", item.EstablishBillID.ToString()));

                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillIds);

                    string BillGrossAmount = dataSet.Tables[0].Rows[0][0].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][0]);
                    string BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][1]); ;
                    string BillDeduction = dataSet.Tables[0].Rows[0][2].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][2]);

                    decimal NetAmount = Convert.ToDecimal(dataSet.Tables[0].Rows[0][1]);
                    string SAmount = NetAmount.ToString("0.#####");// string.Format("{0:n0}", NetAmount);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                    Result.Append("</tr>");
                    Count++;
                    TotalAmount = TotalAmount + NetAmount;

                }
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b>", string.Empty));
                string TotalAmounts = TotalAmount.ToString("0.#####");// string.Format("{0:n0}", TotalAmount);
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalAmounts));
                Result.Append("</tr>");


                Result.Append("</table>");
                if (TotalAmount > 0)
                {
                    Result.Append(string.Format("<br><p style='text-align: left;width:90%; margin-left:4%'><b>Rs. {0}</b></p>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalAmount))));
                }
                Result.Append(string.Format("<div style='text-align:right; font-weight:bold;width:90%;'><p>Yours Sincerely,<br><br><br></p><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div></div><br> <br></section>", AuthorityBy));

                Result.Append("</td></tr> </table><br/>");


                // for creating copy for UCO bank
                Result.Append("<br style='page-break-before: always'>");
                Result.Append("<table style=' width: 100%; margin-left:10%;'>  <tr> <td style='vertical-align: middle; text-align: center'>");
                Result.Append("<h4 style='text-align:center;font-weight:bold'>No: 3-40/80-VS</h4><h4 style='text-align:center;font-weight:bold'>Himachal Pradesh Vidhan Sabha Secretariat</h4><header>");
                Result.Append("<div><table style='width:90%'> <tr> <td style='width: 100px;'></td>  </tr> <tr>   <td style='text-align:center;font-weight:bold'></td> </tr>");
                Result.Append(" <tr><td style='text-align: left;left;font-weight:bold;width:10%;'>To</td> <td style='width:90%'></td>   </tr>");
                Result.Append(string.Format("<tr><td></td> <td  style='text-align: left;font-weight:bold'>The Manager,<br>UCO Bank,<br>H.P.V.S. Branch, Shimla-4.<br> Shimla-171004,Dated: {0}.<br><br></td></tr> <br><br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("<tr><td style='text-align: left;left;font-weight:bold'>Subject:<br><br></td><td style='text-align: left;left;font-weight:bold'>Authority Letter for drawal of Bills of Gazetted/Non-Gazetted Staff.<br><br></td> ");
                Result.Append("<tr></tr> </tr>  <tr><td style='text-align: left;left;font-weight:bold' >Sir/Madam,</td> <td></td> </tr>");
                Result.Append("<tr>  <td colspan=2 style='text-align: left;'>");
                Result.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;");
                Result.Append("I am directed to say that the total amount of the bills detailed below may please be collected from the State Bank of India and the amount thereof be credited to the Savings Bank Account of the officers whose particulars are given below :-<br>");
                Result.Append("</td> </tr>  </table><br>");
                Result.Append("<table style='width:90%;border: 1px solid black;border-collapse:collapse;'><tr><th style='border:1px solid black;border-collapse:collapse;'>S.NO.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill Detail</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");

                // Create Record for all bill Detail

                int UcoCount = 1;

                foreach (var item in MemberList)
                {
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", UcoCount));
                    mBudget Budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = item.BudgetId });
                    string BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}/{6}", Budget.MajorHead, Budget.SubMajorHead, Budget.MinorHead, Budget.SubHead, Budget.Plan, Budget.VotedCharged, Budget.ObjectCode);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'> <b>{0}</b> of  <b>{1}</b> <b>{2}</b> </td>", item.BillNumber, item.FinancialYear, BudgetName));

                    DataSet dataSet = new DataSet();
                    var EstablishBillIds = new List<KeyValuePair<string, string>>();
                    EstablishBillIds.Add(new KeyValuePair<string, string>("@EstablishBillId", item.EstablishBillID.ToString()));

                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillIds);

                    string BillGrossAmount = dataSet.Tables[0].Rows[0][0].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][0]);
                    string BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][1]); ;
                    string BillDeduction = dataSet.Tables[0].Rows[0][2].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][2]);

                    decimal NetAmount = Convert.ToDecimal(dataSet.Tables[0].Rows[0][1]);
                    string SAmount = NetAmount.ToString("0.#####");// string.Format("{0:n0}", NetAmount);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                    Result.Append("</tr>");
                    UcoCount++;
                }
                Result.Append("<tr>");
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b>", string.Empty));

                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", TotalAmounts));
                Result.Append("</tr>");


                Result.Append("</table>");
                if (TotalAmount > 0)
                {
                    Result.Append(string.Format("<br><p style='text-align: left;width:90%;margin-left:4%;'><b>Rs. {0}</b></p>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalAmount))));
                }
                Result.Append(string.Format("<div style='text-align:right; font-weight:bold;width:90%; '><p>Yours Sincerely,<br><br><br></p><p>{0},<br>H.P. Vidhan Sabha<br>DDO Code: 039</p></div></div><br> <br></section>", AuthorityBy));

                Result.Append("</td></tr> </table><br/>");


                // for Binding Sub Voucher with account Details

                decimal TotalSubVoucher = 0;
                var AccountList = (List<AccountDetail>)Helper.ExecuteService("Budget", "GetAllAccountDetail", EstablishIds.ToString());
                Result.Append("<br style='page-break-before: always'>");
                Result.Append(" <table style=' width: 100%;margin-left:10%;'> <tr> <td style='vertical-align: middle; text-align: center'>");
                Result.Append("<div style='text-align:center;'><h2>Bill Breakup</h2></div>");
                Result.Append("<table style='width:90%;border: 1px solid black;border-collapse:collapse;'><tr>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Sr. No.</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Name of the Official</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Bill No./Object Code Text</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Account Number</th>");
                Result.Append("<th style='border:1px solid black;border-collapse:collapse;'>Amount</th></tr>");
                if (AccountList != null && AccountList.Count() > 0)
                {
                    int AccCount = 1;

                    foreach (var item in AccountList)
                    {

                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", AccCount));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}/{1}</td>", item.BillNumber, item.ObjectCodeText));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.AccountNo));
                        string SAmount = item.VoucherAmount.ToString("0.#####");// string.Format("{0:n0}", item.VoucherAmount);
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", SAmount));
                        Result.Append("</tr>");
                        AccCount++;
                        TotalSubVoucher = TotalSubVoucher + item.VoucherAmount;
                    }

                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'></td>"));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b></td>"));
                    string TotalSubVoucherAmounts = TotalSubVoucher.ToString("0.#####");// string.Format("{0:n0}", TotalSubVoucher);
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", TotalSubVoucherAmounts));
                    Result.Append("</tr></table> ");

                    if (TotalSubVoucher > 0)
                        Result.Append(string.Format("<br/><p  style='text-align: left;width:100%;margin-left:4%'><b>Rs. {0}</b></p><br>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalSubVoucher))));
                    Result.Append(string.Format("<div style='text-align:right; font-weight:bold;width:90%; '><p>Yours Sincerely,<br><br><br></p><p>{0},<br>H.P. Vidhan Sabha <br>DDO Code: 039</p></div><br/><br/>", AuthorityBy));

                }
                else
                { Result.Append("<tr><td colspan='5'>No Sub Voucher with Account detail found</td></tr>"); }
                //   Result.Append(string.Format("<br><br><p style='text-align:right;'>Generated On: <b>{0}</b></p><br><br>", GenerateDate.ToString("dd/MM/yyyy")));
                Result.Append("</td></tr> </table>");
            }
            else
            {
                Result.Append("<p><b>No bill found</b><p>");
            }
            Result.Append("</body>");
            return Result.ToString();
        }


        public PartialViewResult GetBillList()
        {
            AuthorityLetterViewModel model = new AuthorityLetterViewModel();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.IsValidAuthority == false && m.IsEncashment == true && m.IsSanctioned == true).ToList();
            model.BillList = BillList;
            return PartialView("/Areas/AccountsAdmin/Views/AuthorityLetter/_BillDdl.cshtml", model);
        }

        public PartialViewResult PopulateRemitterDetail()
        {
            AuthorityLetterViewModel model = new AuthorityLetterViewModel();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "RemitterDetail" });
            string[] RemDetail = SiteSettingsSessn.SettingValue.Split('-');
            model.RemAcNo = RemDetail[0];
            model.RemName = RemDetail[1];
            model.RemAdd = RemDetail[2];
            model.EmpEmailId = RemDetail[3];
            model.EmpName = (RemDetail.Count() > 4 ? RemDetail[4] : "");
            model.EmpMobile = (RemDetail.Count() > 5 ? RemDetail[5] : "");

            return PartialView("/Areas/AccountsAdmin/Views/AuthorityLetter/_PopulateRemitterDetails.cshtml", model);
        }

        public JsonResult SaveRemitterDetail(AuthorityLetterViewModel model)
        {
            string Result = string.Empty;
            try
            {
                string RemitterDetails = string.Format("{0}-{1}-{2}-{3}-{4}-{5}", model.RemAcNo, model.RemName, model.RemAdd, model.EmpEmailId, model.EmpName, model.EmpMobile);
                mSiteSettingsVS DAObj = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "RemitterDetail" });
                DAObj.SettingValue = RemitterDetails;
                Helper.ExecuteService("User", "UpdateSiteSettings", DAObj);
                Result = "Sucesfully Update";
            }
            catch (Exception ex)
            {
                Result = ex.InnerException.ToString();
                //  throw;
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult DeleteAuthorizeLetter(int Id)
        {
            if (Id > 0)
            {
                string path = string.Empty;
                string FileName = string.Empty;
                string LetterTypeS = string.Empty;

                var Model = (mAuthorityLetter)Helper.ExecuteService("Budget", "GetAuthorityLetterById", new mAuthorityLetter { AuthorityLetterID = Id });
                FileName = Model.FileName;

                try
                {
                    LetterTypeS = ((SBL.DomainModel.Models.Enums.AuthorityLetterType)int.Parse(Model.LetterType)).ToString();
                    string[] EstablishIds = Model.BillIds.Split(',');
                    foreach (var item in EstablishIds)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            mEstablishBill stateToEdit = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = int.Parse(item) });
                            if (stateToEdit.IsValidAuthority)
                            {
                                stateToEdit.IsValidAuthority = false;
                                Helper.ExecuteService("Budget", "UpdateEstablishBill", stateToEdit);
                            }
                        }
                    }

                    string Url = string.Format("~/AuthorityLetter/{0}", LetterTypeS);
                    path = System.IO.Path.Combine(Server.MapPath(Url), FileName);

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }

                    Helper.ExecuteService("Budget", "DeleteAuthorityLetter", Model);
                    ViewBag.Msg = "Sucessfully delete authorize letter";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
                catch (Exception ex)
                {

                    ViewBag.Msg = ex.InnerException.ToString();
                    ViewBag.Class = "alert alert-danger";
                    ViewBag.Notification = "Success";
                }



            }
            else
            {
                ViewBag.Msg = "Authorize Letter can't delete due to some technical problem.";
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }
            var State = (List<mAuthorityLetter>)Helper.ExecuteService("Budget", "GetAllAuthorityLetter", null);
            var modelList = State.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/AuthorityLetter/_AuthorizeLetterList.cshtml", modelList);
        }

        #region RTGS Letter
        public ActionResult DownloadRTGS(string BillIds)
        {
            string url = "/RtgsSoftCopyReport/";

            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }
            mRtgsSoft obj = new mRtgsSoft();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "RemitterDetail" });
            if (SiteSettingsSessn != null)
            {
                string[] RemDetail = SiteSettingsSessn.SettingValue.Split('-');
                obj.RemAcNo = RemDetail[0];
                obj.RemName = RemDetail[1];
                obj.RemAdd = RemDetail[2];
                obj.EmpEmailId = RemDetail[3];
            }
            else
            {
                obj.RemAcNo = string.Empty;
                obj.RemName = string.Empty;
                obj.RemAdd = string.Empty;
                obj.EmpEmailId = string.Empty;
            }
            obj.BillIds = BillIds;

            var RtgsSoft = ((List<mRtgsSoft>)Helper.ExecuteService("Budget", "GetAllRtgsSoftByBillId", obj)).Where(m => m.BankName != "SBI").ToList();

            string Result = GetSearchList(RtgsSoft);

            //  string path = "";
            try
            {

                //Guid FId = Guid.NewGuid();
                //string fileName = FId + "_RtgsSoftCopy.excel";



                //if (!Directory.Exists(directory))
                //{
                //    Directory.CreateDirectory(directory);
                //}

                //path = Path.Combine(Server.MapPath("~" + url), fileName);


                //StreamWriter sw;
                //sw = System.IO.File.CreateText(path);
                //sw.WriteLine(Result);
                //sw.Close();

                //string contentType = "application/octet-stream";
                //FilePathResult pathRes = null;
                //if (System.IO.File.Exists(path))
                //{
                //    pathRes = new FilePathResult(path, contentType);
                //    pathRes.FileDownloadName = "RtgsSoftCopy.excel";

                //}

                //return pathRes;



                Result = HttpUtility.UrlDecode(Result);
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment;filename=RTGS.xls");
                Response.Charset = "";
                Response.ContentType = "application/excel";
                Response.Write(Result);
                Response.Flush();
                Response.End();


            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }


            return new EmptyResult();
        }


        public static string GetSearchList(List<mRtgsSoft> model)
        {

            StringBuilder BillReport = new StringBuilder();
            BillReport.Append("<table  style='width:100%;border: 1px solid ##CCCCCC;border-collapse: collapse;color:black; '   border='1px'>");
            BillReport.Append("<tr><thead><th>IFSC Code</th><th>Transaction Amount</th><th>Commission Amount</th><th>Remitter's Account Number</th><th>Remitter's Name</th>");
            BillReport.Append("<th>Remitter's Address</th><th>Beneficiary A/C No.</th><th>Benificiary Name</th><th>Beneficiary Address</th><th>Payment Details</th>");
            BillReport.Append("<th>Sender to Receiver Information</th><th>Email ID</th></thead></tr>");




            if (model != null && model.Count() > 0)
            {

                int count = 1;
                foreach (var obj in model)
                {
                    BillReport.Append("<tr>");
                    BillReport.Append(string.Format("<td>{0}</td>", obj.IFSCCode));
                    BillReport.Append(string.Format("<td>{0}</td>", obj.TransferAmt));
                    BillReport.Append(string.Format("<td>{0}</td>", "0.00"));
                    BillReport.Append(string.Format("<td>{0}</td>", obj.RemAcNo));
                    BillReport.Append(string.Format("<td>{0}</td>", obj.RemName));
                    BillReport.Append(string.Format("<td>{0}</td>", obj.RemAdd));
                    BillReport.Append(string.Format("<td>{0}</td>", obj.FirmAcNo));
                    BillReport.Append(string.Format("<td>{0}</td>", obj.FirmName));
                    //  BillReport.Append(string.Format("<td>{0}</td>", obj.FirmAdd));
                    BillReport.Append(string.Format("<td>{0}</td>", "Shimla"));
                    BillReport.Append(string.Format("<td>{0}</td>", "MR"));
                    BillReport.Append(string.Format("<td>{0}</td>", count));
                    BillReport.Append(string.Format("<td>{0}</td>", obj.EmpEmailId));
                    count++;
                    BillReport.Append("</tr>");
                }
            }
            else
            {
                BillReport.Append("<tr><td colspan='12'>No records available</td></tr>");
            }
            BillReport.Append("</table>");
            return BillReport.ToString();
        }
        public static string GetString(decimal TransferAmt, string AccNo, string BankName)
        {

            int Amount = Decimal.ToInt32(TransferAmt);
            int length = 14 - Amount.ToString().Length;
            StringBuilder AmtSB = new StringBuilder();
            AmtSB.Append("R41");
            for (int i = 0; i < length; i++)
            {
                AmtSB.Append("0");
            }
            AmtSB.Append(Amount);
            for (int i = 0; i < 37 - AccNo.Length; i++)
            {
                AmtSB.Append("0");
            }
            AmtSB.Append(AccNo);
            AmtSB.Append(BankName);
            AmtSB.Append("\t");
            return AmtSB.ToString();

        }
        #endregion

        #region Confirmation Letter

        public ActionResult DownloadConfirmationLetter(string BillIds)
        {
            string url = "/ConfirmationLetter/";

            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }
            mRtgsSoft obj = new mRtgsSoft();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "RemitterDetail" });
            if (SiteSettingsSessn != null)
            {
                string[] RemDetail = SiteSettingsSessn.SettingValue.Split('-');
                obj.RemAcNo = RemDetail[0];
                obj.RemName = RemDetail[1];
                obj.RemAdd = RemDetail[2];
                obj.EmpEmailId = RemDetail[3];
            }
            else
            {
                obj.RemAcNo = string.Empty;
                obj.RemName = string.Empty;
                obj.RemAdd = string.Empty;
                obj.EmpEmailId = string.Empty;
            }
            obj.BillIds = BillIds;

            var RtgsSoft = (List<mRtgsSoft>)Helper.ExecuteService("Budget", "GetAllRtgsSoftByBillId", obj);

            string Result = GetConfirmationLetterHTML(RtgsSoft);

            string path = "";
            try
            {

                Guid FId = Guid.NewGuid();
                string fileName = FId + "_ConfimationLetter.doc";



                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                path = Path.Combine(Server.MapPath("~" + url), fileName);


                StreamWriter sw;
                sw = System.IO.File.CreateText(path);
                sw.WriteLine(Result);
                sw.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "ConfimationLetter.doc";

                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }


#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }

        public static string GetConfirmationLetterHTML(List<mRtgsSoft> RtgsSoftList)
        {

            StringBuilder Result = new StringBuilder();
            StringBuilder EstablishIds = new StringBuilder();

            // Crete Html Template for Authority Letter
            Result.Append("<body style='font-family:verdana;font-size:14px;margin-top:0px;padding-top:0px;'>");
            if (RtgsSoftList != null && RtgsSoftList.Count() > 0)
            {
                foreach (var item in RtgsSoftList)
                {

                    mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = item.EstablishId });
                    Result.Append("<table style=' width: 100%;  margin-left:10%;'><tr><td>");

                    Result.Append("<center><div style='width:1300px;font-size:16px;'>");
                    Result.Append("<center><p style='font-size:24px;'>HIMACHAL PRADESH VIDHAN SABHA SECRETARIAT</p></center><p style='font-size:24px;'>No. 3.40/80.VS</p>");
                    Result.Append("<table style='width:650px'>");
                    Result.Append("<tr><td colspan=3>From <br><br></td></tr>");
                    Result.Append(string.Format("<tr><td colspan=2 style='width:10%'></td><td> The Secretary,<br> H.P. Vidhan Sabha.<br> </td></tr><tr><td colspan=3>To <br></td></tr>"));
                    Result.Append("<tr><td colspan=2 style='width:10%'></td> ");

                    Result.Append(string.Format("<td> {0}<br> {1}<br>Dated, Shimla- {2}	</td></tr>", item.FirmName, item.FirmAdd, Convert.ToDateTime(EstablishBill.EncashmentDate).ToString("dd/MMM/yyyy")));
                    Result.Append("<tr><td colspan=2><br>Subject <br><br></td><td><br>Information of  payment through e.banking/RTGS/NEFT<br><br></td></tr>");
                    Result.Append("<tr><td colspan=3>Sir, <br></td></tr>");
                    string BudgetType = string.Empty;
                    string BillType = string.Empty;
                    mBudgetType BudgetTypeObj = (mBudgetType)Helper.ExecuteService("Budget", "GetBudgetTypeById", new mBudgetType { BudgetTypeID = EstablishBill.BillType });

                    BudgetType = BillType = BudgetTypeObj.TypeName;
                    Result.Append(string.Format("<tr><td colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It is intimated that the payment against your {0} reimbursement ", BillType));
                    Result.Append("bill / bills has been deposited in your bank A/C through RTGS/NEFT  as under :- <br><br></td></tr></table>");
                    Result.Append("<table border=1 style='border-collapse:collapse;width:100%'>");
                    Result.Append("<tr><th>Sr.No.</th><th>Bill No.</th><th>Date</th><th>Total Amount Paid</th><th>Date of RTGS</th></tr>");
                    if (EstablishBill.Designation == 6)
                    {
                        int count = 1;
                        List<mReimbursementBill> ReimbursementBillList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillByEstablishIdAndClaimantId", new mReimbursementBill { EstablishBillId = item.EstablishId, ClaimantId = item.ClaimantId });
                        foreach (var Bill in ReimbursementBillList)
                        {
                            Result.Append(string.Format("<tr><td style='text-align:center;'>{0}</td><td  style='text-align:center;'>{1}</td><td style='text-align:center'>{2}</td><td  style='text-align:center;'>{3}</td><td  style='text-align:center;'>{4}</td></tr>", count, Bill.BillNumberOfVoucher, Convert.ToDateTime(Bill.BillDateOfVoucher).ToString("dd/MMM/yyyy"), Bill.SactionAmount.ToString("0.#####"), Convert.ToDateTime(EstablishBill.EncashmentDate).ToString("dd/MMM/yyyy")));
                            count++;
                        }
                    }
                    else
                    {
                        Result.Append(string.Format("<tr><td style='text-align:center;'>1</td><td style='text-align:center;'>{0}</td><td style='text-align:center'>NILL</td><td style='text-align:center;'>{1}</td><td style='text-align:center;'>{2}</td></tr>", BudgetType, item.TransferAmt.ToString("0.#####"), Convert.ToDateTime(EstablishBill.EncashmentDate).ToString("dd/MMM/yyyy")));
                    }

                    Result.Append("</table>");
                    Result.Append("<table style='width:100%' cellpadding=5>");
                    Result.Append("<tr><td colspan=4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                    Result.Append("You are, therefore, requested to check your  account from the bank & stamped receipt for the remittance amount");
                    Result.Append(" may please be sent to this Secretariat for record at the earliest.<br><br></td></tr>");
                    mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "AuthorityBy" });
                    Result.Append(string.Format("<tr><td style='text-align:right;' colspan=4>Yours faithfully,<br><br>( P.C. Jaswal )<br>{0}<br>H.P. Vidhan Sabha<br>Tel No. 0177-2652716</td></tr>", SiteSettingsSessn.SettingValue));

                    if (EstablishBill.Designation == 3)
                    {
                        Result.Append("<tr><td colspan=4 style='text-align:center;font-size:18px;border-top:2px solid black;'><u><b> RECEIPT</b></u></td></tr>");

                        Result.Append(string.Format("<tr><td colspan=4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Received a sum of Rs.{0} (Rupees {1} )", item.TransferAmt, NumberToWordsConverter.NumbersToWords(Convert.ToInt64(item.TransferAmt))));
                        Result.Append(string.Format(" on A/c of {0} reimbursement through RTGS/ NEFT from the Secretary HP Vidhan Sabha Secretariat, Shimla-4.</td></tr>", BillType));
                        Result.Append(" <tr><td colspan=4>Dated_______________________ </td></tr>");
                        Result.Append("<tr><td colspan=4 style='width:40%;text-align:right;'>Revenue Stamp of Rs. One only <br><br>Signature of MLA/Ex-MLA/RETD.</td></tr></table>");
                    }
                    Result.Append("</table></div></center>");

                    Result.Append("</td></tr></table>");
                    Result.Append("<br style='page-break-before: always'>");
                }

            }
            else
            {
                Result.Append("<p><b>No bill found</b><p>");
            }
            Result.Append("</body>");
            return Result.ToString();
        }

        #endregion
    }
}
