﻿using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class SactionedBillController : Controller
    {
        //
        // GET: /AccountsAdmin/SactionedBill/


        public PartialViewResult SanctionedIndex(int pageId = 1, int pageSize = 10)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var BillList = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).
                Where(m => m.CreatedBy == CurrentSession.UserName &&
        m.IsCanceled == false && (m.IsChallanGenerate == false || m.IsReject == true)).OrderByDescending(m => m.EstablishBillID).ToList();
            ViewBag.PageSize = pageSize;


            return PartialView("/Areas/AccountsAdmin/Views/SactionedBill/_SanctionedIndex.cshtml", BillList);

        }


        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 10)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var BillList = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).
                Where(m => m.CreatedBy == CurrentSession.UserName &&
        m.IsCanceled == false && (m.IsChallanGenerate == false || m.IsReject == true)).OrderByDescending(m => m.EstablishBillID).ToList();
            ViewBag.PageSize = pageSize;
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/SactionedBill/_SactionesBillList.cshtml", pagedRecord);

        }

        public PartialViewResult PopulateEstablishBill(int Id, string Action, int pageId = 1, int pageSize = 10)
        {

            mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = Id });
            if (EstablishBill.AdvanceBillNo > 0)
                EstablishBill.TakenAmountDateS = EstablishBill.TakenAmountDate.Value.ToString("dd/MM/yyyy");
            EstablishBill.Mode = Action;
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;

            return PartialView("/Areas/AccountsAdmin/Views/SactionedBill/_PopulateSactionedBill.cshtml", EstablishBill);
        }





        [HttpPost]
        public PartialViewResult SaveEstablishBill(mEstablishBill model, int pageId = 1, int pageSize = 10)
        {

            mEstablishBill obj = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = model.EstablishBillID });


            decimal Deduction = model.TotalGrossAmount - model.TotalAmount;
            if (Deduction < model.TotalGrossAmount || Deduction == model.TotalGrossAmount)
            {


                // Edit Establish Bill
                obj.TotalAmount = model.TotalAmount;
                obj.TotalGrossAmount = model.TotalGrossAmount;
                obj.Deduction = obj.TotalGrossAmount - obj.TotalAmount;
                obj.AdvanceBillNo = model.AdvanceBillNo;

                DateTime TakenAmountDate = new DateTime();
                if (!string.IsNullOrEmpty(model.TakenAmountDateS))
                    TakenAmountDate = new DateTime(Convert.ToInt32(model.TakenAmountDateS.Split('/')[2]), Convert.ToInt32(model.TakenAmountDateS.Split('/')[1]), Convert.ToInt32(model.TakenAmountDateS.Split('/')[0]));
                else
                    TakenAmountDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                obj.TakenAmountDate = TakenAmountDate;
                obj.TakenAmount = model.TakenAmount;


                // Save Establish Bill
                obj.ModifiedBy = CurrentSession.UserName;
                Helper.ExecuteService("Budget", "UpdateEstablishBill", obj);


                ViewBag.Msg = "Sucessfully updated  Bill";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            else
            {
                ViewBag.Msg = "Deduction can't be more than Gross Amount";
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }


            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var BillList = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).
                Where(m => m.CreatedBy == CurrentSession.UserName &&
        m.IsCanceled == false && (m.IsChallanGenerate == false || m.IsReject == true)).OrderByDescending(m => m.EstablishBillID).ToList();



            return PartialView("/Areas/AccountsAdmin/Views/SactionedBill/_SactionesBillList.cshtml", BillList);
        }

        public JsonResult GetAdvanceBillDetail(int EstablishBillId, int BudgetId)
        {
            mEstablishBill MainEstablishBill = new mEstablishBill();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillByBillNumber", new mEstablishBill { BillNumber = EstablishBillId, FinancialYear = FinYearValue });
            if (EstablishBill.BudgetId == BudgetId)
                MainEstablishBill = EstablishBill;

            return Json(MainEstablishBill, JsonRequestBehavior.AllowGet);
        }


        #region Attach PDF

        public PartialViewResult AttachPdfIndex(int pageId = 1, int pageSize = 10)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { FinancialYear = FinYearValue,CreatedBy = CurrentSession.UserName })).Where(m => m.IsChallanGenerate == false || m.Status == 1 || m.IsRejected == true).ToList();

            var model = BillList.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/SactionedBill/_AttachPDFIndex.cshtml", model);

        }

        public PartialViewResult PopulateAttachPDF(int Id, string Action, int pageId = 1, int pageSize = 10)
        {
            ReimbursementBillViewModel model = new ReimbursementBillViewModel();


            mReimbursementBill stateToEdit = (mReimbursementBill)Helper.ExecuteService("Budget", "GetReimbursementBillById", new mReimbursementBill { ReimbursementBillID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit.ToViewModel1(Action);

            model.Mode = Action;
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;

            return PartialView("/Areas/AccountsAdmin/Views/SactionedBill/_AttachPDFofBill.cshtml", model);
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult SaveAttachPDF(ReimbursementBillViewModel model, int pageId = 1, int pageSize = 10)
        {
            try
            {
                mReimbursementBill mReimbursementBill = (mReimbursementBill)Helper.ExecuteService("Budget", "GetReimbursementBillById", new mReimbursementBill { ReimbursementBillID = model.ReimbursementBillID });
                string ext = string.Empty;
                string url = "~/ScannedReimbursementBills/MainPdf";
                string directory = Server.MapPath(url);

                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }

                string url1 = "~/ScannedReimbursementBills/Pdf";
                string directory1 = Server.MapPath(url1);

                if (Directory.Exists(directory))
                {
                    string[] savedFileName = Directory.GetFiles(Server.MapPath(url1));
                    if (savedFileName.Length > 0)
                    {
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            Guid FileName = Guid.NewGuid();

                            string name = Path.GetFileName(page);

                            string path = System.IO.Path.Combine(directory, FileName + "_" + name);

                            if (!System.IO.Directory.Exists(directory))
                            {
                                System.IO.Directory.CreateDirectory(directory);
                            }
                            if (!string.IsNullOrEmpty(name))
                            {
                                if (!string.IsNullOrEmpty(mReimbursementBill.PDFUrl))
                                    System.IO.File.Delete(System.IO.Path.Combine(directory, mReimbursementBill.PDFUrl));
                                System.IO.File.Copy(SourceFile, path, true);
                                mReimbursementBill.PDFUrl = FileName + "_" + name;

                            }

                        }
                        ext = Path.GetExtension(mReimbursementBill.PDFUrl);
                        if (ext == ".pdf")
                        {
                            //  mReimbursementBill.Status = SBL.DomainModel.Models.Enums.BillStatus.AttachPDF.GetHashCode();
                            if (Directory.Exists(directory1))
                            {
                                string[] filePaths = Directory.GetFiles(directory1);
                                foreach (string filePath in filePaths)
                                {
                                    System.IO.File.Delete(filePath);
                                }
                            }
                            mReimbursementBill.IsPDFAttached = true;
                            Helper.ExecuteService("Budget", "UpdateReimbursementBill", mReimbursementBill);
                            ViewBag.Msg = "Sucessfully attached pdf ";
                            ViewBag.Class = "alert alert-info";
                            ViewBag.Notification = "Success";
                        }
                        else
                        {
                            ViewBag.Msg = "Only pdf file can attach ";
                            ViewBag.Class = "alert alert-danger";
                            ViewBag.Notification = "Error";
                        }
                    }
                    else
                    {
                        ViewBag.Msg = "Please select pdf file";
                        ViewBag.Class = "alert alert-danger";
                        ViewBag.Notification = "Error";
                    }



                }
            }
            catch (Exception ex)
            {

                ViewBag.Msg = ex.InnerException;
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill {FinancialYear = FinYearValue, CreatedBy = CurrentSession.UserName })).Where(m => m.IsChallanGenerate == false || m.Status == 1 || m.IsRejected == true).ToList();
            var modelList = BillList.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/SactionedBill/_AttachPdfBillList.cshtml", modelList);
        }

        [HttpPost]
        public ContentResult UploadFiles()
        {
            string url = "~/ScannedReimbursementBills/Pdf";
            string directory = Server.MapPath(url);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            var r = new List<ReimbursementBillViewModel>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/ScannedReimbursementBills/Pdf"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new ReimbursementBillViewModel()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,

                });

            }

            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} Kb", r[0].Length) + "\"}", "application/json");
        }

        public JsonResult RemovePDFFiles()
        {
            string url = "~/ScannedReimbursementBills/Pdf";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
