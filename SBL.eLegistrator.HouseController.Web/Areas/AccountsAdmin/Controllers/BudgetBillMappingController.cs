﻿using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class BudgetBillMappingController : Controller
    {
        //
        // GET: /AccountsAdmin/BudgetBillMapping/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;
                var State = (List<BudgetMapWithDesig>)Helper.ExecuteService("Budget", "GetAllBudgetMapWithDesig", FinYearValue);
                ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
                return PartialView("/Areas/AccountsAdmin/Views/BudgetBillMapping/_Index.cshtml", State);

            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Budget Type Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult PopulateBudgetMapWithDesig()
        {
            BudgetMapWithDesig stateToEdit = new BudgetMapWithDesig();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetBudgetByFY", FinYearValue);

            ViewBag.BudgetList = new SelectList(State, "BudgetID", "BudgetName");

            ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);

            ViewBag.DesignationList = new SelectList(Enum.GetValues(typeof(DesignationType)).Cast<DesignationType>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");

            return PartialView("_AddBudgetBill", stateToEdit);
        }

        public JsonResult GetBudgetByFY(string FinacialYear)
        {
            var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetBudgetByFY", FinacialYear);
            return Json(State, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public ActionResult SaveBudgetType(int BudgetId, int DesignationId, string FinancialYear)
        {
            BudgetMapWithDesig model = new BudgetMapWithDesig();
            model.BudgetId = BudgetId;
            model.DesignationId = DesignationId;
            model.FinancialYear = FinancialYear;

            Helper.ExecuteService("Budget", "AddBudgetMappedwithDesignation", model);
            var State = (List<BudgetMapWithDesig>)Helper.ExecuteService("Budget", "GetAllBudgetMapWithDesig", model.FinancialYear);
            ViewBag.Msg = "Sucessfully added Budget Mapping with Desingation";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";
            return PartialView("/Areas/AccountsAdmin/Views/BudgetBillMapping/_BudgetBillList.cshtml", State);
        }

        public PartialViewResult DeleteBudgetMapWithDesig(int Id)
        {
            BudgetMapWithDesig BudgetMap = (BudgetMapWithDesig)Helper.ExecuteService("Budget", "GetBudgetMapWithDesigById", new BudgetMapWithDesig { MappedID = Id });
            string FinancialYear = BudgetMap.FinancialYear;
            Helper.ExecuteService("Budget", "DeleteBudgetMappedwithDesignation", BudgetMap);
            var State = (List<BudgetMapWithDesig>)Helper.ExecuteService("Budget", "GetAllBudgetMapWithDesig", FinancialYear);
            ViewBag.Msg = "Sucessfully deleted Budget Mapping";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";
            return PartialView("/Areas/AccountsAdmin/Views/BudgetBillMapping/_BudgetBillList.cshtml", State);
        }
        public PartialViewResult GetBudgetMapWithDesigByFinancialYear(string FinancialYear)
        {
            var State = (List<BudgetMapWithDesig>)Helper.ExecuteService("Budget", "GetAllBudgetMapWithDesig", FinancialYear);
            
            return PartialView("/Areas/AccountsAdmin/Views/BudgetBillMapping/_BudgetBillList.cshtml", State);
        }

    }
}
