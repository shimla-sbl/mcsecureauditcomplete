﻿using EvoPdf;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.UploadImages;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class BillForMembersController : Controller
    {
        //
        // GET: /AccountsAdmin/BillForMembers/

        public ActionResult Index()
        {
            try
            {
                string url = "/BillReport/";

                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] filePaths = Directory.GetFiles(directory);
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                Session["BillList"] = null;
                int Member = int.Parse(CurrentSession.MemberCode);

                List<mReimbursementBill> State = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForMember", new mReimbursementBill { ClaimantId = Member }));

                var BillTypeList = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBillType", null);
                ViewBag.BillTypeList = new SelectList(BillTypeList, "BillTypeID", "TypeName");

                ViewBag.StatusList = new SelectList(Enum.GetValues(typeof(BillStatus)).Cast<BillStatus>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");

                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();

                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Bill for Members";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult BillResult(string BillType, string Staus, string SubmissionDateFrom, string SubmissionDateTo, int pageId = 1, int pageSize = 10)
        {
            int Member = int.Parse(CurrentSession.MemberCode);
           
            List<mReimbursementBill> BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForMember", new mReimbursementBill { ClaimantId = Member  }));


            if (!string.IsNullOrEmpty(BillType))
            {
                int BillTypeI = 0;
                int.TryParse(BillType, out BillTypeI);
                BillList = BillList.Where(m => m.BillType == BillTypeI).ToList();
            }


            if (!string.IsNullOrEmpty(Staus))
            {
                int StausI = 0;
                int.TryParse(Staus, out StausI);
                BillList = BillList.Where(m => m.Status == StausI).ToList();
            }


            if (!string.IsNullOrEmpty(SubmissionDateFrom) && !string.IsNullOrEmpty(SubmissionDateTo))
            {

                DateTime SubmissionDateFromD = new DateTime();
                DateTime.TryParse(SubmissionDateFrom, out SubmissionDateFromD);

                DateTime SubmissionDateToD = new DateTime();
                DateTime.TryParse(SubmissionDateTo, out SubmissionDateToD);
                BillList = BillList.Where(m => m.DateOfPresentation >= SubmissionDateFromD && m.DateOfPresentation <= SubmissionDateToD).ToList();

            }



            List<ReimbursementBillViewModel> model = new List<ReimbursementBillViewModel>();

            Session["BillList"] = BillList.ToViewModel();
            model = BillList.ToViewModel();
            ViewBag.PageSize = pageSize;
            List<ReimbursementBillViewModel> pagedRecord = new List<ReimbursementBillViewModel>();
            if (pageId == 1)
            {
                pagedRecord = model.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = model.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(model.Count, pageId, pageSize);
            return PartialView("/Areas/AccountsAdmin/Views/BillForMembers/_ReimbursementBillList.cshtml", pagedRecord);
        }


        public ActionResult DownloadPdf(string BillType, string Staus, string SubmissionDateFrom, string SubmissionDateTo)
        {
            int Member = int.Parse(CurrentSession.MemberCode);
            List<mReimbursementBill> BillList = new List<mReimbursementBill>();

            List<ReimbursementBillViewModel> Model = new List<ReimbursementBillViewModel>();
            if (Session["BillList"] != null)
            {
                Model = (List<ReimbursementBillViewModel>)Session["BillList"];
            }
            else
            {
                BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForMember", new mReimbursementBill { ClaimantId = Member })); 
                if (!string.IsNullOrEmpty(BillType))
                {
                    int BillTypeI = 0;
                    int.TryParse(BillType, out BillTypeI);
                    BillList = BillList.Where(m => m.BillType == BillTypeI).ToList();
                }


                if (!string.IsNullOrEmpty(Staus))
                {
                    int StausI = 0;
                    int.TryParse(Staus, out StausI);
                    BillList = BillList.Where(m => m.Status == StausI).ToList();
                }


                if (!string.IsNullOrEmpty(SubmissionDateFrom) && !string.IsNullOrEmpty(SubmissionDateTo))
                {

                    DateTime SubmissionDateFromD = new DateTime();
                    DateTime.TryParse(SubmissionDateFrom, out SubmissionDateFromD);

                    DateTime SubmissionDateToD = new DateTime();
                    DateTime.TryParse(SubmissionDateTo, out SubmissionDateToD);
                    BillList = BillList.Where(m => m.DateOfPresentation >= SubmissionDateFromD && m.DateOfPresentation <= SubmissionDateToD).ToList();

                }
                Model = BillList.ToViewModel();
            }







            string Result = GetSearchList(Model);


            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;

            // set the header height in points
            pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;

            TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan v1.0.0",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement1.TextAlign = HorizontalTextAlign.Left;
            TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement3.TextAlign = HorizontalTextAlign.Right;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(Result);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }

            string path = "";
            try
            {




                Guid FId = Guid.NewGuid();
                string fileName = FId + "_BillList.pdf";

                string url = "/BillReport/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "BillReportList.pdf";

                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                pdfDocument.Close();
                // System.IO.File.Delete(path);
            }


#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }


        public ActionResult DownloadExcel(string BillType, string Staus, string SubmissionDateFrom, string SubmissionDateTo)
        {
            string Result = string.Empty;


            int Member = int.Parse(CurrentSession.MemberCode);
            List<mReimbursementBill> BillList = new List<mReimbursementBill>();

            List<ReimbursementBillViewModel> Model = new List<ReimbursementBillViewModel>();
            if (Session["BillList"] != null)
            {
                Model = (List<ReimbursementBillViewModel>)Session["BillList"];
            }
            else
            {
                BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForMember", new mReimbursementBill { ClaimantId = Member })); 
                if (!string.IsNullOrEmpty(BillType))
                {
                    int BillTypeI = 0;
                    int.TryParse(BillType, out BillTypeI);
                    BillList = BillList.Where(m => m.BillType == BillTypeI).ToList();
                }


                if (!string.IsNullOrEmpty(Staus))
                {
                    int StausI = 0;
                    int.TryParse(Staus, out StausI);
                    BillList = BillList.Where(m => m.Status == StausI).ToList();
                }


                if (!string.IsNullOrEmpty(SubmissionDateFrom) && !string.IsNullOrEmpty(SubmissionDateTo))
                {

                    DateTime SubmissionDateFromD = new DateTime();
                    DateTime.TryParse(SubmissionDateFrom, out SubmissionDateFromD);

                    DateTime SubmissionDateToD = new DateTime();
                    DateTime.TryParse(SubmissionDateTo, out SubmissionDateToD);
                    BillList = BillList.Where(m => m.DateOfPresentation >= SubmissionDateFromD && m.DateOfPresentation <= SubmissionDateToD).ToList();

                }
                Model = BillList.ToViewModel();
            }

            Result = GetSearchList(Model);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=BillList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }

        public static string GetSearchList(List<ReimbursementBillViewModel> model)
        {

            StringBuilder BillReport = new StringBuilder();

            if (model != null && model.Count() > 0)
            {
                BillReport.Append("<div style='text-align:center;'><h2>Report of Bills</h2></div>");
                BillReport.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                BillReport.Append("<thead class='header' ><tr style='background-color: #6fb3e0 ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:80px;text-align:left;'>Sr. No.</th>");
                BillReport.Append("<th style='width:150px;text-align:left; ' >Memorandum No.</th><th style='width:150px;text-align:left;border-left: 1px solid #fff; ' >Memorandum Type</th><th style='width:150px;text-align:left;'>Gross Amount</th>");
                BillReport.Append("<th style='width:150px;text-align:left;' >Submission Date</th><th style='width:150px;text-align:left;'>Advance Taken</th><th style='width:150px;text-align:left;'>Sanction Amount</th><th style='width:150px;text-align:left;'>Sanctioned Date</th>");

                BillReport.Append("<th style='width:150px;text-align:left;' >Memorandum Duration</th><th style='width:150px;text-align:left;'>Encashment Date</th><th style='width:150px;text-align:left;'>Status</th>");

                BillReport.Append("</tr></thead>");


                BillReport.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    BillReport.Append("<tr>");
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", count));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>VS-{0}-{1}</td>", obj.FinancialYear.Split('-')[0], obj.ReimbursementBillID));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.BillTypeName));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.GrossAmount));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>",Convert.ToDateTime( obj.DateOfPresentation).ToString("dd/MMM/yyyy")));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.TakenAmount));
                    if (obj.IsSanctioned)
                    {
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.SactionAmount));
                        if (Convert.ToDateTime(obj.SactionDate).ToString("dd/MMM/yyyy") != "01/Jan/1753")
                            BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.SactionDate).ToString("dd/MMM/yyyy")));

                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}-{1}</td>", Convert.ToDateTime(obj.BillFromDate).ToString("dd/MMM/yyyy"), Convert.ToDateTime(obj.BillToDate).ToString("dd/MMM/yyyy")));

                    }
                    else
                    {
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>--</td>"));
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>--</td>"));

                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>--</td>"));

                    }
                    if (obj.IsSetEncashment || Convert.ToDateTime(obj.EncashmentDate).ToString("dd/MMM/yyyy") != "01/Jan/1753")
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.EncashmentDate).ToString("dd/MMM/yyyy")));
                    else
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>--</td>"));

                    if (obj.Status == 0)
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "Bill in Process"));
                    else if (obj.Status == 1)
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "Sanctioned"));

                    else if (obj.Status == 2)
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "Encashment"));


                    BillReport.Append("</tr>");
                }
                BillReport.Append("</tbody></table></div> ");
            }
            else
            {
                BillReport.Append("No records available");
            }
            return BillReport.ToString();
        }


        public PartialViewResult BillIndex(int pageId = 1, int pageSize = 10)
        {
            try
            {
                string url = "/BillReport/";

                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] filePaths = Directory.GetFiles(directory);
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                Session["BillList"] = null;
                List<ReimbursementBillViewModel> model = new List<ReimbursementBillViewModel>();
                int Member = int.Parse(CurrentSession.MemberCode);

                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;

                var BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForMember", new mReimbursementBill { ClaimantId = Member })).Where(m => m.FinancialYear == FinYearValue).ToList();

                model = BillList.ToViewModel();

                var BillTypeList = (List<mBudgetBillTypes>)Helper.ExecuteService("Budget", "GetAllBillType", null);
                ViewBag.BillTypeList = new SelectList(BillTypeList, "BillTypeID", "TypeName");

                ViewBag.StatusList = new SelectList(Enum.GetValues(typeof(BillStatus)).Cast<BillStatus>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");

                ViewBag.PageSize = pageSize;
                List<ReimbursementBillViewModel> pagedRecord = new List<ReimbursementBillViewModel>();
                if (pageId == 1)
                {
                    pagedRecord = model.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = model.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(model.Count, pageId, pageSize);

                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();

                Session["BillList"] = model;
                return PartialView("/Areas/AccountsAdmin/Views/BillForMembers/_BillIndex.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Bill for Members";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 10)
        {
            List<ReimbursementBillViewModel> model = new List<ReimbursementBillViewModel>();
            if (Session["BillList"] != null)
                model = (List<ReimbursementBillViewModel>)Session["BillList"];
            else
            {
                int Member = int.Parse(CurrentSession.MemberCode);
                var State = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForMember", new mReimbursementBill { ClaimantId = Member }));
                model = State.ToViewModel();
                Session["BillList"] = model;

            }

            ViewBag.PageSize = pageSize;
            List<ReimbursementBillViewModel> pagedRecord = new List<ReimbursementBillViewModel>();
            if (pageId == 1)
            {
                pagedRecord = model.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = model.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(model.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/BillForMembers/_ReimbursementBillList.cshtml", pagedRecord);
        }


    }
}
