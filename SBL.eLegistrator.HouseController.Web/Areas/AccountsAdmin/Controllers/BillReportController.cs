﻿using EvoPdf;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class BillReportController : Controller
    {

        // GET: /AccountsAdmin/BillReport/

        #region Reimbursement Bill Report

        public PartialViewResult GenerateBillReport(int pageId = 1, int pageSize = 10)
        {
            try
            {

                Session["BillListForReport"] = null;
                string url = "/BillReport/";

                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] filePaths = Directory.GetFiles(directory);
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                var BillTypeList = (List<mBudgetBillTypes>)Helper.ExecuteService("Budget", "GetAllBillType", null);
                ViewBag.BillTypeList = new SelectList(BillTypeList, "BillTypeID", "TypeName");


                ViewBag.StatusList = new SelectList(Enum.GetValues(typeof(BillStatus)).Cast<BillStatus>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");

                ViewBag.DesignationList = new SelectList(Enum.GetValues(typeof(DesignationType)).Cast<DesignationType>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");

                var Member = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetMemberListByDesignation", "0");
                ViewBag.MemberList = new SelectList(Member, "MemberCode", "Name");

                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;


                var BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForReport", null)).Where(m => m.FinancialYear == FinYearValue).ToList();

                ViewBag.PageSize = pageSize;
                List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
                if (pageId == 1)
                {
                    pagedRecord = BillList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);
                Session["BillListForReport"] = BillList;

                return PartialView("/Areas/AccountsAdmin/Views/BillReport/_BillReport.cshtml", pagedRecord.ToViewModel());
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }


        public PartialViewResult GetBillReport(string Designation, string ClaimantId, string BillType, string Staus, string SubmissionDateFromS, string SubmissionDateToS, string BillNumber, int pageId = 1, int pageSize = 10)
        {
            List<ReimbursementBillViewModel> Model = new List<ReimbursementBillViewModel>();
            var List = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForReport", null);
            if (!string.IsNullOrEmpty(BillNumber))
            {
                int BillNumberInt = 0;
                int.TryParse(BillNumber, out BillNumberInt);
                List = List.Where(m => m.EstblishBillNumber == BillNumberInt).ToList();
            }
            if (!string.IsNullOrEmpty(ClaimantId) && !string.IsNullOrEmpty(Designation))
            {
                int MemberInt = int.Parse(ClaimantId);
                int DesignationId = int.Parse(Designation);
                List = List.Where(m => m.ClaimantId == MemberInt && m.Designation == DesignationId).ToList();
            }

            if (!string.IsNullOrEmpty(BillType))
            {
                int BillTypeInt = int.Parse(BillType);
                List = List.Where(m => m.BillType == BillTypeInt).ToList();
            }

            if (!string.IsNullOrEmpty(Staus))
            {
                int StausInt = int.Parse(Staus);
                List = List.Where(m => m.Status == StausInt).ToList();
            }
            if (!string.IsNullOrEmpty(SubmissionDateFromS) && !string.IsNullOrEmpty(SubmissionDateToS))
            {
                DateTime SubmissionDateFrom = new DateTime();
                DateTime.TryParse(SubmissionDateFromS, out SubmissionDateFrom);

                DateTime SubmissionDateTo = new DateTime();
                DateTime.TryParse(SubmissionDateToS, out SubmissionDateTo);

                List = List.Where(m => m.DateOfPresentation >= SubmissionDateFrom && m.DateOfPresentation <= SubmissionDateTo).ToList();
            }


            ViewBag.PageSize = pageSize;
            List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
            if (pageId == 1)
            {
                pagedRecord = List.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = List.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(List.Count, pageId, pageSize);

            Session["BillListForReport"] = List;
            Model = pagedRecord.ToViewModel();

            return PartialView("/Areas/AccountsAdmin/Views/BillReport/_BillListForReport.cshtml", Model);
        }

        public ActionResult DownloadPdf(string Member, string BillType, string Staus, string SubmissionDateFromS, string SubmissionDateToS, string NotingReport)
        {
            List<ReimbursementBillViewModel> Model = new List<ReimbursementBillViewModel>();
            if (Session["BillListForReport"] != null)
            {
                var List = (List<mReimbursementBill>)Session["BillListForReport"];
                Model = List.ToViewModel();
            }
            else
            {

                var List = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForReport", null);

                if (!string.IsNullOrEmpty(Member))
                {
                    int MemberInt = int.Parse(Member);
                    List = List.Where(m => m.ClaimantId == MemberInt).ToList();
                }
                if (!string.IsNullOrEmpty(BillType))
                {
                    int BillTypeInt = int.Parse(BillType);
                    List = List.Where(m => m.BillType == BillTypeInt).ToList();
                }

                if (!string.IsNullOrEmpty(Staus))
                {
                    int StausInt = int.Parse(Staus);
                    List = List.Where(m => m.Status == StausInt).ToList();
                }
                if (!string.IsNullOrEmpty(SubmissionDateFromS) && !string.IsNullOrEmpty(SubmissionDateToS))
                {
                    DateTime SubmissionDateFrom = new DateTime();
                    DateTime.TryParse(SubmissionDateFromS, out SubmissionDateFrom);

                    DateTime SubmissionDateTo = new DateTime();
                    DateTime.TryParse(SubmissionDateToS, out SubmissionDateTo);

                    List = List.Where(m => m.DateOfPresentation >= SubmissionDateFrom && m.DateOfPresentation <= SubmissionDateTo).ToList();
                }
                Model = List.ToViewModel();
            }
            string Result = string.Empty;
            if (NotingReport.Equals("1"))
                Result = GetSearchListForNoting(Model);
            else
                Result = GetSearchList(Model);


            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;

            // set the header height in points
            pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;

            TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan v1.0.0",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement1.TextAlign = HorizontalTextAlign.Left;
            TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement3.TextAlign = HorizontalTextAlign.Right;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(Result);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }



            string path = "";
            try
            {


                Guid FId = Guid.NewGuid();
                string fileName = FId + "_BillList.pdf";

                string url = "/BillReport/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "BillReportList.pdf";

                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                pdfDocument.Close();
                // System.IO.File.Delete(path);
            }


#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }

        public static string GetSearchList(List<ReimbursementBillViewModel> model)
        {

            StringBuilder BillReport = new StringBuilder();

            if (model != null && model.Count() > 0)
            {
                BillReport.Append("<div style='text-align:center;'><h2>Report of Memorandum</h2></div>");
                BillReport.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                BillReport.Append("<thead class='header' ><tr style='background-color: #6fb3e0 ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:80px;text-align:left;'>Sr. No.</th>");
                BillReport.Append("<th style='width:150px;text-align:left; ' >Memorandum No.</th><th style='width:150px;text-align:left;border-left: 1px solid #fff; ' >Memorandum Type</th><th style='width:150px;text-align:left;border-left: 1px solid #fff; ' >Claimant Name</th><th style='width:150px;text-align:left;'>Gross Amount</th>");
                BillReport.Append("<th style='width:150px;text-align:left;' >Submission Date</th><th style='width:150px;text-align:left;'>Advance Taken</th><th style='width:150px;text-align:left;'>Sanction Amount</th><th style='width:150px;text-align:left;'>Sanctioned Date</th>");

                BillReport.Append("<th style='width:150px;text-align:left;' >GPF / Meter / Telephone No.</th><th style='width:150px;text-align:left;'>Encashment Date</th><th style='width:150px;text-align:left;'>Status</th>");

                BillReport.Append("</tr></thead>");


                BillReport.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    BillReport.Append("<tr>");
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", count));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ReimbursementBillID));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.BillTypeName));
                    //if (obj.Designation == 6)
                    //{
                    //    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ClaimantName));
                    //}
                    //else if (obj.Designation == 4 || obj.Designation == 5)
                    //{
                    //    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.StaffName));
                    //}
                    //else
                    //{
                    //    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.MemberName));
                    //}
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ClaimantName));

                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.GrossAmount));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.DateOfPresentation).ToString("dd/MMM/yyyy")));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.TakenAmount));
                    if (obj.IsSanctioned)
                    {
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.SactionAmount));
                        if (Convert.ToDateTime(obj.SactionDate).ToString("dd/MMM/yyyy") != "01/Jan/1753")
                            BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.SactionDate).ToString("dd/MMM/yyyy")));


                        //BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}-{1}</td>", Convert.ToDateTime(obj.BillFromDate).ToString("dd/MMM/yyyy"), Convert.ToDateTime(obj.BillToDate).ToString("dd/MMM/yyyy")));

                    }
                    else
                    {
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>--</td>"));
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>--</td>"));



                    }
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Number));
                    if (obj.IsSetEncashment || Convert.ToDateTime(obj.EncashmentDate).ToString("dd/MMM/yyyy") != "01/Jan/1753")
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.EncashmentDate).ToString("dd/MMM/yyyy")));
                    else
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>--</td>"));

                    if (obj.Status == 0)
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "Bill in Process"));
                    else if (obj.Status == 1)
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "Sanctioned"));

                    else if (obj.Status == 2)
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "Encashment"));


                    BillReport.Append("</tr>");
                }
                BillReport.Append("</tbody></table></div> ");
            }
            else
            {
                BillReport.Append("No records available");
            }
            return BillReport.ToString();
        }


        public static string GetSearchListForNoting(List<ReimbursementBillViewModel> model)
        {

            StringBuilder BillReport = new StringBuilder();

            if (model != null && model.Count() > 0)
            {
                BillReport.Append("<div style='text-align:left;margin-left:65px;'><h3>T.A. Bills of the Hon'ble Members have been prepared as per detail given below:-</h3></div>");
                BillReport.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;margin-left:65px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                BillReport.Append("<thead class='header' ><tr style='background-color: #6fb3e0 ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:80px;text-align:left;'>Sr. No.</th>");
                BillReport.Append("<th style='width:150px;text-align:left;border-left: 1px solid #fff; ' >Name of Member</th>");
                BillReport.Append("<th style='width:150px;text-align:left;' >Period</th><th style='width:150px;text-align:left;'>Name of Com./Session</th>");
                BillReport.Append("<th style='width:150px;text-align:left;'> Amount</th>");
                BillReport.Append("</tr></thead>");
                BillReport.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    BillReport.Append("<tr>");
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", count));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ClaimantName));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}-{1}</td>", Convert.ToDateTime(obj.BillFromDate).ToString("dd/MMM/yyyy"), Convert.ToDateTime(obj.BillToDate).ToString("dd/MMM/yyyy")));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Comette));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.GrossAmount));

                    BillReport.Append("</tr>");
                }
                BillReport.Append("</tbody></table></div> ");
            }
            else
            {
                BillReport.Append("No records available");
            }
            return BillReport.ToString();
        }

        public PartialViewResult GetBillReportByPaging(int pageId = 1, int pageSize = 10)
        {
            List<mReimbursementBill> BillList = new List<mReimbursementBill>();

            if (Session["BillListForReport"] != null)
                BillList = (List<mReimbursementBill>)Session["BillListForReport"];
            else
                BillList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillForReport", null);

            ViewBag.PageSize = pageSize;
            List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/BillReport/_BillListForReport.cshtml", model);
        }

        #endregion

        #region Esatblish Bill Report
        public PartialViewResult BillIndex(int pageId = 1, int pageSize = 10)
        {
            try
            {
                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;
                var BillList = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.IsCanceled == false).OrderByDescending(m => m.EstablishBillID).ToList();

                var BillTypeList = (List<mBudgetBillTypes>)Helper.ExecuteService("Budget", "GetAllBillType", null);
                ViewBag.BillTypeList = new SelectList(BillTypeList, "BillTypeID", "TypeName");
                //  var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", null);

                var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget {FinancialYear=FinYearValue });
                ViewBag.BudgetList = new SelectList(State, "BudgetID", "BudgetName");

                // this is for Paging & Sorting

                List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
                if (pageId == 1)
                {
                    pagedRecord = BillList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.PageSize = pageSize;
                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);




                Pagination pagination = new Pagination();
                pagination.SortCoulmnName = "EstablishBillID";
                pagination.SortOrder = "asc";
                pagination.SortOrderingClass = "sorting";
                pagination.PageId = pageId;
                pagination.PageSize = pageSize;
                pagination.TableId = "tblEstablishBillList";


                EstablishBillViewModel ViewModel = new EstablishBillViewModel();

                ViewModel.EstablishBillLsit = pagedRecord;
                ViewModel.Pagination = pagination;

                return PartialView("/Areas/AccountsAdmin/Views/BillReport/_EstablishBillReport.cshtml", ViewModel);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Bill for Members";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        public PartialViewResult ShowVoucherBillList(int EstablishBillID)
        {

            List<mReimbursementBill> ReimbursementBillList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillByEstablishId", new mReimbursementBill { EstablishBillId = EstablishBillID });
            return PartialView("/Areas/AccountsAdmin/Views/BillReport/_ShowVoucherBillList.cshtml", ReimbursementBillList);
        }
        public JsonResult GetBudgetHeadList(int BillType, int Designation)
        {
            if (BillType == 18 || BillType == 19 || BillType == 20 || BillType == 22)
                BillType = 18;
            //if (Designation == 0 || Designation == 1)
            //    Designation = 1;
            //else if (Designation == 2 || Designation == 3)
            //    Designation = 2;
            //else if (Designation == 4)
            //    Designation = 3;
            //else if (Designation == 5)
            //    Designation = 4;
            //else if (Designation == 6)
            //    Designation = 6;

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            List<mBudget> State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetByBudgetType", new mBudget { BudgetType = BillType, FinancialYear = FinYearValue, BudgetHeadOrder = Designation });
            return Json(State, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult DataTablePagination(Pagination PaginationModel)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var filteredList = (IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBillForReport", new mEstablishBill { FinancialYear=FinYearValue});


            if (PaginationModel.FunctionType == "Sorting")
            {
                if (PaginationModel.SortOrder == "asc")
                {
                    PaginationModel.SortOrder = "desc";
                    PaginationModel.SortOrderingClass = "sorting_asc";
                }
                else
                {
                    PaginationModel.SortOrder = "asc";
                    PaginationModel.SortOrderingClass = "sorting_desc";
                }
            }
            //else if (PaginationModel.FunctionType == "Searching" || PaginationModel.FunctionType == "Paging")
            //{

            //    if (!string.IsNullOrEmpty(PaginationModel.SortOrder) && PaginationModel.SortOrder.Equals("asc"))
            //        PaginationModel.SortOrder = "desc";
            //    else if (!string.IsNullOrEmpty(PaginationModel.SortOrder) && PaginationModel.SortOrder.Equals("desc"))
            //        PaginationModel.SortOrder = "asc";

            //}



            // this is for Searching Data
            if (!string.IsNullOrEmpty(PaginationModel.SearchWord))
            {
                int BillNimber = 0;
                int.TryParse(PaginationModel.SearchWord, out BillNimber);
                filteredList = filteredList.Where(m => m.SanctioNumber.Contains(PaginationModel.SearchWord) || m.BillNumber == BillNimber ||
                    //   m.BudgetName.Contains(PaginationModel.SearchWord) || Convert.ToDateTime( m.SanctionDate).ToString("dd/MM/yyyy").Contains(PaginationModel.SearchWord) || Convert.ToDateTime(m.EncashmentDate).ToString("dd/MM/yyyy").Contains(PaginationModel.SearchWord)
                                         m.TotalAmount.ToString().Contains(PaginationModel.SearchWord));

            }
            // this is for Sorting Data
            if (PaginationModel.SortCoulmnName != null && PaginationModel.SortOrder != "")
            {
                if (PaginationModel.SortOrder == "asc")
                    filteredList = filteredList.OrderBy(c => c.GetType().GetProperty(PaginationModel.SortCoulmnName).GetValue(c, null));
                else
                    filteredList = filteredList.OrderByDescending(c => c.GetType().GetProperty(PaginationModel.SortCoulmnName).GetValue(c, null));

            }


            // this is for Paging
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            if (filteredList != null)
            {
                if (PaginationModel.PageId == 1)
                {
                    pagedRecord = filteredList.Take(PaginationModel.PageSize).ToList();
                }
                else
                {
                    int r = (PaginationModel.PageId - 1) * PaginationModel.PageSize;
                    pagedRecord = filteredList.Skip(r).Take(PaginationModel.PageSize).ToList();
                }
            }

            PaginationModel.TotalRow = filteredList.Count();
            ViewBag.CurrentPage = PaginationModel.PageId;
            ViewData["PagedList"] = Helper.BindPager(PaginationModel.TotalRow, PaginationModel.PageId, PaginationModel.PageSize);

            EstablishBillViewModel ViewModel = new EstablishBillViewModel();
            ViewModel.EstablishBillLsit = pagedRecord;
            ViewModel.Pagination = PaginationModel;
            return PartialView("/Areas/AccountsAdmin/Views/BillReport/_BillList.cshtml", ViewModel);
        }
        public PartialViewResult SerachEstablishBill(string BillType, string BudgetHead, string SanctionDateFrom, string SanctionDateTo, string SanctionNumber, int pageId = 1, int pageSize = 10)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var BillList = (List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBillForReport", new mEstablishBill { FinancialYear = FinYearValue });

            if (!string.IsNullOrEmpty(BillType))
            {
                int BillTypeI = 0;
                int.TryParse(BillType, out BillTypeI);
                BillList = BillList.Where(m => m.BillType == BillTypeI).ToList();
            }


            if (!string.IsNullOrEmpty(BudgetHead))
            {
                int BudgetHeadI = 0;
                int.TryParse(BudgetHead, out BudgetHeadI);
                BillList = BillList.Where(m => m.BudgetId == BudgetHeadI).ToList();
            }


            if (!string.IsNullOrEmpty(SanctionDateFrom) && !string.IsNullOrEmpty(SanctionDateTo))
            {

                DateTime SanctionDateFromD = new DateTime();
                DateTime.TryParse(SanctionDateFrom, out SanctionDateFromD);

                DateTime SanctionDateToD = new DateTime();
                DateTime.TryParse(SanctionDateTo, out SanctionDateToD);
                BillList = BillList.Where(m => m.SanctionDate >= SanctionDateFromD && m.SanctionDate <= SanctionDateToD).ToList();

            }

            if (!string.IsNullOrEmpty(SanctionNumber))
            {
                BillList = BillList.Where(m => m.SanctioNumber == SanctionNumber).ToList();
            }
            Session["EstablishBillList"] = BillList;

            ViewBag.PageSize = pageSize;
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            Pagination pagination = new Pagination();
            pagination.SortCoulmnName = "EstablishBillID";
            pagination.SortOrder = "asc";
            pagination.SortOrderingClass = "sorting";
            pagination.PageId = pageId;
            pagination.PageSize = pageSize;
            pagination.TableId = "tblEstablishBillList";

            EstablishBillViewModel ViewModel = new EstablishBillViewModel();
            ViewModel.EstablishBillLsit = pagedRecord;
            ViewModel.Pagination = pagination;
            return PartialView("/Areas/AccountsAdmin/Views/BillReport/_BillList.cshtml", ViewModel);
        }

        #endregion


    }
}
