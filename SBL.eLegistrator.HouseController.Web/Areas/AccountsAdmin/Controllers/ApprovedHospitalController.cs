﻿using SBL.DomainModel.Models.Budget;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class ApprovedHospitalController : Controller
    {
        //
        // GET: /AccountsAdmin/ApprovedHospital/

        public PartialViewResult ApprovedHospitalIndex(int pageId = 1, int pageSize = 10)
        {
            try
            {

                var ApprovedHospitalList = (List<mApprovedHospital>)Helper.ExecuteService("Budget", "GetAllApprovedHospital", null);

                ViewBag.PageSize = pageSize;
                List<mApprovedHospital> pagedRecord = new List<mApprovedHospital>();
                if (pageId == 1)
                {
                    pagedRecord = ApprovedHospitalList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = ApprovedHospitalList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(ApprovedHospitalList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_ApprovedIndex.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  Approved Hospital Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 10)
        {
            var ApprovedHospitalList = (List<mApprovedHospital>)Helper.ExecuteService("Budget", "GetAllApprovedHospital", null);

            ViewBag.PageSize = pageSize;
            List<mApprovedHospital> pagedRecord = new List<mApprovedHospital>();
            if (pageId == 1)
            {
                pagedRecord = ApprovedHospitalList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ApprovedHospitalList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ApprovedHospitalList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_ApprovedHospitalList.cshtml", pagedRecord);
        }


        public PartialViewResult PopulateApprovedHospital(int Id, string Action, int pageId = 1, int pageSize = 10)
        {
            mApprovedHospital model = new mApprovedHospital();



            var stateToEdit = (mApprovedHospital)Helper.ExecuteService("Budget", "GetApprovedHospitalById", new mApprovedHospital { HospitalID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit;

            model.Mode = Action;
            ViewBag.Add = Action;
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;
            return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_PopulateApprovedHospital.cshtml", model);
        }

        public PartialViewResult DeleteApprovedHospital(int Id, int pageId = 1, int pageSize = 10)
        {

            Helper.ExecuteService("Budget", "DeleteApprovedHospital", new mApprovedHospital { HospitalID = Id });
            ViewBag.Msg = "Sucessfully deleted hospital";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            var ApprovedHospitalList = (List<mApprovedHospital>)Helper.ExecuteService("Budget", "GetAllApprovedHospital", null);

            ViewBag.PageSize = pageSize;
            List<mApprovedHospital> pagedRecord = new List<mApprovedHospital>();
            if (pageId == 1)
            {
                pagedRecord = ApprovedHospitalList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ApprovedHospitalList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ApprovedHospitalList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_ApprovedHospitalList.cshtml", pagedRecord);
        }

        [HttpPost]
        public ActionResult SaveApprovedHospital(FormCollection collection, int pageId = 1, int pageSize = 10)
        {

            try
            {
                string PCRow = collection["ApprovedHospitalNumRow"];
                if (PCRow != null && PCRow.Length > 0)
                {
                    string[] RowCount = PCRow.Split(',');

                    foreach (var val in RowCount)
                    {
                        mApprovedHospital model = new mApprovedHospital();
                        model.HospitalName = collection["HospitalName-" + val];
                        model.IsActive = true;
                        if (!string.IsNullOrEmpty(model.HospitalName))
                        {
                            model.HospitalName = model.HospitalName.Trim();
                            Helper.ExecuteService("Budget", "CreateApprovedHospital", model);
                        }
                        ViewBag.Msg = "Sucessfully added  Hospital name";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }


            var ApprovedHospitalList = (List<mApprovedHospital>)Helper.ExecuteService("Budget", "GetAllApprovedHospital", null);

            ViewBag.PageSize = pageSize;
            List<mApprovedHospital> pagedRecord = new List<mApprovedHospital>();
            if (pageId == 1)
            {
                pagedRecord = ApprovedHospitalList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ApprovedHospitalList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ApprovedHospitalList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_ApprovedHospitalList.cshtml", pagedRecord);
        }
        #region Search ApprovedHospital
        public PartialViewResult SearchApprovedHospital()
        {
            try
            {
                return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_SearchApprovedHospital.cshtml");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  ApprovedHospital Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }



        public ActionResult GetAutoCompleteData(string SearchWord)
        {
            var ApprovedHospitalList = ((List<mApprovedHospital>)Helper.ExecuteService("Budget", "SearchApprovedHospital", SearchWord)).Where(m => m.IsActive == true).ToList();
            return Json(ApprovedHospitalList, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult SearchApprovedHospitalData(string SearchWord, int pageId = 1, int pageSize = 20)
        {
            try
            {

                var ApprovedHospitalList = ((List<mApprovedHospital>)Helper.ExecuteService("Budget", "SearchApprovedHospital", SearchWord)).Where(m => m.IsActive == true).ToList();

                Session["SearchApprovedHospitalList"] = ApprovedHospitalList;
                ViewBag.PageSize = pageSize;
                List<mApprovedHospital> pagedRecord = new List<mApprovedHospital>();
                if (pageId == 1)
                {
                    pagedRecord = ApprovedHospitalList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = ApprovedHospitalList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(ApprovedHospitalList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_SearchApprovedHospitalList.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  Approved Hospital Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetSearchApprovedHospitalByPaging(string SearchWord, int pageId = 1, int pageSize = 10)
        {
            List<mApprovedHospital> ApprovedHospitalList = new List<mApprovedHospital>();
            if (Session["SearchApprovedHospitalList"] != null)
                ApprovedHospitalList = (List<mApprovedHospital>)Session["SearchApprovedHospitalList"];
            else
                ApprovedHospitalList = ((List<mApprovedHospital>)Helper.ExecuteService("Budget", "SearchApprovedHospital", SearchWord)).Where(m => m.IsActive == true).ToList();


            ViewBag.PageSize = pageSize;
            List<mApprovedHospital> pagedRecord = new List<mApprovedHospital>();
            if (pageId == 1)
            {
                pagedRecord = ApprovedHospitalList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ApprovedHospitalList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ApprovedHospitalList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_SearchApprovedHospitalList.cshtml", pagedRecord);
        }
        #endregion


        #region Update ApprovedHospital
        public PartialViewResult UpdateApprovedHospital()
        {
            try
            {
                return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_UpdateApprovedHospital.cshtml");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  ApprovedHospital Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult SearchUpdateApprovedHospitalData(string SearchWord, int pageId = 1, int pageSize = 20)
        {
            try
            {

                var ApprovedHospitalList = (List<mApprovedHospital>)Helper.ExecuteService("Budget", "SearchApprovedHospital", SearchWord);

                Session["SearchApprovedHospitalList"] = ApprovedHospitalList;
                ViewBag.PageSize = pageSize;
                List<mApprovedHospital> pagedRecord = new List<mApprovedHospital>();
                if (pageId == 1)
                {
                    pagedRecord = ApprovedHospitalList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = ApprovedHospitalList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(ApprovedHospitalList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_UpdateApprovedHospitalList.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  ApprovedHospital Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetUpdateApprovedHospitalByPaging(string SearchWord, int pageId = 1, int pageSize = 10)
        {
            List<mApprovedHospital> ApprovedHospitalList = new List<mApprovedHospital>();
            if (Session["UpdateApprovedHospitalList"] != null)
                ApprovedHospitalList = (List<mApprovedHospital>)Session["UpdateApprovedHospitalList"];
            else
                ApprovedHospitalList = (List<mApprovedHospital>)Helper.ExecuteService("Budget", "SearchApprovedHospital", SearchWord);


            ViewBag.PageSize = pageSize;
            List<mApprovedHospital> pagedRecord = new List<mApprovedHospital>();
            if (pageId == 1)
            {
                pagedRecord = ApprovedHospitalList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ApprovedHospitalList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ApprovedHospitalList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_UpdateApprovedHospitalList.cshtml", pagedRecord);
        }

        public ActionResult GetUpdateAutoCompleteData(string SearchWord)
        {
            var ApprovedHospitalList = (List<mApprovedHospital>)Helper.ExecuteService("Budget", "SearchApprovedHospital", SearchWord);
            return Json(ApprovedHospitalList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateApprovedHospital(FormCollection collection, int pageId = 1, int pageSize = 20)
        {

            try
            {
                string PCRow = collection["RowCount"];
                if (PCRow != null && PCRow.Length > 0)
                {
                    string[] RowCount = PCRow.Split(',');

                    foreach (var val in RowCount)
                    {
                        int ApprovedHospitalID = int.Parse(collection["ApprovedHospitalID-" + val]);
                        var stateToEdit = (mApprovedHospital)Helper.ExecuteService("Budget", "GetApprovedHospitalById", new mApprovedHospital { HospitalID = ApprovedHospitalID });
                        string Checked = collection["IsActive-" + val];

                        if (Checked == "on")
                            stateToEdit.IsActive = true;
                        else
                            stateToEdit.IsActive = false;

                        Helper.ExecuteService("Budget", "UpdateApprovedHospital", stateToEdit);
                        ViewBag.Msg = "Sucessfully updated ApprovedHospital";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            string SearchWord = collection["appendedInputButtons"];

            var ApprovedHospitalList = (List<mApprovedHospital>)Helper.ExecuteService("Budget", "SearchApprovedHospital", SearchWord);

            ViewBag.PageSize = pageSize;
            List<mApprovedHospital> pagedRecord = new List<mApprovedHospital>();
            if (pageId == 1)
            {
                pagedRecord = ApprovedHospitalList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = ApprovedHospitalList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(ApprovedHospitalList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/ApprovedHospital/_UpdateApprovedHospitalList.cshtml", pagedRecord);
        }
        #endregion

    }
}
