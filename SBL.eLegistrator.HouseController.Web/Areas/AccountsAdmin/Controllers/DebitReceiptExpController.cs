﻿using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class DebitReceiptExpController : Controller
    {
        //
        // GET: /AccountsAdmin/DebitReceiptExp/

        public ActionResult Index()
        {
            try
            {
                 
               
                var State = (List<mDebitReceiptExp>)Helper.ExecuteService("Budget", "GetAllDebitReceiptExp", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();

                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult PopulateDebitReceiptExp(int Id, string Action)
        {
            DebitReceiptExpViewModel model = new DebitReceiptExpViewModel();


            mDebitReceiptExp stateToEdit = (mDebitReceiptExp)Helper.ExecuteService("Budget", "GetDebitReceiptExpById", new mDebitReceiptExp { DebitReceiptExpID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit.ToViewModel1(Action);

            model.Mode = Action;
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;


            var State = ((List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget { FinancialYear = FinYearValue })).Where(m => m.DisplayInReport == true).ToList();
            model.BudgetList = new SelectList(State, "BudgetID", "BudgetName");
            return PartialView("/Areas/AccountsAdmin/Views/DebitReceiptExp/_AddDebitReceiptExp.cshtml", model);
        }

        public PartialViewResult DeleteDebitReceiptExp(int Id)
        {
            Helper.ExecuteService("Budget", "DeleteDebitReceiptExp", new mDebitReceiptExp { DebitReceiptExpID = Id });
            ViewBag.Msg = "Sucessfully deleted Debit Receipt Expense";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            var State = (List<mDebitReceiptExp>)Helper.ExecuteService("Budget", "GetAllDebitReceiptExp", null);
            var modelList = State.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/DebitReceiptExp/_DebitReceiptExpList.cshtml", modelList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SaveDebitReceiptExp(DebitReceiptExpViewModel model)
        {
             
            if (ModelState.ContainsKey("ReductionAmountTaken"))
            {
                ModelState["ReductionAmountTaken"].Errors.Clear();
            }
            if (ModelState.IsValid)
            {
                var mDebitReceiptExp = model.ToDomainModel();

                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Budget", "CreateDebitReceiptExp", mDebitReceiptExp);
                    ViewBag.Msg = "Sucessfully added Debit Receipt Expense";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
                else
                {
                    Helper.ExecuteService("Budget", "UpdateDebitReceiptExp", mDebitReceiptExp);
                    ViewBag.Msg = "Sucessfully updated Debit Receipt Expense";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
            }
            var State = (List<mDebitReceiptExp>)Helper.ExecuteService("Budget", "GetAllDebitReceiptExp", null);
            var modelList = State.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/DebitReceiptExp/_DebitReceiptExpList.cshtml", modelList);
        }

    }
}
