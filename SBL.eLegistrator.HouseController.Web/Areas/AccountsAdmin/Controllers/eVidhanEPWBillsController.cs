﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.ComplexModel.EPWSanctionBill;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Member;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class eVidhanEPWBillsController : Controller
    {
        //
        // GET: /AccountsAdmin/eVidhanEPWBills/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult partialIndex()
        {
            return PartialView("_BillsContainer");
        }
        public ActionResult partialSanctionIndex()
        {
            return PartialView("_SanctionBill");
        }
        public ActionResult SearchCriteriaForBill(string membType)
        {
            EVidhanEPWBill EVidhanEPWBillObj = new EVidhanEPWBill();

            if (membType == "MLA")
            {
                EVidhanEPWBillObj.EPWList = (List<EVidhanEPWBill>)Helper.ExecuteService("EPWBills", "getAllMLAList", null);
                return PartialView("_SearchAllBillList", EVidhanEPWBillObj);
            }
            else if (membType == "Office")
            {
                var list = (List<EvidhanMasterPhoneBill>)Helper.ExecuteService("EPWBills", "ShowPhoneBillRecordForOfficeOrMembers", null);
                return PartialView("_MasterPhoneBillList", list);
            }
            else if (membType == "Ministers")
            {
                var list = (List<EvidhanMasterPhoneBill>)Helper.ExecuteService("EPWBills", "ShowPhoneBillRecordForCPSnMinsters", null);
                return PartialView("_MasterPhoneBillList", list);
            }
            else
            {
                return Content("error1");
            }
        }

        public ActionResult UpdateBillersIDPage(string BillersID, string membType, string BillType, string BillersName)
        {
            EVidhanEPWBill EVidhanEPWBillObj = new EVidhanEPWBill();

            if (membType == "MLA")
            {
                EVidhanEPWBillObj.BillersID = Convert.ToInt16(BillersID);
                EVidhanEPWBillObj.IsMLA = true;
                EVidhanEPWBillObj.BillType = BillType;
                EVidhanEPWBillObj.BillersName = BillersName;
                var infolist = Helper.ExecuteService("EPWBills", "GetBasicBillerINFO", EVidhanEPWBillObj);
                if (infolist == null)
                {
                    return PartialView("_BillInfoOfIndividual", EVidhanEPWBillObj);
                }
                else
                {
                    return PartialView("_BillInfoOfIndividual", infolist);
                }
            }
            else
            {
                return Content("error1");
            }
        }

        public ActionResult UpdateBillersIDPageForPhone(int id, bool isMinister, string name, string phoneNo, string type )
        {
            try
            {
                EvidhanMasterPhoneBill obj = new EvidhanMasterPhoneBill();
                if (type == "Office")
                {
                    obj.MasterPhoneBillID = id;
                    obj.isMinister = false;
                    obj.NameOfPersonOrDepart = name;
                    obj.TelephoneNumber = phoneNo;

                    var infolist = Helper.ExecuteService("EPWBills", "GetBasicBillerINFOForOfficePhone", obj);
                    if (infolist == null)
                    {
                        return PartialView("_PhoneInfoOfIndividual", obj);
                    }
                    else
                    {
                        return PartialView("_PhoneInfoOfIndividual", infolist);
                    }
                }
                else if (type == "Ministers")
                {
                    obj.MasterPhoneBillID = id;
                    obj.isMinister = true;
                    obj.NameOfPersonOrDepart = name;
                    obj.TelephoneNumber = phoneNo;
                    
                    var infolist = Helper.ExecuteService("EPWBills", "GetBasicBillerINFOForMinistersPhone", obj);
                    if (infolist == null)
                    {
                        return PartialView("_PhoneInfoOfIndividual", obj);
                    }
                    else
                    {
                        return PartialView("_PhoneInfoOfIndividual", infolist);
                    }
                }
                else
                {
                    return Content("error1");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult UpdateBillersIDInfo(EVidhanEPWBill data, EvidhanMasterPhoneBill PhoneBill)
        {
            try
            {
                if (data.BillersName != null)
                {
                    Helper.ExecuteService("EPWBills", "UpdateBillersInfo", data);
                    return Content("Saved");
                }
                else
                {
                    Helper.ExecuteService("EPWBills", "UpdateBillersInfoForPhoneForSomeID", PhoneBill);
                    return Content("Saved");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult UpdateBillersIDPhoneInfo(EvidhanMasterPhoneBill data)
        {
            try
            {
                Helper.ExecuteService("EPWBills", "UpdateBillersInfo", data);
                return Content("Saved");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult NewBillTransaction(int ID, string BillType, bool IsMLA, string BillersName)
        {
            try
            {
                EVidhanEPWBillTransaction EVidhanEPWBillObj = new EVidhanEPWBillTransaction();
                EVidhanEPWBillObj.BillersID = ID;
                EVidhanEPWBillObj.IsMLA = IsMLA;
                EVidhanEPWBillObj.BillType = BillType;
                EVidhanEPWBillObj.BillersName = BillersName;
                return PartialView("_BillInfoOfIndividualForParticularMonth", EVidhanEPWBillObj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult NewBillTransactionOfPhone(int ID, bool IsMinister, string BillersName, string BillType)
        {
            try
            {
                EVidhanEPWBillTransaction EVidhanEPWBillTransaction = new EVidhanEPWBillTransaction();
                EVidhanEPWBillTransaction.BillersID = ID;
                EVidhanEPWBillTransaction.BillersName = BillersName;
                EVidhanEPWBillTransaction.IsMinster = IsMinister;
                EVidhanEPWBillTransaction.IsMLA = false;
                EVidhanEPWBillTransaction.BillType = BillType;
                return PartialView("_NewPhoneBill", EVidhanEPWBillTransaction);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public ActionResult EditBillTransaction(int ID, string BillType, bool IsMLA, int EPWBillTransactionID, bool IsMinster)
        {
            try
            {
                EVidhanEPWBillTransaction EVidhanEPWBillObj = new EVidhanEPWBillTransaction();
                EVidhanEPWBillObj.BillersID = ID;
                EVidhanEPWBillObj.IsMLA = IsMLA;
                EVidhanEPWBillObj.BillType = BillType;
                EVidhanEPWBillObj.EPWBillTransactionID = EPWBillTransactionID;
                EVidhanEPWBillObj.IsMinster = IsMinster;
                var data = Helper.ExecuteService("EPWBills", "EPWBillTransactionOfIdForEdit", EVidhanEPWBillObj);
                ViewBag.edit = "true";
                if (BillType != "Phone")
                {
                    return PartialView("_BillInfoOfIndividualForParticularMonth", data);
                }
                else
                {
                    return PartialView("_NewPhoneBill", data);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult EditBillTransactionOfPhone(int ID, string BillType, bool IsMLA, int EPWBillTransactionID)
        {
            try
            {
                EVidhanEPWBillTransaction EVidhanEPWBillObj = new EVidhanEPWBillTransaction();
                EVidhanEPWBillObj.BillersID = ID;
                EVidhanEPWBillObj.IsMLA = IsMLA;
                EVidhanEPWBillObj.BillType = BillType;
                EVidhanEPWBillObj.EPWBillTransactionID = EPWBillTransactionID;
                var data = Helper.ExecuteService("EPWBills", "EPWBillTransactionOfIdForEdit", EVidhanEPWBillObj);
                ViewBag.edit = "true";
                return PartialView("_NewPhoneBill", data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult SaveNewBillTransaction(EVidhanEPWBillTransaction model, HttpPostedFileBase BillFileName)
        {
            model.SubmitDateDB = DateTime.ParseExact(model.SubmitDate,"dd/MM/yyyy",null);
            if (model.edit == null)
            {
                string fName = "";
                string pathString = "";
                string NFileName = "";
                try
                {
                    if (BillFileName != null)
                    {
                        //Save file content goes here
                        fName = BillFileName.FileName;
                        if (BillFileName != null && BillFileName.ContentLength > 0)
                        {
                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                            // string directory = FileSettings.SettingValue + SavedPdfPath;
                            string url = "epwBills\\" + model.BillType + "\\" + System.DateTime.Now.Year + "\\" + System.DateTime.Now.Month + "\\";
                            pathString = FileSettings.SettingValue + url;

                            var fileName1 = Path.GetFileName(BillFileName.FileName);
                            string ext = Path.GetExtension(BillFileName.FileName);
                            NFileName = Guid.NewGuid().ToString() + ext;

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, NFileName);
                            BillFileName.SaveAs(path);
                            model.BillFileName = url + NFileName;
                        }
                    }
                    Helper.ExecuteService("EPWBills", "SaveNewBillTransaction", model);
                    return Json(model.BillersID, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else {
                string fName = "";
                string pathString = "";
                string NFileName = "";
                try
                {
                    if (BillFileName != null)
                    {
                        //Save file content goes here
                        fName = BillFileName.FileName;
                        if (BillFileName != null && BillFileName.ContentLength > 0)
                        {
                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                            // string directory = FileSettings.SettingValue + SavedPdfPath;
                            string url = "epwBills\\" + model.BillType + "\\" + System.DateTime.Now.Year + "\\" + System.DateTime.Now.Month + "\\";
                            pathString = FileSettings.SettingValue + url;

                            var fileName1 = Path.GetFileName(BillFileName.FileName);
                            string ext = Path.GetExtension(BillFileName.FileName);
                            NFileName = Guid.NewGuid().ToString() + ext;

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, NFileName);
                            BillFileName.SaveAs(path);
                            model.BillFileName = url + NFileName;
                        }
                    }
                    Helper.ExecuteService("EPWBills", "SavingEPWBillTransactionOfIdAfterEdit", model);
                    return Json(model.BillersID, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public ActionResult TransactionHistoryOfParticularIDOfPhone(int BillersID, bool isMinister, string BillType)
        {
            try
            {
                EVidhanEPWBillTransaction obj = new EVidhanEPWBillTransaction();
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                obj.IsMLA = false;
                obj.BillersID = BillersID;
                obj.IsMinster = isMinister;
                obj.BillType = BillType;
                var list = (List<EVidhanEPWBillTransaction>)Helper.ExecuteService("EPWBills", "PhoneBillHIstoryWithBillerID", obj);
                foreach (var item in list)
                {
                    if (!string.IsNullOrEmpty(item.BillFileName))
                    {
                        item.BillFileName = Path.Combine( FileSettings.SettingValue,item.BillFileName);
                    }
                    else
                    {
                        item.BillFileName = string.Empty;
                    }
                }
                return PartialView("_TransactionBillHistory", list);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult TransactionHistoryOfParticularID(string BillersID, string membType, string BillType)
        {
            try
            {
                EVidhanEPWBillTransaction obj = new EVidhanEPWBillTransaction();
                obj.BillersID = Convert.ToInt16(BillersID);
                if (membType == "MLA")
                {
                    obj.IsMLA = true;
                }
                else
                {
                    obj.IsMLA = false;
                }
                obj.BillType = BillType;
                var list = (List<EVidhanEPWBillTransaction>)Helper.ExecuteService("EPWBills", "BillHIstoryWithBillerID", obj);
                return PartialView("_TransactionBillHistory", list);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult AmitiesView()
        {
            try
            {
                return PartialView("_BillDetailsForMember");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult ShowBillDetailsForMember(string BillType, int FinancialYear, int FinancialMonth)
        {
            try
            {
                EVidhanEPWBillTransaction obj1 = new EVidhanEPWBillTransaction();
                obj1.BillType = BillType;
                obj1.FinancialYear = FinancialYear;
                obj1.FinancialMonth = FinancialMonth;
                obj1.BillersID = Convert.ToInt16(CurrentSession.MemberCode);
                var list = (List<EVidhanEPWBillTransaction>)Helper.ExecuteService("EPWBills", "BillHIstoryForMembersWithCriteria", obj1);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult NewPhoneBillEntryPopup(string membType)
        {
            try
            {
                EvidhanMasterPhoneBill obj = new EvidhanMasterPhoneBill();
                if (membType == "Ministers")
                {
                    obj.MemberCol = (List<fillListGenricInt>)Helper.ExecuteService("EPWBills", "GetMemberListForPhoneMaster", null); 
                    obj.isMinister = true;
                }
                else
                {
                    obj.isMinister = false;
                }

                return PartialView("_MasterNewOrUpdatePhoneRecord", obj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult SaveNewMasterRecordForPhoneBill(EvidhanMasterPhoneBill form)
        {
            try
            {
                Helper.ExecuteService("EPWBills", "SaveNewMasterRecordForPhoneBill", form);
                return Content("Saved");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult ShowMasterRecordForPhoneBill()
        {
            try
            {
                Helper.ExecuteService("EPWBills", "ShowMasterRecordForPhoneBill", null);
                return null;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult SearchCriteriaForSanctionBill(string SanctionBillType, string SanctionSubType, string fromDate, string toDate)
        {
            try
            {
                EVidhanEPWBillTransaction obj = new EVidhanEPWBillTransaction();
                obj.BillType = SanctionBillType;
                if (SanctionSubType == "MLA") {
                    obj.IsMLA = true;
                    obj.IsMinster = false;
                }
                if (SanctionSubType == "Office")
                {
                    obj.IsMLA = false;
                    obj.IsMinster = false;
                }
                if (SanctionSubType == "Ministers")
                {
                    obj.IsMLA = false;
                    obj.IsMinster = true;
                }
                obj.SanctionSubmitRangeFrom = DateTime.ParseExact(fromDate, "dd/MM/yyyy",null);

                obj.SanctionSubmitRangeTo = DateTime.ParseExact(toDate , "dd/MM/yyyy", null);

                

                if (SanctionSubType == "MLA" && SanctionBillType == "Electricity")
                {
                    var data = (List<MLAElectricitySanction>)Helper.ExecuteService("EPWBills", "SearchCriteriaForSanctionEBill", obj);
                    return PartialView("_MLAElectricitySanctionBill",data);
                }
                if (SanctionSubType == "MLA" && SanctionBillType == "Phone")
                {
                    var data = (List<MLAPhoneSanction>)Helper.ExecuteService("EPWBills", "SearchCriteriaForSanctionPBill", obj);
                    
                    return PartialView("_MLAPhoneSanctionBill",data);
                }
                if (SanctionSubType == "MLA" && SanctionBillType == "Water")
                {
                    var data = (List<MLAWaterSanction>)Helper.ExecuteService("EPWBills", "SearchCriteriaForSanctionWBill", obj);
                    return PartialView("_MLAWaterSanctionBill", data);
                }
                if (SanctionSubType == "Office" && SanctionBillType == "Phone")
                {                    
                    var data = Helper.ExecuteService("EPWBills", "SearchCriteriaForSanctionOfficeBill", obj);
                    return PartialView("_PDFSanctionBill", data);
                }
                if (SanctionSubType == "Ministers" && SanctionBillType == "Phone")
                {
                    var data = Helper.ExecuteService("EPWBills", "SearchCriteriaForSanctionMinistersBill", obj);
                    return PartialView("_PDFSanctionBill",data);
                }

                return PartialView("_PDFSanctionBill");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult partialSanctionBill()
        {
            return PartialView("_Index");
        }

        public ActionResult DownloadWordFile(string SanctionBillType, string SanctionSubType, string fromDate, string toDate, int OtherTtlSum)
        {
            string Result = string.Empty;


            Result = GetTotalAmount(SanctionBillType, SanctionSubType, fromDate, toDate, OtherTtlSum);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=SanctionPhonebill.doc");
            Response.Charset = "";
            Response.ContentType = "application/doc";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }

        public ActionResult MLAElectricityWOrdDownload(int mlaETtlSum)
        {

            string Result = string.Empty;

            Result = MlaElectricityFile(mlaETtlSum);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=MlaElectricitySanctionBill.doc");
            Response.Charset = "";
            Response.ContentType = "application/doc";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }

        public ActionResult MLAPhoneWOrdDownload(int mlaPTtlSum)
        {

            string Result = string.Empty;

            Result = MlaPhoneFile(mlaPTtlSum);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=MlaPhoneSanctionBill.doc");
            Response.Charset = "";
            Response.ContentType = "application/doc";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }

        public ActionResult MLAWaterWOrdDownload(int mlaWTtlSum)
        {

            string Result = string.Empty;

            Result = MlaWaterFile(mlaWTtlSum);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=MlaPhoneSanctionBill.doc");
            Response.Charset = "";
            Response.ContentType = "application/doc";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }

        public static string GetTotalAmount(string SanctionBillType, string SanctionSubType, string fromDate, string toDate, int OtherTtlSum)
        {
            EVidhanEPWBillTransaction obj = new EVidhanEPWBillTransaction();
            obj.BillType = SanctionBillType;
            if (SanctionSubType == "MLA")
            {
                obj.IsMLA = true;
                obj.IsMinster = false;
            }
            if (SanctionSubType == "Office")
            {
                obj.IsMLA = false;
                obj.IsMinster = false;
            }
            if (SanctionSubType == "Ministers")
            {
                obj.IsMLA = false;
                obj.IsMinster = true;
            }
            obj.SanctionSubmitRangeFrom = DateTime.ParseExact(fromDate, "dd/MM/yyyy", null);
            obj.SanctionSubmitRangeTo = DateTime.ParseExact(toDate, "dd/MM/yyyy", null);

            StringBuilder ReportList = new StringBuilder();
            SanctionTotalAmount ttl = new SanctionTotalAmount();            
            if (SanctionSubType == "Office" && SanctionBillType == "Phone")
            {
                //var data = (SanctionTotalAmount)Helper.ExecuteService("EPWBills", "SearchCriteriaForSanctionOfficeBill", obj);
                //ttl.TotalAmount = data.TotalAmount;                
                ReportList.Append("<body>");
                ReportList.Append("<center><b>Himachal Pradesh Vidhan Sabha Secretariat</b></center>");
                ReportList.Append("<span style='text-align:left'>No.VS-Fin3m(Tel.V.S)16/06</span><span style='text-align:right'>Dated:-" + DateTime.Now.ToString("dd/MM/yyyy") + "</span>");
                ReportList.Append("<br/>");
                ReportList.Append("<center><b>Sanction Order</b></center><p>In exercise of the power vested in me under Rule 19.5 item No.05 of H.P.F.R Vol.-I(1971) read with rule 3 (i) of the Himachal Pradesh Vidhan Sabha Secretariat Regulation 2002,the Administrative and Financial Sanction is hereby accorded for the payment of Rs " + OtherTtlSum + "on account of Telephone Rent/call charges. </p>");
                ReportList.Append("<p>The Expenditure is debitable to major Head 2011 Parliament/State/Union Territory Legislature,02-State/Union Territory Legislature.101-Legislature Secretariat,03-H.P.Vidhan Sabha Members,05-Office Expenses (Voted)(Non-Plan) during the financial year 2015-16.</p>");
                ReportList.Append("<p style='text-align:right'>Sd/-</p><p style='text-align:right'>Secretary,</p><p style='text-align:right'>H.P.Vidhan Sabha.</p><br/><span>No.VS-Fin3m(Tel.V.S.)16/06</span><span style='text-align:right'>Dated:-" + DateTime.Now.ToString("dd/MM/yyyy") + "</span>");
                ReportList.Append("<p>Copy Forwarded for the information and necessary action to:-</p>");
                ReportList.Append("<ol><li>The Principal Accountant General(Audit),Himachal Pradesh,Shimla-3.</li><li>The Accountant General (A&E)Himachal Pradesh,Shimla-3.</li><li>The Treasury Officer,Himachal Pradesh ,Shimla - 171001.</li><li>Accounts Section ,H.P.Vidhan Sabha,Shimla-171004.</li><li>Bill Copy.</li></ol>");
                ReportList.Append("<p style='text-align:right'><b>Deputy Controller(F&A)</b></p>");
                ReportList.Append("<p style='text-align:right'><b>H.P. Vidhan Sabha</b></p>");
                ReportList.Append("</body>");
                return ReportList.ToString();

            }
            if (SanctionSubType == "Ministers" && SanctionBillType == "Phone")
            {
               // var data = (SanctionTotalAmount)Helper.ExecuteService("EPWBills", "SearchCriteriaForSanctionMinistersBill", obj);
               // ttl.TotalAmount = data.TotalAmount;
                ReportList.Append("<body>");
                ReportList.Append("<center><b>Himachal Pradesh Vidhan Sabha Secretariat</b></center>");
                ReportList.Append("<span style='text-align:left'>No.VS-Fin3m(Tel.TR.Min/CPS)59/2013</span><p style='text-align:right'>Dated:-" + DateTime.Now.ToString("dd/MM/yyyy") + "</p>");
                ReportList.Append("<br/>");
                ReportList.Append("<center><b>Sanction Order</b></center><p>In exercise of the power vested in me under Rule 19.5 item No.05 of H.P.F.R Vol.-I(1971) read with rule 3 (i) of the Himachal Pradesh Vidhan Sabha Secretariat Regulation 2002,the Administrative and Financial Sanction is hereby accorded for the payment of Rs " + OtherTtlSum + "on account of Telephone Rent/call charges of Telephones installed in the Office of the Ministers/CPS in Vidhan Sabha,Secretariat Shimla-4 </p>");
                ReportList.Append("<p>The Expenditure is debitable to major Head 2011 Parliament/State/Union Territory Legislature,02-State/Union Territory Legislature.101-Legislature Secretariat,03-H.P.Vidhan Sabha Members,05-Office Expenses (Voted)(Non-Plan) during the financial year 2015-16.</p>");
                ReportList.Append("<p style='text-align:right'>Sd/-</p><p style='text-align:right'>Secretary,</p><p style='text-align:right'>H.P.Vidhan Sabha.</p><br/><span>No.VS-Fin3m(Tel.TR.Min./CPS)59/2013</span><p style='text-align:right'>Dated:-" + DateTime.Now.ToString("dd/MM/yyyy") + "</p>");
                ReportList.Append("<p>Copy Forwarded for the information and necessary action to:-</p>");
                ReportList.Append("<ol><li>The Principal Accountant General(Audit),Himachal Pradesh,Shimla-3.</li><li>The Accountant General (A&E)Himachal Pradesh,Shimla-3.</li><li>The Treasury Officer,Himachal Pradesh ,Shimla - 171001.</li><li>Accounts Section ,H.P.Vidhan Sabha,Shimla-171004.</li><li>Bill Copy.</li></ol>");
                ReportList.Append("<p style='text-align:right'><b>Deputy Controller(F&A)</b></p>");
                ReportList.Append("<p style='text-align:right'><b>H.P. Vidhan Sabha</b></p>");
                ReportList.Append("</body>");
                return ReportList.ToString();
            }
           
            return ReportList.ToString();
        }


        public static string MlaElectricityFile(int mlaETtlSum) {

            StringBuilder ReportList = new StringBuilder();
            ReportList.Append("<body>");
            ReportList.Append("<center><b>Himachal Pradesh Vidhan Sabha Secretariat</b></center>");
            ReportList.Append("<span style='text-align:left'>No.VS-Fin3m(EWR)141/2003</span><p style='text-align:right'>Dated:-" + DateTime.Now.ToString("dd/MM/yyyy") + "</p>");
            ReportList.Append("<br/>");
            ReportList.Append("<center><b>Sanction Order</b></center><p>In exercise of the power vested in me under Rule 19.5 item No.05 of H.P.F.R Vol.-I(1971) read with rule 3 (i) of the Himachal Pradesh Vidhan Sabha Secretariat Regulation 2002,the Administrative and Financial Sanction is hereby accorded for the payment of Rs " + mlaETtlSum + "on account of reimbursement of Electricity charges to MLAs </p>");
            ReportList.Append("<p>The Expenditure is debitable to major Head 2011 Parliament/State/Union Territory Legislature,02-State/Union Territory Legislature.101-Legislature Secretariat,03-H.P.Vidhan Sabha Members,05-Office Expenses (Voted)(Non-Plan) during the financial year 2015-16.</p>");
            ReportList.Append("<p style='text-align:right'>Sd/-</p><p style='text-align:right'>Secretary,</p><p style='text-align:right'>H.P.Vidhan Sabha.</p><br/><span>No.VS-Fin3m(EWR)141/2003</span><p style='text-align:right'>Dated:-" + DateTime.Now.ToString("dd/MM/yyyy") + "</p>");
            ReportList.Append("<p>Copy Forwarded for the information and necessary action to:-</p>");
            ReportList.Append("<ol><li>The Principal Accountant General(Audit),Himachal Pradesh,Shimla-3.</li><li>The Accountant General (A&E)Himachal Pradesh,Shimla-3.</li><li>The Treasury Officer,Himachal Pradesh ,Shimla - 171001.</li><li>S.0.Accounts H.P.Vidhan Sabha,Shimla-171004.</li><li>Bill Copy.</li></ol>");
            ReportList.Append("<p style='text-align:right'><b>Deputy Controller(F&A)</b></p>");
            ReportList.Append("<p style='text-align:right'><b>H.P. Vidhan Sabha</b></p>");
            ReportList.Append("</body>");
            return ReportList.ToString();
        }

        public static string MlaPhoneFile(int mlaPTtlSum)
        {

            StringBuilder ReportList = new StringBuilder();
            ReportList.Append("<body>");
            ReportList.Append("<center><b>Himachal Pradesh Vidhan Sabha Secretariat</b></center>");
            ReportList.Append("<span style='text-align:left'>No.VS-Fin-3m(TR MLA)10/2003</span><p style='text-align:right'>Dated:-" + DateTime.Now.ToString("dd/MM/yyyy") + "</p>");
            ReportList.Append("<br/>");
            ReportList.Append("<center><b>Sanction Order</b></center><p>In exercise of the power vested in me under Rule 19.5 item No.05 of H.P.F.R Vol.-I(1971) read with rule 3 (i) of the Himachal Pradesh Vidhan Sabha Secretariat Regulation 2002,the Administrative and Financial Sanction is hereby accorded for the payment of Rs " + mlaPTtlSum + "on account of reimbursement of Electricity charges to MLAs </p>");
            ReportList.Append("<p>The Expenditure is debitable to major Head 2011 Parliament/State/Union Territory Legislature,02-State/Union Territory Legislature.101-Legislature Secretariat,03-H.P.Vidhan Sabha Members,05-Office Expenses (Voted)(Non-Plan) during the financial year 2015-16.</p>");
            ReportList.Append("<p style='text-align:right'>Sd/-</p><p style='text-align:right'>Secretary,</p><p style='text-align:right'>H.P.Vidhan Sabha.</p><br/><span>No.VS-Fin-3m(TR MLA)10/2003</span><p style='text-align:right'>Dated:-" + DateTime.Now.ToString("dd/MM/yyyy") + "</p>");
            ReportList.Append("<p>Copy Forwarded for the information and necessary action to:-</p>");
            ReportList.Append("<ol><li>The Principal Accountant General(Audit),Himachal Pradesh,Shimla-3.</li><li>The Accountant General (A&E)Himachal Pradesh,Shimla-3.</li><li>The Treasury Officer,Himachal Pradesh ,Shimla - 171001.</li><li>Accounts section ,H.P.Vidhan Sabha,Shimla-171004.</li><li>Bill Copy.</li></ol>");
            ReportList.Append("<p style='text-align:right'><b>Deputy Controller(F&A)</b></p>");
            ReportList.Append("<p style='text-align:right'><b>H.P. Vidhan Sabha</b></p>");
            ReportList.Append("</body>");
            return ReportList.ToString();
        }

        public static string MlaWaterFile(int mlaWTtlSum)
        {
            StringBuilder ReportList = new StringBuilder();
            ReportList.Append("<body>");
            ReportList.Append("<center><b>Himachal Pradesh Vidhan Sabha Secretariat</b></center>");
            ReportList.Append("<span style='text-align:left'>No.VS-Fin-3m(W MLA)10/2003</span><p style='text-align:right'>Dated:-" + DateTime.Now.ToString("dd/MM/yyyy") + "</p>");
            ReportList.Append("<br/>");
            ReportList.Append("<center><b>Sanction Order</b></center><p>In exercise of the power vested in me under Rule 19.5 item No.05 of H.P.F.R Vol.-I(1971) read with rule 3 (i) of the Himachal Pradesh Vidhan Sabha Secretariat Regulation 2002,the Administrative and Financial Sanction is hereby accorded for the payment of Rs " + mlaWTtlSum + "on account of reimbursement of Water charges to MLAs </p>");
            ReportList.Append("<p>The Expenditure is debitable to major Head 2011 Parliament/State/Union Territory Legislature,02-State/Union Territory Legislature.101-Legislature Secretariat,03-H.P.Vidhan Sabha Members,05-Office Expenses (Voted)(Non-Plan) during the financial year 2015-16.</p>");
            ReportList.Append("<p style='text-align:right'>Sd/-</p><p style='text-align:right'>Secretary,</p><p style='text-align:right'>H.P.Vidhan Sabha.</p><br/><span>No.VS-Fin-3m(W MLA)10/2003</span><p style='text-align:right'>Dated:-" + DateTime.Now.ToString("dd/MM/yyyy") + "</p>");
            ReportList.Append("<p>Copy Forwarded for the information and necessary action to:-</p>");
            ReportList.Append("<ol><li>The Principal Accountant General(Audit),Himachal Pradesh,Shimla-3.</li><li>The Accountant General (A&E)Himachal Pradesh,Shimla-3.</li><li>The Treasury Officer,Himachal Pradesh ,Shimla - 171001.</li><li>Accounts section ,H.P.Vidhan Sabha,Shimla-171004.</li><li>Bill Copy.</li></ol>");
            ReportList.Append("<p style='text-align:right'><b>Deputy Controller(F&A)</b></p>");
            ReportList.Append("<p style='text-align:right'><b>H.P. Vidhan Sabha</b></p>");
            ReportList.Append("</body>");
            return ReportList.ToString();
        }

    }
}
