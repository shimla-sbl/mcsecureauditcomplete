﻿using EvoPdf;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.FormTR2;
using SBL.DomainModel.Models.StaffManagement;
using SBL.DomainModel.Models.User;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Enums;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class SaveTR2FormController : Controller
    {
        //
        // GET: /AccountsAdmin/SaveTR2Form/

        public ActionResult Index()
        {
            ViewBag.StaffClass = new SelectList(Enum.GetValues(typeof(StaffClass)).Cast<StaffClass>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            var StaffList = (List<mStaff>)Helper.ExecuteService("staff", "GetAllStaffByClass", 0);
            ViewBag.StaffList = new SelectList(StaffList, "StaffID", "NameWithDesignation");
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            ViewBag.FinancialMonthList = ModelMapping.GetMonthsList(FinYearValue);
            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_Index.cshtml");
        }
        public ActionResult SaveTR2Form()
        {

            ViewBag.StaffClass = new SelectList(Enum.GetValues(typeof(StaffClass)).Cast<StaffClass>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");

            var StaffList = (List<mStaff>)Helper.ExecuteService("staff", "GetAllStaffByClass", 0);
            ViewBag.StaffList = new SelectList(StaffList, "StaffID", "NameWithDesignation");
            HPTR2Model model = new HPTR2Model();
            var AccountFields = (List<mAccountFields>)Helper.ExecuteService("TR2", "GetAllAccountFields", null);
            model.AccountFieldsList = AccountFields;
            model.DUESList = AccountFields.Where(m => m.Type == "DUES").ToList();
            model.AGDList = AccountFields.Where(m => m.Type == "AGD").ToList();
            model.TDList = AccountFields.Where(m => m.Type == "TD").ToList();

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            ViewBag.FinancialMonthList = ModelMapping.GetMonthsList(FinYearValue);

            mSiteSettingsVS DAPERCENTAGE = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "DAPERCENTAGE" });
            ViewBag.DAPERCENTAGE = DAPERCENTAGE.SettingValue;
            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_SaveTR2Form.cshtml", model);
        }
        public JsonResult GetStaffByClass(int Classes)
        {
            var StaffList = (List<mStaff>)Helper.ExecuteService("staff", "GetAllStaffByClass", Classes);
            return Json(StaffList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveHPTR2Form(FormCollection collection)
        {
            string msg = string.Empty;

            try
            {
                string[] RowCount = collection["AccountFieldsId"].Split(',');
                string desingation = collection["Designation"];
                string FinYear = collection["FinancialYear"];
                string Month = collection["Month"];
                string MonthText = collection["MonthText"];
                var EmpList = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetHPTR2Data", new tEmployeeAccountDetails { EmpId = int.Parse(desingation), finanacialYear = FinYear, AccountMonth = int.Parse(Month) });
                if (EmpList != null && EmpList.Count() > 0)
                    msg = "Already available data for this employee ,month and financial year";
                else
                {
                    foreach (var item in RowCount)
                    {
                        tEmployeeAccountDetails obj = new tEmployeeAccountDetails();
                        obj.EmpId = int.Parse(desingation);
                        obj.finanacialYear = FinYear;
                        obj.AccountFieldId = int.Parse(item);

                        decimal AccountFieldsValue = 0;
                        decimal.TryParse(collection["AccountFieldsValue-" + item], out AccountFieldsValue);
                        obj.AccountFieldValue = AccountFieldsValue;
                        obj.AccountMonth = int.Parse(Month);
                        obj.AccountYear = int.Parse(MonthText.Split('-')[1]);
                        obj.AddUpdateDate1 = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                        obj.AddUpdateDate2 = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                        obj.AddUpdateDate3 = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                        Helper.ExecuteService("TR2", "SaveHPTR2Form", obj);
                    }
                    msg = "Sucessfully saved HPTR2 form";
                }
            }
            catch (Exception ex)
            {
                msg = ex.InnerException.ToString();
            }

            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        // [HttpPost]
        //public PartialViewResult ShowHPTR2Form(int Designation, string FinacialYear, int Month)
        //{
        //    //List<tEmployeeAccountDetails> data = new List<tEmployeeAccountDetails>();
        //    var data = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetHPTR2Data", new tEmployeeAccountDetails { EmpId = Designation, finanacialYear = FinacialYear, AccountMonth = Month });
        //    var Staff = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", Designation);
        //    ViewBag.StaffName = Staff.StaffName + "-" + Staff.Designation;
        //    var MainResult = data.AsQueryable();
        //    //  ViewBag.FinancialMonthList = ModelMapping.GetMonthsList(FinacialYear);

        //    return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_HPTR2List.cshtml", MainResult);
        //}
        [HttpPost]
        public JsonResult ShowHPTR2Form(int EmployeeId, string FinacialYear)
        {


            string data = GetTR2Result(EmployeeId, FinacialYear);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public string GetTR2Result(int EmployeeId, string FinacialYear)
        {
            var Staff = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", EmployeeId);
            ViewBag.StaffName = Staff.StaffName + "-" + Staff.Designation;

            var result = new List<int>();
            result.Add(4);
            result.Add(5);
            result.Add(6);
            result.Add(7);
            result.Add(8);
            result.Add(9);
            result.Add(10);
            result.Add(11);
            result.Add(12);
            result.Add(1);
            result.Add(2);
            result.Add(3);

            StringBuilder sb = new StringBuilder();
            sb.Append("<table class='table table-bordered table-striped'> <thead> <tr>");
            sb.Append("<th>Sr. No.</th><th>Month</th>  <th>Name & Designation <br>1</th> <th>Basic Pay <br>2</th> <th>Special Par<br> 3</th>");
            sb.Append(" <th>Dearness Allowance <br>4</th><th>Compens Allowance<br> 5</th><th>H.R.A. Allowance <br>6</th><th>Capital Allowance<br>7</th>");
            sb.Append(" <th>Convey Allowance<br>8</th><th>Washing Allowance<br>9</th><th>GP<br>10</th><th>Sec. Pay<br>11</th><th>12</th><th>13</th><th>14</th>");
            sb.Append("<th>Total 2 to 14<br> 15</th><th>GPF Account No<br> 16</th>");

            sb.Append("<th>GPF Subs<br>17</th><th>GPF Advance<br>18</th><th>HBA<br>19</th> <th>HBA Interest<br>20</th> <th>N. Car/ Scooter Advance<br>21</th>");
            sb.Append("<th>M.C./ Scooter Interest<br>22</th><th>Warm cloth Advance<br>23</th><th>Warm cloth Interest<br>24</th> <th>Festival Advance<br>25</th>");
            sb.Append("<th>L.T.C. T.A. Advance<br>26</th> <th>Miscel. Recov.<br>27</th> <th>28</th> <th>Total (A) 17 to 28 <br>29</th>");

            sb.Append("<th>Saving Fund<br>30</th> <th>Insur Fund<br>31</th> <th>House Rent<br>32</th> <th>P.L.I.<br>33</th> <th>L.I.C.<br>34</th>");
            sb.Append("<th>IncomeTax<br>35</th> <th>Sur-charge<br>36</th> <th>OtherBT<br>37</th> <th>Total (B) 30 to 37<br>38</th> <th>Total Deduction <br>39</th> <th>Net Payment<br>40</th>");
            sb.Append("</tr> </thead>");


            sb.Append("<tbody>");

            int count = 1;
            foreach (var item in result)
            {
                var Maindata = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetHPTR2Data", new tEmployeeAccountDetails { EmpId = EmployeeId, finanacialYear = FinacialYear, AccountMonth = item });
                System.Globalization.DateTimeFormatInfo mfi = new
System.Globalization.DateTimeFormatInfo();
                string strMonthName = mfi.GetMonthName(item).ToString();
                if (Maindata != null && Maindata.Count() > 0)
                {
                    var QuerableList = Maindata.AsQueryable();
                    sb.Append("<tr>");
                    sb.Append(string.Format("<td>{0}</td>", count));
                    sb.Append(string.Format("<td>{0}</td>", strMonthName));
                    sb.Append(string.Format("<td>{0}</td>", Staff.StaffName + "-" + Staff.Designation));

                    // DUES
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().Id, 2));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().Id, 3));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2ForDAData({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().Id, 4));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 5).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 5).FirstOrDefault().Id, 5));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 6).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 6).FirstOrDefault().Id, 6));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 7).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 7).FirstOrDefault().Id, 7));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 8).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 8).FirstOrDefault().Id, 8));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 9).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 9).FirstOrDefault().Id, 9));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().Id, 10));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().Id, 11));
                    // Getting total of 2-9

                    var idList = new[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
                    decimal DuesTotal = QuerableList.Where(m => idList.Contains(m.AccountFieldId)).Sum(m => m.AccountFieldValue);
                    // sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", DuesTotal));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));


                    // A.G. Deduction
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 17).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 17).FirstOrDefault().Id, 17));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 18).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 18).FirstOrDefault().Id, 18));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 19).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 19).FirstOrDefault().Id, 19));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 20).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 20).FirstOrDefault().Id, 20));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 21).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 21).FirstOrDefault().Id, 21));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 22).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 22).FirstOrDefault().Id, 22));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 23).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 23).FirstOrDefault().Id, 23));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 24).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 24).FirstOrDefault().Id, 24));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 25).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 25).FirstOrDefault().Id, 25));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 26).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 26).FirstOrDefault().Id, 26));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 27).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 27).FirstOrDefault().Id, 27));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td>{0}</td>", Maindata.Where(m => m.AccountFieldId == 28).FirstOrDefault().AccountFieldValue));

                    // Getting total from 17-28
                    var AGList = new[] { 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 };
                    decimal AGTotal = QuerableList.Where(m => AGList.Contains(m.AccountFieldId)).Sum(m => m.AccountFieldValue);
                    sb.Append(string.Format("<td>{0}</td>", AGTotal));

                    // Treasury Deduction
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 30).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 30).FirstOrDefault().Id, 30));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 31).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 31).FirstOrDefault().Id, 31));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 32).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 32).FirstOrDefault().Id, 32));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 33).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 33).FirstOrDefault().Id, 33));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 34).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 34).FirstOrDefault().Id, 34));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 35).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 35).FirstOrDefault().Id, 35));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 36).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 36).FirstOrDefault().Id, 36));
                    sb.Append(string.Format("<td>{0} <a href='#' onclick='PopulateTR2Data({1},{2})'><i class='ace-icon fa fa-pencil bigger-120'></i></a></td>", Maindata.Where(m => m.AccountFieldId == 37).FirstOrDefault().AccountFieldValue, Maindata.Where(m => m.AccountFieldId == 37).FirstOrDefault().Id, 37));

                    // Getting total from 30-37
                    var TDList = new[] { 30, 31, 32, 33, 34, 35, 36, 37 };
                    decimal TDTotal = QuerableList.Where(m => TDList.Contains(m.AccountFieldId)).Sum(m => m.AccountFieldValue);
                    sb.Append(string.Format("<td>{0}</td>", TDTotal));


                    // getting total from 29-38
                    var FinalList = new[] { 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 33, 34, 35, 36, 37 };
                    decimal FinalTotal = QuerableList.Where(m => FinalList.Contains(m.AccountFieldId)).Sum(m => m.AccountFieldValue);

                    sb.Append(string.Format("<td>{0}</td>", FinalTotal));
                    sb.Append(string.Format("<td>{0}</td>", DuesTotal - FinalTotal));
                    sb.Append("</tr>");
                }
                else
                {
                    sb.Append("<tr>");
                    sb.Append(string.Format("<td>{0}</td>", count));
                    sb.Append(string.Format("<td>{0}</td>", strMonthName));
                    sb.Append(string.Format("<td>{0}</td>", Staff.StaffName + "-" + Staff.Designation));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td>{0}</td>", string.Empty));
                    sb.Append("</tr>");
                }
                count++;
            }
            return sb.ToString();
        }
        [HttpPost]
        public PartialViewResult PopulateTR2Data(int Id, int AccountFieldId)
        {
            var EmployeeAccountDetails = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldId", new tEmployeeAccountDetails { Id = Id, AccountFieldId = AccountFieldId });

            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_PopulateTR2Data.cshtml", EmployeeAccountDetails);
        }

        [HttpPost]
        public JsonResult UpdateHPTR2Form(int Id, int AccountFieldId, decimal AccountFieldValue, int EmpId, DateTime? FromDate, DateTime? ToDate)
        {
            string data = string.Empty;

            try
            {

                var EmployeeAccountDetails = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldId", new tEmployeeAccountDetails { Id = Id, AccountFieldId = AccountFieldId });


                if (EmployeeAccountDetails != null && EmployeeAccountDetails.AdditionValue1 == 0)
                {
                    EmployeeAccountDetails.AdditionValue1 = EmployeeAccountDetails.AccountFieldValue;
                    EmployeeAccountDetails.AddUpdateDate1 = DateTime.Now;
                }
                else if (EmployeeAccountDetails != null && EmployeeAccountDetails.AdditionValue2 == 0)
                {
                    EmployeeAccountDetails.AdditionValue2 = EmployeeAccountDetails.AccountFieldValue;
                    EmployeeAccountDetails.AddUpdateDate2 = DateTime.Now;
                }
                else if (EmployeeAccountDetails != null && EmployeeAccountDetails.AdditionValue3 == 0)
                {
                    EmployeeAccountDetails.AdditionValue3 = EmployeeAccountDetails.AccountFieldValue;
                    EmployeeAccountDetails.AddUpdateDate3 = DateTime.Now;
                }
                int DaysDiff = 0;
                if (FromDate != null && FromDate.Value.ToShortDateString() != "01/01/0001" && ToDate != null && ToDate.Value.ToShortDateString() != "01/01/0001")
                {
                    DaysDiff = int.Parse((ToDate.Value - FromDate.Value).TotalDays.ToString()) + 1;
                }

                if (DaysDiff > 0)
                {
                    int DaysInMonth = DateTime.DaysInMonth(EmployeeAccountDetails.AccountYear.Value, EmployeeAccountDetails.AccountMonth.Value);
                    int RemainingDay = DaysInMonth - DaysDiff;
                    decimal PreviousSalary = (EmployeeAccountDetails.AccountFieldValue / DaysInMonth) * RemainingDay;
                    decimal CurrentSalary = (AccountFieldValue / DaysInMonth) * DaysDiff;
                    decimal MonthSalary = PreviousSalary + CurrentSalary;
                    EmployeeAccountDetails.AccountFieldValue = MonthSalary;
                }
                else
                {
                    EmployeeAccountDetails.AccountFieldValue = AccountFieldValue;
                }
                if (FromDate != null && FromDate.Value.ToShortDateString() != "01/01/0001" && ToDate != null && ToDate.Value.ToShortDateString() != "01/01/0001")
                {
                    EmployeeAccountDetails.FromDate = FromDate;
                    EmployeeAccountDetails.ToDate = ToDate;
                    EmployeeAccountDetails.IncrementAmt = AccountFieldValue;
                }
                Helper.ExecuteService("TR2", "UpdateHPTR2Form", EmployeeAccountDetails);
                if (AccountFieldId == 2 || AccountFieldId == 3 || AccountFieldId == 10 || AccountFieldId == 11)
                {
                    decimal DeanessAllowance = 0;
                    mSiteSettingsVS DAPERCENTAGE = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "DAPERCENTAGE" });
                    decimal DAPercentage = decimal.Parse(DAPERCENTAGE.SettingValue);

                    if (AccountFieldId == 2)
                    {
                        var SpecailPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 3, AccountMonth = EmployeeAccountDetails.AccountMonth });
                        var GP = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 10, AccountMonth = EmployeeAccountDetails.AccountMonth });
                        var SecPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 11, AccountMonth = EmployeeAccountDetails.AccountMonth });

                        DeanessAllowance = ((AccountFieldValue + SpecailPay.AccountFieldValue + GP.AccountFieldValue + SecPay.AccountFieldValue) * DAPercentage) / 100;
                    }
                    if (AccountFieldId == 3)
                    {
                        var BasicPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 2, AccountMonth = EmployeeAccountDetails.AccountMonth });
                        var GP = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 10, AccountMonth = EmployeeAccountDetails.AccountMonth });
                        var SecPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 11, AccountMonth = EmployeeAccountDetails.AccountMonth });

                        DeanessAllowance = ((AccountFieldValue + BasicPay.AccountFieldValue + GP.AccountFieldValue + SecPay.AccountFieldValue) * DAPercentage) / 100;
                    }
                    if (AccountFieldId == 10)
                    {
                        var BasicPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 2, AccountMonth = EmployeeAccountDetails.AccountMonth });
                        var SpecailPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 3, AccountMonth = EmployeeAccountDetails.AccountMonth });
                        var SecPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 11, AccountMonth = EmployeeAccountDetails.AccountMonth });

                        DeanessAllowance = ((AccountFieldValue + SpecailPay.AccountFieldValue + BasicPay.AccountFieldValue + SecPay.AccountFieldValue) * DAPercentage) / 100;
                    }
                    if (AccountFieldId == 11)
                    {
                        var BasicPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 2, AccountMonth = EmployeeAccountDetails.AccountMonth });
                        var SpecailPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 3, AccountMonth = EmployeeAccountDetails.AccountMonth });
                        var GP = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 10, AccountMonth = EmployeeAccountDetails.AccountMonth });

                        DeanessAllowance = ((AccountFieldValue + BasicPay.AccountFieldValue + SpecailPay.AccountFieldValue + GP.AccountFieldValue) * DAPercentage) / 100;
                    }

                    var DeanessAllowancePay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 4, AccountMonth = EmployeeAccountDetails.AccountMonth });

                    if (DeanessAllowancePay != null && DeanessAllowancePay.AdditionValue1 == 0)
                    {
                        DeanessAllowancePay.AdditionValue1 = DeanessAllowancePay.AccountFieldValue;
                        DeanessAllowancePay.AddUpdateDate1 = DateTime.Now;
                    }
                    else if (DeanessAllowancePay != null && DeanessAllowancePay.AdditionValue2 == 0)
                    {
                        DeanessAllowancePay.AdditionValue2 = DeanessAllowancePay.AccountFieldValue;
                        DeanessAllowancePay.AddUpdateDate2 = DateTime.Now;
                    }
                    else if (DeanessAllowancePay != null && DeanessAllowancePay.AdditionValue3 == 0)
                    {
                        DeanessAllowancePay.AdditionValue3 = DeanessAllowancePay.AccountFieldValue;
                        DeanessAllowancePay.AddUpdateDate3 = DateTime.Now;
                    }
                    DeanessAllowancePay.AccountFieldValue = DeanessAllowance;
                    Helper.ExecuteService("TR2", "UpdateHPTR2Form", DeanessAllowancePay);
                }

                var EmployeeAccountList = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetTR2ListByAccountFieldId", new tEmployeeAccountDetails { Id = Id, AccountFieldId = AccountFieldId, EmpId = EmpId });
                if (EmployeeAccountList != null)
                {
                    foreach (var item in EmployeeAccountList)
                    {
                        if (item != null && item.AdditionValue1 == 0)
                        {
                            item.AdditionValue1 = item.AccountFieldValue;
                            item.AddUpdateDate1 = DateTime.Now;
                        }
                        else if (item != null && item.AdditionValue2 == 0)
                        {
                            item.AdditionValue2 = item.AccountFieldValue;
                            item.AddUpdateDate2 = DateTime.Now;
                        }
                        else if (item != null && item.AdditionValue3 == 0)
                        {
                            item.AdditionValue3 = item.AccountFieldValue;
                            item.AddUpdateDate3 = DateTime.Now;
                        }
                        item.AccountFieldValue = AccountFieldValue;
                        Helper.ExecuteService("TR2", "UpdateHPTR2Form", item);

                        if (item.AccountFieldId == 2 || item.AccountFieldId == 3 || item.AccountFieldId == 10 || item.AccountFieldId == 11)
                        {
                            decimal DeanessAllowance = 0;
                            mSiteSettingsVS DAPERCENTAGE = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "DAPERCENTAGE" });
                            decimal DAPercentage = decimal.Parse(DAPERCENTAGE.SettingValue);

                            //if (item.AccountFieldId == 2)
                            //{
                            //    var SpecailPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = item.EmpId, finanacialYear = item.finanacialYear, AccountFieldId = 3, AccountMonth = item.AccountMonth });
                            //    DeanessAllowance = ((AccountFieldValue + SpecailPay.AccountFieldValue) * DAPercentage) / 100;
                            //}
                            //if (item.AccountFieldId == 3)
                            //{
                            //    var BasicPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = item.EmpId, finanacialYear = item.finanacialYear, AccountFieldId = 2, AccountMonth = item.AccountMonth });
                            //    DeanessAllowance = ((AccountFieldValue + BasicPay.AccountFieldValue) * DAPercentage) / 100;
                            //}

                            if (item.AccountFieldId == 2)
                            {
                                var SpecailPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 3, AccountMonth = EmployeeAccountDetails.AccountMonth });
                                var GP = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 10, AccountMonth = EmployeeAccountDetails.AccountMonth });
                                var SecPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 11, AccountMonth = EmployeeAccountDetails.AccountMonth });

                                DeanessAllowance = ((AccountFieldValue + SpecailPay.AccountFieldValue + GP.AccountFieldValue + SecPay.AccountFieldValue) * DAPercentage) / 100;
                            }
                            if (item.AccountFieldId == 3)
                            {
                                var BasicPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 2, AccountMonth = EmployeeAccountDetails.AccountMonth });
                                var GP = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 10, AccountMonth = EmployeeAccountDetails.AccountMonth });
                                var SecPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 11, AccountMonth = EmployeeAccountDetails.AccountMonth });

                                DeanessAllowance = ((AccountFieldValue + BasicPay.AccountFieldValue + GP.AccountFieldValue + SecPay.AccountFieldValue) * DAPercentage) / 100;
                            }
                            if (item.AccountFieldId == 10)
                            {
                                var BasicPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 2, AccountMonth = EmployeeAccountDetails.AccountMonth });
                                var SpecailPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 3, AccountMonth = EmployeeAccountDetails.AccountMonth });
                                var SecPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 11, AccountMonth = EmployeeAccountDetails.AccountMonth });

                                DeanessAllowance = ((AccountFieldValue + SpecailPay.AccountFieldValue + BasicPay.AccountFieldValue + SecPay.AccountFieldValue) * DAPercentage) / 100;
                            }
                            if (item.AccountFieldId == 11)
                            {
                                var BasicPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 2, AccountMonth = EmployeeAccountDetails.AccountMonth });
                                var SpecailPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 3, AccountMonth = EmployeeAccountDetails.AccountMonth });
                                var GP = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 10, AccountMonth = EmployeeAccountDetails.AccountMonth });

                                DeanessAllowance = ((AccountFieldValue + BasicPay.AccountFieldValue + SpecailPay.AccountFieldValue + GP.AccountFieldValue) * DAPercentage) / 100;
                            }

                            var DeanessAllowancePay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = item.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 4, AccountMonth = item.AccountMonth });

                            if (DeanessAllowancePay != null && DeanessAllowancePay.AdditionValue1 == 0)
                            {
                                DeanessAllowancePay.AdditionValue1 = DeanessAllowancePay.AccountFieldValue;
                                DeanessAllowancePay.AddUpdateDate1 = DateTime.Now;
                            }
                            else if (DeanessAllowancePay != null && DeanessAllowancePay.AdditionValue2 == 0)
                            {
                                DeanessAllowancePay.AdditionValue2 = DeanessAllowancePay.AccountFieldValue;
                                DeanessAllowancePay.AddUpdateDate2 = DateTime.Now;
                            }
                            else if (DeanessAllowancePay != null && DeanessAllowancePay.AdditionValue3 == 0)
                            {
                                DeanessAllowancePay.AdditionValue3 = DeanessAllowancePay.AccountFieldValue;
                                DeanessAllowancePay.AddUpdateDate3 = DateTime.Now;
                            }
                            DeanessAllowancePay.AccountFieldValue = DeanessAllowance;
                            Helper.ExecuteService("TR2", "UpdateHPTR2Form", DeanessAllowancePay);
                        }
                    }
                }

                data = GetTR2Result(EmployeeAccountDetails.EmpId, EmployeeAccountDetails.finanacialYear);
            }
            catch (Exception ex)
            {
                data = ex.InnerException.ToString();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult PopulateTR2DataForDA(int Id, int AccountFieldId)
        {
            var EmployeeAccountDetails = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldId", new tEmployeeAccountDetails { Id = Id, AccountFieldId = AccountFieldId });

            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/PopulateTR2DataForDA.cshtml", EmployeeAccountDetails);
        }
        [HttpPost]
        public JsonResult UpdateDAForm(int Id, int AccountFieldId, int DAPercentage, int EmpId)
        {
            string data = string.Empty;

            try
            {
                mSiteSettingsVS DAObj = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "DAPERCENTAGE" });
                DAObj.SettingValue = DAPercentage.ToString();
                Helper.ExecuteService("User", "UpdateSiteSettings", DAObj);

                decimal DeanessAllowance = 0;
                var EmployeeAccountDetails = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldId", new tEmployeeAccountDetails { Id = Id, AccountFieldId = AccountFieldId });

                var BasicPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 2, AccountMonth = EmployeeAccountDetails.AccountMonth });

                var SpecailPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 3, AccountMonth = EmployeeAccountDetails.AccountMonth });
                var GP = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 10, AccountMonth = EmployeeAccountDetails.AccountMonth });
                var SecPay = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 11, AccountMonth = EmployeeAccountDetails.AccountMonth });

                DeanessAllowance = ((BasicPay.AccountFieldValue + SpecailPay.AccountFieldValue + GP.AccountFieldValue + SecPay.AccountFieldValue) * DAPercentage) / 100;
                if (EmployeeAccountDetails != null && EmployeeAccountDetails.AdditionValue1 == 0)
                {
                    EmployeeAccountDetails.AdditionValue1 = EmployeeAccountDetails.AccountFieldValue;
                    EmployeeAccountDetails.AddUpdateDate1 = DateTime.Now;
                }
                else if (EmployeeAccountDetails != null && EmployeeAccountDetails.AdditionValue2 == 0)
                {
                    EmployeeAccountDetails.AdditionValue2 = EmployeeAccountDetails.AccountFieldValue;
                    EmployeeAccountDetails.AddUpdateDate2 = DateTime.Now;
                }
                else if (EmployeeAccountDetails != null && EmployeeAccountDetails.AdditionValue3 == 0)
                {
                    EmployeeAccountDetails.AdditionValue3 = EmployeeAccountDetails.AccountFieldValue;
                    EmployeeAccountDetails.AddUpdateDate3 = DateTime.Now;
                }
                EmployeeAccountDetails.AccountFieldValue = DeanessAllowance;
                Helper.ExecuteService("TR2", "UpdateHPTR2Form", EmployeeAccountDetails);


                var EmployeeAccountList = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetTR2ListByAccountFieldId", new tEmployeeAccountDetails { Id = Id, AccountFieldId = AccountFieldId, EmpId = EmpId });
                if (EmployeeAccountList != null)
                {
                    decimal DeanessAllowance1 = 0;
                    foreach (var item in EmployeeAccountList)
                    {
                        var BasicPay1 = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 2, AccountMonth = EmployeeAccountDetails.AccountMonth });

                        var SpecailPay1 = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 3, AccountMonth = EmployeeAccountDetails.AccountMonth });

                        var GP1 = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 10, AccountMonth = EmployeeAccountDetails.AccountMonth });

                        var SecPay1 = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetTR2DetailByAccountFieldIdAndMonthAndEmpId", new tEmployeeAccountDetails { EmpId = EmployeeAccountDetails.EmpId, finanacialYear = EmployeeAccountDetails.finanacialYear, AccountFieldId = 11, AccountMonth = EmployeeAccountDetails.AccountMonth });

                        DeanessAllowance1 = ((BasicPay1.AccountFieldValue + SpecailPay1.AccountFieldValue + GP1.AccountFieldValue + SecPay1.AccountFieldValue) * DAPercentage) / 100;

                        if (item != null && item.AdditionValue1 == 0)
                        {
                            item.AdditionValue1 = item.AccountFieldValue;
                            item.AddUpdateDate1 = DateTime.Now;
                        }
                        else if (item != null && item.AdditionValue2 == 0)
                        {
                            item.AdditionValue2 = item.AccountFieldValue;
                            item.AddUpdateDate2 = DateTime.Now;
                        }
                        else if (item != null && item.AdditionValue3 == 0)
                        {
                            item.AdditionValue3 = item.AccountFieldValue;
                            item.AddUpdateDate3 = DateTime.Now;
                        }
                        item.AccountFieldValue = DeanessAllowance1;
                        Helper.ExecuteService("TR2", "UpdateHPTR2Form", item);
                    }
                }

                data = GetTR2Result(EmployeeAccountDetails.EmpId, EmployeeAccountDetails.finanacialYear);
            }
            catch (Exception ex)
            {
                data = ex.InnerException.ToString();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult CopyTR2Data(int EmployeeId = 0, string FinacialYear = "", int Month = 0)
        {
            var Maindata = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetHPTR2Data", new tEmployeeAccountDetails { EmpId = EmployeeId, finanacialYear = FinacialYear, AccountMonth = Month });
            return Json(Maindata, JsonRequestBehavior.AllowGet);
        }

        #region Form-16

        public ActionResult Form16()
        {


            var StaffList = (List<mStaff>)Helper.ExecuteService("staff", "GetAllStaffList", null);
            ViewBag.StaffList = new SelectList(StaffList, "StaffID", "NameWithDesignation");
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            DataSet dataSet = new DataSet();
            var Parameter1 = new List<KeyValuePair<string, string>>();
            Parameter1.Add(new KeyValuePair<string, string>("@FinancialYear", FinYearValue));
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteLoanAndRebate", Parameter1);

            ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_Form16.cshtml");
        }
        [HttpPost]
        public JsonResult GetGrossAmount(int StaffId = 0, string FinacialYear = "")
        {
            var Maindata = (decimal)Helper.ExecuteService("TR2", "GetNetAmount", new tEmployeeAccountDetails { EmpId = StaffId, finanacialYear = FinacialYear });
            var GrossAmount = Math.Round(Maindata);
            return Json(GrossAmount, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PopulateRebateForm()
        {

            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_CalculateRebateFrom.cshtml");
        }

        public ActionResult PopulateLoanForm(int StaffId = 0, string FinacialYear = "")
        {
            //var StaffList = (List<mStaff>)Helper.ExecuteService("staff", "GetAllStaffList", null);
            //ViewBag.StaffList = new SelectList(StaffList, "StaffID", "NameWithDesignation");
            //mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            //string FinYearValue = SiteSettingsSessn.SettingValue;
            //ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            ViewBag.EmpId = StaffId;
            ViewBag.FinacialYear = FinacialYear;
            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_CalculateLoanFrom.cshtml");
        }

        [HttpPost]
        public JsonResult CalculateHBA(int OpenBalance, int MonthlyDeduction, int InterestPer, string FinancialYear)
        {
            StringBuilder Maindata = new StringBuilder();
            var MonthList = ModelMapping.GetStringMonthsList(FinancialYear);
            Maindata.Append("<table class='table table-bordered'> ");
            Maindata.Append(string.Format("<tr><td>Int Rate</td><td>{0}</td><td></td></tr>", InterestPer));
            Maindata.Append(string.Format("<tr><td>Opening Balance</td><td></td><td>{0}</td></tr>", OpenBalance));

            int Balance = OpenBalance - MonthlyDeduction;
            int TotalBalance = 0;
            int count = 1;
            foreach (var item in MonthList)
            {
                Maindata.Append("<tr>");
                Maindata.Append(string.Format("<input type='hidden' value='{0}' name='HBARowCount' />", count));
                Maindata.Append(string.Format("<td><input type='hidden' value='{0}' name='HBAMonth-{1}' />{2}</td>", item, count, item));
                Maindata.Append(string.Format("<td><input type='hidden' value='{0}' name='HBAMonthDeduction-{1}' />{2}</td>", MonthlyDeduction, count, MonthlyDeduction));
                Maindata.Append(string.Format("<td><input type='hidden' value='{0}' name='HBABalance-{1}' />{2}</td>", Balance, count, Balance));
                Maindata.Append("</tr>");
                TotalBalance = TotalBalance + Balance;
                Balance = Balance - MonthlyDeduction;
                count++;
            }
            decimal FinalIntereset = (TotalBalance * InterestPer) / 1200;
            Maindata.Append(string.Format("<tr><td></td><td>Interest</td><td><input type='hidden' value='{0}' name='HBAFinalIntereset' />{1}</td></tr></table>", FinalIntereset, FinalIntereset));
            return Json(Maindata.ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CalculateEDU(int OpenBalance, int MonthlyDeduction, int InterestPer, string FinancialYear)
        {
            StringBuilder Maindata = new StringBuilder();
            var MonthList = ModelMapping.GetStringMonthsList(FinancialYear);
            Maindata.Append("<table class='table table-bordered'> ");
            Maindata.Append(string.Format("<tr><td>Int Rate</td><td>{0}</td><td></td></tr>", InterestPer));
            Maindata.Append(string.Format("<tr><td >Opening Balance</td><td></td><td>{0}</td></tr>", OpenBalance));

            int Balance = OpenBalance - MonthlyDeduction;
            int TotalBalance = 0;
            int count = 1;
            foreach (var item in MonthList)
            {
                Maindata.Append("<tr>");
                Maindata.Append(string.Format("<input type='hidden' value='{0}' name='EDURowCount' />", count));
                Maindata.Append(string.Format("<td><input type='hidden' value='{0}' name='EDUMonth-{1}' />{2}</td>", item, count, item));
                Maindata.Append(string.Format("<td><input type='hidden' value='{0}' name='EDUMonthDeduction-{1}' />{2}</td>", MonthlyDeduction, count, MonthlyDeduction));
                Maindata.Append(string.Format("<td><input type='hidden' value='{0}' name='EDUBalance-{1}' />{2}</td>", Balance, count, Balance));
                Maindata.Append("</tr>");
                TotalBalance = TotalBalance + Balance;
                Balance = Balance - MonthlyDeduction;
                count++;

            }
            decimal FinalIntereset = (TotalBalance * InterestPer) / 1200;
            Maindata.Append(string.Format("<tr><td></td><td>Interest</td><td><input type='hidden' value='{0}' name='EDUFinalIntereset' />{1}</td></tr></table>", FinalIntereset, FinalIntereset));
            return Json(Maindata.ToString(), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult SaleLoanDetails(FormCollection coll)
        {
            int EmpId = int.Parse(coll["EmpId"]);
            string FinancialYear = coll["FinacialYear"];
            decimal HBAFinalIntereset = 0;
            decimal EDUFinalIntereset = 0;

            DataSet dataSet = new DataSet();
            var Parameter1 = new List<KeyValuePair<string, string>>();
            Parameter1.Add(new KeyValuePair<string, string>("@FinancialYear", FinancialYear));
            Parameter1.Add(new KeyValuePair<string, string>("@EmpId", EmpId.ToString()));
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteLoanDetailsByEmpId", Parameter1);


            // this is for HBA Loan
            if (coll["EDUOpenBal"] != null && coll["EDUOpenBal"] != null && coll["EDUOpenBal"] != null && coll["HBARowCount"] != null)
            {
                int HBATotalBalance = 0;
                int HBAOpeningBalance = int.Parse(coll["HBAOpenBal"]);
                int HBAMonthlyDeduction = int.Parse(coll["HBAMonthlyDed"]);
                int HBAIntPer = int.Parse(coll["HBAIntPerc"]);
                string[] HBARowsCount = coll["HBARowCount"].Split(',');

                foreach (var item in HBARowsCount)
                {
                    string Month = coll["HBAMonth-" + item];
                    int MonthDeduction = int.Parse(coll["HBABalance-" + item]);
                    TR2LoanDetail HBAObj = new TR2LoanDetail();
                    HBAObj.OpeningBalance = HBAOpeningBalance;
                    HBAObj.MonthlyDeduction = HBAMonthlyDeduction;
                    HBAObj.InterestPer = HBAIntPer;
                    HBAObj.Month = Month;
                    HBAObj.RemainingValue = MonthDeduction;
                    HBAObj.EmpId = EmpId;
                    HBAObj.FinancialYear = FinancialYear;
                    HBAObj.LoanType = "HBA";
                    Helper.ExecuteService("TR2", "SaveLoanDetails", HBAObj);
                    HBATotalBalance = HBATotalBalance + MonthDeduction;
                }
                HBAFinalIntereset = (HBATotalBalance * HBAIntPer) / 1200;


            }

            // this is for EDU Loan
            if (coll["EDUOpenBal"] != null && coll["EDUMonthlyDed"] != null && coll["EDUIntPerc"] != null && coll["EDURowCount"] != null)
            {
                int EDUTotalBalance = 0;
                int EDUOpeningBalance = int.Parse(coll["EDUOpenBal"]);
                int EDUMonthlyDeduction = int.Parse(coll["EDUMonthlyDed"]);
                int EDUIntPer = int.Parse(coll["EDUIntPerc"]);
                string[] EDURowsCount = coll["EDURowCount"].Split(',');

                foreach (var item in EDURowsCount)
                {
                    string Month = coll["EDUMonth-" + item];
                    int MonthDeduction = int.Parse(coll["EDUBalance-" + item]);
                    TR2LoanDetail EDUObj = new TR2LoanDetail();
                    EDUObj.OpeningBalance = EDUOpeningBalance;
                    EDUObj.MonthlyDeduction = EDUMonthlyDeduction;
                    EDUObj.InterestPer = EDUIntPer;
                    EDUObj.Month = Month;
                    EDUObj.RemainingValue = MonthDeduction;
                    EDUObj.EmpId = EmpId;
                    EDUObj.FinancialYear = FinancialYear;
                    EDUObj.LoanType = "EDU";
                    Helper.ExecuteService("TR2", "SaveLoanDetails", EDUObj);
                    EDUTotalBalance = EDUTotalBalance + MonthDeduction;
                }
                EDUFinalIntereset = (EDUTotalBalance * EDUIntPer) / 1200;
            }
            decimal FinalInterest = HBAFinalIntereset + EDUFinalIntereset;

            return Json(Math.Round(FinalInterest), JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveForm16Form(FormCollection coll)
        {
            try
            {


                int EmpId = int.Parse(coll["EmpId"]);
                string FinancialYear = coll["FinYear"];
                var Form16 = (Form16)Helper.ExecuteService("TR2", "GetForm16", new Form16 { EmpId = EmpId, FinancialYear = FinancialYear });
                if (Form16 == null)
                {
                    int TotalSalary = 0;
                    int.TryParse(coll["TotalSalary"], out TotalSalary);

                    int IncomeOtherSource = 0;
                    int.TryParse(coll["IncomeOtherSource"], out IncomeOtherSource);

                    int Rebate = 0;
                    int.TryParse(coll["Rebate"], out Rebate);
                    int InterestOnHBA = 0;
                    int.TryParse(coll["InterestOnHBA"], out InterestOnHBA);

                    int Taxable = 0;
                    int.TryParse(coll["Taxable"], out Taxable);

                    int Exempted = 0;
                    int.TryParse(coll["Exempted"], out Exempted);

                    int Tax1 = 0;
                    int.TryParse(coll["Tax1"], out Tax1);

                    int Tax2 = 0;
                    int.TryParse(coll["Tax2"], out Tax2);
                    int Tax3 = 0;
                    int.TryParse(coll["Tax3"], out Tax3);

                    int NetTax = 0;
                    int.TryParse(coll["NetTax"], out NetTax);
                    int Surcharge = 0;
                    int.TryParse(coll["Surcharge"], out Surcharge);
                    int TotalTax = 0;
                    int.TryParse(coll["TotalTax"], out TotalTax);



                    Form16 obj = new Form16();
                    obj.TotalGrossSalary = TotalSalary;
                    obj.FinancialYear = FinancialYear;
                    obj.EmpId = EmpId;
                    obj.IncomeFromOtherSources = IncomeOtherSource;
                    obj.Rebate = Rebate;
                    obj.IntersetOnHBA = InterestOnHBA;
                    obj.TaxableAmt = Taxable;
                    obj.Exampted = Exempted;
                    obj.CalculateTax1 = Tax1;
                    obj.CalculateTax2 = Tax2;
                    obj.CalculateTax3 = Tax3;
                    obj.NetTax = NetTax;
                    obj.Surcharge = Surcharge;
                    obj.TotalTax = TotalTax;
                    int Form16Id = (int)Helper.ExecuteService("TR2", "SaveForm16", obj);

                    DataSet dataSet = new DataSet();
                    var Parameter1 = new List<KeyValuePair<string, string>>();
                    Parameter1.Add(new KeyValuePair<string, string>("@FinancialYear", FinancialYear));
                    Parameter1.Add(new KeyValuePair<string, string>("@EmpId", EmpId.ToString()));
                    Parameter1.Add(new KeyValuePair<string, string>("@Form16Id", Form16Id.ToString()));
                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateLoanAndRebate", Parameter1);


                    return Json(Form16Id, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Form-16 already available. So you can't generate the form-16 of this user.", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(ex.InnerException, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Form16Index(int pageId = 1, int pageSize = 10)
        {
            var Form16List = (List<Form16>)Helper.ExecuteService("TR2", "GetAllForm16", null);
            ViewBag.PageSize = pageSize;
            List<Form16> pagedRecord = new List<Form16>();
            if (pageId == 1)
            {
                pagedRecord = Form16List.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = Form16List.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(Form16List.Count, pageId, pageSize);
            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_Form16Index.cshtml", pagedRecord);
        }

        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 10)
        {
            var Form16List = (List<Form16>)Helper.ExecuteService("TR2", "GetAllForm16", null);

            ViewBag.PageSize = pageSize;
            List<Form16> pagedRecord = new List<Form16>();
            if (pageId == 1)
            {
                pagedRecord = Form16List.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = Form16List.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(Form16List.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_Form16List.cshtml", pagedRecord);
        }

        public ActionResult DownloadFrom16Pdf(int Form16Id)
        {
            string url = "/Form16/";



            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            string path = "";
            string Result = GetForm16Data(Form16Id);
            string FileName = CreateFrom16Pdf(Result, url);
            try
            {

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                path = Path.Combine(Server.MapPath("~" + url), FileName);

#pragma warning disable CS0219 // The variable 'contentType' is assigned but its value is never used
                string contentType = "application/octet-stream";
#pragma warning restore CS0219 // The variable 'contentType' is assigned but its value is never used
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

            byte[] bytes = System.IO.File.ReadAllBytes(path);
            return File(bytes, "application/pdf");
        }

        public string CreateFrom16Pdf(string Report, string Url)
        {
            try
            {
                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 20, 20), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = Report;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }



                Guid FId = Guid.NewGuid();
                string fileName = FId + "_Form16.pdf";


                string directory = Server.MapPath(Url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(Server.MapPath("~" + Url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();



                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {


            }
        }
        public string GetForm16Data(int Form16Id)
        {
            var Form16 = (Form16)Helper.ExecuteService("TR2", "GetForm16ById", new Form16 { ID = Form16Id });
            StringBuilder sb = new StringBuilder();
            sb.Append("<p style='text-align:center; font-size:19px;'>FORM NO. 16</p>");
            sb.Append("<div>");
            sb.Append(string.Format("<div class='' style='border: 1px solid #000000;'>"));

            sb.Append(string.Format("<table style='border-collapse: collapse;width: 100%;'>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td colspan='4' style='vertical-align: top; border: 1px solid #000000;text-align:center;'>Certificate under section 203 of the income tax Act . 1961 for tax deducated at source salary</td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td colspan='2' style='border: 1px solid #000000; text-align:center;  '>Name and address of the Employer</td>"));
            sb.Append(string.Format("<td colspan='2' style='border: 1px solid #000000;text-align:center;'>Name and designation of the Employee</td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format(" <td colspan='2' style='border: 1px solid #000000; text-align:center;  '>Secretrary H.P Vidhan Sabha</td>"));
            sb.Append(string.Format(" <td colspan='2' style='border: 1px solid #000000;text-align:center;'>{0}, {1}</td>", Form16.StaffName, Form16.Designation));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format(" <td style='border: 1px solid #000000; width:25%; text-align:center; '>PAN of the Deductor</td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;width:25%;text-align:center;'>TAN of the Deductor</td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;width:25%;text-align:center;   '>PAN of Employee</td>"));
            sb.Append(string.Format(" <td style='border: 1px solid #000000;width:25%;text-align:center;'>Employee Reference No<br />"));
            sb.Append(string.Format("provided by the Employer(if available)</td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;   '></td>"));
            sb.Append(string.Format(" <td style='border: 1px solid #000000;text-align:center;'>PTLH11004A</td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;  text-align:center; '>AFVPB23337R</td>"));
            sb.Append(string.Format(" <td style='border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td colspan='2' style='border: 1px solid #000000;   '>CIT(TDS)</td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;   '>Assessment Year</td>"));
            sb.Append(string.Format(" <td style='border: 1px solid #000000;'>Period</td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td colspan='2' style='border: 1px solid #000000;   '><span style='text-align: left;'>Addres&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style='text-align: center'>CIT(TDS) Circle</span><br />"));
            sb.Append(string.Format("City : Shimla &nbsp;&nbsp;&nbsp; Pincode : 171004</td>"));
            var AssessmentYear = Form16.FinancialYear.Split('-')[1] + '-' + int.Parse(Form16.FinancialYear.Split('-')[1]) + 1;
            sb.Append(string.Format("<td style='border: 1px solid #000000;   '>{0}</td>", AssessmentYear));
            sb.Append(string.Format("<td style='border: 1px solid #000000; '>"));
            sb.Append(string.Format("<table style='border-collapse: collapse; border-spacing: 0; width: 100%;'>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;   '>From</td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;   '>To</td>"));
            sb.Append(string.Format(" </tr><tr>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;   '>01.04.{0}</td>", Form16.FinancialYear.Split('-')[0]));
            sb.Append(string.Format("<td style='border: 1px solid #000000;   '>31.03.{0}</td>", Form16.FinancialYear.Split('-')[1]));
            sb.Append(string.Format(" </tr></table> </td> </tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td colspan='4' style='border: 1px solid #000000;'>Summary of Tax Deducation at Source</td>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Quarter</td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;text-align:center;'>Reciept Numbers of original quartely statement of TDS<br> under sub-section (3) of section 200</td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Ammount of tax deducated</td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Amount of tax deposited remmitteg</td>"));
            sb.Append(string.Format(" </tr>"));
            var QuarterlyAmt = Form16.TotalTax / 4;
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Quarter 1</td>"));
            sb.Append(string.Format(" <td style='border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Rs. {0}</td>", QuarterlyAmt));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Rs. {0} </td>", QuarterlyAmt));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Quarter 2</td>"));
            sb.Append(string.Format(" <td style='border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Rs. {0}</td>", QuarterlyAmt));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Rs. {0} </td>", QuarterlyAmt));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));

            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Quarter 3</td>"));
            sb.Append(string.Format(" <td style='border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Rs. {0}</td>", QuarterlyAmt));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Rs. {0} </td>", QuarterlyAmt));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Quarter 4</td>"));
            sb.Append(string.Format(" <td style='border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Rs. {0}</td>", QuarterlyAmt));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Rs. {0} </td>", QuarterlyAmt));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td colspan='2' style='border: 1px solid #000000;text-align:center;'>Total</td>"));
            sb.Append(string.Format("<td style='border: 1px solid #000000;'>Rs.{0}</td>", Form16.TotalTax));
            sb.Append(string.Format(" <td style='border: 1px solid #000000;'>Rs.{0}</td>", Form16.TotalTax));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format(" <td colspan='4' style='border: 1px solid #000000;text-align:center;'>PART B (Annexure)<br />"));
            sb.Append(string.Format(" <br />"));
            sb.Append(string.Format("DETAILS OF SALARY PAID AND ANY OTHER INCOME AND TAX DEDUCTED </td></tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td colspan='4' style='border: 1px solid #000000; '>"));
            sb.Append(string.Format("<table style='border-collapse: collapse;width: 100%;'>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>1. Gross salary</td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'> </td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'></td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>(a) Salary as per provision contained in senction 17(1)</td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. {0}</td>", Form16.TotalGrossSalary));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'></td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>(b) Value of perquisites u/s 17(2) (as per From No 12BB wherever applicable)</td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. 0</td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'></td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>(c) Value of perquisites u/s 17(3) (as per From No 12BB wherever applicable)</td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. 0</td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'></td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 40%'>(d) total</td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'></td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. {0}</td>", Form16.TotalGrossSalary));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 40%'>2. Less Allowance to the extent exemt under section 10"));
            sb.Append(string.Format(" <br />"));
            sb.Append(string.Format("<table style='border-collapse: collapse;width: 100%;'>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border:1px 1px 1px 0px  solid #000000; border-top: 1px solid #000000; border-right: 1px solid #000000; text-align: left;  '>Allowance</td>"));
            sb.Append(string.Format("<td style='text-align: left; border-bottom: 1px solid #000000; border-top: 1px solid #000000;  '>Rs</td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-right: 1px solid #000000; text-align: left;  '>1) Conv Allowance</td>"));
            sb.Append(string.Format("<td style='text-align: left; border-bottom: 1px solid #000000; border-top: 1px solid #000000;  '>Rs </td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-right: 1px solid #000000; text-align: left;  '>ii) C.C.A</td>"));
            sb.Append(string.Format("<td style='text-align: left; border-bottom: 1px solid #000000; border-top: 1px solid #000000;  '>Rs </td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-right: 1px solid #000000; text-align: left;  '>iii) HRA</td>"));
            sb.Append(string.Format("<td style='text-align: left; border-bottom: 1px solid #000000; border-top: 1px solid #000000;  '>Rs </td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-right: 1px solid #000000; text-align: left;  '>iv)</td>"));
            sb.Append(string.Format("<td style='text-align: left; border-bottom: 1px solid #000000; border-top: 1px solid #000000;  '>Rs </td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("  </table> </td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'></td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'></td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'></td>"));
            sb.Append(string.Format("  </tr>"));

            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>3. Balance (1-2)</td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>4. Deductions U/s 16 & 17</td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>(a) Entertainment allowance</td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>(b) Tax on Employment</td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>(c) H B A Loan Interest [U/s 24(b)]</td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>5. Aggregate of 4(a) and (c)</td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>6. Income chargeable under the read Salaries (3-5)</td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("</tr>"));

            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 40%'>7.Add any other income reported by the employee"));
            sb.Append(string.Format(" <br />"));
            sb.Append(string.Format(" <table style='border-collapse: collapse; border-spacing: 0; width: 100%; height: 100%; margin: 0px; padding: 0px;'>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format(" <td style='border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-right: 1px solid #000000; text-align: left;  '>Income</td>"));
            sb.Append(string.Format("<td style='text-align: left; border-bottom: 1px solid #000000; border-top: 1px solid #000000;  '>Rs</td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-right: 1px solid #000000; text-align: left;  '>Income from Personal/Family Pension</td>"));
            sb.Append(string.Format("<td style='text-align: left; border-bottom: 1px solid #000000; border-top: 1px solid #000000;  '>Rs </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-right: 1px solid #000000; text-align: left;  '>Income from Savings Bank Interest</td>"));
            sb.Append(string.Format("<td style='text-align: left; border-bottom: 1px solid #000000; border-top: 1px solid #000000;  '>Rs </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-right: 1px solid #000000; text-align: left;  '>Income from House Property</td>"));
            sb.Append(string.Format("<td style='text-align: left; border-bottom: 1px solid #000000; border-top: 1px solid #000000;  '>Rs 000000 </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-right: 1px solid #000000; text-align: left;  '>Income other than Savings int.</td>"));
            sb.Append(string.Format("<td style='text-align: left; border-bottom: 1px solid #000000; border-top: 1px solid #000000;  '>Rs 000000 </td>"));
            sb.Append(string.Format(" </tr> </table></td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'></td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'></td>"));
            sb.Append(string.Format("<td style='text-align: left;width: 20%'></td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format(" <td style='border-right: 1px solid #000000; border-top: 1px solid #000000;text-align: left;width: 40%'>8.Gross total income(6+7)</td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format("<td style='border-right: 1px solid #000000; text-align: left;width: 20%'>Rs. </td>"));

            sb.Append(string.Format("<td style='text-align: left;width: 20%'>Rs. </td>"));
            sb.Append(string.Format(" </tr>  </table>"));
            sb.Append(string.Format(" </td>  </tr>  </table> </div>"));

            sb.Append(string.Format("<br style='page-break-after: always;'>"));

            sb.Append(string.Format("<div  style='padding: 0px; border: 1px solid #000000;'>"));

            tRebate obj = new tRebate();
            obj = (tRebate)Helper.ExecuteService("TR2", "GetRebeteByForm16Id", new tRebate { Form16Id = Form16Id });

            sb.Append(string.Format("<table style='border-collapse: collapse;width: 100%;'>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format(" <td style='vertical-align: middle; border: 1px solid #000000;'>9. Deduction under Chapter VI-A<br />(A) section 80CCC and 80CCD</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(a) section 80C</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Gross amount</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Deductible amount</td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(i) GPF</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.GPF));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(ii) GI(Group Insurance)</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.GI));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(iii) PLI</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.PLI));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(iv) U. LIP</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.ULIP));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(v) Tution Fee</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.TutionFee));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(vi) NSC</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.NSC));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(vii) LIC</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.LIC));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(viii) PPF</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.PPF));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(ix) Stamp Duty & Registration Fees</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.StampRegistrationFees));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(x) Any other deductions U/s 80C</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.AnyOtherDeductionUs));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(xi) Any other deductions U/s 80C</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.AnyOtherDeductionUs2));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(xii) HBA Loan Principal</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.HBALoanPrincpal));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(xiii) Fixed Deposit above 5 years</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.FixedDepositAbove5Year));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(xiv) Equity Link Saving Bond</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.EquilityLinkSavingBond));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(b) section 80CCC</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.Section80CCC));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(c) section 80CCD (2)</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS {0}</td>", obj.Section80CCD));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            sb.Append(string.Format(" </tr>"));
            //sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            //sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(i) GPF</td>"));
            //sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>RS 6576</td>"));
            //sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            //sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'></td>"));
            //sb.Append(string.Format(" </tr>"));

            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(B) other saction (e.g. ,80E , 80G etc)<br />under Chapter VI-A</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Gross Amount</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Qualifying Amount</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Deductible Amount</td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(i) section 80E</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs {0}</td>", obj.Section80E));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));

            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(ii) section 80G</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs {0}</td>", obj.Section80G));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(iii) section 80GG</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs {0}</td>", obj.Section80GG));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(iv) section 80GGA</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs {0}</td>", obj.Section80GGA));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(v) section 80GGC</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs {0}</td>", obj.Section80GGC));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(vi) section 80U</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs {0}</td>", obj.Section80U));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(vii) section 80DD</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs {0}</td>", obj.Section80DD));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(viii) section 80D</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs {0}</td>", obj.Section80D));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(ix) section 80DDB</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs {0}</td>", obj.Section80DDB));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(x) U/s 80 TTA [exemt Saving Bank int Max Rs 10,000/-]</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs {0}</td>", obj.Section80TTA));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(xi) U/s 80CCG [New Rajiv Gandhi Equity Saving Schm] </td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>", obj.Section80CCG));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(10)<b>Aggregate of deductible amount under Chapter VI-A</b></td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(11) Total Income (8-10)</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(12) Tax on total income</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(13) Eduction Cess @3% (on tax at <br> S. No 12 plus surcharge as S No 12)</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(14) Total Tax (12+13)</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(15) Relief under section 89(attach details)</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(16) Tax payable(14-15)</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(17) Tax Deducated at Source</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>(18) Payable</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'>Rs 0</td>"));
            sb.Append(string.Format("<td style='vertical-align: middle; border: 1px solid #000000;'> </td>"));
            sb.Append(string.Format(" </tr>"));

            sb.Append(string.Format(" <tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td colspan='4' style='vertical-align: middle; border: 1px solid #000000; border-width: 0px 1px 1px 0px; text-align: center;   column-span: none'><b>Verification</b></td>"));
            sb.Append(string.Format("</tr>"));
            sb.Append(string.Format("<tr style='background-color: #ffffff;'>"));
            sb.Append(string.Format("<td colspan='4' style='vertical-align: middle; border: 1px solid #000000;'><span style='color: #000000; font-size: 13px;'>I , P.C Jaswal son of Late Shri Chand working in the capacity of Deputy Controller (F&A) do hereby certify</span><br />"));
            sb.Append(string.Format("<span style='text-align: justify'>that a sum of 345345 (Rupees One Lac thorty Thousand Four Hundred & Fifty only) has been deposited to the central goverment . I further certify taht the information ginen about is true.complete and correct ansd is based on the books of amount, document,TDS deposited and other available records.</span><br />"));

            sb.Append(string.Format("<br />Place : Shimla<br /><br />"));
            sb.Append(string.Format("Date:15.05.2015 <span style='color: #000000; font-weight: bold; float: right;'>(Signature of person responsible for deductionof tax)</span><br />"));
            sb.Append(string.Format("<br />Designation &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Dy Controller</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Full Name</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Puran Chand Jaswal</span>"));
            sb.Append(string.Format("  </td>"));
            sb.Append(string.Format("  </tr>"));
            sb.Append(string.Format("  </table>"));
            sb.Append(string.Format("  </div>"));
            sb.Append("</div>");
            return sb.ToString();
        }

        public ActionResult ShowLoanDetails(int From16Id)
        {
            var LoanDetailList = (List<TR2LoanDetail>)Helper.ExecuteService("TR2", "GetLoanDetailByForm16Id", new Form16 { ID = From16Id });
            HPTR2Model model = new HPTR2Model();
            if (LoanDetailList != null && LoanDetailList.Count() > 0)
            {
                model.EDULoanDetail = LoanDetailList.Where(m => m.LoanType == "EDU").ToList();
                model.HBALoanDetail = LoanDetailList.Where(m => m.LoanType == "HBA").ToList();
            }
            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_ShowLoanDetails.cshtml", model);
        }
        #endregion

        #region Rebate

        public ActionResult GetPopupValue(int Form16Id)
        {
            try
            {
                tRebate rebateModel = (tRebate)Helper.ExecuteService("TR2", "GetRebeteByForm16Id", new tRebate { Form16Id = Form16Id });
                return PartialView("/Areas/AccountsAdmin/Views/Rebate/_RebateAddPopup.cshtml", rebateModel);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of other firm Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        #endregion

        #region Summary Report
        public ActionResult SummaryReport()
        {
            //var StaffList = (List<mStaff>)Helper.ExecuteService("staff", "GetAllStaffList", null);
            //ViewBag.StaffList = new SelectList(StaffList, "StaffID", "NameWithDesignation");
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            ViewBag.FinancialMonthList = ModelMapping.GetMonthsList(FinYearValue);
            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_SummaryReport.cshtml");
        }

        [HttpPost]
        public JsonResult GetSummaryReport(int Month, string FinancialYear)
        {
            string Year = string.Empty;
            if (Month >= 1 && Month <= 3)
                Year = FinancialYear.Split('-')[1];
            else
                Year = FinancialYear.Split('-')[0];

            string Result = GetSummaryReportData(Month, FinancialYear, Year);

            return Json(Result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DownloadSummaryReportPdf(int Month, string FinancialYear)
        {
            try
            {
                string url = "/TR2SummaryReport/";



                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] filePaths = Directory.GetFiles(directory);
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                string Year = string.Empty;
                if (Month >= 1 && Month <= 3)
                    Year = FinancialYear.Split('-')[1];
                else
                    Year = FinancialYear.Split('-')[0];
                string Result = GetSummaryReportData(Month, FinancialYear, Year);
                string FileName = CreateSummaryReportPdf(Result, url);
                string path = "";



                path = Path.Combine(Server.MapPath(url), FileName);

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);

                    pathRes.FileDownloadName = string.Format("SummaryReport.pdf");
                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }

        public string CreateSummaryReportPdf(string Report, string Url)
        {
            try
            {
                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 20, 20), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = Report;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }



                Guid FId = Guid.NewGuid();
                string fileName = FId + "_SummaryReport.pdf";


                string directory = Server.MapPath(Url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(Server.MapPath("~" + Url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();



                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {


            }
        }
        public string GetSummaryReportData(int month, string FinancialYear, string year)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<p style='text-align:center; font-size:19px;font-family:verdana;'>H.P. Vidhan Sabha Secretariat, Shimla-46</p>");
            sb.Append("<div>");
            System.Globalization.DateTimeFormatInfo mfi = new
System.Globalization.DateTimeFormatInfo();
            string strMonthName = mfi.GetMonthName(month).ToString();

            sb.Append(string.Format("<div style='text-align:center;font-weight:bold;font-family:verdana;'>Summery of Branch Payment for the month of {0}, {1}</div><br>", strMonthName, year));
            sb.Append(string.Format("<div style='text-align:center;font-family:verdana;'>DDO: Code-039 &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; Phone No.-<b>2881128</b>, Mob. No.<b> 98168-04251</b></div><br>"));

            DataSet dataSet = new DataSet();
            var Parameter1 = new List<KeyValuePair<string, string>>();
            Parameter1.Add(new KeyValuePair<string, string>("@FinancialYear", FinancialYear));
            Parameter1.Add(new KeyValuePair<string, string>("@Month", month.ToString()));
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTR2SummaryReportDetail", Parameter1);
            sb.Append("<div style='text-align:center;margin:10px;'><center>");
            sb.Append(string.Format("<table class='table table-bordered '  cellpading='2' cellspacing='2' style='border: 1px solid black;border-collapse:collapse;font-family:verdana;'><tr><th style='border:1px solid black;border-collapse:collapse;'>Sr. No.</th>"));
            sb.Append(string.Format("<th style='border:1px solid black;border-collapse:collapse;'>Account Head</th><th style='border:1px solid black;border-collapse:collapse;'>Net Pay</th>"));
            sb.Append(string.Format("<th style='border:1px solid black;border-collapse:collapse;'>Total Employess</th></tr>"));

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                int count = 1;
                int TotalEmp = 0;
                decimal NetSalary = 0;
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    sb.Append(string.Format("<tr>"));
                    sb.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", count));
                    string rowdata = string.Empty;
                    if (dataSet.Tables[0].Rows[i][2].ToString() == "0" && dataSet.Tables[0].Rows[i][3].ToString() == "GPF")
                        rowdata = "[ G01 ] GZ Class I(R)";
                    else if (dataSet.Tables[0].Rows[i][2].ToString() == "0" && dataSet.Tables[0].Rows[i][3].ToString() == "CPF")
                        rowdata = "[ G02 ] GZ Class I(R-2)";
                    else if (dataSet.Tables[0].Rows[i][2].ToString() == "2" && dataSet.Tables[0].Rows[i][3].ToString() == "GPF")
                        rowdata = "[ N01 ] NZ Class III(R)";
                    else if (dataSet.Tables[0].Rows[i][2].ToString() == "2" && dataSet.Tables[0].Rows[i][3].ToString() == "CPF")
                        rowdata = "[ N09 ] NZ Class III(R-2)";

                    else if (dataSet.Tables[0].Rows[i][2].ToString() == "3" && dataSet.Tables[0].Rows[i][3].ToString() == "GPF")
                        rowdata = "[ N06 ] GZ Class IV(R)";
                    else if (dataSet.Tables[0].Rows[i][2].ToString() == "3" && dataSet.Tables[0].Rows[i][3].ToString() == "CPF")
                        rowdata = "[ N09 ] GZ Class IV(R-2)";

                    sb.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>01-2011-02-103-01-01-S00N-N-V-01{0}</td>", rowdata));
                    sb.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:right;'>{0}</td>", dataSet.Tables[0].Rows[i][1].ToString()));
                    sb.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:right;'>{0}</td>", dataSet.Tables[0].Rows[i][0].ToString()));
                    sb.Append(string.Format("</tr>"));
                    count++;
                    NetSalary = NetSalary + decimal.Parse(dataSet.Tables[0].Rows[i][1].ToString());
                    TotalEmp = TotalEmp + int.Parse(dataSet.Tables[0].Rows[i][0].ToString());
                }

                sb.Append(string.Format("<tr>"));
                sb.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:right;'></td>", count));
                sb.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:right;'><b>Net Salary</b></td>"));
                sb.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:right;'><b>{0}</b></td>", Math.Round(NetSalary)));
                sb.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:right;'><b>{0}</b></td>", TotalEmp));
                sb.Append(string.Format("</tr>"));

            }
            else
            {
                sb.Append(string.Format("<tr>"));
                sb.Append(string.Format("<td colspan='4'><b>No record found</b></td>"));
                sb.Append(string.Format("</tr>"));
            }
            sb.Append(string.Format("</table>"));
            sb.Append("</center></div>");
            string previousMonth = mfi.GetMonthName(month - 1).ToString();
            sb.Append(string.Format("<br><div style='text-align:center;margin:10px;font-family:verdana;'><b>Salary for the month of  {0}, {1} disbursed correctly</b></div><br><br>", previousMonth, year));
            sb.Append(string.Format("<div style='text-align:right; font-weight:bold;width:95%;font-family:verdana;'><p>Deputy Controller(F&A),<br></p><p>H.P. Vidhan Sabha</p></div></div><br> <br>"));
            sb.Append("</div>");
            return sb.ToString();
        }
        #endregion

        #region SalaryVoucherNumbers
        public ActionResult SalaryVoucherNumbersIndex()
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);

            List<SalaryVoucherNumbers> lstSalaryVoucherNumbers = (List<SalaryVoucherNumbers>)Helper.ExecuteService("Budget", "GetSalaryVoucherNumbersFinYear", FinYearValue);

            if (lstSalaryVoucherNumbers.Count() == 0)
            {
                var FinancialMonthList = ModelMapping.GetStringMonthsList(FinYearValue);
                foreach (var item in FinancialMonthList)
                {
                    SalaryVoucherNumbers objSalaryVoucherNumbers = new SalaryVoucherNumbers();
                    objSalaryVoucherNumbers.Months = item;
                    lstSalaryVoucherNumbers.Add(objSalaryVoucherNumbers);
                }

            }
            else
            {

            }

            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_SalaryVoucherNumbersIndex.cshtml", lstSalaryVoucherNumbers);
        }

        public ActionResult GetSalaryVoucherNumbersList(string FinYear)
        {
            List<SalaryVoucherNumbers> lstSalaryVoucherNumbers = (List<SalaryVoucherNumbers>)Helper.ExecuteService("Budget", "GetSalaryVoucherNumbersFinYear", FinYear);
            if (lstSalaryVoucherNumbers.Count() == 0)
            {
                var FinancialMonthList = ModelMapping.GetStringMonthsList(FinYear);
                foreach (var item in FinancialMonthList)
                {
                    SalaryVoucherNumbers objSalaryVoucherNumbers = new SalaryVoucherNumbers();
                    objSalaryVoucherNumbers.Months = item;
                    lstSalaryVoucherNumbers.Add(objSalaryVoucherNumbers);
                }

            }
            else
            {

            }

            return PartialView("/Areas/AccountsAdmin/Views/SaveTR2Form/_SalaryVoucherNumbersList.cshtml", lstSalaryVoucherNumbers);
        }

        public JsonResult SaveSalaryVoucherNumbers(FormCollection ObjFormCollection, string FinYear)
        {
            string msg = string.Empty;
            SalaryVoucherNumbers objSalaryVoucherNumbers = new SalaryVoucherNumbers();
            string[] Rows = ObjFormCollection["MonthRows"].Split(',');
            foreach (var item in Rows)
            {
                int MonthId = DateTime.ParseExact(ObjFormCollection["MonthName-" + item].Split('-')[0], "MMMM", CultureInfo.InvariantCulture).Month;
                //   string strMonthName = mfi.get

                objSalaryVoucherNumbers.FinancialYear = ObjFormCollection["FinancialYear"];
                objSalaryVoucherNumbers.Months = ObjFormCollection["MonthName-" + item];
                objSalaryVoucherNumbers.MonthId = MonthId;
                objSalaryVoucherNumbers.ClassI = ObjFormCollection["ClassI-" + item];
                objSalaryVoucherNumbers.ClassII = ObjFormCollection["ClassII-" + item];
                objSalaryVoucherNumbers.ClassIII = ObjFormCollection["ClassIII-" + item];
                objSalaryVoucherNumbers.ClassIV = ObjFormCollection["ClassIV-" + item];
                objSalaryVoucherNumbers.ClassIV_CPF = ObjFormCollection["ClassIVCPF-" + item];
                objSalaryVoucherNumbers.ClassIII_CPF = ObjFormCollection["ClassIIICPF-" + item];
                Helper.ExecuteService("Budget", "SaveSalaryVoucherNumbers", objSalaryVoucherNumbers);
            }
            msg = "Salary Voucher Numbers Update Successfully";
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public static string GetCurrentFinancialYear()
        {
            int CurrentYear = DateTime.Today.Year;
            int PreviousYear = DateTime.Today.Year - 1;
            int NextYear = DateTime.Today.Year + 1;
            string PreYear = PreviousYear.ToString();
            string NexYear = NextYear.ToString();
            string CurYear = CurrentYear.ToString();
            string FinYear = null;

            if (DateTime.Today.Month > 3)
                FinYear = CurYear + "-" + NexYear;
            else
                FinYear = PreYear + "-" + CurYear;
            return FinYear.Trim();
        }
        #endregion



    }
}
