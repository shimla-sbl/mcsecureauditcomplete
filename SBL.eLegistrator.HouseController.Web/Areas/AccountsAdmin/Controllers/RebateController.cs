﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.User;

using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.DomainModel.Models.FormTR2;
using SBL.DomainModel.Models.StaffManagement;
using SBL.DomainModel.Models.Budget;
using EvoPdf;
using System.Drawing;
using System.IO;
using iTextSharp.text;
using Ionic.Zip;
using System.Text;
namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class RebateController : Controller
    {
        //
        // GET: /AccountsAdmin/Rebate/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetPopupValue(string StaffId, string FinacialYear)
        {
            try
            {
                tRebate rebateModel = new tRebate();
                rebateModel.EmployeeID = Convert.ToInt32(StaffId);
                rebateModel.FinancialYear = FinacialYear;
                rebateModel = (tRebate)Helper.ExecuteService("TR2", "GetRebete", rebateModel);
                if (rebateModel == null)
                {
                    rebateModel = new tRebate();
                    rebateModel.EmployeeID = Convert.ToInt32(StaffId);
                    rebateModel.FinancialYear = FinacialYear;
                    rebateModel.AccountFID = 18;
                    decimal valueGPF = (decimal)Helper.ExecuteService("TR2", "GetRevateCountAmount", rebateModel);
                    rebateModel.GPF = Convert.ToDouble(valueGPF);
                    rebateModel.AccountFID = 7;
                    decimal valueGI = (decimal)Helper.ExecuteService("TR2", "GetRevateCountAmount", rebateModel);
                    rebateModel.GI = Convert.ToDouble(valueGI);
                    rebateModel.AccountFID = 34;
                    decimal ValueLic = (decimal)Helper.ExecuteService("TR2", "GetRevateCountAmount", rebateModel);
                    rebateModel.LIC = Convert.ToDouble(ValueLic);
                    rebateModel.AccountFID = 20;
                    decimal valueHBA = (decimal)Helper.ExecuteService("TR2", "GetRevateCountAmount", rebateModel);
                    rebateModel.HBALoanPrincpal = Convert.ToDouble(valueHBA);
                }
             
              
                return PartialView("/Areas/AccountsAdmin/Views/Rebate/_RebateAddPopup.cshtml", rebateModel);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of other firm Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult SaveRebate(tRebate model)
        {
            try
            {
                model.CreatedDate = DateTime.Now;
                if (model.Id > 0)
                {
                    Helper.ExecuteService("TR2", "DeleteRebate", model);
                }
                model = (tRebate)Helper.ExecuteService("TR2", "SaveRebate", model);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }
        }
    }
}
