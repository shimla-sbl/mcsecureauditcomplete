﻿using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Enums;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class MedicalTestChargeController : Controller
    {
        //
        // GET: /AccountsAdmin/MedicalTestCharge/


        public PartialViewResult MedTestChargeIndex(int pageId = 1, int pageSize = 10)
        {
            try
            {

                var MedTestChargeList = (List<mMedicalTestCharge>)Helper.ExecuteService("Budget", "GetAllMedicalTestCharge", null);

                ViewBag.PageSize = pageSize;
                List<mMedicalTestCharge> pagedRecord = new List<mMedicalTestCharge>();
                if (pageId == 1)
                {
                    pagedRecord = MedTestChargeList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = MedTestChargeList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(MedTestChargeList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_MediTestChargeIndex.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  Medical Test Charge Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 10)
        {
            var MedTestChargeList = (List<mMedicalTestCharge>)Helper.ExecuteService("Budget", "GetAllMedicalTestCharge", null);

            ViewBag.PageSize = pageSize;
            List<mMedicalTestCharge> pagedRecord = new List<mMedicalTestCharge>();
            if (pageId == 1)
            {
                pagedRecord = MedTestChargeList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedTestChargeList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedTestChargeList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_MedicalTestChargeList.cshtml", pagedRecord);
        }


        public PartialViewResult PopulateMedTestCharge(int Id, string Action, int pageId = 1, int pageSize = 10)
        {
            mMedicalTestCharge model = new mMedicalTestCharge();

            ViewBag.MedTestChargeType = new SelectList(Enum.GetValues(typeof(WardType)).Cast<WardType>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");

            var stateToEdit = (mMedicalTestCharge)Helper.ExecuteService("Budget", "GetMedicalTestChargeById", new mMedicalTestCharge { TestChargeID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit;

            model.Mode = Action;
            ViewBag.Add = Action;
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;
            return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_PopulateMedicalTestCharge.cshtml", model);
        }

        public PartialViewResult DeleteMedTestCharge(int Id, int pageId = 1, int pageSize = 10)
        {

            Helper.ExecuteService("Budget", "DeleteMedicalTestCharge", new mMedicalTestCharge { TestChargeID = Id });
            ViewBag.Msg = "Sucessfully deleted Medical Test";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            var MedTestChargeList = (List<mMedicalTestCharge>)Helper.ExecuteService("Budget", "GetAllMedicalTestCharge", null);

            ViewBag.PageSize = pageSize;
            List<mMedicalTestCharge> pagedRecord = new List<mMedicalTestCharge>();
            if (pageId == 1)
            {
                pagedRecord = MedTestChargeList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedTestChargeList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedTestChargeList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_MedicalTestChargeList.cshtml", pagedRecord);
        }

        [HttpPost]
        public ActionResult SaveMedTestCharge(FormCollection collection, int pageId = 1, int pageSize = 10)
        {

            try
            {
                string PCRow = collection["MedTestChargeNameNumRow"];
                if (PCRow != null && PCRow.Length > 0)
                {
                    string[] RowCount = PCRow.Split(',');

                    foreach (var val in RowCount)
                    {
                        mMedicalTestCharge model = new mMedicalTestCharge();
                        model.TestName = collection["TestChargeName-" + val];
                       
                        model.IsActive = true;
                        model.Category = int.Parse(collection["Category"]);
                        if (!string.IsNullOrEmpty(model.TestName) && !string.IsNullOrEmpty(collection["Price-" + val]))
                        {
                            model.TestPrice = Convert.ToDecimal(collection["Price-" + val]);
                            Helper.ExecuteService("Budget", "CreateMedicalTestCharge", model);
                        }
                        ViewBag.Msg = "Sucessfully added Test Charge";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }


            var MedTestChargeList = (List<mMedicalTestCharge>)Helper.ExecuteService("Budget", "GetAllMedicalTestCharge", null);

            ViewBag.PageSize = pageSize;
            List<mMedicalTestCharge> pagedRecord = new List<mMedicalTestCharge>();
            if (pageId == 1)
            {
                pagedRecord = MedTestChargeList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedTestChargeList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedTestChargeList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_MedicalTestChargeList.cshtml", pagedRecord);
        }
        #region Search MedTestCharge
        public PartialViewResult SearchMedTestCharge()
        {
            try
            {
                return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_SearchMedicalTestCharge.cshtml");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  Medical Search Test Charge Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }



        public ActionResult GetAutoCompleteData(string SearchWord)
        {
            var MedTestChargeList = ((List<mMedicalTestCharge>)Helper.ExecuteService("Budget", "SearchMedicalTestCharge", SearchWord)).Where(m => m.IsActive == true).ToList();
            return Json(MedTestChargeList, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult SearchMedTestChargeData(string SearchWord, int pageId = 1, int pageSize = 20)
        {
            try
            {

                var MedTestChargeList = ((List<mMedicalTestCharge>)Helper.ExecuteService("Budget", "SearchMedicalTestCharge", SearchWord)).Where(m => m.IsActive == true).ToList();

                Session["SearchMedTestChargeList"] = MedTestChargeList;
                ViewBag.PageSize = pageSize;
                List<mMedicalTestCharge> pagedRecord = new List<mMedicalTestCharge>();
                if (pageId == 1)
                {
                    pagedRecord = MedTestChargeList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = MedTestChargeList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(MedTestChargeList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_SearchMedicalTestChargeList.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  Medical Test Charge Search List Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetSearchMedTestChargeByPaging(string SearchWord, int pageId = 1, int pageSize = 10)
        {
            List<mMedicalTestCharge> MedTestChargeList = new List<mMedicalTestCharge>();
            if (Session["SearchMedTestChargeList"] != null)
                MedTestChargeList = (List<mMedicalTestCharge>)Session["SearchMedTestChargeList"];
            else
                MedTestChargeList = ((List<mMedicalTestCharge>)Helper.ExecuteService("Budget", "SearchMedicalTestCharge", SearchWord)).Where(m => m.IsActive == true).ToList();


            ViewBag.PageSize = pageSize;
            List<mMedicalTestCharge> pagedRecord = new List<mMedicalTestCharge>();
            if (pageId == 1)
            {
                pagedRecord = MedTestChargeList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedTestChargeList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedTestChargeList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_SearchMedicalTestChargeList.cshtml", pagedRecord);
        }
        #endregion


        #region Update MedTestCharge
        public PartialViewResult UpdateMedTestCharge()
        {
            try
            {
                return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_UpdateMedicalTestCharge.cshtml");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  update medical test charges Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult SearchUpdateMedTestChargeData(string SearchWord, int pageId = 1, int pageSize = 20)
        {
            try
            {

                var MedTestChargeList = (List<mMedicalTestCharge>)Helper.ExecuteService("Budget", "SearchMedicalTestCharge", SearchWord);

                Session["SearchMedTestChargeList"] = MedTestChargeList;
                ViewBag.PageSize = pageSize;
                List<mMedicalTestCharge> pagedRecord = new List<mMedicalTestCharge>();
                if (pageId == 1)
                {
                    pagedRecord = MedTestChargeList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = MedTestChargeList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(MedTestChargeList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_UpdateMedicalTestChargeList.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  MedTestCharge Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetUpdateMedTestChargeByPaging(string SearchWord, int pageId = 1, int pageSize = 10)
        {
            List<mMedicalTestCharge> MedTestChargeList = new List<mMedicalTestCharge>();
            if (Session["UpdateMedTestChargeList"] != null)
                MedTestChargeList = (List<mMedicalTestCharge>)Session["UpdateMedTestChargeList"];
            else
                MedTestChargeList = (List<mMedicalTestCharge>)Helper.ExecuteService("Budget", "SearchMedicalTestCharge", SearchWord);


            ViewBag.PageSize = pageSize;
            List<mMedicalTestCharge> pagedRecord = new List<mMedicalTestCharge>();
            if (pageId == 1)
            {
                pagedRecord = MedTestChargeList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedTestChargeList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedTestChargeList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_UpdateMedicalTestChargeList.cshtml", pagedRecord);
        }

        public ActionResult GetUpdateAutoCompleteData(string SearchWord)
        {
            var MedTestChargeList = (List<mMedicalTestCharge>)Helper.ExecuteService("Budget", "SearchMedicalTestCharge", SearchWord);
            return Json(MedTestChargeList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateMedTestCharge(FormCollection collection, int pageId = 1, int pageSize = 20)
        {

            try
            {
                string PCRow = collection["RowCount"];
                if (PCRow != null && PCRow.Length > 0)
                {
                    string[] RowCount = PCRow.Split(',');

                    foreach (var val in RowCount)
                    {
                        int MedTestChargeID = int.Parse(collection["MedTestChargeID-" + val]);
                        var stateToEdit = (mMedicalTestCharge)Helper.ExecuteService("Budget", "GetMedicalTestChargeById", new mMedicalTestCharge { TestChargeID = MedTestChargeID });
                        string Checked = collection["IsActive-" + val];

                        if (Checked == "on")
                            stateToEdit.IsActive = true;
                        else
                            stateToEdit.IsActive = false;

                        Helper.ExecuteService("Budget", "UpdateMedicalTestCharge", stateToEdit);
                        ViewBag.Msg = "Sucessfully updated MedTestCharge";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            string SearchWord = collection["appendedInputButtons"];

            var MedTestChargeList = (List<mMedicalTestCharge>)Helper.ExecuteService("Budget", "SearchMedicalTestCharge", SearchWord);

            ViewBag.PageSize = pageSize;
            List<mMedicalTestCharge> pagedRecord = new List<mMedicalTestCharge>();
            if (pageId == 1)
            {
                pagedRecord = MedTestChargeList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedTestChargeList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedTestChargeList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/MedicalTestCharge/_UpdateMedicalTestChargeList.cshtml", pagedRecord);
        }
        #endregion


    }
}
