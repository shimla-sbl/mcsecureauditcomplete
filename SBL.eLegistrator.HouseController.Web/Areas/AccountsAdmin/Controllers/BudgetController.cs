﻿using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class BudgetController : Controller
    {
        //
        // GET: /AccountsAdmin/Budget/
        #region Budget

        public PartialViewResult BudgetIndex()
        {
            try
            {


                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;

                // var State = ((List<mBudget>)Helper.ExecuteService("Budget", "GetBudgetByFY", FinYearValue)).OrderBy(m => m.OrderBy).ToList();
                var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget { FinancialYear = FinYearValue });
                if (TempData["Msg"] != null)
                {
                    ViewBag.Msg = TempData["Msg"].ToString();
                    ViewBag.MsgCss = TempData["MsgCss"].ToString();
                    ViewBag.Notification = TempData["Notification"].ToString();
                }

                var model = State.ToViewModel();
                return PartialView("/Areas/AccountsAdmin/Views/Budget/_BudgetIndex.cshtml", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult PopulateBudget(int Id, string Action)
        {
            BudgetViewModel model = new BudgetViewModel();


            if (Id > 0)
            {
                mBudget stateToEdit = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = Id });
                if (stateToEdit != null)
                    model = stateToEdit.ToViewModel1(Action);
            }

            model.Mode = Action;
            model.GazatedList = ModelMapping.GetGazatedList();
            model.VotedList = ModelMapping.GetVotedList();
            model.PlanList = ModelMapping.GetPlanList();
            model.PlanList = ModelMapping.GetPlanList();
            var BudgetTypeList = (List<mBudgetType>)Helper.ExecuteService("Budget", "GetAllBudgetType", null);
            model.BillTypeList = new SelectList(BudgetTypeList, "BudgetTypeID", "TypeName");

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            model.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);


            return PartialView("/Areas/AccountsAdmin/Views/Budget/_CreateBudget.cshtml", model);
        }

        public PartialViewResult DeleteBudget(int Id)
        {
            mBudget stateToDelete = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = Id });

            Helper.ExecuteService("Budget", "DeleteBudget", stateToDelete);
            ViewBag.Msg = "Sucessfully Deleted";
            ViewBag.MsgCss = "alert-info";
            ViewBag.Notification = "Success";

            //   var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", null);

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget { FinancialYear = FinYearValue });

            //   var State = ((List<mBudget>)Helper.ExecuteService("Budget", "GetBudgetByFY", FinYearValue)).OrderBy(m => m.OrderBy).ToList();
            var result = State.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/Budget/_BudgetListIndex.cshtml", result);
        }

        [HttpPost]
        public PartialViewResult SaveBudget(BudgetViewModel model, string EnableDisplay)
        {
            switch (model.Gazetted)
            {
                case "SPK/DSPK":
                    model.BudgetHeadOrder = 1;
                    break;
                case "Member":
                    model.BudgetHeadOrder = 2;
                    break;
                case "Gazetted":
                    model.BudgetHeadOrder = 3;
                    break;
                case "Gazetted/NonGazetted":
                    model.BudgetHeadOrder = 4;
                    break;
                case "NA":
                    model.BudgetHeadOrder = 5;
                    break;
                case "Other":
                    model.BudgetHeadOrder = 6;
                    break;

            }



            if (Request.Form["IsShowExpenseReport"] == "on")
                model.IsShowExpenseReport = true;
            var mBudget = model.ToDomainModel();

            if (mBudget != null && mBudget.FinancialYear.Length > 0)
            {
                string FinacialYear = mBudget.FinancialYear;
                DateTime FYFrom = new DateTime(Convert.ToInt32(FinacialYear.Split('-')[0]), 4, 30);
                DateTime FYTo = new DateTime(Convert.ToInt32(FinacialYear.Split('-')[1]), 3, 31);

                mBudget.FYFromDate = FYFrom;
                mBudget.FYToDate = FYTo;
                mBudget.OrderBy = model.OrderBy;
                mBudget.DisplayInReport = true;
                mBudget.BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}({5}) {6}-{7}-{8}", mBudget.MajorHead, mBudget.SubMajorHead, mBudget.MinorHead, mBudget.SubHead, mBudget.Gazetted, mBudget.VotedCharged, mBudget.Plan, mBudget.ObjectCode.ToString(), mBudget.ObjectCodeText);
                mBudget state = (mBudget)Helper.ExecuteService("Budget", "GetBudgetByBudgetName", new mBudget { BudgetID = mBudget.BudgetID, BudgetName = mBudget.BudgetName,FinancialYear=mBudget.FinancialYear });
                if (state == null)
                {
                    if (model.Mode == "Add")
                    {
                        //  mBudget.DisplayInReport = true;
                        Helper.ExecuteService("Budget", "CreateBudget", mBudget);
                        ViewBag.Msg = "Sucessfully added Budget";
                        ViewBag.MsgCss = "alert-info";
                        ViewBag.Notification = "Success";

                    }
                    else
                    {
                        Helper.ExecuteService("Budget", "UpdateBudget", mBudget);
                        ViewBag.Msg = "Sucessfully updated Budget";
                        ViewBag.MsgCss = "alert-info";
                        ViewBag.Notification = "Success";
                    }
                }
                else
                {
                    ViewBag.Msg = "This budget head already exist in our database.";
                    ViewBag.MsgCss = "alert-danger";
                    ViewBag.Notification = "Error";

                }
            }
            else
            {
                ViewBag.Msg = "Please Select Financial Year";
                ViewBag.MsgCss = "alert-warning";
                ViewBag.Notification = "Error";
            }

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget { FinancialYear = FinYearValue });
            // var State = ((List<mBudget>)Helper.ExecuteService("Budget", "GetBudgetByFY", FinYearValue)).OrderBy(m => m.OrderBy).ToList();

            var result = State.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/Budget/_BudgetListIndex.cshtml", result);

        }

        public PartialViewResult AllocateFundIndex()
        {
            try
            {
                // This is for showing selected Current Financial Year
                //int CurrentYear = DateTime.Today.Year;
                //int PreviousYear = DateTime.Today.Year - 1;
                //int NextYear = DateTime.Today.Year + 1;
                //string PreYear = PreviousYear.ToString();
                //string NexYear = NextYear.ToString();
                //string CurYear = CurrentYear.ToString();
                //string FinYearValue = null;

                //if (DateTime.Today.Month > 3)
                //    FinYearValue = string.Format("{0}-{1}", CurYear, NexYear);
                //else
                //    FinYearValue = string.Format("{0}-{1}", PreYear, CurYear);

                //// Get Current Financial Year Date
                //DateTime startDate = new DateTime(Convert.ToInt32(FinYearValue.Split('-')[0]), 4, 30);
                //DateTime endDate = new DateTime(Convert.ToInt32(FinYearValue.Split('-')[1]), 3, 31);

                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;


                var State = (List<AllocateBudget>)Helper.ExecuteService("Budget", "GetAllocateBudget", FinYearValue);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();
                var model = State.ToViewModel();
                // For populate FInacial Year
                ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
                ViewBag.FinYear = FinYearValue;

                return PartialView("/Areas/AccountsAdmin/Views/Budget/_AllocateBudgetIndex.cshtml", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        /// <summary>
        /// this is for saving all budget in single(Aloocating fund)
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public PartialViewResult SaveAllocateBudget(FormCollection collection)
        {
            List<AllocateBudgetViewModel> model = new List<AllocateBudgetViewModel>();

            string PCRow = collection["RowCount"];
            string FinancialYear = collection["FinacialYear"];


            if (PCRow != null && PCRow.Length > 0)
            {
                string[] RowCount = PCRow.Split(',');

                foreach (var val in RowCount)
                {

                    try
                    {
                        int BudgetId = 0;
                        int.TryParse(collection["BudgetId-" + val], out BudgetId);
                        int AllocateBudgetID = 0;
                        int.TryParse(collection["AllocateBudgetID-" + val], out AllocateBudgetID);
                        var Todate = collection["AllocateBudgetDate-" + val];
                        decimal Sanctionedbudget = 0;
                        decimal.TryParse(collection["Sanctionedbudget-" + val], out Sanctionedbudget);

                        if (BudgetId > 0)
                        {
                            if (Todate != "")
                            {

                                DateTime AllocationDate = new DateTime(Convert.ToInt32(Todate.Split('/')[2]), Convert.ToInt32(Todate.Split('/')[1]), Convert.ToInt32(Todate.Split('/')[0]));

                                if (AllocateBudgetID > 0)
                                {
                                    if (Sanctionedbudget > 0)
                                    {
                                        AllocateBudget stateToEdit = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetById", new AllocateBudget { AllocateBudgetID = AllocateBudgetID });
                                        stateToEdit.Sanctionedbudget = Sanctionedbudget;
                                        stateToEdit.AllocateBudgetDate = AllocationDate;
                                        Helper.ExecuteService("Budget", "UpdateAllocateBudget", stateToEdit);
                                        ViewBag.Msg = "Budget allocated sucessfully ";
                                        ViewBag.Class = "alert alert-success";
                                        ViewBag.Notification = "Success";
                                    }
                                    else
                                    {
                                        ViewBag.Msg = "Please fill Sanctioned budget properly ";
                                        ViewBag.Class = "alert alert-error";
                                        ViewBag.Notification = "Error";
                                    }
                                }
                                else
                                {
                                    if (Sanctionedbudget > 0)
                                    {
                                        AllocateBudgetViewModel ABudget = new AllocateBudgetViewModel();
                                        ABudget.Sanctionedbudget = Sanctionedbudget;
                                        ABudget.AllocateBudgetDate = AllocationDate;
                                        ABudget.BudgetId = BudgetId;
                                        Helper.ExecuteService("Budget", "AddAllocateBudget", ABudget.ToDomainModel());

                                        ViewBag.Msg = "Budget allocated sucessfully ";
                                        ViewBag.Class = "alert alert-success";
                                        ViewBag.Notification = "Success";
                                    }
                                    else
                                    {
                                        ViewBag.Msg = "Please fill Sanctioned budget properly ";
                                        ViewBag.Class = "alert alert-error";
                                        ViewBag.Notification = "Error";
                                    }
                                }
                            }


                            var State = (List<AllocateBudget>)Helper.ExecuteService("Budget", "GetAllocateBudget", FinancialYear);
                            model = State.ToViewModel();
                        }
                        else
                        {
                            ViewBag.Msg = "Unable to Budget allocated ";
                            ViewBag.Class = "alert alert-error";
                            ViewBag.Notification = "Error";
                        }

                    }
                    catch (Exception ex)
                    {

                        ViewBag.Msg = ex.ToString();
                        ViewBag.Class = "alert alert-error";
                        ViewBag.Notification = "Error";
                    }
                }
            }
            else
            {
                ViewBag.Msg = "Unable to recived data";
                ViewBag.Class = "alert alert-warning";
                ViewBag.Notification = "Error";

            }
            return PartialView("/Areas/AccountsAdmin/Views/Budget/_AllBudgetList.cshtml", model);


        }

        public ActionResult AddBudget()
        {
            BudgetViewModel model = new BudgetViewModel();
            model.GazatedList = ModelMapping.GetGazatedList();
            model.VotedList = ModelMapping.GetVotedList();
            model.PlanList = ModelMapping.GetPlanList();
            // This is for showing selected Current Financial Year
            int CurrentYear = DateTime.Today.Year;
            int PreviousYear = DateTime.Today.Year - 1;
            int NextYear = DateTime.Today.Year + 1;
            string PreYear = PreviousYear.ToString();
            string NexYear = NextYear.ToString();
            string CurYear = CurrentYear.ToString();
            string FinYearValue = null;

            if (DateTime.Today.Month > 3)
                FinYearValue = string.Format("{0}-{1}", CurYear, NexYear);
            else
                FinYearValue = string.Format("{0}-{1}", PreYear, CurYear);


            model.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            model.Mode = "Add";
            return View(model);
        }

        public PartialViewResult SearchAllocatedBudget(string FinacialYear)
        {
            var State = (List<AllocateBudget>)Helper.ExecuteService("Budget", "GetAllocateBudget", FinacialYear);
            var model = State.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/Budget/_AllBudgetList.cshtml", model);
        }

        public ActionResult ShowBudgetInReport(int BudgetId, bool ShowInReport)
        {
            try
            {
                var Budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = BudgetId });
                Budget.DisplayInReport = ShowInReport;
                Helper.ExecuteService("Budget", "UpdateBudget", Budget);

                TempData["Msg"] = "Sucessfully update Budget";
                TempData["MsgCss"] = "alert-info";
                TempData["Notification"] = "Success";
            }
            catch (Exception ex)
            {

                TempData["Msg"] = ex.InnerException.ToString();
                TempData["MsgCss"] = "alert-warning";
                TempData["Notification"] = "Error";
            }


            return RedirectToAction("Index");
        }
        // this is for  alocating fund one by one
        public PartialViewResult UpdateAllocateBudget(int BudgetId, int AllocateBudgetID, decimal Sanctionedbudget, string FinancialYear, string AllocateBudgetDate)
        {
            List<AllocateBudgetViewModel> model = new List<AllocateBudgetViewModel>();


            if (BudgetId > 0)
            {
                if (AllocateBudgetDate != "")
                {

                    DateTime AllocationDate = new DateTime(Convert.ToInt32(AllocateBudgetDate.Split('/')[2]), Convert.ToInt32(AllocateBudgetDate.Split('/')[1]), Convert.ToInt32(AllocateBudgetDate.Split('/')[0]));

                    if (AllocateBudgetID > 0)
                    {
                        if (Sanctionedbudget > 0)
                        {
                            AllocateBudget stateToEdit = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetById", new AllocateBudget { AllocateBudgetID = AllocateBudgetID });
                            stateToEdit.Sanctionedbudget = Sanctionedbudget;
                            stateToEdit.AllocateBudgetDate = AllocationDate;
                            Helper.ExecuteService("Budget", "UpdateAllocateBudget", stateToEdit);
                            ViewBag.Msg = "Budget allocated sucessfully ";
                            ViewBag.Class = "alert alert-success";
                            ViewBag.Notification = "Success";
                        }
                        else
                        {
                            ViewBag.Msg = "Please fill Sanctioned budget properly ";
                            ViewBag.Class = "alert alert-error";
                            ViewBag.Notification = "Error";
                        }
                    }
                    else
                    {
                        if (Sanctionedbudget > 0)
                        {
                            AllocateBudgetViewModel ABudget = new AllocateBudgetViewModel();
                            ABudget.Sanctionedbudget = Sanctionedbudget;
                            ABudget.AllocateBudgetDate = AllocationDate;
                            ABudget.BudgetId = BudgetId;
                            Helper.ExecuteService("Budget", "AddAllocateBudget", ABudget.ToDomainModel());

                            ViewBag.Msg = "Budget allocated sucessfully ";
                            ViewBag.Class = "alert alert-success";
                            ViewBag.Notification = "Success";
                        }
                        else
                        {
                            ViewBag.Msg = "Please fill Sanctioned budget properly ";
                            ViewBag.Class = "alert alert-error";
                            ViewBag.Notification = "Error";
                        }
                    }
                }

                var State = (List<AllocateBudget>)Helper.ExecuteService("Budget", "GetAllocateBudget", FinancialYear);
                model = State.ToViewModel();
            }
            else
            {
                ViewBag.Msg = "Sorry! some technical problem occur ";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
            }


            return PartialView("/Areas/AccountsAdmin/Views/Budget/_AllBudgetList.cshtml", model);


        }


        [HttpPost]
        public ActionResult DownloadExcel(string FinYear)
        {
            var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetBudgetWithAllocateFund", FinYear);
            string Result = this.GetAllocateBudgetListData(State, FinYear);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=AllocatedBudgetList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }


        public string GetAllocateBudgetListData(List<mBudget> model, string FinYear)
        {

            StringBuilder ReplyList = new StringBuilder();

            if (model != null && model.Count() > 0)
            {
                ReplyList.Append(string.Format("<div style='text-align:center;'><h2>SANCTIONED BUDGET FOR FINANCIAL YEAR {0}</h2></div>", FinYear));

                ReplyList.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                ReplyList.Append("<thead class='header' ><tr style='background-color: #428bca ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:30px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px;'>SR. NO.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >BUDGET HEAD PART1</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >OBJECT</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >S.O.E.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >SANCTIONED BUDGET</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >ADDITION BUDGET</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >TOTAL BUDGET</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >ALLOCATED BUDGET</th>");
                ReplyList.Append("</tr></thead>");


                ReplyList.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    ReplyList.Append("<tr>");
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", count));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}-{1}-{2}-{3}-{4} ({5}){6}</td>", obj.MajorHead, obj.SubMajorHead, obj.MinorHead, obj.SubHead, obj.Gazetted, obj.VotedCharged, obj.Plan));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.Object));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}-{1}</td>", obj.ObjectCode, obj.ObjectCodeText));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'></td>"));
                    decimal AdditionFund = (decimal)Helper.ExecuteService("Budget", "GetTotalAdditionById", new AdditionFunds { BudgetId = obj.BudgetID });
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", AdditionFund));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'></td>"));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.AllocateFund));
                    ReplyList.Append("</tr>");
                }
                ReplyList.Append("</tbody></table></div> ");
            }
            else
            {
                ReplyList.Append("No Allocated Budget Found");
            }
            return ReplyList.ToString();
        }

        public JsonResult GetBudgetCode(string Year)
        {

            mBudget model = new mBudget();
            model.FinancialYear = Year;
            model.BudgetCode = Helper.ExecuteService("Budget", "GetBCodebyFYear", model) as string;

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Addional Fund
        public PartialViewResult CreatemAdditionFund(int Id, string Action, string BudgetId)
        {
            AdditionFundsViewModel model = new AdditionFundsViewModel();
            AdditionFunds stateToEdit = (AdditionFunds)Helper.ExecuteService("Budget", "GetAdditionFundById", new AdditionFunds { AdditionFundsID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit.ToViewModel1(Action);

            model.Mode = Action;
            model.FundTypeList = ModelMapping.GetFundType();
            ViewBag.BudgetId = BudgetId;
            AllocateBudget AllocateBudget = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = int.Parse(BudgetId) });
            ViewData["SactionedBudget"] = AllocateBudget.Sanctionedbudget;
            mBudget budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = int.Parse(BudgetId) });
            ViewBag.BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}", budget.MajorHead, budget.SubHead, budget.MinorHead, budget.SubHead, budget.Gazetted, budget.VotedCharged, budget.Plan, budget.ObjectCode.ToString(), budget.ObjectCodeText);
            return PartialView("/Areas/AccountsAdmin/Views/Budget/_AdditionFund.cshtml", model);
        }

        public PartialViewResult AdditionFund(string BudgetId)
        {
            List<AdditionFundsViewModel> ModelList = new List<AdditionFundsViewModel>();
            var State = (List<AdditionFunds>)Helper.ExecuteService("Budget", "GetAllAdditionFund", BudgetId);
            ModelList = State.ToViewModel();
            ViewBag.BudgetId = BudgetId;
            mBudget budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = int.Parse(BudgetId) });
            AllocateBudget AllocateBudget = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = int.Parse(BudgetId) });
            ViewData["SactionedBudget"] = AllocateBudget.Sanctionedbudget;


            ViewBag.BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}", budget.MajorHead, budget.SubHead, budget.MinorHead, budget.SubHead, budget.Gazetted, budget.VotedCharged, budget.Plan, budget.ObjectCode.ToString(), budget.ObjectCodeText);

            return PartialView("/Areas/AccountsAdmin/Views/Budget/_AdditionBudgetList.cshtml", ModelList);
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult SaveAdditonFund(AdditionFundsViewModel model, int BudgetId)
        {
            List<AdditionFundsViewModel> ModelList = new List<AdditionFundsViewModel>();

            try
            {

                if (ModelState.IsValid)
                {

                    var AdditionFund = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        if (model.FundType == "Suplementary")
                        {
                            var Data = (List<AdditionFunds>)Helper.ExecuteService("Budget", "GetAllSuplementaryFund", model.BudgetId.ToString());
                            if (Data.Count() == 0)
                            {
                                model.BudgetId = BudgetId;
                                Helper.ExecuteService("Budget", "AddAdditionFund", AdditionFund);
                                ViewBag.Msg = "Fund saved sucessfully ";
                                ViewBag.Class = "alert alert-success";
                                ViewBag.Notification = "Success";
                            }
                            else
                            {
                                ViewBag.Msg = "Sorry..... You have already added Suplementry Fund";
                                ViewBag.Class = "alert alert-danger";
                                ViewBag.Notification = "Error";
                            }
                        }
                        else
                        {
                            model.BudgetId = BudgetId;
                            Helper.ExecuteService("Budget", "AddAdditionFund", AdditionFund);
                            ViewBag.Msg = "Fund saved sucessfully ";
                            ViewBag.Class = "alert alert-success";
                            ViewBag.Notification = "Success";
                        }
                    }
                    else
                    {
                        Helper.ExecuteService("Budget", "UpdateAdditionFund", AdditionFund);
                        ViewBag.Msg = "Fund saved sucessfully ";
                        ViewBag.Class = "alert alert-success";
                        ViewBag.Notification = "Success";
                    }
                    ViewBag.BudgetId = BudgetId;
                    mBudget budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = BudgetId });
                    ViewBag.BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}", budget.MajorHead, budget.SubHead, budget.MinorHead, budget.SubHead, budget.Gazetted, budget.VotedCharged, budget.Plan, budget.ObjectCode.ToString(), budget.ObjectCodeText);
                    AllocateBudget AllocateBudget = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = BudgetId });
                    ViewData["SactionedBudget"] = AllocateBudget.Sanctionedbudget;
                    var State = (List<AdditionFunds>)Helper.ExecuteService("Budget", "GetAllAdditionFund", model.BudgetId.ToString());
                    ModelList = State.ToViewModel();
                }
                else
                {
                    ViewBag.Msg = "Unable to save fund ";
                    ViewBag.Class = "alert alert-danger";
                    ViewBag.Notification = "Error";
                }
            }
            catch (Exception ex)
            {

                ViewBag.Msg = ex.InnerException;
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }

            return PartialView("/Areas/AccountsAdmin/Views/Budget/_AdditionBudgetList.cshtml", ModelList);
        }

        public PartialViewResult DeleteAdditionFund(int Id = 0)
        {
            List<AdditionFundsViewModel> ModelList = new List<AdditionFundsViewModel>();
            string BudgetId = string.Empty;
            if (Id > 0)
            {
                AdditionFunds stateToDelete = (AdditionFunds)Helper.ExecuteService("Budget", "GetAdditionFundById", new AdditionFunds { AdditionFundsID = Id });
                BudgetId = stateToDelete.BudgetId.ToString();
                Helper.ExecuteService("Budget", "DeleteAdditionFund", stateToDelete);
                ViewBag.Msg = "Addition Fund deleted sucessfully";
                ViewBag.Class = "alert alert-success";
                ViewBag.Notification = "Success";
            }
            else
            {
                ViewBag.Msg = "Unable to delete";
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }
            var State = (List<AdditionFunds>)Helper.ExecuteService("Budget", "GetAllAdditionFund", BudgetId);

            ViewBag.BudgetId = BudgetId;
            mBudget budget = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = int.Parse(BudgetId) });
            ViewBag.BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}", budget.MajorHead, budget.SubHead, budget.MinorHead, budget.SubHead, budget.Gazetted, budget.VotedCharged, budget.Plan, budget.ObjectCode.ToString(), budget.ObjectCodeText);
            AllocateBudget AllocateBudget = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = int.Parse(BudgetId) });
            ViewData["SactionedBudget"] = AllocateBudget.Sanctionedbudget;

            ModelList = State.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/Budget/_AdditionBudgetList.cshtml", ModelList);
        }
        #endregion

    }
}
