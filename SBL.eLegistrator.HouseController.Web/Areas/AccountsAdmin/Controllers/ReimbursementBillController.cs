﻿using Email.API;
using EvoPdf;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.StaffManagement;
using SBL.DomainModel.Models.UploadImages;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SMS.API;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class ReimbursementBillController : Controller
    {


        // GET: /AccountsAdmin/ReimbursementBill/

        #region Reimbursement Bill

        public PartialViewResult ReimbursementBillIndex(int pageId = 1, int pageSize = 10)
        {
            try
            {
                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;

                ViewBag.Layout = 1;
                List<ReimbursementBillViewModel> model = new List<ReimbursementBillViewModel>();

                var BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { FinancialYear = FinYearValue, CreatedBy = CurrentSession.UserName })).Where(m => (m.IsChallanGenerate == false || m.IsRejected == true) && m.IsCanceled == false).ToList();
                model = BillList.ToViewModel();

                ViewBag.PageSize = pageSize;
                List<ReimbursementBillViewModel> pagedRecord = new List<ReimbursementBillViewModel>();
                if (pageId == 1)
                {
                    pagedRecord = model.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = model.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();

                Session["EstablishBillID"] = null;

                return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_ReimbursementBillIndex.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 10)
        {
            List<mReimbursementBill> BillList = new List<mReimbursementBill>();
            int EstablishId = 0;
            if (Session["EstablishBillID"] != null)
                int.TryParse(Session["EstablishBillID"].ToString(), out EstablishId);
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            if (EstablishId > 0)
                BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { FinancialYear = FinYearValue, CreatedBy = CurrentSession.UserName })).Where(m => (m.IsChallanGenerate == false || m.IsRejected == true) && m.IsCanceled == false).Where(m => m.EstblishBillNumber == EstablishId).ToList();
            else
                BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { FinancialYear = FinYearValue, CreatedBy = CurrentSession.UserName })).Where(m => (m.IsChallanGenerate == false || m.IsRejected == true) && m.IsCanceled == false).ToList();

            ViewBag.PageSize = pageSize;
            List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_ReimbursementBillList.cshtml", model);
        }


        public PartialViewResult PopulateReimbursementBill(int Id, string Action, int pageId = 1, int pageSize = 10)
        {
            ReimbursementBillViewModel model = new ReimbursementBillViewModel();

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            mReimbursementBill stateToEdit = (mReimbursementBill)Helper.ExecuteService("Budget", "GetReimbursementBillById", new mReimbursementBill { ReimbursementBillID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit.ToViewModel1(Action);

            model.Mode = Action;

            var BillTypeList = (List<mBudgetBillTypes>)Helper.ExecuteService("Budget", "GetAllBillType", null);
            model.BillTypeList = new SelectList(BillTypeList, "BillTypeID", "TypeName");




            model.DesignationList = new SelectList(Enum.GetValues(typeof(DesignationType)).Cast<DesignationType>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");


            if (model.Mode == "Edit")
            {
                ViewBag.Add = "Edit";
                mEstablishBill EditEstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = model.EstablishBillId });
                ViewBag.BillNumber = EditEstablishBill.BillNumber;
                ViewBag.GrossAmount = EditEstablishBill.TotalGrossAmount;
                ViewBag.EstablishId = EditEstablishBill.EstablishBillID;

                var DesignationType = ModelMapping.GetDescription((SBL.DomainModel.Models.Enums.DesignationType)model.Designation);
                if (DesignationType == "ExMember")
                {
                    var MemberList = (List<SBL.DomainModel.Models.Member.mMember>)Helper.ExecuteService("MemberAccountDetails", "GetExMembersDetails", null);
                    model.MemberList = new SelectList(MemberList, "MemberCode", "Name");

                    var NomineeList = (List<mMemberNominee>)Helper.ExecuteService("MemberAccountDetails", "GetNomineeList", new mMemberNominee { MemberCode = model.ClaimantId });
                    model.NomineeList = new SelectList(NomineeList, "NomineeId", "NomineeName");
                }
                else if (DesignationType == "Member")
                {
                    var MemberList = (List<mMemberAccountsDetails>)Helper.ExecuteService("MemberAccountDetails", "GetCurrentMembersDetails", null);
                    model.MemberList = new SelectList(MemberList, "MemberCode", "MemberName");
                }
                else if (DesignationType == "Speaker")
                {
                    var MemberList = (List<mMember>)Helper.ExecuteService("MemberAccountDetails", "GetAllSpeaker", null);
                    model.MemberList = new SelectList(MemberList, "MemberCode", "Name");
                }
                else if (DesignationType == "DeputySpeaker")
                {
                    var MemberList = (List<mMember>)Helper.ExecuteService("MemberAccountDetails", "GetAllDeputySpeaker", null);
                    model.MemberList = new SelectList(MemberList, "MemberCode", "Name");
                }
                else if (DesignationType == "GazettedStaff")
                {

                    var MemberList = (List<mStaff>)Helper.ExecuteService("staff", "GetStaffListByGroup", new mStaff { Group = "Gazetted" });
                    model.MemberList = new SelectList(MemberList, "StaffID", "NameWithDesignation");
                }
                else if (DesignationType == "NonGazettedStaff")
                {
                    var MemberList = (List<mStaff>)Helper.ExecuteService("staff", "GetStaffListByGroup", new mStaff { Group = "Non-Gazetted" });
                    model.MemberList = new SelectList(MemberList, "StaffID", "NameWithDesignation");
                }
                else if (DesignationType == "ExStaff")
                {
                    var MemberList = (List<mStaff>)Helper.ExecuteService("staff", "GetExStaffList", new mStaff { Group = "Non-Gazetted" });
                    model.MemberList = new SelectList(MemberList, "StaffID", "NameWithDesignation");

                    var NomineeList = (List<mStaffNominee>)Helper.ExecuteService("staff", "StaffNomineeList", model.ClaimantId);
                    model.NomineeList = new SelectList(NomineeList, "NomineeID", "NomineeName");
                }
                else if (DesignationType == "Other")
                {
                    var MemberList = ((List<mOtherFirm>)Helper.ExecuteService("Budget", "GetAllOtherFirm", null)).Where(m => m.IsReject == false).ToList();
                    model.MemberList = new SelectList(MemberList, "FirmID", "Name");
                }
                else if (DesignationType == "ChiefWhip")
                {
                    // Current Membeler
                    var MemberList = (List<mMemberAccountsDetails>)Helper.ExecuteService("MemberAccountDetails", "GetChiefWhipDetails", null);
                    model.MemberList = new SelectList(MemberList, "MemberCode", "MemberName");
                }
                if (EditEstablishBill.BillType == 18 || EditEstablishBill.BillType == 19 || EditEstablishBill.BillType == 20 || EditEstablishBill.BillType == 22)
                    EditEstablishBill.BillType = 18;


                List<mBudget> State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetByBudgetType", new mBudget { BudgetType = EditEstablishBill.BillType, FinancialYear = FinYearValue, BudgetHeadOrder = EditEstablishBill.Designation });

                ViewBag.BudgetList = new SelectList(State, "BudgetID", "BudgetName", EditEstablishBill.BudgetId);

                var ApprovedHospitalList = (List<mApprovedHospital>)Helper.ExecuteService("Budget", "GetAllApprovedHospital", null);
                ViewBag.HospitalNameList = new SelectList(ApprovedHospitalList, "HospitalName", "HospitalName", model.HospitalName);

                ViewBag.AlongWithList = new SelectList(Enum.GetValues(typeof(AlongWithList)).Cast<AlongWithList>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text", model.AlongWith);

                ViewBag.JourneyByList = new SelectList(Enum.GetValues(typeof(JourneyByList)).Cast<JourneyByList>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text", model.JourneyBy);


            }
            else
            {

                ViewBag.BillNumber = 0;
                ViewBag.GrossAmount = 0;
                ViewBag.EstablishId = 0;


                ViewBag.Add = "Add";
                var Member = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetMemberListByDesignation", "0");
                model.MemberList = new SelectList(Member, "MemberCode", "Name");
                var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget { FinancialYear = FinYearValue });
                ViewBag.BudgetList = new SelectList(State, "BudgetID", "BudgetName");

                var ApprovedHospitalList = (List<mApprovedHospital>)Helper.ExecuteService("Budget", "GetAllApprovedHospital", null);
                ViewBag.HospitalNameList = new SelectList(ApprovedHospitalList, "HospitalName", "HospitalName");

                ViewBag.AlongWithList = new SelectList(Enum.GetValues(typeof(AlongWithList)).Cast<AlongWithList>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");

                ViewBag.JourneyByList = new SelectList(Enum.GetValues(typeof(JourneyByList)).Cast<JourneyByList>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");
            }
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;
            return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_AddReimbursementBill.cshtml", model);
        }

        public PartialViewResult DeleteReimbursementBill(int Id, int pageId = 1, int pageSize = 10)
        {
            mReimbursementBill Reimbill = (mReimbursementBill)Helper.ExecuteService("Budget", "GetReimbursementBillById", new mReimbursementBill { ReimbursementBillID = Id });

            var VoucherList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillByEstablishId", new mReimbursementBill { EstablishBillId = Reimbill.EstablishBillId });
            if (VoucherList.Count() > 1)
            {
                mEstablishBill obj = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = Reimbill.EstablishBillId });
                obj.TotalAmount = obj.TotalAmount - Reimbill.SactionAmount;
                obj.TotalGrossAmount = obj.TotalGrossAmount - Reimbill.GrossAmount;
                obj.Deduction = obj.TotalGrossAmount - obj.TotalAmount;
                obj.ModifiedBy = CurrentSession.UserName;
                Helper.ExecuteService("Budget", "UpdateEstablishBill", obj);

                Helper.ExecuteService("Budget", "DeleteReimbursementBill", new mReimbursementBill { ReimbursementBillID = Id });
                ViewBag.Msg = "Sucessfully deleted Reimbursement Bill";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";

            }
            else
            {
                ViewBag.Msg = "Sorry ! Unable to delete  Reimbursement Bill because one voucher is necessary in the bill.";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Warning";
            }
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { FinancialYear = FinYearValue, CreatedBy = CurrentSession.UserName }))
                .Where(m => m.Status == 0 || m.IsRejected == true).Where(m => m.EstablishBillId == Reimbill.EstablishBillId).ToList();


            ViewBag.PageSize = pageSize;
            List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_ReimbursementBillList.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveReimbursementBill(ReimbursementBillViewModel model, int BillNumber = 0, int MainEstablishBillID = 0, int pageId = 1, int pageSize = 10)
        {

            // COnverting Datetime from string


            DateTime BillDateOfVoucher = new DateTime();
            if (!string.IsNullOrEmpty(model.BillDateOfVoucherS))
                BillDateOfVoucher = new DateTime(Convert.ToInt32(model.BillDateOfVoucherS.Split('/')[2]), Convert.ToInt32(model.BillDateOfVoucherS.Split('/')[1]), Convert.ToInt32(model.BillDateOfVoucherS.Split('/')[0]));
            else
                BillDateOfVoucher = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            DateTime SactionDate = new DateTime();
            if (!string.IsNullOrEmpty(model.SactionDateS))
                SactionDate = new DateTime(Convert.ToInt32(model.SactionDateS.Split('/')[2]), Convert.ToInt32(model.SactionDateS.Split('/')[1]), Convert.ToInt32(model.SactionDateS.Split('/')[0]));
            else
                SactionDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;

            DateTime BillFromDate = new DateTime();
            if (!string.IsNullOrEmpty(model.BillFromDateS))
                BillFromDate = new DateTime(Convert.ToInt32(model.BillFromDateS.Split('/')[2]), Convert.ToInt32(model.BillFromDateS.Split('/')[1]), Convert.ToInt32(model.BillFromDateS.Split('/')[0]));
            else
                BillFromDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;

            DateTime BillToDate = new DateTime();
            if (!string.IsNullOrEmpty(model.BillToDateS))
                BillToDate = new DateTime(Convert.ToInt32(model.BillToDateS.Split('/')[2]), Convert.ToInt32(model.BillToDateS.Split('/')[1]), Convert.ToInt32(model.BillToDateS.Split('/')[0]));
            else
                BillToDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;

            DateTime DateOfPresentation = new DateTime();
            if (!string.IsNullOrEmpty(model.DateOfPresentationS))
                DateOfPresentation = new DateTime(Convert.ToInt32(model.DateOfPresentationS.Split('/')[2]), Convert.ToInt32(model.DateOfPresentationS.Split('/')[1]), Convert.ToInt32(model.DateOfPresentationS.Split('/')[0]));
            else
                DateOfPresentation = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;

            // working on establish bill

            int MainEstablishBillId = 0;
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            mEstablishBill obj = new mEstablishBill();

            if (BillNumber > 0)
            {
                mEstablishBill EditEstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillByBillNumber", new mEstablishBill { BillNumber = BillNumber, FinancialYear = FinYearValue });
                obj = EditEstablishBill;
            }
            if (obj.BillType == 18 || obj.BillType == 19 || obj.BillType == 20 || obj.BillType == 22)
                obj.BillType = 18;
            // Creating Establish Bill


            if (obj.EstablishBillID == 0)
            {
                mEstablishBill LastEstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetLastEstablishBill", new mEstablishBill { FinancialYear = SiteSettingsSessn.SettingValue });

                if (LastEstablishBill != null && LastEstablishBill.EstablishBillID > 0)
                {
                    if (SiteSettingsSessn.SettingValue == LastEstablishBill.FinancialYear)
                        obj.BillNumber = LastEstablishBill.BillNumber + 1;
                    else
                        obj.BillNumber = 1;
                }
                else
                {
                    obj.BillNumber = 1;
                }
                obj.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
                obj.SanctioNumber = "NA";
                obj.SanctionDate = DateTime.Now;
                if (Convert.ToDateTime(obj.EncashmentDate).ToShortDateString() == "01/01/0001")
                    obj.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                obj.FinancialYear = FinYearValue;
                obj.Designation = int.Parse(model.DesignationS);
                obj.BudgetId = model.BudgetId;
                obj.TotalAmount = model.SactionAmount;
                obj.TotalGrossAmount = model.GrossAmount;
                obj.Deduction = model.GrossAmount - model.SactionAmount;
                obj.CreatedBy = CurrentSession.UserName;
                obj.IsBillUpdate = false;
                obj.BillType = model.BillType;
                obj.BillProcessingDate = DateTime.Now;
                MainEstablishBillId = (int)Helper.ExecuteService("Budget", "CreateEstablishBill", obj);
            }
            else
            {
                if (obj.IsChallanGenerate == false)
                {
                    // Editing Establish Bill
                    if (model.Mode == "Edit")
                    {
                        mReimbursementBill Reimbill = (mReimbursementBill)Helper.ExecuteService("Budget", "GetReimbursementBillById", new mReimbursementBill { ReimbursementBillID = model.ReimbursementBillID });
                        obj.TotalAmount = obj.TotalAmount - Reimbill.SactionAmount + model.SactionAmount;
                        obj.TotalGrossAmount = obj.TotalGrossAmount - Reimbill.GrossAmount + model.GrossAmount;
                        obj.Deduction = obj.TotalGrossAmount - obj.TotalAmount;
                    }
                    else
                    {
                        obj.TotalAmount = obj.TotalAmount + model.SactionAmount;
                        obj.TotalGrossAmount = obj.TotalGrossAmount + model.GrossAmount;
                        obj.Deduction = obj.TotalGrossAmount - obj.TotalAmount;
                    }

                    MainEstablishBillId = obj.EstablishBillID;
                    obj.ModifiedBy = CurrentSession.UserName;
                    Helper.ExecuteService("Budget", "UpdateEstablishBill", obj);
                }
            }

            if (obj.IsChallanGenerate == false)
            {
                // Population Reimbursement Bill
                mReimbursementBill mReimbursementBill = new mReimbursementBill();

                model.BillDateOfVoucher = BillDateOfVoucher;
                model.SactionDate = SactionDate;
                model.BillFromDate = BillFromDate;
                model.BillToDate = BillToDate;
                model.DateOfPresentation = DateOfPresentation;

                model.BillType = obj.BillType;
                model.Designation = obj.Designation;
                model.DesignationS = obj.Designation.ToString();
                model.BudgetId = obj.BudgetId;
                model.EstblishBillNumber = obj.BillNumber;
                model.Remarks = string.Empty;
                model.IsSetEncashment = false;
                model.Deduction = model.GrossAmount - model.SactionAmount;
                model.EstablishBillId = MainEstablishBillId;

                mReimbursementBill = model.ToDomainModel();

                if (model.Mode == "Add")
                {


                    mReimbursementBill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
                    mReimbursementBill.FinancialYear = FinYearValue;
                    int BillId = (int)Helper.ExecuteService("Budget", "CreateReimbursementBill", mReimbursementBill);

                    if (BillId > 0)
                    {
                        mReimbursementBill stateToEdit = (mReimbursementBill)Helper.ExecuteService("Budget", "GetReimbursementBillById", new mReimbursementBill { ReimbursementBillID = BillId });

                        List<string> mobileList = new List<string>();
                        if (!string.IsNullOrEmpty(stateToEdit.Mobile) && ValidationAtServer.CheckMobileNumber(stateToEdit.Mobile))
                            mobileList.Add(stateToEdit.Mobile);
                        //mobileList.Add("8588031303");
                        //  mobileList.Add("9736305930");
                        //mobileList.Add("9418352977");

                        List<string> EmailList = new List<string>();
                        EmailList.Add(stateToEdit.Email);
                        //EmailList.Add("shubham@sblsoftware.com");
                        // EmailList.Add("pallavisood49@gmail.com");
                        //EmailList.Add("lakshay.verma18@gmail.com");


                        string ClaimaintName = string.Empty;
                        if (stateToEdit.Designation == 0 || stateToEdit.Designation == 1 || stateToEdit.Designation == 2 || stateToEdit.Designation == 3)
                            ClaimaintName = string.Format("Respected Hon'ble Member  {0}", stateToEdit.ClaimantName);
                        else if (stateToEdit.Designation == 6)
                            ClaimaintName = string.Format(" {0}", stateToEdit.ClaimantName);
                        else
                            ClaimaintName = string.Format("Sh./Smt. {0}", stateToEdit.ClaimantName);

                        string msgBody = string.Format("{0}, your {1} No. - {2}, Dated : {3} of amount Rs. {4} is in process. \n Secretary HP Vidhan Sabha", ClaimaintName, stateToEdit.BillTypeName, stateToEdit.EstblishBillNumber, stateToEdit.DateOfPresentation.Value.ToString("dd/MM/yyyy"), stateToEdit.SactionAmount);
                        SendEmailDetails(EmailList, mobileList, msgBody, "Bill Status", msgBody, true, true, string.Empty);
                    }
                    ViewBag.Msg = "Sucessfully added Reimbursement Bill";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
                else
                {
                    // Editing Reimbursement Bill

                    Helper.ExecuteService("Budget", "UpdateReimbursementBill", mReimbursementBill);
                    ViewBag.Msg = "Sucessfully updated Reimbursement Bill";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
            }
            else
            {
                ViewBag.Msg = "Sorry...! Unable to add voucher because challan is generated.";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }

            var BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { FinancialYear = FinYearValue, CreatedBy = CurrentSession.UserName })).Where(m => (m.IsChallanGenerate == false || m.IsRejected == true) && m.IsCanceled == false).Where(m => m.EstablishBillId == MainEstablishBillId).ToList();


            ViewBag.PageSize = pageSize;
            List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);
            var modelList = pagedRecord.ToViewModel();

            EstablishBillViewModel data = new EstablishBillViewModel();
            data.ReimbursementBillViewModelList = pagedRecord.ToViewModel();
            data.BillNumber = obj.BillNumber;
            data.SanctionAmount = obj.TotalAmount;
            data.GrossAmount = obj.TotalGrossAmount;
            data.MainEstablishBillId = MainEstablishBillId;
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult GetReimbursementList(int EstablishBillID = 0, int pageId = 1, int pageSize = 10)
        {
            List<mReimbursementBill> BillList = new List<mReimbursementBill>();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            //   mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillID });
            if (EstablishBillID > 0)
            {
                Session["EstablishBillID"] = EstablishBillID;
                BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { CreatedBy = CurrentSession.UserName, FinancialYear = FinYearValue })).Where(m => m.IsChallanGenerate == false || m.IsRejected == true).Where(m => m.EstblishBillNumber == EstablishBillID && m.FinancialYear == FinYearValue).ToList();
            }
            else
            {
                BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { CreatedBy = CurrentSession.UserName, FinancialYear = FinYearValue })).Where(m => m.IsChallanGenerate == false || m.IsRejected == true).ToList();
            }

            ViewBag.PageSize = pageSize;
            List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_ReimbursementBillList.cshtml", model);
        }

        /// <summary>
        ///  this is for rejecting for bill.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        public PartialViewResult RejectBill(ReimbursementBillViewModel model, int pageId = 1, int pageSize = 10)
        {


            if (model.EncashmentDate == null || Convert.ToDateTime(model.EncashmentDate).ToShortDateString() == "01/01/0001")
                model.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;

            mReimbursementBill stateToEdit = (mReimbursementBill)Helper.ExecuteService("Budget", "GetReimbursementBillById", new mReimbursementBill { ReimbursementBillID = model.ReimbursementBillID });
            stateToEdit.IsRejected = model.IsRejected;
            //  stateToEdit.IsApproved = model.IsApproved;
            stateToEdit.IsSetEncashment = model.IsSetEncashment;
            stateToEdit.EncashmentDate = model.EncashmentDate;
            stateToEdit.Remarks = model.Remarks;
            Helper.ExecuteService("Budget", "UpdateReimbursementBill", stateToEdit);
            ViewBag.Msg = "Sucessfully   saved";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var BillList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { CreatedBy = CurrentSession.UserName, FinancialYear = FinYearValue });


            ViewBag.PageSize = pageSize;
            List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);
            var modelList = pagedRecord.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_ReimbursementBillList.cshtml", modelList);
        }

        public JsonResult GetEstblishByBillNumber(int BillNumber = 0)
        {

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillByBillNumber", new mEstablishBill { BillNumber = BillNumber, FinancialYear = FinYearValue });
            List<mReimbursementBill> ReimbursementList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillByEstablishId", new mReimbursementBill { EstablishBillId = EstablishBill.EstablishBillID });
            mReimbursementBill obj = new mReimbursementBill();
            if (ReimbursementList != null && ReimbursementList.Count() > 0)
            {
                obj = ReimbursementList.OrderByDescending(m => m.ReimbursementBillID).FirstOrDefault();
            }
            EstablishBill.SanctioNumber = obj.SactionNumber;
            EstablishBill.SanctionDate = obj.SactionDate;
            return Json(EstablishBill, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNomineeList(int MemberCode, int Designation)
        {
            ReimbursementBillViewModel model = new ReimbursementBillViewModel();
            if (Designation == 3)
            {
                var NomineeList = (List<mMemberNominee>)Helper.ExecuteService("MemberAccountDetails", "GetNomineeList", new mMemberNominee { MemberCode = MemberCode });
                model.NomineeList = new SelectList(NomineeList, "NomineeId", "NomineeName");
            }
            else if (Designation == 7)
            {

                var NomineeList = (List<mStaffNominee>)Helper.ExecuteService("staff", "StaffNomineeList", MemberCode);
                model.NomineeList = new SelectList(NomineeList, "NomineeID", "NomineeName");
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetClaimentList(int Designation)
        {
            ReimbursementBillViewModel model = new ReimbursementBillViewModel();
            var DesignationType = ModelMapping.GetDescription((SBL.DomainModel.Models.Enums.DesignationType)Designation);
            // Ex-member
            if (DesignationType == "ExMember")
            {
                var MemberList = (List<SBL.DomainModel.Models.Member.mMember>)Helper.ExecuteService("MemberAccountDetails", "GetExMembersDetails", null);
                model.MemberList = new SelectList(MemberList, "MemberCode", "Name");

            }
            else if (DesignationType == "Member")
            {
                // Current Membeler
                var MemberList = (List<mMemberAccountsDetails>)Helper.ExecuteService("MemberAccountDetails", "GetCurrentMembersDetails", null);
                model.MemberList = new SelectList(MemberList, "MemberCode", "MemberName");
            }
            else if (DesignationType == "Speaker")
            {

                var MemberList = (List<mMember>)Helper.ExecuteService("MemberAccountDetails", "GetAllSpeaker", null);
                model.MemberList = new SelectList(MemberList, "MemberCode", "Name");
            }
            else if (DesignationType == "DeputySpeaker")
            {
                var MemberList = (List<mMember>)Helper.ExecuteService("MemberAccountDetails", "GetAllDeputySpeaker", null);
                model.MemberList = new SelectList(MemberList, "MemberCode", "Name");
            }
            else if (DesignationType == "GazettedStaff")
            {

                var MemberList = (List<mStaff>)Helper.ExecuteService("staff", "GetStaffListByGroup", new mStaff { Group = "Gazetted" });
                model.MemberList = new SelectList(MemberList, "StaffID", "NameWithDesignation");
            }
            else if (DesignationType == "NonGazettedStaff")
            {
                var MemberList = (List<mStaff>)Helper.ExecuteService("staff", "GetStaffListByGroup", new mStaff { Group = "Non-Gazetted" });
                model.MemberList = new SelectList(MemberList, "StaffID", "NameWithDesignation");
            }
            else if (DesignationType == "ExStaff")
            {
                var MemberList = (List<mStaff>)Helper.ExecuteService("staff", "GetExStaffList", new mStaff { Group = "ExStaff" });
                model.MemberList = new SelectList(MemberList, "StaffID", "NameWithDesignation");
            }
            else if (DesignationType == "Other")
            {
                var MemberList = ((List<mOtherFirm>)Helper.ExecuteService("Budget", "GetAllOtherFirm", null)).Where(m => m.IsReject == false).ToList(); ;
                model.MemberList = new SelectList(MemberList, "FirmID", "Name");
            }
            else if (DesignationType == "ChiefWhip")
            {
                // Current Membeler
                var MemberList = (List<mMemberAccountsDetails>)Helper.ExecuteService("MemberAccountDetails", "GetChiefWhipDetails", null);
                model.MemberList = new SelectList(MemberList, "MemberCode", "MemberName");
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Bill Report

        public PartialViewResult GenerateBillReport(int pageId = 1, int pageSize = 10)
        {
            try
            {

                Session["BillListForReport"] = null;
                string url = "/BillReport/";

                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] filePaths = Directory.GetFiles(directory);
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                var BillTypeList = (List<mBudgetBillTypes>)Helper.ExecuteService("Budget", "GetAllBillType", null);
                ViewBag.BillTypeList = new SelectList(BillTypeList, "BillTypeID", "TypeName");


                ViewBag.StatusList = new SelectList(Enum.GetValues(typeof(BillStatus)).Cast<BillStatus>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");

                ViewBag.DesignationList = new SelectList(Enum.GetValues(typeof(DesignationType)).Cast<DesignationType>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");

                var Member = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetMemberListByDesignation", "0");
                ViewBag.MemberList = new SelectList(Member, "MemberCode", "Name");

                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;


                var BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { FinancialYear = FinYearValue, CreatedBy = CurrentSession.UserName })).Where(m => m.FinancialYear == FinYearValue).ToList();

                ViewBag.PageSize = pageSize;
                List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
                if (pageId == 1)
                {
                    pagedRecord = BillList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);
                Session["BillListForReport"] = BillList;

                return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_GenerateBillReport.cshtml", pagedRecord.ToViewModel());
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }


        public PartialViewResult GetBillReport(string Designation, string ClaimantId, string BillType, string Staus, string SubmissionDateFromS, string SubmissionDateToS, string BillNumber, int pageId = 1, int pageSize = 10)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            List<ReimbursementBillViewModel> Model = new List<ReimbursementBillViewModel>();
            var List = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { FinancialYear = FinYearValue, CreatedBy = CurrentSession.UserName })).OrderBy(m => m.ReimbursementBillID).ToList();
            if (!string.IsNullOrEmpty(BillNumber))
            {
                int BillNumberInt = 0;
                int.TryParse(BillNumber, out BillNumberInt);
                List = List.Where(m => m.EstblishBillNumber == BillNumberInt).ToList();
            }

            if (!string.IsNullOrEmpty(ClaimantId) && !string.IsNullOrEmpty(Designation))
            {
                int MemberInt = int.Parse(ClaimantId);
                int DesignationId = int.Parse(Designation);
                List = List.Where(m => m.ClaimantId == MemberInt && m.Designation == DesignationId).ToList();
            }

            if (!string.IsNullOrEmpty(BillType))
            {
                int BillTypeInt = int.Parse(BillType);
                List = List.Where(m => m.BillType == BillTypeInt).ToList();
            }

            if (!string.IsNullOrEmpty(Staus))
            {
                int StausInt = int.Parse(Staus);
                List = List.Where(m => m.Status == StausInt).ToList();
            }
            if (!string.IsNullOrEmpty(SubmissionDateFromS) && !string.IsNullOrEmpty(SubmissionDateToS))
            {
                DateTime SubmissionDateFrom = new DateTime();
                DateTime.TryParse(SubmissionDateFromS, out SubmissionDateFrom);

                DateTime SubmissionDateTo = new DateTime();
                DateTime.TryParse(SubmissionDateToS, out SubmissionDateTo);

                List = List.Where(m => m.DateOfPresentation >= SubmissionDateFrom && m.DateOfPresentation <= SubmissionDateTo).ToList();
            }


            ViewBag.PageSize = pageSize;
            List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
            if (pageId == 1)
            {
                pagedRecord = List.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = List.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(List.Count, pageId, pageSize);

            Session["BillListForReport"] = List;
            Model = pagedRecord.ToViewModel();

            return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_BillListForReport.cshtml", Model);
        }

        public ActionResult DownloadPdf(string Member, string BillType, string Staus, string SubmissionDateFromS, string SubmissionDateToS, string NotingReport, string BillNumber)
        {
            List<ReimbursementBillViewModel> Model = new List<ReimbursementBillViewModel>();
            if (Session["BillListForReport"] != null)
            {
                var List = (List<mReimbursementBill>)Session["BillListForReport"];
                Model = List.ToViewModel();
            }
            else
            {

                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;
                var List = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { FinancialYear = FinYearValue, CreatedBy = CurrentSession.UserName })).OrderBy(m=>m.ReimbursementBillID).ToList();

                if (!string.IsNullOrEmpty(BillNumber))
                {
                    int BillNumberInt = 0;
                    int.TryParse(BillNumber, out BillNumberInt);
                    List = List.Where(m => m.EstblishBillNumber == BillNumberInt).ToList();
                }

                if (!string.IsNullOrEmpty(Member))
                {
                    int MemberInt = int.Parse(Member);
                    List = List.Where(m => m.ClaimantId == MemberInt).ToList();
                }
                if (!string.IsNullOrEmpty(BillType))
                {
                    int BillTypeInt = int.Parse(BillType);
                    List = List.Where(m => m.BillType == BillTypeInt).ToList();
                }

                if (!string.IsNullOrEmpty(Staus))
                {
                    int StausInt = int.Parse(Staus);
                    List = List.Where(m => m.Status == StausInt).ToList();
                }
                if (!string.IsNullOrEmpty(SubmissionDateFromS) && !string.IsNullOrEmpty(SubmissionDateToS))
                {
                    DateTime SubmissionDateFrom = new DateTime();
                    DateTime.TryParse(SubmissionDateFromS, out SubmissionDateFrom);

                    DateTime SubmissionDateTo = new DateTime();
                    DateTime.TryParse(SubmissionDateToS, out SubmissionDateTo);

                    List = List.Where(m => m.DateOfPresentation >= SubmissionDateFrom && m.DateOfPresentation <= SubmissionDateTo).ToList();
                }
                Model = List.ToViewModel();
            }
            string Result = string.Empty;
            if (NotingReport.Equals("1"))
                Result = GetSearchListForNoting(Model);
            else
                Result = GetSearchList(Model);



            MemoryStream output = new MemoryStream();

            EvoPdf.Document document1 = new EvoPdf.Document();

            // set the license key
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

            AddElementResult addResult;

            HtmlToPdfElement htmlToPdfElement;
            string htmlStringToConvert = Result;
            string baseURL = "";
            htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

            addResult = page.AddElement(htmlToPdfElement);
            byte[] pdfBytes = document1.Save();

            try
            {
                output.Write(pdfBytes, 0, pdfBytes.Length);
                output.Position = 0;

            }
            finally
            {
                // close the PDF document to release the resources
                document1.Close();
            }

            string path = "";
            try
            {


                Guid FId = Guid.NewGuid();
                string fileName = FId + "_BillList.pdf";

                string url = "/BillReport/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "BillReportList.pdf";

                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

                // System.IO.File.Delete(path);
            }


#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }

        public static string GetSearchList(List<ReimbursementBillViewModel> model)
        {

            StringBuilder BillReport = new StringBuilder();

            if (model != null && model.Count() > 0)
            {
                BillReport.Append("<div style='text-align:center;'><h2>Report of Bills</h2></div>");
                BillReport.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                BillReport.Append("<thead class='header' ><tr style='background-color: #6fb3e0 ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:80px;text-align:left;'>Sr. No.</th>");
                BillReport.Append("<th style='width:150px;text-align:left; ' >Memorandum No.</th><th style='width:150px;text-align:left;border-left: 1px solid #fff; ' >Memorandum Type</th><th style='width:150px;text-align:left;border-left: 1px solid #fff; ' >Claimant Name</th><th style='width:150px;text-align:left;'>Gross Amount</th>");
                BillReport.Append("<th style='width:150px;text-align:left;' >Submission Date</th><th style='width:150px;text-align:left;'>Advance Taken</th><th style='width:150px;text-align:left;'>Sanction Amount</th><th style='width:150px;text-align:left;'>Sanctioned Date</th>");

                BillReport.Append("<th style='width:150px;text-align:left;' >GPF / Meter / Telephone No.</th><th style='width:150px;text-align:left;'>Encashment Date</th><th style='width:150px;text-align:left;'>Status</th>");

                BillReport.Append("</tr></thead>");


                BillReport.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    BillReport.Append("<tr>");
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", count));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ReimbursementBillID));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.BillTypeName));
                    //if (obj.Designation == 6)
                    //{
                    //    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ClaimantName));
                    //}
                    //else if (obj.Designation == 4 || obj.Designation == 5)
                    //{
                    //    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.StaffName));
                    //}
                    //else
                    //{
                    //    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.MemberName));
                    //}
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ClaimantName));

                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.GrossAmount));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.DateOfPresentation).ToString("dd/MMM/yyyy")));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.TakenAmount));
                    if (obj.IsSanctioned)
                    {
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.SactionAmount));
                        if (Convert.ToDateTime(obj.SactionDate).ToString("dd/MMM/yyyy") != "01/Jan/1753")
                            BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.SactionDate).ToString("dd/MMM/yyyy")));


                        //BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}-{1}</td>", Convert.ToDateTime(obj.BillFromDate).ToString("dd/MMM/yyyy"), Convert.ToDateTime(obj.BillToDate).ToString("dd/MMM/yyyy")));

                    }
                    else
                    {
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>--</td>"));
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>--</td>"));



                    }
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Number));
                    if (obj.IsSetEncashment || Convert.ToDateTime(obj.EncashmentDate).ToString("dd/MMM/yyyy") != "01/Jan/1753")
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.EncashmentDate).ToString("dd/MMM/yyyy")));
                    else
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>--</td>"));

                    if (obj.Status == 0)
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "Bill in Process"));
                    else if (obj.Status == 1)
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "Sanctioned"));

                    else if (obj.Status == 2)
                        BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "Encashment"));


                    BillReport.Append("</tr>");
                }
                BillReport.Append("</tbody></table></div> ");
            }
            else
            {
                BillReport.Append("No records available");
            }
            return BillReport.ToString();
        }


        public static string GetSearchListForNoting(List<ReimbursementBillViewModel> model)
        {

            StringBuilder BillReport = new StringBuilder();
            decimal NotingTotalAmount = 0;
            if (model != null && model.Count() > 0)
            {
                BillReport.Append("<div style='text-align:left;margin-left:65px;'><h3>T.A. Bills of the Hon'ble Members have been prepared as per detail given below:-</h3></div>");
                BillReport.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;margin-left:65px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                BillReport.Append("<thead class='header' ><tr style='background-color: #6fb3e0 ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:80px;text-align:left;'>Sr. No.</th>");
                BillReport.Append("<th style='width:150px;text-align:left;border-left: 1px solid #fff; ' >Name of Member</th>");
                BillReport.Append("<th style='width:150px;text-align:left;' >Period</th><th style='width:150px;text-align:left;'>Name of Com./Session</th>");
                BillReport.Append("<th style='width:150px;text-align:left;'> Amount</th>");
                BillReport.Append("</tr></thead>");
                BillReport.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    BillReport.Append("<tr>");
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", count));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ClaimantName));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}-{1}</td>", Convert.ToDateTime(obj.BillFromDate).ToString("dd/MMM/yyyy"), Convert.ToDateTime(obj.BillToDate).ToString("dd/MMM/yyyy")));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;padding-left:6px;'>{0}</td>", obj.Comette));
                    BillReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.GrossAmount));

                    BillReport.Append("</tr>");
                    NotingTotalAmount = NotingTotalAmount + obj.GrossAmount;
                }
                BillReport.Append(string.Format("<tr><td colspan='4'  style='text-align:right;border: 1px dotted #808080;'><b>Total Amount</b> </td><td style='border: 1px dotted #808080;padding-left:6px;'> <b>{0}</b></td> ", NotingTotalAmount));
                BillReport.Append("</tbody></table> ");

                BillReport.Append(string.Format("<p style='width: 820px;font-size: large;font-weight: bold;padding-left:70px;'>A Consolidated T.A. Bill amounting to Rs. {0} only is placed below for signature of the Secretary and D.D.O. please.</p>", NotingTotalAmount));
                BillReport.Append("</div>");

            }
            else
            {
                BillReport.Append("No records available");
            }
            return BillReport.ToString();
        }

        public PartialViewResult GetBillReportByPaging(int pageId = 1, int pageSize = 10)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            List<mReimbursementBill> BillList = new List<mReimbursementBill>();

            if (Session["BillListForReport"] != null)
                BillList = (List<mReimbursementBill>)Session["BillListForReport"];
            else
                BillList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { FinancialYear = FinYearValue, CreatedBy = CurrentSession.UserName });

            ViewBag.PageSize = pageSize;
            List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_BillListForReport.cshtml", model);
        }

        #endregion


        #region Private Methods
        private string UploadPhoto(HttpPostedFileBase File, Guid FileName)
        {
            if (File != null)
            {

                string directory = Server.MapPath("~/Images/SubVoucher");
                string path = System.IO.Path.Combine(directory, FileName + "_" + File.FileName);
                try
                {
                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }
                    File.SaveAs(path);
                }
                catch (Exception)
                {

                    throw;
                }

                return (FileName + "_" + File.FileName);
            }
            return null;
        }

        private void RemoveExistingPhoto(int SubVoucherId, string Module)
        {
            if (SubVoucherId > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(SubVoucherId + ",");
                sb.Append(Module);
                var State = (List<mUploadImages>)Helper.ExecuteService("UploadImages", "GetAllUploadImages", sb.ToString());
                if (State != null && State.Count() > 0)
                {
                    foreach (var obj in State)
                    {
                        if (obj != null && obj.FileName != null)
                        {
                            string path = System.IO.Path.Combine(Server.MapPath("~/Images/SubVoucher"), obj.FileName);

                            if (System.IO.File.Exists(path))
                            {
                                System.IO.File.Delete(path);
                                Helper.ExecuteService("UploadImages", "DeleteImages", new mUploadImages { UploadImagesID = obj.UploadImagesID });

                            }
                        }
                    }
                }


            }

        }

        private static DateTime GetFirstDayOfMonth(int iMonth, int Year)
        {
            // set return value to the last day of the month
            // for any date passed in to the method

            // create a datetime variable set to the passed in date
            DateTime dtFrom = new DateTime(Year, iMonth, 1);

            // remove all of the days in the month
            // except the first day and set the
            // variable to hold that date
            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));

            // return the first day of the month
            return dtFrom;
        }

        private static DateTime GetLastDayOfMonth(int iMonth, int Year)
        {

            // set return value to the last day of the month
            // for any date passed in to the method

            // create a datetime variable set to the passed in date
            DateTime dtTo = new DateTime(Year, iMonth, 1);

            // overshoot the date by a month
            dtTo = dtTo.AddMonths(1);

            // remove all of the days in the next month
            // to get bumped down to the last day of the
            // previous month
            dtTo = dtTo.AddDays(-(dtTo.Day));

            // return the last day of the month
            return dtTo;

        }
        #endregion


        #region Send Email
        public static void SendEmailDetails(List<string> emailList, List<string> mobileList, string msgBody, string mailSubject, string mailBody, bool mobileStatus, bool emailStatus, string SavedPdfPath)
        {
            #region SMS And Email

            List<string> emailAddresses = new List<string>();
            List<string> phoneNumbers = new List<string>();

            SMSMessageList smsMessage = new SMSMessageList();
            CustomMailMessage emailMessage = new CustomMailMessage();

            //# SMS Settings
            if (emailList != null)
            {
                emailAddresses.AddRange(emailList);

            }
            if (mobileList != null)
            {
                phoneNumbers.AddRange(mobileList);
            }
            if (emailAddresses != null && phoneNumbers != null)
            {
                foreach (var item in phoneNumbers)
                {
                    smsMessage.MobileNo.Add(item);
                }
                foreach (var item in emailAddresses)
                {
                    emailMessage._toList.Add(item);
                }
            }

            smsMessage.SMSText = msgBody;
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = 1;
            emailMessage._isBodyHtml = true;
            emailMessage._subject = mailSubject;
            emailMessage._body = mailBody;
            if (!string.IsNullOrEmpty(SavedPdfPath))
            {
                EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes((SavedPdfPath)) };
                emailMessage._attachments = new List<EAttachment>();
                emailMessage._attachments.Add(ea);
            }
            try
            {
                if (emailList != null || mobileList != null)
                {
                    Notification.Send(mobileStatus, emailStatus, smsMessage, emailMessage);
                }

            }
            catch (Exception exp)
            {
                string errorMsg = exp.Message;
                // throw;
            }

            #endregion SMS And Email
        }

        #endregion

    }
}
