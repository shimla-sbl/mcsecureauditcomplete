﻿using SBL.DomainModel.Models.Budget;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class BudgetBillTypeController : Controller
    {
        //
        // GET: /AccountsAdmin/BudgetBillType/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mBudgetBillTypes>)Helper.ExecuteService("BudgetBillType", "GetAllBudgetBillType", null);
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Budget Memorandum Type Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public PartialViewResult PopulateBudgetType(int Id)
        {
            BudgetBillTypeViewModel model = new BudgetBillTypeViewModel();

            if (Id > 0)
            {
                mBudgetBillTypes stateToEdit = (mBudgetBillTypes)Helper.ExecuteService("BudgetBillType", "GetBudgetBillTypeById", new mBudgetBillTypes { BillTypeID = Id });
                model = stateToEdit.ToViewModel1("Edit");
            }
            else
            {
                model.VMode = "Add";
                model.VIsActive = true;
            }
            return PartialView("CreateBudgetBillType", model);
        }

        [HttpPost]
        //  [ValidateAntiForgeryToken]
        public ActionResult SaveBudgetBillType(BudgetBillTypeViewModel model)
        {

            var bill = model.ToDomainModel();
            if (model.VMode == "Add")
            {

                Helper.ExecuteService("BudgetBillType", "CreateBudgetBillType", bill);
            }
            else
            {
                Helper.ExecuteService("BudgetBillType", "UpdateBudgetBillType", bill);
            }
            var State = (List<mBudgetBillTypes>)Helper.ExecuteService("BudgetBillType", "GetAllBudgetBillType", null);
            var data = State.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/BudgetBillType/_BudgetBillTypeList.cshtml", data);


        }


    }
}
