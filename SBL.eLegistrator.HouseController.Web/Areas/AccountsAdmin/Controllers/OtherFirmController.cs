﻿using SBL.DomainModel.Models.Budget;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class OtherFirmController : Controller
    {
        //
        // GET: /AccountsAdmin/OtherFirm/

        public PartialViewResult OtherFirmIndex(int pageId = 1, int pageSize = 10)
        {
            try
            {

                var BillList = (List<mOtherFirm>)Helper.ExecuteService("Budget", "GetAllOtherFirm", null);

                ViewBag.PageSize = pageSize;
                List<mOtherFirm> pagedRecord = new List<mOtherFirm>();
                if (pageId == 1)
                {
                    pagedRecord = BillList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/OtherFirm/_OtherFirmIndex.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of other firm Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 10)
        {
            var BillList = (List<mOtherFirm>)Helper.ExecuteService("Budget", "GetAllOtherFirm", null);

            ViewBag.PageSize = pageSize;
            List<mOtherFirm> pagedRecord = new List<mOtherFirm>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/OtherFirm/_OtherFirmList.cshtml", pagedRecord);
        }


        public PartialViewResult PopulateOtherFirm(int Id, string Action, int pageId = 1, int pageSize = 10)
        {
            mOtherFirm model = new mOtherFirm();


            var stateToEdit = (mOtherFirm)Helper.ExecuteService("Budget", "GetOtherFirmById", new mOtherFirm { FirmID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit;

            model.Mode = Action;
            ViewBag.Add = Action;
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;
            return PartialView("/Areas/AccountsAdmin/Views/OtherFirm/_OtherFirmPopulate.cshtml", model);
        }

        public PartialViewResult RejectFirm(int Id, int pageId = 1, int pageSize = 10)
        {
            var model = (mOtherFirm)Helper.ExecuteService("Budget", "GetOtherFirmById", new mOtherFirm { FirmID = Id });
            if (model.IsReject)
                model.IsReject = false;
            else
                model.IsReject = true;

            Helper.ExecuteService("Budget", "UpdateOtherFirm", model);
            ViewBag.Msg = "Sucessfully rejected/un-rejected Firm";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            var BillList = (List<mOtherFirm>)Helper.ExecuteService("Budget", "GetAllOtherFirm", null);

            ViewBag.PageSize = pageSize;
            List<mOtherFirm> pagedRecord = new List<mOtherFirm>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/OtherFirm/_OtherFirmList.cshtml", pagedRecord);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SaveOtherFirm(mOtherFirm model, int pageId = 1, int pageSize = 10)
        {
            if (model.Mode == "Add")
            {
                int BillId = (int)Helper.ExecuteService("Budget", "CreateOtherFirm", model);
                ViewBag.Msg = "Sucessfully added other firm";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            else
            {
                Helper.ExecuteService("Budget", "UpdateOtherFirm", model);
                ViewBag.Msg = "Sucessfully updated Reimbursement Bill";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }

            var BillList = (List<mOtherFirm>)Helper.ExecuteService("Budget", "GetAllOtherFirm", null);

            ViewBag.PageSize = pageSize;
            List<mOtherFirm> pagedRecord = new List<mOtherFirm>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/OtherFirm/_OtherFirmList.cshtml", pagedRecord);
        }

        [HttpPost]
        public ActionResult DownloadExcel()
        {
            var OtherFrimList = (List<mOtherFirm>)Helper.ExecuteService("Budget", "GetAllOtherFirm", null);
            string Result = this.GetOtherFrimListData(OtherFrimList);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=OtherFrimList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }


        public string GetOtherFrimListData(List<mOtherFirm> model)
        {

            StringBuilder ReplyList = new StringBuilder();

            if (model != null && model.Count() > 0)
            {
                ReplyList.Append(string.Format("<div style='text-align:center;'><h2>Other Firm List</h2></div>"));

                ReplyList.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                ReplyList.Append("<thead class='header' ><tr style='background-color: #428bca ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:30px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px;'>SR. NO.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Name</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Address</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Mobile Number</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Email Id</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Account Number</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >IFSC Code</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Bank Name</th>");
                ReplyList.Append("</tr></thead>");


                ReplyList.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    ReplyList.Append("<tr>");
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", count));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.Name));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.Address));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.MobileNo));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.EmailId));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.AccNo));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.IFSCCode));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.BankName));
                    ReplyList.Append("</tr>");
                }
                ReplyList.Append("</tbody></table></div> ");
            }
            else
            {
                ReplyList.Append("No Other Firm Found");
            }
            return ReplyList.ToString();
        }
    }
}
