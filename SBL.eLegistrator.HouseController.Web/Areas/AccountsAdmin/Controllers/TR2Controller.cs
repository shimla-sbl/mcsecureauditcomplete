﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.DomainModel.Models.FormTR2;
using SBL.DomainModel.Models.StaffManagement;
using SBL.DomainModel.Models.Budget;
using EvoPdf;
using System.Drawing;
using System.IO;
using iTextSharp.text;
using Ionic.Zip;
using System.Text;
namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class TR2Controller : Controller
    {
        //
        // GET: /AccountsAdmin/TR2/

        public ActionResult Index()
        {
            Tr2Model model = new Tr2Model();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            model.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            model.FinancialMonthList = ModelMapping.GetMothsforinitial();
            model.Mstaff = (List<mStaff>)Helper.ExecuteService("TR2", "GetStaff", null);



            model.BudgetHeads = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget { FinancialYear = FinYearValue });
            return View(model);
        }

        [HttpGet]
        public JsonResult GetMonths(string Months)
        {
            Tr2Model model = new Tr2Model();
            model.FinancialMonthList = ModelMapping.GetMonthsList(Months);
            return Json(model.FinancialMonthList, JsonRequestBehavior.AllowGet);
        }



        public string GeneratePdfReportEmp(string FYear, string FMonth, string StaffId, string BudgetHeadId, string TxtStatus, string CDate, string CLetterNo)
        {
            try
            {
                string outXml = "";
                string LOBName = FYear;
                string fileName = "";
                string savedPDF = string.Empty;
                string Ids = StaffId.TrimEnd(',');



                tEmployeeAccountDetails model = new tEmployeeAccountDetails();
                model.finanacialYear = FYear;
                model.AccountMonth = Convert.ToInt32(FMonth);
                model.BID = Convert.ToInt32(BudgetHeadId);
                string s = Ids;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');
                    model.MstaffIds = new List<string>();
                    for (int i = 0; i < values.Length; i++)
                    {
                        model.MstaffIds.Add(values[i]);
                    }

                }

                model = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetBudgetCode", model);
                model = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetSumforReport", model);

                outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                outXml += @"<div> 

<table style='width:100%;margin-left:10px;margin-top:12px;;margin-bottom:5px'>
    <tr style='width:100%;font-size:25px'>
        <td >
       H.P.T.R. 2 
        </td>
        <td style='text-align:center;font-size:25px'>
            DISNIC-TREASURY
        </td>
        <td style='text-align:right;font-size:25px'>
            NICNET-H.P
        </td>
    </tr>
  
</table>

<table style='width:100%;border: 1px solid black;border-spacing: 0;border-collapse: collapse;'> 

<tr> 

<td style='width:50%;font-size:25px'>
<< PAY BILL >>
    <br />
    DETAILED PAYBILL OF INCUMBENT OF ESTABLISHMENT OF: Secretary H.P Vidhan Sabha
    <br />
    FOR THE MONTH OF: " + TxtStatus + @"
</td>

<td style='width:50%;'>
<table style='width:100%;border-spacing: 0;border-collapse: collapse;'>
<tr>
<td style='width:30%;border: 1px solid black;'>
<table>

<tr>
<tr>
<td style='font-size:25px'>
</td>
</tr>
<td style='font-size:25px'>
Bill No.:.........
</td>
<tr>
<td style='width:100%;font-size:25px' >
Bill Date.:.........
</td>
</tr>
</tr>
</table>
</td>
<td style='width:30%;border: 1px solid black;'>
<table>
<tr>
<tr>
<td style='font-size:25px'>
</td>
</tr>
<td style='font-size:25px'>
Token No.:.........
</td>
<tr>
<td style='width:100%;font-size:25px' >
Token Date:.........
</td>
</tr>
</tr>
</table>
</td>
<td style='width:30%;border: 1px solid black;'>
<table>
<tr>
<td style='font-size:25px'>
(For Treasury Officer Use)
</td>
</tr>
<tr>
<td style='font-size:25px'>
Voucher No.:.........
</td>
<tr>
<td style='width:100%;font-size:25px' >
Voucher Date:.........
</td>
</tr>
</tr>
</table>
</td>
</tr>
</table>
</td>

</tr>  



<tr>  
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;font-size:25px'>
    <tr>
        <td> 1. Treasury Code:  </td>
            <td>" + model.TreasuryCD + @"</td>
        <td>2. Demand No:</td>
        <td>" + model.DemandNo + @"</td>
    </tr>   
    <tr>
        <td> 3. D.D.O.Code:  </td>
            <td>" + model.DDOCD + @"</td>
        <td>4. Gaztd./Non-Gaztd:</td>
        <td>" + model.Gazett + @"</td>
    </tr>
    <tr>
        <td>  5. Major Head:  </td>
            <td>" + model.MajorHead + @"</td>
     
    </tr>
    <tr>
        <td> 6. Sub-Major Head:  </td>
            <td>" + model.SubmajorHead + @"</td>
  
    </tr>
     
     <tr>
        <td> 7. Minor-Head:  </td>
            <td>" + model.MinorHead + @"</td>
    
    </tr>
     <tr>
        <td>   8. Sub Head:  </td>
            <td>" + model.SubHead + @"</td>
      
    </tr>
     <tr>
        <td> 9. Budget Code: </td>
            <td>" + model.BudgetCode + @"</td>
        <td>10. Object No:</td>
        <td>" + model.ObjectCode + @"</td>
    </tr>
     <tr>
        <td>11. Plan/Non-Plan:  </td>
            <td>" + model.Plan + @"</td>
        <td> 12. Voted/Charged:</td>
        <td>" + model.Plan + @"</td>
    </tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>
<tr>
<td style='width:100%;'>
<table style='width:100%;font-size:25px''>
    <tr>
        <td>  1. GROSS TOTAL:  </td>
        <td></td>
            <td>Rs " + model.GrossTotalHead + @"</td>
        
    </tr>   
    <tr>
        <td>  2. SHORT DRAWAL:  </td>
         <td></td>
            <td>Rs Nill</td>
        
    </tr>
    <tr>
        <td> 3. TOTAL AMOUNT:</td>
         <td>(1-2)</td>
            <td>Rs " + model.GrossTotalHead + @"</td>
     
    </tr>
    <tr>
        <td>4. A.G DEDUCTIONS  (A):  </td>
         <td></td>
            <td>Rs " + model.TotalDeducA + @"</td>
  
    </tr>
     <tr>
        <td> 5. BALANCE AMOUNT:  </td>
         <td>(3-4) </td>
            <td> Rs " + model.BalanceAmount + @"</td>
     
    </tr>
     <tr>
        <td> 6. B.T.DEDUCTIONS:  (B):  </td>
          <td></td>
            <td>Rs " + model.TotalDeducB + @"</td>
    
    </tr>
     <tr>
        <td>  7. NET AMOUNT (payble):  </td>
          <td>(5-6)</td>
            <td>Rs " + model.NetAmount + @"</td>
      
    </tr>
     
</table>
</td>
</tr>
</table>
</td>
</tr>   <!-- end 2 row -->


<tr>  <!-- Start 3 row -->
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;;font-size:25px'>    
<tr>
<td style='width:50%;border: 1px solid black;'>
       <span>&nbsp&nbsp&nbsp SUB-OBJECT(DETAILED) HEADS</span>
    <table style='width:100%;font-size:25px''>
     
    <tr>
        <td> 1. Basic Pay:  </td>
        <td>01</td>
            <td>Rs " + model.BasicPay + @"</td>
        
    </tr>
          <tr>
        <td> 2. Special Pay:  </td>
        <td>02</td>
            <td>Rs " + model.SpecialPay + @"</td>
        
    </tr>   
         <tr>
        <td> 3. Dearness Allow:  </td>
        <td>03</td>
            <td>Rs " + model.DearnessAllowns + @"</td>
        
    </tr>   
         <tr>
        <td> 4. Compensatory Allow:  </td>
        <td>04</td>
            <td>Rs " + model.CompenseAllowns + @"</td>
        
    </tr>   
         <tr>
        <td> 5. House Rent Allow:  </td>
        <td>05</td>
            <td>Rs " + model.HRAAllowns + @"</td>
        
    </tr>   
         <tr>
        <td> 6. Capital Allowance: </td>
        <td>06</td>
            <td>Rs " + model.CapitalAllowns + @"</td>
        
    </tr>   
        <tr>
        <td>  7. Conveyance Allow: </td>
        <td>07</td>
            <td>Rs " + model.ConveyaceAllowns + @"</td>
        
    </tr>   
        <tr>
        <td> 8. Washing Allowance: </td>
        <td>08</td>
            <td>Rs " + model.WashingAllowns + @".</td>
        
    </tr>   
        <tr>
        <td>  9. : </td>
        <td>.......</td>
            <td>Rs........</td>
        
    </tr>   
          <tr>
        <td>  10. : </td>
        <td>G.P</td>
            <td>Rs  " + model.GP + @"</td>
        
    </tr>   
          <tr>
        <td>  11. : </td>
        <td>Sect. Pay</td>
            <td>Rs " + model.SectPay + @"</td>
        
    </tr>   
          <tr>
        <td>  12. : </td>
        <td>.......</td>
            <td>Rs........</td>
        
    </tr>   
          <tr>
        <td>  13. : </td>
        <td>.......</td>
            <td>Rs........</td>
        
    </tr>   
        </table>
</td>
    <td style='width:50%; border: 1px solid black;'>
<span>&nbsp&nbsp&nbsp (A) DEDUCTIONS (CLASSIFIED BY A.G)</span>
<table style='width:100%;font-size:25px''>
    <tr>
        <td>  1. GPS Subscription:  </td>
        <td></td>
            <td>Rs " + model.GPFSubs + @"</td>
        
    </tr>
          <tr>
        <td> 2. GPF Advance Recovery:  </td>
        <td></td>
            <td>Rs " + model.GPFAdvans + @"</td>
        
    </tr>   
         <tr>
        <td> 3. House Building Advance:  </td>
        <td></td>
            <td>Rs " + model.HBA + @"</td>
        
    </tr>   
         <tr>
        <td> 4. ' '  Intrest:  </td>
        <td></td>
            <td>Rs " + model.HBAInterest + @"</td>
        
    </tr>   
         <tr>
        <td> 5. M.Car/Scooter Advance:  </td>
        <td></td>
            <td>Rs " + model.NCarScooterAdvance + @"</td>
        
    </tr>   
         <tr>
        <td> 6. ' '  Intrest: </td>
        <td></td>
            <td>Rs " + model.MCScooterInterest + @"</td>
        
    </tr>   
        <tr>
        <td>  7. Warm Cloth Advance: </td>
        <td>.......</td>
            <td>Rs " + model.WarmclothAdvance + @"</td>
        
    </tr>   
        <tr>
        <td> 8. ' '  Intrest: </td>
        <td></td>
            <td>Rs " + model.WarmclothInterest + @"</td>
        
    </tr>   
        <tr>
        <td>  9. L.T.C./T.A Advance: </td>
        <td></td>
            <td>Rs " + model.LTCTAAdvance + @"</td>
        
    </tr>   
          <tr>
        <td>  10. Festival Advance: </td>
        <td></td>
            <td>Rs " + model.FestivalAdvance + @"</td>
        
    </tr>   
          <tr>
        <td> 11. Miscellaneous Recovery: </td>
        <td></td>
            <td>Rs " + model.MiscelRecov + @"</td>
        
    </tr>   
          <tr>
        <td>  12.  </td>
        <td></td>
            <td>Rs........</td>
        
    </tr>   
       
        </table>
</td>
</tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;font-size:25px'>
  
<tr>
    
<td style='width:100%;'>
    <span>(B) DEDUCTIONS(CLASSIFIED BY T.O):</span>    
 <span >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp< * CORRESPONDING RECEIPTS CODES * ></span> 
    <table style='width:100%;font-size:25px'>


   <tr>
        <td ></td>     
            <td ></td>
           <td style='border: 1px solid black;'>Major</td>
           <td style='border: 1px solid black;'>S.Maj</td>
           <td style='border: 1px solid black;'>Minor</td>
           <td style='border: 1px solid black;'>S.Head</td>
           <td style='border: 1px solid black;'>000 Code</td>

        
    </tr>
    <tr>
        <td >   1. Insurance fund:  </td>     
            <td>Rs " + model.InsurFund + @"</td>
 <td style='border: 1px solid black;'>8011</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>105</td>
           <td style='border: 1px solid black;'>01</td>
           <td style='border: 1px solid black;'></td>
        
    </tr>
          <tr>
        <td> 2. Savings Fund:  </td>        
            <td>Rs " + model.SavingFund + @"</td>
  <td style='border: 1px solid black;'>8011</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>105</td>
           <td style='border: 1px solid black;'>02</td>
                <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td> 3. House Rent:  </td>
            <td>Rs " + model.HouseRent + @"</td>
  <td style='border: 1px solid black;'>0216</td>
           <td style='border: 1px solid black;'>01</td>
           <td style='border: 1px solid black;'>106</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td>  4.Postal Life Insurance:</td>
            <td>Rs " + model.PLI + @"</td>
    <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>103</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td> 5.Life Insurance Corp:  </td>
            <td>Rs " + model.LIC + @"</td>
    <td style='border: 1px solid black;'>8448</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>104</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td>  6.Income tax: </td>
            <td>Rs " + model.IncomeTax + @"</td>
   <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>112</td>
           <td style='border: 1px solid black;'>01</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td> 7. Surcharge: </td>
            <td>Rs " + model.Surcharge + @"</td>
    <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>112</td>
           <td style='border: 1px solid black;'>02</td>
              <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td>8. Other B.T |: </td>
            <td>Rs " + model.OtherBT + @"</td>
   <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td> 9. Other B.T ||: </td>
            <td>Rs........</td>
         <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
    </tr>   
          <tr>
        <td>    10. Other B.T |||: </td>
            <td>Rs........</td>
         <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
    </tr>   
        
        
       
        </table>
</td>
</tr>
</table>
</td>
</tr>  

    <tr>  
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>    
<tr>
<td style='width:50%;border: 1px solid black;'>
    <table style='width:100%';font-size:25px'>
     
    <tr>
        <td style='font-size:25px'>GROSS TOTAL </td>
        <td></td>
            <td style='font-size:25px'>Rs " + model.GrossTotalHead + @"</td>
        
    </tr>
           
             
        </table>
</td>
    <td style='width:50%; border: 1px solid black;'>
<table style='width:100%'>
    <tr>
        <td style='font-size:25px'> TOTAL (A)  </td>
        <td></td>
            <td style='font-size:25px'>Rs " + model.TotalDeducA + @"</td>
        
    </tr>
              
        </table>
</td>
</tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>
  
<tr>
    
<td style='width:100%;'>
 <table style='width:100%'>
    <tr>
        <td style='font-size:25px'> TOTAL (B)  </td>
        <td></td>
            <td style='font-size:25px'>Rs " + model.TotalDeducB + @"</td>
        
    </tr>
              
        </table>
</td>
</tr>
</table>
</td>
</tr>

</table>
<table style='width:100%'>
    <tr style='width:100%'>
        <td style='width:25%'>
            <table style='width:100%;font-size:25px''>
                <tr style='width:100%;'>
                    <td >
                        Station
                    </td>
                     <td style='margin-top:10px'>
----------------
                    </td>

                </tr>

                <tr style='width:100%'>
                    <td>
                        Date
                    </td>
                     <td>
----------------
                    </td>
                </tr>
            </table>
        </td>
        <td style='width:25%;font-size:25px'>
            (Treasury Clerk)
        </td>
        <td style='width:50%'>
            <table style='width:100%;font-size:25px'>
                <tr style='width:100%'>
                    <td>
                        (Signature & Designation of Drawing Officer)
                    </td>
                     

                </tr>

                <tr style='width:100%'>
                    <td>
                        Code:
                    </td>
                     
                </tr>
            </table>


</tr>

</table>  

    <div style='page-break-after: always;'></div>
<table>
<tr>
" + GetTR2Result(model.MstaffIds, FYear, int.Parse(FMonth)) + @"

</tr>

</table>  
   <div style='page-break-after: always;'></div>

    <table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  CERTIFICATS</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
         1.  The Drawl is being made on account of. " + TxtStatus + @" sanctioned vide letter no." + CLetterNo + @" .Dtd." + CDate + @"
</td>
 </tr>  
    </table>
<table style='font-size:22px'> 
    <tr>
      <td>
         2.  Arrears were less drawn vide T/V No......................................
</td>

 </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         3.  Certified that pay ans allowances drawn in this bill are due ans admissible as per authority is force and the deductions where-ever required have been made, as per the rules.
</td>
         </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         4.  Certified that in case of fresh appointees , the medical fitness certicates have been obtained.

</td>
 </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         5.  Certified that all appointments and promotions, grant of leave and period of suspensions, and the deputation and other events which are required to be recorded, have been recorded in the
         service Book & leave account of the concerned employee.
</td>
         </tr>
        </table>
  <table style='font-size:22px'> 
    <tr>
     <td>
       
         6.  Recieved contents (in Cash)Rs. " + model.NetAmount + @"(in Words) Rupees." + NumberToWordsConverter.NumbersToWords(Convert.ToInt64(model.NetAmount)) + @"
</td>
 </tr>
        </table>

<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:right'>
             (Signature & Designation of Drawing and Disbursing Officer)
        </td>
       
        
    </tr>
  <tr style='width:100%'>
       
        <td style='text-align:right;padding-right:306px'>
             Code No.
        </td>  
    </tr>
</table>
<div style='border: 1px solid black;'>

</div>

<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:16px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  (TO BE USED BY TREASURY OFFICE)</b> 
        </td>
       
    </tr>
  
</table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
      Pay Rs.......................................(In Words) Rupees...............................................................................................................................................................
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
      (Superintendent Treasury)
</td>
        <td>
      Date.....................
</td>
      <td>
      (Treasury Officers )
</td>
 </tr>  
    </table>

<table style='width:100%;font-size:16px'> 

 <tr>  

     <td>
     Acount to be classified by T.O. :
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
      1. Cash
</td>
        <td>
      :..........................
</td>
 
 </tr>  
     <tr>  

     <td>
      2. G.I.S (S.F)
</td>
        <td>
      :..........................
</td>
 
 </tr> 
    
         <tr>  

     <td>
      3.  (I.F.)
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      4. LIC
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      5.  PLI
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      6.  House Rent
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      7.  Income Tax
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      8.  Surcharge
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      9.  Miscellaneous
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
        &nbsp;&nbsp;&nbsp;    **Total**
</td>
        <td>
      :..........................
</td>
 
 </tr>   
    </table>
<div style='border: 1px solid black;'>

</div>
<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:16px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  (TO BE USED BY ACCOUNTANT GENERAL OFFICE)</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:22px'> 
<tr>
     
     <td>
     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Supdt.
</td>
     </tr>
    <tr>
       <td>
     Initials of..........................................in token of
</td>
        <td>
      Admitted Rs.........................................
</td>
 
 </tr>  
 

    <tr>
     
     <td>
     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp A.A.O
</td>
     </tr>
    <tr>
       <td>
     Check of classification of
</td>
        <td>
      Objected Rs.........................................
</td>
 
 </tr>  

    <tr>
     
     <td>
   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp   500
</td>
     </tr>
     <tr>
       <td>
     Item above Rs........
</td>
      
 
 </tr>
    <tr>
     
     <td>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  5000
</td>
     </tr>  
 
    </table>


<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         Auditor 
        </td>
       
    </tr>
  
</table>
<div style='border: 1px solid black;'>

</div>
<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:16px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>INSTRUCTIONS</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         1.  A red line should be drawn right across the sheet after each section of the establishment and GRANDS TOTALS should be in red link.
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         2.  All deductions should be supported by schedules in appropriate form. There should be seperate schedules for each G.P.F series and the G.P.F. account No. be entered therein in ascending order.
</td> 
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         3.  Recovery of House Rent should be supported by rent Rolls in duplicate form the PWD/Estate Officer.Deduction adjustable by B.T. should also be supported by duplicate schedules.
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         4.  Due care should be taken to give correct code numbers wherever specified.
</td>
 </tr>  
    </table>
<div style='border: 1px solid black;'>

</div>




       
                     </body> </html>";


                MemoryStream output = new MemoryStream();

                PdfConverter pdfConverter = new PdfConverter();

                // set the license key
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                // iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 10;


                pdfConverter.PdfFooterOptions.FooterHeight = 10;

                // set the PDF page orientation (portrait or landscape) - default value is portrait
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = (PdfPageOrientation.Landscape);

                //write the page number
                TextElement footerTextElement = new TextElement(0, 30, "&p;",
                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }


                string url = "Account" + "/TR2/" + FYear + "/" + FMonth + "/";
                fileName = FYear + "_" + FMonth + ".pdf";


                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);

                // Filepath is using for storing file 
                string directory = model.FilePath + url;

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();


                var AccessPath = "/" + url + fileName;

                return "Success" + AccessPath;


            }

            catch (Exception ex)
            {

                //throw ex;
                return ex.Message;

            }
            finally
            {

            }


        }
        public string GetTR2Result(List<string> EmployeeIds, string FinacialYear, int month)
        {



            StringBuilder sb = new StringBuilder();
            sb.Append("<table style='border:1px solid black;'> <thead> <tr>");
            sb.Append("<th style='border :1px solid black;'>Sr. No.</th> <th style='border :1px solid black;'>Name & Designation <br>1</th> <th style='border :1px solid black;'>Basic Pay <br>2</th> <th style='border :1px solid black;'>Special Par<br> 3</th>");
            sb.Append(" <th style='border :1px solid black;'>Dearness Allowance <br>4</th><th style='border :1px solid black;'>Compens Allowance<br> 5</th><th style='border :1px solid black;'>H.R.A. Allowance <br>6</th><th style='border :1px solid black;'>Capital Allowance<br>7</th>");
            sb.Append(" <th style='border :1px solid black;'>Convey Allowance<br>8</th><th style='border :1px solid black;'>Washing Allowance<br>9</th><th style='border :1px solid black;'>GP<br>10</th><th style='border :1px solid black;'>Sect. Pay <br> 11</th><th style='border :1px solid black;'>12</th><th style='border :1px solid black;'>13</th><th style='border :1px solid black;'>14</th>");
            sb.Append("<th style='border :1px solid black;'>Total 2 to 14<br> 15</th><th style='border :1px solid black;'>GPF Account No<br> 16</th>");

            sb.Append("<th style='border :1px solid black;'>GPF Subs<br>17</th><th style='border :1px solid black;'>GPF Advance<br>18</th><th style='border :1px solid black;'>HBA<br>19</th> <th style='border :1px solid black;'>HBA Interest<br>20</th> <th style='border :1px solid black;'>N. Car/ Scooter Advance<br>21</th>");
            sb.Append("<th style='border :1px solid black;'>M.C./ Scooter Interest<br>22</th><th style='border :1px solid black;'>Warm cloth Advance<br>23</th><th style='border :1px solid black;'>Warm cloth Interest<br>24</th> <th style='border :1px solid black;'>Festival Advance<br>25</th>");
            sb.Append("<th style='border :1px solid black;'>L.T.C. T.A. Advance<br>26</th> <th style='border :1px solid black;'>Miscel. Recov.<br>27</th> <th style='border :1px solid black;'>28</th> <th style='border :1px solid black;'>Total (A) 17 to 28 <br>29</th>");

            sb.Append("<th style='border :1px solid black;'>Saving Fund<br>30</th> <th style='border :1px solid black;'>Insur Fund<br>31</th> <th style='border :1px solid black;'>House Rent<br>32</th> <th style='border :1px solid black;'>P.L.I.<br>33</th> <th style='border :1px solid black;'>L.I.C.<br>34</th>");
            sb.Append("<th style='border :1px solid black;'>IncomeTax<br>35</th> <th style='border :1px solid black;'>Sur-charge<br>36</th> <th style='border :1px solid black;'>OtherBT<br>37</th> <th style='border :1px solid black;'>Total (B) 30 to 37<br>38</th> <th style='border :1px solid black;'>Total Deduction <br>39</th> <th style='border :1px solid black;'>Net Payment<br>40</th>");
            sb.Append("</tr> </thead>");


            sb.Append("<tbody>");

            int count = 1;
            foreach (var item in EmployeeIds)
            {
                int EmployeeId = int.Parse(item);
                var Staff = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", EmployeeId);
                ViewBag.StaffName = Staff.StaffName + "-" + Staff.Designation;
                var Maindata = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetHPTR2Data", new tEmployeeAccountDetails { EmpId = EmployeeId, finanacialYear = FinacialYear, AccountMonth = month });
                //                System.Globalization.DateTimeFormatInfo mfi = new
                //System.Globalization.DateTimeFormatInfo();
                // string strMonthName = mfi.GetMonthName(item).ToString();
                if (Maindata != null && Maindata.Count() > 0)
                {
                    var QuerableList = Maindata.AsQueryable();
                    sb.Append("<tr>");
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                    // sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", strMonthName));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));

                    // DUES
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 5).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 6).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 7).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 8).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 9).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue));
                    // Getting total of 2-9

                    var idList = new[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
                    decimal DuesTotal = QuerableList.Where(m => idList.Contains(m.AccountFieldId)).Sum(m => m.AccountFieldValue);
                    // sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", DuesTotal));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));


                    // A.G. Deduction
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 17).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 18).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 19).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 20).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 21).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 22).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 23).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 24).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 25).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 26).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 27).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 28).FirstOrDefault().AccountFieldValue));

                    // Getting total from 17-28
                    var AGList = new[] { 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 };
                    decimal AGTotal = QuerableList.Where(m => AGList.Contains(m.AccountFieldId)).Sum(m => m.AccountFieldValue);
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", AGTotal));

                    // Treasury Deduction
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 30).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 31).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 32).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 33).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 34).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 35).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 36).FirstOrDefault().AccountFieldValue));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 37).FirstOrDefault().AccountFieldValue));

                    // Getting total from 30-37
                    var TDList = new[] { 30, 31, 32, 33, 34, 35, 36, 37 };
                    decimal TDTotal = QuerableList.Where(m => TDList.Contains(m.AccountFieldId)).Sum(m => m.AccountFieldValue);
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", TDTotal));


                    // getting total from 29-38
                    var FinalList = new[] { 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 33, 34, 35, 36, 37 };
                    decimal FinalTotal = QuerableList.Where(m => FinalList.Contains(m.AccountFieldId)).Sum(m => m.AccountFieldValue);

                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", FinalTotal));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", DuesTotal - FinalTotal));
                    sb.Append("</tr>");
                }
                else
                {
                    sb.Append("<tr>");
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", strMonthName));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append("</tr>");
                }
                count++;
            }
            return sb.ToString();
        }

        public ActionResult ArrearReport()
        {
            // Tr2Model model = new Tr2Model();
            //mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            //string FinYearValue = SiteSettingsSessn.SettingValue;
            //model.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            //model.FinancialMonthList = ModelMapping.GetMothsforinitial();
            //model.Mstaff = (List<mStaff>)Helper.ExecuteService("TR2", "GetStaff", null);



            //model.BudgetHeads = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget { FinancialYear = FinYearValue });
            return PartialView("_ArrearReport");

        }

        public ActionResult DropList(string Val)
        {
            Tr2Model model = new Tr2Model();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            model.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            model.FinancialMonthList = ModelMapping.GetMothsforinitial();
            model.Mstaff = (List<mStaff>)Helper.ExecuteService("TR2", "GetStaff", null);
            model.BudgetHeads = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget { FinancialYear = FinYearValue });

            return PartialView("_DropList", model);
        }

        public string GenerateDAArrear(string FDate, string ToDate, string StaffId, string BudgetHeadId, string TxtStatus, string CDate, string CLetterNo)
        {
            try
            {
                string outXml = "";
                //   string LOBName = FYear;
                string fileName = "";
                string savedPDF = string.Empty;
                string Ids = StaffId.TrimEnd(',');

#pragma warning disable CS0219 // The variable 'FYear' is assigned but its value is never used
                string FYear = "2015";
#pragma warning restore CS0219 // The variable 'FYear' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'FMonth' is assigned but its value is never used
                string FMonth = "4";
#pragma warning restore CS0219 // The variable 'FMonth' is assigned but its value is never used

                tEmployeeAccountDetails model = new tEmployeeAccountDetails();
                //   model.finanacialYear = FYear;
                //  model.AccountMonth = Convert.ToInt32(FMonth);
                //DateTime dateFrom = DateTime.ParseExact(FDate, "dd/MM/yyyy", null);
                //DateTime dateTo = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);


                model.BID = Convert.ToInt32(BudgetHeadId);
                string s = Ids;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');
                    model.MstaffIds = new List<string>();
                    for (int i = 0; i < values.Length; i++)
                    {
                        model.MstaffIds.Add(values[i]);
                    }

                }

                model = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetBudgetCode", model);
                //  model = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetSumforReport", model);
                decimal CompleteTotal = 0;
                DateTime d0 = DateTime.ParseExact(FDate, "dd/MM/yyyy", null);
                DateTime d1 = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);

                List<DateTime> datemonth = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                          .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
                List<Tuple<int, int>> yearmonth = new List<Tuple<int, int>>();
                foreach (DateTime x in datemonth)
                {
                    yearmonth.Add(new Tuple<int, int>(x.Year, x.Month));
                }


                List<DateTime> datemonth1 = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                      .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
                var result = new List<int>();

                foreach (var x in datemonth)
                {
                    result.Add(x.Month);
                }


                int count = 1;
                decimal GTotal = 0;
                foreach (var item in model.MstaffIds)
                {
                    GTotal = 0;
                    int EmployeeId = int.Parse(item);
                    var Staff = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", EmployeeId);
                    System.Globalization.DateTimeFormatInfo mfi = new
    System.Globalization.DateTimeFormatInfo();

                    foreach (var New in yearmonth)
                    {
                        int Year = New.Item1;
                        int Month = New.Item2;

                        var Maindata = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetDaData", new tEmployeeAccountDetails { EmpId = EmployeeId, Nww = New });
                        string strMonthName = mfi.GetMonthName(Month).ToString();
                        if (Maindata != null && Maindata.Count() > 0)
                        {
                            var YearMonth = strMonthName + " - " + Year;
                            var nameDesig = Staff.StaffName + "-" + Staff.Designation;
                            ViewBag.StaffName = Staff.StaffName + "-" + Staff.Designation;

                            var QuerableList = Maindata.AsQueryable();


                            decimal NewDA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;

                            if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {

                                decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3;

                                if (GTotal == 0)
                                {
                                    GTotal = NewDA - DAGiven;
                                }
                                else
                                {
                                    GTotal = GTotal + (NewDA - DAGiven);
                                }
                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {

                                decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2;

                                if (GTotal == 0)
                                {
                                    GTotal = NewDA - DAGiven;
                                }
                                else
                                {
                                    GTotal = GTotal + (NewDA - DAGiven);
                                }
                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {

                                decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1;

                                if (GTotal == 0)
                                {
                                    GTotal = NewDA - DAGiven;
                                }
                                else
                                {
                                    GTotal = GTotal + (NewDA - DAGiven);
                                }
                            }

                        }

                        count++;
                    }
                    if (CompleteTotal == 0)
                    {
                        CompleteTotal = GTotal;
                    }
                    else
                    {
                        CompleteTotal = CompleteTotal + GTotal;
                    }

                }


                outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                outXml += @"<div> 

<table style='width:100%;margin-left:10px;margin-top:12px;;margin-bottom:5px'>
    <tr style='width:100%;font-size:25px'>
        <td >
       H.P.T.R. 2 
        </td>
        <td style='text-align:center;font-size:25px'>
            DISNIC-TREASURY
        </td>
        <td style='text-align:right;font-size:25px'>
            NICNET-H.P
        </td>
    </tr>
  
</table>

<table style='width:100%;border: 1px solid black;border-spacing: 0;border-collapse: collapse;'> 

<tr> 

<td style='width:50%;font-size:25px'>
<< PAY BILL >>
    <br />
    DETAILED PAYBILL OF INCUMBENT OF ESTABLISHMENT OF: Secretary H.P Vidhan Sabha
    <br />
    FOR THE MONTH OF: " + TxtStatus + @"
</td>

<td style='width:50%;'>
<table style='width:100%;border-spacing: 0;border-collapse: collapse;'>
<tr>
<td style='width:30%;border: 1px solid black;'>
<table>

<tr>
<tr>
<td style='font-size:25px'>
</td>
</tr>
<td style='font-size:25px'>
Bill No.:.........
</td>
<tr>
<td style='width:100%;font-size:25px' >
Bill Date.:.........
</td>
</tr>
</tr>
</table>
</td>
<td style='width:30%;border: 1px solid black;'>
<table>
<tr>
<tr>
<td style='font-size:25px'>
</td>
</tr>
<td style='font-size:25px'>
Token No.:.........
</td>
<tr>
<td style='width:100%;font-size:25px' >
Token Date:.........
</td>
</tr>
</tr>
</table>
</td>
<td style='width:30%;border: 1px solid black;'>
<table>
<tr>
<td style='font-size:25px'>
(For Treasury Officer Use)
</td>
</tr>
<tr>
<td style='font-size:25px'>
Voucher No.:.........
</td>
<tr>
<td style='width:100%;font-size:25px' >
Voucher Date:.........
</td>
</tr>
</tr>
</table>
</td>
</tr>
</table>
</td>

</tr>  



<tr>  
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;font-size:25px'>
    <tr>
        <td> 1. Treasury Code:  </td>
            <td>" + model.TreasuryCD + @"</td>
        <td>2. Demand No:</td>
        <td>" + model.DemandNo + @"</td>
    </tr>   
    <tr>
        <td> 3. D.D.O.Code:  </td>
            <td>" + model.DDOCD + @"</td>
        <td>4. Gaztd./Non-Gaztd:</td>
        <td>" + model.Gazett + @"</td>
    </tr>
    <tr>
        <td>  5. Major Head:  </td>
            <td>" + model.MajorHead + @"</td>
     
    </tr>
    <tr>
        <td> 6. Sub-Major Head:  </td>
            <td>" + model.SubmajorHead + @"</td>
  
    </tr>
     
     <tr>
        <td> 7. Minor-Head:  </td>
            <td>" + model.MinorHead + @"</td>
    
    </tr>
     <tr>
        <td>   8. Sub Head:  </td>
            <td>" + model.SubHead + @"</td>
      
    </tr>
     <tr>
        <td> 9. Budget Code: </td>
            <td>" + model.BudgetCode + @"</td>
        <td>10. Object No:</td>
        <td>" + model.ObjectCode + @"</td>
    </tr>
     <tr>
        <td>11. Plan/Non-Plan:  </td>
            <td>" + model.Plan + @"</td>
        <td> 12. Voted/Charged:</td>
        <td>" + model.Plan + @"</td>
    </tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>
<tr>
<td style='width:100%;'>
<table style='width:100%;font-size:25px''>
    <tr>
        <td>  1. GROSS TOTAL:  </td>
        <td></td>
            <td>Rs " + CompleteTotal + @"</td>
        
    </tr>   
    <tr>
        <td>  2. SHORT DRAWAL:  </td>
         <td></td>
            <td>Rs Nill</td>
        
    </tr>
    <tr>
        <td> 3. TOTAL AMOUNT:</td>
         <td>(1-2)</td>
            <td>Rs " + CompleteTotal + @"</td>
     
    </tr>
    <tr>
        <td>4. A.G DEDUCTIONS  (A):  </td>
         <td></td>
            <td>Rs " + model.TotalDeducA + @"</td>
  
    </tr>
     <tr>
        <td> 5. BALANCE AMOUNT:  </td>
         <td>(3-4) </td>
            <td> Rs " + model.BalanceAmount + @"</td>
     
    </tr>
     <tr>
        <td> 6. B.T.DEDUCTIONS:  (B):  </td>
          <td></td>
            <td>Rs " + model.TotalDeducB + @"</td>
    
    </tr>
     <tr>
        <td>  7. NET AMOUNT (payble):  </td>
          <td>(5-6)</td>
            <td>Rs " + CompleteTotal + @"</td>
      
    </tr>
     
</table>
</td>
</tr>
</table>
</td>
</tr>   <!-- end 2 row -->


<tr>  <!-- Start 3 row -->
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;;font-size:25px'>    
<tr>
<td style='width:50%;border: 1px solid black;'>
       <span>&nbsp&nbsp&nbsp SUB-OBJECT(DETAILED) HEADS</span>
    <table style='width:100%;font-size:25px''>
     
    <tr>
        <td> 1. Basic Pay:  </td>
        <td>01</td>
            <td>Rs " + model.BasicPay + @"</td>
        
    </tr>
          <tr>
        <td> 2. Special Pay:  </td>
        <td>02</td>
            <td>Rs " + model.SpecialPay + @"</td>
        
    </tr>   
         <tr>
        <td> 3. Dearness Allow:  </td>
        <td>03</td>
            <td>Rs " + CompleteTotal + @"</td>
        
    </tr>   
         <tr>
        <td> 4. Compensatory Allow:  </td>
        <td>04</td>
            <td>Rs " + model.CompenseAllowns + @"</td>
        
    </tr>   
         <tr>
        <td> 5. House Rent Allow:  </td>
        <td>05</td>
            <td>Rs " + model.HRAAllowns + @"</td>
        
    </tr>   
         <tr>
        <td> 6. Capital Allowance: </td>
        <td>06</td>
            <td>Rs " + model.CapitalAllowns + @"</td>
        
    </tr>   
        <tr>
        <td>  7. Conveyance Allow: </td>
        <td>07</td>
            <td>Rs " + model.ConveyaceAllowns + @"</td>
        
    </tr>   
        <tr>
        <td> 8. Washing Allowance: </td>
        <td>08</td>
            <td>Rs " + model.WashingAllowns + @".</td>
        
    </tr>   
        <tr>
        <td>  9. : </td>
        <td>.......</td>
            <td>Rs........</td>
        
    </tr>   
          <tr>
        <td>  10. : </td>
        <td>G.P</td>
            <td>Rs  " + model.GP + @"</td>
        
    </tr>   
          <tr>
        <td>  11. : </td>
        <td>Sect. Pay</td>
            <td>Rs " + model.SectPay + @"</td>
        
    </tr>   
          <tr>
        <td>  12. : </td>
        <td>.......</td>
            <td>Rs........</td>
        
    </tr>   
          <tr>
        <td>  13. : </td>
        <td>.......</td>
            <td>Rs........</td>
        
    </tr>   
        </table>
</td>
    <td style='width:50%; border: 1px solid black;'>
<span>&nbsp&nbsp&nbsp (A) DEDUCTIONS (CLASSIFIED BY A.G)</span>
<table style='width:100%;font-size:25px''>
    <tr>
        <td>  1. GPS Subscription:  </td>
        <td></td>
            <td>Rs " + model.GPFSubs + @"</td>
        
    </tr>
          <tr>
        <td> 2. GPF Advance Recovery:  </td>
        <td></td>
            <td>Rs " + model.GPFAdvans + @"</td>
        
    </tr>   
         <tr>
        <td> 3. House Building Advance:  </td>
        <td></td>
            <td>Rs " + model.HBA + @"</td>
        
    </tr>   
         <tr>
        <td> 4. ' '  Intrest:  </td>
        <td></td>
            <td>Rs " + model.HBAInterest + @"</td>
        
    </tr>   
         <tr>
        <td> 5. M.Car/Scooter Advance:  </td>
        <td></td>
            <td>Rs " + model.NCarScooterAdvance + @"</td>
        
    </tr>   
         <tr>
        <td> 6. ' '  Intrest: </td>
        <td></td>
            <td>Rs " + model.MCScooterInterest + @"</td>
        
    </tr>   
        <tr>
        <td>  7. Warm Cloth Advance: </td>
        <td>.......</td>
            <td>Rs " + model.WarmclothAdvance + @"</td>
        
    </tr>   
        <tr>
        <td> 8. ' '  Intrest: </td>
        <td></td>
            <td>Rs " + model.WarmclothInterest + @"</td>
        
    </tr>   
        <tr>
        <td>  9. L.T.C./T.A Advance: </td>
        <td></td>
            <td>Rs " + model.LTCTAAdvance + @"</td>
        
    </tr>   
          <tr>
        <td>  10. Festival Advance: </td>
        <td></td>
            <td>Rs " + model.FestivalAdvance + @"</td>
        
    </tr>   
          <tr>
        <td> 11. Miscellaneous Recovery: </td>
        <td></td>
            <td>Rs " + model.MiscelRecov + @"</td>
        
    </tr>   
          <tr>
        <td>  12.  </td>
        <td></td>
            <td>Rs........</td>
        
    </tr>   
       
        </table>
</td>
</tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;font-size:25px'>
  
<tr>
    
<td style='width:100%;'>
    <span>(B) DEDUCTIONS(CLASSIFIED BY T.O):</span>    
 <span >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp< * CORRESPONDING RECEIPTS CODES * ></span> 
    <table style='width:100%;font-size:25px'>


   <tr>
        <td ></td>     
            <td ></td>
           <td style='border: 1px solid black;'>Major</td>
           <td style='border: 1px solid black;'>S.Maj</td>
           <td style='border: 1px solid black;'>Minor</td>
           <td style='border: 1px solid black;'>S.Head</td>
           <td style='border: 1px solid black;'>000 Code</td>

        
    </tr>
    <tr>
        <td >   1. Insurance fund:  </td>     
            <td>Rs " + model.InsurFund + @"</td>
 <td style='border: 1px solid black;'>8011</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>105</td>
           <td style='border: 1px solid black;'>01</td>
           <td style='border: 1px solid black;'></td>
        
    </tr>
          <tr>
        <td> 2. Savings Fund:  </td>        
            <td>Rs " + model.SavingFund + @"</td>
  <td style='border: 1px solid black;'>8011</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>105</td>
           <td style='border: 1px solid black;'>02</td>
                <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td> 3. House Rent:  </td>
            <td>Rs " + model.HouseRent + @"</td>
  <td style='border: 1px solid black;'>0216</td>
           <td style='border: 1px solid black;'>01</td>
           <td style='border: 1px solid black;'>106</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td>  4.Postal Life Insurance:</td>
            <td>Rs " + model.PLI + @"</td>
    <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>103</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td> 5.Life Insurance Corp:  </td>
            <td>Rs " + model.LIC + @"</td>
    <td style='border: 1px solid black;'>8448</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>104</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td>  6.Income tax: </td>
            <td>Rs " + model.IncomeTax + @"</td>
   <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>112</td>
           <td style='border: 1px solid black;'>01</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td> 7. Surcharge: </td>
            <td>Rs " + model.Surcharge + @"</td>
    <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>112</td>
           <td style='border: 1px solid black;'>02</td>
              <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td>8. Other B.T |: </td>
            <td>Rs " + model.OtherBT + @"</td>
   <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td> 9. Other B.T ||: </td>
            <td>Rs........</td>
         <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
    </tr>   
          <tr>
        <td>    10. Other B.T |||: </td>
            <td>Rs........</td>
         <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
    </tr>   
        
        
       
        </table>
</td>
</tr>
</table>
</td>
</tr>  

    <tr>  
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>    
<tr>
<td style='width:50%;border: 1px solid black;'>
    <table style='width:100%';font-size:25px'>
     
    <tr>
        <td style='font-size:25px'>GROSS TOTAL </td>
        <td></td>
            <td style='font-size:25px'>Rs " + CompleteTotal + @"</td>
        
    </tr>
           
             
        </table>
</td>
    <td style='width:50%; border: 1px solid black;'>
<table style='width:100%'>
    <tr>
        <td style='font-size:25px'> TOTAL (A)  </td>
        <td></td>
            <td style='font-size:25px'>Rs " + model.TotalDeducA + @"</td>
        
    </tr>
              
        </table>
</td>
</tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>
  
<tr>
    
<td style='width:100%;'>
 <table style='width:100%'>
    <tr>
        <td style='font-size:25px'> TOTAL (B)  </td>
        <td></td>
            <td style='font-size:25px'>Rs " + model.TotalDeducB + @"</td>
        
    </tr>
              
        </table>
</td>
</tr>
</table>
</td>
</tr>

</table>
<table style='width:100%'>
    <tr style='width:100%'>
        <td style='width:25%'>
            <table style='width:100%;font-size:25px''>
                <tr style='width:100%;'>
                    <td >
                        Station
                    </td>
                     <td style='margin-top:10px'>
----------------
                    </td>

                </tr>

                <tr style='width:100%'>
                    <td>
                        Date
                    </td>
                     <td>
----------------
                    </td>
                </tr>
            </table>
        </td>
        <td style='width:25%;font-size:25px'>
            (Treasury Clerk)
        </td>
        <td style='width:50%'>
            <table style='width:100%;font-size:25px'>
                <tr style='width:100%'>
                    <td>
                        (Signature & Designation of Drawing Officer)
                    </td>
                     

                </tr>

                <tr style='width:100%'>
                    <td>
                        Code:
                    </td>
                     
                </tr>
            </table>


</tr>

</table>  

        <div style='page-break-after: always;'></div>
<table>
<tr>
" + GetDAResult(model.MstaffIds, FDate, ToDate) + @"

</tr>

</table>

</table>  
   <div style='page-break-after: always;'></div>

    <table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  CERTIFICATS</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
         1.  The Drawl is being made on account of. " + TxtStatus + @" sanctioned vide letter no." + CLetterNo + @" .Dtd." + CDate + @"
</td>
 </tr>  
    </table>
<table style='font-size:22px'> 
    <tr>
      <td>
         2.  Arrears were less drawn vide T/V No......................................
</td>

 </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         3.  Certified that pay ans allowances drawn in this bill are due ans admissible as per authority is force and the deductions where-ever required have been made, as per the rules.
</td>
         </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         4.  Certified that in case of fresh appointees , the medical fitness certicates have been obtained.

</td>
 </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         5.  Certified that all appointments and promotions, grant of leave and period of suspensions, and the deputation and other events which are required to be recorded, have been recorded in the
         service Book & leave account of the concerned employee.
</td>
         </tr>
        </table>
  <table style='font-size:22px'> 
    <tr>
     <td>
       
         6.  Recieved contents (in Cash)Rs. " + model.NetAmount + @"(in Words) Rupees." + NumberToWordsConverter.NumbersToWords(Convert.ToInt64(model.NetAmount)) + @"
</td>
 </tr>
        </table>

<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:right'>
             (Signature & Designation of Drawing and Disbursing Officer)
        </td>
       
        
    </tr>
  <tr style='width:100%'>
       
        <td style='text-align:right;padding-right:306px'>
             Code No.
        </td>  
    </tr>
</table>
<div style='border: 1px solid black;'>

</div>

<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:16px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  (TO BE USED BY TREASURY OFFICE)</b> 
        </td>
       
    </tr>
  
</table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
      Pay Rs.......................................(In Words) Rupees...............................................................................................................................................................
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
      (Superintendent Treasury)
</td>
        <td>
      Date.....................
</td>
      <td>
      (Treasury Officers )
</td>
 </tr>  
    </table>

<table style='width:100%;font-size:16px'> 

 <tr>  

     <td>
     Acount to be classified by T.O. :
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
      1. Cash
</td>
        <td>
      :..........................
</td>
 
 </tr>  
     <tr>  

     <td>
      2. G.I.S (S.F)
</td>
        <td>
      :..........................
</td>
 
 </tr> 
    
         <tr>  

     <td>
      3.  (I.F.)
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      4. LIC
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      5.  PLI
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      6.  House Rent
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      7.  Income Tax
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      8.  Surcharge
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      9.  Miscellaneous
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
        &nbsp;&nbsp;&nbsp;    **Total**
</td>
        <td>
      :..........................
</td>
 
 </tr>   
    </table>
<div style='border: 1px solid black;'>

</div>
<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:16px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  (TO BE USED BY ACCOUNTANT GENERAL OFFICE)</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:22px'> 
<tr>
     
     <td>
     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Supdt.
</td>
     </tr>
    <tr>
       <td>
     Initials of..........................................in token of
</td>
        <td>
      Admitted Rs.........................................
</td>
 
 </tr>  
 

    <tr>
     
     <td>
     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp A.A.O
</td>
     </tr>
    <tr>
       <td>
     Check of classification of
</td>
        <td>
      Objected Rs.........................................
</td>
 
 </tr>  

    <tr>
     
     <td>
   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp   500
</td>
     </tr>
     <tr>
       <td>
     Item above Rs........
</td>
      
 
 </tr>
    <tr>
     
     <td>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  5000
</td>
     </tr>  
 
    </table>


<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         Auditor 
        </td>
       
    </tr>
  
</table>
<div style='border: 1px solid black;'>

</div>
<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:16px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>INSTRUCTIONS</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         1.  A red line should be drawn right across the sheet after each section of the establishment and GRANDS TOTALS should be in red link.
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         2.  All deductions should be supported by schedules in appropriate form. There should be seperate schedules for each G.P.F series and the G.P.F. account No. be entered therein in ascending order.
</td> 
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         3.  Recovery of House Rent should be supported by rent Rolls in duplicate form the PWD/Estate Officer.Deduction adjustable by B.T. should also be supported by duplicate schedules.
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         4.  Due care should be taken to give correct code numbers wherever specified.
</td>
 </tr>  
    </table>
<div style='border: 1px solid black;'>

</div>

 
<div style='page-break-after: always;'></div>
<table>
<tr>
" + GetTR2DAResult(model.MstaffIds, FDate, ToDate) + @"

</tr>
       
                     </body> </html>";


                MemoryStream output = new MemoryStream();

                PdfConverter pdfConverter = new PdfConverter();

                // set the license key
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                // iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 10;


                pdfConverter.PdfFooterOptions.FooterHeight = 10;

                // set the PDF page orientation (portrait or landscape) - default value is portrait
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = (PdfPageOrientation.Landscape);

                //write the page number
                TextElement footerTextElement = new TextElement(0, 30, "&p;",
                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }


                //string url = "Account" + "/TR2/" + FYear + "/" + FMonth + "/";
                //fileName = FYear + "_" + FMonth + ".pdf";


                string url = "Account" + "/TR2/" + FDate + "/";
                fileName = "Test" + ".pdf";
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);

                // Filepath is using for storing file 
                string directory = model.FilePath + url;

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();


                var AccessPath = "/" + url + fileName;

                return "Success" + AccessPath;


            }

            catch (Exception ex)
            {

                //throw ex;
                return ex.Message;

            }
            finally
            {

            }


        }



        public string GetTR2DAResult(List<string> EmployeeIds, string FDate, string ToDate)
        {
            decimal CompleteTotal = 0;
            DateTime d0 = DateTime.ParseExact(FDate, "dd/MM/yyyy", null);
            DateTime d1 = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);

            List<DateTime> datemonth = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                      .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
            List<Tuple<int, int>> yearmonth = new List<Tuple<int, int>>();
            foreach (DateTime x in datemonth)
            {
                yearmonth.Add(new Tuple<int, int>(x.Year, x.Month));
            }


            List<DateTime> datemonth1 = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                  .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
            var result = new List<int>();

            foreach (var x in datemonth)
            {
                result.Add(x.Month);
            }


            StringBuilder sb = new StringBuilder();
            sb.Append("<table style='width=100%, border:1px solid black;'> <thead> <tr >");
            sb.Append("<th style='border :1px solid black;width:5%'>Sr. No.</th><th style='border :1px solid black;width:11%'>Month</th>  <th style='border :1px solid black;width:11%'>Name & Designation <br>1</th> <th style='border :1px solid black;width:11%'>Basic Pay <br>2</th> <th style='border :1px solid black;width:11%'>Special Pay<br> 3</th>");
            sb.Append(" <th style='border :1px solid black;width:11%'>G.P <br>4</th><th style='border :1px solid black;width:11%'>Sectt. Pay<br> 5</th><th style='border :1px solid black;width:11%'>D.A Due <br>6</th><th style='border :1px solid black;width:11%'>D.A Drawn<br>7</th> <th style='border :1px solid black;width:11%'>Difference<br>8</th>");
            sb.Append("<th style='border :1px solid black;width:11%'>Total</th>");
            sb.Append("</tr> </thead>");
            sb.Append("<tbody>");

            int count = 1;
            decimal GTotal = 0;

            foreach (var item in EmployeeIds)
            {
                GTotal = 0;
                int EmployeeId = int.Parse(item);
                var Staff = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", EmployeeId);
                System.Globalization.DateTimeFormatInfo mfi = new
System.Globalization.DateTimeFormatInfo();

                foreach (var New in yearmonth)
                {
                    int Year = New.Item1;
                    int Month = New.Item2;

                    var Maindata = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetDaData", new tEmployeeAccountDetails { EmpId = EmployeeId, Nww = New });
                    string strMonthName = mfi.GetMonthName(Month).ToString();
                    if (Maindata != null && Maindata.Count() > 0)
                    {
                        sb.Append("<tr>");
                        sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", count));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", strMonthName + " - " + Year));
                        ViewBag.StaffName = Staff.StaffName + "-" + Staff.Designation;

                        var QuerableList = Maindata.AsQueryable();
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));

                        // DUES
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue));
                        //var idList = new[] { 4 };
                        //decimal NewDA = QuerableList.Where(m => idList.Contains(m.AccountFieldId)).Sum(m => m.AccountFieldValue);
                        decimal NewDA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                        if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                        {
                            sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3));
                            decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3;
                            sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", NewDA - DAGiven));
                            if (GTotal == 0)
                            {
                                GTotal = NewDA - DAGiven;
                            }
                            else
                            {
                                GTotal = GTotal + (NewDA - DAGiven);
                            }
                        }
                        else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                        {
                            sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2));
                            decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2;
                            sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", NewDA - DAGiven));
                            if (GTotal == 0)
                            {
                                GTotal = NewDA - DAGiven;
                            }
                            else
                            {
                                GTotal = GTotal + (NewDA - DAGiven);
                            }
                        }
                        else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                        {
                            sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1));
                            decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1;
                            sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", NewDA - DAGiven));
                            if (GTotal == 0)
                            {
                                GTotal = NewDA - DAGiven;
                            }
                            else
                            {
                                GTotal = GTotal + (NewDA - DAGiven);
                            }
                        }
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", GTotal));
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", strMonthName + " - " + Year));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append("</tr>");

                    }

                    count++;
                }

                sb.Append("<tr>");
                sb.Append(string.Format("<td style='border :1px solid black;width:11%' colspan='10'>Total </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", GTotal));
                if (CompleteTotal == 0)
                {
                    CompleteTotal = GTotal;
                }
                else
                {
                    CompleteTotal = CompleteTotal + GTotal;
                }


            }
            sb.Append("<tr>");
            sb.Append(string.Format("<td style='border :1px solid black; width:11%' colspan='10'>Complete Total </td>"));
            sb.Append(string.Format("<td style='border :1px solid black; width:11%'>{0} </td>", CompleteTotal));

            return sb.ToString();
        }


        public string GetDAResult(List<string> EmployeeIds, string FDate, string ToDate)
        {

            decimal CompleteTotal = 0;
            DateTime d0 = DateTime.ParseExact(FDate, "dd/MM/yyyy", null);
            DateTime d1 = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);

            List<DateTime> datemonth = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                      .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
            List<Tuple<int, int>> yearmonth = new List<Tuple<int, int>>();
            foreach (DateTime x in datemonth)
            {
                yearmonth.Add(new Tuple<int, int>(x.Year, x.Month));
            }


            List<DateTime> datemonth1 = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                  .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
            var result = new List<int>();

            foreach (var x in datemonth)
            {
                result.Add(x.Month);
            }


            StringBuilder sb = new StringBuilder();
            sb.Append("<table style='border:1px solid black;'> <thead> <tr>");
            sb.Append("<th style='border :1px solid black;'>Sr. No.</th> <th style='border :1px solid black;'>Name & Designation <br>1</th> <th style='border :1px solid black;'>Basic Pay <br>2</th> <th style='border :1px solid black;'>Special Par<br> 3</th>");
            sb.Append(" <th style='border :1px solid black;'>Dearness Allowance <br>4</th><th style='border :1px solid black;'>Compens Allowance<br> 5</th><th style='border :1px solid black;'>H.R.A. Allowance <br>6</th><th style='border :1px solid black;'>Capital Allowance<br>7</th>");
            sb.Append(" <th style='border :1px solid black;'>Convey Allowance<br>8</th><th style='border :1px solid black;'>Washing Allowance<br>9</th><th style='border :1px solid black;'>GP<br>10</th><th style='border :1px solid black;'>Sect. Pay <br> 11</th><th style='border :1px solid black;'>12</th><th style='border :1px solid black;'>13</th><th style='border :1px solid black;'>14</th>");
            sb.Append("<th style='border :1px solid black;'>Total 2 to 14<br> 15</th><th style='border :1px solid black;'>GPF Account No<br> 16</th>");

            sb.Append("<th style='border :1px solid black;'>GPF Subs<br>17</th><th style='border :1px solid black;'>GPF Advance<br>18</th><th style='border :1px solid black;'>HBA<br>19</th> <th style='border :1px solid black;'>HBA Interest<br>20</th> <th style='border :1px solid black;'>N. Car/ Scooter Advance<br>21</th>");
            sb.Append("<th style='border :1px solid black;'>M.C./ Scooter Interest<br>22</th><th style='border :1px solid black;'>Warm cloth Advance<br>23</th><th style='border :1px solid black;'>Warm cloth Interest<br>24</th> <th style='border :1px solid black;'>Festival Advance<br>25</th>");
            sb.Append("<th style='border :1px solid black;'>L.T.C. T.A. Advance<br>26</th> <th style='border :1px solid black;'>Miscel. Recov.<br>27</th> <th style='border :1px solid black;'>28</th> <th style='border :1px solid black;'>Total (A) 17 to 28 <br>29</th>");

            sb.Append("<th style='border :1px solid black;'>Saving Fund<br>30</th> <th style='border :1px solid black;'>Insur Fund<br>31</th> <th style='border :1px solid black;'>House Rent<br>32</th> <th style='border :1px solid black;'>P.L.I.<br>33</th> <th style='border :1px solid black;'>L.I.C.<br>34</th>");
            sb.Append("<th style='border :1px solid black;'>IncomeTax<br>35</th> <th style='border :1px solid black;'>Sur-charge<br>36</th> <th style='border :1px solid black;'>OtherBT<br>37</th> <th style='border :1px solid black;'>Total (B) 30 to 37<br>38</th> <th style='border :1px solid black;'>Total Deduction <br>39</th> <th style='border :1px solid black;'>Net Payment<br>40</th>");
            sb.Append("</tr> </thead>");


            sb.Append("<tbody>");



            int count = 1;
            decimal GTotal = 0;

            foreach (var item in EmployeeIds)
            {
                GTotal = 0;
                int EmployeeId = int.Parse(item);
                var Staff = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", EmployeeId);
                System.Globalization.DateTimeFormatInfo mfi = new
System.Globalization.DateTimeFormatInfo();

                foreach (var New in yearmonth)
                {
                    int Year = New.Item1;
                    int Month = New.Item2;

                    var Maindata = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetDaData", new tEmployeeAccountDetails { EmpId = EmployeeId, Nww = New });
                    string strMonthName = mfi.GetMonthName(Month).ToString();
                    if (Maindata != null && Maindata.Count() > 0)
                    {
                        var YearMonth = strMonthName + " - " + Year;
                        var nameDesig = Staff.StaffName + "-" + Staff.Designation;
                        ViewBag.StaffName = Staff.StaffName + "-" + Staff.Designation;

                        var QuerableList = Maindata.AsQueryable();


                        decimal NewDA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;

                        if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                        {
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3));
                            decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3;

                            if (GTotal == 0)
                            {
                                GTotal = NewDA - DAGiven;
                            }
                            else
                            {
                                GTotal = GTotal + (NewDA - DAGiven);
                            }
                        }
                        else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                        {

                            decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2;

                            if (GTotal == 0)
                            {
                                GTotal = NewDA - DAGiven;
                            }
                            else
                            {
                                GTotal = GTotal + (NewDA - DAGiven);
                            }
                        }
                        else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                        {

                            decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1;

                            if (GTotal == 0)
                            {
                                GTotal = NewDA - DAGiven;
                            }
                            else
                            {
                                GTotal = GTotal + (NewDA - DAGiven);
                            }
                        }

                    }
                    if (CompleteTotal == 0)
                    {
                        CompleteTotal = GTotal;
                    }
                    else
                    {
                        CompleteTotal = CompleteTotal + GTotal;
                    }
                    count++;
                }
                sb.Append("<tr>");
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", GTotal));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", GTotal));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append("</tr>");


            }



            return sb.ToString();
        }


        public ActionResult PayArrearDropList(string Val)
        {
            Tr2Model model = new Tr2Model();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            model.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            model.FinancialMonthList = ModelMapping.GetMothsforinitial();
            model.Mstaff = (List<mStaff>)Helper.ExecuteService("TR2", "GetStaff", null);
            model.BudgetHeads = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget { FinancialYear = FinYearValue });

            return PartialView("_PayDropDown", model);
        }


        public string GeneratePayArrear(string FDate, string ToDate, string StaffId, string BudgetHeadId, string TxtStatus, string CDate, string CLetterNo)
        {
            try
            {
                string outXml = "";
                //   string LOBName = FYear;
                string fileName = "";
                string savedPDF = string.Empty;
                string Ids = StaffId.TrimEnd(',');


                tEmployeeAccountDetails model = new tEmployeeAccountDetails();


                model.BID = Convert.ToInt32(BudgetHeadId);
                string s = Ids;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');
                    model.MstaffIds = new List<string>();
                    for (int i = 0; i < values.Length; i++)
                    {
                        model.MstaffIds.Add(values[i]);
                    }

                }

                model = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetBudgetCode", model);
                //  model = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetSumforReport", model);
#pragma warning disable CS0219 // The variable 'empid' is assigned but its value is never used
                int empid = 0;
#pragma warning restore CS0219 // The variable 'empid' is assigned but its value is never used
                decimal CompleteTotal = 0;
                decimal DaysDA = 0;
                decimal DaysBasic = 0;
                decimal DaysSectt = 0;
                decimal DaysGP = 0;
                decimal DaysSpecial = 0;
                decimal CompNew = 0;
                decimal CompOld = 0;
                decimal Difference = 0;
                decimal TotalPay = 0;
                decimal DifOld = 0;
                decimal B1 = 0;
                decimal B2 = 0;
                decimal B3 = 0;
                decimal B4 = 0;
                decimal B5 = 0;
                DateTime d0 = DateTime.ParseExact(FDate, "dd/MM/yyyy", null);
                DateTime d1 = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);
                List<DateTime> datemonth = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                          .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
                List<Tuple<int, int>> yearmonth = new List<Tuple<int, int>>();
                foreach (DateTime x in datemonth)
                {
                    yearmonth.Add(new Tuple<int, int>(x.Year, x.Month));
                }


                List<DateTime> datemonth1 = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                      .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
                var result = new List<int>();

                foreach (var x in datemonth)
                {
                    result.Add(x.Month);
                }

                int count = 1;


                foreach (var item in model.MstaffIds)
                {
                    decimal GTotal = 0;
                    int EmployeeId = int.Parse(item);
                    var Staff = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", EmployeeId);
                    System.Globalization.DateTimeFormatInfo mfi = new
    System.Globalization.DateTimeFormatInfo();



                    foreach (var New in yearmonth)
                    {
                        int Year = New.Item1;
                        int Month = New.Item2;

                        var Maindata = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetPayData", new tEmployeeAccountDetails { EmpId = EmployeeId, Nww = New, FromDate = d0 });

                        string strMonthName = mfi.GetMonthName(Month).ToString();
                        var firstDayOfMonth = new DateTime(Year, Month, 1);
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                        var First = FDate;
                        var Second = Convert.ToDateTime(lastDayOfMonth).ToString("dd/MM/yyyy");
                        if (Maindata != null && Maindata.Count() > 0)
                        {
                            var QuerableList = Maindata.AsQueryable();
                            //sb.Append("<tr>");
                            //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", count));
                            if (Maindata.First().ToDate != null)
                            {
                                //   sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Convert.ToDateTime(Maindata.First().FromDate.Value).ToString("dd/MM/yyyy") + " To " + Convert.ToDateTime(Maindata.First().ToDate.Value).ToString("dd/MM/yyyy")));
                            }
                            else
                            {
                                //     sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Convert.ToDateTime(firstDayOfMonth).ToString("dd/MM/yyyy") + " To " + Convert.ToDateTime(lastDayOfMonth).ToString("dd/MM/yyyy")));
                            }
                            ViewBag.StaffName = Staff.StaffName + "-" + Staff.Designation;
                            // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));

                            // DUES

                            if (Maindata.First().ToDate != null)
                            {
                                int DaysInMonth = DateTime.DaysInMonth(Year, Month);
                                int DaysDiff = int.Parse((Maindata.First().ToDate.Value - Maindata.First().FromDate.Value).TotalDays.ToString()) + 1;
                                //Basic Pay
                                decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue;
                                decimal BasicPerDay = (BayGiven / DaysInMonth);
                                DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));
                                //Sectt Pay

                                decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue;
                                decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                //    sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));
                                //GP Pay
                                decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue;
                                decimal GPPerDay = (GP / DaysInMonth);
                                DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                                //Special Pay
                                decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue;
                                decimal SpecialPerDay = (Special / DaysInMonth);
                                DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));
                                //DA Pay
                                decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                                decimal DAPerDay = (DA / DaysInMonth);
                                DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                                CompNew = DaysDA + DaysBasic + DaysSectt + DaysGP + DaysSpecial;
                                //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", CompNew));
                                //Difference = TotalPay - Comp;
                                //sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Difference));
                                //sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Difference));
                            }
                            else
                            {

                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue));
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue));
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue));
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue));
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue));
                                TotalPay = (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue);
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", TotalPay));
                            }
                            if (Maindata.First().ToDate != null)
                            {
                                int DaysInMonth = DateTime.DaysInMonth(Year, Month);
                                int DaysDiff = int.Parse((Maindata.First().ToDate.Value - Maindata.First().FromDate.Value).TotalDays.ToString()) + 1;
                                //Basic Pay
                                if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                                {
                                    decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3;
                                    decimal BasicPerDay = (BayGiven / DaysInMonth);
                                    DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                    //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                                }

                                else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                                {
                                    decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2;
                                    decimal BasicPerDay = (BayGiven / DaysInMonth);
                                    DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                    //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                                {
                                    decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1;
                                    decimal BasicPerDay = (BayGiven / DaysInMonth);
                                    DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                    //     sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                                {
                                    decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue;
                                    decimal BasicPerDay = (BayGiven / DaysInMonth);
                                    DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                    //     sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                                }
                                //Sectt Pay
                                if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                                {
                                    decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3;
                                    decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                    DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                    //     sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                                }

                                else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                                {
                                    decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2;
                                    decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                    DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                    //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                                {
                                    decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1;
                                    decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                    DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                    //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue != 0)
                                {
                                    decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue;
                                    decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                    DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                    //    sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                                }
                                //GP Pay
                                if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                                {
                                    decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3;
                                    decimal GPPerDay = (GP / DaysInMonth);
                                    DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                                }

                                else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                                {
                                    decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2;
                                    decimal GPPerDay = (GP / DaysInMonth);
                                    DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                    //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                                {
                                    decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1;
                                    decimal GPPerDay = (GP / DaysInMonth);
                                    DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                    //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                                {
                                    decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue;
                                    decimal GPPerDay = (GP / DaysInMonth);
                                    DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                    ///  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                                }

                                //Special Pay
                                if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                                {
                                    decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3;
                                    decimal SpecialPerDay = (Special / DaysInMonth);
                                    DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                    //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                                }

                                else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                                {
                                    decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2;
                                    decimal SpecialPerDay = (Special / DaysInMonth);
                                    DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                    //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                                {
                                    decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1;
                                    decimal SpecialPerDay = (Special / DaysInMonth);
                                    DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                    //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                                {
                                    decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue;
                                    decimal SpecialPerDay = (Special / DaysInMonth);
                                    DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                    //    sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                                }

                                //DA Pay
                                if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                                {
                                    decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3;
                                    decimal DAPerDay = (DA / DaysInMonth);
                                    DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                    //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                                }

                                else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                                {
                                    decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2;
                                    decimal DAPerDay = (DA / DaysInMonth);
                                    DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                                {
                                    decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1;
                                    decimal DAPerDay = (DA / DaysInMonth);
                                    DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                    //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                                {
                                    decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                                    decimal DAPerDay = (DA / DaysInMonth);
                                    DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                    //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                                }
                                CompOld = DaysDA + DaysBasic + DaysSectt + DaysGP + DaysSpecial;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", CompOld));
                                Difference = CompNew - CompOld;
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Difference));
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Difference));
                            }
                            else
                            {
                                if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                                {
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3));
                                    B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3;
                                }

                                else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                                {

                                    B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2;
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                                {
                                    B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1;
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                                {
                                    B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue;
                                    //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue));

                                }

                                if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                                {
                                    B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3;
                                    //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3));

                                }

                                else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                                {
                                    B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2;
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                                {
                                    B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1;
                                    //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                                {
                                    B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue;
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue));

                                }

                                if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                                {
                                    B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3;
                                    //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3));

                                }

                                else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                                {
                                    B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2;
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                                {
                                    B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1;
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                                {
                                    B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue;
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue));

                                }

                                if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                                {
                                    B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3;
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3));

                                }

                                else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                                {
                                    B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2;
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                                {
                                    B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1;
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                                {
                                    B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue;
                                    //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue));

                                }


                                if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                                {
                                    B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3;
                                    // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3));

                                }

                                else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                                {
                                    B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2;
                                    //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                                {
                                    B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1;
                                    //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1));

                                }
                                else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                                {
                                    B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                                    //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue));

                                }

                                decimal Sum = B1 + B2 + B3 + B4 + B5;
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Sum));
                                DifOld = TotalPay - Sum;
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DifOld));
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DifOld));
                            }
                            if (GTotal == 0)
                            {
                                GTotal = Difference + DifOld;
                            }
                            else
                            {
                                GTotal = GTotal + DifOld;
                            }
                        }
                        else
                        {



                            //sb.Append("<tr>");
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", strMonthName + " - " + Year));
                            //if (empid != EmployeeId)
                            //{
                            //    empid = EmployeeId;
                            //    sb.Append(string.Format("<td style='border :1px solid black;'>{0} </td>", First + " To " + Second));
                            //    //   sb.Append(string.Format("<td style='border :1px solid black;>{0} </td>", First + " To " + Second));
                            //}
                            //else
                            //{
                            //    sb.Append(string.Format("<td style='border :1px solid black;'>{0} </td>", Convert.ToDateTime(firstDayOfMonth).ToString("dd/MM/yyyy") + " To " + Convert.ToDateTime(lastDayOfMonth).ToString("dd/MM/yyyy")));
                            //}
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                            //sb.Append("</tr>");

                        }
                        count++;

                    }



                    //sb.Append("<tr>");
                    //sb.Append(string.Format("<td style='border :1px solid black;width:5%' colspan='16'>Total </td>"));
                    //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", GTotal));
                    if (CompleteTotal == 0)
                    {
                        CompleteTotal = GTotal;
                    }
                    else
                    {
                        CompleteTotal = CompleteTotal + GTotal;
                    }


                }
                //sb.Append("<tr>");
                //sb.Append(string.Format("<td style='border :1px solid black;width:5%' colspan='16'>Gross Total </td>"));
                //sb.Append(string.Format("<td style='border :1px solid black; width:5%'>{0} </td>", CompleteTotal));




                outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                outXml += @"<div> 

<table style='width:100%;margin-left:10px;margin-top:12px;;margin-bottom:5px'>
    <tr style='width:100%;font-size:25px'>
        <td >
       H.P.T.R. 2 
        </td>
        <td style='text-align:center;font-size:25px'>
            DISNIC-TREASURY
        </td>
        <td style='text-align:right;font-size:25px'>
            NICNET-H.P
        </td>
    </tr>
  
</table>

<table style='width:100%;border: 1px solid black;border-spacing: 0;border-collapse: collapse;'> 

<tr> 

<td style='width:50%;font-size:25px'>
<< PAY BILL >>
    <br />
    DETAILED PAYBILL OF INCUMBENT OF ESTABLISHMENT OF: Secretary H.P Vidhan Sabha
    <br />
    FOR THE MONTH OF: " + TxtStatus + @"
</td>

<td style='width:50%;'>
<table style='width:100%;border-spacing: 0;border-collapse: collapse;'>
<tr>
<td style='width:30%;border: 1px solid black;'>
<table>

<tr>
<tr>
<td style='font-size:25px'>
</td>
</tr>
<td style='font-size:25px'>
Bill No.:.........
</td>
<tr>
<td style='width:100%;font-size:25px' >
Bill Date.:.........
</td>
</tr>
</tr>
</table>
</td>
<td style='width:30%;border: 1px solid black;'>
<table>
<tr>
<tr>
<td style='font-size:25px'>
</td>
</tr>
<td style='font-size:25px'>
Token No.:.........
</td>
<tr>
<td style='width:100%;font-size:25px' >
Token Date:.........
</td>
</tr>
</tr>
</table>
</td>
<td style='width:30%;border: 1px solid black;'>
<table>
<tr>
<td style='font-size:25px'>
(For Treasury Officer Use)
</td>
</tr>
<tr>
<td style='font-size:25px'>
Voucher No.:.........
</td>
<tr>
<td style='width:100%;font-size:25px' >
Voucher Date:.........
</td>
</tr>
</tr>
</table>
</td>
</tr>
</table>
</td>

</tr>  



<tr>  
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;font-size:25px'>
    <tr>
        <td> 1. Treasury Code:  </td>
            <td>" + model.TreasuryCD + @"</td>
        <td>2. Demand No:</td>
        <td>" + model.DemandNo + @"</td>
    </tr>   
    <tr>
        <td> 3. D.D.O.Code:  </td>
            <td>" + model.DDOCD + @"</td>
        <td>4. Gaztd./Non-Gaztd:</td>
        <td>" + model.Gazett + @"</td>
    </tr>
    <tr>
        <td>  5. Major Head:  </td>
            <td>" + model.MajorHead + @"</td>
     
    </tr>
    <tr>
        <td> 6. Sub-Major Head:  </td>
            <td>" + model.SubmajorHead + @"</td>
  
    </tr>
     
     <tr>
        <td> 7. Minor-Head:  </td>
            <td>" + model.MinorHead + @"</td>
    
    </tr>
     <tr>
        <td>   8. Sub Head:  </td>
            <td>" + model.SubHead + @"</td>
      
    </tr>
     <tr>
        <td> 9. Budget Code: </td>
            <td>" + model.BudgetCode + @"</td>
        <td>10. Object No:</td>
        <td>" + model.ObjectCode + @"</td>
    </tr>
     <tr>
        <td>11. Plan/Non-Plan:  </td>
            <td>" + model.Plan + @"</td>
        <td> 12. Voted/Charged:</td>
        <td>" + model.Plan + @"</td>
    </tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>
<tr>
<td style='width:100%;'>
<table style='width:100%;font-size:25px''>
    <tr>
        <td>  1. GROSS TOTAL:  </td>
        <td></td>
            <td>Rs " + CompleteTotal + @"</td>
        
    </tr>   
    <tr>
        <td>  2. SHORT DRAWAL:  </td>
         <td></td>
            <td>Rs Nill</td>
        
    </tr>
    <tr>
        <td> 3. TOTAL AMOUNT:</td>
         <td>(1-2)</td>
            <td>Rs " + CompleteTotal + @"</td>
     
    </tr>
    <tr>
        <td>4. A.G DEDUCTIONS  (A):  </td>
         <td></td>
            <td>Rs " + model.TotalDeducA + @"</td>
  
    </tr>
     <tr>
        <td> 5. BALANCE AMOUNT:  </td>
         <td>(3-4) </td>
            <td> Rs " + model.BalanceAmount + @"</td>
     
    </tr>
     <tr>
        <td> 6. B.T.DEDUCTIONS:  (B):  </td>
          <td></td>
            <td>Rs " + model.TotalDeducB + @"</td>
    
    </tr>
     <tr>
        <td>  7. NET AMOUNT (payble):  </td>
          <td>(5-6)</td>
            <td>Rs " + CompleteTotal + @"</td>
      
    </tr>
     
</table>
</td>
</tr>
</table>
</td>
</tr>   <!-- end 2 row -->


<tr>  <!-- Start 3 row -->
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;;font-size:25px'>    
<tr>
<td style='width:50%;border: 1px solid black;'>
       <span>&nbsp&nbsp&nbsp SUB-OBJECT(DETAILED) HEADS</span>
    <table style='width:100%;font-size:25px''>
     
    <tr>
        <td> 1. Basic Pay:  </td>
        <td>01</td>
            <td>Rs " + model.BasicPay + @"</td>
        
    </tr>
          <tr>
        <td> 2. Special Pay:  </td>
        <td>02</td>
            <td>Rs " + model.SpecialPay + @"</td>
        
    </tr>   
         <tr>
        <td> 3. Dearness Allow:  </td>
        <td>03</td>
            <td>Rs " + model.DearnessAllowns + @"</td>
        
    </tr>   
         <tr>
        <td> 4. Compensatory Allow:  </td>
        <td>04</td>
            <td>Rs " + model.CompenseAllowns + @"</td>
        
    </tr>   
         <tr>
        <td> 5. House Rent Allow:  </td>
        <td>05</td>
            <td>Rs " + model.HRAAllowns + @"</td>
        
    </tr>   
         <tr>
        <td> 6. Capital Allowance: </td>
        <td>06</td>
            <td>Rs " + model.CapitalAllowns + @"</td>
        
    </tr>   
        <tr>
        <td>  7. Conveyance Allow: </td>
        <td>07</td>
            <td>Rs " + model.ConveyaceAllowns + @"</td>
        
    </tr>   
        <tr>
        <td> 8. Washing Allowance: </td>
        <td>08</td>
            <td>Rs " + model.WashingAllowns + @".</td>
        
    </tr>   
        <tr>
        <td>  9. : </td>
        <td>.......</td>
            <td>Rs........</td>
        
    </tr>   
          <tr>
        <td>  10. : </td>
        <td>G.P</td>
            <td>Rs  " + model.GP + @"</td>
        
    </tr>   
          <tr>
        <td>  11. : </td>
        <td>Sect. Pay</td>
            <td>Rs " + model.SectPay + @"</td>
        
    </tr>   
          <tr>
        <td>  12. : </td>
        <td>.......</td>
            <td>Rs........</td>
        
    </tr>   
          <tr>
        <td>  13. : </td>
        <td>.......</td>
            <td>Rs........</td>
        
    </tr>   
        </table>
</td>
    <td style='width:50%; border: 1px solid black;'>
<span>&nbsp&nbsp&nbsp (A) DEDUCTIONS (CLASSIFIED BY A.G)</span>
<table style='width:100%;font-size:25px''>
    <tr>
        <td>  1. GPS Subscription:  </td>
        <td></td>
            <td>Rs " + model.GPFSubs + @"</td>
        
    </tr>
          <tr>
        <td> 2. GPF Advance Recovery:  </td>
        <td></td>
            <td>Rs " + model.GPFAdvans + @"</td>
        
    </tr>   
         <tr>
        <td> 3. House Building Advance:  </td>
        <td></td>
            <td>Rs " + model.HBA + @"</td>
        
    </tr>   
         <tr>
        <td> 4. ' '  Intrest:  </td>
        <td></td>
            <td>Rs " + model.HBAInterest + @"</td>
        
    </tr>   
         <tr>
        <td> 5. M.Car/Scooter Advance:  </td>
        <td></td>
            <td>Rs " + model.NCarScooterAdvance + @"</td>
        
    </tr>   
         <tr>
        <td> 6. ' '  Intrest: </td>
        <td></td>
            <td>Rs " + model.MCScooterInterest + @"</td>
        
    </tr>   
        <tr>
        <td>  7. Warm Cloth Advance: </td>
        <td>.......</td>
            <td>Rs " + model.WarmclothAdvance + @"</td>
        
    </tr>   
        <tr>
        <td> 8. ' '  Intrest: </td>
        <td></td>
            <td>Rs " + model.WarmclothInterest + @"</td>
        
    </tr>   
        <tr>
        <td>  9. L.T.C./T.A Advance: </td>
        <td></td>
            <td>Rs " + model.LTCTAAdvance + @"</td>
        
    </tr>   
          <tr>
        <td>  10. Festival Advance: </td>
        <td></td>
            <td>Rs " + model.FestivalAdvance + @"</td>
        
    </tr>   
          <tr>
        <td> 11. Miscellaneous Recovery: </td>
        <td></td>
            <td>Rs " + model.MiscelRecov + @"</td>
        
    </tr>   
          <tr>
        <td>  12.  </td>
        <td></td>
            <td>Rs........</td>
        
    </tr>   
       
        </table>
</td>
</tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;font-size:25px'>
  
<tr>
    
<td style='width:100%;'>
    <span>(B) DEDUCTIONS(CLASSIFIED BY T.O):</span>    
 <span >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp< * CORRESPONDING RECEIPTS CODES * ></span> 
    <table style='width:100%;font-size:25px'>


   <tr>
        <td ></td>     
            <td ></td>
           <td style='border: 1px solid black;'>Major</td>
           <td style='border: 1px solid black;'>S.Maj</td>
           <td style='border: 1px solid black;'>Minor</td>
           <td style='border: 1px solid black;'>S.Head</td>
           <td style='border: 1px solid black;'>000 Code</td>

        
    </tr>
    <tr>
        <td >   1. Insurance fund:  </td>     
            <td>Rs " + model.InsurFund + @"</td>
 <td style='border: 1px solid black;'>8011</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>105</td>
           <td style='border: 1px solid black;'>01</td>
           <td style='border: 1px solid black;'></td>
        
    </tr>
          <tr>
        <td> 2. Savings Fund:  </td>        
            <td>Rs " + model.SavingFund + @"</td>
  <td style='border: 1px solid black;'>8011</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>105</td>
           <td style='border: 1px solid black;'>02</td>
                <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td> 3. House Rent:  </td>
            <td>Rs " + model.HouseRent + @"</td>
  <td style='border: 1px solid black;'>0216</td>
           <td style='border: 1px solid black;'>01</td>
           <td style='border: 1px solid black;'>106</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td>  4.Postal Life Insurance:</td>
            <td>Rs " + model.PLI + @"</td>
    <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>103</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td> 5.Life Insurance Corp:  </td>
            <td>Rs " + model.LIC + @"</td>
    <td style='border: 1px solid black;'>8448</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>104</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td>  6.Income tax: </td>
            <td>Rs " + model.IncomeTax + @"</td>
   <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>112</td>
           <td style='border: 1px solid black;'>01</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td> 7. Surcharge: </td>
            <td>Rs " + model.Surcharge + @"</td>
    <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>112</td>
           <td style='border: 1px solid black;'>02</td>
              <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td>8. Other B.T |: </td>
            <td>Rs " + model.OtherBT + @"</td>
   <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td> 9. Other B.T ||: </td>
            <td>Rs........</td>
         <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
    </tr>   
          <tr>
        <td>    10. Other B.T |||: </td>
            <td>Rs........</td>
         <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
    </tr>   
        
        
       
        </table>
</td>
</tr>
</table>
</td>
</tr>  

    <tr>  
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>    
<tr>
<td style='width:50%;border: 1px solid black;'>
    <table style='width:100%';font-size:25px'>
     
    <tr>
        <td style='font-size:25px'>GROSS TOTAL </td>
        <td></td>
            <td style='font-size:25px'>Rs " + CompleteTotal + @"</td>
        
    </tr>
           
             
        </table>
</td>
    <td style='width:50%; border: 1px solid black;'>
<table style='width:100%'>
    <tr>
        <td style='font-size:25px'> TOTAL (A)  </td>
        <td></td>
            <td style='font-size:25px'>Rs " + model.TotalDeducA + @"</td>
        
    </tr>
              
        </table>
</td>
</tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>
  
<tr>
    
<td style='width:100%;'>
 <table style='width:100%'>
    <tr>
        <td style='font-size:25px'> TOTAL (B)  </td>
        <td></td>
            <td style='font-size:25px'>Rs " + model.TotalDeducB + @"</td>
        
    </tr>
              
        </table>
</td>
</tr>
</table>
</td>
</tr>

</table>
<table style='width:100%'>
    <tr style='width:100%'>
        <td style='width:25%'>
            <table style='width:100%;font-size:25px''>
                <tr style='width:100%;'>
                    <td >
                        Station
                    </td>
                     <td style='margin-top:10px'>
----------------
                    </td>

                </tr>

                <tr style='width:100%'>
                    <td>
                        Date
                    </td>
                     <td>
----------------
                    </td>
                </tr>
            </table>
        </td>
        <td style='width:25%;font-size:25px'>
            (Treasury Clerk)
        </td>
        <td style='width:50%'>
            <table style='width:100%;font-size:25px'>
                <tr style='width:100%'>
                    <td>
                        (Signature & Designation of Drawing Officer)
                    </td>
                     

                </tr>

                <tr style='width:100%'>
                    <td>
                        Code:
                    </td>
                     
                </tr>
            </table>


</tr>

</table>  

        <div style='page-break-after: always;'></div>
<table>
<tr>
" + GetPayResult(model.MstaffIds, FDate, ToDate) + @"

</tr>

</table>

</table>  
   <div style='page-break-after: always;'></div>

    <table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  CERTIFICATS</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
         1.  The Drawl is being made on account of. " + TxtStatus + @" sanctioned vide letter no." + CLetterNo + @" .Dtd." + CDate + @"
</td>
 </tr>  
    </table>
<table style='font-size:22px'> 
    <tr>
      <td>
         2.  Arrears were less drawn vide T/V No......................................
</td>

 </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         3.  Certified that pay ans allowances drawn in this bill are due ans admissible as per authority is force and the deductions where-ever required have been made, as per the rules.
</td>
         </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         4.  Certified that in case of fresh appointees , the medical fitness certicates have been obtained.

</td>
 </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         5.  Certified that all appointments and promotions, grant of leave and period of suspensions, and the deputation and other events which are required to be recorded, have been recorded in the
         service Book & leave account of the concerned employee.
</td>
         </tr>
        </table>
  <table style='font-size:22px'> 
    <tr>
     <td>
       
         6.  Recieved contents (in Cash)Rs. " + model.NetAmount + @"(in Words) Rupees." + NumberToWordsConverter.NumbersToWords(Convert.ToInt64(model.NetAmount)) + @"
</td>
 </tr>
        </table>

<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:right'>
             (Signature & Designation of Drawing and Disbursing Officer)
        </td>
       
        
    </tr>
  <tr style='width:100%'>
       
        <td style='text-align:right;padding-right:306px'>
             Code No.
        </td>  
    </tr>
</table>
<div style='border: 1px solid black;'>

</div>

<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:16px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  (TO BE USED BY TREASURY OFFICE)</b> 
        </td>
       
    </tr>
  
</table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
      Pay Rs.......................................(In Words) Rupees...............................................................................................................................................................
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
      (Superintendent Treasury)
</td>
        <td>
      Date.....................
</td>
      <td>
      (Treasury Officers )
</td>
 </tr>  
    </table>

<table style='width:100%;font-size:16px'> 

 <tr>  

     <td>
     Acount to be classified by T.O. :
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
      1. Cash
</td>
        <td>
      :..........................
</td>
 
 </tr>  
     <tr>  

     <td>
      2. G.I.S (S.F)
</td>
        <td>
      :..........................
</td>
 
 </tr> 
    
         <tr>  

     <td>
      3.  (I.F.)
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      4. LIC
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      5.  PLI
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      6.  House Rent
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      7.  Income Tax
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      8.  Surcharge
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      9.  Miscellaneous
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
        &nbsp;&nbsp;&nbsp;    **Total**
</td>
        <td>
      :..........................
</td>
 
 </tr>   
    </table>
<div style='border: 1px solid black;'>

</div>
<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:16px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  (TO BE USED BY ACCOUNTANT GENERAL OFFICE)</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:22px'> 
<tr>
     
     <td>
     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Supdt.
</td>
     </tr>
    <tr>
       <td>
     Initials of..........................................in token of
</td>
        <td>
      Admitted Rs.........................................
</td>
 
 </tr>  
 

    <tr>
     
     <td>
     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp A.A.O
</td>
     </tr>
    <tr>
       <td>
     Check of classification of
</td>
        <td>
      Objected Rs.........................................
</td>
 
 </tr>  

    <tr>
     
     <td>
   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp   500
</td>
     </tr>
     <tr>
       <td>
     Item above Rs........
</td>
      
 
 </tr>
    <tr>
     
     <td>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  5000
</td>
     </tr>  
 
    </table>


<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         Auditor 
        </td>
       
    </tr>
  
</table>
<div style='border: 1px solid black;'>

</div>
<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:16px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>INSTRUCTIONS</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         1.  A red line should be drawn right across the sheet after each section of the establishment and GRANDS TOTALS should be in red link.
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         2.  All deductions should be supported by schedules in appropriate form. There should be seperate schedules for each G.P.F series and the G.P.F. account No. be entered therein in ascending order.
</td> 
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         3.  Recovery of House Rent should be supported by rent Rolls in duplicate form the PWD/Estate Officer.Deduction adjustable by B.T. should also be supported by duplicate schedules.
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
         4.  Due care should be taken to give correct code numbers wherever specified.
</td>
 </tr>  
    </table>
<div style='border: 1px solid black;'>

</div>

 
<div style='page-break-after: always;'></div>
<table>
<tr>
" + GetTR2PayResult(model.MstaffIds, FDate, ToDate) + @"

</tr>
       
                     </body> </html>";


                MemoryStream output = new MemoryStream();

                PdfConverter pdfConverter = new PdfConverter();

                // set the license key
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                // iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 10;


                pdfConverter.PdfFooterOptions.FooterHeight = 10;

                // set the PDF page orientation (portrait or landscape) - default value is portrait
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = (PdfPageOrientation.Landscape);

                //write the page number
                TextElement footerTextElement = new TextElement(0, 30, "&p;",
                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }


                //string url = "Account" + "/TR2/" + FYear + "/" + FMonth + "/";
                //fileName = FYear + "_" + FMonth + ".pdf";


                string url = "Account" + "/TR2/" + FDate + "/";
                fileName = "Test" + ".pdf";
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);

                // Filepath is using for storing file 
                string directory = model.FilePath + url;

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();


                var AccessPath = "/" + url + fileName;

                return "Success" + AccessPath;


            }

            catch (Exception ex)
            {

                //throw ex;
                return ex.Message;

            }
            finally
            {

            }


        }


        public string GetPayResult(List<string> EmployeeIds, string FDate, string ToDate)
        {
#pragma warning disable CS0219 // The variable 'empid' is assigned but its value is never used
            int empid = 0;
#pragma warning restore CS0219 // The variable 'empid' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'CompleteTotal' is assigned but its value is never used
            decimal CompleteTotal = 0;
#pragma warning restore CS0219 // The variable 'CompleteTotal' is assigned but its value is never used
            decimal DaysDA = 0;
            decimal DaysBasic = 0;
            decimal DaysSectt = 0;
            decimal DaysGP = 0;
            decimal DaysSpecial = 0;
            decimal CompNew = 0;
            decimal CompOld = 0;
            decimal Difference = 0;
            decimal TotalPay = 0;
            decimal DifOld = 0;
            decimal B1 = 0;
            decimal B2 = 0;
            decimal B3 = 0;
            decimal B4 = 0;
            decimal B5 = 0;
            DateTime d0 = DateTime.ParseExact(FDate, "dd/MM/yyyy", null);
            DateTime d1 = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);

            List<DateTime> datemonth = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                      .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
            List<Tuple<int, int>> yearmonth = new List<Tuple<int, int>>();
            foreach (DateTime x in datemonth)
            {
                yearmonth.Add(new Tuple<int, int>(x.Year, x.Month));
            }


            List<DateTime> datemonth1 = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                  .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
            var result = new List<int>();

            foreach (var x in datemonth)
            {
                result.Add(x.Month);
            }


            StringBuilder sb = new StringBuilder();
            sb.Append("<table style='border:1px solid black;'> <thead> <tr>");
            sb.Append("<th style='border :1px solid black;'>Sr. No.</th> <th style='border :1px solid black;'>Name & Designation <br>1</th> <th style='border :1px solid black;'>Basic Pay <br>2</th> <th style='border :1px solid black;'>Special Par<br> 3</th>");
            sb.Append(" <th style='border :1px solid black;'>Dearness Allowance <br>4</th><th style='border :1px solid black;'>Compens Allowance<br> 5</th><th style='border :1px solid black;'>H.R.A. Allowance <br>6</th><th style='border :1px solid black;'>Capital Allowance<br>7</th>");
            sb.Append(" <th style='border :1px solid black;'>Convey Allowance<br>8</th><th style='border :1px solid black;'>Washing Allowance<br>9</th><th style='border :1px solid black;'>GP<br>10</th><th style='border :1px solid black;'>Sect. Pay <br> 11</th><th style='border :1px solid black;'>12</th><th style='border :1px solid black;'>13</th><th style='border :1px solid black;'>14</th>");
            sb.Append("<th style='border :1px solid black;'>Total 2 to 14<br> 15</th><th style='border :1px solid black;'>GPF Account No<br> 16</th>");

            sb.Append("<th style='border :1px solid black;'>GPF Subs<br>17</th><th style='border :1px solid black;'>GPF Advance<br>18</th><th style='border :1px solid black;'>HBA<br>19</th> <th style='border :1px solid black;'>HBA Interest<br>20</th> <th style='border :1px solid black;'>N. Car/ Scooter Advance<br>21</th>");
            sb.Append("<th style='border :1px solid black;'>M.C./ Scooter Interest<br>22</th><th style='border :1px solid black;'>Warm cloth Advance<br>23</th><th style='border :1px solid black;'>Warm cloth Interest<br>24</th> <th style='border :1px solid black;'>Festival Advance<br>25</th>");
            sb.Append("<th style='border :1px solid black;'>L.T.C. T.A. Advance<br>26</th> <th style='border :1px solid black;'>Miscel. Recov.<br>27</th> <th style='border :1px solid black;'>28</th> <th style='border :1px solid black;'>Total (A) 17 to 28 <br>29</th>");

            sb.Append("<th style='border :1px solid black;'>Saving Fund<br>30</th> <th style='border :1px solid black;'>Insur Fund<br>31</th> <th style='border :1px solid black;'>House Rent<br>32</th> <th style='border :1px solid black;'>P.L.I.<br>33</th> <th style='border :1px solid black;'>L.I.C.<br>34</th>");
            sb.Append("<th style='border :1px solid black;'>IncomeTax<br>35</th> <th style='border :1px solid black;'>Sur-charge<br>36</th> <th style='border :1px solid black;'>OtherBT<br>37</th> <th style='border :1px solid black;'>Total (B) 30 to 37<br>38</th> <th style='border :1px solid black;'>Total Deduction <br>39</th> <th style='border :1px solid black;'>Net Payment<br>40</th>");
            sb.Append("</tr> </thead>");


            sb.Append("<tbody>");




            decimal GTotal = 0;
            int count = 1;
            foreach (var item in EmployeeIds)
            {

                GTotal = 0;
                int EmployeeId = int.Parse(item);
                var Staff = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", EmployeeId);
                System.Globalization.DateTimeFormatInfo mfi = new
System.Globalization.DateTimeFormatInfo();

                foreach (var New in yearmonth)
                {
                    int Year = New.Item1;
                    int Month = New.Item2;

                    var Maindata = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetPayData", new tEmployeeAccountDetails { EmpId = EmployeeId, Nww = New, FromDate = d0 });
                    string strMonthName = mfi.GetMonthName(Month).ToString();
                    var firstDayOfMonth = new DateTime(Year, Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                    var First = FDate;
                    var Second = Convert.ToDateTime(lastDayOfMonth).ToString("dd/MM/yyyy");
                    if (Maindata != null && Maindata.Count() > 0)
                    {
                        var QuerableList = Maindata.AsQueryable();
                        //sb.Append("<tr>");
                        //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", count));
                        if (Maindata.First().ToDate != null)
                        {
                            //   sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Convert.ToDateTime(Maindata.First().FromDate.Value).ToString("dd/MM/yyyy") + " To " + Convert.ToDateTime(Maindata.First().ToDate.Value).ToString("dd/MM/yyyy")));
                        }
                        else
                        {
                            //     sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Convert.ToDateTime(firstDayOfMonth).ToString("dd/MM/yyyy") + " To " + Convert.ToDateTime(lastDayOfMonth).ToString("dd/MM/yyyy")));
                        }
                        ViewBag.StaffName = Staff.StaffName + "-" + Staff.Designation;
                        // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));

                        // DUES

                        if (Maindata.First().ToDate != null)
                        {
                            int DaysInMonth = DateTime.DaysInMonth(Year, Month);
                            int DaysDiff = int.Parse((Maindata.First().ToDate.Value - Maindata.First().FromDate.Value).TotalDays.ToString()) + 1;
                            //Basic Pay
                            decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue;
                            decimal BasicPerDay = (BayGiven / DaysInMonth);
                            DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                            // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));
                            //Sectt Pay

                            decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue;
                            decimal SecttPerDay = (SecttGiven / DaysInMonth);
                            DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                            //    sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));
                            //GP Pay
                            decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue;
                            decimal GPPerDay = (GP / DaysInMonth);
                            DaysGP = (Math.Round(GPPerDay * DaysDiff));
                            // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                            //Special Pay
                            decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue;
                            decimal SpecialPerDay = (Special / DaysInMonth);
                            DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                            //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));
                            //DA Pay
                            decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                            decimal DAPerDay = (DA / DaysInMonth);
                            DaysDA = (Math.Round(DAPerDay * DaysDiff));
                            //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                            CompNew = DaysDA + DaysBasic + DaysSectt + DaysGP + DaysSpecial;
                            //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", CompNew));
                            //Difference = TotalPay - Comp;
                            //sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Difference));
                            //sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Difference));
                        }
                        else
                        {

                            //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue));
                            //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue));
                            //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue));
                            //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue));
                            //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue));
                            TotalPay = (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue);
                            //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", TotalPay));
                        }
                        if (Maindata.First().ToDate != null)
                        {
                            int DaysInMonth = DateTime.DaysInMonth(Year, Month);
                            int DaysDiff = int.Parse((Maindata.First().ToDate.Value - Maindata.First().FromDate.Value).TotalDays.ToString()) + 1;
                            //Basic Pay
                            if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3;
                                decimal BasicPerDay = (BayGiven / DaysInMonth);
                                DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2;
                                decimal BasicPerDay = (BayGiven / DaysInMonth);
                                DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1;
                                decimal BasicPerDay = (BayGiven / DaysInMonth);
                                DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                //     sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue;
                                decimal BasicPerDay = (BayGiven / DaysInMonth);
                                DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                //     sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                            }
                            //Sectt Pay
                            if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3;
                                decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                //     sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2;
                                decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1;
                                decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue != 0)
                            {
                                decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue;
                                decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                //    sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                            }
                            //GP Pay
                            if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3;
                                decimal GPPerDay = (GP / DaysInMonth);
                                DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2;
                                decimal GPPerDay = (GP / DaysInMonth);
                                DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1;
                                decimal GPPerDay = (GP / DaysInMonth);
                                DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue;
                                decimal GPPerDay = (GP / DaysInMonth);
                                DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                ///  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                            }

                            //Special Pay
                            if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3;
                                decimal SpecialPerDay = (Special / DaysInMonth);
                                DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2;
                                decimal SpecialPerDay = (Special / DaysInMonth);
                                DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1;
                                decimal SpecialPerDay = (Special / DaysInMonth);
                                DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue;
                                decimal SpecialPerDay = (Special / DaysInMonth);
                                DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                //    sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                            }

                            //DA Pay
                            if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3;
                                decimal DAPerDay = (DA / DaysInMonth);
                                DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2;
                                decimal DAPerDay = (DA / DaysInMonth);
                                DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1;
                                decimal DAPerDay = (DA / DaysInMonth);
                                DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                                decimal DAPerDay = (DA / DaysInMonth);
                                DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                            }
                            CompOld = DaysDA + DaysBasic + DaysSectt + DaysGP + DaysSpecial;
                            // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", CompOld));
                            Difference = CompNew - CompOld;
                            //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Difference));
                            //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Difference));
                        }
                        else
                        {
                            if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3));
                                B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3;
                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {

                                B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue;
                                //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue));

                            }

                            if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3;
                                //   sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1;
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue));

                            }

                            if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3;
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue));

                            }

                            if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue;
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue));

                            }


                            if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3;
                                // sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2;
                                //  sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1;
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                                //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue));

                            }

                            decimal Sum = B1 + B2 + B3 + B4 + B5;
                            //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Sum));
                            DifOld = TotalPay - Sum;
                            //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DifOld));
                            //sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DifOld));
                        }
                        if (GTotal == 0)
                        {
                            GTotal = Difference + DifOld;
                        }
                        else
                        {
                            GTotal = GTotal + DifOld;
                        }
                    }

                }

                sb.Append("<tr>");
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", GTotal));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append("</tr>");
                count++;

            }



            return sb.ToString();
        }


        public string GetTR2PayResult(List<string> EmployeeIds, string FDate, string ToDate)
        {
            int empid = 0;
            decimal CompleteTotal = 0;
            decimal DaysDA = 0;
            decimal DaysBasic = 0;
            decimal DaysSectt = 0;
            decimal DaysGP = 0;
            decimal DaysSpecial = 0;
            decimal CompNew = 0;
            decimal CompOld = 0;
            decimal Difference = 0;
            decimal TotalPay = 0;
            decimal DifOld = 0;
            decimal B1 = 0;
            decimal B2 = 0;
            decimal B3 = 0;
            decimal B4 = 0;
            decimal B5 = 0;
            DateTime d0 = DateTime.ParseExact(FDate, "dd/MM/yyyy", null);
            DateTime d1 = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);
            List<DateTime> datemonth = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                      .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
            List<Tuple<int, int>> yearmonth = new List<Tuple<int, int>>();
            foreach (DateTime x in datemonth)
            {
                yearmonth.Add(new Tuple<int, int>(x.Year, x.Month));
            }


            List<DateTime> datemonth1 = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                                  .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
            var result = new List<int>();

            foreach (var x in datemonth)
            {
                result.Add(x.Month);
            }


            StringBuilder sb = new StringBuilder();
            sb.Append("<table style='width=100%, border:1px solid black;'> <thead> <tr >");
            sb.Append("<th style='border :1px solid black;width:5%'>Sr. No.</th><th style='border :1px solid black;width:11%'>Date From - To</th>  <th style='border :1px solid black;width:5%'>Name & Designation <br>1</th> <th style='border :1px solid black;width:5%'>Basic Pay <br>2</th> <th style='border :1px solid black;width:5%'>Sectt. Pay<br> 3</th>");
            sb.Append(" <th style='border :1px solid black;width:5%'>G.P <br>4</th><th style='border :1px solid black;width:5%'>Special Pay<br> 5</th><th style='border :1px solid black;width:5%'>D.A @ <br>6</th><th style='border :1px solid black;width:5%'>Total<br>7</th> <th style='border :1px solid black;width:5%'>Basic Pay<br>8</th><th style='border :1px solid black;width:5%'>Sectt. Pay<br>9</th> <th style='border :1px solid black;width:5%'>G.P<br>10</th><th style='border :1px solid black;width:5%'>Special Pay<br>5</th><th style='border :1px solid black;width:5%'>Basic Pay<br>DA @</th>");
            sb.Append("<th style='border :1px solid black;width:5%'>Total</th>");
            sb.Append("<th style='border :1px solid black;width:5%'>Difference</th>");
            sb.Append("<th style='border :1px solid black;width:5%'>Net Payable</th>");
            sb.Append("</tr> </thead>");
            sb.Append("<tbody>");

            int count = 1;


            foreach (var item in EmployeeIds)
            {
                int NewSeries = 0;
                decimal GTotal = 0;
                int EmployeeId = int.Parse(item);
                var Staff = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", EmployeeId);
                System.Globalization.DateTimeFormatInfo mfi = new
System.Globalization.DateTimeFormatInfo();

                foreach (var New in yearmonth)
                {

                    int Year = New.Item1;
                    int Month = New.Item2;

                    var Maindata = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetPayData", new tEmployeeAccountDetails { EmpId = EmployeeId, Nww = New, FromDate = d0 });
                    string strMonthName = mfi.GetMonthName(Month).ToString();
                    var firstDayOfMonth = new DateTime(Year, Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                    var First = FDate;
                    var Second = Convert.ToDateTime(lastDayOfMonth).ToString("dd/MM/yyyy");
                    if (Maindata != null && Maindata.Count() > 0)
                    {
                        var QuerableList = Maindata.AsQueryable();
                        sb.Append("<tr>");
                        sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", count));
                        if (Maindata.First().ToDate != null)
                        {
                            sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Convert.ToDateTime(Maindata.First().FromDate.Value).ToString("dd/MM/yyyy") + " To " + Convert.ToDateTime(Maindata.First().ToDate.Value).ToString("dd/MM/yyyy")));
                        }
                        else
                        {
                            sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Convert.ToDateTime(firstDayOfMonth).ToString("dd/MM/yyyy") + " To " + Convert.ToDateTime(lastDayOfMonth).ToString("dd/MM/yyyy")));
                        }
                        ViewBag.StaffName = Staff.StaffName + "-" + Staff.Designation;
                        sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));

                        // DUES

                        if (Maindata.First().ToDate != null)
                        {
                            int DaysInMonth = DateTime.DaysInMonth(Year, Month);
                            int DaysDiff = int.Parse((Maindata.First().ToDate.Value - Maindata.First().FromDate.Value).TotalDays.ToString()) + 1;
                            //Basic Pay
                            decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue;
                            decimal BasicPerDay = (BayGiven / DaysInMonth);
                            DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));
                            //Sectt Pay

                            decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue;
                            decimal SecttPerDay = (SecttGiven / DaysInMonth);
                            DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));
                            //GP Pay
                            decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue;
                            decimal GPPerDay = (GP / DaysInMonth);
                            DaysGP = (Math.Round(GPPerDay * DaysDiff));
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                            //Special Pay
                            decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue;
                            decimal SpecialPerDay = (Special / DaysInMonth);
                            DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));
                            //DA Pay
                            decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                            decimal DAPerDay = (DA / DaysInMonth);
                            DaysDA = (Math.Round(DAPerDay * DaysDiff));
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                            CompNew = DaysDA + DaysBasic + DaysSectt + DaysGP + DaysSpecial;
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", CompNew));
                            //Difference = TotalPay - Comp;
                            //sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Difference));
                            //sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", Difference));
                        }
                        else
                        {

                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue));
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue));
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue));
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue));
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue));
                            TotalPay = (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue) + (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue);
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0}</td>", TotalPay));
                        }
                        if (Maindata.First().ToDate != null)
                        {
                            int DaysInMonth = DateTime.DaysInMonth(Year, Month);
                            int DaysDiff = int.Parse((Maindata.First().ToDate.Value - Maindata.First().FromDate.Value).TotalDays.ToString()) + 1;
                            //Basic Pay
                            if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3;
                                decimal BasicPerDay = (BayGiven / DaysInMonth);
                                DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2;
                                decimal BasicPerDay = (BayGiven / DaysInMonth);
                                DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1;
                                decimal BasicPerDay = (BayGiven / DaysInMonth);
                                DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                decimal BayGiven = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue;
                                decimal BasicPerDay = (BayGiven / DaysInMonth);
                                DaysBasic = (Math.Round(BasicPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysBasic));

                            }
                            //Sectt Pay
                            if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3;
                                decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2;
                                decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1;
                                decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue != 0)
                            {
                                decimal SecttGiven = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue;
                                decimal SecttPerDay = (SecttGiven / DaysInMonth);
                                DaysSectt = (Math.Round(SecttPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSectt));

                            }
                            //GP Pay
                            if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3;
                                decimal GPPerDay = (GP / DaysInMonth);
                                DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2;
                                decimal GPPerDay = (GP / DaysInMonth);
                                DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1;
                                decimal GPPerDay = (GP / DaysInMonth);
                                DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue;
                                decimal GPPerDay = (GP / DaysInMonth);
                                DaysGP = (Math.Round(GPPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysGP));

                            }

                            //Special Pay
                            if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3;
                                decimal SpecialPerDay = (Special / DaysInMonth);
                                DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2;
                                decimal SpecialPerDay = (Special / DaysInMonth);
                                DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1;
                                decimal SpecialPerDay = (Special / DaysInMonth);
                                DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                decimal Special = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue;
                                decimal SpecialPerDay = (Special / DaysInMonth);
                                DaysSpecial = (Math.Round(SpecialPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysSpecial));

                            }

                            //DA Pay
                            if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3;
                                decimal DAPerDay = (DA / DaysInMonth);
                                DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2;
                                decimal DAPerDay = (DA / DaysInMonth);
                                DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1;
                                decimal DAPerDay = (DA / DaysInMonth);
                                DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                decimal DA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                                decimal DAPerDay = (DA / DaysInMonth);
                                DaysDA = (Math.Round(DAPerDay * DaysDiff));
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DaysDA));

                            }
                            CompOld = DaysDA + DaysBasic + DaysSectt + DaysGP + DaysSpecial;
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", CompOld));
                            Difference = CompNew - CompOld;
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Difference));
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Difference));
                        }
                        else
                        {
                            if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3));
                                B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue3;
                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {

                                B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue2));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AdditionValue1));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                B1 = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue));

                            }

                            if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue3));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue2));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AdditionValue1));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                B2 = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue));

                            }

                            if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue3));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue2));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AdditionValue1));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                B3 = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue));

                            }

                            if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue3));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue2));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AdditionValue1));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue));

                            }


                            if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3));

                            }

                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1));

                            }
                            else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue != Convert.ToDecimal(0))
                            {
                                B4 = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue));

                            }

                            decimal Sum = B1 + B2 + B3 + B4 + B5;
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", Sum));
                            DifOld = TotalPay - Sum;
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DifOld));
                            sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", DifOld));
                        }
                        if (GTotal == 0)
                        {
                            GTotal = Difference + DifOld;
                        }
                        else
                        {
                            GTotal = GTotal + DifOld;
                        }
                        NewSeries++;
                    }
                    else
                    {



                        sb.Append("<tr>");
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                        //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", strMonthName + " - " + Year));
                        if (empid != EmployeeId && NewSeries == 0)
                        {
                            empid = EmployeeId;
                            sb.Append(string.Format("<td style='border :1px solid black;'>{0} </td>", First + " To " + Second));
                            //   sb.Append(string.Format("<td style='border :1px solid black;>{0} </td>", First + " To " + Second));
                        }
                        else
                        {
                            sb.Append(string.Format("<td style='border :1px solid black;'>{0} </td>", Convert.ToDateTime(firstDayOfMonth).ToString("dd/MM/yyyy") + " To " + Convert.ToDateTime(lastDayOfMonth).ToString("dd/MM/yyyy")));
                        }
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append("</tr>");
                        NewSeries++;
                    }
                    count++;

                }



                sb.Append("<tr>");
                sb.Append(string.Format("<td style='border :1px solid black;width:5%' colspan='16'>Total </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", GTotal));
                if (CompleteTotal == 0)
                {
                    CompleteTotal = GTotal;
                }
                else
                {
                    CompleteTotal = CompleteTotal + GTotal;
                }


            }
            sb.Append("<tr>");
            sb.Append(string.Format("<td style='border :1px solid black;width:5%' colspan='16'>Gross Total </td>"));
            sb.Append(string.Format("<td style='border :1px solid black; width:5%'>{0} </td>", CompleteTotal));
            return sb.ToString();
        }


        public ActionResult Q1Report()
        {
            Tr2Model model = new Tr2Model();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            model.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            model.QuaterList = ModelMapping.GetQuaterList();
            model.Mstaff = (List<mStaff>)Helper.ExecuteService("TR2", "GetStaff", null);
            return PartialView("_MainQ1", model);

        }


        public string GenerateQuaterlyReport(string FYear, string Quarter, string StaffId)
        {
            try
            {
                string QuarterName = string.Empty;
                if (Quarter == "3,4,5")
                    QuarterName = "1st Quarter";
                else if (Quarter == "6,7,8")
                    QuarterName = "2nd Quarter";
                else if (Quarter == "9,10,11")
                    QuarterName = "3rd Quarter";
                else if (Quarter == "12,1,2")
                    QuarterName = "4th Quarter";

                string outXml = "";
                string LOBName = FYear;
                string fileName = "";
                string savedPDF = string.Empty;
                string Ids = StaffId.TrimEnd(',');
                tEmployeeAccountDetails model = new tEmployeeAccountDetails();
                model.finanacialYear = FYear;
                model.Quaters = Quarter;
                string s = Ids;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');
                    model.MstaffIds = new List<string>();
                    for (int i = 0; i < values.Length; i++)
                    {
                        model.MstaffIds.Add(values[i]);
                    }

                }
                //string Q = Quarter;
                //if (Q != null && Q != "")
                //{
                //    string[] values = Q.Split(',');
                //    model.MonthsQuater = new List<string>();
                //    for (int i = 0; i < values.Length; i++)
                //    {
                //        model.MonthsQuater.Add(values[i]);
                //    }

                //}


                model = (tEmployeeAccountDetails)Helper.ExecuteService("TR2", "GetSiteSettingd", model);

                outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                outXml += @"<div> 

<html>

<head>
  <table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  DETAIL REQUIRED ON FORM NO. 24Q (Quarterly Return under Head 'Salaries':" + QuarterName + " " + FYear + @"</b> 
        </td>
       
    </tr>
  
</table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
         Tax Deduction No. (TAN) :PTLH 11004-A 
</td>
    <td>
         Name of Deductor :SECRETARY
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
        Permanent Account No. (if any)  : 
</td>
    <td>
        Employer Office :
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
        Phone NO   : 
</td>
    <td>
        Type of Deductor :GOVERNMENT

</td>
 </tr>  
    </table>

<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
       3rd Quarter ended/ Year 
</td>

 </tr>  
    </table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
       December, 2014 
</td>
    <td>
        Address : H.P. VIDHAN SABHA SECRETARIAT,SHIMLA-171004



</td>
 </tr>  
    </table>
<table>
<tr>
" + GetQ1Result(model.MstaffIds, FYear, Quarter) + @"

</tr>

</table> 

</body>

</html>";


                MemoryStream output = new MemoryStream();

                PdfConverter pdfConverter = new PdfConverter();

                // set the license key
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                // iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4_LANDSCAPE, 25, 25, 30, 30);
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 10;


                pdfConverter.PdfFooterOptions.FooterHeight = 10;

                // set the PDF page orientation (portrait or landscape) - default value is portrait
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = (PdfPageOrientation.Landscape);

                //write the page number
                TextElement footerTextElement = new TextElement(0, 30, "&p;",
                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.ForeColor = System.Drawing.Color.MidnightBlue;
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);


                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }


                string url = "Account" + "/TR2/" + FYear + "/";
                fileName = FYear + ".pdf";


                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);

                // Filepath is using for storing file 
                string directory = model.FilePath + url;

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();


                var AccessPath = "/" + url + fileName;

                return "Success" + AccessPath;


            }

            catch (Exception ex)
            {

                //throw ex;
                return ex.Message;

            }
            finally
            {

            }


        }


        public string GetQ1Result(List<string> EmployeeIds, string FYear, string Quarter)
        {
            List<Tuple<int, int>> yearmonth = new List<Tuple<int, int>>();

            if (Quarter == "3,4,5")
            {

                string[] values = FYear.Split('-');
                string Year1 = values[0];
                string Year2 = values[1];

                {
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year1), 3));
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year1), 4));
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year1), 5));
                };
            }
            else if (Quarter == "6,7,8")
            {

                string[] values = FYear.Split('-');
                string Year1 = values[0];
                string Year2 = values[1];
                {
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year1), 6));
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year1), 7));
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year1), 8));

                    //Tuple.Create(Convert.ToInt32(Year1),6);
                    //Tuple.Create(Convert.ToInt32(Year1),7);
                    //Tuple.Create(Convert.ToInt32(Year1),8);
                };
            }
            else if (Quarter == "9,10,11")
            {
                string[] values = FYear.Split('-');
                string Year1 = values[0];
                string Year2 = values[1];
                {
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year1), 9));
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year1), 10));
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year1), 11));
                };
            }
            else if (Quarter == "12,1,2")
            {
                string[] values = FYear.Split('-');
                string Year1 = values[0];
                string Year2 = values[1];
                {
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year1), 12));
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year2), 1));
                    yearmonth.Add(Tuple.Create(Convert.ToInt32(Year2), 2));
                };
            }


#pragma warning disable CS0219 // The variable 'CompleteTotal' is assigned but its value is never used
            decimal CompleteTotal = 0;
#pragma warning restore CS0219 // The variable 'CompleteTotal' is assigned but its value is never used
            StringBuilder sb = new StringBuilder();
            sb.Append("<table style='width=100%, border:1px solid black;'> <thead> <tr >");
            sb.Append("<th style='border :1px solid black;width:5%'>Sr. No.</th><th style='border :1px solid black;width:11%'>Name & Designation</th>  <th style='border :1px solid black;width:11%'>PAN <br>1</th> <th style='border :1px solid black;width:11%'>PAY Month <br>2</th> <th style='border :1px solid black;width:11%'>Gross Salary<br> 3</th>");
            sb.Append(" <th style='border :1px solid black;width:11%'>Exempted Allowances<br>4</th><th style='border :1px solid black;width:11%'>Deductible  Saving u/s 80C<br> 5</th><th style='border :1px solid black;width:11%'>Others Deduction <br>6</th><th style='border :1px solid black;width:11%'>Net Taxable income  on which tax  deducted (1-2-3-4)<br>7</th> <th style='border :1px solid black;width:11%'>Tax deducted at source<br>8</th> <th style='border :1px solid black;width:11%'>Date of Deposit<br>9</th><th style='border :1px solid black;width:11%'>Voucher/ Challan No<br>10</th><th style='border :1px solid black;width:11%'>Date of Challan No<br>11</th>");
            //sb.Append("<th style='border :1px solid black;width:11%'>Total</th>");
            sb.Append("</tr> </thead>");
            sb.Append("<tbody>");

            int count = 1;
            decimal GTotal = 0;
            decimal GSTotal = 0;
            decimal EXTotal = 0;
            decimal DedTotal = 0;
            decimal OTotal = 0;
            decimal TTotal = 0;
            decimal NetTotalIncomeTax = 0;
            foreach (var item in EmployeeIds)
            {
                GTotal = 0;
                GSTotal = 0;
                EXTotal = 0;
                DedTotal = 0;
                OTotal = 0;
                TTotal = 0;
                NetTotalIncomeTax = 0;
                int EmployeeId = int.Parse(item);
                var Staff = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", EmployeeId);
                System.Globalization.DateTimeFormatInfo mfi = new
System.Globalization.DateTimeFormatInfo();

                foreach (var New in yearmonth)
                {
                    int Year = New.Item1;
                    int Month = New.Item2;

                    var Maindata = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetQ1Report", new tEmployeeAccountDetails { EmpId = EmployeeId, Nww = New });
                    string strMonthName = mfi.GetMonthName(Month).ToString();
                    if (Maindata != null && Maindata.Count() > 0)
                    {
                        sb.Append("<tr>");
                        sb.Append(string.Format("<td style='border :1px solid black;width:5%'>{0} </td>", count));

                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));
                        var QuerableList = Maindata.AsQueryable();
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", strMonthName + " - " + Year));
                        ViewBag.StaffName = Staff.StaffName + "-" + Staff.Designation;
                        // DUES

                        decimal BasicPay = Maindata.Where(m => m.AccountFieldId == 2).FirstOrDefault().AccountFieldValue;
                        decimal SpecialPay = Maindata.Where(m => m.AccountFieldId == 3).FirstOrDefault().AccountFieldValue;
                        decimal GP = Maindata.Where(m => m.AccountFieldId == 10).FirstOrDefault().AccountFieldValue;
                        decimal SecPay = Maindata.Where(m => m.AccountFieldId == 11).FirstOrDefault().AccountFieldValue;
                        decimal DearnessAllowns = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                        decimal ConveyaceAllowns = Maindata.Where(m => m.AccountFieldId == 8).FirstOrDefault().AccountFieldValue;
                        decimal CapitalAllowns = Maindata.Where(m => m.AccountFieldId == 7).FirstOrDefault().AccountFieldValue;
                        decimal HRAAllowns = Maindata.Where(m => m.AccountFieldId == 6).FirstOrDefault().AccountFieldValue;
                        decimal CompenseAllowns = Maindata.Where(m => m.AccountFieldId == 5).FirstOrDefault().AccountFieldValue;
                        decimal DUES = BasicPay + SpecialPay + GP + SecPay + DearnessAllowns + ConveyaceAllowns + HRAAllowns + CompenseAllowns;
                        decimal GPF = Maindata.Where(m => m.AccountFieldId == 17).FirstOrDefault().AccountFieldValue;
                        decimal NetTaxableIncome = DUES - ConveyaceAllowns - GPF;
                        decimal IncomeTax = Maindata.Where(m => m.AccountFieldId == 35).FirstOrDefault().AccountFieldValue;
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", DUES));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 8).FirstOrDefault().AccountFieldValue));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 17).FirstOrDefault().AccountFieldValue));
                        //GIS
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 17).FirstOrDefault().AccountFieldValue));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", NetTaxableIncome));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", IncomeTax));


                        // getting voucher Data
                        string VoucherNo = string.Empty;
                        string MonthName = string.Empty;
                        var VoucherDetail = (SalaryVoucherNumbers)Helper.ExecuteService("Budget", "GetSalaryVoucherNumbersFinYearAndMonth", new SalaryVoucherNumbers { FinancialYear = FYear, MonthId = Month });
                        if (VoucherDetail != null)
                        {
                            if (Staff.Classes == 0)
                                VoucherNo = VoucherDetail.ClassI;
                            else if (Staff.Classes == 1)
                                VoucherNo = VoucherDetail.ClassII;
                            else if (Staff.Classes == 2)
                                VoucherNo = VoucherDetail.ClassIII;
                            else if (Staff.Classes == 3)
                                VoucherNo = VoucherDetail.ClassIV;
                            else if (Staff.Classes == 4)
                                VoucherNo = VoucherDetail.ClassIII_CPF;
                            else if (Staff.Classes == 5)
                                VoucherNo = VoucherDetail.ClassIV_CPF;

                            MonthName = VoucherDetail.Months;
                        }
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", MonthName));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", VoucherNo));
                        sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", string.Empty));
                        //sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 7).FirstOrDefault().AccountFieldValue));
                        //sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 6).FirstOrDefault().AccountFieldValue));
                        //sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 5).FirstOrDefault().AccountFieldValue));
                        //sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue));

                        //var idList = new[] { 4 };
                        //decimal NewDA = QuerableList.Where(m => idList.Contains(m.AccountFieldId)).Sum(m => m.AccountFieldValue);
                        //decimal NewDA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;
                        //if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
                        //{
                        //    sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3));
                        //    decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3;
                        //    sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", NewDA - DAGiven));
                        //    if (GTotal == 0)
                        //    {
                        //        GTotal = NewDA - DAGiven;
                        //    }
                        //    else
                        //    {
                        //        GTotal = GTotal + (NewDA - DAGiven);
                        //    }
                        //}
                        //else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
                        //{
                        //    sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2));
                        //    decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2;
                        //    sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", NewDA - DAGiven));
                        //    if (GTotal == 0)
                        //    {
                        //        GTotal = NewDA - DAGiven;
                        //    }
                        //    else
                        //    {
                        //        GTotal = GTotal + (NewDA - DAGiven);
                        //    }
                        //}
                        //else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
                        //{
                        //    sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1));
                        //    decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1;
                        //    sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", NewDA - DAGiven));
                        //    if (GTotal == 0)
                        //    {
                        //        GTotal = NewDA - DAGiven;
                        //    }
                        //    else
                        //    {
                        //        GTotal = GTotal + (NewDA - DAGiven);
                        //    }
                        //}
                        // sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0}</td>", GTotal));

                        //if (GSTotal == 0)
                        //{
                        //    GSTotal = DUES;
                        //}
                        //else
                        //{
                        //    GSTotal = GSTotal + DUES;
                        //}

                        GSTotal = GSTotal + DUES;

                        //if (EXTotal == 0)
                        //{
                        //    EXTotal = ConveyaceAllowns;
                        //}
                        //else
                        //{
                        //    EXTotal = EXTotal + ConveyaceAllowns;
                        //}
                        EXTotal = EXTotal + ConveyaceAllowns;
                        DedTotal = DedTotal + GPF;
                        OTotal = OTotal + GPF;
                        NetTotalIncomeTax = NetTotalIncomeTax + NetTaxableIncome;
                        TTotal = TTotal + IncomeTax;
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", strMonthName + " - " + Year));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                        sb.Append("</tr>");

                    }

                    count++;
                }

                sb.Append("<tr>");
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>Total </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", GTotal));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", GTotal));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", GTotal));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", GSTotal));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", EXTotal));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", DedTotal));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", OTotal));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", NetTotalIncomeTax));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", TTotal));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", GTotal));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", GTotal));
                sb.Append(string.Format("<td style='border :1px solid black;width:11%'>{0} </td>", GTotal));



            }

            return sb.ToString();



            ////            decimal CompleteTotal = 0;
            ////            StringBuilder sb = new StringBuilder();
            ////            sb.Append("<table style='border:1px solid black;'> <thead> <tr>");
            ////            sb.Append("<th style='border :1px solid black;'>Sr. No.</th> <th style='border :1px solid black;'>Name & Designation <br>1</th> <th style='border :1px solid black;'>PAN <br>2</th> <th style='border :1px solid black;'>PAY Month<br> 3</th>");
            ////            sb.Append(" <th style='border :1px solid black;'>Gross Salary <br>4</th><th style='border :1px solid black;'>Exempted Allowances<br> 5</th><th style='border :1px solid black;'>Deductible  Saving u/s 80C <br>6</th><th style='border :1px solid black;'>Others Deduction<br>7</th>");
            ////            sb.Append(" <th style='border :1px solid black;'>Net Taxable income  on which tax  deducted (1-2-3-4)<br>8</th><th style='border :1px solid black;'>Tax deducted at source<br>9</th><th style='border :1px solid black;'>Date of Deposit<br>10</th><th style='border :1px solid black;'>Voucher/ Challan No <br> 11</th><th style='border :1px solid black;'>Date of Challan No</th>");
            ////            //sb.Append("<th style='border :1px solid black;'>Total 2 to 14<br> 15</th><th style='border :1px solid black;'>GPF Account No<br> 16</th>");

            ////            //sb.Append("<th style='border :1px solid black;'>GPF Subs<br>17</th><th style='border :1px solid black;'>GPF Advance<br>18</th><th style='border :1px solid black;'>HBA<br>19</th> <th style='border :1px solid black;'>HBA Interest<br>20</th> <th style='border :1px solid black;'>N. Car/ Scooter Advance<br>21</th>");
            ////            //sb.Append("<th style='border :1px solid black;'>M.C./ Scooter Interest<br>22</th><th style='border :1px solid black;'>Warm cloth Advance<br>23</th><th style='border :1px solid black;'>Warm cloth Interest<br>24</th> <th style='border :1px solid black;'>Festival Advance<br>25</th>");
            ////            //sb.Append("<th style='border :1px solid black;'>L.T.C. T.A. Advance<br>26</th> <th style='border :1px solid black;'>Miscel. Recov.<br>27</th> <th style='border :1px solid black;'>28</th> <th style='border :1px solid black;'>Total (A) 17 to 28 <br>29</th>");

            ////            //sb.Append("<th style='border :1px solid black;'>Saving Fund<br>30</th> <th style='border :1px solid black;'>Insur Fund<br>31</th> <th style='border :1px solid black;'>House Rent<br>32</th> <th style='border :1px solid black;'>P.L.I.<br>33</th> <th style='border :1px solid black;'>L.I.C.<br>34</th>");
            ////            //sb.Append("<th style='border :1px solid black;'>IncomeTax<br>35</th> <th style='border :1px solid black;'>Sur-charge<br>36</th> <th style='border :1px solid black;'>OtherBT<br>37</th> <th style='border :1px solid black;'>Total (B) 30 to 37<br>38</th> <th style='border :1px solid black;'>Total Deduction <br>39</th> <th style='border :1px solid black;'>Net Payment<br>40</th>");
            ////            sb.Append("</tr> </thead>");


            ////            sb.Append("<tbody>");



            ////            int count = 1;
            ////            decimal GTotal = 0;

            ////            foreach (var item in EmployeeIds)
            ////            {
            ////                GTotal = 0;
            ////                int EmployeeId = int.Parse(item);
            ////                var Staff = (mStaff)Helper.ExecuteService("staff", "getStaffDetailsByID", EmployeeId);
            ////                System.Globalization.DateTimeFormatInfo mfi = new
            ////System.Globalization.DateTimeFormatInfo();

            ////                foreach (var New in yearmonth)
            ////                {
            ////                    int Year = New.Item1;
            ////                    int Month = New.Item2;

            ////                    var Maindata = (List<tEmployeeAccountDetails>)Helper.ExecuteService("TR2", "GetQ1Report", new tEmployeeAccountDetails { EmpId = EmployeeId, Nww = New });
            ////                    string strMonthName = mfi.GetMonthName(Month).ToString();
            ////                    if (Maindata != null && Maindata.Count() > 0)
            ////                    {
            ////                        var YearMonth = strMonthName + " - " + Year;
            ////                        var nameDesig = Staff.StaffName + "-" + Staff.Designation;
            ////                        ViewBag.StaffName = Staff.StaffName + "-" + Staff.Designation;

            ////                        var QuerableList = Maindata.AsQueryable();


            ////                        decimal NewDA = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AccountFieldValue;

            ////                        if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3 != Convert.ToDecimal(0))
            ////                        {
            ////                            //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3));
            ////                            decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue3;

            ////                            if (GTotal == 0)
            ////                            {
            ////                                GTotal = NewDA - DAGiven;
            ////                            }
            ////                            else
            ////                            {
            ////                                GTotal = GTotal + (NewDA - DAGiven);
            ////                            }
            ////                        }
            ////                        else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2 != Convert.ToDecimal(0))
            ////                        {

            ////                            decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue2;

            ////                            if (GTotal == 0)
            ////                            {
            ////                                GTotal = NewDA - DAGiven;
            ////                            }
            ////                            else
            ////                            {
            ////                                GTotal = GTotal + (NewDA - DAGiven);
            ////                            }
            ////                        }
            ////                        else if (Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1 != Convert.ToDecimal(0))
            ////                        {

            ////                            decimal DAGiven = Maindata.Where(m => m.AccountFieldId == 4).FirstOrDefault().AdditionValue1;

            ////                            if (GTotal == 0)
            ////                            {
            ////                                GTotal = NewDA - DAGiven;
            ////                            }
            ////                            else
            ////                            {
            ////                                GTotal = GTotal + (NewDA - DAGiven);
            ////                            }
            ////                        }

            ////                    }
            ////                    if (CompleteTotal == 0)
            ////                    {
            ////                        CompleteTotal = GTotal;
            ////                    }
            ////                    else
            ////                    {
            ////                        CompleteTotal = CompleteTotal + GTotal;
            ////                    }
            ////                    count++;
            ////                }
            ////                sb.Append("<tr>");
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", Staff.StaffName + "-" + Staff.Designation));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", GTotal));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", GTotal));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
            ////                sb.Append("</tr>");


            ////            }



            ////            return sb.ToString();
        }

    }
    public class TemModel
    {
        List<string> EmployeeIds { get; set; }
        string FDate { get; set; }
        string ToDate { get; set; }
    }
}
