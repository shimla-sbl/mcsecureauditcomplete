﻿using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Enums;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class MedicineController : Controller
    {
        //
        // GET: /AccountsAdmin/Medicine/

        public PartialViewResult MedicineIndex(int pageId = 1, int pageSize = 10)
        {
            try
            {

                var MedicineList = (List<mMedicine>)Helper.ExecuteService("Budget", "GetAllMedicine", null);

                ViewBag.PageSize = pageSize;
                List<mMedicine> pagedRecord = new List<mMedicine>();
                if (pageId == 1)
                {
                    pagedRecord = MedicineList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = MedicineList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(MedicineList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/Medicine/_MedicineIndex.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  Medicine Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 10)
        {
            var MedicineList = (List<mMedicine>)Helper.ExecuteService("Budget", "GetAllMedicine", null);

            ViewBag.PageSize = pageSize;
            List<mMedicine> pagedRecord = new List<mMedicine>();
            if (pageId == 1)
            {
                pagedRecord = MedicineList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedicineList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedicineList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/Medicine/_MedicineList.cshtml", pagedRecord);
        }


        public PartialViewResult PopulateMedicine(int Id, string Action, int pageId = 1, int pageSize = 10)
        {
            mMedicine model = new mMedicine();

            ViewBag.MedicineType = new SelectList(Enum.GetValues(typeof(MedicineType)).Cast<MedicineType>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");

            var stateToEdit = (mMedicine)Helper.ExecuteService("Budget", "GetMedicineById", new mMedicine { MedicineID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit;

            model.Mode = Action;
            ViewBag.Add = Action;
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;
            return PartialView("/Areas/AccountsAdmin/Views/Medicine/_PopulateMedicine.cshtml", model);
        }

        public PartialViewResult EditMedicine(int Id, string Action, int pageId = 1, int pageSize = 10)
        {
            mMedicine model = (mMedicine)Helper.ExecuteService("Budget", "GetMedicineById", new mMedicine { MedicineID = Id });
             
            ViewBag.MedicineType = new SelectList(Enum.GetValues(typeof(MedicineType)).Cast<MedicineType>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text", model.MeidicineType);

            model.Mode = Action;
            ViewBag.Add = Action;
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;
            return PartialView("/Areas/AccountsAdmin/Views/Medicine/_EditMedicine.cshtml", model);
        }

        public PartialViewResult DeleteMedicine(int Id, int pageId = 1, int pageSize = 10)
        {

            Helper.ExecuteService("Budget", "DeleteMedicine", new mMedicine { MedicineID = Id });
            ViewBag.Msg = "Sucessfully deleted Firm";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            var MedicineList = (List<mMedicine>)Helper.ExecuteService("Budget", "GetAllMedicine", null);

            ViewBag.PageSize = pageSize;
            List<mMedicine> pagedRecord = new List<mMedicine>();
            if (pageId == 1)
            {
                pagedRecord = MedicineList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedicineList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedicineList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/Medicine/_MedicineList.cshtml", pagedRecord);
        }

        [HttpPost]
        public ActionResult SaveMedicine(FormCollection collection, int pageId = 1, int pageSize = 10)
        {

            try
            {
                string PCRow = collection["MedicineNameNumRow"];
                if (PCRow != null && PCRow.Length > 0)
                {
                    string[] RowCount = PCRow.Split(',');

                    foreach (var val in RowCount)
                    {
                        mMedicine model = new mMedicine();
                        model.MedicineName = collection["MedicineName-" + val];
                        model.IsActive = true;
                        model.MeidicineType = int.Parse(collection["MeidicineType"]);
                        if (model.MeidicineType == 0)
                            model.IsApproved = false;
                        else
                            model.IsApproved = true;
                        if (!string.IsNullOrEmpty(model.MedicineName))
                            Helper.ExecuteService("Budget", "CreateMedicine", model);

                        ViewBag.Msg = "Sucessfully added medicine name";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }


            var MedicineList = (List<mMedicine>)Helper.ExecuteService("Budget", "GetAllMedicine", null);

            ViewBag.PageSize = pageSize;
            List<mMedicine> pagedRecord = new List<mMedicine>();
            if (pageId == 1)
            {
                pagedRecord = MedicineList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedicineList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedicineList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/Medicine/_MedicineList.cshtml", pagedRecord);
        }

        [HttpPost]
        public ActionResult SaveSingleMedicine(mMedicine Medicine, int pageId = 1, int pageSize = 10)
        {

            try
            {
                Helper.ExecuteService("Budget", "UpdateMedicine", Medicine);
                ViewBag.Msg = "Sucessfully updated medicine";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            catch (Exception)
            {

                ViewBag.Msg = "Error in update medicine";
                ViewBag.Class = "alert alert-warning";
                ViewBag.Notification = "Error";
            }


            var MedicineList = (List<mMedicine>)Helper.ExecuteService("Budget", "GetAllMedicine", null);

            ViewBag.PageSize = pageSize;
            List<mMedicine> pagedRecord = new List<mMedicine>();
            if (pageId == 1)
            {
                pagedRecord = MedicineList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedicineList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedicineList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/Medicine/_MedicineList.cshtml", pagedRecord);
        }
        #region Search Medicine
        public PartialViewResult SearchMedicine()
        {
            try
            {
                return PartialView("/Areas/AccountsAdmin/Views/Medicine/_SearchMedicine.cshtml");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  Medicine Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }



        public ActionResult GetAutoCompleteData(string SearchWord)
        {
            var MedicineList = ((List<mMedicine>)Helper.ExecuteService("Budget", "SearchMedicine", SearchWord)).Where(m => m.IsActive == true).ToList();
            return Json(MedicineList, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult SearchMedicineData(string SearchWord, int pageId = 1, int pageSize = 20)
        {
            try
            {

                var MedicineList = ((List<mMedicine>)Helper.ExecuteService("Budget", "SearchMedicine", SearchWord)).Where(m => m.IsActive == true).ToList();

                Session["SearchMedicineList"] = MedicineList;
                ViewBag.PageSize = pageSize;
                List<mMedicine> pagedRecord = new List<mMedicine>();
                if (pageId == 1)
                {
                    pagedRecord = MedicineList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = MedicineList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(MedicineList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/Medicine/_SearchMedicineList.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  Medicine Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetSearchMedicineByPaging(string SearchWord, int pageId = 1, int pageSize = 10)
        {
            List<mMedicine> MedicineList = new List<mMedicine>();
            if (Session["SearchMedicineList"] != null)
                MedicineList = (List<mMedicine>)Session["SearchMedicineList"];
            else
                MedicineList = ((List<mMedicine>)Helper.ExecuteService("Budget", "SearchMedicine", SearchWord)).Where(m => m.IsActive == true).ToList();


            ViewBag.PageSize = pageSize;
            List<mMedicine> pagedRecord = new List<mMedicine>();
            if (pageId == 1)
            {
                pagedRecord = MedicineList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedicineList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedicineList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/Medicine/_SearchMedicineList.cshtml", pagedRecord);
        }
        #endregion


        #region Update Medicine
        public PartialViewResult UpdateMedicine()
        {
            try
            {
                return PartialView("/Areas/AccountsAdmin/Views/Medicine/_UpdateMedicine.cshtml");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  Medicine Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult SearchUpdateMedicineData(string SearchWord, int pageId = 1, int pageSize = 20)
        {
            try
            {

                var MedicineList = (List<mMedicine>)Helper.ExecuteService("Budget", "SearchMedicine", SearchWord);

                Session["SearchMedicineList"] = MedicineList;
                ViewBag.PageSize = pageSize;
                List<mMedicine> pagedRecord = new List<mMedicine>();
                if (pageId == 1)
                {
                    pagedRecord = MedicineList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = MedicineList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(MedicineList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/Medicine/_UpdateMedicineList.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting  Medicine Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetUpdateMedicineByPaging(string SearchWord, int pageId = 1, int pageSize = 10)
        {
            List<mMedicine> MedicineList = new List<mMedicine>();
            if (Session["UpdateMedicineList"] != null)
                MedicineList = (List<mMedicine>)Session["UpdateMedicineList"];
            else
                MedicineList = (List<mMedicine>)Helper.ExecuteService("Budget", "SearchMedicine", SearchWord);


            ViewBag.PageSize = pageSize;
            List<mMedicine> pagedRecord = new List<mMedicine>();
            if (pageId == 1)
            {
                pagedRecord = MedicineList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedicineList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedicineList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/Medicine/_UpdateMedicineList.cshtml", pagedRecord);
        }

        public ActionResult GetUpdateAutoCompleteData(string SearchWord)
        {
            var MedicineList = (List<mMedicine>)Helper.ExecuteService("Budget", "SearchMedicine", SearchWord);
            return Json(MedicineList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateMedicine(FormCollection collection, int pageId = 1, int pageSize = 20)
        {

            try
            {
                string PCRow = collection["RowCount"];
                if (PCRow != null && PCRow.Length > 0)
                {
                    string[] RowCount = PCRow.Split(',');

                    foreach (var val in RowCount)
                    {
                        int MedicineID = int.Parse(collection["MedicineID-" + val]);
                        var stateToEdit = (mMedicine)Helper.ExecuteService("Budget", "GetMedicineById", new mMedicine { MedicineID = MedicineID });
                        string Checked = collection["IsActive-" + val];

                        if (Checked == "on")
                            stateToEdit.IsActive = true;
                        else
                            stateToEdit.IsActive = false;

                        Helper.ExecuteService("Budget", "UpdateMedicine", stateToEdit);
                        ViewBag.Msg = "Sucessfully updated medicine";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            string SearchWord = collection["appendedInputButtons"];

            var MedicineList = (List<mMedicine>)Helper.ExecuteService("Budget", "SearchMedicine", SearchWord);

            ViewBag.PageSize = pageSize;
            List<mMedicine> pagedRecord = new List<mMedicine>();
            if (pageId == 1)
            {
                pagedRecord = MedicineList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = MedicineList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(MedicineList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/Medicine/_UpdateMedicineList.cshtml", pagedRecord);
        }
        #endregion
    }
}
