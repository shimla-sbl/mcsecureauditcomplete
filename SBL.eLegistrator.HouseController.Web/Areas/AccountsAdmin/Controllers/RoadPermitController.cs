﻿using EvoPdf;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.RoadPermit;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class RoadPermitController : Controller
    {
        //
        // GET: /AccountsAdmin/RoadPermit/

        #region Road Permit
        public ActionResult Index()
        {
            var model = (List<mPermit>)Helper.ExecuteService("RoadPermit", "GetAllPermitDetails", null);
            if (TempData["Msg"] != null)
            {
                ViewBag.Msg = TempData["Msg"].ToString();
                ViewBag.MsgCss = TempData["MsgCss"].ToString();
                ViewBag.Notification = TempData["Notification"].ToString();
            }


            return PartialView("/Areas/AccountsAdmin/Views/RoadPermit/_RoadPermitIndex.cshtml", model);
        }



        public ActionResult CreateRoadPermit()
        {

            RoadPermitViewModel model = new RoadPermitViewModel();
            model.AssemblyCol = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);


            //model.PermitCode = Helper.ExecuteService("RoadPermit", "GetNewRoadPermitCode", null).ToString();
            model.DesignationCol = (List<mMemberDesignation>)Helper.ExecuteService("RoadPermit", "GetAllDesignations", null);
            //model.MemberCol = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetMemberListByDesignation", "0");
            model.MemberCol = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetAllMembers", "0");
            model.SealedRoadsCol = (List<mSealedRoads>)Helper.ExecuteService("RoadPermit", "GetAllSealedRoads", null);

            model.RestrictedRoadsCol = (List<mRestrictedRoads>)Helper.ExecuteService("RoadPermit", "GetAllRestrictedRoads", null);
            //DateTime startDate = new DateTime(DateTime.Today.Year, 4, 1); // Financial Year Start
            //model.ValidFrom = startDate.ToString("dd/MM/yyyy");
            //DateTime endDate = new DateTime(DateTime.Today.Year + 1, 3, 31); // Financial Year End
            //model.ValidTo = endDate.ToString("dd/MM/yyyy");
            return PartialView("/Areas/AccountsAdmin/Views/RoadPermit/_CreateRoadPermit.cshtml", model);
        }

        public ActionResult UpdateRoadPermit(int permitNo)
        {
            RoadPermitViewModel model = new RoadPermitViewModel();
            model.AssemblyCol = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            model.DesignationCol = (List<mMemberDesignation>)Helper.ExecuteService("RoadPermit", "GetAllDesignations", null);
            model.MemberCol = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetAllMembers", "0");
            model.SealedRoadsCol = (List<mSealedRoads>)Helper.ExecuteService("RoadPermit", "GetAllSealedRoads", null);
            model.RestrictedRoadsCol = (List<mRestrictedRoads>)Helper.ExecuteService("RoadPermit", "GetAllRestrictedRoads", null);
            model.PermitNo = permitNo;
            var list = (List<mPermit>)Helper.ExecuteService("RoadPermit", "GetAllIndivisualPermitDetails", model);

            foreach (var element in list)
            {
                model.AssemblyId = element.AssemblyId;
                //  model.VehicleNo = element.VehicleNo;
                model.DesignationCode = element.DesignationCode;
            }
            // model.ValidFrom =Convert.ToString(element.ValidFrom);
            // model.ValidTo = Convert.ToString(element.ValidTo);


            return PartialView("/Areas/AccountsAdmin/Views/RoadPermit/_CreateRoadPermit.cshtml", model);
        }


        public ActionResult EditUpdateRoadPermit(int permitNo)
        {
            RoadPermitViewModel model = new RoadPermitViewModel();
            model.AssemblyCol = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            model.DesignationCol = (List<mMemberDesignation>)Helper.ExecuteService("RoadPermit", "GetAllDesignations", null);
            model.MemberCol = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetAllMembers", "0");

            model.RestrictedRoadsCol = (List<mRestrictedRoads>)Helper.ExecuteService("RoadPermit", "GetAllRestrictedRoads", null);
            model.RestrictedRoadsColA = (List<tPermitRestrictedRoads>)Helper.ExecuteService("RoadPermit", "GetAllPermitRestrictedDetails", new tPermitRestrictedRoads { PermitNo = permitNo }); ;

            model.SealedRoadsCol = (List<mSealedRoads>)Helper.ExecuteService("RoadPermit", "GetAllSealedRoads", null);
            model.SealedRoadsColA = (List<tPermitSealedRoads>)Helper.ExecuteService("RoadPermit", "GetAllPermitSealedDetails", new tPermitSealedRoads { PermitNo = permitNo }); ;

            model.Mode = "Update";
            model.PermitNo = permitNo;
            var list = (List<mPermit>)Helper.ExecuteService("RoadPermit", "GetAllIndivisualPermitDetails", model);
            foreach (var element in list)
            {
                model.AssemblyId = element.AssemblyId;
                model.DesignationCode = element.DesignationCode;
                model.MemberCode = element.MemberCode;
                model.PermitCode = element.PermitCode;
                model.MemberConstituencyCode = element.ConstituencyCode;
                model.VehicleNo = element.VehicleNo;
                //model.ValidFrom = element.ValidFrom.Value.Date("");
                model.ValidFrom = Convert.ToString(element.ValidFrom.Value.ToString("dd/MM/yyyy"));
                model.ValidTo = Convert.ToString(element.ValidTo.Value.ToString("dd/MM/yyyy"));
            }
            //  model.ConstituencyCol = (List<mConstituency>)Helper.ExecuteService("RoadPermit", "GetConstituencybyConCode", model);

            model.ConstituencyCol = (List<mConstituency>)Helper.ExecuteService("RoadPermit", "GetAllConstituency", model);
            return PartialView("/Areas/AccountsAdmin/Views/RoadPermit/_CreateRoadPermit.cshtml", model);
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateRoadPermitData(string permitNo, string permitCode, int? memberCode,
        string vehicleNo, string validFrom, string validTo, string sealedRoadsList, string restrictedRoadsList, int Constituency, int Deignation)
        {
            try
            {
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                RoadPermitViewModel model = new RoadPermitViewModel();

                model.AssemblyId = siteSettingMod.AssemblyCode;
                model.PermitCode = permitCode;
                model.PermitNo = Int32.Parse(permitNo);
                model.DesignationCode = Deignation;
                model.ConstituencyCode = Constituency;

                model.MemberCode = memberCode ?? 0;

                model.VehicleNo = vehicleNo;

                model.ValidFrom = validFrom;

                model.ValidTo = validTo;

                model.SealedRoadsCol = new List<mSealedRoads>();

                model.RestrictedRoadsCol = new List<mRestrictedRoads>();

                string[] selectedSealedRoads = sealedRoadsList.Split(new string[] { "$#**&##" },
                        StringSplitOptions.RemoveEmptyEntries);

                string[] selectedRestrictedRoads = restrictedRoadsList.Split(new string[] { "$#**&##" },
                        StringSplitOptions.RemoveEmptyEntries);

                if (selectedSealedRoads != null && selectedSealedRoads.Length > 0)
                {
                    foreach (string road in selectedSealedRoads)
                    {
                        model.SealedRoadsCol.Add(new mSealedRoads()
                        {
                            RoadCode = road
                        });
                    }
                }

                if (selectedRestrictedRoads != null && selectedRestrictedRoads.Length > 0)
                {
                    foreach (string road in selectedRestrictedRoads)
                    {
                        model.RestrictedRoadsCol.Add(new mRestrictedRoads()
                        {
                            RoadCode = road
                        });
                    }
                }

                Helper.ExecuteService("RoadPermit", "UpdateRoadPermitNew", model);

                ViewBag.Msg = "Road permit sucessfull saved";
                ViewBag.MsgCss = "alert-success";
                ViewBag.Notification = "Success";

            }
            catch (Exception)
            {
                ViewBag.Msg = "Unable to save Road permit";
                ViewBag.MsgCss = "alert-danger";
                ViewBag.Notification = "Error";
                throw;
            }

            var data = (List<mPermit>)Helper.ExecuteService("RoadPermit", "GetAllPermitDetails", null);
            return PartialView("/Areas/AccountsAdmin/Views/RoadPermit/_RoadPermitList.cshtml", data);
        }
        public void CreatepdfNew(string RoadPermitData)
        {
            string outXml = RoadPermitData;
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
            byte[] outPdfBuffer = null;
            string htmlString = outXml;
            string baseUrl = "";
            outPdfBuffer = pdfConverter.ConvertHtml(htmlString, baseUrl);
            Response.AddHeader("Content-Type", "application/pdf");
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename=RoadPermit.pdf; size={1}", "inline", outPdfBuffer.Length.ToString()));
            Response.BinaryWrite(outPdfBuffer);
            Response.End();

        }
        public ActionResult DownloadPdfNew(int PermitId)
        {
            try
            {
                string RoadPermitData = string.Empty;

                var PermitModel = (mPermit)Helper.ExecuteService("RoadPermit", "GetRoadPermitById", new mPermit { PermitNo = PermitId });

                RoadPermitData = GetSealedRoadPermitHTML(PermitModel);

                CreatepdfNew(RoadPermitData);

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
            //byte[] bytes = System.IO.File.ReadAllBytes(path);
            //return File(bytes, "application/pdf");
            return new EmptyResult();
        }

        public static string GetSealedRoadPermitHTML(mPermit mPermit)
        {

            StringBuilder Result = new StringBuilder();
            if (mPermit != null)
            {
                var SealedRoads = (List<tPermitSealedRoads>)Helper.ExecuteService("RoadPermit", "GetAllPermitSealedDetails", new tPermitSealedRoads { PermitNo = mPermit.PermitNo }); ;

                var RstrictedRoads = (List<tPermitRestrictedRoads>)Helper.ExecuteService("RoadPermit", "GetAllPermitRestrictedDetails", new tPermitRestrictedRoads { PermitNo = mPermit.PermitNo }); ;



                // Crete Html Template for Authority Letter
                Result.Append("<body style='width: auto; margin-top: 400px;  font-family:Verdana;'><br>");

                Result.Append("  <div> <div> <div style='align-content: center; text-align: center; font-weight: bold; font-size: 23px;'></div> <br>");
                Result.Append("<div style='align-content: center; text-align: center; font-weight: bold; font-size: 23px;'>");
                Result.Append("    </div>");

                Result.Append(" <div style='align-content: center; text-align: center; font-weight: bold; font-size: 23px;'> </div>");

                Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr> <tr></tr><tr></tr> <tr>");
                Result.Append(" </tr> </table></div>");

                Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr> <tr></tr><tr></tr> <tr>");
                Result.Append("</tr> </table></div>");

                Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr> <tr>");
                Result.Append("</tr> </table></div>");
                Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr> <tr></tr><tr></tr> <tr>");
                Result.Append("</tr> </table></div>");

                Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr> <tr></tr><tr></tr> <tr>");
                Result.Append("</tr> </table></div>");
                Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr> <tr></tr><tr></tr> <tr>");
                Result.Append("</tr> </table></div>");
                //Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr> <tr></tr><tr></tr> <tr>");
                //Result.Append("</tr> </table></div>");
                //Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr> <tr></tr><tr></tr> <tr>");
                //Result.Append("</tr> </table></div>");
                Result.Append(" <div><table border='0' style='width: 100%;font-weight: bold; font-size: 18px;'>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");
                Result.Append("<tr></tr>");

                Result.Append("<tr>");
                Result.Append(string.Format("<td style='text-align: left; padding-left:385px;width:45%;'>{0}</td>", mPermit.PermitCode));
                Result.Append(string.Format("<td style='text-align: left;padding-left:140px;width:55%;'>{0}</td></tr>", Convert.ToDateTime(mPermit.ValidFrom).ToString("dd/MMM/yyyy")));

                Result.Append("<td style='width: 5%;'></td> <td style='text-align: left; font-weight: bold;'></td>");
                Result.Append(" <tr></tr><tr></tr><tr></tr><tr> <td style='text-align: left; font-weight: bold;'></td>");

                string Abbr;
                if (mPermit.Designation != null)
                {
                    
                    if (mPermit.Designation == "Member Legislative Assembly")
                    {
                        Abbr = "MLA";
                        Result.Append(string.Format("<td style='text-align: left;padding-left:10px;'> {0} {1} {2} {3}</td> </tr>", mPermit.Prefix, mPermit.MemberName, ",", Abbr));
                    }
                    else if (mPermit.Designation == "Chief Whip")
                    {
                        Abbr = "Chief Whip";
                        Result.Append(string.Format("<td style='text-align: left;padding-left:10px;'> {0} {1} {2} {3}</td> </tr>", mPermit.Prefix, mPermit.MemberName, ",", Abbr));
                    }
                    else if (mPermit.Designation == "Speaker")
                    {
                        Abbr = "HON'BLE SPEAKER";
                        Result.Append(string.Format("<td style='text-align: left;padding-left:10px;'> {0} {1} {2}</td> </tr>", Abbr, ",", "HPVS"));
                    }
                    else if (mPermit.Designation == "Deputy Speaker")
                    {
                        Abbr = "HON'BLE DY.SPEAKER";
                        Result.Append(string.Format("<td style='text-align: left;padding-left:10px;'> {0} {1} {2}</td> </tr>", Abbr, ",", "HPVS"));
                    }
                }
                else
                {
                    Result.Append(string.Format("<td style='text-align: left;padding-left:10px;'> {0} {1} {2} </td> </tr>", mPermit.Prefix, mPermit.MemberName, mPermit.Designation));

                }
                Result.Append("<tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr> <td style='text-align: center; font-weight: bold;'> </td>");
                Result.Append(string.Format(" <td style='text-align: left; padding-left:30px;'>{0}</td> </tr>", mPermit.VehicleNo));

                Result.Append("<tr></tr></tr><tr></tr><tr></tr> </table></div>");
                Result.Append(" <div><table border='0' style='width: 100%;font-weight: bold; font-size: 18px;'>");

                Result.Append("<tr style='text-align: center;'>");
                Result.Append(string.Format("<td style='text-align:right;padding-right:530px;'>{0}</td> ", Convert.ToDateTime(mPermit.ValidTo).ToString("dd/MMM/yyyy")));
                Result.Append(string.Format(" </tr>"));
                if (SealedRoads != null && SealedRoads.Count() > 0)
                {
                    string sldroad = "";
                    foreach (var item in SealedRoads)
                    {
                        sldroad = sldroad + Convert.ToString(item.RoadCode) + ",";


                    }
                    sldroad = sldroad.TrimEnd(',');


                    Result.Append("</tr> </table></div>");
                    Result.Append(" <div><table border='0' style='width: 100%;font-weight: bold; font-size: 18px;'>");

                    Result.Append("<tr style='text-align: center;'>");
                    //Result.Append(string.Format("<td style='text-align:right;padding-right:530px;'>{0}</td> ", sldroad));
                    Result.Append(string.Format("<td style='text-align:left;padding-left:330px;'>{0}</td> ", sldroad));
                    Result.Append(string.Format(" </tr>"));



                }
                //Result.Append("  </tr>");
                if (RstrictedRoads != null && RstrictedRoads.Count() > 0)
                {
                    string rstroad = "";
                    foreach (var item in RstrictedRoads)
                    {
                        rstroad = rstroad + Convert.ToString(item.RoadCode) + ",";

                    }

                    rstroad = rstroad.TrimEnd(',');


                    Result.Append("<tr>");
                    Result.Append(string.Format(" </tr>"));

                    Result.Append("<tr>");
                    Result.Append(string.Format(" </tr>"));
                    Result.Append("<tr>");
                    Result.Append(string.Format(" </tr>"));


                    Result.Append("<tr>");
                    Result.Append(string.Format(" </tr>"));
                    Result.Append("<tr style='text-align: center;'>");
                    //Result.Append(string.Format("<td style='text-align:right;padding-right:460px;'>{0}</td> ", rstroad));
                    Result.Append(string.Format("<td style='text-align:left;padding-left:330px;'>{0}</td> ", rstroad));
                    Result.Append(string.Format(" </tr>"));


                    //Result.Append("<tr>");
                    //Result.Append(string.Format("<tr></tr><tr></tr><tr></tr><tr></tr> <td style='text-align: right; padding-right:-280px;'>{0}</td> ", rstroad));
                    //Result.Append(string.Format(" </tr>"));
                }

                Result.Append(" <br /> <br><br>");
            }
            else
            {
                Result.Append("Unable to print Road Permit due to some technical permission");
            }
            Result.Replace("{", " ");
            Result.Replace("}", " ");
            Result.Append("</body>");
            return Result.ToString();
        }
        

        //public static string GetSealedRoadPermitHTML(mPermit mPermit)
        //{

        //    StringBuilder Result = new StringBuilder();
        //    if (mPermit != null)
        //    {
        //        var SealedRoads = (List<tPermitSealedRoads>)Helper.ExecuteService("RoadPermit", "GetAllPermitSealedDetails", new tPermitSealedRoads { PermitNo = mPermit.PermitNo }); ;

        //        var RstrictedRoads = (List<tPermitRestrictedRoads>)Helper.ExecuteService("RoadPermit", "GetAllPermitRestrictedDetails", new tPermitRestrictedRoads { PermitNo = mPermit.PermitNo }); ;



        //        // Crete Html Template for Authority Letter
        //        Result.Append("<body style='width: auto; margin-top: 400px;  font-family:Verdana;'><br>");

        //        Result.Append("  <div> <div> <div style='align-content: center; text-align: center; font-weight: bold; font-size: 23px;'></div> <br>");
        //        Result.Append("<div style='align-content: center; text-align: center; font-weight: bold; font-size: 23px;'>");
        //        Result.Append("    </div>");

        //        Result.Append(" <div style='align-content: center; text-align: center; font-weight: bold; font-size: 23px;'> </div>");

        //        Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr> <tr></tr><tr></tr> <tr>");
        //        Result.Append(" </tr> </table></div>");

        //        Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr> <tr></tr><tr></tr> <tr>");
        //        Result.Append("</tr> </table></div>");

        //        Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr> <tr>");
        //        Result.Append("</tr> </table></div>");
        //        Result.Append(" <div > <table style='width:auto;font-weight: bold; font-size: 20px;'><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr> <tr></tr><tr></tr> <tr>");
        //        Result.Append("</tr> </table></div>");

        //        Result.Append(" <div><table border='0' style='width: 100%;font-weight: bold; font-size: 18px;'>");
        //        Result.Append("<tr>");
        //        Result.Append(string.Format("<td style='text-align: left; padding-left:355px;width:45%;'>{0}</td>", mPermit.PermitCode));
        //        Result.Append(string.Format("<td style='text-align: left;padding-left:150px;width:55%;'>{0}</td></tr>", Convert.ToDateTime(mPermit.ValidFrom).ToString("dd/MMM/yyyy")));

        //        Result.Append("<td style='width: 5%;'></td> <td style='text-align: left; font-weight: bold;'></td>");
        //        Result.Append(" <tr></tr><tr></tr><tr></tr><tr> <td style='text-align: left; font-weight: bold;'></td>");

        //        string Abbr;
        //        if (mPermit.Designation != null)
        //        {
        //            if (mPermit.Designation == "Member Legislative Assembly")
        //            {
        //                Abbr = "MLA";
        //                Result.Append(string.Format("<td style='text-align: left;padding-left:10px;'> {0} {1} {2} {3}</td> </tr>", mPermit.Prefix, mPermit.MemberName, ",", Abbr));
        //            }
        //            else if (mPermit.Designation == "Speaker")
        //            {
        //                Abbr = "HON'BLE SPEAKER";
        //                Result.Append(string.Format("<td style='text-align: left;padding-left:10px;'> {0} {1} {2}</td> </tr>", Abbr, ",", "HPVS"));
        //            }
        //            else if (mPermit.Designation == "Deputy Speaker")
        //            {
        //                Abbr = "HON'BLE DY.SPEAKER";
        //                Result.Append(string.Format("<td style='text-align: left;padding-left:10px;'> {0} {1} {2}</td> </tr>", Abbr, ",", "HPVS"));
        //            }
        //        }
        //        else
        //        {
        //            Result.Append(string.Format("<td style='text-align: left;padding-left:10px;'> {0} {1} {2} </td> </tr>", mPermit.Prefix, mPermit.MemberName, mPermit.Designation));

        //        }
        //        Result.Append("<tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr> <td style='text-align: center; font-weight: bold;'> </td>");
        //        Result.Append(string.Format(" <td style='text-align: left; padding-left:30px;'>{0}</td> </tr>", mPermit.VehicleNo));

        //        Result.Append("<tr></tr><tr></tr><tr></tr><tr></tr><tr></tr></tr><tr></tr><tr></tr> </table></div>");
        //        Result.Append(" <div><table border='0' style='width: 100%;font-weight: bold; font-size: 18px;'>");

        //        Result.Append("<tr style='text-align: center;'>");
        //        Result.Append(string.Format("<td style='text-align:right;padding-right:530px;'>{0}</td> ", Convert.ToDateTime(mPermit.ValidTo).ToString("dd/MMM/yyyy")));
        //        Result.Append(string.Format(" </tr>"));
        //        if (SealedRoads != null && SealedRoads.Count() > 0)
        //        {
        //            string sldroad = "";
        //            foreach (var item in SealedRoads)
        //            {
        //                sldroad = sldroad + Convert.ToString(item.RoadCode) + ",";


        //            }
        //            sldroad = sldroad.TrimEnd(',');


        //            Result.Append("</tr> </table></div>");
        //            Result.Append(" <div><table border='0' style='width: 100%;font-weight: bold; font-size: 18px;'>");

        //            Result.Append("<tr style='text-align: center;'>");
        //            //Result.Append(string.Format("<td style='text-align:right;padding-right:530px;'>{0}</td> ", sldroad));
        //            Result.Append(string.Format("<td style='text-align:left;padding-left:330px;'>{0}</td> ", sldroad));
        //            Result.Append(string.Format(" </tr>"));



        //        }
        //        //Result.Append("  </tr>");
        //        if (RstrictedRoads != null && RstrictedRoads.Count() > 0)
        //        {
        //            string rstroad = "";
        //            foreach (var item in RstrictedRoads)
        //            {
        //                rstroad = rstroad + Convert.ToString(item.RoadCode) + ",";

        //            }

        //            rstroad = rstroad.TrimEnd(',');


        //            Result.Append("<tr>");
        //            Result.Append(string.Format(" </tr>"));

        //            Result.Append("<tr>");
        //            Result.Append(string.Format(" </tr>"));
        //            Result.Append("<tr>");
        //            Result.Append(string.Format(" </tr>"));


        //            Result.Append("<tr>");
        //            Result.Append(string.Format(" </tr>"));
        //            Result.Append("<tr style='text-align: center;'>");
        //            //Result.Append(string.Format("<td style='text-align:right;padding-right:460px;'>{0}</td> ", rstroad));
        //            Result.Append(string.Format("<td style='text-align:left;padding-left:330px;'>{0}</td> ", rstroad));
        //            Result.Append(string.Format(" </tr>"));


        //            //Result.Append("<tr>");
        //            //Result.Append(string.Format("<tr></tr><tr></tr><tr></tr><tr></tr> <td style='text-align: right; padding-right:-280px;'>{0}</td> ", rstroad));
        //            //Result.Append(string.Format(" </tr>"));
        //        }

        //        Result.Append(" <br /> <br><br>");
        //    }
        //    else
        //    {
        //        Result.Append("Unable to print Road Permit due to some technical permission");
        //    }
        //    Result.Replace("{", " ");
        //    Result.Replace("}", " ");
        //    Result.Append("</body>");
        //    return Result.ToString();
        //}



        public JsonResult GetNewPermitCode()
        {
            RoadPermitViewModel model = new RoadPermitViewModel();
            model.PermitCode = Helper.ExecuteService("RoadPermit", "GetNewRoadPermitCode", null).ToString();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMemberListByDesignation(int deptId, int AssemblyId)
        {
            RoadPermitViewModel model = new RoadPermitViewModel();
            model.AssemblyId = AssemblyId;
            model.DesignationCode = deptId;
            //   int selectedDeptId = deptId ?? 0;
            model.MemberCol = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetMemberListByDesignation", model);
            if (model.MemberCol.Count > 0)
            {
                model.ConstituencyCol = (List<mConstituency>)Helper.ExecuteService("RoadPermit", "GetAllConstituency", model);
                model.MemberCode = model.MemberCol.ElementAt(0).MemberCode;

                model.MemberConstituencyCode = (int)Helper.ExecuteService("RoadPermit", "GetLastConstituencyByMember", model);
                model.Address = Helper.ExecuteService("RoadPermit", "GetAddressByMember", model).ToString();
            }
            else
            {
                model.ConstituencyCol = new List<mConstituency>();
                model.MemberCode = 0;
                model.MemberConstituencyCode = 0;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLastConstituency(int? memberCode, int AssemblyId)
        {
            RoadPermitViewModel model = new RoadPermitViewModel();
            model.AssemblyId = AssemblyId;
            model.MemberCode = memberCode ?? 0;
            model.MemberConstituencyCode = (int)Helper.ExecuteService("RoadPermit", "GetLastConstituencyByMember", model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllAssembly()
        {
            RoadPermitViewModel model = new RoadPermitViewModel();
            model.AssemblyCol = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult SaveRoadPermitData(string permitNo, string permitCode, int? memberCode,
        string vehicleNo, string validFrom, string validTo, string sealedRoadsList, string restrictedRoadsList, int Constituency, int Deignation)
        {
            try
            {
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                RoadPermitViewModel model = new RoadPermitViewModel();

                model.AssemblyId = siteSettingMod.AssemblyCode;
                model.PermitCode = permitCode;

                model.DesignationCode = Deignation;
                model.ConstituencyCode = Constituency;

                model.MemberCode = memberCode ?? 0;

                model.VehicleNo = vehicleNo;

                model.ValidFrom = validFrom;

                model.ValidTo = validTo;

                model.SealedRoadsCol = new List<mSealedRoads>();

                model.RestrictedRoadsCol = new List<mRestrictedRoads>();

                string[] selectedSealedRoads = sealedRoadsList.Split(new string[] { "$#**&##" },
                        StringSplitOptions.RemoveEmptyEntries);

                string[] selectedRestrictedRoads = restrictedRoadsList.Split(new string[] { "$#**&##" },
                        StringSplitOptions.RemoveEmptyEntries);

                if (selectedSealedRoads != null && selectedSealedRoads.Length > 0)
                {
                    foreach (string road in selectedSealedRoads)
                    {
                        model.SealedRoadsCol.Add(new mSealedRoads()
                        {
                            RoadCode = road
                        });
                    }
                }

                if (selectedRestrictedRoads != null && selectedRestrictedRoads.Length > 0)
                {
                    foreach (string road in selectedRestrictedRoads)
                    {
                        model.RestrictedRoadsCol.Add(new mRestrictedRoads()
                        {
                            RoadCode = road
                        });
                    }
                }

                int PermitId = (int)Helper.ExecuteService("RoadPermit", "SaveRoadPermit", model);
                if (PermitId > 0)
                {
                    model.PermitNo = PermitId;
                    Helper.ExecuteService("RoadPermit", "SavePermitRestrictedRoads", model);
                    Helper.ExecuteService("RoadPermit", "SavePermitSealedRoads", model);

                }
                ViewBag.Msg = "Road permit sucessfull saved";
                ViewBag.MsgCss = "alert-success";
                ViewBag.Notification = "Success";

            }
            catch (Exception)
            {
                ViewBag.Msg = "Unable to save Road permit";
                ViewBag.MsgCss = "alert-danger";
                ViewBag.Notification = "Error";
                throw;
            }

            var data = (List<mPermit>)Helper.ExecuteService("RoadPermit", "GetAllPermitDetails", null);
            return PartialView("/Areas/AccountsAdmin/Views/RoadPermit/_RoadPermitList.cshtml", data);
        }



        public string CretePdf(string RoadPermitData)
        {

            string Result = RoadPermitData;


            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;

            // set the header height in points
            pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;

            TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan v1.0.0",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement1.TextAlign = HorizontalTextAlign.Left;
            TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement3.TextAlign = HorizontalTextAlign.Right;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 20, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(Result);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }



            string path = "";
            try
            {

                Guid FId = Guid.NewGuid();
                string fileName = FId + "_VehicleRoadPermit.pdf";

                //  string url = "/RoadPermit/";

                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "FileLocation" });
                string FileLocation = SiteSettingsSessn.SettingValue;

                string url = FileLocation + "RoadPermit/";

                string directory = url;

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                path = Path.Combine(url, fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();



                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                pdfDocument.Close();

            }
        }
        public ActionResult DownloadPdf(int PermitId)
        {
            string path = string.Empty;
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "FileLocation" });
            string FileLocation = SiteSettingsSessn.SettingValue;

            string url = FileLocation + "RoadPermit/";

            string FileName = string.Empty;
            try
            {
                string RoadPermitData = string.Empty;

                var PermitModel = (mPermit)Helper.ExecuteService("RoadPermit", "GetRoadPermitById", new mPermit { PermitNo = PermitId });


                // if (PermitModel != null && string.IsNullOrEmpty(PermitModel.FileName))
                // {
                RoadPermitData = GetRoadPermitHTML(PermitModel);
                PermitModel.FileName = FileName = CretePdf(RoadPermitData);

                Helper.ExecuteService("RoadPermit", "UpdateRoadPermit", PermitModel);
                //  }
                //else
                //{
                //    FileName = PermitModel.FileName;
                //}

                tPermitSealedRoads obj2 = new tPermitSealedRoads();
                path = Path.Combine(url, FileName);

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
            byte[] bytes = System.IO.File.ReadAllBytes(path);
            return File(bytes, "application/pdf");
            //  return new EmptyResult();
        }


        public static string GetRoadPermitHTML(mPermit mPermit)
        {

            StringBuilder Result = new StringBuilder();
            if (mPermit != null)
            {
                var SealedRoads = (List<tPermitSealedRoads>)Helper.ExecuteService("RoadPermit", "GetAllPermitSealedDetails", new tPermitSealedRoads { PermitNo = mPermit.PermitNo }); ;

                var RstrictedRoads = (List<tPermitRestrictedRoads>)Helper.ExecuteService("RoadPermit", "GetAllPermitRestrictedDetails", new tPermitRestrictedRoads { PermitNo = mPermit.PermitNo }); ;

                // Crete Html Template for Authority Letter
                Result.Append("<body style='width: 700px; margin: auto;  font-family:Verdana;'><br>");

                Result.Append("  <div> <div> <div style='align-content: center; text-align: center; font-weight: bold; font-size: 23px;'>सूची-2 </div> <br>");
                Result.Append("<div style='align-content: center; text-align: center; font-weight: bold; font-size: 23px;'>");
                Result.Append("    HIMACHAL PRADESH VIDHAN SABHA SECRETARIAT<br /> SEALED/RESTRICTED ROAD PERMIT  </div>");

                Result.Append(" <div style='align-content: center; text-align: center; font-weight: bold; font-size: 23px;'> File No. VS-3M(V.PERMIT)MLAs-6/2018 </div>");
                Result.Append(" <hr style='border: 1px solid #000' /> </div>");
                Result.Append("     <div > <table border='0' style='width: 100%;font-weight: bold; font-size: 20px;'>  <tr>");

                // start change here
                Result.Append(" <td style='text-align: left; font-weight: bold;'>Permit No:  </td>");
                Result.Append(string.Format("<td style='text-align: left'>{0}</td>", mPermit.PermitCode));
                Result.Append("<td style='width: 20%;'></td> <td style='text-align: right; font-weight: bold;'>Constituency Name: </td>");
                Result.Append(string.Format("<td style='text-align: right'>{0}-{1}</td>", mPermit.ConstituencyCode, mPermit.ConstituencyName));
                Result.Append("</tr> </table></div>");
                Result.Append(" <div> <hr style='border: 1px solid #000' />  <table border='0' style='width: 100%;font-weight: bold; font-size: 20px;'>");
                Result.Append(" <tr> <td style='text-align: left; font-weight: bold;'>Name and Designation: </td>");
                Result.Append(string.Format("  <td style='text-align: left'> {0}. {1} {2} </td> </tr>", mPermit.Prefix, mPermit.MemberName, mPermit.Designation));
                Result.Append("<tr> <td style='text-align: left; font-weight: bold;'>Address:</td>");
                Result.Append(string.Format(" <td style='text-align: left'>{0}</td></tr>", mPermit.Address));
                Result.Append("<tr> <td style='text-align: left; font-weight: bold;'>Vehicle No: </td>");
                Result.Append(string.Format(" <td style='text-align: left'>{0}</td> </tr>", mPermit.VehicleNo));
                Result.Append(" <tr> <td style='text-align: left; font-weight: bold;'>Valid From: </td>");
                Result.Append(string.Format("<td style='text-align: left'>{0}</td> </tr>", Convert.ToDateTime(mPermit.ValidFrom).ToString("dd/MMM/yyyy")));
                Result.Append("  <tr> <td style='text-align: left; font-weight: bold;'>Valid Till: </td>");
                Result.Append(string.Format("<td style='text-align: left'>{0}</td>", Convert.ToDateTime(mPermit.ValidTo).ToString("dd/MMM/yyyy")));
                Result.Append("  </tr> </table> </div> <hr style='border: 1px solid #000' />");
                Result.Append("<div style='font-weight: bold; font-size: 20px;'>Roads for which permit is valid:-</div> <div>");
                Result.Append(" <ol style='font-weight: bold; font-size: 18px;'>");
                if (SealedRoads != null && SealedRoads.Count() > 0)
                {
                    foreach (var item in SealedRoads)
                    {
                        Result.Append(string.Format(" <li>{0} {1}</li>", item.RoadCode, item.Description));
                    }
                }

                if (RstrictedRoads != null && RstrictedRoads.Count() > 0)
                {
                    foreach (var item in RstrictedRoads)
                    {
                        Result.Append(string.Format(" <li>{0} {1}</li>", item.RoadCode, item.Description));
                    }
                }
                Result.Append(" </ol></div> <br> <br>");
                //Result.Append("<div style='text-align: right; font-weight: bold; font-size: 18px;'><p>(Sunder Singh Verma)<br>Secretary,<br>  H.P. Vidhan Sabha.<br> Tel.No. 0177-2656424 </p>  </div> </div>");
                Result.Append("<div style='text-align: right; font-weight: bold; font-size: 18px;'><p>Secretary,<br>  H.P. Vidhan Sabha.<br> Tel.No. 0177-2656424 </p>  </div> </div>");

                Result.Append(" </div> <br> <div style='text-align: left; font-weight: bold; font-size: 18px;'> <p>Copy to:</p> <p> 1. Permit Holder<br>  2. S.P.Shimla </p> </div>");
                Result.Append(" <br /> <br><br>");
            }
            else
            {
                Result.Append("Unable to print Road Permit due to some technical permission");
            }
            Result.Append("</body>");
            return Result.ToString();
        }


        public JsonResult SaveRoadPermit(string permitNo, string permitCode, int? memberCode,
        string vehicleNo, string validFrom, string validTo, string sealedRoadsList, string restrictedRoadsList)
        {
            RoadPermitViewModel model = new RoadPermitViewModel();

            model.PermitCode = permitCode;

            model.MemberCode = memberCode ?? 0;

            model.VehicleNo = vehicleNo;

            model.ValidFrom = validFrom;

            model.ValidTo = validTo;

            model.SealedRoadsCol = new List<mSealedRoads>();

            model.RestrictedRoadsCol = new List<mRestrictedRoads>();

            string[] selectedSealedRoads = sealedRoadsList.Split(new string[] { "$#**&##" },
                    StringSplitOptions.RemoveEmptyEntries);

            string[] selectedRestrictedRoads = restrictedRoadsList.Split(new string[] { "$#**&##" },
                    StringSplitOptions.RemoveEmptyEntries);

            if (selectedSealedRoads != null && selectedSealedRoads.Length > 0)
            {
                foreach (string road in selectedSealedRoads)
                {
                    model.SealedRoadsCol.Add(new mSealedRoads()
                    {
                        RoadCode = road
                    });
                }
            }

            if (selectedRestrictedRoads != null && selectedRestrictedRoads.Length > 0)
            {
                foreach (string road in selectedRestrictedRoads)
                {
                    model.RestrictedRoadsCol.Add(new mRestrictedRoads()
                    {
                        RoadCode = road
                    });
                }
            }

            string res = Helper.ExecuteService("RoadPermit", "SaveRoadPermit", model).ToString();

            return Json(res, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult CancelRoadPermit(int Id, string Action)
        {
            var model = (mPermit)Helper.ExecuteService("RoadPermit", "GetRoadPermitById", new mPermit { PermitNo = Id });
            return PartialView("/Areas/AccountsAdmin/Views/RoadPermit/_CancelRoadPermit.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SaveCancelRoadPermit(mPermit model)
        {

            var StateToEdit = (mPermit)Helper.ExecuteService("RoadPermit", "GetRoadPermitById", new mPermit { PermitNo = model.PermitNo });
            if (model.IsCanceled)
                StateToEdit.CancelledDate = DateTime.Now;
            else
                StateToEdit.CancelledDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;

            StateToEdit.IsCanceled = model.IsCanceled;
            StateToEdit.Remarks = model.Remarks;
            Helper.ExecuteService("RoadPermit", "UpdateRoadPermit", StateToEdit);
            if (model.IsCanceled)
                ViewBag.Msg = "Road permit canceled successfully.";
            else
                ViewBag.Msg = "Road permit resolved successfully.";

            ViewBag.MsgCss = "alert-success";
            ViewBag.Notification = "Success";

            var Result = (List<mPermit>)Helper.ExecuteService("RoadPermit", "GetAllPermitDetails", null);

            return PartialView("/Areas/AccountsAdmin/Views/RoadPermit/_RoadPermitList.cshtml", Result);
        }
        #endregion

        #region Road Permit Report
        public ActionResult RoadPermitReport()
        {
            string url = "/RoadPermit/RoadPermitReport";

            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            // This is for showing selected Current Financial Year
            int CurrentYear = DateTime.Today.Year;
            int PreviousYear = DateTime.Today.Year - 1;
            int NextYear = DateTime.Today.Year + 1;
            string PreYear = PreviousYear.ToString();
            string NexYear = NextYear.ToString();
            string CurYear = CurrentYear.ToString();
            string FinYearValue = null;

            if (DateTime.Today.Month > 3)
                FinYearValue = string.Format("{0}-{1}", CurYear, NexYear);
            else
                FinYearValue = string.Format("{0}-{1}", PreYear, CurYear);

            RoadPermitViewModel model = new RoadPermitViewModel();
            ViewBag.CurrFinYearValue = FinYearValue;
            ViewBag.FinancialYearList = SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models.ModelMapping.GetFinancialYear(FinYearValue);
            return PartialView("/Areas/AccountsAdmin/Views/RoadPermit/_RoadPermitReportIndex.cshtml", model);
        }

        public PartialViewResult GetRoadPermitReport(string FinacialYear)
        {
            Session["RoadPermitListForReport"] = null;

            // this is for COntaining Search result in session for creating Excelsheet & for Pdf Document
            var Result = Session["RoadPermitListForReport"] = (List<mPermit>)Helper.ExecuteService("RoadPermit", "SearchRoadPermitDetails", FinacialYear);
            return PartialView("/Areas/AccountsAdmin/Views/RoadPermit/_RoadPermitReportList.cshtml", Result);
        }

        public ActionResult DownloadRoadPermitReportPdf(string FinacialYear)
        {

            Guid FId = Guid.NewGuid();
            string fileName = FId + "_RoadPermitReport.pdf";

            List<mPermit> SearchPermitList = new List<mPermit>();

            // this is for COntaining Search result in session for creating Excelsheet & for Pdf Document
            if (Session["RoadPermitListForReport"] != null)
                SearchPermitList = (List<mPermit>)Session["RoadPermitListForReport"];
            else
            {

                DateTime FYFrom = new DateTime(Convert.ToInt32(FinacialYear.Split('-')[0]), 4, 30);
                DateTime FYTo = new DateTime(Convert.ToInt32(FinacialYear.Split('-')[1]), 3, 31);

                StringBuilder sb = new StringBuilder();
                sb.Append(FYFrom.ToShortDateString() + ",");
                sb.Append(FYTo.ToShortDateString());

                SearchPermitList = (List<mPermit>)Helper.ExecuteService("RoadPermit", "SearchRoadPermitDetails", sb.ToString());
            }

            Session["RoadPermitListForReport"] = SearchPermitList;

            string Result = GetRoadPermitReportHTML(SearchPermitList, FinacialYear);



            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = true;

            // set the header height in points
            pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
            pdfConverter.PdfFooterOptions.FooterHeight = 60;

            TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
                    new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement.TextAlign = HorizontalTextAlign.Center;

            TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan v1.0.0",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement1.TextAlign = HorizontalTextAlign.Left;
            TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                   new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
            footerTextElement3.TextAlign = HorizontalTextAlign.Right;

            pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
            pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

            string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

            ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
            // imageElement1.KeepAspectRatio = true;
            imageElement1.Opacity = 100;
            imageElement1.DestHeight = 97f;
            imageElement1.DestWidth = 71f;
            pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
            ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
            imageElement2.KeepAspectRatio = false;
            imageElement2.Opacity = 5;
            imageElement2.DestHeight = 284f;
            imageElement2.DestWidth = 388F;

            EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(Result);
            float stampWidth = float.Parse("400");
            float stampHeight = float.Parse("400");

            // Center the stamp at the top of PDF page
            float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
            float stampYLocation = 150;

            RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

            Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
            stampTemplate.AddElement(imageElement2);
            byte[] pdfBytes = null;

            try
            {
                pdfBytes = pdfDocument.Save();
            }
            finally
            {
                // close the Document to realease all the resources
                pdfDocument.Close();
            }
            string path = "";
            try
            {


                string url = "/RoadPermit/RoadPermitReport";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "RoadPermitReport.pdf";

                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                pdfDocument.Close();
                // System.IO.File.Delete(path);
            }


#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }


        [HttpPost]
        public ActionResult DownloadExcel(string FinacialYear)
        {
            string Result = string.Empty;


            List<mPermit> SearchPermitList = new List<mPermit>();
            if (Session["RoadPermitListForReport"] != null)
                SearchPermitList = (List<mPermit>)Session["RoadPermitListForReport"];
            else
            {

                DateTime FYFrom = new DateTime(Convert.ToInt32(FinacialYear.Split('-')[0]), 4, 30);
                DateTime FYTo = new DateTime(Convert.ToInt32(FinacialYear.Split('-')[1]), 3, 31);

                StringBuilder sb = new StringBuilder();
                sb.Append(FYFrom.ToShortDateString() + ",");
                sb.Append(FYTo.ToShortDateString());

                SearchPermitList = (List<mPermit>)Helper.ExecuteService("RoadPermit", "SearchRoadPermitDetails", sb.ToString());
            }

            Session["RoadPermitListForReport"] = SearchPermitList;

            Result = GetRoadPermitReportHTML(SearchPermitList, FinacialYear);


            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=RoadPermitReport.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }


        public static string GetRoadPermitReportHTML(List<mPermit> List, string FinacialYear)
        {

            StringBuilder Result = new StringBuilder();
            if (List != null && List.Count() > 0)
            {
                Result.Append(string.Format("<div class='panel panel-default' style='width:1000px;font-size:22px;text-align:center'><div><h2>HIMACHAL PRADESH VIDHAN SABHA SECRETARIAT </h2><h3>SEALED/RESTRICTED ROAD PERMIT REPORT FOR FINANCIAL YEAR <br>{0} </h3></div>", FinacialYear));
                Result.Append("<table class='table table-condensed table-bordered'  id='ResultTable'>");
                Result.Append("<thead class='header' ><tr style='background-color: #6fb3e0 ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:50px;text-align:center;'>Sr. No.</th>");
                Result.Append("<th style='width:120px;text-align:left; ' >Permit No.</th><th style='width:180px;text-align:left;'>Name </th><th style='width:180px;text-align:left;' >List of S/R Roads code</th>");
                Result.Append("<th style='width:120px;text-align:left;' >Vehicle No.</th><th style='width:120px;text-align:left;'>Valid From</th><th style='width:120px;text-align:left;' >Valid Till</th><th style='width:120px;text-align:center;' >Canceled Date</th>");

                Result.Append("</tr></thead>");


                Result.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in List)
                {
                    count++;
                    var SealedRoads = ((List<tPermitSealedRoads>)Helper.ExecuteService("RoadPermit", "GetAllPermitSealedDetails", new tPermitSealedRoads { PermitNo = obj.PermitNo })).Select(m => m.RoadCode).ToArray();
                    var SR = String.Join(",", SealedRoads);
                    var RstrictedRoads = ((List<tPermitRestrictedRoads>)Helper.ExecuteService("RoadPermit", "GetAllPermitRestrictedDetails", new tPermitRestrictedRoads { PermitNo = obj.PermitNo })).Select(m => m.RoadCode).ToArray();
                    string RR = String.Join(",", RstrictedRoads);

                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", count));
                    Result.Append(string.Format("<td style='border: 1px dotted #808080;'>'{0}'</td>", obj.PermitCode));
                    // Result.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}-{1}</td>", obj.ConstituencyCode, obj.ConstituencyName));
                    Result.Append(string.Format("<td style='border: 1px dotted #808080;'>{0} {1}</td>", obj.Prefix, obj.MemberName));
                    Result.Append(string.Format("<td style='border: 1px dotted #808080;'>{0},{1}</td>", SR, RR));
                    Result.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.VehicleNo));
                    Result.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.ValidFrom).ToString("dd/MM/yyyy")));
                    Result.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.ValidTo).ToString("dd/MM/yyyy")));
                    if (Convert.ToDateTime(obj.CancelledDate).ToString("dd/MMM/yyyy").Equals("01/Jan/0001") || Convert.ToDateTime(obj.CancelledDate).ToString("dd/MMM/yyyy").Equals("01/Jan/1753"))
                        Result.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "-"));
                    else
                        Result.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.CancelledDate).ToString("dd/MM/yyyy")));

                    Result.Append("</tr>");
                }
                Result.Append("</tbody></table></div> ");
            }
            else
            {
                Result.Append("No Road Permit Found");
            }
            Result.Append("</body>");
            return Result.ToString();
        }

        #endregion

    }
}
