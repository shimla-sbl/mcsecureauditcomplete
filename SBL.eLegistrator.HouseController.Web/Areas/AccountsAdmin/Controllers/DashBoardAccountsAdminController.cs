﻿using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class DashBoardAccountsAdminController : Controller
    {
        //
        // GET: /AccountsAdmin/DashBoardAccountsAdmin/

        public ActionResult Index()
        {
            return View(); 
        }

        public ActionResult ViewUsers()
        {
            try
            {
                var Content = Helper.ExecuteService("User", "GetAllUsersOfAccountType", null);

                ViewBag.SaveMessage = TempData["roles"];

                return View(Content);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public ActionResult ChangeRoles(Guid id)
        {
            try
            {
                // var value = Convert.ToString(id);
                var Content = Helper.ExecuteService("User", "GetUserOfAccountTypeById", new mUserAccountRoles { userID = id });
                return View(Content);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public ActionResult ManageUsers(Guid id)
        {
            try
            {
                // var value = Convert.ToString(id);
                mUserAccountRoles model = new mUserAccountRoles();

                model = (mUserAccountRoles)Helper.ExecuteService("User", "GetUserOfAccountTypeById", new mUserAccountRoles { userID = id });

                model.RolesList = (List<RoleList>)Helper.ExecuteService("User", "AvailableAccountRoles", null);
                //ViewBag.roles = rolesAvailable;

                return View(model);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateAccountRoles(mUserAccountRoles model)
        {
            try
            {
                mUsers model1 = new mUsers();

                Helper.ExecuteService("User", "UpdateAccountRoles", model);
                TempData["roles"] = "Role Saved";

                return RedirectToAction("ViewUsers");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult Reports()
        {
            return View();
        }

        public ActionResult LoanStatement()
        {
            return View();
        }
    }
}
