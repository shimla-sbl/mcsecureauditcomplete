﻿using Email.API;
using EvoPdf;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.User;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SMS.API;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class EstablishBillController : Controller
    {
        //
        // GET: /AccountsAdmin/EstablishBill/

        #region Establish Bill
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult BillIndex(int pageId = 1, int pageSize = 10)
        {
            try
            {
                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;
                var BillList = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.FinancialYear == FinYearValue && m.CreatedBy == CurrentSession.UserName &&
m.IsCanceled == false).OrderByDescending(m => m.EstablishBillID).ToList();

                //                var BillList = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => m.FinancialYear == FinYearValue && m.CreatedBy == CurrentSession.UserName &&
                //m.IsCanceled == false).OrderByDescending(m => m.EstablishBillID).ToList();

                var BillTypeList = (List<mBudgetBillTypes>)Helper.ExecuteService("Budget", "GetAllBillType", null);
                ViewBag.BillTypeList = new SelectList(BillTypeList, "BillTypeID", "TypeName");
                var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget { FinancialYear = FinYearValue });
                ViewBag.BudgetList = new SelectList(State, "BudgetID", "BudgetName");

                // this is for Paging & Sorting

                List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
                if (pageId == 1)
                {
                    pagedRecord = BillList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.PageSize = pageSize;
                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);




                Pagination pagination = new Pagination();
                pagination.SortCoulmnName = "EstablishBillID";
                pagination.SortOrder = "asc";
                pagination.SortOrderingClass = "sorting";
                pagination.PageId = pageId;
                pagination.PageSize = pageSize;
                pagination.TableId = "tblEstablishBillList";


                EstablishBillViewModel ViewModel = new EstablishBillViewModel();

                ViewModel.EstablishBillLsit = pagedRecord;
                ViewModel.Pagination = pagination;


                return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_BillIndex.cshtml", ViewModel);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Bill for Members";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }



        public PartialViewResult PopulateEstablishBill(int Id, string Action)
        {
            EstablishBillViewModel Model = new EstablishBillViewModel();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = Id });
            if (Id > 0)
            {
                Model.mEstablishBill = EstablishBill;
                var BillTypeList = (List<mBudgetBillTypes>)Helper.ExecuteService("Budget", "GetAllBillType", null);
                ViewBag.BillTypeList = new SelectList(BillTypeList, "BillTypeID", "TypeName", EstablishBill.BillType);

                ViewBag.DesignationList = new SelectList(Enum.GetValues(typeof(DesignationType)).Cast<DesignationType>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text", EstablishBill.Designation);
                int BillType = 0;
                if (Model.mEstablishBill.BillType == 18 || Model.mEstablishBill.BillType == 19 || Model.mEstablishBill.BillType == 20 || Model.mEstablishBill.BillType == 22)
                    BillType = 18;
                else if (Model.mEstablishBill.BillType == 27)
                    BillType = 7;
                else
                    BillType = Model.mEstablishBill.BillType;



                var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetByBudgetType", new mBudget { BudgetType = BillType, FinancialYear = FinYearValue, BudgetHeadOrder = Model.mEstablishBill.Designation });
                ViewBag.BudgetList = new SelectList(State, "BudgetID", "BudgetName", EstablishBill.BudgetId);
            }
            else
            {
                var BillTypeList = (List<mBudgetBillTypes>)Helper.ExecuteService("Budget", "GetAllBillType", null);
                ViewBag.BillTypeList = new SelectList(BillTypeList, "BillTypeID", "TypeName");


                ViewBag.DesignationList = new SelectList(Enum.GetValues(typeof(DesignationType)).Cast<DesignationType>().Select(v => new SelectListItem
                {
                    Text = v.GetDescription(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");
                var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetDetails", new mBudget { FinancialYear = FinYearValue });
                ViewBag.BudgetList = new SelectList(State, "BudgetID", "BudgetName");
            }

            ViewBag.SanctionDate = Convert.ToDateTime(Model.mEstablishBill.SanctionDate).ToShortDateString();
            ViewBag.SanctioNumber = Model.mEstablishBill.SanctioNumber;
            Model.mEstablishBill.Mode = Action;


            if (Model.mEstablishBill.EstablishBillID > 0)
                Model.ReimbursementBillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { CreatedBy = CurrentSession.UserName })).Where(m => (m.BillType == Model.mEstablishBill.BillType && m.EstablishBillId == 0 && m.Designation == Model.mEstablishBill.Designation) || m.EstablishBillId == Model.mEstablishBill.EstablishBillID).ToList();
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_PopulateBill.cshtml", Model);
        }

        public PartialViewResult GetVoucherListByBillType(int BillStatus, int Designation)
        {
            EstablishBillViewModel Model = new EstablishBillViewModel();

            Model.ReimbursementBillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", new mReimbursementBill { CreatedBy = CurrentSession.UserName })).Where(m => m.BillType == BillStatus && m.EstablishBillId == 0 && m.Designation == Designation).ToList(); ;
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_VoucherList.cshtml", Model);
        }

        [HttpPost]
        public PartialViewResult SaveEstablishBill(FormCollection collection, int pageId = 1, int pageSize = 10)
        {

            int EstablishBillID = 0;
            int.TryParse(collection["EstablishBillID"], out EstablishBillID);

            int BudgetId = 0;
            int.TryParse(collection["BudgetHead"], out BudgetId);

            DateTime SanctionDate = new DateTime();
            DateTime.TryParse(collection["txtSanctionDate"], out SanctionDate);

            string Mode = collection["Mode"];
            string SanctioNum = collection["txtSanctionNumber"];

            if (SanctioNum == "")
                SanctioNum = "NA";
            if (SanctionDate.ToString("dd/MM/yyyy") == "01/01/0001")
                SanctionDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;

            string[] RowCount = collection["VoucherRowCount"].Split(',');

            mEstablishBill obj = new mEstablishBill();

            mEstablishBill stateToEdit = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillID });
            if (stateToEdit != null)
                obj = stateToEdit;

            if (!obj.IsChallanGenerate)
            {
                if (obj.BillType == 18 || obj.BillType == 19 || obj.BillType == 20 || obj.BillType == 22)
                    obj.BillType = 18;
                obj.BudgetId = BudgetId;
                obj.SanctionDate = SanctionDate;
                obj.SanctioNumber = SanctioNum;
                obj.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                obj.IsBillUpdate = true;
                obj.BillProcessingDate = DateTime.Now;
                obj.ModifiedBy = CurrentSession.UserName;
                Helper.ExecuteService("Budget", "UpdateEstablishBill", obj);
                ViewBag.Msg = "Sucessfully   saved";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";

                if (obj.EstablishBillID > 0)
                {



                    if (obj.BillType == 3 || obj.BillType == 13 || obj.BillType == 19 || obj.BillType == 20 ||
obj.BillType == 22 || obj.BillType == 18 || obj.BillType == 8 || obj.BillType == 9 || obj.BillType == 10
|| obj.BillType == 11 || obj.BillType == 12 || obj.BillType == 4 || obj.BillType == 14 || obj.BillType == 15 || obj.BillType == 21 || obj.BillType == 31
                         || obj.BillType == 27 || obj.BillType == 29 || obj.BillType == 30// || obj.BillType == 26
        )
                    {
                        DataSet dataSet = new DataSet();
                        var Parameter1 = new List<KeyValuePair<string, string>>();
                        Parameter1.Add(new KeyValuePair<string, string>("@EstablishBillId", obj.EstablishBillID.ToString()));
                        Parameter1.Add(new KeyValuePair<string, string>("@IsRejected", "0"));
                        Parameter1.Add(new KeyValuePair<string, string>("@BudgetId", obj.BudgetId.ToString()));
                        dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateBillIdForConsistency", Parameter1);
                    }
                    else
                    {
                        DataSet dataSet = new DataSet();
                        var Parameter = new List<KeyValuePair<string, string>>();
                        Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", obj.EstablishBillID.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@SanctionNumber", obj.SanctioNumber));
                        Parameter.Add(new KeyValuePair<string, string>("@SanctioDate", Convert.ToDateTime(obj.SanctionDate).ToShortDateString()));
                        Parameter.Add(new KeyValuePair<string, string>("@IsRejected", "0"));
                        Parameter.Add(new KeyValuePair<string, string>("@BudgetId", obj.BudgetId.ToString()));
                        dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateBillIdNew", Parameter);
                    }

                }

            }
            else
            {
                ViewBag.Msg = "Sorry..! Unable to save due to reason of Bill Challan.";
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }

            string FinYearValue = obj.FinancialYear;
            Pagination pagination = new Pagination();
            pagination.SortCoulmnName = "EstablishBillID";
            pagination.SortOrder = "asc";
            pagination.SortOrderingClass = "sorting";
            pagination.PageId = pageId;
            pagination.PageSize = pageSize;
            pagination.TableId = "tblEstablishBillList";

            var BillList = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.FinancialYear == FinYearValue && m.CreatedBy == CurrentSession.UserName &&
m.IsCanceled == false).OrderByDescending(m => m.EstablishBillID).ToList();
            //var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => m.FinancialYear == FinYearValue && m.CreatedBy == CurrentSession.UserName && m.IsCanceled == false).OrderByDescending(m => m.EstablishBillID).ToList();

            ViewBag.PageSize = pageSize;
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            EstablishBillViewModel ViewModel = new EstablishBillViewModel();
            ViewModel.EstablishBillLsit = pagedRecord;
            ViewModel.Pagination = pagination;
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_BillList.cshtml", ViewModel);
        }

        public PartialViewResult SerachEstablishBill(string BillType, string BudgetHead, string SanctionDateFrom, string SanctionDateTo, string SanctionNumber, int pageId = 1, int pageSize = 10)
        {
            //var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => m.CreatedBy == CurrentSession.UserName).ToList();

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.CreatedBy == CurrentSession.UserName).ToList();


            if (!string.IsNullOrEmpty(BillType))
            {
                int BillTypeI = 0;
                int.TryParse(BillType, out BillTypeI);
                BillList = BillList.Where(m => m.BillType == BillTypeI).ToList();
            }


            if (!string.IsNullOrEmpty(BudgetHead))
            {
                int BudgetHeadI = 0;
                int.TryParse(BudgetHead, out BudgetHeadI);
                BillList = BillList.Where(m => m.BudgetId == BudgetHeadI).ToList();
            }


            if (!string.IsNullOrEmpty(SanctionDateFrom) && !string.IsNullOrEmpty(SanctionDateTo))
            {

                DateTime SanctionDateFromD = new DateTime();
                DateTime.TryParse(SanctionDateFrom, out SanctionDateFromD);

                DateTime SanctionDateToD = new DateTime();
                DateTime.TryParse(SanctionDateTo, out SanctionDateToD);
                BillList = BillList.Where(m => m.SanctionDate >= SanctionDateFromD && m.SanctionDate <= SanctionDateToD).ToList();

            }

            if (!string.IsNullOrEmpty(SanctionNumber))
            {
                BillList = BillList.Where(m => m.SanctioNumber == SanctionNumber).ToList();
            }
            Session["EstablishBillList"] = BillList;

            ViewBag.PageSize = pageSize;
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            Pagination pagination = new Pagination();
            pagination.SortCoulmnName = "EstablishBillID";
            pagination.SortOrder = "asc";
            pagination.SortOrderingClass = "sorting";
            pagination.PageId = pageId;
            pagination.PageSize = pageSize;
            pagination.TableId = "tblEstablishBillList";

            EstablishBillViewModel ViewModel = new EstablishBillViewModel();
            ViewModel.EstablishBillLsit = pagedRecord;
            ViewModel.Pagination = pagination;
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_BillList.cshtml", ViewModel);
        }

        public PartialViewResult ShowVoucherBillList(int EstablishBillID)
        {

            List<mReimbursementBill> ReimbursementBillList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillByEstablishId", new mReimbursementBill { EstablishBillId = EstablishBillID });
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_ShowVoucherBillList.cshtml", ReimbursementBillList);
        }

        public JsonResult GetBudgetHeadList(int BillType, int Designation)
        {
            if (BillType == 18 || BillType == 19 || BillType == 20 || BillType == 22)
                BillType = 18;
            if (BillType == 27)
                BillType = 7;


            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            List<mBudget> State = (List<mBudget>)Helper.ExecuteService("Budget", "GetAllBudgetByBudgetType", new mBudget { BudgetType = BillType, FinancialYear = FinYearValue, BudgetHeadOrder = Designation });

            //if (BillType == 3)
            //    State = State.OrderBy(m => m.BudgetID).ToList();

            return Json(State, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 10, string sortDataField = "", string sortOrder = "", string OrderIconClass = "")
        {
            // this is for Paging & Sorting
            ViewBag.Order = sortOrder;
            ViewBag.sortDataField = sortDataField;
            ViewBag.OrderIconClass = OrderIconClass;
            ViewBag.PageSize = pageSize;

            //var model = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => m.CreatedBy == CurrentSession.UserName);

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var model = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.CreatedBy == CurrentSession.UserName);


            if (!string.IsNullOrEmpty(sortOrder) && sortOrder.Equals("asc"))
                sortOrder = "desc";
            else if (!string.IsNullOrEmpty(sortOrder) && sortOrder.Equals("desc"))
                sortOrder = "asc";

            if (sortDataField != null && sortOrder != "")
            {
                model = this.SortOrders(model, sortDataField, sortOrder);
            }


            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            if (pageId == 1)
            {
                pagedRecord = model.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = model.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(model.Count(), pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_BillList.cshtml", pagedRecord);
        }

        public PartialViewResult GetBillBySorting(string sortDataField, string sortOrder, int pageId = 1, int pageSize = 10)
        {

            ViewBag.sortDataField = sortDataField;
            ViewBag.PageSize = pageSize;
            if (sortOrder == "asc")
            {
                ViewBag.Order = "desc";
                ViewBag.OrderIconClass = "sorting_asc";
            }
            else
            {
                ViewBag.Order = "asc";
                ViewBag.OrderIconClass = "sorting_desc";
            }

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var model = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.CreatedBy == CurrentSession.UserName);


            if (sortDataField != null && sortOrder != "")
            {
                model = this.SortOrders(model, sortDataField, sortOrder);
            }
            ViewBag.PageSize = pageSize;
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();

            if (pageId == 1)
            {
                pagedRecord = model.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = model.Skip(r).Take(pageSize).ToList();
            }


            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(model.Count(), pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_BillList.cshtml", pagedRecord);
        }

        public PartialViewResult GetBillBySearching(string SearchData, string sortDataField, string sortOrder, string OrderIconClass, int pageId = 1, int pageSize = 10)
        {
            ViewBag.Order = sortOrder;
            ViewBag.sortDataField = sortDataField;
            ViewBag.OrderIconClass = OrderIconClass;
            ViewBag.PageSize = pageSize;


            if (!string.IsNullOrEmpty(sortOrder) && sortOrder.Equals("asc"))
                sortOrder = "desc";
            else if (!string.IsNullOrEmpty(sortOrder) && sortOrder.Equals("desc"))
                sortOrder = "asc";

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            IEnumerable<mEstablishBill> filteredmEstablishBill;
            if (!string.IsNullOrEmpty(SearchData))
            {

                filteredmEstablishBill = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.SanctioNumber.Contains(SearchData) ||
                                        m.BudgetName.Contains(SearchData) || Convert.ToDateTime(m.SanctionDate).ToString("dd/MM/yyyy").Contains(SearchData) ||
                                        m.TotalAmount.ToString().Contains(SearchData) || m.EncashmentDate.ToString().Contains(SearchData));

            }
            else
            {
                filteredmEstablishBill = (IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue });
            }
            if (sortDataField != null && sortOrder != "")
            {
                filteredmEstablishBill = this.SortOrders(filteredmEstablishBill, sortDataField, sortOrder);
            }

            filteredmEstablishBill = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.CreatedBy == CurrentSession.UserName);
            ViewBag.PageSize = pageSize;
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();

            if (pageId == 1)
            {
                pagedRecord = filteredmEstablishBill.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = filteredmEstablishBill.Skip(r).Take(pageSize).ToList();
            }


            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(filteredmEstablishBill.Count(), pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_BillList.cshtml", pagedRecord);
        }

        private IEnumerable<mEstablishBill> SortOrders(IEnumerable<mEstablishBill> collection, string sortField, string sortOrder)
        {
            if (sortOrder == "asc")
                collection = collection.OrderBy(c => c.GetType().GetProperty(sortField).GetValue(c, null));
            else
                collection = collection.OrderByDescending(c => c.GetType().GetProperty(sortField).GetValue(c, null));
            return collection;
        }


        public PartialViewResult DataTablePagination(Pagination PaginationModel)
        {
            //var filteredList = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => m.CreatedBy == CurrentSession.UserName);
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var filteredList = ((IEnumerable<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.CreatedBy == CurrentSession.UserName);


            if (PaginationModel.FunctionType == "Sorting")
            {
                if (PaginationModel.SortOrder == "asc")
                {
                    PaginationModel.SortOrder = "desc";
                    PaginationModel.SortOrderingClass = "sorting_asc";
                }
                else
                {
                    PaginationModel.SortOrder = "asc";
                    PaginationModel.SortOrderingClass = "sorting_desc";
                }
            }
            //else if (PaginationModel.FunctionType == "Searching" || PaginationModel.FunctionType == "Paging")
            //{

            //    if (!string.IsNullOrEmpty(PaginationModel.SortOrder) && PaginationModel.SortOrder.Equals("asc"))
            //        PaginationModel.SortOrder = "desc";
            //    else if (!string.IsNullOrEmpty(PaginationModel.SortOrder) && PaginationModel.SortOrder.Equals("desc"))
            //        PaginationModel.SortOrder = "asc";

            //}



            // this is for Searching Data
            if (!string.IsNullOrEmpty(PaginationModel.SearchWord))
            {
                int BillNimber = 0;
                int.TryParse(PaginationModel.SearchWord, out BillNimber);
                filteredList = filteredList.Where(m => m.SanctioNumber.Contains(PaginationModel.SearchWord) || m.BillNumber == BillNimber ||
                    //   m.BudgetName.Contains(PaginationModel.SearchWord) || Convert.ToDateTime( m.SanctionDate).ToString("dd/MM/yyyy").Contains(PaginationModel.SearchWord) || Convert.ToDateTime(m.EncashmentDate).ToString("dd/MM/yyyy").Contains(PaginationModel.SearchWord)
                                         m.TotalAmount.ToString().Contains(PaginationModel.SearchWord));

            }
            // this is for Sorting Data
            if (PaginationModel.SortCoulmnName != null && PaginationModel.SortOrder != "")
            {
                if (PaginationModel.SortOrder == "asc")
                    filteredList = filteredList.OrderBy(c => c.GetType().GetProperty(PaginationModel.SortCoulmnName).GetValue(c, null));
                else
                    filteredList = filteredList.OrderByDescending(c => c.GetType().GetProperty(PaginationModel.SortCoulmnName).GetValue(c, null));

            }


            // this is for Paging
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            if (filteredList != null)
            {
                if (PaginationModel.PageId == 1)
                {
                    pagedRecord = filteredList.Take(PaginationModel.PageSize).ToList();
                }
                else
                {
                    int r = (PaginationModel.PageId - 1) * PaginationModel.PageSize;
                    pagedRecord = filteredList.Skip(r).Take(PaginationModel.PageSize).ToList();
                }
            }

            PaginationModel.TotalRow = filteredList.Count();
            ViewBag.CurrentPage = PaginationModel.PageId;
            ViewData["PagedList"] = Helper.BindPager(PaginationModel.TotalRow, PaginationModel.PageId, PaginationModel.PageSize);

            EstablishBillViewModel ViewModel = new EstablishBillViewModel();
            ViewModel.EstablishBillLsit = pagedRecord;
            ViewModel.Pagination = PaginationModel;
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_BillList.cshtml", ViewModel);
        }



        #endregion

        #region Cancel Establish Bill
        public PartialViewResult SaveCancelEstablishBill(int EstablishId, int pageId = 1, int pageSize = 10)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishId });
            EstablishBill.IsCanceled = true;

            try
            {
                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@EstablishId", EstablishBill.EstablishBillID.ToString()));
                ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "CancelEstablishBill", methodParameter);
                EstablishBill.ModifiedBy = CurrentSession.UserName;
                Helper.ExecuteService("Budget", "CancelEstablishBill", EstablishBill);


                ViewBag.Msg = "Sucessfully   saved";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            catch (Exception)
            {

                ViewBag.Msg = "Oops...! Something went wrong";
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
                throw;
            }


            string FinYearValue = SiteSettingsSessn.SettingValue;
            Pagination pagination = new Pagination();
            pagination.SortCoulmnName = "EstablishBillID";
            pagination.SortOrder = "asc";
            pagination.SortOrderingClass = "sorting";
            pagination.PageId = pageId;
            pagination.PageSize = pageSize;
            pagination.TableId = "tblEstablishBillList";

            //var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => m.FinancialYear == FinYearValue && m.CreatedBy == CurrentSession.UserName && m.IsCanceled == false).ToList();

            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.CreatedBy == CurrentSession.UserName && m.IsCanceled == false).ToList();



            ViewBag.PageSize = pageSize;
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            EstablishBillViewModel ViewModel = new EstablishBillViewModel();
            ViewModel.EstablishBillLsit = pagedRecord;
            ViewModel.Pagination = pagination;
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_BillList.cshtml", ViewModel);
        }


        public PartialViewResult CenceledEstablishBillIndex(int pageId = 1, int pageSize = 10)
        {
            try
            {
                //var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => m.IsCanceled == true).ToList();
                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;

                var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.IsCanceled == true).ToList();

                ViewBag.PageSize = pageSize;
                List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
                if (pageId == 1)
                {
                    pagedRecord = BillList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_CanceledBillIndex.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetCancelEstablishBillByPaging(int pageId = 1, int pageSize = 10)
        {

            try
            {
                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;

                var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => m.IsCanceled == true).ToList();

                ViewBag.PageSize = pageSize;
                List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
                if (pageId == 1)
                {
                    pagedRecord = BillList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_CanceledBillList.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        #endregion
        #region Sanction Bill
        public PartialViewResult SanctionEstablishBill(int pageId = 1, int pageSize = 10)
        {
            try
            {

                //                var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) && m.IsEncashment == false &&
                //m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();

                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;


                var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) && m.IsEncashment == false &&
m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();

                return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_SanctionBillIndex.cshtml", BillList);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        /// <summary>
        /// Approve multiple Bill in single click
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="pageId"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>

        [HttpPost]
        public PartialViewResult SaveSanctionBill(FormCollection collection, int pageId = 1, int pageSize = 10)
        {

            var FileLocationModel = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "FileLocation" });
            string FileLocation = FileLocationModel.SettingValue;
            string[] RowCount = collection["RowCount"].Split(',');

            foreach (var item in RowCount)
            {
                int EstablishBillId = int.Parse(collection["EstablishBillID-" + item]);
                string Sanctioned = collection["Sanctioned-" + item];

                mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillId });

                if (EstablishBill.IsSOApproved)
                {
                    if (Sanctioned == "1")
                    {
                        EstablishBill.IsSanctioned = true;
                        EstablishBill.IsReject = false;
                    }
                    else if (Sanctioned == "0")
                    {
                        EstablishBill.IsSanctioned = false;
                        EstablishBill.IsReject = true;
                        EstablishBill.RejectedBy = "DDO";
                        EstablishBill.IsChallanGenerate = false;
                        EstablishBill.IsBillUpdate = false;

                        if (!string.IsNullOrEmpty(EstablishBill.PDFUrl))
                        {
                            string url = FileLocation + "/BillChallan/";
                            string directory = url + EstablishBill.PDFUrl;
                            System.IO.File.Delete(directory);
                            EstablishBill.PDFUrl = string.Empty;
                        }
                    }
                    if ((Sanctioned == "1" || Sanctioned == "0") && EstablishBill.IsSOApproved == true)
                    {
                        EstablishBill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                        EstablishBill.Status = SBL.DomainModel.Models.Enums.BillStatus.Sanctioned.GetHashCode();
                        Helper.ExecuteService("Budget", "UpdateEstablishBill", EstablishBill);

                        DataSet dataSet = new DataSet();
                        var Parameter = new List<KeyValuePair<string, string>>();
                        Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", EstablishBill.EstablishBillID.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@Status", EstablishBill.Status.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@IsSOApproved", EstablishBill.IsSOApproved.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@IsSanctioned", EstablishBill.IsSanctioned.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@IsRejected", EstablishBill.IsReject.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@IsChallanGenerate", (EstablishBill.IsSanctioned ? "1" : "0")));
                        Parameter.Add(new KeyValuePair<string, string>("@IsEncashment", "0"));
                        Parameter.Add(new KeyValuePair<string, string>("@EncashmentDate", ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()));
                        dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateStatusOfReimbursementBill", Parameter);

                        ViewBag.Msg = "Sucessfully   sanctioned";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";
                    }
                }
            }


            //            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) && m.IsEncashment == false &&
            //m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;


            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) && m.IsEncashment == false &&
m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_SanctionBillList.cshtml", BillList);
        }

        /// <summary>
        /// Approve only single bIll 
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="pageId"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult UpdateSanctionBill(int EstablishBillId, string Sanctioned, int pageId = 1, int pageSize = 10)
        {


            var FileLocationModel = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "FileLocation" });
            string FileLocation = FileLocationModel.SettingValue;
            mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillId });

            if (Sanctioned == "1")
            {
                EstablishBill.IsSanctioned = true;
                EstablishBill.IsReject = false;
            }
            else if (Sanctioned == "0")
            {
                EstablishBill.IsSanctioned = false;
                EstablishBill.IsReject = true;
                EstablishBill.RejectedBy = "DDO";
                EstablishBill.IsChallanGenerate = false;
                EstablishBill.IsBillUpdate = false;
                if (!string.IsNullOrEmpty(EstablishBill.PDFUrl))
                {
                    string url = FileLocation + "/BillChallan/";
                    string directory = url + EstablishBill.PDFUrl;
                    System.IO.File.Delete(directory);
                    EstablishBill.PDFUrl = string.Empty;
                }
            }

            if (Sanctioned == "1" || Sanctioned == "0")
            {
                EstablishBill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                EstablishBill.Status = SBL.DomainModel.Models.Enums.BillStatus.Sanctioned.GetHashCode();
                Helper.ExecuteService("Budget", "UpdateEstablishBill", EstablishBill);

                DataSet dataSet = new DataSet();
                var Parameter = new List<KeyValuePair<string, string>>();
                Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", EstablishBill.EstablishBillID.ToString()));
                Parameter.Add(new KeyValuePair<string, string>("@Status", EstablishBill.Status.ToString()));
                Parameter.Add(new KeyValuePair<string, string>("@IsSOApproved", EstablishBill.IsSOApproved.ToString()));
                Parameter.Add(new KeyValuePair<string, string>("@IsSanctioned", (EstablishBill.IsSanctioned ? "1" : "0")));
                Parameter.Add(new KeyValuePair<string, string>("@IsRejected", (EstablishBill.IsReject ? "1" : "0")));
                Parameter.Add(new KeyValuePair<string, string>("@IsChallanGenerate", (EstablishBill.IsSanctioned ? "1" : "0")));
                Parameter.Add(new KeyValuePair<string, string>("@IsEncashment", "0"));
                Parameter.Add(new KeyValuePair<string, string>("@EncashmentDate", ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()));

                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateStatusOfReimbursementBill", Parameter);


                ViewBag.Msg = "Sucessfully   sanctioned";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            else
            {

                ViewBag.Msg = "Please  select Sanction or Reject. Unable to save bill";
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) && m.IsEncashment == false &&
m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();




            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_SanctionBillList.cshtml", BillList);
        }


        public PartialViewResult GetSanctionBillByPaging(int pageId = 1, int pageSize = 10)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) &&
m.IsCanceled == false).ToList();

            ViewBag.PageSize = pageSize;
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_SanctionBillList.cshtml", pagedRecord);
        }
        #endregion


        #region Approve Bill
        public PartialViewResult ApproveEstablishBill(int pageId = 1, int pageSize = 10)
        {
            try
            {
                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;

                var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) && m.IsEncashment == false &&
m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();

                //ViewBag.PageSize = pageSize;
                //List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
                //if (pageId == 1)
                //{
                //    pagedRecord = BillList.Take(pageSize).ToList();
                //}
                //else
                //{
                //    int r = (pageId - 1) * pageSize;
                //    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
                //}

                //ViewBag.CurrentPage = pageId;
                //ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);


                //if (TempData["Msg"] != null)
                //    ViewBag.Msg = TempData["Msg"].ToString();

                return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_ApproveBillIndex.cshtml", BillList);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        /// <summary>
        /// Approve multiple Bill in single click
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="pageId"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>

        [HttpPost]
        public PartialViewResult SaveApproveBill(FormCollection collection, int pageId = 1, int pageSize = 10)
        {
            var FileLocationModel = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "FileLocation" });
            string FileLocation = FileLocationModel.SettingValue;

            string[] RowCount = collection["RowCount"].Split(',');

            foreach (var item in RowCount)
            {
                int EstablishBillId = int.Parse(collection["EstablishBillID-" + item]);
                string Approved = collection["Approved-" + item];

                mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillId });
                if (Approved != null)
                {
                    if (Approved == "1" && EstablishBill.IsChallanGenerate)
                    {
                        EstablishBill.IsSOApproved = true;
                        EstablishBill.IsReject = false;

                    }
                    else if (Approved == "0")
                    {
                        EstablishBill.IsSOApproved = false;
                        EstablishBill.IsReject = true;
                        EstablishBill.IsChallanGenerate = false;
                        EstablishBill.IsBillUpdate = false;
                        if (!string.IsNullOrEmpty(EstablishBill.PDFUrl))
                        {
                            string url = FileLocation + "/BillChallan/";
                            string directory = url + EstablishBill.PDFUrl;
                            System.IO.File.Delete(directory);
                            EstablishBill.PDFUrl = string.Empty;
                        }
                    }
                    if (Approved == "1" || Approved == "0")
                    {
                        EstablishBill.RejectedBy = "SO";

                        //EstablishBill.Status = SBL.DomainModel.Models.Enums.BillStatus.Sanctioned.GetHashCode();
                        Helper.ExecuteService("Budget", "UpdateEstablishBill", EstablishBill);

                        DataSet dataSet = new DataSet();
                        var Parameter = new List<KeyValuePair<string, string>>();
                        Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", EstablishBill.EstablishBillID.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@Status", EstablishBill.Status.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@IsSOApproved", EstablishBill.IsSOApproved.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@IsSanctioned", EstablishBill.IsSanctioned.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@IsRejected", EstablishBill.IsReject.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@IsChallanGenerate", (EstablishBill.IsSOApproved ? "1" : "0")));
                        Parameter.Add(new KeyValuePair<string, string>("@IsEncashment", "0"));
                        Parameter.Add(new KeyValuePair<string, string>("@EncashmentDate", ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()));
                        dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateStatusOfReimbursementBill", Parameter);

                        ViewBag.Msg = "Sucessfully   approved";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";
                    }
                }
            }


            //            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) && m.IsEncashment == false &&
            //m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) && m.IsEncashment == false &&
m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();

            //ViewBag.PageSize = pageSize;
            //List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            //if (pageId == 1)
            //{
            //    pagedRecord = BillList.Take(pageSize).ToList();
            //}
            //else
            //{
            //    int r = (pageId - 1) * pageSize;
            //    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            //}

            //ViewBag.CurrentPage = pageId;
            //ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);



            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_ApproveBillList.cshtml", BillList);
        }

        /// <summary>
        /// Approve only single bIll 
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="pageId"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult UpdateApproveBill(int EstablishBillId, string Approved, int pageId = 1, int pageSize = 10)
        {

            var FileLocationModel = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "FileLocation" });
            string FileLocation = FileLocationModel.SettingValue;

            mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillId });

            if (Approved == "1")
            {
                EstablishBill.IsSOApproved = true;
                EstablishBill.IsReject = false;
            }
            else if (Approved == "0")
            {
                EstablishBill.IsSOApproved = false;
                EstablishBill.IsReject = true;
                EstablishBill.RejectedBy = "SO";
                EstablishBill.IsChallanGenerate = false;
                EstablishBill.IsBillUpdate = false;

                if (!string.IsNullOrEmpty(EstablishBill.PDFUrl))
                {
                    string url = FileLocation + "/BillChallan/";
                    //string directory = Server.MapPath(url + EstablishBill.PDFUrl);
                    string directory = url + EstablishBill.PDFUrl;
                    System.IO.File.Delete(directory);
                    EstablishBill.PDFUrl = string.Empty;
                }
            }

            if (Approved == "1" || Approved == "0")
            {

                Helper.ExecuteService("Budget", "UpdateEstablishBill", EstablishBill);

                DataSet dataSet = new DataSet();
                var Parameter = new List<KeyValuePair<string, string>>();
                Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", EstablishBill.EstablishBillID.ToString()));
                Parameter.Add(new KeyValuePair<string, string>("@Status", EstablishBill.Status.ToString()));
                Parameter.Add(new KeyValuePair<string, string>("@IsSOApproved", EstablishBill.IsSOApproved.ToString()));
                Parameter.Add(new KeyValuePair<string, string>("@IsSanctioned", (EstablishBill.IsSanctioned ? "1" : "0")));
                Parameter.Add(new KeyValuePair<string, string>("@IsRejected", (EstablishBill.IsReject ? "1" : "0")));
                Parameter.Add(new KeyValuePair<string, string>("@IsChallanGenerate", (EstablishBill.IsSOApproved ? "1" : "0")));
                Parameter.Add(new KeyValuePair<string, string>("@IsEncashment", "0"));
                Parameter.Add(new KeyValuePair<string, string>("@EncashmentDate", ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()));

                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateStatusOfReimbursementBill", Parameter);


                ViewBag.Msg = "Sucessfully   approved";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            else
            {

                ViewBag.Msg = "Please  select Sanction or Reject. Unable to save bill";
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }

            //            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) && m.IsEncashment == false &&
            //m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) && m.IsEncashment == false &&
m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();
            //ViewBag.PageSize = pageSize;
            //List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            //if (pageId == 1)
            //{
            //    pagedRecord = BillList.Take(pageSize).ToList();
            //}
            //else
            //{
            //    int r = (pageId - 1) * pageSize;
            //    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            //}

            //ViewBag.CurrentPage = pageId;
            //ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);



            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_ApproveBillList.cshtml", BillList);
        }


        public PartialViewResult GetApproveBillByPaging(int pageId = 1, int pageSize = 10)
        {
            //            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) &&
            //m.IsCanceled == false).ToList();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.IsChallanGenerate == true || m.IsReject == true) &&
m.IsCanceled == false).ToList();
            ViewBag.PageSize = pageSize;
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_ApproveBillList.cshtml", pagedRecord);
        }
        #endregion

        #region Bill Challan


        //[HttpPost]
        //public JsonResult GenerateBillChallan(int BillId)
        //{
        //    string Result = string.Empty;
        //    DateTime GenerateDate = DateTime.Now;
        //    StringBuilder sb = new StringBuilder();
        //    Result = GetBillChallan(BillId, GenerateDate);
        //    if (!Result.Contains("No bill found"))
        //    {

        //        string Filename = CreteBillChallanPdf(Result);
        //        sb.Append(Result);
        //        sb.Append(string.Format("<input type='hidden' name='FileName' id='FileName' value='{0}' />", Filename));
        //    }

        //    return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        //}

        public static string GetHRT5Form(mEstablishBill BillObj, int BillType)
        {
            StringBuilder Parameter = new StringBuilder();
            StringBuilder Result = new StringBuilder();
            mBudget BudgetObj = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = BillObj.BudgetId });
            AllocateBudget AllocateSactionBud = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = BillObj.BudgetId });
            //if (BillObj != null && BillObj.EstablishBillID > 0 && BudgetObj != null && AllocateSactionBud != null && AllocateSactionBud.Sanctionedbudget > 0)
            //{
            if (BillObj != null && BillObj.EstablishBillID > 0 && BudgetObj != null)
            {
                var VoucherList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllBillByEstablishBillId", new mEstablishBill { EstablishBillID = BillObj.EstablishBillID });




                // Crete Html Template for Bill Report
                Result.Append("<body style='width:800px;margin:auto;font-weight:bold;font-family:'Times New Roman';'><section><header style='text-align:center;'>");

                Result.Append("<table style='width: 100%;'>  <tr> <td style='vertical-align: middle; text-align: center'>");
                Result.Append("<h4 style='text-align:center;font-weight:bold'>ESTABLISHMENT OF H. P. VIDHAN SABHA</h4><div style='padding:1px;border-bottom:1px solid black;'></div>");
                Result.Append("</header><div>");

                // this is for Bill Basic Detail
                Result.Append("<table style='width:100%'>");
                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;'>Bill Number:</td>   ");
                Result.Append(string.Format(" <td style='text-align: left;'><b>{0}</b> of <b>{1}</b></td>", BillObj.BillNumber, BudgetObj.FinancialYear));
                Result.Append(string.Format(" <td style='text-align: left;'>H. P. T. R. 5</td>"));
                Result.Append(string.Format(" <td style='text-align: left;'>Voucher No. :</td>"));
                Result.Append(string.Format(" <td> </td>"));
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;'>Bill Date:</td>   ");
                Result.Append(string.Format(" <td style='text-align: left;'><b>{0}</b></td>", Convert.ToDateTime(BillObj.BillProcessingDate).ToString("dd/MM/yyyy")));
                Result.Append(string.Format(" <td style='text-align: left;'>Treasury Abstract</td>"));
                Result.Append(string.Format(" <td style='text-align: left;'>Voucher Date :</td>"));
                Result.Append(string.Format(" <td style='text-align: left;'> /&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp; &nbsp;&nbsp; </td>"));
                Result.Append("</tr>");
                Result.Append("</table>  ");
                Result.Append("<div style='padding:1px;border-bottom:1px solid black;'></div>");

                // this is for budget Detail
                Result.Append("<table style='width:100%' cellpadding='6'>");
                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;width:20%'>1. Treasury Code :</td>   ");
                Result.Append(string.Format(" <td style='text-align: left;width:20%'>{0}</td>", BudgetObj.TreasuryCode));

                Result.Append(string.Format(" <td style='text-align: left;width:20%'>2. Demand No. : </td>"));
                Result.Append(string.Format(" <td style='text-align: left;width:20%'>{0}</td>", BudgetObj.DemandNo));
                Result.Append(string.Format(" <td style='width:20%'> </td></td><td>"));
                Result.Append("</tr>");


                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;'>3. D.D.O. Code :</td>   ");
                if (BillObj.Designation == 8)
                {
                    
                    Result.Append(string.Format(" <td style='text-align: left;'>{0}</td>", "126"));
                }
                else
                {
                    Result.Append(string.Format(" <td style='text-align: left;'>{0}</td>", BudgetObj.DDOCode));
                }
                

                Result.Append(string.Format(" <td style='text-align: left;'>4. Gztd/Non-Gztd : </td>"));
                Result.Append(string.Format(" <td style='text-align: left;'><b>{0}</b></td>", BudgetObj.Gazetted));
                Result.Append(string.Format(" <td> </td></td><td>"));
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;'>5. Major Head:</td>   ");
                Result.Append(string.Format(" <td style='text-align: left;'>{0}</td>", BudgetObj.MajorHead));

                Result.Append(string.Format(" <td style='text-align: left;'>6. Sub Major : </td>"));
                Result.Append(string.Format(" <td style='text-align: left;'>{0}</td>", BudgetObj.SubMajorHead));
                Result.Append(string.Format(" <td> </td></td><td>"));
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;'>7. Minor Head:</td>   ");
                Result.Append(string.Format(" <td style='text-align: left;'><b>{0}</b></td>", BudgetObj.MinorHead));

                Result.Append(string.Format(" <td style='text-align: left;'>8. Sub Head : </td>"));
                Result.Append(string.Format(" <td style='text-align: left;'>{0}</td>", BudgetObj.SubHead));
                Result.Append(string.Format(" <td> </td></td><td>"));
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;' >9. Budget Code:</td>   ");
                Result.Append(string.Format(" <td style='text-align: left;'>{0}</td>", BudgetObj.BudgetCode));

                Result.Append(string.Format(" <td style='text-align: left;'>10. Object Code: </td>"));
                Result.Append(string.Format(" <td style='text-align: left;'>{0}</td>", BudgetObj.ObjectCode));
                Result.Append(string.Format(" <td> </td></td><td>"));
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;' >11. Plan/Non-Plan:</td>   ");
                Result.Append(string.Format(" <td style='text-align: left;'>{0}</td>", BudgetObj.Plan));

                Result.Append(string.Format(" <td style='text-align: left;'>12.Voted/Charged: </td>"));
                Result.Append(string.Format(" <td style='text-align: left;'>{0}</td>", BudgetObj.VotedCharged));
                Result.Append(string.Format(" <td> </td></td><td>"));
                Result.Append("</tr>");

                Result.Append("</table> ");


                Result.Append("<div style='padding:1px;border-bottom:1px solid black;'></div>");

                // this is for showing Bill total

                Result.Append("<table style='width:100%' cellpadding='6'>");
                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;' >13. Particular :</td>   ");
                Result.Append(string.Format(" <td style='text-align: left;'><b>{0}</b></td>", BudgetObj.ObjectCodeText));
                Result.Append(string.Format("<td> </td>"));
                Result.Append(string.Format(" <td></td>"));
                Result.Append(string.Format(" <td></td>"));
                Result.Append(string.Format(" <td> </td>"));
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;' >14. To whom paid :</td>   ");
                Result.Append(string.Format(" <td style='text-align: left;'>As per Abstract detils below</td>"));
                Result.Append(string.Format("<td> </td>"));
                Result.Append(string.Format(" <td></td>"));
                Result.Append(string.Format(" <td></td>"));
                Result.Append(string.Format(" <td> </td>"));
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;' >15. Cheque No :</td>   ");
                Result.Append(string.Format(" <td> </td>"));
                Result.Append(string.Format("<td> </td>"));
                Result.Append(string.Format(" <td style='text-align: left;'>Cheque Date :</td>"));
                Result.Append(string.Format(" <td style='text-align: left;'>/ &nbsp;&nbsp; &nbsp;&nbsp; / &nbsp;&nbsp;  </td>"));
                Result.Append(string.Format(" <td> </td>"));
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;' >16. Original No :</td>   ");
                Result.Append(string.Format(" <td> </td>"));
                Result.Append(string.Format("<td> </td>"));
                Result.Append(string.Format(" <td style='text-align: left;'>Original Date :</td>"));
                Result.Append(string.Format(" <td style='text-align: left;'>/ &nbsp;&nbsp; &nbsp;&nbsp; / &nbsp;&nbsp;  </td>"));
                Result.Append(string.Format(" <td> </td>"));
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left; ' colspan='6'>17. Amount to be classified by T.O. (Rs.) :</td>   ");
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: rights;padding-left:15px;' >Total :</td>   ");


                //DataSet dataSet = new DataSet();
                //var EstablishBillIds = new List<KeyValuePair<string, string>>();
                //EstablishBillIds.Add(new KeyValuePair<string, string>("@EstablishBillId", BillObj.EstablishBillID.ToString()));

                //dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillIds);

                //decimal DecimalBillSanctionAmount = 0;
                //if (dataSet.Tables[0].Rows[0][1] != null)
                //    DecimalBillSanctionAmount = Convert.ToDecimal(dataSet.Tables[0].Rows[0][1]);

                //string BillGrossAmount = string.Empty;
                //if (dataSet.Tables[0].Rows[0][0] != null)
                //    BillGrossAmount = dataSet.Tables[0].Rows[0][0].ToString();

                //string BillSanctionAmount = string.Empty;
                //if (dataSet.Tables[0].Rows[0][1] != null)
                //    BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();

                //string BillDeduction = string.Empty;
                //if (dataSet.Tables[0].Rows[0][2] != null)
                //    BillDeduction = dataSet.Tables[0].Rows[0][2].ToString();

                //string BillGrossAmount = dataSet.Tables[0].Rows[0][0].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][0]);
                //string BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][1]);
                //string BillDeduction = dataSet.Tables[0].Rows[0][2].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][2]);


                Result.Append(string.Format(" <td tyle='text-align: left;'><b> {0}</b></td>", BillObj.TotalGrossAmount));
                Result.Append(string.Format("<td tyle='text-align: rights;'>B.T. Deduction : </td>"));
                Result.Append(string.Format(" <td tyle='text-align: left;'><b>{0}</b></td>", BillObj.Deduction));
                Result.Append(string.Format(" <td tyle='text-align: rights;'>Net Amount :</td>"));
                Result.Append(string.Format(" <td tyle='text-align: left;'> <b>{0}</b></td>", BillObj.TotalAmount));
                Result.Append("</tr>");

                Result.Append("</table> ");

                Result.Append("<div style='padding:1px;border-bottom:1px solid black;'></div>");

                // this is for transfer recovery

                Result.Append("<table style='width:100%'>");
                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;font-weight:bold '>BOOK TRANSFER RECOVERIES</td>   ");
                Result.Append(string.Format(" <td></td>"));
                Result.Append(string.Format("<td style='text-align: left;'><div> (* CORRESPONDING  RECIEPT CODES *) <br/>&nbsp; Major &nbsp; | &nbsp; S.Major &nbsp; | &nbsp; S.Hd. &nbsp; |&nbsp; DDO Code</td>"));
                Result.Append(string.Format(" <td></td>"));
                Result.Append("</tr>");
                //Result.Append("<div style='padding:1px;'></div>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;'  colspan='4'>1. Other B.T. I Rs. :</td>   ");
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;'  colspan='4'>2. Other B.T. II Rs. :</td>   ");
                Result.Append("</tr>");

                Result.Append("</table> ");

                Result.Append("<div style='padding:1px;border-bottom:1px solid black;'></div>");

                // this is for showing Detail Sub Heads

                Result.Append("<table style='width:100%'>");
                Result.Append(" <tr>");
                Result.Append("<td style='text-align: left;font-weight:bold; text-align:center;' colspan='6'>DETAILED (SUB OBJECT) HEADS</td>   ");
                Result.Append("</tr>");

                Result.Append(" <tr style='border-bottom:1px solid black;'>");
                Result.Append("<td style='text-align: left;border-bottom:1px solid black;' > D E S C R I P T I O N</td>   ");
                Result.Append(string.Format(" <td style='border-bottom:1px solid black;'></td>"));
                Result.Append(string.Format("<td style='text-align: left;border-bottom:1px solid black;'>Code </td>"));
                Result.Append(string.Format(" <td style='border-bottom:1px solid black;'></td>"));
                Result.Append(string.Format(" <td style='text-align: left;border-bottom:1px solid black;'>Amount</td>"));
                Result.Append(string.Format(" <td> </td>"));
                Result.Append("</tr>");

                //Result.Append("<div style='padding:1px;border-bottom:1px solid black;'></div>");
                Result.Append(" <tr>");
                Result.Append(string.Format("<td style='text-align: left; '><b>{0}</b> </td>", BudgetObj.ObjectCodeText));
                Result.Append(string.Format(" <td></td>"));
                Result.Append(string.Format("<td style='text-align: left;'><b>{0}</b> </td>", BudgetObj.ObjectCode));
                Result.Append(string.Format(" <td></td>"));
                Result.Append(string.Format(" <td style='text-align: left;'><b>{0}</b></td>", BillObj.TotalAmount));
                Result.Append(string.Format(" <td> </td>"));
                Result.Append("</tr>");

                Result.Append("</table> ");

                Result.Append("<div style='padding:1px;border-bottom:1px solid black;'></div>");

                // this is for showing Appropriation

                Result.Append("<table style='width:100%'>");
                Result.Append(" <tr>");
                Result.Append("<td style='font-weight:bold; text-align:center;' colspan='3'>APPROPRIATION</td>   ");
                Result.Append("</tr>");

                decimal TotalAdditonlFund = 0;
                decimal TotalBillExpense = 0;
                decimal TotalFund = 0;
                string TotalFundS = string.Empty;
                string TotalBillExpenseS = string.Empty;
                decimal BalanceAmt = 0;
                string BalanceAmtS = string.Empty;
                if (BillType != 31)
                {
                    TotalAdditonlFund = (decimal)Helper.ExecuteService("Budget", "GetTotalAdditionById", new AdditionFunds { BudgetId = BillObj.BudgetId });

                    TotalBillExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpense", new mEstablishBill { BudgetId = BillObj.BudgetId, EstablishBillID = BillObj.EstablishBillID });

                    // this is for Total Fund for Budget
                    TotalFund = AllocateSactionBud.Sanctionedbudget + TotalAdditonlFund;

                    TotalFundS = TotalFund.ToString();// string.Format("{0:n0}", TotalFund);

                    // this is total expense including this bill
                    TotalBillExpenseS = TotalBillExpense.ToString();// string.Format("{0:n0}", TotalBillExpense);

                    // this is Balance Amount for Budget
                    BalanceAmt = TotalFund - TotalBillExpense;
                    BalanceAmtS = BalanceAmt.ToString();// string.Format("{0:n0}", BalanceAmt);
                }

                Result.Append(" <tr >");

                Result.Append(string.Format(" <td style='text-align: left;'>Apropriation for <b>{0}</b> :</td>", BudgetObj.FinancialYear));
                Result.Append(string.Format("<td style='text-align: left;'>Rs. <b>{0}</b> </td>", TotalFundS));
                Result.Append(string.Format(" <td></td>"));
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append(string.Format(" <td style='text-align: left;'>Deduct Expenditure (Including this Bill) : </td>"));
                Result.Append(string.Format("<td style='text-align: left;'>Rs. <b>{0}</b> </td>", TotalBillExpenseS));
                Result.Append(string.Format(" <td></td>"));
                Result.Append("</tr>");

                Result.Append(" <tr>");
                Result.Append(string.Format(" <td style='text-align: left;'>Balance : </td>"));
                Result.Append(string.Format("<td style='text-align: left;'>Rs. <b>{0}</b> </td>", BalanceAmtS));
                Result.Append(string.Format(" <td></td>"));
                Result.Append("</tr>");

                Result.Append("</table> ");

                Result.Append("<div style='padding:1px;border-bottom:1px solid black;'></div>");




                // this is for Abstration of Bill

                Result.Append("<table style='width:100%'>");
                Result.Append(" <tr>");

                Result.Append(string.Format("<td style='font-weight:bold; text-align:center;' colspan='4' >ABSTRACT OF BILLS FOR BILL NO : {0} OF {1}</td>", BillObj.BillNumber, BudgetObj.FinancialYear));
                Result.Append("</tr>");

                Result.Append(" <tr>");

                Result.Append(string.Format("<td colspan='4'></td>"));
                Result.Append("</tr>");


                Result.Append(" <tr >");

                Result.Append(string.Format(" <td style='text-align: left;'>{0} :</td>", BudgetObj.ObjectCodeText));
                Result.Append(string.Format("<td colspan='2'></td>"));
                Result.Append(string.Format(" <td style=' text-align:right;'>{0}-{1}-{2}-{3}-{4}-{5}</td>", BudgetObj.MajorHead, BudgetObj.SubMajorHead, BudgetObj.MinorHead, BudgetObj.SubHead, BudgetObj.Plan, BudgetObj.VotedCharged));
                Result.Append("</tr>");


                Result.Append(" <tr>");
                Result.Append(string.Format(" <td colspan='4' </td>"));
                Result.Append("</tr>");

                Result.Append("</table> ");
                Result.Append("<br>");
                //   Result.Append("<div style='padding:1px;border-bottom:1px solid black;'></div>");


                // Create Record for all sub voucher Detail
#pragma warning disable CS0219 // The variable 'TotalAmount' is assigned but its value is never used
                Decimal TotalAmount = 0;
#pragma warning restore CS0219 // The variable 'TotalAmount' is assigned but its value is never used

                int Count = 1;
                Result.Append("<table style='width:100%; border-collapse:collapse' >");
                Result.Append("<tr>");
                if (BillObj.BillType == 3 || BillObj.BillType == 13 || BillObj.BillType == 19 || BillObj.BillType == 20 ||
   BillObj.BillType == 18 || BillObj.BillType == 8 || BillObj.BillType == 9 || BillObj.BillType == 10
    || BillObj.BillType == 11 || BillObj.BillType == 12 || BillObj.BillType == 4 || BillObj.BillType == 14 || BillObj.BillType == 15 || BillObj.BillType == 21 ||
                     BillObj.BillType == 22
                    )
                {
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Vr. No.</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Name of Claiment</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Bill No.</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Bill Date</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Total Amont</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Sanction Number</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Sanction Date</th>");
                }
                else if (BillObj.BillType == 31)
                {
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Vr. No.</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Name of Claiment</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Total Amont</th>");
                }
                else if (BillObj.BillType == 26 || BillObj.BillType == 23 || BillObj.BillType == 24 || BillObj.BillType == 25)
                {
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Vr. No.</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Name of Claiment</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Sanction Number</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Sanction Date</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Loan Amont</th>");

                }
                else if (BillObj.BillType == 1)
                {
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Vr. No.</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Name of Claiment</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;width:200px;'>Bill Period</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Total Amount</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Claiment Bill Number</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Claiment Bill Date</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>PPO Num</th>");
                }
                else
                {
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Vr. No.</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Name of Claiment</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;width:200px;'>Bill Period</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Total Amount</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Claiment Bill Number</th>");
                    Result.Append("<th style='border:1px solid black;border-collapse:collapse;text-align:left;'>Claiment Bill Date</th>");
                }
                Result.Append("</tr>");
                if (VoucherList != null && VoucherList.Count() > 0)
                {
                    decimal TotalSubVoucherAmt = 0;

                    foreach (var item in VoucherList)
                    {


                        Result.Append("<tr>");
                        if (BillObj.BillType == 3 || BillObj.BillType == 13 || BillObj.BillType == 19 || BillObj.BillType == 20 ||
    BillObj.BillType == 18 || BillObj.BillType == 8 || BillObj.BillType == 9 || BillObj.BillType == 10
   || BillObj.BillType == 11 || BillObj.BillType == 12 || BillObj.BillType == 4 || BillObj.BillType == 14 || BillObj.BillType == 15 || BillObj.BillType == 21
                  || BillObj.BillType == 22)
                        {
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));

                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName.ToUpper()));
                            //  int BillNumberOfVoucher = 0;
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", (!string.IsNullOrEmpty(item.BillNumberOfVoucher) ? item.BillNumberOfVoucher.ToUpper() : string.Empty)));

                            //Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", (int.TryParse(item.BillNumberOfVoucher, out BillNumberOfVoucher) ? item.BillNumberOfVoucher : item.BillNumberOfVoucher.ToUpper())));

                            if (item.BillDateOfVoucher != null)
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", (item.BillDateOfVoucher.Value.ToString("dd/MM/yyyy") == "01/01/0001" || item.BillDateOfVoucher.Value.ToString("dd/MM/yyyy") == "01/01/1753" ? "--Nil--" : item.BillDateOfVoucher.Value.ToString("dd/MM/yyyy"))));
                            else
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", string.Empty));
                            string SubVocuherAmt = item.SactionAmount.ToString();// string.Format("{0:n0}", item.SactionAmount);
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", SubVocuherAmt));



                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", item.SactionNumber.ToUpper()));//item.ReimbursementBillID
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.SactionDate.Value.ToString("dd/MM/yyyy")));// Convert.ToDateTime(item.DateOfPresentation).ToString("dd/MM/yyyy")
                        }
                        else if (BillObj.BillType == 31)
                        {

                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));

                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName.ToUpper()));
                            string SubVocuherAmt = item.SactionAmount.ToString();
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", SubVocuherAmt));
                        }
                        else if (BillObj.BillType == 26 || BillObj.BillType == 23 || BillObj.BillType == 24 || BillObj.BillType == 25)
                        {

                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));

                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName.ToUpper()));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", (!string.IsNullOrEmpty(item.SactionNumber) ? item.SactionNumber.ToUpper() : " ")));
                            if (item.SactionDate != null)
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", (item.SactionDate.Value.ToString("dd/MM/yyyy") == "01/01/0001" || item.SactionDate.Value.ToString("dd/MM/yyyy") == "01/01/1753" ? "" : item.SactionDate.Value.ToString("dd/MM/yyyy"))));
                            else
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", string.Empty));
                            string SubVocuherAmt = item.SactionAmount.ToString();// string.Format("{0:n0}", item.SactionAmount);
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", SubVocuherAmt));

                        }
                        else if (BillObj.BillType == 1)
                        {
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));

                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName.ToUpper()));
                            if (item.BillFromDate != null && item.BillToDate != null)
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0} To {1}</td>",
                                 (item.BillFromDate.Value.ToString("dd/MM/yyyy") == "01/01/0001" || item.BillFromDate.Value.ToString("dd/MM/yyyy") == "01/01/1753" ? "-" : item.BillFromDate.Value.ToString("dd/MM/yyyy")),
                                 (item.BillToDate.Value.ToString("dd/MM/yyyy") == "01/01/0001" || item.BillToDate.Value.ToString("dd/MM/yyyy") == "01/01/1753" ? "-" : item.BillToDate.Value.ToString("dd/MM/yyyy"))
                                ));
                            else
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0} To {1}</td>", string.Empty, string.Empty));

                            string SubVocuherAmt = item.SactionAmount.ToString();// string.Format("{0:n0}", item.SactionAmount);
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", SubVocuherAmt));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.BillNumberOfVoucher));
                            if (item.BillDateOfVoucher != null)
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", (item.BillDateOfVoucher.Value.ToString("dd/MM/yyyy") == "01/01/0001" || item.BillDateOfVoucher.Value.ToString("dd/MM/yyyy") == "01/01/1753" ? "" : item.BillDateOfVoucher.Value.ToString("dd/MM/yyyy"))));
                            else
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", string.Empty));

                            if (item.PPONum != null)
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.PPONum));
                            else
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", string.Empty));
                        }
                        else
                        {
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", Count));

                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.ClaimantName.ToUpper()));
                            if (item.BillFromDate != null && item.BillToDate != null)
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0} To {1}</td>",
                                 (item.BillFromDate.Value.ToString("dd/MM/yyyy") == "01/01/0001" || item.BillFromDate.Value.ToString("dd/MM/yyyy") == "01/01/1753" ? "-" : item.BillFromDate.Value.ToString("dd/MM/yyyy")),
                                 (item.BillToDate.Value.ToString("dd/MM/yyyy") == "01/01/0001" || item.BillToDate.Value.ToString("dd/MM/yyyy") == "01/01/1753" ? "-" : item.BillToDate.Value.ToString("dd/MM/yyyy"))
                                ));
                            else
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0} To {1}</td>", string.Empty, string.Empty));

                            string SubVocuherAmt = item.SactionAmount.ToString();// string.Format("{0:n0}", item.SactionAmount);
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", SubVocuherAmt));
                            Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", item.BillNumberOfVoucher));
                            if (item.BillDateOfVoucher != null)
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", (item.BillDateOfVoucher.Value.ToString("dd/MM/yyyy") == "01/01/0001" || item.BillDateOfVoucher.Value.ToString("dd/MM/yyyy") == "01/01/1753" ? "" : item.BillDateOfVoucher.Value.ToString("dd/MM/yyyy"))));
                            else
                                Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:left;'>{0}</td>", string.Empty));


                        }
                        TotalSubVoucherAmt = TotalSubVoucherAmt + item.SactionAmount;
                        Result.Append("</tr>");
                        Count++;
                        //  TotalAmount = TotalAmount + item.SactionAmount;

                    }
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'><b>Total</b>", string.Empty));

                    if (BillObj.BillType == 3 || BillObj.BillType == 13 || BillObj.BillType == 19 || BillObj.BillType == 20 ||
 BillObj.BillType == 18 || BillObj.BillType == 8 || BillObj.BillType == 9 || BillObj.BillType == 10
 || BillObj.BillType == 11 || BillObj.BillType == 12 || BillObj.BillType == 4 || BillObj.BillType == 14 || BillObj.BillType == 15 || BillObj.BillType == 21
             || BillObj.BillType == 22)
                    {
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", TotalSubVoucherAmt));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                    }
                    else if (BillObj.BillType == 31)
                    {
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", TotalSubVoucherAmt));
                    }
                    else if (BillObj.BillType == 26 || BillObj.BillType == 23 || BillObj.BillType == 24 || BillObj.BillType == 25)
                    {
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", TotalSubVoucherAmt));
                    }
                    else if (BillObj.BillType == 1)
                    {
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", TotalSubVoucherAmt));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                    }
                    else
                    {
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;text-align:center;'>{0}</td>", TotalSubVoucherAmt));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                        Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;'>{0}</td>", string.Empty));
                    }
                    // string TotalAmounts = string.Format("{0:n0}", TotalAmount);

                    Result.Append("</tr>");
                }
                else
                {
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td style='border:1px solid black;border-collapse:collapse;' colspan='6'><b>No Voucher Found</b></td>", string.Empty));
                    Result.Append("</tr>");
                }

                Result.Append("</table>");


                if (BillObj.AdvanceBillNo > 0 && BillObj.TakenAmountDate != null)
                    Result.Append(string.Format("<br><div style='text-align:left;'><p>Less Advance/undisburse drawn vide T/V</p><p> Bill No. {0} &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  of  &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dated: {1}  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Amount: {2}</p></div><br><br>", BillObj.AdvanceBillNo, BillObj.TakenAmountDate.Value.ToString("dd/MM/yyyy"), BillObj.TakenAmount));
                else
                    Result.Append("<br><div style='text-align:left;'><p>Less Advance/undisburse drawn vide T/V</p><p> Bill No. 0 &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  of  &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dated:    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Amount: </p></div><br><br>");

                Result.Append(string.Format("<div style='text-align:left;'><p>Net Amount Payble &nbsp;&nbsp;&nbsp;  <b>{0}</b> (RUPEES :-  <b>{1}</b> )<br><br></p><p>Received contents Rs:<b> {2} </b> (RUPEES:- <b>{3}</b>) &nbsp;&nbsp;&nbsp; </p></div><br><br>", BillObj.TotalAmount, NumberToWordsConverter.NumbersToWords(Convert.ToInt64(BillObj.TotalAmount)), BillObj.TotalAmount, NumberToWordsConverter.NumbersToWords(Convert.ToInt64(BillObj.TotalAmount))));

                Result.Append(string.Format("<br><div style='text-align:left; '><p>Treasury Clerk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
                Result.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style=' text-align: right; float: right;'>Signature of DDO  with seal Code: "+ BudgetObj.DDOCode +"</span></p></div><br><br>");

                Result.Append("<div style='padding:1px;border-bottom:1px solid black;'><br><br></div>");

                Result.Append(string.Format("<div style='text-align:left; '><p>To be used by Treasury Office &nbsp;&nbsp;&nbsp; </p></div>"));
                Result.Append(string.Format("<div style='text-align:left; '><p>Pay Rs: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp; Rupees:</p></div><br>"));

                Result.Append(string.Format("<div style='text-align:left; '><p>Dated: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; (superintendent )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp; Treasury Officer </p></div>"));
                Result.Append("<div style='padding:1px;border-bottom:1px solid black;'></div>");
                Result.Append(string.Format("<div style='text-align:center; '><p>To be used by Accountant General &nbsp;&nbsp;&nbsp; </p></div><br>"));
                Result.Append(string.Format("<div style='text-align:left; '><p>Admitted for &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  Rs.</p></div>"));
                Result.Append(string.Format("<div style='text-align:left; '><p>Objected to &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  Rs. </p></div>"));
                Result.Append(string.Format("<div style='text-align:left; '><p>Reason for objection &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Rs. </p></div>"));
                Result.Append("</div><br><br>");


                Result.Append("</td></tr> </table><br/></section>");


            }
            else
            {
                Result.Append("<p><b>No bill found or don't allocate fund to budget(related to this bill) till now.</b><p>");
            }
            Result.Append("</body>");
            return Result.ToString();
        }


        public static string GetHRT2Form(mEstablishBill BillObj)
        {

            StringBuilder Parameter = new StringBuilder();
            StringBuilder Result = new StringBuilder();
            string outXml = "";
            string BillType = string.Empty;
            if (BillObj.BillType == 27)
                BillType = "Leave Travel Concession ";
            else
                BillType = "Gratuity As per office order No.";

            mBudget BudgetObj = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = BillObj.BudgetId });
            // AllocateBudget AllocateSactionBud = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = BillObj.BudgetId });
            if (BillObj != null && BillObj.EstablishBillID > 0 && BudgetObj != null)
            {

                var VoucherList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllBillByEstablishBillId", new mEstablishBill { EstablishBillID = BillObj.EstablishBillID });
                string SanctionNumber = string.Empty;
                string SanctionDate = string.Empty;
                decimal RetirementGratuity = VoucherList.Sum(m => m.RetirementGratuity);
                decimal LeaveEncashment = VoucherList.Sum(m => m.LeaveEncashment);
                decimal LTC = VoucherList.Sum(m => m.LTC);
                if (VoucherList != null && VoucherList.Count() > 0)
                {
                    foreach (var item in VoucherList)
                    {
                        SanctionNumber = SanctionNumber + item.SactionNumber + ",";
                        SanctionDate = SanctionDate + item.SactionDate.Value.ToString("dd/MM/yyyy") + ",";
                    }

                }

                outXml = @"<html>                           
                                 <body style='font-family:DVOT-Yogesh'> <div><div style='width: 100%;'><table style='width: 100%;'>";
                outXml += @"<div> 

<table style='width:100%;margin-left:10px;margin-top:12px;;margin-bottom:5px'>
    <tr style='width:100%;font-size:25px'>
        <td >
       H.P.T.R. 2 
        </td>
        <td style='text-align:center;font-size:25px'>
            DISNIC-TREASURY
        </td>
        <td style='text-align:right;font-size:25px'>
            NICNET-H.P
        </td>
    </tr>
  
</table>

<table style='width:100%;border: 1px solid black;border-spacing: 0;border-collapse: collapse;'> 

<tr> 

<td style='width:50%;font-size:25px'>
<< PAY BILL >>
    <br />
    DETAILED PAYBILL OF INCUMBENT OF ESTABLISHMENT OF: Secretary H.P Vidhan Sabha
    <br />
    FOR THE MONTH OF: payment of " + BillObj.BillTypeName + @"
</td>

<td style='width:50%;'>
<table style='width:100%;border-spacing: 0;border-collapse: collapse;'>
<tr>
<td style='width:30%;border: 1px solid black;'>
<table>

<tr>
<tr>
<td style='font-size:25px'>
</td>
</tr>
<td style='font-size:25px'>
Bill No.: " + BillObj.BillNumber + @"
</td>
<tr>
<td style='width:100%;font-size:25px' >
Bill Date.: " + BillObj.BillProcessingDate.Value.ToString("dd/MM/yyyy") + @"
</td>
</tr>
</tr>
</table>
</td>
<td style='width:30%;border: 1px solid black;'>
<table>
<tr>
<tr>
<td style='font-size:25px'>
</td>
</tr>
<td style='font-size:25px'>
Token No.:.........
</td>
<tr>
<td style='width:100%;font-size:25px' >
Token Date:.........
</td>
</tr>
</tr>
</table>
</td>
<td style='width:30%;border: 1px solid black;'>
<table>
<tr>
<td style='font-size:25px'>
(For Treasury Officer Use)
</td>
</tr>
<tr>
<td style='font-size:25px'>
Voucher No.:.........
</td>
<tr>
<td style='width:100%;font-size:25px' >
Voucher Date:.........
</td>
</tr>
</tr>
</table>
</td>
</tr>
</table>
</td>

</tr>  



<tr>  
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;font-size:25px'>
    <tr>
        <td> 1. Treasury Code:  </td>
            <td>" + BudgetObj.TreasuryCode + @"</td>
        <td>2. Demand No:</td>
        <td>" + BudgetObj.DemandNo + @"</td>
    </tr>   
    <tr>
        <td> 3. D.D.O.Code:  </td>
            <td>" + BudgetObj.DDOCode + @"</td>
        <td>4. Gaztd./Non-Gaztd:</td>
        <td>" + BudgetObj.Gazetted + @"</td>
    </tr>
    <tr>
        <td>  5. Major Head:  </td>
            <td>" + BudgetObj.MajorHead + @"</td>
     
    </tr>
    <tr>
        <td> 6. Sub-Major Head:  </td>
            <td>" + BudgetObj.SubMajorHead + @"</td>
  
    </tr>
     
     <tr>
        <td> 7. Minor-Head:  </td>
            <td>" + BudgetObj.MinorHead + @"</td>
    
    </tr>
     <tr>
        <td>   8. Sub Head:  </td>
            <td>" + BudgetObj.SubHead + @"</td>
      
    </tr>
     <tr>
        <td> 9. Budget Code: </td>
            <td>" + BudgetObj.BudgetCode + @"</td>
        <td>10. Object No:</td>
        <td>" + BudgetObj.ObjectCode + @"</td>
    </tr>
     <tr>
        <td>11. Plan/Non-Plan:  </td>
            <td>" + BudgetObj.Plan + @"</td>
        <td> 12. Voted/Charged:</td>
        <td>" + BudgetObj.VotedCharged + @"</td>
    </tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>
<tr>
<td style='width:100%;'>
<table style='width:100%;font-size:25px''>
    <tr>
        <td>  1. GROSS TOTAL:  </td>
        <td></td>
            <td>Rs " + BillObj.TotalGrossAmount + @"</td>
        
    </tr>   
    <tr>
        <td>  2. SHORT DRAWAL:  </td>
         <td></td>
            <td>Rs 0</td>
        
    </tr>
    <tr>
        <td> 3. TOTAL AMOUNT:</td>
         <td>(1-2)</td>
            <td>Rs " + BillObj.TotalGrossAmount + @"</td>
     
    </tr>
    <tr>
        <td>4. A.G DEDUCTIONS  (A):  </td>
         <td></td>
            <td>Rs  " + VoucherList.Sum(m => m.MiscAG) + @" </td>
  
    </tr>
     <tr>
        <td> 5. BALANCE AMOUNT:  </td>
         <td>(3-4) </td>
            <td> Rs " + BillObj.TotalGrossAmount + @"</td>
     
    </tr>
     <tr>
        <td> 6. B.T.DEDUCTIONS:  (B):  </td>
          <td></td>
            <td>Rs  " + VoucherList.Sum(m => m.MiscBT) + @"</td>
    
    </tr>
     <tr>
        <td>  7. NET AMOUNT (payble):  </td>
          <td>(5-6)</td>
            <td>Rs <b> " + BillObj.TotalGrossAmount + @"</b></td>
      
    </tr>
     
</table>
</td>
</tr>
</table>
</td>
</tr>   <!-- end 2 row -->


<tr>  <!-- Start 3 row -->
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;;font-size:25px'>    
<tr>
<td style='width:50%;border: 1px solid black;'>
       <span>&nbsp&nbsp&nbsp SUB-OBJECT(DETAILED) HEADS</span>
    <table style='width:100%;font-size:25px''>
     
    <tr>
        <td> 1. Basic Pay:  </td>
        <td>01</td>
            <td>... </td>
        
    </tr>
          <tr>
        <td> 2. Special Pay:  </td>
        <td>02</td>
            <td>... </td>
        
    </tr>   
         <tr>
        <td> 3. Dearness Allow:  </td>
        <td>03</td>
            <td>... </td>
        
    </tr>   
         <tr>
        <td> 4. Compensatory Allow:  </td>
        <td>04</td>
            <td>...  </td>
        
    </tr>   
         <tr>
        <td> 5. House Rent Allow:  </td>
        <td>05</td>
            <td>... </td>
        
    </tr>   
         <tr>
        <td> 6. Capital Allowance: </td>
        <td>06</td>
            <td>... </td>
        
    </tr>   
        <tr>
        <td>  7. Conveyance Allow: </td>
        <td>07</td>
            <td>... </td>
        
    </tr>   
        <tr>
        <td> 8. Washing Allowance: </td>
        <td>08</td>
            <td>... </td>
        
    </tr>   
        <tr>
        <td>  9. <b> Retirement Gratuity :</b> </td>
        <td> </td>
            <td><b> " + RetirementGratuity + @"</b> </td>
        
    </tr>   
          <tr>
        <td>  10. Leave Encashment: </td>
        <td> </td>
            <td><b> " + LeaveEncashment + @"</b> </td>
        
    </tr>   
          <tr>
        <td>  11. LTC : </td>
        <td> </td>
            <td><b> " + LTC + @"</b> </td>
        
    </tr>   
          <tr>
        <td>  12. : </td>
        <td>.......</td>
            <td>... </td>
        
    </tr>   
          <tr>
        <td>  13. : </td>
        <td>.......</td>
            <td>... </td>
        
    </tr>   
        </table>
</td>
    <td style='width:50%; border: 1px solid black;'>
<span>&nbsp&nbsp&nbsp (A) DEDUCTIONS (CLASSIFIED BY A.G)</span>
<table style='width:100%;font-size:25px''>
    <tr>
        <td>  1. GPS Subscription:  </td>
        <td></td>
            <td>... </td>
        
    </tr>
          <tr>
        <td> 2. GPF Advance Recovery:  </td>
        <td></td>
            <td>... </td>
        
    </tr>   
         <tr>
        <td> 3. House Building Advance:  </td>
        <td></td>
            <td>... </td>
        
    </tr>   
         <tr>
        <td> 4. ' '  Intrest:  </td>
        <td></td>
            <td>... </td>
        
    </tr>   
         <tr>
        <td> 5. M.Car/Scooter Advance:  </td>
        <td></td>
            <td>... </td>
        
    </tr>   
         <tr>
        <td> 6. ' '  Intrest: </td>
        <td></td>
            <td>... </td>
        
    </tr>   
        <tr>
        <td>  7. Warm Cloth Advance: </td>
        <td> </td>
            <td>... </td>
        
    </tr>   
        <tr>
        <td> 8. ' '  Intrest: </td>
        <td></td>
            <td>... </td>
        
    </tr>   
        <tr>
        <td>  9. L.T.C./T.A Advance: </td>
        <td></td>
            <td>... </td>
        
    </tr>   
          <tr>
        <td>  10. Festival Advance: </td>
        <td></td>
            <td>... </td>
        
    </tr>   
          <tr>
        <td> 11. Miscellaneous Recovery: </td>
        <td></td>
            <td>Rs  " + VoucherList.Sum(m => m.MiscAG) + @" </td>
        
    </tr>   
          <tr>
        <td>  12.  </td>
        <td></td>
            <td>... </td>
        
    </tr>   
       
        </table>
</td>
</tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;font-size:25px'>
  
<tr>
    
<td style='width:100%;'>
    <span>(B) DEDUCTIONS(CLASSIFIED BY T.O):</span>    
 <span >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp< * CORRESPONDING RECEIPTS CODES * ></span> 
    <table style='width:100%;font-size:25px'>


   <tr>
        <td ></td>     
            <td ></td>
           <td style='border: 1px solid black;'>Major</td>
           <td style='border: 1px solid black;'>S.Maj</td>
           <td style='border: 1px solid black;'>Minor</td>
           <td style='border: 1px solid black;'>S.Head</td>
           <td style='border: 1px solid black;'>000 Code</td>

        
    </tr>
    <tr>
        <td >   1. Insurance fund:  </td>     
            <td>... </td>
 <td style='border: 1px solid black;'>8011</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>105</td>
           <td style='border: 1px solid black;'>01</td>
           <td style='border: 1px solid black;'></td>
        
    </tr>
          <tr>
        <td> 2. Savings Fund:  </td>        
            <td>... </td>
  <td style='border: 1px solid black;'>8011</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>105</td>
           <td style='border: 1px solid black;'>02</td>
                <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td> 3. House Rent:  </td>
            <td>... </td>
  <td style='border: 1px solid black;'>0216</td>
           <td style='border: 1px solid black;'>01</td>
           <td style='border: 1px solid black;'>106</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td>  4.Postal Life Insurance:</td>
            <td>... </td>
    <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>103</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td> 5.Life Insurance Corp:  </td>
            <td>... </td>
    <td style='border: 1px solid black;'>8448</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>104</td>
           <td style='border: 1px solid black;'>00</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
         <tr>
        <td>  6.Income tax: </td>
            <td>... </td>
   <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>112</td>
           <td style='border: 1px solid black;'>01</td>
               <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td> 7. Surcharge: </td>
            <td>... </td>
    <td style='border: 1px solid black;'>8658</td>
           <td style='border: 1px solid black;'>00</td>
           <td style='border: 1px solid black;'>112</td>
           <td style='border: 1px solid black;'>02</td>
              <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td>8. Other B.T |: </td>
            <td>... </td>
   <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
        
    </tr>   
        <tr>
        <td> 9. Other B.T ||: </td>
            <td>... </td>
         <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
    </tr>   
          <tr>
        <td>    10. Other B.T |||: </td>
            <td>Rs  " + VoucherList.Sum(m => m.MiscBT) + @" </td>
         <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
           <td style='border: 1px solid black;'></td>
              <td style='border: 1px solid black;'></td>
    </tr>   
        
        
       
        </table>
</td>
</tr>
</table>
</td>
</tr>  

    <tr>  
<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>    
<tr>
<td style='width:50%;border: 1px solid black;'>
    <table style='width:100%';font-size:25px'>
     
    <tr>
        <td style='font-size:25px'>GROSS TOTAL </td>
        <td></td>
            <td style='font-size:25px'>Rs<b> " + BillObj.TotalGrossAmount + @"</b></td>
        
    </tr>
           
             
        </table>
</td>
    <td style='width:50%; border: 1px solid black;'>
<table style='width:100%'>
    <tr>
        <td style='font-size:25px'> TOTAL (A)  </td>
        <td></td>
            <td style='font-size:25px'>Rs 0</td>
        
    </tr>
              
        </table>
</td>
</tr>
</table>
</td>

<td style='width:50%;border: 1px solid black;'>
<table style='width:100%;'>
  
<tr>
    
<td style='width:100%;'>
 <table style='width:100%'>
    <tr>
        <td style='font-size:25px'> TOTAL (B)  </td>
        <td></td>
            <td style='font-size:25px'>Rs 0</td>
        
    </tr>
              
        </table>
</td>
</tr>
</table>
</td>
</tr>

</table>
<table style='width:100%'>
    <tr style='width:100%'>
        <td style='width:25%'>
            <table style='width:100%;font-size:25px''>
                <tr style='width:100%;'>
                    <td >
                        Station :
                    </td>
                     <td style='margin-top:10px'>
Shimla
                    </td>

                </tr>

                <tr style='width:100%'>
                    <td>
                        Date :
                    </td>
   <td > " + BillObj.BillProcessingDate.Value.ToString("dd/MM/yyyy") + @"</td>
                     
                </tr>
            </table>
        </td>
        <td style='width:25%;font-size:25px'>
            (Treasury Clerk)
        </td>
        <td style='width:50%'>
            <table style='width:100%;font-size:25px'>
                <tr style='width:100%'>
                    <td>
                        (Signature & Designation of Drawing Officer) 
                    </td>
                     

                </tr>

                <tr style='width:100%'>
                    <td>
                        Code:" + BudgetObj.DDOCode + @"
                    </td>
                     
                </tr>
            </table>


</tr>

</table>  

    <div style='page-break-after: always;'></div>
<table>
<tr>
" + GetTR2Result(VoucherList) + @"

</tr>

</table>  
   <div style='page-break-after: always;'></div>

    <table style='width:100%;margin-left:10px;margin-top:0px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  CERTIFICATS</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
         1.  The Drawl is being made on account of <b> " + BillType + SanctionNumber + @" Dated." + SanctionDate + @"
</td>
 </tr>  
    </table>
<table style='font-size:22px'> 
    <tr>
      <td>
         2.  Arrears were less drawn vide T/V No......................................
</td>

 </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         3.  Certified that pay ans allowances drawn in this bill are due ans admissible as per authority is force and the deductions where-ever required have been made, as per the rules.
</td>
         </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         4.  Certified that in case of fresh appointees , the medical fitness certicates have been obtained.

</td>
 </tr>
        </table> 
<table style='font-size:22px'> 
    <tr>
     <td>
         5.  Certified that all appointments and promotions, grant of leave and period of suspensions, and the deputation and other events which are required to be recorded, have been recorded in the
         service Book & leave account of the concerned employee.
</td>
         </tr>
        </table>
  <table style='font-size:22px'> 
    <tr>
     <td>
       
         6.  Recieved contents (in Cash) Rs.<b> " + BillObj.TotalGrossAmount + @" (" + NumberToWordsConverter.NumbersToWords(Convert.ToInt64(BillObj.TotalGrossAmount)) + @")</b>
</td>
 </tr>
        </table>

<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:2px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:right'>
             (Signature & Designation of Drawing and Disbursing Officer)
        </td>
       
        
    </tr>
  <tr style='width:100%'>
       
        <td style='text-align:right;padding-right:306px'>
             Code No. " + BudgetObj.DDOCode + @"
        </td>  
    </tr>
</table>
<div style='border: 1px solid black;'>

</div>

<table style='width:100%;margin-left:10px;margin-top:12px;margin-bottom:2px;font-size:16px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  (TO BE USED BY TREASURY OFFICE)</b> 
        </td>
       
    </tr>
  
</table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
      Pay Rs.......................................(In Words) Rupees...............................................................................................................................................................
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:22px'> 

 <tr>  

     <td>
      (Superintendent Treasury)
</td>
        <td>
      Date.....................
</td>
      <td>
      (Treasury Officers )
</td>
 </tr>  
    </table>

<table style='width:100%;font-size:16px'> 

 <tr>  

     <td>
     Account to be classified by T.O. :
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:20px'> 

 <tr>  

     <td>
      1. Cash
</td>
        <td>
      :..........................
</td>
 
 </tr>  
     <tr>  

     <td>
      2. G.I.S (S.F)
</td>
        <td>
      :..........................
</td>
 
 </tr> 
    
         <tr>  

     <td>
      3.  (I.F.)
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      4. LIC
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      5.  PLI
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      6.  House Rent
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      7.  Income Tax
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      8.  Surcharge
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
      9.  Miscellaneous
</td>
        <td>
      :..........................
</td>
 
 </tr>   
     <tr>  

     <td>
        &nbsp;&nbsp;&nbsp;    **Total**
</td>
        <td>
      :..........................
</td>
 
 </tr>   
    </table>
<div style='border: 1px solid black;'>

</div>
<table style='width:100%;margin-left:10px;margin-top:2px;margin-bottom:5px;font-size:16px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>  (TO BE USED BY ACCOUNTANT GENERAL OFFICE)</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:22px'> 
<tr>
     
     <td>
     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Supdt.
</td>
     </tr>
    <tr>
       <td>
     Initials of..........................................in token of
</td>
        <td>
      Admitted Rs.........................................
</td>
 
 </tr>  
 

    <tr>
     
     <td>
     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp A.A.O
</td>
     </tr>
    <tr>
       <td>
     Check of classification of
</td>
        <td>
      Objected Rs.........................................
</td>
 
 </tr>  

    <tr>
     
     <td>
   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp   500
</td>
     </tr>
     <tr>
       <td>
     Item above Rs........
</td>
      
 
 </tr>
    <tr>
     
     <td>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  5000
</td>
     </tr>  
 
    </table>


<table style='width:100%;margin-left:10px;margin-top:2px;margin-bottom:5px;font-size:22px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         Auditor 
        </td>
       
    </tr>
  
</table>
<div style='border: 1px solid black;'>

</div>
<table style='width:100%;margin-left:10px;margin-top:2px;margin-bottom:5px;font-size:12px'>
    <tr style='width:100%'>
       
        <td style='text-align:center'>
         <b>INSTRUCTIONS</b> 
        </td>
       
    </tr>
  
</table>

<table style='width:100%;font-size:16px'> 

 <tr>  

     <td>
         1.  A red line should be drawn right across the sheet after each section of the establishment and GRANDS TOTALS should be in red link.
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:16px'> 

 <tr>  

     <td>
         2.  All deductions should be supported by schedules in appropriate form. There should be seperate schedules for each G.P.F series and the G.P.F. account No. be entered therein in ascending order.
</td> 
 </tr>  
    </table>
<table style='width:100%;font-size:16px'> 

 <tr>  

     <td>
         3.  Recovery of House Rent should be supported by rent Rolls in duplicate form the PWD/Estate Officer.Deduction adjustable by B.T. should also be supported by duplicate schedules.
</td>
 </tr>  
    </table>
<table style='width:100%;font-size:16px'> 

 <tr>  

     <td>
         4.  Due care should be taken to give correct code numbers wherever specified.
</td>
 </tr>  
    </table>
<div style='border: 1px solid black;'>

</div>




       
                     </body> </html>";




            }
            else
            {
                Result.Append("<p><b>No bill found or don't allocate fund to budget(related to this bill) till now.</b><p>");
            }
            Result.Append(outXml);
            return Result.ToString();
        }



        public static string GetHRT4Form(mEstablishBill BillObj)
        {
            StringBuilder Parameter = new StringBuilder();
            StringBuilder Result = new StringBuilder();


            var Voucher = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllBillByEstablishBillId", new mEstablishBill { EstablishBillID = BillObj.EstablishBillID })).FirstOrDefault();

            // Crete Html Template for Bill Report
            Result.Append("<body style='width:900px;margin:auto;font-weight:bold;font-family:'Times New Roman';'>");
            Result.Append("<section><header style='text-align:center;'><h4>H.P.T.R. 4</h4>");
            Result.Append("<p>GENERAL PROVIDENT FUND BILL</p><h3>ESTABLISHMENT OF H.P VIDHAN SABHA</h3><P style='font-weight:normal;'>Advance/Withdrawal   <lable style='float:right;font-weight:normal;'> ( For Treasury Office use ) </lable></P>");
            Result.Append("<div style='border-bottom:2px solid black;height: 65px;padding-bottom: 18px;'>");
            Result.Append(string.Format("<p style='width:50%; float:left;text-align: left;font-weight:normal;'>Bill No <span><b>{0}</b> of <b> {1}</b></span><br> <br>", BillObj.BillNumber, BillObj.FinancialYear));
            Result.Append(string.Format(" Bill Date <span><b>{0}</b></span></p>", Convert.ToDateTime(BillObj.BillProcessingDate).ToString("dd/MM/yyyy")));
            Result.Append("<p style='width:50%;font-weight:normal; float:left;text-align: right;'>&nbsp;Vouch.No <span>_____________</span><br> <br>");
            Result.Append(" Vouch.Date <span>____________</span></p>");
            Result.Append("</div> </header>");

            Result.Append("<table cellpadding='6' style='width:100%; float:left'>");
            Result.Append("<tr style='text-align: left;'><td style='width:200px;'>Treasury Code</td><td>SML00</td><td colspan='3'></td></tr><tr><td>D.O.O. Code</td><td>039</td><td colspan='3'></td></tr>");
            Result.Append("<tr><td>Major Head</td> <td>8009</td> <td  ></td><td colspan='2'>: STATE PROVIDENT FUND</td></tr>");
            Result.Append(" <tr><td>Sub-Major Head</td><td>01</td> <td  ></td><td colspan='2' style='border-bottom: 2px solid black;' >: CIVIL</td>");
            Result.Append("<td style='width:200px;' ></td></tr><tr><td>Minor Head</td><td><b>101</b></td>");

            Result.Append("<td colspan='3'></td> </tr><tr><td>Sub Head</td><td>02</td><td> </td><td  > ADVANCE = 01</td><td  >WITHDRAWAL = 02</td>");
            Result.Append("</tr><tr><td>Object Code</td><td>47</td> </tr> </tbody></table>");

            Result.Append("<div style='width:100%; border-top: 2px solid black;display: inline-block;'>");
            Result.Append("<table cellpadding='6'>");
            if (BillObj.SanctionDate != null)
                Result.Append(string.Format("<tr><td width='35%'>Particulars (No and date of Sanction)</td><td>{0} &nbsp;&nbsp; &nbsp;&nbsp;  {1} </td></tr>", BillObj.SanctioNumber, BillObj.SanctionDate.Value.ToString("dd/MM/yyyy")));
            else
                Result.Append(string.Format("<tr><td width='35%'>Particulars (No and date of Sanction)</td><td>{0} &nbsp;&nbsp; &nbsp;&nbsp;  {1} </td></tr>", BillObj.SanctioNumber, string.Empty));

            if (Voucher.Designation == 4 || Voucher.Designation == 5)
                Result.Append(string.Format("<tr><td>Name of Subscriber and Pay</td><td> <b style='font-size:20px;'>{0}</b>  &nbsp;&nbsp; &nbsp;&nbsp;<b style='font-size:20px;'> {1}</b></td></tr>", Voucher.ClaimantName, Voucher.DesignationName));
            else
                Result.Append(string.Format("<tr><td>Name of Subscriber and Pay</td><td><b style='font-size:20px;'>{0}</b>  &nbsp;&nbsp; &nbsp;&nbsp;</td></tr>", Voucher.ClaimantName));

            Result.Append(string.Format("<tr><td>Account Number </td><td>{0}</td></tr>", Voucher.Number));


            string SactionAmount = Voucher.SactionAmount.ToString();// string.Format("{0:n0}", Voucher.SactionAmount);
            Result.Append(string.Format("<tr><td>Amount</td><td><b style='font-size:20px;'>{0} </b> &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; RUPEES :<b style='font-size:20px;'>{1}</b></td></tr>", SactionAmount, NumberToWordsConverter.NumbersToWords(Convert.ToInt64(Voucher.SactionAmount))));

            Result.Append("<tr><td></td><td></td></tr> </table>  </div>");

            Result.Append("<div style='width:100%;text-align:right; border-top: 2px solid black;display: inline-block;'>");
            Result.Append("<table cellpadding='6'>");
            Result.Append(string.Format("<tr><td width='50%'>Recieved Content Rs.<b> {0}</b>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;  &nbsp;</td><td>Rupees: <b>{1}</b> </td></tr>", SactionAmount, NumberToWordsConverter.NumbersToWords(Convert.ToInt64(Voucher.SactionAmount))));
            Result.Append(" </table> </div>");

            Result.Append("<div style='width:100%;text-align:right; border-top: 2px solid black;display: inline-block;'><br><br><br><br><br>");
            Result.Append("<p style='font-weight:normal;'>Signature of the DDO</p></div>");
            Result.Append("<div style='width:100%; border-top: 2px solid black;display: inline-table;'>");
            Result.Append("<p style='text-align:center;font-weight:normal;padding: 0px;margin: 0px;'>FOR USE IN TREASURY OFFICE</p><br>");
            Result.Append("<p style='width:50%;float:left;font-weight:normal;padding: 0px;margin: 0px;'>Pay Rs. </p>  <p style='width:50%;float:left;font-weight:normal;padding: 0px;margin: 0px;'>Rupee(  </p><br><br><br><br><br>");


            Result.Append("<p style='width:50%;float:left;font-weight:normal;font-weight:normal;'>( Treasury Accountant ) </p>  <p style='width:50%;font-weight:normal;float:right;text-align:right;'> Treasury officer </p>");
            Result.Append("</div>");
            Result.Append("<div style='width:100%; border-top: 2px solid black;'>");
            Result.Append("<p style='text-align:center;font-weight:normal;'>( For use in A.G. Office )</p>");
            Result.Append("<table>");
            Result.Append("<tr><td>Admitted for</td><td>Rs.___________</td></tr>");
            Result.Append("<tr><td>Objected for</td><td>Rs.___________</td></tr>");
            Result.Append("<tr><td>Reasons of objection</td><td>_______________</td></tr>");
            Result.Append("</table></div>");
            Result.Append("<br><br><br>");
            Result.Append("</section>");
            Result.Append("</body>");
            return Result.ToString();
        }

        public static string GetMCAForm(mEstablishBill BillObj)
        {
            StringBuilder Parameter = new StringBuilder();
            StringBuilder Result = new StringBuilder();
            mBudget BudgetObj = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = BillObj.BudgetId });
            AllocateBudget AllocateSactionBud = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = BillObj.BudgetId });
            if (BillObj != null && BillObj.EstablishBillID > 0 && BudgetObj != null && AllocateSactionBud != null && AllocateSactionBud.Sanctionedbudget > 0)
            {
                var Voucher = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllBillByEstablishBillId", new mEstablishBill { EstablishBillID = BillObj.EstablishBillID });

                // Crete Html Template for Bill Report
                Result.Append("<html><body style='font-family:verdana;font-size:14px'><center>");
                Result.Append("<div style='width:800px;'><center><p>HIMACHAL PRADESH VIDHAN SABHA SECRETARIAT</p></center>");
                Result.Append("<p>CAR ADVANCE TO HON'BLE MEMBERS OF HIMACHAL PRADESH VIDHAN SABHA</p>");
                Result.Append("<table style='width:100%'>");
                Result.Append(string.Format("<tr><td>Treasury Code:</td><td>: {0}</td><td>Bill Number</td><td>: {1} of {2}</td></tr>", BudgetObj.TreasuryCode, BillObj.BillNumber, BillObj.FinancialYear));
                Result.Append(string.Format("<tr><td>Demand Number </td><td>: {0}</td><td>Bill Date</td><td>: {1}</td></tr></tr>", BudgetObj.DemandNo, BillObj.BillProcessingDate.Value.ToString("dd/MM/yyyy")));
                Result.Append(string.Format("<tr><td>DDO COde</td><td>: {0}</td></tr></table>", BudgetObj.DDOCode));
                Result.Append(" <p style='text-align:left'><u> HEAD OF ACCOUNT</u></p>");
                Result.Append("<table style='float:left;width:100%;' cellpadding=3>");

                Result.Append(string.Format("<tr><td>'{0}-Advances to the govt. employees etc</td></tr>", BudgetObj.MajorHead));
                Result.Append(string.Format("<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{0}-Advance for purchase of Motor Conveyance</td></tr>", BudgetObj.MinorHead));
                Result.Append(string.Format("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{0}-{1} to MLA's for purchase of Motor conveyance-Loans</td></tr>", BudgetObj.SubHead, BudgetObj.ObjectCodeText));
                Result.Append("<tr><td></td></tr><tr><td></td></tr><tr><td></td></tr></table>");
                Result.Append("<table border=1 style='border-collapse:collapse;width:100%'>");

                Result.Append("<tr><th>Sr.No.</th><th>Name of the Member</th><th>Pay</th><th>Amount of Advance</th></tr>");
                if (Voucher != null && Voucher.Count() > 0)
                {
                    int count = 1;
                    foreach (var item in Voucher)
                    {
                        Result.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td></td><td style='text-align:right'>{2}</td></tr>", count, item.ClaimantName, item.SactionAmount));
                        count++;
                    }
                }
                else
                {
                    Result.Append(string.Format("<tr><td colspan=4> No voucher found</td></tr>"));
                }
                Result.Append("</table>");

                Result.Append("<table style='width:100%' cellpadding=5>");
                Result.Append("<tr><td></td></tr>");
                Result.Append(string.Format("<tr><td colspan=4>Rupees :<b> {0}</b></td></tr>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(BillObj.TotalAmount))));
                if (BillObj.SanctionDate != null)
                    Result.Append(string.Format("<tr><td>Sanctioned vide order No:</td><td>{0}</td><td>Dated :{1}</td></tr>", BillObj.SanctioNumber, BillObj.SanctionDate.Value.ToString("dd/MMM/yyyy")));
                else
                    Result.Append(string.Format("<tr><td>Sanctioned vide order No:</td><td>{0}</td><td>Dated :{1}</td></tr>", BillObj.SanctioNumber, string.Empty));
                Result.Append(string.Format("<tr><td colspan=4>(Copy Enclosed)</td></tr>"));



                Result.Append(string.Format("<tr><td colspan=4>Head of Account to which the pay is to be charged '2011 - Parliament---'</td></tr>"));

                Result.Append(string.Format("<tr><td colspan=4>Rs. {0}</td></tr>", BillObj.TotalAmount));

                Result.Append(string.Format("<tr><td colspan=4>Rupees: <b>{0}</b></td></tr>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(BillObj.TotalAmount))));
                Result.Append("<tr><td colspan=4></td></tr><tr><td colspan=4></td></tr>");
                Result.Append("<tr><td colspan=4></td></tr><tr><td colspan=4></td></tr>");
                Result.Append("<tr><td colspan=2><Center>Received Payment</center></td> <td colspan=2 style='text-align:right;'><Center>Received Contents</center></td></tr></table>");

                Result.Append("<hr><table style='float:left;width:100%;' cellpadding=5>");
                Result.Append("<tr><td>List of payment for examination for use in Ag's Office:</td></tr>");
                Result.Append("<tr><td>Admitted for Rs.:</td></tr>");
                Result.Append("<tr><td>Objected for Rs.:</td></tr></table>");

                Result.Append("<hr><table style='float:left;width:100%;' cellpadding=5>");

                decimal TotalAdditonlFund = (decimal)Helper.ExecuteService("Budget", "GetTotalAdditionById", new AdditionFunds { BudgetId = BillObj.BudgetId });

                decimal TotalBillExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpense", new mEstablishBill { BudgetId = BillObj.BudgetId, EstablishBillID = BillObj.EstablishBillID });
                decimal TotalFund = AllocateSactionBud.Sanctionedbudget + TotalAdditonlFund;

                decimal BalanceAmt = TotalFund - TotalBillExpense;
                Result.Append(string.Format("<tr><td style='width:40%;'>Budget for {0}</td><td>Rs:{1}</td></tr>", BudgetObj.FinancialYear, TotalFund));
                Result.Append(string.Format("<tr><td>Expenditure including this Bill</td><td>Rs:{0}</td></tr>", TotalBillExpense));
                Result.Append(string.Format("<tr><td>Balance</td><td>Rs:{0}</td></tr></table>", BalanceAmt));

                Result.Append("</div></center></body></html>");
            }
            else
            {
                Result.Append("<p><b>No bill found or don't allocate fund to budget(related to this bill) till now.</b><p>");
            }

            return Result.ToString();
        }

        public static string GetHBAForm(mEstablishBill BillObj)
        {
            StringBuilder Parameter = new StringBuilder();
            StringBuilder Result = new StringBuilder();
            mBudget BudgetObj = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = BillObj.BudgetId });
            AllocateBudget AllocateSactionBud = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = BillObj.BudgetId });
            if (BillObj != null && BillObj.EstablishBillID > 0 && BudgetObj != null && AllocateSactionBud != null && AllocateSactionBud.Sanctionedbudget > 0)
            {
                var Voucher = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllBillByEstablishBillId", new mEstablishBill { EstablishBillID = BillObj.EstablishBillID });

                // Crete Html Template for Bill Report
                Result.Append("<html><body style='font-family:verdana;font-size:14px'><center>");
                Result.Append("<div style='width:800px;'><center><p>HIMACHAL PRADESH VIDHAN SABHA SECRETARIAT</p></center>");
                Result.Append("<p>HOUSE BUILDING ADVANCE TO HON'BLE MEMBERS OF HIMACHAL PRADESH VIDHAN SABHA SABHA</p>");
                Result.Append("<table style='width:100%'>");
                Result.Append(string.Format("<tr><td>Treasury Code:</td><td>: {0}</td><td></td><td>Bill Number</td><td>: {1} of {2}</td></tr>", BudgetObj.TreasuryCode, BillObj.BillNumber, BillObj.FinancialYear));
                Result.Append(string.Format("<tr><td>Demand Number </td><td>: {0}</td><td></td><td>Bill Date</td><td>: {1}</td></tr>", BudgetObj.DemandNo, BillObj.BillProcessingDate.Value.ToString("dd/MM/yyyy")));
                Result.Append(string.Format("<tr><td>DDO COde</td><td>: {0}</td></tr></table>", BudgetObj.DDOCode));
                Result.Append(" <p style='text-align:left'><u> HEAD OF ACCOUNT</u></p>");
                Result.Append("<table style='float:left;width:100%;' cellpadding=3>");

                Result.Append(string.Format("<tr><td>'{0}-Advances to the govt. employees etc</td></tr>", BudgetObj.MajorHead));
                Result.Append(string.Format("<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{0}-House Building Advance</td></tr>", BudgetObj.MinorHead));
                Result.Append(string.Format("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{0}-{1} House Building Advance to MLA's for Construction of House</td></tr>", BudgetObj.SubHead, BudgetObj.ObjectCodeText));
                Result.Append("<tr><td></td></tr><tr><td></td></tr><tr><td></td></tr></table>");
                Result.Append("<table border=1 style='border-collapse:collapse;width:100%'>");

                Result.Append("<tr><th>Sr.No.</th><th>Name of the Member</th><th>Pay</th><th>Amount of Advance</th></tr>");
                if (Voucher != null && Voucher.Count() > 0)
                {
                    int count = 1;
                    foreach (var item in Voucher)
                    {
                        Result.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td></td><td style='text-align:right'>{2}</td></tr>", count, item.ClaimantName, item.SactionAmount));
                        count++;
                    }
                }
                else
                {
                    Result.Append(string.Format("<tr><td colspan=4> No voucher found</td></tr>"));
                }
                Result.Append("</table>");

                Result.Append("<table style='width:100%' cellpadding=5>");
                Result.Append("<tr><td></td></tr>");
                Result.Append(string.Format("<tr><td colspan=4>Rupees :<b> {0}</b></td></tr>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(BillObj.TotalAmount))));
                if (BillObj.SanctionDate != null)
                    Result.Append(string.Format("<tr><td>Sanctioned vide order No:</td><td>{0}</td><td>Dated :{1}</td></tr>", BillObj.SanctioNumber, BillObj.SanctionDate.Value.ToString("dd/MMM/yyyy")));
                else
                    Result.Append(string.Format("<tr><td>Sanctioned vide order No:</td><td>{0}</td><td>Dated :{1}</td></tr>", BillObj.SanctioNumber, string.Empty));
                Result.Append(string.Format("<tr><td colspan=4>(Copy Enclosed)</td></tr>"));



                Result.Append(string.Format("<tr><td colspan=4>Head of Account to which the pay is to be charged '2011 - Parliament---'</td></tr>"));

                Result.Append(string.Format("<tr><td colspan=4>Rs. {0}</td></tr>", BillObj.TotalAmount));

                Result.Append(string.Format("<tr><td colspan=4>Rupees: <b>{0}</b></td></tr>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(BillObj.TotalAmount))));
                Result.Append("<tr><td colspan=4></td></tr><tr><td colspan=4></td></tr>");
                Result.Append("<tr><td colspan=4></td></tr><tr><td colspan=4></td></tr>");
                Result.Append("<tr><td colspan=2><Center>Received Payment</center></td> <td colspan=2 style='text-align:right;'><Center>Received Contents</center></td></tr></table>");

                Result.Append("<hr><table style='float:left;width:100%;' cellpadding=5>");
                Result.Append("<tr><td>List of payment for examination for use in Ag's Office:</td></tr>");
                Result.Append("<tr><td>Admitted for Rs.:</td></tr>");
                Result.Append("<tr><td>Objected for Rs.:</td></tr></table>");

                Result.Append("<hr><table style='float:left;width:100%;' cellpadding=5>");

                decimal TotalAdditonlFund = (decimal)Helper.ExecuteService("Budget", "GetTotalAdditionById", new AdditionFunds { BudgetId = BillObj.BudgetId });

                decimal TotalBillExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpense", new mEstablishBill { BudgetId = BillObj.BudgetId, EstablishBillID = BillObj.EstablishBillID });
                decimal TotalFund = AllocateSactionBud.Sanctionedbudget + TotalAdditonlFund;

                decimal BalanceAmt = TotalFund - TotalBillExpense;
                Result.Append(string.Format("<tr><td style='width:40%;'>Budget for {0}</td><td>Rs:{1}</td></tr>", BudgetObj.FinancialYear, TotalFund));
                Result.Append(string.Format("<tr><td>Expenditure including this Bill</td><td>Rs:{0}</td></tr>", TotalBillExpense));
                Result.Append(string.Format("<tr><td>Balance</td><td>Rs:{0}</td></tr></table>", BalanceAmt));

                Result.Append("</div></center></body></html>");
            }
            else
            {
                Result.Append("<p><b>No bill found or don't allocate fund to budget(related to this bill) till now.</b><p>");
            }

            return Result.ToString();
        }

        public string CreteBillChallanPdf(string Report, string Url, int BillType)
        {
            try
            {
                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), ((BillType == 27 || BillType == 29 || BillType == 30) ? PdfPageOrientation.Landscape : PdfPageOrientation.Portrait));

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = Report;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }



                Guid FId = Guid.NewGuid();
                string fileName = FId + "_BillChallan.pdf";

                string directory = Url;

                //    string directory = Server.MapPath(Url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                //string path = Path.Combine(Server.MapPath("~" + Url), fileName);
                string path = Path.Combine(Url, fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();



                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {


            }
        }

        public static string GetLoanForm(mEstablishBill BillObj)
        {
            StringBuilder Parameter = new StringBuilder();
            StringBuilder Result = new StringBuilder();
            mBudget BudgetObj = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = BillObj.BudgetId });
            //AllocateBudget AllocateSactionBud = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = BillObj.BudgetId });
            if (BillObj != null && BillObj.EstablishBillID > 0 && BudgetObj != null)
            {

                var VoucherList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllBillByEstablishBillId", new mEstablishBill { EstablishBillID = BillObj.EstablishBillID });




                // Crete Html Template for Bill Report
                Result.Append("<body style='width:800px;margin:auto;font-family:'Times New Roman';'><section><header style='text-align:center;'>");

                Result.Append("<p align=center><b>PERMANENT ESTABLISHMENT OF:SECRETARY,HIMACHAL PRADESH VIDHAN SABHA<br><br>HOUSE BUILDEING ADVANCE</b></p>");
                Result.Append("<table border='0' width='100%' style='border-collapse: collapse'>");
                Result.Append("<tr>");


                Result.Append("<td width='70%' valign='top'><b>HEAD OF ACCOUNT<br>");
                Result.Append("<b> 7610-Loan to Government Servants etc;<br>201-House Building Advance,<br>01-Advance to Government Servants.</b></td>");
                Result.Append(string.Format("<td width='30%'>Ags` Account No:--------------<p>BILL 		NO. <b>{0}</b></p>", BillObj.BillNumber));
                Result.Append(string.Format("<p>DATED:  <b>{0}</b></p></td>", BillObj.BillProcessingDate.Value.ToString("dd/MM/yyyy")));
                Result.Append(string.Format("</tr></table>"));
                Result.Append(string.Format("<p></p>"));
                Result.Append(string.Format("<table border='1' width='100%' style='border-collapse: collapse'>"));
                Result.Append("<tr>");

                Result.Append("<td width='20%'><b>Name of Employee</b></td><td width='20%'><b>Permanent/Temporary</b></td>		<td width='20%'><b>Whether surety taken</b></td>");
                Result.Append("<td width='20%'><b>Amount of Advance (Rupees)</b></td>		<td width='20%'><b>Remarks</b></td>");
                Result.Append(string.Format("</tr>"));
                foreach (var item in VoucherList)
                {
                    Result.Append("<tr>");
                    Result.Append(string.Format(" <td width='20%'>{0}</td>		<td width='20%'> </td>", item.ClaimantName));
                    Result.Append(string.Format("<td width='20%'>&nbsp;</td>		<td width='20%'>{0}</td>		<td width='20%'> </td>", item.SactionAmount));
                    Result.Append("</tr>");
                }
                Result.Append("</table>");
                Result.Append(string.Format("<p style='text-align:left;'>(In Words):&nbsp; {0}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{1}<br>------------------------------------------------------------------------------------------------------------------------------------</p>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(BillObj.TotalAmount)), BillObj.TotalAmount));
                Result.Append(string.Format("<p style='text-align:left;'>1.&nbsp;&nbsp;&nbsp; Sanction No. &amp; dated vide office Order No: <b>{0}</b><br> </p>", BillObj.SanctioNumber));


                Result.Append("<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(copy attached)</p>");
                Result.Append("<p style='text-align:left;'>2.&nbsp;&nbsp; Certified that the amount claimed in this bill is equal to 20% , 30%, 50% of the total advance.</p>");
                Result.Append("<p style='text-align:left;'>3.&nbsp;&nbsp; Head&nbsp; of Account to which pay is to be charged:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2011-- Parliament</p>");
                Result.Append(string.Format("<p>&nbsp;</p>"));

                Result.Append(string.Format("<table border='0' width='100%' style='border-collapse: collapse'>"));
                Result.Append(string.Format("<tr>"));
                Result.Append(string.Format("<td width='25%'>&nbsp;&nbsp;&nbsp;&nbsp; Rupees</td>		<td width='25%'>Countersigned</td>"));
                Result.Append("<td width='25%'>&nbsp;Received</td>		<td width='25%'>&nbsp;Received Contents</td>");


                Result.Append("</tr></table><p>&nbsp;</p><p>&nbsp;</p>");
                Result.Append("<p>---------------------------------------------------------------------------------------------------------------------------------</p>");
                Result.Append(string.Format("<p>&nbsp;</p><p style='text-align:left;'>Voucher No.<br>List of payment for:</p><p>&nbsp;</p><p style='text-align:left;'>Examined:</p><p style='text-align:left;'>Treasury Accountant<br>"));

                Result.Append(string.Format("-------------------------------------------------------------------------------------------------------------------------------------<br>For use in Accountant General's Office</p>"));
                Result.Append(string.Format("<p style='text-align:left;'>Admitted Rs-------------<br>Objected Rs-------------</p>"));
                Result.Append(string.Format("<br style='page-break-after: always;'/>"));




                Result.Append(" <div > <table style='width:100%'>  ");
                Result.Append("<tr><td style='width:60%'>PAYMENT DATA INPUT SHEET</td><td></td><td  style='text-align:left'>FOR TREASURY OFFICERS</td> </tr>");
                Result.Append(string.Format("<tr><td></td><td></td><td  style='text-align:left'>VOUCHER/DATE -----------------------------</td></tr>"));
                Result.Append(string.Format("<tr><td></td><td></td><td  style='text-align:left'>VOUCHER NO  ---------------------------------</td></tr>"));
                Result.Append("</table></div><br>");
                Result.Append(string.Format("<table border='0' width='100%' style='border-collapse: collapse'>"));
                Result.Append("<tr>");

                Result.Append("<td width='70%'>");
                Result.Append("<br>Treasury Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ");
                Result.Append(string.Format(" SMLOO<p>Demand No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	29</p>"));

                Result.Append(string.Format(" <p>DDO Code No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 039</p>"));
                Result.Append(string.Format("<p>Major Head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 		7610</p>"));
                Result.Append(string.Format("<p>Sub-Major Head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 00</p>"));
                Result.Append("<p>Minor Head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 		201</p>");

                Result.Append("<p>Sub-Head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 		01</td>");
                Result.Append("<td width='30%' valign='top'>");
                Result.Append(string.Format(" 	Gazetted/Non Gazetted&nbsp;&nbsp;&nbsp;&nbsp;N&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G/N "));

                Result.Append(string.Format("<p>Scheme Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1</p>"));
                Result.Append(string.Format("<p>Object Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;48</p>"));
                Result.Append(string.Format("<p>Voted/Charged&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 		V&nbsp;&nbsp; V/C</p>"));
                Result.Append("<p>Plan/Non-Plan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;N&nbsp;&nbsp; P/N</td>");

                Result.Append("</tr></table>");
                Result.Append("<p>-----------------------------------------------------------------------------------------------------------------------------------</p>");
                Result.Append(string.Format("<p>&nbsp;</p><p>&nbsp;</p><br>"));
                Result.Append(string.Format("<table><tr>"));
                Result.Append(string.Format("<tr><td colspan='2' style='text-align:left'>Particulars-------------------------------------------------------------------------------------------------------------------</td></tr>"));
                Result.Append(string.Format("<tr><td colspan='2' style='text-align:left'> To whom paid---------------------------------------------------------------------------------------------------------------</td></tr>"));
                Result.Append(string.Format("<tr><td style='text-align:left'>Cheque No-----------------------------------------------------------------------</td><td>Cheque Date------------------------</td></tr> "));
                Result.Append("<tr><td style='text-align:left'>Original No-----------------------------------------------------------------------</td><td>Original Date------------------------</td></tr>");

                Result.Append(string.Format("<tr><td style='text-align:left'>Account No-------------------------------------------------------------</td><td>(G.P.F)</td></tr>"));


                Result.Append(string.Format("<tr><td colspan='2' style='text-align:left'>Total amount to be classified by T.O. Rs {0}</td></tr>", BillObj.TotalAmount));


                Result.Append("<tr><td colspan='2' style='text-align:right'>Deduction---------------------------------------------------------------</td></tr>");
                Result.Append(string.Format("<tr><td colspan='2' style='text-align:right'>Net Amount {0}&nbsp;&nbsp;&nbsp;({1}) </td>", BillObj.TotalAmount, NumberToWordsConverter.NumbersToWords(Convert.ToInt64(BillObj.TotalAmount))));
                Result.Append("</tr></table>");
                Result.Append(string.Format("<p>------------------------------------------------------------------------------------------------------------------------------------</p>"));
                Result.Append(string.Format("<p>&nbsp;</p><p>&nbsp;</p><p style='text-align:left'>B.T.RECOVERIES:<br></p>"));
                Result.Append(string.Format("<table border='0' width='100%' style='border-collapse: collapse'>"));
                Result.Append(string.Format("<tr>"));
                Result.Append("<td width='50%'>1. Savings fund Rs--------------------------------------<p>");

                Result.Append("2. Insurance fund Rs------------------------------------</p>");
                Result.Append("<p>3. House Rent Rs.-------------------------------------</p>");
                Result.Append(string.Format("<p>4. P.L.I. Rs-------------------------------------------</p>"));
                Result.Append(string.Format("<p>5. L.I.C. Rs -------------------------------------------</p>"));
                Result.Append(string.Format("<p>6. Income Tax Rs.-------------------------------------</p>"));
                Result.Append(string.Format(" <p>7. Surcharge Rs.---------------------------------------</p>"));
                Result.Append(string.Format("<p>8. Other Department B.T. Rs-------------------------</p>"));
                Result.Append("<p>9. Other B.T. Rs--------------------------------------</td>");

                Result.Append("<td width='50%' valign='top'>Corresponding Receipt Heads to be filled in 		case of B.T.C. de No. 8 &amp; 9<p>");
                Result.Append("<table border='1' width='100%' style='border-collapse: collapse'>");
                Result.Append(string.Format("<tr>"));
                Result.Append(string.Format("<td width='25%'>Major</td>	<td width='25%'>S-Major</td>"));
                Result.Append(string.Format(" 	<td width='25%'>Minor</td><td width='25%'>S-Head</td>"));
                Result.Append(string.Format("</tr>			<tr>"));
                Result.Append(string.Format("<td width='25%'>&nbsp;<p>&nbsp;</p><p>&nbsp;</td><td width='25%'>&nbsp;</td>"));
                Result.Append("<td width='25%'>&nbsp;</td><td width='25%'>&nbsp;</td>");

                Result.Append("</tr><tr>");
                Result.Append("<td width='25%'>&nbsp;</td>	<td width='25%'>&nbsp;</td><td width='25%'>&nbsp;</td><td width='25%'>&nbsp;</td>");
                Result.Append(string.Format("</tr></table></p></td></tr></table>"));


            }
            else
            {
                Result.Append("<p><b>No bill found .</b><p>");
            }
            Result.Append("</body>");
            return Result.ToString();
        }

        public static string GetTR2Result(List<mReimbursementBill> VoucherList)
        {



            StringBuilder sb = new StringBuilder();
            sb.Append("<table style='border:1px solid black;width:2400px;border-collapse: collapse;'> <thead> <tr>");
            sb.Append("<th style='border :1px solid black;'>Sr. No.</th> <th style='border :1px solid black;'>Name & Designation <br>1</th> <th style='border :1px solid black;'>Basic Pay <br>2</th> <th style='border :1px solid black;'>Special Par<br> 3</th>");
            sb.Append(" <th style='border :1px solid black;'>Dearness Allowance <br>4</th><th style='border :1px solid black;'>Compens Allowance<br> 5</th><th style='border :1px solid black;'>H.R.A. Allowance <br>6</th><th style='border :1px solid black;'>Capital Allowance<br>7</th>");
            sb.Append(" <th style='border :1px solid black;'>Convey Allowance<br>8</th><th style='border :1px solid black;'>Washing Allowance<br>9</th><th style='border :1px solid black;'>GP<br>10</th><th style='border :1px solid black;'>Sect. Pay <br> 11</th><th style='border :1px solid black;width:100px;'>12</th><th style='border :1px solid black;width:100px;'>13</th><th style='border :1px solid black;width:100px;'>14</th>");
            sb.Append("<th style='border :1px solid black;'>Total 2 to 14<br> 15</th><th style='border :1px solid black;'>GPF Account No<br> 16</th>");
            sb.Append("</tr> </thead>");
            sb.Append("<tbody>");

            int count = 1;
            decimal TotalGrossAmount = 0;
            if (VoucherList != null && VoucherList.Count() > 0)
            {
                foreach (var item in VoucherList)
                {
                    sb.Append("<tr>");
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                    sb.Append(string.Format("<td style='border :1px solid black;'><b>{0}</b></td>", item.ClaimantName + " S.N. " + item.SactionNumber + " S. Date " + item.SactionDate.Value.ToString("dd/MM/yyyy")));

                    // DUES
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    // Getting total of 2-9

                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", item.GrossAmount));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//16


                    //// A.G. Deduction
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty)); //17
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//27
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//29

                    //// Treasury Deduction
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", item.GrossAmount));
                    //sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//40
                    sb.Append("</tr>");
                    count++;
                    TotalGrossAmount = TotalGrossAmount + item.GrossAmount;
                }


                sb.Append("<tr>");
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                sb.Append(string.Format("<td style='border :1px solid black;text-align:right;'><b>Grand Total </b></td>"));

                // DUES
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                // Getting total of 2-9

                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'><b>{0}</b></td>", TotalGrossAmount));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//16
                sb.Append("</table>");


                // Dedduction Part on next page
                sb.Append("<br style='page-break-after: always;'/>");
                sb.Append("<table style='border:1px solid black;page-break-after: always;width:2400px;'> <thead> <tr>");
                sb.Append("<th style='border :1px solid black;'>GPF Subs<br>17</th><th style='border :1px solid black;'>GPF Advance<br>18</th><th style='border :1px solid black;'>HBA<br>19</th> <th style='border :1px solid black;'>HBA Interest<br>20</th> <th style='border :1px solid black;'>N. Car/ Scooter Advance<br>21</th>");
                sb.Append("<th style='border :1px solid black;'>M.C./ Scooter Interest<br>22</th><th style='border :1px solid black;'>Warm cloth Advance<br>23</th><th style='border :1px solid black;'>Warm cloth Interest<br>24</th> <th style='border :1px solid black;'>Festival Advance<br>25</th>");
                sb.Append("<th style='border :1px solid black;'>L.T.C. T.A. Advance<br>26</th> <th style='border :1px solid black;'>Miscel. Recov.<br>27</th> <th style='border :1px solid black;'>28</th> <th style='border :1px solid black;'>Total (A) 17 to 28 <br>29</th>");

                sb.Append("<th style='border :1px solid black;'>Saving Fund<br>30</th> <th style='border :1px solid black;'>Insur Fund<br>31</th> <th style='border :1px solid black;'>House Rent<br>32</th> <th style='border :1px solid black;'>P.L.I.<br>33</th> <th style='border :1px solid black;'>L.I.C.<br>34</th>");
                sb.Append("<th style='border :1px solid black;'>IncomeTax<br>35</th> <th style='border :1px solid black;'>Sur-charge<br>36</th> <th style='border :1px solid black;'>OtherBT<br>37</th> <th style='border :1px solid black;'>Total (B) 30 to 37<br>38</th> <th style='border :1px solid black;'>Total Deduction <br>39</th> <th style='border :1px solid black;'>Net Payment<br>40</th>");
                sb.Append("</tr> </thead>");

                sb.Append("<tbody>");

                foreach (var item in VoucherList)
                {
                    sb.Append("<tr>");
                    // A.G. Deduction
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty)); //17
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//27
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//29

                    // Treasury Deduction
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'><b>{0}</b></td>", item.GrossAmount));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//40
                    sb.Append("</tr>");
                    //count++;
                    //TotalGrossAmount = TotalGrossAmount + item.GrossAmount;
                }
                sb.Append("<tr>");
                // A.G. Deduction
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty)); //17
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//27
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//29

                // Treasury Deduction
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'><b>Total:</b></td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'><b>{0}</b></td>", TotalGrossAmount));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//40
                sb.Append("</tr>");

                //   Deduction Final ROw
                sb.Append("<tr>");
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty)); //17
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black; text-align:right;' colspan='13'><b>{0}</b></td>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalGrossAmount))));//27
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append("</tr></table>");

            }
            else
            {
                sb.Append("<tr>");
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append("</tr>");
            }
            sb.Append(GetTR2DummyResult(VoucherList));
            return sb.ToString();
        }
        public ActionResult DownloadBillChallanPdf(int BillId)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "FileLocation" });
            string FileLocation = SiteSettingsSessn.SettingValue;

            string url = FileLocation + "BillChallan/";
            string WithoutRecordUrl = FileLocation + "BillChallan/WithoutRecord";
            string MainUrl = string.Empty;

            string directory = WithoutRecordUrl;

            //string directory = Server.MapPath(WithoutRecordUrl);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }


            string path = "";
            string FileName = string.Empty;
            string Result = string.Empty;

            mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = BillId });

            if (EstablishBill != null && !string.IsNullOrEmpty(EstablishBill.PDFUrl))
            {
                MainUrl = url;
                FileName = EstablishBill.PDFUrl;
            }
            else
            {

                DateTime GenerateDate = DateTime.Now;
                StringBuilder sb = new StringBuilder();
                mEstablishBill BillObj = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = BillId });

                if (EstablishBill.BillType == 6 || EstablishBill.BillType == 7 || //EstablishBill.BillType == 31 ||
                    // EstablishBill.BillType == 29 || EstablishBill.BillType == 28 ||
                    EstablishBill.BillType == 33)
                    Result = GetHRT4Form(BillObj);
                else if (EstablishBill.BillType == 23 || EstablishBill.BillType == 24)
                    Result = GetHBAForm(BillObj);
                else if (EstablishBill.BillType == 28 || EstablishBill.BillType == 32)
                    Result = GetLoanForm(BillObj);
                else if (EstablishBill.BillType == 25 || EstablishBill.BillType == 26)
                    Result = GetMCAForm(BillObj);
                else if (EstablishBill.BillType == 29 || EstablishBill.BillType == 30 || EstablishBill.BillType == 27)
                    Result = GetHRT2Form(BillObj);
                else
                    Result = GetHRT5Form(BillObj, EstablishBill.BillType);

                if (!Result.Contains("No bill found"))
                {
                    MainUrl = url;
                    FileName = CreteBillChallanPdf(Result, url, EstablishBill.BillType);
                    mEstablishBill stateToEdit = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = BillId });
                    stateToEdit.PDFUrl = FileName;
                    stateToEdit.IsChallanGenerate = true;
                    Helper.ExecuteService("Budget", "UpdateEstablishBill", stateToEdit);

                    DataSet dataSet = new DataSet();
                    var Parameter1 = new List<KeyValuePair<string, string>>();
                    Parameter1.Add(new KeyValuePair<string, string>("@EstablishBillId", stateToEdit.EstablishBillID.ToString()));
                    Parameter1.Add(new KeyValuePair<string, string>("@IsChallanGenerate", "1"));
                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateBillChallanOfReimbursementBill", Parameter1);
                }
                else
                {
                    MainUrl = WithoutRecordUrl;
                    FileName = CreteBillChallanPdf(Result, WithoutRecordUrl, EstablishBill.BillType);
                }
            }
            try
            {

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                //  path = Path.Combine(Server.MapPath("~" + MainUrl), FileName);

                path = Path.Combine(MainUrl, FileName);

#pragma warning disable CS0219 // The variable 'contentType' is assigned but its value is never used
                string contentType = "application/octet-stream";
#pragma warning restore CS0219 // The variable 'contentType' is assigned but its value is never used
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

            byte[] bytes = System.IO.File.ReadAllBytes(path);
            return File(bytes, "application/pdf");
        }

        public static string GetTR2DummyResult(List<mReimbursementBill> VoucherList)
        {



            StringBuilder sb = new StringBuilder();
            //  sb.Append("<br style='page-break-after: always;'/>");
            sb.Append("<table style='border:1px solid black;'> <thead> <tr>");
            sb.Append("<th style='border :1px solid black;'>Sr. No.</th> <th style='border :1px solid black;'>Name & Designation <br>1</th> <th style='border :1px solid black;'>Basic Pay <br>2</th> <th style='border :1px solid black;'>Special Par<br> 3</th>");
            sb.Append(" <th style='border :1px solid black;'>Dearness Allowance <br>4</th><th style='border :1px solid black;'>Compens Allowance<br> 5</th><th style='border :1px solid black;'>H.R.A. Allowance <br>6</th><th style='border :1px solid black;'>Capital Allowance<br>7</th>");
            sb.Append(" <th style='border :1px solid black;'>Convey Allowance<br>8</th><th style='border :1px solid black;'>Washing Allowance<br>9</th><th style='border :1px solid black;'>GP<br>10</th><th style='border :1px solid black;'>Sect. Pay <br> 11</th><th style='border :1px solid black;'>12</th><th style='border :1px solid black;'>13</th><th style='border :1px solid black;'>14</th>");
            sb.Append("<th style='border :1px solid black;'>Total 2 to 14<br> 15</th><th style='border :1px solid black;'>GPF Account No<br> 16</th>");

            sb.Append("<th style='border :1px solid black;'>GPF Subs<br>17</th><th style='border :1px solid black;'>GPF Advance<br>18</th><th style='border :1px solid black;'>HBA<br>19</th> <th style='border :1px solid black;'>HBA Interest<br>20</th> <th style='border :1px solid black;'>N. Car/ Scooter Advance<br>21</th>");
            sb.Append("<th style='border :1px solid black;'>M.C./ Scooter Interest<br>22</th><th style='border :1px solid black;'>Warm cloth Advance<br>23</th><th style='border :1px solid black;'>Warm cloth Interest<br>24</th> <th style='border :1px solid black;'>Festival Advance<br>25</th>");
            sb.Append("<th style='border :1px solid black;'>L.T.C. T.A. Advance<br>26</th> <th style='border :1px solid black;'>Miscel. Recov.<br>27</th> <th style='border :1px solid black;'>28</th> <th style='border :1px solid black;'>Total (A) 17 to 28 <br>29</th>");

            sb.Append("<th style='border :1px solid black;'>Saving Fund<br>30</th> <th style='border :1px solid black;'>Insur Fund<br>31</th> <th style='border :1px solid black;'>House Rent<br>32</th> <th style='border :1px solid black;'>P.L.I.<br>33</th> <th style='border :1px solid black;'>L.I.C.<br>34</th>");
            sb.Append("<th style='border :1px solid black;'>IncomeTax<br>35</th> <th style='border :1px solid black;'>Sur-charge<br>36</th> <th style='border :1px solid black;'>OtherBT<br>37</th> <th style='border :1px solid black;'>Total (B) 30 to 37<br>38</th> <th style='border :1px solid black;'>Total Deduction <br>39</th> <th style='border :1px solid black;'>Net Payment<br>40</th>");
            sb.Append("</tr> </thead>");


            sb.Append("<tbody>");

            int count = 1;
            decimal TotalGrossAmount = 0;
            if (VoucherList != null && VoucherList.Count() > 0)
            {
                foreach (var item in VoucherList)
                {
                    sb.Append("<tr>");
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", item.ClaimantName + " S.N. " + item.SactionNumber + " S. Date " + item.SactionDate.Value.ToString("dd/MM/yyyy")));

                    // DUES
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                    // Getting total of 2-9

                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", item.GrossAmount));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//16


                    // A.G. Deduction
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty)); //17
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//27
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//29

                    // Treasury Deduction
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", item.GrossAmount));
                    sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//40
                    sb.Append("</tr>");
                    count++;
                    TotalGrossAmount = TotalGrossAmount + item.GrossAmount;
                }


                sb.Append("<tr>");
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                sb.Append(string.Format("<td style='border :1px solid black;text-align:right;'><b>Grand Total </b></td>"));

                // DUES
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                // Getting total of 2-9

                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'><b>{0}</b></td>", TotalGrossAmount));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//16


                // A.G. Deduction
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty)); //17
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//27
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//29

                // Treasury Deduction
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'><b>Total:</b></td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", TotalGrossAmount));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//40
                sb.Append("</tr>");

                // this is final row
                sb.Append("<tr>");
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;text-align:right;'></td>"));

                // DUES
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                sb.Append(string.Format("<td style='border :1px solid black;'> </td>"));
                // Getting total of 2-9

                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));//16


                // A.G. Deduction
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty)); //17
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black; text-align:right;' colspan='13'>{0}</td>", NumberToWordsConverter.NumbersToWords(Convert.ToInt64(TotalGrossAmount))));//27
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append("</tr>");
            }
            else
            {
                sb.Append("<tr>");
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", count));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append(string.Format("<td style='border :1px solid black;'>{0}</td>", string.Empty));
                sb.Append("</tr>");




            }
            return sb.ToString();
        }

        #endregion


        #region  Encashment of Bill
        // this is for saving encashment date
        public PartialViewResult EncashmentDateIndex(int pageId = 1, int pageSize = 10)
        {
            try
            {

                //                var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => (m.Status == 2 || (m.Status == 1 && m.IsSanctioned == true)) && m.IsValidAuthority == false &&
                //m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();

                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;

                var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.Status == 2 || (m.Status == 1 && m.IsSanctioned == true)) && m.IsValidAuthority == false &&
m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();

                return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_EncashmentDateIndex.cshtml", BillList);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }
        /// <summary>
        /// this is for saving encashment date only single bill
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="pageId"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>

        [HttpPost]
        public PartialViewResult UpdateSingleEncashmentDate(int EstablishBillId, DateTime EncashmentDate, int pageId = 1, int pageSize = 10)
        {

            if (EncashmentDate.ToString("dd/MMM/yyyy") != "01/Jan/1753" && EncashmentDate.ToString("dd/MMM/yyyy") != "01/Jan/1900")
            {
                mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillId });
                EstablishBill.EncashmentDate = EncashmentDate;
                EstablishBill.Status = SBL.DomainModel.Models.Enums.BillStatus.Encashment.GetHashCode();
                EstablishBill.IsEncashment = true;
                Helper.ExecuteService("Budget", "UpdateEstablishBill", EstablishBill);

                DataSet dataSet = new DataSet();
                var Parameter = new List<KeyValuePair<string, string>>();
                Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", EstablishBill.EstablishBillID.ToString()));
                Parameter.Add(new KeyValuePair<string, string>("@Status", EstablishBill.Status.ToString()));
                Parameter.Add(new KeyValuePair<string, string>("@IsSanctioned", (EstablishBill.IsSanctioned ? "1" : "0")));
                Parameter.Add(new KeyValuePair<string, string>("@IsRejected", (EstablishBill.IsReject ? "1" : "0")));
                Parameter.Add(new KeyValuePair<string, string>("@IsSOApproved", EstablishBill.IsSOApproved.ToString()));
                Parameter.Add(new KeyValuePair<string, string>("@IsChallanGenerate", (EstablishBill.IsSanctioned ? "1" : "0")));
                Parameter.Add(new KeyValuePair<string, string>("@IsEncashment", "1"));
                Parameter.Add(new KeyValuePair<string, string>("@EncashmentDate", Convert.ToDateTime(EstablishBill.EncashmentDate).ToShortDateString()));

                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateStatusOfReimbursementBill", Parameter);

                // this is for update encashment in monthly salry
                if (EstablishBill != null && EstablishBill.BillType == 7)
                {
                    DataSet dataSet1 = new DataSet();
                    var Parameter1 = new List<KeyValuePair<string, string>>();
                    Parameter1.Add(new KeyValuePair<string, string>("@EstablishmentId", EstablishBill.EstablishBillID.ToString()));
                    Parameter1.Add(new KeyValuePair<string, string>("@Encashmentdate", Convert.ToDateTime(EstablishBill.EncashmentDate).ToShortDateString()));

                    dataSet1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateStatusOfMonthlySalary", Parameter1);
                }

                Thread thread = new Thread(() => SendEmailToMember(EstablishBillId.ToString()));
                thread.Start();
                thread.Join();
            }
            else
            {
                ViewBag.Msg = "Please Select Encashement Date";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }

            //            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => (m.Status == 2 || (m.Status == 1 && m.IsSanctioned == true)) && m.IsValidAuthority == false &&
            //m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.Status == 2 || (m.Status == 1 && m.IsSanctioned == true)) && m.IsValidAuthority == false &&
m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();

            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_EncashementDateList.cshtml", BillList);
        }

        [HttpPost]
        public PartialViewResult SaveAllEncashmentDate(FormCollection collection, int pageId = 1, int pageSize = 10)
        {
            StringBuilder EstablishIds = new StringBuilder();
            string[] RowCount = collection["RowCount"].Split(',');

            foreach (var item in RowCount)
            {
                EstablishIds.Append(collection["EstablishBillID-" + item] + ",");
                int EstablishBillId = int.Parse(collection["EstablishBillID-" + item]);
                string EncashmentDateS = collection["EncashmentDate-" + item];
                if (!string.IsNullOrEmpty(EncashmentDateS))
                {
                    DateTime EncashmentDate = new DateTime();
                    DateTime.TryParse(EncashmentDateS, out EncashmentDate);

                    if (EncashmentDate.ToString("dd/MMM/yyyy") != "01/Jan/0001" && EncashmentDate.ToString("dd/MMM/yyyy") != "01/Jan/1753" && EncashmentDate.ToString("dd/MMM/yyyy") != "01/Jan/1900")
                    {
                        mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillId });
                        EstablishBill.EncashmentDate = EncashmentDate;
                        EstablishBill.IsEncashment = true;
                        EstablishBill.Status = SBL.DomainModel.Models.Enums.BillStatus.Encashment.GetHashCode();
                        //   EstablishBill.IsEncashementInfoUpdate = true;
                        Helper.ExecuteService("Budget", "UpdateEstablishBill", EstablishBill);

                        DataSet dataSet = new DataSet();
                        var Parameter = new List<KeyValuePair<string, string>>();
                        Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", EstablishBill.EstablishBillID.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@Status", EstablishBill.Status.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@IsSanctioned", (EstablishBill.IsSanctioned ? "1" : "0")));
                        Parameter.Add(new KeyValuePair<string, string>("@IsRejected", (EstablishBill.IsReject ? "1" : "0")));
                        Parameter.Add(new KeyValuePair<string, string>("@IsSOApproved", EstablishBill.IsSOApproved.ToString()));
                        Parameter.Add(new KeyValuePair<string, string>("@IsChallanGenerate", (EstablishBill.IsSanctioned ? "1" : "0")));
                        Parameter.Add(new KeyValuePair<string, string>("@IsEncashment", "1"));
                        Parameter.Add(new KeyValuePair<string, string>("@EncashmentDate", Convert.ToDateTime(EstablishBill.EncashmentDate).ToShortDateString()));

                        dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateStatusOfReimbursementBill", Parameter);


                        // this is for update encashment in monthly salry
                        if (EstablishBill != null && EstablishBill.BillType == 7)
                        {
                            DataSet dataSet1 = new DataSet();
                            var Parameter1 = new List<KeyValuePair<string, string>>();
                            Parameter1.Add(new KeyValuePair<string, string>("@EstablishmentId", EstablishBill.EstablishBillID.ToString()));
                            Parameter1.Add(new KeyValuePair<string, string>("@Encashmentdate", Convert.ToDateTime(EstablishBill.EncashmentDate).ToShortDateString()));

                            dataSet1 = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateStatusOfMonthlySalary", Parameter1);
                        }

                    }

                    ViewBag.Msg = "Sucessfully   updated encashment date";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }



            }

            Thread thread = new Thread(() => SendEmailToMember(EstablishIds.ToString()));
            thread.Start();
            thread.Join();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.Status == 2 || (m.Status == 1 && m.IsSanctioned == true)) && m.IsValidAuthority == false &&
m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();


            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_EncashementDateList.cshtml", BillList);
        }

        public PartialViewResult GeEncashmentBillByPaging(int pageId = 1, int pageSize = 10)
        {

            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;

            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.Status == 2 || (m.Status == 1 && m.IsSanctioned == true)) && m.IsValidAuthority == false &&
m.IsCanceled == false).ToList();


            ViewBag.PageSize = pageSize;
            List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_EncashmentBillList.cshtml", pagedRecord);
        }

        // this  is for Approve Encashment Bill after Saving Encashment Date
        //public PartialViewResult ApproveEncashmentBills(int pageId = 1, int pageSize = 10)
        //{
        //    try
        //    {


        //        var BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", null)).Where(m => m.IsEncashementInfoUpdate == true && m.IsSanctioned == true && (m.Status == 2 || m.Status == 1)).ToList();

        //        ViewBag.PageSize = pageSize;
        //        List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
        //        if (pageId == 1)
        //        {
        //            pagedRecord = BillList.Take(pageSize).ToList();
        //        }
        //        else
        //        {
        //            int r = (pageId - 1) * pageSize;
        //            pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
        //        }

        //        ViewBag.CurrentPage = pageId;
        //        ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);


        //        if (TempData["Msg"] != null)
        //            ViewBag.Msg = TempData["Msg"].ToString();

        //        var model = pagedRecord.ToViewModel();

        //        return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_ApproveEncashmentBill.cshtml", model);
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
        //        return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
        //        throw ex;
        //    }
        //}

        //[HttpPost]
        //public PartialViewResult ApproveEncashmentBill(FormCollection collection, int pageId = 1, int pageSize = 10)
        //{


        //    string[] RowCount = collection["RowCount"].Split(',');

        //    foreach (var item in RowCount)
        //    {
        //        int ReimbursementBillID = int.Parse(collection["ReimbursementBillID-" + item]);
        //        string Approved = collection["Encashment-" + item];

        //        mReimbursementBill stateToEdit = (mReimbursementBill)Helper.ExecuteService("Budget", "GetReimbursementBillById", new mReimbursementBill { ReimbursementBillID = ReimbursementBillID });

        //        if (Approved == "1")
        //        {
        //            stateToEdit.IsSetEncashment = true;
        //            stateToEdit.Status = SBL.DomainModel.Models.Enums.BillStatus.Encashment.GetHashCode();

        //            List<string> mobileList = new List<string>();
        //            // mobileList.Add(stateToEdit.Mobile);
        //            mobileList.Add("8588031303");

        //            List<string> EmailList = new List<string>();
        //            // EmailList.Add(stateToEdit.Email);
        //            EmailList.Add("shubham@sblsoftware.com");

        //            string msgBody = "Dear Member,<br> your bill is encashment.";
        //            SendEmailDetails(EmailList, mobileList, msgBody, "Bill Status", msgBody, true, true, string.Empty);
        //        }
        //        else if (Approved == "0")
        //            stateToEdit.IsSetEncashment = false;

        //        Helper.ExecuteService("Budget", "UpdateReimbursementBill", stateToEdit);
        //        ViewBag.Msg = "Sucessfully   Encashment";
        //        ViewBag.Class = "alert alert-info";
        //        ViewBag.Notification = "Success";
        //    }


        //    var BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", null)).Where(m => m.IsSanctioned == true && m.IsEncashementInfoUpdate == true && (m.Status == 2 || m.Status == 1)).ToList();

        //    ViewBag.PageSize = pageSize;
        //    List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
        //    if (pageId == 1)
        //    {
        //        pagedRecord = BillList.Take(pageSize).ToList();
        //    }
        //    else
        //    {
        //        int r = (pageId - 1) * pageSize;
        //        pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
        //    }

        //    ViewBag.CurrentPage = pageId;
        //    ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

        //    var modelList = pagedRecord.ToViewModel();

        //    return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_AppEncashmentList.cshtml", modelList);
        //}

        ///// <summary>
        ///// this is for approving encashment date of single bill
        ///// </summary>
        ///// <param name="collection"></param>
        ///// <param name="pageId"></param>
        ///// <param name="pageSize"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public PartialViewResult ApproveSingleEncashmentBill(int ReimbursementBillID, string Approved, int pageId = 1, int pageSize = 10)
        //{


        //    mReimbursementBill stateToEdit = (mReimbursementBill)Helper.ExecuteService("Budget", "GetReimbursementBillById", new mReimbursementBill { ReimbursementBillID = ReimbursementBillID });

        //    if (Approved == "1")
        //    {
        //        stateToEdit.IsSetEncashment = true;
        //        stateToEdit.Status = SBL.DomainModel.Models.Enums.BillStatus.Encashment.GetHashCode();

        //        List<string> mobileList = new List<string>();
        //        // mobileList.Add(stateToEdit.Mobile);
        //        mobileList.Add("8588031303");

        //        List<string> EmailList = new List<string>();
        //        // EmailList.Add(stateToEdit.Email);
        //        EmailList.Add("shubham@sblsoftware.com");

        //        string msgBody = "Dear Member,<br> your bill is encashment.";
        //        SendEmailDetails(EmailList, mobileList, msgBody, "Bill Status", msgBody, true, true, string.Empty);
        //    }
        //    else if (Approved == "0")
        //        stateToEdit.IsSetEncashment = false;

        //    Helper.ExecuteService("Budget", "UpdateReimbursementBill", stateToEdit);
        //    ViewBag.Msg = "Sucessfully   Encashment";
        //    ViewBag.Class = "alert alert-info";
        //    ViewBag.Notification = "Success";



        //    var BillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill", null)).Where(m => m.IsSanctioned == true && m.IsEncashementInfoUpdate == true && (m.Status == 2 || m.Status == 1)).ToList();

        //    ViewBag.PageSize = pageSize;
        //    List<mReimbursementBill> pagedRecord = new List<mReimbursementBill>();
        //    if (pageId == 1)
        //    {
        //        pagedRecord = BillList.Take(pageSize).ToList();
        //    }
        //    else
        //    {
        //        int r = (pageId - 1) * pageSize;
        //        pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
        //    }

        //    ViewBag.CurrentPage = pageId;
        //    ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

        //    var modelList = pagedRecord.ToViewModel();

        //    return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_AppEncashmentList.cshtml", modelList);
        //}

        public void SendEmailToMember(string EstablishId)
        {
            Thread.Sleep(100);
            List<mReimbursementBill> BillList = (List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBillByMultipleEstablishId", EstablishId);
            //DataSet dataSet = new DataSet();
            //var Parameter = new List<KeyValuePair<string, string>>();
            //Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", EstablishId));
            //dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetMemberDetailByEstablishId", Parameter);
            if (BillList != null && BillList.Count() > 0)
            {
                List<string> mobileList = new List<string>();
                List<string> EmailList = new List<string>();
                foreach (var item in BillList)
                {
                    mobileList.Add(item.Mobile);
                    //mobileList.Add("8588031303");
                    //       mobileList.Add("9736305930");
                    //mobileList.Add("9418352977");

                    EmailList.Add(item.Email);
                    //EmailList.Add("shubham@sblsoftware.com");
                    //      EmailList.Add("pallavisood49@gmail.com");
                    //EmailList.Add("lakshay.verma18@gmail.com");

                    string ClaimaintName = string.Empty;
                    if (item.Designation == 0 || item.Designation == 1 || item.Designation == 2 || item.Designation == 3)
                        ClaimaintName = string.Format("Respected Hon'ble Member {0}", item.ClaimantName);
                    else if (item.Designation == 6)
                        ClaimaintName = string.Format(" {0}", item.ClaimantName);
                    else
                        ClaimaintName = string.Format("Sh./Smt. {0}", item.ClaimantName);

                    string msgBody = string.Format("{0}, your {1} No. -{2}, Dated : {3} of amount Rs. {4} is encashed of amount Rs. {5}  Dated: {6}. \n Secretary HP Vidhan Sabha", ClaimaintName, item.BillTypeName, item.EstblishBillNumber, item.DateOfPresentation.Value.ToString("dd/MM/yyyy"), item.SactionAmount, item.SactionAmount, item.EncashmentDate.Value.ToString("dd/MM/yyyy"));
                    //  string msgBody = string.Format("{0} your {1} {2} Dated {3} of amount Rs. {4} is in process. /n Secratary , /n HP Vidhan Sabha", ClaimaintName, item.BillTypeName, item.EstblishBillNumber, item.DateOfPresentation.Value.ToString("dd/MM/yyyy"), item.SactionAmount);

                    SendEmailDetails(EmailList, mobileList, msgBody, "Bill Status", msgBody, true, true, string.Empty);
                    mobileList.Clear();
                    EmailList.Clear();
                }
                //for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                //{
                //    if (!string.IsNullOrEmpty(dataSet.Tables[0].Rows[i][0].ToString()) && ValidationAtServer.CheckMobileNumber(dataSet.Tables[0].Rows[i][0].ToString()))
                //        mobileList.Add(dataSet.Tables[0].Rows[i][0].ToString());

                //    //mobileList.Add("8588031303");
                //    if (!string.IsNullOrEmpty(dataSet.Tables[0].Rows[i][1].ToString()))
                //        EmailList.Add(dataSet.Tables[0].Rows[i][1].ToString());
                //    //EmailList.Add("shubham@sblsoftware.com");
                //}


                //  string msgBody = string.Format("Respected Hon’ble Member your {0} {1} Dated {2} of amount Rs. {3} is in process. /n Secratary , /n Vidhan Sabha", stateToEdit.BillTypeName, stateToEdit.EstblishBillNumber, stateToEdit.DateOfPresentation.Value.ToString("dd/MM/yyyy"), stateToEdit.SactionAmount);
                //  string msgBody = "Dear Member,<br> your bill is encashed.";
                // SendEmailDetails(EmailList, mobileList, msgBody, "Bill Status", msgBody, true, true, string.Empty);
            }


        }


        [HttpPost]
        public PartialViewResult CancelEncashmentDate(int EstablishBillId, int pageId = 1, int pageSize = 10)
        {
            var FileLocationModel = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "FileLocation" });
            string FileLocation = FileLocationModel.SettingValue;

            mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillId });
            EstablishBill.EncashmentDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            EstablishBill.Status = SBL.DomainModel.Models.Enums.BillStatus.BillInProcess.GetHashCode();
            EstablishBill.IsEncashment = false;
            EstablishBill.IsSOApproved = false;
            EstablishBill.IsReject = true;
            EstablishBill.RejectedBy = "Cashier";
            EstablishBill.IsChallanGenerate = false;
            EstablishBill.IsBillUpdate = false;
            EstablishBill.IsSanctioned = false;

            if (!string.IsNullOrEmpty(EstablishBill.PDFUrl))
            {
                string url = FileLocation + "/BillChallan/";
                string directory = url + EstablishBill.PDFUrl;
                System.IO.File.Delete(directory);
                EstablishBill.PDFUrl = string.Empty;
            }

            Helper.ExecuteService("Budget", "UpdateEstablishBill", EstablishBill);

            DataSet dataSet = new DataSet();
            var Parameter = new List<KeyValuePair<string, string>>();
            Parameter.Add(new KeyValuePair<string, string>("@EstablishBillId", EstablishBill.EstablishBillID.ToString()));
            Parameter.Add(new KeyValuePair<string, string>("@Status", EstablishBill.Status.ToString()));
            Parameter.Add(new KeyValuePair<string, string>("@IsSanctioned", (EstablishBill.IsSanctioned ? "1" : "0")));
            Parameter.Add(new KeyValuePair<string, string>("@IsRejected", (EstablishBill.IsReject ? "1" : "0")));
            Parameter.Add(new KeyValuePair<string, string>("@IsSOApproved", EstablishBill.IsSOApproved.ToString()));
            Parameter.Add(new KeyValuePair<string, string>("@IsChallanGenerate", (EstablishBill.IsSanctioned ? "1" : "0")));
            Parameter.Add(new KeyValuePair<string, string>("@IsEncashment", "0"));
            Parameter.Add(new KeyValuePair<string, string>("@EncashmentDate", Convert.ToDateTime(EstablishBill.EncashmentDate).ToShortDateString()));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "UpdateStatusOfReimbursementBill", Parameter);





            //            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", null)).Where(m => (m.Status == 2 || (m.Status == 1 && m.IsSanctioned == true)) && m.IsValidAuthority == false &&
            //m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;


            var BillList = ((List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBill", new mEstablishBill { FinancialYear = FinYearValue })).Where(m => (m.Status == 2 || (m.Status == 1 && m.IsSanctioned == true)) && m.IsValidAuthority == false &&
m.IsCanceled == false).OrderBy(m => m.EstablishBillID).ToList();
            //ViewBag.PageSize = pageSize;
            //List<mEstablishBill> pagedRecord = new List<mEstablishBill>();
            //if (pageId == 1)
            //{
            //    pagedRecord = BillList.Take(pageSize).ToList();
            //}
            //else
            //{
            //    int r = (pageId - 1) * pageSize;
            //    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            //}

            //ViewBag.CurrentPage = pageId;
            //ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);


            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_EncashementDateList.cshtml", BillList);
        }

        #endregion


        #region Comments of Bill


        public PartialViewResult GetComments(string ParantName, int ParentId, int RowNumber)
        {
            EstablishBillViewModel model = new EstablishBillViewModel();
            model.mEstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = ParentId });
            model.Comments = (List<mComments>)Helper.ExecuteService("Budget", "GetAllComments", new mComments { ParentName = ParantName, ParentId = ParentId });
            ViewBag.RowNumber = RowNumber;
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_CommentPartialView.cshtml", model);
        }

        public PartialViewResult GetCommentsForSanction(string ParantName, int ParentId, int RowNumber)
        {
            EstablishBillViewModel model = new EstablishBillViewModel();
            model.mEstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = ParentId });
            model.Comments = (List<mComments>)Helper.ExecuteService("Budget", "GetAllComments", new mComments { ParentName = ParantName, ParentId = ParentId });
            ViewBag.RowNumber = RowNumber;
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_CommentPartialViewForSanction.cshtml", model);
        }

        public PartialViewResult GetLayoutComments(string ParantName, int ParentId, int RowNumber)
        {
            EstablishBillViewModel model = new EstablishBillViewModel();
            model.mEstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = ParentId });
            model.Comments = (List<mComments>)Helper.ExecuteService("Budget", "GetAllComments", new mComments { ParentName = ParantName, ParentId = ParentId });
            ViewBag.RowNumber = RowNumber;
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_LayoutCommentsPartialView.cshtml", model);
        }
        [HttpPost]
        public PartialViewResult SaveComents(string Comments, string ParantName, int ParentId, int RowNumber)
        {
            mComments obj = new mComments();
            obj.Comments = Comments;
            obj.ParentId = ParentId;
            obj.ParentName = ParantName;
            obj.UserName = CurrentSession.UserName;
            Helper.ExecuteService("Budget", "CreateComments", obj);
            EstablishBillViewModel model = new EstablishBillViewModel();
            model.mEstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = ParentId });
            model.Comments = (List<mComments>)Helper.ExecuteService("Budget", "GetAllComments", new mComments { ParentName = ParantName, ParentId = ParentId });
            ViewBag.RowNumber = RowNumber;
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_CommentPartialView.cshtml", model);
        }

        [HttpPost]
        public PartialViewResult SaveComentsForSanction(string Comments, string ParantName, int ParentId, int RowNumber)
        {
            mComments obj = new mComments();
            obj.Comments = Comments;
            obj.ParentId = ParentId;
            obj.ParentName = ParantName;
            obj.UserName = CurrentSession.UserName;
            Helper.ExecuteService("Budget", "CreateComments", obj);

            EstablishBillViewModel model = new EstablishBillViewModel();
            model.mEstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = ParentId });
            model.Comments = (List<mComments>)Helper.ExecuteService("Budget", "GetAllComments", new mComments { ParentName = ParantName, ParentId = ParentId });
            ViewBag.RowNumber = RowNumber;
            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_CommentPartialViewForSanction.cshtml", model);
        }


        #endregion


        #region Expense Report

        public PartialViewResult ExpenseBillReport(int pageId = 1, int pageSize = 10)
        {
            try
            {


                string url = "/ExpenseReport/";

                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] filePaths = Directory.GetFiles(directory);
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }



                mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
                string FinYearValue = SiteSettingsSessn.SettingValue;

                var State = ((List<mBudget>)Helper.ExecuteService("Budget", "GetBudgetByFY", FinYearValue)).OrderBy(m => m.OrderBy).ToList();
                ViewBag.BudgetList = new SelectList(State, "BudgetID", "BudgetName");

                // For populate FInacial Year
                ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
                ViewBag.FinYear = FinYearValue;

                return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_ExpenseBillReport.cshtml");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }


        public PartialViewResult GetBudgetByFY(string FinacialYear)
        {
            //DateTime startDate = new DateTime(Convert.ToInt32(FinacialYear.Split('-')[0]), 4, 30);
            //DateTime endDate = new DateTime(Convert.ToInt32(FinacialYear.Split('-')[1]), 3, 31);

            //StringBuilder sb = new StringBuilder();

            //sb.Append(startDate.ToShortDateString() + ",");
            //sb.Append(endDate.ToShortDateString());

            var State = (List<mBudget>)Helper.ExecuteService("Budget", "GetBudgetByFY", FinacialYear);

            ViewBag.BudgetList = new SelectList(State, "BudgetID", "BudgetName");
            return PartialView("/Areas/AccountsAdmin/Views/ReimbursementBill/_BudgetDDL.cshtml");
        }


        public JsonResult GetExpenseReport(string BudgetIds, string FinacialYear, string Duration, string Quarterly, string month)
        {
            StringBuilder sb = new StringBuilder();
            string Result = string.Empty;
            if (Duration == "Monthly")
                Result = GetExpenseReportMonthlyWise(BudgetIds, FinacialYear, Duration, int.Parse(month));
            else if (Duration == "Yearly")
                Result = GetExpenseReportYearlyWise(BudgetIds, FinacialYear, Duration);
            else
                Result = GetExpenseReportQuartleyWise(BudgetIds, FinacialYear, Duration, Quarterly);

            string Filename = CreteExpenseReportPdf(Result);
            sb.Append(Result);

            sb.Append(string.Format("<input type='hidden' name='FileName' id='FileName' value='{0}' />", Filename));

            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }


        public static string GetExpenseReportMonthlyWise(string BudgetIds, string FinacialYear, string Duration, int month)
        {
            string[] FinalcialYearArray = FinacialYear.Split('-');
            string PreYear = FinalcialYearArray[0];
            string CurYear = FinalcialYearArray[1];
            // this is for showing month with year
            string MonthWithYear = string.Empty;
            DateTime FirstDateOfMonth = new DateTime();
            DateTime LastDateOfMonth = new DateTime();
            DateTime FirstDateOfPreFirstMonth = new DateTime();
            DateTime LastDateOfPreLastMonth = new DateTime();
            if (month == 1 || month == 2 || month == 3)
            {
                MonthWithYear = string.Format("{0}/{1}", month, CurYear);
                FirstDateOfMonth = GetFirstDayOfMonth(month, int.Parse(CurYear));
                LastDateOfMonth = GetLastDayOfMonth(month, int.Parse(CurYear));
            }
            else
            {

                MonthWithYear = string.Format("{0}/{1}", month, PreYear);
                FirstDateOfMonth = GetFirstDayOfMonth(month, int.Parse(PreYear));
                LastDateOfMonth = GetLastDayOfMonth(month, int.Parse(PreYear));

            }

            //this is for getting remaining privious month Date
            FirstDateOfPreFirstMonth = new DateTime(int.Parse(PreYear), 4, 1);
            LastDateOfPreLastMonth = LastDateOfMonth.AddMonths(-1);
            StringBuilder Result = new StringBuilder();

            string[] Budgets = BudgetIds.Split(',');

            // Crete Html Template for Authority Letter
            Result.Append("<body style='width:900px;margin:auto;'><section><header style='text-align:center;'>");
            Result.Append("<table style='width: 100%;border-collapse:collapse'>  <tr> <td style='vertical-align: middle; text-align: center'>");
            Result.Append("<h2 style='text-align:center;font-weight:bold'>ESTABLISHMENT OF H.P. VIDHAN SABHA</h2><header>");

            if (Budgets != null && Budgets.Count() > 0)
            {

                Result.Append("<div><table style='width:100%;border: 1px solid ##CCCCCC;border-collapse: collapse;color:black; '   border='1px'> ");
                Result.Append(string.Format("<tr> <td style='width: 100px;text-align:left;' colspan='4'>Monthly Expenditure Statement for the month of {0}</td>  </tr> ", MonthWithYear));
                Result.Append("<tr>   <td style='text-align:left;' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                foreach (var item in Budgets)
                {
                    int BudgetId = int.Parse(item);
                    StringBuilder BillParam = new StringBuilder();
                    BillParam.Append(BudgetId + ",");
                    BillParam.Append(FirstDateOfMonth.ToShortDateString() + ",");
                    BillParam.Append(LastDateOfMonth.ToShortDateString() + ",");
                    // this is for getting list of 
                    List<mEstablishBill> BillList = (List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBillForExpense", BillParam.ToString());
                    //if (BillList != null && BillList.Count() > 0)
                    //{
                    mBudget BudgetObj = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = BudgetId });
                    AllocateBudget AllocateSactionBud = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = BudgetId });
                    decimal TotalAdditonlFund = (decimal)Helper.ExecuteService("Budget", "GetTotalAdditionById", new AdditionFunds { BudgetId = BudgetId });
                    decimal TotalFund = 0;
                    TotalFund = AllocateSactionBud.Sanctionedbudget + TotalAdditonlFund;
                    if (TotalFund > 0)
                    {
                        // string TotalFundS = string.Format("{0:n0}", TotalFund);

                        Result.Append(string.Format("<tr>   <td style='text-align:left; ' colspan='4'>Account Head : {0}</td> </tr>", BudgetObj.BudgetName));
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                        Result.Append(string.Format("<tr>   <td style='text-align:left; ' colspan='4'>Allocate Budget : {0}</td> </tr>", TotalFund));
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");



                        Result.Append("<tr>   <td style='text-align:left;font-weight:bold' >Bill Number</td>");
                        Result.Append("<td style='text-align:left;font-weight:bold' >Expenditure Amount</td>  ");
                        Result.Append("<td style='text-align:left;font-weight:bold' >Encashment Date</td> ");
                        Result.Append("<td style='text-align:left;font-weight:bold' >Remarks</td> </tr>");

                        decimal TotalBillExpense = 0;
                        int BillType = 0;
                        foreach (var obj in BillList)
                        {
                            if (obj.IsDebitReceiptExpense)
                            {


                                Result.Append("<tr>");
                                Result.Append(string.Format("<td style='text-align:left; ' >{0} - {1}</td>", "Debit Receipt ", obj.DebitReceiptRemarks));
                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", obj.TotalAmount));
                                BillType = obj.BillType;
                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", Convert.ToDateTime(obj.EncashmentDate).ToString("dd/MM/yyyy")));
                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", string.Empty));
                                Result.Append("</tr>");
                                TotalBillExpense = TotalBillExpense + obj.TotalAmount;
                            }
                            else
                            {
                                //DataSet dataSet = new DataSet();
                                //var EstablishBillIds = new List<KeyValuePair<string, string>>();
                                //EstablishBillIds.Add(new KeyValuePair<string, string>("@EstablishBillId", obj.EstablishBillID.ToString()));

                                //dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillIds);
                                //decimal GrossAmount = Convert.ToDecimal(dataSet.Tables[0].Rows[0][1]);
                                //string BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][1]); ;

                                Result.Append("<tr>");

                                Result.Append(string.Format("<td style='text-align:left; ' >{0} - {1}</td>", obj.BillNumber, FinacialYear));

                                if (obj.BillType == 7)
                                    Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", obj.TotalGrossAmount));
                                else
                                    Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", obj.TotalAmount));
                                BillType = obj.BillType;
                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", Convert.ToDateTime(obj.EncashmentDate).ToString("dd/MM/yyyy")));
                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", string.Empty));
                                Result.Append("</tr>");
                                if (obj.BillType == 7)
                                    TotalBillExpense = TotalBillExpense + obj.TotalGrossAmount;
                                else
                                    TotalBillExpense = TotalBillExpense + obj.TotalAmount;
                            }

                        }
                        // string TotalBillExpenseS = string.Format("{0:n0}", TotalBillExpense);
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Total</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", TotalBillExpense));
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Receipt Expenditure during Month</td>"));
                        Result.Append(string.Format("<td style='text-align:left;left' >0 </td>"));
                        Result.Append("</tr>");

                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                        // Calculate Last all month expense
                        decimal LasAllMonthExpense = 0;
                        if (month != 4)
                        {
                            StringBuilder PreBillParam = new StringBuilder();
                            PreBillParam.Append(BudgetId + ",");
                            PreBillParam.Append(FirstDateOfPreFirstMonth.ToShortDateString() + ",");
                            PreBillParam.Append(LastDateOfPreLastMonth.ToShortDateString() + ",");

                            if (BillType == 7)
                                LasAllMonthExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForExpenseReportForSalary", PreBillParam.ToString());
                            else
                                LasAllMonthExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForExpenseReport", PreBillParam.ToString());
                        }
                        //  string LasAllMonthExpenseS = string.Format("{0:n0}", LasAllMonthExpense);
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Expenditure during the month</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", TotalBillExpense));
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Privious months Expenditure</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0} </td>", LasAllMonthExpense));
                        Result.Append("</tr>");

                        decimal TotalExpendituretilldate = LasAllMonthExpense + TotalBillExpense;

                        decimal Balance = TotalFund - TotalExpendituretilldate;
                        string BalanceS = Balance.ToString();// string.Format("{0:n0}", Balance);
                        string TotalExpendituretilldateS = TotalExpendituretilldate.ToString();// string.Format("{0:n0}", TotalExpendituretilldate);
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Total Expenditure till date</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", TotalExpendituretilldate));
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Balance</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0} </td>", Balance));
                        Result.Append("</tr>");
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");

                        TotalBillExpense = 0;
                        LasAllMonthExpense = 0;
                    }
                    // }
                }
                Result.Append("</table>");
                Result.Append("</div>");
            }
            Result.Append(" <br stylr='page-break-after: always;'>");
            string SummaryReport = GetSummaryReportMonthlyWise(FinacialYear, month);
            Result.Append(SummaryReport);
            Result.Append("</body>");
            return Result.ToString();
        }


        public static string GetExpenseReportQuartleyWise(string BudgetIds, string FinacialYear, string Duration, string Quartely)
        {
            string[] FinalcialYearArray = FinacialYear.Split('-');
            string[] QuartelyArray = Quartely.Split('-');
            string PreYear = FinalcialYearArray[0];
            string CurYear = FinalcialYearArray[1];
            int FirstMonth = int.Parse(QuartelyArray[0]);
            int LastMonth = int.Parse(QuartelyArray[1]);
            // this is for showing month with year
            string MonthWithYear = string.Empty;
            DateTime FirstDateOfMonth = new DateTime();
            DateTime LastDateOfMonth = new DateTime();
            DateTime FirstDateOfPreFirstMonth = new DateTime();
            DateTime LastDateOfPreLastMonth = new DateTime();
            if (FirstMonth == 1 || FirstMonth == 2 || FirstMonth == 3)
            {
                MonthWithYear = string.Format("{0}/{1}", Quartely, CurYear);
                FirstDateOfMonth = GetFirstDayOfMonth(FirstMonth, int.Parse(CurYear));
                LastDateOfMonth = GetLastDayOfMonth(LastMonth, int.Parse(CurYear));
            }
            else
            {

                MonthWithYear = string.Format("{0}/{1}", Quartely, PreYear);
                FirstDateOfMonth = GetFirstDayOfMonth(FirstMonth, int.Parse(PreYear));
                LastDateOfMonth = GetLastDayOfMonth(LastMonth, int.Parse(PreYear));

            }

            //this is for getting remaining privious month Date
            FirstDateOfPreFirstMonth = new DateTime(int.Parse(PreYear), 4, 1);
            LastDateOfPreLastMonth = GetLastDayOfMonth(FirstDateOfMonth.Month, FirstDateOfMonth.Year).AddMonths(-1);
            StringBuilder Result = new StringBuilder();

            string[] Budgets = BudgetIds.Split(',');

            // Crete Html Template for Authority Letter
            Result.Append("<body style='width:900px;margin:auto;'><section><header style='text-align:center;'>");
            Result.Append("<table style='width: 100%;'>  <tr> <td style='vertical-align: middle; text-align: center'>");
            Result.Append("<h2 style='text-align:center;font-weight:bold'>ESTABLISHMENT OF H.P. VIDHAN SABHA</h2><header>");

            if (Budgets != null && Budgets.Count() > 0)
            {

                Result.Append("<div><table style='width:100%;border: 1px solid black; border-collapse:collapse'   border='1px'> ");
                Result.Append(string.Format("<tr> <td style='width: 100px;text-align:left;' colspan='4'>Monthly Expenditure Statement for the month of {0}</td>  </tr> ", MonthWithYear));
                Result.Append("<tr>   <td style='text-align:left;' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                foreach (var item in Budgets)
                {
                    int BudgetId = int.Parse(item);
                    StringBuilder BillParam = new StringBuilder();
                    BillParam.Append(BudgetId + ",");
                    BillParam.Append(FirstDateOfMonth.ToShortDateString() + ",");
                    BillParam.Append(LastDateOfMonth.ToShortDateString() + ",");
                    // this is for getting list of 
                    List<mEstablishBill> BillList = (List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBillForExpense", BillParam.ToString());
                    // if (BillList != null && BillList.Count() > 0)
                    //{
                    mBudget BudgetObj = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = BudgetId });
                    AllocateBudget AllocateSactionBud = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = BudgetId });
                    decimal TotalAdditonlFund = (decimal)Helper.ExecuteService("Budget", "GetTotalAdditionById", new AdditionFunds { BudgetId = BudgetId });
                    decimal TotalFund = 0;
                    TotalFund = AllocateSactionBud.Sanctionedbudget + TotalAdditonlFund;
                    if (TotalFund > 0)
                    {
                        //   string TotalFundS = string.Format("{0:n0}", TotalFund);

                        Result.Append(string.Format("<tr>   <td style='text-align:left; ' colspan='4'>Account Head : {0}</td> </tr>", BudgetObj.BudgetName));
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                        Result.Append(string.Format("<tr>   <td style='text-align:left; ' colspan='4'>Allocate Budget : {0}</td> </tr>", TotalFund));
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");



                        Result.Append("<tr>   <td style='text-align:left;font-weight:bold' >Bill Number</td>");
                        Result.Append("<td style='text-align:left;font-weight:bold' >Expenditure Amount</td>  ");
                        Result.Append("<td style='text-align:left;font-weight:bold' >Encashment Date</td> ");
                        Result.Append("<td style='text-align:left;font-weight:bold' >Remarks</td> </tr>");
                        int BillType = 0;
                        decimal TotalBillExpense = 0;
                        foreach (var obj in BillList)
                        {
                            //DataSet dataSet = new DataSet();
                            //var EstablishBillIds = new List<KeyValuePair<string, string>>();
                            //EstablishBillIds.Add(new KeyValuePair<string, string>("@EstablishBillId", obj.EstablishBillID.ToString()));

                            //dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillIds);

                            //decimal GrossAmount = 0;
                            //decimal.TryParse(dataSet.Tables[0].Rows[0][1].ToString(), out GrossAmount);
                            //// Convert.ToDecimal(dataSet.Tables[0].Rows[0][1]);
                            //string BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][1]); ;
                            // string BillDeduction = string.Format("{0:n0}", dataSet.Tables[0].Rows[0][2]);
                            if (obj.IsDebitReceiptExpense)
                            {
                                Result.Append("<tr>");
                                Result.Append(string.Format("<td style='text-align:left; ' >{0} - {1}</td>", "Debit Receipt", obj.DebitReceiptRemarks));
                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", obj.TotalAmount));
                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", Convert.ToDateTime(obj.EncashmentDate).ToString("dd/MM/yyyy")));
                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", string.Empty));
                                Result.Append("</tr>");
                                TotalBillExpense = TotalBillExpense + obj.TotalAmount;
                            }
                            else
                            {
                                Result.Append("<tr>");
                                Result.Append(string.Format("<td style='text-align:left; ' >{0} - {1}</td>", obj.BillNumber, FinacialYear));
                                if (obj.BillType == 7)
                                    Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", obj.TotalGrossAmount));
                                else
                                    Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", obj.TotalAmount));
                                BillType = obj.BillType;
                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", Convert.ToDateTime(obj.EncashmentDate).ToString("dd/MM/yyyy")));
                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", string.Empty));
                                Result.Append("</tr>");
                                if (obj.BillType == 7)
                                    TotalBillExpense = TotalBillExpense + obj.TotalGrossAmount;
                                else
                                    TotalBillExpense = TotalBillExpense + obj.TotalAmount;
                            }

                        }

                        //  string TotalBillExpenseS = string.Format("{0:n0}", TotalBillExpense);
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Total</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", TotalBillExpense));
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Receipt Expenditure during Month</td>"));
                        Result.Append(string.Format("<td style='text-align:left;left' >0 </td>"));
                        Result.Append("</tr>");

                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                        // Calculate Last all month expense
                        decimal LasAllMonthExpense = 0;
                        if (FirstMonth != 4)
                        {
                            StringBuilder PreBillParam = new StringBuilder();
                            PreBillParam.Append(BudgetId + ",");
                            PreBillParam.Append(FirstDateOfPreFirstMonth.ToShortDateString() + ",");
                            PreBillParam.Append(LastDateOfPreLastMonth.ToShortDateString() + ",");
                            if (BillType == 7)
                                LasAllMonthExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForExpenseReportForSalary", PreBillParam.ToString());
                            else
                                LasAllMonthExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForExpenseReport", PreBillParam.ToString());

                            // LasAllMonthExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForExpenseReport", PreBillParam.ToString());
                        }
                        string LasAllMonthExpenseS = string.Format("{0:n0}", LasAllMonthExpense);
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Expenditure during the month</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", TotalBillExpense));
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Previous months Expenditure</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0} </td>", LasAllMonthExpense));
                        Result.Append("</tr>");

                        decimal TotalExpendituretilldate = LasAllMonthExpense + TotalBillExpense;

                        decimal Balance = TotalFund - TotalExpendituretilldate;
                        // string BalanceS = string.Format("{0:n0}", Balance);
                        // string TotalExpendituretilldateS = string.Format("{0:n0}", TotalExpendituretilldate);
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Total Expenditure till date</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", TotalExpendituretilldate));
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Balance</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0} </td>", Balance));
                        Result.Append("</tr>");
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");

                        TotalBillExpense = 0;
                        LasAllMonthExpense = 0;
                    }
                    //      }
                }
                Result.Append("</table>");
                Result.Append("</div>");
            }
            else
            { Result.Append("<p><b>No Budget found</b><p>"); }


            Result.Append("</body>");
            return Result.ToString();
        }

        public static string GetExpenseReportYearlyWise(string BudgetIds, string FinacialYear, string Duration)
        {
            string[] FinalcialYearArray = FinacialYear.Split('-');
            string PreYear = FinalcialYearArray[0];
            string CurYear = FinalcialYearArray[1];

            // this is for showing month with year
            string MonthWithYear = string.Empty;
            DateTime FirstDateOfMonth = new DateTime(int.Parse(PreYear), 4, 1);
            DateTime LastDateOfMonth = new DateTime(int.Parse(CurYear), 3, 31);

            StringBuilder Result = new StringBuilder();

            string[] Budgets = BudgetIds.Split(',');

            // Crete Html Template for Authority Letter
            Result.Append("<body style='width:900px;margin:auto;'><section><header style='text-align:center;'>");
            Result.Append("<table style='width: 100%;'>  <tr> <td style='vertical-align: middle; text-align: center'>");
            Result.Append("<h2 style='text-align:center;font-weight:bold'>ESTABLISHMENT OF H.P. VIDHAN SABHA</h2><header>");

            if (Budgets != null && Budgets.Count() > 0)
            {

                Result.Append("<div><table style='width:100%;border: 1px solid black; border-collapse:collapse'   border='1px'> ");
                Result.Append(string.Format("<tr> <td style='width: 100px;text-align:left;' colspan='4'>Yearly Expenditure Statement for the year of {0}</td>  </tr> ", MonthWithYear));
                Result.Append("<tr>   <td style='text-align:left;' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                foreach (var item in Budgets)
                {
                    int BudgetId = int.Parse(item);
                    StringBuilder BillParam = new StringBuilder();
                    BillParam.Append(BudgetId + ",");
                    BillParam.Append(FirstDateOfMonth.ToShortDateString() + ",");
                    BillParam.Append(LastDateOfMonth.ToShortDateString() + ",");
                    // this is for getting list of 
                    List<mEstablishBill> BillList = (List<mEstablishBill>)Helper.ExecuteService("Budget", "GetAllEstablishBillForExpense", BillParam.ToString());
                    // if (BillList != null && BillList.Count() > 0)
                    //{
                    mBudget BudgetObj = (mBudget)Helper.ExecuteService("Budget", "GetBudgetById", new mBudget { BudgetID = BudgetId });
                    AllocateBudget AllocateSactionBud = (AllocateBudget)Helper.ExecuteService("Budget", "GetAllocateBudgetByBudgetId", new AllocateBudget { BudgetId = BudgetId });
                    decimal TotalAdditonlFund = (decimal)Helper.ExecuteService("Budget", "GetTotalAdditionById", new AdditionFunds { BudgetId = BudgetId });
                    decimal TotalFund = 0;
                    TotalFund = AllocateSactionBud.Sanctionedbudget + TotalAdditonlFund;
                    if (TotalFund > 0)
                    {
                        //   string TotalFundS = string.Format("{0:n0}", TotalFund);

                        Result.Append(string.Format("<tr>   <td style='text-align:left; ' colspan='4'>Account Head : {0}</td> </tr>", BudgetObj.BudgetName));
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                        Result.Append(string.Format("<tr>   <td style='text-align:left; ' colspan='4'>Allocate Budget : {0}</td> </tr>", TotalFund));
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");



                        Result.Append("<tr>   <td style='text-align:left;font-weight:bold' >Bill Number</td>");
                        Result.Append("<td style='text-align:left;font-weight:bold' >Expenditure Amount</td>  ");
                        Result.Append("<td style='text-align:left;font-weight:bold' >Encashment Date</td> ");
                        Result.Append("<td style='text-align:left;font-weight:bold' >Remarks</td> </tr>");
                        //  int BillType = 0;
                        decimal TotalBillExpense = 0;
                        foreach (var obj in BillList)
                        {
                            //DataSet dataSet = new DataSet();
                            //var EstablishBillIds = new List<KeyValuePair<string, string>>();
                            //EstablishBillIds.Add(new KeyValuePair<string, string>("@EstablishBillId", obj.EstablishBillID.ToString()));

                            //dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetTotalAmountOfReimbursBill", EstablishBillIds);

                            //decimal GrossAmount = 0;
                            //decimal.TryParse(dataSet.Tables[0].Rows[0][1].ToString(), out GrossAmount);
                            //// Convert.ToDecimal(dataSet.Tables[0].Rows[0][1]);
                            //string BillSanctionAmount = dataSet.Tables[0].Rows[0][1].ToString();// string.Format("{0:n0}", dataSet.Tables[0].Rows[0][1]); ;
                            //// string BillDeduction = string.Format("{0:n0}", dataSet.Tables[0].Rows[0][2]);
                            if (obj.IsDebitReceiptExpense)
                            {
                                Result.Append("<tr>");
                                Result.Append(string.Format("<td style='text-align:left; ' >{0} - {1}</td>", "Debit Receipt", obj.DebitReceiptRemarks));
                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", obj.TotalAmount));
                                if (obj.EncashmentDate != null)
                                    Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", obj.EncashmentDate.Value.ToString("dd/MM/yyyy")));
                                else
                                    Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", string.Empty));

                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", string.Empty));
                                Result.Append("</tr>");
                                TotalBillExpense = TotalBillExpense + obj.TotalAmount;
                            }
                            else
                            {
                                Result.Append("<tr>");
                                Result.Append(string.Format("<td style='text-align:left; ' >{0} - {1}</td>", obj.BillNumber, FinacialYear));
                                if (obj.BillType == 7)
                                    Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", obj.TotalGrossAmount));
                                else
                                    Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", obj.TotalAmount));
                                if (obj.EncashmentDate != null)
                                    Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", obj.EncashmentDate.Value.ToString("dd/MM/yyyy")));
                                else
                                    Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", string.Empty));

                                // BillType = obj.BillType;

                                Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", string.Empty));
                                Result.Append("</tr>");
                                if (obj.BillType == 7)
                                    TotalBillExpense = TotalBillExpense + obj.TotalGrossAmount;
                                else
                                    TotalBillExpense = TotalBillExpense + obj.TotalAmount;
                            }
                        }

                        //  string TotalBillExpenseS = string.Format("{0:n0}", TotalBillExpense);
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Total</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", TotalBillExpense));
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Receipt Expenditure during Month</td>"));
                        Result.Append(string.Format("<td style='text-align:left;left' >0 </td>"));
                        Result.Append("</tr>");

                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");
                        // Calculate Last all month expense
                        decimal LasAllMonthExpense = 0;
                        //if (FirstMonth != 4)
                        //{
                        //    StringBuilder PreBillParam = new StringBuilder();
                        //    PreBillParam.Append(BudgetId + ",");
                        //    PreBillParam.Append(FirstDateOfPreFirstMonth.ToShortDateString() + ",");
                        //    PreBillParam.Append(LastDateOfPreLastMonth.ToShortDateString() + ",");
                        //    LasAllMonthExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForExpenseReport", PreBillParam.ToString());
                        //}
                        // string LasAllMonthExpenseS = string.Format("{0:n0}", LasAllMonthExpense);
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Expenditure during the month</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", TotalBillExpense));
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Previous months Expenditure</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0} </td>", LasAllMonthExpense));
                        Result.Append("</tr>");

                        decimal TotalExpendituretilldate = LasAllMonthExpense + TotalBillExpense;

                        decimal Balance = TotalFund - TotalExpendituretilldate;
                        // string BalanceS = string.Format("{0:n0}", Balance);
                        // string TotalExpendituretilldateS = string.Format("{0:n0}", TotalExpendituretilldate);
                        Result.Append("<tr>");
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Total Expenditure till date</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0}</td>", TotalExpendituretilldate));
                        Result.Append(string.Format("<td style='text-align:left;font-weight:bold' >Balance</td>"));
                        Result.Append(string.Format("<td style='text-align:left; ' >{0} </td>", Balance));
                        Result.Append("</tr>");
                        Result.Append("<tr>   <td style='text-align:left; ' colspan='4'>&nbsp;&nbsp;&nbsp;</td> </tr>");

                        TotalBillExpense = 0;
                        LasAllMonthExpense = 0;
                    }
                    //      }
                }
                Result.Append("</table>");
                Result.Append("</div>");
            }
            else
            { Result.Append("<p><b>No Budget found</b><p>"); }


            Result.Append("</body>");
            return Result.ToString();
        }



        public ActionResult DownloadPdfOfExpenseReport(string FileName, string Duaration)
        {

            try
            {


                string url = "/ExpenseReport/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(Server.MapPath("~" + url), FileName);

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    if (Duaration == "Monthly")
                        pathRes.FileDownloadName = "MonthlyExpenseReport.pdf";
                    else if (Duaration == "Yearly")
                        pathRes.FileDownloadName = "YearlyExpenseReport.pdf";
                    else
                        pathRes.FileDownloadName = "QuartelyExpenseReport.pdf";

                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }

        public string CreteExpenseReportPdf(string Report)
        {
            string Result = Report;

            MemoryStream output = new MemoryStream();

            EvoPdf.Document document1 = new EvoPdf.Document();

            // set the license key
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

            AddElementResult addResult;

            HtmlToPdfElement htmlToPdfElement;
            string htmlStringToConvert = Report;
            string baseURL = "";
            htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

            addResult = page.AddElement(htmlToPdfElement);
            byte[] pdfBytes = document1.Save();

            try
            {
                output.Write(pdfBytes, 0, pdfBytes.Length);
                output.Position = 0;

            }
            finally
            {
                // close the PDF document to release the resources
                document1.Close();
            }


            string path = "";
            try
            {

                Guid FId = Guid.NewGuid();
                string fileName = FId + "_ExpenseReport.pdf";

                string url = "/ExpenseReport/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();



                return fileName;
            }
            catch (Exception)
            {
                throw;
            }

        }

        private static DateTime GetFirstDayOfMonth(int iMonth, int Year)
        {
            // set return value to the last day of the month
            // for any date passed in to the method

            // create a datetime variable set to the passed in date
            DateTime dtFrom = new DateTime(Year, iMonth, 1);

            // remove all of the days in the month
            // except the first day and set the
            // variable to hold that date
            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));

            // return the first day of the month
            return dtFrom;
        }

        private static DateTime GetLastDayOfMonth(int iMonth, int Year)
        {

            // set return value to the last day of the month
            // for any date passed in to the method

            // create a datetime variable set to the passed in date
            DateTime dtTo = new DateTime(Year, iMonth, 1);

            // overshoot the date by a month
            dtTo = dtTo.AddMonths(1);

            // remove all of the days in the next month
            // to get bumped down to the last day of the
            // previous month
            dtTo = dtTo.AddDays(-(dtTo.Day));

            // return the last day of the month
            return dtTo;

        }
        #endregion

        #region Summary Report Of Budget Head


        public static string GetSummaryReportMonthlyWise(string FinacialYear, int month)
        {
            string[] FinalcialYearArray = FinacialYear.Split('-');
            string PreYear = FinalcialYearArray[0];
            string CurYear = FinalcialYearArray[1];
            // this is for showing month with year
            string MonthWithYear = string.Empty;
            DateTime FirstDateOfMonth = new DateTime();
            DateTime LastDateOfMonth = new DateTime();
            DateTime FirstDateOfPreFirstMonth = new DateTime();
            DateTime LastDateOfPreLastMonth = new DateTime();
            if (month == 1 || month == 2 || month == 3)
            {
                MonthWithYear = string.Format("{0}/{1}", month, CurYear);
                FirstDateOfMonth = GetFirstDayOfMonth(month, int.Parse(CurYear));
                LastDateOfMonth = GetLastDayOfMonth(month, int.Parse(CurYear));
            }
            else
            {

                MonthWithYear = string.Format("{0}/{1}", month, PreYear);
                FirstDateOfMonth = GetFirstDayOfMonth(month, int.Parse(PreYear));
                LastDateOfMonth = GetLastDayOfMonth(month, int.Parse(PreYear));

            }

            //this is for getting remaining privious month Date
            FirstDateOfPreFirstMonth = new DateTime(int.Parse(PreYear), 4, 1);
            LastDateOfPreLastMonth = LastDateOfMonth.AddMonths(-1);
            StringBuilder Result = new StringBuilder();
            List<mBudget> Budgets = (List<mBudget>)Helper.ExecuteService("Budget", "GetGroupedBudget", FinacialYear);

            Result.Append(" <div style='page-break-after: always;'></div>");
            Result.Append("<table style='width: 100%;border-collapse:collapse'>  <tr> <td style='vertical-align: middle; text-align: center'>");
            Result.Append("<h2 style='text-align:center;font-weight:bold'>ESTABLISHMENT OF H.P. VIDHAN SABHA</h2><header>");


            Result.Append("<div><table style='width:100%;border: 1px solid ##CCCCCC;border-collapse: collapse;color:black; '   border='1px'> ");
            Result.Append(string.Format("<tr> <td style='width: 100px;text-align:left;' colspan='7'>Monthly Summary Report for the month of {0}</td>  </tr> ", MonthWithYear));
            Result.Append("<tr>   <td style='text-align:left;' colspan='7'>&nbsp;&nbsp;&nbsp;</td> </tr>");
            Result.Append("<tr><td style='width:150px;'></td><td><b>SOE</b></td><td><b>Allocated Budget</b></td><td><b>Previous Expenditure</b></td><td><b>Expenditure during the month</b></td>");
            Result.Append("<td><b>Total Expenditure</b></td><td><b>Balance</b></td></tr>");

            decimal TotalAllocateBudget = 0;
            decimal TotalPreExp = 0;
            decimal TotalExpOfThisMonth = 0;
            decimal TotalExpenditure = 0;
            decimal TotalBalance = 0;
            if (Budgets != null && Budgets.Count() > 0)
            {

                foreach (var item in Budgets)
                {
                    Result.Append(string.Format("<tr>   <td style='text-align:left;' colspan='7'>{0}-{1}-{2}-{3}-{4}-{5}-{6}</td> </tr>", item.MajorHead, item.SubMajorHead, item.MinorHead, item.SubHead, item.Gazetted, item.VotedCharged, item.Plan));
                    List<mBudget> BudgetsList = (List<mBudget>)Helper.ExecuteService("Budget", "GetBudgetWithTotalFund", item);
                    if (BudgetsList != null && BudgetsList.Count() > 0)
                    {
                        foreach (var bud in BudgetsList)
                        {
                            //GetBudgetWithTotalFund
                            Result.Append("<tr>");
                            Result.Append(string.Format("<td>{0}</td>", string.Empty));
                            Result.Append(string.Format("<td>{0}-{1}</td>", bud.ObjectCode, bud.ObjectCodeText));
                            Result.Append(string.Format("<td>{0}</td>", bud.AllocateFund));
                            decimal LasAllMonthExpense = 0;
                            if (month != 4)
                            {
                                StringBuilder PreBillParam = new StringBuilder();
                                PreBillParam.Append(bud.BudgetID + ",");
                                PreBillParam.Append(FirstDateOfPreFirstMonth.ToShortDateString() + ",");
                                PreBillParam.Append(LastDateOfPreLastMonth.ToShortDateString() + ",");

                                if (bud.BudgetName.Contains("Salary"))
                                    LasAllMonthExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForExpenseReportForSalary", PreBillParam.ToString());
                                else
                                    LasAllMonthExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForExpenseReport", PreBillParam.ToString());
                            }
                            Result.Append(string.Format("<td>{0}</td>", LasAllMonthExpense));
                            decimal CurrentMonthExpense = 0;

                            StringBuilder BillParam = new StringBuilder();
                            BillParam.Append(bud.BudgetID + ",");
                            BillParam.Append(FirstDateOfMonth.ToShortDateString() + ",");
                            BillParam.Append(LastDateOfMonth.ToShortDateString() + ",");

                            if (bud.BudgetName.Contains("Salary"))
                                CurrentMonthExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForExpenseReportForSalary", BillParam.ToString());
                            else
                                CurrentMonthExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForExpenseReport", BillParam.ToString());

                            Result.Append(string.Format("<td>{0}</td>", CurrentMonthExpense));
                            decimal Expense = CurrentMonthExpense + LasAllMonthExpense;
                            Result.Append(string.Format("<td>{0}</td>", Expense));
                            decimal Balance = bud.AllocateFund - Expense;
                            Result.Append(string.Format("<td>{0}</td>", Balance));
                            Result.Append("</tr>");
                            TotalAllocateBudget = TotalAllocateBudget + bud.AllocateFund;
                            TotalPreExp = TotalPreExp + LasAllMonthExpense;
                            TotalExpOfThisMonth = TotalExpOfThisMonth + CurrentMonthExpense;
                            TotalExpenditure = TotalExpenditure + Expense;
                            Expense = 0;
                            TotalBalance = TotalBalance + Balance;
                            Balance = 0;

                        }
                        Result.Append(string.Format("<tr><td> </td><td>Account Head Total:</td>"));
                        Result.Append(string.Format("<td>{0}</td>", TotalAllocateBudget));
                        Result.Append(string.Format("<td>{0}</td>", TotalPreExp));
                        Result.Append(string.Format("<td>{0}</td>", TotalExpOfThisMonth));
                        Result.Append(string.Format("<td>{0}</td>", TotalExpenditure));
                        Result.Append(string.Format("<td>{0}</td>", TotalBalance));
                        Result.Append("</tr>");
                        TotalAllocateBudget = 0;
                        TotalPreExp = 0;
                        TotalExpOfThisMonth = 0;
                        TotalExpenditure = 0;
                        TotalBalance = 0;

                    }

                    Result.Append("<tr>   <td style='text-align:left;' colspan='7'>&nbsp;&nbsp;&nbsp;</td> </tr>");

                }
                Result.Append("</table>");
                Result.Append("</br></br>");
                Result.Append("<table style='width:100%;border: 1px solid ##CCCCCC;border-collapse: collapse;color:black; '   border='1px'>");
                int Count = 1;
                decimal TotalPreExpLast = 0;
                decimal TotalCurrentExpLast = 0;
                decimal TotalExpLast = 0;
                foreach (var item in Budgets)
                {
                    Result.Append("<tr>");
                    Result.Append(string.Format("<td>{0}</td>", Count));
                    Result.Append(string.Format("<td style='text-align:left;'>{0}-{1}-{2}-{3}-{4}-{5}-{6}</td> ", item.MajorHead, item.SubMajorHead, item.MinorHead, item.SubHead, item.Gazetted, item.VotedCharged, item.Plan));
                    List<mBudget> BudgetsListN = (List<mBudget>)Helper.ExecuteService("Budget", "GetBudgetWithTotalFund", item);
                    decimal TPreExp = 0;
                    decimal TCExpense = 0;
                    StringBuilder sb = new StringBuilder();
                    if (BudgetsListN != null && BudgetsListN.Count() > 0)
                    {
                        foreach (var bud in BudgetsListN)
                        {
                            sb.Append(bud.BudgetID + "-");
                        }
                    }
                    if (month != 4)
                    {
                        StringBuilder PreBillParam = new StringBuilder();
                        PreBillParam.Append(sb.ToString() + ",");
                        PreBillParam.Append(FirstDateOfPreFirstMonth.ToShortDateString() + ",");
                        PreBillParam.Append(LastDateOfPreLastMonth.ToShortDateString() + ",");


                        TPreExp = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForSummaryReport", PreBillParam.ToString());
                    }
                    TotalPreExpLast = TotalPreExpLast + TPreExp;
                    StringBuilder BillParam = new StringBuilder();
                    BillParam.Append(sb.ToString() + ",");
                    BillParam.Append(FirstDateOfMonth.ToShortDateString() + ",");
                    BillParam.Append(LastDateOfMonth.ToShortDateString() + ",");


                    TCExpense = (decimal)Helper.ExecuteService("Budget", "GetTotalBillExpenseForSummaryReport", BillParam.ToString());
                    TotalCurrentExpLast = TotalCurrentExpLast + TCExpense;

                    Result.Append(string.Format("<td>{0}</td>", TPreExp));
                    Result.Append(string.Format("<td>{0}</td>", TCExpense));
                    Result.Append(string.Format("<td>{0}</td>", TPreExp + TCExpense));
                    Result.Append("<tr>");
                    TotalExpLast = TotalExpLast + (TPreExp + TCExpense);
                    Count++;
                    sb.Clear();
                }
                Result.Append(string.Format("<tr><td></td><td>Grand Total</td><td>{0}</td><td>{1}</td><td>{2}</td></tr>", TotalPreExpLast, TotalCurrentExpLast, TotalExpLast));
                Result.Append("</table>");
            }
            else
            {
                Result.Append("<tr>   <td style='text-align:left;' colspan='7'>No record found</td> </tr>");
                Result.Append("</table>");
            }




            Result.Append("</div>");
            //   Result.Append("</body>");
            return Result.ToString();
        }

        #endregion
        #region Send Email
        public static void SendEmailDetails(List<string> emailList, List<string> mobileList, string msgBody, string mailSubject, string mailBody, bool mobileStatus, bool emailStatus, string SavedPdfPath)
        {
            #region SMS And Email

            List<string> emailAddresses = new List<string>();
            List<string> phoneNumbers = new List<string>();

            SMSMessageList smsMessage = new SMSMessageList();
            CustomMailMessage emailMessage = new CustomMailMessage();

            //# SMS Settings
            if (emailList != null)
            {
                emailAddresses.AddRange(emailList);

            }
            if (mobileList != null)
            {
                phoneNumbers.AddRange(mobileList);
            }
            if (emailAddresses != null && phoneNumbers != null)
            {
                foreach (var item in phoneNumbers)
                {
                    smsMessage.MobileNo.Add(item);
                }
                foreach (var item in emailAddresses)
                {
                    emailMessage._toList.Add(item);
                }
            }

            smsMessage.SMSText = msgBody;
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = 1;
            emailMessage._isBodyHtml = true;
            emailMessage._subject = mailSubject;
            emailMessage._body = mailBody;
            if (!string.IsNullOrEmpty(SavedPdfPath))
            {
                EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes((SavedPdfPath)) };
                emailMessage._attachments = new List<EAttachment>();
                emailMessage._attachments.Add(ea);
            }
            try
            {
                if (emailList != null || mobileList != null)
                {
                    Notification.Send(mobileStatus, emailStatus, smsMessage, emailMessage);
                }

            }
            catch (Exception exp)
            {
                string errorMsg = exp.Message;
                // throw;
            }

            #endregion SMS And Email
        }

        #endregion

        #region Reimbursement Bill


        [HttpPost]
        public PartialViewResult SaveReimbursementBill(int ReimbursementBillID, int EstablishBillId, decimal GrossAmount, decimal SanctionAmount)
        {
            mReimbursementBill Bill = (mReimbursementBill)Helper.ExecuteService("Budget", "GetReimbursementBillById", new mReimbursementBill { ReimbursementBillID = ReimbursementBillID });
            mEstablishBill obj = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = Bill.EstablishBillId });

            decimal Deduction = GrossAmount - SanctionAmount;
            if (Deduction < GrossAmount || Deduction == GrossAmount)
            {
                // Edit Establish Bill
                obj.TotalAmount = obj.TotalAmount - Bill.SactionAmount + SanctionAmount;
                obj.TotalGrossAmount = obj.TotalGrossAmount - Bill.GrossAmount + GrossAmount;
                obj.Deduction = obj.TotalGrossAmount - obj.TotalAmount;

                // Edit Reimbursement Bill
                Bill.SactionAmount = SanctionAmount;
                Bill.GrossAmount = GrossAmount;
                Bill.Deduction = Deduction;
                Helper.ExecuteService("Budget", "UpdateReimbursementBill", Bill);

                // Save Establish Bill
                obj.ModifiedBy = CurrentSession.UserName;
                Helper.ExecuteService("Budget", "UpdateEstablishBill", obj);
                ViewBag.Msg = "Sucessfully updated Reimbursement Bill";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";

            }
            else
            {
                ViewBag.Msg = "Deduction can't be more than Gross Amount";
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }
            EstablishBillViewModel Model = new EstablishBillViewModel();
            mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = EstablishBillId });
            Model.mEstablishBill = EstablishBill;

            Model.ReimbursementBillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill",
                new mReimbursementBill { CreatedBy = CurrentSession.UserName }))
                .Where(m => m.EstablishBillId == Model.mEstablishBill.EstablishBillID).ToList(); ;

            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_VoucherList.cshtml", Model);
        }

        public PartialViewResult DeleteReimbursementBill(int Id)
        {
            mReimbursementBill Reimbill = (mReimbursementBill)Helper.ExecuteService("Budget", "GetReimbursementBillById", new mReimbursementBill { ReimbursementBillID = Id });
            mEstablishBill obj = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = Reimbill.EstablishBillId });

            obj.TotalAmount = obj.TotalAmount - Reimbill.SactionAmount;
            obj.TotalGrossAmount = obj.TotalGrossAmount - Reimbill.GrossAmount;
            obj.Deduction = obj.TotalGrossAmount - obj.TotalAmount;
            obj.ModifiedBy = CurrentSession.UserName;
            Helper.ExecuteService("Budget", "UpdateEstablishBill", obj);

            Helper.ExecuteService("Budget", "DeleteReimbursementBill", new mReimbursementBill { ReimbursementBillID = Id });
            ViewBag.Msg = "Sucessfully deleted Reimbursement Bill";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            EstablishBillViewModel Model = new EstablishBillViewModel();
            mEstablishBill EstablishBill = (mEstablishBill)Helper.ExecuteService("Budget", "GetEstablishBillById", new mEstablishBill { EstablishBillID = obj.EstablishBillID });
            Model.mEstablishBill = EstablishBill;

            Model.ReimbursementBillList = ((List<mReimbursementBill>)Helper.ExecuteService("Budget", "GetAllReimbursementBill",
                new mReimbursementBill { CreatedBy = CurrentSession.UserName }))
                .Where(m => m.EstablishBillId == Model.mEstablishBill.EstablishBillID).ToList(); ;

            return PartialView("/Areas/AccountsAdmin/Views/EstablishBill/_VoucherList.cshtml", Model);
        }

        #endregion
    }
}
