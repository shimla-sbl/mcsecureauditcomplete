﻿using EvoPdf;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class MemberIncomeTaxController : Controller
    {
        //
        // GET: /AccountsAdmin/MemberIncomeTax/

        public ActionResult Index()
        {
            var MemberIncomeTaxList = (List<mSpeakerIncomeTax>)Helper.ExecuteService("Budget", "GetAllSpeakerIncomeTax", null);
            return PartialView("/Areas/AccountsAdmin/Views/MemberIncomeTax/_Index.cshtml", MemberIncomeTaxList);
        }

        public PartialViewResult CalculateIncomeTax()
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            var MemberList = (List<mMember>)Helper.ExecuteService("Budget", "GetAllMembers", null);
            ViewBag.MemberList = new SelectList(MemberList, "MemberCode", "Name");
            return PartialView("/Areas/AccountsAdmin/Views/MemberIncomeTax/_CalculateIncomeTax.cshtml");
        }

        public JsonResult GetAllowanceDetail(int MemberCode)
        {
            var AllowanceList = (List<mSpeakerIncomeTax>)Helper.ExecuteService("Budget", "GetAllowance", MemberCode);

            return Json(AllowanceList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public JsonResult SaveIncomeTax(FormCollection coll)
        {
            string Data = string.Empty;
            try
            {
                mSpeakerIncomeTax obj = new mSpeakerIncomeTax();
                obj.EmpId = int.Parse(coll["StaffId"]);
                obj.Quarter = int.Parse(coll["QuarterDdl"]);
                obj.FinancialYear = coll["FinancialYear"];
                DateTime FromDate = new DateTime();
                DateTime.TryParse(coll["FromDate"], out FromDate);
                obj.FromDate = FromDate;

                DateTime ToDate = new DateTime();
                DateTime.TryParse(coll["ToDate"], out ToDate);
                obj.ToDate = ToDate;

                // obj.ToDate = obj.FromDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                obj.TotalSalary = decimal.Parse(coll["TotalSalary"]);
                obj.Accomodation = decimal.Parse(coll["FreeAccommodation"]);
                obj.AccomodPercentage = decimal.Parse(coll["FreeAccommodationPerc"]);


                obj.FurniturePerc = decimal.Parse(coll["furniturePerTxt"]);
                obj.TotalFurnitureCost = decimal.Parse(coll["TotalfurniturecostTxt"]);
                obj.FurnitureCost = decimal.Parse(coll["FurnitureCost"]);

                obj.ElectricityCharge = decimal.Parse((!string.IsNullOrEmpty(coll["ElectrictyCharge"]) ? coll["ElectrictyCharge"] : "0"));
                obj.WaterCharge = decimal.Parse((!string.IsNullOrEmpty(coll["WaterCharge"]) ? coll["WaterCharge"] : "0"));

                obj.TotalIncomeWithAF = decimal.Parse(coll["TotalIncomeWithAF"]);

                obj.TotalHaltingAllowance = decimal.Parse(coll["TotalHaltingAllowance"]);

                obj.FinalTotalIncome = decimal.Parse(coll["FinalTotalIncome"]);

                obj.Tax1 = decimal.Parse(coll["Tax1"]);
                obj.Tax2 = decimal.Parse(coll["Tax2"]);
                obj.Tax3 = decimal.Parse(coll["Tax3"]);
                obj.TotalTax1 = decimal.Parse(coll["TotalTax"]);
                obj.Cess1 = decimal.Parse(coll["Cess"]);
                obj.TaxableAmtWithTotalTax = decimal.Parse((!string.IsNullOrEmpty(coll["TotalTaxInWithTotalTax"]) ? coll["TotalTaxInWithTotalTax"] : "0"));

                obj.TaxA1 = decimal.Parse(coll["TA1"]);
                obj.TaxA2 = decimal.Parse(coll["TA2"]);
                obj.TaxA3 = decimal.Parse(coll["TA3"]);
                obj.TotalTaxA1 = decimal.Parse(coll["TotalTax1"]);
                obj.CessA1 = decimal.Parse(coll["Cess1"]);

                //
                obj.TotalPayable = decimal.Parse((!string.IsNullOrEmpty(coll["TotalPayable"]) ? coll["TotalPayable"] : "0"));
                obj.FinalTotalTax = decimal.Parse((!string.IsNullOrEmpty(coll["FinalTotalTax"]) ? coll["FinalTotalTax"] : "0"));
                obj.PaidTax = decimal.Parse((!string.IsNullOrEmpty(coll["PaidTax"]) ? coll["PaidTax"] : "0"));
                obj.Balance = decimal.Parse((!string.IsNullOrEmpty(coll["Balance"]) ? coll["Balance"] : "0"));
                long IncomeTaxId = (long)Helper.ExecuteService("Budget", "SaveSpeakerIncomeTax", obj);
                if (IncomeTaxId > 0)
                {
                    string[] BasicSalaryNumRow = coll["BasicSalaryNumRow"].Split(',');
                    foreach (var item in BasicSalaryNumRow)
                    {
                        mSalaryTypeDetailForTax Basic = new mSalaryTypeDetailForTax();
                        Basic.Amount = decimal.Parse(coll["BasicAmount-" + item]);
                        Basic.IncomeTaxId = IncomeTaxId;
                        Basic.MonthAndDayNo = int.Parse(coll["BasicMonth-" + item]);
                        Basic.SalaryType = "BasicSalary";
                        Basic.TotalAmount = decimal.Parse(coll["BasicTotalAmount-" + item]);
                        Helper.ExecuteService("Budget", "SaveSalryTypeDetail", Basic);
                    }
                    string[] SumptuaryAllowanceNumRow = coll["SumptuaryAllowanceNumRow"].Split(',');
                    foreach (var item in SumptuaryAllowanceNumRow)
                    {
                        mSalaryTypeDetailForTax Sumptuary = new mSalaryTypeDetailForTax();
                        Sumptuary.Amount = decimal.Parse(coll["SumptuaryAllowanceAmount-" + item]);
                        Sumptuary.IncomeTaxId = IncomeTaxId;
                        Sumptuary.MonthAndDayNo = int.Parse(coll["SumptuaryAllowanceMonth-" + item]);
                        Sumptuary.SalaryType = "SumptuaryAllowance";
                        Sumptuary.TotalAmount = decimal.Parse(coll["SumptuaryAllowanceTotalAmount-" + item]);
                        Helper.ExecuteService("Budget", "SaveSalryTypeDetail", Sumptuary);
                    }
                    string[] HaltingAllowanceNumRow = coll["HaltingAllowanceNumRow"].Split(',');
                    foreach (var item in HaltingAllowanceNumRow)
                    {
                        mSalaryTypeDetailForTax Halting = new mSalaryTypeDetailForTax();
                        Halting.Amount = decimal.Parse(coll["HaltingAllowanceAmount-" + item]);
                        Halting.IncomeTaxId = IncomeTaxId;
                        Halting.MonthAndDayNo = int.Parse(coll["HaltingAllowanceMonth-" + item]);
                        Halting.SalaryType = "HaltingAllowance";
                        Halting.TotalAmount = decimal.Parse(coll["HaltingAllowanceTotalAmount-" + item]);
                        Helper.ExecuteService("Budget", "SaveSalryTypeDetail", Halting);
                    }
                    string[] CompAllowanceNumRow = coll["CompAllowanceNumRow"].Split(',');
                    foreach (var item in CompAllowanceNumRow)
                    {
                        mSalaryTypeDetailForTax Comp = new mSalaryTypeDetailForTax();
                        Comp.Amount = decimal.Parse(coll["CompAllowanceAmount-" + item]);
                        Comp.IncomeTaxId = IncomeTaxId;
                        Comp.MonthAndDayNo = int.Parse(coll["CompAllowanceMonth-" + item]);
                        Comp.SalaryType = "CompAllowance";
                        Comp.TotalAmount = decimal.Parse(coll["CompAllowanceTotalAmount-" + item]);
                        Helper.ExecuteService("Budget", "SaveSalryTypeDetail", Comp);
                    }
                    string[] TeleAllowanceNumRow = coll["BasicSalaryNumRow"].Split(',');
                    foreach (var item in TeleAllowanceNumRow)
                    {
                        mSalaryTypeDetailForTax Tele = new mSalaryTypeDetailForTax();
                        Tele.Amount = decimal.Parse(coll["TeleAllowanceAmount-" + item]);
                        Tele.IncomeTaxId = IncomeTaxId;
                        Tele.MonthAndDayNo = int.Parse(coll["TeleAllowanceMonth-" + item]);
                        Tele.SalaryType = "TeleAllowance";
                        Tele.TotalAmount = decimal.Parse(coll["TeleAllowanceTotalAmount-" + item]);
                        Helper.ExecuteService("Budget", "SaveSalryTypeDetail", Tele);
                    }

                }
                Data = IncomeTaxId.ToString();
            }
            catch (Exception ex)
            {
                Data = ex.InnerException.ToString();
                throw;
            }

            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadSpeakerIncomeTaxPdf(int IncomeTaxId)
        {
            string url = "/SpeakerIncomeTax/";



            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            string path = "";
            string Result = GetSpeakerIncomeTaxData(IncomeTaxId);
            string FileName = CreateIncomeTaxPdf(Result, url, "Speaker");
            try
            {

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                path = Path.Combine(Server.MapPath("~" + url), FileName);

#pragma warning disable CS0219 // The variable 'contentType' is assigned but its value is never used
                string contentType = "application/octet-stream";
#pragma warning restore CS0219 // The variable 'contentType' is assigned but its value is never used
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

            byte[] bytes = System.IO.File.ReadAllBytes(path);
            return File(bytes, "application/pdf");
        }

        public string CreateIncomeTaxPdf(string Report, string Url, string ForWhom)
        {
            try
            {
                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 20, 20), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = Report;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }



                Guid FId = Guid.NewGuid();
                string fileName = FId + "_" + ForWhom + ".pdf";


                string directory = Server.MapPath(Url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(Server.MapPath("~" + Url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();



                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {


            }
        }

        public string GetSpeakerIncomeTaxData(long IncomeTaxId)
        {
            mSpeakerIncomeTax MemberIncomeTax = (mSpeakerIncomeTax)Helper.ExecuteService("Budget", "GetSpeakerIncomeTaxById", new mSpeakerIncomeTax { IncomeTaxID = IncomeTaxId });
            List<mSalaryTypeDetailForTax> SalaryDetail = (List<mSalaryTypeDetailForTax>)Helper.ExecuteService("Budget", "GetSalaryDetailIncomeTaxById", new mSalaryTypeDetailForTax { IncomeTaxId = IncomeTaxId });

            StringBuilder Result = new StringBuilder();
            Result.Append(string.Format("<body style='font-family:verdana;'>"));
            Result.Append("<center><p>Statement Showing estimated details of Income Tax Payable on the Salary,Allowances and other perquisites admissible to </br>");
            Result.Append(string.Format("<b>{0} ,{1} during the</br>", MemberIncomeTax.EmpName, MemberIncomeTax.Designation));
            Result.Append(string.Format("Financial Year {0} w.e.f. {1} to {2} </b></p></br>", MemberIncomeTax.FinancialYear, MemberIncomeTax.FromDate.ToString("dd-MM-yyyy"), MemberIncomeTax.ToDate.ToString("dd-MM-yyyy")));
            Result.Append(string.Format("<hr><table style='width:100%;text-align:left;'><tr><td>1.&nbsp;Salary {0} to {1}</td><td>", MemberIncomeTax.FromDate.ToString("dd-MM-yyyy"), MemberIncomeTax.ToDate.ToString("dd-MM-yyyy")));
            decimal TotalBasicSalary = 0;

            if (SalaryDetail != null && SalaryDetail.Count() > 0)
            {
                Result.Append("<table>");
                foreach (var item in SalaryDetail.Where(m => m.SalaryType == "BasicSalary"))
                {
                    Result.Append(string.Format("<tr><td>Rs {0} x {1} = </td><td>{2}</td></tr>", item.Amount, item.MonthAndDayNo, item.TotalAmount));
                    TotalBasicSalary = TotalBasicSalary + item.TotalAmount;
                }
                Result.Append("</table>");
            }

            Result.Append("</td></tr><tr><td>2.&nbsp;Sumtuary Allow.</td>");
            Result.Append("<td>");
            if (SalaryDetail != null && SalaryDetail.Count() > 0)
            {
                Result.Append("<table>");
                foreach (var item in SalaryDetail.Where(m => m.SalaryType == "SumptuaryAllowance"))
                {
                    Result.Append(string.Format("<tr><td>Rs {0} x {1} = </td><td>{2}</td></tr>", item.Amount, item.MonthAndDayNo, item.TotalAmount));
                }
                Result.Append("</table>");
            }

            //   Result.Append("<tr><td>Rs 30000 x 12 = = </td><td> 3,60,000-00</td></tr>");
            Result.Append("</td></tr>");
            Result.Append("<tr><td>3.&nbsp;Halting Allow</td>");
            Result.Append("<td>");
            if (SalaryDetail != null && SalaryDetail.Count() > 0)
            {
                Result.Append("<table>");
                foreach (var item in SalaryDetail.Where(m => m.SalaryType == "HaltingAllowance"))
                {
                    Result.Append(string.Format("<tr><td>Rs {0} x {1} = </td><td>{2}</td></tr>", item.Amount, item.MonthAndDayNo, item.TotalAmount));
                }
                Result.Append("</table>");
            }

            //  Result.Append("<tr><td>Rs 1000 x 94 = </td><td> 3,60,000-00</td></tr>");
            //  Result.Append("<tr><td>Rs 15000 x 271</td><td>4,06,500-00</td></tr>");


            Result.Append("</td></tr>");
            //
            Result.Append("<tr><td>4.&nbsp;Comp. Allow</td><td>");
            if (SalaryDetail != null && SalaryDetail.Count() > 0)
            {
                Result.Append("<table>");
                foreach (var item in SalaryDetail.Where(m => m.SalaryType == "CompAllowance"))
                {
                    Result.Append(string.Format("<tr><td>Rs {0} x {1} = </td><td>{2}</td></tr>", item.Amount, item.MonthAndDayNo, item.TotalAmount));
                }
                Result.Append("</table>");
            }
            //   Result.Append("<tr><td>Rs 5000 x 12 = </td><td> 60,000-00</td></tr>");
            Result.Append("</td></tr>");
            Result.Append("<tr><td>5.&nbsp;Tele Allow</td><td> ");
            if (SalaryDetail != null && SalaryDetail.Count() > 0)
            {
                Result.Append("<table>");
                foreach (var item in SalaryDetail.Where(m => m.SalaryType == "TeleAllowance"))
                {
                    Result.Append(string.Format("<tr><td>Rs {0} x {1} = </td><td>{2}</td></tr>", item.Amount, item.MonthAndDayNo, item.TotalAmount));
                }
                // Result.Append("</table>");
            }

            //  Result.Append("<tr><td>Rs 20,000 x 12 = </td><td> 2,40,000-00</td></tr>");
            Result.Append(string.Format("<tr><td><b>Total = </b></td><td><b> {0}</b></td></tr>", MemberIncomeTax.TotalSalary));
            Result.Append("</table></td></tr>");
            Result.Append(string.Format("<tr><td style='colspan:2'>7.&nbsp;Valuation of perquisites:-</br>(a) Free accommodation @ {0} % of salary Rs {1}</br>i.e. Rs {2}</td>", MemberIncomeTax.AccomodPercentage, TotalBasicSalary, MemberIncomeTax.Accomodation));
            Result.Append(string.Format("<td style='rowspan:2'>{0}</td></tr>", MemberIncomeTax.Accomodation));
            Result.Append(string.Format("<tr><td style='colspan:2'>(b) Additional perquisites of furniture @{0}% of the cost of</br>Rs {1}/- i.e. Rs {2}</td></tr>", MemberIncomeTax.FurniturePerc, MemberIncomeTax.TotalFurnitureCost, MemberIncomeTax.FurnitureCost));
            Result.Append(string.Format("<tr><td style='colspan:2'>(c) Free Electricity and water charges for </br>Rs.{0} + {1} = {2}</td><td>{3}</td></tr>", MemberIncomeTax.ElectricityCharge, MemberIncomeTax.WaterCharge, MemberIncomeTax.WaterCharge + MemberIncomeTax.ElectricityCharge, MemberIncomeTax.WaterCharge + MemberIncomeTax.ElectricityCharge));

            Result.Append(string.Format("<tr><td style='colspan:2;text-align:right'><b>Total Income &nbsp;&nbsp;</b></td><td><b>{0}</b></td></tr>", MemberIncomeTax.TotalIncomeWithAF));
            Result.Append(string.Format("<tr><td style='colspan:2'>Less on a/c of halting allowance</br>w.e.f. 1-3-{0} to 28-02-{1} = {2} = (-)</td><td>{3}</td></tr>", MemberIncomeTax.FinancialYear.Split('-')[0], MemberIncomeTax.FinancialYear.Split('-')[1], MemberIncomeTax.TotalHaltingAllowance, MemberIncomeTax.FinalTotalIncome));
            Result.Append(string.Format("<tr><td style='colspan:2;text-align:center'><b>Total Income &nbsp;&nbsp;</b></td><td><b>{0}</b></td></tr>", MemberIncomeTax.FinalTotalIncome));
            Result.Append("<tr><td colspan='2'><table style='border:1px solid black;width:100%;'>");
            Result.Append("<tr><td>Income Tax Payable up to Rs. 2,50,000</td><td>=</td><td>Nil</td></tr>");
            Result.Append(string.Format("<tr><td>2,50,001 to 5,00,000 @ 10%</td><td>=</td><td>{0}</td></tr>", MemberIncomeTax.Tax1));
            Result.Append(string.Format("<tr><td>5,00,001 to 10,00,000 i.e. @ 20%</td><td>=</td><td>{0}</td></tr>", MemberIncomeTax.Tax2));
            Result.Append(string.Format("<tr><td>10,00,000 to above @30%</td><td>=</td><td>{0}</td></tr>", MemberIncomeTax.Tax3));
            Result.Append(string.Format("<tr><td></td><td><b>Total</b></td><td>2,08,573-00</td></tr>", MemberIncomeTax.TotalTax1));
            Result.Append(string.Format("<tr><td>Cess 3%</td><td></td><td>{0}</td></tr>", MemberIncomeTax.Cess1));
            Result.Append(string.Format("<tr><td>Total Taxable Income + Total Tax </br>{0} + {1} + {2} = {3}</td><td>=</td><td>{4}</td></tr>", MemberIncomeTax.FinalTotalIncome, MemberIncomeTax.TotalTax1, MemberIncomeTax.Cess1, MemberIncomeTax.TaxableAmtWithTotalTax, MemberIncomeTax.TaxableAmtWithTotalTax));
            //
            Result.Append("<tr><td>Income Tax Payable up to Rs. 2,50,000</td><td>=</td><td>Nil</td></tr>");
            Result.Append(string.Format("<tr><td>2,50,001 to 5,00,000 @ 10%</td><td>=</td><td>{0}</td></tr>", MemberIncomeTax.TaxA1));
            Result.Append(string.Format("<tr><td>5,00,001 to 10,00,000 @ 20%</td><td>=</td><td>{0}</td></tr>", MemberIncomeTax.TaxA2));
            Result.Append(string.Format("<tr><td>10,00,000 to 14,93,406/- @30%</td><td>=</td><td>{0}</td></tr>", MemberIncomeTax.TaxA3));
            Result.Append(string.Format("<tr><td style='text-align:right'><b>Total Tax</b></td><td>=</td><td><b>{0}</b></td></tr>", MemberIncomeTax.TotalTaxA1));
            Result.Append(string.Format("<tr><td style='text-align:right'><b>Cess 3%</b></td><td>=</td><td><b>{0}</b></td></tr>", MemberIncomeTax.CessA1));
            Result.Append(string.Format("<tr><td style='text-align:right'><b>Total Payable</b></td><td>=</td><td><b>{0}</b></td></tr>", MemberIncomeTax.TotalPayable));
            Result.Append("</table></td></tr>");
            Result.Append(string.Format("<tr><td style='colspan:2;text-align:right'><b>Total Tax =</b></td><td>{0}</td></tr>", MemberIncomeTax.FinalTotalTax));
            Result.Append(string.Format("<tr><td style='colspan:2;text-align:right'><b>Paid Tax =</b></td><td><b>{0}</b></td></tr>", MemberIncomeTax.PaidTax));
            Result.Append(string.Format("<tr><td style='colspan:2;text-align:right'><b>Balance=</b></td><td>{0}</td></tr>", MemberIncomeTax.TotalPayable));
            //starting from here
            Result.Append(string.Format("<tr><td><b>For the {0} quarter Rs.{1} only</b></br>SANCTIONED FOR RS.{2}</td></tr>", MemberIncomeTax.Quarter, MemberIncomeTax.PaidTax, NumberToWordsConverter.NumbersToWords(Convert.ToInt64(MemberIncomeTax.PaidTax))));
            Result.Append("</table></center></body>");


            return Result.ToString();
        }

        #region Member Income Tax
        public ActionResult MemberIndex()
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            //var MemberIncomeTaxList = (List<mMemberIncomeTax>)Helper.ExecuteService("Budget", "GetAllMemberIncomeTax", null);
            return PartialView("/Areas/AccountsAdmin/Views/MemberIncomeTax/_MemberIndex.cshtml");
        }

        public PartialViewResult PopulateMemberIncomeTax()
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "CurrentFinancialYear" });
            string FinYearValue = SiteSettingsSessn.SettingValue;
            ViewBag.FinancialYearList = ModelMapping.GetFinancialYear(FinYearValue);
            var MemberList = (List<mMemberAccountsDetails>)Helper.ExecuteService("MemberAccountDetails", "GetCurrentMembersDetails", null);
            ViewBag.MemberList = new SelectList(MemberList, "MemberCode", "MemberName");

            List<mSpeakerIncomeTax> AllowanceList = (List<mSpeakerIncomeTax>)Helper.ExecuteService("Budget", "GetMemberAllowance", null);
            if (AllowanceList != null && AllowanceList.Count() > 0)
            {
                ViewBag.ErrorMsg = "No";

                decimal TotalSalary = (decimal)(AllowanceList.Sum(m => m.AllowanceAmount) * 12);
                ViewBag.TotalSalary = TotalSalary;

                decimal ExamptedAllowance = (decimal)(AllowanceList.Where(m => m.HeadName == "Const Allow").FirstOrDefault().AllowanceAmount * 12);
                ViewBag.ExamptedAllowance = ExamptedAllowance;

                decimal GrossIncome = TotalSalary - ExamptedAllowance;
                ViewBag.GrossIncome = GrossIncome;

                decimal Tax1 = 0;
                decimal Tax2 = 0;
                decimal Tax3 = 0;
                decimal TotalTax1 = 0;
                decimal Cess1 = 0;
                if (GrossIncome <= 250000)
                {
                    TotalTax1 = 0;

                }
                else if (GrossIncome <= 500000)
                {
                    TotalTax1 = (GrossIncome - 250000) * 10 / 100;
                    Tax1 = TotalTax1;
                }
                else if (GrossIncome <= 1000000)
                {
                    Tax1 = 250000 * 10 / 100;
                    Tax2 = (GrossIncome - 500000) * 20 / 100;
                    TotalTax1 = Tax1 + Tax2;
                }
                else if (GrossIncome > 1000000)
                {
                    Tax1 = 250000 * 10 / 100;
                    Tax2 = 500000 * 20 / 100;
                    Tax3 = (GrossIncome - 1000000) * 30 / 100;
                    TotalTax1 = Tax3 + Tax1 + Tax2;
                }
                ViewBag.TaxOnIncome = TotalTax1;

                Cess1 = TotalTax1 * 3 / 100;
                ViewBag.Cess1 = Cess1;

                decimal TaxableIncome = GrossIncome + TotalTax1 + Cess1;
                ViewBag.TaxableIncome = TaxableIncome;



                decimal TaxA1 = 0;
                decimal TaxA2 = 0;
                decimal TaxA3 = 0;
                decimal TotalTaxA1 = 0;
                decimal CessA1 = 0;
                if (TaxableIncome <= 250000)
                {
                    TotalTax1 = 0;

                }
                else if (TaxableIncome <= 500000)
                {
                    TotalTaxA1 = (TaxableIncome - 250000) * 10 / 100;
                    TaxA1 = TotalTax1;
                }
                else if (TaxableIncome <= 1000000)
                {
                    TaxA1 = 250000 * 10 / 100;
                    TaxA2 = (TaxableIncome - 500000) * 20 / 100;
                    TotalTaxA1 = TaxA1 + TaxA2;
                }
                else if (TaxableIncome > 1000000)
                {
                    TaxA1 = 250000 * 10 / 100;
                    TaxA2 = 500000 * 20 / 100;
                    TaxA3 = (TaxableIncome - 1000000) * 30 / 100;
                    TotalTaxA1 = TaxA3 + TaxA1 + TaxA2;
                }

                ViewBag.TaxPayable = TotalTaxA1;

                CessA1 = TotalTaxA1 * 3 / 100;
                ViewBag.CessA1 = CessA1;

                decimal FinalTotalTaxA1 = TotalTaxA1 + CessA1;
                ViewBag.TotalTaxPayable = FinalTotalTaxA1;
            }
            else
            {
                ViewBag.ErrorMsg = "No allowance availbale";
            }
            return PartialView("/Areas/AccountsAdmin/Views/MemberIncomeTax/_PopulateMemberIncomeTax.cshtml");
        }

        [HttpPost]

        public JsonResult SaveMemberIncomeTax(FormCollection coll)
        {
            string Data = string.Empty;
            try
            {
                var MemberIncomeTaxList = (List<mMemberIncomeTax>)Helper.ExecuteService("Budget", "GetAllMemberIncomeTax", new mMemberIncomeTax { FinancialYear = coll["FinancialYear"], Quarter = coll["QuarterDdl"] });
                if ( MemberIncomeTaxList.Count() == 0)
                {
                    string[] MemberIds = coll["StaffId"].Split(','); ;
                    string FinancialYear = coll["FinancialYear"];
                    string Quarter = coll["QuarterDdl"];
                    string TotalSalary = coll["TotalSalary"];
                    string AllowanceExampt = coll["AllowanceExampt"];
                    string GrossIncome = coll["GrossIncome"];
                    string TaxOnIncome = coll["TaxOnIncome"];
                    string Cess = coll["Cess"];
                    string TaxableIncome = coll["TaxableIncome"];
                    string TaxPayable = coll["TaxPayable"];
                    string Cess1 = coll["Cess1"];
                    string TotalTaxPayable = coll["TotalTaxPayable"];
                    foreach (var item in MemberIds)
                    {
                        mMemberIncomeTax obj = new mMemberIncomeTax();
                        obj.EmpId = int.Parse(item);
                        obj.FinancialYear = FinancialYear;
                        obj.Quarter = Quarter;
                        obj.TotalSalary = decimal.Parse(TotalSalary);
                        obj.AllowanceExampt = decimal.Parse(AllowanceExampt);
                        obj.GrossIncome = decimal.Parse(GrossIncome);
                        obj.TaxOnIncome = decimal.Parse(TaxOnIncome);
                        obj.Cess = decimal.Parse(Cess);
                        obj.TaxableIncome = decimal.Parse(TaxableIncome);
                        obj.TaxPayable = decimal.Parse(TaxPayable);
                        obj.Cess1 = decimal.Parse(Cess1);
                        obj.TotalTaxPayable = decimal.Parse(TotalTaxPayable);
                        Helper.ExecuteService("Budget", "SaveMemberIncomeTax", obj);
                    }
                    Data = "Member income tax Sucessfully saved";
                }
                else
                {
                    Data = "For this fiancial year and quarter , data already saved please change the financial year or quarter";
                }
            }
            catch (Exception ex)
            {
                Data = ex.InnerException.ToString();
                //  throw;
            }

            return Json(Data, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetMemberIncomeTaxListByFinancialYear(string FinancialYear, string Quarter)
        {
            var MemberIncomeTaxList = (List<mMemberIncomeTax>)Helper.ExecuteService("Budget", "GetAllMemberIncomeTax", new mMemberIncomeTax { FinancialYear = FinancialYear, Quarter = Quarter });
            string Data = GetMemberIncomeListData(MemberIncomeTaxList, FinancialYear, Quarter);
            return Json(Data, JsonRequestBehavior.AllowGet);
        }
        public string GetMemberIncomeListData(List<mMemberIncomeTax> model, string FinancialYear, string Quarter)
        {

            StringBuilder ReplyList = new StringBuilder();

            if (model != null && model.Count() > 0)
            {
                ReplyList.Append(string.Format("<div class='panel panel-default' style='width:1100px;font-size:15px;'><div style='text-align:center;'><h2>STATEMENT OF INCOME TAX PAYABLE ON THE SALARY AND OTHER ALLOWANCES TO THE HON'BLE MEMBERS OF H.P. LEGISLATIVE ASSEMBLY DURING THE FINANCIAL YEAR {0}</h2></div>", FinancialYear));

                ReplyList.Append("<table class='table table-condensed table-bordered'  id='ResultTable'>");
                ReplyList.Append("<thead class='header' ><tr style='background-color: #428bca ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:30px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px;'>(1)<br>SR. NO.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' > </th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >(2)<br> Name of the Member</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >(3)<br>Total Sal. And Allow.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >(4)<br> Allow. Exempted</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >(5)<br>Gross Income<br>(3-4)</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >(6)<br>Tax on Income<br>(Col.5)</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >(7) <br>Cess<br>3%</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >(8)<br>Taxable Income<br>(5+6+7) rounding off</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >(9)<br>Tax Payable</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >(10)<br>Cess<br>3%</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >(11)<br>Total Tax Payable</th>");
                ReplyList.Append("</tr></thead>");


                ReplyList.Append("<tbody  class='body'>");
                int count = 0;
                decimal TotalTaxPayable = 0;
                decimal TotalCess1 = 0;
                decimal FinalTotalTaxPayable = 0;
                foreach (var obj in model)
                {
                    count++;
                    ReplyList.Append("<tr>");
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", count));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.PanNumber));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.EmpName));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.TotalSalary));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.AllowanceExampt));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.GrossIncome));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.TaxOnIncome));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.Cess));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.TaxableIncome));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.TaxPayable));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.Cess1));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", obj.TotalTaxPayable));
                    ReplyList.Append("</tr>");
                    TotalTaxPayable = TotalTaxPayable + obj.TaxPayable;
                    TotalCess1 = TotalTaxPayable + obj.Cess1;
                    FinalTotalTaxPayable = TotalTaxPayable + obj.TotalTaxPayable;
                }
                ReplyList.Append("<tr>");
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", string.Empty));
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", string.Empty));
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", string.Empty));
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", string.Empty));
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", string.Empty));
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", string.Empty));
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", string.Empty));
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", string.Empty));
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", string.Empty));
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'><b>{0}</b></td>", TotalTaxPayable));
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'><b>{0}</b></td>", TotalCess1));
                ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'><b>{0}</b></td>", FinalTotalTaxPayable));
                ReplyList.Append("</tr>");
                ReplyList.Append("</tbody></table> ");
                ReplyList.Append(string.Format("<div style='text-align:right;font-weight:bold;font-size=16px;'>Total Tax = {0}</div>", FinalTotalTaxPayable));
                var PaidTax = FinalTotalTaxPayable / 4;
                ReplyList.Append(string.Format("<div style='text-align:right;font-weight:bold;font-size=16px;'>Paid Tax = {0}</div>", PaidTax));
                var Balance = FinalTotalTaxPayable - PaidTax;
                ReplyList.Append(string.Format("<div style='text-align:right;font-weight:bold;font-size=16px;'>Balance Tax = {0}</div>", Balance));
                ReplyList.Append(string.Format("<div style='text-align:left;font-weight:bold;font-size=16px;'>For the {0} QUARTER Rs. {1} <br>SANCTIONE FOR RS. {2}</div>",Quarter, PaidTax,  NumberToWordsConverter.NumbersToWords(Convert.ToInt64(PaidTax))));
                ReplyList.Append("</div>");
            }
            else
            {
                ReplyList.Append("No data Found");
            }
            return ReplyList.ToString();
        }

        public ActionResult DownloadMemberIncomeTaxPdf(string FinancialYear, string Quarter)
        {
            string url = "/MemberIncomeTax/";

            var MemberIncomeTaxList = (List<mMemberIncomeTax>)Helper.ExecuteService("Budget", "GetAllMemberIncomeTax", new mMemberIncomeTax { FinancialYear = FinancialYear, Quarter = Quarter });

            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }

            string path = "";
            string Result = GetMemberIncomeListData(MemberIncomeTaxList, FinancialYear, Quarter);
            string FileName = CreateIncomeTaxPdf(Result, url, "Member");
            try
            {

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                path = Path.Combine(Server.MapPath("~" + url), FileName);

#pragma warning disable CS0219 // The variable 'contentType' is assigned but its value is never used
                string contentType = "application/octet-stream";
#pragma warning restore CS0219 // The variable 'contentType' is assigned but its value is never used
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

            byte[] bytes = System.IO.File.ReadAllBytes(path);
            return File(bytes, "application/pdf");
        }


        #endregion
    }
}
