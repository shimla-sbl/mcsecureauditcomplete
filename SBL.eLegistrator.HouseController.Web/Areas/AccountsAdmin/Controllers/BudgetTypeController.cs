﻿using SBL.DomainModel.Models.Budget;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class BudgetTypeController : Controller
    {
        //
        // GET: /AccountsAdmin/BudgetType/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mBudgetType>)Helper.ExecuteService("BudgetType", "GetAllBudgetType", null);
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Budget Type Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult PopulateBudgetType(int Id)
        {
             
            BudgetTypeViewModel model = new BudgetTypeViewModel();

            if (Id > 0)
            {
                mBudgetType stateToEdit = (mBudgetType)Helper.ExecuteService("BudgetType", "GetBudgetTypeById", new mBudgetType { BudgetTypeID = Id });
                 model = stateToEdit.ToViewModel1("Edit");
            }
            else
            {
                model.VMode = "Add";
                model.VIsActive = true;
            }
            return PartialView("CreateBudgetType", model);
        }

        [HttpPost]
       // [ValidateAntiForgeryToken]
        public ActionResult SaveBudgetType(BudgetTypeViewModel model)
        {

             

            var bill = model.ToDomainModel();
            if (model.VMode == "Add")
            {

                Helper.ExecuteService("BudgetType", "CreateBudgetType", bill);
            }
            else
            {
                Helper.ExecuteService("BudgetType", "UpdateBudgetType", bill);
            }
            var State = (List<mBudgetType>)Helper.ExecuteService("BudgetType", "GetAllBudgetType", null);
            var data = State.ToViewModel();
            return PartialView("/Areas/AccountsAdmin/Views/BudgetType/_BudgetTypeList.cshtml", data);


        }

         

    }
}
