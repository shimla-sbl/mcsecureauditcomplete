﻿using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Controllers
{
    public class RtgsSoftController : Controller
    {

        // GET: /AccountsAdmin/RtgsSoft/

        public PartialViewResult RtgsSoftIndex(int pageId = 1, int pageSize = 10)
        {
            try
            {
                string url = "/RtgsSoftCopyReport/";


                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] filePaths = Directory.GetFiles(directory);
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                var BillList = (List<mRtgsSoft>)Helper.ExecuteService("Budget", "GetAllRtgsSoft", null);

                ViewBag.PageSize = pageSize;
                List<mRtgsSoft> pagedRecord = new List<mRtgsSoft>();
                if (pageId == 1)
                {
                    pagedRecord = BillList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();



                return PartialView("/Areas/AccountsAdmin/Views/RtgsSoft/_RtgsSoftIndex.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of RTGS Soft Copy Details";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 10)
        {
            var BillList = (List<mRtgsSoft>)Helper.ExecuteService("Budget", "GetAllRtgsSoft", null);

            ViewBag.PageSize = pageSize;
            List<mRtgsSoft> pagedRecord = new List<mRtgsSoft>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/RtgsSoft/_RtgsSoftList.cshtml", pagedRecord);
        }


        public PartialViewResult PopulateRtgsSoft(int Id, string Action, int pageId = 1, int pageSize = 10)
        {
            var MemberList = (List<mOtherFirm>)Helper.ExecuteService("Budget", "GetAllOtherFirm", null);
            ViewBag.FirmList = new SelectList(MemberList, "FirmID", "Name");

            mRtgsSoft model = new mRtgsSoft();


            var stateToEdit = (mRtgsSoft)Helper.ExecuteService("Budget", "GetRtgsSoftById", new mRtgsSoft { RtgsSoftID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit;

            model.Mode = Action;
            ViewBag.Add = Action;
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "RemitterDetail" });
            if (SiteSettingsSessn != null)
            {
                string[] RemDetail = SiteSettingsSessn.SettingValue.Split('-');
                ViewBag.RemAcNo = RemDetail[0];
                ViewBag.RemName = RemDetail[1];
                ViewBag.RemAdd = RemDetail[2];
                ViewBag.EmpEmailId = RemDetail[3];
            }
            else
            {
                ViewBag.RemAcNo = string.Empty;
                ViewBag.RemName = string.Empty;
                ViewBag.RemAdd = string.Empty;
                ViewBag.EmpEmailId = string.Empty;
            }


            return PartialView("/Areas/AccountsAdmin/Views/RtgsSoft/_RtgsSoftPopulate.cshtml", model);
        }

        public PartialViewResult DeleteRtgsSoft(int Id, int pageId = 1, int pageSize = 10)
        {

            Helper.ExecuteService("Budget", "DeleteRtgsSoft", new mRtgsSoft { RtgsSoftID = Id });
            ViewBag.Msg = "Sucessfully deleted Firm";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            var BillList = (List<mRtgsSoft>)Helper.ExecuteService("Budget", "GetAllRtgsSoft", null);

            ViewBag.PageSize = pageSize;
            List<mRtgsSoft> pagedRecord = new List<mRtgsSoft>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/RtgsSoft/_RtgsSoftList.cshtml", pagedRecord);
        }

        [HttpPost]
        public PartialViewResult SaveRtgsSoft(mRtgsSoft model, int pageId = 1, int pageSize = 10)
        {
            mSiteSettingsVS SiteSettingsSessn = (mSiteSettingsVS)Helper.ExecuteService("User", "GetSettingSettingName", new mSiteSettingsVS { SettingName = "RemDetail" });

            string RemDetail = string.Format("{0}-{1}-{2}", model.RemAcNo, model.RemName, model.RemAdd);
            if (SiteSettingsSessn != null && SiteSettingsSessn.SettingValue != RemDetail)
            {
                SiteSettingsSessn.SettingValue = RemDetail;
                Helper.ExecuteService("User", "UpdateSiteSettings", SiteSettingsSessn);
            }

            if (model.Mode == "Add")
            {
                model.CreatedOn = DateTime.Now;
                int BillId = (int)Helper.ExecuteService("Budget", "CreateRtgsSoft", model);
                ViewBag.Msg = "Sucessfully added RTGS Soft copy";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            else
            {
                Helper.ExecuteService("Budget", "UpdateRtgsSoft", model);
                ViewBag.Msg = "Sucessfully updated RTGS Soft copy";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }

            var BillList = (List<mRtgsSoft>)Helper.ExecuteService("Budget", "GetAllRtgsSoft", null);

            ViewBag.PageSize = pageSize;
            List<mRtgsSoft> pagedRecord = new List<mRtgsSoft>();
            if (pageId == 1)
            {
                pagedRecord = BillList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = BillList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(BillList.Count, pageId, pageSize);

            return PartialView("/Areas/AccountsAdmin/Views/RtgsSoft/_RtgsSoftList.cshtml", pagedRecord);
        }

        public PartialViewResult SerchRtgsSoftCopy(string CreatedOnFromS, string CreatedOnToS, int pageId = 1, int pageSize = 10)
        {

            var RtgsSoft = (List<mRtgsSoft>)Helper.ExecuteService("Budget", "GetAllRtgsSoft", null);


            if (!string.IsNullOrEmpty(CreatedOnFromS) && !string.IsNullOrEmpty(CreatedOnToS))
            {
                DateTime CreatedOnFrom = new DateTime();
                DateTime.TryParse(CreatedOnFromS, out CreatedOnFrom);

                DateTime CreatedOnTo = new DateTime();
                DateTime.TryParse(CreatedOnToS, out CreatedOnTo);

                RtgsSoft = RtgsSoft.Where(m => m.CreatedOn >= CreatedOnFrom && m.CreatedOn <= CreatedOnTo).ToList();
            }


            ViewBag.PageSize = pageSize;
            List<mRtgsSoft> pagedRecord = new List<mRtgsSoft>();
            if (pageId == 1)
            {
                pagedRecord = RtgsSoft.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = RtgsSoft.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(RtgsSoft.Count, pageId, pageSize);

            Session["RtgsSoft"] = RtgsSoft;

            return PartialView("/Areas/AccountsAdmin/Views/RtgsSoft/_RtgsSoftList.cshtml", pagedRecord);
        }


        public static string GetSearchList(List<mRtgsSoft> model)
        {

            StringBuilder BillReport = new StringBuilder();

            if (model != null && model.Count() > 0)
            {

                int count = 0;
                foreach (var obj in model)
                {
                    count++;

                    // this is for left alignment
                    BillReport.AppendFormat("{0, -10} {1, -15} {2, -20} {3, -25} {4, -30} {5, -30} {6, -35} {7, -40} {8, -40}", GetString(obj.TransferAmt, obj.RemAcNo, obj.RemName),
                        obj.RemAdd, obj.FirmAcNo, obj.FirmName, obj.FirmAdd, obj.IFSCCode, "ATTN", count, obj.EmpEmailId);
                    BillReport.Append(Environment.NewLine);

                    // this is for right alignment {0,10} {1,15}
                }
            }
            else
            {
                BillReport.Append("No records available");
            }
            return BillReport.ToString();
        }

        public static string GetString(decimal TransferAmt, string AccNo, string BankName)
        {
            
            int Amount = Decimal.ToInt32(TransferAmt);
            int length = 14 - Amount.ToString().Length;
            StringBuilder AmtSB = new StringBuilder();
            AmtSB.Append("R41");
            for (int i = 0; i < length; i++)
            {
                AmtSB.Append("0");
            }
            AmtSB.Append(Amount);
            for (int i = 0; i < 37 - AccNo.Length; i++)
            {
                AmtSB.Append("0");
            }
            AmtSB.Append(AccNo);
            AmtSB.Append(BankName);
            AmtSB.Append("\t");
            return AmtSB.ToString();

        }
        public ActionResult DownloadNotepad(string CreatedOnFromS, string CreatedOnToS)
        {
            List<mRtgsSoft> Model = new List<mRtgsSoft>();
            if (Session["RtgsSoft"] != null)
            {
                var List = (List<mRtgsSoft>)Session["RtgsSoft"];
                Model = List;
            }
            else
            {

                var RtgsSoft = (List<mRtgsSoft>)Helper.ExecuteService("Budget", "GetAllRtgsSoft", null);

                if (!string.IsNullOrEmpty(CreatedOnFromS) && !string.IsNullOrEmpty(CreatedOnToS))
                {
                    DateTime CreatedOnFrom = new DateTime();
                    DateTime.TryParse(CreatedOnFromS, out CreatedOnFrom);

                    DateTime CreatedOnTo = new DateTime();
                    DateTime.TryParse(CreatedOnToS, out CreatedOnTo);

                    RtgsSoft = RtgsSoft.Where(m => m.CreatedOn >= CreatedOnFrom && m.CreatedOn <= CreatedOnTo).ToList();
                }
                Model = RtgsSoft;
            }
             
            string Result = GetSearchList(Model);

            string path = "";
            try
            {

                Guid FId = Guid.NewGuid();
                string fileName = FId + "_RtgsSoftCopy.txt";

                string url = "/RtgsSoftCopyReport/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);


                StreamWriter sw;
                sw = System.IO.File.CreateText(path);
                sw.WriteLine(Result);
                sw.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "RtgsSoftCopy.txt";

                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }


#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }

    }
}
