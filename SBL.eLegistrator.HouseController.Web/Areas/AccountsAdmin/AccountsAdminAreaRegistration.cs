﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin
{
    public class AccountsAdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AccountsAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AccountsAdmin_default",
                "AccountsAdmin/{controller}/{action}/{id}",
                new { action = "Index", Controller = "DashBoardAccountsAdmin", id = UrlParameter.Optional }
            );
        }
    }
}
