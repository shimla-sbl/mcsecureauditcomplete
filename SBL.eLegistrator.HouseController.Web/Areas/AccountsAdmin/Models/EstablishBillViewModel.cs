﻿using SBL.DomainModel.Models.Budget;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    public class EstablishBillViewModel
    {
        public EstablishBillViewModel()
        {
            this.mEstablishBill = new mEstablishBill();
            this.EstablishBillLsit = new List<mEstablishBill>();
            this.mReimbursementBill = new mReimbursementBill();
            this.ReimbursementBillList = new List<mReimbursementBill>();
            this.ReimbursementBillViewModelList = new List<ReimbursementBillViewModel>();
            this.Comments = new List<mComments>();
            this.Pagination = new Pagination();

        }
        public mEstablishBill mEstablishBill { get; set; }
        public List<mEstablishBill> EstablishBillLsit { get; set; }
        public mReimbursementBill mReimbursementBill { get; set; }
        public List<mReimbursementBill> ReimbursementBillList { get; set; }
        public List<mComments> Comments { get; set; }
        public Pagination Pagination { get; set; }
        public int BillNumber { get; set; }
        public decimal SanctionAmount { get; set; }
        public decimal GrossAmount { get; set; }
        public int MainEstablishBillId { get; set; }
        public List<ReimbursementBillViewModel> ReimbursementBillViewModelList { get; set; }
        

    }
}