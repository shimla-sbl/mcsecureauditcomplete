﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    public class DebitReceiptExpViewModel
    {

        public int DebitReceiptExpID { get; set; }

        public int BudgetId { get; set; }

        [Required(ErrorMessage = "Debit Receipt Date  Required")]
        public DateTime DebitReceiptDate { get; set; }

        [Required(ErrorMessage = "Debit Receipt Amount Required")]
        public decimal DebitReceiptAmount { get; set; }

        // public decimal DebitDeduction { get; set; }

        //[Required(ErrorMessage = "Reduction Amount Taken Required")]
        //public decimal ReductionAmountTaken { get; set; }


        //[Required(ErrorMessage = "Net Amount Required")]
        //public decimal NetAmount { get; set; }
        public string Remarks { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }


        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public string BudgetName { get; set; }
        public string Mode { get; set; }
        public SelectList BudgetList { get; set; }

    }
}