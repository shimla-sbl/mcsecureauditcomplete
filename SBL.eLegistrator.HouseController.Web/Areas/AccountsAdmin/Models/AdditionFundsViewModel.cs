﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    public class AdditionFundsViewModel
    {

        public int AdditionFundsID { get; set; }


        public int BudgetId { get; set; }

        [Required(ErrorMessage = "Additional Fund Required")]
        public decimal AdditionalFund { get; set; }

        [Required(ErrorMessage = "Fund Date Required")]
        public DateTime FundDate { get; set; }
        [Required(ErrorMessage = "Fund Type Required")]
        public string FundType { get; set; }

        public string Remarks { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }


        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public string BudgetName { get; set; }
        public string Mode { get; set; }
        public SelectList FundTypeList { get; set; }
    }
}