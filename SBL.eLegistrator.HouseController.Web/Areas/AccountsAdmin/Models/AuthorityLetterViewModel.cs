﻿using SBL.DomainModel.Models.Budget;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    public class AuthorityLetterViewModel
    {
        public int AuthorityLetterID { get; set; }

        [Required(ErrorMessage = "Encashment Date Required")]
        public DateTime EncashmentDate { get; set; }

        [Required(ErrorMessage = "Letter Type Required")]
        public string LetterType { get; set; }

        public string BillIds { get; set; }
        public string BillNumbers { get; set; }
        public string Remarks { get; set; }
        public string AuthorityBy { get; set; }
        public string FileName { get; set; }
        public bool IsActive { get; set; }
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }


        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public string BudgetName { get; set; }
        public string Mode { get; set; }
        public SelectList LetterTypeList { get; set; }
        public List<mEstablishBill> BillList { get; set; }


        public string RemAcNo { get; set; }
        public string RemName { get; set; }
        public string RemAdd { get; set; }
        public string EmpName { get; set; }
        public string EmpEmailId { get; set; }
        public string EmpMobile { get; set; }


    }
}