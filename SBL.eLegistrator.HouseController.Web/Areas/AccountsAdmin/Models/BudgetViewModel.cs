﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    [Serializable]
    public class BudgetViewModel
    {
        public int BudgetID { get; set; }

        // [Required(ErrorMessage = "ObjectRequired")]
        public string Object { get; set; }

        [Required(ErrorMessage = "DDO Code")]
        public string DDOCode { get; set; }

        [Required(ErrorMessage = "Treasury Code")]
        public string TreasuryCode { get; set; }

        [Required(ErrorMessage = "Demand No.")]
        public string DemandNo { get; set; }
        [Required(ErrorMessage = "Gazetted/Non Gazetted")]
        public string Gazetted { get; set; }
        [Required(ErrorMessage = "Major Head")]
        public string MajorHead { get; set; }
        [Required(ErrorMessage = "Sub Major Head")]
        public string SubMajorHead { get; set; }
        [Required(ErrorMessage = "Minor Head")]
        public string MinorHead { get; set; }
        [Required(ErrorMessage = "Sub Head")]
        public string SubHead { get; set; }
        [Required(ErrorMessage = "Budget Code")]
        public string BudgetCode { get; set; }
        [Required(ErrorMessage = "Object Code")]
        public string ObjectCode { get; set; }
        [Required(ErrorMessage = "Plan")]
        public string Plan { get; set; }
        [Required(ErrorMessage = "Voted/Charged")]
        public string VotedCharged { get; set; }

        public bool DisplayInReport { get; set; }

        [Required(ErrorMessage = "Financial Year From")]
        public DateTime FYFromDate { get; set; }

        [Required(ErrorMessage = "Financial Year To")]
        public DateTime FYToDate { get; set; }

        public string FinancialYear { get; set; }
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public string ObjectCodeText { get; set; }
        public int AllocateCount { get; set; }
        public string Mode { get; set; }
        public string Msg { get; set; }

        public bool IsShowExpenseReport { get; set; }
        public int BudgetType { get; set; }
        public int? OrderBy { get; set; }
        public decimal AllocateFund { get; set; }

        public SelectList GazatedList { get; set; }
        public SelectList PlanList { get; set; }
        public SelectList VotedList { get; set; }
        public SelectList FinancialYearList { get; set; }
        public SelectList BillTypeList { get; set; }
        public string BudgetTypeName { get; set; }
        public int BudgetHeadOrder { get; set; }
    }
}