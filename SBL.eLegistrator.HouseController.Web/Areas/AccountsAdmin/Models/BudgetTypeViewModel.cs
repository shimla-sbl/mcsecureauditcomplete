﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    public class BudgetTypeViewModel
    {
        public int VBudgetTypeID { get; set; }

        [Required(ErrorMessage = "Budget Type Name is Required")]
        public string VTypeName { get; set; }
        
        public bool VIsActive { get; set; }

        
        public string VMode { get; set; }

        
    }
}