﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    public class SubVoucherViewModel
    {
        public int SubVoucherID { get; set; }
        //[Required(ErrorMessage = "Sub Voucher No. Required")]
        //public int SubVoucherNo { get; set; }
        [Required(ErrorMessage = "Sub Voucher Amount Required")]
        public decimal SubVoucherAmt { get; set; }
        [Required(ErrorMessage = "Claimant Name Required")]
        public int MemberCode { get; set; }

        public string ClaimantName { get; set; }


        [Required(ErrorMessage = "Claimant Bill No. Required")]
        public string ClaimantBillNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Claimant Bill Date Required")]
        public DateTime ClaimantBillDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Claimant Period From Required")]
        public DateTime ClaimantPeriodFrom { get; set; }
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Claimant Period To Required")]
        public DateTime ClaimantPeriodTo { get; set; }
        [Required(ErrorMessage = "Sanction No. Required")]
        public int SanctionNo { get; set; }
        [Required(ErrorMessage = "Sanction Date Required")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime SanctionDate { get; set; }
        //[Required(ErrorMessage = "Gpf No Required")]
        public string GpfNo { get; set; }

        public int ReimbursementBillId { get; set; }

        public SelectList MembersList { get; set; }
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }


        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public string Mode { get; set; }
      
    }
}