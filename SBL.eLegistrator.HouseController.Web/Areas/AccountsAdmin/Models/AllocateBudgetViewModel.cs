﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    public class AllocateBudgetViewModel
    {

        public int AllocateBudgetID { get; set; }


        public int BudgetId { get; set; }

        [Required(ErrorMessage = "Sanctioned budget")]
        public decimal Sanctionedbudget { get; set; }

        [Required(ErrorMessage = "Allocation  Date")]
        public DateTime AllocateBudgetDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }


        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public string BudgetName { get; set; }
        public string Mode { get; set; }
        public SelectList FinancialYearList { get; set; }
    }
}