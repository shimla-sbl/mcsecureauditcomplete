﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    public class ReimbursementBillViewModel
    {

        public int ReimbursementBillID { get; set; }
        public int EstablishBillId { get; set; }

        public int BudgetId { get; set; }
        public int Status { get; set; }
        [Required(ErrorMessage = "Memorandum Type Required")]
        public int BillType { get; set; }
        public string DesignationS { get; set; }
        [Required(ErrorMessage = "Gross Amount Required")]
        public decimal GrossAmount { get; set; }
        [Required(ErrorMessage = "Deduction Required")]
        public decimal Deduction { get; set; }
        public string Comette { get; set; }
        public string StationName { get; set; }
        public string AlongWith { get; set; }
        public string JourneyBy { get; set; }
        public decimal TakenAmount { get; set; }
        public decimal SactionAmount { get; set; }
        public string Number { get; set; }
        public string PDFUrl { get; set; }
        public DateTime? BillFromDate { get; set; }
        public DateTime? BillToDate { get; set; }
        public DateTime? SactionDate { get; set; }
        public string SactionNumber { get; set; }
        public int AdvanceBillNo { get; set; }
        public int Designation { get; set; }
        public int ClaimantId { get; set; }
        public DateTime? DateOfPresentation { get; set; }
        public DateTime? EncashmentDate { get; set; }
        public bool IsChallanGenerate { get; set; }
        public bool IsEncashementInfoUpdate { get; set; }
        public int EstblishBillNumber { get; set; }
        public string Remarks { get; set; }
        public bool IsSanctioned { get; set; }
        public bool IsValidAuthority { get; set; }
        public bool IsSetEncashment { get; set; }
        public bool IsSOApproved { get; set; }
        public bool IsPDFAttached { get; set; }
        public bool IsRejected { get; set; }
        public DateTime? TakenAmountDate { get; set; }
        public string ClaimantName { get; set; }
        public string HospitalName { get; set; }
        public string MedicalShopName { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        public DateTime? CreationDate { get; set; }
        public string CreatedBy { get; set; }
        public string BudgetName { get; set; }
        public string FinancialYear { get; set; }
        public string Mode { get; set; }
        public SelectList BudgetList { get; set; }
        public SelectList BillTypeList { get; set; }
        public SelectList DesignationList { get; set; }
        public SelectList MemberList { get; set; }
        public SelectList NomineeList { get; set; }
        public string Prefix { get; set; }
        public string Mobile { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Length { get; set; }
        public int NomineeId { get; set; }

        public string BillTypeName { get; set; }

        public DateTime? BillDateOfVoucher { get; set; }
        public string BillNumberOfVoucher { get; set; }
        public bool IsCanceled { get; set; }


        public string BillDateOfVoucherS { get; set; }
        public string SactionDateS { get; set; }
        public string BillFromDateS { get; set; }
        public string BillToDateS { get; set; }
        public string DateOfPresentationS { get; set; }
        public string TakenAmountDateS { get; set; }

        public decimal RetirementGratuity { get; set; }
        public decimal LeaveEncashment { get; set; }
        public decimal LTC { get; set; }
        public decimal MiscAG { get; set; }
        public decimal MiscBT { get; set; }

    }
}