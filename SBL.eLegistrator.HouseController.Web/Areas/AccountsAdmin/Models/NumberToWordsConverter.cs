﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    public static class NumberToWordsConverter
    {
        public static string Convert(decimal number)
        {
            if (number == 0)
                return "ZERO";

            if (number < 0)
                return "MINUS " + Convert(Math.Abs(number));

            string words = String.Empty;

            long intPortion = (long)number;
            decimal fraction = (number - intPortion);
            int decimalPrecision = GetDecimalPrecision(number);

            fraction = CalculateFraction(decimalPrecision, fraction);

            long decPortion = (long)fraction;

            words = IntToWords(intPortion);
            if (decPortion > 0)
            {
                words += " POINT ";
                words += IntToWords(decPortion);
            }

            return words.Trim();
        }

        public static string IntToWords(long number)
        {
            if (number == 0)
                return "ZERO";

            if (number < 0)
                return "MINUS " + IntToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000000000000) > 0)
            {
                words += IntToWords(number / 1000000000000000) + " QUADRILLION ";
                number %= 1000000000000000;
            }

            if ((number / 1000000000000) > 0)
            {
                words += IntToWords(number / 1000000000000) + " TRILLION ";
                number %= 1000000000000;
            }

            if ((number / 1000000000) > 0)
            {
                words += IntToWords(number / 1000000000) + " BILLION ";
                number %= 1000000000;
            }

            if ((number / 1000000) > 0)
            {
                words += IntToWords(number / 1000000) + " MILLION ";
                number %= 1000000;
            }

            if ((number / 100000) > 0)
            {
                words += IntToWords(number / 100000) + " LAKH ";
                number %= 100000;
            }
            if ((number / 1000) > 0)
            {
                words += IntToWords(number / 1000) + " THOUSAND ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += IntToWords(number / 100) + " HUNDRED ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != String.Empty)
                    words += "AND ";

                var unitsMap = new[] { "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN" };
                var tensMap = new[] { "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words.Trim();
        }
        public static string NumbersToWords(Int64 inputNumber)
        {
            Int64 inputNo = inputNumber;

            if (inputNo == 0)
                return "Zero";

            Int64[] numbers = new Int64[4];
            Int64 first = 0;
            Int64 u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (inputNo < 0)
            {
                sb.Append("Minus ");
                inputNo = -inputNo;
            }

            string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ",
            "Five " ,"Six ", "Seven ", "Eight ", "Nine "};
            string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
            "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};
            string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
            "Seventy ","Eighty ", "Ninety "};
            string[] words3 = { "Thousand ", "Lac ", "Crore " };

            numbers[0] = inputNo % 1000; // units
            numbers[1] = inputNo / 1000;
            numbers[2] = inputNo / 100000;
            numbers[1] = numbers[1] - 100 * numbers[2]; // thousands
            numbers[3] = inputNo / 10000000; // crores
            numbers[2] = numbers[2] - 100 * numbers[3]; // lakhs

            for (int i = 3; i > 0; i--)
            {
                if (numbers[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (Int64 i = first; i >= 0; i--)
            {
                if (numbers[i] == 0) continue;
                u = numbers[i] % 10; // ones
                t = numbers[i] / 10;
                h = numbers[i] / 100; // hundreds
                t = t - 10 * h; // tens
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i == 0) sb.Append("and ");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            sb.Append(" only");
            return sb.ToString().TrimEnd().ToUpper();
        }


        private static int GetDecimalPrecision(decimal number)
        {
            return (Decimal.GetBits(number)[3] >> 16) & 0x000000FF;
        }

        private static decimal CalculateFraction(int decimalPrecision, decimal fraction)
        {
            switch (decimalPrecision)
            {
                case 1:
                    return fraction * 10;
                case 2:
                    return fraction * 100;
                case 3:
                    return fraction * 1000;
                case 4:
                    return fraction * 10000;
                case 5:
                    return fraction * 100000;
                case 6:
                    return fraction * 1000000;
                case 7:
                    return fraction * 10000000;
                case 8:
                    return fraction * 100000000;
                case 9:
                    return fraction * 1000000000;
                case 10:
                    return fraction * 10000000000;
                case 11:
                    return fraction * 100000000000;
                case 12:
                    return fraction * 1000000000000;
                case 13:
                    return fraction * 10000000000000;
                default:
                    return fraction * 10000000000000;
            }
        }
    }
}