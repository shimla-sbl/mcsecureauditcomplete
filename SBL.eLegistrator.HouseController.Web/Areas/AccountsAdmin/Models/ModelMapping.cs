﻿using SBL.DomainModel.Models.Budget;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.EnterpriseServices;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    public static class ModelMapping
    {
        // Extenstion Method for Budget
        public static List<BudgetViewModel> ToViewModel(this IEnumerable<mBudget> model)
        {

            var ViewModel = model.Select(part => new BudgetViewModel()
            {
                BudgetID = part.BudgetID,
                Object = part.Object,
                DDOCode = part.DDOCode,
                TreasuryCode = part.TreasuryCode,
                DemandNo = part.DemandNo,
                Gazetted = part.Gazetted,
                BudgetTypeName = part.BudgetTypeName,
                MajorHead = part.MajorHead,
                BudgetType = part.BudgetType,
                IsShowExpenseReport = part.IsShowExpenseReport,
                SubMajorHead = part.SubMajorHead,
                MinorHead = part.MinorHead,
                SubHead = part.SubHead,
                AllocateFund = part.AllocateFund,
                BudgetCode = part.BudgetCode,
                ObjectCode = part.ObjectCode,
                ObjectCodeText = part.ObjectCodeText,
                OrderBy = part.OrderBy,
                Plan = part.Plan,
                VotedCharged = part.VotedCharged,
                FYFromDate = part.FYFromDate,
                FYToDate = part.FYToDate,
                DisplayInReport = part.DisplayInReport,
                FinancialYear = part.FinancialYear,
                AllocateCount = part.AllocateCount,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                BudgetHeadOrder = part.BudgetHeadOrder
            });
            return ViewModel.OrderBy(x=>x.BudgetHeadOrder).ToList();
        }

        public static BudgetViewModel ToViewModel1(this mBudget model, string Mode)
        {
            return new BudgetViewModel
            {
                BudgetID = model.BudgetID,
                Object = model.Object,
                DDOCode = model.DDOCode,
                TreasuryCode = model.TreasuryCode,
                DemandNo = model.DemandNo,
                Gazetted = model.Gazetted,
                MajorHead = model.MajorHead,
                BudgetTypeName = model.BudgetTypeName,
                SubMajorHead = model.SubMajorHead,
                MinorHead = model.MinorHead,
                SubHead = model.SubHead,
                BudgetCode = model.BudgetCode,
                ObjectCode = model.ObjectCode,
                ObjectCodeText = model.ObjectCodeText,
                OrderBy=model.OrderBy,
                Plan = model.Plan,
                AllocateFund = model.AllocateFund,
                BudgetType = model.BudgetType,
                IsShowExpenseReport = model.IsShowExpenseReport,
                VotedCharged = model.VotedCharged,
                DisplayInReport = model.DisplayInReport,
                FYFromDate = model.FYFromDate,
                FYToDate = model.FYToDate,
                FinancialYear = model.FinancialYear,
                AllocateCount = model.AllocateCount,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static mBudget ToDomainModel(this BudgetViewModel model)
        {
            return new mBudget
            {
                BudgetID = model.BudgetID,
                Object = model.Object,
                DDOCode = model.DDOCode,
                TreasuryCode = model.TreasuryCode,
                DemandNo = model.DemandNo,
                Gazetted = model.Gazetted,
                MajorHead = model.MajorHead,
                SubMajorHead = model.SubMajorHead,
                MinorHead = model.MinorHead,
                OrderBy = model.OrderBy,
                BudgetTypeName = model.BudgetTypeName,
                SubHead = model.SubHead,
                AllocateFund = model.AllocateFund,
                BudgetType = model.BudgetType,
                IsShowExpenseReport = model.IsShowExpenseReport,
                BudgetCode = model.BudgetCode,
                ObjectCode = model.ObjectCode,
                ObjectCodeText = model.ObjectCodeText,
                Plan = model.Plan,
                VotedCharged = model.VotedCharged,
                FYFromDate = model.FYFromDate,
                FYToDate = model.FYToDate,
                DisplayInReport = model.DisplayInReport,
                FinancialYear = model.FinancialYear,
                AllocateCount = model.AllocateCount,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                BudgetHeadOrder=model.BudgetHeadOrder
            };
        }

        // Extenstion Method for AllocateBudget

        public static List<AllocateBudgetViewModel> ToViewModel(this IEnumerable<AllocateBudget> model)
        {

            var ViewModel = model.Select(part => new AllocateBudgetViewModel()
            {
                AllocateBudgetID = part.AllocateBudgetID,
                BudgetId = part.BudgetId,
                Sanctionedbudget = part.Sanctionedbudget,
                AllocateBudgetDate = part.AllocateBudgetDate,
                BudgetName = part.BudgetName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            });
            return ViewModel.ToList();
        }

        public static AllocateBudgetViewModel ToViewModel1(this AllocateBudget model, string Mode)
        {
            return new AllocateBudgetViewModel
            {
                AllocateBudgetID = model.AllocateBudgetID,
                BudgetId = model.BudgetId,
                Sanctionedbudget = model.Sanctionedbudget,
                AllocateBudgetDate = model.AllocateBudgetDate,
                BudgetName = model.BudgetName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static AllocateBudget ToDomainModel(this AllocateBudgetViewModel model)
        {
            return new AllocateBudget
            {
                AllocateBudgetID = model.AllocateBudgetID,
                BudgetId = model.BudgetId,
                Sanctionedbudget = model.Sanctionedbudget,
                AllocateBudgetDate = model.AllocateBudgetDate,
                BudgetName = model.BudgetName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }

        // Extenstion Method for Addition Funds

        public static List<AdditionFundsViewModel> ToViewModel(this IEnumerable<AdditionFunds> model)
        {

            var ViewModel = model.Select(part => new AdditionFundsViewModel()
            {
                AdditionFundsID = part.AdditionFundsID,
                BudgetId = part.BudgetId,
                AdditionalFund = part.AdditionalFund,
                FundDate = part.FundDate,
                Remarks = part.Remarks,
                FundType = part.FundType,
                BudgetName = part.BudgetName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            });
            return ViewModel.ToList();
        }

        public static AdditionFundsViewModel ToViewModel1(this AdditionFunds model, string Mode)
        {
            return new AdditionFundsViewModel
            {
                AdditionFundsID = model.AdditionFundsID,
                BudgetId = model.BudgetId,
                AdditionalFund = model.AdditionalFund,
                FundDate = model.FundDate,
                Remarks = model.Remarks,
                FundType = model.FundType,
                BudgetName = model.BudgetName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static AdditionFunds ToDomainModel(this AdditionFundsViewModel model)
        {
            return new AdditionFunds
            {
                AdditionFundsID = model.AdditionFundsID,
                BudgetId = model.BudgetId,
                AdditionalFund = model.AdditionalFund,
                FundDate = model.FundDate,
                Remarks = model.Remarks,
                FundType = model.FundType,
                BudgetName = model.BudgetName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }

        // Extenstion Method for Reimbursement Bill  

        public static List<ReimbursementBillViewModel> ToViewModel(this IEnumerable<mReimbursementBill> model)
        {

            var ViewModel = model.Select(part => new ReimbursementBillViewModel()
            {
                ReimbursementBillID = part.ReimbursementBillID,
                BudgetId = part.BudgetId,
                BillType = part.BillType,
                Status = part.Status,
                MiscAG = part.MiscAG,
                MiscBT = part.MiscBT,
                IsCanceled = part.IsCanceled,
                AlongWith = part.AlongWith,
                JourneyBy = part.JourneyBy,
                EstblishBillNumber = part.EstblishBillNumber,
                BillTypeName = part.BillTypeName,
                StationName=part.StationName,
                PDFUrl = part.PDFUrl,
                IsPDFAttached = part.IsPDFAttached,
                DesignationS = part.DesignationS,
                GrossAmount = part.GrossAmount,
                SactionAmount = part.SactionAmount,
                Prefix = part.Prefix,
                Comette = part.Comette,
                EstablishBillId = part.EstablishBillId,
                IsChallanGenerate = part.IsChallanGenerate,
                Mobile = part.Mobile,
                BillFromDate = DateTime.Parse(part.BillFromDate != null ? part.BillFromDate.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()),
                BillToDate = DateTime.Parse(part.BillToDate != null ? part.BillToDate.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()),
                SactionDate = DateTime.Parse(part.SactionDate != null ? part.SactionDate.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()),
                SactionNumber = part.SactionNumber,
                Designation = part.Designation,
                ClaimantId = part.ClaimantId,
                Number = part.Number,
                Deduction = part.Deduction,
                DateOfPresentation = DateTime.Parse(part.DateOfPresentation != null ? part.DateOfPresentation.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()),
                IsRejected = part.IsRejected,
                IsSOApproved = part.IsValidAuthority,
                EncashmentDate = DateTime.Parse(part.EncashmentDate != null ? part.EncashmentDate.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()),
                Remarks = part.Remarks,
                FinancialYear = part.FinancialYear,
                BudgetName = part.BudgetName,
                IsValidAuthority = part.IsValidAuthority,
                IsSetEncashment = part.IsSetEncashment,
                IsSanctioned = part.IsSanctioned,

                ClaimantName = part.ClaimantName,
                HospitalName = part.HospitalName,
                MedicalShopName = part.MedicalShopName,
                BillDateOfVoucher = part.BillDateOfVoucher,
                BillNumberOfVoucher = part.BillNumberOfVoucher,
                RetirementGratuity = part.RetirementGratuity,
                LeaveEncashment = part.LeaveEncashment,
                LTC = part.LTC,
                CreatedBy = part.CreatedBy,
                ModifiedBy = CurrentSession.UserName,
                NomineeId = part.NomineeId,


            });
            return ViewModel.ToList();
        }

        public static ReimbursementBillViewModel ToViewModel1(this mReimbursementBill model, string Mode)
        {
            return new ReimbursementBillViewModel
            {
                ReimbursementBillID = model.ReimbursementBillID,
                Status = model.Status,
                BudgetId = model.BudgetId,
                BillType = model.BillType,
                GrossAmount = model.GrossAmount,
                MiscAG = model.MiscAG,
                MiscBT = model.MiscBT,
                IsPDFAttached = model.IsPDFAttached,
                BillTypeName = model.BillTypeName,
                AlongWith = model.AlongWith,
                JourneyBy = model.JourneyBy,
                Deduction = model.Deduction,
                StationName = model.StationName,
                EstblishBillNumber = model.EstblishBillNumber,
                IsCanceled = model.IsCanceled,
                DesignationS = model.DesignationS,
                PDFUrl = model.PDFUrl,
                DateOfPresentation = DateTime.Parse(model.DateOfPresentation != null ? model.DateOfPresentation.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()),
                BillFromDate = DateTime.Parse(model.BillFromDate != null ? model.BillFromDate.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()),
                Comette = model.Comette,
                EstablishBillId = model.EstablishBillId,
                BillToDate = DateTime.Parse(model.BillToDate != null ? model.BillToDate.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()),
                IsChallanGenerate = model.IsChallanGenerate,
                SactionAmount = model.SactionAmount,
                Number = model.Number,
                IsSOApproved = model.IsValidAuthority,
                IsSanctioned = model.IsSanctioned,
                SactionDate = DateTime.Parse(model.SactionDate != null ? model.SactionDate.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()),
                SactionNumber = model.SactionNumber,
                Designation = model.Designation,
                ClaimantId = model.ClaimantId,
                Prefix = model.Prefix,
                Mobile = model.Mobile,
                BudgetName = model.BudgetName,
                IsRejected = model.IsRejected,
                EncashmentDate = DateTime.Parse(model.EncashmentDate != null ? model.EncashmentDate.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()),
                Remarks = model.Remarks,
                FinancialYear = model.FinancialYear,
                IsValidAuthority = model.IsValidAuthority,
                IsSetEncashment = model.IsSetEncashment,

                ClaimantName = model.ClaimantName,
                HospitalName = model.HospitalName,
                MedicalShopName = model.MedicalShopName,
                BillDateOfVoucher = model.BillDateOfVoucher,
                BillNumberOfVoucher = model.BillNumberOfVoucher,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode,
                NomineeId = model.NomineeId,
                BillDateOfVoucherS = model.BillDateOfVoucher != null ? model.BillDateOfVoucher.Value.ToString("dd/MM/yyyy") : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString(),
                SactionDateS = model.SactionDate != null ? model.SactionDate.Value.ToString("dd/MM/yyyy") : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString(),
                BillFromDateS = model.BillFromDate != null ? model.BillFromDate.Value.ToString("dd/MM/yyyy") : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString(),
                BillToDateS = model.BillToDate != null ? model.BillToDate.Value.ToString("dd/MM/yyyy") : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString(),
                DateOfPresentationS = model.DateOfPresentation != null ? model.DateOfPresentation.Value.ToString("dd/MM/yyyy") : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString(),
                RetirementGratuity = model.RetirementGratuity,
                LeaveEncashment = model.LeaveEncashment,
                LTC = model.LTC
            };
        }

        public static mReimbursementBill ToDomainModel(this ReimbursementBillViewModel model)
        {
            return new mReimbursementBill
            {
                ReimbursementBillID = model.ReimbursementBillID,
                BudgetId = model.BudgetId,
                Status = model.Status,
                BillType = model.BillType,
                BillTypeName = model.BillTypeName,
                GrossAmount = model.GrossAmount,
                AlongWith = model.AlongWith,
                MiscAG = model.MiscAG,
                MiscBT = model.MiscBT,
                JourneyBy = model.JourneyBy,
                IsPDFAttached = model.IsPDFAttached,
                Deduction = model.Deduction,
                StationName = model.StationName,
                SactionAmount = model.SactionAmount,
                DateOfPresentation = model.DateOfPresentation,
                DesignationS = model.DesignationS,
                IsCanceled = model.IsCanceled,
                Number = model.Number,
                PDFUrl = model.PDFUrl,
                EstblishBillNumber = model.EstblishBillNumber,
                IsSOApproved = model.IsSOApproved,
                IsChallanGenerate = model.IsChallanGenerate,
                IsSanctioned = model.IsSanctioned,
                BillFromDate = model.BillFromDate,
                BillToDate = model.BillToDate,
                SactionDate = model.SactionDate,
                EstablishBillId = model.EstablishBillId,
                SactionNumber = model.SactionNumber,
                Designation = model.Designation,
                ClaimantId = model.ClaimantId,
                Prefix = model.Prefix,
                Comette = model.Comette,
                Mobile = model.Mobile,
                BudgetName = model.BudgetName,
                IsRejected = model.IsRejected,
                FinancialYear = model.FinancialYear,
                EncashmentDate = DateTime.Parse(model.EncashmentDate != null ? model.EncashmentDate.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString()),
                Remarks = model.Remarks,
                IsValidAuthority = model.IsValidAuthority,
                IsSetEncashment = model.IsSetEncashment,
                ClaimantName = model.ClaimantName,
                HospitalName = model.HospitalName,
                MedicalShopName = model.MedicalShopName,
                BillDateOfVoucher = model.BillDateOfVoucher,
                BillNumberOfVoucher = model.BillNumberOfVoucher,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                NomineeId = model.NomineeId,
                RetirementGratuity = model.RetirementGratuity,
                LeaveEncashment = model.LeaveEncashment,
                LTC = model.LTC
            };
        }

        // Extenstion Method for SubVoucher Bill  

        public static List<SubVoucherViewModel> ToViewModel(this IEnumerable<mSubVoucher> model)
        {

            var ViewModel = model.Select(part => new SubVoucherViewModel()
            {
                SubVoucherID = part.SubVoucherID,
                // SubVoucherNo = part.SubVoucherNo,
                SubVoucherAmt = part.SubVoucherAmt,
                ClaimantName = part.ClaimantName,
                MemberCode = part.MemberCode,
                ClaimantBillNo = part.ClaimantBillNo,
                ClaimantBillDate = part.ClaimantBillDate,
                ClaimantPeriodFrom = part.ClaimantPeriodFrom,
                ClaimantPeriodTo = part.ClaimantPeriodTo,
                SanctionNo = part.SanctionNo,
                SanctionDate = part.SanctionDate,
                GpfNo = part.GpfNo,
                ReimbursementBillId = part.ReimbursementBillId,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            });
            return ViewModel.ToList();
        }

        public static SubVoucherViewModel ToViewModel1(this mSubVoucher model, string Mode)
        {
            return new SubVoucherViewModel
            {
                SubVoucherID = model.SubVoucherID,
                //   SubVoucherNo = model.SubVoucherNo,
                SubVoucherAmt = model.SubVoucherAmt,
                ClaimantName = model.ClaimantName,
                ClaimantBillNo = model.ClaimantBillNo,
                MemberCode = model.MemberCode,
                ClaimantBillDate = model.ClaimantBillDate,
                ClaimantPeriodFrom = model.ClaimantPeriodFrom,
                ClaimantPeriodTo = model.ClaimantPeriodTo,
                SanctionNo = model.SanctionNo,

                SanctionDate = model.SanctionDate,
                GpfNo = model.GpfNo,
                ReimbursementBillId = model.ReimbursementBillId,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static mSubVoucher ToDomainModel(this SubVoucherViewModel model)
        {
            return new mSubVoucher
            {
                SubVoucherID = model.SubVoucherID,
                //   SubVoucherNo = model.SubVoucherNo,
                SubVoucherAmt = model.SubVoucherAmt,
                ClaimantName = model.ClaimantName,
                MemberCode = model.MemberCode,
                ClaimantBillNo = model.ClaimantBillNo,
                ClaimantBillDate = model.ClaimantBillDate,
                ClaimantPeriodFrom = model.ClaimantPeriodFrom,
                ClaimantPeriodTo = model.ClaimantPeriodTo,
                SanctionNo = model.SanctionNo,
                SanctionDate = model.SanctionDate,
                GpfNo = model.GpfNo,
                ReimbursementBillId = model.ReimbursementBillId,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }


        // Extenstion Method for Debit Receipt Expense

        public static List<DebitReceiptExpViewModel> ToViewModel(this IEnumerable<mDebitReceiptExp> model)
        {

            var ViewModel = model.Select(part => new DebitReceiptExpViewModel()
            {
                DebitReceiptExpID = part.DebitReceiptExpID,
                BudgetId = part.BudgetId,
                DebitReceiptDate = part.DebitReceiptDate,
                DebitReceiptAmount = part.DebitReceiptAmount,
                //DebitDeduction = part.DebitDeduction,
               // ReductionAmountTaken = part.ReductionAmountTaken,
                Remarks = part.Remarks,
                BudgetName = part.BudgetName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            });
            return ViewModel.ToList();
        }

        public static DebitReceiptExpViewModel ToViewModel1(this mDebitReceiptExp model, string Mode)
        {
            return new DebitReceiptExpViewModel
            {
                DebitReceiptExpID = model.DebitReceiptExpID,
                BudgetId = model.BudgetId,
                DebitReceiptDate = model.DebitReceiptDate,
                DebitReceiptAmount = model.DebitReceiptAmount,
                //DebitDeduction = model.DebitDeduction,
                //ReductionAmountTaken = model.ReductionAmountTaken,
                Remarks = model.Remarks,
                BudgetName = model.BudgetName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static mDebitReceiptExp ToDomainModel(this DebitReceiptExpViewModel model)
        {
            return new mDebitReceiptExp
            {
                DebitReceiptExpID = model.DebitReceiptExpID,
                BudgetId = model.BudgetId,
                DebitReceiptDate = model.DebitReceiptDate,
                DebitReceiptAmount = model.DebitReceiptAmount,
                //DebitDeduction = model.DebitDeduction,
                //ReductionAmountTaken = model.ReductionAmountTaken,
                Remarks = model.Remarks,
                BudgetName = model.BudgetName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }

        // Extenstion Method for Authority Letter 

        public static List<AuthorityLetterViewModel> ToViewModel(this IEnumerable<mAuthorityLetter> model)
        {

            var ViewModel = model.Select(part => new AuthorityLetterViewModel()
            {
                AuthorityLetterID = part.AuthorityLetterID,
                EncashmentDate = part.EncashmentDate,
                LetterType = part.LetterType,
                BillIds = part.BillIds,
                FileName = part.FileName,
                IsActive = part.IsActive,
                BillNumbers = part.BillNumbers,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            });
            return ViewModel.ToList();
        }

        public static AuthorityLetterViewModel ToViewModel1(this mAuthorityLetter model, string Mode)
        {
            return new AuthorityLetterViewModel
            {
                AuthorityLetterID = model.AuthorityLetterID,
                EncashmentDate = model.EncashmentDate,
                LetterType = model.LetterType,
                BillIds = model.BillIds,
                FileName = model.FileName,
                CreationDate = model.CreationDate,
                IsActive = model.IsActive,
                BillNumbers = model.BillNumbers,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static mAuthorityLetter ToDomainModel(this AuthorityLetterViewModel model)
        {
            return new mAuthorityLetter
            {
                AuthorityLetterID = model.AuthorityLetterID,
                EncashmentDate = model.EncashmentDate,
                LetterType = model.LetterType,
                BillIds = model.BillIds,
                FileName = model.FileName,
                IsActive = model.IsActive,
                BillNumbers = model.BillNumbers,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }



        #region BudgetBillType



        public static mBudgetBillTypes ToDomainModel(this BudgetBillTypeViewModel model)
        {
            return new mBudgetBillTypes
            {

                BillTypeID = model.VBillTypeID,
                TypeName = model.VTypeName,
                IsActive = model.VIsActive



            };
        }


        public static List<BudgetBillTypeViewModel> ToViewModel(this IEnumerable<mBudgetBillTypes> model)
        {

            var ViewModel = model.Select(part => new BudgetBillTypeViewModel()
            {
                VBillTypeID = part.BillTypeID,
                VTypeName = part.TypeName,
                VIsActive = part.IsActive


            });
            return ViewModel.ToList();
        }



        public static BudgetBillTypeViewModel ToViewModel1(this mBudgetBillTypes model, string Mode)
        {
            return new BudgetBillTypeViewModel
            {
                VBillTypeID = model.BillTypeID,
                VTypeName = model.TypeName,
                VIsActive = model.IsActive,


                VMode = Mode

            };
        }



        #endregion


        #region BudgetType



        public static mBudgetType ToDomainModel(this BudgetTypeViewModel model)
        {
            return new mBudgetType
            {

                BudgetTypeID = model.VBudgetTypeID,
                TypeName = model.VTypeName,
                IsActive = model.VIsActive



            };
        }


        public static List<BudgetTypeViewModel> ToViewModel(this IEnumerable<mBudgetType> model)
        {

            var ViewModel = model.Select(part => new BudgetTypeViewModel()
            {
                VBudgetTypeID = part.BudgetTypeID,
                VTypeName = part.TypeName,
                VIsActive = part.IsActive


            });
            return ViewModel.ToList();
        }



        public static BudgetTypeViewModel ToViewModel1(this mBudgetType model, string Mode)
        {
            return new BudgetTypeViewModel
            {
                VBudgetTypeID = model.BudgetTypeID,
                VTypeName = model.TypeName,
                VIsActive = model.IsActive,


                VMode = Mode

            };
        }



        #endregion



        #region Private
        public static string ShowFinacialYear(this string FinacialYear)
        {
            if (!string.IsNullOrEmpty(FinacialYear))
                return string.Format("April {0} - March {1}", FinacialYear.Split('-')[0], FinacialYear.Split('-')[1]);
            else
                return string.Empty;

        }
        public static SelectList GetFinancialYear(string SelectedValue)
        {

            var result = new List<SelectListItem>();
            for (int i = 5; i >= -5; i--)
            {
                int CurrentYear = DateTime.Today.AddYears(i).Year;
                int PreviousYear = DateTime.Today.AddYears(i).Year - 1;
                int NextYear = DateTime.Today.AddYears(i).Year + 1;
                string PreYear = PreviousYear.ToString();
                string NexYear = NextYear.ToString();
                string CurYear = CurrentYear.ToString();
                string FinYearText = null;
                string FinYearValue = null;
                if (DateTime.Today.Month > 3)
                {
                    FinYearText = string.Format("April {0} - March {1}", CurYear, NexYear);
                    FinYearValue = string.Format("{0}-{1}", CurYear, NexYear);
                }
                else
                {
                    FinYearText = string.Format("April {0} - March {1}", PreYear, CurYear);
                    FinYearValue = string.Format("{0}-{1}", PreYear, CurYear);
                }

                result.Add(new SelectListItem() { Text = FinYearText, Value = FinYearValue });

            }


            return new SelectList(result, "Value", "Text", SelectedValue);
        }

        public static SelectList GetGazatedList()
        {
            var result = new List<SelectListItem>();

             result.Add(new SelectListItem() { Text = "SPK/ DSPK", Value = "SPK/DSPK" });
             result.Add(new SelectListItem() { Text = "Member", Value = "Member" });
             result.Add(new SelectListItem() { Text = "Gazetted", Value = "Gazetted" });
             result.Add(new SelectListItem() { Text = "Gazetted / Non Gazetted", Value = "Gazetted/NonGazetted" });
             result.Add(new SelectListItem() { Text = "NA", Value = "NA" });
             result.Add(new SelectListItem() { Text = "Other", Value = "Other" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetPlanList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Plan", Value = "Plan" });
            result.Add(new SelectListItem() { Text = "NP", Value = "NP" });
            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetVotedList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Voted", Value = "Voted" });
            result.Add(new SelectListItem() { Text = "Charged", Value = "Charged" });
            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetFundType()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Additional", Value = "Additional" });
            result.Add(new SelectListItem() { Text = "Suplementary", Value = "Suplementary" });
            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetBillType()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Staff Gazetted", Value = "StaffGazetted" });
            result.Add(new SelectListItem() { Text = "Staff Non-Gazetted", Value = "StaffNonGazetted" });
            result.Add(new SelectListItem() { Text = "Speaker", Value = "Speaker" });
            result.Add(new SelectListItem() { Text = "Deputy Speaker", Value = "DeputySpeaker" });
            result.Add(new SelectListItem() { Text = "Member", Value = "Member" });
            result.Add(new SelectListItem() { Text = "Ex-member or their nominee", Value = "ExMember" });
            result.Add(new SelectListItem() { Text = "other", Value = "other" });
            return new SelectList(result, "Value", "Text");
        }


        public static SelectList GetAccompanied()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Alone", Value = "Alone" });
            result.Add(new SelectListItem() { Text = "Spouse", Value = "Spouse" });
            result.Add(new SelectListItem() { Text = "Attendant", Value = "Attendant" });
            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetReimbursementType()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Travel", Value = "Travel" });
            result.Add(new SelectListItem() { Text = "Non Travel", Value = "NonTravel" });
            return new SelectList(result, "Value", "Text");
        }
        public static SelectList GetModeofTravel()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Train", Value = "Train" });
            result.Add(new SelectListItem() { Text = "Air", Value = "Air" });
            result.Add(new SelectListItem() { Text = "Bus", Value = "Bus" });
            result.Add(new SelectListItem() { Text = "Taxi", Value = "Taxi" });
            result.Add(new SelectListItem() { Text = "Member", Value = "Member" });
            result.Add(new SelectListItem() { Text = "Person Vehicle", Value = "PersonVehicle" });
            return new SelectList(result, "Value", "Text");
        }
        public static string GetDescription(this Enum enumValue)
        {
            try
            {
                object[] attr = enumValue.GetType().GetField(enumValue.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);

                return attr.Length > 0
                   ? ((DescriptionAttribute)attr[0]).ToString()
                   : enumValue.ToString();
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex )
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                return "";
            }
           
        }

        public static T ParseEnum<T>(this string stringVal)
        {
            return (T)Enum.Parse(typeof(T), stringVal);

        }

        public static string GetDisplayName(this Enum enumValue)
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<DisplayAttribute>()
                            .Name;
        }

        public static SelectList GetMonthsList(string Month)
        {
            string months = Month;
            string[] parts = months.Split('-');
            string fist = parts[0].ToString();
            string Second = parts[1].ToString();
            var result = new List<SelectListItem>();
            result.Add(new SelectListItem() { Text = "April" + "-" + fist, Value = "4" });
            result.Add(new SelectListItem() { Text = "May" + "-" + fist, Value = "5" });
            result.Add(new SelectListItem() { Text = "June" + "-" +fist, Value = "6" });
            result.Add(new SelectListItem() { Text = "July" + "-" + fist, Value = "7" });
            result.Add(new SelectListItem() { Text = "August" + "-" + fist, Value = "8" });
            result.Add(new SelectListItem() { Text = "September" + "-" + fist, Value = "9" });
            result.Add(new SelectListItem() { Text = "October" + "-" + fist, Value = "10" });
            result.Add(new SelectListItem() { Text = "November" + "-" + fist, Value = "11" });
            result.Add(new SelectListItem() { Text = "December" + "-" + fist, Value = "12" });
            result.Add(new SelectListItem() { Text = "January" + "-" + Second, Value = "1" });
            result.Add(new SelectListItem() { Text = "February" + "-" + Second, Value = "2" });
            result.Add(new SelectListItem() { Text = "March" + "-" + Second, Value = "3" });
            
            return new SelectList(result, "Value", "Text");
        }

        public static List<string> GetStringMonthsList(string FinacialYear)
        {
          
           
            string fist = FinacialYear.Split('-')[0].ToString();
            string Second = FinacialYear.Split('-')[1].ToString();
            List<string> result = new List<string>();
            result.Add("April" + "-" + fist);
            result.Add("May" + "-" + fist);
            result.Add("June" + "-" + fist);
            result.Add("July" + "-" + fist);
            result.Add("August" + "-" + fist);
            result.Add("September" + "-" + fist);
            result.Add("October" + "-" + fist);
            result.Add("November" + "-" + fist);
            result.Add("December" + "-" + fist);
            result.Add("January" + "-" + Second);
            result.Add("February" + "-" + Second);
            result.Add("March" + "-" + Second);

            return result;
        }
        public static SelectList GetMothsforinitial()
        {
            var result = new List<SelectListItem>();
            result.Add(new SelectListItem() { Text = "Select Month", Value = "0" });          
            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetQuaterList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Q-1 Mar,April,May", Value = "3,4,5" });
            result.Add(new SelectListItem() { Text = "Q-2 Jun,Jul,Aug", Value = "6,7,8" });
            result.Add(new SelectListItem() { Text = "Q-3 Sep,Oct,Noc", Value = "9,10,11" });
            result.Add(new SelectListItem() { Text = "Q-4 Dec,Jan,Feb", Value = "12,1,2" });
            return new SelectList(result, "Value", "Text");
        }

        #endregion
    }
}