﻿using SBL.DomainModel.Models.FormTR2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models
{
    public class HPTR2Model
    {
        public HPTR2Model()
        {
            this.AccountFieldsList = new List<mAccountFields>();
            this.DUESList = new List<mAccountFields>();
            this.AGDList = new List<mAccountFields>();
            this.TDList = new List<mAccountFields>();
            this.HBALoanDetail = new List<TR2LoanDetail>();
            this.EDULoanDetail = new List<TR2LoanDetail>();

        }
        public List<mAccountFields> AccountFieldsList { get; set; }
        public List<mAccountFields> DUESList { get; set; }
        public List<mAccountFields> AGDList { get; set; }
        public List<mAccountFields> TDList { get; set; }

        public List<TR2LoanDetail> HBALoanDetail { get; set; }
        public List<TR2LoanDetail> EDULoanDetail { get; set; }
    }
}