﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.RoadPermit
{
    public class RoadPermitAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "RoadPermit";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "RoadPermit_default",
                "RoadPermit/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
