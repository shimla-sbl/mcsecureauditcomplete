﻿using SBL.DomainModel.Models.Constituency;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.ConstituencyPanchyat.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.ConstituencyPanchyat.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.ConstituencyPanchyat.Controllers
{
    public class ConstituencyPanchyatController : Controller
    {
        //
        // GET: /ConstituencyPanchyat/ConstituencyPanchyat/

        public ActionResult Index()
        {

            return View();

        }

        public ActionResult GetAllConstPanchayat()
        {
            var PanchayatCode = (List<mPanchayat>)Helper.ExecuteService("ConstituencyPanchayat", "GetAllPanchayats", null);
            var model = PanchayatCode.ToViewModelOfPanc();
            return PartialView("_PartialOfIndex", model);
        }

        public ActionResult CreatePanchayat()
        {

            var model = new ConstituencyPanchayatViewModel()
            {


                Mode = "Add",
                IsActive = true

            };
            return View("CreatePanchayat", model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePanchayat(ConstituencyPanchayatViewModel model)
        {

            if (ModelState.IsValid)
            {
                var panchayat = model.ToDomainModel();
                if (model.Mode == "Add")
                {
                    //panchayat.IsActive = true;
                    Helper.ExecuteService("panchayat", "CreatePanchayat", panchayat);
                }
                else
                {
                    //panchayat.IsActive = true;
                    Helper.ExecuteService("panchayat", "UpdatePanchayat", panchayat);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult DeletePanchayat(int Id)
        {
            tConstituencyPanchayat TypeToDelete = (tConstituencyPanchayat)Helper.ExecuteService("panchayat", "ConstituencyPanchayatID", new tConstituencyPanchayat { ConstituencyPanchayatID = Id });
            Helper.ExecuteService("panchayat", "DeletePanchayat", TypeToDelete);
            return RedirectToAction("Index");

        }

        public ActionResult GetAllPanchayats()
        {
            return View();
        }



    }
}
