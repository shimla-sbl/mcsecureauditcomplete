﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.ConstituencyPanchyat
{
    public class ConstituencyPanchyatAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ConstituencyPanchyat";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ConstituencyPanchyat_default",
                "ConstituencyPanchyat/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
