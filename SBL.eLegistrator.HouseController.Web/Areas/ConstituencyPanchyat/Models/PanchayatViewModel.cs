﻿using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace SBL.eLegistrator.HouseController.Web.Areas.ConstituencyPanchyat.Models
{
    public class PanchayatViewModel

    {
        public int PanchayatID { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<mUserModules> MenuList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalValue { get; set; }

        public string PanchayatCode { get; set; }

        public string PanchayatName { get; set; }

        public string PanchayatNameLocal { get; set; }

  
        public bool IsActive { get; set; }


        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string Mode { get; set; }

    

    }
}