﻿using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.ConstituencyPanchyat.Models
{
    public class ConstituencyPanchayatViewModel
    {

        public int ConstituencyPanchayatID { get; set; }



        public int PanchayatCode { get; set; }

        public int StateCode { get; set; }

        public int ConstituencyCode { get; set; }

        public int DistrictCode { get; set; }

  
        public bool IsActive { get; set; }


        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string Mode { get; set; }

    }
}