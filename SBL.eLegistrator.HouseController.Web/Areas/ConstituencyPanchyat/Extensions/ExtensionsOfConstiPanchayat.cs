﻿using SBL.DomainModel.Models.Constituency;
using System;
using System.Collections.Generic;
using SBL.eLegistrator.HouseController.Web.Areas.ConstituencyPanchyat.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.ConstituencyPanchyat.Extensions
{
    public static class ExtensionsOfConstiPanchayat
    {
         public static IEnumerable<ConstituencyPanchayatViewModel> ToViewModel(this IEnumerable<tConstituencyPanchayat> model)
        {

            var ViewModel = model.Select(consti => new ConstituencyPanchayatViewModel()
            {
                ConstituencyPanchayatID = consti.ConstituencyPanchayatID,
                PanchayatCode = consti.PanchayatCode,
                StateCode = consti.StateCode,
                ConstituencyCode = consti.ConstituencyCode,
                DistrictCode = consti.DistrictCode,
                IsActive = consti.IsActive,
               
            });
            return ViewModel;
        }

         public static IEnumerable<PanchayatViewModel> ToViewModelOfPanc(this IEnumerable<mPanchayat> model)
         {

             var ViewModel = model.Select(consti => new PanchayatViewModel()
             {
                 PanchayatID = consti.PanchayatID,
               //  PanchayatCode = consti.PanchayatCode,
                 PanchayatName = consti.PanchayatName,
                 PanchayatNameLocal = consti.PanchayatNameLocal,
                 //DistrictCode = consti.DistrictCode,
                 //IsActive = consti.IsActive,

             });
             return ViewModel;
         }

        public static tConstituencyPanchayat ToDomainModel(this ConstituencyPanchayatViewModel model)
        {
            return new tConstituencyPanchayat
            {
                ConstituencyPanchayatID = model.ConstituencyPanchayatID,
                PanchayatCode = model.PanchayatCode,
                
                StateCode = model.StateCode,
                ConstituencyCode = model.ConstituencyCode,
                DistrictCode = model.DistrictCode, 
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,

            };
            }
        

        public static ConstituencyPanchayatViewModel ToViewModel1(this tConstituencyPanchayat model,string Mode)
        {
            return new ConstituencyPanchayatViewModel
            {
                ConstituencyPanchayatID = model.ConstituencyPanchayatID,
                PanchayatCode = model.PanchayatCode,
                StateCode = model.StateCode,
                ConstituencyCode = model.ConstituencyCode,
                DistrictCode = model.DistrictCode, 
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                 //CreatedWhen = CurrentSession.UserName,
                 //ModifiedWhen = CurrentSession.UserName,            
                Mode = Mode

            };
        }
    }
}
    


       
