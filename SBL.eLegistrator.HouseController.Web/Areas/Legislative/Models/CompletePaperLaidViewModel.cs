﻿using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
 
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models
{
    public class CompletePaperLaidViewModel
    {

        public CompletePaperLaidViewModel()
        {


        }

        public CompletePaperLaidViewModel(CompletePaperLaidViewModel model)
        {

            this.ItemId = model.ItemId;
            this.CommonNumberForAll = model.CommonNumberForAll;
            this.Subject = model.Subject;
            this.AskedBy = model.AskedBy;
            this.MinistryName = model.MinistryName;
            this.DepartmentName = model.DepartmentName;
            this.QuestionNumber = model.QuestionNumber;
            this.IsPending = model.IsPending;
            this.IsContentFreeze = model.IsContentFreeze;
            this.IsDetailFreeze = model.IsDetailFreeze;
            this.IsQuestionFreeze = model.IsQuestionFreeze;
            this.IsInitialApprove = model.IsInitialApprove;
            this.IsRejected = model.IsRejected;
            this.IsTypeChanged = model.IsTypeChanged;
            this.IsPaperLaid = model.IsPaperLaid;
            this.QuesStage = model.QuesStage;
            this.IsFinalApprove = model.IsFinalApprove;
            this.Priority = model.Priority;
            this.PaperlaidPriority = model.PaperlaidPriority;
            this.IsTranslation = model.IsTranslation;
            this.IsClubbed = model.IsClubbed;
            this.RuleNo = model.RuleNo;
            this.IsBracketed = model.IsBracketed;

            // this field is using to find QuestionType Id 
            this.PaperCategoryTypeId = model.PaperCategoryTypeId;
			this.IsFixedDate = model.IsFixedDate;
            this.BussinessType = model.BussinessType;
            this.StatusText = model.StatusText;
        }

        [HiddenInput(DisplayValue = false)]
        public bool? IsRejected { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsFinalApprove { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int PaperCategoryTypeId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsByDepartmentWise { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsByMinisterWise { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsByPaperLaidWise { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string PaperCategoryType { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Id { get; set; } //Can be Ministry Id or Department Id or Null(Directly From Paper Laid)

        [HiddenInput(DisplayValue = false)]
        public bool IsStarredQuestion { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Status { get; set; }// Values: Sent,Recieved,LaidInHome,Pending,CurrentLob

        [HiddenInput(DisplayValue = false)]
        public PagedList<PaperLaidViewModel> PagedList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsByMinistery { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? ItemType { get; set; } //null: PaperLaid, true:Question, false:Notice

        [HiddenInput(DisplayValue = false)]
        public long ItemId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsTranslation { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsTypeChanged { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsClubbed { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsBracketed { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Title { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int MinistrywisePriority { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int DepartmentwisePriority { get; set; }
    


        [HiddenInput(DisplayValue = false)]
        public bool? IsPaperLaid { get; set; } //null: PaperLaid, true:Question, false:Notice

        [HiddenInput(DisplayValue = false)]
        public string DiaryNumber { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string NoticeNumber { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string RuleNo { get; set; }
        //[DisplayName("Number")]
        [HiddenInput(DisplayValue = false)]
        [JqGridColumnFormatter("$.DiaryFormatter")]
        public string CommonNumberForAll { get; set; }

        public string Subject { get; set; }

        //[DisplayName("Asked By")]
        public string AskedBy { get; set; }

        //[DisplayName("Ministry")]
        public string MinistryName { get; set; }

        //[DisplayName("Department")]
        public string DepartmentName { get; set; }

        [NotMapped]
        public string BussinessType { get; set; }

        [NotMapped]
        public string StatusText { get; set; }



        [HiddenInput(DisplayValue = false)]
        [JqGridColumnFormatter("$.PriorityFormatter")]
        public int? Priority { get; set; }

        [JqGridColumnFormatter("$.StatusFormatter")]
        public int? QuesStatus { get; set; }

        //[HiddenInput(DisplayValue = false)]
        [JqGridColumnFormatter("$.StageFormatter")]
        public int? QuesStage { get; set; }
        
        [HiddenInput(DisplayValue = false)]
        [JqGridColumnFormatter("$.PaperLaidPriorityFormatter")]
        public int PaperlaidPriority { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? QuestionNumber { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsPending { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsContentFreeze { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsDetailFreeze { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsInitialApprove { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsQuestionFreeze { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }
        [HiddenInput(DisplayValue = false)]
        public List<CompletePaperLaidViewModel> ListofData { get; set; }

       [HiddenInput(DisplayValue = false)]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
		public DateTime IsFixedDate { get; set; }



    }

    public class PaperLaidViewModel
    {
        public long ItemId { get; set; }
        public string MinistryName { get; set; }
        public string DepartmentName { get; set; }
        public string Title { get; set; }
        public int MinistrywisePriority { get; set; }
        public int DepartmentwisePriority { get; set; }
        public int PaperlaidPriority { get; set; }
        public bool? IsPaperLaid { get; set; } //null: PaperLaid, true:Question, false:Notice
        public string DiaryNumber { get; set; }
        public string NoticeNumber { get; set; }
        public string Subject { get; set; }
        public string AskedBy { get; set; }
        public int? QuestionNumber { get; set; }
        public bool? IsPending { get; set; }
        public bool? IsContentFreeze { get; set; }
        public bool? IsDetailFreeze { get; set; }
        public bool? IsInitialApprove { get; set; }
        public bool? IsFinalApprove { get; set; }
        public bool? IsQuestionFreeze { get; set; }
        public bool? IsRejected { get; set; }
        public bool? IsTypeChanged { get; set; }
        public bool? IsClubbed { get; set; }
		

    }
}