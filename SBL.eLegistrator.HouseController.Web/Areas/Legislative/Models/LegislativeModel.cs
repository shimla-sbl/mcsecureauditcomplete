﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models
{
    public class LegislativeModel
    {

        public List<LegislativeModel> ListLegislative { get; set; }

        public String PaperCategoryTypeId { get; set; }
        public String PaperCategoryType { get; set; }
        public String PaperSent { get; set; }
        public String PaperReceived { get; set; }
        public String PaperLaidinHouse { get; set; }
        public String PaperPendingForLay { get; set; }
        public String PaperCurrentLOB { get; set; }
        public String PaperUpdatedForCurrent { get; set; }

        public String TotalPaperSent { get; set; }
        public String TotalPaperReceived { get; set; }
        public String TotalPaperLaidinHouse { get; set; }
        public String TotalPaperPendingForLay { get; set; }
        public String TotalPaperCurrentLOB { get; set; }
        public String TotalPaperUpdatedForCurrent { get; set; }

        /*Paging Properties Start*/
        public int ResultCount { get; set; }
        public int RowsPerPage { get; set; }
        public int selectedPage { get; set; }
        public int loopStart { get; set; }
        public int loopEnd { get; set; }
        public int PageNumber { get; set; }

        /*Paging Properties End*/


    }
}