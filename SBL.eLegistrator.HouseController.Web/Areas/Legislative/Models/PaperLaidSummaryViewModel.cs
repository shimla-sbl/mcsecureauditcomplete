﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models
{
      [Serializable]
    public class PaperLaidSummaryViewModel
    {
        public int PendingStarredQuestionCount { get; set; }
        public int SentStarredQuestionCount { get; set; }
        public int RecievedStarredQuestionCount { get; set; }
        public int CurrentLobStarredQuestionCount { get; set; }
        public int LaidInHomeStarredQuestionCount { get; set; }

        public int PendingUnStarredQuestionCount { get; set; }
        public int SentUnStarredQuestionCount { get; set; }
        public int RecievedUnStarredQuestionCount { get; set; }
        public int CurrentLobUnStarredQuestionCount { get; set; }
        public int LaidInHomeUnStarredQuestionCount { get; set; }

        public int PendingNoticesCount { get; set; }
        public int SentNoticesCount { get; set; }
        public int RecievedNoticesCount { get; set; }
        public int CurrentLobNoticesCount { get; set; }
        public int LaidInHomeNoticesCount { get; set; }


        public int RecievedCommitteePaperCount { get; set; }
        public int SentCommitteePaperCount { get; set; }
        public int CurrentLobCommitteePaperCount { get; set; }
        public int LaidInHomeCommitteePaperCount { get; set; }


        public int RecievedBillsCount { get; set; }
        public int SentBillsCount { get; set; }
        public int CurrentLobBillsCount { get; set; }
        public int LaidInHomeBillsCount { get; set; }

        public int SentPaperToLayCount { get; set; }
        public int RecievedPaperToLayCount { get; set; }
        public int CurrentLobPaperToLayCount { get; set; }
        public int LaidInHomePaperToLayCount { get; set; }

        public string SessionName { get; set; }
        public string AssesmblyName { get; set; }


        public string DepartmentName { get; set; }
        public string UserDesignation { get; set; }
        public string CurrentUserName { get; set; }
        public string UserName { get; set; }
        public string CustomMessage { get; set; }
        //pendin to lay
        public int PendingToLayStarredQuestionCount { get; set; }
        //unstarred  pending to lay
        public int PendingToLayUnStarredQuestionCount { get; set; }
        // notice Pending to lay 
        public int PendingToLayNoticeCount { get; set; }
        //bills pending to lay
        public int PendingToLayBillsCount { get; set; }
        //Committee pending to lay
        public int PendingToLayCommitteeCount { get; set; }
        //OtherPaper pending to lay
        public int PendingToLayOtherPaperCount { get; set; }
        //Updated By Nitin
        public int TotalUpdatedFileCounter { get; set; }

        public int StarredUnfixed { get; set; }
        public int Starredfixed { get; set; }
        public int UnstarredUnfixed { get; set; }
        public int Unstarredfixed { get; set; }
        public int PostPoneCount { get; set; }
        public int ReFixPostPoneCount { get; set; }

        //updated By Sameer
        public int TotalValue { get; set; }
        public int TotalStaredReceived { get; set; }
        public int TotalUnStaredReceived { get; set; }
        public int NoticeResultCount { get; set; }
        public int TotalClubbedQuestion { get; set; }
        public int TotalInActiveQuestion { get; set; }
        public int TotalValueCl { get; set; }
        public int TotalStaredReceivedCl { get; set; }
        public int QuestionTypeId { get; set; }
        public string UId { get; set; }
        public string RoleName { get; set; }
        public int UnstaredTotalClubbedQuestion { get; set; }
        public int TotalUnStaredReceivedCl { get; set; }
        public int TotalSBracktedQuestion { get; set; }
        public int TotalUBractedQuestion { get; set; }
        public int StarredToUnstarredCount { get; set; }
        public int StarredRejectedCount { get; set; }
        public int UnstarredRejectedCount { get; set; }
        public int ChangedRuleNoticeCount { get; set; }

        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public List<mAssembly> mAssemblyList { get; set; }
        public IList<mSession> sessionList { get; set; }
        public string AccessFilePath { get; set; }
        public int PendingNoticesAssignCount { get; set; }
        public int CompleteSDashCount { get; set; }
        public int CompleteUNDashCount { get; set; }
        public int CompleteNDashCount { get; set; }
        public int StarredCountExess { get; set; }
        public int StarredCountExessed { get; set; }
        public int UnStarredCountExess { get; set; }
        public int UnStarredCountExessed { get; set; }

        public int StarredCountForWithdrawn { get; set; }
        public int StarredWithdrawnCount { get; set; }
        public int UnstarredCountForWithdrawn { get; set; }
        public int UntarredWithdrawnCount { get; set; }
        public int CountForClub { get; set; }
        public int CountForClubbed { get; set; }

          //house committee count - by robin
        public int ReceivedListCount { get; set; }
        public int DraftListCount { get; set; }
        public int SentPaperListCount { get; set; }

    }
}