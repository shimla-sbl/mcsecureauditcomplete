﻿using SBL.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models
{
    public class PaperLaidDetailedSummaryViewModel
    {
        public int PaperCategoryTypeId { get; set; }       
        public bool IsStarredQuestion { get; set; }
        public bool IsByMinistery { get; set; }
        public List<PaperLaidSummary> PaperLaidList { get; set; }
       

    }

    public class Group<T, K1,K2>
    {
        public K1 IdKey;
        public K2 NameKey;
        public IEnumerable<T> Values;
    }

    public class PaperLaidSummary
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int RecievedCount { get; set; }
        public int LaidInHomeCount { get; set; }
        public int CurrentLobCount { get; set; }
    }
}