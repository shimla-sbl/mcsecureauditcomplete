﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models
{
    public class PaperLaidPriorityViewModel
    {
        public List<PrioritiesModel> PriorityModel { get; set; }
        public string FilteredBy { get; set; }
    }
    public class PrioritiesModel
    {
        public int Priority { get; set; }
        public long PaperLaidId { get; set; }

    }
}