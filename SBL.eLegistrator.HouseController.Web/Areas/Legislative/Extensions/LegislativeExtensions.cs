﻿using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Question;
using SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models;
using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SBL.DomainModel.Models.Member;

namespace SBL.eLegistrator.HouseController.Web.Areas.Legislative.Extensions
{
    public static class LegislativeExtensions
    {
        public static List<PaperLaidViewModel> ToViewModel(this List<tPaperLaidV> list)
        {
            var result = list.Select(paper => new PaperLaidViewModel()
            {
                DepartmentName = paper.DeparmentName,
                MinistryName = paper.MinistryName,
                ItemId = paper.PaperLaidId,
                Title = paper.Title,
                IsPaperLaid = null
            });
            return result.ToList();
        }

        public static List<PaperLaidViewModel> ToViewModel(this List<tQuestion> list)
        {
            var result = list.Select(paper => new PaperLaidViewModel()
            {
                DepartmentName = paper.DepartmentID.GetDepartmentName(),
                DiaryNumber = paper.DiaryNumber,
                QuestionNumber = paper.QuestionNumber,
                MinistryName = paper.MinistryId.GetMinisterName(),
                ItemId = paper.QuestionID,
                Subject = paper.Subject.ToString(),
                AskedBy = paper.MemberID.GetMemberNameByID(),
                IsPending = paper.IsPending,
                IsPaperLaid = true
            });
            return result.ToList();
        }

        public static List<PaperLaidViewModel> ToViewModel(this List<tMemberNotice> list)
        {
            var result = list.Select(paper => new PaperLaidViewModel()
            {
                DepartmentName = paper.DepartmentId.GetDepartmentName(),
                NoticeNumber = paper.NoticeNumber,
                MinistryName = paper.MinistryId.GetMinisterName(),
                ItemId = paper.NoticeId,
                Subject = paper.Subject.ToString(),
                AskedBy = paper.MemberId.GetMemberNameByID(),
                IsPending = paper.IsPending,
                IsPaperLaid = false
                //DepartmentName = paper.DepartmentName,
                //MinistryName = paper.MinisterName,
                //ItemId = paper.NoticeId.ToString(),
                //Title = paper.Notice.ToString(),
                //IsPaperLaid = false,
                //NoticeNumber = paper.NoticeNumber
            });
            return result.ToList();
        }


        public static string GetDepartmentName(this String Id)
        {
            if (Id != null && Id != "")
            {
                var result = (mDepartment)Helper.ExecuteService("Legislative", "GetDepartmentById", new mDepartment { deptId = Id });
                return result.deptname;
            }
            return null;
        }

        public static string GetMinisterName(this int? Id)
        {
            if (Id != null && Id != 0)
            {
                var result = (mMinistry)Helper.ExecuteService("Legislative", "GetMinistryById", new tQuestion { MinistryId = Id });
                return result.MinistryName;
            }

            return null;
        }

        public static string GetMemberNameByID(this int Id)
        {
            if (Id != 0)
            {
                var result = (mMember)Helper.ExecuteService("Legislative", "GetMemberNameByID", new mMember { MemberCode = Id });
                return result.Prefix.Trim() + " " + result.Name.Trim();
            }
            return null;
        }

        public static string GetMemberNameByID(this int? Id)
        {
            if (Id != 0 && Id != null)
            {
                var result = (mMember)Helper.ExecuteService("Legislative", "GetMemberNameByID", new mMember { MemberCode = (int)Id });
                return result.Prefix.Trim() + " " + result.Name.Trim();
            }
            return null;
        }

       
    }
}