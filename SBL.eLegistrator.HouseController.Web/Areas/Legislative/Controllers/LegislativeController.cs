﻿using SBL.eLegistrator.HouseController.Web.Areas.Legislative.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel;
using SBL.DomainModel.Models.PaperLaid;
using SBL.eLegistrator.HouseController.Web.Areas.Legislative.Extensions;
using Microsoft.Security.Application;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Assembly;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Department;
using System.IO;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Enums;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Legislative.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class LegislativeController : Controller
    {
        const int pageSize = 5;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PaperLaidList()
        {
            return View();
        }

        public ActionResult PartialPaperLaidListCategoryWise(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            LegislativeModel legislativeModel = new LegislativeModel();

            int count = 5;

            List<LegislativeModel> ListLeg = new List<LegislativeModel>();

            for (int i = 0; i <= count; i++)
            {
                LegislativeModel legislative = new LegislativeModel();
                legislative.PaperCategoryType = "Q/A";
                legislative.PaperSent = "80";
                legislative.PaperReceived = "40";
                legislative.PaperLaidinHouse = "16";
                legislative.PaperPendingForLay = "24";
                legislative.PaperCurrentLOB = "6";
                legislative.PaperUpdatedForCurrent = "2";
                ListLeg.Add(legislative);
            }
            legislativeModel.ListLegislative = ListLeg;

            legislativeModel.TotalPaperSent = "400";
            legislativeModel.TotalPaperReceived = "200";
            legislativeModel.TotalPaperLaidinHouse = "80";
            legislativeModel.TotalPaperPendingForLay = "120";
            legislativeModel.TotalPaperCurrentLOB = "30";
            legislativeModel.TotalPaperUpdatedForCurrent = "10";

            return PartialView("PartialPaperLaidListCategoryWise", legislativeModel);
        }
        
        #region Paper Laid Summary

        public ActionResult PaperLaidSummary()
        {
            var model = new PaperLaidSummaryViewModel
            {
                PendingNoticesCount = (int)Helper.ExecuteService("Legislative", "GetNoticePendingCount", null),
                SentNoticesCount = (int)Helper.ExecuteService("Legislative", "GetNoticeSentCount", null),
                RecievedNoticesCount = (int)Helper.ExecuteService("Legislative", "GetNoticePaperRecievedCount", null),
                CurrentLobNoticesCount = (int)Helper.ExecuteService("Legislative", "GetNoticePaperCurrentLobCount", null),
                LaidInHomeNoticesCount = (int)Helper.ExecuteService("Legislative", "GetNoticePaperLaidInHomeCount", null),
                //pending to lay Notice
                PendingToLayNoticeCount = (int)Helper.ExecuteService("Legislative", "GetNoticePaperPendingToLayListCount", null),

                RecievedBillsCount = (int)Helper.ExecuteService("Legislative", "GetBillsPaperRecievedCount", null),
                CurrentLobBillsCount = (int)Helper.ExecuteService("Legislative", "GetBillsPaperCurrentLobCount", null),
                LaidInHomeBillsCount = (int)Helper.ExecuteService("Legislative", "GetBillsPaperLaidInHomeCount", null),
                //Pending to lay Bills
                PendingToLayBillsCount = (int)Helper.ExecuteService("Legislative", "GetBillsPaperPendingToLayListCount", null),

                RecievedCommitteePaperCount = (int)Helper.ExecuteService("Legislative", "GetCommittePaperRecievedCount", null),
                CurrentLobCommitteePaperCount = (int)Helper.ExecuteService("Legislative", "GetCommittePaperCurrentLobCount", null),
                LaidInHomeCommitteePaperCount = (int)Helper.ExecuteService("Legislative", "GetCommittePaperLaidInHomeCount", null),

                //pending to lay committee
                PendingToLayCommitteeCount = (int)Helper.ExecuteService("Legislative", "GetCommittePaperPendingToLayListCount", null),


                PendingStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetStarredQuestionPendingCount", null),
                SentStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetStarredQuestionSentCount", null),
                RecievedStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetStarredQuestionPaperRecievedCount", null),
                CurrentLobStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetStarredQuestionPaperCurrentLobCount", null),
                LaidInHomeStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetStarredQuestionPaperLaidInHomeCount", null),

                //starred Question pending to lay
                PendingToLayStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetStarredQuestionPaperPendingToLayListCount", null),

                PendingUnStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetUnStarredQuestionPendingCount", null),
                SentUnStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetUnStarredQuestionSentCount", null),
                RecievedUnStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetUnStarredQuestionPaperRecievedCount", null),
                CurrentLobUnStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetUnStarredQuestionPaperCurrentLobCount", null),
                LaidInHomeUnStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetUnStarredQuestionPaperLaidInHomeCount", null),
                //pending to lay Unstarred
                PendingToLayUnStarredQuestionCount = (int)Helper.ExecuteService("Legislative", "GetUnStarredQuestionPaperPendingToLayListCount", null),


                RecievedPaperToLayCount = (int)Helper.ExecuteService("Legislative", "GetPaperToLayRecievedCount", null),
                CurrentLobPaperToLayCount = (int)Helper.ExecuteService("Legislative", "GetPaperToLayCurrentLobCount", null),
                LaidInHomePaperToLayCount = (int)Helper.ExecuteService("Legislative", "GetPaperToLayLaidInHomeCount", null),
                //Pending to lay
                PendingToLayOtherPaperCount = (int)Helper.ExecuteService("Legislative", "GetOtherPaperPendingToLayListCount", null)



            };
            // added by nitin

            tPaperLaidV mdl = new tPaperLaidV();
            SiteSettings siteSettingMod = new SiteSettings();

            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            mdl.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            mdl.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

            mSession Mdl = new mSession();
            Mdl.SessionCode = siteSettingMod.SessionCode;
            Mdl.AssemblyID = siteSettingMod.AssemblyCode;

            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = siteSettingMod.AssemblyCode;

            model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
            model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
            mdl = (tPaperLaidV)Helper.ExecuteService("Legislative", "GetUpdatedOtherPaperLaidCounters", mdl);
            if (mdl != null)
            {
                model.TotalUpdatedFileCounter = mdl.TotalOtherpaperLaidUpdated;

            }
            mDepartment deptInfo = new mDepartment();

            deptInfo.deptId = CurrentSession.DeptID;
            deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
            model.DepartmentName = deptInfo.deptname;


            model.CurrentUserName = CurrentSession.UserName;
            model.UserDesignation = CurrentSession.MemberDesignation;
            model.UserName = CurrentSession.Name;

            // added by Sameer
            tMemberNotice model1 = new tMemberNotice();
            //Get the Total count of All Type of question.
            model1.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForProofReader", model1);
            model.TotalStaredReceived = model1.TotalStaredReceived;
            model1.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model1 = (tMemberNotice)Helper.ExecuteService("Notice", "GetCountForLegisUnstartQuestion", model1);
            model.TotalUnStaredReceived = model1.TotalUnStaredReceived;
            int c = model.TotalStaredReceived + model.TotalUnStaredReceived;
            model.TotalValue = c;
            return View(model);
        }

        public PartialViewResult GetAllDetailedSummary(int PaperCategoryTypeId, bool IsStarredQuestion = false)
        {
            return PartialView("_CompletePaperLaidDetails");
        }
        
        public PartialViewResult PaperLaidDetailedSummary(int PaperCategoryTypeId, bool ByMinistry = true, bool IsStarredQuestion = false)
        {
            var model = new PaperLaidDetailedSummaryViewModel()
            {
                PaperCategoryTypeId = PaperCategoryTypeId,
                IsByMinistery = ByMinistry,
                IsStarredQuestion = IsStarredQuestion,

            };
            var parameter = new tPaperLaidV()
            {
                EventId = PaperCategoryTypeId,
                AssemblyId = (IsStarredQuestion) ? 1 : 0,
                SessionId = (ByMinistry) ? 1 : 0
            };
            var Summary = (List<PaperLaidByPaperCategoryType>)Helper.ExecuteService("Legislative", "GetPaperLaidSummaryForPaperCategory", parameter);
            var GroupedData = (from s in Summary
                               group s by new { s.Id, s.Name } into grp
                               select new Group<PaperLaidByPaperCategoryType, string, string> { IdKey = grp.Key.Id, NameKey = grp.Key.Name, Values = grp }).ToList();

            var PaperLaidSummary = new List<PaperLaidSummary>();

            foreach (var item in GroupedData)
            {
                var data = new PaperLaidSummary();
                data.Name = item.NameKey;
                data.Id = item.IdKey;
                int Recieved = 0;
                int CurrentLob = 0;
                int LaidInHome = 0;
                foreach (var sub in item.Values)
                {
                    Recieved = (sub.IsRecieved) ? Recieved + sub.Count : Recieved;
                    CurrentLob = (sub.IsCurrentLob) ? CurrentLob + sub.Count : CurrentLob;
                    LaidInHome = (sub.IsLaidInHome) ? LaidInHome + sub.Count : LaidInHome;
                }
                data.RecievedCount = Recieved;
                data.CurrentLobCount = CurrentLob;
                data.LaidInHomeCount = LaidInHome;
                PaperLaidSummary.Add(data);
            }
            model.PaperLaidList = PaperLaidSummary;
            return PartialView("_PaperLaidDetailedSummary", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id">Ministry id or depatrment id</param>
        /// <param name="PaperCategoryTypeId"></param>
        /// <param name="IsByMinistry">True: By Ministry,False: By Department, Null: By Paper Laid</param>
        /// <returns></returns>
        public PartialViewResult CompletePaperLaidInformation(string Id, int PaperCategoryTypeId, bool IsStarredQuestion = false, bool? IsByMinistry = null,
            int PageNumber = 1, int LoopStart = 1, int LoopEnd = 5, string Status = null, bool? ItemType = null)
        {
            string chkruningsession = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SetSessionAlive;
            SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.SetSessionAlive = "0";
           // CurrentSession.SetSessionAlive.ab
            Id = Sanitizer.GetSafeHtmlFragment(Id);
            Status = Sanitizer.GetSafeHtmlFragment(Status);
            var model = new CompletePaperLaidViewModel()
            {
                PaperCategoryTypeId = PaperCategoryTypeId,
                Id = Id,
                IsStarredQuestion = IsStarredQuestion,
                Status = Status,
                ItemType = ItemType,

                PagedList = new PagedList<PaperLaidViewModel>()
                {
                    CurrentPage = PageNumber,
                    RowsPerPage = pageSize,
                    LoopEnd = LoopEnd,
                    LoopStart = LoopStart
                }
            };

            var PaperCategoryType = (mPaperCategoryType)Helper.ExecuteService("Legislative", "GetPapercategoryDetailsById",
                new mPaperCategoryType { PaperCategoryTypeId = PaperCategoryTypeId });

            model.PaperCategoryType = PaperCategoryType.Name;

            var PaperLaidList = new List<tPaperLaidV>();
            var QuestionList = new List<tQuestion>();
            var NoticeList = new List<tMemberNotice>();

            if (Status != "")
            {
                if (ItemType == null) //For Paper Laid
                {
                    PaperLaidList = (List<tPaperLaidV>)Helper.ExecuteService("Legislative", "GetCompletePaperLaidDetailsByStatus",
                        new tPaperLaidV { PaperCategoryTypeId = PaperCategoryTypeId, QuestionID = (!IsStarredQuestion) ? 0 : 1, Title = Status });

                }
                else if (ItemType == true) //For Question
                {
                    QuestionList = (List<tQuestion>)Helper.ExecuteService("Legislative", "GetCompletePaperLaidDetailsByStatus",
                       new tPaperLaidV { PaperCategoryTypeId = PaperCategoryTypeId, QuestionID = (!IsStarredQuestion) ? 0 : 1, Title = Status });
                }
                if (ItemType == false) //For Notice
                {
                    NoticeList = (List<tMemberNotice>)Helper.ExecuteService("Legislative", "GetCompletePaperLaidDetailsByStatus",
                       new tPaperLaidV { PaperCategoryTypeId = PaperCategoryTypeId, QuestionID = (!IsStarredQuestion) ? 0 : 1, Title = Status });
                }
            }

            //Sorting based on Minister name
            if (IsByMinistry != null && IsByMinistry == true)
            {
                PaperLaidList = PaperLaidList.OrderBy(a => a.MinistryName).ToList();
                NoticeList = NoticeList.OrderBy(a => a.MinisterName).ToList();
                QuestionList = QuestionList.OrderBy(a => a.MinisterID).ToList();
                model.IsByMinisterWise = true;
            }
            else if (IsByMinistry != null && IsByMinistry == false)
            {
                PaperLaidList = PaperLaidList.OrderBy(a => a.DeparmentName).ToList();
                NoticeList = NoticeList.OrderBy(a => a.DepartmentName).ToList();
                QuestionList = QuestionList.OrderBy(a => a.DepartmentID).ToList();

                model.IsByDepartmentWise = true;
            }
            else
            {
                model.IsByPaperLaidWise = true;
            }

            //Final Viewmodel values

            if (ItemType == null) //Paper Laid
            {
                model.PagedList.TotalRecordCount = PaperLaidList == null ? 0 : PaperLaidList.Count();
                //Sorting based on priority
                PaperLaidList = (model.IsByDepartmentWise) ? PaperLaidList.OrderBy(a => a.DepartmentwisePriority).ToList() :
                                (model.IsByMinisterWise) ? PaperLaidList.OrderBy(a => a.MinistrywisePriority).ToList() :
                                (model.IsByPaperLaidWise) ? PaperLaidList.OrderBy(a => a.PaperlaidPriority).ToList() : PaperLaidList.OrderBy(a => a.PaperLaidId).ToList();

                model.PagedList.List = PaperLaidList.ToViewModel().Skip((PageNumber - 1) * pageSize).Take(pageSize).ToList();
            }
            else if (ItemType == true) //Question
            {
                model.PagedList.TotalRecordCount = QuestionList == null ? 0 : QuestionList.Count();
                model.PagedList.List = QuestionList.ToViewModel().Skip((PageNumber - 1) * pageSize).Take(pageSize).ToList();
            }
            else if (ItemType == false) //Notice
            {
                model.PagedList.TotalRecordCount = NoticeList == null ? 0 : NoticeList.Count();
                model.PagedList.List = NoticeList.ToViewModel().Skip((PageNumber - 1) * pageSize).Take(pageSize).ToList();
            }
            return PartialView("_CompletePaperLaidDetails", model);
        }

        public PartialViewResult ViewPaperLaidDetails(string ItemId, bool? ITemType)
        {
            switch (ITemType)
            {
                case null:
                    {
                        var PaperLaid = (tPaperLaidV)Helper.ExecuteService("Legislative", "GetPaperLaidDetailsById", new tPaperLaidV { PaperLaidId = Convert.ToInt64(ItemId) });
                        return PartialView("_PaperLaidDetailPopup", PaperLaid);
                    }
                case true:
                    {
                        var Question = (tQuestion)Helper.ExecuteService("Legislative", "GetQuestionDetailsById", new tQuestion { QuestionID = Convert.ToInt32(ItemId) });
                        return PartialView("_SentQuestionDetails", Question);
                    }
                case false:
                    {
                        var Notice = (tMemberNotice)Helper.ExecuteService("Legislative", "GetNoticeDetailsById", new tMemberNotice { NoticeId = Convert.ToInt32(ItemId) });
                        return PartialView("_SentNoticeDetails", Notice);
                    }
                default: return null;
            }
        }

        [HttpPost]
        public JsonResult UpdatePriority(long PaperlaidId, string Prioritytype, int Priority)
        {
            var parameter = new tPaperLaidV
            {
                PaperLaidId = PaperlaidId,
                PaperlaidPriority = Priority,
                MinistryName = Prioritytype

            };
            var model = (tPaperLaidV)Helper.ExecuteService("Legislative", "UpdatePaperLaidPriority", parameter);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Private Methods

        #endregion

        #region Added By Nitin

        public ActionResult GetUpdatedPaperList(string type, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
        {
            if (CurrentSession.UserID != null)
            {
                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);

                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mEvent> PaperCategoryTypeList = Helper.ExecuteService("Legislative", "GetOtherPaperLaidPaperCategoryType", PaperCategoryTypeListMod) as List<mEvent>;
                tPaperLaidDepartmentModel.mEvents = PaperCategoryTypeList;

                #endregion

                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                objPaperModel.ResultCount = int.Parse(ResultCount);
                tPaperLaidV UpdatedPapers = new tPaperLaidV();
                UpdatedPapers.PageIndex = int.Parse(PageNumber);
                UpdatedPapers.PAGE_SIZE = int.Parse(RowsPerPage);
                UpdatedPapers = (tPaperLaidV)Helper.ExecuteService("Legislative", "GetUpdatedOtherPaperList", UpdatedPapers);
                if (UpdatedPapers != null)
                {
                    if (UpdatedPapers.DepartmentSubmitList.Count > 0)
                    {
                        tPaperLaidDepartmentModel.DepartmentSubmitList = UpdatedPapers.DepartmentSubmitList;
                    }
                }
                if (tPaperLaidDepartmentModel.DepartmentSubmitList.Count > 0)
                {
                    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentSubmitList.Count;
                }
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("20");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetUpdatedPapersList", tPaperLaidDepartmentModel);
            }
            return RedirectPermanent("/Legislative/Legislative/PaperLaidSummary");
        }

        public ActionResult FullViewUpdatedPaperLaidDetailByID(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("Legislative", "ShowUpdatedOtherPaperLaidDetailByID", movementModel);
                return PartialView("_ShowUpdatedOtherPaperLaidDetailByID", movementModel);
            }
            return null;
        }

        public ActionResult SubmittAllCheckedIds(string PaperIds)
        {
            bool doRedirect = false;
            if (!String.IsNullOrEmpty(PaperIds))
            {
                int[] Ids = PaperIds.Split(',').Select(i => Convert.ToInt32(i)).ToArray();
                foreach (int id in Ids)
                {
                    PaperMovementModel model = new PaperMovementModel();
                    model.PaperLaidId = id;
                    model = (PaperMovementModel)Helper.ExecuteService("Legislative", "GetDetailsByPaperLaidId", model);
                    string existFileUrl = model.actualFilePath;
                    string replaceFileUrl = model.FilePath;
                    string DirName = Path.GetDirectoryName(replaceFileUrl);
                    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(DirName));
                    if (!Dir.Exists)
                    { Dir.Create(); }
                    string actualFileName = replaceFileUrl;
                    string fullSourceFile = Server.MapPath(existFileUrl);
                    string fullDestinationFile = Server.MapPath(replaceFileUrl);
                    System.IO.File.Copy(fullSourceFile, fullDestinationFile, true);
                    //HttpPostedFile file;
                    //file.FileName = existFileUrl;
                    //file.SaveAs(Server.MapPath(actualFileName));
                    model = (PaperMovementModel)Helper.ExecuteService("Legislative", "UpdateLOBTempIdByPaperLiadID", model);
                    doRedirect = true;
                }
                if (doRedirect)
                {
                    return Json(new
                    {
                        RedirectUrl = Url.Action("PaperLaidSummary", "Legislative")
                    });
                }

            }
            return Json(new
            {
                RedirectUrl = Url.Action("PaperLaidSummary", "Legislative")
            });
        }

        #endregion

        #region "Method Added By Sunil"
        public PartialViewResult PaperDetailWithAction(string ItemId, bool? ITemType)
        {
            switch (ITemType)
            {
                case null:
                    {
                        var PaperLaid = (tPaperLaidV)Helper.ExecuteService("Legislative", "GetPaperLaidDetailsById", new tPaperLaidV { PaperLaidId = Convert.ToInt64(ItemId) });
                        return PartialView("_PaperLaidDetailPopup", PaperLaid);
                    }
                case true:
                    {
                        var Question = (tQuestion)Helper.ExecuteService("Legislative", "GetQuestionDetailsById", new tQuestion { QuestionID = Convert.ToInt32(ItemId) });
                        return PartialView("_DetailWithAction", Question);
                    }
                case false:
                    {
                        var Notice = (tMemberNotice)Helper.ExecuteService("Legislative", "GetNoticeDetailsById", new tMemberNotice { NoticeId = Convert.ToInt32(ItemId) });
                        return PartialView("_DetailWithActionNotice", Notice);
                    }
                default: return null;
            }

        }

        public ActionResult EditQuestionNotice(int Id, string EditType)
        {
            DiaryModel DMdl = new DiaryModel();
            DiaryModel DMdl2 = new DiaryModel();

            if (EditType == "Question")
            {
                if (Id != 0)
                {

                    DMdl.QuestionId = Id;
                }

                DMdl2 = Helper.ExecuteService("Legislative", "GetQuestionByIdForEdit", DMdl) as DiaryModel;
                DMdl = DMdl2;
                DMdl.ViewType = "QDraft";
                return PartialView("_EditQuestion", DMdl);

            }
            else if (EditType == "Notice")
            {
                if (Id != 0)
                {
                    DMdl.NoticeId = Id;
                }

                DMdl2 = Helper.ExecuteService("Legislative", "GetNoticeByIdForEdit", DMdl) as DiaryModel;
                DMdl = DMdl2;

                if (DMdl.DateForBind != null)
                {
                    DMdl.stringDate = Convert.ToDateTime(DMdl.DateForBind).ToString("dd/MM/yy");
                }
                if (DMdl.TimeForBind != null)
                {
                    DMdl.NoticeTime = (TimeSpan)DMdl.TimeForBind;
                }
                DMdl.ViewType = "NDraft";
                return PartialView("_EditNotice", DMdl);
            }

            return null;
        }

        public JsonResult GetDepartmentByMinister(int MinistryId)
        {
            DiaryModel DMdl = new DiaryModel();
            DMdl.MinistryId = MinistryId;
            DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("Legislative", "GetDepartmentByMinister", DMdl);
            return Json(DMdl.DiaryList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetConstByMemberId(int MemberId, int AssemId)
        {
            DiaryModel DMdl = new DiaryModel();
            if (MemberId != 0)
            {
                DMdl.MemberId = MemberId;
            }
            if (AssemId != 0)
            {
                DMdl.AssemblyID = AssemId;
            }

            DMdl = (DiaryModel)Helper.ExecuteService("Legislative", "GetConstByMemberId", DMdl);
            return Json(DMdl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveNotice(DiaryModel Dmdl)
        {
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                Dmdl.SubmittedBy = CurrentSession.UserID;
            }

            TempData["Message"] = Helper.ExecuteService("Legislative", "SaveNotice", Dmdl) as string;
            return RedirectToAction("PaperLaidSummary", "Legislative");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveQuestion(DiaryModel Dmdl)
        {
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                Dmdl.SubmittedBy = CurrentSession.UserID;
            }
           

            TempData["Message"] = Helper.ExecuteService("Legislative", "SaveQuestion", Dmdl) as string;
            return RedirectToAction("PaperLaidSummary", "Legislative");
        }

        public ActionResult GetQuestionNoticeDetails(int Id, string PaperType)
        {
            DiaryModel DMdl = new DiaryModel();
            DiaryModel DMdl2 = new DiaryModel();


            if (PaperType == "Question")
            {
                if (Id != 0)
                {
                    DMdl.QuestionId = Id;

                }

                DMdl2 = Helper.ExecuteService("Legislative", "GetQuestionDetailById", DMdl) as DiaryModel;
                DMdl = DMdl2;
                DMdl.PaperEntryType = PaperType;
                return PartialView("_ViewDetailsById", DMdl);

            }
            else if (PaperType == "Notice")
            {
                if (Id != 0)
                {

                    DMdl.NoticeId = Id;
                }

                DMdl2 = Helper.ExecuteService("Legislative", "GetNoticeDetailById", DMdl) as DiaryModel;
                DMdl = DMdl2;
                DMdl.PaperEntryType = PaperType;
                return PartialView("_ViewDetailsById", DMdl);
            }
            return null;
        }

        [HttpPost]
        public JsonResult CheckQuestionNo(string QuestionNumber, int QtypeId, int AssemId)
        {

            DiaryModel obj = new DiaryModel();

            QuestionNumber = Sanitizer.GetSafeHtmlFragment(QuestionNumber);
            obj.QuestionNumber = Convert.ToInt32(QuestionNumber);
            if (QtypeId > 0)
            {
                obj.QuestionTypeId = QtypeId;
            }
            if (AssemId != 0)
            {
                obj.AssemblyID = AssemId;
            }
            string st = Helper.ExecuteService("Legislative", "CheckQuestionNo", obj) as string;
            return Json(st, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllRecord(int AssemId, int SessId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string PaperType)
        {
            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                DiaryModel DMdl = new DiaryModel();
                DiaryModel DMdl2 = new DiaryModel();

                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

                DMdl.loopStart = Convert.ToInt16(loopStart);
                DMdl.loopEnd = Convert.ToInt16(loopEnd);
                DMdl.PageSize = Convert.ToInt16(RowsPerPage);
                DMdl.PageIndex = Convert.ToInt16(PageNumber);

                if (AssemId != 0)
                {
                    DMdl.AssemblyID = AssemId;
                }

                if (SessId != 0)
                {
                    DMdl.SessionID = SessId;
                }

                if (PaperType != "" && PaperType != null)
                {
                    DMdl.PaperEntryType = PaperType;
                }

                if (PaperType == "Starred")
                {
                    DMdl.QuestionTypeId = (int)QuestionType.StartedQuestion;
                }
                else if (PaperType == "Unstarred")
                {
                    DMdl.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                }

                DMdl2 = Helper.ExecuteService("Legislative", "GetAllDiariedRecord", DMdl) as DiaryModel;
                DMdl.DiaryList = DMdl2.DiaryList;
                DMdl.ResultCount = DMdl2.ResultCount;

                if (PageNumber != null && PageNumber != "")
                {
                    DMdl.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    DMdl.PageNumber = Convert.ToInt32("1");
                }
                if (RowsPerPage != null && RowsPerPage != "")
                {
                    DMdl.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    DMdl.RowsPerPage = Convert.ToInt32("10");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    DMdl.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    DMdl.selectedPage = Convert.ToInt32("1");
                }
                if (loopStart != null && loopStart != "")
                {
                    DMdl.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    DMdl.loopStart = Convert.ToInt32("1");
                }
                if (loopEnd != null && loopEnd != "")
                {
                    DMdl.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    DMdl.loopEnd = Convert.ToInt32("5");
                }
                return PartialView("_GetAllDiariedRecord", DMdl);

            }
            else
            {
                //return RedirectToAction("Login", "Account", new { @area = "" });
                return RedirectToAction("PaperLaidSummary", "Legislative");
            }
        }

        #endregion

    }
}
