﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Legislative
{
    public class LegislativeAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Legislative";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Legislative_default",
                "Legislative/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
