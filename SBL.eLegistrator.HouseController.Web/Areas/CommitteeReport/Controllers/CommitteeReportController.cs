﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.CommitteeReport.Controllers
{
    public class CommitteeReportController : Controller
    {
        //
        // GET: /CommitteeReport/CommitteeReport/ViewEditCommitteeReport

        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Edit and save committee report -SECTION OFFICER 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ViewEditCommitteeReport()
        {
            try
            {
                SBL.eLegistrator.HouseController.Web.Areas.CommitteeReport.Models.CommitteeReportViewModel reportList = new SBL.eLegistrator.HouseController.Web.Areas.CommitteeReport.Models.CommitteeReportViewModel();
                reportList.eFileAttachment = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("eFile", "GeteFileReport", null);       
                reportList.Mode = 1;
                return View(reportList);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
            }
            return View();
        }



        /// <summary>
        /// Review Reports from SECTION-OFFICER .Get all reports records for section officer FOR APPROVE
        /// </summary>
        /// <returns></returns>
        public ActionResult ReviewApproveCommitteeReportSectionOffice()
        {
            List<SBL.DomainModel.Models.eFile.eFileAttachment> AttachmentsList = new List<SBL.DomainModel.Models.eFile.eFileAttachment>();
            AttachmentsList = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("CommitteeReport", "GetReviewCommitteeReportSectionOffice", "SectionOffice");
            return View("ReviewApproveCommitteeReport",AttachmentsList);           
        }

        /// <summary>
        /// Review Reports from BRANCH-OFFICER .Get all reports records for branch officer
        /// </summary>
        /// <returns></returns>
        public ActionResult ReviewApproveCommitteeReportBranchOffice()
        {
            List<SBL.DomainModel.Models.eFile.eFileAttachment> AttachmentsList = new List<SBL.DomainModel.Models.eFile.eFileAttachment>();
            AttachmentsList = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("CommitteeReport", "GetReviewCommitteeReportSectionOffice", "BranchOffice");
            return View("ReviewApproveCommitteeReport",AttachmentsList);
        }

        /// <summary>
        /// Review Reports from VIDHAN SABHA SECRETARY .Get all reports records for vidhan sabha secretary
        /// </summary>
        /// <returns></returns>
        public ActionResult ReviewApproveCommitteeReportVidhanSabhaSecretary()
        {
            List<SBL.DomainModel.Models.eFile.eFileAttachment> AttachmentsList = new List<SBL.DomainModel.Models.eFile.eFileAttachment>();
            AttachmentsList = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("CommitteeReport", "GetReviewCommitteeReportSectionOffice", "VidhanSabhaSecretary");
            return View("ReviewApproveCommitteeReport", AttachmentsList);
        }


        /// <summary>
        /// Listing committee report details from Typist
        /// </summary>
        /// <param name="AttachmentId"></param>
        /// <returns></returns>
        public PartialViewResult ReportListDetails(string AttachmentId)
        {
            SBL.DomainModel.Models.CommitteeReport.CommitteeReportDetails commReportDetails = new DomainModel.Models.CommitteeReport.CommitteeReportDetails();
            commReportDetails = (SBL.DomainModel.Models.CommitteeReport.CommitteeReportDetails)SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("CommitteeReport", "eFileCommitteeReport", AttachmentId);
            return PartialView("_ReportListDetails", commReportDetails);
        }


        /// <summary>
        /// Listing committee report details from different logins
        /// </summary>
        /// <param name="AttachmentId"></param>
        /// <returns></returns>
        public PartialViewResult ReviewReportListDetails(string AttachmentId)
        {
            SBL.DomainModel.Models.CommitteeReport.CommitteeReportDetails commReportDetails = new DomainModel.Models.CommitteeReport.CommitteeReportDetails();
            commReportDetails = (SBL.DomainModel.Models.CommitteeReport.CommitteeReportDetails)SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("CommitteeReport", "eFileCommitteeReport", AttachmentId);
            return PartialView("_ReviewApproveReportListDetails", commReportDetails);
        }


        /// <summary>
        /// Listing final committee report details for BRANCH OFFICER.
        /// </summary>
        /// <param name="AttachmentId"></param>
        /// <returns></returns>
        public ActionResult FinalApproveCommitteeReportBranchOffice()
        {
            List<SBL.DomainModel.Models.eFile.eFileAttachment> AttachmentsList = new List<SBL.DomainModel.Models.eFile.eFileAttachment>();
            AttachmentsList = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("CommitteeReport", "FinalApprovalForCommitteeReport", "VidhanSabhaSecretary");
            return View("FinalApproveCommitteeReport", AttachmentsList);
        }


        /// <summary>
        /// Listing committee report details from different logins for Final Approval.
        /// </summary>
        /// <param name="AttachmentId"></param>
        /// <returns></returns>
        public PartialViewResult FinalReportListDetails(string AttachmentId)
        {
            SBL.DomainModel.Models.CommitteeReport.CommitteeReportDetails commReportDetails = new DomainModel.Models.CommitteeReport.CommitteeReportDetails();
            commReportDetails = (SBL.DomainModel.Models.CommitteeReport.CommitteeReportDetails)SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("CommitteeReport", "eFileCommitteeReport", AttachmentId);
            return PartialView("_FinalApproveReportListDetails", commReportDetails);
        }


        /// <summary>
        /// Listing final committee report details for SPEAKER.
        /// </summary>
        /// <param name="AttachmentId"></param>
        /// <returns></returns>
        public ActionResult SpeakerApproveCommitteeReport()
        {
            List<SBL.DomainModel.Models.eFile.eFileAttachment> AttachmentsList = new List<SBL.DomainModel.Models.eFile.eFileAttachment>();
            AttachmentsList = (List<SBL.DomainModel.Models.eFile.eFileAttachment>)SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("CommitteeReport", "SpeakerApprovalForCommitteeReport", "BranchOffice");
            return View("SpeakerApproveCommitteeReport", AttachmentsList);
        }


        /// <summary>
        /// Listing  committee report details for SPEAKER Details.
        /// </summary>
        /// <param name="AttachmentId"></param>
        /// <returns></returns>
        public PartialViewResult SpeakerApproveCommitteeReportListDetails(string AttachmentId)
        {
            SBL.DomainModel.Models.CommitteeReport.CommitteeReportDetails commReportDetails = new DomainModel.Models.CommitteeReport.CommitteeReportDetails();
            commReportDetails = (SBL.DomainModel.Models.CommitteeReport.CommitteeReportDetails)SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("CommitteeReport", "eFileCommitteeReport", AttachmentId);
            return PartialView("_SpeakerApproveReportListDetails", commReportDetails);
        }

        /// <summary>
        /// Report file uploading using javascript
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ContentResult UploadReportFiles()
        {

            string url = "~/eFileAttachments/";
            string directory = Server.MapPath(url);
            if (System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.Delete(directory, true);
            }

            var r = new List<SBL.DomainModel.Models.PaperLaid.tPaperLaidV>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                string url1 = "~/eFileAttachments/";
                string directory1 = Server.MapPath(url1);
                if (!System.IO.Directory.Exists(directory1))
                {
                    System.IO.Directory.CreateDirectory(directory1);
                }
                string savedFileName =System.IO.Path.Combine(Server.MapPath("~/eFileAttachments/"),System.IO.Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new SBL.DomainModel.Models.PaperLaid.tPaperLaidV()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

        /// <summary>
        /// Report file removing using javascript
        /// </summary>
        /// <returns></returns>
        public JsonResult RemoveMainFiles()
        {
            string url = "~/eFileAttachments/"; 
            string directory = Server.MapPath(url);
            Array.ForEach(System.IO.Directory.GetFiles(Server.MapPath(url)),System.IO.File.Delete);
            return Json("Update.Message", JsonRequestBehavior.AllowGet);
        }

        public ActionResult _GetPdf(string FilePath)
        {
            FilePath = "/eFileAttachments/" + FilePath + ".pdf";
            ViewBag.PDFPath = Microsoft.Security.Application.Encoder.UrlPathEncode(FilePath);
            return PartialView();
        }

               
        
        /// <summary>
        /// Submit report to database
        /// </summary>
        /// <param name="eFileAttachments"></param>
        /// <returns></returns>
        public JsonResult ReportSubmission(SBL.DomainModel.Models.eFile.eFileAttachment eFileAttachments)
        {
            eFileAttachments.CreatedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
            eFileAttachments.CreatedDate = DateTime.Now;
            eFileAttachments.ModifiedBy = new Guid(SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserID);
            eFileAttachments.ModifiedDate = DateTime.Now;
            eFileAttachments.eFileNameId = Guid.NewGuid();
            int AttachId = eFileAttachments.eFileAttachmentId;
            eFileAttachments.eFileAttachmentId = 0;
            bool value=(bool)SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("eFile", "AddeFileAttachments", eFileAttachments);
            if (value) SBL.eLegistrator.HouseController.Web.Helpers.Helper.ExecuteService("eFile", "UpdateFileSendStatus", AttachId); 
            return Json("Data Inserted", JsonRequestBehavior.AllowGet);
        }
    }
}
