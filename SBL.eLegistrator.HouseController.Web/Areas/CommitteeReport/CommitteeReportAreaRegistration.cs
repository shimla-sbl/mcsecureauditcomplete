﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.CommitteeReport
{
    public class CommitteeReportAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CommitteeReport";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CommitteeReport_default",
                "CommitteeReport/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
