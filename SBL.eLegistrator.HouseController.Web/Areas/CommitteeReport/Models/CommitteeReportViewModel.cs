﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SBL.eLegistrator.HouseController.Web.Areas.eFile.Models;

namespace SBL.eLegistrator.HouseController.Web.Areas.CommitteeReport.Models
{
    [Serializable]
    public class CommitteeReportViewModel
    {       
        public int Mode { get; set; }
        public List<SBL.DomainModel.Models.eFile.eFileAttachment> eFileAttachment { get; set; }
    }
}