﻿using Newtonsoft.Json;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.Role;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Filters.Security;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SBL.eLegistrator.HouseController.Web.Areas.SetSession.Controllers
{
    public class SetSessionController : Controller
    {
        //
        // GET: /SetSession/SetSession/

        public ActionResult Index(string Id)
        {
           
            CurrentSession.RoleName = "";
        CurrentSession.RoleID = "";

#pragma warning disable CS0219 // The variable 'returnUrl' is assigned but its value is never used
            string returnUrl = "";
#pragma warning restore CS0219 // The variable 'returnUrl' is assigned but its value is never used

            Int64 n;
            bool isNumeric = Int64.TryParse(Id, out n);
            string input = Id;



            string sub = input.Substring(0, 1);
            string subUser = "";
            if (input.Length >= 5)
            {
                subUser = input.Substring(0, 5);
            }
            else
            {
                subUser = input;
            }
            string subUsercon = "";
            if (input.Length >= 3)
            {
                subUsercon = input.Substring(0, 3);
            }
            else
            {
                subUsercon = input;
            }

            if (isNumeric)
            {


                ServiceAdaptor adapter = new ServiceAdaptor();


                mUsers user = new mUsers();
                user.AadarId = Id;
                CurrentSession.AadharId = user.AadarId;
                user.mUserList = (ICollection<mUsers>)Helper.ExecuteService("User", "GetDetailsByadhaarID", user);

                if (user.mUserList != null && user.mUserList.Count > 0)
                {
                    foreach (var list in user.mUserList)
                    {




                        // set status session var as successful
                        CurrentSession.LoginStatus = "Successful";

                        // Avoid Session Hijacking

                        string _browserInfo = Request.Browser.Browser
                        + Request.Browser.Version
                        + Request.UserAgent + "~"
                        + Request.ServerVariables["REMOTE_ADDR"];

                        string _sessionValue = Convert.ToString(Session["UserId"]) + "^"
                                                + DateTime.Now.Ticks + "^"
                                                + _browserInfo + "^"
                                                + System.Guid.NewGuid();

                        byte[] _encodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_sessionValue);
                        string _newEncryptedString = System.Convert.ToBase64String(_encodeAsBytes);

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            var c = new HttpCookie("AuthToken");
                            c.Value = _newEncryptedString;
                            c.Expires = DateTime.Now.AddDays(-1);
                            c.HttpOnly = true;
                            if (!Request.IsLocal)
                            {
                                c.Secure = true;
                            }
                            Response.SetCookie(c);
                        }
                        else
                        {
                            var c = new HttpCookie("AuthToken");
                            c.Value = _newEncryptedString;
                            c.Expires = DateTime.Now.AddDays(-1);
                            c.HttpOnly = true;
                            if (!Request.IsLocal)
                            {
                                c.Secure = true;
                            }
                            Response.Cookies.Add(c);
                        }
                        Session["encryptedSession"] = _newEncryptedString;






                        FormsAuthentication.SetAuthCookie(list.UserName, false);
                        CurrentSession.UserID = list.UserId.ToString();
                        CurrentSession.UserName = list.UserName;
                        CurrentSession.SubUserTypeID = list.UserType.ToString();
                        CurrentSession.IsMember = list.IsMember;
                        CurrentSession.OfficeId = list.OfficeId.ToString();

                        CurrentSession.OfficeLevel = list.OfficeLevel;
                        CurrentSession.OfficeId = list.OfficeId.ToString();
                        if (list.DeptId != null && list.DeptId != "")
                        {
                            CurrentSession.DeptID = Convert.ToString(list.DeptId);
                        }
                        if (list.SubdivisionCode != null && list.SubdivisionCode != "")
                        {
                            CurrentSession.SubDivisionId = Convert.ToString(list.SubdivisionCode);
                        }
                        SetSession(Id);


                        SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();

                     
                        var res = Json(true, JsonRequestBehavior.AllowGet);
                        //return res;
                        return RedirectToAction("Redirect", "SetSession");

                    }

                }
            }




            else if (subUsercon == "HPD" || subUsercon == "MMC")
            {


                ServiceAdaptor adapter = new ServiceAdaptor();


                mUsers user = new mUsers();
                user.AadarId = Id;
                CurrentSession.AadharId = user.AadarId;

                user.mUserList = (ICollection<mUsers>)Helper.ExecuteService("User", "GetDetailsByadhaarID", user);

                if (user.mUserList != null && user.mUserList.Count > 0)
                {
                    //if (CaptchaText != "CookieCaptcha")
                    //{
                    //    HttpCookie loginCookie = new HttpCookie("Login");
                    //    //Set the Cookie value.
                    //    loginCookie.Values["Name"] = model.ActualPassword;
                    //    loginCookie.Values["Password"] = model.UserName;
                    //    loginCookie.Path = Request.ApplicationPath;
                    //    //Set the Expiry date.
                    //    loginCookie.Expires = DateTime.Now.AddDays(365);
                    //    //Add the Cookie to Browser.
                    //    Response.Cookies.Add(loginCookie);
                    //}


                    foreach (var list in user.mUserList)
                    {



                        if (Request.Cookies["userAadharID"] != null)
                        {
                            var c = new HttpCookie("userAadharID");
                            c.Expires = DateTime.Now.AddDays(-1);
                            c.HttpOnly = true;

                            Response.Cookies.Add(c);
                        }



                        // set status session var as successful
                        CurrentSession.LoginStatus = "Successful";

                        // Avoid Session Hijacking

                        string _browserInfo = Request.Browser.Browser
                        + Request.Browser.Version
                        + Request.UserAgent + "~"
                        + Request.ServerVariables["REMOTE_ADDR"];

                        string _sessionValue = Convert.ToString(Session["UserId"]) + "^"
                                                + DateTime.Now.Ticks + "^"
                                                + _browserInfo + "^"
                                                + System.Guid.NewGuid();

                        byte[] _encodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_sessionValue);
                        string _newEncryptedString = System.Convert.ToBase64String(_encodeAsBytes);

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            var c = new HttpCookie("AuthToken");
                            c.Value = _newEncryptedString;
                            if (!Request.IsLocal)
                            {
                                c.Secure = true;
                            }
                            c.Expires = DateTime.Now.AddDays(-1);
                            c.HttpOnly = true;
                            Response.SetCookie(c);
                        }
                        else
                        {
                            var c = new HttpCookie("AuthToken");
                            c.Value = _newEncryptedString;
                            if (!Request.IsLocal)
                            {
                                c.Secure = true;
                            }
                            c.Expires = DateTime.Now.AddDays(-1);
                            c.HttpOnly = true;
                            Response.Cookies.Add(c);
                        }
                        Session["encryptedSession"] = _newEncryptedString;






                        FormsAuthentication.SetAuthCookie(list.UserName, false);
                        CurrentSession.UserID = list.UserId.ToString();
                        CurrentSession.UserName = list.UserName;
                        CurrentSession.SubUserTypeID = list.UserType.ToString();
                        CurrentSession.IsMember = list.IsMember;
                        CurrentSession.OfficeId = list.OfficeId.ToString();
                        // CurrentSession.MemberCode = list.UserName; 
                        CurrentSession.OfficeLevel = list.OfficeLevel;
                        CurrentSession.OfficeId = list.OfficeId.ToString();
                        if (list.DeptId != null && list.DeptId != "")
                        {
                            CurrentSession.DeptID = Convert.ToString(list.DeptId);
                        }
                        else
                        {

                            CurrentSession.DeptID = "0";

                        }
                        if (list.SubdivisionCode != null && list.SubdivisionCode != "")
                        {
                            CurrentSession.SubDivisionId = Convert.ToString(list.SubdivisionCode);
                        }

                        SetSession(Id);
                        if (list.AadarId.Contains("MMC"))
                        {

                        }
                        else
                        {
                            string Memcode = Helper.ExecuteService("User", "GetMemberCode_ForConUser", user) as string;
                            if (Memcode != "" || Memcode != null)
                            {
                                string[] str = new string[3];
                                str[0] = CurrentSession.OfficeId;
                                str[1] = Memcode;
                                str[2] = CurrentSession.AssemblyId;
                                string UserCondId = Helper.ExecuteService("User", "GetConId_ForConUser", str) as string;
                                CurrentSession.ConstituencyID = UserCondId;
                            }
                        }
                        SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();



                    
                        var res = Json(true, JsonRequestBehavior.AllowGet);
                        //return res;
                        return RedirectToAction("Redirect", "SetSession");


                    }



                }

            }




            return RedirectToAction("Redirect", "SetSession");


        }

        internal void SetSession(string Id)
        {           





            var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@AdharID", Id));


                DataSet dataSetUser = new DataSet();
                dataSetUser = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[SelectUserbyAdhaarId]", methodParameter);


                if (dataSetUser != null && dataSetUser.Tables.Count > 0 && dataSetUser.Tables[0].Rows.Count > 0)
                {

                    //  Remove this as this method is not require as now 
                    //if (!string.IsNullOrEmpty(CurrentSession.UserID))
                    //{
                    //    mUserDSHDetails details = new mUserDSHDetails();
                    //    details.UserId = Guid.Parse(CurrentSession.UserID);
                    //    var userDscLst = (List<mUserDSHDetails>)Helper.ExecuteService("User", "getDscDetailsByUserId", details);
                    //    if (userDscLst != null)
                    //    {
                    //        foreach (var item in userDscLst)
                    //        {
                    //            CurrentSession.UserHashKey = item.HashKey;
                    //        }
                    //    }

                    //}


                    bool IsMember = false;
                    //remove this if condition 
                    //if ( CurrentSession.SubUserTypeID != "22")
                    //{
                    if (dataSetUser.Tables[0].Rows[0]["Roleid"] != DBNull.Value)
                    {
                        CurrentSession.RoleID = Convert.ToString(dataSetUser.Tables[0].Rows[0]["Roleid"]);
                        CurrentSession.RoleName = Convert.ToString(dataSetUser.Tables[0].Rows[0]["RoleName"]);
                    }

                    if (dataSetUser.Tables[0].Rows[0]["UserTypeConstituencyId"] != DBNull.Value)
                    {
                        CurrentSession.UserTypeConstituencyId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["UserTypeConstituencyId"]);
                    }
                    if (dataSetUser.Tables[0].Rows[0]["DistrictCode"] != DBNull.Value)
                    {
                        CurrentSession.DistrictCode = Convert.ToString(dataSetUser.Tables[0].Rows[0]["DistrictCode"]);
                    }

                    //}
                    /////added by dharmendra,add department in session, if additional department exist
                    if (dataSetUser.Tables[0].Rows[0]["DeptId"] != DBNull.Value)
                    {
                        if (dataSetUser.Tables[0].Rows[0]["ApprovedAdditionalDept"] != DBNull.Value)
                        {
                            string Department = Convert.ToString(dataSetUser.Tables[0].Rows[0]["DeptId"]);
                            string AdditionalDept = Convert.ToString(dataSetUser.Tables[0].Rows[0]["ApprovedAdditionalDept"]);
                            string alldept = Department + "," + AdditionalDept;
                            List<string> deptlist = alldept.Split(',').Distinct().ToList();
                            string DistDept = deptlist.Aggregate((a, x) => a + "," + x);
                            CurrentSession.DeptID = DistDept;
                        }
                        else
                        {
                            CurrentSession.DeptID = Convert.ToString(dataSetUser.Tables[0].Rows[0]["DeptId"]);
                        }

                    }

                    if (dataSetUser.Tables[0].Rows[0]["SignaturePath"] != DBNull.Value)
                    {
                        CurrentSession.SignaturePath = Convert.ToString(dataSetUser.Tables[0].Rows[0]["SignaturePath"]);

                    }


                if (dataSetUser.Tables[1] != null)
                {

                    if (dataSetUser.Tables[1].Rows.Count > 0)
                    {
                        CurrentSession.UserId_House = Convert.ToString(dataSetUser.Tables[1].Rows[0]["UserId_House"]);



                        CurrentSession.UserId_DeptReply = Convert.ToString(dataSetUser.Tables[1].Rows[0]["UserId_DeptReply"]);



                        CurrentSession.UserId_eConstituency = Convert.ToString(dataSetUser.Tables[1].Rows[0]["UserId_eConstituency"]);
                    }
                    else
                    {
                        CurrentSession.UserId_House = "";



                        CurrentSession.UserId_DeptReply = "";


                        CurrentSession.UserId_eConstituency = "";

                    }

                }
                else
                {
                    CurrentSession.UserId_House = "";



                    CurrentSession.UserId_DeptReply = "";


                    CurrentSession.UserId_eConstituency = "";

                }







                if (dataSetUser.Tables[0].Rows[0]["Designation"] != DBNull.Value)
                    {
                        CurrentSession.Designation = Convert.ToString(dataSetUser.Tables[0].Rows[0]["Designation"]);

                    }


                    if (dataSetUser.Tables[0].Rows[0]["SecretoryId"] != DBNull.Value)
                    {
                        CurrentSession.SecretaryId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["SecretoryId"]);

                    }

                    if (dataSetUser.Tables[0].Rows[0]["IsHOD"] != DBNull.Value)
                    {
                        CurrentSession.IsHOD = Convert.ToString(dataSetUser.Tables[0].Rows[0]["IsHOD"]);

                    }

                    if (dataSetUser.Tables[0].Rows[0]["IsSecretoryId"] != DBNull.Value)
                    {
                        CurrentSession.IsSecretoryId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["IsSecretoryId"]);

                    }

                    if (dataSetUser.Tables[0].Rows[0]["IsMember"] != DBNull.Value)
                    {
                        CurrentSession.MemberDesignation = Convert.ToString(dataSetUser.Tables[0].Rows[0]["Designation"]);
                        IsMember = Convert.ToBoolean(dataSetUser.Tables[0].Rows[0]["IsMember"]);
                    }

                    if (dataSetUser.Tables[0].Rows[0]["OfficeId"] != DBNull.Value)
                    {
                        CurrentSession.OfficeId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["OfficeId"]);

                    }
                    if (dataSetUser.Tables[0].Rows[0]["OfficeId"] != DBNull.Value)
                    {
                        CurrentSession.OfficeId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["OfficeId"]);

                    }

                    if (dataSetUser.Tables[0].Rows[0]["SubdivisionCode"] != DBNull.Value)
                    {
                        CurrentSession.SubDivisionId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["SubdivisionCode"]);

                    }
                    if (dataSetUser.Tables[0].Rows[0]["ConstituencyID"] != DBNull.Value)
                    {
                        CurrentSession.ConstituencyID = Convert.ToString(dataSetUser.Tables[0].Rows[0]["ConstituencyID"]);

                    }
                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                    if (siteSettingMod != null)
                    {
                        CurrentSession.AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);
                        CurrentSession.SessionId = Convert.ToString(siteSettingMod.SessionCode);

                        //added by robin to compare current session and assembly 
                        CurrentSession.SiteSettingsCurrentAssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);
                        CurrentSession.SiteSettingsCurrentSessionId = Convert.ToString(siteSettingMod.SessionCode);
                    }

                    // set session alive = 
                    // CurrentSession.SetSessionAlive = "1";
                    methodParameter = new List<KeyValuePair<string, string>>();
                    //put condition for member emp and member itself
                    methodParameter.Add(new KeyValuePair<string, string>("@MemberId", CurrentSession.UserName));
                DataSet dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "SelectMamberIdByLoginId", methodParameter);

                    if (!IsMember && CurrentSession.SubUserTypeID == "22")//add suser subtype id for members Employee
                    {
                        methodParameter = new List<KeyValuePair<string, string>>();
                        methodParameter.Add(new KeyValuePair<string, string>("@empcd", CurrentSession.UserName));
                        methodParameter.Add(new KeyValuePair<string, string>("@dept", CurrentSession.DeptID));
                        DataSet dataSetEmp = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEmployeeById", methodParameter);
                        if (dataSetEmp != null)
                        {
                            if (dataSetEmp.Tables[0].Rows.Count > 0)
                            {
                                CurrentSession.Name = Convert.ToString(dataSetEmp.Tables[0].Rows[0]["empfname"]) + " " + Convert.ToString(dataSetEmp.Tables[0].Rows[0]["empmname"]) + " " + Convert.ToString(dataSetEmp.Tables[0].Rows[0]["emplname"]);
                            }
                        }

                    }


                    if (dataSet != null)
                    {
                        if (dataSet.Tables[0].Rows.Count > 0)
                        {

                            CurrentSession.MemberID = Convert.ToString(dataSet.Tables[0].Rows[0][0]);
                            if (CurrentSession.IsMember == "False" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login
                            {
                                CurrentSession.MemberCode = CurrentSession.UserName;
                                CurrentSession.MemberUserID = (string)Helper.ExecuteService("User", "GetMemberDetailsForOSD", CurrentSession.MemberCode);
                                CurrentSession.AadharId = Convert.ToString(dataSet.Tables[0].Rows[0]["AadhaarNo"]);
                            }
                            else
                            {
                                CurrentSession.MemberCode = Convert.ToString(dataSet.Tables[0].Rows[0][1]);
                            }
                            CurrentSession.Name = Convert.ToString(dataSet.Tables[0].Rows[0]["Name"]);
                            CurrentSession.Name_Hindi = Convert.ToString(dataSet.Tables[0].Rows[0]["NameLocal"]);
                            CurrentSession.MemberLocation = Convert.ToString(dataSet.Tables[0].Rows[0]["Location"]);
                            CurrentSession.OldUserName = Convert.ToString(dataSet.Tables[0].Rows[0]["mMemberOldID"]);
                            CurrentSession.MemberPrefix = Convert.ToString(dataSet.Tables[0].Rows[0]["Prefix"]);
                            CurrentSession.MemberEmail = Convert.ToString(dataSet.Tables[0].Rows[0]["Email"]);
                            CurrentSession.MemberMobile = Convert.ToString(dataSet.Tables[0].Rows[0]["Mobile"]);
                            methodParameter = new List<KeyValuePair<string, string>>();
                            methodParameter.Add(new KeyValuePair<string, string>("@MemberID", Convert.ToString(dataSet.Tables[0].Rows[0][1])));
                            methodParameter.Add(new KeyValuePair<string, string>("@Match", null));
                            CurrentSession.MemberDesignation = Convert.ToString(dataSet.Tables[0].Rows[0]["memDesigname"]);
                            DataSet dataSetconst = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "eVidhan_SelectMemberConstinuency", methodParameter);
                            if (dataSetconst.Tables[0].Rows.Count > 0)
                            {
                                CurrentSession.ConstituencyID = Convert.ToString(dataSetconst.Tables[0].Rows[0]["ConstituencyID"]);
                                CurrentSession.ConstituencyName = Convert.ToString(dataSetconst.Tables[0].Rows[0]["ConstituencyName"]);
                                CurrentSession.ConstituencyName_Local = Convert.ToString(dataSetconst.Tables[0].Rows[0]["ConstituencyName_Local"]);
                            }
                        }
                    }

                }

            }




       
        public ActionResult Redirect()
        {
            //added by durgesh for error handling and logging testing only
            //long i = Convert.ToInt64("e");
            //if (!string.IsNullOrEmpty(CurrentSession.UserUpdateFlag) || CurrentSession.UserID.ToString() == "e7e83032-1cb0-41ea-bca8-ffdedd859173")
            //{
            if (CurrentSession.UserName != "")
            {
                string UserName = CurrentSession.UserName;
                string RoleName = CurrentSession.RoleName;
                string RoleId = CurrentSession.RoleID;
                string UserID = CurrentSession.UserID.ToString();
                string SubUserTypeID = Convert.ToString(CurrentSession.SubUserTypeID);
                string officeId = Convert.ToString(CurrentSession.OfficeId);
                #region OldAuth

                switch (RoleId)
                {
                    case "1fca5265-162b-4bf3-8926-70a9c1548095":
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });

                    case "d457778d-b3e2-4a1f-a522-203ec7b09608":
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be997":
                        return RedirectToAction("Index", "Reporters", new { area = "Reporters" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be998":
                        return RedirectToAction("Index", "Media", new { area = "Media" });
                    /// Translator for LegislationFixation
                    case "c4c92e7c-3d31-4cc7-bdce-8ded55e77b99":
                        return RedirectToAction("Index", "Changer", new { area = "Notices" });
                    /// Secretary for LegislationFixation
                    case "f601de42-22f5-4b77-92b2-cd506564da25":
                        return RedirectToAction("PaperLaidSummary", "LegislationFixation", new { area = "Notices" });
                    /// Legislation Emp for LegislationFixation
                    case "e5144dad-7a5d-4308-976a-8a5d2a462bd1":
                        return RedirectToAction("PaperLaidSummary", "LegislationFixation", new { area = "Notices" });
                    /// For Committee
                    case "c4c92e7c-3d31-4cc7-bdce-8ded55e77b94":
                        return RedirectToAction("CommitteeDashboard", "CommitteePaper", new { area = "CommitteePaperLaid" });
                    /// For Diary
                    case "5e023679-e59e-407d-b487-9df5c0faae47":
                        return RedirectToAction("DiariesDashboard", "Diaries", new { area = "Notices" });
                    /// For Create CommitteeXML
                    case "0d22a8f9-2bfc-4f7b-8963-4d8ce230d173":
                        return RedirectToAction("CreateLOB", "CommitteeXML", new { area = "Committee" });
                    /// For Create LOB
                    case "42b80446-fbbb-4ed1-a9e9-8d43c8cafb80":
                        return RedirectToAction("CreateLOB", "ListOfBusiness", new { area = "ListOfBusiness" });
                    /// for approval of LOB
                    case "139dd5da-1bee-4bac-af7d-02cd53c82632":
                        return RedirectToAction("Approval", "ListOfBusiness", new { area = "ListOfBusiness" });
                    ///for create Speaker Pad
                    case "ea86388d-43f7-46ea-9201-47929da4733e":
                        return RedirectToAction("SpeakerPad", "Speaker", new { area = "ListOfBusiness" });
                    ///For approval of Speaker Pad
                    case "00ed74e6-1ea4-41ed-ad78-90049d4508ff":
                        return RedirectToAction("ApprovalSpeakerPad", "Speaker", new { area = "ListOfBusiness" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be999":
                        return RedirectToAction("Index", "AdministrationBranch", new { area = "AdministrationBranch" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be994":
                        return RedirectToAction("Index", "PublicPass", new { area = "PublicPasses" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be100":
                        return RedirectToAction("Index", "SuperAdmin", new { area = "SuperAdmin" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba111":
                        return RedirectToAction("Index", "DashBoardAccountsAdmin", new { area = "AccountsAdmin" });//Module done by Robin--AccountsAdmin
                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba112":
                        return RedirectToAction("Index", "DashBoardDisbursementOfficer", new { area = "DisbursementOfficer" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba113":
                        return RedirectToAction("Index", "DashBoardBudgetSuperindent", new { area = "BudgetSuperindent" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba114":
                        return RedirectToAction("Index", "AccountDashBoard", new { area = "Accounts" });//Module done by durgesh----BillClerk
                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba115":
                        return RedirectToAction("AllMembersAccountDetails", "MemberAccountDetails", new { area = "SalaryClerks" });//Module - by robin as Salary clerk
                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba116":
                        return RedirectToAction("Index", "DashBoardCashier", new { area = "Cashier" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba117":
                        return RedirectToAction("Index", "DashBoardLoanDealingOfficer", new { area = "LoanDealingOfficer" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba118":
                        return RedirectToAction("Index", "DashBoardPensionClerk", new { area = "PensionClerk" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba119":
                        return RedirectToAction("Index", "PublicPass", new { area = "PublicPasses" });//for public- Admin

                    //Part by robin 
                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be150":
                        Session["LibraryRole"] = SBL.DomainModel.Models.Enums.LibraryRoleType.DocumentationOfficer.GetHashCode();
                        return RedirectToAction("Index", "LibraryDashBoard", new { area = "Library" });//Library Part ----Documentation Officer

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be151":
                        Session["LibraryRole"] = SBL.DomainModel.Models.Enums.LibraryRoleType.Librarion.GetHashCode();
                        return RedirectToAction("Index", "LibraryDashBoard", new { area = "Library" });//Library Part ----Librarion

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be152":
                        Session["LibraryRole"] = SBL.DomainModel.Models.Enums.LibraryRoleType.LibrarionClerk.GetHashCode();
                        return RedirectToAction("Index", "LibraryDashBoard", new { area = "Library" });//Library Part ----Librarion Clerk

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be153":
                        Session["LibraryRole"] = SBL.DomainModel.Models.Enums.LibraryRoleType.SaleCounterClerk.GetHashCode();
                        return RedirectToAction("Index", "LibraryDashBoard", new { area = "Library" });//Library Part -----Sale Counter CLerk

                    case "0fc60e41-c78e-46b0-b8ac-f6eb709be999":
                        return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });//for Minister- Admin
                    case "0fc50e41-c78f-78b0-b8ac-f6eb709be999":  // Con User 0fc50e41-c78f-78b0-b8ac-f6eb709be999
                        var OId = (int)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                        var MId = (int)Helper.ExecuteService("Grievance", "GetMemberByOfficeUser", CurrentSession.AadharId);
                        if (MId > 0)
                        {
                            CurrentSession.MemberCode = Convert.ToString(MId);
                        }
                        if (OId == Convert.ToInt32(officeId))
                        {

                            return RedirectToAction("Index", "Grievances", new { area = "Grievances" });
                        }
                        else
                        {

                            return RedirectToAction("Index", "Blank", new { area = "BlankRed" });
                        }

                    case "0fc60e41-c78e-49b0-b8ac-f6eb708be151":  // SDM-MLA Diary USer
                        return RedirectToAction("Index", "Grievances", new { area = "Grievances" });
                    //var OIdd = (int)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                    //var MIdd = (int)Helper.ExecuteService("Grievance", "GetMemberByOfficeUser", CurrentSession.AadharId);
                    //if (MIdd > 0)
                    //{
                    //    CurrentSession.MemberCode = Convert.ToString(MIdd);
                    //}
                    //if (OIdd == Convert.ToInt32(officeId))
                    //{

                    //    return RedirectToAction("Index", "Grievances", new { area = "Grievances" });
                    //}
                    //else
                    //{

                    //    return RedirectToAction("Index", "Blank", new { area = "BlankRed" });
                    //}
                    case "f8e0ef76-afe7-4e94-88f4-f399f967e37e":
                        //CommitteeChairpersonUserId();


                        tCommitteeMember cm = new tCommitteeMember();
                        tCommitteeModel Obj = new tCommitteeModel();

                        List<tCommitteeMember> AuthorisedMem = new List<tCommitteeMember>();

                        mUsers musr = new mUsers();

                        string userid = CurrentSession.UserName;
                        AuthorisedMem = (List<tCommitteeMember>)Helper.ExecuteService("Committee", "ShowCommitteeMemberByID", userid);

                        if (AuthorisedMem.Count == 0)
                        {
                            //return RedirectToAction("LoginUP", "Account", new { area = "" });
                            return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                        }
                        else
                        {
                            //return RedirectToAction("CommitteeDashboard", "CommitteePaperLiadChairperson", new { area = "CommitteePaperLiadChairperson" });
                            return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                        }

                    default:
                        break;
                }

                switch (RoleName)
                {
                    //case "Member":
                    //    return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                    case "Vidhan Sabha Typist":
                        return RedirectToAction("Index", "NoticeDetails", new { area = "Notices" });

                    case "Vidhan Sabha Proof Reader":
                        return RedirectToAction("Index", "NoticeDetails", new { area = "Notices" });

                    case "PaperLaidDepartment":
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });

                    case "PaperLaidMinister":
                        return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });

                    default:
                        break;
                }

                switch (UserName)
                {
                    //case "10023":
                    //    return RedirectToAction("PaperLaidSummary", "LegislationFixation", new { area = "Notices" });
                    //case "10103":
                    //    return RedirectToAction("CommitteeDashboard", "CommitteePaper", new { area = "CommitteePaperLaid" });
                    //case "61":
                    //    return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
                    //case "407":
                    //    return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
                    //case "10014":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10031":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    case "10032":
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10033":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10034":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10035":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10036":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10041":
                    //case "10016":
                    //    return RedirectToAction("DiariesDashboard", "Diaries", new { area = "Notices" });
                    //case "10029":
                    //    return RedirectToAction("Index", "Reporters", new { area = "Reporters" });
                    //case "10011":
                    //    return RedirectToAction("PaperLaidSummary", "LegislationFixation", new { area = "Notices" });
                    //case "10090":
                    //    return RedirectToAction("CreateLOB", "ListOfBusiness", new { area = "ListOfBusiness" });

                    //case "10091":
                    //    return RedirectToAction("Approval", "ListOfBusiness", new { area = "ListOfBusiness" });
                    //case "10092":
                    //    return RedirectToAction("SpeakerPad", "Speaker", new { area = "ListOfBusiness" });
                    //case "10094":
                    //    return RedirectToAction("ApprovalSpeakerPad", "Speaker", new { area = "ListOfBusiness" });
                    case "admin":
                        return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
                    case "Vivek":
                        return RedirectToAction("Index", "MetaData", new { area = "Admin" });
                    case "lobadmin":
                        return RedirectToAction("GetAssebmlySessionDetails", "BusinessWork", new { area = "BusinessWork" });

                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    case "user":
                        return View("UserDashboard");

                    default:
                        break;
                }

                #endregion OldAuth

                #region CustumAuth
                //User=>ForUserType
                //role=>ForModule
                string ModuleIds = "";
                tUserAccessRequest objtUserAccessRequest = new tUserAccessRequest();
                objtUserAccessRequest.UserID = Guid.Parse(UserID);
                var UserModules = (List<tUserAccessActions>)Helper.ExecuteService("Module", "GetUseModulesByUserID", objtUserAccessRequest);

                var usres = (List<mUsers>)Helper.ExecuteService("Module", "GetUsers", null);
                var user = usres.Where(u => u.UserName == UserName && u.UserId.ToString() == UserID).FirstOrDefault();
                if (user != null)
                {
                    mRoles iObjRole = new mRoles();
                    var ilstRole = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserType", null);
                    var _UserType = ilstRole.Select(m => m.iSubUserTypeID = SubUserTypeID).Take(1).ToArray();
                    // var roles = user.Roles.Select(m => m.iRoleId).ToArray();
                    CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                    serializeModel.UserId = user.UserId.ToString();
                    serializeModel.FirstName = user.UserName;
                    serializeModel.LastName = user.UserName;
                    serializeModel.roles = _UserType;
                    if (UserModules != null && UserModules.Count() > 0)
                    {
                        foreach (var item in UserModules)
                        {
                            ModuleIds += item.ModuleId + ",";
                        }
                        bool flag = ModuleIds.EndsWith(",");
                        if (flag)
                        {
                            ModuleIds = ModuleIds.Substring(0, ModuleIds.Length - 1);
                        }
                        serializeModel.modules = ModuleIds.Split(',');
                    }

                    string userData = JsonConvert.SerializeObject(serializeModel);
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                             1,
                            user.UserName,
                             DateTime.Now,
                             DateTime.Now.AddMinutes(20),
                             false,
                             userData);

                    string encTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    faCookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(faCookie);

                    #region ByRoleID
                    // redirect for Secretary,other than secreatary, HOD, Other than Hod,
                    if (_UserType.Contains("2"))
                    {
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    }

                    else if (_UserType.Contains("3"))
                    {
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    }

                    else if (_UserType.Contains("15"))
                    {
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    }
                    else if (_UserType.Contains("16"))
                    {
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    }
                    else if (_UserType.Contains("37"))
                    {
                        //  var OId = (SBL.DomainModel.Models.Grievance.tGrievanceOfficerDeleted)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                        var OId = (int)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                        var MId = (int)Helper.ExecuteService("Grievance", "GetMemberByOfficeUser", CurrentSession.AadharId);
                        if (MId > 0)
                        {
                            CurrentSession.MemberCode = Convert.ToString(MId);
                        }
                        if (OId == Convert.ToInt32(officeId))
                        {

                            return RedirectToAction("Index", "Grievances", new { area = "Grievances" });
                        }
                        else
                        {

                            return RedirectToAction("Index", "Blank", new { area = "BlankRed" });
                        }

                    }
                    else if (_UserType.Contains("40"))
                    {
                        //  var OId = (SBL.DomainModel.Models.Grievance.tGrievanceOfficerDeleted)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                        //var OId = (int)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                        //  var MId = (int)Helper.ExecuteService("Grievance", "GetMemberByOfficeUser", CurrentSession.AadharId);
                        // if (MId > 0)
                        //  {
                        //     CurrentSession.MemberCode = Convert.ToString(MId);
                        // }
                        //if (OId == Convert.ToInt32(officeId))
                        //{

                        return RedirectToAction("Index", "Grievances", new { area = "Grievances" });
                        //}
                        //else
                        //{

                        //    return RedirectToAction("Index", "Blank", new { area = "BlankRed" });
                        //}

                    }
                    //------------------------------------------------------------------------------------------------------------------------------------------
                    //redirect for Vidhan Shabha Secretary,Other than Vidhan Shabha Secretary
                    else if (_UserType.Contains("4"))
                    {
                        return RedirectToAction("Index", "VidhanSabhaDept", new { area = "VidhanSabhaDepartment" });
                    }
                    else if (_UserType.Contains("5"))
                    {
                        return RedirectToAction("Index", "VidhanSabhaDept", new { area = "VidhanSabhaDepartment" });
                    }

                    //------------------------------------------------------------------------------------------------------------------------------------------
                    //redirect for member, spaker, dupty speaker,personal staf
                    else if (_UserType.Contains("22"))
                    {
                        return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                    }
                    else if (_UserType.Contains("17"))
                    {
                        return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                    }
                    else if (_UserType.Contains("18"))
                    {
                        return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                    }
                    else if (_UserType.Contains("36"))
                    {
                        return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                    }
                    else if (_UserType.Contains("38"))
                    {
                        if (!string.IsNullOrEmpty(CurrentSession.UserName))
                        {
                            string[] arr = CurrentSession.UserName.Split(',').ToArray();
                            CurrentSession.MemberCode = arr[0];
                            return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                        }
                    }
                    //------------------------------------------------------------------------------------------------------------------------------------------
                    //redirect for miniser, CPS
                    else if (_UserType.Contains("19"))
                    {
                        return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
                    }
                    else if (_UserType.Contains("20"))
                    {
                        return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
                    }
                    #endregion ByRoleID
                }

                #endregion CustumAuth

            }
            else
            {
                return RedirectToAction("Login", "Account", new { area = "" });
            }
            return View();
            //}
            //else
            //{
            //    return View();
            //}

        }







    }
}
