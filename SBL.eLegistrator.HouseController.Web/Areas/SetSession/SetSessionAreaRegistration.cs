﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SetSession
{
    public class SetSessionAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SetSession";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SetSession_default",
                "SetSession/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
