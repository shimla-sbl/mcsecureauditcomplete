﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PublishDocument
{
    public class PublishDocumentAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PublishDocument";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PublishDocument_default",
                "PublishDocument/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
