﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Passes;
using SBL.DomainModel.Models.PISModules;
using SBL.DomainModel.Models.PublishDocument;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Web.Areas.PublishDocument.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PublishDocument.Controllers
{
    public class PublishDocumentController : Controller
    {
        //
        // GET: /PublishDocument/PublishDocument/

        public ActionResult PublishDocumentIndexList()
        {
            PublishDocumentViewModel model = new PublishDocumentViewModel();
            PublishDocumentDetails OojectList = new PublishDocumentDetails();
            OojectList.IsUserType = CurrentSession.SubUserTypeID;
            OojectList.IsUserId = new Guid(CurrentSession.UserID);
            List<PublishDocumentDetails> ListdeprtmentPasses = new List<PublishDocumentDetails>();
            ListdeprtmentPasses = (List<PublishDocumentDetails>)Helper.ExecuteService("PublishDocument", "GetPublishDocumentList", OojectList);

            List<PublishDocumentDetails> NewPublishDocumentDetails = new List<PublishDocumentDetails>();
            List<PublishDocumentDetails> New1PublishDocumentDetails = new List<PublishDocumentDetails>();


            string Departments = CurrentSession.DeptID;
            string[] strArry = Departments.Split(',');

            foreach (var item in strArry)
            {
                NewPublishDocumentDetails = ListdeprtmentPasses.Where(x => x.TomdepartmentId == item || x.FromdepartmentId == item).ToList();
                if (NewPublishDocumentDetails.Count > 0)
                {
                    New1PublishDocumentDetails.AddRange(NewPublishDocumentDetails);
                }
            }

            model.PublishDocumentList = New1PublishDocumentDetails.ToModelList();
            // }


            return PartialView("/Areas/PublishDocument/Views/PublishDocument/_PublishDocumentList.cshtml", model);
        }
        public ActionResult PopulatePublisDocument(int Id, string Mode)
        {
            PublishDocumentViewModel model = new PublishDocumentViewModel();
            PublishDocumentDetails obj = new PublishDocumentDetails();
            mDepartmentPasses model1 = new mDepartmentPasses();
            if (Mode == "Edit")
            {
                PublishDocumentDetails EditObj = (PublishDocumentDetails)Helper.ExecuteService("PublishDocument", "GetDocumentById", new PublishDocumentDetails { PublishDocumentID = Id });
                if (EditObj != null)
                    obj = EditObj;

                model = obj.ToModel();
                ViewBag.ToOfficeId = model.ToOfficeId;
                ViewBag.DepartmentTo = model.DepartmentTo;
                ViewBag.FromOfficeId = model.FromOfficeId;
                ViewBag.DepartmentFrom = model.DepartmentFrom;
            }


            if (!string.IsNullOrEmpty(CurrentSession.AadharId as string) && !string.IsNullOrEmpty(CurrentSession.DeptID as string))
            {

                tPISEmployeePersonal offMdl = new tPISEmployeePersonal();

                offMdl.AadharID = CurrentSession.AadharId;
                offMdl.deptid = CurrentSession.DeptID;
                var OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);

                if (OfficeList.Count > 0)
                {
                    model.OfficeList = OfficeList;
                }


            }


            model.Mode = Mode;
            model.UserType = CurrentSession.SubUserTypeID;
            return PartialView("_PublishDocumentForm", model);
        }
        public ActionResult OfficeDrpList(string deptid)
        {
            tPISEmployeePersonal offMdl = new tPISEmployeePersonal();

            offMdl.AadharID = CurrentSession.AadharId;
            offMdl.deptid = deptid;
            var OfficeList = (List<mOffice>)Helper.ExecuteService("Department", "GetOfficeByAadharId", offMdl);
            return Json(OfficeList, JsonRequestBehavior.AllowGet);
            // return PartialView("_OfficeList", user);

        }


        public ActionResult SavePublishDocument(PublishDocumentViewModel model, HttpPostedFileBase file)
        {
            var EntityModel = model.ToSaveModel();
            var AuditModel = model.ToAuditModel();
            PublishDocumentDetails OojectList = new PublishDocumentDetails();
            OojectList.FromOfficeId = model.FromOfficeId;
            OojectList.DocumentType = model.DocumentType;
            var ListdeprtmentPasses = (List<PublishDocumentDetails>)Helper.ExecuteService("PublishDocument", "GetOfficeandDocumentCount", OojectList);

            if (model.Mode == "Add")
            {
                if (ListdeprtmentPasses.Count > 0)
                {
                    foreach (var item in ListdeprtmentPasses)
                    {
                        EntityModel.PublishDocumentID = item.PublishDocumentID;
                        EntityModel.FromdepartmentId = item.FromdepartmentId;
                        AuditModel.Attachment = item.Attachment;
                        AuditModel.fromDepartmentId = item.FromdepartmentId;
                        AuditModel.PublishId = item.PublishDocumentID;
                    }
                    //New Attachment when Office Id and DocumentType both are same.
                    Helper.ExecuteService("PublishDocument", "UpdatePublishDocument", EntityModel);
                    Helper.ExecuteService("PublishDocument", "AddauditPublishDocument", AuditModel);
                }
                else
                {
                    EntityModel.FromdepartmentId = CurrentSession.DeptID;
                    Helper.ExecuteService("PublishDocument", "AddPublishDocument", EntityModel);
                }
                ViewBag.Msg = "Sucessfully Save Publish Document";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            else
            {

                Helper.ExecuteService("PublishDocument", "UpdatePublishDocument", EntityModel);
                Helper.ExecuteService("PublishDocument", "AddauditPublishDocument", AuditModel);
                ViewBag.Msg = "Sucessfully Update Publish Document";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }

            return RedirectToAction("PublishDocumentIndexList", "PublishDocument", new { area = "PublishDocument" });

        }
        public ActionResult DeletePublisDocument(int Id)
        {
            Helper.ExecuteService("PublishDocument", "DeletePublisDocument", new PublishDocumentDetails { PublishDocumentID = Id });
            ViewBag.Msg = "Sucessfully deleted Publish Document";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            return RedirectToAction("PublishDocumentIndexList", "PublishDocument", new { area = "PublishDocument" });
        }

        [HttpPost]
        public string UploadFiles(HttpPostedFileBase file, string documentType, string OfficeName, string Mode)
        {
            if (file != null)
            {

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string FileLocation = "PublishDocument/File/Office";
                string url = FileSettings.SettingValue + FileLocation;
                DirectoryInfo directory = new DirectoryInfo(url);
                if (!directory.Exists)
                {
                    directory.Create();
                }

                string FileName = "";
                string extension = System.IO.Path.GetExtension(file.FileName);
                if (extension == ".pdf")
                {
                    // for GUID File Name
                    //var stream = file.InputStream;
                    //Guid PicName;
                    //PicName = Guid.NewGuid();
                    FileName = file.FileName;

                    string savedFileName = System.IO.Path.Combine(url, FileName);
                    file.SaveAs(savedFileName);
                    //* For File Rename
                    PublishDocumentDetails OojectList = new PublishDocumentDetails();
                    OojectList.FromOfficeId = Convert.ToInt32(OfficeName);
                    OojectList.DocumentType = documentType;
                    var ListdeprtmentPasses = (List<PublishDocumentDetails>)Helper.ExecuteService("PublishDocument", "GetOfficeandDocumentCount", OojectList);
                    Int32 PublishID = (Int32)Helper.ExecuteService("PublishDocument", "GetMaxPublishID", null);
                    Int32 AuditPublishID = (Int32)Helper.ExecuteService("PublishDocument", "GetMaxAuditPublishID", null);
                    if (documentType == "Rules/Policies/Directions")
                    {
                        documentType = "Rules_Policies_Directions";
                    }
                    if (Mode == "Edit" || ListdeprtmentPasses.Count > 0)
                    {
                        FileName = PublishID + "-" + AuditPublishID + "-" + OfficeName + "-" + documentType + "-" + "file" + extension;
                    }
                    else
                    {
                        FileName = PublishID + "-" + OfficeName + "-" + documentType + "-" + "file" + extension;
                    }


                    string DestFileName = System.IO.Path.Combine(url, FileName);
                    if (Directory.Exists(DestFileName))
                    {
                        System.IO.File.Delete(DestFileName);

                    }
                    System.IO.File.Move(savedFileName, DestFileName);

                    file.SaveAs(DestFileName);
                    //* End
                    var picLocation = FileName;
                    return picLocation;
                }
                else
                {
                    return "NotAllowed";
                }
            }
            return null;
        }


        public static string GenerateValue()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 2)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }
    }
}
