﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SBL.DomainModel.Models.PublishDocument;
using SBL.DomainModel.Models.Office;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Department;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.PublishDocument.Models
{
    public static class ModelMapping
    {
        static List<mOffice> ListofficeList = new List<mOffice>();
        public static List<PublishDocumentViewModel> ToModelList(this List<PublishDocumentDetails> entityList)
        {
            var ModelList = entityList.Select(entity => new PublishDocumentViewModel()
            {
                DepartmentFrom = GetDepartmentByOfficeId(entity.FromdepartmentId),
                PublishDocumentID = entity.PublishDocumentID,
                DepartmentTo = GetDepartmentByOfficeId(entity.TomdepartmentId),
                DocumentType = entity.DocumentType,
                Subject = entity.Subject,
                FromOfficeId = entity.FromOfficeId,
                ToOfficeId = entity.ToOfficeId,
                DocumentCreatedDate = entity.DocumentCreatedDate,
                Attachment = entity.Attachment,
                FromOfficeName = GetOfficeName(entity.FromOfficeId),
                ToOfficeName = GetOfficeName(entity.ToOfficeId)

            });
            return ModelList.ToList();
        }
        public static PublishDocumentViewModel ToModel(this PublishDocumentDetails model)
        {
            PublishDocumentViewModel entity = new PublishDocumentViewModel()
            {
                // DepartmentFrom = GetDepartmentByOfficeId(model.FromOfficeId),
                //DepartmentTo = GetDepartmentByOfficeId(model.ToOfficeId),
                DepartmentFrom = model.FromdepartmentId,
                DepartmentTo = model.TomdepartmentId,
                FromOfficeId = model.FromOfficeId,
                ToOfficeId = model.ToOfficeId,
                DocumentType = model.DocumentType,
                PublishDocumentID = model.PublishDocumentID,
                Subject = model.Subject,
                Attachment = model.Attachment,
                DocumentCreatedDate = DateTime.Now,
                FromOfficeName = GetOfficeName(model.FromOfficeId),
                AuditFile = model.Attachment,
                ToOfficeName = GetOfficeName(model.ToOfficeId)
            };


            return entity;
        }
        public static PublishDocumentDetails ToSaveModel(this PublishDocumentViewModel model)
        {
            PublishDocumentDetails entity = new PublishDocumentDetails()
            {
                PublishDocumentID = model.PublishDocumentID,
                FromdepartmentId = model.DepartmentFrom,
                TomdepartmentId = model.DepartmentTo,
                FromOfficeId = model.FromOfficeId,
                ToOfficeId = model.ToOfficeId,
                DocumentType = model.DocumentType,
                Subject = model.Subject,
                Attachment = model.Attachment,
                DocumentCreatedDate = DateTime.Now

            };
            return entity;
        }
        public static AuditPublishDocument ToAuditModel(this PublishDocumentViewModel model)
        {
            AuditPublishDocument entity = new AuditPublishDocument()
            {
                PublishId = model.PublishDocumentID,
                fromDepartmentId = model.DepartmentFrom,
                fromOfficeId = model.FromOfficeId,
                Documenttype = model.DocumentType,
                Attachment = model.AuditFile,
                auditDate = DateTime.Now

            };
            return entity;
        }
        public static string GetOfficeName(int OfficeID)
        {
            mOffice model = new mOffice();
            model.OfficeId = OfficeID;
            var officeName = (mOffice)Helper.ExecuteService("Office", "GetOfficeNameByOfficeID", model);
            if (officeName != null)
            {
                return officeName.officename;
            }
            return "";
        }
        public static string GetDepartmentByOfficeId(string DeptId)
        {

            mDepartment Dmodel = new mDepartment();
            Dmodel.deptId = DeptId;
            var Departname = (mDepartment)Helper.ExecuteService("Office", "GetDepartmentNameByDepartmentId", Dmodel);
            if (Departname != null)
            {
                return Departname.deptname;
            }
            return "";
        }

       

    }
}