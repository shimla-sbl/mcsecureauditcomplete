﻿using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Office;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.PublishDocument.Models
{
    public class PublishDocumentViewModel
    {
        public PublishDocumentViewModel()
        {
            this.OfficeList = new List<mOffice>();
            this.PublishDocumentList = new List<PublishDocumentViewModel>();
        }
        public int PublishDocumentID { get; set; }
        public string DepartmentFrom { get; set; }
        public int FromOfficeId { get; set; }
        public string DepartmentTo { get; set; }
        public int ToOfficeId { get; set; }
        public string DocumentType { get; set; }
        public string Subject { get; set; }
        public string Attachment { get; set; }
        public DateTime DocumentCreatedDate { get; set; }
        public string Mode { get; set; }

        public List<mDepartment> DepartmentFromList { get; set; }

        public List<mDepartment> DepartmentToList { get; set; }

        public virtual ICollection<mOffice> OfficeList { get; set; }

        public List<PublishDocumentViewModel> PublishDocumentList { get; set; }

        public string FromOfficeName { get; set; }

        public string ToOfficeName { get; set; }

        public string UserType { get; set; }

        public string AuditFile { get; set; }
    }
}