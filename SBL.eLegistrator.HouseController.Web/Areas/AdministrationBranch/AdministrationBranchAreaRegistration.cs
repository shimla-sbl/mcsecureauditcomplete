﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch
{
    public class AdministrationBranchAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AdministrationBranch";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AdministrationBranch_default",
                "AdministrationBranch/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
