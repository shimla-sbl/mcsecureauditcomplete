﻿using SBL.DomainModel.Models.Passes;
using SBL.eLegistrator.HouseController.Web.Areas.Media.Models;
using SBL.eLegistrator.HouseController.Web.Areas.Reporters.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using System.Web.Mvc;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Models
{
    public class AdministrationBranchDashboardModel
    {
        public AdministrationBranchDashboardModel()
        {
            this.ListPassCategory = new List<PassCategory>();
            this.ListJounalist = new List<JournalistViewModel>();
        }
        //Pass List of the Model to the 
        public List<mPassModel> mPassModelList { get; set; }
        public List<mPasses> mPassDeptList { get; set; }
        public List<mPasses> mPassTypelList { get; set; }
        public List<PassCategory> ListPassCategory { get; set; }
        public IEnumerable<JournalistViewModel> ListJounalist { get; set; }

        public int PassCategoryID { get; set; }

        public string AssemblyName { get; set; }

        public string SessionName { get; set; }

        public int Status { get; set; }
        public string Year { get; set; }

        public DateTime datePassReport { get; set; }
        public List<mSessionDateModel> mSessionDateList { get; set; }
        public List<mSessionDateModel> mSessionDateListFrom { get; set; }

        //For List Of Assembly and Session-- sanjay
        [HiddenInput(DisplayValue = false)]
        public int AssemblyId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SessionId { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mAssembly> mAssemblyList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mSession> sessionList { get; set; }
        public string ReportWise { get; set; }
        public string ActionFirstValue { get; set; }
        public string TrnsferValue { get; set; }
        public string ReportInformation { get; set; }

        public string CurrentDateFrom { get; set; }
        public string CurrentDateTo { get; set; }
        public string DepartmentID { get; set; }

        public int TotalCount { get; set; }
        public int TotalDpartPanding { get; set; }
        public int TotalDpartApproval { get; set; }
        public int TotalJouApproval { get; set; }
        public int TotalJouPanding { get; set; }
        public int TotalPublicPanding { get; set; }
        public int TotalPublicApproval { get; set; }
        public int pageMode { get; set; }

        public List<SBL.DomainModel.Models.Department.mDepartment> mDepartment { get; set; }
    }
}