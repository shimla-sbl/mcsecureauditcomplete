﻿using SBL.DomainModel.Models.Passes;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Models
{
    public class mPassModel : PhotoCaptureGenericModel
    {
        public int PassID { get; set; }

        public Int32? PassCode { get; set; }

        public int RequestedPassCategoryID { get; set; }

        public int ApprovedPassCategoryID { get; set; }

        public int AssemblyCode { get; set; }

        public int SessionCode { get; set; }

        public string Prefix { get; set; }

        public string AadharID { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        public int? Age { get; set; }

        public string FatherName { get; set; }

        public int? NoOfPersions { get; set; }

        public string RecommendationType { get; set; }

        public string RecommendationBy { get; set; }

        public string RecommendationDescription { get; set; }

        public string MobileNo { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string OrganizationName { get; set; }

        public string Designation { get; set; }
        
        public string DayTime { get; set; }

        public TimeSpan? Time { get; set; }

        public string DepartmentID { get; set; }
        
        public string SessionDateFrom { get; set; }
        
        public string SessionDateTo { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public string ApprovedBy { get; set; }

        public string VehicleNumber { get; set; }

        public string GateNumber { get; set; }

        public bool IsRequested { get; set; }

        public DateTime? RequestedDate { get; set; }

        public int? Status { get; set; }

        public string RequestedBy { get; set; }

        public bool IsActive { get; set; }

        public string RecommendationByName { get; set; }

        public string Mode { get; set; }

        public string RejectedBy { get; set; }

        public DateTime? RejectionDate { get; set; }

        public string RejectionReason { get; set; }

        public string RequestedPassCategoryName { get; set; }

        public string AllowedPassCategoryName { get; set; }

        public List<PassCategory> ListPassCategory { get; set; }

        public string PassCategoryName { get; set; }

        public string PassCategoryTemplate { get; set; }

        public string AssemblyName { get; set; }

        public string SessionName { get; set; }

        public string xmlDocument { get; set; }

         public string RequestID { get; set; }

         public int TotalDeptCount { get; set; }

         public int TotalCount { get; set; }

         public virtual ICollection<mSessionDate> mSessionDateList { get; set; }
    }
}