﻿using SBL.DomainModel.Models.Passes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Models
{
    public class LeftNavigationMenuModel
    {
        public LeftNavigationMenuModel()
        {
            this.ListPassCategory = new List<PassCategory>();
        }

        public int TotalPublicPassCount { get; set; }

        public int PendingPublicPassCount { get; set; }

        public int ApprovedPublicPassCount { get; set; }

        public int RejectedPublicPassCount { get; set; }

        public int TotalPournalistPassesPending { get; set; }

        public int PendingEmployeePasses { get; set; }

        public int PendingID { get; set; }

        public int ApprovedID { get; set; }

        public int RejectedID { get; set; }

        public List<PassCategory> ListPassCategory { get; set; }

        [NotMapped]
       public int SessionID { get; set; }
    }
}