﻿namespace SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Controllers
{
    #region Namespace reffrences.
    using Microsoft.Security.Application;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Enums;
    using SBL.DomainModel.Models.Media;
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.eLegistrator.HouseController.Filters;
    using SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Extensions;
    using SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Models;
    using SBL.eLegistrator.HouseController.Web.Areas.Media.Models;
    using SBL.eLegistrator.HouseController.Web.Areas.Reporters.Models;
    using SBL.eLegistrator.HouseController.Web.Filters;
    using SBL.eLegistrator.HouseController.Web.Helpers;
    using SBL.eLegistrator.HouseController.Web.Utility;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    #endregion

    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class AdministrationBranchController : Controller
    {
        #region Variable Initialization
        static int SessionCode = 0;
        static int AssemblyCode = 0;
#pragma warning disable CS0414 // The field 'AdministrationBranchController.TotalPendingMasterPasses' is assigned but its value is never used
        static int TotalPendingMasterPasses = 0;
#pragma warning restore CS0414 // The field 'AdministrationBranchController.TotalPendingMasterPasses' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'AdministrationBranchController.TotalApprovedMatsterPasses' is assigned but its value is never used
        static int TotalApprovedMatsterPasses = 0;
#pragma warning restore CS0414 // The field 'AdministrationBranchController.TotalApprovedMatsterPasses' is assigned but its value is never used
        static int TotalRejectedPassesCount = 0;
        static string SessionNameStatic = string.Empty;
        static string AssesmblyNameStatic = string.Empty;

        #endregion

        //public ActionResult Index(int Status = 0, int PassCatID = 0, string DepartmentCatID = "")
        //{
        //    if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

        //    var masterPasses = new List<mPassModel>();
        //    var masterPassesForView = new List<mPassModel>();
        //    mPasses mpass = new mPasses();

        //    AdministrationBranchDashboardModel model = new AdministrationBranchDashboardModel();

        //    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
        //    {
        //        mpass.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);

        //    }

        //    if (!string.IsNullOrEmpty(CurrentSession.SessionId))
        //    {
        //        mpass.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
        //    }

        //    var model1 = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetAdminAssemblyList", mpass);
        //    if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
        //    {
        //        CurrentSession.AssemblyId = model1.AssemblyCode.ToString();
        //    }

        //    if (string.IsNullOrEmpty(CurrentSession.SessionId))
        //    {
        //        CurrentSession.SessionId = model1.SessionCode.ToString();
        //    }

        //    SessionCode = model1.SessionCode;
        //    AssemblyCode = model1.AssemblyCode;

        //    mSession Mdl = new mSession();
        //    Mdl.SessionCode = SessionCode;
        //    Mdl.AssemblyID = AssemblyCode;
        //    mAssembly assmblyMdl = new mAssembly();
        //    assmblyMdl.AssemblyID = AssemblyCode;

        //    var SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
        //    var AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

        //    switch (Status)
        //    {
        //        case 0:
        //            { //masterPasses = GetResultsAsList(0); 
        //                break;
        //            }
        //        case 1: //Pending Passes From Department, Reception.
        //            {
        //                #region Pending Pass Requests
        //                var pendingPassRequestDept = (List<mDepartmentPasses>)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassRequests", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });
        //                var pendingList = pendingPassRequestDept.ToModelList();

        //                var pendingPassRequestRec = (List<PublicPass>)Helper.ExecuteService("AdministrationBranch", "GetReceptionPassRequests", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });
        //                pendingList.AddRange(pendingPassRequestRec.ToModelList());
        //                //TotalPendingMasterPasses = pendingList.Count; 
        //                #endregion
        //                Session["pendingPassRequestDept"] = pendingPassRequestDept;
        //                Session["pendingPassRequestRec"] = pendingPassRequestRec;
        //                masterPasses = pendingList;
        //                break;
        //            }
        //        case 2:  //Approved passes by the Administrative Branch.
        //            {
        //                #region Approved Passes
        //                var masterPassesFromDB = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetMasterPassesByStatus", new mPasses { Status = Status, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
        //                var masterPassesFromDBToModel = masterPassesFromDB.ToModelListForGrid();
        //                //TotalApprovedMatsterPasses = masterPassesFromDBToModel.Count;
        //                #endregion

        //                masterPasses = masterPassesFromDBToModel;
        //                break;
        //            }
        //        case 3:  //Rejected Passes by the Administrative Branch. 
        //            {
        //                var masterPassesFromDB = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetMasterPassesByStatus", new mPasses { Status = Status, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
        //                var masterPassesFromDBToModel = masterPassesFromDB.ToModelList();
        //                masterPasses = masterPassesFromDBToModel;

        //                TotalRejectedPassesCount = masterPassesFromDBToModel.Count;
        //                break;
        //            }
        //    }

        //    if (PassCatID == 0 && DepartmentCatID == "")
        //    {
        //        masterPassesForView = masterPasses;

        //    }
        //    else
        //    {
        //        if (Status == 1)
        //        {
        //            if (PassCatID != 0 && DepartmentCatID == "")
        //            {
        //                masterPassesForView = masterPasses.Where(m => m.RequestedPassCategoryID == PassCatID).ToList();
        //            }
        //            else if (PassCatID == 0 && DepartmentCatID != "")
        //            {
        //                masterPassesForView = masterPasses.Where(m => m.DepartmentID == DepartmentCatID).ToList();
        //            }
        //            else if (PassCatID != 0 && DepartmentCatID != "")
        //            {
        //                masterPassesForView = masterPasses.Where(m => m.RequestedPassCategoryID == PassCatID && m.DepartmentID == DepartmentCatID).ToList();
        //            }
        //        }
        //        if (Status == 2)
        //        {
        //            if (PassCatID != 0 && DepartmentCatID == "")
        //            {
        //                masterPassesForView = masterPasses.Where(m => m.ApprovedPassCategoryID == PassCatID).ToList();
        //            }
        //            else if (PassCatID == 0 && DepartmentCatID != "")
        //            {
        //                masterPassesForView = masterPasses.Where(m => m.DepartmentID == DepartmentCatID).ToList();
        //            }
        //            else if (PassCatID != 0 && DepartmentCatID != "")
        //            {
        //                masterPassesForView = masterPasses.Where(m => m.DepartmentID == DepartmentCatID && m.ApprovedPassCategoryID == PassCatID).ToList();
        //            }
        //        }
        //    }

        //    var passCategoryList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
        //    var DepartmentCategoryList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

        //    model.mPassModelList = masterPassesForView;
        //    model.Status = Status;
        //    model.ListPassCategory = passCategoryList;
        //    model.PassCategoryID = PassCatID;
        //    model.mDepartment = DepartmentCategoryList;
        //    model.DepartmentID = DepartmentCatID;
        //    model.SessionId = model1.SessionCode;
        //    model.AssemblyId = model1.AssemblyCode;
        //    model.AssemblyName = model1.AssesmblyName;
        //    model.SessionName = model1.SessionName;
        //    model.mAssemblyList = model1.mAssemblyList;
        //    model.sessionList = model1.sessionList;

        //    if (Status == 1)
        //    {
        //        return PartialView("_PassesGridPending", model);
        //    }
        //    else if (Status == 2)
        //    {
        //        return PartialView("_PassesGridApproved", model);
        //    }
        //    else
        //    {
        //        return View(model);
        //    }
        //    //if (!Request.IsAjaxRequest())
        //    //{
        //    //    return View(model);
        //    //}
        //    //else
        //    //{
        //    //    return View(model);
        //    //}
        //    // return View();
        //}

        public ActionResult Index(int pageId = 1, int pageSize = 50, int Status = 0, int PassCatID = 0, string DepartmentCatID = "")
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            var masterPasses = new List<mPassModel>();
            var masterPassesForView = new List<mPassModel>();
            mPasses mpass = new mPasses();

            AdministrationBranchDashboardModel model = new AdministrationBranchDashboardModel();

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                mpass.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                mpass.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
            }

            var model1 = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetAdminAssemblyList", mpass);
            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model1.AssemblyCode.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model1.SessionCode.ToString();
            }

            SessionCode = model1.SessionCode;
            AssemblyCode = model1.AssemblyCode;

            mSession Mdl = new mSession();
            Mdl.SessionCode = SessionCode;
            Mdl.AssemblyID = AssemblyCode;
            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = AssemblyCode;

            var SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
            var AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

            switch (Status)
            {
                case 0:
                    { //masterPasses = GetResultsAsList(0); 
                        break;
                    }
                case 1: //Pending Passes From Department, Reception.
                    {
                        #region Pending Pass Requests
                        var pendingPassRequestDept = (List<mDepartmentPasses>)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassRequests", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                        var pendingList = pendingPassRequestDept.ToModelList();

                        var pendingPassRequestRec = (List<PublicPass>)Helper.ExecuteService("AdministrationBranch", "GetReceptionPassRequests", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                        pendingList.AddRange(pendingPassRequestRec.ToModelList());
                        //TotalPendingMasterPasses = pendingList.Count; 
                        #endregion
                        Session["pendingPassRequestDept"] = pendingPassRequestDept;
                        Session["pendingPassRequestRec"] = pendingPassRequestRec;
                        masterPasses = pendingList;
                        pageSize = masterPasses.Count();
                        break;
                    }
                case 2:  //Approved passes by the Administrative Branch.
                    {
                        #region Approved Passes
                        var masterPassesFromDB = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetMasterPassesByStatus", new mPasses { Status = Status, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                        var masterPassesFromDBToModel = masterPassesFromDB.ToModelListForGrid();
                        //TotalApprovedMatsterPasses = masterPassesFromDBToModel.Count;
                        #endregion

                        //ViewBag.PageSize = pageSize;
                        //List<mPassModel> pagedRecord = new List<mPassModel>();
                        //if (pageId == 1)
                        //{
                        //    pagedRecord = masterPassesFromDBToModel.Take(pageSize).ToList();
                        //}
                        //else
                        //{
                        //    int r = (pageId - 1) * pageSize;
                        //    pagedRecord = masterPassesFromDBToModel.Skip(r).Take(pageSize).ToList();
                        //}

                        //ViewBag.CurrentPage = pageId;
                        //ViewData["PagedList"] = Helper.BindPager(masterPassesFromDB.Count, pageId, pageSize);
                        //masterPasses = pagedRecord;


                        masterPasses = masterPassesFromDBToModel;
                        break;
                    }
                case 3:  //Rejected Passes by the Administrative Branch. 
                    {
                        var masterPassesFromDB = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetMasterPassesByStatus", new mPasses { Status = Status, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                        var masterPassesFromDBToModel = masterPassesFromDB.ToModelList();
                        masterPasses = masterPassesFromDBToModel;

                        TotalRejectedPassesCount = masterPassesFromDBToModel.Count;
                        break;
                    }
            }

            if (PassCatID == 0 && DepartmentCatID == "")
            {
                masterPassesForView = masterPasses;

            }
            else
            {
                if (Status == 1)
                {
                    if (PassCatID != 0 && DepartmentCatID == "")
                    {
                        masterPassesForView = masterPasses.Where(m => m.RequestedPassCategoryID == PassCatID).ToList();
                    }
                    else if (PassCatID == 0 && DepartmentCatID != "")
                    {
                        masterPassesForView = masterPasses.Where(m => m.DepartmentID == DepartmentCatID).ToList();
                    }
                    else if (PassCatID != 0 && DepartmentCatID != "")
                    {
                        masterPassesForView = masterPasses.Where(m => m.RequestedPassCategoryID == PassCatID && m.DepartmentID == DepartmentCatID).ToList();
                    }
                }
                if (Status == 2)
                {
                    if (PassCatID != 0 && DepartmentCatID == "")
                    {
                        masterPassesForView = masterPasses.Where(m => m.ApprovedPassCategoryID == PassCatID).ToList();
                        //pageSize = masterPassesForView.Count();
                    }
                    else if (PassCatID == 0 && DepartmentCatID != "")
                    {
                        masterPassesForView = masterPasses.Where(m => m.DepartmentID == DepartmentCatID).ToList();
                        //pageSize = masterPassesForView.Count();
                    }
                    else if (PassCatID != 0 && DepartmentCatID != "")
                    {
                        masterPassesForView = masterPasses.Where(m => m.DepartmentID == DepartmentCatID && m.ApprovedPassCategoryID == PassCatID).ToList();
                        //pageSize = masterPassesForView.Count();
                    }
                }
            }

            ViewBag.PageSize = pageSize;
            List<mPassModel> pagedRecord = new List<mPassModel>();
            if (pageId == 1)
            {
                pagedRecord = masterPassesForView.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = masterPassesForView.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            if (masterPassesForView.Count != 0)
            {
                ViewData["PagedList"] = Helper.BindPager(masterPassesForView.Count, pageId, pageSize);
            }
            masterPassesForView = pagedRecord;
            var passCategoryList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
            var DepartmentCategoryList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);

            model.mPassModelList = masterPassesForView;
            model.Status = Status;
            model.ListPassCategory = passCategoryList;
            model.PassCategoryID = PassCatID;
            model.mDepartment = DepartmentCategoryList;
            model.DepartmentID = DepartmentCatID;
            model.SessionId = model1.SessionCode;
            model.AssemblyId = model1.AssemblyCode;
            model.AssemblyName = model1.AssesmblyName;
            model.SessionName = model1.SessionName;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;
            model.pageMode = Status;
         


            if (Status == 1)
            {
                return PartialView("_PassesGridPending", model);
            }
            else if (Status == 2)
            {
                return PartialView("_PassesGridApproved", model);
            }
            else if (Status == 3)
            {
                return PartialView("_PassesGridRejected", model);
            }
            else
            {
                return View(model);
            }
            //if (!Request.IsAjaxRequest())
            //{
            //    return View(model);
            //}
            //else
            //{
            //    return View(model);
            //}
            // return View();
        }





        public PartialViewResult GetIndexByPaging(int pageId, int pageSize, int PassCatID = 0, string DepartmentCatID = "")
        {
            int Status = 2;
            // int PassCatID = 0;
            // string DepartmentCatID = "";

            var masterPasses = new List<mPassModel>();
            var masterPassesForView = new List<mPassModel>();
            mPasses mpass = new mPasses();

            AdministrationBranchDashboardModel model = new AdministrationBranchDashboardModel();

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                mpass.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                mpass.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
            }

            var model1 = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetAdminAssemblyList", mpass);
            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model1.AssemblyCode.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model1.SessionCode.ToString();
            }

            SessionCode = model1.SessionCode;
            AssemblyCode = model1.AssemblyCode;

            mSession Mdl = new mSession();
            Mdl.SessionCode = SessionCode;
            Mdl.AssemblyID = AssemblyCode;
            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = AssemblyCode;

            var SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
            var AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

            var masterPassesFromDB = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetMasterPassesByStatus", new mPasses { Status = Status, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
            if (Status == 2)
            {
                if (PassCatID != 0 && DepartmentCatID == "")
                {
                    masterPassesFromDB = masterPassesFromDB.Where(m => m.ApprovedPassCategoryID == PassCatID).ToList();
                    //pageSize = masterPassesForView.Count();
                }
                else if (PassCatID == 0 && DepartmentCatID != "")
                {
                    masterPassesFromDB = masterPassesFromDB.Where(m => m.DepartmentID == DepartmentCatID).ToList();
                    //pageSize = masterPassesForView.Count();
                }
                else if (PassCatID != 0 && DepartmentCatID != "")
                {
                    masterPassesFromDB = masterPassesFromDB.Where(m => m.DepartmentID == DepartmentCatID && m.ApprovedPassCategoryID == PassCatID).ToList();
                    //pageSize = masterPassesForView.Count();
                }
            }
            ViewBag.PageSize = pageSize;

            List<mPasses> pagedRecord = new List<mPasses>();
            if (pageId == 1)
            {
                pagedRecord = masterPassesFromDB.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = masterPassesFromDB.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(masterPassesFromDB.Count, pageId, pageSize);
            var masterPassesFromDBToModel = pagedRecord.ToModelListForGrid();

            var passCategoryList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
            var DepartmentCategoryList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("PrintingPress", "GetDepartment", null);


            model.mPassModelList = masterPassesFromDBToModel;
            model.Status = Status;
            model.ListPassCategory = passCategoryList;
            model.PassCategoryID = PassCatID;
            model.mDepartment = DepartmentCategoryList;
            model.DepartmentID = DepartmentCatID;
            model.SessionId = model1.SessionCode;
            model.AssemblyId = model1.AssemblyCode;
            model.AssemblyName = model1.AssesmblyName;
            model.SessionName = model1.SessionName;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;
            return PartialView("/Areas/AdministrationBranch/Views/AdministrationBranch/_PassesGridApproved.cshtml", model);
        }

        public PartialViewResult LeftNavigationMenu()
        {
            int PendingPassesCount = 0;
            int ApprovedPassesCount = 0;
            int PendingJournalists = 0;
            int PendingEmployeePasses = 0;

            //Pending Pass Count
            #region Pending Pass Requests Count

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                SessionCode = Convert.ToInt16(CurrentSession.SessionId);
            }

            var pendingPassRequestDept = (Int32)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassRequestsCount", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });
            var pendingPassRequestRec = (Int32)Helper.ExecuteService("AdministrationBranch", "GetReceptionPassRequestsCount", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });

            // var GetPublicPassesRequestsCount = (Int32)Helper.ExecuteService("AdministrationBranch", "GetPublicPassesRequestsCount", new PublicPass { AssemblyCode = AssemblyCode, SessionCode = SessionCode });
            // var GetDepartmentPassesRequestsCount = (Int32)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassesRequestsCount", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });
            var GetPublicDepartmentPassCount = (Int32)Helper.ExecuteService("AdministrationBranch", "GetPublicDepartmentPassCount", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });

            //PendingJournalists = AllPassRequestsMediaModel(DateTime.Now.Year.ToString()).Count;
            var pJournalists = AllPassRequestsMediaModelForGrid(Convert.ToString(SessionCode)) as List<JournalistViewModel>;

            var totJournalists = pJournalists.Count;
            var journalistInSession = pJournalists.Where(m => m.IsInCurrentSession == true).ToList();
            var journalistInSessionCount = journalistInSession.Count;

            PendingJournalists = totJournalists - journalistInSessionCount;
            Session["pendingPassRequestDept"] = pendingPassRequestDept;
            Session["pendingPassRequestRec"] = pendingPassRequestRec;
            Session["PendingJournalists"] = PendingJournalists;
            Session["journalistInSessionCount"] = journalistInSessionCount;

            PendingEmployeePasses = pendingPassRequestDept;
            PendingPassesCount = pendingPassRequestDept + pendingPassRequestRec + PendingJournalists;
            #endregion

            //ApprovedPassCount.
            #region Pending Pass Requests Count
            var ApprovedPasses = (Int32)Helper.ExecuteService("AdministrationBranch", "GetMasterPassesByStatusCount", new mPasses { Status = 2, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
            var RejectedPasses = (int)Helper.ExecuteService("AdministrationBranch", "GetMasterPassesByStatusCount", new mPasses { Status = 3, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
            ApprovedPassesCount = ApprovedPasses;
            #endregion

            //Rejected Pass Count.
            if (TotalRejectedPassesCount == 0)
            {
            }

            var passCategoryList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
            var model = new LeftNavigationMenuModel()
            {
                TotalPublicPassCount = GetPublicDepartmentPassCount + totJournalists,//PendingEmployeePasses + ApprovedPassesCount + PendingJournalists,
                PendingPublicPassCount = PendingPassesCount,
                ApprovedPublicPassCount = ApprovedPassesCount,
                RejectedPublicPassCount = RejectedPasses,
                TotalPournalistPassesPending = PendingJournalists,
                PendingEmployeePasses = PendingEmployeePasses + pendingPassRequestRec,
                ListPassCategory = passCategoryList,
                SessionID = SessionCode
            };


            return PartialView("_LeftNavigationMenu", model);
        }

        public List<JournalistViewModel> AllPassRequestsMediaModelForGrid(string Id)
        {


            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                SessionCode = Convert.ToInt16(CurrentSession.SessionId);
            }
            JournalistsForPass JournalistForPassModel = new JournalistsForPass();
            JournalistForPassModel.SessionCode = Convert.ToInt32(Id);
            // JournalistForPassModel.SessionCode = SessionCode;
            JournalistForPassModel.AssemblyCode = AssemblyCode;

            var journalist = (List<Journalist>)Helper.ExecuteService("Media", "GetRequestedMediaByYear", JournalistForPassModel);

            IEnumerable<JournalistViewModel> ListJounalist = journalist.ToViewModel();

            var ViewModel = new List<JournalistViewModel>();
            ViewModel = ListJounalist.Select(a => new JournalistViewModel()
            {
                JournalistID = a.JournalistID,
                Name = a.Name,
                Email = a.Email,
                IsActive = a.IsActive,
                Phone = a.Phone,
                UID = a.UID,
                AcreditationNumber = a.AcreditationNumber,
                OrganizationID = a.OrganizationID,
                Photo = a.ImageLocation,
                OrganisationName = a.OrganisationName,
                Designation = a.Designation,
                Address = a.Address,
                FathersName = a.FathersName,
                RequestId = GetJournalistsForPassId(a.JournalistID, Id, AssemblyCode),
                IsCurrentYear = DateTime.Now.Year == Convert.ToInt16(Id),
                //IsInCurrentSession = ChkJounalistForCurrentSession(a.JournalistID, Id, AssemblyCode, SessionCode)
                IsInCurrentSession = a.IsInCurrentSession
            }).ToList();

            return ViewModel;
        }

        public List<JournalistViewModel> AllPassRequestsMediaModel(string Id)
        {

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                SessionCode = Convert.ToInt16(CurrentSession.SessionId);
            }

            JournalistsForPass JournalistForPassModel = new JournalistsForPass();
            JournalistForPassModel.SessionCode = Convert.ToInt32(Id);
            //  JournalistForPassModel.SessionCode = SessionCode;
            JournalistForPassModel.AssemblyCode = AssemblyCode;

            var journalist = (List<Journalist>)Helper.ExecuteService("Media", "GetRequestedMediaByYear", JournalistForPassModel);

            IEnumerable<JournalistViewModel> ListJounalist = journalist.ToViewModel();

            var ViewModel = new List<JournalistViewModel>();
            ViewModel = ListJounalist.Select(a => new JournalistViewModel()
            {
                JournalistID = a.JournalistID,
                Name = a.Name,
                Email = a.Email,
                IsActive = a.IsActive,
                Phone = a.Phone,
                UID = a.UID,
                AcreditationNumber = a.AcreditationNumber,
                OrganizationID = a.OrganizationID,
                Photo = a.ImageLocation,
                OrganisationName = a.OrganisationName,
                Designation = a.Designation,
                Address = a.Address,
                FathersName = a.FathersName,
                RequestId = GetJournalistsForPassId(a.JournalistID, Id, AssemblyCode),
                IsCurrentYear = DateTime.Now.Year == Convert.ToInt16(Id),
                //IsInCurrentSession = ChkJounalistForCurrentSession(a.JournalistID, Id, AssemblyCode, SessionCode)
                IsInCurrentSession = a.IsInCurrentSession
            }).ToList();

            return ViewModel;
        }

        public Int32 AllPassRequestsMediaModelCount(string Id)
        {
            Int32 journalistCount = 0;
            if (SessionCode == 0 && AssemblyCode == 0)
            {
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                SessionCode = siteSettingMod.SessionCode;
                AssemblyCode = siteSettingMod.AssemblyCode;
            }

            JournalistsForPass JournalistForPassModel = new JournalistsForPass();
            JournalistForPassModel.Year = Id;
            var journalist = (List<Journalist>)Helper.ExecuteService("Media", "GetRequestedMediaByYear", JournalistForPassModel);

            if (journalist != null)
            {
                journalistCount = journalist.Count;
            }
            return journalistCount;
        }

        public ActionResult ListOfJournalistPass(string Id)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            try
            {
                Id = Sanitizer.GetSafeHtmlFragment(Id);
                AdministrationBranchDashboardModel model = new AdministrationBranchDashboardModel();

                //var ViewModel = AllPassRequestsMediaModel(Id); //.Where(m => m.IsInCurrentSession != null || m => m.IsInCurrentSession != true)
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                model.Year = Convert.ToString(siteSettingMod.SessionCode);
                mPasses mpass = new mPasses();

                var model1 = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetAdminAssemblyList", mpass);
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                ///
                /// Here Year work As a SessionCode (Year=sessioncode)
                ///
                //  model.Year = Convert.ToString(model.SessionId);
                // model.ListJounalist = null;
                mSession Mdl = new mSession();
                Mdl.SessionCode = model.SessionId;
                Mdl.AssemblyID = model.AssemblyId;
                mAssembly assmblyMdl = new mAssembly();
                assmblyMdl.AssemblyID = model.AssemblyId;
                var SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                var AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
                model.AssemblyName = AssesmblyName;
                model.SessionName = SessionName;
                model.mAssemblyList = model1.mAssemblyList;
                model.sessionList = model1.sessionList;
                return PartialView("_ListOfJournalistPass", model);
                // return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return null;
            }
        }
        public ActionResult ListOfJournalistPassPartial(string Id)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            try
            {
                Id = Sanitizer.GetSafeHtmlFragment(Id);
                AdministrationBranchDashboardModel model = new AdministrationBranchDashboardModel();

                var ViewModel = AllPassRequestsMediaModel(Id); //.Where(m => m.IsInCurrentSession != null || m => m.IsInCurrentSession != true)
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                model.Year = Convert.ToString(siteSettingMod.SessionCode);
                mPasses mpass = new mPasses();

                var model1 = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetAdminAssemblyList", mpass);
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }

                ///
                /// Here Year work As a SessionCode (Year=sessioncode)
                ///
                //  model.Year = Convert.ToString(model.SessionId);
                model.ListJounalist = ViewModel;
                mSession Mdl = new mSession();
                Mdl.SessionCode = model.SessionId;
                Mdl.AssemblyID = model.AssemblyId;
                mAssembly assmblyMdl = new mAssembly();
                assmblyMdl.AssemblyID = model.AssemblyId;
                var SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                var AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);
                model.AssemblyName = AssesmblyName;
                model.SessionName = SessionName;
                model.mAssemblyList = model1.mAssemblyList;
                model.sessionList = model1.sessionList;
                return PartialView("_ListOfJournalistPass", model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return null;
            }
        }
        public bool ChkJounalistForCurrentSession(int JournalistID, string year, int currentAssemblyCode, int currentSessionCode)
        {
            bool status = false;

            JournalistsForPass model = new JournalistsForPass();
            model.JournalistId = JournalistID;
            model.Year = year;
            var Journalist = (JournalistsForPass)Helper.ExecuteService("Media", "GetAllJournalistsForPassByIdYear", model);
            var singleJournalist = Journalist;

            if (singleJournalist != null)
            {
                if (singleJournalist.AssemblyCode != null && singleJournalist.SessionCode != null)
                {
                    status = currentAssemblyCode == singleJournalist.AssemblyCode;
                    status = currentSessionCode == singleJournalist.SessionCode;
                }
            }
            return status;
        }

        public Int32 GetJournalistsForPassId(int JournalistID, string year, int AssemblyCode)
        {
            JournalistsForPass model = new JournalistsForPass();
            model.JournalistId = JournalistID;
            model.SessionCode = Convert.ToInt32(year);
            model.AssemblyCode = AssemblyCode;
            var Journalist = (JournalistsForPass)Helper.ExecuteService("Media", "GetAllJournalistsForPassByIdSession", model);
            var singleJournalist = Journalist;

            if (singleJournalist != null)
            {
                return singleJournalist.JournalistsForPassId;
            }
            return 0;
        }

        public ActionResult FullMasterPassDetails(int Id)
        {
            var masterPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = Id });
            var masterPassModel = masterPass.ToModel();
            return View("_FullPassDetails", masterPassModel);
        }

        public object SubmitSelectedForCurrentSession(string JIDs)
        {
            string[] JornalistIDs = JIDs.Split(',');

            foreach (var JID in JornalistIDs)
            {
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                JournalistsForPass model = new JournalistsForPass();
                model.JournalistsForPassId = Convert.ToInt16(JID);
                model.Year = DateTime.Now.Year.ToString();
                model.SessionCode = siteSettingMod.SessionCode;
                model.AssemblyCode = siteSettingMod.AssemblyCode;
                var Journalist = (List<JournalistsForPass>)Helper.ExecuteService("Media", "GetJournalistsForPassByID", model);
                JournalistsForPass singleJournalist = new JournalistsForPass();
                singleJournalist = Journalist.FirstOrDefault() as JournalistsForPass;
                singleJournalist.AssemblyCode = siteSettingMod.AssemblyCode;
                singleJournalist.SessionCode = siteSettingMod.SessionCode;
                singleJournalist.IsJournalistIdForApproval = true;
                var updatedRec = Helper.ExecuteService("Media", "AddJournalistToCurrentSession", singleJournalist) as JournalistsForPass;
                if (updatedRec.JournalistId != 0)
                {
                    //Add the new entry to mPasses table.
                    var journalist = Helper.ExecuteService("Media", "GetJournalistsById", new Journalist { JournalistID = updatedRec.JournalistId }) as Journalist;
                    var journalistTomPass = journalist.TomPassEntity(siteSettingMod.AssemblyCode, siteSettingMod.SessionCode, 2);

                    if (journalist.IsAcredited == false)
                    {
                        journalistTomPass.ApprovedPassCategoryID = 11;
                        journalistTomPass.RequestedPassCategoryID = 11;
                    }
                    string Url = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImageLocation(siteSettingMod.AssemblyCode, siteSettingMod.SessionCode);
                    var ServerLocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting1", null);
                    var mediaSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMediaFileSetting", null);
                    DirectoryInfo Dir = new DirectoryInfo(Url);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    if (journalistTomPass.Photo != null)
                    {
                        string SourceFile = ServerLocation.SettingValue + mediaSettings.SettingValue + journalistTomPass.Photo;
                        string destFile = System.IO.Path.Combine(Url, journalistTomPass.Photo);
                        if (System.IO.File.Exists(destFile))
                        {
                            System.IO.File.Delete(destFile);
                        }
                        if (System.IO.File.Exists(SourceFile))
                        {
                            System.IO.File.Copy(SourceFile, destFile);
                        }


                    }
                    else
                    {
                        journalistTomPass.Photo = "No Photo";
                    }


                    Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPass", journalistTomPass);
                }
            }
            return Convert.ToInt32(CurrentSession.SessionId);
        }

        public object SubmitForCurrentSession(string ID)
        {
            JournalistsForPass model = new JournalistsForPass();
            model.JournalistsForPassId = Convert.ToInt16(ID);
            model.Year = DateTime.Now.Year.ToString();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
            }
            var Journalist = (List<JournalistsForPass>)Helper.ExecuteService("Media", "GetJournalistsForPassByID", model);
            JournalistsForPass singleJournalist = new JournalistsForPass();
            singleJournalist = Journalist.FirstOrDefault();

            // SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            singleJournalist.AssemblyCode = model.AssemblyCode;
            singleJournalist.SessionCode = model.SessionCode;
            singleJournalist.IsJournalistIdForApproval = true;
            var updatedRec = Helper.ExecuteService("Media", "AddJournalistToCurrentSession", singleJournalist) as JournalistsForPass;
            if (updatedRec.JournalistId != 0)
            {
                //Add the new entry to mPasses table.
                var journalist = Helper.ExecuteService("Media", "GetJournalistsById", new Journalist { JournalistID = updatedRec.JournalistId }) as Journalist;
                var journalistTomPass = journalist.TomPassEntity(Convert.ToInt32(model.AssemblyCode), Convert.ToInt32(model.SessionCode), 2);

                if (journalist.IsAcredited == false)
                {
                    journalistTomPass.ApprovedPassCategoryID = 11;
                    journalistTomPass.RequestedPassCategoryID = 11;
                }
                string Url = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImageLocation(Convert.ToInt32(CurrentSession.AssemblyId), Convert.ToInt32(CurrentSession.SessionId));
                var ServerLocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting1", null);
                var mediaSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMediaFileSetting", null);
                DirectoryInfo Dir = new DirectoryInfo(Url);
                if (!Dir.Exists)
                {
                    Dir.Create();
                }
                if (journalistTomPass.Photo != null)
                {
                    string SourceFile = ServerLocation.SettingValue + mediaSettings.SettingValue + journalistTomPass.Photo;
                    string destFile = System.IO.Path.Combine(Url, journalistTomPass.Photo);
                    if (System.IO.File.Exists(destFile))
                    {
                        System.IO.File.Delete(destFile);
                    }
                    if (System.IO.File.Exists(SourceFile))
                    {
                        System.IO.File.Copy(SourceFile, destFile);
                    }
                }
                else
                {
                    journalistTomPass.Photo = "No Photo";
                }
                Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPass", journalistTomPass);
            }
            return singleJournalist.SessionCode;
        }

        public static void UpdateDepartmentPasses(mDepartmentPasses PassRequest)
        {
            PassRequest.IsApproved = true;
            PassRequest.ApprovedDate = DateTime.Now;
            PassRequest.ApprovedBy = CurrentSession.UserName;
            PassRequest.Status = (int)PassStatus.Approved;
            Helper.ExecuteService("Passes", "UpdatePassRequest", PassRequest);
        }

        public object ApproveSelectedMasterPass(string PassIDs, string PassModes, string allowedPassTypes)
        {
            string[] PassIds = PassIDs.Split(',');
            string[] PassModesNew = PassModes.Split(',');
            string[] allowedPassTypeList = allowedPassTypes.Split(',');
            //int status = (int)PassStatus.Approved;
            for (int i = 0; i < PassIds.Length; i++)
            {
                Int32 passIDInt = Convert.ToInt32(PassIds[i]);
                string mode = Regex.Replace(PassModesNew[i], @"[\d-]", string.Empty);
                if (passIDInt != 0)
                {
                    if (mode == EnumHelper.GetDescription(PassRequestTypes.PassFromDepartment))
                    {
                        var PassRequest = (mDepartmentPasses)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassByPassID", new mDepartmentPasses { PassID = passIDInt, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                        if (PassRequest != null)
                        {
                            var mPassFromDeptPass = PassRequest.TomPassEntity(2);
                            if (!string.IsNullOrEmpty(allowedPassTypeList[i]))
                            {
                                mPassFromDeptPass.ApprovedPassCategoryID = Convert.ToInt32(allowedPassTypeList[i]);
                                PassRequest.ApprovedPassCategoryID = Convert.ToInt32(allowedPassTypeList[i]);
                            }
                            var savedmPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPass", mPassFromDeptPass);
                            //Update the mDepartmentPasses table for IsApproved, ApprobedBy and ApprovedDate feilds.
                            PassRequest.PassCode = savedmPass.PassCode;
                            UpdateDepartmentPasses(PassRequest);
                            if (PassRequest.RequestdID != null)
                            {
                                //  Notification.SendEmailDetails(PassRequest);
                            }
                        }
                    }
                    else if (mode == EnumHelper.GetDescription(PassRequestTypes.PassFromReception))
                    {
                        var publicPassRequest = (PublicPass)Helper.ExecuteService("AdministrationBranch", "GetPublicPassesByPassID", new PublicPass { PassID = passIDInt, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                        if (publicPassRequest != null)
                        {
                            var mPassFromDeptPass = publicPassRequest.TomPassEntity(2);
                            if (!string.IsNullOrEmpty(allowedPassTypeList[i]))
                            {
                                mPassFromDeptPass.ApprovedPassCategoryID = Convert.ToInt32(allowedPassTypeList[i]);
                            }
                            var mPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPass", mPassFromDeptPass);
                            // string PassValue = string.Format("{0}{1}{2}", DateTime.Now.Year, String.Format("{0:00}", DateTime.Now.Month), mPass.PassCode);
                            //  string PassValue = mPass.PassCode.HasValue ? mPass.PassCode.ToString() : "0";
                            int? PassCode = Convert.ToInt32(mPassFromDeptPass.PassCode);
                            mPass.PassCode = PassCode;
                            //Update the mDepartmentPasses table for IsApproved, ApprobedBy and ApprovedDate feilds.
                            UpdatePublicPasses(publicPassRequest, PassCode);

                            //var masterPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = mPass.PassID });
                            //var masterPassModel = masterPass.ToModel();
                            //masterPass.PassCode = PassCode;
                            //Helper.ExecuteService("AdministrationBranch", "UpdateMasterPass", masterPass);
                        }
                    }
                }
            }
            string URL = "/AdministrationBranch/AdministrationBranch/Index?Status=" + 1 + "&PassCatID=" + 0 + "&DepartmentCatID=" + "";
            return Json(URL, JsonRequestBehavior.AllowGet);
        }

        public static void UpdatePublicPasses(PublicPass PassRequest, Int32? passCode)
        {
            PassRequest.IsApproved = true;
            PassRequest.ApprovedBy = CurrentSession.UserID;
            PassRequest.ApprovedDate = DateTime.Now;
            PassRequest.PassCode = passCode;
            PassRequest.Status = 2;
            var updatedRecord = (PublicPass)Helper.ExecuteService("PublicPass", "UpdatePublicPass", PassRequest);
        }

        #region Grid Pending Action Methods.
        [HttpGet]
        public ActionResult ApproveMasterPassPending(int passID, string mode, int allowedPassType)
        {
            int status = (int)PassStatus.Approved;
            string URL = "/AdministrationBranch/AdministrationBranch/Index?Status=" + (int)PassStatus.Pending + "&PassCatID=" + 0 + "&DepartmentCatID=" + "";

            mDepartmentPasses PassRequest = new mDepartmentPasses();
            try
            {
                if (passID != 0)
                {
                    if (mode == EnumHelper.GetDescription(PassRequestTypes.PassFromDepartment))
                    {
                        PassRequest = (mDepartmentPasses)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassByPassID", new mDepartmentPasses { PassID = passID, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                        if (PassRequest != null)
                        {
                            var mPassFromDeptPass = PassRequest.TomPassEntity(status);
                            if (allowedPassType != 0)
                            {
                                mPassFromDeptPass.ApprovedPassCategoryID = Convert.ToInt32(allowedPassType);
                                PassRequest.ApprovedPassCategoryID = Convert.ToInt32(allowedPassType);
                            }
                            Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPass", mPassFromDeptPass);

                            //Update the mDepartmentPasses table for IsApproved, ApprobedBy and ApprovedDate feilds.
                            PassRequest.PassCode = mPassFromDeptPass.PassCode;
                            UpdateDepartmentPasses(PassRequest);
                            if (PassRequest.RequestdID != null)
                            {
                                //  Notification.SendEmailDetails(PassRequest);
                            }
                        }
                    }
                    else if (mode == EnumHelper.GetDescription(PassRequestTypes.PassFromReception))
                    {
                        var publicPassRequest = (PublicPass)Helper.ExecuteService("AdministrationBranch", "GetPublicPassesByPassID", new PublicPass { PassID = passID, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                        if (publicPassRequest != null)
                        {
                            var mPassFromDeptPass = publicPassRequest.TomPassEntity(2);
                            if (allowedPassType != 0)
                            {
                                mPassFromDeptPass.ApprovedPassCategoryID = Convert.ToInt32(allowedPassType);
                            }
                            var mPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPass", mPassFromDeptPass);

                            //Update the mDepartmentPasses table for IsApproved, ApprobedBy and ApprovedDate feilds.
                            //string PassValue = string.Format("{0}{1}{2}", DateTime.Now.Year, String.Format("{0:00}", DateTime.Now.Month), mPass.PassCode);
                            string PassValue = mPass.PassCode.HasValue ? mPass.PassCode.ToString() : "0";
                            int? PassCode = Convert.ToInt32(mPassFromDeptPass.PassCode);
                            UpdatePublicPasses(publicPassRequest, PassCode);

                            //var masterPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = mPass.PassID });
                            //var masterPassModel = masterPass.ToModel();
                            //masterPass.PassCode = PassCode;
                            //Helper.ExecuteService("AdministrationBranch", "UpdateMasterPass", masterPass);
                        }
                        return Json(URL, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(URL, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var errMsg = ex.Message;
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult RejectMasterPassPending(int passID, string mode)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            mPassModel model = new mPassModel();
            model.PassID = passID;
            model.Mode = mode;

            return View("_RejectMasterPass", model);
        }

        [HttpPost, ValidateAntiForgeryToken] 
        public ActionResult RejectMasterPassPending(mPassModel model)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            int status = (int)PassStatus.Rejected;
            mDepartmentPasses PassRequest = new mDepartmentPasses();
            try
            {
                if (model.PassID != 0)
                {
                    if (model.Mode == EnumHelper.GetDescription(PassRequestTypes.PassFromDepartment))
                    {
                        PassRequest = (mDepartmentPasses)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassByPassID", new mDepartmentPasses { PassID = model.PassID, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                        if (PassRequest != null)
                        {
                            var mPassFromDeptPass = PassRequest.TomPassEntity(status);
                            mPassFromDeptPass.ApprovedBy = null;
                            mPassFromDeptPass.ApprovedDate = null;
                            mPassFromDeptPass.RejectionDate = DateTime.Now;
                            mPassFromDeptPass.RejectionReason = model.RejectionReason;

                            Helper.ExecuteService("AdministrationBranch", "CreateNewRejectedMasterPass", mPassFromDeptPass);

                            PassRequest.Status = status;
                            PassRequest.IsApproved = false;
                            PassRequest.ApprovedBy = null;
                            PassRequest.ApprovedDate = null;
                            PassRequest.RejectionDate = DateTime.Now;
                            PassRequest.RejectionReason = model.RejectionReason;

                            Helper.ExecuteService("Passes", "UpdatePassRequest", PassRequest);
                        }
                    }
                    else if (model.Mode == EnumHelper.GetDescription(PassRequestTypes.PassFromReception))
                    {
                        var publicPassRequest = (PublicPass)Helper.ExecuteService("AdministrationBranch", "GetPublicPassesByPassID", new PublicPass { PassID = model.PassID, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                        if (publicPassRequest != null)
                        {
                            var mPassFromDeptPass = PassRequest.TomPassEntity(status);
                            mPassFromDeptPass.ApprovedBy = null;
                            mPassFromDeptPass.ApprovedDate = null;
                            mPassFromDeptPass.RejectionDate = DateTime.Now;
                            mPassFromDeptPass.RejectionReason = model.RejectionReason;

                            Helper.ExecuteService("AdministrationBranch", "CreateNewRejectedMasterPass", mPassFromDeptPass);

                            publicPassRequest.RejectionReason = model.RejectionReason;
                            publicPassRequest.RejectionDate = DateTime.Now;
                            publicPassRequest.Status = status;
                            publicPassRequest.IsApproved = false;
                            var updatedRecord = Helper.ExecuteService("PublicPass", "UpdatePublicPass", publicPassRequest);
                        }
                    }
                }
                return Json("", JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Index", new { Status = (int)PassStatus.Pending, PassCatID = 0, DepartmentCatID = "" });
            }
            catch (Exception ex)
            {
                var errMsg = ex.Message;
                return Json("", JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Index", new { Status = (int)PassStatus.Pending, PassCatID = 0, DepartmentCatID = "" });
            }
        }

        public ActionResult FullPassDetailsPending(int Id, string mode)
        {
            mPassModel masterPassModel = new mPassModel();
            if (mode == EnumHelper.GetDescription(PassRequestTypes.PassFromDepartment))
            {
                var deptPassDetails = (mDepartmentPasses)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassByPassID", new mDepartmentPasses { PassID = Id, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                masterPassModel = deptPassDetails.TomPassModel();

            }
            else if (mode == EnumHelper.GetDescription(PassRequestTypes.PassFromReception))
            {
                var publicPassDetails = (PublicPass)Helper.ExecuteService("AdministrationBranch", "GetPublicPassesByPassID", new PublicPass { PassID = Id, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                masterPassModel = publicPassDetails.TomPassMode();
            }

            return View("_FullPassDetails", masterPassModel);
        }
        #endregion

        public ActionResult PrintSelectedMasterPass(string PassIDs)
        {
            List<mPassModel> PrintPreview = new List<mPassModel>();
            try
            {
                string[] PassIds = PassIDs.Split(',');
                List<mPasses> PrintPreviewModel = new List<mPasses>();
                int AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
                int SessionCode = Convert.ToInt32(CurrentSession.SessionId);
                foreach (var passID in PassIds)
                {
                    int IDForPass = Convert.ToInt32(passID);
                    var publicPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = IDForPass });
                    //var publicPassAsXML = (string)Helper.ExecuteService("AdministrationBranch", "GetMasterPassByPassID", IDForPass);
                    PrintPreviewModel.Add(publicPass);
                }
                PrintPreview.AddRange(PrintPreviewModel.ToModelListWithXML());

                return PartialView("_PrintPasses", PrintPreview.OrderBy(m => m.PassCode));
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw;
            }
        }

        public ActionResult GetPdfMasterPass(string PassIDs)
        {
            List<mPassModel> PrintPreview = new List<mPassModel>();
            try
            {
                string[] PassIds = PassIDs.Split(',');
                List<mPasses> PrintPreviewModel = new List<mPasses>();
                foreach (var passID in PassIds)
                {
                    int IDForPass = Convert.ToInt32(passID);
                    var publicPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = IDForPass });
                    PrintPreviewModel.Add(publicPass);
                }
                PrintPreview.AddRange(PrintPreviewModel.ToModelList());

                var list = PrintPreviewModel.ToModelList();
                byte[] bytes = null;
                //bytes = list.ToArray();
                return File(bytes, "application/pdf", string.Format("Pass_{0}.pdf", DateTime.Now));

                //return PartialView("_PrintPasses", PrintPreviewModel.ToModelList());
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw;
            }
        }

        [HttpGet]
        public ActionResult ChangeApprovedPassType(int Id)
        {
            var masterPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = Id });
            var masterPassModel = masterPass.ToModel();
            var sessionDates = Helper.ExecuteService("PublicPass", "GetSessionDatesEdit", new PublicPass { SessionCode = Convert.ToInt32(CurrentSession.SessionId), AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId) }) as List<mSessionDate>;
            masterPassModel.mSessionDateList = sessionDates;
            return View("_ChangeApprovedPassType", masterPassModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ChangeApprovedPassType(mPassModel model)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            var masterPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = model.PassID });
            var masterPassModel = masterPass.ToModel();
            masterPass.ApprovedPassCategoryID = model.ApprovedPassCategoryID;
            masterPass.SessionDateTo = model.SessionDateTo;
            Helper.ExecuteService("AdministrationBranch", "UpdateMasterPass", masterPass);
            return RedirectToAction("Index", new { Status = 2, PassCatID = 0, DepartmentCatID = "" });
        }


        public ActionResult GatePassReport(string id)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            AdministrationBranchDashboardModel model = new AdministrationBranchDashboardModel();

            mPasses mpass = new mPasses();
            var model1 = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetAdminAssemblyList", mpass);
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            if (id == "Session-Wise")
            {
                var SessionDateFrom = ExtensionMethods.GetSessionDatesFromList(model.AssemblyId, model.SessionId);
                var SessionDateTo = ExtensionMethods.GetSessionDatesFromList(model.AssemblyId, model.SessionId);
                if (SessionDateFrom != "" || SessionDateTo != "")
                {
                    DateTime SessionFrom = DateTime.Parse(SessionDateFrom).AddDays(-19);
                    string SessionDFrom = SessionFrom.ToString("dd/MM/yyyy");
                    model.CurrentDateFrom = SessionDFrom;
                    DateTime SessionTo = DateTime.Parse(SessionDateTo).AddDays(+15);
                    string SessionDTo = SessionTo.ToString("dd/MM/yyyy");
                    model.CurrentDateTo = SessionDTo;
                }
                var passCategoryList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
                model.ListPassCategory = passCategoryList;
            }
            model.AssemblyName = model.AssemblyName;
            model.SessionName = model.AssemblyName;
            model.mAssemblyList = model1.mAssemblyList;
            model.sessionList = model1.sessionList;
            if (id == "Session-Wise")
            {
                return PartialView("_GatePassReport", model);
            }
            else
            {
                return PartialView("GetSummaryReport", model);
            }
        }
        private List<DateTime> GetDateRange(DateTime StartingDate, DateTime EndingDate)
        {
            if (StartingDate > EndingDate)
            {
                return null;
            }
            List<DateTime> rv = new List<DateTime>();
            DateTime tmpDate = StartingDate;
            do
            {
                rv.Add(tmpDate);
                tmpDate = tmpDate.AddDays(1);
            } while (tmpDate <= EndingDate);
            return rv;
        }

        public ActionResult GatePassReportList(string SessionDateFrom = null, string SessionDateTo = null, string Status = null, string DepartmentID = null, string id = null, int PassCatID = 0, string ViewListDetails = null, int PassCategoryID = 0)
        {
            try
            {
                AdministrationBranchDashboardModel model = new AdministrationBranchDashboardModel();

                mPasses mpass = new mPasses();
                var model1 = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetAdminAssemblyList", mpass);
                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }
                SessionCode = model.SessionId;
                AssemblyCode = model.AssemblyId;

                mpass.AssemblyCode = AssemblyCode;
                mpass.SessionCode = SessionCode;
                List<mDepartmentPasses> DeptList = new List<mDepartmentPasses>();
                List<mPasses> FinalList = new List<mPasses>();
                var masterPassesFromDB = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetMasterPassesByStatus", new mPasses { Status = 2, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                // var masterPassesFromDBToModel = masterPassesFromDB.ToModelListForGrid();
                var DepartmentApproved = masterPassesFromDB.Where(m => m.RecommendationType == "Department").ToList();
                var MinisterApproved = masterPassesFromDB.Where(m => m.RecommendationType == "Minister").ToList();
                var OfficerPApproved = masterPassesFromDB.Where(m => m.RecommendationType == "Officer").ToList();
                var MemberPApproved = masterPassesFromDB.Where(m => m.RecommendationType == "Member").ToList();
                var JournalisApproved = masterPassesFromDB.Where(m => m.RecommendationType == "Media").ToList();
                if (SessionDateFrom != null)
                {
                    mpass.SessionDateF = DateTime.ParseExact(SessionDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    mpass.SessionDateT = DateTime.ParseExact(SessionDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    FinalList = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetPassesApprovedByDate", mpass);
                    model.CurrentDateFrom = SessionDateFrom;
                    model.CurrentDateTo = SessionDateTo;
                    if (PassCategoryID != 0)
                    {
                        FinalList = FinalList.Where(a => a.ApprovedPassCategoryID == PassCategoryID).ToList();
                    }

                }
                model.ReportWise = Status;

                if (Status == "1")
                {

                    FinalList = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetPassesDepartmentCount", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                    FinalList.RemoveAll(m => m.OrganizationName == null);

                    model.ReportInformation = "Department/Minister";

                }
                if (Status == "2")
                {
                    FinalList = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetPassesCategoryCount", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                    model.ReportInformation = "Pass Type";
                }
                if (Status == "3")
                {

                    model.ReportInformation = "Department,Minister,Pass Type,Public,Journalist";
                    model.TotalDpartPanding = (int)Session["pendingPassRequestDept"];
                    model.TotalDpartApproval = DepartmentApproved.Count + MinisterApproved.Count;
                    model.TotalJouApproval = JournalisApproved.Count;//(int)Session["journalistInSessionCount"];
                    model.TotalJouPanding = (int)Session["PendingJournalists"];
                    model.TotalPublicPanding = (int)Session["pendingPassRequestRec"];
                    model.TotalPublicApproval = OfficerPApproved.Count + MemberPApproved.Count;
                }
                if (DepartmentID != null)
                {

                    mpass.DepartmentID = DepartmentID;
                    FinalList = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetPassesApprovedByDate", mpass);
                    var OrganisationName = FinalList.Where(m => m.DepartmentID == DepartmentID).Select(j => j.OrganizationName).Take(1);

                    foreach (var item in OrganisationName)
                    {
                        model.ReportInformation = item;
                    }
                    model.ReportWise = "1";
                    model.ActionFirstValue = "Department List";

                }
                if (PassCatID != 0)
                {
                    mpass.ApprovedPassCategoryID = PassCatID;
                    FinalList = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetPassesApprovedByDate", mpass);
                    var passCatName = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetPassesCategoryCount", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                    var PassTypeName = passCatName.Where(m => m.ApprovedPassCategoryID == PassCatID).Select(j => j.PassTypeName).Take(1);

                    foreach (var item in PassTypeName)
                    {
                        model.ReportInformation = item;
                    }
                    model.ReportWise = "2";
                    model.ActionFirstValue = "Pass Type List";
                }
                if (ViewListDetails == "DeptMinApproved")
                {
                    FinalList = DepartmentApproved.Concat(MinisterApproved).ToList();
                    model.ReportWise = "3";
                    model.ActionFirstValue = "Status List";
                }
                if (ViewListDetails == "DeptMinPanding")
                {
                    DeptList = (List<mDepartmentPasses>)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassRequests", new mPasses { AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                    model.ReportWise = "3";
                    model.ActionFirstValue = "Status List";
                }
                if (ViewListDetails == "PublicApproved")
                {
                    FinalList = OfficerPApproved.Concat(MemberPApproved).ToList();
                    model.ReportWise = "3";
                    model.ActionFirstValue = "Status List";
                }
                if (ViewListDetails == "PublicPanding")
                {
                }
                if (ViewListDetails == "JournalistApproved")
                {
                    FinalList = JournalisApproved;
                    model.ReportWise = "3";
                    model.ActionFirstValue = "Status List";
                }
                if (ViewListDetails == "JournalistPanding")
                {
                }
                if (ViewListDetails == "DeptMinPanding")
                {
                    model.mPassModelList = DeptList.ToModelList();
                }
                else
                {
                    model.mPassModelList = FinalList.ToModelList();
                }

                model.AssemblyName = model.AssemblyName;
                model.SessionName = model.AssemblyName;
                model.mAssemblyList = model1.mAssemblyList;
                model.sessionList = model1.sessionList;
                if (Status == "1" || Status == "2")
                {
                    return PartialView("_GetDeptPassSummaryList", model);

                }
                else if (Status == "3")
                {
                    return PartialView("_GetStatusReportList", model);
                }
                else if (DepartmentID != null || PassCatID != 0 || ViewListDetails != null)
                {
                    if (ViewListDetails == "DeptMinPanding")
                    {
                        return PartialView("_PassStatusPanding", model);
                    }
                    else
                    {
                        return PartialView("_GetSummaryPassReportList", model);
                    }

                }
                else if (id == "Summary-Wise")
                {
                    model.ActionFirstValue = "WithoutDataGrid";
                    return PartialView("_GetDeptPassSummaryList", model);
                }
                else
                {
                    return PartialView("_GatePassReportList", model);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public void GenericSessionListFunction(AdministrationBranchDashboardModel model)
        {
            SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();
            session.AssemblyID = model.AssemblyId;
            session.SessionCode = model.SessionId;
            List<mSessionDate> ListSessionDates = new List<mSessionDate>();
            ListSessionDates = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", session);

            List<mSessionDateModel> ListSession = new List<mSessionDateModel>();
            List<mSessionDateModel> ListSessionFrom = new List<mSessionDateModel>();
            for (int i = 0; i < ListSessionDates.Count; i++)
            {
                mSessionDateModel Session1 = new mSessionDateModel();
                Session1.SessionDate = ListSessionDates[i].SessionDate.ToString("dd/MM/yyyy");
                ListSession.Add(Session1);
            }

            var SessionDateFrom1 = SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Extensions.ExtensionMethods.GetSessionDatesFromList(model.AssemblyId, model.SessionId);
            List<DateTime> lstDates = new List<DateTime>();
            DateTime StartingDate = DateTime.Parse(SessionDateFrom1).AddDays(-16);
            DateTime EndingDate = DateTime.Parse(SessionDateFrom1).AddDays(-1);
            foreach (DateTime date in GetDateRange(StartingDate, EndingDate))
            {
                lstDates.Add(date);
            }
            for (int i = 0; i < lstDates.Count; i++)
            {
                mSessionDateModel Session2 = new mSessionDateModel();
                Session2.SessionDate = lstDates[i].ToString("dd/MM/yyyy");
                ListSessionFrom.Add(Session2);
            }

            model.mSessionDateListFrom = ListSessionFrom.Concat(ListSession).ToList();
            model.mSessionDateList = ListSession;
        }
        public JsonResult GetSessionByAssemblyId(int AssemblyId)
        {
            mSession mdl = new mSession();
            List<mSession> SessLst = new List<mSession>();
            if (AssemblyId != 0)
            {
                mdl.AssemblyID = AssemblyId;
                SessLst = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", mdl);
            }
            return Json(SessLst, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ChangeStatusSelectedMasterPass(string PassIDs)
        {
            try
            {

                string[] PassIds = PassIDs.Split(',');

                for (int i = 0; i < PassIds.Length; i++)
                {
                    Int32 passIDInt = Convert.ToInt32(PassIds[i]);
                    var publicPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = passIDInt });
                    string RecomType = publicPass.RecommendationType;
                    string Name = publicPass.Name;
                    int PassCat = publicPass.RequestedPassCategoryID;
                    int? PassCode = publicPass.PassCode;
                    string RequestID = publicPass.RequestID;
                    if (RecomType == "Minister" || RecomType == "Department")
                    {
                        var PassRequest = (mDepartmentPasses)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassDetails", new mDepartmentPasses { RecommendationType = RecomType, Name = Name, PassCode = PassCode, RequestdID = RequestID, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
                        if (PassRequest != null)
                        {
                            PassRequest.IsApproved = false;
                            PassRequest.ApprovedDate = null;
                            PassRequest.ApprovedBy = null;
                            PassRequest.Status = 1;
                            PassRequest.PassCode = null;
                            Helper.ExecuteService("Passes", "UpdatePassRequest", PassRequest);
                        }

                    }
                    else if (RecomType == "Media")
                    {

                    }
                    else if (PassCat == 9 || PassCat == 20 || PassCat == 5 || PassCat == 10 || PassCat == 8)
                    {

                    }
                    Helper.ExecuteService("AdministrationBranch", "DeleteMasterPassByID", new mPasses { PassID = passIDInt });
                }

                return Json("Sucess", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                var errMsg = ee.Message;
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        #region Approved Pass In Single
        public ActionResult ApproveMasterPassPrint(int passID, string mode, int allowedPassType)
        {
            int status = (int)PassStatus.Approved;
            string URL = "/AdministrationBranch/AdministrationBranch/Index?Status=" + (int)PassStatus.Pending + "&PassCatID=" + 0 + "&DepartmentCatID=" + "";
            List<mPasses> PrintPreviewModelList = new List<mPasses>();
            mDepartmentPasses PassRequest = new mDepartmentPasses();
            try
            {

                if (passID != 0)
                {
                    if (mode == EnumHelper.GetDescription(PassRequestTypes.PassFromDepartment))
                    {
                        PassRequest = (mDepartmentPasses)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassByPassID", new mDepartmentPasses { PassID = passID, AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId), SessionCode = Convert.ToInt32(CurrentSession.SessionId) });
                        if (PassRequest != null)
                        {
                            var mPassFromDeptPass = PassRequest.TomPassEntity(status);
                            if (allowedPassType != 0)
                            {
                                mPassFromDeptPass.ApprovedPassCategoryID = Convert.ToInt32(allowedPassType);
                                PassRequest.ApprovedPassCategoryID = Convert.ToInt32(allowedPassType);
                            }
                            Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPass", mPassFromDeptPass);

                            //Update the mDepartmentPasses table for IsApproved, ApprobedBy and ApprovedDate feilds.
                            PassRequest.PassCode = mPassFromDeptPass.PassCode;
                            UpdateDepartmentPasses(PassRequest);
                            if (PassRequest.RequestdID != null)
                            {
                                //  Notification.SendEmailDetails(PassRequest);
                            }
                            PrintPreviewModelList.Add(mPassFromDeptPass);
                            // PrintPreviewModel = mPassFromDeptPass;
                        }
                    }
                    else if (mode == EnumHelper.GetDescription(PassRequestTypes.PassFromReception))
                    {
                        var publicPassRequest = (PublicPass)Helper.ExecuteService("AdministrationBranch", "GetPublicPassesByPassID", new PublicPass { PassID = passID, AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId), SessionCode = Convert.ToInt32(CurrentSession.SessionId) });
                        if (publicPassRequest != null)
                        {
                            var mPassFromDeptPass = publicPassRequest.TomPassEntity(2);
                            if (allowedPassType != 0)
                            {
                                mPassFromDeptPass.ApprovedPassCategoryID = Convert.ToInt32(allowedPassType);
                            }
                            var mPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPass", mPassFromDeptPass);

                            string PassValue = mPass.PassCode.HasValue ? mPass.PassCode.ToString() : "0";
                            int? PassCode = Convert.ToInt32(mPassFromDeptPass.PassCode);
                            UpdatePublicPasses(publicPassRequest, PassCode);
                            PrintPreviewModelList.Add(mPassFromDeptPass);
                        }
                        return Json(URL, JsonRequestBehavior.AllowGet);
                    }
                    #region Print Operation
                    List<mPassModel> PrintPreview = new List<mPassModel>();

                    int AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
                    int SessionCode = Convert.ToInt32(CurrentSession.SessionId);

                    PrintPreview = PrintPreviewModelList.ToModelListWithXML();

                    return PartialView("_PrintSinglePasses", PrintPreview);
                    #endregion
                    // return RedirectToAction("PrintSelectedMasterPass", new { PassIDs = Convert.ToString(passID) });
                }
                return Json(URL, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var errMsg = ex.Message;
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        public object ApproveSelectedMasterPassPrint(string PassIDs, string PassModes, string allowedPassTypes)
        {
            string[] PassIds = PassIDs.Split(',');
            string[] PassModesNew = PassModes.Split(',');
            string[] allowedPassTypeList = allowedPassTypes.Split(',');
            //int status = (int)PassStatus.Approved;
            List<mPasses> PrintPreviewModelList = new List<mPasses>();
            for (int i = 0; i < PassIds.Length; i++)
            {
                Int32 passIDInt = Convert.ToInt32(PassIds[i]);
                string mode = Regex.Replace(PassModesNew[i], @"[\d-]", string.Empty);
                if (passIDInt != 0)
                {
                    if (mode == EnumHelper.GetDescription(PassRequestTypes.PassFromDepartment))
                    {
                        var PassRequest = (mDepartmentPasses)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassByPassID", new mDepartmentPasses { PassID = passIDInt, AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId), SessionCode = Convert.ToInt32(CurrentSession.SessionId) });
                        if (PassRequest != null)
                        {
                            var mPassFromDeptPass = PassRequest.TomPassEntity(2);
                            if (!string.IsNullOrEmpty(allowedPassTypeList[i]))
                            {
                                mPassFromDeptPass.ApprovedPassCategoryID = Convert.ToInt32(allowedPassTypeList[i]);
                                PassRequest.ApprovedPassCategoryID = Convert.ToInt32(allowedPassTypeList[i]);
                            }
                            var savedmPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPass", mPassFromDeptPass);
                            //Update the mDepartmentPasses table for IsApproved, ApprobedBy and ApprovedDate feilds.
                            PassRequest.PassCode = savedmPass.PassCode;
                            UpdateDepartmentPasses(PassRequest);
                            if (PassRequest.RequestdID != null)
                            {
                                //  Notification.SendEmailDetails(PassRequest);
                            }
                            PrintPreviewModelList.Add(mPassFromDeptPass);
                        }
                    }
                    else if (mode == EnumHelper.GetDescription(PassRequestTypes.PassFromReception))
                    {
                        var publicPassRequest = (PublicPass)Helper.ExecuteService("AdministrationBranch", "GetPublicPassesByPassID", new PublicPass { PassID = passIDInt, AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId), SessionCode = Convert.ToInt32(CurrentSession.SessionId) });
                        if (publicPassRequest != null)
                        {
                            var mPassFromDeptPass = publicPassRequest.TomPassEntity(2);
                            if (!string.IsNullOrEmpty(allowedPassTypeList[i]))
                            {
                                mPassFromDeptPass.ApprovedPassCategoryID = Convert.ToInt32(allowedPassTypeList[i]);
                            }
                            var mPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "CreateNewMasterPass", mPassFromDeptPass);

                            int? PassCode = Convert.ToInt32(mPassFromDeptPass.PassCode);
                            mPass.PassCode = PassCode;

                            UpdatePublicPasses(publicPassRequest, PassCode);

                            PrintPreviewModelList.Add(mPassFromDeptPass);
                        }
                    }

                }
            }
            #region Print Operation
            List<mPassModel> PrintPreview = new List<mPassModel>();

            int AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
            int SessionCode = Convert.ToInt32(CurrentSession.SessionId);

            PrintPreview = PrintPreviewModelList.ToModelListWithXML();

            return PartialView("_PrintSinglePasses", PrintPreview.OrderBy(m => m.PassCode));
            #endregion
            //  string URL = "/AdministrationBranch/AdministrationBranch/Index?Status=" + 1 + "&PassCatID=" + 0 + "&DepartmentCatID=" + "";
            // return Json(URL, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public ActionResult GetImagePriview(string PassID)
        {
            var masterPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = Convert.ToInt32(PassID) });
            var photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(masterPass.AssemblyCode, masterPass.SessionCode, masterPass.Photo);
            return Json(photo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPendingImagePriview(string PassID)
        {
            var PassRequest = (mDepartmentPasses)Helper.ExecuteService("AdministrationBranch", "GetDepartmentPassByPassID", new mDepartmentPasses { PassID = Convert.ToInt32(PassID), AssemblyCode = AssemblyCode, SessionCode = SessionCode });
            var photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(PassRequest.AssemblyCode, PassRequest.SessionCode, PassRequest.Photo);
            return Json(photo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetMediaImagePriview(string JournalistID)
        {
            var journalist = Helper.ExecuteService("Media", "GetJournalistsById", new Journalist { JournalistID = Convert.ToInt32(JournalistID) }) as Journalist;

            var mediaSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMediaFileSetting", null);
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSettingLocation", null);
            var photo = FileSettings.SettingValue + mediaSettings.SettingValue + journalist.ThumbName; //SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(PassRequest.AssemblyCode, PassRequest.SessionCode, PassRequest.Photo);
            return Json(photo, JsonRequestBehavior.AllowGet);
        }
    }
}
