﻿namespace SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Extensions
{
    #region Namespace reffrences.
    using SBL.DomainModel.Models.Enums;
    using SBL.DomainModel.Models.Media;
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.Session;
    using SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Models;
    using SBL.eLegistrator.HouseController.Web.Helpers;
    using SBL.eLegistrator.HouseController.Web.Utility;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using System.Xml.Linq;
    #endregion

    public static class ExtensionMethods
    {
        static List<PassCategory> ListPassCategoryList = new List<PassCategory>();
        #region List Extension for the Models
        public static List<mPassModel> ToModelList(this List<mPasses> entityList)
        {
            var ModelList = entityList.Select(entity => new mPassModel()
            {
                // AadharID = entity.AadharID,
                Address = entity.Address,
                //  Age = entity.Age,
                // ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                //AssemblyCode = entity.AssemblyCode,
                //  DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                // Email = entity.Email,
                FatherName = entity.FatherName,
                // GateNumber = entity.GateNumber,
                //  Gender = entity.Gender,
                //  IsActive = entity.IsActive,
                // IsRequested = entity.IsRequested,
                //MobileNo = entity.MobileNo,
                Name = entity.Name,
                //NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                //RequestedPassCategoryID = entity.RequestedPassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                // Photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                //Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                //RequestedBy = entity.RequestedBy,
                //RequestedDate = entity.RequestedDate,
                //SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                //Status = entity.Status,
                //Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                ApprovedPassCategoryID = entity.ApprovedPassCategoryID,
                PassCategoryName = GetPassCategoryNameByID(entity.ApprovedPassCategoryID),
                PassCategoryTemplate = GetPassCategoryTemplateByID(entity.ApprovedPassCategoryID),
                //RequestID = entity.RequestID,
                TotalCount = entity.TotalCount,
                TotalDeptCount = entity.TotalOfTotalcount

            });
            return ModelList.ToList();
        }

        public static List<mPassModel> ToModelListForGrid(this List<mPasses> entityList)
        {
            var ModelList = entityList.Select(entity => new mPassModel()
            {
                //AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                //AssemblyCode = entity.AssemblyCode,
                // DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                // Email = entity.Email,
                FatherName = entity.FatherName,
                //GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                //IsActive = entity.IsActive,
                //IsRequested = entity.IsRequested,
                //MobileNo = entity.MobileNo,
                Name = entity.Name,
                //NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                //RequestedPassCategoryID = entity.RequestedPassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                //Photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                //Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                //RecommendationType = entity.RecommendationType,
                //RequestedBy = entity.RequestedBy,
                //RequestedDate = entity.RequestedDate,
                //SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                //Status = entity.Status,
                //Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                //RejectionReason = entity.RejectionReason,
                //RejectionDate = entity.RejectionDate,
                PassCategoryName = GetPassCategoryNameByID(entity.ApprovedPassCategoryID),
                ApprovedPassCategoryID = entity.ApprovedPassCategoryID,
                RequestID = entity.RequestID
            });
            return ModelList.ToList();
        }

        public static List<mPassModel> ToModelListWithXML(this List<mPasses> entityList)
        {
            var ModelList = entityList.Select(entity => new mPassModel()
            {
                AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                Email = entity.Email,
                FatherName = entity.FatherName,
                GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,
                MobileNo = entity.MobileNo,
                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                RequestedPassCategoryID = entity.RequestedPassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                Photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                Status = entity.Status,
                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                ApprovedPassCategoryID = entity.ApprovedPassCategoryID,
                PassCategoryName = GetPassCategoryNameByID(entity.ApprovedPassCategoryID),
                PassCategoryTemplate = GetPassCategoryTemplateByID(entity.ApprovedPassCategoryID),
                xmlDocument = XMLForQRCode(entity)
            });
            return ModelList.ToList();
        }

        public static List<mPassModel> ToModelList(this List<PublicPass> entityList)
        {
            var ModelList = entityList.Select(entity => new mPassModel()
            {
                //AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                //ApprovedBy = entity.ApprovedBy,
                //ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                //DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                //Email = entity.Email,
                FatherName = entity.FatherName,
                //GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                //IsActive = entity.IsActive,
                //IsRequested = entity.IsRequested,
                //MobileNo = entity.MobileNo,
                Name = entity.Name,
                //NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                //RequestedPassCategoryID = entity.PassCategoryID,
                //PassCode = entity.PassCode,
                PassID = entity.PassID,
                //Photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                //Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                //SessionDateTo = entity.SessionDateTo,
                //SessionDateFrom = entity.SessionDateFrom,
                //Status = entity.Status,
                //Time = entity.Time,
                //VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                Mode = EnumHelper.GetDescription(PassRequestTypes.PassFromReception)
            });
            return ModelList.ToList();
        }

        public static List<mPassModel> ToModelList(this List<mDepartmentPasses> entityList)
        {
            var ModelList = entityList.Select(entity => new mPassModel()
            {
                PassID = entity.PassID,
                //AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                //ApprovedBy = entity.ApprovedBy,
                //ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                //DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                //Email = entity.Email,
                FatherName = entity.FatherName,
                //GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                //IsActive = entity.IsActive,
                //IsRequested = entity.IsRequested,
                //MobileNo = entity.MobileNo,
                Name = entity.Name,
                //NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                RequestedPassCategoryID = entity.PassCategoryID,
                //PassCode = entity.PassCode,
                //Photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                //Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                //RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                //SessionDateTo = entity.SessionDateTo,
                //SessionDateFrom = entity.SessionDateFrom,
                //Status = entity.Status,
                //Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                Mode = EnumHelper.GetDescription(PassRequestTypes.PassFromDepartment),
                ListPassCategory = GetListPassCategoryList(),
                RequestID = entity.RequestdID
            });
            return ModelList.ToList();
        }

        #endregion

        #region ModelToEntity and EntityToModel Methods.
        public static mPassModel ToModel(this mPasses entity)
        {
            var Model = new mPassModel()
            {
                AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                Email = entity.Email,
                FatherName = entity.FatherName,
                GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,
                MobileNo = entity.MobileNo,
                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                RequestedPassCategoryID = entity.RequestedPassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                Photo = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(entity.AssemblyCode, entity.SessionCode, entity.Photo),
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                Status = entity.Status,
                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                RequestedPassCategoryName = GetPassCategoryNameByID(entity.RequestedPassCategoryID),
                AllowedPassCategoryName = GetPassCategoryNameByID(entity.ApprovedPassCategoryID),
                ListPassCategory = GetListPassCategoryList(),
                RecommendationByName = GetMemberNameByCode(entity.RecommendationBy)
            };
            return Model;
        }

        #region To mPasses Entity From mDepartmentPasses, Journalist Passses etc.
        public static mPasses TomPassEntity(this Journalist model, Int32 assemblyCode, Int32 sessionCode, Int32 status)
        {
            //var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            var mPassEntity = new mPasses()
            {
                Name = model.Name,
                Email = model.Email,
                IsActive = model.IsActive,
                MobileNo = model.Phone,
                AadharID = model.UID,
                OrganizationName = GetOrganizationNameByID(model.OrganizationID),
                Photo = model.ThumbName,
                Gender = model.Gender,
                Age = model.Age,
                Designation = model.Designation,
                FatherName = model.FatherName,
                Address = model.Address,
                Status = status,
                ApprovedBy = CurrentSession.UserName,
                ApprovedDate = DateTime.Now,
                AssemblyCode = assemblyCode,
                SessionCode = sessionCode,
                SessionDateFrom = GetSessionDateFrom(assemblyCode, sessionCode),
                SessionDateTo = GetSessionDateTo(assemblyCode, sessionCode),
                IsRequested = true,
                NoOfPersions = 1,
                RequestedPassCategoryID = 6,
                ApprovedPassCategoryID = 6,
                PassCode = GeneratePassCode(status, assemblyCode, sessionCode),
                PassID = 1,
                Prefix = "",
                RecommendationBy = "Media",
                RecommendationDescription = "Media Person",
                RecommendationType = "Media",
                RequestedBy = "",
                IsApproved = true
            };
            return mPassEntity;
        }

        public static mPasses TomPassEntity(this mDepartmentPasses entity, int status)
        {
            var mPassEntity = new mPasses()
            {
                PassID = entity.PassID,
                Name = entity.Name,
                Email = entity.Email,
                IsActive = entity.IsActive,
                MobileNo = entity.MobileNo,
                AadharID = entity.AadharID,
                OrganizationName = entity.OrganizationName,
                Photo = entity.Photo,
                Gender = entity.Gender,
                Age = entity.Age,
                Designation = entity.Designation,
                FatherName = entity.FatherName,
                Address = entity.Address,
                Status = status,
                ApprovedBy = CurrentSession.UserName,
                ApprovedDate = DateTime.Now,
                AssemblyCode = entity.AssemblyCode,
                SessionCode = entity.SessionCode,
                SessionDateFrom = entity.SessionDateFrom,
                SessionDateTo = entity.SessionDateTo,
                // SessionDateFrom = GetSessionDateFrom(entity.AssemblyCode, entity.SessionCode),
                //SessionDateTo = GetSessionDateTo(entity.AssemblyCode, entity.SessionCode),
                IsRequested = entity.IsRequested,
                NoOfPersions = 1,
                RequestedPassCategoryID = entity.PassCategoryID,
                PassCode = GeneratePassCode(status, entity.AssemblyCode, entity.SessionCode),
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                IsApproved = true,
                VehicleNumber = entity.VehicleNumber,
                DepartmentID = entity.DepartmentID,
                GateNumber = entity.GateNumber,
                RequestID = entity.RequestdID
            };
            return mPassEntity;
        }

        public static mPasses TomPassEntity(this PublicPass entity, int status)
        {
            var mPassEntity = new mPasses()
            {
                Name = entity.Name,
                Email = entity.Email,
                IsActive = entity.IsActive,
                MobileNo = entity.MobileNo,
                AadharID = entity.AadharID,
                OrganizationName = entity.OrganizationName,
                Photo = entity.Photo,
                Gender = entity.Gender,
                Age = entity.Age,
                Designation = entity.Designation,
                FatherName = entity.FatherName,
                Address = entity.Address,
                Status = status,
                ApprovedBy = CurrentSession.UserName,
                ApprovedDate = DateTime.Now,
                AssemblyCode = entity.AssemblyCode,
                SessionCode = entity.SessionCode,
                SessionDateFrom = entity.SessionDateFrom,
                SessionDateTo = entity.SessionDateTo,
                IsRequested = entity.IsRequested,
                NoOfPersions = entity.NoOfPersions,
                RequestedPassCategoryID = entity.PassCategoryID,
                PassCode = GeneratePassCode(status, entity.AssemblyCode, entity.SessionCode),
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                IsApproved = true
            };
            return mPassEntity;
        }
        #endregion

        #region To mPassModel

        public static mPassModel TomPassModel(this mDepartmentPasses entity)
        {
            var masterPassModel = new mPassModel()
            {
                PassID = entity.PassID,
                AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                Email = entity.Email,
                FatherName = entity.FatherName,
                GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,
                MobileNo = entity.MobileNo,
                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                RequestedPassCategoryID = entity.PassCategoryID,
                PassCode = entity.PassCode,
                Photo = entity.Photo,
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                Status = entity.Status,
                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                Mode = EnumHelper.GetDescription(PassRequestTypes.PassFromDepartment),
                ListPassCategory = GetListPassCategoryList(),
                RequestedPassCategoryName = GetPassCategoryNameByID(entity.PassCategoryID)
            };
            return masterPassModel;
        }

        public static mPassModel TomPassMode(this PublicPass entity)
        {
            var masterPassModel = new mPassModel()
            {
                AadharID = entity.AadharID,
                Address = entity.Address,
                Age = entity.Age,
                ApprovedBy = entity.ApprovedBy,
                ApprovedDate = entity.ApprovedDate,
                AssemblyCode = entity.AssemblyCode,
                DayTime = entity.DayTime,
                DepartmentID = entity.DepartmentID,
                Designation = entity.Designation,
                Email = entity.Email,
                FatherName = entity.FatherName,
                GateNumber = entity.GateNumber,
                Gender = entity.Gender,
                IsActive = entity.IsActive,
                IsRequested = entity.IsRequested,
                MobileNo = entity.MobileNo,
                Name = entity.Name,
                NoOfPersions = entity.NoOfPersions,
                OrganizationName = entity.OrganizationName,
                RequestedPassCategoryID = entity.PassCategoryID,
                PassCode = entity.PassCode,
                PassID = entity.PassID,
                Photo = entity.Photo,
                Prefix = entity.Prefix,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                RecommendationType = entity.RecommendationType,
                RequestedBy = entity.RequestedBy,
                RequestedDate = entity.RequestedDate,
                SessionCode = entity.SessionCode,
                SessionDateTo = entity.SessionDateTo,
                SessionDateFrom = entity.SessionDateFrom,
                Status = entity.Status,
                Time = entity.Time,
                VehicleNumber = entity.VehicleNumber,
                RejectionReason = entity.RejectionReason,
                RejectionDate = entity.RejectionDate,
                Mode = EnumHelper.GetDescription(PassRequestTypes.PassFromReception),
                RequestedPassCategoryName = GetPassCategoryNameByID(entity.PassCategoryID)
            };
            return masterPassModel;
        }

        #endregion
        #endregion

        #region Helper Methods
        public static string GetOrganizationNameByID(int organizationID)
        {
            var org = Helper.ExecuteService("Media", "GetOrganizationById", new Organization { OrganizationID = organizationID }) as Organization;
            if (org != null)
            {
                return org.Name;
            }
            return "";
        }

        public static Int32 GeneratePassCode(int status, int AssemblyCode, int SessionCode)
        {
            var maxPassCode = (Int32)Helper.ExecuteService("AdministrationBranch", "GetMaxPassCodeForSession", new mPasses { Status = status, AssemblyCode = AssemblyCode, SessionCode = SessionCode });
            if (maxPassCode != 0)
            {
                return maxPassCode;
            }
            else
                return 1;

        }

        public static string GetSessionDateFrom(int AssemblyCode, int SessionCode)
        {
            var sessionDates = Helper.ExecuteService("PublicPass", "GetSessionDatesList", new PublicPass { SessionCode = SessionCode, AssemblyCode = AssemblyCode }) as List<mSessionDate>;
            var sessionDate = sessionDates.OrderBy(m => m.SessionDate).Take(1).FirstOrDefault() as mSessionDate;
            if (sessionDate != null)
            {
                return sessionDate.DisplayDate.ToString();
            }
            return "";
        }
        public static string GetSessionDatesFromList(int AssemblyCode, int SessionCode)
        {
            var sessionDates = Helper.ExecuteService("PublicPass", "GetSessionDatesFromList", new PublicPass { SessionCode = SessionCode, AssemblyCode = AssemblyCode }) as List<mSessionDate>;
            var sessionDate = sessionDates.OrderBy(m => m.SessionDate).Take(1).FirstOrDefault() as mSessionDate;
            if (sessionDate != null)
            {
                return sessionDate.DisplayDate.ToString();
            }
            return "";
        }

        public static string GetSessionDateTo(int AssemblyCode, int SessionCode)
        {
            var sessionDates = Helper.ExecuteService("PublicPass", "GetSessionDatesList", new PublicPass { SessionCode = SessionCode, AssemblyCode = AssemblyCode }) as List<mSessionDate>;
            var sessionDate = sessionDates.OrderByDescending(m => m.SessionDate).Take(1).FirstOrDefault() as mSessionDate;
            if (sessionDate != null)
            {
                return sessionDate.DisplayDate.ToString();
            }
            return "";
        }

        public static string GetPassCategoryNameByID(int passCategoryID)
        {
            if (ListPassCategoryList.Count == 0)
            {
                ListPassCategoryList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
            }

            var passCategoryModel = (PassCategory)ListPassCategoryList.Where(m => m.PassCategoryID == passCategoryID).FirstOrDefault();

            if (passCategoryModel != null)
            {
                return passCategoryModel.Name;
            }
            return "";
        }

        public static string GetPassCategoryTemplateByID(int passCategoryID)
        {
            if (ListPassCategoryList.Count == 0)
            {
                ListPassCategoryList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
            }

            var passCategoryModel = (PassCategory)ListPassCategoryList.Where(m => m.PassCategoryID == passCategoryID).FirstOrDefault();

            if (passCategoryModel != null)
            {
                return passCategoryModel.Template;
            }
            return "";
        }

        public static List<PassCategory> GetListPassCategoryList()
        {
            if (ListPassCategoryList.Count == 0)
            {
                ListPassCategoryList = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
                return ListPassCategoryList;
            }
            else
            {
                return ListPassCategoryList;
            }
        }

        public static string GetMemberNameByCode(string memberCode)
        {
            return (string)Helper.ExecuteService("PublicPass", "GetMemberNameByCode", new PublicPass { RecommendationBy = memberCode });
        }

        public static string XMLForQRCode(mPasses model)
        {
            //XElement passParentNode = new System.Xml.Linq.XElement("PassDetails");

            XElement passParentNode = new System.Xml.Linq.XElement("PassCode");

            XElement passChildChildNode1 = new System.Xml.Linq.XElement("Name");
            passParentNode.Add(passChildChildNode1);

            passParentNode.SetAttributeValue("ID", model.PassCode);

            string information = model.Name + (model.Gender + "," + Convert.ToString(model.Age) + " Years");

            passChildChildNode1.ReplaceNodes(model.Name != null ? information : "");

            return passParentNode.ToString();
        }
        #endregion

    }
}