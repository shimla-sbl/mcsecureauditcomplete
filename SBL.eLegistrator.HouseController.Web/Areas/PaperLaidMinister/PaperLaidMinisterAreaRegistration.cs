﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister
{
    public class PaperLaidMinisterAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PaperLaidMinister";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PaperLaidMinister_default",
                "PaperLaidMinister/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
