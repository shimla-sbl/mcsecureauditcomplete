﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister.Models
{
    public class StaticControlBinder
    {
        public static SelectList GetPagesToApplySignature()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "All Pages", Value = "1" });
            result.Add(new SelectListItem() { Text = "Alternate Pages", Value = "2" });
            result.Add(new SelectListItem() { Text = "First Page", Value = "3" });
            result.Add(new SelectListItem() { Text = "Last Page", Value = "4" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetPositionToApplySignature()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Top Left", Value = "1" });
            result.Add(new SelectListItem() { Text = "Top Right", Value = "2" });
            result.Add(new SelectListItem() { Text = "Bottom Left", Value = "3" });
            result.Add(new SelectListItem() { Text = "Bottom Right", Value = "4" });

            return new SelectList(result, "Value", "Text");
        }
    }
}