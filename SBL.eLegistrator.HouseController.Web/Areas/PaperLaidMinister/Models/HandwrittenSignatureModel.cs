﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister.Models
{
    [Serializable]
    public class HandwrittenSignatureModel
    {
        public SelectList PagesToApplySignatureList { get; set; }

        public SelectList PositionToApplySignatureList { get; set; }

        public int PagesToApplySignatureID { get; set; }

        public int PositionToApplySignatureID { get; set; }
        public int HindiRadioSignatureID { get; set; }
        public int EnglishRadioSignatureID { get; set; }

        public string lSMCode { get; set; }

        public string SIds { get; set; }
    }
}