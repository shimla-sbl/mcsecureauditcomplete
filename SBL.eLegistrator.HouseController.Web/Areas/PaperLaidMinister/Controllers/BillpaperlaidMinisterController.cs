﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.PaperLaid;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Bill;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.SiteSetting;
using Lib.Web.Mvc.JQuery.JqGrid;
using SBL.DomainModel;
using System.Linq;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;


namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class BillpaperlaidMinisterController : Controller
    {
        //
        // GET: /PaperLaidDepartment/BillPaperLaid/
        public ActionResult Index()
        {
            return View();
        }

        #region Bills For Minister

        [HttpGet]
        public ActionResult GetAllBills(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {

            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_GetAllBills", model);

           // PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
           // RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
           // loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
           // loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

           // tPaperLaidV model = new tPaperLaidV();
           // mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
           // if (CurrentSession.UserName != null && CurrentSession.UserName != "")
           // {
           //     model.LoginId = CurrentSession.UserName;
           // }

           // model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            
           // foreach (var item in model.mMinisteryMinister)
           // {
           //     model.MinisterName = item.MinisterName;
           //     break;
           // }
           // model.MinistryId = model.MinistryId;


           // model.loopStart = Convert.ToInt16(loopStart);
           // model.loopEnd = Convert.ToInt16(loopEnd);
           // model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
           // model.PageIndex = Convert.ToInt16(PageNumber);

           //// SiteSettings siteSettingMod = new SiteSettings();
           //// siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
           ////model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
           ////model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;

           // SiteSettings siteSettingMod = new SiteSettings();
           // siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

           // if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
           // {
           //     model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
           // }

           // if (!string.IsNullOrEmpty(CurrentSession.SessionId))
           // {
           //     model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
           // }

           // model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetAllBills", model);
           // //    model.AssemblyID =
           // model.RowsPerPage = 10;
           // model.PageNumber = 1;
           // model.loopEnd = Convert.ToInt16(loopEnd);
           // model.loopStart = Convert.ToInt16(loopStart);
           // model.PageIndex = Convert.ToInt16(PageNumber);
           // model.PAGE_SIZE = 10;
           // //    model.ResultCount = 1;
           // model.selectedPage = 1;


           // // obj.actualFilePath = obj.FilePath + obj.FileName;
           // return PartialView("_GetAllBills", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetAllBills(JqGridRequest request, tPaperLaidV customModel)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;
            model.MinistryId = model.MinistryId;
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetAllBills", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.PaperLaidByPaperCategoryTypeList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<PaperLaidByPaperCategoryType>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from BillsList in resultForGrid
                                      select new JqGridRecord<PaperLaidByPaperCategoryType>(Convert.ToString(BillsList.PaperLaidId), new PaperLaidByPaperCategoryType(BillsList)));
            return new JqGridJsonResult() { Data = response };
        }
        //Added code venkat for dynamic 
        [HttpGet]
        public ActionResult GetDraftBills(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string Billnum, string Subject)
        {

            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;

            //Added code venkat for dynamic 
            model.BillNumber = Billnum;
            model.Subject = Subject;
            //end----------------------------
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model.DSCApplicable = siteSettingMod.DSCApplicable;
            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetDraftsBill", model);
            //    model.AssemblyID =
            //model.RowsPerPage = 10;
            //model.PageNumber = 1;
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.PAGE_SIZE = 10;
            ////    model.ResultCount = 1;
            model.selectedPage = 1;
            // obj.actualFilePath = obj.FilePath + obj.FileName;
            return PartialView("_GetDraftBills", model);
        }
        //Added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetDraftBills(JqGridRequest request, string BillNumber, string Subject)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
           // SiteSettings siteSettingMod = new SiteSettings();
           // siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
           //model.AssemblyCode =  model.AssemblyId = Convert.ToInt32(siteSettingMod.AssemblyCode);
           //model.SessionCode =  model.SessionId = Convert.ToInt32(siteSettingMod.SessionCode);

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


           
            model.DSCApplicable = siteSettingMod.DSCApplicable;

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;

            model.MinistryId = model.MinistryId;
            //Added code venkat for dynamic 
            model.BillNumber = BillNumber;
            model.Subject = Subject;

            //end-------------------------------------------

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetDraftsBill", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterBillsDraftModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<MinisterBillsDraftModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from BillsList in resultForGrid
                                      select new JqGridRecord<MinisterBillsDraftModel>(Convert.ToString(BillsList.PaperLaidId), new MinisterBillsDraftModel(BillsList)));

            return new JqGridJsonResult() { Data = response };
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult GetDraftBills(JqGridRequest request, tPaperLaidV customModel)
        //{
        //    tPaperLaidV model = new tPaperLaidV();
        //    model.PAGE_SIZE = request.RecordsCount;

        //    model.PageIndex = request.PageIndex;
        //    model.AssemblyId = 12;
        //    model.SessionId = 4;
        //    model.PaperLaidId = 5;

        //    if (CurrentSession.UserName != null && CurrentSession.UserName != "")
        //    {
        //        model.LoginId = CurrentSession.UserName;
        //    }

        //    model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


        //    SiteSettings siteSettingMod = new SiteSettings();
        //    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
        //    model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
        //    model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

        //    model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
        //    model.DepartmentId = CurrentSession.DeptID;

        //    model.MinistryId = model.MinistryId;


        //    var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetDraftsBill", model);

        //    JqGridResponse response = new JqGridResponse()
        //    {
        //        TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
        //        PageIndex = request.PageIndex,
        //        TotalRecordsCount = result.ResultCount
        //    };

        //    var resultToSort = result.PaperLaidByPaperCategoryTypeList.AsQueryable();

        //    var resultForGrid = resultToSort.OrderBy<PaperLaidByPaperCategoryType>(request.SortingName, request.SortingOrder.ToString());

        //    response.Records.AddRange(from BillsList in resultForGrid
        //                              select new JqGridRecord<PaperLaidByPaperCategoryType>(Convert.ToString(BillsList.PaperLaidId), new PaperLaidByPaperCategoryType(BillsList)));

        //    return new JqGridJsonResult() { Data = response };
        //}


        //Added code venkat for dynamic 
        public ActionResult GetSentBills(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string Billnum, string Subject)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }
            model.MinistryId = model.MinistryId;

            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);
            //Added code venkat for dynamic 
            model.BillNumber = Billnum;
            model.Subject = Subject;
            //end-------------------------------
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetsentBill", model);
            ////    model.AssemblyID =
            //model.RowsPerPage = 10;
            //model.PageNumber = 1;
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.PAGE_SIZE = 10;
            ////    model.ResultCount = 1;
            //model.selectedPage = 1;

            return PartialView("_GetSentBills", model);
        }
        //Added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetSentBills(JqGridRequest request, string BillNumber, string Subject)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;
            model.MinistryId = model.MinistryId;

            //Added code venkat for dynamic 
            model.Subject = Subject;
            model.BillNumber = BillNumber;
            //end------------------
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetsentBill", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.PaperLaidByPaperCategoryTypeList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<PaperLaidByPaperCategoryType>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from BillsList in resultForGrid
                                      select new JqGridRecord<PaperLaidByPaperCategoryType>(Convert.ToString(BillsList.PaperLaidId), new PaperLaidByPaperCategoryType(BillsList)));
            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult GetBillPLIHList(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillPLIHList", model);
            //    model.AssemblyID =
            //model.RowsPerPage = 10;
            //model.PageNumber = 1;
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.PAGE_SIZE = 10;
            ////    model.ResultCount = 1;
            //model.selectedPage = 1;

            return PartialView("_GetBillPLIHList", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetBillPLIHList(JqGridRequest request, tPaperLaidV customModel)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;
            model.MinistryId = model.MinistryId;
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillPLIHList", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.PaperLaidByPaperCategoryTypeList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<PaperLaidByPaperCategoryType>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from BillsList in resultForGrid
                                      select new JqGridRecord<PaperLaidByPaperCategoryType>(Convert.ToString(BillsList.PaperLaidId), new PaperLaidByPaperCategoryType(BillsList)));
            return new JqGridJsonResult() { Data = response };
        }
        //Added code venkat for dynamic 
        public ActionResult GetBillUPLOBList(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string Billnum, string Subject)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);

            //Added code venkat for dynamic 
            model.BillNumber = Billnum;
            model.Subject = Subject;

            //end---------------------------


           // SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillULOBList", model);
            ////    model.AssemblyID =
            //model.RowsPerPage = 10;
            //model.PageNumber = 1;
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.PAGE_SIZE = 10;
            ////    model.ResultCount = 1;
            //model.selectedPage = 1;

            return PartialView("_GetBillUPLOBList", model);
        }
        //Added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetBillUPLOBList(JqGridRequest request, string Billnum, string Subject)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;
            model.MinistryId = model.MinistryId;
            //Added code venkat for dynamic 
            model.BillNumber = Billnum;
            model.Subject = Subject;
            //end--------------------------
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillULOBList", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.PaperLaidByPaperCategoryTypeList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<PaperLaidByPaperCategoryType>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from BillsList in resultForGrid
                                      select new JqGridRecord<PaperLaidByPaperCategoryType>(Convert.ToString(BillsList.PaperLaidId), new PaperLaidByPaperCategoryType(BillsList)));

            return new JqGridJsonResult() { Data = response };
        }
        //Added code venkat for dynamic 
        public ActionResult GetBillLIHList(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string Billnum, string Subject)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);

            //Added code venkat for dynamic 
            model.BillNumber = Billnum;
            model.Subject = Subject;
            //end--------------
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillLIHList", model);
            //    model.AssemblyID =
            //model.RowsPerPage = 10;
            //model.PageNumber = 1;
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.PAGE_SIZE = 10;
            ////    model.ResultCount = 1;
            //model.selectedPage = 1;

            return PartialView("_GetBillLIHList", model);
        }
        //Added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetBillLIHList(JqGridRequest request, string Billnum, string Subject)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;
            model.MinistryId = model.MinistryId;

            //Added code venkat for dynamic 
            model.BillNumber = Billnum;
            model.Subject = Subject;
            //end-----------------
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetBillLIHList", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.PaperLaidByPaperCategoryTypeList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<PaperLaidByPaperCategoryType>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from BillsList in resultForGrid
                                      select new JqGridRecord<PaperLaidByPaperCategoryType>(Convert.ToString(BillsList.PaperLaidId), new PaperLaidByPaperCategoryType(BillsList)));
            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult GetBillSentRecordById(int paperLaidId)
        {
            tPaperLaidV mdl = new tPaperLaidV();
            mdl.PaperLaidId = Convert.ToInt64(paperLaidId);
            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "ShowSentBillById", mdl);
            return PartialView("_ShowBillSentDetailsByID", mdl);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PaperLaidSubmitt(HttpPostedFileBase file, tPaperLaidV obj, string submitButton)
        {
            if (CurrentSession.UserID != null)
            {
                tPaperLaidV mdl = new tPaperLaidV();
                tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }
                switch (submitButton)
                {
                    case "Send":
                        {                            
                            tPaperLaidV billDetails = new tPaperLaidV();

                            obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                            billDetails = obj;
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                            mdl.EventId = obj.EventId;
                            // int paperLaidId = (int)mdl.PaperLaidId;
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetFileSaveDetails", mdl);
                            
                            string categoryType = mdl.RespectivePaper;
                            if (file != null)
                            {
                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                               //string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + mdl.RespectivePaper + "/";
                                string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                if (!Dir.Exists)
                                {
                                    Dir.Create();
                                }

                                var ver = obj.FileVersion.GetValueOrDefault() + 1;
                                string ext = Path.GetExtension(file.FileName);
                                string fileName = file.FileName.Replace(ext, "");
                                string actualFileName = siteSettingMod.AssemblyCode + "_" + siteSettingMod.SessionCode + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + ver + ext;
                                file.SaveAs(Server.MapPath(Url + actualFileName));
                                mdlTemp.FileName = actualFileName;
                                mdlTemp.FilePath = Url;
                                mdlTemp.Version = obj.FileVersion+1;
                                mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                mdlTemp.DeptSubmittedDate = DateTime.Now;
                                mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                            }

                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                            //if (categoryType.IndexOf("bill", 0, StringComparison.CurrentCultureIgnoreCase) != -1)                            //  {
                            billDetails.PaperLaidId = mdl.PaperLaidId;
                            tBillRegister bills = new tBillRegister();
                            bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "InsertBillDetails", billDetails);
                            //  }
                            mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                            mdl.ModifiedBy = CurrentSession.UserName;
                            mdl.ModifiedWhen = DateTime.Now;
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntryForBilReplace", mdl);
                            //SendBillPaperByDept(mdl);
                            break;
                        }
                    case "Update":
                        {
                            obj.tpaperLaidTempId = 0;
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);
                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", mdl);
                            if (file != null)
                            {
                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                //string Url = "~/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + mdl.RespectivePaper + "/";
                                string Url = mdlTemp.FilePath;
                                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                if (!Dir.Exists)
                                {
                                    Dir.Create();
                                }
                                var ver = obj.FileVersion.GetValueOrDefault();
                                string ext = Path.GetExtension(file.FileName);
                                string fileName = file.FileName.Replace(ext, "");
                                string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + ver + ext;
                                file.SaveAs(Server.MapPath(Url + actualFileName));
                                mdlTemp.FileName = actualFileName;

                            }

                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaid", mdlTemp);
                            tBillRegister billDetails = new tBillRegister();
                            billDetails.PaperLaidId = obj.PaperLaidId;
                            billDetails.BillTypeId = obj.BillTypeId;
                            billDetails.BillNumber = obj.BillNUmberValue.ToString()+" of "+obj.BillNumberYear.ToString();
                            billDetails.ShortTitle = obj.Description;
                            billDetails.MinisterId = obj.MinistryId;
                            //billDetails.
                            tBillRegister bills = new tBillRegister();
                            bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "UpdateBillDetails", billDetails);

                            break;
                        }
                    case "Save":
                        {
                            //obj.AssemblyId = obj.AssemblyId;
                            //obj.SessionId = obj.SessionId;
                            //int questionId = obj.QuestionID;
                            //int noticeId = obj.NoticeID;
                            tPaperLaidV billDetails = new tPaperLaidV();

                            obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                            billDetails = obj;
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                            mdl.EventId = obj.EventId;
                            int paperLaidId = (int)mdl.PaperLaidId;
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetFileSaveDetails", mdl);
                            
                            //tPaperLaidV versioncount = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetFileVersion", obj);
                            string categoryType = mdl.RespectivePaper;
                            if (file != null)
                            {
                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                                //string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + mdl.RespectivePaper + "/";
                                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                if (!Dir.Exists)
                                {
                                    Dir.Create();
                                }
                                var ver = obj.FileVersion.GetValueOrDefault() + 1;
                                string ext = Path.GetExtension(file.FileName);
                                string fileName = file.FileName.Replace(ext, "");
                                string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + ver + ext;
                                file.SaveAs(Server.MapPath(Url + actualFileName));
                                mdlTemp.FileName = actualFileName;
                                mdlTemp.FilePath = Url;
                                mdlTemp.Version = obj.FileVersion+1;//versioncount.FileVersion;
                                mdlTemp.PaperLaidId = mdl.PaperLaidId;
                            }

                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                            if (categoryType.IndexOf("bill", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                            {
                                billDetails.PaperLaidId = paperLaidId;
                                tBillRegister bills = new tBillRegister();
                                bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "InsertBillDetails", billDetails);
                            }
                            break;
                        }
                    case "Update Send":
                        {
                            // obj.tpaperLaidTempId = 0;
                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", obj);
                            obj.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);

                            if (file != null)
                            {
                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                //  string Url = "~/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + mdl.RespectivePaper + "/";
                                string Url = mdlTemp.FilePath;
                                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                if (!Dir.Exists)
                                {
                                    Dir.Create();
                                }
                                var ver = obj.FileVersion.GetValueOrDefault();
                                string ext = Path.GetExtension(file.FileName);
                                string fileName = file.FileName.Replace(ext, "");
                                string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "B" + "_" + mdl.PaperLaidId + "_V" + ver + ext;
                                file.SaveAs(Server.MapPath(Url + actualFileName));
                                mdlTemp.FileName = actualFileName;

                            }

                            mdlTemp.DeptSubmittedDate = DateTime.Now;
                            mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaid", mdlTemp);

                            tBillRegister billDetails = new tBillRegister();
                            billDetails.PaperLaidId = obj.PaperLaidId;
                            billDetails.BillTypeId = obj.BillTypeId;
                            billDetails.BillNumber = obj.BillNUmberValue.ToString() + " of " + obj.BillNumberYear.ToString();
                            billDetails.ShortTitle = obj.Description;
                            billDetails.MinisterId = obj.MinistryId;
                            
                            tBillRegister bills = new tBillRegister();
                            bills = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "UpdateBillDetails", billDetails);
                            break;
                        }
                }
                return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment"); //RedirectToAction("DepartmentDashboard");
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }

        //public ActionResult ShowBillSentDetailsByID(int paperLaidId)
        //{

        //    tPaperLaidV mdl = new tPaperLaidV();
        //    mdl.PaperLaidId = Convert.ToInt64(paperLaidId);
        //    mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "ShowDraftBillById", mdl);
        //    return View();
        //}

        public ActionResult ShowDraftBillById(int paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {

                tPaperLaidV mdl = new tPaperLaidV();
                mdl.PaperLaidId = Convert.ToInt64(paperLaidId);
                mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "ShowDraftBillById", mdl);

                //mDepartment departmentModel = new mDepartment();
                mEvent mEventModel = new mEvent();
                mdl.DepartmentId = CurrentSession.DeptID;
                tPaperLaidV ministerDetails = new tPaperLaidV();
                ministerDetails.DepartmentId = mdl.DepartmentId;
                //mdl.SessionId = sessionId;
                // mdl.AssemblyCode = assemblyId;

                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                mdl.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", mdl);

                mdl.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("BillPaperLaid", "GetDocumentType", mdl);
                //mdl.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);

                List<mEvent> events = (List<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
                mdl.mEvents = events.FindAll(a => a.EventId == 5 || a.EventId == 6);

                mdl.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);
                mdl.mBillTypes = (List<mBillType>)Helper.ExecuteService("BillPaperLaid", "GetBillTypes", "Testdata");

                ICollection<int> yearList = new List<int>();
                int currentYear = DateTime.Now.Year;
                for (int i = currentYear - 5; i < currentYear; i++)
                {
                    yearList.Add(i);
                }
                for (int i = currentYear; i < currentYear + 5; i++)
                {
                    yearList.Add(i);
                }
               // mdl.yearList = yearList;

                return PartialView("_CreateNewBillDraft", mdl);
            }
            return null;
        }

        public ActionResult CreateNewBillDraft(int sessionId, int assemblyId)
        {
            tPaperLaidV paperLaidModel = new tPaperLaidV();
            if (CurrentSession.UserID != null)
            {

                //mDepartment departmentModel = new mDepartment();
                mEvent mEventModel = new mEvent();
                paperLaidModel.DepartmentId = CurrentSession.DeptID;
                tPaperLaidV ministerDetails = new tPaperLaidV();
                ministerDetails.DepartmentId = paperLaidModel.DepartmentId;
                paperLaidModel.SessionId = sessionId;
                paperLaidModel.AssemblyCode = assemblyId;
                paperLaidModel.AssemblyId = assemblyId;
                paperLaidModel.SessionCode = sessionId;

                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                paperLaidModel.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", paperLaidModel);

                paperLaidModel.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("BillPaperLaid", "GetDocumentType", paperLaidModel);
                //paperLaidModel.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);

                List<mEvent> events = (List<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
                paperLaidModel.mEvents = events.FindAll(a => a.EventId == 5 || a.EventId == 6);

                paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);
                paperLaidModel.mBillTypes = (List<mBillType>)Helper.ExecuteService("BillPaperLaid", "GetBillTypes", "Testdata");

                ICollection<int> yearList = new List<int>();
                int currentYear = DateTime.Now.Year;
                for (int i = currentYear - 5; i < currentYear; i++)
                {
                    yearList.Add(i);
                }
                for (int i = currentYear; i < currentYear + 5; i++)
                {
                    yearList.Add(i);
                }
               // paperLaidModel.yearList = yearList;

                //   paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("BillPaperLaid", "GetMinisterMinistryName", paperLaidModel.DepartmentId);
                //   paperLaidModel.MinistryId = mMinisteryMinisterModel.MinistryID;



                //return View();
            }

            return PartialView("_CreateNewBillDraft", paperLaidModel);
        }

        public JsonResult CheckBillNumber(string number, string year)
        {
            if (CurrentSession.UserID != null)
            {
                tBillRegister mdl = new tBillRegister();
                string billNumber = number + " of " + year;
                mdl = (tBillRegister)Helper.ExecuteService("BillPaperLaid", "CheckBillNUmber", billNumber);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public ActionResult GetBillSentByID(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {
                tPaperLaidV movementModel = new tPaperLaidV();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetSubmittedPaperLaidByID", movementModel);
                return PartialView("_GetBillSentByID", movementModel);
            }
            return null;
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateBillPaperLaidFile(HttpPostedFileBase file, tPaperLaidV tPaper)
        {
            if (CurrentSession.UserID != "")
            {
                tPaperLaidTemp tPaperTemp = new tPaperLaidTemp();
                tPaper = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", tPaper);

                if (file != null)
                {
                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    string Url = "/PaperLaid/" + siteSettingMod.AssemblyCode + "/" + siteSettingMod.SessionCode + "/";
                    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }

                    var ver = tPaper.FileVersion.GetValueOrDefault() + 1;
                    string ext = Path.GetExtension(file.FileName);
                    string fileName = file.FileName.Replace(ext, "");
                    string actualFileName = siteSettingMod.AssemblyCode + "_" + siteSettingMod.SessionCode + "_" + "B" + "_" + tPaper.PaperLaidId + "_V" + ver + ext;
                    file.SaveAs(Server.MapPath(Url + actualFileName));
                    tPaperTemp.FileName = actualFileName;
                    tPaperTemp.FilePath = Url;
                    tPaperTemp.Version = ver;
                    tPaperTemp.PaperLaidId = tPaper.PaperLaidId;
                    tPaperTemp.DeptSubmittedDate = DateTime.Now;
                    tPaperTemp.DeptSubmittedBy = CurrentSession.UserName;
                }

                tPaperTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateBillPaperLaidFileByID", tPaperTemp);
                tPaper.DeptActivePaperId = tPaperTemp.PaperLaidTempId;
                tPaper.ModifiedBy = CurrentSession.UserName;
                tPaper.ModifiedWhen = DateTime.Now;
                tPaper = (tPaperLaidV)Helper.ExecuteService("Notice", "UpdatePaperLaidEntryForNoticeReplace", tPaper);

            }
            return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment");
        }

        //public tPaperLaidTemp SaveFileToDirectory(tQuestion tQuestions, HttpPostedFileBase file, tPaperLaidTemp mdlTemp, tPaperLaidV mdl, tPaperLaidV obj)
        //{
        //    string QType = string.Empty;

        //    if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
        //    {
        //        QType = "S";
        //    }
        //    else if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
        //    {
        //        QType = "US";
        //    }
        //    string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
        //    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
        //    if (!Dir.Exists)
        //    {
        //        Dir.Create();
        //    }
        //    string ext = Path.GetExtension(file.FileName);
        //    string fileName = file.FileName.Replace(ext, "");
        //    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + tQuestions.QuestionNumber + "_V" + mdl.Count + ext;
        //    //+ mdl.Count 
        //    file.SaveAs(Server.MapPath(Url + actualFileName));
        //    mdlTemp.FileName = actualFileName;
        //    mdlTemp.FilePath = Url;
        //    mdlTemp.Version = mdl.Count;
        //    mdlTemp.PaperLaidId = mdl.PaperLaidId;

        //    return mdlTemp;
        //}
        #endregion
    }
}
