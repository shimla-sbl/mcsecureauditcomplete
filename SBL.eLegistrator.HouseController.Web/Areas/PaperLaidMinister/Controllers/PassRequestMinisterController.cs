﻿namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister.Controllers
{
    #region
    using SBL.DomainModel.Models.Adhaar;
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Models;
    using SBL.eLegistrator.HouseController.Web.Helpers;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Extensions;
    using SBL.DomainModel.Models.PaperLaid;
    using SBL.DomainModel.Models.Session;
    using SBL.eLegistrator.HouseController.Web.Areas.Reporters.Models;
    using SBL.eLegistrator.HouseController.Web.Utility;
    using System.Data;
    using SBL.DomainModel.Models.Diaries;
    using SBL.DomainModel.Models.Department;
    using SBL.eLegistrator.HouseController.Filters;
    using SBL.eLegistrator.HouseController.Web.Filters;
    using SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Models;
    #endregion

    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class PassRequestMinisterController : Controller
    {
        public ActionResult Index()
        {
            PassesViewModel model = new PassesViewModel();
            return View(model);
        }

        public ActionResult PartialPassRequestList(int Status = 0, int PassCategoryID = 0, string DepartmentCatID = "")
        {
            PassesViewModel model = new PassesViewModel();
            List<mDepartmentPasses> ListdeprtmentPasses = new List<mDepartmentPasses>();
            mDepartmentPasses model1 = new mDepartmentPasses();
            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                model1.IsCurrentAssemblyID = Convert.ToInt32(siteSettingMod.AssemblyCode);
                model1.IsCurrentSessionID = Convert.ToInt32(siteSettingMod.SessionCode);

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model1.AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model1.SessionCode = Convert.ToInt32(CurrentSession.SessionId);
                }

                tPaperLaidV objPaperLaid = new tPaperLaidV();

                if (CurrentSession.UserName != null && CurrentSession.UserName != "")
                {
                    objPaperLaid.LoginId = CurrentSession.UserName;
                    model1.IsRequestUserID = CurrentSession.UserID;
                    objPaperLaid.UserID = new Guid(CurrentSession.UserID);
                }

                objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);
                DiaryModel DMdl = new DiaryModel();
                DMdl.MinistryId = objPaperLaid.MinistryId;
                DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);

                if (DMdl.DiaryList != null)
                {
                    List<mDepartment> objDept = new List<mDepartment>();
                    foreach (var item in DMdl.DiaryList.Distinct())
                    {
                        model1.DepartmentID += item.DepartmentId + ",";
                    }
                    model1.DepartmentID = model1.DepartmentID.Substring(0, model1.DepartmentID.LastIndexOf(","));
                }
                else
                {
                    model1.DepartmentID = string.Empty;
                }


                model.ListPassCategory = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);

                objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);
                DiaryModel DMdl1 = new DiaryModel();
                DMdl1.MinistryId = objPaperLaid.MinistryId;
                DMdl1.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl1);

                if (DMdl1.DiaryList != null)
                {
                    List<mDepartment> objDept = new List<mDepartment>();
                    foreach (var item in DMdl.DiaryList.Distinct())
                    {
                        mDepartment obj = new mDepartment();
                        obj.deptId = item.DepartmentId;
                        obj.deptname = item.DepartmentName;
                        objDept.Add(obj);
                    }

                    if (objDept != null)
                    {
                        model.mDepartmentList = objDept;
                    }
                }
                ListdeprtmentPasses = (List<mDepartmentPasses>)Helper.ExecuteService("Passes", "GetDepartmentPassesByDeptId", model1);

                var ListViewModel = ListdeprtmentPasses.ToModelList();
                if (DepartmentCatID != "")
                {
                    ListViewModel = ListViewModel.Where(m => m.DepartmentID == DepartmentCatID).ToList();

                }

                if (PassCategoryID != 0)
                {
                    ListViewModel = ListViewModel.Where(m => m.PassCategoryID == PassCategoryID).ToList();

                }

                if (Status != 0)
                {
                    if (Status == 10)
                    {
                        ListViewModel = ListViewModel.Where(m => m.Status == 1 && m.IsRequested == false).ToList();
                    }
                    else if (Status == 1)
                    {
                        ListViewModel = ListViewModel.Where(m => m.Status == 1 && m.IsRequested == true && m.IsApproved == false).ToList();
                    }
                    else if (Status == 2)
                    {
                        ListViewModel = ListViewModel.Where(m => m.Status == 2 && m.IsRequested == true).ToList();
                    }

                }

                model.DepartmentID = DepartmentCatID;
                model.PassCategoryID = PassCategoryID;

                model.SessionCode = model1.SessionCode;

                model.ListPasses = ListViewModel;
                model.ValidateSessionID = siteSettingMod.SessionCode;
                return PartialView("PartialPassRequestList", model);
            }
            return PartialView("PartialPassRequestList", model);
        }

        public ActionResult PartialPassApprovedList(int Status = 2, int PassCategoryID = 0, string DepartmentCatID = "")
        {
            PassesViewModel model = new PassesViewModel();
            List<mPasses> ListdeprtmentPasses = new List<mPasses>();
            mDepartmentPasses model1 = new mDepartmentPasses();
            mPasses model2 = new mPasses();
            var siteSettingPrint = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetMinisterPrintSetting", null);

            if (PassCategoryID != 0 || DepartmentCatID != "")
            {
                if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
                {
                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                    model1.IsCurrentAssemblyID = Convert.ToInt32(siteSettingMod.AssemblyCode);
                    model1.IsCurrentSessionID = Convert.ToInt32(siteSettingMod.SessionCode);

                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                    {
                        model1.AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
                        model2.AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
                    }

                    if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                    {
                        model1.SessionCode = Convert.ToInt32(CurrentSession.SessionId);
                        model2.SessionCode = Convert.ToInt32(CurrentSession.SessionId);
                    }

                    tPaperLaidV objPaperLaid = new tPaperLaidV();

                    if (CurrentSession.UserName != null && CurrentSession.UserName != "")
                    {
                        objPaperLaid.LoginId = CurrentSession.UserName;
                        model1.IsRequestUserID = CurrentSession.UserID;
                        objPaperLaid.UserID = new Guid(CurrentSession.UserID);
                        model2.IsRequestUserID = CurrentSession.UserID;
                    }

                    objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);
                    DiaryModel DMdl = new DiaryModel();
                    DMdl.MinistryId = objPaperLaid.MinistryId;
                    DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);

                    if (DMdl.DiaryList != null)
                    {
                        List<mDepartment> objDept = new List<mDepartment>();
                        foreach (var item in DMdl.DiaryList.Distinct())
                        {
                            model1.DepartmentID += item.DepartmentId + ",";
                        }
                        model1.DepartmentID = model1.DepartmentID.Substring(0, model1.DepartmentID.LastIndexOf(","));
                    }
                    else
                    {
                        model1.DepartmentID = string.Empty;
                    }


                    model.ListPassCategory = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);

                    objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);
                    DiaryModel DMdl1 = new DiaryModel();
                    DMdl1.MinistryId = objPaperLaid.MinistryId;
                    DMdl1.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl1);

                    if (DMdl1.DiaryList != null)
                    {
                        List<mDepartment> objDept = new List<mDepartment>();
                        foreach (var item in DMdl.DiaryList.Distinct())
                        {
                            mDepartment obj = new mDepartment();
                            obj.deptId = item.DepartmentId;
                            obj.deptname = item.DepartmentName;
                            objDept.Add(obj);
                        }

                        if (objDept != null)
                        {
                            model.mDepartmentList = objDept;
                        }
                    }
                    ListdeprtmentPasses = (List<mPasses>)Helper.ExecuteService("AdministrationBranch", "GetmPassListForDepartmentPrint", model2);

                    var ListViewModel = ListdeprtmentPasses.TomPassModelList();
                    if (DepartmentCatID != "")
                    {
                        ListViewModel = ListViewModel.Where(m => m.DepartmentID == DepartmentCatID).ToList();

                    }

                    if (PassCategoryID != 0)
                    {
                        ListViewModel = ListViewModel.Where(m => m.PassCategoryID == PassCategoryID).ToList();

                    }

                    if (Status != 0)
                    {

                        ListViewModel = ListViewModel.Where(m => m.Status == 2 && m.IsRequested == true).ToList();
                    }
                    model.IsDepartmentPrint = siteSettingPrint.SettingValue;
                    model.DepartmentID = DepartmentCatID;
                    model.PassCategoryID = PassCategoryID;
                    model.SessionCode = model1.SessionCode;
                    model.ListPasses = ListViewModel;
                    model.ValidateSessionID = siteSettingMod.SessionCode;
                    return PartialView("PartialPassApprovedList", model);
                }
            }
            else {
                model.ListPassCategory = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);

                tPaperLaidV objPaperLaid = new tPaperLaidV();

                objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);
                DiaryModel DMdl1 = new DiaryModel();
                DMdl1.MinistryId = objPaperLaid.MinistryId;
                DMdl1.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl1);
                if (DMdl1.DiaryList != null)
                {
                    List<mDepartment> objDept = new List<mDepartment>();
                    foreach (var item in DMdl1.DiaryList.Distinct())
                    {
                        mDepartment obj = new mDepartment();
                        obj.deptId = item.DepartmentId;
                        obj.deptname = item.DepartmentName;
                        objDept.Add(obj);
                    }

                    if (objDept != null)
                    {
                        model.mDepartmentList = objDept;
                    }
                }
            }
            
            return PartialView("PartialPassApprovedList", model);
        }
        public ActionResult PrintSelectedMasterPass(string PassIDs)
        {
            List<mPassModel> PrintPreview = new List<mPassModel>();
            try
            {
                string[] PassIds = PassIDs.Split(',');
                List<mPasses> PrintPreviewModel = new List<mPasses>();
                foreach (var passID in PassIds)
                {
                    int IDForPass = Convert.ToInt32(passID);
                    var publicPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = IDForPass });
                    //var publicPassAsXML = (string)Helper.ExecuteService("AdministrationBranch", "GetMasterPassByPassID", IDForPass);
                    PrintPreviewModel.Add(publicPass);
                }
                PrintPreview.AddRange(PrintPreviewModel.ToModelListWithXML());
                // return PartialView("/Areas/VidhanSabhaDepartment/Views/VidhanSabhaDept/MemberAccounts/_UpdateMemberAccounts.cshtml");
                return PartialView("~/Views/DepartmentMinisterPrint/_PrintPasses.cshtml", PrintPreview.OrderBy(m => m.PassCode));
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw;
            }
        }
        public ActionResult FullMasterPassDetails(int Id)
        {
            var masterPass = (mPasses)Helper.ExecuteService("AdministrationBranch", "GetMasterPassById", new mPasses { PassID = Id });
            var model = masterPass.ToModelmPass();
            return View("FullPassDetails", model);
        }
        public ActionResult FullPassDetails(int Id)
        {
            var PassRequest = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", new mDepartmentPasses { PassID = Id });
            var model = PassRequest.ToModel();
            return View(model);
        }

        public ActionResult PartialAddPassRequest()
        {
            PassesViewModel model = new PassesViewModel();
            tPaperLaidV objPaperLaid = new tPaperLaidV();
            model.GenderList = ExtensionMethods.GetGender();
            model.TimeTypeList = ExtensionMethods.GetTimeTypeList();

            SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            session.AssemblyID = Convert.ToInt32(siteSettingMod.AssemblyCode);
            session.SessionCode = Convert.ToInt32(siteSettingMod.SessionCode);
            List<mSessionDate> ListSessionDates = new List<mSessionDate>();
            ListSessionDates = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", session);

            List<mSessionDateModel> ListSession = new List<mSessionDateModel>();

            for (int i = 0; i < ListSessionDates.Count; i++)
            {
                mSessionDateModel Session = new mSessionDateModel();
                Session.SessionDate = ListSessionDates[i].SessionDate.ToString("dd/MM/yyyy");
                ListSession.Add(Session);
            }
            model.mSessionDateList = ListSession;

            model.Mode = "Add";

            string DepartmentId = Utility.CurrentSession.DeptID;


            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                objPaperLaid.UserID = new Guid(CurrentSession.UserID);
            }
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                objPaperLaid.LoginId = CurrentSession.UserName;
            }

            objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);

            DiaryModel DMdl = new DiaryModel();
            DMdl.MinistryId = objPaperLaid.MinistryId;
            DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);

            if (DMdl.DiaryList != null)
            {
                List<mDepartment> objDept = new List<mDepartment>();
                foreach (var item in DMdl.DiaryList.Distinct())
                {
                    mDepartment obj = new mDepartment();
                    obj.deptId = item.DepartmentId;
                    obj.deptname = item.DepartmentName;
                    objDept.Add(obj);
                }

                if (objDept != null)
                {
                    model.mDepartment = objDept;
                }
            }

            AdhaarDetails mUserDetails = new AdhaarDetails();
            string AadharID = CurrentSession.AadharId;
            if (AadharID != null)
            {
                mUserDetails.AdhaarID = AadharID;
                mUserDetails = (AdhaarDetails)Helper.ExecuteService("User", "GetUser_DetailsByAdhaarID", mUserDetails);
                if (mUserDetails != null)
                {
                    model.RecommendationBy = mUserDetails.Name;
                }
            }
            mDepartmentPasses department = new mDepartmentPasses();
            department.DepartmentID = model.DepartmentID;

            //model.mDepartment = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Passes", "GetDepartmentByids", department);

            model.NoOfPersions = 1;
            if (ListSessionDates.Count != 1 && ListSessionDates.Count > 0)
            {
                model.SessionDateTo = ListSessionDates[ListSessionDates.Count - 1].SessionDate.ToString("dd/MM/yyyy");
            }
            else if (ListSessionDates.Count == 1)
            {
                model.SessionDateTo = ListSessionDates[0].SessionDate.ToString("dd/MM/yyyy");
            }

            model.RecommendationType = "Minister";

            model.AssemblyCode = session.AssemblyID;
            model.SessionCode = session.SessionCode;

            model.ListPassCategory = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);

            return PartialView("PartialAddPassRequest", model);
        }

        public ActionResult EditPassRequest(int Id)
        {
            var PassRequest = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", new mDepartmentPasses { PassID = Id });
            var model = PassRequest.ToModel();
            tPaperLaidV objPaperLaid = new tPaperLaidV();
            string DepartmentId = Utility.CurrentSession.DeptID;
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {

                objPaperLaid.UserID = new Guid(CurrentSession.UserID);
            }
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                objPaperLaid.LoginId = CurrentSession.UserName;
            }



            objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);

            DiaryModel DMdl = new DiaryModel();
            DMdl.MinistryId = objPaperLaid.MinistryId;
            DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);

            if (DMdl.DiaryList != null)
            {
                List<mDepartment> objDept = new List<mDepartment>();
                foreach (var item in DMdl.DiaryList.Distinct())
                {
                    mDepartment obj = new mDepartment();
                    obj.deptId = item.DepartmentId;
                    obj.deptname = item.DepartmentName;
                    objDept.Add(obj);
                }

                if (objDept != null)
                {
                    model.mDepartment = objDept;
                }
            }


            SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            session.AssemblyID = Convert.ToInt32(siteSettingMod.AssemblyCode);
            session.SessionCode = Convert.ToInt32(siteSettingMod.SessionCode);
            List<mSessionDate> ListSessionDates = new List<mSessionDate>();
            ListSessionDates = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateBySessionCode", session);

            List<mSessionDateModel> ListSession = new List<mSessionDateModel>();
            for (int i = 0; i < ListSessionDates.Count; i++)
            {
                mSessionDateModel Session = new mSessionDateModel();
                Session.SessionDate = ListSessionDates[i].SessionDate.ToString("dd/MM/yyyy");
                ListSession.Add(Session);
            }

            model.mSessionDateList = ListSession;
            model.GenderList = ExtensionMethods.GetGender();
            model.TimeTypeList = ExtensionMethods.GetTimeTypeList();
            model.Mode = "Update";
            model.ListPassCategory = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
            return View("PartialAddPassRequest", model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SavePassRequest(PassesViewModel model, HttpPostedFileBase file)
        {
            if (file != null)
            {
                if (model.Mode == "Add")
                {
                    string extension = System.IO.Path.GetExtension(file.FileName);

                    SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();
                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    session.AssemblyID = Convert.ToInt32(siteSettingMod.AssemblyCode);
                    session.SessionCode = Convert.ToInt32(siteSettingMod.SessionCode);
                    string urlPath = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImageLocation(session.AssemblyID, session.SessionCode);
                    string Url = urlPath;
                    string baseTemp = urlPath + "Temp" + "/";
                    DirectoryInfo Dir = new DirectoryInfo(Url);
                    DirectoryInfo Dir1 = new DirectoryInfo(baseTemp);
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    if (!Dir1.Exists)
                    {
                        Dir1.Create();
                    }
                    Guid PicName;
                    PicName = Guid.NewGuid();
                    string path = System.IO.Path.Combine(Url, PicName + ".jpg");
                    string Temp = System.IO.Path.Combine(baseTemp, PicName + ".jpg");
                    model.Photo = Url + PicName + ".jpg";
                    SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions sdf = new SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions(180);
                    file.SaveAs(Temp);
                    sdf.Resize(Temp, path);
                    System.IO.File.Delete(Temp);
                }
                else
                {
                    var PassRequest = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", new mDepartmentPasses { PassID = model.PassID });
                    string filelocation = PassRequest.Photo;
                    int indexof = filelocation.LastIndexOf("/");
                    string url = filelocation.Substring(0, indexof + 1);
                    string FileName = filelocation.Substring(indexof + 1);
                    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(url));
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    var path = url + FileName;
                    file.SaveAs(path);

                }
            }


            SBL.DomainModel.Models.Department.mDepartment department = new DomainModel.Models.Department.mDepartment();
            department.deptId = model.DepartmentID;
            department = (SBL.DomainModel.Models.Department.mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", department);
            model.OrganizationName = department.deptname.Trim();

            model.IsActive = true;
            var EntityModel = model.ToEntity();
            if (model.Mode == "Add")
            {
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    EntityModel.IsRequestUserID = CurrentSession.UserID;
                }
                Helper.ExecuteService("Passes", "AddPassRequest", EntityModel);
                TempData["Msg"] = "Save";
                TempData["Dept"] = "Mins";
                TempData["ReqMesg"] = EntityModel.RequestdID;
                //  Notification.SendEmailDetails(EntityModel);
            }
            else
            {
                EntityModel.Photo = model.PhotoName;
                Helper.ExecuteService("Passes", "UpdatePassRequest", EntityModel);
                TempData["Msg"] = "Update";
            }
            string Value = "";
#pragma warning disable CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
            if (TempData["Msg"] == "Save")
#pragma warning restore CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
            {
                Value = "Save" + "," + TempData["ReqMesg"];
                return Json(Value, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Value = "Update";
                return Json(Value, JsonRequestBehavior.AllowGet);

            }

            // return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
        }

        public JsonResult GetAdhaarDetails(string AdhaarID, string fileLoaction)
        {
            AdhaarDetails details = new AdhaarDetails();
            AdhaarServices.ServiceSoapClient obj = new AdhaarServices.ServiceSoapClient();
            EncryptionDecryption.EncryptionDecryption objencr = new EncryptionDecryption.EncryptionDecryption();
            string inputValue = objencr.Encryption(AdhaarID.Trim());
            try
            {
                DataTable dt = obj.getHPKYCInDataTable(inputValue);
                if (dt != null && dt.Rows.Count > 0)
                {
                    details.AdhaarID = AdhaarID;
                    details.Name = objencr.Decryption(Convert.ToString(dt.Rows[0]["Name"]));
                    details.FatherName = objencr.Decryption(Convert.ToString(dt.Rows[0]["FatherName"]));
                    details.Gender = objencr.Decryption(Convert.ToString(dt.Rows[0]["Gender"]));
                    details.Address = objencr.Decryption(Convert.ToString(dt.Rows[0]["Address"]).ToString());
                    details.DOB = objencr.Decryption(Convert.ToString(dt.Rows[0]["DOB"]));
                    details.MobileNo = objencr.Decryption(Convert.ToString(dt.Rows[0]["MobileNumber"]));
                    details.Email = objencr.Decryption(Convert.ToString(dt.Rows[0]["EmailID"]));
                    details.District = objencr.Decryption(Convert.ToString(dt.Rows[0]["DistrictName"]).ToString());
                    details.PinCode = objencr.Decryption(Convert.ToString(dt.Rows[0]["PinCOde"]).ToString());


                    //Calculate the age.
                    DateTime dateOfBirth;

                    if (!string.IsNullOrEmpty(details.DOB) && DateTime.TryParse(details.DOB, out dateOfBirth))
                    {
                        dateOfBirth = Convert.ToDateTime(details.DOB);
                        DateTime today = DateTime.Today;
                        int age = today.Year - dateOfBirth.Year;
                        if (dateOfBirth > today.AddYears(-age))
                            age--;

                        details.DOB = Convert.ToString(age);
                    }
                    else
                    {
                        details.DOB = "";
                    }
                    Byte[] bytes = (Byte[])Convert.FromBase64String(objencr.Decryption(dt.Rows[0]["photo"].ToString()));
                    if (fileLoaction != null && fileLoaction != "" && (fileLoaction.IndexOf("/") != -1))
                    {
                        int indexof = fileLoaction.LastIndexOf("/");
                        string url = fileLoaction.Substring(0, indexof + 1);
                        string FileName = fileLoaction.Substring(indexof + 1);
                        var path = Server.MapPath(url) + FileName;
                        System.IO.File.WriteAllBytes(path, bytes);

                        details.Photo = fileLoaction;
                    }
                    else
                    {
                        Guid PicName;
                        PicName = Guid.NewGuid();
                        SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();
                        SiteSettings siteSettingMod = new SiteSettings();
                        siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                        session.AssemblyID = Convert.ToInt32(siteSettingMod.AssemblyCode);
                        session.SessionCode = Convert.ToInt32(siteSettingMod.SessionCode);

                        string urlPath = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImageLocation(session.AssemblyID, session.SessionCode);
                        string Url = urlPath;

                        // string Url = "/Images/Pass/Photo/" + session.AssemblyID + "/" + session.SessionCode + "/";
                        DirectoryInfo Dir = new DirectoryInfo(Url);
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        var path = Url + PicName + ".jpg";
                        System.IO.File.WriteAllBytes(path, bytes);
                        string showUrl = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(session.AssemblyID, session.SessionCode, PicName + ".jpg");
                        details.Photo = showUrl + "," + PicName + ".jpg";
                    }

                    return Json(details, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(details, JsonRequestBehavior.AllowGet);
            }
        }

        public object SubmitDepartmentPass(string PassID)
        {
            mDepartmentPasses model = new mDepartmentPasses();
            model.PassID = Convert.ToInt32(PassID);
            Helper.ExecuteService("Passes", "SubmitDepartmentPassRequest", model);
            return null;
        }

        public object SubmitSelectedDepartmentPass(string PassIDs)
        {
            string[] PassIds = PassIDs.Split(',');

            foreach (var PassID in PassIds)
            {
                mDepartmentPasses model = new mDepartmentPasses();
                model.PassID = Convert.ToInt32(PassID);
                Helper.ExecuteService("Passes", "SubmitDepartmentPassRequest", model);
            }

            return null;
        }


        public object TransferSelectedDepartmentPass(string PassIDs)
        {
            string[] PassIds = PassIDs.Split(',');

            foreach (var PassID in PassIds)
            {
                mDepartmentPasses model = new mDepartmentPasses();
                mDepartmentPasses model1 = new mDepartmentPasses();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                model.PassID = Convert.ToInt32(PassID);
                model = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", model);
                model.SessionCode = Convert.ToInt32(siteSettingMod.SessionCode);
                model.PassCode = null;
                model.ApprovedDate = null;
                model.ApprovedBy = null;
                model.IsRequested = false;
                model.RejectionDate = null;
                model.RejectionDate = DateTime.Now;
                model.IsApproved = false;
                model.Status = 1;
                model.RequestdID = SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Extensions.ExtensionMethods.GenerateValue(model.RecommendationType.Substring(0, 1), siteSettingMod.AssemblyCode, siteSettingMod.SessionCode);
                model.SessionDateFrom = SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Extensions.ExtensionMethods.GetSessionDateFrom(siteSettingMod.AssemblyCode, siteSettingMod.SessionCode);
                model.SessionDateTo = SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Extensions.ExtensionMethods.GetSessionDateTo(siteSettingMod.AssemblyCode, siteSettingMod.SessionCode);
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    model.IsRequestUserID = CurrentSession.UserID;
                }

                ///
                /// Insert New Record To Next Section
                ///
                string Url = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImageLocation(Convert.ToInt32(CurrentSession.AssemblyId), Convert.ToInt32(CurrentSession.SessionId));
                string CurrentUrl = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImageLocation(siteSettingMod.AssemblyCode, siteSettingMod.SessionCode);
                DirectoryInfo Dir = new DirectoryInfo(Url);
                if (!Dir.Exists)
                {
                    Dir.Create();
                }
                DirectoryInfo Dir1 = new DirectoryInfo(CurrentUrl);
                if (!Dir1.Exists)
                {
                    Dir1.Create();
                }
                if (model.Photo != null)
                {
                    string SourceFile = System.IO.Path.Combine(Url + model.Photo);
                    string destFile = System.IO.Path.Combine(CurrentUrl, model.Photo);

                    if (System.IO.File.Exists(destFile))
                    {
                        System.IO.File.Delete(destFile);
                    }
                    if (System.IO.File.Exists(SourceFile))
                    {
                        System.IO.File.Copy(SourceFile, destFile);
                    }
                }
                Helper.ExecuteService("Passes", "AddPassRequest", model);

                ///
                /// Update Is TransferID Coressponding PassID
                ///
                model1 = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", model);
                model1.PassID = Convert.ToInt32(PassID);
                model1.IsSessionTransferID = true;

                Helper.ExecuteService("Passes", "UpdatePassRequest", model1);
                // Notification.SendEmailDetails(model);
            }

            return null;
        }
        public string GetSession()
        {
            string value = "";
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            int SessionCode = Convert.ToInt32(siteSettingMod.SessionCode);
            value = Convert.ToString(SessionCode);
            return value;
        }

    }
}
