﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.PaperLaid;
using SBL.eLegistrator.HouseController.Web.Filters;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Event;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.Enums;
using Lib.Web.Mvc.JQuery.JqGrid;
using System.Linq;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister.Controllers
{
    [SBLAuthorize(Allow = "Authenticated")]
    [Audit]
    [NoCache]
    public class OtherPaperLaidMinisterController : Controller
    {
        //
        // GET: /PaperLaidMinister/OtherPaperLaidMinister/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DepartmentDashboard()
        {
            tPaperLaidV model = new tPaperLaidV();
            model.DepartmentId = CurrentSession.DeptID;
            model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetCountForOtherPaperLaidTypes", model);
            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            return View(model);
        }

        #region Other PaperLaid
        
        [HttpGet]
        public ActionResult GetAllOtherPaperLaid(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);

            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetAllOtherPaperLaid", model);
            //    model.AssemblyID =
            //model.RowsPerPage = 10;
            //model.PageNumber = 1;
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.PAGE_SIZE = 10;
            ////    model.ResultCount = 1;
            //model.selectedPage = 1;

            // obj.actualFilePath = obj.FilePath + obj.FileName;
            return PartialView("_GetAllOtherPaperLaid", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetAllOtherPaperLaid(JqGridRequest request, tPaperLaidV customModel)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.AssemblyId = 12;
            model.SessionId = 4;
            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;

            model.MinistryId = model.MinistryId;
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetAllOtherPaperLaid", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterPendingList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<MinisterPaperMovementModel>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from PaperList in resultForGrid
                                      select new JqGridRecord<MinisterPaperMovementModel>(Convert.ToString(PaperList.PaperLaidId), new MinisterPaperMovementModel(PaperList)));
            return new JqGridJsonResult() { Data = response };
        }
        //Added code venkat for dynamic 
        [HttpGet]
        public ActionResult GetOtherPaperLaidPendings(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, int? BusinessType, string Subject)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetHashKeyByUIserId", model);
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;
            model.HashKey = model.HashKey;

            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);
            //Added code venkat for dynamic 
            model.BType = BusinessType;
            model.Subject = Subject;
            //end----------------------------------
            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            model.DSCApplicable = siteSettingMod.DSCApplicable;

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPaperLaidPendingsDetails", model);
            //    model.AssemblyID =
            //model.RowsPerPage = 10;
            //model.PageNumber = 1;
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.PAGE_SIZE = 10;
            ////    model.ResultCount = 1;
            //model.selectedPage = 1;

            // obj.actualFilePath = obj.FilePath + obj.FileName;
            return PartialView("_GetPendingListOtherPaperLaid", model);
        }
        //Added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetOtherPaperLaidPendings(JqGridRequest request, int? BusinessType, string Subject)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.AssemblyId = 12;
            model.SessionId = 4;
            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            //    Obj.LoginId=
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetHashKeyByUIserId", model);

            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;
            model.BType = BusinessType;
            //Added code venkat for dynamic 
            model.Subject = Subject;
            model.MinistryId = model.MinistryId;
            //end-------------------------------
            model.HashKey = CurrentSession.UserHashKey;

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPaperLaidPendingsDetails", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterOtherPaperDraft.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<MinisterOtherPaperDraft>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from PaperList in resultForGrid
                                      select new JqGridRecord<MinisterOtherPaperDraft>(Convert.ToString(PaperList.PaperLaidId), new MinisterOtherPaperDraft(PaperList)));
            return new JqGridJsonResult() { Data = response };
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult GetOtherPaperLaidPendings(JqGridRequest request, tPaperLaidV customModel)
        //{
        //    tPaperLaidV model = new tPaperLaidV();
        //    model.PAGE_SIZE = request.RecordsCount;

        //    model.PageIndex = request.PageIndex;
        //    model.AssemblyId = 12;
        //    model.SessionId = 4;
        //    model.PaperLaidId = 5;

        //    if (CurrentSession.UserName != null && CurrentSession.UserName != "")
        //    {
        //        model.LoginId = CurrentSession.UserName;
        //    }


        //    if (CurrentSession.UserID != null && CurrentSession.UserID != "")
        //    {
        //        model.UserID = new Guid(CurrentSession.UserID);
        //    }

        //    //    Obj.LoginId=
        //    model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);

        //    model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetHashKeyByUIserId", model);

           


        //    SiteSettings siteSettingMod = new SiteSettings();
        //    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
        //    model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
        //    model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

        //    model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
        //    model.DepartmentId = CurrentSession.DeptID;

        //    model.MinistryId = model.MinistryId;

        //    model.HashKey = CurrentSession.UserHashKey;

        //    var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPaperLaidPendingsDetails", model);

        //    JqGridResponse response = new JqGridResponse()
        //    {
        //        TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
        //        PageIndex = request.PageIndex,
        //        TotalRecordsCount = result.ResultCount
        //    };

        //    var resultToSort = result.MinisterPendingList.AsQueryable();

        //    var resultForGrid = resultToSort.OrderBy<MinisterPaperMovementModel>(request.SortingName, request.SortingOrder.ToString());

        //    response.Records.AddRange(from PaperList in resultForGrid
        //                              select new JqGridRecord<MinisterPaperMovementModel>(Convert.ToString(PaperList.PaperLaidId), new MinisterPaperMovementModel(PaperList)));

        //    return new JqGridJsonResult() { Data = response };
        //}

        public ActionResult FilterByBusinessType(int PaperCategoryTypeId, int CategoryTypeId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount, string view)
        {
            if (CurrentSession.UserID != null)
            {

                PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
               // ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

                #region Paper Category Type Information

                mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
                List<mEvent> PaperCategoryTypeList = Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidPaperCategoryType", PaperCategoryTypeListMod) as List<mEvent>;
                tPaperLaidDepartmentModel.mEvents = PaperCategoryTypeList;

                #endregion

                tPaperLaidV objMovement = new tPaperLaidV();
                objMovement.PaperCategoryTypeId = PaperCategoryTypeId;
                objMovement.EventId = PaperCategoryTypeId;
                PaperMovementModel objPaperModel = new PaperMovementModel();
                objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
                objPaperModel.PageIndex = int.Parse(PageNumber);
                //objPaperModel.ResultCount = int.Parse(ResultCount);
                objPaperModel.PaperCategoryTypeId = PaperCategoryTypeId;
                if (view != "Submit")
                {
                    tPaperLaidDepartmentModel.DepartmentPendingList = (ICollection<PaperMovementModel>)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidPendingsDetails", objPaperModel);
                }
                else {
                    tPaperLaidDepartmentModel.DepartmentPendingList = (ICollection<PaperMovementModel>)Helper.ExecuteService("OtherPaperLaid", "GetSubmittedOtherPaperLaidList", objPaperModel);
           
                }
                tPaperLaidDepartmentModel.EventId = objMovement.EventId;
                if (tPaperLaidDepartmentModel.DepartmentPendingList.Count > 0)
                {
                    objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentPendingList.Count;
                }
                tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
                tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


                tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
                }

                if (RowsPerPage != null && RowsPerPage != "")
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
                }
                else
                {
                    tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("20");
                }
                if (PageNumber != null && PageNumber != "")
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
                }
                else
                {
                    tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
                }

                if (loopStart != null && loopStart != "")
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
                }

                if (loopEnd != null && loopEnd != "")
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
                }
                else
                {
                    tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
                }
                if (view != "Submit")
                {
                    return PartialView("_GetPendingListOtherPaperLaid", tPaperLaidDepartmentModel);
                }
                else {
                    return PartialView("_GetPaperLaidSubmittList", tPaperLaidDepartmentModel);
                }
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }
        
        public ActionResult EditPendingRecordDashboard(string paperLaidId)
        {
            if (CurrentSession.UserID != "")
            {
                tPaperLaidV model = new tPaperLaidV();

                model.PaperLaidId = Convert.ToInt32(paperLaidId);
                model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "EditPendingOtherPaperLaidDetials", model);

                mDepartment departmentModel = new mDepartment();
                mEvent mEventModel = new mEvent();
                tPaperLaidV siteSetting = new tPaperLaidV();
                siteSetting = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", siteSetting);
                model.SessionId = siteSetting.SessionCode;
                model.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("OtherPaperLaid", "GetSessionDates", model);
                model.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("OtherPaperLaid", "GetDocumentType", model);
                model.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
                model.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
                //model.DepartmentId = CurrentSession.DeptID;
                //    model.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistry", mMinisteryMinisterModel);
                tPaperLaidV ministryDetails = new tPaperLaidV();
                ministryDetails.DepartmentId = CurrentSession.DeptID;
                model.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministryDetails);

                //mEventModel.EventId = 0;
                //mEventModel.EventName = "Select Category";
                model.mEvents.Add(mEventModel);
                return PartialView("_GetPaperEntryForDashBoard", model);
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PaperLaidSubmittFromDashBoard(HttpPostedFileBase file, tPaperLaidV obj, string submitButton)
        {
            if (CurrentSession.UserID != null)
            {
                submitButton = submitButton.Replace(" ", "");
                tPaperLaidV mdl = new tPaperLaidV();
                tPaperLaidTemp mdlTemp = new tPaperLaidTemp();

                switch (submitButton)
                {
                    case "Save":
                        {
                            if (obj.PaperLaidId != 0)
                            {
                                mdl = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "UpdateOtherPaperLaidEntry", obj);

                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("OtherPaperLaid", "GetExisitngFileDetailsForOtherPaperLaid", mdl);

                                if (file != null)
                                {
                                    // string Url = "~/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/" + mdl.RespectivePaper + "/";
                                    string Url = mdlTemp.FilePath;
                                    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));

                                    if (!Dir.Exists)
                                    {
                                        Dir.Create();
                                    }

                                    string ext = Path.GetExtension(file.FileName);

                                    string fileName = file.FileName.Replace(ext, "");
                                    tPaperLaidV siteSetting = new tPaperLaidV();
                                    siteSetting = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", siteSetting);
                                    obj.SessionId = siteSetting.SessionCode;
                                    obj.AssemblyId = siteSetting.AssemblyCode;

                                    string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + "O_" + obj.PaperLaidId + "_v1" + ext;
                                    file.SaveAs(Server.MapPath(Url + actualFileName));
                                    mdlTemp.FileName = actualFileName;
                                }

                                mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("OtherPaperLaid", "UpdateDetailsInTempOtherPaperLaid", mdlTemp);
                            }
                            else
                            {
                                SaveOtherPaperLaid(file, obj);
                            }
                            break;
                        }

                    case "Send":
                        {
                            if (obj.PaperLaidId == 0)
                            {
                                long papaerLaidId = SaveOtherPaperLaid(file, obj);
                                tPaperLaidTemp model = new tPaperLaidTemp();
                                tPaperLaidV mod = new tPaperLaidV();
                                model.PaperLaidId = Convert.ToInt32(papaerLaidId);
                                model.DeptSubmittedBy = CurrentSession.UserName;
                                model.DeptSubmittedDate = DateTime.Now;
                                model = (tPaperLaidTemp)Helper.ExecuteService("OtherPaperLaid", "SubmitPendingDepartmentOtherPaperLaid", model);
                                mod = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "UpdateOthePaperLaidDeptActiveId", model);
                            }
                            else
                            {
                                tPaperLaidTemp model = new tPaperLaidTemp();
                                tPaperLaidV mod = new tPaperLaidV();
                                model.PaperLaidId = Convert.ToInt32(obj.PaperLaidId);
                                model.DeptSubmittedBy = CurrentSession.UserName;
                                model.DeptSubmittedDate = DateTime.Now;
                                model = (tPaperLaidTemp)Helper.ExecuteService("OtherPaperLaid", "SubmitPendingDepartmentOtherPaperLaid", model);
                                mod = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "UpdateOthePaperLaidDeptActiveId", model);
                            }
                            break;
                        }
                }
                return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }

        public ActionResult GetOtherPaperEntry()
        {
            if (CurrentSession.UserID != null)
            {
                tPaperLaidV paperLaidModel = new tPaperLaidV();
                mDepartment departmentModel = new mDepartment();
                mEvent mEventModel = new mEvent();
                paperLaidModel.DepartmentId = CurrentSession.DeptID;
                tPaperLaidV ministerDetails = new tPaperLaidV();
                ministerDetails.DepartmentId = paperLaidModel.DepartmentId;
                tPaperLaidV siteSetting = new tPaperLaidV();
                siteSetting = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", siteSetting);
                paperLaidModel.SessionId = siteSetting.SessionCode;
                paperLaidModel.AssemblyCode = siteSetting.AssemblyCode;
                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                paperLaidModel.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("OtherPaperLaid", "GetSessionDates", paperLaidModel);

                paperLaidModel.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("OtherPaperLaid", "GetDocumentType", paperLaidModel);
                paperLaidModel.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
                paperLaidModel.mEvents = (ICollection<mEvent>)Helper.ExecuteService("OtherPaperLaid", "GetAllEventsByOtherPaperLaid", mEventModel);

                paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);
                //   paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("OtherPaperLaid", "GetMinisterMinistryName", paperLaidModel.DepartmentId);
                //   paperLaidModel.MinistryId = mMinisteryMinisterModel.MinistryID;
                return PartialView("_GetPaperEntryForDashBoard", paperLaidModel);
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }
        //Added code venkat for dynamic 
        [HttpGet]
        public ActionResult GetUpComingOtherPaperLaid(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, int? BusinessType, string Subject)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;
            //Added code venkat for dynamic 
            model.BType = BusinessType;
            model.Subject = Subject;
            //end----------------------------------
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);

            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "UpcomingLOBByOtherpaperLaidId", model);
          
           // model.RowsPerPage = 10;
           // model.PageNumber = 1;
           // model.loopEnd = Convert.ToInt16(loopEnd);
           // model.loopStart = Convert.ToInt16(loopStart);
           // model.PageIndex = Convert.ToInt16(PageNumber);
           // model.PAGE_SIZE = 10;
            
           //model.selectedPage = 1;

            return PartialView("_GetUpComingPaperLaidList", model);
        }
        //Added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetUpComingOtherPaperLaid(JqGridRequest request, int? BusinessType, string Subject)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.AssemblyId = 12;
            model.SessionId = 4;
            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);

            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;

            model.MinistryId = model.MinistryId;
            //Added code venkat for dynamic 
            model.BType = BusinessType;
            model.Subject = Subject;
            //end------------------------------
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "UpcomingLOBByOtherpaperLaidId", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterPendingList.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<MinisterPaperMovementModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from PaperList in resultForGrid
                                      select new JqGridRecord<MinisterPaperMovementModel>(Convert.ToString(PaperList.PaperLaidId), new MinisterPaperMovementModel(PaperList)));

            return new JqGridJsonResult() { Data = response };
        }
        //Added code venkat for dynamic menu
        [HttpGet]
        public ActionResult OtherPaperLaidInHouse(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, int? BusinessType, string Subject)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;


            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);
            //Added code venkat for dynamic menu
            model.BType = BusinessType;
            model.Subject = Subject;
            //end----------------------------------

            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPaperLaidInHouseByType", model);

            //model.RowsPerPage = 10;
            //model.PageNumber = 1;
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.PAGE_SIZE = 10;

            //model.selectedPage = 1;

            return PartialView("_GetPaperLaidInHouseList", model);
        }
        //Added code venkat for dynamic menu
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OtherPaperLaidInHouse(JqGridRequest request, int? BusinessType, string Subject)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.AssemblyId = 12;
            model.SessionId = 4;
            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;

            model.MinistryId = model.MinistryId;
            //Added code venkat for dynamic menu
            model.BType = BusinessType;
            model.Subject = Subject;
            //end-------------------------------
            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingQuestionsByType", model);



            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPaperLaidInHouseByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterPendingList.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<MinisterPaperMovementModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from PaperList in resultForGrid
                                      select new JqGridRecord<MinisterPaperMovementModel>(Convert.ToString(PaperList.PaperLaidId), new MinisterPaperMovementModel(PaperList)));

            return new JqGridJsonResult() { Data = response };
        }
        //Added code venkat for dynamic 
        [HttpGet]
        public ActionResult OtherPaperPendingToLay(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, int? BusinessType, string Subject)
        {

            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;
            //Added code venkat for dynamic 
            model.Subject = Subject;
            model.BType = BusinessType;
            //end---------------------------

            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);

            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }
            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingToLayOtherPaperLaidByType", model);

            //model.RowsPerPage = 10;
            //model.PageNumber = 1;
            //model.loopEnd = Convert.ToInt16(loopEnd);
            //model.loopStart = Convert.ToInt16(loopStart);
            //model.PageIndex = Convert.ToInt16(PageNumber);
            //model.PAGE_SIZE = 10;

            //model.selectedPage = 1;

            return PartialView("_GetPaperPendinToLayList", model);
        }
        //Added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OtherPaperPendingToLay(JqGridRequest request, int? BusinessType, string Subject)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.AssemblyId = 12;
            model.SessionId = 4;
            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;

            model.MinistryId = model.MinistryId;
            //Added code venkat for dynamic 
            model.BType = BusinessType;
            model.Subject = Subject;
            //end------------------------------
            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingQuestionsByType", model);



            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingToLayOtherPaperLaidByType", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterPendingList.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<MinisterPaperMovementModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from PaperList in resultForGrid
                                      select new JqGridRecord<MinisterPaperMovementModel>(Convert.ToString(PaperList.PaperLaidId), new MinisterPaperMovementModel(PaperList)));

            return new JqGridJsonResult() { Data = response };
        }
        //Added code venkat for dynamic 
        [HttpGet]
        public ActionResult GetOtherPaperLaidSubmitList(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, int? BusinessType, string Subject)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;
            //Added code venkat for dynamic 
            model.BType = BusinessType;
            model.Subject = Subject;
            //end--------------------------------
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);

            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetSubmittedOtherPaperLaidList", model);
            //    model.AssemblyID =
            model.RowsPerPage = 10;
            model.PageNumber = 1;
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.loopStart = Convert.ToInt16(loopStart);
            model.PageIndex = Convert.ToInt16(PageNumber);
            model.PAGE_SIZE = 10;
            //    model.ResultCount = 1;
            model.selectedPage = 1;


            // obj.actualFilePath = obj.FilePath + obj.FileName;
            return PartialView("_GetPaperLaidSubmittList", model);
        }
        //Added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetOtherPaperLaidSubmitList(JqGridRequest request, int? BusinessType, string Subject)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            model.AssemblyId = 12;
            model.SessionId = 4;
            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


            SiteSettings siteSettingMod = new SiteSettings();
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);


            if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                CurrentSession.AssemblyId = model.AssemblyId.ToString();
            }

            if (string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                CurrentSession.SessionId = model.SessionId.ToString();
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;

            model.MinistryId = model.MinistryId;
            //Added code venkat for dynamic 
            model.BType = BusinessType;
            model.Subject = Subject;
            //end-------------------------------------
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetSubmittedOtherPaperLaidList", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterPendingList.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<MinisterPaperMovementModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from PaperList in resultForGrid
                                      select new JqGridRecord<MinisterPaperMovementModel>(Convert.ToString(PaperList.PaperLaidId), new MinisterPaperMovementModel(PaperList)));

            return new JqGridJsonResult() { Data = response };
        }
        
        public long SaveOtherPaperLaid(HttpPostedFileBase file, tPaperLaidV obj)
        {
            tPaperLaidV siteSetting = new tPaperLaidV();
            tPaperLaidV mdl = new tPaperLaidV();
            tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
            siteSetting = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", siteSetting);
            obj.SessionId = siteSetting.SessionCode;
            obj.AssemblyId = siteSetting.AssemblyCode;
            obj = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
            mdl = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "SubmittOtherPaperLaidEntry", obj);
            mdl.EventId = obj.EventId;
            int paperLaidId = (int)mdl.PaperLaidId;
            mdl = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetFileSaveDetailsForOtherPaperLaid", mdl);
            if (file != null)
            {
                string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                if (!Dir.Exists)
                {
                    Dir.Create();
                }
                string ext = Path.GetExtension(file.FileName);
                string fileName = file.FileName.Replace(ext, "");

                string actualFileName = obj.AssemblyId+"_"+obj.SessionId+"_"+"O_" + paperLaidId + "_v" + mdl.Count + ext;

                file.SaveAs(Server.MapPath(Url + actualFileName));
                mdlTemp.FileName = actualFileName;
                mdlTemp.FilePath = Url;
                mdlTemp.Version = mdl.Count;
                mdlTemp.PaperLaidId = mdl.PaperLaidId;

            }
            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("OtherPaperLaid", "SaveDetailsInTempOtherPaperLaid", mdlTemp);
            return mdl.PaperLaidId;
        }

        public ActionResult ShowOtherPaperLaidDetailByID(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByID", movementModel);
                return PartialView("_GetOtherPaperLaidSubmittedListById", movementModel);
            }
            return null;
        }

        public ActionResult FullViewOtherPaperLaidDetailByID(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByID", movementModel);
                return PartialView("_ShowFullDetailByOtherPaperLaidId", movementModel);
            }
            return null;
        }

        public ActionResult GetSubmittedOtherPaperLaidByID(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {
                PaperMovementModel movementModel = new PaperMovementModel();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (PaperMovementModel)Helper.ExecuteService("OtherPaperLaid", "ShowOtherPaperLaidDetailByID", movementModel);
                return PartialView("_GetSubmittedOtherPaperLaidByID", movementModel);
            }
            return null;
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateOtherPaperLaidFile(HttpPostedFileBase file, tPaperLaidV tPaper)
        {
            if (CurrentSession.UserID != "")
            {
                tPaperLaidTemp tPaperTemp = new tPaperLaidTemp();
                PaperMovementModel obj = new PaperMovementModel();
                obj.PaperLaidId = tPaper.PaperLaidId;
                obj.actualFilePath = tPaper.FilePath;
                tPaperLaidV model = new tPaperLaidV();
                tPaperLaidV siteSetting = new tPaperLaidV();
                siteSetting = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", siteSetting);
                obj.SessionId = siteSetting.SessionCode;
                obj.AssemblyId = siteSetting.AssemblyCode;
                model = (tPaperLaidV)Helper.ExecuteService("OtherPaperLaid", "GetOtherPaperLaidFileVersion", obj);
                if (model != null)
                {
                    if (file != null)
                    {
                        string Url = obj.actualFilePath;
                        DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        string ext = Path.GetExtension(file.FileName);
                        string fileName = file.FileName.Replace(ext, "");
                        string actualFileName = obj.AssemblyId+"_"+obj.SessionId+"_"+"O_"+obj.PaperLaidId + "_v" + model.Count + ext;
                        file.SaveAs(Server.MapPath(Url + actualFileName));
                        tPaperTemp.FileName = actualFileName;
                        tPaperTemp.FilePath = Url;
                        tPaperTemp.PaperLaidId = obj.PaperLaidId;
                        tPaperTemp.Version = model.Count;
                        tPaperTemp.DeptSubmittedBy = CurrentSession.DeptID;
                        tPaperTemp.DeptSubmittedDate = DateTime.Now;
                    }
                    tPaperTemp = (tPaperLaidTemp)Helper.ExecuteService("OtherPaperLaid", "UpdateOtherPaperLaidFileByID", tPaperTemp);
                    TempData["valID"] = 1;
                }
            }
          //  return RedirectToAction("/PaperLaidDepartment/DepartmentDashboard");
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }
        #endregion
    }
}

